module.exports = {
  cacheDirectory: './.tmp/jest',
  coverageDirectory: './.tmp/coverage',
  moduleNameMapper: {
    '^.+\\.(css|scss)$': 'identity-obj-proxy'
  },
  modulePaths: ['<rootDir>'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  globals: {
    NODE_ENV: 'test',
    WebsocketHandler: true,
    __DEV__: true
  },
  verbose: true,
  testRegex: '(/__tests__/.*\\.(test))\\.(ts|tsx|js)$',
  testPathIgnorePatterns: ['/node_modules/', '/__tests__/mocks/.*'],
  coveragePathIgnorePatterns: ['typings.d.ts'],
  transformIgnorePatterns: ['.*(node_modules)(?!.*torn.*).*$'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.tsx?$': 'ts-jest'
  },
  setupFiles: [
    './setupTests.js'
    // '<rootDir>/node_modules/whatwg-fetch/fetch.js' // should be removed in future due to the tests running collision
  ],
  snapshotSerializers: ['enzyme-to-json/serializer']
}
