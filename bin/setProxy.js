const { modifyRemoteFile, writeRemoteFile } = require('./helpers/remoteAccessUtils')
const { proxyURL } = require('../config/boxConfig')

const remoteTemplate = `./torn/${process.env.remoteStaticPath}app-template.php`
const remoteVersionMD = `./torn/${process.env.remoteStaticPath}VERSION.MD`

const changeTemplate = str => str.replace(/(href|src)="(.*)\.(css|js)"/gm, `$1="${proxyURL}/app.$3"`)

modifyRemoteFile(remoteTemplate, changeTemplate)
writeRemoteFile(remoteVersionMD, 'manual')
