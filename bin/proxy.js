const { createProxyMiddleware } = require('http-proxy-middleware')
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const { argv } = yargs(hideBin(process.argv))
let { boxURL } = require('../config/boxConfig')

boxURL = argv.boxURL || boxURL

const proxyFiles = [
  '/js/**',
  '/status_monitor/**',
  '/css/**',
  '/images/**',
  '/fonts/**',
  '/casino/**',
  '/audio/**',
  '/builds/**',
  '/min/**',
  '/cdn-cgi/**',
  '**/favicon.ico',
  '**/manifest.json',
  '**.php' // able to load real Torn pages
]

const commonProxyMiddleware = createProxyMiddleware(proxyFiles, {
  target: boxURL,
  changeOrigin: true,
  auth: 'staff:Zuff3qaMvlZlGtZT6',
  ws: true,
  secure: true,
  cookieDomainRewrite: 'localhost',
  headers: {
    origin: boxURL,
    Connection: 'keep-alive',
    'Access-Control-Allow-Origin': '*'
  },
  onProxyRes: proxyRes => {
    // it's a hack for activating ServiceWorkers while local developing is going on
    if (proxyRes.req.path.match(/(sw\.js)$/i)) {
      proxyRes.headers['Service-Worker-Allowed'] = '/'
    }
  }
})

const centrifugeProxyMiddleware = createProxyMiddleware(
  ['wss://ws-centrifugo.torncity.com/connection/websocket', '/connection/websocket'],
  {
    target: 'wss://ws-centrifugo.torncity.com/connection/websocket',
    changeOrigin: true,
    ws: true,
    secure: true,
    cookieDomainRewrite: 'localhost',
    headers: {
      origin: 'wss://ws-centrifugo.torncity.com/connection/websocket',
      Connection: 'keep-alive',
      'Access-Control-Allow-Origin': '*'
    }
  }
)

// TODO: it doesn't work with requests emitted externally for now.
// Works in case of injection inside some react app
// Need some better investigation on how to put this on work globally.
const chatProxyMiddleware = createProxyMiddleware(['https://ws-chat.torn.com/chat/status', '/chat/status'], {
  target: 'https://ws-chat.torn.com',
  changeOrigin: true,
  ws: true,
  secure: true,
  cookieDomainRewrite: 'localhost',
  headers: {
    origin: 'https://ws-chat.torn.com',
    Connection: 'keep-alive',
    'Access-Control-Allow-Origin': '*'
  }
})

module.exports = {
  commonProxyMiddleware,
  centrifugeProxyMiddleware,
  chatProxyMiddleware
}
