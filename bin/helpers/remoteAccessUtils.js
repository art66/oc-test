const Client = require('ssh2-sftp-client')
const fs = require('fs')

const credentials = require('../../config/credentials')
const privateKey = fs.readFileSync(credentials.privateKey)
const connectionConfig = { ...credentials, privateKey }

const modifyRemoteFile = (remoteFile, callback) => {
  const sftp = new Client()

  return sftp
    .connect(connectionConfig)
    .then(() => sftp.get(remoteFile))
    .then(buf => sftp.put(Buffer.from(callback(buf.toString()), 'utf8'), remoteFile))
    .catch(err => console.log(err))
    .finally(() => sftp.end())
}

const writeRemoteFile = (remoteFile, dataToWrite) => {
  const sftp = new Client()

  return sftp
    .connect(connectionConfig)
    .then(() => sftp.put(Buffer.from(dataToWrite, 'utf8'), remoteFile))
    .catch(err => console.log(err))
    .finally(() => sftp.end())
}

module.exports = {
  modifyRemoteFile,
  writeRemoteFile
}
