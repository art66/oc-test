const { writeRemoteFile } = require('./helpers/remoteAccessUtils')

const remoteVersionMD = `./torn/${process.env.remoteStaticPath}VERSION.MD`

writeRemoteFile(remoteVersionMD, '')
