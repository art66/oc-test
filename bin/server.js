/**
 *  @name server
 *  @author 3p-dima, harabara, 3p-sviat, catherinei
 *  @version 1.0.0
 *  @description main app's local server that serv all the app static to run
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

const webpack = require('webpack')
const express = require('express')
const port = 3000
const app = express()
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const proxyMiddleware = require('./proxy')
const webpackConfig = require('../config/webpack-dev.js')

const startWebpackServer = () => {
  const devMiddlewareOptions = {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'OPTIONS, GET, POST, PUT, PATCH, DELETE',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization'
    },
    stats: {
      version: true,
      warnings: true,
      outputPath: true,
      entrypoints: false,
      chunks: true,
      children: false,
      chunkModules: true,
      colors: true,
      modules: true,
      timings: true,
      maxModules: 15,
      errorDetails: true
    },
    publicPath: webpackConfig.output.publicPath
  }
  const bundler = webpack(webpackConfig)

  app.use(proxyMiddleware.commonProxyMiddleware)
  app.use(proxyMiddleware.centrifugeProxyMiddleware)
  app.use(proxyMiddleware.chatProxyMiddleware)
  app.use(webpackDevMiddleware(bundler, devMiddlewareOptions))
  app.use(webpackHotMiddleware(bundler))

  app.listen(port, err => {
    if (err) {
      console.error(err)
    } else if (process.argv.length !== 3) {
      console.info('==> Webpack development server listening on port %s', port)
    }
  })
}

startWebpackServer()
