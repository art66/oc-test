/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
/**
 *  @name uploader
 *  @author 3p-dima, 3p-harabara, 3p-sviat
 *  @version 1.0.0
 *  @description upload function that helps us send all static bundles on dev server without any tear or effort.
 *
 *  @property {object} credentials - a config file with personal data about of the uploading dev
 *  @property {object} uploadConfig - holds all info about app to upload on the server
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

const Client = require('ssh2-sftp-client')
const walk = require('walk')
const fs = require('fs')
const debug = require('debug')

const debugRoot = debug('app:deploy::rooots')
const debugRecreatePaths = debug('app:deploy::create')
const debugUpload = debug('app:deploy::upload')
const debugFinallize = debug('app:deploy::finale')
const debugError = debug('app:deploy::errors')

const remotePaths = require('../config/remotePaths')
const localPaths = require('../config/localPaths')
const credentials = require('../config/credentials')

const ARROW_CHECK_ICON = '✔'
const POLICE_ALARM_ICON = '🚨'
const FROM_ICON = '➡'

const sftp = new Client()

const filesProcessors = (files, distFolder, remoteDir) => ({
  uploadFiles: async () => {
    for (const file of files) {
      const localPath = `${distFolder}/${file}`
      const serverPathData = remoteDir + file

      await sftp
        .put(localPath, serverPathData)
        .then(() => {
          debugUpload('\x1b[32m', `${FROM_ICON} from ${localPath}`)
          debugUpload('\x1b[32m', `${ARROW_CHECK_ICON} to   ${serverPathData}`)
        })
        .catch(e => debugError('\x1b[41m%s\x1b[0m', '=== Something bad just happened. See the error log: ', e))
    }
  }
})

const walkerCore = (remoteConfig, localConfig) => {
  const files = []

  const walker = walk.walk(localConfig.app_public, { followLinks: false })
  const { uploadFiles } = filesProcessors(files, localConfig.app_public, remoteConfig.remote_static_base)

  const uploadSteps = {
    recreatePaths: async () => {
      debugRecreatePaths('=== Step 1: Recreating bundles path... ===')
      debugRecreatePaths('\x1b[35m', `${ARROW_CHECK_ICON} mkdir ${remoteConfig.remote_static_base}`)

      await sftp
        .mkdir(remoteConfig.remote_static_base)
        .then(() => debugRecreatePaths('=== [DONE] Step 1: Assets path is recreated successful! ==='))
        .catch(() => {
          debugRecreatePaths('\x1b[35m', 'Seems like directory already exists')
        })
    },
    startUploadFiles: async () => {
      debugUpload('=== Step 2: Uploading files...')

      await uploadFiles().then(() => debugUpload('=== [DONE] Step 2: All files were copied! ==='))
    },
    finallizeUpload: () => {
      debugFinallize(
        '\x1b[33m%s\x1b[0m',
        `=== ${POLICE_ALARM_ICON}  [FINISH] All processes were finish. You're good to go! ===`
      )

      process.exit(0)
    }
  }

  const collectFilesToUpload = () => {
    walker.on('file', (root, stat, next) => {
      if (!root) {
        return
      }

      files.push(stat.name)
      next()
    })
  }

  const processAllFilesToUpload = () => {
    const privateKey = fs.readFileSync(credentials.privateKey)
    const connectionConfig = { ...credentials, privateKey }
    const { recreatePaths, startUploadFiles, finallizeUpload } = uploadSteps

    walker.on('end', async () => {
      if (!credentials.username) {
        debug('Please create or fill credentials.js properly in the ./config directory')

        return
      }

      try {
        await sftp.connect(connectionConfig)
        await recreatePaths()
        await startUploadFiles()
        await finallizeUpload()
      } catch (err) {
        debugError('\x1b[41m%s\x1b[0m', 'Something went wrong:')
        debugError('\x1b[41m%s\x1b[0m', err)

        process.exit(1)
      }
    })
  }

  return {
    collectFilesToUpload,
    processAllFilesToUpload
  }
}

const upload = (remoteConfig, localConfig) => {
  debugRoot('\x1b[33m%s\x1b[0m', `=== ${POLICE_ALARM_ICON}  [START] Starting deploing ===`)
  const { collectFilesToUpload, processAllFilesToUpload } = walkerCore(remoteConfig, localConfig)

  collectFilesToUpload()
  processAllFilesToUpload()
}

module.exports = upload(remotePaths, localPaths)
