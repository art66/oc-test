if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

git_root_path=$(git rev-parse --show-toplevel)
rm -rf  $git_root_path/${remoteStaticPath}
