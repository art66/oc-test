if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

git_root_path=$(git rev-parse --show-toplevel)
yarn prettier --config $git_root_path/prettier.config.js ./**/[a-zA-Z]+."(js|ts|tsx)" --write
yarn eslint -c $git_root_path/.eslintrc.js --ignore-path $git_root_path/.eslintignore ./**/*.+"(js|jsx|ts|tsx)" --fix
yarn stylelint ./**/*."(scss)" --config $git_root_path/.stylelintrc --fix
