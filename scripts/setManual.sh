if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi
git_root_path=$(git rev-parse --show-toplevel)
node $git_root_path/bin/setManual
