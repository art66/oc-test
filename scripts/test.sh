appPath=$PWD
git_root_path=$(git rev-parse --show-toplevel)
cd $git_root_path
yarn jest "$appPath"
