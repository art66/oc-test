# TOC

- [Torn's Core Changelog](CHANGELOG.md)
- [Tests suits](docs/tests.md)
- [Basic Workflow](docs/workflow.md)
- [Git cheatsheet](docs/git.md)
- [Components](docs/components.md)
- [E2E Testing](docs/e2e_tests.md)
