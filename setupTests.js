// TODO: Remove these polyfills once the below issue is sorted
// https://github.com/facebookincubator/create-react-app/issues/3199#issuecomment-332842582
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

global.requestAnimationFrame = cb => {
  setTimeout(cb, 0)
}

const matchMedia = () => ({
  matches: false,
  addListener: () => {},
  removeListener: () => {}
})

global.matchMedia = window && window.matchMedia || matchMedia

global.MutationObserver = class {
  constructor(callback) {
    this._callback = callback
  }

  disconnect = () => {}

  _mockedClassListGenerator = () => { // mocked just for unit tests in desktopLayoutChecker
    ['d', 't', 'r', 'm', 'r', 'dark-mode'].forEach((key, index) => {
      setTimeout(() => {
        document.body.classList = key

        this._callback([{
          type: 'attributes'
        }])
      }, index * 500)
    })
  }

  observe = (element, initObject) => { // eslint-disable-line
    this._mockedClassListGenerator()
  }
}

global.WebsocketHandler = class {
  setActions = callback => callback
}

global.getAction = class {
  action = ''
  success = () => {}
}

Enzyme.configure({ adapter: new Adapter() })
