import centered from '@storybook/addon-centered/react'
import { withKnobs } from '@storybook/addon-knobs'
import { withMobileMode } from './addons'

import { addTornsRegularLayout, createSVGGeneratorContainerNode } from './utils'
import webpChecker from '@torn/shared/utils/webpChecker'

import './styles/reset.css'
import './styles/storybook.scss'

// run particular Torn scripts
addTornsRegularLayout()
createSVGGeneratorContainerNode()
webpChecker({ injectBodyClass: true })

export const parameters = {
  backgrounds: {
    default: 'grey',
    values: [
      {
        name: 'grey',
        value: '#ccc'
      },
      {
        name: 'white',
        value: '#fff'
      },
      {
        name: 'black',
        value: '#111'
      }
    ]
  }
}

export const decorators = [withKnobs, withMobileMode, centered]
