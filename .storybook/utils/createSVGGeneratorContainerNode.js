const createSVGGeneratorContainerNode = () => {
  const nodesWrap = document.createElement('div')
  nodesWrap.className = 'svgs_defs_container'

  const nodeContainer = document.createElement('svg')
  nodeContainer.className = 'react_svgs_defs'
  nodeContainer.style.cssText = 'width:0;height:0;position:absolute;'

  const nodeFilters = document.createElement('defs')
  nodeFilters.className = 'react_svgs__filters'

  const nodeGradients = document.createElement('defs')
  nodeGradients.className = 'react_svgs__gradients'

  nodeContainer.append(nodeFilters)
  nodeContainer.append(nodeGradients)
  nodesWrap.append(nodeContainer)

  const rootNode = document.body
  const insertBeforeRootNode = rootNode.querySelector('#root')

  document && rootNode.insertBefore(nodesWrap, insertBeforeRootNode);
}

export default createSVGGeneratorContainerNode
