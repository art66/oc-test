const TIMER_ID = null

const appendRequiredSVGStyles = ({ defsHTML }) => {
  const svgNodeToRemove = document.querySelector('.svgs_defs_container')

  if (svgNodeToRemove && TIMER_ID) {
    clearTimeout(TIMER_ID)
  } else if (!svgNodeToRemove) {
    setTimeout(() => {
      appendRequiredSVGStyles()
    }, 1000)

    return
  }

  const generateFakeSVG = () => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svg.id = 'svg_pseudo_container'
    const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')

    defs.insertAdjacentHTML('afterbegin', defsHTML)
    svg.append(defs)

    return svg
  }

  const injectFakeSVG = () => {
    if (!document.querySelector('#svg_pseudo_container')) {
      document.body.append(generateFakeSVG())
    }
  }

  svgNodeToRemove.remove()
  injectFakeSVG()
}


export default appendRequiredSVGStyles
