// Adds an ability to test any component with mobile mode.
const withMobileMode = (story: Function) => {
  document.body.classList.add('d')
  document.body.classList.add('r')

  return story()
}

export default withMobileMode
