import React from 'react'
import Tooltips from '../../../shared/components/TooltipNew'

import { ITooltipWrapper } from './interfaces'
import styles from './index.cssmodule.scss'

const ANIM_PROPS = {
  className: 'expTooltip',
  timeExit: 50,
  timeIn: 300
}

const STYLES = {
  container: styles.tooltipContainer,
  title: styles.tooltipTitle,
  tooltipArrow: styles.tooltipArrow
}

const TooltipWrapper = ({ children }: ITooltipWrapper) => {
  return (
    <div>
      <Tooltips
        overrideStyles={STYLES}
        manualCoodsFix={{ fixX: -17 }}
        position={{ x: 'center', y: 'top' }}
        animProps={ANIM_PROPS}
      />
      {children}
    </div>
  )
}

export default TooltipWrapper
