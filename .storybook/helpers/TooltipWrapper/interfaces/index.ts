export interface ITooltipWrapper {
  children: JSX.Element
}
