# Storybook - Torn


## 2.0.0
 * MAJOR UPDATE! Migrated from v.5 to v.6.

## 1.6.0
 * Added websockets inside storybook config.

## 1.5.1
 * Fixed fake icon generator util.

## 1.5.0
 * React bundles now serves from UMD modules.
 * Added appendRequiredSVGStyles util-hack for SVG issues render solving.

## 1.4.0
 * Lifted up background options in independent constants file.
 * Created reusable HOC Component for Tooltips rendering/testing over any other component.

## 1.3.0
 * Added HTTPS support of server running.
 * Improved webpack config.

## 1.2.0
 * Added proper configuration for serving static files and their folder.

## 1.1.0
 * Improved webpack config.

## 1.0.1
 * Fixed webpack config globals.
