const webpack = require('webpack')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const globals = require('../globals/js/environments')

module.exports = {
  core: {
    builder: 'webpack5'
  },
  stories: ['../**/*.story.*'],
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-backgrounds',
    '@storybook/addon-knobs',
    '@storybook/addon-links',
    '@storybook/addon-viewport'
  ],
  webpackFinal: async config => {
    // TODO: investigate how we can implement pure webpack.config.js in Storybook!!!
    const customConfig = {
      ...config,
      plugins: [
        ...config.plugins,
        new webpack.DefinePlugin(globals),
        new MiniCssExtractPlugin({
          filename: '[name].css',
          chunkFilename: '[id].css'
        })
      ],
      module: {
        ...config.module,
        rules: [
          {
            test: /\.(js|jsx|ts|tsx|mjs)?$/,
            loader: 'ts-loader',
            options: {
              compilerOptions: {
                target: 'ES5'
              }
            },
            exclude: [/node_modules/]
          },
          {
            type: 'javascript/auto',
            test: /\.json$/,
            loader: 'json-loader'
          },
          {
            test: /\.css$/,
            use: [
              MiniCssExtractPlugin.loader,
              {
                loader: 'css-loader',
                options: {
                  url: false
                }
              },
              'postcss-loader'
            ]
          },
          {
            test: /\.scss$/,
            exclude: /\.cssmodule\.scss$/,
            use: [
              MiniCssExtractPlugin.loader,
              {
                loader: 'css-loader',
                options: {
                  url: false,
                  modules: {
                    mode: 'global'
                  }
                }
              },
              'postcss-loader',
              {
                loader: 'sass-loader',
                options: {
                  additionalData: '@import "styles/index.scss";',
                  sassOptions: {
                    includePaths: [path.resolve(__dirname, '../globals')]
                  }
                }
              }
            ]
          },
          {
            test: /(\.cssmodule\.scss)$/,
            use: [
              MiniCssExtractPlugin.loader,
              'css-modules-typescript-loader',
              {
                loader: 'css-loader',
                options: {
                  url: false,
                  modules: {
                    localIdentName: process.env.isGlobal
                      ? `${process.env.localPath}_[local]___[hash:base64:5]`
                      : '[local]___[hash:base64:5]'
                  }
                }
              },
              'postcss-loader',
              {
                loader: 'sass-loader',
                options: {
                  additionalData: '@import "styles/index.scss";',
                  sassOptions: {
                    includePaths: [path.resolve(__dirname, '../globals')]
                  }
                }
              }
            ]
          }
        ]
      },
      externals: {
        ...config.externals,
        react: 'React',
        'react-dom': 'ReactDOM'
      },
      resolve: {
        ...config.resolve,
        modules: [...config.resolve.modules, 'node_modules'],
        extensions: [...config.resolve.extensions, '.ts', '.tsx', '.js', '.jsx', '.json']
      }
    }

    return customConfig
  },
  typescript: {
    reactDocgen: 'none' // TODO: remove this once the error on 70% will be fixed (about undefined .properties)
  }
}
