# Cypress - real-world Torn E2E testing


## 3.4.1
 * Fixed login tests.

## 3.4.0
 * Move all the Cypress examples into TS.

## 3.3.1
 * Added full cypress typization.

## 3.3.0
 * Added universal util to manage and check CSS styles along with their ::pseudo.

## 3.2.0
 * Added custom commands fro getting styles from pseudo selectors on DOM nodes.

## 3.1.0
 * Move all the examples tests into independent folder.

## 3.0.1
 * Fixed roots for tsconfig.

## 3.0.0
 * Added several useful Cypress shared across utils.
 * Added Cypress service for init.
 * Improved TSconfig of Cypress for a better development.
 * Added minor code improvements in Cypress.
 * Added getMockData util into Attack tests.
 * Improved env variables along with viewport on start.

## 2.4.0
 * Significantly improved E2E tests in Attack.

## 2.3.1
 * Improved controller in Attack.

## 2.3.0
 * Improved Attack test suits.

## 2.2.0
 * Added interfaces for TS typification.

## 2.1.1
 * Fixed default imports of triggers.

## 2.1.0
 * Improved MVC architecture!

## 2.0.0
 * Created MVC architecture pattern for E2E testing!
 * Added starting kit template.

## 1.4.0
 * Created first stable arhitecture for Cypress!

## 1.3.0
 * Improved architecture.

## 1.2.0
 * Created reusable buttons pattern.

## 1.1.1
 * Improved arhitercture.

## 1.1.0
 * Updated arhitercture.

## 1.0.0
 * First stable release
