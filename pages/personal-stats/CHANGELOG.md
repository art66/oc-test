# Personal-Stats - Torn


## 1.1.1
 * Set SelectedSearch module along with its outdated colleague for a future implementation.

## 1.1.0
 * Added SelectSearch Module instead of Autocomplete (deprecated).

## 1.0.0
 * Init commit.
