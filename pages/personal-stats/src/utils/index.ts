import { COLORS, DARK_COLORS } from '../constants'

export const getUserColor = (userID: number, users: any, isDarkMode: boolean): string => {
  const colors = isDarkMode ? DARK_COLORS : COLORS

  return colors[users.findIndex(user => user === userID || (typeof user === 'object' && user.userID === userID))]
}

export const formatDateNumber = (value: number): string => {
  const result = value.toString()

  return result.length < 2 ? `0${result}` : result
}

export const formatDate = (date: Date): string => {
  const year = date.getFullYear()
  const month = formatDateNumber(date.getMonth() + 1)
  const day = formatDateNumber(date.getDate())

  return `${year}-${month}-${day}`
}

export const resetHash = (): void => {
  location.hash = ''
}

export const resetObjectData = (dates, obj) => {
  const newObj = {}

  dates.forEach((date, index) => {
    if (obj[date] || obj[date] === 0) {
      newObj[date] = obj[date]
    } else {
      newObj[date] = newObj[Object.keys(newObj)[index > 0 ? index - 1 : 0]] || 0
    }
  })

  return newObj
}
