const fetchUrl = (url, data) => {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then(response =>
      response.text().then(text => {
        try {
          var json = JSON.parse(text)
        } catch (e) {
          throw 'Malformed response'
        }
        if (json && json.error) {
          throw json.message || json.error
        }
        if (json.content) {
          throw json.content
        }
        return json
      })
    )
    .catch(e => {
      throw e
    })
}

export default fetchUrl
