import uniq from 'lodash.uniq'
import { resetObjectData } from '../utils'
import IState from '../interfaces/IState'
import IChartData from '../interfaces/IChartData'

export const getChartDateFrom = (state: IState) => state.chart.dateFrom
export const getChartDateTo = (state: IState) => state.chart.dateTo
export const getChartStatTypes = (state: IState) => state.chart.types
export const getUsersIds = (state: IState) => state.users.list.map(user => user.userID)
export const getCurrentUser = (state: IState) => state.users.currentUser
export const getStats = (state: IState) =>
  state.stats.categories.reduce((acc, cat) => acc.concat(cat.columns[0].stats.filter(s => s.chartAvailable)), [])
export const getStatsCharts = (state: IState) => {
  const { data } = state.chart
  const servers = {}

  data
    && data?.data
    && Object.values(data.data).forEach((item, index) => {
      const el1 = data.definitions[Object.keys(data.data)[index]]

      item.forEach(el => {
        const obj = {}

        el.data.map(it => {
          const date = new Date(it.time * 1000)
          const utcTime = (date.getTime() + date.getTimezoneOffset() * 60 * 1000) / 1000

          obj[utcTime] = it.value

          return obj
        })
        const playerName = data.definitions[el.uid]
        const element = `${playerName} (${el1})`
        const player = `${playerName} [${el.uid}]`

        const type = data.definitions[Object.keys(data.data)[index]]

        servers[element] = {
          chartData: obj,
          player,
          type
        }
      })
    })

  let dates = []

  Object.values(servers).forEach((stat: IChartData) => {
    dates.push(...Object.keys(stat.chartData))
  })

  dates = uniq(dates.map(Number))
  dates.sort((a, b) => (a > b ? 1 : -1))

  const newObj = Object.entries(servers).map((stat: [string, IChartData]) => {
    return {
      ...stat[1],
      chartData: resetObjectData(dates, stat[1].chartData),
      key: stat[0]
    }
  })

  newObj.forEach(item => {
    servers[item.key] = item
  })

  return servers
}
