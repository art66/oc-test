import { handleActions } from 'redux-actions'
import { users as actions } from '../actions/actionTypes'
import User from '../interfaces/IUser'
import { getCookie } from '@torn/shared/utils'
import { MAX_USERS } from '../constants'

const userID = parseInt(getCookie('uid'), 10)

interface IState {
  currentUser: User
  list: User[]
}

interface IAction {
  type: string
  payload: any
}

const initialState = {
  currentUser: {
    userID,
    playername: ''
  },
  list: []
}

export default handleActions(
  {
    [actions.ADD_USER](state: IState, action: IAction) {
      return {
        ...state,
        list: state.list
          .filter(user => user.userID !== action.payload.user.userID)
          .concat([action.payload.user])
      }
    },
    [actions.REMOVE_USER](state: IState, action: IAction) {
      return {
        ...state,
        list: state.list.filter(user =>  user.userID !== action.payload.userID)
      }
    },
    [actions.SET_CURRENT_USER](state: IState, action: IAction) {
      return {
        ...state,
        currentUser: action.payload,
        list: (state.list.length === 0) ? [action.payload] : state.list
      }
    },
    [actions.SET_USERS](state: IState, action: IAction) {
      return {
        ...state,
        list: action.payload.slice(0, MAX_USERS)
      }
    },
    [actions.CLEAR_USERS](state: IState) {
      return {
        ...state,
        list: [state.currentUser]
      }
    }
  },
  initialState
)
