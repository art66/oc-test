import { handleActions } from 'redux-actions'
import { chart as chartActions, statTypes as statTypesActions } from '../actions/actionTypes'
import { formatDate } from '../utils'
import { MAX_STATS } from '../constants'
/* global getCurrentTimestamp */

const date = new Date(getCurrentTimestamp())

date.setMonth(date.getMonth() - 1)

const initialState = {
  dateFrom: formatDate(date),
  dateTo: formatDate(new Date(getCurrentTimestamp())),
  types: ['useractivity'],
  data: {}
}

export default handleActions(
  {
    [chartActions.SET_CHART_DATA](state, action) {
      return {
        ...state,
        data: action.payload
      }
    },
    [chartActions.SET_DATE_FROM](state, action) {
      return {
        ...state,
        dateFrom: action.payload
      }
    },
    [statTypesActions.SET_TYPES](state, action) {
      return {
        ...state,
        types: action.payload
      }
    },
    [statTypesActions.ADD_TYPE](state, action) {
      if (state.types.length === MAX_STATS || state.types.includes(action.payload)) return state

      return {
        ...state,
        types: [...state.types, action.payload]
      }
    },
    [statTypesActions.CHANGE_TYPE](state, action) {
      return {
        ...state,
        types: state.types.map((type, index) => (action.payload.index === index ? action.payload.type : type))
      }
    },
    [statTypesActions.REMOVE_TYPE](state, action) {
      const index = action.payload

      return {
        ...state,
        types: [...state.types.slice(0, index), ...state.types.slice(index + 1)]
      }
    }
  },
  initialState
)
