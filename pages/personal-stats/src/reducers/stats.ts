import { handleActions } from 'redux-actions'
import * as actionTypes from '../actions/actionTypes'
import ICategory from '../interfaces/ICategory'
import IUser from '../interfaces/IUser'

interface IStats {
  categories: ICategory[]
  currentUser: IUser
  loading: boolean
  message: { msg: string; color: string }
}

const initialState = {
  currentUser: {},
  categories: [],
  loading: false,
  message: { msg: '', color: '' }
}

interface IAction {
  type: string
  payload: any
}

export default handleActions(
  {
    [actionTypes.FETCH_STATS](state: IStats) {
      return {
        ...state,
        loading: true
      }
    },
    [actionTypes.SET_STATS](state: IStats, action: IAction) {
      return {
        ...state,
        ...action.payload,
        loading: false
      }
    },
    [actionTypes.SET_INFO_MESSAGE](state: IStats, action: IAction) {
      return {
        ...state,
        message: {
          msg: action.payload.msg,
          color: action.payload.color
        }
      }
    }
  },
  initialState
)
