import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import stats from './stats'
import users from './users'
import chart from './chart'

const browser = createResponsiveStateReducer({
  mobile: 600,
  tablet: 1000,
  desktop: 5000
})

export default combineReducers({
  stats,
  users,
  chart,
  browser
})
