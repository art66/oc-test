import { createStore, applyMiddleware } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import rootReducer from '../reducers'
import rootSaga from '../sagas'

const sagaMiddleware = createSagaMiddleware()

export default function configure(initialState) {
  const store = createStore(rootReducer, initialState, composeWithDevTools(
    responsiveStoreEnhancer,
    applyMiddleware(sagaMiddleware)
  ))

  sagaMiddleware.run(rootSaga)

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers')

      store.replaceReducer(nextReducer)
    })
  }

  return store
}
