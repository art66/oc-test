export default interface IHoveredStat {
  name: string
  category: string
}
