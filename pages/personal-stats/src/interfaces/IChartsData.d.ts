import IChartTypeData from './IChartTypeData'

export interface IChartsData {
  data: {
    [key: string]: [IChartTypeData]
  }
  definitions: {
    [key: string]: string
  }
}
