export default interface IChartData {
  chartData: {
    [key: string]: string
  }
  player: string
  type: string
}
