import IStatColumn from './IStatColumn'

export default interface ICategory {
  name: string
  title: string
  columns: IStatColumn[]
}
