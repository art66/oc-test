export default interface IChartPointData {
  time: number
  value: number
}
