import IStat from './IStat'

export default interface IStatColumn {
  userID?: number
  name: string
  heading: string
  hoverText: string
  stats: IStat[]
}
