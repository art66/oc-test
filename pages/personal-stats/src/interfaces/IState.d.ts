import { IChartsData } from './IChartsData'
import IUser from './IUser'
import ICategory from './ICategory'

export default interface IState {
  chart: {
    data: IChartsData
    dateFrom: string
    dateTo: string
    types: string[]
  }
  users: {
    list: IUser[]
    currentUser: IUser
  }
  stats: {
    categories: ICategory[]
    currentUser: IUser
    loading: boolean
    message: { msg: string; color: string }
  }
}
