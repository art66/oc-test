import IChartPointData from './IChartPointData'

export default interface IChartTypeData {
  uid: number
  data: IChartPointData[]
}
