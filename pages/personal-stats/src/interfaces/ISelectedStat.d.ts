export default interface ISelectedStat {
  name: string
  value: string
}
