export default interface IUser {
  userID: number
  playername: string
}
