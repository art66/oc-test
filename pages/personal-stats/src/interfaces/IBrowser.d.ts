import { IBrowser as IReduxResponsiveBrowser, IBreakPoints, IBreakPointResults } from 'redux-responsive'

interface IBP extends IBreakPoints<'desktop' | 'tablet' | 'mobile'> {}

export default interface IBrowser extends IReduxResponsiveBrowser<IBP> {
  lessThan: IBreakPointResults<IBP>
}
