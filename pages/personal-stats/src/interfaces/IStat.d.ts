export default interface IStat {
  name: string
  title: string
  value: number | string
  format: string
  percentage: number
  chartAvailable: boolean
  isPositive: boolean
  isPrivate: boolean
  shortName: string
}
