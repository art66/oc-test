import { put, takeEvery, select } from 'redux-saga/effects'
import { getUsersIds, getChartDateFrom, getChartStatTypes } from '../selectors'
import fetchUrl from '../utils/fetchUrl'
import { setChartData } from '../actions/chart'
import { setInfoMessage } from '../actions'
import { chart as actionTypes } from '../actions/actionTypes'
import { ROOT_URL } from '../constants'

// eslint-disable-next-line max-statements
function* fetchChart(action: any) {
  try {
    let userIDs

    if (action.payload && action.payload.userIDs && action.payload.userIDs.length) {
      userIDs = action.payload.userIDs
    } else {
      userIDs = yield select(getUsersIds)
    }

    const from = yield select(getChartDateFrom)
    const statNames = yield select(getChartStatTypes)
    const step = 'getGraphData'
    const data = yield fetchUrl(ROOT_URL, { step, userIDs, from, statNames })

    if (data.error) {
      throw Error(data.error)
    }

    if (data) {
      yield put(setChartData(data))
      yield put(setInfoMessage('', ''))
    }
  } catch (error) {
    yield put(setInfoMessage(error, 'red'))
    console.error(error)
  }
}

export default function* chart() {
  yield takeEvery(actionTypes.FETCH_CHART, fetchChart)
}
