import { put, takeLatest, select } from 'redux-saga/effects'
import fetchUrl from '../utils/fetchUrl'
import { ROOT_URL } from '../constants'
import { setCurrentUser, setUsers } from '../actions/users'
import { setStats } from '../actions/stats'
import { setInfoMessage } from '../actions'
import * as actions from '../actions/actionTypes'
import { getUsersIds, getCurrentUser } from '../selectors'

// eslint-disable-next-line max-statements
function* fetchStats(action: any) {
  try {
    let userIDs

    if (action.payload && action.payload.userIDs && action.payload.userIDs.length) {
      userIDs = action.payload.userIDs
    } else {
      userIDs = yield select(getUsersIds)
    }
    const currentUser = yield select(getCurrentUser)
    const step =
      !userIDs.length || (userIDs.length === 1 && userIDs[0] === currentUser.userID) ? 'getMyStats' : 'getUserStats'
    const data = yield fetchUrl(ROOT_URL, { step, userIDs })

    if (data.error) {
      throw Error(data.error)
    }

    yield put(setCurrentUser(data.currentUser))

    if (data.users) {
      yield put(setUsers(data.users))
      yield put(setInfoMessage('', ''))
    }
    yield put(setStats({ categories: data.categories }))
  } catch (error) {
    yield put(setInfoMessage(error, 'red'))
    console.log(error)
  }
}

export default function* stats() {
  yield takeLatest(actions.FETCH_STATS, fetchStats)
}
