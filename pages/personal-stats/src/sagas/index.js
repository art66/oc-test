import { all, fork } from 'redux-saga/effects'
import stats from './stats'
import chart from './chart'

export default function* rootSaga() {
  // yield fork(watchStats)
  // yield fork(watchChart)
  yield all([stats(), chart()])
}
