import { createAction } from 'redux-actions'
import { chart as actions } from './actionTypes'

export const setChartData = createAction(actions.SET_CHART_DATA, data => data)
export const fetchChart = createAction(actions.FETCH_CHART)
export const setDateFrom = createAction(actions.SET_DATE_FROM, dateFrom => dateFrom)
