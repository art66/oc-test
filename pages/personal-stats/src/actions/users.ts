import { createAction } from 'redux-actions'
import { users as actions } from './actionTypes'

export const setCurrentUser = createAction(actions.SET_CURRENT_USER)
export const addUser = createAction(actions.ADD_USER, user => ({ user }))
export const removeUser = createAction(actions.REMOVE_USER, user => user)
export const setUsers = createAction(actions.SET_USERS, users => users)
export const clearUsers = createAction(actions.CLEAR_USERS)
