import { createAction } from 'redux-actions'
import { statTypes as actions } from './actionTypes'

export const setTypes = createAction(actions.SET_TYPES, types => types)
export const addType = createAction(actions.ADD_TYPE, type => type)
export const changeType = createAction(actions.CHANGE_TYPE, (type, index) => ({ type, index }))
export const removeType = createAction(actions.REMOVE_TYPE, index => index)
