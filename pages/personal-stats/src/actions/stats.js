import { createAction } from 'redux-actions'
import * as actionTypes from './actionTypes'

export const setStats = createAction(actionTypes.SET_STATS)
export const fetchStats = createAction(actionTypes.FETCH_STATS)
