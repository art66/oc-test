import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dropdown from '@torn/shared/components/Dropdown'
import { addType, changeType, removeType } from '../../actions/statTypes'
import IStat from '../../interfaces/IStat'
import ISelectedStat from '../../interfaces/ISelectedStat'
import { getStats } from '../../selectors'
import { resetHash } from '../../utils'
import { MAX_STATS } from '../../constants'

import s from './styles.cssmodule.scss'

interface IProps {
  stats: IStat[]
  types: string[]
  loading: boolean
  changeType: (value: string, index: number) => void
  addType: (value: string) => void
  removeType: (index: number) => void
  onChange: () => void
  onRef: (component?: DropdownType) => void
}

export class DropdownType extends Component<IProps> {
  static defaultProps = {
    onRef: () => {}
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    this.props.onRef(null)
  }

  addStatType = (selected: ISelectedStat) => {
    const { addType, onChange } = this.props

    addType(selected.value)
    onChange()
  }

  removeStateType = (index: number) => {
    const { types, removeType, onChange } = this.props

    if (types.length > 1) {
      removeType(index)
      onChange()
    }
  }

  handleChange = (selected: ISelectedStat, index: number) => {
    this.props.changeType(selected.value, index)
    this.props.onChange()
    resetHash()
  }

  getList = () =>
    [{ name: 'User Activity', value: 'useractivity' }].concat(
      this.props.stats.map(stat => ({ name: stat.title, value: stat.name }))
    )

  getFilteredList = () => {
    const { types } = this.props

    return this.getList().filter(stat => !types.includes(stat.value))
  }

  getSelectedStat = (index: number): ISelectedStat => {
    const { types } = this.props
    const list = this.getList()
    const selectedStat = list.find(stat => stat.value === types[index])

    if (types[index] !== selectedStat.value) {
      this.props.changeType(selectedStat.value, index)
      this.props.onChange()
    }

    return selectedStat
  }

  renderStatNamesDropdowns = list => {
    const { types, loading } = this.props
    const selectedStatsDropdowns = types.map((type, index) => (
      <li key={type} className={s.statTypeWrapper}>
        <button
          type='button'
          className={s.removeStat}
          disabled={types.length <= 1}
          onClick={() => this.removeStateType(index)}
          aria-label={`Unselect stat ${type}`}
        >
          <span className={s.text}>+</span>
        </button>
        <Dropdown
          className='react-dropdown-default dropdown-stat-type'
          list={list}
          selected={!loading ? this.getSelectedStat(index) : null}
          placeholder='Add another statistic'
          onChange={(selected) => this.handleChange(selected as ISelectedStat, index)}
          search={true}
        />
      </li>
    ))
    const newStatDropdown = types.length < MAX_STATS ? (
      <li className={s.statSelectNew}>
        <Dropdown
          className='react-dropdown-default dropdown-stat-type'
          list={list}
          placeholder='Add another statistic'
          onChange={this.addStatType}
          search={true}
        />
      </li>
    ) : []

    return selectedStatsDropdowns.concat(newStatDropdown)
  }

  render() {
    return (
      <ul className={s.statTypes}>
        {this.renderStatNamesDropdowns(this.getFilteredList())}
      </ul>
    )
  }
}

export default connect(
  (state: any) => ({
    types: state.chart.types,
    loading: state.stats.loading,
    stats: getStats(state)
  }),
  {
    addType,
    changeType,
    removeType
  }
)(DropdownType)
