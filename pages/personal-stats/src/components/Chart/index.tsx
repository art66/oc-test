import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import GoogleCharts from '@torn/shared/modules/google-charts'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import dateFormat from 'dateformat'
import { toMoney } from '@torn/shared/utils'
import { fetchChart, setDateFrom } from '../../actions/chart'
import DropdownTime from './DropdownTime'
import DropdownType, { DropdownType as DropdownTypeClass } from './DropdownType'
import { getStatsCharts, getStats } from '../../selectors'
import Users from '../Users'
import { DARK_THEME, GRIDLINES_COUNT_Y } from '../../constants'
import { LIGHT_OPTIONS, DARK_OPTIONS } from '../../constants/chartOptions'
import IStat from '../../interfaces/IStat'
import IChartData from '../../interfaces/IChartData'
import IState from '../../interfaces/IState'

import s from './styles.cssmodule.scss'
import sCommon from '../common.cssmodule.scss'
const GOOGLE_CHARTS_VERSION = '45.2'

interface IProps extends TWithThemeInjectedProps {
  stats: IStat[]
  types: string[]
  statsCharts: {
    [key: string]: IChartData
  }
  fetchChart: () => void
}

interface IChartInstance {
  draw(data: Object, options: Object): void
  clearChart(): void
}

class Chart extends Component<IProps> {
  dropdownType: DropdownTypeClass
  chartPlaceholder: HTMLDivElement
  chartInstance: IChartInstance
  chartLoaded = false
  chartLoading = false

  componentDidMount() {
    window.addEventListener('resize', this.drawChart)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.drawChart)
  }

  componentDidUpdate() {
    this.loadChartAndDraw()
  }

  getSelectedStatTitle = () => {
    const { types, stats } = this.props

    if (types) {
      return stats?.length ? types.map(type => stats.find(stat => stat.name === type).shortName).join(', ') : ''
    }
  }

  loadChartAndDraw() {
    try {
      if (this.chartLoaded) {
        this.drawChart()
      } else {
        this.chartLoading = true
        GoogleCharts.load(GOOGLE_CHARTS_VERSION, { packages: ['corechart', 'line'], callback: this.drawChart })
      }
    } catch (e) {
      console.error(e)
    }
  }

  errorHandler = errorMessage => {
    GoogleCharts.api.visualization.errors.removeError(errorMessage.id)
  }

  // eslint-disable-next-line max-statements
  drawChart = () => {
    const { theme, statsCharts } = this.props

    try {
      this.chartLoaded = true
      this.chartLoading = false

      if (this.chartInstance) {
        this.chartInstance.clearChart()
      } else {
        this.chartInstance = new GoogleCharts.api.visualization.LineChart(this.chartPlaceholder)
      }

      const data = new GoogleCharts.api.visualization.DataTable()

      let rows = []
      let maxValue = 0
      let minValue = 0

      let counter = 1

      if (data) {
        data.addColumn('datetime', 'time')
      }

      Object.keys(statsCharts).forEach((statChartName, statChartIndex) => {
        const { chartData, player, type } = statsCharts[statChartName]

        data.addColumn('number', statChartName)
        data.addColumn({ type: 'string', role: 'tooltip', p: { html: true } })
        Object.keys(chartData)
          .reverse()
          .forEach((recordTimestamp, recordIndex) => {
            const value = parseFloat(chartData[recordTimestamp])

            if (!rows[recordIndex]) {
              rows[recordIndex] = []
            }

            if (statChartIndex === 0) {
              rows[recordIndex][0] = new Date(+recordTimestamp * 1000)
            }
            rows[recordIndex][counter] = value
            rows[recordIndex][counter + 1] = `
            <div class=${s.tooltip}>
              <p class=${s.player}><b>${player}</b></p>
              <p class=${s.player}>${type}</p>
              <p class=${s.date}>${dateFormat(new Date(+recordTimestamp * 1000), 'mmm d, yyyy')}</p>
              ${toMoney(value.toString())}
            </div>
          `

            if (minValue === null) {
              minValue = value
            }
            maxValue = Math.max(value, maxValue)
            minValue = Math.min(value, minValue)
          })

        counter += 2
      })

      rows = rows.reverse()

      data.addRows(rows)

      const formatter = new GoogleCharts.api.visualization.DateFormat({ pattern: 'HH:mm', zone: 0 })

      formatter.format(data, 0)

      GoogleCharts.api.visualization.events.addListener(this.chartInstance, 'error', this.errorHandler)
      this.applyMinMaxValues(minValue, maxValue)
      this.chartInstance.draw(data, theme === DARK_THEME ? DARK_OPTIONS : LIGHT_OPTIONS)
    } catch (e) {
      console.log(e)
    }
  }

  applyMinMaxValues = (minValue: number, maxValue: number) => {
    let min = minValue
    let max = maxValue
    const chartOptions = this.props.theme === DARK_THEME ? DARK_OPTIONS : LIGHT_OPTIONS

    if (max < GRIDLINES_COUNT_Y) {
      max = Math.max(GRIDLINES_COUNT_Y, max)
      min = Math.min(-GRIDLINES_COUNT_Y, min)
      max += max / 2
      min += min / 2
      chartOptions.vAxis.viewWindow.max = max.toString()

      if (min < 0) {
        chartOptions.vAxis.viewWindow.min = 'auto'
      }

      if (min < 0 && minValue >= 0) {
        chartOptions.vAxis.viewWindow.min = '0'
      }
    } else {
      chartOptions.vAxis.viewWindow.max = 'auto'
      chartOptions.vAxis.viewWindow.min = minValue <= 0 ? 'auto' : '0'
    }
  }

  render() {
    const selectedStatTitle = this.getSelectedStatTitle()

    return (
      <section id='chartSection' className={s.chart}>
        <div className={sCommon.title}>{selectedStatTitle}</div>
        <Users />
        <div className={cn(sCommon.content, s.chartContent)}>
          <div className={s.dropDowns}>
            <DropdownTime onChange={this.props.fetchChart} />
            <DropdownType onChange={this.props.fetchChart} onRef={component => (this.dropdownType = component)} />
          </div>
          <div className={s.chartWrapper}>
            <div ref={el => (this.chartPlaceholder = el)} />
          </div>
        </div>
      </section>
    )
  }
}

const ChartWithTheme = withTheme(Chart)

export default connect(
  (state: IState) => ({
    types: state.chart.types,
    users: state.users.list,
    stats: getStats(state),
    statsCharts: getStatsCharts(state)
  }),
  {
    fetchChart,
    setDateFrom
  }
)(ChartWithTheme)
