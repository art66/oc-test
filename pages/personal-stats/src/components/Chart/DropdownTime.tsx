import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dropdown from '@torn/shared/components/Dropdown'
import { setDateFrom } from '../../actions/chart'
import { formatDate, resetHash } from '../../utils'
import { ONE_MONTH } from '../../constants'

const DATE_ALL = '2009-01-01'
const DEFAULT_SELECTED_INDEX = 0

interface IProps {
  dateFrom: string
  setDateFrom: (value: string) => void
  onChange: () => void
}

interface ISelected {
  name: string
  value: string
}

class DropdownTime extends Component<IProps> {
  public static getDropdownList(): ISelected[] {
    const date = new Date()
    const month1 = new Date(Date.now() - ONE_MONTH)
    const month3 = new Date()
    const month6 = new Date()
    const year1 = new Date()
    const year5 = new Date()

    month3.setMonth(date.getMonth() - 3)
    month6.setMonth(date.getMonth() - 6)
    year1.setFullYear(date.getFullYear() - 1)
    year5.setFullYear(date.getFullYear() - 5)

    return [
      { name: '1 month', value: formatDate(month1) },
      { name: '3 months', value: formatDate(month3) },
      { name: '6 months', value: formatDate(month6) },
      { name: '1 year', value: formatDate(year1) },
      { name: '5 years', value: formatDate(year5) },
      { name: 'All', value: DATE_ALL }
    ]
  }

  public static getSelectedByName(name: string): ISelected {
    const periods = DropdownTime.getDropdownList()

    return periods.find(item => item.name === name) || periods[0]
  }

  public static getSelectedByValue(value: string): ISelected {
    const periods = DropdownTime.getDropdownList()

    return periods.find(item => item.value === value) || periods[0]
  }

  handlePeriodChange = (selected: ISelected) => {
    this.props.setDateFrom(selected.value)
    this.props.onChange()
    resetHash()
  }

  render() {
    const { dateFrom } = this.props
    const list = DropdownTime.getDropdownList()

    return (
      <Dropdown
        className='react-dropdown-default dropdown-time'
        list={list}
        selected={list.find(item => item.value === dateFrom) || list[DEFAULT_SELECTED_INDEX]}
        placeholder='Select something'
        onChange={this.handlePeriodChange}
        onInit={(selected: ISelected) => this.props.setDateFrom(selected.value)}
      />
    )
  }
}

export default connect(
  (state: any) => ({
    dateFrom: state.chart.dateFrom
  }),
  {
    setDateFrom
  }
)(DropdownTime)
