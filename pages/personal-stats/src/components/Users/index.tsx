import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'

import UsersAutocomplete from '@torn/shared/components/UsersAutocomplete'
import SelectSearch from '@torn/shared/components/SelectSearch'

import { addUser, removeUser, setCurrentUser, clearUsers } from '../../actions/users'
import { fetchStats } from '../../actions/stats'
import { fetchChart } from '../../actions/chart'

import IUser from '../../interfaces/IUser'
import IStat from '../../interfaces/IStat'
import IStateGlobal from '../../interfaces/IState'
import { MAX_USERS } from '../../constants'
import { USER_INPUT_DISABLED, USER_INPUT_ENABLED } from '../../constants/messages'
import { resetHash } from '../../utils'
import { getStats } from '../../selectors'

import s from './styles.cssmodule.scss'

interface IProps {
  stats: IStat[]
  types: string[]
  users: { list: IUser[] }
  addUser: (user: IUser) => void
  removeUser: (user: IUser) => void
  setCurrentUser: (user: IUser) => void
  fetchStats: (payload?: { userIDs: number[] }) => void
  fetchChart: (payload?: { userIDs: number[] }) => void
  clearUsers: () => void
}

class Users extends Component<IProps> {
  autoComplete: UsersAutocomplete

  getUsers = () => {
    return this.props.users.list.map(user => (
      <div key={user.userID} className={s.userLabel}>
        <button
          type='button'
          className={s.removeUserButton}
          disabled={!this.canRemoveUser()}
          onClick={() => this.removeUser(user)}
          aria-label={`Unselect ${user.playername}`}
        >
          <span className={s.text}>+</span>
        </button>
        <div className={s.user}>
          {user.playername} [{user.userID}]
        </div>
      </div>
    ))
  }

  handleAutocompleteSelect = (selected: { name: string; ID: string }) => {
    const user = {
      playername: selected.name,
      userID: parseInt(selected.ID, 10)
    }

    this.addUser(user)
  }

  _getDisabledState = () => {
    const { stats, types } = this.props

    return types.map(type => stats.find(stat => stat.name === type)).some(stat => stat?.isPrivate)
  }

  addUser = (user: IUser) => {
    if (user && this.props.users.list.length < MAX_USERS) {
      this.props.addUser(user)
      this.props.fetchStats()
      this.props.fetchChart()
      resetHash()
    }
  }

  removeUser = (user: IUser) => {
    if (this.canRemoveUser()) {
      this.props.removeUser(user)
      this.props.fetchStats()
      this.props.fetchChart()
      resetHash()
    }
  }

  canRemoveUser = () => this.props.users.list.length > 1

  clearAll = () => {
    this.props.clearUsers()
    this.props.fetchStats()
    this.props.fetchChart()
    resetHash()
  }

  renderUsersSearch = () => {
    const { users: { list } = {} } = this.props
    const disabled = this._getDisabledState()

    const userSearchConfig = {
      saveOnClick: this.handleAutocompleteSelect,
      systemData: {
        stylePreset: 'grey' as 'grey'
      },
      localStorage: {
        // temporary disabled
        isDisabled: true
      },
      websockets: {
        // temporary disabled
        isDisabled: true
      },
      enhancers: {
        isActive: true,
        list: [{ type: 'addSave' as 'addSave' }]
      },
      buttons: {
        // temporary disabled for a future improvements
        historyButton: {
          isDisabled: true
        }
      },
      dropdown: {
        disabledList: list.map(user => user.userID),
        alreadySelected: {
          alreadySelectedText: '(selected)'
        },
        customConfig: {
          users: {
            // temporary disabled. Postponed because of the back-end.
            status: {
              isDisabled: true
            },
            name: {
              preventFirstListLetterUpper: true
            }
          }
        }
      },
      input: {
        placeholder: disabled ? USER_INPUT_DISABLED : USER_INPUT_ENABLED,
        clearInputOnClick: true,
        readOnly: disabled
      }
    }

    return (
      <SelectSearch
        systemData={userSearchConfig.systemData}
        localStorage={userSearchConfig.localStorage}
        websockets={userSearchConfig.websockets}
        buttons={userSearchConfig.buttons}
        enhancers={userSearchConfig.enhancers}
        dropdown={userSearchConfig.dropdown}
        saveOnClick={userSearchConfig.saveOnClick}
        input={userSearchConfig.input}
      />
    )
  }

  renderUserSelect = () => {
    const { users: { list } = {} } = this.props

    return list.length < MAX_USERS && <div className={s.autocomplete}>{this.renderUsersSearch()}</div>
  }

  render() {
    const {
      users: { list }
    } = this.props

    return (
      <div className={s.selectUsersCont}>
        <div className={s.users}>{this.getUsers()}</div>
        {this.renderUserSelect()}
        <button
          type='button'
          onClick={this.clearAll}
          className={cn(s.clearUsers, { [s.usersFull]: list.length >= MAX_USERS })}
        >
          Clear all
        </button>
      </div>
    )
  }
}

export default connect(
  (state: IStateGlobal) => ({
    users: state.users,
    types: state.chart.types,
    stats: getStats(state)
  }),
  {
    addUser,
    removeUser,
    setCurrentUser,
    fetchStats,
    fetchChart,
    clearUsers
  }
)(Users)
