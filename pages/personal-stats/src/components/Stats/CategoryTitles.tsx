import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import IStat from '../../interfaces/IStat'
import IUser from '../../interfaces/IUser'
import ICategory from '../../interfaces/ICategory'
import IHoveredStat from '../../interfaces/IHoveredStat'
import IBrowser from '../../interfaces/IBrowser'
import Tooltip from '@torn/shared/components/Tooltip'
import s from './styles.cssmodule.scss'
import sCommon from '../common.cssmodule.scss'

interface IProps {
  category: ICategory
  users: IUser[]
  chartTypes: string[]
  hoveredStat: IHoveredStat
  setChartType: (type: string) => void
  setHoveredStat: (stat: IStat, category: string) => void
  unsetHoveredStat: () => void
  fetchStats: () => void
  fetchChart: () => void
  scrollToChart: () => void
  isNotDesktop: boolean
}

class CategoryItem extends Component<IProps> {
  componentDidMount() {
    const { hash } = location
    const hashName = hash.replace('#', '')
    const scrollTo = document.getElementById(hashName)

    if (scrollTo) {
      scrollTo.scrollIntoView()
    }
  }

  shouldComponentUpdate(newProps: IProps) {
    const { category, chartTypes } = this.props

    return (
      newProps.hoveredStat.category === category.name ||
      this.props.hoveredStat.category === category.name ||
      newProps.category.columns.length !== category.columns.length ||
      JSON.stringify(newProps.chartTypes) !== JSON.stringify(chartTypes) ||
      newProps.users.length !== this.props.users.length ||
      newProps.isNotDesktop !== this.props.isNotDesktop
    )
  }

  setChartType(stat: IStat): void {
    const { isNotDesktop } = this.props

    if (stat.chartAvailable) {
      this.props.setChartType(stat.name)
      this.props.fetchChart()
      !isNotDesktop && this.props.scrollToChart()
    }
  }

  getStatTooltip = stat => {
    const { isNotDesktop } = this.props

    return isNotDesktop &&
      <Tooltip parent={`${stat.name}-stat`} position='top' arrow='center'>{stat.title}</Tooltip>
  }

  getStat = (stat, category) => {
    const { isNotDesktop } = this.props
    const statCn = cn(s.statName, {
      [s.hover]: stat.name === this.props.hoveredStat.name,
      [s.selected]: this.props.chartTypes.includes(stat.name),
      [s.chartAvailable]: stat.chartAvailable
    })

    return (
      <div key={stat.name} className={s.statRow}>
        <div
          id={`${stat.name}-stat`}
          className={statCn}
          onMouseEnter={() => this.props.setHoveredStat(stat, category.name)}
          onMouseLeave={() => this.props.unsetHoveredStat()}
          onClick={() => this.setChartType(stat)}
        >
          {isNotDesktop ? stat.shortName : stat.title}
        </div>
        {this.getStatTooltip(stat)}
      </div>
    )
  }

  getStats = (category: ICategory) => {
    return (
      <div className={s.statCols}>
        <div className={cn(s.statRows, s.statName)}>
          {category.columns[0].stats.map(stat => this.getStat(stat, category))}
        </div>
      </div>
    )
  }

  render() {
    const { category } = this.props

    return (
      <div key={category.name} className={s.category}>
        <div className={cn(sCommon.title, s.titles)}>
          <div id={category.name} className={cn(s.titleItem, s.statName)}>
            {category.title}
          </div>
        </div>
        {this.getStats(category)}
      </div>
    )
  }
}

export default connect(
  (state: { browser: IBrowser }) => ({
    isNotDesktop: state.browser.lessThan.desktop
  })
)(CategoryItem)
