import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import IStat from '../../interfaces/IStat'
import IUser from '../../interfaces/IUser'
import ICategory from '../../interfaces/ICategory'
import IHoveredStat from '../../interfaces/IHoveredStat'
import IBrowser from '../../interfaces/IBrowser'
import Cell from './Cell'
import { getUserColor } from '../../utils'
import { DARK_THEME } from '../../constants'
import sCommon from '../common.cssmodule.scss'
import s from './styles.cssmodule.scss'

interface IProps extends TWithThemeInjectedProps {
  category: ICategory
  users: IUser[]
  chartTypes: string[]
  hoveredStat: IHoveredStat
  isNotDesktop: boolean
  setChartType: (type: string) => void
  setHoveredStat: (stat: IStat, category: string) => void
  unsetHoveredStat: () => void
  fetchStats: () => void
  fetchChart: () => void
  scrollToChart: () => void
}

class CategoryItem extends Component<IProps> {
  shouldComponentUpdate(newProps: IProps) {
    const { category, chartTypes, theme } = this.props

    return (
      newProps.hoveredStat.category === category.name
      || this.props.hoveredStat.category === category.name
      || newProps.category.columns.length !== category.columns.length
      || JSON.stringify(newProps.chartTypes) !== JSON.stringify(chartTypes)
      || newProps.users.length !== this.props.users.length
      || newProps.theme !== theme
    )
  }

  getStats = (category: ICategory) => {
    const { users, isNotDesktop } = this.props

    return (
      <div className={s.statRows}>
        {category.columns[0].stats.map(stat => {
          const statName = isNotDesktop ? stat.shortName : stat.title

          return (
            <div key={stat.name} className={s.statRow} tabIndex={0}>
              <div
                className={cn(s.statName, {
                  [s.hover]: stat.name === this.props.hoveredStat.name,
                  [s.selected]: this.props.chartTypes.includes(stat.name),
                  [s.chartAvailable]: stat.chartAvailable
                })}
                onMouseEnter={() => this.props.setHoveredStat(stat, category.name)}
                onMouseLeave={() => this.props.unsetHoveredStat()}
                onClick={() => this.setChartType(stat)}
                aria-label={`${statName},`}
              >
                {statName}
              </div>
              {category.columns
                .filter(col => !col.userID || users.findIndex(u => u.userID === col.userID) !== -1)
                .map((col, colIndex) => (
                  <Cell
                    key={stat.name + colIndex}
                    stat={col.stats.find(st => st.name === stat.name)}
                    chartTypes={this.props.chartTypes}
                    hoveredStat={this.props.hoveredStat}
                    statColumn={col}
                    category={category}
                    columnIndex={colIndex}
                    setChartType={this.setChartType}
                    setHoveredStat={this.props.setHoveredStat}
                    unsetHoveredStat={this.props.unsetHoveredStat}
                    fetchChart={this.props.fetchChart}
                    fetchStats={this.props.fetchStats}
                    scrollToChart={this.props.scrollToChart}
                    isShowGraphIcon={colIndex === category.columns.length - 1}
                  />
                ))}
            </div>
          )
        })}
      </div>
    )
  }

  setChartType = (stat: IStat): void => {
    if (stat.chartAvailable) {
      this.props.setChartType(stat.name)
      this.props.fetchChart()
      this.props.scrollToChart()
    }
  }

  render() {
    const { category, users, theme } = this.props
    const columns = category.columns.filter(col => !col.userID || users.findIndex(u => u.userID === col.userID) !== -1)

    return (
      <div key={category.name} className={s.category}>
        <div className={cn(sCommon.title, s.titles)}>
          <div id={category.name} className={cn(s.titleItem, s.statName)}>
            {category.title}
          </div>
          {columns.map((col, colIndex) => {
            const id = category.name + colIndex

            return (
              <div
                key={id}
                id={id}
                className={cn(s.titleItem, { [s.history]: !col.userID && colIndex > 0 })}
                style={{ color: getUserColor(col.userID, this.props.users, theme === DARK_THEME) }}
              >
                {col.heading}
                {col.hoverText && (
                  <Tooltip parent={id} arrow='center' position='top'>
                    {col.hoverText}
                  </Tooltip>
                )}
              </div>
            )
          })}
        </div>
        {this.getStats(category)}
      </div>
    )
  }
}

const CategoryItemWithTheme = withTheme(CategoryItem)

export default connect(
  (state: { browser: IBrowser }) => ({
    isNotDesktop: state.browser.lessThan.desktop
  }),
  {}
)(CategoryItemWithTheme)
