import React, { Component } from 'react'
import cn from 'classnames'
import { toMoney } from '@torn/shared/utils'
import { SVGIconGenerator } from '@torn/shared/SVG'
import personalStats from '@torn/shared/SVG/helpers/iconsHolder/personalStats'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import IStat from '../../interfaces/IStat'
import IStatColumn from '../../interfaces/IStatColumn'
import ICategory from '../../interfaces/ICategory'
import IHoveredStat from '../../interfaces/IHoveredStat'

import s from './styles.cssmodule.scss'
const NA = '--'

const STAT_FORMAT = {
  MONEY: 'money',
  NUMBER: 'number',
  TIME: 'time'
}

const QUALITY = {
  BEST: 'best',
  WORSE: 'worse',
  MEDIUM: 'medium',
  CURRENT: 'current',
  HISTORY: 'history'
}

interface IProps extends TWithThemeInjectedProps {
  stat: IStat
  chartTypes: string[]
  hoveredStat: IHoveredStat
  statColumn: IStatColumn
  category: ICategory
  columnIndex: number
  setChartType: (stat: IStat) => void
  setHoveredStat: (stat: IStat, category: string) => void
  unsetHoveredStat: () => void
  fetchStats: () => void
  fetchChart: () => void
  scrollToChart: () => void
  isShowGraphIcon: boolean
}

class Cell extends Component<IProps> {
  getStatQuality(stat: IStat, category: ICategory, col: IStatColumn, colIndex: number): string {
    let quality = QUALITY.MEDIUM

    if (!category.columns[colIndex].userID) {
      return colIndex === 0 ? QUALITY.CURRENT : QUALITY.HISTORY
    }

    if (category.columns.length <= 1 || stat.isPrivate || stat.value === NA) {
      return quality
    }

    const otherStats = category.columns
      .filter(c => c.userID !== col.userID)
      .reduce((acc, c) => acc.concat(c.stats.filter(st => st.name === stat.name && st.value !== NA)), [])

    if (otherStats.length < 1) {
      return quality
    }

    if (otherStats.every(st => st.value < stat.value)) {
      quality = QUALITY.BEST
    } else if (otherStats.every(st => st.value > stat.value)) {
      quality = QUALITY.WORSE
    }

    return quality
  }

  formatStatValue = (val: number | string, format: string): string => {
    let value

    if (format === STAT_FORMAT.TIME && typeof val === 'number') {
      let seconds = val
      const days = Math.floor(seconds / (3600 * 24))

      seconds -= days * 3600 * 24
      const hours = Math.floor(seconds / 3600)

      seconds -= hours * 3600
      const minutes = Math.floor(seconds / 60)

      seconds -= minutes * 60
      value = `${days}d ${hours}h ${minutes}m ${seconds}s`
    } else {
      value = val.toString()
      const valueDotIndex = value.indexOf('.')

      value = valueDotIndex !== -1 ? value.substring(0, valueDotIndex + 4) : value
      value = toMoney(value)
    }

    return value
  }

  formatPercentage = (percentage: number): string | null => {
    let value = percentage ? percentage.toString() : null

    if (value) {
      const percentageDotIndex = value.indexOf('.')

      value = percentageDotIndex !== -1 ? value.substring(0, percentageDotIndex + 3) : value
    }

    return value
  }

  renderGraphIcon = () => {
    const { stat, theme, isShowGraphIcon } = this.props
    const isDarkMode = theme === 'dark'

    return (
      stat.chartAvailable
      && isShowGraphIcon && (
        <SVGIconGenerator
          iconName='Graph'
          iconsHolder={personalStats}
          customClass={s.chartIcon}
          preset={{ type: 'personalStats', subtype: isDarkMode ? 'DEFAULT_DARK_MODE' : 'DEFAULT' }}
          onHover={{
            active: true,
            preset: { type: 'personalStats', subtype: isDarkMode ? 'HOVER_DARK_MODE' : 'HOVER' }
          }}
        />
      )
    )
  }

  render() {
    const { stat, category, statColumn, columnIndex, chartTypes, hoveredStat } = this.props
    const value = this.formatStatValue(stat.value, stat.format)
    const percentage = this.formatPercentage(stat.percentage)

    return (
      // eslint-disable-next-line jsx-a11y/click-events-have-key-events
      <div
        key={stat.name + columnIndex}
        className={cn(s.statValue, s[this.getStatQuality(stat, category, statColumn, columnIndex)], {
          [s.hover]: stat.name === hoveredStat.name,
          [s.selected]: chartTypes.includes(stat.name),
          [s.chartAvailable]: stat.chartAvailable
        })}
        onClick={() => this.props.setChartType(stat)}
        onMouseEnter={() => this.props.setHoveredStat(stat, category.name)}
        onMouseLeave={() => this.props.unsetHoveredStat()}
      >
        <span className='wai'>{`${statColumn?.heading},`}</span>
        <span>
          {`${stat.value}` !== '--' && stat.format === STAT_FORMAT.MONEY && '$'}
          {value}
          {percentage && percentage !== 'N/A' && ` (${percentage}%)`}
        </span>
        <span className='wai'>,</span>
        {this.renderGraphIcon()}
      </div>
    )
  }
}

export default withTheme(Cell)
