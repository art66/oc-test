import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { fetchChart } from '../../actions/chart'
import { fetchStats } from '../../actions/stats'
import { setTypes, addType } from '../../actions/statTypes'
import IUser from '../../interfaces/IUser'
import IStat from '../../interfaces/IStat'
import IStateGlobal from '../../interfaces/IState'
import ICategory from '../../interfaces/ICategory'
import IHoveredStat from '../../interfaces/IHoveredStat'
import CategoryItem from './Category'
import CategoryTitles from './CategoryTitles'
import s from './styles.cssmodule.scss'
const COLUMNS_FOR_SCROLL = 3

interface IProps {
  categories: ICategory[]
  users: IUser[]
  currentUser: IUser
  chartTypes: string[]
  fetchStats: () => void
  fetchChart: () => void
  setTypes: (value: string[]) => void
  addType: (value: string) => void
}

interface IState {
  hoveredStat: IHoveredStat
}

class Stats extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = { hoveredStat: { name: '', category: '' } }
  }

  componentDidMount() {
    // this.props.fetchStats();
  }

  getCategoriesLinks = () => {
    const { categories } = this.props
    const categories1 = [...categories].splice(0, categories.length / 2)
    const categories2 = [...categories].splice(categories.length / 2, categories.length)

    return (
      <div className={s.quickLinks}>
        <ul className={s.categoryLinks}>{this.renderLinks(categories1)}</ul>
        <ul className={s.categoryLinks}>{this.renderLinks(categories2)}</ul>
      </div>
    )
  }

  getCategoriesTitles = () => {
    const list = this.props.categories.map(category => (
      <CategoryTitles
        key={category.name}
        category={category}
        chartTypes={this.props.chartTypes}
        users={this.props.users}
        hoveredStat={this.state.hoveredStat}
        setHoveredStat={this.setHoveredStat}
        unsetHoveredStat={this.unsetHoveredStat}
        setChartType={this.handleStatChange}
        fetchChart={this.props.fetchChart}
        fetchStats={this.props.fetchStats}
        scrollToChart={this.scrollToChart}
      />
    ))

    return <div className={s.categoriesTitles}>{list}</div>
  }

  getCategories = () => {
    const list = this.props.categories.map(category => (
      <CategoryItem
        key={category.name}
        category={category}
        chartTypes={this.props.chartTypes}
        users={this.props.users}
        hoveredStat={this.state.hoveredStat}
        setHoveredStat={this.setHoveredStat}
        unsetHoveredStat={this.unsetHoveredStat}
        setChartType={this.handleStatChange}
        fetchChart={this.props.fetchChart}
        fetchStats={this.props.fetchStats}
        scrollToChart={this.scrollToChart}
      />
    ))
    const withScroll = list.length >= COLUMNS_FOR_SCROLL

    return (
      <div className={cn(s.categoriesValues, { [s.withScroll]: withScroll })}>
        <div className={cn(s.scrollArea)}>{list}</div>
      </div>
    )
  }

  handleStatChange = (stat: string) => {
    const { chartTypes } = this.props

    chartTypes.length === 1 ? this.props.setTypes([stat]) : this.props.addType(stat)
  }

  unsetHoveredStat = () => {
    this.setState({ hoveredStat: { name: '', category: '' } })
  }

  setHoveredStat = (stat: IStat, category: string) => {
    if (stat.chartAvailable) {
      this.setState({ hoveredStat: { name: stat.name, category: category } })
    }
  }

  scrollToChart = () => {
    const chartSection = document.getElementById('chartSection')

    try {
      chartSection.scrollIntoView({ block: 'start', behavior: 'smooth' })
    } catch (e) {
      chartSection.scrollIntoView()
    }
  }

  renderLinks = categories => {
    return categories.map(category => {
      const id = `categoryLink-${category.name}`

      return (
        <li key={category.name} className={s.listItem}>
          <a
            href={`#${category.name}0`}
            className={cn(s.link, s[category.name])}
            id={id}
            aria-label={`Category ${category.name}`}
          >
            <i className={cn(s.icon, s[category.name])} />
            <Tooltip parent={id} position='top' arrow='center'>
              {category.title}
            </Tooltip>
          </a>
        </li>
      )
    })
  }

  render() {
    const links = this.getCategoriesLinks()
    const titles = this.getCategoriesTitles()
    const categories = this.getCategories()

    return (
      <section className={s.statsSection}>
        {links}
        <div className={s.stats}>
          {titles}
          {categories}
        </div>
      </section>
    )
  }
}

export default connect(
  (state: IStateGlobal) => ({
    categories: state.stats.categories,
    users: state.users.list,
    currentUser: state.users.currentUser,
    chartTypes: state.chart.types
  }),
  {
    fetchStats,
    fetchChart,
    addType,
    setTypes
  }
)(Stats)
