import React from 'react'
import { getCookie, getURLParameterByName } from '@torn/shared/utils'
import uniq from 'lodash.uniq'
import { connect } from 'react-redux'
import InfoBox from '@torn/shared/components/InfoBox'
import Chart from './Chart'
import Stats from './Stats'
import DropdownTime from './Chart/DropdownTime'
import { fetchStats } from '../actions/stats'
import { fetchChart, setDateFrom } from '../actions/chart'
import { setTypes } from '../actions/statTypes'
import { MAX_USERS, MAX_STATS } from '../constants'
import User from '../interfaces/IUser'
import './DM_vars.scss'
import './vars.scss'

interface IProps {
  dateFrom: string
  users: { list: User[] }
  types: string[]
  message: {
    msg: string
    color: string
  }
  fetchStats: (payload?: { userIDs: number[] }) => void
  fetchChart: (payload?: { userIDs: number[] }) => void
  setTypes: (value: string[]) => void
  setDateFrom: (value: string) => void
}

class App extends React.Component<IProps> {
  componentDidMount() {
    this.loadInitialData()
  }

  componentDidUpdate(prevProps: IProps) {
    const { users, types, dateFrom, message } = this.props

    if (
      JSON.stringify(users.list) !== JSON.stringify(prevProps.users.list)
      || JSON.stringify(prevProps.types) !== JSON.stringify(types)
      || prevProps.dateFrom !== dateFrom
      || prevProps.message.msg !== message.msg
    ) {
      this.changeURL()
    }
  }

  // eslint-disable-next-line max-statements
  loadInitialData = () => {
    const currentUserId = parseInt(getCookie('uid'), 10)
    let userIDs = []

    try {
      const ids = getURLParameterByName('ID')
      const rowStats = getURLParameterByName('stats')
      const from = getURLParameterByName('from')
      const stats = rowStats ? uniq(rowStats.split(',')).slice(0, MAX_STATS) : []

      userIDs = ids ? ids.split(',').map(id => parseInt(id, 10)) : []
      userIDs = uniq(userIDs).slice(0, MAX_USERS)

      if (stats.length) {
        this.props.setTypes(stats)
      }

      if (from) {
        const dateFrom = DropdownTime.getSelectedByName(from).value

        this.props.setDateFrom(dateFrom)
      }
      // eslint-disable-next-line no-empty
    } catch (e) {}

    if (userIDs.length === 0) {
      userIDs.push(currentUserId)
    }

    this.props.fetchStats({ userIDs })
    this.props.fetchChart({ userIDs })
    this.changeURL()
  }

  changeURL = () => {
    try {
      const { users, types, dateFrom } = this.props
      const { protocol, hostname, port, pathname, hash } = location
      const from = DropdownTime.getSelectedByValue(dateFrom).name
      const usersIDs = users.list.map(u => u.userID).join(',')
      const urlHash = hash.length ? hash : ''
      const urlPort = port ? `:${port}` : ''
      const url = `${protocol}//${hostname}${urlPort}${pathname}?ID=${usersIDs}&stats=${types.join(
        ','
      )}&from=${from}${urlHash}`

      history.replaceState(null, null, url)
    } catch (e) {
      console.error(e)
    }
  }

  render() {
    const { message } = this.props

    return (
      <div>
        <InfoBox msg={message.msg} color={message.color} />
        <Chart />
        <Stats />
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    users: state.users,
    types: state.chart.types,
    dateFrom: state.chart.dateFrom,
    message: state.stats.message
  }),
  {
    fetchStats,
    fetchChart,
    setTypes,
    setDateFrom
  }
)(App)
