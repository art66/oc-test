import { GRIDLINES_COUNT_X, GRIDLINES_COUNT_Y } from './index'

export const LIGHT_OPTIONS = {
  height: 315,
  width: '92.5%',
  hAxis: {
    maxAlternation: 1,
    maxTextLines: 2,
    textStyle: {
      color: '#333',
      fontSize: 10
    },
    format: 'MMM d \n yyyy',
    gridlines: {
      count: GRIDLINES_COUNT_X,
      color: '#dedede'
    }
  },
  vAxis: {
    textStyle: {
      color: '#333',
      fontSize: 11
    },
    format: 'short',
    gridlines: {
      count: GRIDLINES_COUNT_Y,
      color: '#dedede'
    },
    viewWindow: {
      min: 'auto',
      max: 'auto'
    },
    baselineColor: '#333'
  },
  style: {
    color: '#000000'
  },
  crosshair: {
    color: '#000',
    trigger: 'none'
  },
  backgroundColor: 'transparent',
  chartArea: {
    left: 45,
    right: 10,
    width: '92%',
    height: 205,
    backgroundColor: {
      stroke: '#dcdcdc',
      strokeWidth: 2
    }
  },
  legend: {
    position: 'top',
    maxLines: 3,
    textStyle: {
      color: '#333',
      fontSize: 12
    },
    alignment: 'center',
    scrollArrows: {
      activeColor: '#666'
    },
    pagingTextStyle: {
      color: '#666'
    }
  },
  tooltip: {
    isHtml: true,
    pointSize: 5
  }
}

export const DARK_OPTIONS = {
  height: 315,
  width: '92.5%',
  hAxis: {
    maxAlternation: 1,
    maxTextLines: 2,
    textStyle: {
      color: '#ddd',
      fontSize: 10
    },
    format: 'MMM d \n yyyy',
    gridlines: {
      count: GRIDLINES_COUNT_X,
      color: '#222'
    }
  },
  vAxis: {
    textStyle: {
      color: '#ddd',
      fontSize: 11
    },
    format: 'short',
    gridlines: {
      count: GRIDLINES_COUNT_Y,
      color: '#222'
    },
    viewWindow: {
      min: 'auto',
      max: 'auto'
    },
    baselineColor: '#222'
  },
  style: {
    color: '#000000'
  },
  crosshair: {
    color: '#000',
    trigger: 'none'
  },
  backgroundColor: 'transparent',
  chartArea: {
    left: 45,
    right: 10,
    width: '92%',
    height: 205,
    backgroundColor: {
      stroke: '#222',
      strokeWidth: 2
    }
  },
  legend: {
    position: 'top',
    maxLines: 3,
    textStyle: {
      color: '#ddd',
      fontSize: 12
    },
    alignment: 'center',
    scrollArrows: {
      activeColor: '#ddd',
      inactiveColor: '#222'
    },
    pagingTextStyle: {
      color: '#ddd'
    }
  },
  tooltip: {
    isHtml: true,
    textStyle: {
      color: '#ddd'
    }
  }
}
