import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { HOVER_FILL, DEFAULT_FILL } from '@torn/shared/SVG/presets/icons/ics'
import Loader from './Loader'
import Editor from './Editor'
import { ICON_TYPE, DEFAULT_TOP, MIN_HEIGHT_TABLET } from '../constants'
import IWindow from '../models/IWindow'
import IInitParams from '../models/IInitParams'
import '../styles/vars.scss'
import '../styles/DM_vars.scss'
import s from '../styles/ImagesCropSystem.cssmodule.scss'

interface IProps {
  ics: {
    loaded: boolean
  }
  initParams: IInitParams
  browser: {
    lessThan: {
      tablet: boolean
    }
    width: number
    height: number
    mediaType: string
  }
  getConstraints: (initParams: IInitParams) => void
  resetICS: () => void
}

interface IState {
  height?: number
  top?: number
  handleCropping: () => void
}

class ImagesCropSystem extends Component<IProps, IState> {
  icsWindow: HTMLElement
  icsWrapper: HTMLElement

  constructor(props: IProps) {
    super(props)
    this.state = {
      handleCropping: () => {}
    }
  }

  componentDidMount() {
    const { ics, initParams, getConstraints: getImageConstraints } = this.props

    this.calculateIcsPosition()
    document.addEventListener('focus', this.keyboardGrab, true)
    window.addEventListener('resize', this.calculateIcsPosition)
    getImageConstraints(initParams)

    if (!ics.loaded) {
      document.addEventListener('keydown', this.handleKey)
    }
  }

  componentWillUnmount = () => {
    const { resetICS: resetApp } = this.props

    window.removeEventListener('resize', this.calculateIcsPosition)
    document.removeEventListener('focus', this.keyboardGrab, true)
    document.removeEventListener('keydown', this.handleKey)
    resetApp()
  }

  handleOpeningEditor = callback => {
    this.setState({
      handleCropping: callback
    })
  }

  handleKey = e => {
    if (e.key === 'q') {
      this.closeICS()
    }
  }

  _handleCloseICS = e => {
    if (e.target === this.icsWrapper) {
      this.closeICS()
    }
  }

  closeICS = () => {
    const win = window as IWindow
    const onCloseICSEvent = document.createEvent('CustomEvent')

    onCloseICSEvent.initCustomEvent('onCloseICS', false, false, {})
    document.dispatchEvent(onCloseICSEvent)
    win.unmountICS && win.unmountICS()
  }

  calculateIcsPosition = () => {
    const icsRect = this.icsWindow.getBoundingClientRect()
    const icsWrapperRect = this.icsWrapper.getBoundingClientRect()
    const icsWrapperHeight = icsWrapperRect.height
    const icsWidth = icsRect.width
    let icsTop = DEFAULT_TOP
    let icsHeight = icsWrapperHeight - icsTop * 2

    if (icsHeight > icsWidth) {
      icsHeight = icsWidth
      icsTop = (icsWrapperHeight - icsHeight) / 2
    }

    if (icsTop !== icsWrapperHeight - (icsTop + icsHeight)) {
      icsTop = (icsWrapperHeight - icsHeight) / 2
    }

    this.setState({
      height: icsHeight,
      top: icsTop
    })
  }

  canSetCalculatedStyles = () => {
    const { browser } = this.props
    const { height, top } = this.state

    return (
      (top && height && browser.mediaType === 'mobile')
      || (browser.mediaType === 'tablet' && browser.height < MIN_HEIGHT_TABLET)
    )
  }

  keyboardGrab = event => {
    const modal = document.getElementById('icsroot')

    if (modal && modal.style.display !== 'none' && !modal.contains(event.target)) {
      event.stopPropagation()
      modal.focus()
    }
  }

  renderEditor = () => {
    const { ics, initParams } = this.props

    return !ics.loaded ? (
      <div className={s['loader-wrapper']}>
        <Loader />
      </div>
    ) : (
      <Editor handleCropping={this.handleOpeningEditor} closeICS={this.closeICS} initParams={initParams} />
    )
  }

  render() {
    const { height, top, handleCropping } = this.state
    const styles = this.canSetCalculatedStyles() ? {} : { height: `${height}px`, top: `${top}px`, bottom: `${top}px` }

    return (
      <div
        className={s.icsWrapper}
        ref={el => (this.icsWrapper = el)}
        onTouchEnd={handleCropping}
        onMouseUp={handleCropping}
        onClick={this._handleCloseICS}
      >
        <button type='button' className={s.closeBtn} onClick={this.closeICS}>
          <SVGIconGenerator
            iconName='Cross'
            customClass={s.closeIcon}
            preset={{ type: ICON_TYPE, subtype: 'CROSS' }}
            fill={DEFAULT_FILL}
            onHover={{ active: true, fill: HOVER_FILL }}
          />
        </button>
        <div className={s['ics']} ref={el => (this.icsWindow = el)} style={styles}>
          {this.renderEditor()}
        </div>
      </div>
    )
  }
}

export default ImagesCropSystem
