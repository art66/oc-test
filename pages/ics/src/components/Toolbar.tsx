import React, { Component } from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import ToolButton from './ToolButton'
import s from '../styles/Toolbar.cssmodule.scss'

interface IProps {
  mode: string
  setDragMode: (mode: string) => void
  zoomIn: () => void
  zoomTo100: () => void
  zoomOut: () => void
  rotateLeft: () => void
  rotateRight: () => void
  cropBoxAllowed: boolean
  isRestricted: boolean
}

class Toolbar extends Component<IProps> {
  setMoveDragMode = () => {
    const { setDragMode } = this.props

    setDragMode('move')
  }

  setCropDragMode = () => {
    const { setDragMode } = this.props

    setDragMode('crop')
  }

  render() {
    const {
      mode,
      zoomIn,
      zoomTo100,
      zoomOut,
      rotateLeft,
      rotateRight,
      cropBoxAllowed,
      isRestricted
    } = this.props
    const isCropMode = mode === 'crop'
    let cropBtnPreset = 'CROP'

    if (isCropMode) {
      cropBtnPreset = 'ACTIVE'
    }
    if (!cropBoxAllowed) {
      cropBtnPreset = 'DISABLE'
    }

    return (
      <div className={s.toolbar}>
        <ToolButton
          buttonID='btn-move'
          onCLickHandler={this.setMoveDragMode}
          ariaLabel='Set move drag mode'
          iconName='Pointer'
          presetName={isCropMode ? 'POINTER' : 'ACTIVE'}
          hoverPresetName='DEFAULT_HOVER'
          isHoverActive={isCropMode}
          tooltipText='Move (M)'
        />
        <ToolButton
          buttonID='btn-crop'
          onCLickHandler={this.setCropDragMode}
          ariaLabel='Set crop drag mode'
          iconName='Crop'
          presetName={cropBtnPreset}
          hoverPresetName='DEFAULT_HOVER'
          isHoverActive={!isCropMode && (cropBoxAllowed || !isRestricted)}
          tooltipText='Crop (C)'
          disabled={!cropBoxAllowed}
        />
        <ToolButton
          buttonID='btn-zoom-in'
          onCLickHandler={zoomIn}
          ariaLabel='Zoom in'
          iconName='Plus'
          presetName='PLUS'
          hoverPresetName='PLUS_HOVER'
          isHoverActive={true}
          tooltipText='Zoom In (I)'
        />
        <ToolButton
          buttonID='btn-zoom-to-100'
          onCLickHandler={zoomTo100}
          ariaLabel='Zoom to 100'
          iconName='Scale'
          presetName='SCALE'
          hoverPresetName='SCALE_HOVER'
          isHoverActive={true}
          tooltipText='Zoom To 100% (P)'
        />
        <ToolButton
          buttonID='btn-zoom-out'
          onCLickHandler={zoomOut}
          ariaLabel='Zoom out'
          iconName='Minus'
          presetName='MINUS'
          hoverPresetName='MINUS_HOVER'
          isHoverActive={true}
          tooltipText='Zoom Out (O)'
        />
        <ToolButton
          buttonID='btn-rotate-left'
          onCLickHandler={rotateLeft}
          ariaLabel='Rotate left'
          type='left'
          iconName='Rotate'
          presetName='ROTATE'
          hoverPresetName='DEFAULT_HOVER'
          isHoverActive={true}
          tooltipText='Rotate Left (L)'
        />
        <ToolButton
          buttonID='btn-rotate-right'
          onCLickHandler={rotateRight}
          ariaLabel='Rotate right'
          type='right'
          iconName='Rotate'
          presetName='ROTATE'
          hoverPresetName='DEFAULT_HOVER'
          isHoverActive={true}
          tooltipText='Rotate Right (R)'
        />
      </div>
    )
  }
}

export default Toolbar
