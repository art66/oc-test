import React, { Component } from 'react'
import cn from 'classnames'
import s from '../styles/SocialGuidelines.cssmodule.scss'

interface IProps {
  hideGuidelines: boolean
}

class SocialGuidelines extends Component<IProps> {
  render() {
    const { hideGuidelines } = this.props

    return (
      <div className={cn(s.socialGuidelines, { [s.hide]: hideGuidelines })}>
        <span className={s.paragraph}>Do not upload any offensive material. </span>
        <span className={s.paragraph}>
          Please keep all content PG13 and abide by our <a className={s.link} href='/rules.php'>Social Guidelines</a>.
        </span>
      </div>
    )
  }
}

export default SocialGuidelines
