import React, { Component } from 'react'
import { connect } from 'react-redux'
import INotifications from '../models/INotifications'
import s from '../styles/Notifications.cssmodule.scss'

interface IProps {
  ics: {
    notifications: INotifications
  }
}

class Notifications extends Component<IProps> {
  renderServerNotifications = () => {
    const { ics: { notifications } } = this.props

    return notifications && notifications.serverMessages && notifications.serverMessages
      .map(message => {
        return <div key={message} className={s.notification}>{message}</div>
      })
  }

  renderClientNotifications = () => {
    const { ics: { notifications } } = this.props

    return notifications && Object.keys(notifications).map(key => {
      return (
        notifications[key]?.show ?
          (
            <div key={key} className={s.notification}>
              {notifications[key].message}
            </div>
          ) : null
      )
    })
  }

  render() {
    return (
      <div className={s['notifications']}>
        {this.renderClientNotifications()}
        {this.renderServerNotifications()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  ics: state.ics
})

export default connect(mapStateToProps)(Notifications)
