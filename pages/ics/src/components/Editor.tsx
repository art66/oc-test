import React, { Component } from 'react'
import Cropper from 'cropperjs'
import { connect } from 'react-redux'
import cn from 'classnames'
import Preloader from '@torn/shared/components/Preloader'
import Toolbar from './Toolbar'
import Navbar from './Navbar'
import { /*fetchUrl, */ validateImage, checkImageDimensions } from '../utils'
import {
  isCropping,
  setDragMode,
  cropImage,
  isLoaded,
  hideNotifications,
  isOpening,
  toggleNotifications,
  setErrorsFromServer
} from '../modules'
import Notifications from './Notifications'
import IRestrictions from '../models/IRestrictions'
import IConstraints from '../models/IConstraints'
import INotifications from '../models/INotifications'
import IInitParams from '../models/IInitParams'
import s from '../styles/Editor.cssmodule.scss'

interface IState extends IRestrictions {
  uploading: boolean
}

interface IProps {
  img?: string
  cropImage: (imageUrl: string, cropped: boolean) => void
  isLoaded: (loaded: boolean) => void
  isCropping: (cropping: boolean) => void
  isOpening: (opening: boolean) => void
  setDragMode: (mode: Cropper.DragMode | string) => void
  hideNotifications: () => void
  toggleNotifications: (keys: string[], show: boolean) => void
  closeICS: () => void
  setErrorsFromServer: (messages: string[]) => void
  handleCropping: (callback: () => void) => void
  ics: {
    cropped: boolean
    opening: boolean
    cropResult: string
    fileType: string
    cropping: boolean
    src: string
    mode: string
    loaded: boolean
    showNotification: boolean
    restrictions: IConstraints
    notifications: INotifications
  }
  initParams: IInitParams
}

interface ICropper extends Cropper {
  cropBoxData?: any
  stop?: () => void
  options?: {
    dragMode: Cropper.DragMode | string
  }
}

class Editor extends Component<IProps, IState> {
  cropper: ICropper
  data: any
  canvasData: HTMLCanvasElement | null
  img: HTMLImageElement
  cropBoxData: HTMLImageElement | HTMLCanvasElement | null
  canvasEl: HTMLElement
  initialCropBoxPosition: {
    left: number
    top: number
  }

  initialImageDimensions: {
    width: number
    height: number
  }

  constructor(props: IProps) {
    super(props)
    const {
      ics: { restrictions }
    } = this.props

    this.state = {
      width: (restrictions && restrictions.dimensions.width) || '',
      height: (restrictions && restrictions.dimensions.height) || '',
      maxWidth: (restrictions && restrictions.dimensions.maxWidth) || '',
      maxHeight: (restrictions && restrictions.dimensions.maxHeight) || '',
      fileSize: (restrictions && restrictions.maxSize) || '',
      uploading: false
    }
  }

  componentDidMount() {
    const { width, height } = this.state
    const { handleCropping: handleCrop, isOpening: isOpeningICS } = this.props
    const cropBoxAllowed = this.checkAllowableCropBoxSize()

    document.addEventListener('keydown', this.handleKeyPress)
    window.addEventListener('resize', this.handleResizeWindow)
    handleCrop(this.isCropping)

    if (!cropBoxAllowed) {
      const { toggleNotifications: toggle } = this.props

      toggle(['cropBoxSize'], true)
    }
    this.img.onload = () => {
      this.initialImageDimensions = { width: this.img.naturalWidth, height: this.img.naturalHeight }
      const imageHasCorrectDimensions = checkImageDimensions(this.initialImageDimensions, width, height)
      const setCropBoxByDefault = !!(width && height) && cropBoxAllowed && !imageHasCorrectDimensions

      this.cropper = new Cropper(this.img, {
        dragMode: setCropBoxByDefault ? ('crop' as Cropper.DragMode) : ('move' as Cropper.DragMode),
        background: false,
        cropBoxResizable: !width || !height,
        autoCrop: setCropBoxByDefault,
        ready: () => {
          if (this.data) {
            this.cropper
              .crop()
              .setData(this.data)
              .setCanvasData(this.canvasData)
              .setCropBoxData(this.cropBoxData)

            this.data = null
            this.canvasData = null
            this.cropBoxData = null
          }
          this.cropper.zoomTo(1)
          isOpeningICS(false)

          if (setCropBoxByDefault) {
            this.setDragMode('crop')
            this.isCropping()
            const cropBox = this.cropper.getCropBoxData()

            this.initialCropBoxPosition = { top: cropBox.top, left: cropBox.left }
            const container = this.cropper.getContainerData()

            this.cropper.setCropBoxData({
              width: +width,
              height: +height,
              top: (container.height - cropBox.height) / 2,
              left: (container.width - cropBox.width) / 2
            })
          }
        }
      })
    }
  }

  componentDidUpdate() {
    if (this.cropper) {
      const { width, height, maxWidth, maxHeight } = this.state
      const cropBox = this.cropper.getCropBoxData()

      if (width || height) {
        this.updateCropBoxWithStrongRestrictions(width, height, maxHeight, maxWidth, cropBox)
      } else if (maxWidth || maxHeight) {
        this.updateCropBoxWithoutStrongRestrictions(maxHeight, maxWidth, cropBox)
      }
    }
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress)
    window.removeEventListener('resize', this.handleResizeWindow)
  }

  getNotCroppedChangedImage = (roundedHeight, roundedWidth) => {
    const { ics } = this.props
    const { naturalHeight, naturalWidth } = this.cropper.getCanvasData()
    const imageData = this.cropper.getImageData()
    const canvasData = this.cropper.getCanvasData()
    let notCroppedChangedImg = this.cropper
      .getCroppedCanvas({
        width: canvasData.width,
        height: canvasData.height
      })
      .toDataURL(ics.fileType)

    if (naturalHeight < roundedHeight && naturalWidth < roundedWidth) {
      notCroppedChangedImg = this.cropper
        .getCroppedCanvas({
          minWidth: canvasData.width,
          minHeight: canvasData.height
        })
        .toDataURL(ics.fileType)
    }

    if (naturalHeight > roundedHeight && naturalWidth > roundedWidth) {
      if (imageData.naturalWidth > imageData.naturalHeight) {
        notCroppedChangedImg = this.cropper
          .getCroppedCanvas({
            width: canvasData.width,
            maxHeight: canvasData.height
          })
          .toDataURL(ics.fileType)
      } else {
        notCroppedChangedImg = this.cropper
          .getCroppedCanvas({
            height: canvasData.height,
            maxWidth: canvasData.width
          })
          .toDataURL(ics.fileType)
      }
    }
    return notCroppedChangedImg
  }

  handleResizeWindow = () => {
    const { toggleNotifications: toggleMessages } = this.props
    const cropBoxAllowed = this.checkAllowableCropBoxSize()

    if (cropBoxAllowed) {
      this.setCropBox()
      toggleMessages(['cropBoxSize'], false)
    } else {
      this.cancelCropMode()
      toggleMessages(['cropBoxSize'], true)
    }
  }

  handleKeyPress = e => {
    const { uploading } = this.state
    const {
      ics: { loaded }
    } = this.props

    if (uploading) {
      return
    }

    if (loaded) {
      this.handleKey(e)
    }
  }

  handleKey = (e) => {
    switch (e.key) {
      case 'z':
        this.resetImage()
        break
      case 's':
        this.saveImage()
        break
      case 'ArrowLeft':
        e.preventDefault()
        this.cropper.move(-1, 0)
        break
      case 'ArrowUp':
        e.preventDefault()
        this.cropper.move(0, -1)
        break
      case 'ArrowRight':
        e.preventDefault()
        this.cropper.move(1, 0)
        break
      case 'ArrowDown':
        e.preventDefault()
        this.cropper.move(0, 1)
        break
      case 'c':
        this.setDragMode('crop')
        break
      case 'm':
        this.setDragMode('move')
        break
      case 'i':
        this.zoomIn()
        break
      case 'o':
        this.zoomOut()
        break
      case 'p':
        this.zoomTo100()
        break
      case 'l':
        this.rotateLeft()
        break
      case 'r':
        this.rotateRight()
        break
      default:
        break
    }
  }

  cancelCropMode = () => {
    const { cropImage: crop } = this.props

    this.cropper.clear()
    this.setDragMode('move')
    crop('', false)
  }

  checkAllowableCropBoxSize = () => {
    const canvasElRect = this.canvasEl && this.canvasEl.getBoundingClientRect()
    const { width, height } = this.state

    return canvasElRect && canvasElRect.width >= width && canvasElRect.height >= height
  }

  updateCropBoxWithStrongRestrictions = (width, height, maxHeight, maxWidth, cropBox) => {
    if (width && height) {
      this.cropper.setCropBoxData({ width: +width, height: +height })
    } else if (width && maxHeight) {
      const tempHeight = maxHeight < cropBox.height ? maxHeight : cropBox.height

      this.cropper.setCropBoxData({ width: +width, height: +tempHeight })
    } else if (height && maxWidth) {
      const tempWidth = maxWidth < cropBox.width ? maxWidth : cropBox.width

      this.cropper.setCropBoxData({ width: +tempWidth, height: +height })
    } else if (width) {
      this.cropper.setCropBoxData({ width: +width })
    } else if (height) {
      this.cropper.setCropBoxData({ height: +height })
    }
  }

  updateCropBoxWithoutStrongRestrictions = (maxHeight, maxWidth, cropBox) => {
    const tempWidth = maxWidth && maxWidth < cropBox.width ? maxWidth : cropBox.width
    const tempHeight = maxHeight && maxHeight < cropBox.height ? maxHeight : cropBox.height

    this.cropper.setCropBoxData({ width: +tempWidth, height: +tempHeight })
  }

  saveImage = () => {
    const { uploading } = this.state
    const { ics, hideNotifications: hideMessages, toggleNotifications: toggleMessages } = this.props

    if (uploading) {
      return
    }

    const cropBox = this.cropper.getCropBoxData()
    let validationResult = {}
    let imageSrc = ics.src
    const cropBoxAllowed = this.checkAllowableCropBoxSize()

    hideMessages()
    if (!cropBoxAllowed) {
      toggleMessages(['cropBoxSize'], true)
    }

    if (this.cropper && ics.cropping && cropBox.width) {
      const croppedImage = this.cropper
        .getCroppedCanvas({ width: cropBox.width, height: cropBox.height })
        .toDataURL(ics.fileType)

      validationResult = validateImage(cropBox, croppedImage, this.state)
      imageSrc = croppedImage
    } else {
      const { rotate } = this.cropper.getData()
      const { naturalHeight, naturalWidth } = this.cropper.getCanvasData()
      const imageData = this.cropper.getImageData()
      const canvasData = this.cropper.getCanvasData()

      if (
        rotate === 0
        && imageData.height === Math.round(imageData.naturalHeight)
        && imageData.width === Math.round(imageData.naturalWidth)
      ) {
        validationResult = validateImage({ width: naturalWidth, height: naturalHeight }, ics.src, this.state)
      } else {
        const roundedHeight = Math.round(canvasData.height)
        const roundedWidth = Math.round(canvasData.width)
        const notCroppedChangedImg = this.getNotCroppedChangedImage(roundedHeight, roundedWidth)
        const dimensions = { width: roundedWidth, height: roundedHeight }

        validationResult = validateImage(dimensions, notCroppedChangedImg, this.state)
        imageSrc = notCroppedChangedImg
      }
    }
    const notificationsKeys = Object.keys(validationResult).filter(item => !validationResult[item])

    if (notificationsKeys.length) {
      toggleMessages(notificationsKeys, true)
    } else {
      this.uploadImage(imageSrc)
    }
  }

  uploadImage = imageSrc => {
    // TODO: recheck the process of uploading images
    const { /*setErrorsFromServer: setErrors, */ closeICS, initParams } = this.props

    const onImageUploadedEvent = document.createEvent('CustomEvent')

    onImageUploadedEvent.initCustomEvent('onImageUploaded', false, false, {})
    initParams.callback && initParams.callback(imageSrc)
    document.dispatchEvent(onImageUploadedEvent)
    closeICS()
    /*const reqData = {
      method: 'upload',
      imageData: imageSrc,
      imageType: initParams.imageType,
      parameters: initParams.parameters
    }

    this.setState({
      uploading: true
    })

    fetchUrl('/ics.php', reqData).then(
      data => {
        if (data.success) {
          const onImageUploadedEvent = document.createEvent('CustomEvent')

          onImageUploadedEvent.initCustomEvent('onImageUploaded', false, false, {})
          document.dispatchEvent(onImageUploadedEvent)
          initParams.callback && initParams.callback(data)
          closeICS()
        } else {
          setErrors(data.messages)
        }

        this.setState({
          uploading: false
        })
      },
      error => {
        console.error(error)
      }
    )*/
  }

  resetImage = () => {
    const { uploading, width, height } = this.state

    if (uploading) {
      return
    }

    const cropBoxAllowed = this.checkAllowableCropBoxSize()
    const imageHasCorrectDimensions = checkImageDimensions(this.initialImageDimensions, width, height)
    const setCropBoxByDefault = !!(width && height) && cropBoxAllowed && !imageHasCorrectDimensions

    this.resetEditor()

    if (setCropBoxByDefault) {
      this.setCropBoxByDefault()
    } else {
      this.setDragMode('move')
    }
    if (!imageHasCorrectDimensions) {
      this.setCropBox()
    }
  }

  resetEditor = () => {
    const { cropImage: crop } = this.props

    this.cropper.reset()
    this.cropper.clear()
    crop('', false)
    this.zoomTo100()
  }

  setCropBoxByDefault = () => {
    this.setDragMode('crop')
    this.isCropping()
    this.cropper.crop()
  }

  isCropping = () => {
    const {
      ics: { mode, cropped },
      isCropping: isActiveCropping
    } = this.props

    if (mode && mode === 'crop' && !cropped) {
      isActiveCropping(true)
    }
  }

  hideNotifications = () => {
    const { hideNotifications: hideMessages, toggleNotifications: toggleMessages } = this.props
    const cropBoxAllowed = this.checkAllowableCropBoxSize()

    hideMessages()

    if (!cropBoxAllowed) {
      toggleMessages(['cropBoxSize'], true)
    }
  }

  setDragMode = mode => {
    const { uploading } = this.state
    const { setDragMode: setMode } = this.props

    if (uploading) {
      return
    }
    const cropBoxAllowed = this.checkAllowableCropBoxSize()

    if (mode === 'crop' && !cropBoxAllowed) {
      return
    }

    this.hideNotifications()
    setMode(mode)

    this.cropper.setDragMode(mode)
  }

  zoomIn = () => {
    const { uploading } = this.state

    if (uploading) {
      return
    }
    this.hideNotifications()
    this.cropper.zoom(0.1)
  }

  zoomOut = () => {
    const { uploading } = this.state

    if (uploading) {
      return
    }
    this.hideNotifications()
    this.cropper.zoom(-0.1)
  }

  zoomTo100 = () => {
    const { uploading } = this.state

    if (uploading) {
      return
    }
    this.hideNotifications()
    this.cropper.zoomTo(1)
  }

  rotateLeft = () => {
    const { uploading } = this.state

    if (uploading) {
      return
    }
    this.hideNotifications()
    this.cropper.rotate(-90)
  }

  rotateRight = () => {
    const { uploading } = this.state

    if (uploading) {
      return
    }
    this.hideNotifications()
    this.cropper.rotate(90)
  }

  setCropBox = () => {
    const { width, height } = this.state
    const cropBoxAllowed = this.checkAllowableCropBoxSize()

    if (!!(width && height) && cropBoxAllowed) {
      this.setDragMode('crop')
      const container = this.cropper.getContainerData()

      this.cropper.setCropBoxData({
        width: +width,
        height: +height,
        top: (container.height - +height) / 2,
        left: (container.width - +width) / 2
      })
      this.cropper.crop()
    }
  }

  renderToolbar = () => {
    const { ics } = this.props
    const { width, height } = this.state
    const isRestricted = !!(width && height)
    const cropBoxAllowed = this.checkAllowableCropBoxSize()

    return !ics.cropped ? (
      <Toolbar
        mode={ics.mode}
        setDragMode={this.setDragMode}
        zoomIn={this.zoomIn}
        zoomTo100={this.zoomTo100}
        zoomOut={this.zoomOut}
        rotateLeft={this.rotateLeft}
        rotateRight={this.rotateRight}
        cropBoxAllowed={cropBoxAllowed}
        isRestricted={isRestricted}
      />
    ) : null
  }

  render() {
    const { ics } = this.props
    const { uploading } = this.state

    return (
      <div className={s['editor']}>
        <div id='editor-id' className={s['editor']}>
          <Notifications />
          {ics.opening ? <Preloader /> : ''}
          <div
            className={cn(s.canvas, { [s.hidden]: ics.opening })}
            ref={el => (this.canvasEl = el)}
            onClick={this.isCropping}
            onTouchStart={this.isCropping}
            onTouchEnd={this.isCropping}
          >
            <img
              ref={img => {
                this.img = img
              }}
              src={ics.src}
              alt=''
            />
          </div>
          {this.renderToolbar()}
          <Navbar uploading={uploading} saveImage={this.saveImage} resetImage={this.resetImage} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  ics: state.ics
})

const mapDispatchToProps = {
  isCropping,
  setDragMode,
  cropImage,
  isLoaded,
  hideNotifications,
  isOpening,
  toggleNotifications,
  setErrorsFromServer
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor)
