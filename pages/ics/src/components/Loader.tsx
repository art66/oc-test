import React, { Component, Fragment } from 'react'
import Dropzone from 'react-dropzone'
import { connect } from 'react-redux'
import cn from 'classnames'
import Preloader from '@torn/shared/components/Preloader'
import { setImage, isLoaded, isOpening, hideNotifications, toggleNotifications } from '../modules'
import Notifications from './Notifications'
import IConstraints from '../models/IConstraints'
import INotifications from '../models/INotifications'
import SocialGuidelines from './SocialGuidelines'
import s from '../styles/Loader.cssmodule.scss'

interface IProps {
  setImage: (result: string | ArrayBuffer, type: string) => void
  isLoaded: (loaded: boolean) => void
  isOpening: (opening: boolean) => void
  toggleNotifications: (keys: string[], show: boolean) => void
  hideNotifications: () => void
  ics: {
    showNotification?: boolean
    restrictions: IConstraints
    notifications: INotifications
    opening?: boolean
  }
}

class Loader extends Component<IProps> {
  handleDrop = files => {
    const file = files[0]

    this.handleImageLoad(file)
  }

  handleImageLoad = (file) => {
    const {
      ics,
      hideNotifications: hideMessages,
      setImage: setImg,
      isOpening: isImageOpening,
      isLoaded: isImageLoaded,
      toggleNotifications: toggleMessages
    } = this.props
    const { allowedExtensions } = ics.restrictions
    const reader = new FileReader()
    const isAllow = allowedExtensions.some(item => file.type.includes(item))

    if (file && /^image\/(\w|\+)+$/.test(file.type) && isAllow) {
      reader.onloadend = () => {
        setImg(reader.result, file.type)
        hideMessages()
        isImageOpening(true)
        isImageLoaded(true)
      }
      reader.readAsDataURL(file)
    } else {
      toggleMessages(['fileTypes'], true)
    }
  }

  fileChangedHandler = e => {
    e.preventDefault()
    const file = e.target.files[0]

    this.handleImageLoad(file)
  }

  render() {
    const { ics } = this.props
    const { notifications, opening } = ics
    const hideGuidelines = notifications && Object.keys(notifications).some(item => notifications[item].show)

    return (
      <Fragment>
        <SocialGuidelines hideGuidelines={hideGuidelines || opening} />
        {opening ? <Preloader /> : ''}
        <div className={cn(s.loader, { [s.hide]: opening })}>
          <Notifications />
          <div className={s.dngAreaWrapper}>
            <Dropzone
              disableClick={true}
              className='drag-and-drop-area'
              onDrop={this.handleDrop}
              multiple={false}
            />
            <div className={`${s.corner} ${s.left} ${s.top}`} />
            <div className={`${s.corner} ${s.right} ${s.top}`} />
            <div className={`${s.corner} ${s.right} ${s.bottom}`} />
            <div className={`${s.corner} ${s.left} ${s.bottom}`} />
            <div className={s.uploadSection}>
              <div className={s.uploadImg} />
              <p className={s['description']}>
                Drop image here or{' '}
                <label htmlFor='browse-file' className={s['browse']}>
                  browse...
                </label>
                <input
                  id='browse-file'
                  type='file'
                  className={s['sr-only']}
                  accept='image/*'
                  onChange={this.fileChangedHandler}
                />
              </p>
            </div>
          </div>
        </div>
        <div className={s.navbar}>
          <button type='button' className={`${s.doneButton} torn-btn disabled`}>DONE</button>
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  ics: state.ics
})

const mapDispatchToProps = {
  setImage,
  isLoaded,
  isOpening,
  hideNotifications,
  toggleNotifications
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Loader)
