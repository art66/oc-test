import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { ICON_TYPE } from '../constants'
import s from '../styles/Toolbar.cssmodule.scss'

interface IProps {
  onCLickHandler: () => void
  buttonID: string
  ariaLabel: string
  iconName: string
  presetName: string
  hoverPresetName: string
  isHoverActive: boolean
  tooltipText: string
  type?: string
  disabled?: boolean
}

class ToolButton extends Component<IProps> {
  render() {
    const {
      onCLickHandler,
      buttonID,
      ariaLabel,
      iconName,
      presetName,
      hoverPresetName,
      isHoverActive,
      tooltipText,
      type,
      disabled
    } = this.props

    return (
      <button
        type='button'
        id={buttonID}
        className={cn(s['toolbar-button'], { [s.disabled]: disabled })}
        onClick={onCLickHandler}
        aria-label={ariaLabel}
      >
        <SVGIconGenerator
          iconName={iconName}
          type={type}
          preset={{ type: ICON_TYPE, subtype: presetName }}
          onHover={{ active: isHoverActive, preset: { type: ICON_TYPE, subtype: hoverPresetName } }}
          customClass={s.toolsIcon}
        />
        <Tooltip parent={buttonID} arrow='center' position='top'>
          <span>{tooltipText}</span>
        </Tooltip>
      </button>
    )
  }
}

export default ToolButton
