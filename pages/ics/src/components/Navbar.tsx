import React, { Component } from 'react'
import Preloader from '@torn/shared/components/Preloader'
import cn from 'classnames'
import s from '../styles/Navbar.cssmodule.scss'

interface IProps {
  saveImage: () => void
  resetImage: () => void
  uploading: boolean
}

class Navbar extends Component<IProps> {
  render() {
    const { saveImage, resetImage, uploading } = this.props

    return (
      <div className={s.navbar}>
        <button type='button' className={`${cn(s.doneButton, { [s.hide]: uploading })} torn-btn`} onClick={saveImage}>
          {uploading ? <Preloader /> : ''}
          <span className={s.lbl}>DONE</span>
        </button>
        <button type='button' className={s.backButton} onClick={resetImage}>Reset</button>
      </div>
    )
  }
}

export default Navbar
