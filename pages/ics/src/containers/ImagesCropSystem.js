import { connect } from 'react-redux'
import {
  setImage,
  isLoaded,
  isCropping,
  setDragMode,
  cropImage,
  toggleNotifications,
  getConstraints,
  hideNotifications,
  resetICS
} from '../modules'

import ImagesCropSystem from '../components/ImagesCropSystem'

const mapStateToProps = state => ({
  ics: state.ics,
  browser: state.browser
})

const mapDispatchToProps = {
  setImage,
  isLoaded,
  isCropping,
  setDragMode,
  cropImage,
  toggleNotifications,
  getConstraints,
  hideNotifications,
  resetICS
}

export default connect(mapStateToProps, mapDispatchToProps)(ImagesCropSystem)
