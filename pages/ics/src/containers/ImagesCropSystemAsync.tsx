import React from 'react'
import Loadable from 'react-loadable'

import reducer from '../modules'
import { Provider } from 'react-redux'

const ICSBundle = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const ICS = await import(/* webpackChunkName: "ics" */ './ImagesCropSystem')

    return ICS
  },
  render(asyncComponent: any, { rootStore, injector, initParams }: any) {
    const { default: ICS } = asyncComponent

    injector(rootStore, { reducer, key: 'ics' })

    return (
      <Provider store={rootStore}>
        <ICS initParams={initParams} />
      </Provider>
    )
  },
  loading: () => <div />
})

export default ICSBundle
