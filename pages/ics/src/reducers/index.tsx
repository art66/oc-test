import * as actionTypes from '../actionTypes'

export const ACTION_HANDLERS = {
  [actionTypes.SET_CONSTRAINTS]: (state, action) => {
    return {
      ...state,
      restrictions: action.constraints
    }
  },
  [actionTypes.SET_NOTIFICATIONS]: (state, action) => {
    return {
      ...state,
      notifications: action.notifications
    }
  },
  [actionTypes.TOGGLE_NOTIFICATIONS]: (state, action) => {
    return {
      ...state,
      notifications: {
        ...state.notifications,
        ...Object.keys(state.notifications).reduce((acc, key) => {
          const hasKey = action.keys.some(item => item === key)

          if (hasKey) {
            acc[key] = {
              message: state.notifications[key].message,
              show: action.show
            }
          }
          return acc
        }, {})
      }
    }
  },
  [actionTypes.HIDE_NOTIFICATIONS]: state => {
    return {
      ...state,
      notifications: {
        ...state.notifications,
        ...Object.keys(state.notifications).reduce((acc, key) => {
          acc[key] = {
            message: state.notifications[key].message,
            show: false
          }
          return acc
        }, {}),
        serverMessages: []
      }
    }
  },
  [actionTypes.SET_ERRORS_FROM_SERVER]: (state, action) => {
    return {
      ...state,
      notifications: {
        ...state.notifications,
        serverMessages: action.errors
      }
    }
  },
  [actionTypes.HIDE_ERRORS_FROM_SERVER]: state => {
    return {
      ...state,
      notifications: {
        ...state.notifications,
        serverMessages: []
      }
    }
  },
  [actionTypes.SET_IMAGE]: (state, action) => {
    return {
      ...state,
      src: action.src,
      fileType: action.fileType
    }
  },
  [actionTypes.IS_LOADED]: (state, action) => {
    return {
      ...state,
      loaded: action.loaded
    }
  },
  [actionTypes.IS_OPENING]: (state, action) => {
    return {
      ...state,
      opening: action.opening
    }
  },
  [actionTypes.IS_CROPPING]: (state, action) => {
    return {
      ...state,
      cropping: action.cropping
    }
  },
  [actionTypes.SET_DRAG_MODE]: (state, action) => {
    return {
      ...state,
      mode: action.mode
    }
  },
  [actionTypes.CROP_IMAGE]: (state, action) => {
    return {
      ...state,
      cropResult: action.cropResult,
      cropped: action.cropped,
      cropping: false
    }
  },
  [actionTypes.GET_AJAX_ERROR]: state => {
    return {
      ...state
    }
  },
  [actionTypes.RESET_ICS]: () => ({})
}
