import { fetchUrl, toDataURL, getFileTypeByImageURL, generateNotifications } from '../utils'
import * as actionTypes from '../actionTypes'
import { ACTION_HANDLERS } from '../reducers'

export const setConstraints = constraints => {
  return {
    type: actionTypes.SET_CONSTRAINTS,
    constraints
  }
}

const getAjaxError = error => {
  console.log(error)
  return {
    type: actionTypes.GET_AJAX_ERROR,
    error
  }
}

export const setErrorsFromServer = errors => {
  return {
    type: actionTypes.SET_ERRORS_FROM_SERVER,
    errors
  }
}

export const hideErrorsFromServer = () => {
  return {
    type: actionTypes.HIDE_ERRORS_FROM_SERVER
  }
}

const setNotifications = notifications => {
  return {
    type: actionTypes.SET_NOTIFICATIONS,
    notifications
  }
}

export const hideNotifications = () => {
  return {
    type: actionTypes.HIDE_NOTIFICATIONS
  }
}

export const setImage = (src, fileType) => {
  return {
    type: actionTypes.SET_IMAGE,
    src,
    fileType
  }
}

export const isLoaded = loaded => {
  return {
    type: actionTypes.IS_LOADED,
    loaded
  }
}

export const isOpening = opening => {
  return {
    type: actionTypes.IS_OPENING,
    opening
  }
}

export const isCropping = cropping => {
  return {
    type: actionTypes.IS_CROPPING,
    cropping
  }
}

export const setDragMode = mode => {
  return {
    type: actionTypes.SET_DRAG_MODE,
    mode
  }
}

export const cropImage = (cropResult, cropped) => {
  return {
    type: actionTypes.CROP_IMAGE,
    cropResult,
    cropped
  }
}

export const toggleNotifications = (keys, show) => {
  return {
    type: actionTypes.TOGGLE_NOTIFICATIONS,
    keys,
    show
  }
}

export const getConstraints = initParams => dispatch => {
  const { imageType, imageUrl } = initParams
  const data = {
    method: 'getConstraints',
    imageType
  }

  if (imageUrl) {
    dispatch(isOpening(true))
  }

  return fetchUrl('/ics.php', data).then(
    json => {
      const notifications = generateNotifications(json)

      dispatch(setConstraints(json))
      dispatch(setNotifications(notifications))

      if (imageUrl) {
        toDataURL(imageUrl, dataUrl => {
          dispatch(isOpening(false))
          dispatch(setImage(dataUrl, getFileTypeByImageURL(imageUrl)))
          dispatch(isLoaded(true))
        })
      }
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const resetICS = () => {
  return {
    type: actionTypes.RESET_ICS
  }
}

const initialState = {}

export default function icsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
