import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import createStore from './store/createStore'
import ImagesCropSystem from './containers/ImagesCropSystem'

// ========================================================
// Store Instantiation
// ========================================================
const initialState = window.___INITIAL_STATE__
const store = createStore(initialState)
const MOUNT_NODE = document.getElementById('icsroot')

const render = initParams => {
  ReactDOM.render(
    <Provider store={store}>
      <ImagesCropSystem initParams={initParams} />
    </Provider>,
    MOUNT_NODE
  )
}

if (__DEV__) {
  render({ imageType: 'demoStrict' })
}

window.renderICS = render

window.unmountICS = () => ReactDOM.unmountComponentAtNode(MOUNT_NODE)
