import INotifications from '../models/INotifications'

const generateMsgAboutFileTypes = types => {
  const uppercaseTypes = types.map(item => item.toUpperCase())
  const amountTypes = uppercaseTypes.length

  if (amountTypes === 1) {
    return `Only ${uppercaseTypes[0]} format is accepted.`
  }
  const typesToStr = `${uppercaseTypes.slice(0, amountTypes - 1).join(', ')} and ${uppercaseTypes[amountTypes - 1]}`

  return `Only ${typesToStr} formats are accepted.`
}

const bytesToSize = bytes => {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']

  if (bytes === 0) {
    return 'Byte'
  }
  const i = parseInt((Math.floor(Math.log(bytes) / Math.log(1024))) + '', 10)

  return `${(bytes / Math.pow(1024, i)).toFixed(2)} ${sizes[i]}`
}

const getMsgForStrictDimensions = (label, value) => {
  return {
    message: `The image ${label} should be ${value}px.`,
    show: false
  }
}

const getMsgForNotStrictDimensions = (label, value) => {
  return {
    message: `The image ${label} is too big (${value}px max).`,
    show: false
  }
}

export const generateNotifications = restrictions => {
  const { allowedExtensions, dimensions, maxSize } = restrictions
  const notifications:INotifications = {
    fileTypes: {
      message: generateMsgAboutFileTypes(allowedExtensions),
      show: false
    },
    fileSize: {
      message: `File is too large (max ${bytesToSize(maxSize)})`,
      show: false
    },
    cropBoxSize: {
      message: 'Crop area is larger than your screen. Please upload an image that matches the requirements or use'
        + ' a larger screen to crop your image.',
      show: false
    }
  }

  if (dimensions.width) {
    notifications.width = getMsgForStrictDimensions('width', dimensions.width)
  }
  if (dimensions.height) {
    notifications.height = getMsgForStrictDimensions('height', dimensions.height)
  }
  if (dimensions.maxWidth) {
    notifications.maxWidth = getMsgForNotStrictDimensions('width', dimensions.maxWidth)
  }
  if (dimensions.maxHeight) {
    notifications.maxHeight = getMsgForNotStrictDimensions('height', dimensions.maxHeight)
  }

  return notifications
}
