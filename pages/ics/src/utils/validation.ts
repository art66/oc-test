const checkFileSize = src => {
  const base64str = src.substr(src.indexOf('base64,') + 'base64,'.length)
  const decoded = atob(base64str)

  return decoded.length
}

export const validateImage = (data, src, state) => {
  const { width, height, maxWidth, maxHeight, fileSize } = state
  const validationResult = {
    width: true,
    height: true,
    maxWidth: true,
    maxHeight: true,
    fileSize: checkFileSize(src) <= fileSize || !fileSize
  }

  if (width || height) {
    if (width) {
      validationResult.width = data.width === +width
    }
    if (height) {
      validationResult.height = data.height === +height
    }
    if (maxHeight) {
      validationResult.maxHeight = data.height <= +maxHeight
    }
    if (maxWidth) {
      validationResult.maxWidth = data.width <= +maxWidth
    }
  } else if (maxWidth || maxHeight) {
    validationResult.maxHeight = !maxHeight || data.height <= +maxHeight
    validationResult.maxWidth = !maxWidth || data.width <= +maxWidth
  }

  return validationResult
}

export const checkImageDimensions = (initialImageDimensions, width, height) => {
  return width === initialImageDimensions.width && height === initialImageDimensions.height
}
