export function toDataURL(url: string, callback: any) {
  const xhr = new XMLHttpRequest()

  xhr.onload = () => {
    const reader = new FileReader()

    reader.onloadend = () => {
      callback(reader.result)
    }
    reader.readAsDataURL(xhr.response)
  }
  xhr.open('GET', url)
  xhr.responseType = 'blob'
  xhr.send()
}

export const getFileTypeByImageURL = imageURL => {
  const splitURL = imageURL.split('.')
  const lengthArr = splitURL.length

  return splitURL[lengthArr - 1]
}

export const removeElementsByClassName = className => {
  const elements = document.getElementsByClassName(className)

  for (let i = 0; i < elements.length; i += 1) {
    elements[i].parentNode.removeChild(elements[i])
  }
}
