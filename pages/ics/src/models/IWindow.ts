export default interface IWindow extends Window {
  unmountICS?: () => void
  addRFC?: (url: string) => string
}
