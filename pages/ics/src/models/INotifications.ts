import INotification from './INotification'

export default interface INotifications {
  width?: INotification
  height?: INotification
  maxWidth?: INotification
  maxHeight?: INotification
  fileSize?: INotification
  fileTypes: INotification
  cropBoxSize: INotification
  serverMessages?: string[]
}
