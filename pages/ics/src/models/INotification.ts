export default interface INotification {
  message: string
  show: boolean
}
