export default interface IInitParams {
  imageType: string
  imageUrl?: string
  parameters?: any
  callback?: any
}
