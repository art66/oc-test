export default interface IConstraints {
  allowedExtensions: string[]
  dimensions: {
    height?: number
    width?: number
    maxHeight?: number
    maxWidth?: number
  }
  maxSize: number
}
