export default interface IRestrictions {
  width?: number | string
  height?: number | string
  maxHeight?: number | string
  maxWidth?: number | string
  fileSize?: number | string
  fileTypes?: string
}
