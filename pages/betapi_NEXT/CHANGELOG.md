# BetApi - Torn


# 1.1.0
 * Fixed roots for react-bundles include.

# 1.0.0
 * Fixed broken update logic of GameBox section due to React v.16.8.6 update.
