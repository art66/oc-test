import React, { memo } from 'react'
import MatchTeamsNames from '../MatchTeamsNames'
import { TProps } from './interfaces'
import { getCompetitionString } from '../../selectors/matchName'

export default memo(function MatchName(props: TProps) {
  const { stage, tournament, gender, template, teams, teamsName, title } = props

  const competitionString = getCompetitionString({ stage, tournament, gender, template })

  return (
    <p className='t-overflow' title={title}>
      <MatchTeamsNames teams={teams} teamsName={teamsName} />
      {competitionString}
    </p>
  )
})
