import { TTeams } from '../../modules/interfaces'

export type TProps = {
  sport: string
  stage: string
  tournament: string
  gender: string
  template: string
  teams: TTeams
  teamsName: string
  title: string
}
