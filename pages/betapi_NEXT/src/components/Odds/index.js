import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Bet from '../Bet'
import { finishTimestamp, dateTimeFormat } from '../../utils'
import { MATCH_FINISHED_PURE_STATUS, TIME_FORMAT_NAME, MATCH_POSTPONED_PURE_STATUS } from '../../constants'
import styles from './index.cssmodule.scss'

class Odds extends Component {
  _renderOdds() {
    const {
      match,
      market,
      now,
      userData,
      betLoading,
      betOnGame,
      selectBet,
      setBetState,
      activeBet,
      betState,
      pageMode,
      params
    } = this.props

    const { activeGameBox } = params
    const activeGame = params.activeEventId

    if (!market.odds || market.odds.length < 1) {
      return null
    }

    return market.odds.map((odd, i) => (
      <Bet
        key={i}
        match={match}
        odd={odd}
        now={now}
        userData={userData}
        betLoading={betLoading}
        betOnGame={betOnGame}
        activeGameBox={activeGameBox}
        activeGame={activeGame}
        selectBet={selectBet}
        setBetState={setBetState}
        activeBet={activeBet}
        betState={betState}
        pageMode={pageMode}
      />
    ))
  }

  _renderMarketTime(betComponent, isSingleRow) {
    const { market, isFirst } = this.props

    return (
      <div
        className={cn(styles.marketName, isSingleRow && styles.isMarketNameSingleRow, 'bet market-name-cell', {
          first: isFirst
        })}
      >
        <p className={styles.marketTime}>
          <span className='bold'>{market.outcome}</span>
          {betComponent}
        </p>
      </div>
    )
  }

  _renderBet() {
    const { match, now } = this.props

    if (match.status !== MATCH_POSTPONED_PURE_STATUS && match.startTimestamp > now) {
      return (
        match.startTimestamp &&
        this._renderMarketTime(
          <span className={styles.time}>
            due to start at&nbsp;
            <span className={cn(styles.date, 'bold')}>
              {dateTimeFormat(match.startTimestamp)} {TIME_FORMAT_NAME}
            </span>
          </span>
        )
      )
    } else if (match.status === MATCH_FINISHED_PURE_STATUS) {
      if (!match.endedTimestamp) {
        return this._renderMarketTime(null, true)
      }

      return this._renderMarketTime(
        <span className={styles.time}>
          finished at&nbsp;
          <span className={cn(styles.date, 'bold')}>
            {finishTimestamp(match.endedTimestamp)} {TIME_FORMAT_NAME}
          </span>
        </span>
      )
    } else if (match.status === MATCH_POSTPONED_PURE_STATUS) {
      return this._renderMarketTime(null, true)
    } else if (match.startTimestamp) {
      return this._renderMarketTime(
        <span className={styles.time}>
          started at&nbsp;
          <span className={cn(styles.date, 'bold')}>
            {dateTimeFormat(match.startTimestamp)} {TIME_FORMAT_NAME}
          </span>
        </span>
      )
    }
  }

  render() {
    const { market } = this.props

    return market.odds.msg || market.odds.length === 0 ? (
      <ul className='bets-wrap clearfix'>
        <li className='title'>{market.odds.msg ? market.odds.msg : 'Odds for this market were removed'}</li>
      </ul>
    ) : (
      <ul className='bets-wrap clearfix'>
        <li className='bets'>{this._renderBet()}</li>
        {this._renderOdds()}
      </ul>
    )
  }
}

Odds.propTypes = {
  match: PropTypes.object,
  market: PropTypes.object,
  isFirst: PropTypes.bool,
  now: PropTypes.number,
  userData: PropTypes.object,
  betLoading: PropTypes.bool,
  betOnGame: PropTypes.func,
  selectBet: PropTypes.func,
  setBetState: PropTypes.func,
  activeBet: PropTypes.string,
  betState: PropTypes.string,
  pageMode: PropTypes.string,
  params: PropTypes.object
}

export default Odds
