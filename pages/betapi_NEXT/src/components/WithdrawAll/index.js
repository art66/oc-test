import React from 'react'
import PropTypes from 'prop-types'
import { numberFormat as nf } from '../../utils'

const WithdrawAll = props => {
  const { userData, activeGameBox, activeEventId, withdrawAll } = props

  if (userData.withdrawAmount > 0) {
    return (
      <div className='info-msg-cont green border-round m-top10 m-bottom10'>
        <div className='info-msg border-round'>
          <i className='info-icon' />
          <div className='delimiter'>
            <div className='msg right-round'>
              {userData.withdrawing ? (
                <img className='ajax-placeholder' src='/images/v2/main/ajax-loader.gif' />
              ) : (
                <span className='withdraw-all-msg'>
                  <span>
                    You have ${nf(userData.withdrawAmount)} of winnings in the bookie waiting to be cashed out
                  </span>
                  <span className='special withdraw-all'>
                    <button
                      className='torn-btn c-pointer'
                      role='button'
                      onClick={() => {
                        withdrawAll(activeGameBox, activeEventId)
                      }}
                    >
                      WITHDRAW
                    </button>
                  </span>
                </span>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
  return <div />
}

WithdrawAll.propTypes = {
  userData: PropTypes.object,
  activeGameBox: PropTypes.string,
  activeEventId: PropTypes.string,
  withdrawAll: PropTypes.func
}

export default WithdrawAll
