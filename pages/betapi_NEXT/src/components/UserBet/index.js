import React, { Component } from 'react'
import PropTypes from 'prop-types'

class UserBet extends Component {
  render() {
    const { bet, symbol, title, betState, withdraw } = this.props

    return (
      <li className={`stick ${betState} ${withdraw}`}>
        <div className='figure' />
        <span className='text' title={title}>
          {/* eslint-disable-next-line no-undef */}
          {symbol}${betState === 'won' ? toShortNumberFormat(bet.moneyGain) : toShortNumberFormat(bet.amount)}
        </span>
      </li>
    )
  }
}

UserBet.propTypes = {
  bet: PropTypes.object,
  symbol: PropTypes.string,
  title: PropTypes.string,
  betState: PropTypes.string,
  withdraw: PropTypes.string
}

export default UserBet
