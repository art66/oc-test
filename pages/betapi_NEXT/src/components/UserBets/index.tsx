import React, { memo, useCallback } from 'react'
import UserBet from '../UserBet'
import { groupBetsSelector, groupByResultSelector } from './selectors'
import { IProps } from './interfaces'

export default memo(function UserBets(props: IProps) {
  const { match } = props

  const results = groupByResultSelector(match.bets)
  const groupBets = groupBetsSelector(results)

  const getTitle = useCallback(titles => titles.join('<br>'), [])

  return (
    <ul className='stick-wrap right'>
      {groupBets.map((bet, index) => {
        return (
          <UserBet
            bet={bet}
            key={index}
            symbol={bet.symbol}
            title={getTitle(bet.titles)}
            betState={bet.betState}
            withdraw={bet.withdraw}
          />
        )
      })}
    </ul>
  )
})
