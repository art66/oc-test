import { createSelector } from 'reselect'
import { TUserBets } from '../../modules/interfaces'
import { TYPES } from './constants'
import { betTitle } from '../../utils'

export const groupByResultSelector = createSelector<TUserBets, TUserBets, any[]>(
  bets => bets,
  bets => {
    const response = []

    TYPES.map(() => response.push([]))

    bets.map(bet => response[TYPES.indexOf(bet.result)].push(bet))

    return response
  }
)

export const groupBetsSelector = createSelector<any, any, any>(
  results => results,
  results => {
    const groupBets = []

    for (const i in results) {
      const result = results[i]

      if (result.length === 0) {
        continue
      }

      const item = {
        moneyGain: 0,
        amount: 0,
        titles: [],
        symbol: null,
        betState: null,
        withdraw: null
      }

      for (const j in result) {
        const bet = result[j]
        const { symbol, title, betState, withdraw } = betTitle(bet)

        item.symbol = symbol
        item.betState = betState
        item.withdraw = withdraw
        item.titles.push(title)
        item.amount += bet.amount
        item.moneyGain += bet.moneyGain
      }

      groupBets.push(item)
    }

    return groupBets
  }
)
