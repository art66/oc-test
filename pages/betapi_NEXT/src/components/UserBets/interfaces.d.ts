import { IMatch } from '../../modules/interfaces'

export interface IProps {
  match: IMatch
}
