import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import MatchName from '../../../MatchName'
import { dayDateFormat, shortMoneyFormat } from '../../../../utils'
import { getMatchName } from '../../../../selectors/matchName'
import styles from './index.cssmodule.scss'

class GameStats extends Component {
  getAmountOfBets(bets) {
    if (!bets) {
      return 0
    }
    let amount = 0
    const types = {}

    for (const i in bets) {
      if (!types[bets[i].state]) {
        amount++
        types[bets[i].state] = true
      }
    }
    return amount
  }

  render() {
    const { match, icon } = this.props

    const bets = [
      {
        state: 'won'
      },
      {
        state: 'lost'
      }
    ]

    if (match.refunded > 0) {
      bets.splice(1, 0, {
        state: 'refunded'
      })
    }

    const { status, sport, stage, tournament, gender, template, ep: teams, name: teamsName } = match
    const matchName = getMatchName({ status, sport, stage, tournament, gender, template, teams, teamsName })

    return (
      <li className='disabled'>
        <ul className={cn(styles.match, 'pop-game clearfix t-gray-9')}>
          <li className='game' title={sport}>
            <i className={`gm-${icon.split('-').join('')}-icon`} />
          </li>
          <li className={cn(styles.matchInfo, 'team t-overflow match')}>
            <div className={cn(styles.matchName, `name t-overflow bets-num-${this.getAmountOfBets(bets)}`)}>
              <MatchName
                sport={sport}
                stage={stage}
                tournament={tournament}
                gender={gender}
                template={template}
                teams={teams}
                teamsName={teamsName}
                title={matchName}
              />
            </div>
            <ul className='stick-wrap'>
              {bets.map((bet, index) => (
                <li key={index} className={`stick ${bet.state}`}>
                  <div className='figure' />
                  <span className='text'>${shortMoneyFormat(match[bet.state] || 0)}</span>
                </li>
              ))}
            </ul>
          </li>
          <li className='state-wrap'>{dayDateFormat(match.endedTimestamp)}</li>
        </ul>
      </li>
    )
  }
}

GameStats.propTypes = {
  match: PropTypes.object,
  alias: PropTypes.string,
  now: PropTypes.number,
  withdrawMatchBet: PropTypes.string,
  icon: PropTypes.string
}

export default GameStats
