import React, { Component } from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'
import cn from 'classnames'
import styles from '../../../Sports/index.cssmodule.scss'
import { $, isManualDesktopMode } from '../../../../utils'

const DESKTOP_SLIDER_WIDTH = '100%'
let mobileSliderWidth

class SportStats extends Component {
  componentDidUpdate() {
    this.initGamesSlider()
  }

  componentDidMount() {
    this.initGamesSlider()
  }

  _getIconName(sport) {
    const icon = sport.icon || sport.alias

    return icon.split('-').join('')
  }

  getIcon(sport) {
    return <i className={'gm-' + this._getIconName(sport) + '-icon'} />
  }

  initGamesSlider() {
    const $gamesSlider = $(this.refs.gamesSlider)

    if (this.props.pageMode === 'desktop' || isManualDesktopMode()) {
      $gamesSlider.find('.slides').width(DESKTOP_SLIDER_WIDTH)
      return
    }

    if (mobileSliderWidth !== undefined) {
      $gamesSlider.find('.slides').width(mobileSliderWidth)
    }

    $gamesSlider.flexslider({
      selector: '.slides > li',
      animation: 'slide',
      animationLoop: false,
      itemWidth: 38,
      controlNav: false,
      directionNav: true,
      slideshow: false
    })

    mobileSliderWidth = $gamesSlider.find('.slides')[0].style.width
  }

  render() {
    const { gameBoxesList, children, activeGameBox, gameBoxLoading } = this.props

    const activeSport = gameBoxesList.filter(item => item.alias === activeGameBox)[0]

    return (
      <div className='bookie-popular-wrap m-top10'>
        <div className='title-black top-round'>{activeSport.name} Matches</div>
        <div className='t-blue-cont h cont-gray bottom-round'>
          <div className='games-tabs-wrap'>
            <div className='game-slider white-grad bookie-sports-tabs-wrap' ref='gamesSlider'>
              <ul className='bookie-games-tabs white-grad-tabs slides'>
                {gameBoxesList.slice(2).map(sport => (
                  <li
                    className={cn(styles.sport, sport.alias, this._getIconName(sport), {
                      'ui-tabs-active': sport.alias === activeGameBox,
                      disabled: sport.matchAmount <= 0
                    })}
                    title={sport.title}
                    key={sport.alias}
                    style={{ width: 38, float: 'left', marginRight: 0, display: 'block' }}
                  >
                    {sport.matchAmount > 0 ? (
                      <Link to={`/stats/${sport.alias}`}>{this.getIcon(sport)}</Link>
                    ) : (
                      <a>{this.getIcon(sport)}</a>
                    )}
                  </li>
                ))}
              </ul>
            </div>

            <div className='bookie-games-boxes'>
              {gameBoxLoading ? (
                <img className='ajax-placeholder m-top15 m-bottom10' src='/images/v2/main/ajax-loader.gif' />
              ) : (
                children
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

SportStats.propTypes = {
  gameBoxesList: PropTypes.array,
  children: PropTypes.element,
  activeGameBox: PropTypes.string,
  gameBoxLoading: PropTypes.bool,
  pageMode: PropTypes.string
}

export default SportStats
