import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import checkMediaType from '@torn/shared/utils/checkMediaType'
import Header from '../Header'
import TopStats from './components/TopStats'
import SportStats from './components/SportStats'
import { loadStatsData } from '../../actions'
import './styles/statistics.css'

class Stats extends Component {
  componentDidMount() {
    this.props.loadStatsData(this.props.params.activeGameBox)
  }

  render() {
    const { params, children, statsData, gameBoxesList, pageMode, isDesktop } = this.props

    return (
      <div>
        <Header isStats />
        {statsData ? (
          <div className='stats-main-wrap'>
            <TopStats topStats={statsData.topStats} isDesktop={isDesktop} />
            <hr className='page-head-delimiter m-top10' />
            <SportStats
              children={children}
              gameBoxesList={gameBoxesList}
              activeGameBox={params.activeGameBox}
              pageMode={pageMode}
            />
          </div>
        ) : (
          <img className='ajax-placeholder m-top10 m-bottom10' src='/images/v2/main/ajax-loader.gif' />
        )}
      </div>
    )
  }
}

Stats.propTypes = {
  params: PropTypes.object,
  children: PropTypes.element,
  statsData: PropTypes.object,
  gameBoxesList: PropTypes.array,
  loadStatsData: PropTypes.func,
  pageMode: PropTypes.string,
  isDesktop: PropTypes.bool
}

Stats.defaultProps = {
  isDesktop: null
}

const mapStateToProps = state => {
  const { mediaType } = state.browser
  const mediaTypeState = checkMediaType(mediaType, state.app.isDesktopLayout)

  return {
    statsData: state.app.statsData,
    gameBoxesList: state.app.gameBoxesList,
    pageMode: mediaType,
    isDesktop: mediaTypeState.isDesktop
  }
}

const mapActionsToProps = {
  loadStatsData
}

export default connect(mapStateToProps, mapActionsToProps)(Stats)
