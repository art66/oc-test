export type TBet = {
  eventId: string
  outcomeId: string
}

export type TBets = TBet[]

export type TOdd = { eventId: string; outcomeId: string }

export type TOutcomeType = {
  odds: TOdd[]
}

export type TOutcomeTypes = TOutcomeType[]

export interface IExtraOutcomeTypesWithBetsSelectorProps {
  withoutBets?: boolean
  bets: TBets
  outcomeTypes: TOutcomeTypes
}

export interface IOutcomeTypesSelectorProps {
  isExtraOutcomeTypes: boolean
  bets: TBets
  outcomeTypes: TOutcomeTypes
  showExtraOutcomes: boolean
}

export interface IMatchSelectorProps {
  gameBoxesList: {
    alias: string
    matches: { ID: string }[]
  }[]
  activeGameBox: string
  activeEventId: string
}
