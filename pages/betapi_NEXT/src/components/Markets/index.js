import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'
import Odds from '../Odds'
import MatchTeamsNames from '../MatchTeamsNames'
import { selectBet, setBetState, betOnGame, loadGameData } from '../../actions'
import { MATCH_NOTSTARTED_PURE_STATUS, SCROLL_OFFSET } from '../../constants'
import { isInViewport } from '../../utils'
import styles from './index.cssmodule.scss'
import { outcomeTypesSelector, areExtraOutcomeTypesWithoutBetsSelector, matchSelector } from './selectors'

class Markets extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showExtraOutcomes: false
    }

    this.marketRef = React.createRef()
  }

  componentDidMount() {
    this.loadGameData()
  }

  componentDidUpdate(prevProps) {
    // scroll to the active match
    if (this.marketRef.current !== null) {
      const { top } = this.marketRef.current.getBoundingClientRect()

      if (!isInViewport(top, -SCROLL_OFFSET)) {
        window.scrollTo({
          top: window.scrollY + top - SCROLL_OFFSET
        })
      }
    }

    if (prevProps.params.activeEventId === this.props.params.activeEventId) {
      return false
    }

    this.loadGameData()
  }

  onShowExtralLinkClick = () => {
    const { showExtraOutcomes } = this.state

    this.setState({ showExtraOutcomes: !showExtraOutcomes })
  }

  loadGameData() {
    this.props.loadGameData(this.props.params.activeGameBox, this.props.params.activeEventId)
  }

  _getTitle() {
    const { match } = this.props

    if (match.outcomeTypes.msg) {
      return match.outcomeTypes.msg
    }

    if (match.status === MATCH_NOTSTARTED_PURE_STATUS && match.bets?.length === 0) {
      return 'There are no bets currently available for this listing'
    }

    return 'Markets for this match were removed'
  }

  _renderOdd(market, isFirst = false) {
    const {
      match,
      now,
      userData,
      betLoading,
      betOnGame,
      selectBet,
      setBetState,
      activeBet,
      betState,
      pageMode,
      params,
      loadGameData
    } = this.props

    return (
      <Odds
        isFirst={isFirst}
        key={`${market.eventId}${market.outcome}`}
        market={market}
        match={match}
        now={now}
        userData={userData}
        betLoading={betLoading}
        betOnGame={betOnGame}
        selectBet={selectBet}
        setBetState={setBetState}
        loadGameData={loadGameData}
        activeBet={activeBet}
        betState={betState}
        pageMode={pageMode}
        params={params}
      />
    )
  }

  _renderMainOdds() {
    const { match } = this.props
    const { showExtraOutcomes } = this.state

    const mainOdds = outcomeTypesSelector({
      isExtraOutcomeTypes: false,
      bets: match.bets,
      outcomeTypes: match.outcomeTypes,
      showExtraOutcomes
    })

    return mainOdds.map(market => this._renderOdd(market))
  }

  _renderExtraOddsActivator() {
    const { match } = this.props
    const { showExtraOutcomes } = this.state

    const areExtraOutcomeTypesWithoutBets = areExtraOutcomeTypesWithoutBetsSelector({
      bets: match.bets,
      outcomeTypes: match.outcomeTypes
    })

    if (!areExtraOutcomeTypesWithoutBets) {
      return null
    }

    const extraOutcomesLength = match.outcomeTypes.length - 1
    const toggleText = showExtraOutcomes ? 'Hide' : 'Show'
    const optionsText = extraOutcomesLength > 1 ? 'options' : 'option'

    return (
      <li
        className={cn(styles.extraOddsActivator, { [styles.active]: showExtraOutcomes })}
        onClick={this.onShowExtralLinkClick}
      >
        <a>
          {toggleText} {extraOutcomesLength} additional betting {optionsText}
        </a>
      </li>
    )
  }

  _renderExtraOdds() {
    const { match } = this.props
    const { showExtraOutcomes } = this.state

    const outcomeTypes = outcomeTypesSelector({
      isExtraOutcomeTypes: true,
      bets: match.bets,
      outcomeTypes: match.outcomeTypes,
      showExtraOutcomes
    })

    if (!showExtraOutcomes) {
      return null
    }

    return outcomeTypes.map(market => this._renderOdd(market))
  }

  render() {
    const { match } = this.props

    if (match.outcomeTypes === undefined) {
      return (
        <img
          ref={this.marketRef}
          className='ajax-placeholder m-top15 m-bottom10'
          src='/images/v2/main/ajax-loader.gif'
        />
      )
    }

    if (match.outcomeTypes.msg || match.outcomeTypes.length === 0) {
      return (
        <ul className='bets-wrap clearfix'>
          <li className='title'>
            {this._getTitle()}
            {/* FIXME: Do we need to replace title from Markets to Outcome types */}
          </li>
        </ul>
      )
    }

    return (
      <ul className='bets-wrap clearfix'>
        <li className={cn(styles.title, 'title')}>
          <MatchTeamsNames teams={match.ep} teamsName={match.name} />
        </li>
        {this._renderMainOdds()}
        {this._renderExtraOddsActivator()}
        {this._renderExtraOdds()}
      </ul>
    )
  }
}

Markets.propTypes = {
  match: PropTypes.object,
  now: PropTypes.number,
  userData: PropTypes.object,
  betLoading: PropTypes.bool,
  betOnGame: PropTypes.func,
  selectBet: PropTypes.func,
  setBetState: PropTypes.func,
  loadGameData: PropTypes.func,
  activeBet: PropTypes.string,
  betState: PropTypes.string,
  pageMode: PropTypes.string,
  params: PropTypes.object
}

const mapStateToProps = (state, props) => {
  return {
    // match: state.app.gameBoxesList
    //   .find(item => item.alias === props.params.activeGameBox)
    //   .matches.find(match => match.ID === props.params.activeEventId),
    match: matchSelector({
      gameBoxesList: state.app.gameBoxesList,
      activeGameBox: props.params.activeGameBox,
      activeEventId: props.params.activeEventId
    }),
    now: state.app.now,
    userData: state.app.userData,
    betLoading: state.app.betLoading,
    activeBet: state.app.activeBet,
    betState: state.app.betState,
    pageMode: state.app.pageMode
  }
}

const mapActionsToProps = {
  selectBet,
  setBetState,
  betOnGame,
  loadGameData
}

export default connect(mapStateToProps, mapActionsToProps)(Markets)
