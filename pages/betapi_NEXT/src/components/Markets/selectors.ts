import { createSelector } from 'reselect'
import {
  IExtraOutcomeTypesWithBetsSelectorProps,
  IMatchSelectorProps,
  IOutcomeTypesSelectorProps,
  TOutcomeTypes
} from './interfaces'

export function extraOutcomeTypesSelector(outcomeTypes: TOutcomeTypes) {
  return outcomeTypes.slice(1)
}

export const matchSelector = createSelector(
  (props: IMatchSelectorProps) => props,
  ({ gameBoxesList, activeGameBox, activeEventId }) => {
    return gameBoxesList.find(item => item.alias === activeGameBox).matches.find(match => match.ID === activeEventId)
  }
)

export const extraOutcomeTypesWithBetsSelector = createSelector(
  (props: IExtraOutcomeTypesWithBetsSelectorProps) => props,
  ({ withoutBets = false, bets, outcomeTypes }) => {
    const extraOutcomeTypes = extraOutcomeTypesSelector(outcomeTypes)

    let extraOutcomeTypesWithBets = []

    extraOutcomeTypes.forEach(outcomeType => {
      const oddWithBets = outcomeType.odds.find(odd => {
        return bets.find(bet => {
          return bet.eventId === odd.eventId && bet.outcomeId === odd.outcomeId
        })
      })

      if (!oddWithBets === withoutBets) {
        extraOutcomeTypesWithBets.push(outcomeType)
      }
    })

    return extraOutcomeTypesWithBets
  }
)

export const areExtraOutcomeTypesWithoutBetsSelector = createSelector(
  (props: IOutcomeTypesSelectorProps) => props,
  ({ bets, outcomeTypes }) => {
    const extraOutcomeTypesWithoutBets = extraOutcomeTypesWithBetsSelector({ withoutBets: true, bets, outcomeTypes })

    return extraOutcomeTypesWithoutBets?.length > 0
  }
)

export const outcomeTypesSelector = createSelector(
  (props: IOutcomeTypesSelectorProps) => props,
  ({ isExtraOutcomeTypes, bets, outcomeTypes, showExtraOutcomes }) => {
    const firstOutcomeType = outcomeTypes[0]

    if (isExtraOutcomeTypes) {
      return extraOutcomeTypesSelector(outcomeTypes)
    }

    const outcomeTypesWithFirstOutcomeType = [firstOutcomeType]

    if (showExtraOutcomes) {
      return outcomeTypesWithFirstOutcomeType
    }

    const extraOutcomeTypesWithBets = extraOutcomeTypesWithBetsSelector({ bets, outcomeTypes })

    return [...outcomeTypesWithFirstOutcomeType, ...extraOutcomeTypesWithBets]
  }
)
