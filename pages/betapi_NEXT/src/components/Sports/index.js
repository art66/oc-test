import React, { Component } from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { WithdrawAll } from '..'
import { $, getCurrentTimestamp, isManualDesktopMode } from '../../utils'
import { SECOND } from '../../constants'
import styles from './index.cssmodule.scss'

const DESKTOP_SLIDER_WIDTH = '100%'
let mobileSliderWidth

class Sports extends Component {
  _getIconName(sport) {
    const icon = sport.icon || sport.alias

    return icon.split('-').join('')
  }

  initGamesSlider() {
    const $gamesSlider = $(this.refs.gamesSlider)

    if (this.props.pageMode === 'desktop' || isManualDesktopMode()) {
      $gamesSlider.find('.slides').width(DESKTOP_SLIDER_WIDTH)
      return
    }

    if (mobileSliderWidth !== undefined) {
      $gamesSlider.find('.slides').width(mobileSliderWidth)
    }

    $gamesSlider.flexslider({
      selector: '.slides > li',
      animation: 'slide',
      animationLoop: false,
      itemWidth: 38,
      controlNav: false,
      directionNav: true,
      slideshow: false
    })

    mobileSliderWidth = $gamesSlider.find('.slides')[0].style.width
  }

  componentDidUpdate() {
    this.initGamesSlider()
  }

  componentDidMount() {
    this.initGamesSlider()

    setInterval(() => {
      const now = Math.floor(getCurrentTimestamp() / SECOND)

      if (now % 10 === 0) {
        // const $userMoney = $('#user-money')

        // const money = $userMoney.length > 0 ? parseInt($userMoney.attr('data-money')) : Number.NaN

        this.props.updateNow(now * SECOND/* , money */)
      }
    }, 1000)
  }

  getIcon(sport) {
    return <i className={cn(styles.icon, `gm-${this._getIconName(sport)}-icon`)} />
  }

  render() {
    const { gameBoxesList, userData, children, activeGameBox, activeEventId, gameBoxLoading, withdrawAll } = this.props

    const activeSport = gameBoxesList.filter(item => item.alias === activeGameBox)[0]

    return (
      <div className='bookie-popular-wrap m-top10'>
        <WithdrawAll
          userData={userData}
          activeGameBox={activeGameBox}
          activeEventId={activeEventId}
          withdrawAll={withdrawAll}
        />
        <div className='title-black top-round'>
          {activeSport.matchName ? activeSport.matchName : `${activeSport.name} Matches`}
        </div>
        <div className='t-blue-cont h cont-gray bottom-round'>
          <div className='games-tabs-wrap'>
            <div className='game-slider white-grad bookie-sports-tabs-wrap' ref='gamesSlider'>
              <ul className='bookie-games-tabs white-grad-tabs slides'>
                {gameBoxesList.map(sport => {
                  return (
                    <li
                      className={cn(styles.sport, sport.alias, this._getIconName(sport), {
                        'ui-tabs-active': sport.alias === activeGameBox,
                        disabled: sport.matchAmount <= 0
                      })}
                      title={sport.title}
                      key={sport.alias}
                      style={{
                        width: 37,
                        float: 'left'
                      }}
                    >
                      {sport.matchAmount > 0 ? (
                        <Link to={sport.alias}>{this.getIcon(sport)}</Link>
                      ) : (
                        <a>{this.getIcon(sport)}</a>
                      )}
                    </li>
                  )
                })}
              </ul>
            </div>
            <div className='bookie-games-boxes'>
              {gameBoxLoading ? (
                <img className='ajax-placeholder m-top15 m-bottom10' src='/images/v2/main/ajax-loader.gif' />
              ) : (
                children
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Sports.propTypes = {
  gameBoxesList: PropTypes.array,
  userData: PropTypes.object,
  pageMode: PropTypes.string,
  children: PropTypes.element,
  activeGameBox: PropTypes.string,
  activeEventId: PropTypes.string,
  gameBoxLoading: PropTypes.bool,
  updateNow: PropTypes.func,
  withdrawAll: PropTypes.func
}

export default Sports
