import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Dropdown from '@torn/shared/components/Dropdown'
import { MIN_SEARCH_LENGTH } from '../../constants'
import styles from './index.cssmodule.scss'

const ALL = 'All'
const defaultCompetition = {
  id: '',
  name: ALL
}

class SearchBox extends Component {
  _getCompetitionName = () => {
    const { competitions, competitionBox } = this.props

    const selectedCompetition = competitions.find(item => item.id === competitionBox)
    const selectedCompetitionName = selectedCompetition ? selectedCompetition.competition : ''

    return selectedCompetitionName
  }

  onChange(event) {
    if (event.target.value.length >= MIN_SEARCH_LENGTH) {
      this.props.setSearch(event.target.value)
    } else {
      this.props.setSearch('')
    }
  }

  setSearchField(value) {
    this.refs.search.value = value
    this.props.setSearch(value)
  }

  render() {
    const { competitions, competitionBox, setCompetitionBox } = this.props

    return (
      <div className='search-box'>
        <input
          className='search-input m-bottom5'
          name='search'
          ref='search'
          autoComplete='off'
          onChange={event => this.onChange(event)}
        />
        {competitions.map(item => (
          <div
            className={cn(styles.searchLabel, { [styles.selected]: competitionBox === item.id })}
            key={item.id}
            onClick={() => setCompetitionBox(item.id === competitionBox ? '' : item.id)}
          >
            {item.competition}
          </div>
        ))}
        <Dropdown
          placeholder='Select competition'
          list={[defaultCompetition].concat(
            competitions.map(item => ({
              id: item.id,
              name: item.competition
            }))
          )}
          selected={competitionBox ? { id: competitionBox, name: this._getCompetitionName() } : undefined}
          onChange={value => setCompetitionBox(value.id)}
        />
      </div>
    )
  }
}

SearchBox.propTypes = {
  competitions: PropTypes.array,
  competitionBox: PropTypes.string,
  setCompetitionBox: PropTypes.func,
  setSearch: PropTypes.func
}

export default SearchBox
