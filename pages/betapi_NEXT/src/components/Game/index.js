import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import cn from 'classnames'
import UserBets from '../UserBets'
import MatchName from '../MatchName'
// import { YOUR_BETS_ALIAS } from '../../constants'
import { getCountdown, dateTimeFormat, finishTimestamp } from '../../utils'
import { isInFutureMathes } from '../../selectors/matches'
import { getMatchName } from '../../selectors/matchName'
import styles from './index.cssmodule.scss'
import {
  MATCH_CANCELLED_PURE_STATUS,
  MATCH_POSTPONED_PURE_STATUS,
  MATCH_FINISHED_PURE_STATUS,
  MATCH_INTERRUPTED_PURE_STATUS,
  MATCH_UNKNOWN_PURE_STATUS,
  MATCH_DELETED_PURE_STATUS
} from '../../constants'

class Game extends Component {
  getBet(bets, result = 'won') {
    let wonBet = false

    for (const i in bets) {
      if (bets[i].result === result) {
        wonBet = bets[i]
        break
      }
    }
    return wonBet
  }

  getAmountOfBets(bets) {
    if (!bets) {
      return 0
    }
    let amount = 0
    const types = {}

    for (const i in bets) {
      if (!types[bets[i].result]) {
        amount++
        types[bets[i].result] = true
      }
    }
    return amount
  }

  _getFinishedTitle() {
    const { match } = this.props

    if (match.endedTimestamp) {
      return `Finished at ${finishTimestamp(match.endedTimestamp)}`
    }

    return 'Finished'
  }

  _renderWithdraw() {
    return (
      <span className='special' style={{ marginTop: '3px' }}>
        <button
          className='windraw-icon-button'
          type='button'
          onClick={e => {
            e.preventDefault()
            this.props.withdrawBet(this.props.match.ID, this.props.activeGameBox)
          }}
        >
          <i className='state gm-withdraw-icon' />
        </button>
      </span>
    )
  }

  getStateIconClassName(status, isRefunded) {
    if (status === MATCH_FINISHED_PURE_STATUS) {
      return isRefunded ? 'lost' : 'won'
    } else if (
      status === MATCH_POSTPONED_PURE_STATUS ||
      status === MATCH_CANCELLED_PURE_STATUS ||
      status === MATCH_DELETED_PURE_STATUS ||
      status === MATCH_INTERRUPTED_PURE_STATUS
    ) {
      return 'cancelled'
    }

    return 'waiting'
  }

  getStateIconTitle(status, status_desc) {
    if (status === MATCH_CANCELLED_PURE_STATUS) {
      return 'Match is cancelled'
    } else if (status === MATCH_POSTPONED_PURE_STATUS) {
      return 'Match is postponed'
    } else if (status === MATCH_FINISHED_PURE_STATUS) {
      return this._getFinishedTitle()
    } else if (
      status === MATCH_UNKNOWN_PURE_STATUS ||
      status === MATCH_DELETED_PURE_STATUS ||
      status === MATCH_INTERRUPTED_PURE_STATUS
    ) {
      return status_desc
    }

    return 'Match is in progress'
  }

  getMatchState(match) {
    const { now } = this.props
    const wonBet = this.getBet(match.bets, 'won')
    const refundedBet = this.getBet(match.bets, 'refunded')

    if ((wonBet && wonBet.withdrawn === 0) || (refundedBet && refundedBet.withdrawn === 0)) {
      return this._renderWithdraw()
    }

    if (match.status !== MATCH_POSTPONED_PURE_STATUS && match.startTimestamp > now) {
      return (
        <span className='state countdown' title={`Due to start at ${dateTimeFormat(match.startTimestamp)}`}>
          {getCountdown(match.startTimestamp, now)}
        </span>
      )
    }

    const stateIconClassName = this.getStateIconClassName(match.status, match.isRefunded)
    const stateIconTitle = this.getStateIconTitle(match.status, match.status_desc)

    return <i className={cn('state gm-state-icon', stateIconClassName)} title={stateIconTitle} />
  }

  render() {
    const { match, alias, activeGame, withdrawMatchBet, children, icon } = this.props
    const { ID, status, sport, stage, tournament, gender, template, ep: teams, name: teamsName } = match
    const matchName = getMatchName({ status, sport, stage, tournament, gender, template, teams, teamsName })

    const isActive = isInFutureMathes(match.status) || ID === activeGame

    return (
      <li className={isActive ? `c-pointer${ID === activeGame ? ' active' : ''}` : 'disabled'}>
        {withdrawMatchBet === ID ? (
          <img
            className='ajax-placeholder m-top10 m-bottom10'
            src='/images/v2/main/ajax-loader.gif'
            style={{ paddingTop: 2 }}
          />
        ) : (
          <Link to={`${alias}/${activeGame === ID ? '' : ID}`}>
            <ul className={cn(styles.match, { [styles.isNotActive]: !isActive }, 'pop-game clearfix')}>
              <li className='game' title={sport}>
                <i className={`gm-${icon.split('-').join('')}-icon`} />
              </li>
              <li className={cn(styles.matchName, 'team t-overflow team-names matchName')}>
                <div className={`name t-overflow bets-num-${this.getAmountOfBets(match.bets)}`}>
                  <MatchName
                    sport={sport}
                    stage={stage}
                    tournament={tournament}
                    gender={gender}
                    template={template}
                    teams={teams}
                    teamsName={teamsName}
                    title={matchName}
                  />
                </div>
                {!match.bets || <UserBets match={match} />}
              </li>
              <li className='state-wrap'>{this.getMatchState(match)}</li>
            </ul>
          </Link>
        )}
        <div className='info-wrap' style={{ display: ID === activeGame ? 'block' : 'none' }}>
          {(() => {
            if (ID === activeGame) {
              return children
            }
          })()}
        </div>
      </li>
    )
  }
}

Game.propTypes = {
  match: PropTypes.object,
  alias: PropTypes.string,
  now: PropTypes.number,
  activeGame: PropTypes.string,
  activeGameBox: PropTypes.string,
  withdrawBet: PropTypes.func,
  withdrawMatchBet: PropTypes.string,
  children: PropTypes.element,
  icon: PropTypes.string
}

export default Game
