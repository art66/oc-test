import { TTeams } from '../../modules/interfaces'

export type TProps = {
  teams: TTeams
  teamsName: string
}
