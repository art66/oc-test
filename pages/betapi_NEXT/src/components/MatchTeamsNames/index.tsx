import React, { memo, Fragment, useMemo } from 'react'
import { TProps } from './interfaces'

export default memo(function MatchTeamsNames(props: TProps) {
  const { teams, teamsName } = props

  const teamNames = useMemo(() => {
    if (teams && teams.length) {
      return teams.map((team, index) => {
        if (index === 0) {
          return <b key={team.name}>{team.name}</b>
        }

        return (
          <Fragment key={team.name}>
            {' '}
            v <b>{team.name}</b>
          </Fragment>
        )
      })
    }

    return <b>{teamsName}</b>
  }, [teams, teamsName])

  return <span>{teamNames}</span>
})
