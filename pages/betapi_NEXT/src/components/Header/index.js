import React from 'react'
import PropTypes from 'prop-types'
import AppHeader from '@torn/shared/components/AppHeader'
import { numberFormat } from '../../utils'
import { MAX_BET_LIMIT } from '../../constants'
import { DEFAULT, FULL, BACK } from './constants'

class Header extends React.Component {
  _getAppHeaderProps = () => {
    const { isStats } = this.props

    const clientProps = {
      ...DEFAULT,
      ...(isStats ? BACK : FULL)
    }

    return clientProps
  }

  render() {
    const clientProps = this._getAppHeaderProps()

    return (
      <div>
        <AppHeader appID='attack' pageID='0' clientProps={clientProps} />
        <div>
          <div className='info-msg-cont blue border-round m-top10 m-bottom10'>
            <div className='info-msg border-round'>
              <i className='info-icon' />
              <div className='delimiter'>
                <div className='msg right-round'>
                  We{'\''}re happy to announce that the temporary betting limit
                  has now been increased to ${numberFormat(MAX_BET_LIMIT)} per match.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Header.propTypes = {
  isStats: PropTypes.bool
}

export default Header
