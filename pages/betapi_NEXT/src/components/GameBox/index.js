import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Game, SearchBox } from '..'
import {
  loadGameBoxData,
  loadMoreGameBoxData,
  withdrawBet,
  setCompetitionBox,
  setSearch,
  showMoreLiveMatches
} from '../../actions'
import { MIN_MATCHE_NUMBER } from '../../constants'
import { matchesSelector, selectMatchesSelector } from './selectors'

class GameBox extends Component {
  scrollListener(e) {
    const { params } = this.props

    if (
      // params.activeGameBox !== POPULAR_ALIAS &&
      document.documentElement.scrollHeight === window.innerHeight + Math.round(window.scrollY) &&
      !this.props.moreGamesLoading &&
      this.props.sport.loadedMatchesAmount > 0
    ) {
      const { currentPage, search, competitionBox } = this.props

      this.props.loadMoreGameBoxData(params.activeGameBox, currentPage, search, competitionBox)
    }
  }

  componentDidMount() {
    const { sport, route } = this.props
    const isYourBets = sport.alias === 'your-bets'
    const noMatch = sport.matchAmount === 0

    if (isYourBets && noMatch) {
      route.history.push('/popular')
    }

    this.scrollListener = this.scrollListener.bind(this)
    window.addEventListener('scroll', this.scrollListener, false)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollListener, false)
  }

  componentDidUpdate(prevProps) {
    const {
      sport,
      params,
      moreGamesLoading,
      currentSearchPage,
      search,
      competitionBox,
      loadedMatchesAmountBySearch,
      loadMoreGameBoxData
    } = this.props

    if (prevProps.params.activeGameBox !== params.activeGameBox) {
      this.loadGameBoxData()
    }

    if (!sport.matches) {
      return
    }

    const sportMaches = selectMatchesSelector({
      matches: sport.matches,
      competitionBox,
      search,
      activeGameBox: params.activeGameBox
    })
    const loadedMatchesAmount = loadedMatchesAmountBySearch[`${competitionBox}_${search}`]

    if (
      sportMaches.length < MIN_MATCHE_NUMBER &&
      !moreGamesLoading &&
      (loadedMatchesAmount === undefined || loadedMatchesAmount > 0)
      // && params.activeGameBox !== POPULAR_ALIAS
    ) {
      loadMoreGameBoxData(params.activeGameBox, currentSearchPage, search, competitionBox)
    }
  }

  loadGameBoxData() {
    this.props.loadGameBoxData(this.props.params.activeGameBox)
  }

  render() {
    const {
      sport,
      now,
      moreGamesLoading,
      competitionBox,
      search,
      chosenShowMore,
      params,
      withdrawBet,
      withdrawMatchBet,
      setCompetitionBox,
      setSearch,
      showMoreLiveMatches,
      children
    } = this.props

    const matches = matchesSelector({
      matchName: sport.matchName,
      matches: sport.matches,
      alias: sport.alias,
      competitionBox,
      search,
      activeGameBox: params.activeGameBox,
      chosenShowMore,
      moreGamesLoading,
      activeEventId: params.activeEventId,
      showMoreLiveMatches
    })

    if (!sport.matches) {
      return <img className='ajax-placeholder m-top15 m-bottom10' src='/images/v2/main/ajax-loader.gif' />
    }

    return (
      <div id={sport.alias}>
        <div className='date-tabs-wrap'>
          <div className='bookie-date-boxes'>
            <SearchBox
              competitions={sport.competitions}
              competitionBox={competitionBox}
              setCompetitionBox={setCompetitionBox}
              setSearch={setSearch}
            />
            <ul className='pop-list'>
              {matches.length > 0 ? (
                matches.map(match =>
                  match.loadingRow ? (
                    <li key={-1}>
                      <img
                        className='ajax-placeholder m-top10 m-bottom10'
                        src='/images/v2/main/ajax-loader.gif'
                        style={{ paddingTop: 2 }}
                      />
                    </li>
                  ) : match.title ? (
                    <li key={match.key} className={match.className} onClick={match.onClick}>
                      <ul className='pop-game clearfix text-a-center'>
                        <li className='team t-overflow' style={{ width: '100%', padding: '0 0 0 0' }}>
                          <span>{match.title}</span>
                        </li>
                      </ul>
                    </li>
                  ) : (
                    <Game
                      key={match.ID}
                      match={match}
                      alias={sport.alias}
                      activeGameBox={params.activeGameBox}
                      now={now}
                      activeGame={params.activeEventId}
                      withdrawBet={withdrawBet}
                      withdrawMatchBet={withdrawMatchBet}
                      children={children}
                      icon={match.icon}
                    />
                  )
                )
              ) : (
                <li>
                  <ul className='pop-game clearfix text-a-center'>
                    <li className='team t-overflow' style={{ width: '100%', padding: '0 0 0 0' }}>
                      {moreGamesLoading ? (
                        <img src='/images/v2/main/ajax-loader.gif' />
                      ) : params.activeGameBox === 'your-bets' && !search && !competitionBox ? (
                        <span>You haven't placed any bets yet</span>
                      ) : (
                        <span>No matches found</span>
                      )}
                    </li>
                  </ul>
                </li>
              )}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

GameBox.propTypes = {
  loadGameBoxData: PropTypes.func,
  loadMoreGameBoxData: PropTypes.func,
  withdrawBet: PropTypes.func,
  setCompetitionBox: PropTypes.func,
  setSearch: PropTypes.func,
  showMoreLiveMatches: PropTypes.func,
  sport: PropTypes.object,
  now: PropTypes.number,
  currentPage: PropTypes.number,
  currentSearchPage: PropTypes.number,
  moreGamesLoading: PropTypes.bool,
  withdrawMatchBet: PropTypes.string,
  competitionBox: PropTypes.string,
  loadedMatchesAmountBySearch: PropTypes.object,
  search: PropTypes.string,
  chosenShowMore: PropTypes.bool,
  params: PropTypes.object,
  route: PropTypes.object,
  children: PropTypes.element
}

const mapStateToProps = (state, props) => {
  return {
    sport: state.app.gameBoxesList.find(item => item.alias === props.params.activeGameBox),
    currentPage: state.app.currentPage,
    currentSearchPage: state.app.currentSearchPage,
    now: state.app.now,
    withdrawMatchBet: state.app.withdrawMatchBet,
    moreGamesLoading: state.app.moreGamesLoading,
    competitionBox: state.app.competitionBox,
    loadedMatchesAmountBySearch: state.app.loadedMatchesAmountBySearch,
    search: state.app.search,
    chosenShowMore: state.app.chosenShowMore
  }
}

const mapActionsToProps = {
  loadGameBoxData,
  loadMoreGameBoxData,
  withdrawBet,
  setCompetitionBox,
  setSearch,
  showMoreLiveMatches
}

export default connect(mapStateToProps, mapActionsToProps)(GameBox)
