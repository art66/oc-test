export interface IMatch {
  competitionId: string
  countryCode: string
  competition: string
  name: string
  bets: {
    result: string
  }[]
}

export type TMatches = IMatch[]

export interface ISelectMatchesProps {
  matches: TMatches
  competitionBox: string
  search: string
  activeGameBox: string
}

export interface ICheckBetsResultProps {
  bets: IMatch['bets']
  competitionBox: string
  activeGameBox: string
}

export interface IMatchesSelectorProps {
  matches: TMatches
  alias: string
  matchName: string
  competitionBox: string
  search: string
  activeGameBox: string
  chosenShowMore: boolean
  moreGamesLoading: boolean
  activeEventId: string
  showMoreLiveMatches: () => void
}
