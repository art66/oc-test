import { createSelector } from 'reselect'
import { getSortedMatches } from '../../selectors/matches'
import cn from 'classnames'
import { sportNameByAlias } from '../../utils'
import { MIN_SEARCH_LENGTH, POPULAR_ALIAS, YOUR_BETS_ALIAS } from '../../constants'
import styles from './index.cssmodule.scss'
import { ICheckBetsResultProps, IMatchesSelectorProps, ISelectMatchesProps } from './interfaces'

export const checkBetsResultSelector = createSelector(
  (props: ICheckBetsResultProps) => props,
  ({ bets, competitionBox, activeGameBox }) => {
    if (activeGameBox === YOUR_BETS_ALIAS && bets) {
      competitionBox = competitionBox.toLocaleLowerCase()

      for (const i in bets) {
        if (bets[i].result == competitionBox) {
          return true
        }
      }

      return false
    }

    return false
  }
)

export const selectMatchesSelector = createSelector(
  (props: ISelectMatchesProps) => props,
  ({ matches, competitionBox, search, activeGameBox }) => {
    const sportMatches = []

    if (matches) {
      for (let i = 0; i < matches.length; i++) {
        const match = matches[i]
        const matchResult = checkBetsResultSelector({ bets: match.bets, competitionBox, activeGameBox })

        if (
          !competitionBox ||
          match.competitionId === competitionBox ||
          match.countryCode === competitionBox ||
          matchResult
        ) {
          if (
            search.length < MIN_SEARCH_LENGTH ||
            match.competition.toLocaleLowerCase().indexOf(search) > -1 ||
            match.name.toLocaleLowerCase().indexOf(search) > -1 ||
            matchResult
          ) {
            sportMatches.push(match)
          }
        }
      }
    }

    return sportMatches
  }
)

export const matchesSelector = createSelector(
  (props: IMatchesSelectorProps) => props,
  ({
    matchName,
    matches,
    alias,
    competitionBox,
    search,
    activeGameBox,
    chosenShowMore,
    moreGamesLoading,
    activeEventId,
    showMoreLiveMatches
  }) => {
    const sportMatches = selectMatchesSelector({ matches, competitionBox, search, activeGameBox })
    const [currentMatches, finishedMatches, futureMatches] = getSortedMatches(sportMatches) as any // FIXME: Should has a type
    const sportAlias = sportNameByAlias(alias).toUpperCase()
    const noMatches = currentMatches.length === 0 ? 'NO ' : ''
    const isYourBets = alias === 'your-bets'
    let matchesNormalized = []

    if (activeGameBox == POPULAR_ALIAS) {
      futureMatches.sort((a, b) => (a.time < b.time ? -1 : 1))
    }

    const matchNameNormalized = matchName ? matchName.toUpperCase() : `${sportAlias} MATCHES`

    const isActiveGame = currentMatches.slice(5).filter(match => match.ID === activeEventId).length > 0

    if (currentMatches.length > 0) {
      matchesNormalized = matchesNormalized.concat({
        title: isYourBets ? 'YOUR LIVE BETS' : `${noMatches} LIVE ${matchNameNormalized}`,
        key: 0,
        className: cn(styles.live, 'live title')
      })

      if (chosenShowMore || isActiveGame) {
        matchesNormalized = matchesNormalized.concat(currentMatches)
      } else {
        matchesNormalized = matchesNormalized.concat(currentMatches.slice(0, 5))
      }
    }

    if (!chosenShowMore && currentMatches.length > 5 && !isActiveGame) {
      matchesNormalized.push({
        title: 'Show more',
        key: 'show-more',
        className: 'show-more',
        onClick: showMoreLiveMatches
      })
    }

    if (futureMatches.length > 0) {
      matchesNormalized = matchesNormalized.concat(
        {
          title: isYourBets ? 'YOUR UPCOMING BETS' : `UPCOMING ${matchNameNormalized}`,
          key: 1,
          className: cn(styles.upcoming, 'upcomming title')
        },
        futureMatches
      )
    }

    if (finishedMatches.length > 0) {
      matchesNormalized = matchesNormalized.concat(
        {
          title: isYourBets ? 'YOUR COMPLETED BETS' : `COMPLETED ${matchNameNormalized}`,
          key: 2,
          className: cn(styles.completed, 'completed title')
        },
        finishedMatches
      )
    }

    if (moreGamesLoading) {
      matchesNormalized[matchesNormalized.length - 1] = {
        loadingRow: true
      }
    }

    return matchesNormalized
  }
)
