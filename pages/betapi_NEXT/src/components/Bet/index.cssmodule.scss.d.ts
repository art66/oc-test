// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'accept': string;
  'acceptButtons': string;
  'confirmWrap': string;
  'confirmYes': string;
  'description': string;
  'globalSvgShadow': string;
  'inputContainer': string;
  'isActive': string;
  'isConfirming': string;
  'isLoading': string;
  'multiplier': string;
  'multiplierLabel': string;
  'odds': string;
  'oddsLabel': string;
  'shortConfirm': string;
}
export const cssExports: CssExports;
export default cssExports;
