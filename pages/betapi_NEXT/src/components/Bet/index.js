import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import UserBet from '../UserBet'
import { $, decimalToFractionalOdd, numberFormat, decimalOdd, betTitle } from '../../utils'
import { MATCH_NOTSTARTED_PURE_STATUS, MAX_BET_LIMIT, BET_SUSPENDED_STATUS } from '../../constants'

class Bet extends Component {
  constructor(props) {
    super(props)

    this.betRef = React.createRef()
    this.betButtonRef = React.createRef()

    this.state = {
      bet: false,
      blocked: true,
      betValue: 0
    }
  }

  componentDidMount() {
    const { betRef, betButtonRef } = this
    const $bet = $(betRef.current)

    $bet.tornInputMoney({
      buttonElement: $(betButtonRef.current),
      buttonDisabledClass: 'disabled',
      strictMode: false
    })

    $bet
      .parents('.bet-cell.input-wrap')
      .find('.input-money-symbol')
      .click(() => {
        this.onChange({
          target: betRef.current
        })
      })
  }

  componentWillUnmount() {
    const { betRef } = this
    const $bet = $(betRef.current)

    $bet.tornInputMoney('destroy')
  }

  onConfirm(activeGameBox, activeGame, marketId, bettingofferId, amount, odds, handicap) {
    const { setBetState } = this.props

    setBetState('response')

    this.props.betOnGame(activeGameBox, activeGame, marketId, bettingofferId, amount, odds, handicap, () => {
      setBetState('ok')

      this.setState({
        bet: false,
        blocked: true,
        betValue: 0
      })

      this._clearInput()
    })
  }

  onReject() {
    const { selectBet, setBetState } = this.props

    selectBet({})
    setBetState('confirm')

    this.setState({
      blocked: false
    })

    this._clearInput()
  }

  onBetComplete() {
    const { selectBet, setBetState } = this.props

    selectBet({})
    setBetState('')
  }

  onBet() {
    const { selectBet, setBetState, odd } = this.props

    if (this.state.blocked) {
      return
    }

    this.setState({
      bet: true
    })

    selectBet(odd)
    setBetState('accept')
  }

  onChange(event) {
    const { value, isValue } = this._getBetValue(event.target)

    this.setState({
      bet: false,
      blocked: !isValue,
      betValue: isValue ? value : 0
    })
  }

  onKeyDown(event) {
    if (event.charCode === 13) {
      this.onBet()
    }
  }

  _clearInput() {
    this.betRef?.current?.dispatchEvent(new Event('input', { bubbles: true }))
  }

  _getBetValue(inputEl) {
    const value = parseInt(inputEl.value?.split(',').join(''), 10)
    const isValue = !(isNaN(value) || value <= 0)

    return {
      value,
      isValue
    }
  }

  _onBlurFocus(target) {
    setTimeout(() => {
      const { isValue } = this._getBetValue(target)

      this.setState({
        blocked:
          $(target)
            .parents('.input-money-group')
            .hasClass('error') || !isValue
      })
    }, 0)
  }

  _getAmount() {
    return this.state.betValue
  }

  _getBetStrLength() {
    return (numberFormat(this._getAmount()) + this.props.odd.bettingoffer).length
  }

  _isBetInputHiding() {
    const { match } = this.props

    return !(match.status === 'notstarted' || match.status === 'inprogress')
  }

  _getPotentialWinning(odds) {
    const purePotentialWinning = this._getAmount() * (odds - 1)

    let potentialWinning
    if (purePotentialWinning >= 1) {
      potentialWinning = Math.round(purePotentialWinning)
    } else {
      potentialWinning = purePotentialWinning.toFixed('2')
    }

    return ` $${numberFormat(potentialWinning)}`
  }

  _getConfirmMsg(odd) {
    return (
      <span>
        Bet <span className='bold'> ${numberFormat(this._getAmount())} </span> on{' '}
        <span className='bold'>{odd.bettingoffer}</span> to win? The potential winnings are
        <span className='bold'>{this._getPotentialWinning(odd.odds)}</span>.
      </span>
    )
  }

  _getShortConfirmMsg(odd) {
    return (
      <span>
        Are you sure you want to bet <span className='bold'> ${numberFormat(this._getAmount())} </span>
        on{' '}
        <span className='bold' title={odd.bettingoffer}>
          {odd.bettingoffer.toString().substr(0, 6)}...{' '}
        </span>
        to win? Your potential winnings are
        <span className='bold'>{` $${numberFormat(Math.round(this._getAmount() * (odd.odds - 1)))}`}</span>.
      </span>
    )
  }

  _getMoneyLimit() {
    const {
      userData: { money },
      match: { bets }
    } = this.props
    let sumOutcomeBets = 0

    bets.forEach(bet => {
      if (bet.result === 'pending') {
        sumOutcomeBets += bet.amount
      }
    })

    const limit = money === undefined ? MAX_BET_LIMIT - sumOutcomeBets : Math.min(money, MAX_BET_LIMIT - sumOutcomeBets)

    return limit
  }

  _getInputValue() {
    const { odd } = this.props

    if (odd.state === BET_SUSPENDED_STATUS && !odd.betIsAllowed) {
      return 'Suspended'
    }

    if (this.state.bet) {
      return ''
    }

    return undefined
  }

  _renderBetInput() {
    const { match } = this.props

    const isNotStarted = match.status === MATCH_NOTSTARTED_PURE_STATUS

    if (!isNotStarted) {
      return null
    }

    const { odd, activeBet } = this.props
    const { blocked } = this.state
    const isActive = activeBet === `${odd.eventId}-${odd.outcomeId}`
    const moneyLimit = this._getMoneyLimit()
    const isDisabled = isNotStarted && !odd.betIsAllowed

    return (
      <div
        className={cn(styles.inputContainer, 'bet-cell input-wrap')}
        style={{
          display: isActive ? 'none' : 'block'
        }}
      >
        <input
          ref={this.betRef}
          type='text'
          className='amount'
          name='amount'
          value={this._getInputValue()}
          data-money={moneyLimit}
          data-limit={moneyLimit}
          size='10'
          autoComplete='off'
          maxLength='15'
          onKeyUp={e => this.onChange(e)}
          onChange={e => this.onChange(e)}
          onKeyDown={e => this.onKeyDown(e)}
          onBlur={e => this._onBlurFocus(e.target)}
          onFocus={e => this._onBlurFocus(e.target)}
          disabled={isDisabled}
        />
        <button
          ref={this.betButtonRef}
          type='button'
          className={`input-btn${blocked ? ' disabled' : ''}`}
          style={{
            marginLeft: 0,
            height: 24,
            padding: '4px 8px'
          }}
          onClick={() => this.onBet()}
        >
          BET
        </button>
      </div>
    )
    /*
      Hide - when is not "notstarted"
      Disable - when "notstarted" and "betIsAllowed" = false
      Enabled - when "notstarted" and "betIsAllowed" = true
      Message "There are no bets currently available for this listing" - when "notstarted" and no bets
    */
  }

  _renderOkMessage() {
    return (
      <span
        className='t-blue bet-no bold m-left10 c-pointer'
        onClick={() => {
          this.onBetComplete()
        }}
      >
        {' '}
        Ok
      </span>
    )
  }

  _renderPlaceBetAnyway() {
    const { odd, activeGameBox, activeGame } = this.props

    return (
      <span
        className='t-blue bet-no bold m-left10 c-pointer'
        onClick={() => {
          this.onConfirm(
            activeGameBox,
            activeGame,
            odd.marketId,
            odd.bettingofferId,
            this._getAmount(),
            odd.odds,
            odd.handicap
          )
        }}
      >
        {' '}
        [Place bet anyway]
      </span>
    )
  }

  _renderAccept() {
    const { odd, activeGameBox, activeGame, pageMode } = this.props

    return (
      <div className={styles.accept}>
        {pageMode === 'desktop' && this._getBetStrLength() >= 20 ? (
          <div className={cn(styles.shortConfirm, 'left')}>{this._getShortConfirmMsg(odd)}</div>
        ) : (
          this._getConfirmMsg(odd)
        )}
        <span
          className={cn(
            styles.acceptButtons,
            `link-wrap${pageMode === 'desktop' && this._getBetStrLength() >= 20 ? ' left' : ''}`
          )}
        >
          <span
            className={cn(styles.confirmYes, 't-blue bet-yes bold m-left10 c-pointer')}
            onClick={() => {
              this.onConfirm(
                activeGameBox,
                activeGame,
                odd.marketId,
                odd.bettingofferId,
                this._getAmount(),
                odd.odds,
                odd.handicap
              )
            }}
          >
            {' '}
            Yes
          </span>
          <span
            className='t-blue bet-no bold m-left10 c-pointer'
            onClick={() => {
              this.onReject()
            }}
          >
            {' '}
            No
          </span>
        </span>
      </div>
    )
  }

  _renderConfirmContent() {
    const { odd, betLoading, betState } = this.props

    if (betLoading) {
      return <img src='/images/v2/main/ajax-loader.gif' />
    } else if (odd.betResponse && betState === 'response' && odd.betResponse.askToBet) {
      return (
        <div className={`bold ${odd.betResponse.success ? 't-green' : 't-red'}`}>
          {odd.betResponse.msg}
          {this._renderPlaceBetAnyway()}
        </div>
      )
    } else if (odd.betResponse && betState === 'ok') {
      return (
        <div className={`bold ${odd.betResponse.success ? 't-green' : 't-red'}`}>
          {odd.betResponse.msg}
          {this._renderOkMessage()}
        </div>
      )
    } else if (betState === 'accept') {
      return this._renderAccept()
    }
  }

  _renderConfirm() {
    const { odd, activeBet, betLoading } = this.props
    const isActive = activeBet === `${odd.eventId}-${odd.outcomeId}`

    return (
      (!isActive && !activeBet) || (
        <div
          className={cn(
            styles.confirmWrap,
            { [styles.isActive]: isActive, [styles.isLoading]: betLoading },
            'confirm-wrap'
          )}
        >
          {this._renderConfirmContent()}
        </div>
      )
    )
  }

  render() {
    const { match, odd, now, activeBet } = this.props
    const isActive = activeBet === `${odd.eventId}-${odd.outcomeId}`

    return (
      <li className={`bets${odd.result === 'won' ? ' bg-green ' : ''}`}>
        <div className='bet clearfix'>
          <div className='cells-wrap left clearfix'>
            <div className={cn('bet-cell odds fractional', styles.odds)}>
              <span className={cn(styles.oddsLabel, 'label bold t-show')}>Odds:</span>{' '}
              {odd.numerator && odd.denominator
                ? `${odd.numerator}/${odd.denominator}`
                : decimalToFractionalOdd(odd.odds)}
            </div>
            <div className={cn(styles.multiplier, 'bet-cell odds')}>
              <span className={cn(styles.multiplierLabel, 'label bold t-show')}>Multiplier:</span> x
              {decimalOdd(parseFloat(odd.odds))}
            </div>
            <div
              className={cn(styles.description, 'bet-cell result', {
                closed: this._isBetInputHiding(),
                [styles.isConfirming]: isActive
              })}
            >
              {odd.odds > 1 || match.startTimestamp < now
                ? odd.bettingoffer === 'The Draw'
                  ? 'Draw'
                  : odd.bettingoffer + (odd.handicap ? ` (${odd.handicap > 0 ? '+' : ''}${odd.handicap})` : '')
                : 'Odds are currently unavailable for this bet'}
              {!match.bets || (
                <ul className='stick-wrap right'>
                  {match.bets.map((bet, index) => {
                    if (bet.eventId === odd.eventId && bet.outcomeId === odd.outcomeId) {
                      const { symbol, title, betState } = betTitle(bet)

                      return <UserBet bet={bet} key={index} symbol={symbol} title={title} betState={betState} />
                    }
                  })}
                </ul>
              )}
            </div>
            {this._renderBetInput()}
            {this._renderConfirm()}
          </div>
        </div>
      </li>
    )
  }
}

Bet.propTypes = {
  match: PropTypes.object,
  odd: PropTypes.object,
  now: PropTypes.number,
  userData: PropTypes.object,
  activeGameBox: PropTypes.string,
  activeGame: PropTypes.string,
  betLoading: PropTypes.bool,
  betOnGame: PropTypes.func,
  selectBet: PropTypes.func,
  setBetState: PropTypes.func,
  activeBet: PropTypes.string,
  betState: PropTypes.string,
  pageMode: PropTypes.string
}

export default Bet
