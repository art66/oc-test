import {
  MATCH_NOTSTARTED_PURE_STATUS,
  MATCH_INPROGRESS_PURE_STATUS,
  MATCH_FINISHED_PURE_STATUS,
  MATCH_CANCELLED_PURE_STATUS,
  MATCH_INTERRUPTED_PURE_STATUS,
  MATCH_UNKNOWN_PURE_STATUS,
  MATCH_DELETED_PURE_STATUS
} from '../constants'

export type TStatus =
  | typeof MATCH_NOTSTARTED_PURE_STATUS
  | typeof MATCH_INPROGRESS_PURE_STATUS
  | typeof MATCH_FINISHED_PURE_STATUS
  | typeof MATCH_CANCELLED_PURE_STATUS
  | typeof MATCH_INTERRUPTED_PURE_STATUS
  | typeof MATCH_UNKNOWN_PURE_STATUS
  | typeof MATCH_DELETED_PURE_STATUS
  | typeof MATCH_POSTPONED_PURE_STATUS

export type TTeam = {
  name: string
}

export type TTeams = TTeam[]

export type TMatch = {
  status: TStatus
  sport: string
  stage: string
  tournament: string
  gender: string
  template: string
  teams: TTeams
  teamsName: string
}

export type TMatches = TMatch[]

export type TSortedMatches = {
  current: TMatch[]
  finished: TMatch[]
  future: TMatch[]
}
