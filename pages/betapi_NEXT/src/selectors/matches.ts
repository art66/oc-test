import { createSelector } from 'reselect'
import {
  MATCH_CURRENT_STATUS,
  MATCH_POSTPONED_PURE_STATUS,
  MATCH_FINISHED_STATUS,
  MATCH_FUTURE_STATUS,
  MATCH_NOTSTARTED_PURE_STATUS,
  MATCH_INPROGRESS_PURE_STATUS
} from '../constants'
import { TStatus, TMatches } from './types'

export const isInCurrentMathes = createSelector([status => status], (status: TStatus): boolean => {
  return status === MATCH_INPROGRESS_PURE_STATUS
})

export const isInFutureMathes = createSelector([status => status], (status: TStatus): boolean => {
  return status === MATCH_NOTSTARTED_PURE_STATUS
})

export const getMatchStatus = createSelector([status => status], (status: TStatus):
  | typeof MATCH_CURRENT_STATUS
  | typeof MATCH_FINISHED_STATUS
  | typeof MATCH_FUTURE_STATUS
  | typeof MATCH_POSTPONED_PURE_STATUS => {
  if (isInCurrentMathes(status)) {
    return MATCH_CURRENT_STATUS
  } else if (isInFutureMathes(status)) {
    return MATCH_FUTURE_STATUS
  }

  // FIXME: All others statuses can't be in "finished" mathes list

  return MATCH_FINISHED_STATUS
})

export const getSortedMatches = createSelector([matches => matches], (matches: TMatches): object => {
  const current = []
  const finished = []
  const future = []

  matches.forEach(match => {
    const status = getMatchStatus(match.status)

    switch (status) {
      case MATCH_CURRENT_STATUS: {
        current.push(match)

        break
      }
      case MATCH_FINISHED_STATUS: {
        finished.push(match)

        break
      }
      case MATCH_POSTPONED_PURE_STATUS: {
        finished.push(match)

        break
      }
      case MATCH_FUTURE_STATUS: {
        future.push(match)

        break
      }
      default: {
        current.push(match)
      }
    }
  })

  const finishedWithPostponedPriority = finished.sort(finishedMatch => {
    if (finishedMatch.status === MATCH_POSTPONED_PURE_STATUS) {
      return 1
    }

    return 0
  })

  return [current, finishedWithPostponedPriority, future]
})
