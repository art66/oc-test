import { createSelector } from 'reselect'
import { GENDER_FEMALE } from '../constants'
import { TMatch } from './types'
import { decodeHtmlCharCodes } from '../utils'

export const getCompetitionString = createSelector([data => data], ({ stage, tournament, template, gender }) => {
  return decodeHtmlCharCodes(
    `${stage ? ` - ${stage}` : ''}${tournament ? ` ${tournament}` : ''}${
      template ? ` (${template}${gender === GENDER_FEMALE ? ', female' : ''})` : ''
    }`
  )
})

export const getTeamsNameString = createSelector(
  [({ teams, teamsName }: { teams: TMatch['teams']; teamsName: TMatch['teamsName'] }) => ({ teams, teamsName })],
  ({ teams, teamsName }): string => {
    if (teams && teams.length) {
      return teams
        .map((team, index) => {
          if (index === 0) {
            return team.name
          }

          return ` v ${team.name}`
        })
        .join('')
    }

    return decodeHtmlCharCodes(teamsName)
  }
)

export const getMatchName = createSelector(
  [getTeamsNameString, getCompetitionString],
  (teamsName, competition): string => {
    return `${teamsName}${competition}`
  }
)
