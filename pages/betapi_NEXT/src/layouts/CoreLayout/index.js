import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import DebugInfo from '../../components/DebugInfo'
import InfoBox from '@torn/shared/components/InfoBox'

export const CoreLayout = ({ children, app: { dbg, infoBox } }) => (
  <div className='core-layout'>
    {dbg && <DebugInfo msg={dbg} />}
    {infoBox && <InfoBox {...infoBox} />}
    {children}
  </div>
)

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
  app: PropTypes.object
}

const mapStateToProps = state => ({
  app: state.app
})

export default connect(mapStateToProps)(CoreLayout)
