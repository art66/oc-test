import { createAction } from '@reduxjs/toolkit'
import { createAction as createActionDeprecated } from 'redux-actions'
import { fetchUrl } from '@torn/shared/utils'
import { BASE_URL } from '../constants'
import { SET_IS_DESKTOP_LAYOUT_TYPE } from './constants'

export function updateNow(now /* , userMoney */) {
  return dispatch => {
    dispatch(setNow(now /* , userMoney */))
  }
}

export const setIsDesktopLayoutAction = createAction(SET_IS_DESKTOP_LAYOUT_TYPE)

export const setUserMoney = createActionDeprecated('SET_USER_MONEY')

export function initPageMode(dispatch, getState) {
  const isResponsive = document.body.classList.contains('d') && document.body.classList.contains('r')
  const mediaType = getState().browser.mediaType === 'infinity' ? 'desktop' : getState().browser.mediaType
  const pageMode = isResponsive ? mediaType : 'desktop'

  dispatch(setPageMode(pageMode))
}

export function getInitialData(gameBox = '', isStats = false) {
  return (dispatch, getState) => {
    return fetchUrl(`${BASE_URL}&action=getState&gamebox=${gameBox}&isstats=${isStats}`).then(json => {
      initPageMode(dispatch, getState)

      const { gameBoxesList, userData, ...otherData } = json

      dispatch(dataLoaded({ gameBoxesList, userData, ...otherData }))
    })
  }
}

export const setPageMode = createActionDeprecated('set page mode', pageMode => ({ pageMode }))

export function loadGameBoxData(gameBox, isStats = false) {
  return dispatch => {
    return fetchUrl(`${BASE_URL}&action=getState&gamebox=${gameBox}&isstats=${isStats}`).then(json => {
      const { gameBoxesList, userData, ...otherData } = json

      dispatch(gameBoxLoaded({ gameBoxesList, userData, ...otherData }, gameBox))
    })
    // .catch((error) => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export function loadMoreGameBoxData(gameBox, currentPage, search = '', competition = '', isStats = false) {
  return dispatch => {
    if (loadMoreGameBoxData.isLoading) return

    loadMoreGameBoxData.isLoading = true

    dispatch(moreGamesLoading())

    return fetchUrl(
      `${BASE_URL}&action=loadMatches&gamebox=${gameBox}&page=${currentPage}` +
        `&search=${search}&competition=${competition}&isstats=${isStats}`
    )
      .then(json => {
        const { matches, ...otherData } = json

        dispatch(gameMoreBoxLoaded({ matches, ...otherData }, gameBox, currentPage))
      })
      .finally(() => {
        loadMoreGameBoxData.isLoading = false
      })
  }
}

export function loadGameData(gameBox, eventId) {
  return dispatch => {
    dispatch(gameMarketsLoading(gameBox, eventId))
    return fetchUrl(`${BASE_URL}&action=getEventOutcomeTypes&gamebox=${gameBox}&eventId=${eventId}`).then(json => {
      dispatch(gameMarketsLoaded(json, gameBox, eventId))
    })
  }
}

export function selectGame(sportAlias, eventId) {
  return dispatch => {
    dispatch(gameLoading(eventId))
    if (eventId) {
      return fetchUrl(`${BASE_URL}&action=getState&gamebox=${sportAlias}&eventId=${eventId}`).then(json => {
        dispatch(gameLoaded(json, sportAlias, eventId))
      })
      // .catch((error) => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
    }
  }
}

export function betOnGame(sportAlias, eventId, marketId, bettingofferId, amount, odds, handicap = 0, callback) {
  return dispatch => {
    dispatch(betLoading())

    return fetchUrl(`${BASE_URL}&action=addBet&gamebox=${sportAlias}&eventId=${eventId}`, {
      data: { marketId, bettingofferId, amount, odds, handicap }
    }).then(json => {
      callback && callback()

      dispatch(betLoaded(json, sportAlias, eventId, marketId, bettingofferId))
    })
    // .catch((error) => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export function withdrawBet(matchID, gameBox) {
  return dispatch => {
    dispatch(withdrawingBet(matchID))
    return fetchUrl(`${BASE_URL}&action=withdrawBet`, {
      eventId: matchID
    }).then(json => dispatch(betWithdrawn(json, matchID, gameBox)))
  }
}

export function withdrawAll(gameBox, eventId) {
  return dispatch => {
    dispatch(withdrawingAll())

    return fetchUrl(`${BASE_URL}&action=withdrawAll&gamebox=${gameBox}&eventId=${eventId}`, {}).then(json => {
      return dispatch(allBetWithdrawn(json))
    })
  }
}

export function selectBet(runner) {
  return dispatch => {
    dispatch(betSelected(runner))
  }
}

export const setBetState = createActionDeprecated('set betState', betState => ({
  betState
}))

export const gameMoreBoxLoaded = createActionDeprecated('more game box loaded', (json, gameBox, page) => ({
  json,
  gameBox,
  page,
  meta: 'ajax'
}))

export const withdrawingAll = createActionDeprecated('withdrawing all', () => ({}))

export const allBetWithdrawn = createActionDeprecated('all bet withdrawn', json => ({
  json
}))

export function setCompetitionBox(competitionBox) {
  return dispatch => {
    dispatch(setSearchByCompetition(competitionBox))
  }
}

export const setSearchByCompetition = createActionDeprecated('set competition box', competitionBox => ({
  competitionBox
}))

export function setSearch(search) {
  return dispatch => {
    dispatch(setMatchSearch(search))
  }
}

export const setMatchSearch = createActionDeprecated('set search', search => ({
  search
}))

export function showMoreLiveMatches() {
  return dispatch => {
    dispatch(setShowMoreLiveMatches())
  }
}

export const setShowMoreLiveMatches = createActionDeprecated('set show more live matches', () => ({}))

export function loadStatsData(gameBox) {
  return (dispatch, getState) => {
    dispatch(statsDataLoading())
    return fetchUrl(`${BASE_URL}&action=getStats&gamebox=${gameBox}`).then(json => {
      initPageMode(dispatch, getState)
      dispatch(statsDataLoaded(json))
    })
  }
}

export const statsDataLoading = createActionDeprecated('stats data loading', () => ({}))

export const statsDataLoaded = createActionDeprecated('stats data loaded', json => ({
  json
}))

export const withdrawingBet = createActionDeprecated('withdrawing bet', eventId => ({
  eventId
}))

export const betWithdrawn = createActionDeprecated('withdrawn bet', (json, eventId, gameBox) => ({
  json,
  eventId,
  gameBox
}))

export const betSelected = createActionDeprecated('bet selected', runner => ({
  runner
}))

export const setNow = createActionDeprecated('set now', (now /* , userMoney */) => ({ now /* , userMoney */ }))

export const gameLoaded = createActionDeprecated('game loaded', (json, sportAlias, eventId) => ({
  json,
  sportAlias,
  eventId,
  meta: 'ajax'
}))

export const gameMarketsLoading = createActionDeprecated('game markets loading', (gameBox, eventId) => ({
  gameBox,
  eventId
}))

export const gameMarketsLoaded = createActionDeprecated('game markets loaded', (json, gameBox, eventId) => ({
  json,
  gameBox,
  eventId,
  meta: 'ajax'
}))

export const gameBoxLoaded = createActionDeprecated('game box loaded', (json, sportAlias) => ({
  json,
  sportAlias,
  meta: 'ajax'
}))

export const moreGamesLoading = createActionDeprecated('more games loading')

export const dataLoaded = createActionDeprecated('data loaded', json => ({
  json,
  meta: 'ajax'
}))

export const gameLoading = createActionDeprecated('game loading', eventId => ({ eventId }))

export const betLoading = createActionDeprecated('bet loading', () => ({}))

export const betLoaded = createActionDeprecated('bet loaded', (json, gameBox, eventId, marketId, bettingofferId) => ({
  json,
  gameBox,
  eventId,
  marketId,
  bettingofferId
}))

export const bringInfoBox = createActionDeprecated('bring info box')

export const hideInfoBox = createActionDeprecated('hide info box')

export function showInfoBox(argsObj) {
  return dispatch => {
    console.error(argsObj.msg)
    dispatch(bringInfoBox(argsObj))

    if (!argsObj.persistent) {
      setTimeout(() => dispatch(hideInfoBox()), 5000)
    }
  }
}
