import { createSelector } from 'reselect'
import { SECOND, MINUTE, HOUR, DAY } from '../constants'

const globalNumberFormat = window.numberFormat
const { $, getCurrentTimestamp } = window

export { $, getCurrentTimestamp }

export const plusZero = createSelector(
  num => num,
  num => {
    return num < 10 ? `0${num}` : num
  }
)

export const dateTimeFormat = createSelector(
  timestamp => timestamp,
  timestamp => {
    const d = new Date(timestamp)
    const year = d.getUTCFullYear()
    const month = plusZero(d.getUTCMonth() + 1)
    const date = plusZero(d.getUTCDate())
    const hours = plusZero(d.getUTCHours())
    const minutes = plusZero(d.getUTCMinutes())
    const seconds = plusZero(d.getUTCSeconds())

    return `${hours}:${minutes}:${seconds} - ${date}/${month}/${year}`
  }
)

export const shortMoneyFormat = createSelector(
  num => num,
  num => {
    const billion = 'b'
    const million = 'm'
    const thousand = 'k'
    let result = false

    if (num < 1000) {
      result = num
    } else if (num < 1000000) {
      if (num < 10000) {
        result = Math.floor(num / 100) / 10 + thousand
      } else {
        result = Math.round(num / 1000) + thousand
      }
    } else if (num < 1000000000) {
      result = Math.floor(num / 100000)
      result = (result < 100 ? result / 10 : Math.round(result / 10)) + million
    } else if (num >= 1000000000) {
      result = Math.floor(num / 100000000)
      result = (result < 100 ? result / 10 : Math.round(result / 10)) + billion
    }

    if (!result) {
      return numberFormat(num)
    }

    return result
  }
)

export const decimalToFractionalOdd = createSelector(
  decimal => decimal,
  decimal => {
    const p = 1 / decimal

    for (let diff = 0.00001; diff <= 0.001; diff *= 10) {
      for (let x = 1; x <= 1000; x++) {
        for (let y = 1; y <= 1000; y++) {
          const pf = y / (y + x)

          if (Math.abs(p - pf) < diff) {
            return `${x}/${y}`
          }
        }
      }
    }
  }
)

export function decimalOdd(decimal) {
  return decimal.toFixed(2)
}

export function numberFormat(number) {
  return globalNumberFormat(number)
}

export const getCountdownSelector = createSelector(
  props => props,
  props => {
    const { timestamp, now } = props

    const day = Math.floor((timestamp - now) / DAY)

    if (day > 0) {
      return `${day}d`
    }

    const hour = Math.floor((timestamp - now) / HOUR)

    if (hour > 0) {
      return `${hour}h`
    }

    const minute = Math.floor((timestamp - now) / MINUTE)

    if (minute > 0) {
      return `${minute}m`
    }

    const second = Math.floor((timestamp - now) / SECOND)

    if (second > 0) {
      return `${second}s`
    }
  }
)

export function getCountdown(timestamp, now) {
  return getCountdownSelector({ timestamp, now })
}

export const finishTimestamp = createSelector(
  ts => ts,
  ts => {
    return dateTimeFormat(new Date(Math.floor(ts / MINUTE) * MINUTE).getTime())
  }
)

export const sportNameByAlias = createSelector(
  alias => alias,
  alias => {
    return (alias[0].toUpperCase() + alias.slice(1)).split('-').join(' ')
  }
)

export const dayDateFormat = createSelector(
  timestamp => timestamp,
  timestamp => {
    if (timestamp) {
      const d = new Date(timestamp)
      const year = d
        .getUTCFullYear()
        .toString()
        .substr(-2)
      const month = plusZero(d.getUTCMonth() + 1)
      const date = plusZero(d.getUTCDate())

      return `${date}/${month}/${year}`
    }

    return 'Unknown'
  }
)

export const betTitle = createSelector(
  bet => bet,
  bet => {
    const { outcome } = bet
    let betState = ''
    let title = ''
    const marketPart = `(${
      bet.handicap != 0 ? `${(bet.handicap > 0 ? '+' : '') + bet.handicap} handicap` : bet.outcomeType
    })`

    if (bet.result === 'won') {
      betState = 'won'
      title = `Won $${numberFormat(bet.moneyGain)} (x${decimalOdd(bet.odds, 2)})
    from a $${numberFormat(bet.amount)} bet on ${outcome} (${bet.outcomeType})`
    } else if (bet.result === 'lost') {
      betState = 'lost'
      title = `Lost $${numberFormat(bet.amount)} (x${decimalOdd(bet.odds, 2)}) bet on ${outcome} ${marketPart}`
    } else if (bet.result === 'refunded') {
      betState = 'refunded'
      title = `Refunded $${numberFormat(bet.amount)} (x${decimalOdd(bet.odds, 2)}) bet on ${outcome} ${marketPart}`
    } else {
      title = `Pending $${numberFormat(bet.amount)} (x${decimalOdd(bet.odds, 2)}) bet on ${outcome} ${marketPart}`
      betState = 'pending'
    }
    const symbol = betState === 'refunded' || betState === 'pending' ? '' : betState === 'lost' ? '-' : '+'
    // const symbol = ''
    const withdraw = ['won', 'refunded'].indexOf(bet.result) >= 0 && !bet.withdrawn ? 'withdraw' : ''

    return {
      symbol,
      title,
      betState,
      withdraw
    }
  }
)

export const isManualDesktopMode = () => document.body.classList.contains('d') && !document.body.classList.contains('r')

// check if element in the viewport
export const isInViewport = (top, offset = 0) => {
  return top + offset >= 0 && top - offset <= window.innerHeight
}

export const decodeHtmlCharCodes = (() => {
  const element = document.createElement('div')

  return function decodeHtmlCharCodesHandler(str) {
    element.innerHTML = str

    return element.innerHTML
  }
})()
