export interface TTeam {
  id: string
  name: string
}

export type TTeams = TTeam[]

export interface TUserBet {
  result: string
}

export type TUserBets = TUserBet[]

export interface IMatch {
  bets: TUserBets
}
