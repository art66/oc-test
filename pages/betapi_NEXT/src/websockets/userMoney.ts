import { Dispatch } from 'react'
import { setUserMoney } from '../actions'

const initWS = (dispatch: Dispatch<any>) => {
  interface ITornWindow extends Window {
    WebsocketHandler?: {
      addEventListener: (namespace: string, action: string, callback: (data?: any) => any) => any
    }
  }

  const win = window as ITornWindow

  win.WebsocketHandler.addEventListener('sidebar', 'updateMoney', (payload: { money: string }) => {
    dispatch(setUserMoney(String(payload.money)))
  })
}

export default initWS
