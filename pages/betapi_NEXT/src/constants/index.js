// export const BASE_URL_OLD = '/bookies_logout.php?step=betfair'
export const BASE_URL = '/page.php?sid=bookieApi'

export const TIME_FORMAT_NAME = 'TCT'

export const SECOND = 1000
export const MINUTE = 60000
export const HOUR = 3600000
export const DAY = 86400000

export const YOUR_BETS_ALIAS = 'your-bets'
export const POPULAR_ALIAS = 'popular'

export const MIN_SEARCH_LENGTH = 3
export const MIN_MATCHE_NUMBER = 25

export const MATCH_NOTSTARTED_PURE_STATUS = 'notstarted'
export const MATCH_INPROGRESS_PURE_STATUS = 'inprogress'
export const MATCH_FINISHED_PURE_STATUS = 'finished'
export const MATCH_CANCELLED_PURE_STATUS = 'cancelled'
export const MATCH_INTERRUPTED_PURE_STATUS = 'interrupted'
export const MATCH_UNKNOWN_PURE_STATUS = 'unknown'
export const MATCH_DELETED_PURE_STATUS = 'deleted'
export const MATCH_POSTPONED_PURE_STATUS = 'postponed'

export const MATCH_ABANDONED_STATUS_DESC = 'Abandoned'

export const MATCH_CURRENT_STATUS = 'MATCH_CURRENT_STATUS'
export const MATCH_FINISHED_STATUS = 'MATCH_FINISHED_STATUS'
export const MATCH_FUTURE_STATUS = 'MATCH_FUTURE_STATUS'

export const BET_SUSPENDED_STATUS = 'suspended'

export const GENDER_FEMALE = 'female'

export const MAX_BET_LIMIT = 100000000

export const SCROLL_OFFSET = 50
