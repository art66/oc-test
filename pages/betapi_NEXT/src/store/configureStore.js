import { createStore, applyMiddleware, compose } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
// import createLogger from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import { fetchInterceptor } from './middleware/fetchInterceptor'
import makeRootReducer from '../reducers'
import initUserMoneyWS from '../websockets/userMoney'

export default function configureStore(preloadedState) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  const store = createStore(
    makeRootReducer(),
    preloadedState,
    composeEnhancers(
      responsiveStoreEnhancer,
      applyMiddleware(
        thunkMiddleware,
        fetchInterceptor
        // createLogger()
      )
    )
  )

  store.asyncReducers = {}

  initUserMoneyWS(store.dispatch)

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const reducers = require('../reducers').default

      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
