import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-dom'

import createStore from './store/createStore'
import Gym from './containers/Gym'

// ========================================================
// Store Instantiation
// ========================================================
const initialState = window.___INITIAL_STATE__

export const store = createStore(initialState)

if (__DEV__) {
  import('../../../modules/header/src/main')
  import('../../../modules/sidebar/src/main')
  window.state = store.getState()
}

render(
  <Provider store={store}>
    <Gym />
  </Provider>,
  document.getElementById('gymroot')
)
