import { getCookie } from '@torn/sidebar/src/utils'
/* global addRFC */

export const fetchUrl = (url, data) => {
  return fetch(addRFC(url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      let json

      try {
        json = JSON.parse(text)
        if (json && json.error) {
          error = json.error
        }
        if (!json.success && json.reload) {
          window.location.href = window.location.pathname
          return
        }
      } catch (e) {
        error = `Server responded with: ${text}`
      }

      if (error) {
        throw new Error(error)
      } else {
        return json
      }
    })
  })
}

export const getNewStatValue = (currStatValue, gain, isInt) => {
  let currStat = parseFloat(currStatValue.replace(/\,/g, ''))

  const tmpCurrStat = (currStat + gain).toFixed(2)
  const fraction = tmpCurrStat.substr(tmpCurrStat.indexOf('.'))

  currStat = tmpCurrStat.substr(0, tmpCurrStat.indexOf('.')).split('').reverse()
  const groupArr = []

  for (let i = 0; i < currStat.length; i += 3) {
    groupArr.push(
      currStat
        .slice(i, i + 3)
        .reverse()
        .join('')
    )
  }
  return isInt ? groupArr.reverse().join(',') : groupArr.reverse().join(',') + fraction
}

export const checkDarkMode = () => {
  const DMCookieValue = getCookie('darkModeEnabled')

  return DMCookieValue === 'true'
}
