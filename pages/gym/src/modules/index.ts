import { getCookie } from '@torn/shared/utils'
import { fetchUrl, getNewStatValue } from '../utils'
import {
  GYM_TRAINS_COOKIE_NAME,
  YEAR,
  MAX_AMOUNT_OF_TRAINS,
  ANIMATION_INTERVAL,
  AMOUNT_ANIMATIONS_ITERATIONS,
  GAIN_PERCENTAGE
} from '../constants'
import IGymItem from '../models/GymItem'
import IAction from '../models/Action'

const timer = {
  strength: 0,
  defense: 0,
  speed: 0,
  dexterity: 0
}
const HIGHLIGHT_TIMEOUT = 100

const SET_INITIAL_INFORMATION = 'SET_INITIAL_INFORMATION'
const GET_AJAX_ERROR = 'GET_AJAX_ERROR'
const SET_AMOUNT_TRAIN = 'SET_AMOUNT_TRAIN'
const INCREASE_AMOUNT_TRAIN = 'INCREASE_AMOUNT_TRAIN'
const DECREASE_AMOUNT_TRAIN = 'DECREASE_AMOUNT_TRAIN'
const UPDATE_GYMS_INFORMATION = 'UPDATE_GYMS_INFORMATION'
const UPDATE_ACTIVE_GYM = 'UPDATE_ACTIVE_GYM'
const UPDATE_ENERGY = 'UPDATE_ENERGY'
const UPDATE_TRAIN_STATUS = 'UPDATE_TRAIN_STATUS'
const UPDATE_STAT = 'UPDATE_STAT'
const SET_PREVIEW_GYM = 'SET_PREVIEW_GYM'
const DISABLE_GYM_PREVIEW = 'DISABLE_GYM_PREVIEW'
const SET_MESSAGE = 'SET_ERROR_MESSAGE'
const RESET_GYM_DETAILS_STATE = 'RESET_GYM_DETAILS_STATE'
const SET_TIME_TO_UPDATE = 'SET_TIME_TO_UPDATE'
const TOGGLE_CONFIRMATION = 'TOGGLE_CONFIRMATION'
const UPDATE_STATS_PERCENTAGE = 'UPDATE_STATS_PERCENTAGE'
const SET_DARK_MODE_STATE = 'gym/SET_DARK_MODE_STATE'

export const setDarkModeState = darkModeEnabled => {
  return {
    type: SET_DARK_MODE_STATE,
    darkModeEnabled
  }
}

export const updateStatsPercentage = statsPercentages => {
  return {
    type: UPDATE_STATS_PERCENTAGE,
    statsPercentages
  }
}

const setInitialInformation = initialData => {
  return {
    type: SET_INITIAL_INFORMATION,
    initialData
  }
}

const getAjaxError = error => {
  return {
    type: GET_AJAX_ERROR,
    error
  }
}

export const setAmountTrain = (statName, amountTrain) => {
  return {
    type: SET_AMOUNT_TRAIN,
    statName,
    amountTrain
  }
}

export const increaseAmountTrain = statName => {
  return {
    type: INCREASE_AMOUNT_TRAIN,
    statName
  }
}

export const decreaseAmountTrain = statName => {
  return {
    type: DECREASE_AMOUNT_TRAIN,
    statName
  }
}

export const updateGymsInformation = updatedGyms => {
  return {
    type: UPDATE_GYMS_INFORMATION,
    updatedGyms
  }
}

export const updateActiveGym = () => {
  return {
    type: UPDATE_ACTIVE_GYM
  }
}

export const updateEnergy = energy => {
  return {
    type: UPDATE_ENERGY,
    energy
  }
}

export const updateStat = (statName, statValue) => {
  return {
    type: UPDATE_STAT,
    statName,
    statValue
  }
}

export const updateTrainStatus = (statName, success, message, gainMessage, turnOnBg) => {
  return {
    type: UPDATE_TRAIN_STATUS,
    statName,
    success,
    message,
    gainMessage,
    turnOnBg
  }
}

export const setPreviewGym = gym => {
  return {
    type: SET_PREVIEW_GYM,
    gym
  }
}

export const disableGymPreview = () => {
  return {
    type: DISABLE_GYM_PREVIEW
  }
}

export const setMessage = message => {
  return {
    type: SET_MESSAGE,
    message
  }
}

export const resetGymDetailsState = () => {
  return {
    type: RESET_GYM_DETAILS_STATE
  }
}

export const setTimeToUpdate = timeToUpdate => {
  return {
    type: SET_TIME_TO_UPDATE,
    timeToUpdate
  }
}

export const toggleConfirmation = isTurnOn => {
  return {
    type: TOGGLE_CONFIRMATION,
    isTurnOn
  }
}

export const setCookie = (name, value, days) => {
  let expires = ''

  if (days) {
    const date = new Date()

    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
    expires = `; expires=${date.toUTCString()}`
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`
}

export const getStatValueFromCookies = statName => {
  try {
    const amountTrainsJSON = getCookie(GYM_TRAINS_COOKIE_NAME)
    const amountTrains = amountTrainsJSON && parseInt(JSON.parse(amountTrainsJSON)[statName], 10)

    return (amountTrains || amountTrains === 0) && amountTrains <= MAX_AMOUNT_OF_TRAINS ? amountTrains : 1
  } catch (e) {
    return 1
  }
}

export const setStatToCookie = (statName, value) => {
  const trainsJSON = getCookie(GYM_TRAINS_COOKIE_NAME)

  let trains = {}

  try {
    trains = JSON.parse(trainsJSON)
  } catch (e) {
    console.error(e)
  }
  trains[statName] = value < 0 ? 0 : value
  setCookie(GYM_TRAINS_COOKIE_NAME, JSON.stringify(trains), YEAR * 10)
}

const ACTION_HANDLERS = {
  [SET_INITIAL_INFORMATION]: (state, action) => {
    const { stats, gyms } = action.initialData

    const activeGym = Object.keys(gyms)
      .map(gymC => gyms[gymC].find(gym => gym.status === 'active'))
      .find(item => item)

    const genStats = {}

    Object.keys(stats).forEach(statName => {
      genStats[statName] = {
        value: stats[statName].value ? stats[statName].value : '0.00',
        percentage: stats[statName].percentage,
        amountTrain: getStatValueFromCookies(statName)
      }
    })

    return {
      ...state,
      gyms: {
        ...gyms
      },
      activeGym,
      stats: {
        ...genStats
      },
      energy: action.initialData.energy,
      trainInfo: {}
    }
  },
  [SET_TIME_TO_UPDATE]: (state, action) => {
    return {
      ...state,
      timeToUpdate: action.timeToUpdate
    }
  },
  [GET_AJAX_ERROR]: state => {
    return {
      ...state
    }
  },
  [SET_AMOUNT_TRAIN]: (state, action) => {
    setStatToCookie(action.statName, action.amountTrain)
    return {
      ...state,
      stats: {
        ...state.stats,
        [action.statName]: {
          ...state.stats[action.statName],
          amountTrain: action.amountTrain
        }
      }
    }
  },
  [UPDATE_STATS_PERCENTAGE]: (state, action) => {
    return {
      ...state,
      stats: {
        ...state.stats,
        strength: {
          ...state.stats.strength,
          percentage: action.statsPercentages.strength
        },
        defense: {
          ...state.stats.defense,
          percentage: action.statsPercentages.defense
        },
        speed: {
          ...state.stats.speed,
          percentage: action.statsPercentages.speed
        },
        dexterity: {
          ...state.stats.dexterity,
          percentage: action.statsPercentages.dexterity
        }
      }
    }
  },
  [INCREASE_AMOUNT_TRAIN]: (state, action) => {
    setStatToCookie(action.statName, state.stats[action.statName].amountTrain + 1)
    return {
      ...state,
      stats: {
        ...state.stats,
        [action.statName]: {
          ...state.stats[action.statName],
          amountTrain:
            state.stats[action.statName].amountTrain + 1 > MAX_AMOUNT_OF_TRAINS ?
              state.stats[action.statName].amountTrain :
              state.stats[action.statName].amountTrain + 1
        }
      }
    }
  },
  [DECREASE_AMOUNT_TRAIN]: (state, action) => {
    setStatToCookie(action.statName, state.stats[action.statName].amountTrain - 1)
    return {
      ...state,
      stats: {
        ...state.stats,
        [action.statName]: {
          ...state.stats[action.statName],
          amountTrain: state.stats[action.statName].amountTrain >= 1 ? state.stats[action.statName].amountTrain - 1 : 0
        }
      }
    }
  },
  [UPDATE_GYMS_INFORMATION]: (state, action) => {
    const updGyms = {}

    Object.keys(state.gyms).forEach(gymClass => {
      updGyms[gymClass] = state.gyms[gymClass].map(gym => {
        let tempGym = gym

        action.updatedGyms.forEach(updatedGym => {
          if (updatedGym.id === gym.id) tempGym = Object.assign({}, gym, updatedGym)
        })
        return tempGym
      })
    })
    return {
      ...state,
      gyms: {
        ...state.gyms,
        ...updGyms
      }
    }
  },
  [UPDATE_ACTIVE_GYM]: state => {
    let activeGym = {}

    Object.keys(state.gyms).forEach(gym => {
      state.gyms[gym].forEach(item => {
        if (item.status === 'active') activeGym = item
      })
    })
    return {
      ...state,
      activeGym
    }
  },
  [UPDATE_ENERGY]: (state, action) => {
    return {
      ...state,
      energy: {
        ...state.energy,
        ...action.energy
      }
    }
  },
  [UPDATE_TRAIN_STATUS]: (state, action) => {
    return {
      ...state,
      trainInfo: {
        prevStat: state.trainInfo ? state.trainInfo.stat : '',
        stat: action.statName,
        success: action.success,
        message: action.message,
        gainMessage: action.success ? action.gainMessage : '',
        turnOnBg: action.turnOnBg
      }
    }
  },
  [UPDATE_STAT]: (state, action) => {
    return {
      ...state,
      stats: {
        ...state.stats,
        [action.statName]: {
          ...state.stats[action.statName],
          value: action.statValue
        }
      }
    }
  },
  [SET_PREVIEW_GYM]: (state, action) => {
    return {
      ...state,
      trainInfo: {},
      viewingGym: {
        isPreview: true,
        confirmation: false,
        gym: action.gym
      }
    }
  },
  [DISABLE_GYM_PREVIEW]: state => {
    return {
      ...state,
      viewingGym: {
        isPreview: false,
        gym: {}
      }
    }
  },
  [SET_MESSAGE]: (state, action) => {
    return {
      ...state,
      viewingGym: {
        ...state.viewingGym,
        message: {
          success: false,
          text: action.message
        }
      }
    }
  },
  [RESET_GYM_DETAILS_STATE]: state => {
    return {
      ...state,
      viewingGym: {
        isPreview: state.viewingGym.isPreview,
        gym: state.viewingGym.gym
      }
    }
  },
  [TOGGLE_CONFIRMATION]: (state, action) => {
    return {
      ...state,
      viewingGym: {
        ...state.viewingGym,
        confirmation: action.isTurnOn
      }
    }
  },
  [SET_DARK_MODE_STATE]: (state, action) => {
    return {
      ...state,
      darkModeEnabled: action.darkModeEnabled
    }
  }
}

export const updateInitialInformation = () => dispatch => {
  return fetchUrl('/gym.php?step=getInitialGymInfo', { step: 'getInitialGymInfo' }).then(
    json => {
      dispatch(setInitialInformation(json))
      if (json.updateTime) {
        dispatch(setTimeToUpdate(json.updateTime))
      } else {
        dispatch(setTimeToUpdate(null))
      }
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const getEnergy = () => dispatch => {
  return fetchUrl('/gym.php?step=getEnergy').then(
    json => {
      dispatch(updateEnergy(json))
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

const animateStat = (currStatValue, nextCurrStatValue, energySpent, statName) => dispatch => {
  clearInterval(timer[statName])
  const floatNextStat = parseFloat(nextCurrStatValue.replace(/\,/g, ''))

  const isInt = `${floatNextStat}`.indexOf('.') === -1

  const tempGainValue =
    (floatNextStat - parseFloat(currStatValue.replace(/\,/g, ''))) /
    (AMOUNT_ANIMATIONS_ITERATIONS + GAIN_PERCENTAGE * energySpent)

  let gain = !isInt ? tempGainValue : parseInt(`${tempGainValue}`, 10)

  gain = gain === 0 ? 1 : gain
  let nextStatValue = getNewStatValue(currStatValue, gain, isInt)

  dispatch(updateStat(statName, nextStatValue))
  timer[statName] = setInterval(() => {
    nextStatValue = getNewStatValue(nextStatValue, gain, isInt)
    dispatch(updateStat(statName, nextStatValue))
    if (parseFloat(nextStatValue.replace(/\,/g, '')) >= floatNextStat) {
      dispatch(updateStat(statName, nextCurrStatValue))
      clearInterval(timer[statName])
    }
  }, ANIMATION_INTERVAL)
}

export const updateInformationAfterTraining = statName => (dispatch, getState) => {
  const repeatsAmount = getState().gym.stats[statName].amountTrain

  return fetchUrl('/gym.php?step=train', { step: 'train', stat: statName, repeats: repeatsAmount }).then(
    // eslint-disable-next-line max-statements
    json => {
      let activeGym = {} as IGymItem

      if (json.gymsStatuses) {
        json.gymsStatuses.forEach(item => {
          if (item.status === 'active') activeGym = item
        })
      }

      if (activeGym.id && activeGym.id !== getState().gym.activeGym.id) {
        dispatch(updateInitialInformation())
      } else {
        if (json.success) {
          const currStatValue = getState().gym.stats[statName].value

          const nextCurrStatValue = json.stat.newValue

          const { energySpent } = json

          if (currStatValue !== nextCurrStatValue) {
            dispatch(animateStat(currStatValue, nextCurrStatValue, energySpent, statName))
          }
          dispatch(updateStatsPercentage(json.percentages))
          dispatch(updateGymsInformation(json.gymsStatuses))
          dispatch(updateActiveGym())
          dispatch(updateEnergy(json.energy))
        }
        dispatch(updateTrainStatus(statName, json.success, json.message, json.gainMessage, true))
        setTimeout(() => {
          dispatch(updateTrainStatus(statName, json.success, json.message, json.gainMessage, false))
        }, HIGHLIGHT_TIMEOUT)
      }
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const viewGym = id => dispatch => {
  return fetchUrl('/gym.php?step=getGymInfo', { step: 'getGymInfo', gymID: id }).then(
    json => {
      dispatch(setPreviewGym(json))
      dispatch(updateGymsInformation([{ id: json.id, status: json.status }]))
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const changeGym = () => (dispatch, getState) => {
  return fetchUrl('/gym.php?step=changeGym', { step: 'changeGym', gymID: getState().gym.viewingGym.gym.id }).then(
    json => {
      if (json.success) {
        dispatch(disableGymPreview())
      } else {
        dispatch(setMessage(json.message))
      }
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const purchaseMembership = () => (dispatch, getState) => {
  return fetchUrl('/gym.php?step=purchaseMembership', {
    step: 'purchaseMembership',
    gymID: getState().gym.viewingGym.gym.id
  }).then(
    json => {
      if (json.success) {
        dispatch(disableGymPreview())
      } else {
        dispatch(setMessage(json.message))
      }
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

// ------------------------------------
// Reducer
// ------------------------------------

const initialState = {}

export default function sidebarReducer(state: any = initialState, action: IAction) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
