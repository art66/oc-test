import React, { Component } from 'react'
import { TOGGLE_DARK_MODE_EVENT } from '@torn/header/src/constants'
import Notification from './Notification'
import GymContent from './GymContent'
import GymDetails from './GymDetails'
import GymList from './GymList'
import IMessage from '../models/Message'
import IGymItem from '../models/GymItem'
import ITornWindow from '../models/TornWindow'
import IEnergy from '../models/Energy'
import { checkDarkMode } from '../utils'
import '../styles/vars.scss'
import '../styles/DM_vars.scss'
import s from '../styles/Gym.cssmodule.scss'

interface IProps {
  updateInitialInformation: () => void
  getEnergy: () => void
  updateEnergy: (energy: IEnergy) => void
  setDarkModeState: (enabled: boolean) => void
  gym: {
    energy: IEnergy
    gyms: {
      jail: IGymItem[]
      lightweight: IGymItem[]
      middleweight: IGymItem[]
      heavyweight: IGymItem[]
      specialist: IGymItem[]
    }
    viewingGym: {
      message: IMessage
      confirmation: boolean
      isPreview: boolean
      gym: IGymItem
    }
  }
}

class Gym extends Component<IProps> {
  componentDidMount() {
    this.subscribeForGymChanges()
    this.updateDarkModeState()
    window.addEventListener(TOGGLE_DARK_MODE_EVENT, this.updateDarkModeState)
    window.addEventListener('syncRecoveringEnergyWithSidebar', this.recoverEnergy)
  }

  componentWillUnmount() {
    window.removeEventListener(TOGGLE_DARK_MODE_EVENT, this.updateDarkModeState)
    window.removeEventListener('syncRecoveringEnergyWithSidebar', this.recoverEnergy)
  }

  updateDarkModeState = () => {
    const { setDarkModeState } = this.props
    const darkModeEnabled = checkDarkMode()

    setDarkModeState(darkModeEnabled)
  }

  recoverEnergy = () => {
    const {
      gym: { energy },
      updateEnergy
    } = this.props

    updateEnergy({ available: energy.available + energy.step, max: energy.max })
  }

  reloadPage = () => {
    window.location.href = window.location.pathname
  }

  updateEnergyInfo = data => {
    const { updateEnergy } = this.props

    updateEnergy({ available: data.amount, max: data.max, step: data.step })
  }

  subscribeForGymChanges = () => {
    const { updateInitialInformation } = this.props
    const win = window as ITornWindow

    win.WebsocketHandler.addEventListener('sidebar', 'onJail', updateInitialInformation)
    win.WebsocketHandler.addEventListener('sidebar', 'updateEnergy', data => this.updateEnergyInfo(data))
    win.WebsocketHandler.addEventListener('sidebar', 'onLeaveFromJail', updateInitialInformation)
    win.WebsocketHandler.addEventListener('sidebar', 'onRevive', this.reloadPage)
  }

  renderGymList = () => {
    const { gym } = this.props
    const inJail = gym.gyms && gym.gyms.jail[0].status === 'active'

    return !inJail ? (
      <div>
        <hr className='page-head-delimiter' />
        <GymList />
      </div>
    ) : null
  }

  render() {
    const { gym } = this.props

    return (
      <div className={s.gym}>
        <Notification />
        <hr className='page-head-delimiter' />
        {gym.viewingGym && gym.viewingGym.isPreview ? <GymDetails /> : <GymContent />}
        {this.renderGymList()}
      </div>
    )
  }
}

export default Gym
