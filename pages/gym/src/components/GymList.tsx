import React, { Component } from 'react'
import { connect } from 'react-redux'
import GymButton from './GymButton'
import IGymItem from '../models/GymItem'
import s from '../styles/GymList.cssmodule.scss'

interface IProps {
  gym: {
    gyms: {
      lightweight: IGymItem[]
      middleweight: IGymItem[]
      heavyweight: IGymItem[]
      specialist: IGymItem[]
    }
  }
}

class GymList extends Component<IProps> {
  renderGymList = (gymClass) => {
    const { gym } = this.props

    return gym.gyms[gymClass].map(item => (
      <GymButton
        item={item}
        gymClass={gymClass}
        key={item.id}
      />
    ))
  }

  renderGymCategory = () => {
    const { gym } = this.props

    return Object.keys(gym.gyms)
      .map(gymClass => {
        if (gymClass !== 'jail') {
          return (
            <div key={gymClass} className={s[gymClass]}>
              {this.renderGymList(gymClass)}
            </div>
          )
        }
      })
  }

  renderGymCategoryList = () => {
    const { gym } = this.props

    return gym.gyms ? (
      <div className={s.gymList}>
        {this.renderGymCategory()}
      </div>
    ) : (
      ''
    )
  }

  render() {
    return (
      <div className={s.gymListWrapper}>
        {this.renderGymCategoryList()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  gym: state.gym
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(GymList)
