import React, { Component } from 'react'
import { connect } from 'react-redux'
import { purchaseMembership, resetGymDetailsState, toggleConfirmation } from '../modules'
import GymDetailsActions from './GymDetailsActions'
import IMessage from '../models/Message'
import IGymItem from '../models/GymItem'
import s from '../styles/GymDetails.cssmodule.scss'

interface IProps {
  purchaseMembership: () => void
  resetGymDetailsState: () => void
  toggleConfirmation: (value: boolean) => void
  gym: {
    viewingGym: {
      message: IMessage
      confirmation: boolean
      gym: IGymItem
    }
  }
}

class GymDetailsActionsBlock extends Component<IProps> {
  getDescription = viewedGym => {
    if (viewedGym.description && viewedGym.description.length) {
      return (
        <div className={s.descriptionWrapper}>
          <p className={s.description}>{viewedGym.description}</p>
        </div>
      )
    }
  }

  render() {
    const { gym } = this.props
    const viewedGym = gym.viewingGym.gym

    if (gym.viewingGym.message && !gym.viewingGym.message.success) {
      return (
        <div className={s.message}>
          <p role='alert' className={s.text}>
            {gym.viewingGym.message.text}
          </p>
          <button
            type='button'
            className={s.okButton}
            onClick={this.props.resetGymDetailsState}
          >
            Okay
          </button>
        </div>
      )
    }
    if (gym.viewingGym.confirmation) {
      return (
        <div className={s.confirmMessage}>
          <p role='alert' className={s.text}>
            Are you sure you would like to buy this membership?
          </p>
          <button
            type='button'
            aria-label='Press if you want buy membership'
            className={s.yesButton}
            onClick={this.props.purchaseMembership}
          >
            Yes
          </button>
          <button
            type='button'
            aria-label="Press if you don't want buy membership"
            className={s.noButton}
            onClick={() => this.props.toggleConfirmation(false)}
          >
            No
          </button>
        </div>
      )
    }
    return (
      <div className={s.action}>
        {this.getDescription(viewedGym)}
        <GymDetailsActions gym={viewedGym} />
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    gym: state.gym
  }),
  {
    purchaseMembership,
    resetGymDetailsState,
    toggleConfirmation
  }
)(GymDetailsActionsBlock)
