import React, { Component } from 'react'
import classnames from 'classnames'
import { connect } from 'react-redux'
import { THE_EDGE_GYM_ID } from '../constants'
import IEnergy from '../models/Energy'
import s from '../styles/Notification.cssmodule.scss'

interface IProps {
  browser: {
    lessThan: {
      desktop: boolean
    }
  }
  gym: {
    energy: IEnergy
    darkModeEnabled: boolean
    activeGym: {
      id: number
      name: string
      energyCost: number
    }
  }
}

class Notification extends Component<IProps> {
  renderNotification = () => {
    const { gym, browser } = this.props
    const notDesktop = browser.lessThan.desktop
    const c = classnames({
      [s.notification]: true,
      [s.gym23]: gym.activeGym && gym.activeGym.id === THE_EDGE_GYM_ID
    })
    const energyClass = classnames({
      [s.energy]: true,
      [s.notEnoughEnergy]: gym.activeGym && gym.activeGym.energyCost > gym.energy.available
    })
    const pathToLogos = gym.darkModeEnabled ? 'dark-mode/' : ''

    return gym.energy ? (
      <div className={c}>
        <div className={s.notificationText}>
          <p>
            {!notDesktop ? 'Welcome to ' : ''}
            <b>{gym.activeGym.name}</b>
            {!notDesktop ? '! You walk into the gym and begin browsing the exercises available.' : ''}
          </p>
          <p>
            You have{' '}
            <span className={energyClass}>
              {gym.energy.available}/{gym.energy.max}
            </span>{' '}
            energy
            {!notDesktop ? ' available to use.' : ''}
          </p>
        </div>
        <img
          className={s.logo}
          src={`/images/v2/gym-rebuild/logos/${pathToLogos}${gym.activeGym.id}.png`}
          alt={gym.activeGym.name}
        />
        <div className={s.logoWrapper}>
          <div className={s.innerLogoWrapper} />
        </div>
      </div>
    ) : (
      ''
    )
  }

  render() {
    return <div className={s.notificationWrapper}>{this.renderNotification()}</div>
  }
}

const mapStateToProps = state => ({
  browser: state.browser,
  gym: state.gym
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Notification)
