import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/ProgressBar.cssmodule.scss'
import classnames from 'classnames'

class ProgressBar extends Component {
  render() {
    const { statName, exercise, available, color, percentage } = this.props
    const c = classnames({
      [s.progressBar]: true,
      [s.notAvailable]: !available
    })
    const cLine = classnames({
      [s.line]: true,
      [s[color]]: true
    })
    let waiMsg = !available
      ? `${statName} is not available in this gym.`
      : `${statName} Gains ${percentage / 10} out of 10`
    return (
      <div className={c} tabIndex={0} aria-label={waiMsg}>
        <div className={s.barDescription}>
          <span className={s.statName}>
            <b>{statName} Gains</b>
          </span>
          <span className={s.exerciseName}>{exercise}</span>
        </div>
        <div className={s.progressLineWrapper}>
          <div className={s.progressLine}>
            <div className={cLine} style={{ width: `${percentage}%` }} />
          </div>
        </div>
      </div>
    )
  }
}

ProgressBar.propTypes = {
  statName: PropTypes.string,
  exercise: PropTypes.string,
  available: PropTypes.bool,
  color: PropTypes.string,
  percentage: PropTypes.number
}

export default ProgressBar
