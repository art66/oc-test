import React, { Component } from 'react'
import { connect } from 'react-redux'
import ProgressBar from './ProgressBar'
import GymDetailsActionsBlock from './GymDetailsActionsBlock'
import GymStats from './GymStats'
import { disableGymPreview } from '../modules'
import IMessage from '../models/Message'
import IGymItem from '../models/GymItem'
import s from '../styles/GymDetails.cssmodule.scss'

interface IProps {
  disableGymPreview: () => void
  gym: {
    viewingGym: {
      message: IMessage
      confirmation: boolean
      gym: IGymItem
    }
  }
}

class GymDetails extends Component<IProps> {
  getDescription = viewedGym => {
    if (viewedGym.description && viewedGym.description.length) {
      return (
        <div className={s.descriptionTooltip}>
          <p className={s.description}>{viewedGym.description}</p>
        </div>
      )
    }
  }

  render() {
    const { gym } = this.props
    const viewedGym = gym.viewingGym.gym

    return (
      <div className={s.gymDetailsWrapper}>
        {this.getDescription(viewedGym)}
        <div className={s.gymDetails}>
          <div className={s.gymTitle}>
            <h3>{viewedGym.name}</h3>
            <button
              className={s.closeButton}
              aria-label='Back to gym'
              onClick={this.props.disableGymPreview}
            />
          </div>
          <ul className={s.gymInfo}>
            <li>
              <GymStats viewedGym={viewedGym} />
            </li>
            <li>
              <ProgressBar
                statName='Strength'
                exercise={viewedGym.strength.exercise}
                available={viewedGym.strength.available}
                color={viewedGym.strength.color}
                percentage={viewedGym.strength.modifier}
              />
              <ProgressBar
                statName='Speed'
                exercise={viewedGym.speed.exercise}
                available={viewedGym.speed.available}
                color={viewedGym.speed.color}
                percentage={viewedGym.speed.modifier}
              />
            </li>
            <li>
              <ProgressBar
                statName='Defense'
                exercise={viewedGym.defense.exercise}
                available={viewedGym.defense.available}
                color={viewedGym.defense.color}
                percentage={viewedGym.defense.modifier}
              />
              <ProgressBar
                statName='Dexterity'
                exercise={viewedGym.dexterity.exercise}
                available={viewedGym.dexterity.available}
                color={viewedGym.dexterity.color}
                percentage={viewedGym.dexterity.modifier}
              />
            </li>
          </ul>
          <div className={s.actionWrapper}>
            <GymDetailsActionsBlock />
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    gym: state.gym
  }),
  {
    disableGymPreview
  }
)(GymDetails)
