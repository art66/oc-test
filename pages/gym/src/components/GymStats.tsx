import React, { Component } from 'react'
import IGymItem from '../models/GymItem'
import s from '../styles/GymDetails.cssmodule.scss'

interface IProps {
  viewedGym: IGymItem
}

class GymStats extends Component<IProps> {
  render() {
    const { viewedGym } = this.props

    return (
      <div className={s.gymStats}>
        <div className={s.propertyWrapper}>
          <span className={s.property}>
            <b>Class: </b>
          </span>
          <span className={s.value}>{viewedGym.class}</span>
        </div>
        <div className={s.propertyWrapper}>
          <span className={s.property}>
            <b>Membership cost: </b>
          </span>
          <span className={s.value}>${viewedGym.membershipCost}</span>
        </div>
        <div className={s.propertyWrapper}>
          <span className={s.property}>
            <b>Energy usage: </b>
          </span>
          <span className={s.value}>{viewedGym.energyCost} per train</span>
        </div>
      </div>
    )
  }
}

export default GymStats
