import React, { Component } from 'react'
import { decreaseAmountTrain, increaseAmountTrain, setAmountTrain, updateInformationAfterTraining } from '../modules'
import { connect } from 'react-redux'
import classnames from 'classnames'
import IStatValue from '../models/StatValue'
import IEnergy from '../models/Energy'
import s from '../styles/Properties.cssmodule.scss'

const MAX_NUMBER_OF_TRAININGS_LENGTH = 3

interface IProps {
  statName: string
  isLocked: boolean
  decreaseAmountTrain: (statName: string) => void
  increaseAmountTrain: (statName: string) => void
  setAmountTrain: (statName: string, amountTrain: number) => void
  updateInformationAfterTraining: (statName: string) => void
  gym: {
    stats: {
      strength: IStatValue
      defense: IStatValue
      speed: IStatValue
      dexterity: IStatValue
    }
    energy: IEnergy
  }
}

class TrainBlock extends Component<IProps> {
  declare refs: {
    textInput: HTMLInputElement
  }

  handleChange = () => {
    const { statName, setAmountTrain: setTrains } = this.props
    const inputField = this.refs[statName]
    let { value } = inputField

    if (value.length > MAX_NUMBER_OF_TRAININGS_LENGTH) {
      value = value.slice(0, -1)
    }
    const amountTrain = value.replace(/(?![0-9])./gim, '')

    setTrains(statName, +amountTrain)
  }

  trainStat = () => {
    const { statName, gym: { stats, energy }, updateInformationAfterTraining: updateInfo } = this.props

    if (stats[statName].amountTrain !== 0 && energy.available !== 0) {
      updateInfo(statName)
    }
  }

  render() {
    const { gym, statName, isLocked } = this.props
    const c = classnames({
      [s.button]: true,
      [s.disabled]: gym.stats[statName].amountTrain === 0 || gym.energy.available === 0 || isLocked
    })

    return (
      <div>
        <div className={s.inputWrapper}>
          <button
            className={`${s.inputButton} ${s.left}`}
            onClick={() => this.props.decreaseAmountTrain(statName)}
            aria-label={`Decrease the number of ${statName} training by one`}
          />
          <input
            className={s.input}
            type='text'
            value={gym.stats[statName].amountTrain}
            onChange={this.handleChange}
            ref={statName}
            aria-label={`Enter the number of ${statName} training`}
          />
          <button
            className={`${s.inputButton} ${s.right}`}
            onClick={() => this.props.increaseAmountTrain(statName)}
            aria-label={`Increase the number of ${statName} training by one`}
          />
        </div>
        <button className={c} onClick={this.trainStat} aria-label={`Train ${statName}`}>
          TRAIN
        </button>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  gym: state.gym
})

const mapDispatchToProps = {
  setAmountTrain,
  decreaseAmountTrain,
  increaseAmountTrain,
  updateInformationAfterTraining
}

export default connect(mapStateToProps, mapDispatchToProps)(TrainBlock)
