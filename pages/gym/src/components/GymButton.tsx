import React, { Component } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import Tooltip from './Tooltip'
import { viewGym, disableGymPreview } from '../modules'
import IMessage from '../models/Message'
import IGymItem from '../models/GymItem'
import s from '../styles/GymList.cssmodule.scss'

interface IProps {
  viewGym: (value: number) => void
  disableGymPreview: () => void
  gymClass: string
  gym: {
    viewingGym: {
      message: IMessage
      confirmation: boolean
      gym: IGymItem
    }
  }
  item: IGymItem
}

class GymButton extends Component<IProps> {
  getTooltip = (item, gymClass) => {
    if (item.status !== 'inProgress' && item.status !== 'locked') {
      const capitalizeGymClass = gymClass.charAt(0)
        .toUpperCase() + gymClass.slice(1)

      return (
        <Tooltip
          position='bottom'
          arrow='center'
          parent={`gym-${item.id}`}
        >
          <h5 className={s.gymName}>
            {item.name} <span className={s.gymClass}>({capitalizeGymClass})</span>
          </h5>
          <div className={s.gymInfo}>
            <p>Membership cost - ${item.membershipCost}</p>
            <p>Energy usage - {item.energyCost} per train</p>
          </div>
          <div className={`${s.bottomLine} ${s[gymClass]}`} />
        </Tooltip>
      )
    }

    return ''
  }

  handleGymAction = item => {
    const { gym } = this.props

    if (gym.viewingGym && item.id === gym.viewingGym.gym.id) {
      this.props.disableGymPreview()
    } else {
      this.viewGym(item)
    }
  }

  viewGym = item => {
    if (item.status !== 'locked' && item.status !== 'inProgress' && item.status !== 'lockedPurchased') {
      this.props.viewGym(item.id)
    }
  }

  renderProgressBar = () => {
    const { item } = this.props

    return item.status === 'inProgress' ? (
      <div>
        <div className={s.percentage}>{item.percentage}%</div>
        <div className={s.progressBar}>
          <div className={s.inner} style={{ width: `${item.percentage}%` }} />
        </div>
      </div>
    ) : null
  }

  render() {
    const { item, gymClass, gym } = this.props
    // TODO: move gym statuses to constants
    const c = classnames({
      [s.gymButton]: true,
      [s.lockedPurchased]: item.status === 'lockedPurchased',
      [s.locked]: item.status === 'locked',
      [s.inProgress]: item.status === 'inProgress',
      [s.active]: item.status === 'active',
      [s.new]: item.status === 'new',
      [s.newSeen]: item.status === 'newSeen',
      [s.selected]: gym.viewingGym && gym.viewingGym && item.id === gym.viewingGym.gym.id
    })
    const percentageLeft = item.status === 'inProgress' ?
      `Unlock progress - ${item.percentage} percent.` : ''
    const srGymDescription = `${item.name}. Membership cost - $${item.membershipCost}. Energy usage -
     ${item.energyCost} per train. ${percentageLeft}`

    return (
      <button
        type='button'
        id={`gym-${item.id}`}
        className={c}
        onClick={() => this.handleGymAction(item)}
        aria-label={srGymDescription}
      >
        <div className={`${s.gymIcon} ${s[`gym-${item.id}`]}`}>
          {this.renderProgressBar()}
        </div>
        {this.getTooltip(item, gymClass)}
      </button>
    )
  }
}

export default connect(
  (state: any) => ({
    gym: state.gym
  }),
  {
    disableGymPreview,
    viewGym
  }
)(GymButton)
