import React, { Component } from 'react'
import { connect } from 'react-redux'
import { disableGymPreview, changeGym, toggleConfirmation } from '../modules'

import s from '../styles/GymDetails.cssmodule.scss'

interface IGym {
  status: string
}

interface IProps {
  disableGymPreview: () => void
  changeGym: () => void
  toggleConfirmation: (value: boolean) => void
  gym: IGym
}

class GymDetailsActions extends Component<IProps> {
  render() {
    const { gym } = this.props

    if (gym.status === 'available') {
      return (
        <div className={s.btnWrapper}>
          <button className={s.cancel} onClick={this.props.disableGymPreview}>
            Cancel
          </button>
          <button className={s.button} onClick={this.props.changeGym}>
            ACTIVATE MEMBERSHIP
          </button>
        </div>
      )
    }
    if (gym.status === 'active') {
      return (
        <div className={s.btnWrapper}>
          <button className={s.button} onClick={this.props.disableGymPreview}>
            BACK TO GYM
          </button>
        </div>
      )
    }
    return (
      <div className={s.btnWrapper}>
        <button className={s.cancel} onClick={this.props.disableGymPreview}>
          Cancel
        </button>
        <button className={s.button} onClick={() => this.props.toggleConfirmation(true)}>
          BUY MEMBERSHIP
        </button>
      </div>
    )
  }
}

export default connect(
  () => ({}),
  {
    disableGymPreview,
    changeGym,
    toggleConfirmation
  }
)(GymDetailsActions)
