import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import s from '../styles/GymContent.cssmodule.scss'
import Properties from './Properties'
import { updateInitialInformation } from '../modules'
import { connect } from 'react-redux'

const FONT_SIZE_14 = 14
const FONT_SIZE_13 = 13
const FONT_SIZE_12 = 12
const FONT_SIZE_11 = 11
const FONT_SIZE_10 = 10
let timeout

class GymContent extends Component {
  componentDidMount() {
    this.props.updateInitialInformation()
  }

  componentDidUpdate(prevProps) {
    let timeToUpdate = this.props.gym.timeToUpdate
    let props = this.props
    if (!prevProps.gym.timeToUpdate && timeToUpdate) {
      clearTimeout(timeout)
      timeout = setTimeout(() => {
        props.updateInitialInformation()
      }, timeToUpdate)
    }
  }

  calculateFontSize = () => {
    let stats = this.props.gym.stats
    let statsValues = [stats.strength.value, stats.defense.value, stats.speed.value, stats.dexterity.value]
    let isFont10 = statsValues.some(item => item.length > 16)
    let isFont11 = statsValues.some(item => item.length > 14)
    let isFont12 = statsValues.some(item => item.length > 13)
    let isFont13 = statsValues.some(item => item.length > 11)
    if (isFont10) {
      return FONT_SIZE_10
    } else if (isFont11) {
      return FONT_SIZE_11
    } else if (isFont12) {
      return FONT_SIZE_12
    } else if (isFont13) {
      return FONT_SIZE_13
    } else {
      return FONT_SIZE_14
    }
  }

  render() {
    const { gym } = this.props
    const c = classnames({
      [s.message]: true,
      [s.success]: gym.trainInfo && gym.trainInfo.success,
      [s.error]: gym.trainInfo && gym.trainInfo.success === false,
      [s.default]: gym.trainInfo && gym.trainInfo.turnOnBg
    })
    let isDefault = gym.trainInfo && !gym.trainInfo.hasOwnProperty('success')
    let fontSize = FONT_SIZE_12
    if (gym.stats) {
      fontSize = this.calculateFontSize()
    }
    return (
      <div className={s.gymContentWrapper}>
        {gym.activeGym ? (
          <div className={s.gymContent}>
            <Properties fontSize={fontSize} />
            <div className={c}>
              {isDefault ? (
                <div className={s.messageWrapper}>
                  <p>What would you like to train today?</p>
                </div>
              ) : (
                <div className={s.messageWrapper}>
                  {gym.trainInfo.success ? (
                    <div>
                      <p>{gym.trainInfo.message}</p>
                      <p role={'alert'} className={s.gained}>
                        {gym.trainInfo.gainMessage}
                      </p>
                    </div>
                  ) : (
                    <div>
                      <p>{gym.trainInfo.message}</p>
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        ) : (
          ''
        )}
      </div>
    )
  }
}

GymContent.propTypes = {
  browser: PropTypes.object,
  updateInitialInformation: PropTypes.func,
  gym: PropTypes.object
}

const mapStateToProps = state => ({
  browser: state.browser,
  gym: state.gym
})

const mapDispatchToProps = {
  updateInitialInformation
}

export default connect(mapStateToProps, mapDispatchToProps)(GymContent)
