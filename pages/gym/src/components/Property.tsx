import React, { Component } from 'react'
import classnames from 'classnames'
import { connect } from 'react-redux'
import TrainBlock from './TrainBlock'
import Tooltip from './Tooltip'
import IGymItem from '../models/GymItem'
import ITrainInfo from '../models/TrainInfo'
import IExercise from '../models/Exercise'
import IStatValue from '../models/StatValue'
import { STAT_LENGTH_BEFORE_SHORT_STAT_NAME } from '../constants'
import s from '../styles/Properties.cssmodule.scss'

const propertyNames = {
  strength: {
    default: 'Strength',
    short: 'STR'
  },
  defense: {
    default: 'Defense',
    short: 'DEF'
  },
  speed: {
    default: 'Speed',
    short: 'SPD'
  },
  dexterity: {
    default: 'Dexterity',
    short: 'DEX'
  }
}

const propertyDescriptions = {
  strength: {
    default: 'Damage you make on impact',
    short: 'Damage on impact'
  },
  defense: {
    default: 'Ability to withstand damage',
    short: 'Withstand damage'
  },
  speed: {
    default: 'Chance of hitting opponent',
    short: 'Chance of hitting'
  },
  dexterity: {
    default: 'Ability to evade an attack',
    short: 'Ability to evade an attack'
  }
}

interface IProps {
  propertyName: string
  fontSize: number
  gym: {
    trainInfo: ITrainInfo
    gyms: {
      jail: IGymItem[]
      lightweight: IGymItem[]
      middleweight: IGymItem[]
      heavyweight: IGymItem[]
      specialist: IGymItem[]
    }
    stats: {
      strength: IStatValue
      defense: IStatValue
      speed: IStatValue
      dexterity: IStatValue
    }
    activeGym: {
      energyCost: number
      exercises: {
        strength: IExercise
        defense: IExercise
        speed: IExercise
        dexterity: IExercise
      }
    }
  }
  browser: {
    lessThan: {
      tablet: boolean
      desktop: boolean
    }
  }
}

class Property extends Component<IProps> {
  renderPropertyName = (isMobile) => {
    const { propertyName, gym } = this.props
    const statsValues = [
      gym.stats.strength.value,
      gym.stats.defense.value,
      gym.stats.speed.value,
      gym.stats.dexterity.value
    ]
    const isShort = statsValues.some(item => item.length > STAT_LENGTH_BEFORE_SHORT_STAT_NAME)

    return isMobile && isShort ? (
      <h3 className={s.title}>{propertyNames[propertyName].short}</h3>
    ) : (
      <h3 className={s.title}>{propertyNames[propertyName].default}</h3>
    )
  }

  render() {
    const { propertyName, gym, browser, fontSize } = this.props
    const isMobile = browser.lessThan.tablet
    const isLocked = !gym.activeGym.exercises[propertyName].available
    const cProp = classnames({
      [s[propertyName]]: true,
      [s.success]: gym.trainInfo.success && gym.trainInfo.stat === propertyName,
      [s.error]: gym.trainInfo.success === false && gym.trainInfo.stat === propertyName,
      [s.default]: gym.trainInfo && gym.trainInfo.turnOnBg,
      [s.locked]: isLocked,
      [s.jail]: gym.gyms.jail[0].status === 'active'
    })
    const cPropTitle = classnames({
      [s.propertyTitle]: true,
      [s[`fontSize${fontSize}`]]: true
    })

    return (
      <li className={cProp}>
        <div className={cPropTitle}>
          {this.renderPropertyName(isMobile)}
          <span id={`${propertyName}-val`} className={s.propertyValue}>
            {gym.stats[propertyName].value}
            <Tooltip
              position={browser.lessThan.desktop ? 'bottom' : 'top'}
              arrow='center'
              parent={`${propertyName}-val`}
            >
              <span>{gym.stats[propertyName].percentage} of total</span>
            </Tooltip>
          </span>
        </div>
        <div className={s.propertyContent}>
          <div className={s.description}>
            <p>{!isMobile ? propertyDescriptions[propertyName].default : propertyDescriptions[propertyName].short}</p>
            {isLocked ? <p>Unavailable</p> : <p>{gym.activeGym.energyCost} energy per train</p>}
          </div>
          <TrainBlock statName={propertyName} isLocked={isLocked} />
        </div>
      </li>
    )
  }
}

const mapStateToProps = state => ({
  browser: state.browser,
  gym: state.gym
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Property)
