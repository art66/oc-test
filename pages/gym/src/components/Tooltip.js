import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ToolTip from 'react-portal-tooltip'
import { connect } from 'react-redux'
import { setDarkModeState } from '../modules'

class Tooltip extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isTooltipActive: false
    }
  }

  componentDidMount() {
    this.parent = document.getElementById(this.props.parent)
    if (!this.parent) {
      return
    }

    window.addEventListener('click', this.hideContextMenuOnContentClick)
    this.parent.addEventListener('blur', this.hideTooltip)
    this.parent.addEventListener('focus', this.showTooltip)
    this.parent.addEventListener('touchstart', this.showTooltip)
    this.parent.addEventListener('mouseleave', this.hideTooltip)
    this.parent.addEventListener('mouseenter', this.showTooltip)
    this.parent.addEventListener('touchstart', this.showTooltip)
  }

  componentWillUnmount() {
    if (!this.parent) {
      return
    }

    window.removeEventListener('click', this.hideContextMenuOnContentClick)
    this.parent.removeEventListener('blur', this.hideTooltip)
    this.parent.removeEventListener('focus', this.showTooltip)
    this.parent.removeEventListener('touchstart', this.showTooltip)
    this.parent.removeEventListener('mouseleave', this.hideTooltip)
    this.parent.removeEventListener('mouseenter', this.showTooltip)
    this.parent.removeEventListener('touchstart', this.toggleTooltip)
  }

  hideContextMenuOnContentClick = e => {
    if (e && e.target && e.target.closest('#gymroot') && e.target.closest('#gymroot').length === 0) {
      this.hideTooltip()
    }
  }

  showTooltip = () => {
    this.setState({ isTooltipActive: true })
  }

  hideTooltip = () => {
    this.setState({ isTooltipActive: false })
  }

  toggleTooltip = () => {
    const { isTooltipActive } = this.state

    this.setState({ isTooltipActive: !isTooltipActive })
  }

  render() {
    const { children, position, arrow, parent, darkModeEnabled } = this.props
    const tooltipBg = darkModeEnabled ? '#444' : '#f2f2f2'
    const tooltipShadow = darkModeEnabled ? '0px 0px 2px rgba(0, 0, 0, 0.65)' : '0px 0px 8px rgba(0, 0, 0, 0.3)'
    const arrowBorder = darkModeEnabled ? 'rgba(0, 0, 0, 0.2)' : '#dadada'
    const style = {
      style: {
        background: tooltipBg,
        padding: '5px 8px',
        boxShadow: tooltipShadow
      },
      arrowStyle: {
        color: tooltipBg,
        borderColor: arrowBorder,
        transition: 'none'
      }
    }
    const speed = 0

    return (
      <ToolTip
        active={this.state.isTooltipActive}
        position={position}
        arrow={arrow}
        parent={`#${parent}`}
        style={style}
        tooltipTimeout={speed}
      >
        <div>{children}</div>
      </ToolTip>
    )
  }
}

Tooltip.propTypes = {
  children: PropTypes.node,
  position: PropTypes.string,
  arrow: PropTypes.string,
  parent: PropTypes.string,
  windowSize: PropTypes.string,
  mediaType: PropTypes.string,
  isMobileMode: PropTypes.bool,
  iconTip: PropTypes.string,
  setDarkModeState: PropTypes.func,
  darkModeEnabled: PropTypes.bool
}

export default connect(
  state => ({
    darkModeEnabled: state.gym.darkModeEnabled
  }),
  { setDarkModeState }
)(Tooltip)
