import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Properties.cssmodule.scss'
import { DEFENSE_STAT_NAME, DEXTERITY_STAT_NAME, SPEED_STAT_NAME, STRENGTH_STAT_NAME } from '../constants';
import Property from './Property';

class Properties extends Component {
  render() {
    const { fontSize } = this.props
    return (
      <ul className={s.properties}>
        <Property propertyName={STRENGTH_STAT_NAME} fontSize={fontSize} />
        <Property propertyName={DEFENSE_STAT_NAME} fontSize={fontSize} />
        <Property propertyName={SPEED_STAT_NAME} fontSize={fontSize} />
        <Property propertyName={DEXTERITY_STAT_NAME} fontSize={fontSize} />
      </ul>
    )
  }
}

Properties.propTypes = {
  fontSize: PropTypes.number
}

export default Properties
