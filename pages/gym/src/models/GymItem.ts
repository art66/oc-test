import IExercise from './Exercise'

export default interface IGymItem {
  id: number
  status: string
  name: string
  membershipCost: string
  description: string
  energyCost: number
  percentage?: number
  class?: string
  strength?: IExercise
  defense?: IExercise
  speed?: IExercise
  dexterity?: IExercise
}
