export default interface ITrainInfo {
  success: boolean
  stat: string
  message: string
  gainMessage: string
  turnOnBg: boolean
}
