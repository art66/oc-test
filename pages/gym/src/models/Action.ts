export default interface IAction<TPayload = any> {
  type: string
  payload: TPayload
}
