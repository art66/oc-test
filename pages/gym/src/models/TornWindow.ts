export default interface ITornWindow extends Window {
  WebsocketHandler?: {
    addEventListener: (namespace: string, action: string, callback: (data?: any) => any) => any
  }
}
