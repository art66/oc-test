export default interface IExercise {
  available: boolean
  exercise: string
  color?: string
  modifier?: number
}
