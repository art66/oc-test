export default interface IEnergy {
  available: number
  max: number
  step?: number
}
