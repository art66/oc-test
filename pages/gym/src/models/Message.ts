export default interface IMessage {
  text: string
  success: boolean
}
