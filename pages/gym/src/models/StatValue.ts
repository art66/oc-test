export default interface IStatValue {
  value: string
  percentage: string
  amountTrain: number
}
