import { connect } from 'react-redux'
import {
  updateInitialInformation,
  setAmountTrain,
  decreaseAmountTrain,
  increaseAmountTrain,
  updateInformationAfterTraining,
  viewGym,
  disableGymPreview,
  changeGym,
  purchaseMembership,
  resetGymDetailsState,
  toggleConfirmation,
  updateEnergy,
  getEnergy,
  setDarkModeState
} from '../modules'

import Gym from '../components/Gym'

const mapStateToProps = state => ({
  browser: state.browser,
  gym: state.gym
})

const mapDispatchToProps = {
  updateInitialInformation,
  setAmountTrain,
  decreaseAmountTrain,
  increaseAmountTrain,
  updateInformationAfterTraining,
  viewGym,
  disableGymPreview,
  changeGym,
  purchaseMembership,
  resetGymDetailsState,
  toggleConfirmation,
  updateEnergy,
  getEnergy,
  setDarkModeState
}

export default connect(mapStateToProps, mapDispatchToProps)(Gym)
