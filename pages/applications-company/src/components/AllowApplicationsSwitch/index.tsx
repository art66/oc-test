import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import RadioButton from '../../../../applications-faction/src/components/RadioButton'
import s from './index.cssmodule.scss'
import { IReduxState } from '../../interfaces'
import { APPLICATIONS_ALLOWED_LABEL } from '../../constants/messages'
import { updateApplicationsAllowance as updateApplicationsAllowanceAction } from '../../actions'

interface IProps {
  applicationAllowed: boolean
  userPermissions: { manageApplications: boolean }
  updateApplicationsAllowance: (applicationsAllowed: boolean) => void
}

class AllowApplicationsSwitch extends PureComponent<IProps> {
  _handleAllowApplicationChange = () => {
    const { applicationAllowed, updateApplicationsAllowance } = this.props

    updateApplicationsAllowance(!applicationAllowed)
  }

  render() {
    const { applicationAllowed, userPermissions } = this.props

    return (
      <div className={s.switchWrapper}>
        <RadioButton
          id='applications-state'
          label={APPLICATIONS_ALLOWED_LABEL}
          checked={applicationAllowed}
          disabled={!userPermissions.manageApplications}
          customStyles={{ wrapper: s.radioWrapper, label: s.radioLabel }}
          onChange={this._handleAllowApplicationChange}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    applicationAllowed: state.data.company.applicationAllowed,
    userPermissions: state.data.currentUser.permissions
  }
}

const mapActionsToProps = {
  updateApplicationsAllowance: updateApplicationsAllowanceAction
}

export default connect(mapStateToProps, mapActionsToProps)(AllowApplicationsSwitch)
