import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import DropdownContent from '@torn/applications-faction/src/components/ApplicationsList/Row/DropdownContent'
import { Countdown, Message, Person, View } from '@torn/applications-faction/src/components/ApplicationsList/Row/Cells'
import tableStyles from '../index.cssmodule.scss'
import { IApplication, ICurrentUser, TApplicationsStateObject, IReduxState } from '../../../interfaces'
import {
  acceptApplication as acceptApplicationAction,
  declineApplication as declineApplicationAction,
  removeApplication as removeApplicationAction
} from '../../../actions'

interface IProps {
  application: IApplication
  applicationsState: TApplicationsStateObject
  allowedUsersAmount: number
  currentUser: ICurrentUser
  acceptApplication: (id: number) => void
  declineApplication: (id: number) => void
  removeApplication: (id: number) => void
}

interface IState {
  expanded: boolean
}

class Row extends PureComponent<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      expanded: false
    }
  }

  componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>): void {
    if (!prevProps.applicationsState[prevProps.application.id] && this.getApplicationState() && prevState.expanded) {
      this.closeDropdown()
    }
  }

  getApplicationState = () => {
    const {
      application: { id },
      applicationsState
    } = this.props

    return applicationsState[id]
  }

  closeDropdown = () => {
    this.setState({ expanded: false })
  }

  _handleAccept = () => {
    const {
      application: { id },
      acceptApplication
    } = this.props

    acceptApplication(id)
  }

  _handleDecline = () => {
    const {
      application: { id },
      declineApplication
    } = this.props

    declineApplication(id)
  }

  _handleViewClick = () => {
    this.setState((prevState: IState) => {
      return {
        expanded: !prevState.expanded
      }
    })
  }

  _handleApplicationExpire = () => {
    const {
      application: { id },
      removeApplication
    } = this.props

    removeApplication(id)
  }

  renderRowPreview = () => {
    const {
      application,
      currentUser: { settings }
    } = this.props
    const {
      id,
      user: { userID, ...userProps },
      expires,
      message
    } = application
    const personProps = { ...userProps, ...settings, userID }
    const state = this.getApplicationState()

    return (
      <div key={id} className={tableStyles.tableRow}>
        <div className={cn(tableStyles.tableCell, tableStyles.person)}>
          <Person {...personProps} />
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.application)}>
          <Message message={String(message)} applicationState={state} />
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.expires)}>
          <Countdown value={expires} onExpire={this._handleApplicationExpire} showTime={!state} />
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.view)}>
          <View onClick={this._handleViewClick} />
        </div>
      </div>
    )
  }

  renderInfo = () => {
    const {
      application,
      allowedUsersAmount,
      currentUser: { permissions },
      applicationsState
    } = this.props
    const { expanded } = this.state

    return (
      expanded && (
        <DropdownContent
          application={application}
          allowedUsersAmount={allowedUsersAmount}
          userPermissions={permissions}
          onAccept={this._handleAccept}
          onDecline={this._handleDecline}
          applicationState={applicationsState[application.id]?.state}
        />
      )
    )
  }

  render() {
    const { expanded } = this.state
    const applicationState = this.getApplicationState()?.state
    const rowWrapperStyles = cn({
      [tableStyles.rowWrapper]: true,
      [tableStyles[applicationState]]: applicationState,
      [tableStyles.expanded]: expanded
    })

    return (
      <div className={rowWrapperStyles}>
        {this.renderRowPreview()}
        {this.renderInfo()}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    applicationsState: state.data.applicationsState,
    currentUser: state.data.currentUser
  }
}

const mapActionsToProps = {
  acceptApplication: acceptApplicationAction,
  declineApplication: declineApplicationAction,
  removeApplication: removeApplicationAction
}

export default connect(mapStateToProps, mapActionsToProps)(Row)
