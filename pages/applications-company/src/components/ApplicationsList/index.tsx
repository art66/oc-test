import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import s from './index.cssmodule.scss'
import { ICompany, IApplication, IReduxState } from '../../interfaces'
import { MEMBERS_FULL, APPLICATIONS_EMPTY } from '../../constants/messages'
import { TABLE_COLUMNS } from '../../constants'

import Row from './Row'

interface IProps {
  company: ICompany
  applications: IApplication[]
}

class ApplicationList extends Component<IProps> {
  getAllowedUsersAmount = () => {
    const { company: { amountOfMembers, maxAmountOfMembers } } = this.props

    return maxAmountOfMembers - amountOfMembers
  }

  renderAllowedMembersCount = () => {
    const allowedMembers = this.getAllowedUsersAmount()

    return allowedMembers > 0 ? (
      <div className={s.applicationListHeader}>{`Your company can accept ${allowedMembers} more employees`}</div>
    ) : (
      <div className={cn(s.applicationListHeader, s.warning)}>{MEMBERS_FULL}</div>
    )
  }

  renderTableHeaderCells = () => {
    return TABLE_COLUMNS.map(label => (
      <div key={label} className={cn(s.tableCell, s[label])}>{firstLetterUpper({ value: label })}</div>
    ))
  }

  renderTableRows = () => {
    const { applications } = this.props

    return applications.map(application => (
      <Row
        key={application.id}
        allowedUsersAmount={this.getAllowedUsersAmount()}
        application={application}
      />
    ))
  }

  render() {
    const { applications } = this.props

    if (!applications.length) {
      return <div className={s.applicationsEmpty}>{APPLICATIONS_EMPTY}</div>
    }

    return (
      <div className={s.applicationListWrapper}>
        <div className={s.applicationListPromt}>
          <span>When users submit an application to join the company, they can be accepted or declined below.</span>
          <span>
            If you accept an application, the person will join the company as the
            <strong> &apos;Unassigned&apos; </strong>
            rank. You can then select a rank for the user.
          </span>
        </div>
        {this.renderAllowedMembersCount()}
        <div className={s.tableHeader}>
          {this.renderTableHeaderCells()}
        </div>
        <div className={s.tableBody}>
          {this.renderTableRows()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    company: state.data.company,
    applications: state.data.applications
  }
}

export default connect(mapStateToProps)(ApplicationList)
