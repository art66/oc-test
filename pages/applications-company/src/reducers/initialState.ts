export default {
  loading: false,
  currentUser: {
    permissions: {},
    settings: {}
  },
  company: {},
  applications: [],
  applicationsState: {}
}
