import { ICharacteristic } from '../../interfaces'

export type TUpdateApplicationSocket = {
  id: number
  message?: string
  user?: {
    characteristics: ICharacteristic
  }
}
export type TAcceptApplicationSocket = { id: number }
export type TWithdrawApplicationSocket = { id: number }
export type TDeclineApplicationSocket = { id: number }
export type TRemoveApplicationSocket = { id: number }
export type TUpdateCurrentUserSocket = { manageApplication: boolean }
export type TUpdateSettingsSocket = { applicationAllowed: boolean }
