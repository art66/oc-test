import React from 'react'
import { connect } from 'react-redux'
import AllowApplicationsSwitch from '../../components/AllowApplicationsSwitch'
import ApplicationsList from '../../components/ApplicationsList'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'

interface IProps {
  loading: boolean
}

class AppLayout extends React.PureComponent<IProps> {
  render() {
    const { loading } = this.props

    // use legacy preloader here to sync with tabs preloader on company controls page
    if (loading) {
      return <img src='/images/v2/main/ajax-loader.gif' className='ajax-placeholder m-top10 m-bottom10' />
    }

    return (
      <>
        <AllowApplicationsSwitch />
        <ApplicationsList />
      </>
    )
  }
}

const mapStateToProps = (state: any) => ({
  loading: state.data.loading
})

export default connect(mapStateToProps)(AppLayout)
