import { put, takeEvery } from 'redux-saga/effects'
import * as a from '../constants/actionTypes'
import { IApplication } from '../interfaces'
import { APPLICATION_STATES } from '../constants'
import fetchUrl from '../utils/fetchURL'
import * as actions from '../actions'

interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* fetchData() {
  try {
    const data = yield fetchUrl('init')

    const sortApplications = (a: IApplication, b: IApplication) => {
      const { applicationsState } = data

      if (!applicationsState[a.id] && applicationsState[b.id])
        return -1

      if (applicationsState[a.id] && !applicationsState[b.id])
        return 1

      return b.expires - a.expires
    }

    yield put(actions.fetchApplicationDataSuccess(
      {
        ...data,
        applications: data.applications.sort(sortApplications)
      }
    ))
  } catch (error) {
    console.error(error)
  }
}

function* updateApplicationsAllowance(action: IAction<{ payload: boolean }>) {
  try {
    const { payload } = action

    // response coming from socket, don't handle response here to avoid double action dispatch
    yield fetchUrl('updateSettings', { applicationAllowed: payload }, 'applicationCompanySettings')
  } catch (error) {
    console.error(error)
  }
}

function* acceptApplication(action: IAction<{ payload: number }>) {
  try {
    const { payload } = action

    // response coming from socket, don't handle response here to avoid double action dispatch
    yield fetchUrl('accept', { application: { id: payload } })
  } catch (error) {
    yield put(actions.changeApplicationState(action.payload, APPLICATION_STATES.DECLINED, error.message))
  }
}

function* declineApplication(action: IAction<{ payload: number }>) {
  try {
    const { payload } = action

    // response coming from socket, don't handle response here to avoid double action dispatch
    yield fetchUrl('decline', { application: { id: payload } })
  } catch (error) {
    yield put(actions.changeApplicationState(action.payload, APPLICATION_STATES.DECLINED, error.message))
  }
}

export default function* appSaga() {
  yield takeEvery(a.FETCH_APPLICATIONS_DATA, fetchData)
  yield takeEvery(a.UPDATE_APPLICATIONS_ALLOWANCE, updateApplicationsAllowance)
  yield takeEvery(a.ACCEPT_APPLICATION, acceptApplication)
  yield takeEvery(a.DECLINE_APPLICATION, declineApplication)
}
