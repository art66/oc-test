export const APPLICATIONS_ALLOWED_LABEL = 'Allow applications'
export const MEMBERS_FULL = 'This company does not have the capacity for any more employees'
export const APPLICATIONS_EMPTY = 'There are no applications at this time.'
