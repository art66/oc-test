export const TABLE_COLUMNS = ['person', 'application', 'expires', 'view']
export const APPLICATION_STATES = {
  DECLINED: 'declined',
  ACCEPTED: 'accepted',
  WITHDRAWN: 'withdrawn'
}
