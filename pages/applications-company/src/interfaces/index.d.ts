type TOnlineStatus = 'online' | 'idle' | 'offline'

export interface IFactionTag {
  imageUrl?: string
  shortName?: string
}

export interface IFaction {
  id: number
  tag: IFactionTag
}

export interface ICompany {
  amountOfMembers: number
  maxAmountOfMembers: number
  applicationAllowed: boolean
}

export interface IUserSettings {
  showImages: boolean
}

export interface ICurrentUser {
  permissions: {
    manageApplications: boolean
  }
  settings: IUserSettings
}

export interface ICharacteristic {
  title: string
  value: number
}

export interface IUser {
  userID: number
  onlineStatus: TOnlineStatus
  faction?: IFaction
  userImageUrl: string
  playername: string
  level: number
  characteristics: ICharacteristic[]
}

export interface IApplication {
  id: number
  user: IUser
  message: string
  expires: number
}

export type TApplicationState = {
  state: 'accepted' | 'declined' | 'withdrawn'
  message?: string
}

export type TApplicationsStateObject = {
  [K in IApplication['id']]: TApplicationState
}

export interface IReduxState {
  browser: {
    mediaType: string
  }
  data: {
    loading: boolean
    currentUser: ICurrentUser
    company: ICompany
    applications: IApplication[]
    applicationsState: TApplicationsStateObject
  }
}
