import { IAPIState } from '../routes/API/reducers'

export type TMediaType = 'desktop' | 'tablet' | 'mobile'

export default interface IStore {
  api: IAPIState
  browser: {
    mediaType: TMediaType
  }
}
