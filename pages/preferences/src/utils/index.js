import { MAIN_URL } from '../constants'
/* global addRFC */

export function fetchUrl(url, data) {
  return fetch(addRFC(MAIN_URL + url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      try {
        var json = JSON.parse(text)
        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = 'Server responded with: ' + text
      }

      if (error) {
        throw new Error(error.message)
      } else {
        return json
      }
    })
  })
}
