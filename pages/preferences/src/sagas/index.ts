import { all } from 'redux-saga/effects'
import api from '../routes/API/sagas'
import accountClosure from '../routes/AccountClosure/sagas'

export default function* rootSaga() {
  yield all([
    api(),
    accountClosure()
  ])
}
