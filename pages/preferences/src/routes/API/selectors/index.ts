import { createSelector } from 'reselect'
import IStore from '../../../store/IStore'
import { CUSTOM_TYPE_ACCESS } from '../constants'

export const getApiKeys = (state: IStore) => state.api.keys
export const getIsLoading = (state: IStore) => state.api.loading
export const getAccessTypes = (state: IStore) => state.api.accessTypes
export const getLogs = (state: IStore) => state.api.log
export const getKeysAmount = (state: IStore) => state.api.keysAmount
export const getKeysAmountMax = (state: IStore) => state.api.keysAmountMax
export const getIsAddButtonEnabled = createSelector(
  [getKeysAmount, getKeysAmountMax],
  (keysAmount, keysAmountMax) => keysAmount < keysAmountMax
)
export const getKeySuitableForLog = (state: IStore) => state.api.keySuitableForLog
export const getMediaType = (state: IStore) => state.browser.mediaType
export const getNewlyCreatedKey = (state: IStore) => state.api.newlyCreatedKey?.key
export const getNewlyCreatedKeyAccessRules = (state: IStore) => state.api.newlyCreatedKey?.accessRules
export const getNewlyCreatedKeyType = (state: IStore) => state.api.newlyCreatedKeyType
export const getIsShowingCreatedKeyPopup = (state: IStore) => state.api.isShowCreatedKeyPopup
export const getAccessTypesList = createSelector([getAccessTypes], accessTypes => {
  return Object.entries(accessTypes)
    .map(accessType => {
      return {
        id: accessType[0],
        name: accessType[1],
        customClass: accessType[1].replace(' ', '')
      }
    })
    .filter(({ name }) => name !== CUSTOM_TYPE_ACCESS)
})
