import React from 'react'

interface IProps {
  customClasses?: string
}

/* eslint-disable max-len */
export const RemoveKeyIcon = (props: IProps) => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
      width='15'
      height='16'
      viewBox='0 0 15 16'
      className={props.customClasses}
    >
      <path
        d='M14,11.776,9.15,6.988l4.783-4.831L11.776,0,6.986,4.852,2.138.067,0,2.206,4.854,7.012.067,11.861,2.206,14l4.8-4.852,4.833,4.785Z'
        transform='translate(0.71 0.71)'
      />
    </svg>
  )
}

export default RemoveKeyIcon
