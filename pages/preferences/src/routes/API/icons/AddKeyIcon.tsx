import React from 'react'

/* eslint-disable max-len */
export const AddKeyIcon = () => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
      width='14'
      height='14'
      viewBox='0 0 14 14'
    >
      <defs>
        <linearGradient id='api-key-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#999' />
          <stop offset='1' stopColor='#ccc' />
        </linearGradient>
        <linearGradient id='api-key-active-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#666' />
          <stop offset='1' stopColor='#999' />
        </linearGradient>
        <linearGradient id='api-key-hover-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#333' /><stop offset='1' stopColor='#666' />
        </linearGradient>
        <linearGradient id='api-key-dark-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#ddd' />
          <stop offset='1' stopColor='#999' />
        </linearGradient>
      </defs>
      <path d='M5,14V9H0V5H5V0H9V5h5V9H9v5Z' />
    </svg>
  )
}

export default AddKeyIcon
