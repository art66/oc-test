import React from 'react'

interface IProps {
  customClasses?: string
}

/* eslint-disable max-len */
export const HistoryKeyIcon = (props: IProps) => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
      width='19'
      height='20'
      viewBox='0 0 19 20'
      className={props.customClasses}
    >
      <defs>
        <linearGradient id='api-key-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#999' />
          <stop offset='1' stopColor='#ccc' />
        </linearGradient>
        <linearGradient id='api-key-active-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#666' />
          <stop offset='1' stopColor='#999' />
        </linearGradient>
        <linearGradient id='api-key-hover-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#333' /><stop offset='1' stopColor='#666' />
        </linearGradient>
        <linearGradient id='api-key-dark-gradient' gradientUnits='objectBoundingBox' gradientTransform='rotate(90)'>
          <stop offset='0' stopColor='#ddd' />
          <stop offset='1' stopColor='#999' />
        </linearGradient>
      </defs>
      <path
        d='M18,9A9,9,0,0,1,0,9H1.5A7.526,7.526,0,1,0,3.676,3.733L5.221,5.279,0,6.294l.979-5.26L2.613,2.669A8.993,8.993,0,0,1,18,9ZM8.25,4.5v6H13.5V9H9.75V4.5Z'
        transform='translate(0.63 0.5)'
      />
    </svg>
  )
}

export default HistoryKeyIcon
