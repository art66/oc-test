import React, { useState } from 'react'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { buildUsedTime } from '../../helpers'
import Copyable from '../Copyable'
import TypeTooltip from '../TypeTooltip'
import HistoryButton from '../HistoryButton'
import DeleteButton from '../DeleteButton'
import { HISTORY_PANEL, DELETE_PANEL, TOOLTIP_STYLES } from '../../constants'
import { TAccessRules } from '../../services/getApiData'
import s from './keyRow.cssmodule.scss'

interface IProp {
  apiKey: string
  type: string
  name: string
  lastUsed: number
  id: number
  accessRules?: TAccessRules
  renderExpanded?: (id: number, key: string) => React.ReactNode
  historyClick?: (key: string) => void
  deleteClick?: (key: string) => void
  isHighlighted?: boolean
  expandedPanel?: string
}

const KeyRow = (props: IProp) => {
  const typeWithoutSpace = props.type && props.type.replace(' ', '')
  const [isEllipsisActive, setIsEllipsisActive] = useState(false)

  const handleNameMouseOver = e => setIsEllipsisActive(e.target.offsetWidth < e.target.scrollWidth)
  const handleHistoryButton = () => props.historyClick(props.apiKey)
  const handleDeleteButton = () => props.deleteClick(props.apiKey)

  const renderKeyNameTooltip = () => {
    return (
      isEllipsisActive && (
        <Tooltip parent={`key-name-${props.id}`} arrow='center' position='top' style={TOOLTIP_STYLES}>
          {props.name}
        </Tooltip>
      )
    )
  }

  return (
    <li
      key={props.id}
      className={cn(s.keyRow, { [s.highlighted]: props.isHighlighted, [s.expandedKey]: props.expandedPanel.length })}
    >
      <div className={s.info}>
        <p
          id={`key-name-${props.id}`}
          className={s.name}
          onMouseOver={handleNameMouseOver}
          onFocus={handleNameMouseOver}
        >
          {props.name}
        </p>
        <p className={s.usedTime}>{props.lastUsed ? `Used: ${buildUsedTime(props.lastUsed)} ago` : 'Not used'}</p>
      </div>
      <Copyable id={`key-row-${props.apiKey}`} value={props.apiKey} />
      <div className={s.blockTablet}>
        <input
          id={`${typeWithoutSpace}${props.id}`}
          className={cn(s.type, s[typeWithoutSpace])}
          value={props.type}
          readOnly={true}
        />
        <TypeTooltip id={`${typeWithoutSpace}${props.id}`} type={props.type} accessRules={props.accessRules} />
        <div className={s.actions}>
          <HistoryButton
            id={`history-button-${props.apiKey}`}
            onClick={handleHistoryButton}
            active={props.expandedPanel === HISTORY_PANEL}
          />
          <DeleteButton
            id={`delete-button-${props.apiKey}`}
            onClick={handleDeleteButton}
            active={props.expandedPanel === DELETE_PANEL}
          />
        </div>
      </div>
      <div className={s.expandedPanel}>{props.renderExpanded(props.id, props.apiKey)}</div>
      {renderKeyNameTooltip()}
    </li>
  )
}

export default KeyRow
