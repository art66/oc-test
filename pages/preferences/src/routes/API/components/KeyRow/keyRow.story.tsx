import React from 'react'
import KeyRow from './KeyRow'

export const keyRow = () => (
  <ul>
    <KeyRow apiKey='qRmzTHmKLYIvBKkd' type='Full Access' name='test key' lastUsed={20} id={1} />
    <KeyRow apiKey='qRmzTHmKLYIvBKkd' type='Full Access' name='test key' lastUsed={20} id={1} />
    <KeyRow apiKey='qRmzTHmKLYIvBKkd' type='Full Access' name='test key' lastUsed={20} id={1} />
    <KeyRow apiKey='qRmzTHmKLYIvBKkd' type='Full Access' name='test key' lastUsed={20} id={1} />
  </ul>
)

keyRow.story = {
  name: 'KeyRow'
}

export default {
  title: 'Preferences/KeyRow'
}
