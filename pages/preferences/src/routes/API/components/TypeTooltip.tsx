import React from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import { getTypeDescription } from '../helpers'
import { TAccessRules } from '../services/getApiData'
import { CUSTOM_TYPE_ACCESS, TOOLTIP_STYLES } from '../constants'

interface IProp {
  id: string
  type: string
  accessRules: TAccessRules
}

const TypeTooltip = (props: IProp) => {
  const renderAccessRules = () =>
    props.type === CUSTOM_TYPE_ACCESS
    && props.accessRules
    && Object.entries(props.accessRules).map(accessRule => (
      <p key={`${props.id}-${accessRule[0]}`}>
        <strong>{accessRule[0]}</strong>: <span className='categoriesApi'>{accessRule[1].join(', ')}</span>
      </p>
    ))

  return props.type ? (
    <Tooltip
      parent={`${props.id}`}
      arrow='center'
      position='top'
      style={{
        ...TOOLTIP_STYLES,
        style: {
          ...TOOLTIP_STYLES.style,
          width: '320px',
          lineHeight: '16px',
          textAlign: 'left'
        }
      }}
    >
      <strong>{props.type}:</strong> {getTypeDescription(props.type)}
      {props.accessRules && <div className='selectionsApi'>{renderAccessRules()}</div>}
    </Tooltip>
  ) : null
}

export default TypeTooltip
