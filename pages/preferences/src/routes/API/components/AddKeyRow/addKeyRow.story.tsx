import React from 'react'
import AddKeyRow from './AddKeyRow'

const LIST = [
  { id: '0', name: 'Public', customClass: 'gray' },
  { id: '1', name: 'Minimal', customClass: 'green' },
  { id: '2', name: 'Limited', customClass: 'yellow' },
  { id: '3', name: 'Full', customClass: 'red' },
  { id: '3', name: 'Custom', customClass: 'violet' }
]

const collapse = () => null

export const addKeyRow = () => (<AddKeyRow typeList={LIST} collapse={collapse} />)

addKeyRow.story = {
  name: 'AddKeyRow'
}

export default {
  title: 'Preferences/AddKeyRow'
}
