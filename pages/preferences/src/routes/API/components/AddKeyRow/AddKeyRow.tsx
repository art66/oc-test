import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import Dropdown from '@torn/shared/components/Dropdown'
import TypeTooltip from '../TypeTooltip'
import { addApiKey } from '../../actions'
import { getMediaType } from '../../selectors'
import { INPUT_TITLE_KEY_PLACEHOLDER, DESKTOP_TYPE } from '../../constants'
import s from './AddKeyRow.cssmodule.scss'
import AddKeyIcon from '../../icons/AddKeyIcon'

interface IProps {
  typeList: {
    id: string
    name: string
    customClass: string
  }[]
  collapse: () => void
}

const AddKeyRow = (props: IProps) => {
  const dispatch = useDispatch()
  const [title, setTitle] = useState('')
  const [isValidTitle, setIsValidTitle] = useState(false)
  const [isTyping, setIsTyping] = useState(false)
  const [type, setType] = useState({ id: 1, name: 'Public Only', customClass: 'PublicOnly' })
  const mediaType = useSelector(getMediaType)

  const handleAddNewKey = () => {
    dispatch(addApiKey({ title, type: +type.id }))
    props.collapse()
  }

  const handleChangeTitle = e => {
    if (e.target.value.length && e.target.value.trim().length) {
      setTitle(e.target.value.trim())
      setIsValidTitle(true)
    } else {
      setTitle('')
      setIsValidTitle(false)
    }
  }

  const handleChangeType = e => setType(e)
  const hidePlaceholder = e => {
    e.target.placeholder = ''
    setIsTyping(true)
  }
  const showPlaceholder = e => {
    e.target.placeholder = INPUT_TITLE_KEY_PLACEHOLDER
    setIsTyping(title.length > 0)
  }

  return (
    <div className={s.addKeyRow}>
      <input
        placeholder={INPUT_TITLE_KEY_PLACEHOLDER}
        className={cn(s.keyTitle, { [s.typing]: isTyping })}
        onChange={handleChangeTitle}
        onFocus={hidePlaceholder}
        onBlur={showPlaceholder}
      />
      <input value='' className={s.key} readOnly={true} />
      <Dropdown
        id='typeDropdown'
        selected={type}
        list={props.typeList}
        className={cn(s.typesDropdown, s[type.customClass])}
        onChange={handleChangeType}
      />
      <button
        type='button'
        disabled={!isValidTitle}
        className={cn('torn-btn gray', s.addButton)}
        onClick={handleAddNewKey}
      >
        {mediaType === DESKTOP_TYPE ? 'Add' : <AddKeyIcon />}
      </button>
      <TypeTooltip id='typeDropdown' type={type.name} accessRules={null} />
    </div>
  )
}

export default AddKeyRow
