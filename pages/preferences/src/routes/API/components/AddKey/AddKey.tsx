import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import AddKeyButton from '../AddKeyButton/AddKeyButton'
import AddKeyRow from '../AddKeyRow/AddKeyRow'
import { getAccessTypesList } from '../../selectors'
import s from './addKey.cssmodule.scss'

const AddKey = () => {
  const [isExpanded, setIsExpanded] = useState(false)
  const typeList = useSelector(getAccessTypesList)

  const handleExpand = () => setIsExpanded(true)

  const handleCollapse = () => setIsExpanded(false)

  return (
    <div className={s.addKeyBlock}>
      {isExpanded ? (
        <AddKeyRow collapse={handleCollapse} typeList={typeList} />
      ) : (
        <AddKeyButton onClick={handleExpand} />
      )}
    </div>
  )
}

export default AddKey
