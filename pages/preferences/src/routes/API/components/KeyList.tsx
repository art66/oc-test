import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import KeyRow from './KeyRow/KeyRow'
import Logs from './Logs/Logs'
import DeleteConfirmationDialog from './DeleteConfirmationDialog/DeleteConfirmationDialog'
import Popup from './Popup/Popup'
import { getIsShowingCreatedKeyPopup, getLogs, getMediaType, getNewlyCreatedKey } from '../selectors'
import { removeApiKey } from '../actions'
import { TKey } from '../services/getApiData'
import { HISTORY_PANEL, DELETE_PANEL } from '../constants'

interface IProps {
  keys: TKey[]
  accessTypes: {
    [key: number]: string
  }
}

const KeyList = (props: IProps) => {
  const dispatch = useDispatch()
  const logs = useSelector(getLogs)
  const mediaType = useSelector(getMediaType)
  const newlyCreatedKey = useSelector(getNewlyCreatedKey)
  const isShowingPopup = useSelector(getIsShowingCreatedKeyPopup)
  const [expandedRow, setExpandedRow] = useState<{ key: string; panel?: string }>({
    key: null,
    panel: null
  })

  const getExpandedRowValue = (key: string, panel: string) =>
    panel === expandedRow.panel && key === expandedRow.key ? { key: null, panel: null } : { key, panel }

  const handleHistoryClick = (key: string) => setExpandedRow(getExpandedRowValue(key, HISTORY_PANEL))

  const handleDeleteClick = (key: string) => setExpandedRow(getExpandedRowValue(key, DELETE_PANEL))

  const deleteKey = (keyId: number) => dispatch(removeApiKey(keyId))

  const renderExpanded = (id: number, key: string) => {
    if (expandedRow.key === key) {
      return expandedRow.panel === HISTORY_PANEL ? (
        <Logs logs={logs[key]} mediaType={mediaType} isExpanded={expandedRow.panel === HISTORY_PANEL} />
      ) : (
        <DeleteConfirmationDialog
          confirmAction={() => deleteKey(id)}
          rejectAction={() => setExpandedRow({ key: null })}
        />
      )
    }

    return null
  }

  return (
    <>
      <ul>
        {props.keys.map(key => (
          <KeyRow
            key={key.ID}
            id={key.ID}
            apiKey={key.key}
            type={props.accessTypes[key.accessType]}
            lastUsed={key.lastRequestTime}
            accessRules={key.accessRules}
            name={key.title}
            renderExpanded={renderExpanded}
            historyClick={handleHistoryClick}
            deleteClick={handleDeleteClick}
            isHighlighted={newlyCreatedKey === key.key}
            expandedPanel={key.key === expandedRow.key ? expandedRow.panel : ''}
          />
        ))}
      </ul>
      {isShowingPopup && <Popup />}
    </>
  )
}

export default KeyList
