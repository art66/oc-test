import React from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import HistoryKeyIcon from '../icons/HistoryKeyIcon'
import { TOOLTIP_STYLES } from '../constants'
import s from './KeyRow/keyRow.cssmodule.scss'

interface IProps {
  id: string
  active: boolean
  onClick: () => void
}

const HistoryButton = (props: IProps) => {
  return (
    <>
      <button id={props.id} type='button' className={s.historyButton} onClick={props.onClick}>
        <HistoryKeyIcon customClasses={`${s.icon} ${props.active ? s.active : ''}`} />
      </button>
      <Tooltip parent={props.id} arrow='center' position='top' style={TOOLTIP_STYLES}>
        Recent usage
      </Tooltip>
    </>
  )
}

export default HistoryButton
