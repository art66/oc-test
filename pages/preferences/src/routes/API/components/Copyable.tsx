import React, { useState } from 'react'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { TOOLTIP_STYLES } from '../constants'
import s from './KeyRow/keyRow.cssmodule.scss'

interface IProps {
  id: string
  value: string
  customClass?: string
}

const Copyable = (props: IProps) => {
  let timeout
  const [isTooltipVisible, setIsTooltipVisible] = useState(false)

  const copyToClipboard = () => {
    clearTimeout(timeout)
    setIsTooltipVisible(true)
    navigator.clipboard.writeText(props.value)

    timeout = setTimeout(() => {
      setIsTooltipVisible(false)
    }, 500)
  }

  return (
    <>
      <input
        id={props.id}
        className={cn(s.key, props.customClass)}
        value={props.value}
        readOnly={true}
        onClick={copyToClipboard}
      />
      <Tooltip active={isTooltipVisible} parent={props.id} arrow='center' position='top' style={TOOLTIP_STYLES}>
        Copied to clipboard
      </Tooltip>
    </>
  )
}

export default Copyable
