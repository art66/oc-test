import React from 'react'
import AddKeyButton from './AddKeyButton'

const onClick = () => null

export const addKeyButton = () => (<AddKeyButton onClick={onClick} />)

addKeyButton.story = {
  name: 'AddKeyButton'
}

export default {
  title: 'Preferences/AddKeyButton'
}
