import React from 'react'
import { useSelector } from 'react-redux'
import Tooltip from '@torn/shared/components/Tooltip'
import AddKeyIcon from '../../icons/AddKeyIcon'
import { getIsAddButtonEnabled, getKeysAmountMax } from '../../selectors'
import { TOOLTIP_STYLES } from '../../constants'
import s from './addKeyButton.cssmodule.scss'

interface IProps {
  onClick: () => void
}

const AddKeyButton = (props: IProps) => {
  const isAddButtonEnabled = useSelector(getIsAddButtonEnabled)
  const keysAmountMax = useSelector(getKeysAmountMax)

  return (
    <>
      <div id='key-createNewKey' className={s.createKeyContainer}>
        <button type='button' className={s.addKey} onClick={props.onClick} disabled={!isAddButtonEnabled}>
          <AddKeyIcon />
          <span className={s.text}>Create New Key</span>
        </button>
      </div>
      <Tooltip
        active={!isAddButtonEnabled}
        parent='key-createNewKey'
        arrow='center'
        position='top'
        style={TOOLTIP_STYLES}
      >
        {`You can only have ${keysAmountMax} API keys`}
      </Tooltip>
    </>
  )
}

export default AddKeyButton
