import React from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import RemoveKeyIcon from '../icons/RemoveKeyIcon'
import { TOOLTIP_STYLES } from '../constants'
import s from './KeyRow/keyRow.cssmodule.scss'

interface IProps {
  id: string
  active: boolean
  onClick: () => void
}

const DeleteButton = (props: IProps) => {
  return (
    <>
      <button id={props.id} type='button' className={s.deleteButton} onClick={props.onClick}>
        <RemoveKeyIcon customClasses={`${s.icon} ${props.active ? s.active : ''}`} />
      </button>
      <Tooltip style={TOOLTIP_STYLES} parent={props.id} arrow='center' position='top'>
        Delete
      </Tooltip>
    </>
  )
}

export default DeleteButton
