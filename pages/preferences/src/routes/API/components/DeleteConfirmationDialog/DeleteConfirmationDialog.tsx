import React from 'react'
import { useSelector } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'
import { getIsLoading } from '../../selectors'
import s from './deleteConfirmationDialog.cssmodule.scss'

interface IProps {
  confirmAction: () => void
  rejectAction: () => void
}

const DeleteConfirmationDialog = (props: IProps) => {
  const isLoading = useSelector(getIsLoading)

  return (
    <div className={s.deleteDialog}>
      {
        isLoading ? <Preloader /> : (
          <>
            <p>Are you sure you would like to delete this API Key?</p>
            <div className={s.deleteActions}>
              <button className={s.dialogButton} type='button' onClick={props.confirmAction}>
                Delete
              </button>
              <button className={s.dialogButton} type='button' onClick={props.rejectAction}>
                Cancel
              </button>
            </div>
          </>
        )
      }
    </div>
  )
}

export default DeleteConfirmationDialog
