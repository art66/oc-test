import React from 'react'
import Popup from './Popup'

export const popup = () => (<Popup />)

popup.story = {
  name: 'Popup'
}

export default {
  title: 'Preferences/Popup'
}
