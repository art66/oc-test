import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import Copyable from '../Copyable'
import { getTypeDescription } from '../../helpers'
import { getNewlyCreatedKey, getNewlyCreatedKeyAccessRules, getNewlyCreatedKeyType } from '../../selectors'
import { setIsShowCreatedKeyPopup } from '../../actions'
import { CUSTOM_TYPE_ACCESS } from '../../constants'
import s from './popup.cssmodule.scss'

const Popup = () => {
  const dispatch = useDispatch()
  const apiKey = useSelector(getNewlyCreatedKey)
  const apiKeyType = useSelector(getNewlyCreatedKeyType)
  const accessRules = useSelector(getNewlyCreatedKeyAccessRules)

  const renderAccessRules = () =>
    apiKeyType === CUSTOM_TYPE_ACCESS
    && Object.entries(accessRules).map(accessRule => (
      <p key={`${apiKey}-${accessRule[0]}`}>
        <strong>{accessRule[0]}</strong>: {accessRule[1].join(', ')}
      </p>
    ))

  const renderDescription = () => {
    return (
      <>
        {apiKeyType === CUSTOM_TYPE_ACCESS ? '' : <strong>{apiKeyType}: </strong>}
        {getTypeDescription(apiKeyType)}
      </>
    )
  }

  const handleOkay = () => dispatch(setIsShowCreatedKeyPopup(false))

  return (
    <div className={s.popupContainer}>
      <div className={cn(s.popup, s[apiKeyType.replace(' ', '')])}>
        <p className={s.title}>API KEY CREATED</p>
        <div className={s.info}>
          <p className={s.description}>{renderDescription()}</p>
          <div className={s.categories}>{renderAccessRules()}</div>
          <p>If you are happy to reveal this information, please provide the key below.</p>
          <Copyable id={`key-popup-${apiKey}`} value={apiKey} customClass={s.popupInput} />
        </div>
        <button type='button' className='torn-btn gray' onClick={handleOkay}>
          Okay
        </button>
      </div>
    </div>
  )
}

export default Popup
