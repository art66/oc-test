import React from 'react'
import cn from 'classnames'
import Log from './Log'
import { TLog } from '../../services/getApiData'
import { TMediaType } from '../../../../store/IStore'
import s from './logs.cssmodule.scss'

interface IProps {
  logs: TLog[]
  isExpanded: boolean
  mediaType: TMediaType
}

const Logs = (props: IProps) => {
  return (
    <div className={cn(s.logs, { [s.logsMsg]: !props.logs.length, [s.expandedLogs]: props.isExpanded })}>
      {props.logs.length ? (
        <>
          <div className={cn(s.row, s.title)}>
            <div className={cn(s.cell, s.time)}>Time</div>
            <div className={cn(s.cell, s.ip)}>IP address</div>
            <div className={cn(s.cell, s.request)}>Most recent requests</div>
            <div className={cn(s.cell, s.comment)}>Comment</div>
          </div>
          {props.logs.map(log => (
            <Log key={log.logID} log={log} mediaType={props.mediaType} />
          ))}
        </>
      ) : (
        <p>This key has not been recently used</p>
      )}
    </div>
  )
}

export default Logs
