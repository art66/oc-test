import React from 'react'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { formatTimeNumber as formatNum } from '@torn/shared/utils'
import { TLog } from '../../services/getApiData'
import { TMediaType } from '../../../../store/IStore'
import { DESKTOP_TYPE, TOOLTIP_STYLES } from '../../constants'
import s from './logs.cssmodule.scss'

interface IProps {
  log: TLog
  mediaType: TMediaType
}

const Log = ({ log, mediaType }: IProps) => {
  const t = new Date(log.time * 1000)
  const year = t.getUTCFullYear().toString().substring(2)
  const time = `${formatNum(t.getUTCHours())}:${formatNum(t.getUTCMinutes())}:${formatNum(t.getUTCSeconds())}`
  const date = `${formatNum(t.getUTCDate())}/${formatNum(t.getUTCMonth() + 1)}/${formatNum(year)}`

  const getTooltip = (id, comment) => {
    return (
      mediaType === DESKTOP_TYPE && (
        <Tooltip position='top' arrow='center' parent={id} style={TOOLTIP_STYLES}>
          {comment}
        </Tooltip>
      )
    )
  }

  return (
    <div key={log.logID} className={s.row}>
      <div className={cn(s.cell, s.time)}>
        {time} - {date}
      </div>
      <div className={cn(s.cell, s.ip)}>{log.ip}</div>
      <div className={cn(s.cell, s.request)}>
        /{log.type}/{log.ID}?selections={log.sel}
        {log.cat ? `&cat=${log.cat}` : ''}
        {log.log ? `&log=${log.log}` : ''}
      </div>
      <div id={`log-${log.ID}`} className={cn(s.cell, s.comment, log?.comment ? '' : s.hide)}>
        {log?.comment ?? '-'}
        {log?.comment && getTooltip(`log-${log.ID}`, log.comment)}
      </div>
    </div>
  )
}

export default Log
