import React, { Component } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import InfoBox from '@torn/shared/components/InfoBox'
import { initApi, stopPollingLogs } from '../actions'
import AddKey from './AddKey/AddKey'
import KeyList from './KeyList'
import IStore from '../../../store/IStore'
import s from './styles.cssmodule.scss'

declare interface ITornWindowProps {
  $: (el: string) => any
  informationMessageTemplateIn: (el: any, withTopDelim: boolean, withBottomDelim: boolean, color: string) => void
}
declare type TTornWindow = typeof window & ITornWindowProps

const { $, informationMessageTemplateIn } = window as TTornWindow

const KEY_SUITABLE_FOR_LOG_MSG = `The next time you reset your API key, your <a href='/page.php?sid=log'>Log</a>
 will become accessible to anything or anyone with access to it. Any personal information appearing in logs via the API
 like Emails or IP addresses will be hidden.`

type TProps = TReduxProps

class API extends Component<TProps> {
  componentDidMount() {
    this.props.initApi()
  }

  componentWillUnmount() {
    this.props.stopPollingLogs()
  }

  componentDidUpdate(oldProps: TProps) {
    const { msg, color } = this.props.infoMessage

    if (msg !== oldProps.infoMessage.msg) {
      this.showInfoMessage(msg, color)
    }
  }

  showInfoMessage = (msg: string, color: string) => {
    if (!msg) {
      return
    }

    /* temp solution until all preferences tabs are on react */
    informationMessageTemplateIn($('#ajaxinfobox'), false, true, color)
    $('#ajaxinfobox .info-msg .msg').text(msg)
  }

  renderKeySuitableForLogMsg = () => {
    const { keySuitableForLog } = this.props

    return (
      !keySuitableForLog && (
        <InfoBox customClass='keySuitableForLogMsg' msg={KEY_SUITABLE_FOR_LOG_MSG} color='red' showDangerously={true} />
      )
    )
  }

  render() {
    return (
      <div className={s.api}>
        {this.renderKeySuitableForLogMsg()}
        <p className={s.apiDescription}>
          Here you can share your account&#39;s information with software like scripts, extensions, and applications by
          providing them with an API key. Once you have given an API key with the relevant permissions, you can remove
          this access at any time by deleting the key.
        </p>
        <AddKey />
        <KeyList keys={this.props.keys} accessTypes={this.props.accessTypes} />
      </div>
    )
  }
}

const mapDispatchToProps = {
  stopPollingLogs,
  initApi
}

const mapStateToProps = (state: IStore) => ({
  keys: state.api.keys,
  accessTypes: state.api.accessTypes,
  keySuitableForLog: state.api.keySuitableForLog,
  logs: state.api.log,
  infoMessage: state.api.infoMessage,
  mediaType: state.browser.mediaType
})

const connector = connect(mapStateToProps, mapDispatchToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(API)
