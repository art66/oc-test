import {
  CUSTOM_TYPE_ACCESS,
  FULL_TYPE_ACCESS,
  LIMITED_TYPE_ACCESS,
  MINIMAL_TYPE_ACCESS,
  PUBLIC_TYPE_ACCESS,
  CUSTOM_DESCRIPTION,
  FULL_ACCESS_DESCRIPTION,
  LIMITED_ACCESS_DESCRIPTION,
  MINIMAL_ACCESS_DESCRIPTION,
  PUBLIC_ONLY_DESCRIPTION
} from '../constants'

interface IWindow {
  getCurrentTimestamp?: () => number
}

export const addPlural = (num: number): string => (num === 1 ? '' : 's')

// eslint-disable-next-line max-statements
export const buildUsedTime = (time: number): string => {
  const win = window as IWindow
  const currentTime = win.getCurrentTimestamp()
  const msPerMinute = 60 * 1000
  const msPerHour = msPerMinute * 60
  const msPerDay = msPerHour * 24
  const msPerMonth = msPerDay * 30
  const msPerYear = msPerDay * 365

  const elapsed = currentTime - time * 1000

  if (elapsed < msPerMinute) {
    const seconds = Math.round(elapsed / 1000)

    return `${seconds} second${addPlural(seconds)}`
  } else if (elapsed < msPerHour) {
    const minutes = Math.round(elapsed / msPerMinute)

    return `${minutes} minute${addPlural(minutes)}`
  } else if (elapsed < msPerDay) {
    const hours = Math.round(elapsed / msPerHour)

    return `${hours} hour${addPlural(hours)}`
  } else if (elapsed < msPerMonth) {
    const days = Math.round(elapsed / msPerDay)

    return `${days} day${addPlural(days)}`
  } else if (elapsed < msPerYear) {
    const months = Math.round(elapsed / msPerMonth)

    return `${months} month${addPlural(months)}`
  }

  const years = Math.round(elapsed / msPerYear)

  return `${years} year${addPlural(years)}`
}

export const getTypeDescription = (type: string): string => {
  let typeDescription = ''

  switch (type) {
    case FULL_TYPE_ACCESS:
      typeDescription = `${FULL_ACCESS_DESCRIPTION}`
      break
    case LIMITED_TYPE_ACCESS:
      typeDescription = `${LIMITED_ACCESS_DESCRIPTION}`
      break
    case MINIMAL_TYPE_ACCESS:
      typeDescription = `${MINIMAL_ACCESS_DESCRIPTION}`
      break
    case PUBLIC_TYPE_ACCESS:
      typeDescription = `${PUBLIC_ONLY_DESCRIPTION}`
      break
    case CUSTOM_TYPE_ACCESS:
      typeDescription = `${CUSTOM_DESCRIPTION}`
      break
    default:
      typeDescription = ''
  }

  return typeDescription
}
