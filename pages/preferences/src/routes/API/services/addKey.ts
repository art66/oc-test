import { fetchUrl } from '@torn/shared/utils'
import { TAddKeyPayload, TApiData } from './getApiData'

type TAddKey = TApiData & {
  success?: boolean
  error?: string
}

export const addKey = (payload: TAddKeyPayload): Promise<TAddKey> => {
  return fetchUrl('page.php?sid=apiData', {
    step: 'addNewKey',
    ...payload
  })
}
