import { fetchUrl } from '@torn/shared/utils'

export type TKey = {
  ID: number
  title: string
  userID: number
  active: 0 | 1
  trusted: 0 | 1
  accessType: number
  accessRules: TAccessRules | null
  keyType: number
  lastRequestTime: number
  key: string
}

export type TLog = {
  ID: string
  type: string
  sel: string
  ip: string
  time: number
  comment?: string
  cat?: string
  log?: string
  logID: string
}

export type TAccessRules = {
  [key: string]: string[]
}

export type TLogs = {
  [key: string]: TLog[]
}

export type TApiData = {
  accessTypes: {
    [key: number]: string
  }
  keys: TKey[]
  keysAmount: number
  keysAmountMax: number
  log: TLogs
}

export type TAddKeyPayload = {
  title?: string
  type?: number
  user?: string
  faction?: string
  company?: string
  properties?: string
  market?: string
  torn?: string
}

export const getApiData = (): Promise<TApiData> => {
  return fetchUrl('page.php?sid=apiData')
}
