import { fetchUrl } from '@torn/shared/utils'
import { TApiData } from './getApiData'

type TRemoveKey = TApiData & {
  success?: boolean
  error?: string
}

export const removeKey = (ID: number): Promise<TRemoveKey> => {
  return fetchUrl('page.php?sid=apiData', {
    step: 'removeKey',
    ID
  })
}
