export const CUSTOM_TYPE_ACCESS = 'Custom'
export const FULL_TYPE_ACCESS = 'Full Access'
export const LIMITED_TYPE_ACCESS = 'Limited Access'
export const MINIMAL_TYPE_ACCESS = 'Minimal Access'
export const PUBLIC_TYPE_ACCESS = 'Public Only'

export const INPUT_TITLE_KEY_PLACEHOLDER = 'New key\'s name'
export const HISTORY_PANEL = 'history'
export const DELETE_PANEL = 'delete'
export const DESKTOP_TYPE = 'desktop'

export const TOOLTIP_STYLES = {
  style: {
    background: 'var(--preferences-api-tooltip-bg)'
  },
  arrowStyle: {
    color: 'var(--preferences-api-tooltip-bg)',
    borderColor: 'var(--preferences-api-tooltip-bg)'
  }
}

export const FULL_ACCESS_DESCRIPTION = `This API key can access all API selections including your log. This means that
 this Full Access key can access everything that limited access can, but also the full history of your account and all
  actions you've ever made.`
export const LIMITED_ACCESS_DESCRIPTION = `This API key can access all selections other than your log. This Limited
 Access key will reveal your battle stats, financial information, events, message subjects, attack history
  and personal stats.`
export const MINIMAL_ACCESS_DESCRIPTION = `This API key can access a large range of data that most people would not
 consider to be private. Simply put, no one using this Minimal Access key will be able to use it to profit
  from you, or otherwise cause you harm.`
export const PUBLIC_ONLY_DESCRIPTION = `This API key can only access your account's information which is already
 public. This Public Only key provides no additional account information that anyone's API key can't already access.`
export const CUSTOM_DESCRIPTION = `This API key is limited to accessing your account's data
 within the following selections...`
