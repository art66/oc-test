import { createAction } from 'redux-actions'
import * as actionTypes from './actionTypes'
import { TApiData, TAddKeyPayload, TKey } from '../services/getApiData'

export const initApi = createAction(actionTypes.INIT_API)
export const setKeysData = createAction(actionTypes.SET_KEYS_DATA, (data: TApiData) => ({ data }))
export const removeApiKey = createAction(actionTypes.REMOVE_API_KEY, (ID: number) => ({ ID }))
export const addApiKey = createAction(actionTypes.ADD_API_KEY, (payload: TAddKeyPayload) => payload)
export const setKeys = createAction(actionTypes.SET_KEYS)
export const showInfoMessage = createAction(actionTypes.SHOW_INFO_MESSAGE, (msg: string, color: string) => ({
  msg,
  color
}))
export const stopPollingLogs = createAction(actionTypes.STOP_POLLING_LOGS)
export const loading = createAction(actionTypes.LOADING)
export const setKeySuitableForLog = createAction(actionTypes.SET_KEY_SUITABLE_FOR_LOG)
export const saveNewlyCreatedKey = createAction(actionTypes.SAVE_NEWLY_CREATED_KEY, (key: TKey, type: string) => ({
  key,
  type
}))
export const setIsShowCreatedKeyPopup = createAction(
  actionTypes.SET_IS_SHOW_CREATED_KEY_POPUP,
  (isShowPopup: boolean) => ({ isShowPopup })
)
