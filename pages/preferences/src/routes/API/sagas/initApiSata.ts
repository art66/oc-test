import { call } from 'redux-saga/effects'
import { loadApiDataSaga } from './loadApiDataSaga'
import { handleURLSaga } from './handleURLSaga'
import { startPollingLogs } from './pollApiDataSaga'

export function* initApiSata() {
  yield call(loadApiDataSaga)
  yield call(handleURLSaga)
  yield call(startPollingLogs)
}
