import { call, delay, race, take } from 'redux-saga/effects'
import * as actions from '../actions/actionTypes'
import { loadApiDataSaga } from './loadApiDataSaga'

const POLL_DELAY = 3000

function* pollAPI() {
  while (true) {
    // temp solution until all preferences tabs are on react
    if (window.location.hash === '#tab=api') {
      yield call(loadApiDataSaga)
    }

    yield delay(POLL_DELAY)
  }
}

export function* startPollingLogs() {
  yield race({
    task: call(pollAPI),
    cancel: take(actions.STOP_POLLING_LOGS)
  })
}
