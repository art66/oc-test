import { put } from 'redux-saga/effects'
import { setKeysData, showInfoMessage } from '../actions'
import { getApiData } from '../services/getApiData'

export function* loadApiDataSaga() {
  try {
    const data = yield getApiData()

    if (data.result === 'error') {
      throw new Error(data.message)
    }

    if (data) {
      yield put(setKeysData(data))
    }

    yield put(showInfoMessage('', ''))
  } catch (error) {
    yield put(showInfoMessage(error.message, 'red'))
  }
}
