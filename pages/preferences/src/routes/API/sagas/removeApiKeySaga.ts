import { put } from 'redux-saga/effects'
import { loading, setKeysData, showInfoMessage, removeApiKey } from '../actions'
import { removeKey } from '../services/removeKey'

export function* removeApiKeySaga(action: ReturnType<typeof removeApiKey>) {
  try {
    yield put(loading(true))
    const data = yield removeKey(action.payload.ID)

    if (data.result === 'error') {
      throw new Error(data.message)
    }

    if (data.result === 'success') {
      yield put(setKeysData(data))
      yield put(loading(false))
      yield put(showInfoMessage(data.message, 'green'))
    }
  } catch (error) {
    yield put(showInfoMessage(error.message, 'red'))
  }
}
