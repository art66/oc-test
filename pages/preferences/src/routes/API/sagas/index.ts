import { takeEvery } from 'redux-saga/effects'
import * as actions from '../actions/actionTypes'
import { initApiSata } from './initApiSata'
import { addApiKeySaga } from './addApiKeySaga'
import { removeApiKeySaga } from './removeApiKeySaga'

export default function* rootApiSaga() {
  yield takeEvery(actions.INIT_API, initApiSata)
  yield takeEvery(actions.ADD_API_KEY, addApiKeySaga)
  yield takeEvery(actions.REMOVE_API_KEY, removeApiKeySaga)
}
