import { put } from 'redux-saga/effects'
import { addApiKey, setKeysData, showInfoMessage, saveNewlyCreatedKey, setIsShowCreatedKeyPopup } from '../actions'
import { addKey } from '../services/addKey'

export function* addApiKeySaga(action: ReturnType<typeof addApiKey>) {
  try {
    const data = yield addKey(action.payload)

    if (data.result === 'error') {
      throw new Error(data.message)
    }

    if (data.result === 'success') {
      yield put(setKeysData(data))
      yield put(saveNewlyCreatedKey(data.keys[0], data.accessTypes[data.keys[0].accessType]))
      yield put(setIsShowCreatedKeyPopup(true))
    }
  } catch (error) {
    yield put(showInfoMessage(error.message, 'red'))
  }
}
