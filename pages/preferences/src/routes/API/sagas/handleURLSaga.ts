import { put } from 'redux-saga/effects'
import omit from 'lodash/omit'
import { addApiKey, removeApiKey } from '../actions'

const getURLParameters = (): {
  step?: string
  title?: string
  type?: number
  ID?: number
  user?: string
  faction?: string
  company?: string
  properties?: string
  market?: string
  torn?: string
} => {
  const hash = window.location.hash.replace('?', '&')

  return hash.split('&').reduce((result, item) => {
    const parts = item.split('=')

    // eslint-disable-next-line prefer-destructuring
    result[parts[0]] = parts[1]

    return result
  }, {})
}

export function* handleURLSaga() {
  const parameters = getURLParameters()
  const payload = omit(parameters, ['step', '#tab'])

  if (parameters.step === 'addNewKey') {
    yield put(addApiKey(payload))
  }

  if (parameters.step === 'removeKey') {
    yield put(removeApiKey(parameters.ID))
  }

  window.history.pushState(null, null, `${location.pathname}#tab=api`)
}
