import { injectReducer } from '../../store/reducers';

export default store => ({
  path: 'api',
  getComponent(_: any, cb: (n: null, component: object) => object) {
    (require as any).ensure(
      [],
      require => {
        const NameChange = require('./components/API').default
        const reducer = require('./reducers').default

        injectReducer(store, { reducer, key: 'api' })
        cb(null, NameChange)
      },
      'api'
    );
  }
});
