import { handleActions } from 'redux-actions'
import * as actions from '../actions/actionTypes'
import { TApiData, TKey } from '../services/getApiData'

export interface IAPIState extends TApiData {
  keySuitableForLog: boolean
  infoMessage: TInfoMessage
  loading: boolean
  newlyCreatedKey?: TKey
  newlyCreatedKeyType?: string
  isShowCreatedKeyPopup?: boolean
}

type TInfoMessage = {
  msg: string
  color: string
}

type TAction<TPayload = any> = {
  type: string
  payload: TPayload
}

const initialState: IAPIState = {
  keySuitableForLog: true,
  accessTypes: {},
  keysAmountMax: 0,
  keysAmount: 0,
  loading: false,
  infoMessage: { msg: '', color: '' },
  keys: [],
  log: {}
}

export default handleActions(
  {
    [actions.SET_KEYS_DATA](state: IAPIState, action: TAction): IAPIState {
      return {
        ...state,
        ...action.payload.data
      }
    },
    [actions.SET_KEY_SUITABLE_FOR_LOG](state: IAPIState, action: TAction): IAPIState {
      return {
        ...state,
        keySuitableForLog: action.payload
      }
    },
    [actions.SHOW_INFO_MESSAGE](state: IAPIState, action: TAction): IAPIState {
      return {
        ...state,
        infoMessage: action.payload
      }
    },
    [actions.LOADING](state: IAPIState, action: TAction): IAPIState {
      return {
        ...state,
        loading: action.payload
      }
    },
    [actions.SAVE_NEWLY_CREATED_KEY](state: IAPIState, action: TAction): IAPIState {
      return {
        ...state,
        newlyCreatedKey: action.payload.key,
        newlyCreatedKeyType: action.payload.type
      }
    },
    [actions.SET_IS_SHOW_CREATED_KEY_POPUP](state: IAPIState, action: TAction): IAPIState {
      return {
        ...state,
        isShowCreatedKeyPopup: action.payload.isShowPopup
      }
    }
  },
  initialState
)
