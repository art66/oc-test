import CoreLayout from '../layouts/CoreLayout'
import NameChange from './NameChange'
import API from './API'
import AccountClosure from './AccountClosure'

export const createRoutes = store => ({
  path: '/',
  component: CoreLayout,
  // indexRoute: () => null,
  childRoutes: [
    NameChange(store),
    API(store),
    AccountClosure(store)
  ]
})

export default createRoutes
