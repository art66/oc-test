import { createAction } from 'redux-actions'
import * as a from './actionTypes'

export const loading = createAction(a.LOADING, (loading: boolean) => ({ loading }))
export const showClosingAccountConfirmation = createAction(a.SHOW_CLOSING_ACCOUNT_CONFIRMATION)
export const hideClosingAccountConfirmation = createAction(a.HIDE_CLOSING_ACCOUNT_CONFIRMATION)
export const setAccountClosed = createAction(a.SET_ACCOUNT_CLOSED)
export const fetchAccountClosureData = createAction(a.FETCH_ACCOUNT_CLOSURE_DATA)
export const setAccountClosureData = createAction(a.SET_ACCOUNT_CLOSURE_DATA)
export const closeAccount = createAction(a.CLOSE_ACCOUNT)
export const updateTimeToWait = createAction(a.UPDATE_TIME_TO_WAIT)
