import React, { Component } from 'react'
import { connect } from 'react-redux'
import s from './styles.cssmodule.scss'
import Button from '@torn/shared/components/Button'
import { hhmmss } from '@torn/shared/utils'
import {
  closeAccount,
  fetchAccountClosureData,
  showClosingAccountConfirmation,
  hideClosingAccountConfirmation
} from '../actions'

interface IProps {
  loading: boolean
  initialLoadCompleted: boolean
  accountClosed: boolean
  closingAccountConfirmation: boolean
  timeToWait: number
  closeAccount(): void
  fetchAccountClosureData(): void
  showClosingAccountConfirmation(): void
  hideClosingAccountConfirmation(): void
}

class AccountClosure extends Component<IProps> {
  componentDidMount() {
    this.props.fetchAccountClosureData()
  }

  getContent = () => {
    const { accountClosed, timeToWait, closingAccountConfirmation, loading } = this.props
    let content

    if (accountClosed) {
      content = (
        <div>
          <p className={s.highlightedText}>Your account has been closed</p>
          <div className={s.divider} />
          <Button onClick={() => window.location.reload()}>CONTINUE</Button>
        </div>
      )
    } else if (closingAccountConfirmation) {
      content = (
        <div>
          <span className={s.highlightedText}>Are you sure you want to close your Torn account</span>
          <div className={s.divider} />
          <div>
            <Button
              onClick={this.props.closeAccount}
              className={s.confirmButtonYes}
              loading={loading}
              color='orange'
            >
              YES
            </Button>
            <Button onClick={this.props.hideClosingAccountConfirmation}>NO</Button>
          </div>
        </div>
      )
    } else {
      const disabled = timeToWait > 0
      const message = disabled
        ? (
          <p className={s.darkHighlighterdText}>
            You have recently reopened your account. You must wait {hhmmss(timeToWait)} before you can close it again
          </p>
        )
        : (
          <div className={s.highlightedText}>
            <p>
              Click below to close your account.
            </p>
            <p>
              Upon closing your account, you will no longer be able to access Torn.
            </p>
          </div>
        )

      content = (
        <div>
          {message}
          <div className={s.divider} />
          <Button
            onClick={this.props.showClosingAccountConfirmation}
            disabled={disabled}
            color='orange'
          >
            CLOSE ACCOUNT
          </Button>
        </div>
      )
    }

    return content
  }

  render() {
    const { initialLoadCompleted } = this.props
    const content = this.getContent()

    return (
      <div>
        {initialLoadCompleted && content}
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    accountClosed: state.accountClosure.accountClosed,
    loading: state.accountClosure.loading,
    initialLoadCompleted: state.accountClosure.initialLoadCompleted,
    timeToWait: state.accountClosure.timeToWait,
    closingAccountConfirmation: state.accountClosure.closingAccountConfirmation
  }),
  {
    closeAccount,
    fetchAccountClosureData,
    showClosingAccountConfirmation,
    hideClosingAccountConfirmation
  }
)(AccountClosure)
