import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'accountClosure',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const component = require('./components').default
        const reducer = require('./reducers').default

        injectReducer(store, { key: 'accountClosure', reducer })
        cb(null, component)
      },
      'accountclosure'
    )
  }
})
