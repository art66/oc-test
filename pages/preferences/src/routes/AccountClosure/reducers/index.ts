import { handleActions } from 'redux-actions'
import * as a from '../actions/actionTypes'

interface IState {
  loading: boolean
  initialLoadCompleted: boolean
  timeToWait: number
  closingAccountConfirmation: boolean
  accountClosed: boolean
}

interface IAction<TPayload = any> {
  type: string
  payload: TPayload
}

const initialState = {
  loading: false,
  initialLoadCompleted: false,
  timeToWait: 0,
  closingAccountConfirmation: false,
  accountClosed: false
}

export default handleActions(
  {
    [a.LOADING]: (state: IState, action: IAction) => ({
      ...state,
      loading: action.payload.loading
    }),
    [a.SET_ACCOUNT_CLOSURE_DATA]: (state: IState, action: IAction) => ({
      ...state,
      ...action.payload,
      initialLoadCompleted: true
    }),
    [a.SHOW_CLOSING_ACCOUNT_CONFIRMATION]: (state: IState) => ({
      ...state,
      closingAccountConfirmation: true
    }),
    [a.HIDE_CLOSING_ACCOUNT_CONFIRMATION]: (state: IState) => ({
      ...state,
      closingAccountConfirmation: false
    }),
    [a.SET_ACCOUNT_CLOSED]: (state: IState) => ({
      ...state,
      accountClosed: true
    }),
    [a.UPDATE_TIME_TO_WAIT]: (state: IState) => ({
      ...state,
      timeToWait: state.timeToWait - 1
    })
  },
  initialState
)
