import { call, put, takeLatest, delay } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { loading, setAccountClosed, setAccountClosureData, updateTimeToWait } from '../actions'
import * as actions from '../actions/actionTypes'

function* fetchAccountClosureData() {
  try {
    const data = yield fetchUrl('/preferences.php?ajax=getAccountClosureData')

    yield put(loading(false))

    if (data) {
      yield put(setAccountClosureData(data))
    }
  } catch (error) {
    console.error(error)
  }
}

function* closeAccount() {
  try {
    yield put(loading(true))
    const data = yield fetchUrl('/preferences.php?ajax=closeAccount')

    yield put(loading(false))

    if (data && !data.error) {
      yield put(setAccountClosed())
    }
  } catch (error) {
    console.error(error)
  }
}

function* timer() {
  while (true) {
    yield delay(1000)
    yield put(updateTimeToWait())
  }
}

export default function* stats() {
  yield takeLatest(actions.FETCH_ACCOUNT_CLOSURE_DATA, fetchAccountClosureData)
  yield takeLatest(actions.CLOSE_ACCOUNT, closeAccount)
  yield timer()
}
