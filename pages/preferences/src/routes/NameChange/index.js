import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'name',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const NameChange = require('./containers/NameChangeContainer').default
        const reducer = require('./modules/nameChange').default
        injectReducer(store, { key: 'namechange', reducer })
        cb(null, NameChange)
      },
      'namechange'
    )
  }
})
