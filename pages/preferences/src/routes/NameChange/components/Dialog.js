import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'
import Button from './Button'
import s from './styles.cssmodule.scss'

class Dialog extends Component {
  getButtons = () => {
    const { buttons, loading } = this.props

    return buttons.map((button, index) => {
      if (button.style === 'link') {
        return (
          <a key={index} onClick={e => button.onClick(e)} className="t-blue h c-pointer">
            {button.text}
          </a>
        )
      }

      return (
        <Button
          key={index}
          onClick={e => button.onClick(e)}
          className={cn('m-right10', s.button, { [s.loading]: loading })}
        >
          {button.text}
        </Button>
      )
    })
  }

  render() {
    const { text } = this.props
    const buttons = this.getButtons()

    return (
      <div className={s.ctDialog}>
        <div className={s.centerBlock}>
          <div className={s.text} dangerouslySetInnerHTML={{ __html: text }} />
          <div className="m-top20">{buttons}</div>
        </div>
      </div>
    )
  }
}

Dialog.propTypes = {
  text: PropTypes.string,
  textColor: PropTypes.string,
  buttons: PropTypes.array,
  loading: PropTypes.bool
}

const mapStateToProps = state => ({
  loading: state.namechange.loading
})

export default connect(mapStateToProps)(Dialog)
