import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import TornButton from '@torn/shared/components/Button'

class Button extends Component {
  render() {
    return <TornButton {...this.props} loading={this.props.loading} />
  }
}

Button.propTypes = {
  loading: PropTypes.bool
}

const mapStateToProps = state => ({
  loading: state.namechange.loading
})

export default connect(mapStateToProps)(Button)
