import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Button from '@torn/shared/components/Button'
import s from './styles.cssmodule.scss'
import Dialog from './Dialog'
const KEY_PRESS_DELAY = 1000

class NameChange extends Component {
  constructor(props) {
    super(props)
    this.state = { validatingName: false, inputValue: '' }
  }

  componentDidMount() {
    this.props.loadAppData()
  }

  handleSubmit = e => {
    e.preventDefault()
    const { nameStatus, newName } = this.props.nameChange

    if (nameStatus.type !== 'error' && newName) {
      this.props.showDialog()
    }
  }

  handleInputChange = e => {
    const { value } = e.target

    this.setState({ inputValue: value })

    if (!this.state.validatingName) {
      this.validateName(value)
    }
  }

  validateName = name => {
    const timeStart = Date.now()

    this.setState({ validatingName: true })
    this.props.checkName(name).then(() => {
      const delay = KEY_PRESS_DELAY - (Date.now() - timeStart)

      setTimeout(() => {
        if (name !== this.state.inputValue) {
          this.validateName(this.state.inputValue)
        } else {
          this.setState({ validatingName: false })
        }
      }, delay > 0 ? delay : 0)
    })
  }

  getConfirmationDialog = () => {
    const { newName, cost, defaultCost, nextChangeAvailable, user } = this.props.nameChange
    let text =
      cost === defaultCost ?
        `Are you sure you would like to change your name to <span class='bold'>${newName}</span>?
        It will cost <span class='bold t-red'>${cost} points</span> and you will
        not be able to change your name again for <span class='bold t-red'>${nextChangeAvailable} months</span>.` :
        `The name ${newName} is already taken by someone else who is inactive, you can
        acquire it now for an increased cost of <span class='t-red bold'>${cost} points</span> if you wish. You will
        not be able to change your name again for <span class='t-red bold'>${nextChangeAvailable} months</span>.`

    if (newName.toLowerCase() === user.playername.toLowerCase()) {
      text = `Are you sure you would like to change your name to <span class='bold'>${newName}</span>?
        It will cost <span class='bold t-red'>${cost} points.</span>`
    }

    return (
      <Dialog
        text={text}
        buttons={[
          { text: 'CHANGE NAME', style: 'button', onClick: () => this.props.changeName(newName) },
          {
            text: 'Cancel',
            style: 'link',
            onClick: () => {
              this.props.hideDialog()
              this.props.resetAppState()
              this.props.loadAppData()
            }
          }
        ]}
      />
    )
  }

  getResultScreen = () => {
    const { newName, cost, error } = this.props.nameChange
    const text =
      error.code === 0 && error.message ?
        `<span class='t-red'>${error.message}</span>` :
        `<span class='t-green'>You have changed your name to <span class='bold resultInfo'>${newName}</span>
        for a cost of <span class='bold resultInfo'>${cost} points</span></span>.`

    return (
      <Dialog
        text={text}
        buttons={[
          {
            text: 'OKAY',
            style: 'button',
            onClick: () => {
              this.props.hideDialog()
              this.props.resetAppState()
              this.props.loadAppData()
            }
          }
        ]}
      />
    )
  }

  getContent = () => {
    const { submitted, confirmed, nextChangeAvailable } = this.props.nameChange

    if (confirmed) {
      return this.getResultScreen()
    } else if (submitted) {
      return this.getConfirmationDialog()
    }

    const { nameStatus, defaultCost, user, newName } = this.props.nameChange
    const inputDisabled = !user.canChangeName
    const buttonDisabled = nameStatus.type === 'error' || !newName || this.state.validatingName

    return (
      <div>
        <p>
          Changing your name on the city's register is expensive and cannot be done frequently. Please take your time
          and decide carefully before proceeding.
        </p>
        <p className='m-top20'>
          Your new name can be a maximum of <span className='bold'>15 characters</span> long and only contain letters,
          numbers, dashes or underscores. It costs <span className='bold t-red'>{defaultCost} points</span> to change
          your name, this cost will increase if you request a name which is already taken by an inactive user. Upon
          changing your name, you will not be able to do so again for
          <span className='bold t-red'> {nextChangeAvailable} months</span>.
        </p>
        <div className='m-top20'>
          <form onSubmit={e => this.handleSubmit(e)}>
            <label
              htmlFor='name'
              className={cn(s.label, {
                [s[nameStatus.type]]: !this.state.validatingName,
                't-gray-9': this.state.validatingName
              })}
            >
              {this.state.validatingName ? 'Checking...' : nameStatus.message || 'Your name:'}
            </label>
            <input
              id='name'
              className={cn(s.input, { [s.disabled]: inputDisabled })}
              autoComplete='off'
              disabled={inputDisabled}
              onChange={e => this.handleInputChange(e)}
              data-lpignore='true'
            />
            <Button className={s.button} disabled={buttonDisabled}>
              CHANGE NAME
            </Button>
          </form>
        </div>
      </div>
    )
  }

  render() {
    return <div className={s.nameChange}>{this.getContent()}</div>
  }
}

NameChange.propTypes = {
  nameChange: PropTypes.shape({
    cost: PropTypes.number,
    defaultCost: PropTypes.number,
    nextChangeAvailable: PropTypes.number,
    error: PropTypes.object,
    warning: PropTypes.string,
    nameStatus: PropTypes.object,
    submitted: PropTypes.bool,
    confirmed: PropTypes.bool,
    newName: PropTypes.string,
    user: PropTypes.object
  }),
  loadAppData: PropTypes.func,
  checkName: PropTypes.func,
  changeName: PropTypes.func,
  showDialog: PropTypes.func,
  hideDialog: PropTypes.func,
  resetAppState: PropTypes.func
}

export default NameChange
