import { connect } from 'react-redux'
import { loadAppData, checkName, changeName, showDialog, hideDialog, resetAppState } from '../modules/nameChange'

import NameChange from '../components/NameChange'

const mapDispatchToProps = {
  loadAppData,
  checkName,
  changeName,
  showDialog,
  hideDialog,
  resetAppState
}

const mapStateToProps = state => ({
  nameChange: state.namechange
})

export default connect(mapStateToProps, mapDispatchToProps)(NameChange)
