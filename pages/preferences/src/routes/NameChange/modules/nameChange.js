import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'
import { get } from 'lodash'
/* global refreshTopOfSidebar */

const initialState = {
  cost: 100,
  defaultCost: 100,
  user: {
    points: 0,
    canChangeName: false
  },
  nextChangeAvailable: 3,
  nameStatus: {
    message: '',
    type: ''
  },
  warning: '',
  error: {
    code: 0,
    message: ''
  },
  submitted: false,
  confirmed: false,
  loading: false
}

export const setAppState = createAction('set app state', data => ({ data }))
export const resetAppState = createAction('reset app state')
export const setNewName = createAction('set new name', newName => ({ newName }))
export const setNameStatus = createAction('set name status', status => ({ status }))
export const showDialog = createAction('show dialog')
export const hideDialog = createAction('hide dialog')
export const confirm = createAction('confirm')
export const setError = createAction('set error', error => ({ error }))
export const loading = createAction('loading', (loading = true) => ({ loading }))

export const loadAppData = () => dispatch =>
  fetchUrl('ajax=getNameChangeData').then(data => dispatch(setAppState(data)))

export const checkName = name => dispatch =>
  fetchUrl('ajax=checkName&name=' + encodeURIComponent(name)).then(status => {
    dispatch(setNameStatus(status))
    if (status && status.type !== 'error') {
      dispatch(setNewName(name))
    }
  })

export const changeName = name => (dispatch, getState) => {
  const { cost, user, newName } = getState().namechange

  if (cost > user.points) {
    dispatch(
      setError({
        code: 0,
        message: `
        <span class='t-red'>You do not have the <span class='bold t-gray-3'>${cost} points </span>
        required to change your name to <span class='bold t-gray-3'>${newName}</span></span>
      `
      })
    )

    return dispatch(confirm())
  }

  dispatch(loading(true))

  return fetchUrl('ajax=changeName&name=' + name)
    .then(() => {
      dispatch(loading(false))
      dispatch(confirm())
      refreshTopOfSidebar()
    })
    .catch(error => {
      dispatch(loading(false))
      dispatch(setError({ code: 0, message: error.message }))
      dispatch(confirm())
    })
}

export default handleActions(
  {
    'set app state'(state, action) {
      return {
        ...state,
        ...action.payload.data
      }
    },
    'reset app state'() {
      return initialState
    },
    'set new name'(state, action) {
      return {
        ...state,
        newName: action.payload.newName
      }
    },
    'set name status'(state, action) {
      return {
        ...state,
        nameStatus: {
          message: get(action, 'payload.status.message'),
          type: get(action, 'payload.status.type')
        },
        cost: get(action, 'payload.status.cost') ?? state.cost
      }
    },
    'show dialog'(state) {
      return {
        ...state,
        submitted: true
      }
    },
    'hide dialog'(state) {
      return {
        ...state,
        submitted: false,
        confirmed: false
      }
    },
    confirm(state) {
      return {
        ...state,
        confirmed: true
      }
    },
    'set error'(state, action) {
      return {
        ...state,
        error: action.payload.error
      }
    },
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    }
  },
  initialState
)
