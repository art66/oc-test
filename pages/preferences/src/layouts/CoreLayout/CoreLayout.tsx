import React from 'react';
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'
/* global $ */

declare interface ITornWindowProps {
  $: (el: string) => any
}
declare type TTornWindow = (typeof window) & ITornWindowProps

type Props = {
  router: { push: (location: string) => void };
};

class CoreLayout extends React.Component<Props> {
  componentDidMount() {
    this.handleRoutes();
  }

  handleRoutes = () => {
    const hash = location.hash.replace(/[#\/]/g, '');
    const route = hash.substring(hash.indexOf('=') + 1);
    this.props.router.push(route || '/name');

    (window as TTornWindow).$('#prefs-tab-menu').on('tabsbeforeactivate', (_, ui) => {
      const tabName = ui.newPanel.attr('id');
      this.props.router.push('/' + tabName);
    });
  }

  render() {
    const { children } = this.props;

    return (
      <div>
        <div className='core-layout__viewport'>{children}</div>
      </div>
    );
  }
}

export default CoreLayout;
