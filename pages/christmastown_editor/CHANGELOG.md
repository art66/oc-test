# CT-Editor - Torn


## 2.7.1
 * Added typization for rootReducer store argument.

## 2.7.0
 * Added basic Skeleton for both MapsEditor and ParametersEditor Apps.

## 2.6.0
 * Added ability for MapsEditor and ParametersEditor to be injected inside CT app directly.

## 2.5.5
 * Fixed redux location action in case of outer store usage.

## 2.5.4
 * Moved some redux-* packages to the root.

## 2.5.3
 * Removed MyMaps from CTE app.

## 2.5.3
 * Fixed several imports from ChristmasTown main App.

## 2.5.2
 * Fixed inputs spaces allow in addMap/renameMap functionalities.
 * Fixed TooltipWarning disappear.

## 2.5.1
 * Fixed (prevented) current user invite opportunity.

## 2.5.0
 * Fully added tablet layout in MyMaps App.

## 2.4.5
 * Moved Debug & Info Boxes under AppHeader layout.

## 2.4.4
 * Reverted content-wrapper hardcoded class due to routing problems.

## 2.4.3
 * Reverted content-wrapper class logged-out.
 * Added sidebar root node inside template.ejs (TEMPORARY!).

## 2.4.3
 * Injected sidebar inside MyMaps app (TEMPORARY!).

## 2.4.2
 * Fixed invite link text to copy in share section of Outcome.

## 2.4.1
 * Fixed time creation while new map created to TCT time.

## 2.4.0
 * Implemented Torn Tooltips makes inside CTE.

## 2.3.0
 * Renamed dataMocks on rows in Body.
 * Improved stability of sorting rows.

## 2.2.0
 * Added debug dialog windows in a whole CTE.
 * Improved Core HMR works.

## 2.1.0
 * Replaced legacy huge lodash import in favor of particular modues including.

## 2.0.2
 * Fixed history import.
 * Bumped up react-transition-group package.

## 2.0.1
 * Fixed bug with title throwing in the AppHeader.

## 2.0.0
 * Replaced legacy SVGIconGenerator and SVGButtonGenerator components on shared ones.

## 2.0.0
 * Created first stabel verion of MyMaps Route App (with mock data).

## 1.1.0
 * Replaced legacy Header Component with global react-based Header from shared folder.

## 1.0.0
 * Replaced legacy Routing system in whole ChristmasTown Editor App.
 * Removed some legacy from common functions in cteateStore and rootRedeucer.
 * Rewritten common Components on TypeScript.
 * Added native LOCATION_CHANGE action from react-router-redux package.
 * Other minor fixes.

## 0.5.0
 * Refactored Row Component and nested children for better hover handling.

## 0.4.0
 * Created SVG icons library for CTE usage. Beta version.

## 0.3.1
 * Added Text, Release, Rating, Join section Components in Row Component of My Maps app.
 * Created all SVG Icons for using in the app with partial-reusable logic included.
 * Created styles for Components above.
 * Created initial Mocks data for Components above.
 * Minor fixes.

## 0.2.0
 * Added Body and Row Components in the My Maps app.
 * Created styles for Components above.
 * Created initial Mocks data.
 * Minor fixes.

## 0.1.1
 * Minor style fixes

## 0.1.0
 * Added Header and Columns sections in the My Maps app.
 * Created SVG Placeholder for right and left sections in Header Component.
 * Created SVG Button with API for customization usage.
 * Create Column Titles for Rows Components.
 * Added responsive layout for created Components.

## 0.0.1
 * Added New Route - "My Maps" in the CT-Editor App.
 * Created Basic folder and hierarchy structure for My Maps route.
 * Created the most explicit components for subsequent implementation.
 * Implemented Redux-Logger inside global Redux store.
 * Updated global style for global header.

