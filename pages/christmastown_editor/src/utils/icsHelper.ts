export const addICSRootElement = () => {
  const icsRootElement = document.createElement('div')

  icsRootElement.id = 'icsroot'
  document.body.append(icsRootElement)
}

export const removeICSRootElement = () => {
  const icsRootElement = document.getElementById('icsroot')

  icsRootElement.remove()
}
