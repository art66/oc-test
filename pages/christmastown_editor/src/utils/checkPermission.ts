export default (userAccessLevel, requiredAccessLevel, userRole, availableRoles) => {
  return userAccessLevel <= requiredAccessLevel && availableRoles.includes(userRole)
}
