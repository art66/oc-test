import uniqBy from 'lodash.uniqby'
import orderBy from 'lodash.orderby'
import {
  MAIN_URL,
  ADMIN_MAP_VIEWPORT_X,
  ADMIN_MAP_VIEWPORT_Y,
  FORBIDDEN_ERROR_CODE,
  FORBIDDEN_ERROR_TEXT
} from '../constants'
// require('es6-promise').polyfill()
// require('isomorphic-fetch')
/* global addRFC */

export function getColor(color) {
  return '#' + color
}

export function fetchUrl(url, data, isFullUrl) {
  const reqUrl = isFullUrl ? url : MAIN_URL + url

  return fetch(addRFC(reqUrl), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    if (response.status === FORBIDDEN_ERROR_CODE) {
      throw new Error(FORBIDDEN_ERROR_TEXT)
    }

    return response.text().then(text => {
      try {
        const json = JSON.parse(text)

        if (json && json.message && json.message.type === 'error') {
          return Promise.reject(json)
        }

        return json
      } catch (e) {
        console.log(e)
        throw new Error(`Server responded with: ${text}`)
      }
    })
  })
}

export function isInViewport(position, mapPosition, scale = 1) {
  if (!position || !mapPosition) {
    return false
  }

  let x = position.x
  let y = position.y

  let leftTop = {
    x: (mapPosition.x - 100) * scale,
    y: (mapPosition.y + 100) / scale
  }

  let rightBottom = {
    x: (mapPosition.x + ADMIN_MAP_VIEWPORT_X + 100) / scale,
    y: (mapPosition.y - ADMIN_MAP_VIEWPORT_Y - 100) / scale
  }

  return x > leftTop.x && y < leftTop.y && x < rightBottom.x && y > rightBottom.y
}

export function applyScaleEndpoints(endpoints, scale = 1) {
  return {
    leftTop: {
      x: Math.round(endpoints.leftTop.x * scale),
      y: Math.round(endpoints.leftTop.y / scale)
    },
    rightBottom: {
      x: Math.round(endpoints.rightBottom.x / scale),
      y: Math.round(endpoints.rightBottom.y / scale)
    }
  }
}

export function mergeMapElements(arr1, arr2, arr3 = []) {
  return uniqBy(orderBy(arr1.concat(arr2, arr3), ['order'], ['desc']), 'hash')
}

export function isInt(value) {
  const x = parseFloat(value)

  return !isNaN(value) && (x | 0) === x
}

export function hexToRgb(hex, opacity = 1) {
  if (typeof hex !== 'string') {
    return
  }

  hex = hex.replace(/^#/, '')

  if (hex.length === 3) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
  }

  let num = parseInt(hex, 16)

  let result = [num >> 16, (num >> 8) & 255, num & 255]
  result[3] = opacity
  result = `rgba(${result.join(',')})`

  return result
}

export function getObjectImagePath(object) {
  return `/images/v2/christmas_town/library/${object.category}/${object.type}.png`
}

export function positionsEqual(...positions) {
  return positions && positions.every(pos => pos.x === positions[0].x && pos.y === positions[0].y)
}

export function setMapPosition(position) {
  window.adminMapComponent && window.adminMapComponent.setMapPosition(position.x, position.y)
}

export function getErrorMessage(json) {
  const error = json.message && json.message.type === 'error' && json.message.text

  console.error(error)

  return error
}
