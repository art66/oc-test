import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import { fetchUrl, getErrorMessage } from '../../utils'
import { getAjaxError } from '../../routes/MapEditor/modules/mapeditor'
import s from './styles.cssmodule.scss'

interface IProps {
  label: string
  confirmMessage: string
  endpoint: string
  getAjaxError: (e: any) => void
}

interface IState {
  confirmActive: boolean
}

class ResetState extends Component<IProps, IState> {
  state = {
    confirmActive: false
  }

  _handleResetClick = () => {
    this.setState({ confirmActive: true })
  }

  _handleResetConfirm = async () => {
    const { endpoint, getAjaxError } = this.props

    try {
      await fetchUrl(endpoint)
      this.setState({ confirmActive: false })
    } catch (e) {
      getAjaxError(getErrorMessage(e))
    }
  }

  _handleResetCancel = () => {
    this.setState({ confirmActive: false })
  }

  _renderResetButton = () => {
    const { label } = this.props

    return (
      <button type='button' className={s.resetButton} onClick={this._handleResetClick}>
        <SVGIconGenerator
          iconName='RefreshCircular'
          type='twoArrows'
          preset={{ type: 'christmasTown', subtype: 'FULLSCREEN_MODE' }}
        />
        <span className={s.resetButtonText}>
          {label}
        </span>
      </button>
    )
  }

  _renderConfirmMessage = () => {
    const { confirmMessage } = this.props

    return (
      <>
        <span className={s.resetConfirmation}>
          {confirmMessage}
        </span>
        <button type='button' className={cn(s.resetButton, s.red)} onClick={this._handleResetConfirm}>
          Yes
        </button>
        <button type='button' className={s.resetButton} onClick={this._handleResetCancel}>
          No
        </button>
      </>
    )
  }

  render() {
    const { confirmActive } = this.state

    return (
      <div className={s.resetContainer}>
        {confirmActive ? this._renderConfirmMessage() : this._renderResetButton()}
      </div>
    )
  }
}

export default connect(null, { getAjaxError })(ResetState)
