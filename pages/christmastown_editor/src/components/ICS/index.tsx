import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import ICSGlobal from '@torn/ics/src/containers/ImagesCropSystemAsync'
import IInitParams from '@torn/ics/src/models/IInitParams'
import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/rootReducer'

interface IProps {
  initParams: IInitParams
}

class ICS extends Component<IProps> {
  render() {
    const { initParams } = this.props
    const icsRoot = document.getElementById('icsroot')

    if (!icsRoot) {
      return null
    }

    return ReactDOM.createPortal(
      <ICSGlobal rootStore={rootStore} injector={injectReducer} initParams={initParams} />,
      icsRoot
    )
  }
}

export default ICS
