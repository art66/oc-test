import React, { Component } from 'react'
import Dropdown from '@torn/shared/components/Dropdown'
import IDropdownData from '../../../interfaces/IDropdownData'

interface IProps {
  index: number,
  values: IDropdownData[],
  disabled: boolean,
  value: string,
  onChange: (value: any, props: any) => void
}

class GiftShop extends Component<IProps> {
  render() {
    const { index, values, value, disabled, onChange } = this.props

    return (
      <div style={{ float: 'left' }}>
        <Dropdown
          id={index.toString()}
          list={values}
          name='giftShopType'
          placeholder='Select shop type'
          disabled={disabled}
          selected={value ? { name: value } : undefined}
          onChange={onChange}
        />
      </div>
    )
  }
}

export default GiftShop
