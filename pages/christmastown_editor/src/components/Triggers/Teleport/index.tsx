import React, { Component, SyntheticEvent } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import Dropdown from '@torn/shared/components/Dropdown'
import { TELEPORT_ANIMATIONS } from '../../../constants'
import IPosition from '../../../interfaces/IPosition'
import ErrorTooltip from '../../ErrorTooltip'
import s from './styles.cssmodule.scss'

interface IProps {
  index?: number
  animation?: string
  positions?: IPosition[]
  disabled: boolean
  customErrorClass?: string
  errors: any
  handleDelete: (posIndex: number) => void
  handleAdd: () => void
  handleFocus?: () => void
  handleAnimationChange?: (value: any, props: any) => void
  handleChange: (e: SyntheticEvent, triggerIndex: number, posIndex: number) => void
}
interface IState {
  activePosition: number
}

class Teleport extends Component<IProps, IState> {
  private _deleteButtonRef: React.RefObject<HTMLButtonElement>

  static defaultProps = {
    positions: []
  }

  constructor(props: IProps) {
    super(props)

    this._deleteButtonRef = React.createRef()
    this.state = {
      activePosition: null
    }
  }

  _getIconPreset = () => {
    const { disabled } = this.props

    return !disabled ?
      { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER' } :
      { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_DISABLED' }
  }

  _getAnimationsList = () => Object.keys(TELEPORT_ANIMATIONS).map(key => ({ id: key, name: TELEPORT_ANIMATIONS[key] }))

  _handleFocus = (posIndex: number) => {
    const { handleFocus } = this.props

    handleFocus()
    this.setState({ activePosition: posIndex })
  }

  _handleBlur = e => {
    if (this._deleteButtonRef.current !== e.relatedTarget) {
      this.setState({ activePosition: null })
    }
  }

  _renderDeleteButton = (posIndex: number) => {
    const { handleDelete } = this.props

    return (
      <button
        type='button'
        className={cn(s.deleteCoord, s.spanBtn)}
        onClick={() => handleDelete(posIndex)}
        ref={this._deleteButtonRef}
      >
        <SVGIconGenerator
          iconName='Cross'
          preset={{ type: 'christmasTown', subtype: 'DELETE_COORD' }}
          onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'DELETE_COORD_HOVER' } }}
        />
      </button>
    )
  }

  _renderTeleportPositions = () => {
    const { positions, index, disabled, errors, handleChange } = this.props
    const { activePosition } = this.state

    return positions && positions.map((pos, posIndex) => {
      const error = errors[`position-${index}-${posIndex}`] || {}
      const isActive = activePosition === posIndex

      return (
        <div key={`${index}+${posIndex}`} className={s.inputPositionGroup}>
          {positions.length > 1 && activePosition === posIndex && this._renderDeleteButton(posIndex)}
          {error.message && <ErrorTooltip customClass={s.coordsError} error={error.message} />}
          <input
            type='text'
            name='x'
            placeholder='x'
            disabled={disabled}
            data-index={posIndex}
            className={cn(s.positionX, { [s.error]: error && error.xIncorrect, [s.active]: isActive })}
            onChange={(e: SyntheticEvent) => handleChange(e, index, posIndex)}
            onFocus={() => this._handleFocus(posIndex)}
            onBlur={e => this._handleBlur(e)}
            value={pos.x}
          />
          <input
            type='text'
            name='y'
            placeholder='y'
            disabled={disabled}
            data-index={posIndex}
            className={cn(s.positionY, { [s.error]: error && error.yIncorrect, [s.active]: isActive })}
            onChange={(e: SyntheticEvent) => handleChange(e, index, posIndex)}
            onFocus={() => this._handleFocus(posIndex)}
            onBlur={e => this._handleBlur(e)}
            value={pos.y}
          />
        </div>
      )
    })
  }

  render() {
    const { index, disabled, animation, handleAdd, handleAnimationChange } = this.props

    return (
      <>
        <div className={cn('m-right10'/* , s.fieldsGroup */)}>
          <Dropdown
            id={index}
            list={this._getAnimationsList()}
            className={cn('react-dropdown-default'/* , s.teleportAnimationDropdown */)}
            name='animation'
            placeholder='Select animation'
            selected={{ name: TELEPORT_ANIMATIONS[animation] }}
            disabled={disabled}
            onChange={handleAnimationChange}
          />
        </div>
        {this._renderTeleportPositions()}
        <button
          type='button'
          onClick={handleAdd}
          className={cn(s.addPosition, s.spanBtn, { [s.disabled]: disabled })}
        >
          <SVGIconGenerator
            iconName='Plus'
            preset={this._getIconPreset()}
            onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_HOVERED' } }}
          />
        </button>
      </>
    )
  }
}

export default Teleport
