// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'addPosition': string;
  'cancel': string;
  'coordsError': string;
  'deleteCoord': string;
  'disabled': string;
  'error': string;
  'globalSvgShadow': string;
  'icon': string;
  'imageIcon': string;
  'input': string;
  'inputContainer': string;
  'inputPositionGroup': string;
  'positionX': string;
  'positionY': string;
  'removeIcon': string;
  'replaceImageBtn': string;
  'spanBtn': string;
}
export const cssExports: CssExports;
export default cssExports;
