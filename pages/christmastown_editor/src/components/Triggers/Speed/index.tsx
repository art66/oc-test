import React, { Component } from 'react'
import Dropdown from '@torn/shared/components/Dropdown'
import IDropdownData from '../../../interfaces/IDropdownData'

interface IProps {
  index: number,
  values: IDropdownData[],
  disabled: boolean,
  value: string,
  onChange: (value: any, props: any) => void
}

class Speed extends Component<IProps> {
  render() {
    const { index, values, value, disabled, onChange } = this.props

    return (
      <div style={{ float: 'left' }}>
        <Dropdown
          id={index.toString()}
          list={values}
          name='speed'
          placeholder='Select speed modifier'
          disabled={disabled}
          selected={value ? { name: value } : undefined}
          onChange={onChange}
        />
      </div>
    )
  }
}

export default Speed
