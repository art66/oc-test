import React, { Component, SyntheticEvent } from 'react'
import cn from 'classnames'
import RadioButton from '../../../../../../shared/components/RadioButton'
import s from './style.cssmodule.scss'
import editorsStyles from '../../../styles/editors.cssmodule.scss'

interface IProps {
  index: number
  disabled: boolean
  time: string
  radius: string
  onTimeChange: (e: SyntheticEvent) => void
  onRadiusChange: (radius: string) => void
}

const RADIUS_3 = '3'
const RADIUS_6 = '6'
const RADIUS_9 = '9'

class GateControl extends Component<IProps> {
  render() {
    const { index, time, radius, disabled, onTimeChange, onRadiusChange } = this.props

    return (
      <div className={s.gateControlWrapper}>
        <label className={s.fieldsGroup}>
          <div className={s.label}>Active time:</div>
          <input
            type='text'
            name='activeTime'
            data-index={index}
            className={cn(s.activeTimeInput, editorsStyles.input)}
            placeholder=''
            disabled={disabled}
            onChange={onTimeChange}
            value={time || ''}
          />
        </label>
        <div className={cn(s.fieldsGroup, 'm-left10')}>
          <div className={s.label}>Control radius:</div>
          <RadioButton
            id={`radius3x3-${index}`}
            name={`radius3x3-${index}`}
            value={RADIUS_3}
            label='3x3'
            checked={radius === RADIUS_3}
            containerClassName={cn('choice-container m-right5', { disabled })}
            onChange={() => onRadiusChange(RADIUS_3)}
          />
          <RadioButton
            id={`radius6x6-${index}`}
            name={`radius6x6-${index}`}
            value={RADIUS_6}
            label='6x6'
            checked={radius === RADIUS_6}
            containerClassName={cn('choice-container m-right5', { disabled })}
            onChange={() => onRadiusChange(RADIUS_6)}
          />
          <RadioButton
            id={`radius9x9-${index}`}
            name={`radius9x9-${index}`}
            value={RADIUS_9}
            label='9x9'
            checked={radius === RADIUS_9}
            containerClassName={cn('choice-container m-right5', { disabled })}
            onChange={() => onRadiusChange(RADIUS_9)}
          />
        </div>
      </div>
    )
  }
}

export default GateControl
