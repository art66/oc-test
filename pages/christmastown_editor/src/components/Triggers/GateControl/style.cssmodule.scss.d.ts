// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'activeTimeInput': string;
  'fieldsGroup': string;
  'gateControlWrapper': string;
  'globalSvgShadow': string;
  'label': string;
  'teleportAnimationDropdown': string;
}
export const cssExports: CssExports;
export default cssExports;
