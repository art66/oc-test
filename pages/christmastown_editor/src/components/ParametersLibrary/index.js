import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import ParameterCategoryList from './ParameterCategoryList'
import ResetState from '../ResetState'

export class ParametersLibrary extends Component {
  constructor(props) {
    super(props)
    this.state = { searchValue: '' }
  }

  setFilterValue = (event) => {
    this.setState({ searchValue: event.target.value })
  }

  buildCategoryLists = (parameterCategories) => {
    const { editParameter, confirmRemovingParameter, selectLibraryParameter } = this.props
    const { searchValue } = this.state
    const parameterCategoriesList = []

    parameterCategories.forEach((category) => {
      parameterCategoriesList.push(
        <ParameterCategoryList
          key={category.name}
          category={category}
          searchValue={searchValue}
          editParameter={editParameter}
          confirmRemovingParameter={confirmRemovingParameter}
          selectLibraryParameter={selectLibraryParameter}
        />
      )
    })

    return parameterCategoriesList
  }

  _renderResetStatePanel = () => (
    <ResetState
      label='Reset chest spawns'
      confirmMessage='Are you sure you would like to reset all chest spawns?'
      endpoint='resetMapParameters'
    />
  )

  render() {
    const { parameterCategories, editParameter } = this.props
    const categoryLists = this.buildCategoryLists(parameterCategories)

    return (
      <div className={cn('parameters-container', { library: !editParameter })}>
        {!editParameter && this._renderResetStatePanel()}
        <div className='filter-wrap'>
          <input
            type='text'
            className='input-text filter-parameters'
            placeholder='Filter by name'
            onInput={this.setFilterValue}
            data-lpignore='true'
          />
          <i className='search-icon' />
        </div>
        <div className='parameters scrollarea scroll-area scrollbar-thin'>
          <div className='scrollarea-content content'>{categoryLists}</div>
        </div>
      </div>
    )
  }
}

ParametersLibrary.propTypes = {
  parameterCategories: PropTypes.array.isRequired,
  editParameter: PropTypes.func,
  confirmRemovingParameter: PropTypes.func,
  selectLibraryParameter: PropTypes.func
}

export default ParametersLibrary
