import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { selectParameterOnMap, loading } from '../../routes/MapEditor/modules/mapeditor'
import { searchParameterOnMap, fetchParametersOnMapById } from '../../routes/MapEditor/modules/mapData'
import checkPermission from '../../utils/checkPermission'
import { ACCESS_LEVEL_2 } from '../../constants/accessLevels'
import s from './styles.cssmodule.scss'

const PREV = -1
const NEXT = 1

class ParameterListItem extends React.Component {
  _handleParameterSearch = (e, modifier) => {
    e.stopPropagation()

    const {
      fetchParametersOnMapById,
      parameter,
      searchParameterOnMap,
      parametersSearch
    } = this.props
    const id = parameter.parameter_id
    const loaded = parametersSearch && parametersSearch[id] && parametersSearch[id].loaded

    if (!loaded) {
      fetchParametersOnMapById(parameter.parameter_id, modifier)
      return
    }

    searchParameterOnMap(parameter.parameter_id, modifier)
  }

  render() {
    const {
      parameter,
      category,
      selectedParameter,
      parametersSearch,
      userAccessLevel,
      userRole,
      availableMapRoles,
      selectLibraryParameter,
      editParameter,
      confirmRemovingParameter
    } = this.props
    const id = parameter.parameter_id
    const parameterCount = parametersSearch && parametersSearch[id] && parametersSearch[id].length || parameter.quantity
    const index = parametersSearch && parametersSearch[id] && parametersSearch[id].searchIndex || 0

    return (
      <li
        className={selectedParameter && selectedParameter.parameter_id === parameter.parameter_id && 'selected'}
        onClick={() => selectLibraryParameter && selectLibraryParameter(parameter)}
      >
        <div className='quantity'>
          {parameterCount >= 0 ? parameter.quantity : '-'}
        </div>
        <div className='code' style={{ color: `#${parameter.color}` }}>
          {parameter.code}
        </div>
        <div className='name'>{parameter.name}</div>
        {editParameter && confirmRemovingParameter && (
          <div className='actions-wrapper'>
            {parameter.can_edit && (
              <button className='action-button' onClick={() => editParameter(parameter, category)}>
                <SVGIconGenerator iconName='Pen' preset={{ type: 'christmasTown', subtype: 'LIBRARY_ICONS' }} />
              </button>
            )}
            {parameter.can_remove && checkPermission(userAccessLevel, ACCESS_LEVEL_2, userRole, availableMapRoles) && (
              <button className='action-button' onClick={() => confirmRemovingParameter(parameter)}>
                <SVGIconGenerator iconName='Close' preset={{ type: 'christmasTown', subtype: 'LIBRARY_ICONS' }} />
              </button>
            )}
          </div>
        )}
        {!editParameter && parameter.quantity > 0 && (
          <div className={s.searchOnMap}>
            <button onClick={e => this._handleParameterSearch(e, PREV)} className={cn(s.button)}>&lsaquo;</button>
            {index} / {parameterCount || '-'}
            <button onClick={e => this._handleParameterSearch(e, NEXT)} className={cn(s.button)}>&rsaquo;</button>
          </div>
        )}
      </li>
    )
  }
}

ParameterListItem.propTypes = {
  parameter: PropTypes.object.isRequired,
  parametersSearch: PropTypes.object,
  category: PropTypes.object.isRequired,
  selectedParameter: PropTypes.object,
  userAccessLevel: PropTypes.number,
  userRole: PropTypes.string,
  availableMapRoles: PropTypes.array,
  editParameter: PropTypes.func,
  confirmRemovingParameter: PropTypes.func,
  selectLibraryParameter: PropTypes.func,
  searchParameterOnMap: PropTypes.func,
  fetchParametersOnMapById: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  selectedParameter: state.mapeditor && state.mapeditor.mapData.selectedParameter,
  parametersSearch: state.mapeditor && state.mapeditor.mapData.parametersSearch,
  userAccessLevel: state.parametereditor && state.parametereditor.user.accessLevel,
  userRole: state.parametereditor && state.parametereditor.user.role,
  availableMapRoles: state.parametereditor && state.parametereditor.user.availableMapRoles
})

const mapActionsToProps = {
  selectParameterOnMap,
  searchParameterOnMap,
  fetchParametersOnMapById,
  loading
}

export default connect(mapStateToProps, mapActionsToProps)(ParameterListItem)
