import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import ParameterListItem from './ParameterListItem'

export class ParameterCategoryList extends PureComponent {
  buildParameterList = (category) => {
    const { editParameter, confirmRemovingParameter, selectLibraryParameter } = this.props
    const parameterList = []
    const parametersFiltered = category.parameters.filter((p) => {
      const { searchValue } = this.props
      const lowercaseSearchValue = searchValue.toLowerCase()
      const name = p.name.toLowerCase()
      const code = p.code.toLowerCase()

      return name.indexOf(lowercaseSearchValue) !== -1 || code.indexOf(lowercaseSearchValue) !== -1
    })

    parametersFiltered.forEach((parameter) => {
      parameterList.push(
        <ParameterListItem
          key={`${parameter.parameter_id}${category.name}`}
          parameter={parameter}
          category={{ id: category.id, name: category.name }}
          editParameter={editParameter}
          confirmRemovingParameter={confirmRemovingParameter}
          selectLibraryParameter={selectLibraryParameter}
        />
      )
    })

    return parameterList
  }

  render() {
    const { category } = this.props
    const parameterList = this.buildParameterList(category)

    return (
      <ul className='parameters-list'>
        {parameterList.length ? <li className='category'>{category.name}</li> : ''}
        {parameterList}
      </ul>
    )
  }
}

ParameterCategoryList.propTypes = {
  category: PropTypes.object.isRequired,
  searchValue: PropTypes.string,
  editParameter: PropTypes.func,
  confirmRemovingParameter: PropTypes.func,
  selectLibraryParameter: PropTypes.func
}

export default ParameterCategoryList
