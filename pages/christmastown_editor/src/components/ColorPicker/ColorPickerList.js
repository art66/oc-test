import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classNames from 'classnames'
import { getColor } from '../../utils'

export class ColorPickerList extends Component {
  render() {
    return (
      <ul className='colorpicker'>
        {this.props.colors.map((instance, index) => {
          const selected = instance === this.props.selectedColor

          const pickerClasses = classNames({
            selected: selected,
            'last-in-row': index + 1 === this.props.maxInRow
          })

          const radioId = `${this.props.name}-${index + 1}`

          const color = getColor(instance)

          return (
            <li className={pickerClasses} style={selected ? { borderColor: color } : {}} key={instance}>
              <input
                type='radio'
                name={this.props.name}
                value={instance}
                id={radioId}
                onClick={() => this.props.changeColor(instance)}
              />
              <label htmlFor={radioId} style={{ backgroundColor: color }} />
            </li>
          )
        })}
      </ul>
    )
  }
}

ColorPickerList.propTypes = {
  colors: PropTypes.array.isRequired,
  maxInRow: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  changeColor: PropTypes.func,
  selectedColor: PropTypes.string
}

export default ColorPickerList
