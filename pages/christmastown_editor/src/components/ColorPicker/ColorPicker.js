import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ColorPickerList from './ColorPickerList'

export class ColorPicker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: ''
    }
  }
  changeColor = color => {
    const colorUpperCase = color.toUpperCase()
    const propsSelectedUpperCase = this.props.selected.toUpperCase()
    const changedColor = propsSelectedUpperCase === colorUpperCase ? '' : colorUpperCase
    this.setState({
      color: changedColor,
      selected: propsSelectedUpperCase
    })
    this.props.onChange && this.props.onChange(changedColor, this.props)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { selected: nextPropsSelected = '' } = nextProps
    const { selected: prevSelected = '' } = prevState

    if (nextPropsSelected.toUpperCase() !== prevSelected.toUpperCase()) {
      return {
        selected: nextPropsSelected
      }
    }

    return null
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps(nextProps) {
  //   const nextPropsSelected = nextProps.selected.toUpperCase()
  //   const propsSelected = this.props.selected.toUpperCase()
  //   if (nextPropsSelected !== propsSelected) {
  //     this.setState({
  //       selected: nextPropsSelected
  //     })
  //   }
  // }
  render() {
    return (
      <ColorPickerList
        changeColor={this.changeColor}
        selectedColor={this.state.selected}
        name={this.props.name}
        colors={this.props.colors}
        maxInRow={this.props.maxInRow}
      />
    )
  }
}

ColorPicker.propTypes = {
  colors: PropTypes.array.isRequired,
  maxInRow: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  selected: PropTypes.string
}

export default ColorPicker
