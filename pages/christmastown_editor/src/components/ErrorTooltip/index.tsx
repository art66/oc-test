import React from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

interface IProps {
  customClass?: string,
  error?: string,
}

const TextField = (props: IProps) => {
  const { customClass, error } = props

  return (
    <span className={cn(customClass, s.errorTooltip)}>
      {error}
    </span>
  )
}

export default TextField
