const LEFT_ORIENTED_NPC = [
  'tiger',
  'sports4',
  'photographer',
  'cat',
  'jackal',
  'beaver',
  'wolverine',
  'deer',
  'squirrel',
  'hare',
  'raccoon',
  'moose',
  'owl',
  'fox',
  'bear',
  'mummy'
]

export default LEFT_ORIENTED_NPC
