const RIGHT_ORIENTED_NPC = [
  'tallestMan',
  'tattooedLady',
  'sports2',
  'businessman2',
  'tourist',
  'convict',
  'cowboy',
  'doctor',
  'dogWalker',
  'hunter',
  'musician2',
  'musician4',
  'transport1',
  'transport2',
  'transport3',
  'transport4',
  'transport5',
  'pug',
  'lion',
  'fish',
  'otter',
  'duck',
  'walrus',
  'seal',
  'stork',
  'bison',
  'boar',
  'weasel',
  'hedgehog',
  'lynx',
  'wolf',
  'ghost1',
  'zombie1',
  'kevinTheHoley'
]

export default RIGHT_ORIENTED_NPC
