const LIBRARY_NPC_DIMENSIONS = {
  beardedLady: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  grinch: {
    width: 12,
    height: 16,
    viewbox: '0 0 12 16'
  },
  santa: {
    width: 15,
    height: 16,
    viewbox: '-1 0 14 16'
  },
  penguin: {
    width: 14,
    height: 16,
    viewbox: '0 0 14 16'
  },
  shortestMan: {
    width: 10,
    height: 12,
    viewbox: '0 0 10 12'
  },
  strongMan: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  tallestMan: {
    width: 17,
    height: 18,
    viewbox: '0 0 17 18'
  },
  siameseTwins: {
    width: 18,
    height: 16,
    viewbox: '0 0 18 16'
  },
  tattooedLady: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  tiger: {
    width: 22,
    height: 15,
    viewbox: '0 0 25 15'
  },
  bear: {
    width: 16,
    height: 9,
    viewbox: '0 0 16 9'
  },
  beaver: {
    width: 16,
    height: 11,
    viewbox: '0 0 16 11'
  },
  bison: {
    width: 17,
    height: 12,
    viewbox: '0 0 17 12'
  },
  boar: {
    width: 16,
    height: 10,
    viewbox: '0 0 16 10'
  },
  cat: {
    width: 13,
    height: 14,
    viewbox: '0 0 13 14'
  },
  deer: {
    width: 14,
    height: 16,
    viewbox: '0 0 14 16'
  },
  duck: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  fish: {
    width: 16,
    height: 11,
    viewbox: '0 0 16 11'
  },
  fox: {
    width: 16,
    height: 7,
    viewbox: '0 0 16 7'
  },
  hare: {
    width: 12,
    height: 16,
    viewbox: '0 0 12 16'
  },
  hedgehog: {
    width: 16,
    height: 10,
    viewbox: '0 0 16 10'
  },
  jackal: {
    width: 16,
    height: 9,
    viewbox: '0 0 16 9'
  },
  lion: {
    width: 18,
    height: 14,
    viewbox: '0 0 18 14'
  },
  lynx: {
    width: 16,
    height: 12,
    viewbox: '0 0 16 12'
  },
  moose: {
    width: 16,
    height: 12,
    viewbox: '0 0 16 12'
  },
  otter: {
    width: 16,
    height: 9,
    viewbox: '0 0 16 9'
  },
  owl: {
    width: 15,
    height: 16,
    viewbox: '0 0 15 16'
  },
  pug: {
    width: 13,
    height: 14,
    viewbox: '0 0 13 14'
  },
  raccoon: {
    width: 16,
    height: 8,
    viewbox: '0 0 16 8'
  },
  seal: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  squirrel: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  stork: {
    width: 9,
    height: 16,
    viewbox: '0 0 9 16'
  },
  walrus: {
    width: 16,
    height: 11,
    viewbox: '0 0 16 11'
  },
  weasel: {
    width: 16,
    height: 11,
    viewbox: '0 0 16 11'
  },
  wolf: {
    width: 16,
    height: 9,
    viewbox: '0 0 16 9'
  },
  wolverine: {
    width: 16,
    height: 7,
    viewbox: '0 0 16 7'
  },
  astronaut: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  cowboy: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  businessman1: {
    width: 7,
    height: 16,
    viewbox: '0 0 7 16'
  },
  businessman2: {
    width: 12,
    height: 16,
    viewbox: '0 0 7 16'
  },
  gardener: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  sports1: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  sports2: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  sports3: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  sports4: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  tourist: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  photographer: {
    width: 12,
    height: 16,
    viewbox: '0 0 12 16'
  },
  convict: {
    width: 18,
    height: 16,
    viewbox: '0 0 18 16'
  },
  doctor: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  nurse: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  dogWalker: {
    width: 16,
    height: 15,
    viewbox: '0 0 16 15'
  },
  farmer: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  fisherman: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  hunter: {
    width: 14,
    height: 16,
    viewbox: '0 0 14 16'
  },
  policeMan: {
    width: 6,
    height: 16,
    viewbox: '0 0 6 16'
  },
  spearman: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  sailor: {
    width: 6,
    height: 16,
    viewbox: '0 0 6 16'
  },
  priest: {
    width: 7,
    height: 16,
    viewbox: '0 0 7 16'
  },
  nun: {
    width: 6,
    height: 16,
    viewbox: '0 0 6 16'
  },
  scientist: {
    width: 12,
    height: 16,
    viewbox: '0 0 12 16'
  },
  policeWoman: {
    width: 6,
    height: 16,
    viewbox: '0 0 6 16'
  },
  chef: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  cleaner: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  bartender1: {
    width: 15,
    height: 16,
    viewbox: '0 0 15 16'
  },
  bartender2: {
    width: 7,
    height: 16,
    viewbox: '0 0 7 16'
  },
  bartender3: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  bartender4: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  womanInRedDress1: {
    width: 6,
    height: 16,
    viewbox: '0 0 6 16'
  },
  womanInRedDress2: {
    width: 7,
    height: 16,
    viewbox: '0 0 7 16'
  },
  firefighter1: {
    width: 7,
    height: 16,
    viewbox: '0 0 7 16'
  },
  firefighter2: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  handyman1: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  handyman2: {
    width: 6,
    height: 16,
    viewbox: '0 0 6 16'
  },
  handyman3: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  handyman4: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  handyman5: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  handyman6: {
    width: 9,
    height: 16,
    viewbox: '0 0 9 16'
  },
  waitress1: {
    width: 12,
    height: 16,
    viewbox: '0 0 12 16'
  },
  waitress2: {
    width: 15,
    height: 16,
    viewbox: '0 0 15 16'
  },
  secretary1: {
    width: 6,
    height: 16,
    viewbox: '0 0 6 16'
  },
  secretary2: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  mechanic1: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  mechanic2: {
    width: 11,
    height: 16,
    viewbox: '0 0 10 16'
  },
  drunkard1: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  drunkard2: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  drunkard3: {
    width: 9,
    height: 16,
    viewbox: '0 0 9 16'
  },
  transport1: {
    width: 16,
    height: 15,
    viewbox: '0 0 16 15'
  },
  transport2: {
    width: 16,
    height: 14,
    viewbox: '0 0 16 14'
  },
  transport3: {
    width: 15,
    height: 16,
    viewbox: '0 0 15 16'
  },
  transport4: {
    width: 16,
    height: 15,
    viewbox: '0 0 16 15'
  },
  transport5: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  musician1: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  musician2: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  musician3: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  musician4: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  musician5: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  monster1: {
    width: 15,
    height: 16,
    viewbox: '0 0 15 16'
  },
  monster2: {
    width: 16,
    height: 13,
    viewbox: '0 0 16 13'
  },
  ghost1: {
    width: 16,
    height: 15,
    viewbox: '0 0 16 15'
  },
  ghost2: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  goblin1: {
    width: 12,
    height: 16,
    viewbox: '0 0 12 16'
  },
  goblin2: {
    width: 16,
    height: 13,
    viewbox: '0 0 16 13'
  },
  goblin3: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  skeleton: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  snowman: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  elf: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  scarecrow: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  mummy: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  vampire: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  zombie1: {
    width: 8,
    height: 16,
    viewbox: '0 0 8 16'
  },
  zombie2: {
    width: 9,
    height: 16,
    viewbox: '0 0 9 16'
  },
  zombie3: {
    width: 10,
    height: 16,
    viewbox: '0 0 10 16'
  },
  zombie4: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  },
  zombie5: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  kevinTheHoley: {
    width: 16,
    height: 16,
    viewbox: '0 0 20 20'
  },
  bobThePlank: {
    width: 25,
    height: 25,
    viewbox: '3 3 30 30'
  }
}

export default LIBRARY_NPC_DIMENSIONS
