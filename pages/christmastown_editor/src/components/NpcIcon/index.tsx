import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { capitalizeString } from '../../routes/MapEditor/utils'
import LIBRARY_NPC_DIMENSIONS from './libraryNpcDimensions'
import RIGHT_ORIENTED_NPC from './rightOrientedNpc'
import LEFT_ORIENTED_NPC from './leftOrientedNpc'

interface IProps {
  npcType: string
  color: string
  isMapNpc?: boolean
  isHudNpc?: boolean
  direction?: string
}

const MAP_NPC_DIMENSIONS = {
  width: 23,
  height: 23,
  viewbox: '0 -3 23 23'
}

export class NpcIcon extends Component<IProps> {
  static defaultProps = {
    isMapNpc: false
  }

  shouldComponentUpdate(nextProps: Readonly<IProps>): boolean {
    const { direction } = this.props

    return direction !== nextProps.direction && nextProps.direction !== 'top' && nextProps.direction !== 'bottom'
  }

  _getNpcIconName = () => {
    const { npcType } = this.props

    return npcType && `CTNpc${capitalizeString(npcType)}`
  }

  _getNpcIconFill = () => {
    const { color, isHudNpc, isMapNpc } = this.props

    return {
      color: `#${color}`,
      stroke: isHudNpc || isMapNpc ? 'black' : 'transparent',
      strokeWidth: isHudNpc || isMapNpc ? 1 : 0
    }
  }

  _getNpcIconDimensions = () => {
    const { npcType, isMapNpc, isHudNpc } = this.props

    if (npcType === 'bobThePlank') {
      return LIBRARY_NPC_DIMENSIONS[npcType]
    }

    return isMapNpc || isHudNpc ? MAP_NPC_DIMENSIONS : LIBRARY_NPC_DIMENSIONS[npcType]
  }

  _getCustomClass = () => {
    const { direction, npcType } = this.props
    const isLeftOriented = LEFT_ORIENTED_NPC.includes(npcType)
    const isRightOriented = RIGHT_ORIENTED_NPC.includes(npcType)

    return `${isLeftOriented ? 'leftOriented' : ''} ${isRightOriented ? 'rightOriented' : ''} ${direction}`
  }

  render() {
    return (
      <SVGIconGenerator
        customClass={this._getCustomClass()}
        iconName={this._getNpcIconName()}
        fill={this._getNpcIconFill()}
        dimensions={this._getNpcIconDimensions()}
        filter={{ active: false }}
      />
    )
  }
}

export default NpcIcon
