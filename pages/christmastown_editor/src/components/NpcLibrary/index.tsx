import React, { Component } from 'react'
import cn from 'classnames'
import INpc from '../../interfaces/INpc'
import ILibraryNpc from '../../interfaces/ILibraryNpc'
import NpcLibraryItem from './NpcLibraryItem'
import ResetState from '../ResetState'
import s from './styles.cssmodule.scss'

interface IProps {
  editor?: boolean
  libraryNpc: ILibraryNpc[]
  userAccessLevel?: number
  userRole?: string
  availableMapRoles?: string[]
  mapNpc?: INpc[]
  selectedNpc?: INpc
  editNpc?: (npc: ILibraryNpc) => void
  removeNpc?: (npc: ILibraryNpc) => void
}

interface IState {
  searchValue: string
}

export class NpcLibrary extends Component<IProps, IState> {
  static defaultProps: IProps = {
    libraryNpc: []
  }

  constructor(props: IProps) {
    super(props)
    this.state = { searchValue: '' }
  }

  setFilterValue = event => {
    this.setState({ searchValue: event.target.value })
  }

  filterListBySearchValue = (npcList: ILibraryNpc[]) => {
    const { searchValue } = this.state

    return npcList.filter(npc => npc.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1
      || npc.code.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1)
  }

  _renderLibraryNpcItems = () => {
    const {
      libraryNpc,
      mapNpc,
      selectedNpc,
      editor,
      editNpc,
      removeNpc,
      userAccessLevel,
      userRole,
      availableMapRoles
    } = this.props
    const filteredNpc = this.filterListBySearchValue(libraryNpc)

    return filteredNpc.map(npc => (
      <NpcLibraryItem
        key={`${npc.libraryId}${npc.name}`}
        editor={editor}
        npc={npc}
        mapNpc={mapNpc}
        userAccessLevel={userAccessLevel}
        userRole={userRole}
        availableMapRoles={availableMapRoles}
        selectedNpc={selectedNpc}
        removeNpc={removeNpc}
        editNpc={editNpc}
      />
    ))
  }

  _renderResetStatePanel = () => (
    <ResetState
      label='Reset NPC spawns'
      confirmMessage='Are you sure you would like to reset all NPC spawns?'
      endpoint='resetMapNpc'
    />
  )

  render() {
    const { editor } = this.props

    return (
      <div className={cn('parameters-container npc', { library: !editor })}>
        {!editor && this._renderResetStatePanel()}
        <div className={s.delayInfo}>Please keep in mind that all changes will take effect after a minute.</div>
        <div className='filter-wrap'>
          <input
            type='text'
            className='input-text filter-parameters'
            placeholder='Filter by name'
            onInput={this.setFilterValue}
            data-lpignore='true'
          />
          <i className='search-icon' />
        </div>
        <div className='parameters scrollarea scroll-area scrollbar-thin'>
          <div className='scrollarea-content content'>
            <ul className='parameters-list'>
              {this._renderLibraryNpcItems()}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default NpcLibrary
