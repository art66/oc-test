import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { SVGIconGenerator } from '@torn/shared/SVG'
import ILibraryNpc from '../../interfaces/ILibraryNpc'
import INpc from '../../interfaces/INpc'
import { selectNpc, highlightNpcOnMap } from '../../routes/MapEditor/modules/npc'
import { setMapPosition } from '../../utils'
import checkPermission from '../../utils/checkPermission'
import { ACCESS_LEVEL_2 } from '../../constants/accessLevels'
import { NPC_TYPE_SANTA, NPC_TYPE_GRINCH } from '../../constants'
import NpcIcon from '../NpcIcon'
import s from './styles.cssmodule.scss'

const PREV = -1
const NEXT = 1
const TOOLTIP_CONTENT = {
  [NPC_TYPE_SANTA]: 'Santa gives items, bucks and ornaments, and cannot be edited',
  [NPC_TYPE_GRINCH]: 'Grinch sends users to First Aid and cannot be edited'
}

interface IProps {
  editor?: boolean
  npc: ILibraryNpc
  mapNpc?: INpc[]
  userAccessLevel?: number
  userRole: string
  availableMapRoles: string[]
  selectedNpc?: INpc
  selectNpc?: (npc: ILibraryNpc, showPointer: boolean) => void
  highlightNpcOnMap?: (id: string, hash: string) => void
  editNpc?: (npc: ILibraryNpc) => void
  removeNpc?: (npc: ILibraryNpc) => void
}

interface IState {
  index: number
}

class NpcLibraryItem extends React.Component<IProps, IState> {
  state: IState = { index: 0 }

  static getDerivedStateFromProps(nextProps: IProps, prevState: IState) {
    if (nextProps.npc.quantity < prevState.index) {
      return {
        index: prevState.index - 1
      }
    }

    return null
  }

  _getNextSearchIndex = modifier => {
    const { npc } = this.props
    const { index } = this.state
    const nextIndex = index + modifier

    if (nextIndex < 1) {
      return npc.quantity
    }

    return nextIndex > npc.quantity || nextIndex <= 1 ? 1 : nextIndex
  }

  _handleNpcSearch = (e, modifier) => {
    e.stopPropagation()

    const { npc, mapNpc, highlightNpcOnMap } = this.props
    const npcById = mapNpc.filter(item => item.libraryId === npc.libraryId)
    const nextIndex = this._getNextSearchIndex(modifier)
    const foundNpc = npcById[nextIndex - 1]

    this.setState({ index: nextIndex })
    highlightNpcOnMap(foundNpc.libraryId, foundNpc.hash)
    setMapPosition(foundNpc.position)
  }

  _renderActionButtons = () => {
    const { npc, editor, editNpc, userAccessLevel, userRole, availableMapRoles } = this.props
    const canDelete = checkPermission(userAccessLevel, ACCESS_LEVEL_2, userRole, availableMapRoles)
      && (editor && npc.outcomes.find(outcome => outcome.canEdit) || npc.outcomes && !npc.outcomes.length)

    if (editor && (npc.type.id === NPC_TYPE_SANTA || npc.type.id === NPC_TYPE_GRINCH)) {
      return (
        <div className='actions-wrapper'>
          <button type='button' className='action-button' id={`description-type-${npc.type.id}`}>
            <SVGIconGenerator iconName='Pen' type='crossedOut' preset={{ type: 'christmasTown', subtype: 'NO_EDIT' }} />
          </button>
          <Tooltip
            parent={`description-type-${npc.type.id}`}
            arrow='center'
            position='top'
          >
            <span className={s.tooltipContent}>{TOOLTIP_CONTENT[npc.type.id]}</span>
          </Tooltip>
        </div>
      )
    }

    return editor && npc.canEdit && (
      <div className='actions-wrapper'>
        <button type='button' className='action-button' onClick={() => editNpc(npc)}>
          <SVGIconGenerator iconName='Pen' preset={{ type: 'christmasTown', subtype: 'LIBRARY_ICONS' }} />
        </button>
        {canDelete && this._renderDeleteButton()}
      </div>
    )
  }

  _renderDeleteButton = () => {
    const { npc, removeNpc } = this.props

    return (
      <button type='button' className='action-button' onClick={() => removeNpc(npc)}>
        <SVGIconGenerator iconName='Close' preset={{ type: 'christmasTown', subtype: 'LIBRARY_ICONS' }} />
      </button>
    )
  }

  _renderPagination = () => {
    const { editor, npc } = this.props
    const { index } = this.state

    return !editor && npc.quantity > 0 && (
      <div className={s.searchOnMap}>
        <button type='button' onClick={e => this._handleNpcSearch(e, PREV)} className={s.button}>&lsaquo;</button>
        {index} / {npc.quantity || '-'}
        <button type='button' onClick={e => this._handleNpcSearch(e, NEXT)} className={s.button}>&rsaquo;</button>
      </div>
    )
  }

  render() {
    const { npc, editor, selectedNpc, selectNpc } = this.props

    return (
      <li
        className={cn({ selected: selectedNpc && selectedNpc.libraryId === npc.libraryId })}
        onClick={() => !editor && selectNpc(npc, true)}
      >
        <div className='quantity'>
          {npc.quantity >= 0 ? npc.quantity : '-'}
        </div>
        <div className='code'>
          <NpcIcon npcType={npc.type.name} color={npc.color} />
        </div>
        <div className='name'>{npc.name}</div>
        {this._renderActionButtons()}
        {this._renderPagination()}
      </li>
    )
  }
}

const mapActionsToProps = {
  selectNpc,
  highlightNpcOnMap
}

export default connect(null, mapActionsToProps)(NpcLibraryItem)
