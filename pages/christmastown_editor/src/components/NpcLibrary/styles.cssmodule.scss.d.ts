// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'button': string;
  'delayInfo': string;
  'globalSvgShadow': string;
  'searchOnMap': string;
  'tooltipContent': string;
}
export const cssExports: CssExports;
export default cssExports;
