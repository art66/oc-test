import React from 'react'

import { IProps } from './interfaces'
import styles from './index.cssmodule.scss'

class Skeleton extends React.PureComponent<IProps> {
  render() {
    const { bodyType } = this.props

    return (
      <div className={`${styles.skeletonWrap}  ${styles[`${bodyType}Skeleton`]}`}>
        <div className={styles.skeletonHeader} />
        <div className={styles.skeletonBody} />
      </div>
    )
  }
}

export default Skeleton
