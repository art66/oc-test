export const skeletonWrap: string;
export const mapEditorSkeleton: string;
export const paramEditorSkeleton: string;
export const skeletonHeader: string;
export const skeletonBody: string;
