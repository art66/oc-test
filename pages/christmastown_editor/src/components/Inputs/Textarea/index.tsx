import React, { Component, SyntheticEvent } from 'react'
import cn from 'classnames'
import ErrorTooltip from '../../ErrorTooltip'
import s from './styles.cssmodule.scss'

interface IProps {
  index?: number
  name?: string
  className?: string
  customErrorClass?: string
  placeholder?: string
  disabled?: boolean
  value?: any
  error?: string
  maxLength?: number
  onChange?: (e: SyntheticEvent) => void
  onFocus?: (e: SyntheticEvent) => void
  onClick?: (e: SyntheticEvent) => void
  onBlur?: (e: SyntheticEvent) => void
}

class Textarea extends Component<IProps> {
  _handleChange = e => {
    const { maxLength, onChange } = this.props

    if (e.target.value.length > maxLength) {
      return
    }

    onChange(e)
  }

  render() {
    const { index, className, value, error, customErrorClass, ...props } = this.props

    return (
      <div className={s.textareaContainer}>
        {error && <ErrorTooltip customClass={customErrorClass} error={error} />}
        <textarea
          {...props}
          data-index={index}
          className={cn({ [className]: true, [s.error]: error })}
          value={value || ''}
          onChange={this._handleChange}
        />
      </div>
    )
  }
}

export default Textarea
