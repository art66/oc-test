import React, { Component, SyntheticEvent } from 'react'
import cn from 'classnames'
import ErrorTooltip from '../../ErrorTooltip'
import s from './styles.cssmodule.scss'

interface IProps {
  index?: number
  name?: string
  className?: string
  customErrorClass?: string
  customContainerClass?: string
  placeholder?: string
  disabled?: boolean
  value?: any
  error?: string
  maxLength?: number
  onChange?: (e: SyntheticEvent) => void
  onFocus?: (e: SyntheticEvent) => void
  onClick?: (e: SyntheticEvent) => void
  onBlur?: (e: SyntheticEvent) => void
  onMouseEnter?: (e: SyntheticEvent) => void
  onMouseLeave?: (e: SyntheticEvent) => void
}

class TextField extends Component<IProps> {
  _handleChange = e => {
    const { maxLength, onChange } = this.props

    if (e.target.value.length > maxLength) {
      return
    }

    onChange(e)
  }

  render() {
    const { index, className, value, error, customErrorClass, customContainerClass, ...props } = this.props

    return (
      <div className={cn(s.inputContainer, customContainerClass)}>
        {error && <ErrorTooltip customClass={customErrorClass} error={error} />}
        <input
          {...props}
          data-index={index}
          type='text'
          className={cn({ [className]: true, [s.error]: error })}
          value={value}
          onChange={this._handleChange}
          data-lpignore='true'
          autoComplete='off'
        />
      </div>
    )
  }
}

export default TextField
