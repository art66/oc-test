import IPosition from '../routes/MapEditor/interfaces/IPosition'

export default interface INpcOutcome {
  chance: string
  type: number
  text: string
  canEdit: boolean
  positions?: IPosition[]
  animation?: string
  globe?: string
}
