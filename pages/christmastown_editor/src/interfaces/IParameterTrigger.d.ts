import IPosition from './IPosition'

export default interface IParameterTrigger {
  chance: string,
  text?: string,
  type: string,
  speed?: string,
  giftShopType?: string,
  positions?: IPosition[],
  animation?: string
}
