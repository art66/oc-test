import IMapEndpoints from './IMapEndpoints'

export default interface IMap {
  id: string,
  name: string,
  size: IMapEndpoints
}
