import IParameterTrigger from './IParameterTrigger'

export default interface ILibraryParameter {
  allowItemSpawn: boolean,
  can_edit: boolean,
  can_remove: boolean,
  code: string,
  color?: string,
  name: string,
  parameter_id: number,
  quantity: number,
  triggers: IParameterTrigger[]
}
