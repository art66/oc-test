import IPosition from './IPosition'

export default interface IMapEndpoints {
  leftTop: IPosition,
  rightBottom: IPosition
}
