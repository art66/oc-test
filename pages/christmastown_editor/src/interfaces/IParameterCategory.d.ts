import ILibraryParameter from './ILibraryParameter'

export default interface IParameterCategory {
  id: string,
  name: string,
  parameters?: ILibraryParameter[]
}
