import IPosition from '../routes/MapEditor/interfaces/IPosition'

export default interface INpc {
  name: string
  type: {
    id: number
    name: string
  }
  color: string
  created: number | null
  libraryId: string
  hash: string
  position: IPosition
  radius: number
  speed: number
}
