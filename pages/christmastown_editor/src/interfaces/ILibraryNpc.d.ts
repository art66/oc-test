import INpcOutcome from './INpcOutcome'

export default interface ILibraryNpc {
  libraryId: string
  type: {
    id: number,
    name: string
  }
  name: string
  code: string
  speed: number
  color: string
  radius: number
  quantity: number
  created?: number
  canEdit: boolean
  outcomes: INpcOutcome[]
}
