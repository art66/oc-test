export default interface IDropdownData {
  id?: number,
  name: string,
  availableValues?: any[]
}
