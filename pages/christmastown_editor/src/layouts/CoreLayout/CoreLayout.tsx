import React, { PureComponent } from 'react'
import { match, RouteComponentProps } from 'react-router'

import AppHeader from '@torn/shared/components/AppHeader'
import DebugBox from '@torn/shared/components/DebugBox'
import InfoBox from '@torn/shared/components/InfoBox'
import checkMediaType from '@torn/shared/utils/checkMediaType'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

import Tooltips from '../../components/Tooltips'
import { IInitialState } from '../../store/initialState'
import { UNSUPPORTED_DEVICE_MESSAGE } from '../../constants'

import './CoreLayout.scss'
import '../../styles/core.scss'

interface IProps extends RouteComponentProps, IInitialState {
  children: any
  mediaType: TMediaType
  match: match<{ myParam: string }>
  infoBoxShow: (msg: string) => void
  infoBoxHide: () => void
}

class CoreLayout extends PureComponent<IProps> {
  componentDidMount() {
    this._checkUnsupportedDevice()
  }

  componentDidUpdate() {
    this._checkUnsupportedDevice()
  }

  _checkUnsupportedDevice = () => {
    const { infoBoxShow, infoBoxHide } = this.props

    this._unsupportedDevice() ? infoBoxShow(UNSUPPORTED_DEVICE_MESSAGE) : infoBoxHide()
  }

  _unsupportedDevice = () => {
    const { mediaType } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    return !isDesktop
  }

  _renderBody = () => {
    const { children } = this.props

    return (
      <div className='core-layout__viewport'>
        {children}
      </div>
    )
  }

  render() {
    const { appID, pageID, debug, info, debugCloseAction } = this.props

    return (
      <div>
        <Tooltips />
        <AppHeader appID={appID} pageID={pageID} />
        {debug && <DebugBox debugMessage={debug} close={debugCloseAction} isBeatifyError={true} />}
        {info && <InfoBox msg={info} />}
        {!this._unsupportedDevice() && this._renderBody()}
      </div>
    )
  }
}

export default CoreLayout
