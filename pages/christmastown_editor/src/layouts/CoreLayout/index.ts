import { Store } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { IInitialState } from '../../store/initialState'
import { infoShow, infoHide, debugHide } from '../../store/actions'
import CoreLayout from './CoreLayout'

interface IStore {
  common: IInitialState
  browser: any
}

const mapStateToProps = (state: IStore & Store) => ({
  appTitle: state.common.appTitle,
  appID: state.common.appID,
  pageID: state.common.pageID,
  debug: state.common.debug,
  info: state.common.info,
  mediaType: state.browser.mediaType
})

const mapDispatchToState = dispatch => ({
  debugCloseAction: () => dispatch(debugHide()),
  infoBoxShow: (msg: string) => dispatch(infoShow(msg)),
  infoBoxHide: () => dispatch(infoHide())
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToState
  )(CoreLayout)
)
