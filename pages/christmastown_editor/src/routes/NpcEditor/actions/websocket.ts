import {
  npcCreated,
  npcEdited,
  npcRemoved,
  updateNpcQuantity
} from './index'

const WS_SAVE_MAP_NPC_TYPE = 'saveMapNpc'
const WS_REMOVE_MAP_NPC_TYPE = 'removeMapNpc'
const COUNTER_INCREMENT = 1
const COUNTER_DECREMENT = -1

function initWS(dispatch: any) {
  // @ts-ignore
  const handler = new WebsocketHandler('cTownAdmin')

  const handleHistoryParamUpdate = (action, reverse = false) => {
    const saveNpcCountChange = reverse ? COUNTER_DECREMENT : -COUNTER_DECREMENT
    const removeNpcCountChange = reverse ? COUNTER_INCREMENT : -COUNTER_INCREMENT

    switch (action.type) {
      case WS_SAVE_MAP_NPC_TYPE:
        dispatch(updateNpcQuantity(action.payload.savedMapNpc.libraryId, saveNpcCountChange))
        break
      case WS_REMOVE_MAP_NPC_TYPE:
        dispatch(updateNpcQuantity(action.payload.removedMapNpc.libraryId, removeNpcCountChange))
        break
      default:
        return null
    }
  }

  handler.setActions({
    saveLibraryNpc: payload => {
      dispatch(npcCreated(payload.actionData.savedLibraryNpc))
    },
    updateLibraryNpc: payload => {
      dispatch(npcEdited(payload.actionData.updatedLibraryNpc))
    },
    removeLibraryNpc: payload => {
      dispatch(npcRemoved(payload.actionData.removedLibraryNpcId))
    },
    saveMapNpc: data => {
      dispatch(updateNpcQuantity(data.data.action.payload.savedMapNpc.libraryId, COUNTER_INCREMENT))
    },
    removeMapNpc: data => {
      dispatch(updateNpcQuantity(data.data.action.payload.removedMapNpc.libraryId, COUNTER_DECREMENT))
    },
    undo: data => {
      handleHistoryParamUpdate(data.data.action, true)
    },
    redo: data => {
      handleHistoryParamUpdate(data.data.action)
    }
  })
}

export default initWS
