import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import reducer from './reducers'
import rootStore from '../../store/createStore'
import Preloader from '@torn/shared/components/Preloader'
import initWS from './actions/websocket'
import CTStore from '../../../../christmastown/src/store/createStore'
import { injectReducer as CTInjectReducer } from '../../../../christmastown/src/store/helpers/rootReducer'

const PreloaderComponent = () => <Preloader />

const NpcEditorRoute = Loadable({
  loader: async() => {
    const NpcEditor = await import('./components/NpcEditor')

    return NpcEditor
  },
  render(asyncComponent: any, { isSPA }: { isSPA: boolean }) {
    const { default: NpcEditor } = asyncComponent
    const store = isSPA ? CTStore : rootStore
    const reducerInjector = isSPA ? CTInjectReducer : injectReducer

    reducerInjector(store, { reducer, key: 'npceditor' })
    initWS(store.dispatch)

    return <NpcEditor />
  },
  loading: PreloaderComponent
})

export default NpcEditorRoute
