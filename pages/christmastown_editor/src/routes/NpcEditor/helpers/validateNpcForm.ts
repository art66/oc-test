import { capitalizeString } from '../../MapEditor/utils'
import { TYPE_TELEPORT, TYPE_CHEST_CLUE, CHEST_CLUE_CODE, MAX_TEXTAREA_LINE_BREAKS } from '../constants'
import * as messages from '../constants/messages'

export default (values, validationProps) => {
  const { speed, radius, outcomes } = values
  const { mapSize } = validationProps
  const fields = ['type']
  const textFields = ['name', 'code']
  const errors: any = {}

  fields.forEach(field => {
    if (!values[field]) {
      errors[field] = `${capitalizeString(field)} ${messages.ERROR_REQUIRED}`
    }
  })

  textFields.forEach(field => {
    if (!values[field] || !values[field].trim()) {
      errors[field] = `${capitalizeString(field)} ${messages.ERROR_REQUIRED}`
      return
    }

    if (!values[field].match(/^[A-Za-z0-9\-_.]+$/i)) {
      errors[field] = messages.ERROR_CHARACTERS
    }
  })
  if (
    radius !== '0'
    && radius !== 0
    && speed === null
    || +speed < validationProps.speed.min
    || +speed > validationProps.speed.max
    || Number.isNaN(+speed)
  ) {
    errors.speed = `Speed must be ${validationProps.speed.min}-${validationProps.speed.max}`
  }

  if (
    speed !== '0'
    && speed !== 0
    && radius === null
    || +radius < validationProps.radius.min
    || +radius > validationProps.radius.max
    || Number.isNaN(+radius)
  ) {
    errors.radius = `Radius must be integer ${validationProps.radius.min}-${validationProps.radius.max}`
  }

  outcomes.forEach((outcome, index) => {
    const messageErrorKey = `message-${index}`
    const chanceErrorKey = `chance-${index}`

    if (!outcome.text || !outcome.text.length) {
      errors[messageErrorKey] = messages.ERROR_OUTCOME_MESSAGE
    }

    if (outcome.text && outcome.text.split('\n').length > MAX_TEXTAREA_LINE_BREAKS) {
      errors[`message-${index}`] = messages.ERROR_OUTCOME_LINE_BREAKS
    }

    if (
      outcome.chance === ''
      || +outcome.chance < validationProps.chance.min
      || +outcome.chance > validationProps.chance.max
      || !Number.isInteger(+outcome.chance)
    ) {
      errors[chanceErrorKey] = `Chance must be integer ${validationProps.chance.min}-${validationProps.chance.max}`
    }

    if (outcome.type === TYPE_CHEST_CLUE && (!outcome.text || !outcome.text.includes(CHEST_CLUE_CODE))) {
      errors[messageErrorKey] = messages.ERROR_OUTCOME_CHEST_CLUE
    }

    if (outcome.type === TYPE_TELEPORT) {
      outcome.positions.forEach((pos, posIndex) => {
        const positionErrorKey = `position-${index}-${posIndex}`

        errors[positionErrorKey] = {}
        if (outcome.positions.slice(0, posIndex).find(prevPos => prevPos.x === pos.x && prevPos.y === pos.y)) {
          errors[positionErrorKey].xIncorrect = true
          errors[positionErrorKey].yIncorrect = true
          errors[positionErrorKey].message = messages.ERROR_OUTCOME_COORDINATES_EXIST
        }

        if (pos.x === '' || pos.x < mapSize.leftTop.x || pos.x > mapSize.rightBottom.x || isNaN(+pos.x)) {
          errors[positionErrorKey].xIncorrect = true
          errors[positionErrorKey].message = messages.ERROR_OUTCOME_COORDINATES
        }

        if (pos.y === '' || pos.y > mapSize.leftTop.y || pos.y < mapSize.rightBottom.y || isNaN(+pos.y)) {
          errors[positionErrorKey].yIncorrect = true
          errors[positionErrorKey].message = messages.ERROR_OUTCOME_COORDINATES
        }
      })
    }
  })

  return errors
}
