import React, { Component } from 'react'
import cn from 'classnames'
import Slider from 'react-slick'
import Tooltip from '@torn/shared/components/Tooltip'
import IDropdownData from '../../../../interfaces/IDropdownData'
import { capitalizeString } from '../../../MapEditor/utils'
import NpcIcon from '../../../../components/NpcIcon'
import ErrorTooltip from '../../../../components/ErrorTooltip'
import s from './styles.cssmodule.scss'

interface IProps {
  npcForm: any
  npcs: IDropdownData[]
  error: string
  formEditable: boolean
  updateFormData: (newValues: any) => void
  resetErrors: () => void
}

const NPC_COLOR_DEFAULT = 'aaa'
const NPC_COLOR_ACTIVE = '66a80f'
const MAX_SLIDER_ITEMS = 13
const SLIDER_WIDTH = 570
const SLIDER_ITEM_WIDTH = 35

class NpcSlider extends Component<IProps> {
  _getSlidesCount = () => {
    const { npcs } = this.props
    const isNpcsExceedsSliderLimit = npcs.length > MAX_SLIDER_ITEMS

    return isNpcsExceedsSliderLimit ? MAX_SLIDER_ITEMS : npcs.length
  }

  _getSlideMargin = () => Math.ceil(SLIDER_WIDTH / this._getSlidesCount() - SLIDER_ITEM_WIDTH)

  sliderIsActive = () => this.props.npcs.length > MAX_SLIDER_ITEMS

  _getSliderSettings = () => {
    const isNpcsExceedsSliderLimit = this.sliderIsActive()
    const slidesCount = this._getSlidesCount()

    return {
      infinite: false,
      slidesToShow: slidesCount,
      slidesToScroll: slidesCount,
      arrows: isNpcsExceedsSliderLimit,
      variableWidth: true,
      touchMove: false,
      swipe: false,
      dots: true,
      speed: 500,
      initialSlide: 0
    }
  }

  _getNpcIconName = npcName => {
    return `CTNpc${capitalizeString(npcName)}`
  }

  _getActiveNpc = () => {
    const { npcs, npcForm } = this.props

    return npcs.find(npc => npc.id === npcForm.data.type)
  }

  _handleNpcClick = (name, value) => {
    const { updateFormData, resetErrors } = this.props

    resetErrors()
    updateFormData({ [name]: value })
  }

  _renderActiveNpc = () => {
    const activeNpc = this._getActiveNpc()

    return activeNpc && this._renderNpcIcon(activeNpc.name, true)
  }

  _renderNpcIcon = (npcName, isActive) => (
    <NpcIcon
      color={isActive ? NPC_COLOR_ACTIVE : NPC_COLOR_DEFAULT}
      npcType={npcName}
    />
  )

  _renderSliderItem = (npc, activeNpc) => (
    <div
      key={`${npc.name}-${npc.id}`}
      id={`${npc.name}-${npc.id}`}
      className={cn({ [s.npcSliderItem]: true, [s.active]: activeNpc === npc.id })}
      style={{ marginRight: this._getSlideMargin() }}
      onClick={() => this._handleNpcClick('type', npc.id)}
    >
      <Tooltip
        parent={`${npc.name}-${npc.id}`}
        arrow='center'
        position='top'
      >
        <span className='tipContent'>{capitalizeString(npc.name)}</span>
      </Tooltip>
      {this._renderNpcIcon(npc.name, activeNpc === npc.id)}
    </div>
  )

  _renderSlider = activeNpc => {
    const { npcs } = this.props

    return !!npcs.length && (
      <Slider {...this._getSliderSettings()}>
        {npcs.map(npc => this._renderSliderItem(npc, activeNpc))}
      </Slider>
    )
  }

  render() {
    const { formEditable, npcForm, error } = this.props
    const { type } = npcForm.data

    return (
      <>
        {error && <ErrorTooltip error={error} />}
        <div className={cn(s.npcSlider, { 'active-slider': this.sliderIsActive() })}>
          <div className={cn({ [s.sliderContent]: true, [s.disabled]: !formEditable })}>
            {this._renderSlider(type)}
          </div>
        </div>
      </>
    )
  }
}

export default NpcSlider
