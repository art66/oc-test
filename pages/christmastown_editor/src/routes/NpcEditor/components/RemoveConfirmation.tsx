import React from 'react'
import cn from 'classnames'
import ILibraryNpc from '../../../interfaces/ILibraryNpc'
import s from './styles.cssmodule.scss'

interface IProps {
  npc: ILibraryNpc
  removeNpcConfirmed: (id: string) => void
  removeNpcCanceled: () => void
}

export const removeConfirmationMessage = (props: IProps) => {
  const { npc, removeNpcConfirmed, removeNpcCanceled } = props

  return (
    <div className='parameter-editor-info-msg remove-confirmation'>
      <h3>
        Are you sure you want to remove NPC {npc.name} [{npc.code}] from the Library?
      </h3>
      <button
        type='button'
        className={cn('link-blue', s.spanBtn)}
        onClick={() => removeNpcConfirmed(npc.libraryId)}
      >
        Yes
      </button>
      <button
        type='button'
        className={cn('link-blue', s.spanBtn)}
        onClick={removeNpcCanceled}
      >
        No
      </button>
    </div>
  )
}

export default removeConfirmationMessage
