import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import IInitParams from '@torn/ics/src/models/IInitParams'
import {
  updateFormData,
  addOutcome,
  cancelEditingNpc,
  sendFormData,
  setFormErrors
} from '../actions'
import {
  toggleICS,
  setICSInitParams
} from '../../../store/actions'
import IDropdownData from '../../../interfaces/IDropdownData'
import IMapEndpoints from '../../../interfaces/IMapEndpoints'
import INpcForm from '../interfaces/INpcForm'
import ILibraryNpc from '../../../interfaces/ILibraryNpc'
import checkPermission from '../../../utils/checkPermission'
import { ACCESS_LEVEL_3 } from '../../../constants/accessLevels'
import { MAX_COLORS_IN_ROW, MAX_NUMBER_INPUT_LENGTH, MAX_INPUT_LENGTH } from '../constants'
import validateForm from '../helpers/validateNpcForm'
import ColorPicker from '../../../components/ColorPicker'
import NpcSlider from './Slider'
import FormOutcomes from './FormOutcomes'
import FormFooter from './NpcFormFooter'
import TextField from '../../../components/Inputs/TextField'
import editorsStyles from '../../../styles/editors.cssmodule.scss'
import s from './styles.cssmodule.scss'

interface IProps {
  npcForm: INpcForm
  colors: string[]
  outcomes: IDropdownData[]
  npcs: IDropdownData[]
  mapSize: IMapEndpoints
  validationValues: any
  userAccessLevel: number
  userRole: string
  availableMapRoles: string[]
  updateFormData: (newValues: any) => void
  addOutcome: () => void
  cancelEditingNpc: () => void
  sendFormData: (data: ILibraryNpc) => void
  setFormErrors: (errors: {}) => void
  toggleICS: (show: boolean) => void
  setICSInitParams: (initParams: IInitParams) => void
}

interface IState {
  showRadiusTooltip: boolean
}

class NpcForm extends Component<IProps, IState> {
  state = {
    showRadiusTooltip: false
  }

  _getActiveNpc = () => {
    const { npcs, npcForm } = this.props

    return npcs.find(npc => +npc.id === +npcForm.data.type)
  }

  _handleSpeedOrRadiusChange = (e, fieldName) => {
    const { updateFormData } = this.props
    const nullValues = { speed: null, radius: null }
    const newValues = +e.target.value === 0 ?
      { ...nullValues, [fieldName]: e.target.value } :
      { [fieldName]: e.target.value }

    updateFormData(newValues)
  }

  _handleFormSubmit = () => {
    const { npcForm, mapSize, validationValues, sendFormData, setFormErrors } = this.props
    const errors = validateForm(npcForm.data, { mapSize, ...validationValues })
    const hasErrors = Object.keys(errors)
      .find(key => typeof errors[key] === 'string' || errors[key].xIncorrect || errors[key].yIncorrect)

    if (hasErrors) {
      setFormErrors(errors)
    } else {
      sendFormData(npcForm.data)
    }
  }

  _handleRadiusTooltipFocus = () => {
    const { setFormErrors } = this.props

    this._handleRadiusTooltipHide()
    setFormErrors({})
  }

  _handleRadiusTooltipShow = () => {
    this.setState({ showRadiusTooltip: true })
  }

  _handleRadiusTooltipHide = () => {
    this.setState({ showRadiusTooltip: false })
  }

  _renderOutcomes = (npcForm, formEditable) => {
    const { outcomes, userAccessLevel, setFormErrors, userRole, availableMapRoles } = this.props
    const { data, errors } = npcForm

    return data.outcomes && data.outcomes.map((outcome, index) => (
      <FormOutcomes
        key={`outcome-${index}`}
        index={index}
        outcomes={outcomes}
        outcomeData={outcome}
        userAccessLevel={userAccessLevel}
        userRole={userRole}
        availableMapRoles={availableMapRoles}
        formEditable={formEditable}
        errors={errors}
        resetErrors={() => setFormErrors({})}
      />
    ))
  }

  _renderRadiusTooltip = () => (
    <span className={s.radiusInputTooltip}>
      Radius 1 = 3x3 cells, 2 = 5x5 cells etc. Insert 0 to keep NPC in one cell
    </span>
  )

  render() {
    const {
      npcForm,
      colors,
      npcs,
      addOutcome,
      cancelEditingNpc,
      updateFormData,
      userAccessLevel,
      setFormErrors,
      userRole,
      availableMapRoles
    } = this.props
    const { showRadiusTooltip } = this.state
    const { name, code, color, speed, radius } = npcForm.data
    const formEditable = checkPermission(userAccessLevel, ACCESS_LEVEL_3, userRole, availableMapRoles)
    const addOutcomeIconPreset = {
      type: 'christmasTown',
      subtype: !formEditable ? 'ADD_REMOVE_TRIGGER_DISABLED' : 'ADD_REMOVE_TRIGGER'
    }

    return (
      <form>
        <div className={s.mainSection}>
          <div className={s.mainOptions}>
            <NpcSlider
              npcForm={npcForm}
              npcs={npcs}
              formEditable={formEditable}
              updateFormData={updateFormData}
              error={npcForm.errors.type}
              resetErrors={() => setFormErrors({})}
            />
            <TextField
              name='name'
              className={s.inputName}
              customContainerClass={editorsStyles.inputContainer}
              placeholder='Name'
              disabled={!formEditable}
              onChange={(e: any) => updateFormData({ [e.target.name]: e.target.value })}
              value={name || ''}
              error={npcForm.errors.name}
              maxLength={MAX_INPUT_LENGTH}
              onFocus={() => setFormErrors({})}
            />
            <TextField
              name='code'
              className={s.inputCode}
              customContainerClass={editorsStyles.inputContainer}
              placeholder='Code'
              disabled={!formEditable}
              onChange={(e: any) => updateFormData({ [e.target.name]: e.target.value })}
              value={code || ''}
              error={npcForm.errors.code}
              maxLength={MAX_INPUT_LENGTH}
              onFocus={() => setFormErrors({})}
            />
            <TextField
              name='speed'
              className={s.inputSpeed}
              customContainerClass={editorsStyles.inputContainer}
              placeholder='Speed'
              disabled={!formEditable || (parseInt(radius, 10) === 0 && speed === null)}
              onChange={(e: any) => this._handleSpeedOrRadiusChange(e, 'speed')}
              value={speed === null ? '' : speed}
              error={npcForm.errors.speed}
              maxLength={MAX_NUMBER_INPUT_LENGTH}
              onFocus={() => setFormErrors({})}
            />
            <div className={s.radiusInputWrapper}>
              {showRadiusTooltip && this._renderRadiusTooltip()}
              <TextField
                name='radius'
                className={s.inputRadius}
                customContainerClass={editorsStyles.inputContainer}
                placeholder='Radius'
                disabled={!formEditable || (parseFloat(speed) === 0 && radius === null)}
                onChange={(e: any) => this._handleSpeedOrRadiusChange(e, 'radius')}
                value={radius === null ? '' : radius}
                error={npcForm.errors.radius}
                maxLength={MAX_NUMBER_INPUT_LENGTH}
                onMouseEnter={this._handleRadiusTooltipShow}
                onMouseLeave={this._handleRadiusTooltipHide}
                onFocus={this._handleRadiusTooltipFocus}
              />
            </div>
            <div className={cn({ [s.colorPickerWrapper]: true, [s.disabled]: !formEditable })}>
              <ColorPicker
                colors={colors}
                selected={color || ''}
                maxInRow={MAX_COLORS_IN_ROW}
                name='color'
                onChange={(value, props) => updateFormData({ [props.name]: value })}
              />
            </div>
          </div>
        </div>
        {this._renderOutcomes(npcForm, formEditable)}
        <div className={s.addOutcomeWrapper}>
          <button
            type='button'
            className={cn(s.addOutcome, editorsStyles.spanBtn, { [s.disabled]: !formEditable })}
            onClick={addOutcome}
          >
            <SVGIconGenerator
              iconName='Plus'
              preset={addOutcomeIconPreset}
              onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_HOVERED' } }}
              customClass={s.icon}
            />
            Add another outcome
          </button>
        </div>
        <FormFooter
          disabled={!formEditable}
          onSubmit={this._handleFormSubmit}
          onCancel={cancelEditingNpc}
          toggleICS={this.props.toggleICS}
          setICSInitParams={this.props.setICSInitParams}
          npcForm={npcForm}
          updateFormData={updateFormData}
        />
      </form>
    )
  }
}

const mapStateToProps = (state: any) => ({
  npcForm: state.npceditor.form,
  colors: state.npceditor.colors,
  outcomes: state.npceditor.outcomes,
  npcs: state.npceditor.npcs,
  mapSize: state.npceditor.map.size,
  userAccessLevel: state.npceditor.user.accessLevel,
  userRole: state.npceditor.user.role,
  availableMapRoles: state.npceditor.user.availableMapRoles,
  validationValues: state.npceditor.validationValues
})

const mapActionsToProps = {
  updateFormData,
  addOutcome,
  cancelEditingNpc,
  sendFormData,
  setFormErrors,
  toggleICS,
  setICSInitParams
}

export default connect(mapStateToProps, mapActionsToProps)(NpcForm)
