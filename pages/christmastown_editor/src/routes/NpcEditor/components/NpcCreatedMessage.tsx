import React from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

interface IProps {
  message: string,
  createNpc: () => void
}

export const npcCreatedMessage = (props: IProps) => {
  const { createNpc, message } = props

  return (
    <div className='parameter-editor-info-msg'>
      <h3>{message}</h3>
      <button
        type='button'
        className={cn('link-blue', s.spanBtn)}
        onClick={createNpc}
      >
        Create new NPC
      </button>
    </div>
  )
}

export default npcCreatedMessage
