import React, { Component } from 'react'
import { connect } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'
import ILibraryNpc from '../../../interfaces/ILibraryNpc'
import IMap from '../../../interfaces/IMap'
import NpcLibrary from '../../../components/NpcLibrary'
import NpcForm from './NpcForm'
import NpcCreatedMessage from './NpcCreatedMessage'
import RemoveConfirmation from './RemoveConfirmation'
import {
  fetchNpc,
  createNpc,
  editNpc,
  removeNpc,
  removeNpcConfirmed,
  removeNpcCanceled,
  checkNpcTabsSync
} from '../actions'
import checkPermission from '../../../utils/checkPermission'
import { ACCESS_LEVEL_3 } from '../../../constants/accessLevels'
import '../../../styles/christmas_town.css'
import '../../../styles/christmas_town_admin.css'
import s from './styles.cssmodule.scss'

interface IProps {
  npcForm: any
  map: IMap
  libraryNpc: ILibraryNpc[]
  userAccessLevel: number
  userRole: string
  availableMapRoles: string[]
  loadingInitData: boolean
  selectedNpc: ILibraryNpc
  checkNpcTabsSync: (mapID: string) => void
  editNpc: (npc: ILibraryNpc) => void
  removeNpc: (npc: ILibraryNpc) => void
  removeNpcConfirmed: (id: string) => void
  removeNpcCanceled: () => void
  createNpc: () => void
  fetchNpc: () => void
}

export class NpcEditor extends Component<IProps> {
  componentDidMount() {
    this.props.fetchNpc()
    window.addEventListener('focus', this._handleTabChange)
  }

  componentWillUnmount() {
    window.removeEventListener('focus', this._handleTabChange)
  }

  _handleTabChange = () => {
    const { map } = this.props

    map.id && this.props.checkNpcTabsSync(map.id)
  }

  _renderConfirmationMessage = () => {
    const { npcForm } = this.props

    return (
      <RemoveConfirmation
        npc={npcForm.confirmMessage.npc}
        removeNpcConfirmed={this.props.removeNpcConfirmed}
        removeNpcCanceled={this.props.removeNpcCanceled}
      />
    )
  }

  render() {
    const { npcForm, map, libraryNpc, userAccessLevel, userRole, availableMapRoles, loadingInitData } = this.props

    if (loadingInitData) {
      return <Preloader />
    }

    return (
      checkPermission(userAccessLevel, ACCESS_LEVEL_3, userRole, availableMapRoles)
      && map && (
        <div>
          <div className='ct-wrap ct-admin-wrap'>
            <div className='editor-container'>
              <div className='title-wrap clearfix'>
                <span className='text left'>{map.name}</span>
              </div>
              <div className={s.editorContent}>
                {!npcForm.added ? (
                  <NpcForm />
                ) : (
                  <NpcCreatedMessage createNpc={this.props.createNpc} message={npcForm.message} />
                )}
                {npcForm.confirmMessage.show && this._renderConfirmationMessage()}
              </div>
            </div>
            <div className='tools-container npc'>
              <div className='title-wrap clearfix'>
                <span className='text'>Library</span>
              </div>
              <NpcLibrary
                libraryNpc={libraryNpc}
                editor={true}
                userAccessLevel={userAccessLevel}
                userRole={userRole}
                availableMapRoles={availableMapRoles}
                editNpc={this.props.editNpc}
                removeNpc={this.props.removeNpc}
              />
            </div>
          </div>
        </div>
      )
    )
  }
}

const mapStateToProps = (state: any) => ({
  npcForm: state.npceditor.form,
  libraryNpc: state.npceditor.libraryNpc,
  map: state.npceditor.map,
  userAccessLevel: state.npceditor.user && state.npceditor.user.accessLevel,
  userRole: state.npceditor.user && state.npceditor.user.role,
  availableMapRoles: state.npceditor.user && state.npceditor.user.availableMapRoles,
  loadingInitData: state.npceditor.loadingInitData
})

const mapActionsToProps = {
  fetchNpc,
  createNpc,
  editNpc,
  removeNpc,
  removeNpcConfirmed,
  removeNpcCanceled,
  checkNpcTabsSync
}

export default connect(mapStateToProps, mapActionsToProps)(NpcEditor)
