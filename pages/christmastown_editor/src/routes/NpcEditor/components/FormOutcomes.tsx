import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import Dropdown from '@torn/shared/components/Dropdown'
import INpcOutcome from '../../../interfaces/INpcOutcome'
import IDropdownData from '../../../interfaces/IDropdownData'
import * as constants from '../constants'
import * as messages from '../constants/messages'
import { ACCESS_LEVEL_1 } from '../../../constants/accessLevels'
import checkPermission from '../../../utils/checkPermission'
import { removeOutcome, updateOutcomes, updateOutcomeType, updateOutcomePositions } from '../actions'
import Textarea from '../../../components/Inputs/Textarea'
import TextField from '../../../components/Inputs/TextField'
import Teleport from '../../../components/Triggers/Teleport'
import editorsStyles from '../../../styles/editors.cssmodule.scss'
import s from './styles.cssmodule.scss'

interface IProps {
  index: number
  outcomes: IDropdownData[]
  errors: any
  outcomeData: INpcOutcome
  userAccessLevel: number
  userRole: string
  availableMapRoles: string[]
  formEditable: boolean
  updateOutcomes: (name: string, value: any, index: number) => void
  updateOutcomeType: (type: any, index: number) => void
  updateOutcomePositions: (name: string, value: number, outcomeIndex: number, positionIndex: number) => void
  removeOutcome: (index: number) => void
  resetErrors: () => void
}

export class FormOutcomes extends Component<IProps> {
  _getIconPreset = () => {
    const { formEditable } = this.props

    return formEditable ?
      { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER' } :
      { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_DISABLED' }
  }

  _handleOutcomeMessageChange = e => {
    this.props.updateOutcomes(e.target.name, e.target.value, +e.target.getAttribute('data-index'))
  }

  _handleOutcomeChanceChange = e => {
    if (!isNaN(+e.target.value)) {
      this.props.updateOutcomes(e.target.name, e.target.value, +e.target.getAttribute('data-index'))
    }
  }

  _handleOutcomeTypeChange = (value, props) => {
    this.props.updateOutcomeType(value.id, +props.id)
  }

  _handleSnowGlobeChange = (value, props) => {
    this.props.updateOutcomes('globe', value.name, +props.id)
  }

  _handleTeleportAnimationChange = (value, props) => {
    this.props.updateOutcomes(props.name, value.id, +props.id)
  }

  _handleOutcomePositionChange = (e, outcomeIndex, positionIndex) => {
    if (!isNaN(+e.target.value) || e.target.value === '-') {
      this.props.updateOutcomePositions(e.target.name, e.target.value, outcomeIndex, positionIndex)
    }
  }

  _handleAddPosition = () => {
    const { index, outcomeData, updateOutcomes } = this.props

    updateOutcomes('positions', [...outcomeData.positions, constants.EMPTY_POSITION], +index)
  }

  _handleDeletePosition = (posIndex: number) => {
    const { index, outcomeData, updateOutcomes } = this.props

    updateOutcomes('positions', outcomeData.positions.filter((_, i) => i !== +posIndex), +index)
  }

  _renderChestClueMessage = () => (
    <span className={s.insertClueMessage}>{messages.MESSAGE_INSERT_CLUE}</span>
  )

  _renderTeleportFields = () => {
    const { index, outcomeData, formEditable, resetErrors, errors } = this.props

    return (
      <Teleport
        index={index}
        positions={outcomeData.positions}
        animation={outcomeData.animation}
        disabled={!formEditable}
        errors={errors}
        handleDelete={(posIndex: number) => this._handleDeletePosition(posIndex)}
        handleAdd={this._handleAddPosition}
        handleFocus={resetErrors}
        handleAnimationChange={this._handleTeleportAnimationChange}
        handleChange={(e, outcomeIndex, positionIndex) => this._handleOutcomePositionChange(e, outcomeIndex, positionIndex)}
      />
    )
  }

  _renderSnowGlobesDropdown = () => {
    const { index, outcomes, outcomeData, formEditable } = this.props
    const { globe } = outcomeData
    const snowGlobesOutcome = outcomes.find(outcome => outcome.id === constants.TYPE_SNOW_GLOBE)

    if (!snowGlobesOutcome) return undefined

    return (
      <Dropdown
        id={index.toString()}
        list={snowGlobesOutcome.availableValues || []}
        name='type'
        placeholder='Select Snow Globe'
        disabled={!formEditable}
        selected={globe ? { name: globe } : undefined}
        onChange={this._handleSnowGlobeChange}
        onInit={this._handleSnowGlobeChange}
      />
    )
  }

  render() {
    const {
      index,
      outcomes,
      errors,
      outcomeData,
      userAccessLevel,
      removeOutcome,
      formEditable,
      resetErrors,
      userRole,
      availableMapRoles
    } = this.props
    const { text, chance, type } = outcomeData
    const outcomeType = outcomes.find(outcome => outcome.id === type)
    const typeDropdownDisabled = !formEditable
      || (type === constants.TYPE_SNOW_GLOBE && !checkPermission(userAccessLevel, ACCESS_LEVEL_1, userRole, availableMapRoles))

    return (
      <div className={s.outcomesSection}>
        {type === constants.TYPE_CHEST_CLUE && this._renderChestClueMessage()}
        <Textarea
          index={index}
          name='text'
          disabled={!formEditable}
          className={s.messageInput}
          onChange={this._handleOutcomeMessageChange}
          value={text || ''}
          error={errors[`message-${index}`]}
          maxLength={constants.MAX_TEXTAREA_LENGTH}
          onFocus={resetErrors}
        />
        <div className={s.outcomeInputsWrapper}>
          <TextField
            name='chance'
            index={index}
            disabled={!formEditable}
            className={s.chanceInput}
            customContainerClass={editorsStyles.inputContainer}
            customErrorClass={s.chanceError}
            onChange={this._handleOutcomeChanceChange}
            value={chance}
            error={errors[`chance-${index}`]}
            onFocus={resetErrors}
          />
          <div className={s.outcomeDropdown}>
            <Dropdown
              id={index.toString()}
              list={outcomes || []}
              name='type'
              placeholder='Select outcome'
              disabled={typeDropdownDisabled}
              selected={type && outcomeType ? { name: outcomeType.name } : undefined}
              onChange={this._handleOutcomeTypeChange}
            />
          </div>
          {type === constants.TYPE_TELEPORT && this._renderTeleportFields()}
          {type === constants.TYPE_SNOW_GLOBE && this._renderSnowGlobesDropdown()}
        </div>
        <button
          type='button'
          className={cn(s.removeOutcome, s.spanBtn, { [s.disabled]: !formEditable })}
          onClick={() => removeOutcome(index)}
        >
          <SVGIconGenerator
            iconName='Close'
            preset={this._getIconPreset()}
            onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_HOVERED' } }}
            customClass={s.icon}
          />
          Remove outcome
        </button>
      </div>
    )
  }
}

const mapActionsToProps = {
  removeOutcome,
  updateOutcomes,
  updateOutcomeType,
  updateOutcomePositions
}

export default connect(null, mapActionsToProps)(FormOutcomes)
