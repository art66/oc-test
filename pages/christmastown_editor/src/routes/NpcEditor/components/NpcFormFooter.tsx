import React, { Component } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import IInitParams from '@torn/ics/src/models/IInitParams'
import INpcForm from '../interfaces/INpcForm'
import editorsStyles from '../../../styles/editors.cssmodule.scss'
import sTip from '../../../styles/tooltips.cssmodule.scss'
import s from './styles.cssmodule.scss'

interface IProps {
  disabled: boolean
  onSubmit: () => void
  onCancel: () => void
  npcForm: INpcForm
  toggleICS: (show: boolean) => void
  setICSInitParams: (initParams: IInitParams) => void
  updateFormData: (newValues: any) => void
}

class NpcFormFooter extends Component<IProps> {
  componentDidMount() {
    this.initTooltips()
  }

  componentWillUnmount() {
    tooltipsSubscriber.unsubscribe(this.getImagePreviewTooltip())
  }

  componentDidUpdate() {
    this.updateTooltips()
  }

  getImagePreviewTooltip = () => {
    const { npcForm: { data: { imageData, imageUrl } } } = this.props

    return {
      ID: 'replace-npc-image',
      child: (
        <div className={cn(sTip.imagePreviewWrap, sTip.withFilter)}>
          <img className={sTip.imagePreview} src={imageData || imageUrl} />
        </div>
      ),
      customConfig: {
        overrideStyles: {
          container: cn(sTip.imagePreviewTooltipContainer, sTip.container),
          title: sTip.imagePreviewTooltipTitle
        }
      }
    }
  }

  updateTooltips = () => {
    tooltipsSubscriber.render({
      tooltipsList: [this.getImagePreviewTooltip()],
      type: 'update'
    })
  }

  initTooltips = () => {
    const tooltipsData = [
      this.getImagePreviewTooltip(),
      {
        child: 'Custom image will be replaced by default one',
        ID: 'remove-npc-image'
      }
    ]

    tooltipsData.forEach(item => {
      tooltipsSubscriber.subscribe(item)
    })
  }

  _handleImageUpload = (data) => this.props.updateFormData({ imageData: data })

  _handleRemoveImage = () => this.props.updateFormData({ imageData: null, imageUrl: null, imagePath: null })

  _handleOpenICS = () => {
    const { toggleICS: openICS, setICSInitParams: setInitParams, npcForm } = this.props
    const initParams = {
      imageType: 'christmasTownNpc',
      callback: this._handleImageUpload,
      parameters: {
        npcId: npcForm.data.id || null
      }
    }

    openICS(true)
    setInitParams(initParams)
  }

  renderImageButtons = () => {
    const { disabled, npcForm: { data: { imageData, imageUrl } } } = this.props
    const removeEditImageIconPreset = {
      type: 'christmasTown',
      subtype: disabled ? 'REMOVE_EDIT_IMAGE_DISABLED' : 'REMOVE_EDIT_IMAGE_DEFAULT'
    }
    const removeEditImageIconHoverPreset = {
      active: true,
      preset: {
        type: 'christmasTown',
        subtype: 'REMOVE_EDIT_IMAGE_HOVERED'
      }
    }

    return imageData || imageUrl ? (
      <>
        <button
          id='replace-npc-image'
          type='button'
          className={cn(editorsStyles.spanBtn, editorsStyles.replaceImageBtn)}
          onClick={this._handleOpenICS}
          disabled={disabled}
        >
          <SVGIconGenerator
            iconName='Image'
            type='edit'
            preset={removeEditImageIconPreset}
            onHover={removeEditImageIconHoverPreset}
            customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
          />
          Replace character image
        </button>
        <button
          id='remove-npc-image'
          type='button'
          className={editorsStyles.spanBtn}
          onClick={this._handleRemoveImage}
          disabled={disabled}
        >
          <SVGIconGenerator
            iconName='Image'
            type='remove'
            preset={removeEditImageIconPreset}
            onHover={removeEditImageIconHoverPreset}
            customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
          />
          Remove character image
        </button>
      </>
    ) : (
      <button type='button' className={editorsStyles.spanBtn} onClick={this._handleOpenICS} disabled={disabled}>
        <SVGIconGenerator
          iconName='Image'
          preset={{ type: 'christmasTown', subtype: disabled ? 'UPLOAD_IMAGE_DISABLED' : 'UPLOAD_IMAGE_DEFAULT' }}
          onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'UPLOAD_IMAGE_HOVERED' } }}
          customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
        />
        Upload character image
      </button>
    )
  }

  render() {
    const { disabled, onSubmit, onCancel, npcForm: { fetching } } = this.props

    return (
      <div className={s.formFooter}>
        <div className={cn('save-cancel-container', s.saveCancelContainer)}>
          <div className={s.actionsWrap}>
            {this.renderImageButtons()}
          </div>
          <div className={s.buttonsWrap}>
            <button
              type='button'
              className={cn(s.cancelButton, editorsStyles.spanBtn, { disabled })}
              onClick={onCancel}
            >
              Cancel
            </button>
            <button
              type='button'
              className='btn-blue big'
              disabled={disabled || fetching}
              onClick={onSubmit}
            >
              SAVE
            </button>
          </div>
        </div>
      </div>
    )
  }
}

export default NpcFormFooter
