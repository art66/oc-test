import initialState from './initialState'
import * as a from '../actions/actionTypes'
import * as constants from '../constants'

const handleInitialData = ({ libraryNpc, npcs, colors, outcomes, map, user, validationValues }) => {
  return { libraryNpc, npcs, colors, outcomes, map, user, validationValues }
}

const ACTION_HANDLERS = {
  [a.FETCH_NPC_SUCCESS]: (state, { payload }) => {
    return {
      ...state,
      ...handleInitialData(payload),
      loadingInitData: false
    }
  },

  [a.FETCH_NPC_ERROR]: state => {
    return {
      ...state,
      loadingInitData: false
    }
  },

  [a.SYNCHRONIZE_NPC_DATA]: (state, { payload }) => {
    return {
      ...state,
      ...handleInitialData(payload),
      form: {
        ...state.form,
        confirmMessage: initialState.form.confirmMessage,
        data: state.form.data.libraryId ? initialState.form.data : state.form.data,
      },
      error: ''
    }
  },

  [a.UPDATE_NPC_FORM_DATA]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          ...payload
        }
      }
    }
  },

  [a.SET_MESSAGE]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        added: true,
        data: initialState.form.data,
        message: payload,
        confirmMessage: {
          show: false
        }
      }
    }
  },

  [a.CREATE_NPC]: state => {
    return {
      ...state,
      form: {
        ...state.form,
        added: false,
        data: initialState.form.data
      }
    }
  },

  [a.NPC_CREATED]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        errors: {}
      },
      libraryNpc: [
        ...state.libraryNpc,
        payload
      ]
    }
  },

  [a.EDIT_NPC]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        added: false,
        data: {
          ...payload,
          type: payload.type.id
        },
        confirmMessage: {
          show: false
        },
        errors: {}
      }
    }
  },

  [a.NPC_EDITED]: (state, { payload }) => {
    return {
      ...state,
      libraryNpc: state.libraryNpc.map(npc => npc.libraryId === payload.libraryId ? { ...payload } : npc)
    }
  },

  [a.CANCEL_EDITING_NPC]: state => {
    return {
      ...state,
      form: {
        ...state.form,
        data: initialState.form.data,
        added: true,
        message: '',
        confirmMessage: { show: false },
        errors: {}
      }
    }
  },

  [a.REMOVE_NPC_FROM_LIBRARY]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        confirmMessage: {
          show: true,
          npc: payload
        }
      }
    }
  },

  [a.NPC_REMOVED]: (state, { payload }) => {
    return {
      ...state,
      libraryNpc: state.libraryNpc.filter(npc => npc.libraryId !== payload)
    }
  },

  [a.REMOVE_NPC_CANCELED]: state => {
    return {
      ...state,
      form: {
        ...state.form,
        confirmMessage: {
          show: false
        },
        added: false,
        message: ''
      }
    }
  },

  [a.UPDATE_NPC_QUANTITY]: (state, { payload }) => {
    return {
      ...state,
      libraryNpc: state.libraryNpc.map(npc => {
        return npc.libraryId === payload.npcID ?
          {
            ...npc,
            quantity: npc.quantity + payload.value
          } :
          npc
      })
    }
  },

  [a.UPDATE_OUTCOMES]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          outcomes: state.form.data.outcomes.map((outcome, index) => payload.index === index ? {
            ...state.form.data.outcomes[index],
            [payload.name]: payload.value
          } : outcome)
        }
      }
    }
  },

  [a.UPDATE_OUTCOME_TYPE]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          outcomes: state.form.data.outcomes.map((outcome, index) => {
            const { chance, text } = state.form.data.outcomes[index]
            const defaultFields = { chance, text }
            const defaultAdditionalFields = constants.DEFAULT_ADDITIONAL_SETTINGS_BY_TYPE[payload.type]

            return payload.index === index ? {
              ...defaultFields,
              ...defaultAdditionalFields,
              type: payload.type
            } : outcome
          })
        }
      }
    }
  },

  [a.UPDATE_OUTCOME_POSITIONS]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          outcomes: state.form.data.outcomes.map((outcome, outcomeIndex) => {
            if (payload.outcomeIndex === outcomeIndex) {
              return {
                ...state.form.data.outcomes[outcomeIndex],
                positions: outcome.positions.map((position, positionIndex) => {
                  if (payload.positionIndex === positionIndex) {
                    return {
                      ...outcome.positions[positionIndex],
                      [payload.name]: payload.value
                    }
                  } else {
                    return position
                  }
                })
              }
            } else {
              return outcome
            }
          })
        }
      }
    }
  },

  [a.REMOVE_OUTCOME]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          outcomes: [
            ...state.form.data.outcomes.slice(0, payload.index),
            ...state.form.data.outcomes.slice(payload.index + 1)
          ]
        },
        errors: {}
      }
    }
  },

  [a.ADD_OUTCOME]: state => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          outcomes: [...state.form.data.outcomes, { type: constants.TYPE_MESSAGE }]
        }
      }
    }
  },

  [a.SET_NPC_FORM_ERRORS]: (state, { payload }) => {
    return {
      ...state,
      form: {
        ...state.form,
        errors: payload
      }
    }
  },

  [a.IS_FETCHING_DATA]: (state, action) => {
    return {
      ...state,
      form: {
        ...state.form,
        fetching: action.payload.fetching
      }
    }
  }
}

const reducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
