export default {
  form: {
    added: false,
    confirmMessage: {
      show: false,
      npc: {}
    },
    data: { outcomes: [{ chance: 1, type: 3 }] },
    errors: {},
    fetching: false
  },
  loadingInitData: true,
  libraryNpc: [],
  colors: [],
  outcomes: [
    {
      id: 1,
      name: 'Tr1'
    }
  ],
  npcs: [],
  map: {
    id: '',
    name: '',
    size: {
      leftTop: {},
      rightBottom: {}
    }
  },
  user: {},
  ics: {}
}
