import ILibraryNpc from '../../../interfaces/ILibraryNpc'

export default interface INpcForm {
  added: boolean
  confirmMessage: {
    show: boolean
    npc: ILibraryNpc
  }
  data: any
  errors: any
  fetching: boolean
}
