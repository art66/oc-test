import { put, takeEvery } from 'redux-saga/effects'
import * as a from '../actions/actionTypes'
import * as messages from '../constants/messages'
import { fetchUrl, getErrorMessage } from '../../../utils'
import { debugShow, debugHide } from '../../../store/actions'
import {
  fetchNpcSuccess,
  synchronizeNpcData,
  setMessage,
  fetchNpcError, isFetchingData
} from '../actions'
import { getRouteHeader } from '../../../../../christmastown/src/controller/actions'

interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* removeNpc(action: IAction<{ payload: string }>) {
  try {
    const { payload } = action

    yield fetchUrl('removeLibraryNpc', { id: payload })
    yield put(setMessage(messages.MESSAGE_NPC_REMOVED))
    yield put(debugHide())
  } catch (error) {
    yield put(debugShow(getErrorMessage(error)))
  }
}

function* fetchNpc() {
  try {
    const response = yield fetchUrl('npcEditor')

    yield put(fetchNpcSuccess(response))
  } catch (error) {
    yield put(fetchNpcError())
    yield put(debugShow(getErrorMessage(error)))
  } finally {
    yield put(getRouteHeader())
  }
}

function* checkTabsSync(action: IAction<{ mapID: string }>) {
  try {
    const mapID = action.payload
    const { mapChanged, npcEditorData } = yield fetchUrl('checkNpcEditorSynchronization', { mapID })

    if (mapChanged) {
      yield put(synchronizeNpcData(npcEditorData))
    }
  } catch (error) {
    yield put(debugShow(getErrorMessage(error)))
  }
}

function* sendFormData(action: any) {
  try {
    const { payload } = action

    yield put(isFetchingData(true))

    if (payload.libraryId) {
      yield fetchUrl('updateLibraryNpc', { npc: { id: payload.libraryId, ...payload } })
      yield put(setMessage(`${payload.name} ${messages.MESSAGE_NPC_UPDATED}`))
    } else {
      yield fetchUrl('saveLibraryNpc', { npc: payload })
      yield put(setMessage(messages.MESSAGE_NPC_CREATED))
    }

    yield put(debugHide())
  } catch (error) {
    yield put(debugShow(getErrorMessage(error)))
  } finally {
    yield put(isFetchingData(false))
  }
}

export default function* npcEditor() {
  yield takeEvery(a.REMOVE_NPC_CONFIRMED, removeNpc)
  yield takeEvery(a.FETCH_NPC, fetchNpc)
  yield takeEvery(a.SEND_FORM_DATA, sendFormData)
  yield takeEvery(a.CHECK_NPC_TABS_SYNC, checkTabsSync)
}
