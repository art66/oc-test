export const TYPE_TELEPORT = 1
export const TYPE_CHEST_CLUE = 2
export const TYPE_MESSAGE = 3
export const TYPE_SNOW_GLOBE = 7
export const EMPTY_POSITION = { x: '', y: '' }
export const CHEST_CLUE_CODE = '{clue}'
export const MAX_COLORS_IN_ROW = 7
export const MAX_NUMBER_INPUT_LENGTH = 4
export const MAX_INPUT_LENGTH = 50
export const MAX_TEXTAREA_LENGTH = 150
export const MAX_TEXTAREA_LINE_BREAKS = 3

export const DEFAULT_CHEST_CLUE_VALUE = { text: CHEST_CLUE_CODE }
export const DEFAULT_TELEPORT_VALUE = { positions: [{ ...EMPTY_POSITION }], animation: 'default' }
export const DEFAULT_ADDITIONAL_SETTINGS_BY_TYPE = {
  [TYPE_TELEPORT]: DEFAULT_TELEPORT_VALUE,
  [TYPE_CHEST_CLUE]: DEFAULT_CHEST_CLUE_VALUE
}
