import { TELEPORT } from '../../../constants'
import * as messages from '../constants/messages'

export default (values, validationProps) => {
  const { name, code, triggers } = values
  const { mapSize } = validationProps
  const errors: any = {}

  if (!name || !name.length) {
    errors.name = messages.ERROR_NAME_REQUIRED
  }

  if (!code || !code.length) {
    errors.code = messages.ERROR_CODE_REQUIRED
  }

  triggers.forEach((trigger, index) => {
    if (!trigger.text || !trigger.text.length) {
      errors[`text-${index}`] = messages.ERROR_TRIGGER_TEXT_REQUIRED
    }

    if (!trigger.chance || +trigger.chance < 1 || +trigger.chance > 100 || !Number.isInteger(+trigger.chance)) {
      errors[`chance-${index}`] = messages.ERROR_TRIGGER_CHANCE
    }

    if (trigger.type === TELEPORT) {
      trigger.positions.forEach((pos, posIndex) => {
        const positionErrorKey = `position-${index}-${posIndex}`

        errors[positionErrorKey] = {}
        if (trigger.positions.slice(0, posIndex).find(prevPos => prevPos.x === pos.x && prevPos.y === pos.y)) {
          errors[positionErrorKey].xIncorrect = true
          errors[positionErrorKey].yIncorrect = true
          errors[positionErrorKey].message = messages.ERROR_TRIGGER_COORDINATES_EXIST
        }

        if (pos.x === '' || pos.x < mapSize.leftTop.x || pos.x > mapSize.rightBottom.x || isNaN(+pos.x)) {
          errors[positionErrorKey].xIncorrect = true
          errors[positionErrorKey].message = messages.ERROR_TRIGGER_COORDINATES
        }

        if (pos.y === '' || pos.y > mapSize.leftTop.y || pos.y < mapSize.rightBottom.y || isNaN(+pos.y)) {
          errors[positionErrorKey].yIncorrect = true
          errors[positionErrorKey].message = messages.ERROR_TRIGGER_COORDINATES
        }
      })
    }
  })

  return errors
}
