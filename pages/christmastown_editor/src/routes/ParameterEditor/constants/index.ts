import { TELEPORT, GATE_CONTROL, SPEED, GAME_GIFT_SHOP } from '../../../constants'

export const CATEGORY_MAIN = 1
export const INITIAL_TRIGGER_DATA = { chance: 1, type: 'no trigger', text: '' }
export const EMPTY_POSITION = { x: '', y: '' }
export const DEFAULT_TELEPORT_VALUE = { positions: [{ ...EMPTY_POSITION }], animation: 'default' }
export const DEFAULT_GATE_CONTROL_VALUE = { radius: '9', activeTime: '10' }
export const DEFAULT_SPEED_VALUE = { speed: null }
export const DEFAULT_GIFT_SHOP_VALUE = { giftShopType: null }
export const DEFAULT_ADDITIONAL_SETTINGS_BY_TYPE = {
  [TELEPORT]: DEFAULT_TELEPORT_VALUE,
  [GATE_CONTROL]: DEFAULT_GATE_CONTROL_VALUE,
  [SPEED]: DEFAULT_SPEED_VALUE,
  [GAME_GIFT_SHOP]: DEFAULT_GIFT_SHOP_VALUE
}

export const MAX_INPUT_LENGTH = 50
export const MAX_TEXTAREA_LENGTH = 300
