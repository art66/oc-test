// import ILibraryNpc from '../../../interfaces/ILibraryNpc'

export default interface IParameterForm {
  added: boolean,
  confirmMessage: {
    show: boolean,
    parameter: ILibraryNpc
  },
  data: ILibraryNpc,
  errors: any
}
