import { updateParameterQuantity, updateParameterCollection } from './index'
import { ADD_PARAMETER_ACTION_TYPE, REMOVE_PARAMETER_ACTION_TYPE } from '../../MapEditor/constants/history'

function initWS(dispatch: any) {
  // @ts-ignore Global websocket handler
  const handler = new WebsocketHandler('cTownAdmin')

  const handleHistoryParamUpdate = action => {
    action.type === ADD_PARAMETER_ACTION_TYPE && dispatch(updateParameterQuantity(action.payload.addedParameter))
    action.type === REMOVE_PARAMETER_ACTION_TYPE && dispatch(updateParameterQuantity(action.payload.deletedParameter))
  }

  handler.setActions({
    createParameter: data => {
      if (data.data.parameterCategories) {
        dispatch(updateParameterCollection(data.data.parameterCategories))
      }
    },
    removeParameter: data => {
      if (data.data.parameterCategories) {
        dispatch(updateParameterCollection(data.data.parameterCategories))
      }
    },
    putParameter: data => {
      dispatch(updateParameterQuantity(data.data.action.payload.addedParameter))
    },
    removeParameterFromMap: data => {
      dispatch(updateParameterQuantity(data.data.action.payload.deletedParameter))
    },
    undo: data => {
      handleHistoryParamUpdate(data.data.action)
    },
    redo: data => {
      handleHistoryParamUpdate(data.data.action)
    }
  })
}

export default initWS
