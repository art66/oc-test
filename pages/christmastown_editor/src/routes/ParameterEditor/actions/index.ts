import { createAction } from 'redux-actions'
import { fetchUrl, getErrorMessage } from '../../../utils'
import { debugHide, debugShow } from '../../../store/actions'
import * as actionTypes from './actionTypes'
import { getRouteHeader } from '../../../../../christmastown/src/controller/actions'

export const fetchData = () => dispatch => fetchUrl('parameterEditor')
  .then(json => {
    dispatch(dataFetched(json))
  })
  .catch(error => {
    dispatch(debugShow(getErrorMessage(error)))
    dispatch(dataFetchedWithError())
  })
  .finally(() => dispatch(getRouteHeader()))

export const dataFetched = json => ({
  type: actionTypes.DATA_FETCHED,
  json,
  meta: 'ajax'
})

export const dataFetchedWithError = () => ({
  type: actionTypes.DATA_FETCHED_WITH_ERROR
})

export const synchronizeData = json => ({
  type: actionTypes.SYNCHRONIZE_DATA,
  json
})

export const checkTabsSync = () => {
  return (dispatch, getState) => {
    const { mapID } = getState().parametereditor.map

    if (!mapID) return

    return fetchUrl('checkParametersEditorSynchronization', { mapID })
      .then(json => {
        if (json.mapChanged) {
          dispatch(synchronizeData(json.parametersEditorData))
        }
      })
      .catch(error => {
        dispatch(debugShow(getErrorMessage(error)))
      })
  }
}

export const createNewParameter = () => ({
  type: actionTypes.CREATE_NEW_PARAMETER
})

export const editParameter = (parameter, category) => ({
  type: actionTypes.EDIT_PARAMETER,
  parameter,
  category
})

export const removeParameter = parameter => {
  return dispatch => {
    return fetchUrl('removeParameter', {
      parameter_id: parameter.parameter_id
    })
      .then(json => {
        dispatch(parameterRemoved(json.parameterCategories))
        dispatch(debugHide())
      })
      .catch(error => {
        dispatch(debugShow(getErrorMessage(error)))
      })
  }
}

export const parameterRemoved = parameters => ({
  type: actionTypes.PARAMETER_REMOVED,
  parameters
})

export const confirmRemovingParameter = parameter => ({
  type: actionTypes.CONFIRM_REMOVING_PARAMETER,
  parameter
})

export const hideRemovingConfirmation = () => ({
  type: actionTypes.HIDE_REMOVING_CONFIRMATION
})

export const updateFormData = (name, value) => ({
  type: actionTypes.UPDATE_FORM_DATA,
  name,
  value
})

export const updateTriggersFormData = (name, value, index) => ({
  type: actionTypes.UPDATE_TRIGGERS_FORM_DATA,
  name,
  value,
  index
})

export const parameterCreated = parameters => ({
  type: actionTypes.PARAMETER_CREATED,
  parameters
})

export const addTrigger = () => ({
  type: actionTypes.ADD_TRIGGER
})

export const cancelEditingParameter = () => ({
  type: actionTypes.CANCEL_EDITING_PARAMETER
})

export const removeTrigger = triggerId => {
  return {
    type: actionTypes.REMOVE_TRIGGER,
    triggerId
  }
}

export const loadedColorData = json => ({
  type: actionTypes.LOADED_COLOR_DATA,
  json,
  meta: 'ajax'
})

export const setFormErrors = errors => {
  return {
    type: actionTypes.SET_FORM_ERRORS,
    errors
  }
}

export const startCreatingParameter = () => {
  return {
    type: actionTypes.START_CREATING_PARAMETER
  }
}

export const endCreatingParameter = () => {
  return {
    type: actionTypes.END_CREATING_PARAMETER
  }
}

export const loadColorData = () => dispatch => fetchUrl('getColorsList')
  .then(json => dispatch(loadedColorData(json)))
  .catch(error => {
    dispatch(debugShow(getErrorMessage(error)))
  })

export const sendFormData = () => (dispatch, getState) => {
  dispatch(startCreatingParameter())

  return fetchUrl('createParameter', getState().parametereditor.parameterForm.data)
    .then(json => {
      dispatch(parameterCreated(json.parameterCategories))
      dispatch(debugHide())
    })
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })
    .finally(() => {
      dispatch(endCreatingParameter())
    })
}

export const updateParameterCollection = parameterCategories => ({
  type: actionTypes.UPDATE_PARAMETER_COLLECTION,
  parameterCategories
})

export const updateParameterQuantity = parameter => ({
  type: actionTypes.UPDATE_PARAMETER_QUANTITY,
  parameter
})

export const updateTriggerType = createAction(actionTypes.UPDATE_TRIGGER_TYPE, (type, index) => ({ type, index }))

export const updateTeleportPositions = createAction(
  actionTypes.UPDATE_TELEPORT_POSITIONS,
  (name, value, triggerIndex, positionIndex) => ({ name, value, triggerIndex, positionIndex })
)

export const setParameterImage = createAction(actionTypes.SET_PARAMETER_IMAGE, (uploaded, src) => ({ uploaded, src }))
