import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Dropdown from '@torn/shared/components/Dropdown'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { DEFAULT_CATEGORY_NAME } from '../../../constants'
import { MAX_INPUT_LENGTH } from '../constants'
import { ACCESS_LEVEL_3 } from '../../../constants/accessLevels'
import { updateFormData, sendFormData, setFormErrors, cancelEditingParameter, addTrigger } from '../actions'
import checkPermission from '../../../utils/checkPermission'
import validateForm from '../helpers/validateParameterForm'
import IDropdownData from '../../../interfaces/IDropdownData'
import IMapEndpoints from '../../../interfaces/IMapEndpoints'
import TriggerFields from './FormTriggerFields'
import TextField from '../../../components/Inputs/TextField'
import ColorPicker from '../../../components/ColorPicker'
import FormFooter from './ParameterFormFooter'
import editorsStyles from '../../../styles/editors.cssmodule.scss'
import s from './styles.cssmodule.scss'

interface IProps {
  parameterForm: any
  colors: string[]
  triggers: IDropdownData[]
  categories: IDropdownData[]
  mapSize: IMapEndpoints
  userAccessLevel: number
  userRole: string
  availableMapRoles: string[]
  updateFormData: (name: string, value: any) => void
  sendFormData: () => void
  setFormErrors: (errors: any) => void
  cancelEditingParameter: () => void
  addTrigger: () => void
}

class ParameterForm extends Component<IProps> {
  _handleFormSubmit = () => {
    const { parameterForm, mapSize, sendFormData: sendData, setFormErrors: setErrors } = this.props
    const errors = validateForm(parameterForm.data, { mapSize })
    const hasErrors = Object.keys(errors).find(
      (key) => typeof errors[key] === 'string' || errors[key].xIncorrect || errors[key].yIncorrect
    )

    if (hasErrors) {
      setErrors(errors)
    } else {
      sendData()
    }
  }

  _handleDropdownChange = (value, props) => {
    const { updateFormData: updateForm } = this.props

    updateForm(props.name, value.id)
  }

  _handleUpdateColor = (value, props) => {
    const { updateFormData: updateForm } = this.props

    updateForm(props.name, value)
  }

  _handleUpdateInputValue = (e) => {
    const { updateFormData: updateForm } = this.props

    updateForm(e.target.name, e.target.value)
  }

  _handleResetErrors = () => {
    const { setFormErrors: setErrors } = this.props

    setErrors({})
  }

  renderTriggerFields = (disabled) => {
    const { parameterForm } = this.props
    const { data } = parameterForm

    return (
      data.triggers
      && data.triggers.map((trigger, index) => (
        <TriggerFields key={index} index={index} triggerData={trigger} disabled={disabled} />
      ))
    )
  }

  render() {
    const {
      parameterForm,
      colors,
      categories,
      userAccessLevel,
      userRole,
      availableMapRoles,
      addTrigger: addAnotherTrigger
    } = this.props
    const { errors, data } = parameterForm
    const { category, name, code, color } = data
    const disabled = !checkPermission(userAccessLevel, ACCESS_LEVEL_3, userRole, availableMapRoles)
    const plusIconPreset = {
      type: 'christmasTown',
      subtype: disabled ? 'ADD_REMOVE_TRIGGER_DISABLED' : 'ADD_REMOVE_TRIGGER'
    }

    const categoryPresent = category || {}

    return (
      <form>
        {categoryPresent.name !== DEFAULT_CATEGORY_NAME && (
          <div className='main-options clearfix'>
            <Dropdown
              id='3'
              className='react-dropdown-default'
              list={categories || []}
              disabled={disabled}
              selected={category || { id: 1, name: 'main' }}
              name='category_id'
              onChange={this._handleDropdownChange}
            />
            <div>
              <TextField
                name='name'
                className={s.inputName}
                placeholder='Name'
                disabled={disabled}
                onChange={this._handleUpdateInputValue}
                value={name || ''}
                error={errors.name}
                maxLength={MAX_INPUT_LENGTH}
                onFocus={this._handleResetErrors}
              />
            </div>
            <div>
              <TextField
                name='code'
                className={s.inputCode}
                placeholder='Code'
                disabled={disabled}
                onChange={this._handleUpdateInputValue}
                value={code || ''}
                error={errors.code}
                maxLength={MAX_INPUT_LENGTH}
                onFocus={this._handleResetErrors}
              />
            </div>
            <div className={cn({ disabled })}>
              <ColorPicker
                colors={colors}
                selected={color || ''}
                maxInRow={7}
                name='color'
                onChange={this._handleUpdateColor}
              />
            </div>
          </div>
        )}

        {this.renderTriggerFields(disabled)}

        <div className={s.addOutcome}>
          <button
            type='button'
            className={cn('add-text', editorsStyles.spanBtn, { disabled })}
            onClick={addAnotherTrigger}
          >
            <SVGIconGenerator
              iconName='Plus'
              preset={plusIconPreset}
              onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_HOVERED' } }}
              customClass={editorsStyles.icon}
            />
            Add another outcome
          </button>
        </div>
        <FormFooter
          disabled={disabled}
          isParameterCreating={parameterForm.creating}
          allowItemSpawn={data.allowItemSpawn}
          allowChestSpawn={data.allowChestSpawn}
          onSubmit={this._handleFormSubmit}
        />
      </form>
    )
  }
}

const mapStateToProps = (state: any) => ({
  parameterForm: state.parametereditor.parameterForm,
  colors: state.parametereditor.colors,
  triggers: state.parametereditor.triggers,
  categories: state.parametereditor.categories,
  mapSize: state.parametereditor.map.size,
  userAccessLevel: state.parametereditor.user.accessLevel,
  userRole: state.parametereditor.user.role,
  availableMapRoles: state.parametereditor.user.availableMapRoles
})

const mapActionsToProps = {
  updateFormData,
  sendFormData,
  setFormErrors,
  cancelEditingParameter,
  addTrigger
}

export default connect(mapStateToProps, mapActionsToProps)(ParameterForm)
