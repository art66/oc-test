import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dropdown from '@torn/shared/components/Dropdown'
import { SVGIconGenerator } from '@torn/shared/SVG'
import cn from 'classnames'
import editorsStyles from '../../../styles/editors.cssmodule.scss'
import * as c from '../../../constants'
import { MAX_TEXTAREA_LENGTH } from '../constants'
import {
  cancelEditingParameter,
  removeTrigger,
  sendFormData,
  setFormErrors,
  updateTriggersFormData,
  updateTriggerType,
  updateTeleportPositions
} from '../actions'
import IDropdownData from '../../../interfaces/IDropdownData'
import Textarea from '../../../components/Inputs/Textarea'
import TextField from '../../../components/Inputs/TextField'
import Teleport from '../../../components/Triggers/Teleport'
import GateControl from '../../../components/Triggers/GateControl'
import Speed from '../../../components/Triggers/Speed'
import GiftShop from '../../../components/Triggers/GiftShop'
import * as constants from '../../NpcEditor/constants'
import s from './styles.cssmodule.scss'

interface IProps {
  index: number
  disabled: boolean
  triggerData: any
  triggers: IDropdownData[]
  errors: any
  updateTriggersFormData: (name: string, value: any, index: number) => void
  setFormErrors: (errors: any) => void
  cancelEditingParameter: any
  updateTriggerType: (type: any, index: number) => void
  removeTrigger: (index: number) => void
  updateTeleportPositions: (name: string, value: number, triggerIndex: number, positionIndex: number) => void
}

export class TriggerFields extends Component<IProps> {
  componentDidUpdate() {
    const { triggerData, index } = this.props
    const { type, radius, activeTime } = triggerData

    if (type === c.GATE_CONTROL) {
      if (!radius) {
        this.props.updateTriggersFormData('radius', '6', index)
      }

      if (activeTime === undefined) {
        this.props.updateTriggersFormData('activeTime', '10', index)
      }
    }
  }

  _getIconPreset = () => {
    const { disabled } = this.props

    return disabled ?
      { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_DISABLED' } :
      { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER' }
  }

  _getSpeedTriggerValues = () => {
    const { triggers } = this.props
    const speedTrigger = triggers.find(item => item.name === 'speed')

    return speedTrigger.availableValues.map(value => ({ name: `${value}` }))
  }

  _handleTriggerInputChange = e => {
    this.props.updateTriggersFormData(e.target.name, e.target.value, +e.target.getAttribute('data-index'))
  }

  _handleTriggerDropdownChange = (value, props) => {
    this.props.updateTriggersFormData(props.name, value.name, +props.id)
  }

  _handleTeleportAnimationChange = (value, props) => {
    this.props.updateTriggersFormData(props.name, value.id, +props.id)
  }

  _handleOutcomeTypeChange = (value, props) => {
    this.props.updateTriggerType(value.name, +props.id)
  }

  _handleTeleportPositionChange = (e, triggerIndex, positionIndex) => {
    if (!isNaN(+e.target.value) || e.target.value === '-') {
      this.props.updateTeleportPositions(e.target.name, e.target.value, triggerIndex, positionIndex)
    }
  }

  _handleAddPosition = () => {
    const { index, triggerData } = this.props

    this.props.updateTriggersFormData('positions', [...triggerData.positions, constants.EMPTY_POSITION], +index)
  }

  _handleDeletePosition = (posIndex: number) => {
    const { index, triggerData } = this.props

    this.props.updateTriggersFormData('positions', triggerData.positions.filter((_, i) => i !== +posIndex), +index)
  }

  _renderAdditionalSettingsFields = () => {
    const { index, disabled, triggerData, errors } = this.props
    const { type, speed, giftShopType, activeTime, radius, positions, animation } = triggerData

    switch (type) {
      case c.SPEED:
        return (
          <Speed
            index={index}
            values={this._getSpeedTriggerValues()}
            disabled={disabled}
            value={speed}
            onChange={this._handleTriggerDropdownChange}
          />
        )
      case c.GAME_GIFT_SHOP:
        return (
          <GiftShop
            index={index}
            values={[{ name: 'Beer tent' }, { name: 'Candy Store' }, { name: 'Gift Shop' }, { name: 'Ornament Stall' }]}
            disabled={disabled}
            value={giftShopType}
            onChange={this._handleTriggerDropdownChange}
          />
        )
      case c.GATE_CONTROL:
        return (
          <GateControl
            index={index}
            disabled={disabled}
            time={activeTime}
            radius={radius}
            onTimeChange={this._handleTriggerInputChange}
            onRadiusChange={value => this.props.updateTriggersFormData('radius', value, index)}
          />
        )
      case c.TELEPORT:
        return (
          <Teleport
            index={index}
            positions={positions}
            animation={animation}
            disabled={disabled}
            errors={errors}
            handleDelete={(posIndex: number) => this._handleDeletePosition(posIndex)}
            handleAdd={this._handleAddPosition}
            handleFocus={() => setFormErrors({})}
            handleAnimationChange={this._handleTeleportAnimationChange}
            handleChange={(e, triggerIndex, posIndex) => this._handleTeleportPositionChange(e, triggerIndex, posIndex)}
          />
        )
      default: return null
    }
  }

  render() {
    const { index, triggers, triggerData, errors, disabled } = this.props
    const { text, chance, type } = triggerData

    return (
      <div className={s.triggerSection}>
        <Textarea
          error={errors[`text-${index}`]}
          index={index}
          name='text'
          disabled={disabled}
          className={s.messageInput}
          value={text || ''}
          maxLength={MAX_TEXTAREA_LENGTH}
          onChange={this._handleTriggerInputChange}
          onFocus={() => setFormErrors({})}
        />
        <div className={s.triggerInputsWrapper}>
          <TextField
            index={index}
            name='chance'
            className={s.inputChance}
            customContainerClass={editorsStyles.inputContainer}
            customErrorClass={s.chanceError}
            disabled={disabled}
            onChange={this._handleTriggerInputChange}
            value={chance}
            error={errors[`chance-${index}`]}
            onFocus={() => setFormErrors({})}
          />
          <div className={s.triggerTypeDropdown}>
            <Dropdown
              id={index.toString()}
              list={triggers || []}
              name='type'
              placeholder='Select trigger'
              selected={type ? { name: type } : { name: c.DEFAULT_TRIGGER_NAME }}
              disabled={disabled}
              onChange={this._handleOutcomeTypeChange}
            />
          </div>
          {this._renderAdditionalSettingsFields()}
        </div>
        <button
          type='button'
          className={cn(editorsStyles.spanBtn, s.removeTrigger, { disabled })}
          onClick={() => this.props.removeTrigger(index)}
        >
          <SVGIconGenerator
            iconName='Close'
            preset={this._getIconPreset()}
            onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'ADD_REMOVE_TRIGGER_HOVERED' } }}
            customClass={cn(editorsStyles.icon, editorsStyles.removeIcon)}
          />
          Remove
        </button>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  parameterForm: state.parametereditor.parameterForm,
  triggers: state.parametereditor.triggers,
  errors: state.parametereditor.parameterForm.errors
})

const mapActionsToProps = {
  updateTriggersFormData,
  sendFormData,
  setFormErrors,
  cancelEditingParameter,
  removeTrigger,
  updateTriggerType,
  updateTeleportPositions
}

export default connect(mapStateToProps, mapActionsToProps)(TriggerFields)
