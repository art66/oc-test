import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import IInitParams from '@torn/ics/src/models/IInitParams'
import { addTrigger, updateFormData, cancelEditingParameter } from '../actions'
import { toggleICS, setICSInitParams } from '../../../store/actions'
import editorsStyles from '../../../styles/editors.cssmodule.scss'
import sTip from '../../../styles/tooltips.cssmodule.scss'
import s from './styles.cssmodule.scss'

interface IProps {
  disabled: boolean
  formData: any
  allowItemSpawn?: boolean
  allowChestSpawn: boolean
  addTrigger: () => void
  updateFormData: (name: string, value: any) => void
  onSubmit: () => void
  cancelEditingParameter: () => void
  isParameterCreating: boolean
  toggleICS: (show: boolean) => void
  setICSInitParams: (initParams: IInitParams) => void
}

class ParameterFormFooter extends Component<IProps> {
  componentDidMount() {
    this.initTooltips()
  }

  componentWillUnmount() {
    tooltipsSubscriber.unsubscribe(this.getImagePreviewTooltip())
  }

  componentDidUpdate() {
    this.updateTooltips()
  }

  getImagePreviewTooltip = () => {
    const { formData } = this.props

    return {
      ID: 'replace-parameter-image',
      child: (
        <div className={cn(sTip.imagePreviewWrap, sTip.withFilter)}>
          <img className={sTip.imagePreview} src={formData.imageData || formData.imageUrl} />
        </div>
      ),
      customConfig: {
        overrideStyles: {
          container: cn(sTip.imagePreviewTooltipContainer, sTip.container),
          title: sTip.imagePreviewTooltipTitle
        }
      }
    }
  }

  updateTooltips = () => {
    tooltipsSubscriber.render({
      tooltipsList: [this.getImagePreviewTooltip()],
      type: 'update'
    })
  }

  initTooltips = () => {
    const tooltipsData = [
      this.getImagePreviewTooltip(),
      {
        child: 'Custom image will be replaced by default one',
        ID: 'remove-parameter-image'
      }
    ]

    tooltipsData.forEach((item) => {
      tooltipsSubscriber.subscribe(item)
    })
  }

  _handleUpdateItemSpawnValue = (e) => {
    const { updateFormData: updateForm } = this.props

    updateForm(e.target.name, e.target.checked)
  }

  _handleImageUpload = (data) => {
    const { updateFormData: updateForm } = this.props

    updateForm('imageData', data)
  }

  _handleOpenICS = () => {
    const { toggleICS: openICS, setICSInitParams: setInitParams, formData } = this.props
    const initParams = {
      imageType: 'christmasTownParameter',
      callback: this._handleImageUpload,
      parameters: {
        parameter_id: formData.parameter_id || null
      }
    }

    openICS(true)
    setInitParams(initParams)
  }

  _handleRemoveImage = () => {
    this.props.updateFormData('imageData', null)
    this.props.updateFormData('imageUrl', null)
    this.props.updateFormData('imagePath', null)
  }

  renderImageButtons = () => {
    const { formData, disabled } = this.props
    const removeEditImageIconPreset = {
      type: 'christmasTown',
      subtype: disabled ? 'REMOVE_EDIT_IMAGE_DISABLED' : 'REMOVE_EDIT_IMAGE_DEFAULT'
    }
    const removeEditImageIconHoverPreset = {
      active: true,
      preset: {
        type: 'christmasTown',
        subtype: 'REMOVE_EDIT_IMAGE_HOVERED'
      }
    }

    return formData.imageData || formData.imageUrl ? (
      <>
        <button
          id='replace-parameter-image'
          type='button'
          className={cn(editorsStyles.spanBtn, editorsStyles.replaceImageBtn)}
          onClick={this._handleOpenICS}
          disabled={disabled}
        >
          <SVGIconGenerator
            iconName='Image'
            type='edit'
            preset={removeEditImageIconPreset}
            onHover={removeEditImageIconHoverPreset}
            customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
          />
          Replace background image
        </button>
        <button
          id='remove-parameter-image'
          type='button'
          className={editorsStyles.spanBtn}
          onClick={this._handleRemoveImage}
          disabled={disabled}
        >
          <SVGIconGenerator
            iconName='Image'
            type='remove'
            preset={removeEditImageIconPreset}
            onHover={removeEditImageIconHoverPreset}
            customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
          />
          Remove background image
        </button>
      </>
    ) : (
      <button type='button' className={editorsStyles.spanBtn} onClick={this._handleOpenICS} disabled={disabled}>
        <SVGIconGenerator
          iconName='Image'
          preset={{ type: 'christmasTown', subtype: disabled ? 'UPLOAD_IMAGE_DISABLED' : 'UPLOAD_IMAGE_DEFAULT' }}
          onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'UPLOAD_IMAGE_HOVERED' } }}
          customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
        />
        Upload background image
      </button>
    )
  }

  renderToolsContainer = () => {
    const {
      disabled,
      isParameterCreating,
      allowItemSpawn,
      allowChestSpawn,
      onSubmit,
      cancelEditingParameter: cancelEditing
    } = this.props

    return (
      <div className={cn('save-cancel-container', s.saveCancelContainer)}>
        <div className={s.actionsWrap}>
          {this.renderImageButtons()}
          <div className={cn('choice-container', s.itemSpawnContainer, { disabled })}>
            <input
              onChange={this._handleUpdateItemSpawnValue}
              checked={allowItemSpawn || false}
              className='checkbox-css'
              type='checkbox'
              name='allowItemSpawn'
              id='allow-item-spawn'
            />
            <label htmlFor='allow-item-spawn' className='marker-css'>
              Allow item spawns
            </label>
          </div>
          <div className={cn('choice-container', { disabled })}>
            <input
              onChange={this._handleUpdateItemSpawnValue}
              checked={allowChestSpawn || false}
              className='checkbox-css'
              type='checkbox'
              name='allowChestSpawn'
              id='allow-chest-spawn'
            />
            <label htmlFor='allow-chest-spawn' className='marker-css'>
              Allow chest spawns
            </label>
          </div>
        </div>
        <div className={s.buttonsWrap}>
          <button
            type='button'
            className={cn(editorsStyles.spanBtn, editorsStyles.cancel, { disabled })}
            onClick={cancelEditing}
          >
            Cancel
          </button>
          <button type='button' className='btn-blue big' disabled={disabled || isParameterCreating} onClick={onSubmit}>
            SAVE
          </button>
        </div>
      </div>
    )
  }

  render() {
    return <div className={cn('footer-container', s.footerContainer)}>{this.renderToolsContainer()}</div>
  }
}

const mapStateToProps = (state) => ({
  formData: state.parametereditor.parameterForm.data
})

const mapActionsToProps = {
  addTrigger,
  cancelEditingParameter,
  updateFormData,
  toggleICS,
  setICSInitParams
}

export default connect(mapStateToProps, mapActionsToProps)(ParameterFormFooter)
