import React from 'react'

interface IProps {
  parameter: any,
  removeParameter: any,
  hideRemovingConfirmation: () => void
}

export const RemoveConfirmationMessage = (props: IProps) => {
  const { parameter, removeParameter, hideRemovingConfirmation } = props
  const { name, code } = parameter

  return (
    <div className='parameter-editor-info-msg remove-confirmation'>
      <h3>
        Are you sure you want to remove '{name} [{code}]' parameter from the Library?
      </h3>
      <div className='link-blue' onClick={() => removeParameter(parameter)}>
        Yes
      </div>
      <div className='link-blue' onClick={hideRemovingConfirmation}>
        No
      </div>
    </div>
  )
}

export default RemoveConfirmationMessage
