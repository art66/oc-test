import React from 'react'

interface IProps {
  message: string,
  createNewParameter: () => void
}

export const Message = (props: IProps) => {
  const { message, createNewParameter } = props

  return (
    <div className='parameter-editor-info-msg'>
      <h3>{message}</h3>
      <div className='link-blue' onClick={createNewParameter}>
        Create new Parameter
      </div>
    </div>
  )
}

export default Message
