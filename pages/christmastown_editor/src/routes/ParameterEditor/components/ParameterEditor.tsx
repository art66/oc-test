import React, { Component } from 'react'
import { connect } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'
import ParametersLibrary from '../../../components/ParametersLibrary'
import ParameterForm from './ParameterForm'
import Message from './Message'
import RemoveConfirmationMessage from './RemoveConfirmationMessage'
import '../../../styles/christmas_town.css'
import '../../../styles/christmas_town_admin.css'
import IMap from '../../../interfaces/IMap'
import IParameterCategory from '../../../interfaces/IParameterCategory'
import ILibraryParameter from '../../../interfaces/ILibraryParameter'
import {
  checkTabsSync,
  createNewParameter,
  editParameter,
  removeParameter,
  confirmRemovingParameter,
  hideRemovingConfirmation,
  fetchData
} from '../actions'
import checkPermission from '../../../utils/checkPermission'
import { ACCESS_LEVEL_3 } from '../../../constants/accessLevels'

interface IProps {
  parameterForm: any
  map: IMap
  user: {
    accessLevel: number
    role: string
    availableMapRoles: string[]
  }
  loadingInitData: boolean
  parameterCategories: IParameterCategory[]
  checkTabsSync: () => void
  createNewParameter: () => void
  editParameter: (parameter: ILibraryParameter, category: IParameterCategory) => void
  removeParameter: (parameter: ILibraryParameter) => void
  confirmRemovingParameter: (parameter: any) => void
  hideRemovingConfirmation: () => void
  fetchData: () => void
}

export class ParameterEditor extends Component<IProps> {
  componentDidMount() {
    const { fetchData } = this.props

    fetchData()
    window.addEventListener('focus', this._handleTabChange)
  }

  componentWillUnmount() {
    window.removeEventListener('focus', this._handleTabChange)
  }

  _handleTabChange = () => {
    const { checkTabsSync } = this.props

    checkTabsSync()
  }

  render() {
    const {
      parameterForm,
      createNewParameter,
      editParameter,
      removeParameter,
      confirmRemovingParameter,
      hideRemovingConfirmation,
      parameterCategories,
      map,
      user,
      loadingInitData
    } = this.props

    if (loadingInitData) {
      return <Preloader />
    }

    return checkPermission(user.accessLevel, ACCESS_LEVEL_3, user.role, user.availableMapRoles) && map && (
      <div>
        <div className='ct-wrap ct-admin-wrap'>
          <div className='editor-container parameter-editor'>
            <div className='title-wrap clearfix'>
              <span className='text left'>{map.name}</span>
            </div>
            <div className='editor-content'>
              {!parameterForm.added ? (
                <ParameterForm />
              ) : (
                <Message createNewParameter={createNewParameter} message={parameterForm.message} />
              )}
              {parameterForm.confirmMessage.show && (
                <RemoveConfirmationMessage
                  parameter={parameterForm.confirmMessage.parameter}
                  removeParameter={removeParameter}
                  hideRemovingConfirmation={hideRemovingConfirmation}
                />
              )}
            </div>
          </div>
          <div className='tools-container parameters'>
            <div className='title-wrap clearfix'>
              <span className='text'>Library</span>
            </div>
            <ParametersLibrary
              parameterCategories={parameterCategories}
              editParameter={editParameter}
              confirmRemovingParameter={confirmRemovingParameter}
            />
          </div>
        </div>
      </div>
    )
  }
}

const mapActionsToProps = {
  checkTabsSync,
  createNewParameter,
  editParameter,
  removeParameter,
  confirmRemovingParameter,
  hideRemovingConfirmation,
  fetchData
}

const mapStateToProps = state => ({
  map: state.parametereditor.map,
  parameterCategories: state.parametereditor.parameterCategories,
  parameterForm: state.parametereditor.parameterForm,
  user: state.parametereditor.user,
  loadingInitData: state.parametereditor.loadingInitData
})

export default connect(mapStateToProps, mapActionsToProps)(ParameterEditor)
