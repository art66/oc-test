import initialState from './initialState'
import * as actionTypes from '../actions/actionTypes'
import { DEFAULT_CATEGORY_NAME } from '../../../constants'
import { DEFAULT_ADDITIONAL_SETTINGS_BY_TYPE, INITIAL_TRIGGER_DATA, CATEGORY_MAIN } from '../constants'

const ACTION_HANDLERS = {
  [actionTypes.DATA_FETCHED]: (state, action) => {
    return {
      ...state,
      map: action.json.map,
      parameterCategories: action.json.parameterCategories,
      categories: action.json.categories.filter(category => category.name !== DEFAULT_CATEGORY_NAME),
      colors: action.json.colors.map(color => color.toUpperCase()),
      triggers: action.json.triggers,
      loadingInitData: false,
      user: action.json.user
    }
  },

  [actionTypes.DATA_FETCHED_WITH_ERROR]: state => {
    return {
      ...state,
      loadingInitData: false
    }
  },

  [actionTypes.SYNCHRONIZE_DATA]: (state, action) => {
    return {
      ...state,
      map: action.json.map,
      parameterCategories: action.json.parameterCategories,
      parameterForm: {
        ...state.parameterForm,
        confirmMessage: initialState.parameterForm.confirmMessage,
        data: state.parameterForm.data.parameter_id ? initialState.parameterForm.data : state.parameterForm.data,
        errors: initialState.parameterForm.errors
      },
      user: action.json.user
    }
  },

  [actionTypes.CREATE_NEW_PARAMETER]: state => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        added: false,
        data: {
          category_id: CATEGORY_MAIN,
          triggers: [INITIAL_TRIGGER_DATA]
        },
        errors: {}
      }
    }
  },

  [actionTypes.EDIT_PARAMETER]: (state, action) => {
    const newFormData = Object.assign({}, state.parameterForm)

    newFormData.data = Object.assign({}, action.parameter)
    newFormData.data.category = action.category
    newFormData.data.category_id = action.category.id
    newFormData.data.triggers = newFormData.data.triggers || [{}]
    newFormData.added = false
    newFormData.confirmMessage = { show: false }
    return {
      ...state,
      parameterForm: {
        ...newFormData,
        errors: {}
      }
    }
  },

  [actionTypes.PARAMETER_REMOVED]: (state, action) => {
    return {
      ...state,
      parameterCategories: action.parameters,
      parameterForm: {
        ...state.parameterForm,
        added: true,
        data: initialState.parameterForm.data,
        confirmMessage: {
          show: false
        },
        message: 'Parameter successfully removed from the Library!'
      }
    }
  },

  [actionTypes.CONFIRM_REMOVING_PARAMETER]: (state, action) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        confirmMessage: {
          show: true,
          parameter: action.parameter
        },
        errors: {}
      }
    }
  },

  [actionTypes.HIDE_REMOVING_CONFIRMATION]: state => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        confirmMessage: {
          show: false
        },
        added: false,
        message: ''
      }
    }
  },

  [actionTypes.UPDATE_FORM_DATA]: (state, action) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        data: {
          ...state.parameterForm.data,
          [action.name]: action.value
        }
      }
    }
  },

  [actionTypes.UPDATE_TRIGGERS_FORM_DATA]: (state, action) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        data: {
          ...state.parameterForm.data,
          triggers: state.parameterForm.data.triggers.map((trigger, index) => (action.index === index ? {
            ...state.parameterForm.data.triggers[index],
            [action.name]: action.value
          } : trigger))
        }
      }
    }
  },

  [actionTypes.UPDATE_TRIGGER_TYPE]: (state, { payload }) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        data: {
          ...state.parameterForm.data,
          triggers: state.parameterForm.data.triggers.map((trigger, index) => {
            const { chance, text } = state.parameterForm.data.triggers[index]
            const defaultTriggerFields = { chance, text }
            const defaultAdditionalFields = DEFAULT_ADDITIONAL_SETTINGS_BY_TYPE[payload.type]

            return payload.index === index ? {
              ...defaultTriggerFields,
              ...defaultAdditionalFields,
              type: payload.type
            } : trigger
          })
        }
      }
    }
  },

  [actionTypes.UPDATE_TELEPORT_POSITIONS]: (state, { payload }) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        data: {
          ...state.parameterForm.data,
          triggers: state.parameterForm.data.triggers.map((trigger, triggerIndex) => {
            if (payload.triggerIndex === triggerIndex) {
              return {
                ...state.parameterForm.data.triggers[triggerIndex],
                positions: trigger.positions.map((position, positionIndex) => {
                  if (payload.positionIndex === positionIndex) {
                    return {
                      ...trigger.positions[positionIndex],
                      [payload.name]: payload.value
                    }
                  }
                  return position
                })
              }
            }
            return trigger
          })
        }
      }
    }
  },

  [actionTypes.START_CREATING_PARAMETER]: state => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        creating: true
      }
    }
  },

  [actionTypes.END_CREATING_PARAMETER]: state => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        creating: false
      }
    }
  },

  [actionTypes.PARAMETER_CREATED]: (state, action) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        data: { triggers: [{}] },
        added: true,
        message: 'Parameter added to the Library!',
        confirmMessage: {
          show: false
        }
      },
      parameterCategories: action.parameters || state.parameterCategories
    }
  },

  [actionTypes.CANCEL_EDITING_PARAMETER]: state => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        data: { triggers: [{}] },
        added: true,
        message: '',
        confirmMessage: {
          show: false
        },
        errors: {}
      }
    }
  },

  [actionTypes.ADD_TRIGGER]: state => {
    const newFormData = Object.assign({}, state.parameterForm)

    newFormData.data.triggers ?
      newFormData.data.triggers.push(INITIAL_TRIGGER_DATA) :
      (newFormData.data.triggers = [INITIAL_TRIGGER_DATA])

    return {
      ...state,
      parameterForm: newFormData
    }
  },

  [actionTypes.REMOVE_TRIGGER]: (state, action) => {
    const newFormData = Object.assign({}, state.parameterForm)

    newFormData.data.triggers.splice(action.triggerId, 1)

    return {
      ...state,
      parameterForm: newFormData
    }
  },

  [actionTypes.UPDATE_PARAMETER_COLLECTION]: (state, action) => {
    return {
      ...state,
      parameterCategories: action.parameterCategories
    }
  },

  [actionTypes.UPDATE_PARAMETER_QUANTITY]: (state, action) => {
    const { category_id, parameter_id, quantity } = action.parameter

    return {
      ...state,
      parameterCategories: state.parameterCategories.map(category => {
        return category.id === category_id ?
          {
            ...category,
            parameters: category.parameters.map(param => {
              return param.parameter_id === parameter_id ?
                {
                  ...param,
                  quantity
                } :
                param
            })
          } :
          category
      })
    }
  },

  [actionTypes.SET_FORM_ERRORS]: (state, action) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        errors: action.errors
      }
    }
  },

  [actionTypes.SET_PARAMETER_IMAGE]: (state, action) => {
    return {
      ...state,
      parameterForm: {
        ...state.parameterForm,
        image: {
          uploaded: action.payload.uploaded,
          src: action.payload.src
        }
      }
    }
  }
}

const reducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
