export default {
  map: {
    mapID: '',
    name: ''
  },
  loadingInitData: true,
  colors: [],
  categories: [
    {
      id: 1,
      name: 'Cat1'
    },
    {
      id: 2,
      name: 'Cat2'
    }
  ],
  triggers: [
    {
      id: 1,
      name: 'Tr1'
    }
  ],
  parameterForm: {
    added: false,
    confirmMessage: {
      show: false,
      parameter: {}
    },
    image: {
      uploaded: false
    },
    data: {
      category_id: 1,
      triggers: [{ chance: 1, type: 'no trigger', text: '' }]
    },
    errors: {}
  },
  parameterCategories: [],
  user: {},
  ics: {
    show: false
  }
}
