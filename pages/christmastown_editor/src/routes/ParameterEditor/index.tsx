import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import reducer from './reducers'
import Preloader from '@torn/shared/components/Preloader'
import rootStore from '../../store/createStore'
import initWS from './actions/websocket'
import CTStore from '../../../../christmastown/src/store/createStore'
import { injectReducer as CTInjectReducer } from '../../../../christmastown/src/store/helpers/rootReducer'

const PreloaderComponent = () => <Preloader />

const ParameterEditorRoute = Loadable({
  loader: async() => {
    const ParameterEditor = await import('./components/ParameterEditor')

    return ParameterEditor
  },
  render(asyncComponent: any, { isSPA }: { isSPA: boolean }) {
    const { default: ParameterEditor } = asyncComponent
    const store = isSPA ? CTStore : rootStore
    const reducerInjector = isSPA ? CTInjectReducer : injectReducer

    reducerInjector(store, { reducer, key: 'parametereditor' })
    initWS(store.dispatch)

    return <ParameterEditor />
  },
  loading: PreloaderComponent
})

export default ParameterEditorRoute
