import { connect } from 'react-redux'
import {
  fetchMapEditorData,
  updateMap,
  initMovement,
  setPointerPos,
  selectLibraryParameter,
  addParameterToMap,
  selectParameterOnMap,
  unSelectOnMap,
  removeParameterOnMap,
  selectLibraryObject,
  selectObjectOnMap,
  selectObjectsOnMap,
  addSelectedObject,
  unselectObject,
  addObjectToMap,
  removeObjectOnMap,
  changeObjectPosition,
  toggleCoordinates,
  setCurrentTab,
  setFilterCondition,
  setMapPosition,
  toggleFullscreenMode,
  toggleViewLayers,
  setViewToggleLayers,
  addAreaOnMap,
  clearLocalStorage,
  setScale,
  deleteAreasOnMapByHashes,
  addAreasOnMap,
  fetchMapEditorDataWithBlockingCheck
} from '../modules/mapeditor'

import { reorderObjects } from '../modules/layers'

import { changeControlsStatus } from '../modules/backups'

import { checkTabsSync } from '../modules/mapData'

import MapEditor from '../components/MapEditor'

const mapDispatchToProps = {
  fetchMapEditorData,
  updateMap,
  initMovement,
  setPointerPos,
  selectLibraryParameter,
  addParameterToMap,
  selectParameterOnMap,
  unSelectOnMap,
  removeParameterOnMap,
  toggleCoordinates,
  setCurrentTab,
  setFilterCondition,
  addObjectToMap,
  selectLibraryObject,
  selectObjectOnMap,
  selectObjectsOnMap,
  addSelectedObject,
  unselectObject,
  removeObjectOnMap,
  changeObjectPosition,
  setMapPosition,
  toggleFullscreenMode,
  toggleViewLayers,
  setViewToggleLayers,
  addAreaOnMap,
  clearLocalStorage,
  setScale,
  reorderObjects,
  deleteAreasOnMapByHashes,
  addAreasOnMap,
  changeControlsStatus,
  checkTabsSync,
  fetchMapEditorDataWithBlockingCheck
}

const mapStateToProps = state => ({
  mapData: state.mapeditor.mapData,
  library: state.mapeditor.library,
  history: state.mapeditor.history,
  backups: state.mapeditor.backups,
  overlay: state.mapeditor.overlay,
  user: state.mapeditor.user
})

export default connect(mapStateToProps, mapDispatchToProps)(MapEditor)
