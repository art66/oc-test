import initialState from './initialState'
import * as actionTypes from '../actionTypes/overlay'
import { CHECK_TABS_SYNC, SYNCHRONIZE_MAP_DATA } from '../actionTypes'

const ACTION_HANDLERS = {
  [actionTypes.ACTIVATE_OVERLAY]: state => {
    return {
      ...state,
      active: true
    }
  },
  [actionTypes.DEACTIVATE_OVERLAY]: state => {
    return {
      ...state,
      active: false
    }
  },
  [CHECK_TABS_SYNC]: state => {
    return {
      ...state,
      active: true
    }
  },
  [SYNCHRONIZE_MAP_DATA]: state => {
    return {
      ...state,
      active: false
    }
  }
}

export default function overlayReducer(state: any = initialState.overlay, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action, fullState) : state
}
