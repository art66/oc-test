import { handleActions } from 'redux-actions'
import uniqBy from 'lodash.uniqby'
import * as a from '../../actionTypes'

const initialState = {}

export const ACTION_HANDLERS = {
  [a.FETCH_PARAMETERS_ON_MAP_BY_ID_SUCCESS]: (state, action) => {
    const { id, parameters } = action.payload

    return {
      ...state,
      [id]: {
        parameters,
        searchIndex: 0,
        loaded: true
      }
    }
  },
  [a.SHOW_PARAMETER_ON_MAP]: (state, action) => {
    const { parameter_id } = action.parameter

    if (!state[parameter_id]) return state

    return {
      ...state,
      [parameter_id]: {
        ...state[parameter_id],
        parameters: uniqBy(state[parameter_id].parameters.concat([action.parameter]), 'hash')
      }
    }
  },
  [a.SHOW_REMOVING_PARAMETER_ON_MAP]: (state, action) => {
    const { parameter_id, hash } = action.parameter

    if (!state[parameter_id]) return state

    const newParameters = state[parameter_id].parameters.filter(parameter => parameter.hash !== hash)

    return {
      ...state,
      [parameter_id]: {
        ...state[parameter_id],
        parameters: newParameters,
        searchIndex: state[parameter_id].searchIndex > newParameters.length ?
          newParameters.length :
          state[parameter_id].searchIndex
      }
    }
  },
  [a.HIGHLIGHT_PARAMETER_ON_MAP]: (state, action) => {
    const { id, index } = action.payload

    return {
      ...state,
      [id]: {
        ...state[id],
        searchIndex: index
      }
    }
  }
}

const parametersSearch = handleActions(ACTION_HANDLERS, initialState)

parametersSearch.hasActionHandler = (actionType: string) => ACTION_HANDLERS[actionType] !== undefined

export default parametersSearch
