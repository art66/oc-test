import uniqBy from 'lodash.uniqby'
import sortBy from 'lodash.sortby'
import orderBy from 'lodash.orderby'
import { getCookie } from '@torn/shared/utils'
import initialState from '../initialState'
import { CELL_SIZE, MAP_VIEWPORT_CELLS_STOCK, TAB_OBJECTS, TAB_LAYERS, TAB_BLOCKING } from '../../../../constants/index'
import { LAYERS_NAMES_ARR } from '../../constants/layers'
import * as actionTypes from '../../actionTypes/index'
import * as aBlocking from '../../actionTypes/blocking'
import * as aNpc from '../../actionTypes/npc'
import blocking from './blocking'
import parametersSearch from './parametersSearch'
import npc from './npc'
import { isOnTopOfUser } from '../../utils'
import { updateLayers, updateObjectsOrderInList } from '../../utils/layers'

import { FULLSCREEN_MODE, FULLSCREEN_MODE_COOKIE, LOAD_STATUS_SUCCESSFUL, MAX_NPC_PER_MAP } from '../../constants'

const handleInitialMapData = (mapData, state) => {
  const { adminPosition, mapObjects, mapParameters, mapAreas, mapBlocks, mapNpc, map } = mapData

  return {
    mapId: map.mapId,
    mapName: map ? map.name : 'CT Map',
    npcLimit: map ? map.npcLimit : MAX_NPC_PER_MAP,
    mapEditorDataFetched: true,
    mapPosition: adminPosition.leftTop,
    mapSize: {
      mapWidth: (map.size.rightBottom.x - map.size.leftTop.x + MAP_VIEWPORT_CELLS_STOCK) * CELL_SIZE,
      mapHeight: (map.size.leftTop.y - map.size.rightBottom.y + MAP_VIEWPORT_CELLS_STOCK) * CELL_SIZE
    },
    loading: false,
    loadingInitData: false,
    loadStatus: LOAD_STATUS_SUCCESSFUL,
    objects: uniqBy(sortBy(state.objects
      .concat(mapObjects)
      .map(object => {
        return {
          ...object,
          onTopOfUser: isOnTopOfUser(object.layer.name),
          order: object.layer.number * object.createdAt
        }
      }), ['order'], ['desc']), 'hash'),
    parameters: uniqBy(mapParameters, 'hash'),
    areas: {
      ...state.areas,
      list: uniqBy(
        state.areas.list.concat(mapAreas.map(area => ({ ...area, saved: true }))),
        'hash'
      )
    },
    blocking: {
      ...state.blocking,
      cells: uniqBy(state.blocking.cells.concat(mapBlocks), 'position')
    },
    npc: uniqBy(mapNpc, 'hash')
  }
}

const ACTION_HANDLERS = {
  [actionTypes.MAP_EDITOR_DATA_FETCHED]: (state, action) => {
    return {
      ...state,
      ...handleInitialMapData(action.json, state),
      objects: uniqBy(sortBy(action.json.mapObjects
        .map(object => {
          return {
            ...object,
            onTopOfUser: isOnTopOfUser(object.layer.name),
            order: object.layer.number * object.createdAt
          }
        }), ['order'], ['desc']), 'hash'),
      cellPointer: {
        ...state.cellPointer,
        show: false
      }
    }
  },

  [actionTypes.MAP_EDITOR_DATA_FETCHED_WITH_ERROR]: state => {
    return {
      ...state,
      loadingInitData: false
    }
  },

  [actionTypes.DISABLE_DATA_LOADING]: (state, action) => {
    return {
      ...state,
      dataLoadingIsDisabled: action.payload.allowed
    }
  },

  [actionTypes.CHECK_TABS_SYNC]: (state) => {
    return {
      ...state,
      fullscreenMode: getCookie(FULLSCREEN_MODE_COOKIE) === FULLSCREEN_MODE
    }
  },

  [actionTypes.SYNCHRONIZE_MAP_DATA]: (state, action) => {
    return {
      ...state,
      ...handleInitialMapData(action.payload.mapData, initialState.mapData),
      selectedParameter: {},
      highlightedCells: {},
      contextMenuObject: {},
      selectedObjects: [],
      buffer: {
        action: '',
        objects: []
      },
      parametersSearch: {}
    }
  },

  [actionTypes.SET_SUCCESSFUL_LOAD_STATUS]: (state) => {
    return {
      ...state,
      loadStatus: LOAD_STATUS_SUCCESSFUL
    }
  },

  [actionTypes.GET_AJAX_ERROR]: (state, action) => {
    return {
      ...state,
      loading: false,
      loadStatus: { status: 'fail', message: action.error }
    }
  },

  [actionTypes.UPDATE_MAP_DATA]: (state, action) => {
    return {
      ...state,
      loadStatus: LOAD_STATUS_SUCCESSFUL,
      objects: action.mapData.objects.map(object => {
        return {
          ...object,
          onTopOfUser: isOnTopOfUser(object.layer.name),
          order: object.layer.number * object.createdAt
        }
      }) || [],
      parameters: action.mapData.parameters || []
    }
  },

  [actionTypes.START_FETCHING_ADMIN_MAP_DATA]: (state, action) => {
    return {
      ...state,
      endpoints: action.endpoints,
      loading: true
    }
  },

  [actionTypes.ADMIN_MAP_DATA_FETCHED]: (state, action) => {
    const { mapAreas, mapObjects, mapParameters, mapBlocks } = action.json

    return {
      ...state,
      loading: false,
      loadStatus: LOAD_STATUS_SUCCESSFUL,
      areas: {
        ...state.areas,
        list: mapAreas
      },
      objects: mapObjects.map(object => {
        return {
          ...object,
          onTopOfUser: isOnTopOfUser(object.layer.name),
          order: object.layer.number * object.createdAt
        }
      }),
      parameters: mapParameters,
      blocking: {
        ...state.blocking,
        cells: mapBlocks
      }
    }
  },

  [actionTypes.RENAME_MAP]: (state, action) => {
    return {
      ...state,
      mapName: action.payload
    }
  },

  [actionTypes.SET_POINTER_POS]: (state, action) => {
    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: true,
        top: action.pointerPos.top,
        left: action.pointerPos.left,
        y: Math.round((action.pointerPos.top - state.mapSize.mapHeight / 2) / -CELL_SIZE),
        x: Math.round(action.pointerPos.left / CELL_SIZE)
      }
    }
  },

  [actionTypes.SELECT_LIBRARY_PARAMETER]: (state, action) => {
    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: true,
        top: -30,
        left: -30
      },
      selectedObject: {},
      selectedParameter:
        JSON.stringify(action.parameter) !== JSON.stringify(state.selectedParameter) ?
          (<any>Object).assign({}, action.parameter) :
          {}
    }
  },

  [actionTypes.SHOW_PARAMETER_ON_MAP]: (state, action) => {
    return {
      ...state,
      loading: true,
      parameters: uniqBy(state.parameters.concat([action.parameter]), 'hash')
    }
  },

  [actionTypes.PARAMETER_ADDED_ON_MAP]: (state, action) => {
    return {
      ...state,
      loading: false,
      loadStatus: LOAD_STATUS_SUCCESSFUL,
      selectedParameter: {
        ...state.selectedParameter,
        hash: action.parameter.hash
      }
    }
  },

  [actionTypes.SELECT_PARAMETER_ON_MAP]: (state, action) => {
    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: false,
        left: -1000,
        top: -1000
      },
      selectedParameter: state.selectedParameter.hash !== action.parameter.hash ? action.parameter : {}
    }
  },

  [aNpc.SELECT_NPC]: (state, { payload }) => {
    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: payload.showPointer,
        left: -1000,
        top: -1000
      },
      selectedNpc: state.selectedNpc.libraryId !== payload.npc.libraryId
        || state.selectedNpc.hash !== payload.npc.hash
        || state.cellPointer.show !== payload.showPointer ? payload.npc : {}
    }
  },

  [aNpc.REMOVE_LIBRARY_NPC]: (state, { payload }) => {
    return {
      ...state,
      selectedNpc: state.selectedNpc.libraryId === payload.id ? { } : state.selectedNpc
    }
  },

  [actionTypes.HIGHLIGHT_PARAMETER_ON_MAP]: (state, action) => {
    const { id, hash } = action.payload

    return {
      ...state,
      highlightedCells: {
        type: 'parameter',
        id,
        hash
      }
    }
  },

  [aNpc.HIGHLIGHT_NPC_ON_MAP]: (state, action) => {
    const { id, hash } = action.payload

    return {
      ...state,
      highlightedCells: {
        type: 'npc',
        id,
        hash
      }
    }
  },

  [actionTypes.UNSELECT_ON_MAP]: (state) => {
    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: false,
        left: -1000,
        top: -1000
      },
      blocking: {
        ...state.blocking,
        blockingTool: ''
      },
      selectedParameter: {},
      selectedNpc: {},
      selectedObjects: [],
      contextMenuObject: {},
      layers: initialState.mapData.layers
    }
  },

  [actionTypes.SHOW_REMOVING_PARAMETER_ON_MAP]: (state, action) => {
    return {
      ...state,
      loading: true,
      parameters: state.parameters.filter(
        parameter => JSON.stringify(parameter.position) !== JSON.stringify(action.parameter.position)
      )
    }
  },

  [actionTypes.PARAMETER_REMOVED_ON_MAP]: (state, action) => {
    return {
      ...state,
      loading: false,
      loadStatus: LOAD_STATUS_SUCCESSFUL,
      selectedParameter: state.selectedParameter.hash === action.parameter.hash ? {} : state.selectedParameter
    }
  },

  [actionTypes.UPDATE_PARAMETER_LIST]: (state, { payload }) => {
    if (payload.removedParameterId) {
      return {
        ...state,
        selectedParameter: state.selectedParameter.parameter_id === payload.removedParameterId ?
          { } :
          state.selectedParameter,
        parameters: state.parameters.filter(param => param.parameter_id !== payload.removedParameterId)
      }
    } else if (payload.changedParameter) {
      return {
        ...state,
        parameters: state.parameters.map(param => {
          return param.parameter_id === payload.changedParameter._id ?
            {
              ...param,
              code: payload.changedParameter.code,
              color: payload.changedParameter.color
            } :
            param
        })
      }
    }

    return state
  },

  [actionTypes.SELECT_LIBRARY_OBJECT]: (state, action) => {
    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: true,
        width: action.object.width,
        height: action.object.height,
        top: -30,
        left: -30
      },
      selectedParameter: {},
      selectedObjects: uniqBy(orderBy([
        JSON.stringify(action.object) !== JSON.stringify(state.selectedObject) ?
          (<any>Object).assign({}, action.object) :
          {}
      ], ['order'], ['desc']), 'hash'),
      buffer: {
        ...state.buffer,
        objects: []
      }
    }
  },

  [actionTypes.SAVE_OBJECT_Z_INDEX]: (state, action) => {
    return {
      ...state,
      selectedObject: {
        ...state.selectedObject,
        onTopOfUser: action.onTopOfUser
      },
      objects: state.objects.map(object => {
        if (object.object_id === action.object.object_id) {
          return {
            ...object,
            onTopOfUser: action.onTopOfUser
          }
        }

        return object
      })
    }
  },

  [actionTypes.TOGGLE_LIBRARY_MODE]: (state, _, fullState) => {
    return {
      ...state,
      library: {
        ...state.library,
        editObjectsMode: !fullState.library.editObjectsMode
      }
    }
  },

  [actionTypes.SHOW_OBJECT_ON_MAP]: (state, action) => {
    return {
      ...state,
      loading: true,
      objects:
        state.objects.findIndex(object => object.hash === action.object.hash) < 0 ?
          state.objects
            .concat([{
              ...action.object,
              onTopOfUser: isOnTopOfUser(action.object.layer.name),
              order: action.object.layer.number * action.object.createdAt
            }]) :
          state.objects
    }
  },

  [actionTypes.OBJECT_ADDED_TO_MAP]: (state, action) => {
    return {
      ...state,
      objects: state.objects.map(item => {
        const object = action.createdObjects.find(obj => item.hash === obj.hash)

        return object ?
          {
            ...item,
            onTopOfUser: isOnTopOfUser(object.layer.name),
            order: object.layer.number * object.createdAt
          } : item
      }),
      selectedObjects: state.selectedObjects.map(item => {
        const object = action.createdObjects.find(obj => item.hash === obj.hash)

        return object ? {
          ...item,
          onTopOfUser: isOnTopOfUser(object.layer.name),
          order: object.layer.number * object.createdAt
        } : { ...item }
      }),
      loading: false,
      loadStatus: LOAD_STATUS_SUCCESSFUL
    }
  },

  [actionTypes.SELECT_OBJECT_ON_MAP]: (state, action) => {
    const layerName = action.object.layer.name
    const layersObj = {}

    LAYERS_NAMES_ARR.forEach(name => {
      layersObj[name] = layerName === name ? [action.object] : []
    })

    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: false
      },
      selectedObjects: uniqBy(orderBy([action.object], ['order'], ['desc']), 'hash'),
      layers: layersObj
    }
  },

  [actionTypes.SELECT_OBJECTS_ON_MAP]: (state, action) => {
    const layersObj = {}

    LAYERS_NAMES_ARR.forEach(name => {
      layersObj[name] = orderBy(action.payload.objects
        .filter(obj => obj.layer.name === name), ['order'], ['desc'])
    })

    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: false
      },
      selectedObjects: orderBy(action.payload.objects, ['order'], ['desc']),
      layers: layersObj
    }
  },

  [actionTypes.ADD_SELECTED_OBJECT]: (state, action) => {
    const objects = [...state.selectedObjects, action.payload.object]
    const layersObj = {}

    LAYERS_NAMES_ARR.forEach(name => {
      layersObj[name] = orderBy(objects.filter(obj => obj.layer.name === name), ['order'], ['desc'])
    })

    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: false
      },
      selectedObjects: uniqBy(orderBy(state.selectedObjects
        .filter(obj => obj.hash !== action.payload.object.hash)
        .concat([action.payload.object]), ['order'], ['desc']), 'hash'),
      layers: layersObj
    }
  },

  [actionTypes.UNSELECT_OBJECT]: (state, action) => {
    const layersObj = {}

    LAYERS_NAMES_ARR.forEach(name => {
      layersObj[name] = state.layers[name].filter(obj => obj.hash !== action.payload.object.hash)
    })

    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: false
      },
      selectedObjects: state.selectedObjects.filter(obj => obj.hash !== action.payload.object.hash),
      layers: layersObj
    }
  },

  [actionTypes.UNSELECT_LAYER]: (state, action) => {
    return {
      ...state,
      selectedObjects: state.selectedObjects.filter(obj => obj.layer.name !== action.payload.layerName),
      layers: {
        ...state.layers,
        [action.payload.layerName]: []
      }
    }
  },

  [actionTypes.SHOW_REMOVING_OBJECT_ON_MAP]: (state, action) => {
    return {
      ...state,
      loading: true,
      objects: state.objects.filter(object => object.hash !== action.object.hash)
    }
  },

  [actionTypes.OBJECT_REMOVED_ON_MAP]: (state, action) => {
    const layersObj = {}

    LAYERS_NAMES_ARR.forEach(name => {
      layersObj[name] = state.layers[name].filter(obj => obj.hash !== action.object.hash)
    })

    return {
      ...state,
      loading: false,
      loadStatus: LOAD_STATUS_SUCCESSFUL,
      selectedObjects: state.selectedObjects.filter(obj => obj.hash !== action.object.hash),
      layers: layersObj
    }
  },

  [actionTypes.REMOVE_OBJECTS_ON_MAP]: (state, action) => {
    const layersObj = {}

    LAYERS_NAMES_ARR.forEach(name => {
      layersObj[name] = state.layers[name]
        .filter(selectedObj => !action.payload.objects.some(obj => obj.hash === selectedObj.hash))
    })

    return {
      ...state,
      objects: state.objects.filter(mapObj => !action.payload.objects.some(obj => obj.hash === mapObj.hash)),
      selectedObjects: state.selectedObjects
        .filter(selected => !action.payload.objects.some(obj => obj.hash === selected.hash)),
      layers: layersObj
    }
  },

  [actionTypes.PUT_OBJECTS_INTO_BUFFER]: (state, action) => ({
    ...state,
    buffer: {
      ...state.buffer,
      action: action.payload.action,
      objects: state.objects.filter(mapObj => action.payload.objects.some(obj => obj.hash === mapObj.hash))
    },
    cellPointer: {
      ...state.cellPointer,
      show: true,
      width: 'auto',
      height: 'auto'
    }
  }),

  [actionTypes.CLEAR_BUFFER]: (state) => ({
    ...state,
    buffer: {
      ...state.buffer,
      action: null,
      objects: []
    }
  }),

  [actionTypes.ADD_OBJECTS]: (state, action) => ({
    ...state,
    objects: orderBy(state.objects
      .filter(obj => !action.payload.objects.some(pObj => pObj.hash === obj.hash))
      .concat(action.payload.objects)
      .map(object => {
        return {
          ...object,
          onTopOfUser: isOnTopOfUser(object.layer.name),
          order: object.layer.number * object.createdAt
        }
      }), ['order'], ['desc'])
  }),

  [actionTypes.SHOW_MAP_OBJECT_CONTEXT_MENU]: (state, action) => ({
    ...state,
    contextMenuObject: action.payload.object
  }),

  [actionTypes.HIDE_MAP_OBJECT_CONTEXT_MENU]: (state) => ({
    ...state,
    contextMenuObject: {}
  }),

  [actionTypes.CHANGE_OBJECT_POSITION]: (state, action) => {
    return {
      ...state,
      objects: state.objects.map(obj => ({
        ...obj,
        position: obj.hash === action.object.hash ? action.newPos : obj.position
      }))
    }
  },

  [actionTypes.MOVE_OBJECTS]: (state, action) => {
    const layersObj = {}

    LAYERS_NAMES_ARR.forEach(name => {
      layersObj[name] = state.layers[name].map(obj => ({
        ...obj,
        position: (action.payload.moveObjects.find(moveObj => moveObj.hash === obj.hash) || obj).position
      }))
    })

    return {
      ...state,
      objects: state.objects.map(obj => ({
        ...obj,
        position: (action.payload.moveObjects.find(moveObj => moveObj.hash === obj.hash) || obj).position
      })),
      selectedObjects: state.selectedObjects.map(obj => ({
        ...obj,
        position: (action.payload.moveObjects.find(moveObj => moveObj.hash === obj.hash) || obj).position
      })),
      layers: layersObj
    }
  },

  [actionTypes.MOVE_OBJECTS_ON_MAP]: (state, action) => {
    return {
      ...state,
      objects: state.objects.map(obj => ({
        ...obj,
        position: (action.payload.moveObjects.find(moveObj => moveObj.hash === obj.hash) || obj).position
      }))
    }
  },

  [actionTypes.TOGGLE_COORDINATES]: (state) => {
    return {
      ...state,
      showCoordinates: !state.showCoordinates
    }
  },

  [actionTypes.SET_CURRENT_TAB]: (state, action) => {
    return {
      ...state,
      loadStatus: LOAD_STATUS_SUCCESSFUL,
      selectedParameter: {},
      selectedObjects: [TAB_LAYERS, TAB_OBJECTS].includes(action.tab) ?
        state.selectedObjects :
        initialState.mapData.selectedObjects,
      layers: [TAB_LAYERS, TAB_OBJECTS].includes(action.tab) ? state.layers : initialState.mapData.layers,
      highlightedCells: {},
      areas: {
        ...state.areas,
        list: state.areas.list
          .map(area => ({ ...area, selected: false }))
      },
      cellPointer: {
        ...state.cellPointer,
        width: 30,
        height: 30
      },
      blocking: {
        ...state.blocking,
        blockingTool: action.tab === TAB_BLOCKING ? state.blocking.blockingTool : ''
      }
    }
  },

  [actionTypes.SET_MAP_POSITION]: (state, action) => {
    return {
      ...state,
      mapPosition: {
        x: action.x,
        y: action.y
      }
    }
  },

  [actionTypes.TOGGLE_FULLSCREEN_MODE]: (state) => {
    return {
      ...state,
      fullscreenMode: !state.fullscreenMode
    }
  },

  [actionTypes.TOGGLE_VIEW_LAYERS]: (state) => {
    return {
      ...state,
      viewToggle: !state.viewToggle
    }
  },

  [actionTypes.SET_TOGGLE_VIEW_LAYERS]: (state, action) => {
    return {
      ...state,
      viewToggleLayers: action.layers
    }
  },

  [actionTypes.UPDATE_INFO_TEXT]: (state, action) => {
    return {
      ...state,
      infoText: action.text
    }
  },

  [actionTypes.HIDE_PRELOADER]: state => {
    return {
      ...state,
      loading: false
    }
  },

  [actionTypes.LOADING]: (state, action) => ({
    ...state,
    loading: action.payload
  }),

  [actionTypes.ADD_AREA_TO_LIBRARY]: (state) => {
    return {
      ...state
    }
  },

  [actionTypes.REMOVE_AREA_FROM_LIBRARY]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.filter(area => area._id !== action._id)
      }
    }
  },

  [actionTypes.SELECT_AREA]: (state) => {
    return {
      ...state,
      cellPointer: {
        ...state.cellPointer,
        show: true,
        width: 30,
        height: 30,
        top: -30,
        left: -30
      },
      selectedParameter: {},
      selectedObjects: [],
      layers: initialState.mapData.layers
    }
  },

  [actionTypes.ADD_AREA_ON_MAP]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.concat([action.area])
      }
    }
  },

  [actionTypes.ADD_AREAS_ON_MAP]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        list: uniqBy(state.areas.list.concat(action.areas.map(area => ({ ...area, saved: true }))), 'hash')
      }
    }
  },

  [actionTypes.SELECT_AREA_ON_MAP]: (state, action) => {
    const areasList = state.areas.list.map(area => {
      if (area.hash === action.area.hash) {
        return { ...area, selected: !area.selected }
      }

      return area
    })

    return {
      ...state,
      areas: {
        ...state.areas,
        list: areasList
      }
    }
  },

  [actionTypes.SAVE_AREAS_ON_MAP]: (state) => {
    const areasToSave = state.areas.list.filter(area => !area.saved)
    const quantities = {}

    areasToSave.forEach(area => {
      quantities[area._id] = quantities[area._id] > 0 ? quantities[area._id] += 1 : 1
    })

    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.map(area => ({
          ...area,
          saved: true,
          selected: false
        }))
      },
      cellPointer: {
        ...state.cellPointer,
        show: false
      }
    }
  },

  [actionTypes.DELETE_AREAS_ON_MAP]: (state) => {
    const areasToRemove = state.areas.list.filter(area => area.selected)
    const quantities = {}

    areasToRemove.forEach(area => {
      quantities[area._id] = quantities[area._id] > 0 ? quantities[area._id] += 1 : 1
    })

    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.filter(area => !area.selected)
      }
    }
  },

  [actionTypes.DELETE_AREAS_ON_MAP_BY_HASHES]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.filter(area => !action.hashes.includes(area.hash))
      }
    }
  },

  [actionTypes.HIDE_AREAS_EDITOR]: state => {
    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.filter(area => area.saved).map(area => ({ ...area, selected: false }))
      },
      cellPointer: {}
    }
  },

  [actionTypes.SET_SCALE]: (state, action) => {
    return {
      ...state,
      scale: action.scale
    }
  },

  [actionTypes.CHANGE_OBJECTS_ORDER]: (state, action) => {
    const layerName = action.selectedObjects[0].layer.name

    return {
      ...state,
      selectedObjects: orderBy(
        updateObjectsOrderInList(state.selectedObjects, action.selectedObjects), ['order'], ['desc']
      ),
      layers: {
        ...state.layers,
        [layerName]: orderBy(action.selectedObjects.map(obj => ({
          ...obj,
          order: obj.layer.number * obj.createdAt
        })), ['order'], ['desc'])
      }
    }
  },

  [actionTypes.CHANGE_LAYER]: (state, action) => {
    const { object, layerName, newLayer } = action
    const oldObjectLayer = object.layer.name

    return {
      ...state,
      layers: {
        ...state.layers,
        [oldObjectLayer]: state.layers[oldObjectLayer].filter(obj => obj.hash !== object.hash),
        [layerName]: newLayer
      },
      selectedObjects: updateObjectsOrderInList(state.selectedObjects, newLayer)
    }
  },

  [actionTypes.UPDATE_OBJECTS_ORDER]: (state, action) => {
    return {
      ...state,
      objects: updateObjectsOrderInList(state.objects, action.objects),
      selectedObjects: updateObjectsOrderInList(state.selectedObjects, action.objects),
      layers: updateLayers(state.layers, action.objects)
    }
  },

  [aBlocking.SET_BLOCKING_TOOL]: (state) => ({
    ...state,
    cellPointer: {
      ...state.cellPointer,
      show: true
    }
  })
}

export default function mapDataReducer(state: any = initialState.mapData, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]
  const nextState = handler ? handler(state, action, fullState) : state

  if (parametersSearch.hasActionHandler(action.type)) {
    nextState.parametersSearch = parametersSearch(state.parametersSearch, action, fullState)
  }

  if (blocking.hasActionHandler(action.type)) {
    nextState.blocking = blocking(state.blocking, action, fullState)
  }

  if (npc.hasActionHandler(action.type)) {
    nextState.npc = npc(state.npc, action, fullState)
  }

  return nextState
}
