import { handleActions } from 'redux-actions'
import uniqBy from 'lodash.uniqby'
import * as aNpc from '../../actionTypes/npc'

const initialState = []

export const ACTION_HANDLERS = {
  [aNpc.PLACE_NPC_ON_MAP]: (state, { payload }) => {
    return uniqBy(state.concat([payload.npc]), 'hash')
  },

  [aNpc.REMOVE_NPC_FROM_MAP]: (state, { payload }) => {
    return state.filter(item => item.hash !== payload.npc.hash)
  },

  [aNpc.UPDATE_LIBRARY_NPC]: (state, { payload }) => {
    console.log(payload.npc)
    return state.map(npcItem => {
      return npcItem.libraryId === payload.npc.libraryId ? {
        ...npcItem,
        name: payload.npc.name,
        type: payload.npc.type,
        color: payload.npc.color,
        radius: payload.npc.radius
      } :
        npcItem
    })
  },

  [aNpc.REMOVE_LIBRARY_NPC]: (state, { payload }) => {
    return state.filter(npcItem => npcItem.libraryId !== payload.id)
  }
}

const npc = handleActions(ACTION_HANDLERS, initialState)

npc.hasActionHandler = (actionType: string) => ACTION_HANDLERS[actionType] !== undefined

export default npc
