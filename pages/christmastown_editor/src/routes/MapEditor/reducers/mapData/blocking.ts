import { handleActions } from 'redux-actions'
import * as a from '../../actionTypes/blocking'
import { positionsEqual } from '../../../../utils'

const initialState = {
  blockingTool: '',
  cells: []
}

export const ACTION_HANDLERS = {
  [a.SET_BLOCKING_TOOL]: (state, action) => ({
    ...state,
    blockingTool: action.payload.blockingTool
  }),

  [a.UPDATE_CELLS]: (state, action) => {
    const cells = [...state.cells]

    action.payload.cells.forEach(cell => {
      const cellIndex = cells.findIndex(c => positionsEqual(c.position, cell.position))

      if (cellIndex === -1) {
        cells.push(cell)
      } else {
        cells[cellIndex] = { ...cells[cellIndex], ...cell }
      }
    })

    return {
      ...state,
      cells
    }
  }
}

const blocking = handleActions(ACTION_HANDLERS, initialState)

blocking.hasActionHandler = (actionType: string) => ACTION_HANDLERS[actionType] !== undefined

export default blocking
