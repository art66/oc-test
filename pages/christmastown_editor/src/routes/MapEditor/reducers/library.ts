import countBy from 'lodash.countby'
import identity from 'lodash.identity'
import orderBy from 'lodash.orderby'
import initialState from './initialState'
import * as actionTypes from '../actionTypes'
import * as aLib from '../actionTypes/library'
import * as aNpc from '../actionTypes/npc'
import * as c from '../constants'
import { LAYERS_MAP } from '../constants/layers'

const handleInitialLibraryData = (libraryData, state) => {
  const { recentlyUsed, parameterCategories, objectCategories, objects, areas, libraryNpc } = libraryData

  return {
    parameterCategories: [{ name: c.RECENTLY_USED, parameters: recentlyUsed.parameters }]
      .concat(parameterCategories),
    objects,
    recentlyUsedObjects: orderBy(recentlyUsed.objects, ['order'], ['desc']),
    objectCategories: objectCategories.map(category => {
      let layerIsModified = false

      if (category.layer) {
        layerIsModified = Object.keys(objects).some(objectID => {
          return category.name.toLowerCase() === objects[objectID].category.toLowerCase() &&
            category.layer !== objects[objectID].layer.name
        })
      }
      return {
        ...category,
        categoryLayerIsModified: layerIsModified
      }
    }),
    areas: {
      ...state.areas,
      selectedArea: {},
      editingArea: null,
      list: areas
    },
    libraryNpc
  }
}

const ACTION_HANDLERS = {
  [actionTypes.MAP_EDITOR_DATA_FETCHED]: (state, action) => {
    return {
      ...state,
      ...handleInitialLibraryData(action.json, initialState.library)
    }
  },

  [actionTypes.SYNCHRONIZE_MAP_DATA]: (state, action) => {
    return {
      ...state,
      ...handleInitialLibraryData(action.payload.mapData, state),
      popupObject: {}
    }
  },

  [actionTypes.SHOW_PARAMETER_ON_MAP]: (state, action) => {
    const parameterCategories = [...state.parameterCategories]

    if (action.websocket) {
      parameterCategories.forEach((category, i) => {
        parameterCategories[i].parameters.forEach((parameter, parameterIndex) => {
          if (parameter.parameter_id === action.parameter.parameter_id) {
            parameterCategories[i].parameters[parameterIndex].quantity += 1
          }
        })

        if (category.name === c.RECENTLY_USED) {
          parameterCategories[i].parameters = [action.parameter]
            .concat(parameterCategories[i].parameters.filter(p => p.parameter_id !== action.parameter.parameter_id))
            .slice(0, c.MAX_RECENTLY_USED_PARAMS)
        }
      })
    }

    return {
      ...state,
      parameterCategories
    }
  },

  [actionTypes.SHOW_REMOVING_PARAMETER_ON_MAP]: (state, action) => {
    const parameterCategories = [...state.parameterCategories]

    if (action.websocket) {
      parameterCategories.forEach((_, i) => {
        parameterCategories[i].parameters.forEach((parameter, parameterIndex) => {
          if (parseInt(parameter.parameter_id) === parseInt(action.parameter.parameter_id)) {
            parameterCategories[i].parameters[parameterIndex].quantity =
              parameterCategories[i].parameters[parameterIndex].quantity > 0
                ? parameterCategories[i].parameters[parameterIndex].quantity - 1
                : 0
          }
        })
      })
    }

    return {
      ...state,
      parameterCategories
    }
  },

  [aLib.UPDATE_PARAMETERS_COUNT]: (state, action) => {
    return {
      ...state,
      parameterCategories: state.parameterCategories.map(category => {
        return {
          ...category,
          parameters: category.parameters.map(parameter => {
            if (parameter.parameter_id === action.payload.parameterID) {
              return {
                ...parameter,
                quantity: action.payload.count
              }
            }
            return parameter
          })
        }
      })
    }
  },

  [actionTypes.PARAMETER_REMOVED_ON_MAP]: (state, action) => {
    const parameterCategories = [...state.parameterCategories]
    parameterCategories.forEach((_, i) => {
      parameterCategories[i].parameters.forEach((parameter, parameterIndex) => {
        if (parseInt(parameter.parameter_id) === parseInt(action.parameter.parameter_id)) {
          parameterCategories[i].parameters[parameterIndex].quantity = action.parameter.quantity
        }
      })
    })

    return {
      ...state,
      parameterCategories
    }
  },

  [aLib.SHOW_LIBRARY_OBJECT_POPUP]: (state, action) => ({
    ...state,
    popupObject: action.payload.object
  }),

  [aLib.HIDE_LIBRARY_OBJECT_POPUP]: (state) => ({
    ...state,
    popupObject: {}
  }),

  [actionTypes.SAVE_OBJECT_POSITION_IN_LIBRARY]: (state, action) => {
    return {
      ...state,
      objects: {
        ...state.objects,
        [action.object.object_id]: {
          ...state.objects[action.object.object_id],
          position: action.position
        }
      }
    }
  },

  [actionTypes.SAVE_OBJECT_Z_INDEX]: (state, action) => {
    return {
      ...state,
      objects: {
        ...state.objects,
        [action.object.object_id]: {
          ...state.objects[action.object.object_id],
          onTopOfUser: action.onTopOfUser
        }
      }
    }
  },

  [actionTypes.TOGGLE_LIBRARY_MODE]: state => {
    return {
      ...state,
      editObjectsMode: !state.editObjectsMode,
      ...!state.editObjectsMode && { popupObject: {} }
    }
  },

  [actionTypes.SHOW_OBJECT_ON_MAP]: (state, action) => {
    return {
      ...state,
      objects: {
        ...state.objects,
        [action.object.object_id]: {
          ...state.objects[action.object.object_id],
          quantity: state.objects[action.object.object_id].quantity + 1
        }
      },
      recentlyUsedObjects: state.recentlyUsedObjects
        .filter(obj => obj.object_id !== action.object.object_id)
        .concat([{
          ...action.object,
          order: state.recentlyUsedObjects[0] && state.recentlyUsedObjects[0].order + 1 || 1
        }])
    }
  },

  [actionTypes.SHOW_REMOVING_OBJECT_ON_MAP]: (state, action) => {
    return {
      ...state,
      objects: {
        ...state.objects,
        [action.object.object_id]: {
          ...state.objects[action.object.object_id],
          quantity: state.objects[action.object.object_id].quantity > 0
            ? state.objects[action.object.object_id].quantity - 1 : 0
        }
      }
    }
  },

  [actionTypes.OBJECT_REMOVED_ON_MAP]: (state, action) => {
    return {
      ...state,
      objects: {
        ...state.objects,
        [action.object.object_id]: {
          ...state.objects[action.object.object_id],
          quantity: action.object.quantity
        }
      }
    }
  },

  [actionTypes.SET_CURRENT_TAB]: (state, action) => {
    return {
      ...state,
      prevTab: state.prevTab === action.tab ? null : state.currentTab,
      currentTab: action.tab
    }
  },

  [actionTypes.SET_FILTER_CONDITION]: (state, action) => {
    return {
      ...state,
      filterObjectsConditions: {
        ...state.filterObjectsConditions,
        [action.name]: action.value
      }
    }
  },

  [actionTypes.UPDATE_PARAMETER_LIST]: (state, { payload }) => {
    return {
      ...state,
      parameterCategories: [{
        name: c.RECENTLY_USED,
        parameters: (payload && payload.recentlyUsed && payload.recentlyUsed.parameters) || []
      }].concat(payload.parameterCategories)
    }
  },

  [actionTypes.SHOW_AREA_FORM]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        editingArea: state.areas.list.find(area => area._id === action._id) || {}
      }
    }
  },

  [actionTypes.HIDE_AREA_FORM]: state => {
    return {
      ...state,
      areas: {
        ...state.areas,
        editingArea: null
      }
    }
  },

  [actionTypes.CHANGE_EDITING_AREA]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        editingArea: {
          ...state.areas.editingArea,
          ...action.area
        }
      }
    }
  },

  [actionTypes.SAVE_AREA_IN_LIBRARY]: (state, action) => {
    const exists = state.areas.list.some(area => area._id === action.area._id)

    return {
      ...state,
      areas: {
        ...state.areas,
        editingArea: action.websocket ? state.areas.editingArea : null,
        list: exists ?
          state.areas.list.map(area => {
            return action.area._id === area._id ? {
              ...area,
              ...action.area
            } : area
          }) :
          state.areas.list.concat([action.area])
      }
    }
  },

  [actionTypes.REMOVE_AREA_FROM_LIBRARY]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.filter(area => area._id !== action._id),
        selectedArea: action.websocket ? state.areas.selectedArea : {}
      }
    }
  },

  [actionTypes.SELECT_AREA]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        selectedArea: action.area
      }
    }
  },

  [actionTypes.ADD_AREAS_ON_MAP]: (state, action) => {
    const areasToSave = action.areas
    const quantities = {}
    areasToSave.forEach(area => {
      quantities[area._id] = quantities[area._id] > 0 ? ++quantities[area._id] : 1
    })

    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.map(area => ({
          ...area,
          quantity: quantities[area._id] ? area.quantity + quantities[area._id] : area.quantity
        }))
      }
    }
  },

  [actionTypes.SELECT_AREA_ON_MAP]: (state, action, fullState) => {
    const areasList = fullState.mapData.areas.list.map(area => {
      if (area.hash === action.area.hash) {
        return { ...area, selected: !area.selected }
      }

      return area
    })

    return {
      ...state,
      areas: {
        ...state.areas,
        selectedArea: areasList.some(area => area.selected) ? action.area : {}
      }
    }
  },

  [actionTypes.SAVE_AREAS_ON_MAP]: state => {
    return {
      ...state,
      areas: {
        ...state.areas,
        selectedArea: {}
      }
    }
  },

  [actionTypes.DELETE_AREAS_ON_MAP]: (state, _, fullState) => {
    const areasToRemove = fullState.mapData.areas.list.filter(area => area.selected)
    const quantities = {}
    areasToRemove.forEach(area => {
      quantities[area._id] = quantities[area._id] > 0 ? ++quantities[area._id] : 1
    })

    return {
      ...state,
      areas: {
        ...state.areas,
        selectedArea: {},
        list: state.areas.list.map(area => ({
          ...area,
          quantity: quantities[area._id] ? area.quantity - quantities[area._id] : area.quantity
        }))
      }
    }
  },

  [actionTypes.DELETE_AREAS_ON_MAP_BY_HASHES]: (state, action, fullState) => {
    const _ids = fullState.mapData.areas.list.filter(area => action.hashes.includes(area.hash)).map(area => area._id)
    const idsCounts = countBy(_ids, identity)

    return {
      ...state,
      areas: {
        ...state.areas,
        list: state.areas.list.map(area => ({
          ...area,
          quantity: _ids.includes(area._id) ? area.quantity - idsCounts[area._id] : area.quantity
        }))
      }
    }
  },

  [actionTypes.HIDE_AREAS_EDITOR]: state => {
    return {
      ...state,
      areas: {
        ...state.areas,
        selectedArea: {}
      }
    }
  },

  [actionTypes.SHOW_AREAS_DIALOG]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        dialog: action.dialog
      }
    }
  },

  [actionTypes.HIDE_AREAS_DIALOG]: state => {
    return {
      ...state,
      areas: {
        ...state.areas,
        dialog: {}
      }
    }
  },

  [aLib.ADD_TAGS_TO_OBJECT]: (state, action) => ({
    ...state,
    objects: {
      ...state.objects,
      [action.payload.objectLibraryId]: {
        ...state.objects[action.payload.objectLibraryId],
        tags: (state.objects[action.payload.objectLibraryId].tags || []).concat(action.payload.tags)
      }
    }
  }),

  [aLib.REMOVE_OBJECT_TAGS]: (state, action) => ({
    ...state,
    objects: {
      ...state.objects,
      [action.payload.objectLibraryId]: {
        ...state.objects[action.payload.objectLibraryId],
        tags: state.objects[action.payload.objectLibraryId].tags
          .filter(tag => action.payload.tags.every(removeTag => removeTag !== tag))
      }
    }
  }),

  [aLib.SET_DEFAULT_LAYER]: (state, action) => {
    const newObjects = {
      ...state.objects,
      [state.popupObject.object_id]: {
        ...state.objects[state.popupObject.object_id],
        layer: {
          name: action.payload.layerName,
          number: LAYERS_MAP[action.payload.layerName]
        }
      }
    }
    const categoryObj = state.objectCategories
      .find(category => {
        return category.name.toLowerCase() === state.filterObjectsConditions.category.toLowerCase()
      })
    let layerIsModified = false

    if (categoryObj.layer) {
      layerIsModified = Object.keys(newObjects).some(objID => {
        return categoryObj.name.toLowerCase() === newObjects[objID].category.toLowerCase() &&
          categoryObj.layer !== newObjects[objID].layer.name
      })
    }

    return {
      ...state,
      popupObject: {
        ...state.popupObject,
        layer: {
          name: action.payload.layerName,
          number: LAYERS_MAP[action.payload.layerName]
        }
      },
      objects: newObjects,
      objectCategories: state.objectCategories.map(category => {
        return category.name.toLowerCase() === state.filterObjectsConditions.category.toLowerCase() ?
          {
            ...category,
            categoryLayerIsModified: layerIsModified
          } : category
      })
    }
  },

  [actionTypes.UNSELECT_ON_MAP]: (state) => {
    return {
      ...state,
      popupObject: {}
    }
  },

  [aNpc.ADD_LIBRARY_NPC]: (state, { payload }) => {
    return {
      ...state,
      libraryNpc: [
        ...state.libraryNpc,
        payload.npc
      ]
    }
  },

  [aNpc.UPDATE_LIBRARY_NPC]: (state, { payload }) => {
    return {
      ...state,
      libraryNpc: state.libraryNpc.map(npc => npc.libraryId === payload.npc.libraryId ? { ...payload.npc } : npc)
    }
  },

  [aNpc.REMOVE_LIBRARY_NPC]: (state, { payload }) => {
    return {
      ...state,
      libraryNpc: state.libraryNpc.filter(npc => npc.libraryId !== payload.id)
    }
  },

  [aNpc.UPDATE_LIBRARY_NPC_QUANTITY]: (state, { payload }) => {
    return {
      ...state,
      libraryNpc: state.libraryNpc.map(npc => {
        return npc.libraryId === payload.id ?
          {
            ...npc,
            quantity: npc.quantity + payload.value
          } :
          npc
      })
    }
  }
}

export default function libraryReducer(state: any = initialState.library, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action, fullState) : state
}
