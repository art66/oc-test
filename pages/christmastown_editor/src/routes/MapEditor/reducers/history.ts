import uniqBy from 'lodash.uniqby'
import orderBy from 'lodash.orderby'
import initialState from './initialState'
import * as actionTypes from '../actionTypes/history'
import { SYNCHRONIZE_MAP_DATA } from '../actionTypes'
import { getDate, getPosition } from '../utils'
import { OBJECTS_KEY_IN_PAYLOAD } from '../constants/history'

const HISTORY_ITEMS_LIMIT = 100

const handleInitialHistoryActions = actions => {
  return uniqBy(orderBy(actions, 'timestamp', 'desc'), 'actionId')
    .slice(0, HISTORY_ITEMS_LIMIT)
    .map(item => {
      const isArr = item.payload[OBJECTS_KEY_IN_PAYLOAD[item.type]].length

      return {
        ...item,
        date: getDate(item.timestamp),
        position: isArr ?
          getPosition(item.payload[OBJECTS_KEY_IN_PAYLOAD[item.type]]) :
          item.payload[OBJECTS_KEY_IN_PAYLOAD[item.type]].position
      }
    })
}

const ACTION_HANDLERS = {
  [SYNCHRONIZE_MAP_DATA]: (_, { payload }) => {
    return {
      ...initialState.history,
      actions: handleInitialHistoryActions(payload.mapData.history)
    }
  },

  [actionTypes.HISTORY_DATA_FETCHED]: (state, action) => {
    return {
      ...state,
      actions: handleInitialHistoryActions(action.payload.json.history)
    }
  },
  [actionTypes.SAVE_TO_HISTORY]: (state, action) => {
    const newAction = { ...action.payload.action }
    const payload = newAction.payload[OBJECTS_KEY_IN_PAYLOAD[newAction.type]]
    const isArr = payload.length
    const updatedAction = {
      ...newAction,
      position: isArr ? getPosition(payload) : payload.position
    }
    const newActionArr = state.actions.some(item => item.actionId === action.payload.action.actionId) ?
      [...state.actions] :
      [updatedAction, ...state.actions]

    return {
      ...state,
      actions: uniqBy(
        orderBy(
          newActionArr.map(item => {
            return action.payload.action.user.userID === item.user.userID ? { ...item, isReverted: false } : item
          }),
          'timestamp',
          'desc'
        ),
        'actionId'
      ).slice(0, HISTORY_ITEMS_LIMIT)
    }
  },
  [actionTypes.UNDO_ACTION]: (state, action) => {
    return {
      ...state,
      actions: state.actions.map(item => {
        return item.actionId === action.payload.action.actionId ? { ...item, isReverted: true } : item
      })
    }
  },
  [actionTypes.REDO_ACTION]: (state, action) => {
    return {
      ...state,
      actions: state.actions.map(item => {
        return item.actionId === action.payload.action.actionId ? { ...item, isReverted: false } : item
      })
    }
  },
  [actionTypes.UPDATE_ACTION]: (state, action) => {
    return {
      ...state,
      actions: uniqBy(
        state.actions.map(item => {
          return item.actionId === action.payload.id ?
            {
              ...item,
              actionId: action.payload.action.actionId,
              status: action.payload.action.status,
              timestamp: action.payload.action.timestamp,
              date: getDate(action.payload.action.timestamp)
            } : item
        }),
        'actionId'
      )
    }
  },
  [actionTypes.REMOVE_FROM_HISTORY]: (state, action) => {
    return {
      ...state,
      actions: state.actions.filter(item => !action.payload.IDs.some(id => id === item.actionId))
    }
  }
}

export default function historyReducer(state: any = initialState.history, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action, fullState) : state
}
