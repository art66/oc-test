import { getCookie } from '@torn/shared/utils'
import { HIGHEST_LAYER, HIGH_LAYER, LOW_LAYER, LOWEST_LAYER } from '../constants/layers'
import {
  FULLSCREEN_MODE,
  FULLSCREEN_MODE_COOKIE,
  COORDINATES_COOKIE,
  VIEW_TOGGLE,
  DEFAULT_COOKIE_VIEW_TOGGLE
} from '../constants'

export default {
  library: {
    currentTab: 'objects',
    filterObjectsConditions: {
      category: 'Housing Fancy Wood'
    },
    editObjectsMode: false,
    objectCategories: [],
    popupObject: {},
    objects: [],
    recentlyUsedObjects: [],
    areas: {
      selectedArea: {},
      editingArea: null,
      list: []
    },
    libraryNpc: [
      {
        id: 444,
        code: 'S',
        name: 'Santa',
        color: '000000',
        quantity: 2,
        icon: 'santa',
        speed: '5',
        radius: 1,
        outcomes: [
          {
            chance: '10',
            type: 'Jail',
            text: 'Some text 1',
            positions: [{ x: 1, y: 1 }]
          }
        ]
      },
      {
        id: 333,
        code: 'G',
        name: 'Grinch',
        color: '000000',
        quantity: 5,
        icon: 'grinch',
        speed: '5',
        radius: 2,
        outcomes: [
          {
            chance: '10',
            type: 'Teleport',
            text: 'Some text 2',
            positions: [{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }]
          }
        ]
      }
    ]
  },
  mapData: {
    scale: 1,
    mapName: 'CT Map',
    loading: true,
    loadStatus: {
      status: 'success',
      message: ''
    },
    loadingInitData: true,
    cellPointer: {
      show: false,
      top: 0,
      left: 0,
      x: 0,
      y: 0,
      width: 30,
      height: 30
    },
    mapPosition: {
      // x: 0,
      // y: -MAP_HEIGHT / 2
    },
    endpoints: {
      leftTop: { x: 0, y: 0 },
      rightBottom: { x: 30, y: -30 }
    },
    fullscreenMode: getCookie(FULLSCREEN_MODE_COOKIE) === FULLSCREEN_MODE,
    viewToggle: getCookie(VIEW_TOGGLE) && JSON.parse(getCookie(VIEW_TOGGLE)).open || DEFAULT_COOKIE_VIEW_TOGGLE.open,
    viewToggleLayers: getCookie(VIEW_TOGGLE) && JSON.parse(getCookie(VIEW_TOGGLE)).tools
      || DEFAULT_COOKIE_VIEW_TOGGLE.tools,
    showCoordinates: getCookie(COORDINATES_COOKIE) ? JSON.parse(getCookie(COORDINATES_COOKIE)) : true,
    selectedNpc: {},
    selectedParameter: {},
    highlightedCells: {},
    selectedObjects: [],
    layers: {
      [HIGHEST_LAYER]: [],
      [HIGH_LAYER]: [],
      [LOW_LAYER]: [],
      [LOWEST_LAYER]: []
    },
    buffer: {
      action: '',
      objects: []
    },
    areas: {
      list: []
    },
    objects: [],
    parameters: [],
    parametersSearch: {},
    blocking: {
      blockingTool: '',
      cells: []
    },
    npc: [],
    contextMenuObject: {}
  },
  parameterCategories: [
    {
      id: 1,
      name: 'category1',
      parameters: [
        {
          parameter_id: 1,
          code: 'MG',
          name: 'Main Entrance Gate',
          color: '000000',
          quantity: 199,
          chance: 10,
          triggers: [
            {
              chance: '100',
              duration: '1:00:00',
              reason: 'Reason1',
              text: 'Trigger text',
              trigger: { id: 1, name: 'Trigger1' }
            }
          ]
        },
        {
          parameter_id: 2,
          code: 'NG',
          name: 'Nain Gate',
          color: '666',
          quantity: 500,
          chance: 10,
          triggers: [
            {
              chance: '50',
              duration: '2:30:00',
              reason: 'Reason2',
              text: 'Trigger2 texttexttext',
              trigger: { id: 1, name: 'Trigger1' }
            }
          ]
        }
      ]
    }
  ],
  history: {
    actions: []
  },
  backups: {
    controlsStatus: 'default', // 'saving'
    controls: {
      canSave: true,
      message: ''
    },
    backupsList: []
  },
  overlay: {
    active: false
  },
  ics: {
    show: false
  },
  user: {}
}
