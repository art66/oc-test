import initialState from './initialState'
import * as actionTypes from '../actionTypes/ics'

const ACTION_HANDLERS = {
  [actionTypes.TOGGLE_ICS]: (state, action) => {
    return {
      ...state,
      show: action.payload.show
    }
  },
  [actionTypes.SET_ICS_INIT_PARAMS]: (state, action) => {
    return {
      ...state,
      initParams: action.payload.initParams
    }
  }
}

export default function icsReducer(state: any = initialState.ics, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action, fullState) : state
}
