import initialState from './initialState'
import * as actionTypes from '../actionTypes'

const ACTION_HANDLERS = {
  [actionTypes.MAP_EDITOR_DATA_FETCHED]: (_, action) => {
    return action.json.user
  },
  [actionTypes.SYNCHRONIZE_MAP_DATA]: (_, action) => {
    return action.payload.mapData.user
  }
}

export default function userReducer(state: any = initialState.user, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action, fullState) : state
}
