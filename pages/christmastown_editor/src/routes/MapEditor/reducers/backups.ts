import initialState from './initialState'
import * as actionTypes from '../actionTypes/backups'
import { SYNCHRONIZE_MAP_DATA } from '../actionTypes'

const handleInitialBackupsList = backups => {
  return backups.map(backup => {
    return {
      ...backup,
      editing: false,
      confirmation: {
        enabled: false,
        type: ''
      }
    }
  })
}

const ACTION_HANDLERS = {
  [SYNCHRONIZE_MAP_DATA]: (_, { payload }) => {
    return {
      ...initialState.backups,
      backupsList: payload.mapData.backups ? handleInitialBackupsList(payload.mapData.backups.versions) : []
    }
  },

  [actionTypes.SET_BACKUPS]: (state, action) => {
    return {
      ...state,
      backupsList: handleInitialBackupsList(action.payload.backups)
    }
  },
  [actionTypes.IS_WRONG_NAME]: (state, action) => {
    return {
      ...state,
      backupsList: state.backupsList.map(item => {
        return {
          ...item,
          wrongName: item.backupId === action.payload.ID ? action.payload.wrongName : false,
          message: action.payload.message
        }
      })
    }
  },
  [actionTypes.CHANGE_CONTROLS_STATE]: (state, action) => {
    return {
      ...state,
      controls: {
        ...state.controls,
        canSave: action.payload.canSave,
        message: action.payload.message
      }
    }
  },
  [actionTypes.CHANGE_CONTROLS_STATUS]: (state, action) => {
    return {
      ...state,
      controlsStatus: action.payload.status
    }
  },
  [actionTypes.EDIT_BACKUP_NAME]: (state, action) => {
    return {
      ...state,
      backupsList: state.backupsList.map(item => {
        const isEditedBackup = item.backupId === action.payload.ID

        return {
          ...item,
          editing: isEditedBackup,
          active: isEditedBackup,
          confirmation: { ...item.confirmation, enabled: false },
          wrongName: false
        }
      })
    }
  },
  [actionTypes.SAVE_BACKUP_NAME]: (state, action) => {
    return {
      ...state,
      backupsList: state.backupsList.map(item => {
        return {
          ...item,
          editing: false,
          name: action.payload.ID === item.backupId ? action.payload.name : item.name,
          active: item.confirmation.enabled
        }
      })
    }
  },
  [actionTypes.SAVE_BACKUP]: (state, action) => {
    return {
      ...state,
      backupsList: [
        {
          ...action.payload.backup,
          editing: false,
          confirmation: {
            enabled: false,
            type: ''
          }
        },
        ...state.backupsList
      ]
    }
  },
  [actionTypes.REMOVE_BACKUP_FROM_LIST]: (state, action) => {
    return {
      ...state,
      backupsList: state.backupsList.filter(item => item.backupId !== action.payload.ID)
    }
  },
  [actionTypes.TOGGLE_BACKUP_CONFIRMATION]: (state, action) => {
    return {
      ...state,
      backupsList: state.backupsList.map(item => {
        const isCurrBackup = item.backupId === action.payload.ID
        const confirmationEnabled = action.payload.confirmation && action.payload.confirmation.enabled

        return {
          ...item,
          confirmation: isCurrBackup ? action.payload.confirmation : { ...item.confirmation, enabled: false },
          active: isCurrBackup && (confirmationEnabled || item.editing),
          editing: false,
          wrongName: false
        }
      })
    }
  }
}

export default function backupsReducer(state: any = initialState.backups, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action, fullState) : state
}
