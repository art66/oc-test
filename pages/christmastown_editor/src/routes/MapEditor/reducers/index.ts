import mapData from './mapData'
import library from './library'
import history from './history'
import backups from './backups'
import overlay from './overlay'
import user from './user'
import ics from './ics'

const rootReducer = (state: any = {}, action: any) => {
  return {
    mapData: mapData(state.mapData, action, state),
    library: library(state.library, action, state),
    history: history(state.history, action, state),
    backups: backups(state.backups, action, state),
    overlay: overlay(state.overlay, action, state),
    user: user(state.user, action, state),
    ics: ics(state.ics, action, state)
  }
}

export default rootReducer
