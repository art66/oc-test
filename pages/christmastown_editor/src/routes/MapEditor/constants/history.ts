export const CREATE_OBJECTS_ACTION_TYPE = 'putObjects'
export const DELETE_OBJECTS_ACTION_TYPE = 'removeObjects'
export const MOVE_OBJECTS_ACTION_TYPE = 'moveObjects'
export const ADD_PARAMETER_ACTION_TYPE = 'putParameter'
export const REMOVE_PARAMETER_ACTION_TYPE = 'removeParameter'
export const DELETE_AREAS_ACTION_TYPE = 'deleteAreasOnMap'
export const UPDATE_OBJECTS_ACTION_TYPE = 'updateObjects'
export const ADD_AREAS_ACTION_TYPE = 'saveAreasOnMap'
export const SET_BLOCK_ACTION_TYPE = 'setBlocks'
export const ADD_NPC_ACTION_TYPE = 'saveMapNpc'
export const REMOVE_NPC_ACTION_TYPE = 'removeMapNpc'

export const ACTION_TYPES = {
  create: 'create',
  delete: 'delete',
  order: 'layers',
  move: 'move'
}

export const TITLES = {
  [ADD_PARAMETER_ACTION_TYPE]: {
    message: 'Create parameter',
    type: ACTION_TYPES.create
  },
  [REMOVE_PARAMETER_ACTION_TYPE]: {
    message: 'Delete parameter',
    type: ACTION_TYPES.delete
  },
  [DELETE_AREAS_ACTION_TYPE]: {
    message: 'Delete area',
    type: ACTION_TYPES.delete
  },
  [UPDATE_OBJECTS_ACTION_TYPE]: {
    message: 'Reorder objects',
    type: ACTION_TYPES.order
  },
  [ADD_AREAS_ACTION_TYPE]: {
    message: 'Create area',
    type: ACTION_TYPES.create
  },
  [ADD_NPC_ACTION_TYPE]: {
    message: 'Create NPC',
    type: ACTION_TYPES.create
  },
  [REMOVE_NPC_ACTION_TYPE]: {
    message: 'Delete NPC',
    type: ACTION_TYPES.delete
  }
}

export const OBJECTS_KEY_IN_PAYLOAD = {
  [CREATE_OBJECTS_ACTION_TYPE]: 'objectsCreated',
  [DELETE_OBJECTS_ACTION_TYPE]: 'objectsRemoved',
  [MOVE_OBJECTS_ACTION_TYPE]: 'objectsMoved',
  [ADD_PARAMETER_ACTION_TYPE]: 'addedParameter',
  [REMOVE_PARAMETER_ACTION_TYPE]: 'deletedParameter',
  [DELETE_AREAS_ACTION_TYPE]: 'deletedAreas',
  [UPDATE_OBJECTS_ACTION_TYPE]: 'oldObjects',
  [ADD_AREAS_ACTION_TYPE]: 'createdAreas',
  [SET_BLOCK_ACTION_TYPE]: 'blocks',
  [ADD_NPC_ACTION_TYPE]: 'savedMapNpc',
  [REMOVE_NPC_ACTION_TYPE]: 'removedMapNpc'
}

export const UNDO_ACTION = 'undo'
export const REDO_ACTION = 'redo'
