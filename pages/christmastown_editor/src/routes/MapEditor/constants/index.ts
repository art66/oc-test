export const RECENTLY_USED = 'recently used'
export const CUT = 'cut'
export const COPY = 'copy'
export const TIME_TO_UPDATE_MAP_FULLSCREEN = 500
export const COORDINATES_COOKIE = 'showCoordinates'
export const FULLSCREEN_MODE_COOKIE = 'CTMode'
export const VIEW_TOGGLE = 'viewToggleCookie'
export const FULLSCREEN_MODE = 'fullscreen'
export const NORMAL_MODE = 'normal'
export const YEAR = 365
export const MAX_LENGTH_INFO_MSG_STATUS_PANEL = 60

export const LOCAL_STORAGE_MAP_PARAMETERS = 'mapParameters'
export const LOCAL_STORAGE_MAP_OBJECTS = 'mapObjects'

export const BACKUPS_TAB = 'backups'
export const HISTORY_TAB = 'history'
export const OBJECTS_TAB = 'objects'
export const LAYERS_TAB = 'layers'
export const PARAMETERS_TAB = 'parameters'
export const NPC_TAB = 'npc'
export const BLOCKING_TAB = 'blocking'
export const AREAS_TAB = 'areas'

export const TOOLS_BUTTONS = [
  { iconName: 'CTObjects', libraryTab: 'objects', label: 'Objects', customPreset: 'OBJECTS' },
  { iconName: 'CTParameters', libraryTab: 'parameters', label: 'Parameters', customPreset: 'PARAMETERS' },
  { iconName: 'CTNpc', libraryTab: 'npc', label: 'NPC', customPreset: 'NPC' },
  { iconName: 'CTBlocking', libraryTab: 'blocking', label: 'Blocking', customPreset: 'BLOCKS' },
  { iconName: 'CTAreas', libraryTab: 'areas', label: 'Areas', customPreset: 'AREAS' },
  { iconName: 'CTLayers', libraryTab: 'layers', label: 'Layers' },
  { iconName: 'CTHistory', libraryTab: 'history', label: 'History' },
  { iconName: 'CTBackups', libraryTab: 'backups', label: 'Backups' }
]

export const TOGGLE_LAYERS = [
  { iconName: 'CTParameters', tab: PARAMETERS_TAB, layer: 'parameters', label: 'Parameters' },
  { iconName: 'CTNpc', tab: NPC_TAB, layer: 'npc', label: 'NPC' },
  { iconName: 'CTBlocking', tab: BLOCKING_TAB, layer: 'blocking', label: 'Blocking' },
  { iconName: 'CTAreas', tab: AREAS_TAB, layer: 'areas', label: 'Areas' }
]

export const DEFAULT_SETTING_VIEW_TOGGLE = {
  parameters: false,
  npc: false,
  blocking: false,
  areas: false
}

export const DEFAULT_COOKIE_VIEW_TOGGLE = {
  open: false,
  tools: DEFAULT_SETTING_VIEW_TOGGLE
}

export const QUOTA_EXCEEDED_ERR_CODE = 22
export const QUOTA_EXCEEDED_ERR_CODE_FF = 1014
export const QUOTA_EXCEEDED_ERR_NAME_FF = 'NS_ERROR_DOM_QUOTA_REACHED'

export const MAX_RECENTLY_USED_PARAMS = 10
export const MAX_NPC_PER_MAP = 7
export const MAX_GRINCH_PER_MAP = 1
export const MAX_SANTA_PER_MAP = 1
export const MESSAGE_MAX_GRINCH_PER_MAP = `Maximum ${MAX_GRINCH_PER_MAP} Grinch allowed per map`
export const MESSAGE_MAX_SANTA_PER_MAP = `Maximum ${MAX_SANTA_PER_MAP} Santa allowed per map`
export const MESSAGE_NPC_PARAMETER_SAME_CELL = 'NPC and parameter can\'t be placed in the same cell'

export const LOADING_DATA_SUCCESSFUL_MESSAGE = 'All data loaded'
export const LOAD_STATUS_SUCCESSFUL = { status: 'success', message: LOADING_DATA_SUCCESSFUL_MESSAGE }
