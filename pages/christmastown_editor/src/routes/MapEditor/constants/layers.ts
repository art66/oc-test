export const HIGHEST_LAYER = 'highest'
export const HIGH_LAYER = 'high'
export const LOW_LAYER = 'low'
export const LOWEST_LAYER = 'lowest'
export const LAYERS_NAMES_ARR = [HIGHEST_LAYER, HIGH_LAYER, LOW_LAYER, LOWEST_LAYER]
export const LIBRARY_IMAGES_URL = '/images/v2/christmas_town/library/'
export const LAYERS_MAP = {
  [LOWEST_LAYER]: 1,
  [LOW_LAYER]: 2,
  [HIGH_LAYER]: 3,
  [HIGHEST_LAYER]: 4
}
export const ITERATION_DURATION = 5
export const STEP = 1
