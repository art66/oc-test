export const DEFAULT = 'default'
export const SAVING = 'saving'
export const LOAD_BACKUP_CONFIRM_TYPE = 'LOAD_BACKUP'
export const REMOVE_BACKUP_CONFIRM_TYPE = 'REMOVE_BACKUP'
export const MIN_BACKUP_NAME_LENGTH = 4
export const MAX_BACKUP_NAME_LENGTH = 15

/* Messages */
export const EXISTED_NAME_ERROR = 'This name already exists please choose another one.'
export const EMPTY_NAME_ERROR = 'Please name your backup.'
export const LENGTH_NAME_ERROR = 'Backup name can not be less than 4 and more than 15 characters.'
