export const LINE_BLOCK = 'line'
export const CORNER_BLOCK = 'cornerBlock'
export const LINE_END_BLOCK = 'lineEndBlock'
export const ERASER = 'eraser'
export const BLOCK_ERASER = 'blockEraser'
