import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import cn from 'classnames'
import { SERVER_ERROR_MESSAGE } from '../../../constants'

interface IProps {
  mapData: {
    fullscreenMode: boolean
    mapName: string
    loading: boolean
    loadStatus: any
  }
  library: {
    currentTab: string
  }
  userAccessLevel: number
  toggleTab: (tab: string) => void
}

const MAX_ERROR_LENGTH = 100
const STATUS_FAIL = 'fail'

class MapTitle extends Component<IProps> {
  _getErrorIcon = () => {
    const { mapData } = this.props

    if (mapData.loadStatus.status === STATUS_FAIL && !mapData.loading) {
      return (
        <SVGIconGenerator
          customStyles={{ margin: '0 0 -2px 8px' }}
          iconName='CTIssue'
          preset={{ type: 'christmasTown', subtype: 'DANGER' }}
        />
      )
    }
  }

  render() {
    const { mapData } = this.props
    const statusMessage = mapData.loadStatus.message.length > MAX_ERROR_LENGTH ?
      SERVER_ERROR_MESSAGE :
      mapData.loadStatus.message
    const statusMessageStyles = cn({
      status: true,
      right: true,
      error: mapData.loadStatus.status === STATUS_FAIL
    })

    return (
      <div className='title-wrap clearfix'>
        <span className='text left'>{mapData.mapName}</span>
        <span className={statusMessageStyles}>
          {mapData.loading ? <i className='saving-icon' /> : (statusMessage)}
          {this._getErrorIcon()}
        </span>
      </div>
    )
  }
}

export default MapTitle
