import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { CELL_SIZE } from '../../../../constants'

export class CoordinatesY extends Component {
  constructor(props) {
    super(props)
    this.mapHeight = props.mapHeight
    this.state = {
      coordinatesList: []
    }
  }

  componentDidMount() {
    const coordinatesList = []
    const coordinatesQty = this.mapHeight / CELL_SIZE

    for (let y = coordinatesQty / 2; y >= coordinatesQty / -2; y--) {
      coordinatesList.push(<li key={'y' + y}>{y}</li>)
    }

    this.setState({
      coordinatesList
    })
  }

  render() {
    const { coordinatesList } = this.state
    const { dragY } = this.props
    const css = {
      width: CELL_SIZE,
      height: this.mapHeight,
      transform: `translateY(${dragY}px)`
    }

    return (
      <div className='coordinates-y-wrap'>
        <ul className='coordinates-y' style={css}>
          {coordinatesList}
        </ul>
      </div>
    )
  }
}

CoordinatesY.propTypes = {
  dragY: PropTypes.number.isRequired,
  mapHeight: PropTypes.number.isRequired
}

export default CoordinatesY
