import PropTypes from 'prop-types'
import React, { Component } from 'react'
import CoordinatesX from './CoordinatesX'
import CoordinatesY from './CoordinatesY'

export class Coordinates extends Component {
  render() {
    const { show, dragX, dragY, mapSize, fullscreenMode } = this.props

    return (
      <div className='coordinates'>
        {show && (
          <div>
            <CoordinatesX dragX={dragX} mapWidth={mapSize.mapWidth} fullscreenMode={fullscreenMode} />
            <CoordinatesY dragY={dragY} mapHeight={mapSize.mapHeight} />
          </div>
        )}
      </div>
    )
  }
}

Coordinates.propTypes = {
  dragY: PropTypes.number.isRequired,
  dragX: PropTypes.number.isRequired,
  show: PropTypes.bool.isRequired,
  mapSize: PropTypes.object,
  fullscreenMode: PropTypes.bool
}

export default Coordinates
