import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { CELL_SIZE, MAP_VIEWPORT_CELLS_STOCK } from '../../../../constants'

export class CoordinatesX extends Component {
  constructor(props) {
    super(props)
    this.mapWidth = props.mapWidth
    this.state = {
      coordinatesList: []
    }
  }

  componentDidMount() {
    const coordinatesList = []
    const coordinatesQtx = this.mapWidth / CELL_SIZE

    for (let x = 0; x < coordinatesQtx + MAP_VIEWPORT_CELLS_STOCK; x++) {
      coordinatesList.push(<li key={'x' + x}>{x}</li>)
    }

    this.setState({
      coordinatesList
    })
  }

  render() {
    const { fullscreenMode, dragX } = this.props
    const { coordinatesList } = this.state
    const css = {
      width: this.mapWidth + CELL_SIZE,
      transform: `translateX(${dragX}px)`
    }

    return (
      <ul className='coordinates-x' style={css}>
        {coordinatesList}
      </ul>
    )
  }
}

CoordinatesX.propTypes = {
  dragX: PropTypes.number.isRequired,
  mapWidth: PropTypes.number.isRequired,
  fullscreenMode: PropTypes.bool
}

export default CoordinatesX
