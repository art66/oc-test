import React, { Component } from 'react'
import cn from 'classnames'
import orderBy from 'lodash.orderby'
import ObjectsLayerItem from './ObjectsLayerItem'
import IMapObject from '../../interfaces/IMapObject'
import IPosition from '../../interfaces/IPosition'
import s from './styles.cssmodule.scss'

interface IProps {
  objects: IMapObject[]
  currentTab: string
  selectedObjects: IMapObject[]
  scale: number
  keysPressed: string[]
  selectObjectOnMap: (object: IMapObject) => void
  addSelectedObject: (object: IMapObject) => void
  unselectObject: (object: IMapObject) => void
  removeObjectOnMap: (object: IMapObject) => void
  changeObjectPosition: (object: IMapObject, position: IPosition) => void
}

export class Index extends Component<IProps> {
  hash: string

  shouldComponentUpdate(newProps: IProps) {
    const { currentTab, selectedObjects, scale, keysPressed } = this.props
    const hash = newProps.objects.reduce((objHash, item) => objHash + item.hash + item.position.x + item.position.y, '')
    const needUpdate =
      this.hash !== hash
      || newProps.currentTab !== currentTab
      || newProps.selectedObjects.length !== selectedObjects.length
      || newProps.selectedObjects[0] !== selectedObjects[0]
      || newProps.scale !== scale
      || newProps.keysPressed.length > 0
      || newProps.keysPressed.length !== keysPressed.length

    this.hash = hash

    return needUpdate
  }

  buildObjectsList = () => {
    const { objects } = this.props

    return orderBy(objects, ['order'], ['asc']).map(object => {
      const {
        selectObjectOnMap,
        addSelectedObject,
        unselectObject,
        selectedObjects,
        changeObjectPosition,
        currentTab
      } = this.props

      return (
        <ObjectsLayerItem
          key={object.hash}
          object={object}
          selectObjectOnMap={selectObjectOnMap}
          addSelectedObject={addSelectedObject}
          unselectObject={unselectObject}
          selectedObjects={selectedObjects}
          changeObjectPosition={changeObjectPosition}
          currentTab={currentTab}
        />
      )
    })
  }

  render() {
    const objectsList = this.buildObjectsList()
    const style = { transform: 'scale(1)' }

    return (
      <div className={cn('objects-layer', s.objectsLayer)} style={style}>
        {objectsList}
      </div>
    )
  }
}

export default Index
