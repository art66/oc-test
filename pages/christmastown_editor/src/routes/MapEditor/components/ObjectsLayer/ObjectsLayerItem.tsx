import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { getObjectImagePath, positionsEqual } from '../../../../utils/index'
import { CELL_SIZE, TAB_OBJECTS, TAB_LAYERS } from '../../../../constants/index'
import { showMapObjectContextMenu } from '../../modules/mapeditor'
import IMapObject from '../../interfaces/IMapObject'
import IPosition from '../../interfaces/IPosition'
import sAnim from '../../../../../../christmastown/src/routes/ChristmasTown/components/ObjectsLayer/animations.cssmodule.scss'

interface IProps {
  object: IMapObject,
  changeObjectPosition: (object: IMapObject, position: IPosition) => void,
  selectedObjects: IMapObject[],
  currentTab: string,
  selectObjectOnMap: (object: IMapObject) => void,
  addSelectedObject: (object: IMapObject) => void,
  unselectObject: (object: IMapObject) => void,
  showMapObjectContextMenu?: (object: IMapObject) => void,
  libraryObject?: IMapObject
}

class ObjectsLayerItem extends Component<IProps> {
  mouse: { clientX: null | number, clientY: null | number } = {
    clientX: null,
    clientY: null
  }

  handleDragStop = (_, eventObject) => {
    const { object, changeObjectPosition } = this.props
    const newPos = {
      x: Math.round(eventObject.x / CELL_SIZE),
      y: Math.round(eventObject.y / -CELL_SIZE)
    }

    if (!positionsEqual(object.position, newPos)) {
      changeObjectPosition(object, newPos)
    }
  }

  handleMouseDown = e => {
    const { clientX, clientY } = e

    this.mouse.clientX = clientX
    this.mouse.clientY = clientY
  }

  handleMouseUp = e => {
    const { clientX, clientY } = e
    const { object, selectedObjects, unselectObject, addSelectedObject, selectObjectOnMap } = this.props
    const targetObject = (clientX === this.mouse.clientX && clientY === this.mouse.clientY) ? object : null

    if (e.nativeEvent.which === 1 && targetObject) {
      if (selectedObjects.find(obj => obj.hash === targetObject.hash) && (e.ctrlKey || e.metaKey)) {
        unselectObject(targetObject)
      } else if (e.ctrlKey || e.metaKey) {
        addSelectedObject(targetObject)
      } else {
        selectObjectOnMap(targetObject)
      }
    }
  }

  handleContextMenu = e => {
    e.preventDefault()
    const { addSelectedObject, selectObjectOnMap, object, showMapObjectContextMenu } = this.props

    if (e.ctrlKey || e.metaKey) {
      addSelectedObject(object)
    } else {
      selectObjectOnMap(object)
    }

    showMapObjectContextMenu(object)
  }

  render() {
    const { object, selectedObjects, currentTab, libraryObject } = this.props
    const { position, onTopOfUser } = object
    const { width, height, animation } = libraryObject
    const isSelected = selectedObjects.some(obj => JSON.stringify(obj) === JSON.stringify(object))

    if (isSelected) {
      return <div />
    }

    const animationName = animation ? animation.name : ''
    const isSingle = selectedObjects.length === 1
    const className = cn(
      'ct-object',
      { selected: isSelected },
      { single: isSingle },
      { 'top-level': onTopOfUser },
      { [sAnim.animation]: animation },
      { [sAnim[animationName]]: animation }
    )
    const objectStyles = {
      width,
      height,
      pointerEvents: currentTab === TAB_OBJECTS || currentTab === TAB_LAYERS ? 'auto' : 'none',
      left: position.x * CELL_SIZE,
      top: position.y * -CELL_SIZE
    } as React.CSSProperties

    return (
      <div
        className={className}
        onContextMenu={this.handleContextMenu}
        style={objectStyles}
      >
        <img
          src={getObjectImagePath(libraryObject)}
          draggable={false}
          onMouseDown={e => this.handleMouseDown(e)}
          onMouseUp={e => this.handleMouseUp(e)}
          onContextMenu={e => e.preventDefault()}
          alt=''
        />
      </div>
    )
  }
}

export default connect(
  (state: any, ownProps: IProps) => ({
    contextMenuObject: state.mapeditor.mapData.contextMenuObject,
    libraryObject: state.mapeditor.library.objects[ownProps.object.object_id]
  }),
  {
    showMapObjectContextMenu
  }
)(ObjectsLayerItem)
