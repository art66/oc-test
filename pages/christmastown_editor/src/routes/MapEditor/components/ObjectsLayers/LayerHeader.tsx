import React, { Component } from 'react'
import cn from 'classnames'
import s from './LayerHeader.cssmodule.scss'

interface IProps {
  isUserLayer: boolean
  collapsed: boolean
  toggleLayer: () => void
  name: string
  unselectLayer?: (layerName: string) => void
}

class LayerHeader extends Component<IProps> {
  getExcludeLayerButton = () => {
    const { isUserLayer } = this.props

    return !isUserLayer ? (
      <button
        type='button'
        className={s.exclude}
        onClick={this._handleUnselectLayer}
      />
    ) : ''
  }

  _handleUnselectLayer = () => {
    const { unselectLayer, name } = this.props

    unselectLayer(name.toLowerCase())
  }

  render() {
    const { collapsed, toggleLayer, name } = this.props

    return (
      <div className={cn(s.layerNameWrapper)}>
        <div className={s.collapseSectionWrapper}>
          <button
            type='button'
            className={s.collapseSectionButton}
            onClick={toggleLayer}
          >
            <div className={cn(s.collapseSign, { [s.collapsed]: collapsed })} />
          </button>
        </div>
        <div className={s.layerName}>{name}</div>
        <div className={s.excludeLayerWrapper}>{this.getExcludeLayerButton()}</div>
      </div>
    )
  }
}

export default LayerHeader
