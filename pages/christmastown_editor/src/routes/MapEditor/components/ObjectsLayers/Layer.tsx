import React, { Component } from 'react'
import {
  HIGHEST_LAYER,
  HIGH_LAYER,
  LAYERS_NAMES_ARR,
  LOW_LAYER
} from '../../constants/layers'
import ILayer from '../../interfaces/layers/ILayer'
import ILayers from '../../interfaces/layers/ILayers'
import { capitalizeString } from '../../utils'
import UserLayer from './UserLayer'
import LayerHeader from './LayerHeader'
import ObjectsList from './ObjectsList'

interface IProps {
  name: string,
  index: number,
  mapData: {
    layers: ILayers,
    selectedObjects: ILayer[]
  },
  unselectObject: (layer: ILayer) => void,
  key: string,
  reorderObjects: (selectedObjects: ILayer[], timestampArray: number[]) => void,
  unselectLayer: (layerName: string) => void,
  changeObjectLayer: (object: ILayer, layerName: string) => void
}

interface IState {
  collapsed: boolean,
  scroll?: number
}

class Layer extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      collapsed: false
    }
  }

  getLayerNameWrapper = () => {
    const { mapData, name, unselectLayer } = this.props
    const { collapsed } = this.state
    const capitalizedName = capitalizeString(name)
    const amountObjectsInLayer = mapData.layers[name].length

    return amountObjectsInLayer ?
      (
        <LayerHeader
          collapsed={collapsed}
          name={capitalizedName}
          toggleLayer={this.toggleLayer}
          isUserLayer={false}
          unselectLayer={unselectLayer}
        />
      ) : ''
  }

  getObjectsList = () => {
    const { collapsed } = this.state
    const { mapData, name, unselectObject, changeObjectLayer, reorderObjects } = this.props
    const objectsInLayer = mapData.layers[name]

    return !collapsed ? (
      <ObjectsList
        layersArr={objectsInLayer}
        unselectObject={unselectObject}
        changeObjectLayer={changeObjectLayer}
        reorderObjects={reorderObjects}
      />
    ) : ''
  }

  toggleLayer = () => {
    const { collapsed } = this.state

    this.setState({
      collapsed: !collapsed
    })
  }

  render() {
    const { mapData, name, index } = this.props
    const placeUser = name === HIGH_LAYER && LAYERS_NAMES_ARR[index + 1] === LOW_LAYER
    const userIsHighestLayer = !mapData.layers[HIGH_LAYER].length && !mapData.layers[HIGHEST_LAYER].length

    return (
      <>
        <div className='layer-dropdown'>
          {this.getLayerNameWrapper()}
          {this.getObjectsList()}
        </div>
        {placeUser ? <UserLayer userIsHighestLayer={userIsHighestLayer} /> : ''}
      </>
    )
  }
}

export default Layer
