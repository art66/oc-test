import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layer from './Layer'
import { LAYERS_NAMES_ARR } from '../../constants/layers'
import { unselectLayer } from '../../modules/mapeditor'
import { changeObjectLayer } from '../../modules/layers'
import ILayer from '../../interfaces/layers/ILayer'
import ILayers from '../../interfaces/layers/ILayers'
import s from './style.cssmodule.scss'

interface IObjectsLayers {
  reorderObjects: (selectedObjects: any, timestampArray: any) => void,
  unselectObject: (layer: ILayer) => void,
  unselectLayer: (layerName: string) => void,
  changeObjectLayer: (object: ILayer, layerName: string) => void,
  mapData: {
    layers: ILayers,
    selectedObjects: ILayer[]
  }
}

export class ObjectsLayers extends Component<IObjectsLayers, { layers: any }> {
  getLayers = () => {
    const { unselectObject, mapData, reorderObjects, unselectLayer, changeObjectLayer } = this.props

    return LAYERS_NAMES_ARR.map((name, index) => {
      return (
        <Layer
          key={name}
          name={name}
          index={index}
          unselectObject={unselectObject}
          mapData={mapData}
          reorderObjects={reorderObjects}
          unselectLayer={unselectLayer}
          changeObjectLayer={changeObjectLayer}
        />
      )
    })
  }

  getLayersWrapper = () => {
    const { mapData } = this.props
    const objectsSelected = LAYERS_NAMES_ARR.some(name => mapData.layers[name].length)

    return !objectsSelected ? (
      <div className={s.placeholder}>No objects selected</div>
    ) : (
      <div className='scrollarea scroll-area scrollbar-thin'>
        <div className='scrollarea-content content'>
          {this.getLayers()}
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className='layers-container'>
        <div className={s.infoContainer}>
          <span className={s.title}>Groups</span>
        </div>
        {this.getLayersWrapper()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  mapData: state.mapeditor.mapData
})

const mapDispatchToProps = {
  unselectLayer,
  changeObjectLayer
}

export default connect(mapStateToProps, mapDispatchToProps)(ObjectsLayers)
