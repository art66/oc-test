import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { LAYERS_NAMES_ARR, LIBRARY_IMAGES_URL } from '../../constants/layers'
import { capitalizeString } from '../../utils'
import ILayer from '../../interfaces/layers/ILayer'
import ILibraryObject from '../../interfaces/ILibraryObject'
import s from './Layer.cssmodule.scss'

interface IProps {
  item: ILayer,
  unselectObject: (item: ILayer) => void,
  changeObjectLayer: (object: ILayer, layerName: string) => void,
  enabledHover: boolean,
  libraryObject?: ILibraryObject
}

interface IState {
  activeLayer: boolean
}

class LayerObject extends Component<IProps, IState> {
  objectEl: HTMLElement

  constructor(props: IProps) {
    super(props)
    this.state = {
      activeLayer: false
    }
  }

  shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    const { enabledHover } = this.props
    const { activeLayer } = this.state

    return nextProps.enabledHover !== enabledHover || nextState.activeLayer !== activeLayer
  }

  componentDidUpdate() {
    const { enabledHover } = this.props
    const { activeLayer } = this.state

    if (!enabledHover && activeLayer) {
      this.activateLayer(false)
    }
  }

  activateLayer = active => {
    this.setState({
      activeLayer: active
    })
  }

  getLayersButtons = () => {
    const { item } = this.props

    return LAYERS_NAMES_ARR.map(name => {
      const capitalizedName = capitalizeString(name)

      return (
        <div key={name} className={s.layerBtnWrapper}>
          <button
            type='button'
            className={cn(s.layerBtn, { [s.active]: item.layer.name === name })}
            onClick={() => this._handleChangeLayer(name)}
          >
            {capitalizedName}
          </button>
        </div>
      )
    })
  }

  _handleChangeLayer = layerName => {
    const { item, changeObjectLayer } = this.props

    if (item.layer.name === layerName) {
      return
    }

    changeObjectLayer(item, layerName)
  }

  _handleUnselectObject = () => {
    const { item, unselectObject } = this.props

    unselectObject(item)
  }

  _handleShowingOfButtons = () => {
    this.activateLayer(true)
  }

  _handleHidingOfButtons = () => {
    this.activateLayer(false)
  }

  render() {
    const { item, libraryObject } = this.props
    const { activeLayer } = this.state

    return (
      <li
        ref={el => this.objectEl = el}
        className={cn(s.layer, { [s.active]: activeLayer })}
        onMouseOver={this._handleShowingOfButtons}
        onFocus={this._handleShowingOfButtons}
        onMouseLeave={this._handleHidingOfButtons}
        data-id={item.hash}
      >
        <div className={`${s.dragIcon} dragIcon`}>
          <div className={s.icon} />
        </div>
        <div className={s.objectWrapper}>
          <img
            className={s.object}
            src={`${LIBRARY_IMAGES_URL + libraryObject.category}/${libraryObject.type}.png`}
            alt=''
          />
          <div className={cn(s.layerButtonsWrapper, { [s.show]: activeLayer })}>{this.getLayersButtons()}</div>
        </div>
        <div className={s.excludeLayer}>
          <button
            type='button'
            className={cn(s.icon, s.exclude)}
            onClick={this._handleUnselectObject}
          />
        </div>
      </li>
    )
  }
}

const ObjectElement = LayerObject

export default connect(
  (state: any, ownProps: IProps) => ({
    libraryObject: state.mapeditor.library.objects[ownProps.item.object_id]
  }),
  {}
)(ObjectElement)
