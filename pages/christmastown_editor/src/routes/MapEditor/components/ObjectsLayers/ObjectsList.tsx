import React, { Component } from 'react'
import Sortable from 'react-sortablejs'
import cn from 'classnames'
import { comparator } from '../../utils'
import ObjectElement from './ObjectElement'
import ILayer from '../../interfaces/layers/ILayer'
import s from './Layer.cssmodule.scss'

interface IProps {
  layersArr: ILayer[],
  unselectObject: (layer: ILayer) => void,
  reorderObjects: (selectedObjects: ILayer[], timestampArray: number[]) => void,
  changeObjectLayer: (object: ILayer, layerName: string) => void
}

interface IState {
  enabledHover: boolean
}

class ObjectsList extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      enabledHover: true
    }
  }

  getLayers = () => {
    const { layersArr, unselectObject, changeObjectLayer } = this.props
    const { enabledHover } = this.state

    return layersArr.map(item => (
      <ObjectElement
        key={item.hash}
        item={item}
        unselectObject={unselectObject}
        changeObjectLayer={changeObjectLayer}
        enabledHover={enabledHover}
      />
    ))
  }

  _handleSortEnd = order => {
    const { reorderObjects, layersArr } = this.props
    const newLayersArr = order.map(hash => {
      return layersArr.find(item => item.hash === hash)
    })
    const timestampArray = newLayersArr.slice().map(item => item.createdAt).sort(comparator)

    reorderObjects(newLayersArr, timestampArray)
  }

  toggleHover = enable => {
    this.setState({
      enabledHover: enable
    })
  }

  render() {
    const { layersArr } = this.props
    const { enabledHover } = this.state
    const options = {
      animation: 100,
      handle: '.dragIcon',
      onStart: () => {
        this.toggleHover(false)
      },
      onEnd: () => {
        this.toggleHover(true)
      }
    }
    const cnLayersList = cn({
      [s.layersList]: true,
      [s.removeHover]: !enabledHover
    })

    return layersArr.length ?
      (
        <div className={cnLayersList}>
          <Sortable
            tag='ul'
            onChange={this._handleSortEnd}
            options={options}
          >
            {this.getLayers()}
          </Sortable>
        </div>
      ) : ''
  }
}

export default ObjectsList
