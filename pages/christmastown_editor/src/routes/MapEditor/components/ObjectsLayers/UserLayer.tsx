import React, { Component } from 'react'
import cn from 'classnames'
import LayerHeader from './LayerHeader'
import s from './Layer.cssmodule.scss'

interface IState {
  collapsed: boolean
}

interface IProps {
  userIsHighestLayer: boolean
}

class UserLayer extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      collapsed: false
    }
  }

  getUser = () => {
    const { collapsed } = this.state

    return !collapsed ?
      (
        <div className={cn(s.layer, s.user)}>
          <div className={s.dragIcon} />
          <div className={s.objectWrapper}>
            <img
              className={s.object}
              src='images/v2/christmas_town/users/new/you.png'
              alt=''
            />
          </div>
          <div className={s.excludeLayer} />
        </div>
      ) : ''
  }

  toggleLayer = () => {
    const { collapsed } = this.state

    this.setState({
      collapsed: !collapsed
    })
  }

  render() {
    const { collapsed } = this.state
    const { userIsHighestLayer } = this.props

    return (
      <div className={cn(s.userLayerWrapper, { [s.collapsed]: collapsed, [s.noTopBorder]: userIsHighestLayer })}>
        <LayerHeader
          collapsed={collapsed}
          toggleLayer={this.toggleLayer}
          name='User'
          isUserLayer={true}
        />
        {this.getUser()}
      </div>
    )
  }
}

export default UserLayer
