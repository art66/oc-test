import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BACKUPS_TAB, TOOLS_BUTTONS } from '../../constants'
import Tab from './Tab'
import checkPermission from '../../../../utils/checkPermission'
import { ACCESS_LEVEL_2 } from '../../../../constants/accessLevels'
import s from './styles.cssmodule.scss'

interface IProps {
  currentTab: string
  setCurrentTab: (tab: string) => void
  user: {
    role: string
    accessLevel: number
    availableMapRoles: string[]
  }
}

class TabsPanel extends Component<IProps> {
  getButtons = () => {
    const { user } = this.props

    if (checkPermission(user.accessLevel, ACCESS_LEVEL_2, user.role, user.availableMapRoles)) {
      return TOOLS_BUTTONS
    }

    return TOOLS_BUTTONS.filter(item => item.libraryTab !== BACKUPS_TAB)
  }

  render() {
    const toolsButtons = this.getButtons()

    return (
      <div className={s.titleWrap}>
        {toolsButtons.map(item => <Tab key={item.libraryTab} item={item} />)}
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    user: state.mapeditor.user
  })
)(TabsPanel)
