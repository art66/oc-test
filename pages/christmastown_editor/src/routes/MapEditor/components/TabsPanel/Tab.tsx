import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { setCurrentTab } from '../../modules/mapeditor'
import { NPC_TAB } from '../../constants'
import s from './styles.cssmodule.scss'

interface IProps {
  currentTab: string
  item: {
    libraryTab: string
    iconName: string
    customPreset?: string
    label: string
  }
  setCurrentTab: (tab: string) => void
}

class Tab extends Component<IProps> {
  _handleChangeCurrentTab = () => {
    const { item } = this.props

    this.props.setCurrentTab(item.libraryTab)
  }

  render() {
    const { item, currentTab } = this.props
    const preset = {
      type: 'christmasTown',
      subtype: item.customPreset || 'DEFAULTS_BLUE'
    }

    return (
      <button
        key={item.libraryTab}
        id={item.libraryTab}
        className={`${s.tab} ${currentTab === item.libraryTab ? s.active : ''}`}
        type='button'
        onClick={this._handleChangeCurrentTab}
      >
        <div className={s.iconWrapper}>
          <SVGIconGenerator
            iconName={item.iconName}
            customClass={s.tabIcon}
            preset={preset}
          />
        </div>
        <Tooltip
          parent={item.libraryTab}
          arrow='center'
          position='top'
        >
          <span className='tipContent'>{item.label}</span>
        </Tooltip>
      </button>
    )
  }
}

export default connect(
  (state: any) => ({
    currentTab: state.mapeditor.library.currentTab
  }),
  {
    setCurrentTab
  }
)(Tab)
