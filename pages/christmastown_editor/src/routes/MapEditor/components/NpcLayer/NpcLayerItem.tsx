import React, { Component, CSSProperties } from 'react'
import cn from 'classnames'
import { CELL_SIZE, TAB_NPC } from '../../../../constants'
import INpc from '../../../../interfaces/INpc'
import IHighlightedCells from '../../interfaces/IHighlightedCells'
import NpcIcon from '../../../../components/NpcIcon'
import s from './styles.cssmodule.scss'

interface IProps {
  currentTab: string
  npc: INpc
  isSelected: boolean
  highlightedCells: IHighlightedCells
  selectNpc: (npc: INpc, showPointer: boolean) => void
  removeNpc: (npc: INpc) => void
  unselectNpcOnMap: () => void
}

export class NpcLayerItem extends Component<IProps> {
  _getCellStyles = (): CSSProperties => {
    const { npc, currentTab } = this.props

    return {
      left: npc.position.x * CELL_SIZE,
      top: npc.position.y * -CELL_SIZE,
      pointerEvents: currentTab === TAB_NPC ? 'auto' : 'none'
    }
  }

  _getCellClasses = () => {
    const { npc, isSelected, highlightedCells } = this.props
    const highlightAllowed = highlightedCells.type === 'npc' && highlightedCells.hash
    const fullHighlight = highlightAllowed && npc.hash === highlightedCells.hash
    const halfHighlight = highlightAllowed && npc.libraryId === highlightedCells.id && !fullHighlight

    return cn({
      [s.layerItem]: true,
      [s.selected]: isSelected,
      'half-highlight': halfHighlight,
      'full-highlight': fullHighlight
    })
  }

  _getRadiusStyles = (): CSSProperties => {
    const { npc } = this.props
    const diameter = CELL_SIZE + (2 * npc.radius * CELL_SIZE)

    return {
      width: diameter,
      height: diameter,
      backgroundColor: `#${npc.color}19`,
      border: `1px dashed #${npc.color}`,
      transform: `translate(${-npc.radius * CELL_SIZE}px, ${-npc.radius * CELL_SIZE}px)`
    }
  }

  _handleContextMenu = e => {
    const { unselectNpcOnMap } = this.props

    e.preventDefault()
    unselectNpcOnMap()
  }

  _renderNpcIconAndRadius = () => {
    const { npc, removeNpc } = this.props

    return (
      <>
        <i className={s.iconDelete} onClick={() => removeNpc(npc)} />
        <div className={s.radius} style={this._getRadiusStyles()} />
      </>
    )
  }

  render() {
    const { npc, isSelected, selectNpc } = this.props

    return (
      <div
        className={this._getCellClasses()}
        style={this._getCellStyles()}
        onClick={() => selectNpc(npc, false)}
        onContextMenu={this._handleContextMenu}
      >
        <NpcIcon npcType={npc.type.name} color={npc.color} isMapNpc={true} />
        {isSelected && this._renderNpcIconAndRadius()}
      </div>
    )
  }
}

export default NpcLayerItem
