import React, { Component } from 'react'
import { connect } from 'react-redux'
import NpcLayerItem from './NpcLayerItem'
import INpc from '../../../../interfaces/INpc'
import IHighlightedCells from '../../interfaces/IHighlightedCells'
import { removeNpc, selectNpc } from '../../modules/npc'
import { unSelectOnMap as unselectNpcOnMap } from '../../modules/mapeditor'
import s from './styles.cssmodule.scss'

interface IProps {
  currentTab: string
  mapNpc?: INpc[]
  selectedNpc: INpc
  highlightedCells: IHighlightedCells
  selectNpc: (npc: INpc, showPointer: boolean) => void
  removeNpc: (npc: INpc) => void
  unselectNpcOnMap: () => void
}

export class NpcLayer extends Component<IProps> {
  _hash: string

  constructor(props: IProps) {
    super(props)

    this._hash = ''
  }

  shouldComponentUpdate(newProps: IProps) {
    const { mapNpc, currentTab, selectedNpc, highlightedCells } = this.props

    return JSON.stringify(mapNpc) !== JSON.stringify(newProps.mapNpc)
      || newProps.currentTab !== currentTab
      || newProps.selectedNpc.hash !== selectedNpc.hash
      || newProps.highlightedCells.hash !== highlightedCells.hash
  }

  _renderNpcLayerItems = () => {
    const { currentTab, mapNpc, selectedNpc, highlightedCells, selectNpc, removeNpc, unselectNpcOnMap } = this.props

    return mapNpc.map(npc => (
      <NpcLayerItem
        key={npc.hash}
        currentTab={currentTab}
        npc={npc}
        isSelected={selectedNpc.hash === npc.hash}
        highlightedCells={highlightedCells}
        selectNpc={selectNpc}
        removeNpc={removeNpc}
        unselectNpcOnMap={unselectNpcOnMap}
      />
    ))
  }

  render() {
    return (
      <div className={s.layer}>
        {this._renderNpcLayerItems()}
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  currentTab: state.mapeditor.library.currentTab,
  mapNpc: state.mapeditor.mapData.npc,
  selectedNpc: state.mapeditor.mapData.selectedNpc,
  highlightedCells: state.mapeditor.mapData.highlightedCells
})

const mapActionsToProps = {
  selectNpc,
  removeNpc,
  unselectNpcOnMap
}

export default connect(mapStateToProps, mapActionsToProps)(NpcLayer)
