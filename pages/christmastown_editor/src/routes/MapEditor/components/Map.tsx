import React, { Component } from 'react'
import Draggable from 'react-draggable'
import { connect } from 'react-redux'
import cn from 'classnames'
import uniq from 'lodash.uniq'
import {
  removeObjectsOnMapAndSave,
  putObjectsIntoBuffer,
  putObjectsFromBufferOnMap,
  clearBuffer,
  unSelectOnMap
} from '../modules/mapeditor'
import ParametersLayer from './ParametersLayer'
import ObjectsLayer from './ObjectsLayer'
import AreasLayer from './AreasLayer'
import BlockingLayer from './BlockingLayer'
import NpcLayer from './NpcLayer'
import CellPointer from './CellPointer'
// eslint-disable-next-line import/no-cycle
import SelectedGroupRectangle from './SelectedGroupRectangle'
import { isInRectangle } from '../utils'

import IMapData from '../interfaces/IMapData'
import IMapObject from '../interfaces/IMapObject'
import IParameter from '../interfaces/IParameter'
import IArea from '../interfaces/IArea'
import IPosition from '../interfaces/IPosition'
import IPositionInPixels from '../interfaces/IPositionInPixels'
import IHighlightedCells from '../interfaces/IHighlightedCells'

import {
  CELL_SIZE,
  ADMIN_MAP_VIEWPORT_Y,
  ADMIN_MAP_VIEWPORT_X,
  TAB_AREAS,
  TAB_PARAMETERS,
  TAB_BLOCKING,
  TAB_HISTORY,
  TAB_OBJECTS,
  TAB_LAYERS,
  TAB_NPC
} from '../../../constants'

import * as blocks from '../constants/blocks'

import s from '../styles/map.cssmodule.scss'

const KEY_CODE_LEFT_BUTTON = 0
const KEY_CODE_C = 67
const KEY_CODE_X = 88
const KEY_CODE_V = 86
const KEY_CODE_DELETE = 46

interface IProps {
  mapData: IMapData
  currentTab: string
  cursorType: string
  dragX: number
  dragY: number
  selectedArea: IArea
  highlightedCells: IHighlightedCells
  addAreaOnMap(area?: IArea): void
  setPointerPos(position: IPositionInPixels): void
  addParameterToMap(): void
  selectParameterOnMap(parameter: IParameter): void
  unSelectOnMap(): void
  removeParameterOnMap(parameter?: IParameter): void
  handleDrag(event: Event, object: IPosition): void
  handleStartDrag(event: Event, object: IPosition): void
  handleStopDrag(event: Event, object: IPosition): void
  addObjectToMap(): void
  selectObjectOnMap(object: IMapObject): void
  selectObjectsOnMap(objects: IMapObject[]): void
  addSelectedObject(object: IMapObject): void
  unselectObject(object: IMapObject): void
  removeObjectOnMap(object?: IMapObject): void
  removeObjectsOnMapAndSave(objects: IMapObject[]): void
  putObjectsIntoBuffer(action: string, objects: IMapObject[]): void
  putObjectsFromBufferOnMap(position: IPosition): void
  clearBuffer(): void
  changeObjectPosition(object: IMapObject, position: IPosition): void
  getViewportPoints(dragX?: number, dragY?: number): void
  unselectOnMap(): void
}

interface IState {
  mouse: {
    pressed: boolean
    pageX: number
    pageY: number
    startPageX: number
    startPageY: number
  }
}

export class Map extends Component<IProps, IState> {
  world: HTMLDivElement
  keysPressed: string[]
  mapHeight: number
  mapWidth: number
  mapFocused: boolean

  constructor(props: IProps) {
    super(props)
    const { mapHeight, mapWidth } = props.mapData.mapSize

    this.mapHeight = mapHeight
    this.mapWidth = mapWidth
    this.mapFocused = false
    this.keysPressed = []
    this.state = {
      mouse: {
        pressed: false,
        pageX: 0,
        pageY: 0,
        startPageX: 0,
        startPageY: 0
      }
    }
  }

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown)
    window.addEventListener('keyup', this.handleKeyUp)
    window.addEventListener('mousedown', this.checkMapFocus)
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown)
    window.removeEventListener('keyup', this.handleKeyUp)
    window.removeEventListener('mousedown', this.checkMapFocus)
  }

  getMousePosition = (e: React.MouseEvent<Element>): IPositionInPixels => ({
    top: e.pageY - this.world.parentElement.offsetTop - this.props.dragY - CELL_SIZE / 2,
    left: e.pageX - this.world.parentElement.offsetLeft - this.props.dragX - CELL_SIZE / 2
  })

  getCellPositionByClick = (e: React.MouseEvent<Element>): IPosition => {
    const mousePosition: IPositionInPixels = this.getMousePosition(e)

    return {
      x: Math.round(mousePosition.left / CELL_SIZE),
      y: Math.round((mousePosition.top - this.mapHeight / 2) / -CELL_SIZE)
    }
  }

  getWorldRef = worldElement => (this.world = worldElement)

  checkMapFocus = e => {
    const target = e.target as HTMLElement

    this.mapFocused = !!target.closest(`#${this.world.id}`)
  }

  keyboardInteractionAllowed = () => {
    const { currentTab } = this.props

    return [TAB_LAYERS, TAB_OBJECTS, TAB_PARAMETERS].includes(currentTab) && this.mapFocused
  }

  handleMouseDown = e => {
    e.preventDefault()
    const mousePos = this.getMousePosition(e)

    this.setState({
      mouse: {
        pressed: e.button === KEY_CODE_LEFT_BUTTON,
        pageX: mousePos.left,
        pageY: mousePos.top,
        startPageX: mousePos.left,
        startPageY: mousePos.top
      }
    })
  }

  handleMouseUp = e => {
    e.preventDefault()
    this.setState({
      mouse: {
        pressed: false,
        pageX: 0,
        pageY: 0,
        startPageX: 0,
        startPageY: 0
      }
    })

    if (e.ctrlKey || e.metaKey) {
      const { pageX, pageY, startPageX, startPageY } = this.state.mouse
      const rectangle = {
        leftTop: { x: startPageX / CELL_SIZE, y: (startPageY - this.mapHeight / 2) / -CELL_SIZE },
        rightBottom: { x: pageX / CELL_SIZE, y: (pageY - this.mapHeight / 2) / -CELL_SIZE }
      }

      if (rectangle.leftTop.x !== rectangle.rightBottom.x && rectangle.leftTop.y !== rectangle.rightBottom.y) {
        const objects = this.props.mapData.objects.filter(obj => isInRectangle(obj.position, rectangle))

        this.props.selectObjectsOnMap(objects)
      }
    }
  }

  handleMouseMove = e => {
    e.preventDefault()
    const mousePos = this.getMousePosition(e)

    if (this.props.mapData.cellPointer.show) {
      let pointerPos
      const { blockingTool } = this.props.mapData.blocking

      if (blockingTool && blockingTool === blocks.BLOCK_ERASER) {
        pointerPos = {
          top: mousePos.top,
          left: mousePos.left
        }
      } else if (blockingTool && blockingTool !== blocks.LINE_BLOCK && blockingTool !== blocks.ERASER) {
        pointerPos = {
          top: Math.round((mousePos.top - CELL_SIZE / 2) / CELL_SIZE) * CELL_SIZE,
          left: Math.round((mousePos.left + CELL_SIZE / 2) / CELL_SIZE) * CELL_SIZE
        }
      } else {
        pointerPos = {
          top: Math.round(mousePos.top / CELL_SIZE) * CELL_SIZE,
          left: Math.round(mousePos.left / CELL_SIZE) * CELL_SIZE
        }
      }
      this.props.setPointerPos(pointerPos)
    } else if (this.state.mouse.pressed && [TAB_OBJECTS, TAB_LAYERS].includes(this.props.currentTab)) {
      this.setState({
        mouse: {
          // eslint-disable-next-line react/no-access-state-in-setstate
          ...this.state.mouse,
          pageX: mousePos.left,
          pageY: mousePos.top
        }
      })
    }
  }

  checkRectangularSelection = (): boolean => {
    const { pressed, pageX, pageY, startPageX, startPageY } = this.state.mouse

    return pressed && this.keysPressed.indexOf('Control') !== -1 && (pageX !== startPageX || pageY !== startPageY)
  }

  handleKeyDown = e => {
    const key = e.key === 'Meta' ? 'Control' : e.key

    this.keysPressed = uniq(this.keysPressed.concat([key]))
    this.forceUpdate()

    if (!this.keyboardInteractionAllowed()) return

    switch (e.keyCode) {
      case KEY_CODE_DELETE:
        this.removeOnMap()
        break
      case KEY_CODE_C:
      case KEY_CODE_X:
        if (e.ctrlKey || e.metaKey) {
          const action = e.keyCode === KEY_CODE_X ? 'cut' : 'copy'

          if (this.props.mapData.selectedObjects.length) {
            this.props.putObjectsIntoBuffer(action, this.props.mapData.selectedObjects)
          }
        }
        break
      case KEY_CODE_V:
        if (e.ctrlKey || e.metaKey) {
          const { x, y } = this.props.mapData.cellPointer

          if (this.props.mapData.selectedObjects.length) {
            this.props.putObjectsFromBufferOnMap({ x, y })
          }
        }
        break
      default:
        break
    }
  }

  handleKeyUp = e => {
    const key = e.key === 'Meta' ? 'Control' : e.key

    this.keysPressed = this.keysPressed.filter(item => item !== key)
  }

  handleUnselect = () => {
    if (this.keysPressed.indexOf('Control') === -1) {
      this.props.unSelectOnMap()
    }
  }

  handleRightMouseClick = e => {
    e.preventDefault()
    this.props.unSelectOnMap()
  }

  removeOnMap = () => {
    const { selectedObjects, selectedParameter } = this.props.mapData

    if (selectedObjects.length) {
      this.props.removeObjectsOnMapAndSave(selectedObjects)
    }

    if (selectedParameter && selectedParameter.hash) {
      this.props.removeParameterOnMap()
    }
  }

  renderObjectsLayer = () => {
    const {
      mapData,
      selectObjectOnMap,
      addSelectedObject,
      unselectObject,
      removeObjectOnMap,
      changeObjectPosition,
      currentTab
    } = this.props

    return (
      <ObjectsLayer
        objects={mapData.objects}
        selectObjectOnMap={selectObjectOnMap}
        addSelectedObject={addSelectedObject}
        unselectObject={unselectObject}
        selectedObjects={mapData.selectedObjects}
        removeObjectOnMap={removeObjectOnMap}
        changeObjectPosition={changeObjectPosition}
        currentTab={currentTab}
        scale={mapData.scale}
        keysPressed={this.keysPressed}
      />
    )
  }

  renderAreasLayer = () => {
    const { mapData, currentTab } = this.props
    const showAreas = [TAB_AREAS, TAB_HISTORY].includes(currentTab)

    return (showAreas || mapData.viewToggleLayers[TAB_AREAS]) && mapData.scale === 1 && <AreasLayer />
  }

  renderBlockingLayer = () => {
    const { mapData, currentTab } = this.props
    const showBlocking = [TAB_BLOCKING, TAB_HISTORY].includes(currentTab)

    return (showBlocking || mapData.viewToggleLayers[TAB_BLOCKING]) && mapData.scale === 1 && <BlockingLayer />
  }

  renderNpcLayer = () => {
    const { mapData, currentTab } = this.props
    const showNpc = [TAB_NPC, TAB_HISTORY].includes(currentTab)

    return (showNpc || mapData.viewToggleLayers[TAB_NPC]) && mapData.scale === 1 && <NpcLayer />
  }

  renderParametersLayer = () => {
    const { mapData, selectParameterOnMap, removeParameterOnMap, currentTab, highlightedCells } = this.props
    const showParameters = [TAB_PARAMETERS, TAB_HISTORY].includes(currentTab)

    return (
      (showParameters || mapData.viewToggleLayers[TAB_PARAMETERS])
      && mapData.scale === 1 && (
        <ParametersLayer
          parameters={mapData.parameters}
          selectParameterOnMap={selectParameterOnMap}
          selectedParameter={mapData.selectedParameter}
          removeParameterOnMap={removeParameterOnMap}
          currentTab={currentTab}
          unSelectOnMap={this.props.unSelectOnMap}
          highlightedCells={highlightedCells}
        />
      )
    )
  }

  renderSelectingMultipleObjects = () => {
    const { mouse } = this.state
    const selectingMultipleObjects = this.checkRectangularSelection()
    const stylesMultipleObjects = {
      left: mouse.startPageX,
      top: mouse.startPageY,
      width: mouse.pageX - mouse.startPageX,
      height: mouse.pageY - mouse.startPageY
    }

    return selectingMultipleObjects && <div className={s.rectangularSelection} style={stylesMultipleObjects} />
  }

  renderUnselectOnMap = () => {
    const { mapData } = this.props

    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
    return mapData.scale === 1 && <div className='substrate-layer' onClick={this.handleUnselect} />
  }

  render() {
    const { mapData, currentTab, cursorType, handleDrag, handleStartDrag, handleStopDrag } = this.props

    const { blockingTool } = this.props.mapData.blocking
    const styleWorld = mapData.fullscreenMode ?
      { width: window.innerWidth * CELL_SIZE, height: window.innerHeight * CELL_SIZE } :
      { width: this.mapWidth, height: this.mapHeight }
    const defaultPosition = {
      x: mapData.mapPosition.x,
      y: mapData.mapPosition.y
    }
    const bounds = {
      left: -this.mapWidth + ADMIN_MAP_VIEWPORT_X * CELL_SIZE,
      top: -this.mapHeight + ADMIN_MAP_VIEWPORT_Y * CELL_SIZE,
      right: 0,
      bottom: 0
    }
    const ctrlPressed = this.keysPressed.indexOf('Control') !== -1
    const blockEraserEnabled = !!(blockingTool && blockingTool === blocks.BLOCK_ERASER)

    return (
      <div className='map-overview' style={{ cursor: cursorType }} onContextMenu={e => e.preventDefault()}>
        <Draggable
          defaultPosition={defaultPosition}
          position={{ x: this.props.dragX, y: this.props.dragY }}
          bounds={bounds}
          onStart={handleStartDrag}
          onDrag={handleDrag}
          onStop={handleStopDrag}
          disabled={ctrlPressed || blockEraserEnabled}
          cancel={mapData.selectedObjects.length ? '.ct-object' : ''}
        >
          <div
            id='world'
            className={cn('world', 'tabParametersSelected', `tab-${currentTab}`)}
            ref={this.getWorldRef}
            style={styleWorld}
          >
            <div
              className='grid-layer'
              onMouseDown={this.handleMouseDown}
              onMouseUp={this.handleMouseUp}
              onMouseMove={this.handleMouseMove}
            >
              {this.renderSelectingMultipleObjects()}
              <CellPointer mapHeight={this.mapHeight} mapWidth={this.mapWidth} />
              {this.renderUnselectOnMap()}
              <div className='negative-coordinates' style={{ top: this.mapHeight / 2 }}>
                <SelectedGroupRectangle getCellPositionByClick={this.getCellPositionByClick} />
                {this.renderObjectsLayer()}
                {this.renderAreasLayer()}
                {this.renderBlockingLayer()}
                {this.renderNpcLayer()}
                {this.renderParametersLayer()}
              </div>
            </div>
          </div>
        </Draggable>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    highlightedCells: state.mapeditor.mapData.highlightedCells
  }),
  {
    removeObjectsOnMapAndSave,
    putObjectsIntoBuffer,
    putObjectsFromBufferOnMap,
    clearBuffer,
    unSelectOnMap
  }
)(Map)
