import React, { Component } from 'react'

export class GridLayer extends Component {
  render() {
    return <div className="grid-layer">GRID</div>
  }
}

GridLayer.propTypes = {
  // parameterCategories      : React.PropTypes.array.isRequired,
  // editParameter            : React.PropTypes.func,
  // confirmRemovingParameter : React.PropTypes.func
}

export default GridLayer
