import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getDirectionToPosition, getOppositeDirection } from '@torn/christmas-town/src/routes/ChristmasTown/utils'
import cn from 'classnames'
import ICell from '../../interfaces/ICell'
import ICellPointer from '../../interfaces/ICellPointer'
import IPosition from '../../interfaces/IPosition'
import { setCellsBlocking, removeBlocksLocally } from '../../modules/blocking'
import * as c from '../../../../constants'
import * as blocks from '../../constants/blocks'
import { positionsEqual } from '../../../../utils'
import { findNearCellByDirection } from '../../utils'
import replaceCells from '../../utils/replaceCells'
import s from './styles.cssmodule.scss'

const KEY_CODE_CTRL = 17

const emptyCell: ICell = {
  position: { x: 0, y: 0 },
  blocking: {}
}

interface IProps {
  mapWidth: number
  mapHeight: number
  blockingTool?: string
  cellPointer?: ICellPointer
  cells?: ICell[]
  getCellPointerStyles?(): { left: number; top: number }
  onContextMenu(event?: React.MouseEvent<HTMLDivElement>): void
  setCellsBlocking?(cells: ICell[], blockAdded: boolean): void
  removeBlocksLocally?(cells: ICell[]): void
}

interface IState {
  blockingSide?: string
  unblocking: boolean
  dragging: boolean
  dragEraseAllowed: boolean
}

class BlockingPointer extends Component<IProps, IState> {
  cellsToUpdate: ICell[]
  isBlocksRemoved: boolean
  constructor(props: IProps) {
    super(props)
    this.cellsToUpdate = []
    this.isBlocksRemoved = false
    this.state = {
      blockingSide: '',
      unblocking: false,
      dragging: false,
      dragEraseAllowed: false
    }
  }

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown)
    window.addEventListener('keyup', this.handleKeyUp)
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown)
    window.removeEventListener('keyup', this.handleKeyUp)
  }

  componentDidUpdate(prevProps: Readonly<IProps>): void {
    const { cellPointer } = this.props
    const { dragEraseAllowed } = this.state

    if (cellPointer.x !== prevProps.cellPointer.x || cellPointer.y !== prevProps.cellPointer.y && !dragEraseAllowed) {
      this.setState({ dragEraseAllowed: true })
    }
  }

  getCellPointerStyles = () => {
    const { getCellPointerStyles, blockingTool } = this.props
    const styles = getCellPointerStyles()

    if (blockingTool === blocks.LINE_END_BLOCK || blockingTool === blocks.CORNER_BLOCK) {
      styles.left -= 15
      styles.top += 15
    }

    return styles
  }

  getBlock = () => {
    const { blockingTool } = this.props
    const { blockingSide } = this.state

    if (blockingTool === blocks.LINE_BLOCK || blockingTool === blocks.ERASER || blockingTool === blocks.BLOCK_ERASER) {
      return blockingSide
    }

    if (blockingTool === blocks.CORNER_BLOCK) {
      return this.getAngle()
    }

    if (blockingTool === blocks.LINE_END_BLOCK) {
      return this.checkLineEndBlock() ? 'allowed' : 'disallowed'
    }
  }

  getCellByAbsolutePosition = (top, left) => {
    const x = Math.round(left / c.CELL_SIZE)
    const y = Math.round((top - this.props.mapHeight / 2) / -c.CELL_SIZE)

    return { x, y }
  }

  getCellsOnEraserCorners = () => {
    const { cellPointer } = this.props
    const { top, left, width } = cellPointer

    const halfLength = width / 2
    const leftTop = this.getCellByAbsolutePosition(top + 1 - halfLength, left - halfLength)
    const leftBottom = this.getCellByAbsolutePosition(top + halfLength, left - halfLength)
    const rightTop = this.getCellByAbsolutePosition(top - halfLength + 1, left + halfLength - 1)
    const rightBottom = this.getCellByAbsolutePosition(top + halfLength, left + halfLength - 1)

    return { leftTop, leftBottom, rightTop, rightBottom }
  }

  getAffectedByBlockEraserCells = (cornerCells) => {
    const { cells } = this.props
    const { leftTop, leftBottom, rightTop } = cornerCells

    return cells.filter(cell => cell.position.y <= leftTop.y + 1 && cell.position.y >= leftBottom.y - 1
        && cell.position.x <= rightTop.x + 1 && cell.position.x >= leftTop.x - 1)
  }

  getCellsIntersectionWithBlockEraserBySides = (cornerCells) => {
    const { cells } = this.props
    const { leftTop, leftBottom, rightTop } = cornerCells

    const intersectedCellsFromBottom = cells.filter(cell => cell.position.y === leftTop.y
      && cell.position.x <= rightTop.x && cell.position.x >= leftTop.x)

    const intersectedCellsFromTop = cells.filter(cell => cell.position.y === leftBottom.y
      && cell.position.x <= rightTop.x && cell.position.x >= leftTop.x)

    const intersectedCellsFromRight = cells.filter(cell => cell.position.x === leftTop.x
      && cell.position.y <= rightTop.y && cell.position.y >= leftBottom.y)

    const intersectedCellsFromLeft = cells.filter(cell => cell.position.x === rightTop.x
      && cell.position.y <= rightTop.y && cell.position.y >= leftBottom.y)

    return { intersectedCellsFromBottom, intersectedCellsFromTop, intersectedCellsFromRight, intersectedCellsFromLeft }
  }

  handleKeyDown = (e) => {
    if (e.keyCode === KEY_CODE_CTRL) {
      this.setState({ unblocking: true })
    }
  }

  handleKeyUp = (e) => {
    if (e.keyCode === KEY_CODE_CTRL) {
      this.setState({ unblocking: false })
    }
  }

  handleDragEraseStart = (e) => {
    const { blockingTool } = this.props

    if (e.nativeEvent.which !== 1 || blockingTool !== blocks.BLOCK_ERASER) {
      return
    }

    this.handleBlockEraserErase()
    this.setState({ dragging: true })
  }

  handleDragErase = () => {
    const { dragging, dragEraseAllowed } = this.state

    if (!dragging || !dragEraseAllowed) {
      return
    }

    this.handleBlockEraserErase()
  }

  handleBlockEraserErase = () => {
    const { removeBlocksLocally } = this.props
    const blockEraserCornerCells = this.getCellsOnEraserCorners()

    const {
      intersectedCellsFromBottom,
      intersectedCellsFromTop,
      intersectedCellsFromRight,
      intersectedCellsFromLeft
    } = this.getCellsIntersectionWithBlockEraserBySides(blockEraserCornerCells)

    const affectedAndRelatedCells = this.getAffectedByBlockEraserCells(blockEraserCornerCells)

    let affectedCellsAfterErase = []

    intersectedCellsFromBottom.forEach(cell => {
      affectedCellsAfterErase = this.removeBlockMutable(affectedAndRelatedCells, cell, c.BOTTOM)
    })
    intersectedCellsFromTop.forEach(cell => {
      affectedCellsAfterErase = this.removeBlockMutable(affectedAndRelatedCells, cell, c.TOP)
    })
    intersectedCellsFromRight.forEach(cell => {
      affectedCellsAfterErase = this.removeBlockMutable(affectedAndRelatedCells, cell, c.RIGHT)
    })
    intersectedCellsFromLeft.forEach(cell => {
      affectedCellsAfterErase = this.removeBlockMutable(affectedAndRelatedCells, cell, c.LEFT)
    })

    this.cellsToUpdate = replaceCells(this.cellsToUpdate, affectedCellsAfterErase)
    this.setState({ dragEraseAllowed: false })
    removeBlocksLocally(affectedCellsAfterErase)
  }

  handleDragEraseEnd = (e) => {
    const { blockingTool, setCellsBlocking } = this.props

    if (e.nativeEvent.which !== 1 && blockingTool !== blocks.BLOCK_ERASER) {
      return
    }

    if (this.isBlocksRemoved) {
      setCellsBlocking(this.cellsToUpdate, false)
    }

    this.setState({ dragging: false })
    this.cellsToUpdate = []
    this.isBlocksRemoved = false
  }

  handleMouseMove = e => {
    const { blockingTool } = this.props
    const { dragging, dragEraseAllowed } = this.state

    if (blockingTool === blocks.LINE_BLOCK) {
      const { offsetX, offsetY } = e.nativeEvent
      let direction = getDirectionToPosition({ x: 15, y: 15 }, { x: offsetX, y: offsetY }, false)

      direction = direction === 'top' || direction === 'bottom' ? getOppositeDirection(direction) : direction
      this.setState({ blockingSide: direction })
    } else if (blockingTool === blocks.BLOCK_ERASER && dragging || dragEraseAllowed) {
      this.handleDragErase()
    }
  }

  handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
    const { blockingTool, cellPointer } = this.props
    const isBlock = !e.ctrlKey

    if (blockingTool === blocks.LINE_BLOCK) {
      this.setLineBlock(isBlock)
    } else if (blockingTool === blocks.CORNER_BLOCK) {
      this.setCornerBlock(isBlock)
    } else if (blockingTool === blocks.LINE_END_BLOCK) {
      this.setLineEndBlock(isBlock)
    } else if (blockingTool === blocks.ERASER) {
      const { x, y } = cellPointer
      const position = { x, y }

      this.erase(position)
    }
  }

  removeBlocks = (cells: ICell[]) => {
    const { setCellsBlocking } = this.props
    const cellsToUpdate = cells.filter(cell => !cell.empty && Object.keys(cell.blocking).length)

    if (cellsToUpdate.length) {
      setCellsBlocking(cellsToUpdate, false)
    }
  }

  deepCopyObject = (obj: object) => {
    try {
      return JSON.parse(JSON.stringify(obj))
    } catch (e) {
      console.error(e)
    }
  }

  erase = (position: IPosition) => {
    const { cells } = this.props
    const cell = this.deepCopyObject(cells.find(c => positionsEqual(position, c.position)) || { ...emptyCell, position })
    const top = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.TOP))
    const rTop = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT_TOP))
    const right = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT))
    const rBottom = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT_BOTTOM))
    const bottom = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.BOTTOM))
    const lBottom = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT_BOTTOM))
    const left = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT))
    const lTop = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT_TOP))

    for (const direction in cell.blocking) {
      cell.blocking[direction] = false
    }

    top.blocking.bottom = false
    top.blocking.leftBottom = false
    top.blocking.rightBottom = false
    rTop.blocking.leftBottom = false
    right.blocking.leftTop = false
    right.blocking.left = false
    right.blocking.leftBottom = false
    rBottom.blocking.leftTop = false
    bottom.blocking.rightTop = false
    bottom.blocking.top = false
    bottom.blocking.leftTop = false
    lBottom.blocking.rightTop = false
    left.blocking.rightTop = false
    left.blocking.right = false
    left.blocking.rightBottom = false
    lTop.blocking.rightBottom = false

    this.removeBlocks([cell, top, rTop, right, rBottom, bottom, lBottom, left, lTop])
  }

  removeCellBlocking = (cell: ICell, direction: string) => {
    const { cells } = this.props
    let top,
      rTop,
      rBottom,
      bottom,
      lBottom,
      lTop,
      left,
      right

    if (direction === c.TOP || direction === c.BOTTOM) {
      top = direction === c.TOP ?
        this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.TOP)) :
        cell
      bottom = direction === c.TOP ?
        cell :
        this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.BOTTOM))
      rTop = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT_TOP))
      rBottom = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT_BOTTOM))
      lBottom = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT_BOTTOM))
      lTop = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT_TOP))

      top.blocking.bottom = false
      top.blocking.rightBottom = false
      top.blocking.leftBottom = false
      rTop.blocking.leftBottom = false
      rBottom.blocking.leftTop = false
      bottom.blocking.top = false
      bottom.blocking.rightTop = false
      bottom.blocking.leftTop = false
      lBottom.blocking.rightTop = false
      lTop.blocking.rightBottom = false

      this.removeBlocks([top, rTop, rBottom, bottom, lBottom, lTop])
    } else if (direction === c.LEFT || direction === c.RIGHT) {
      left = direction === c.LEFT ? this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT)) : cell
      right = direction === c.LEFT ? cell : this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT))
      lTop = direction === c.LEFT ?
        this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT_TOP)) :
        this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.TOP))
      rTop = direction === c.LEFT ?
        this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.TOP)) :
        this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT_TOP))
      rBottom = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.RIGHT_BOTTOM))
      lBottom = this.deepCopyObject(findNearCellByDirection(cells, cell.position, c.LEFT_BOTTOM))

      left.blocking.rightTop = false
      left.blocking.right = false
      left.blocking.rightBottom = false
      lTop.blocking.rightBottom = false
      rTop.blocking.leftBottom = false
      right.blocking.leftTop = false
      right.blocking.left = false
      right.blocking.leftBottom = false
      rBottom.blocking.leftTop = false
      lBottom.blocking.rightTop = false

      this.removeBlocks([left, lTop, rTop, right, rBottom, lBottom])
    }
  }

  removeBlockMutable = (cells: ICell[], cell: ICell, direction: string) => {
    let top,
      rTop,
      rBottom,
      bottom,
      lBottom,
      lTop,
      left,
      right

    if (direction === c.TOP || direction === c.BOTTOM) {
      top = direction === c.TOP ? findNearCellByDirection(cells, cell.position, c.TOP) : cell
      bottom = direction === c.TOP ? cell : findNearCellByDirection(cells, cell.position, c.BOTTOM)
      rTop = findNearCellByDirection(cells, cell.position, c.RIGHT_TOP)
      rBottom = findNearCellByDirection(cells, cell.position, c.RIGHT_BOTTOM)
      lBottom = findNearCellByDirection(cells, cell.position, c.LEFT_BOTTOM)
      lTop = findNearCellByDirection(cells, cell.position, c.LEFT_TOP)

      if (
        top.blocking.bottom || top.blocking.rightBottom || top.blocking.leftBottom
        || rTop.blocking.leftBottom || rBottom.blocking.leftTop || bottom.blocking.top || bottom.blocking.rightTop
        || bottom.blocking.leftTop || lBottom.blocking.rightTop || lTop.blocking.rightBottom
      ) {
        this.isBlocksRemoved = true
      }

      if (top.blocking.bottom) top.blocking.bottom = false
      if (top.blocking.rightBottom) top.blocking.rightBottom = false
      if (top.blocking.leftBottom) top.blocking.leftBottom = false
      if (rTop.blocking.leftBottom) rTop.blocking.leftBottom = false
      if (rBottom.blocking.leftTop) rBottom.blocking.leftTop = false
      if (bottom.blocking.top) bottom.blocking.top = false
      if (bottom.blocking.rightTop) bottom.blocking.rightTop = false
      if (bottom.blocking.leftTop) bottom.blocking.leftTop = false
      if (lBottom.blocking.rightTop) lBottom.blocking.rightTop = false
      if (lTop.blocking.rightBottom) lTop.blocking.rightBottom = false

      return replaceCells(cells, [top, rTop, rBottom, bottom, lBottom, lTop])
    }

    if (direction === c.LEFT || direction === c.RIGHT) {
      left = direction === c.LEFT ? findNearCellByDirection(cells, cell.position, c.LEFT) : cell
      right = direction === c.LEFT ? cell : findNearCellByDirection(cells, cell.position, c.RIGHT)
      lTop = direction === c.LEFT ?
        findNearCellByDirection(cells, cell.position, c.LEFT_TOP) :
        findNearCellByDirection(cells, cell.position, c.TOP)
      rTop = direction === c.LEFT ?
        findNearCellByDirection(cells, cell.position, c.TOP) :
        findNearCellByDirection(cells, cell.position, c.RIGHT_TOP)
      rBottom = findNearCellByDirection(cells, cell.position, c.RIGHT_BOTTOM)
      lBottom = findNearCellByDirection(cells, cell.position, c.LEFT_BOTTOM)

      if (
        left.blocking.rightTop || left.blocking.right || left.blocking.rightBottom
        || lTop.blocking.rightBottom || rTop.blocking.leftBottom || right.blocking.leftTop || right.blocking.left
        || right.blocking.leftBottom || rBottom.blocking.leftTop || lBottom.blocking.rightTop
      ) {
        this.isBlocksRemoved = true
      }

      if (left.blocking.rightTop) left.blocking.rightTop = false
      if (left.blocking.right) left.blocking.right = false
      if (left.blocking.rightBottom) left.blocking.rightBottom = false
      if (lTop.blocking.rightBottom) lTop.blocking.rightBottom = false
      if (rTop.blocking.leftBottom) rTop.blocking.leftBottom = false
      if (right.blocking.leftTop) right.blocking.leftTop = false
      if (right.blocking.left) right.blocking.left = false
      if (right.blocking.leftBottom) right.blocking.leftBottom = false
      if (rBottom.blocking.leftTop) rBottom.blocking.leftTop = false
      if (lBottom.blocking.rightTop) lBottom.blocking.rightTop = false

      return replaceCells(cells, [left, lTop, rTop, right, rBottom, lBottom])
    }
  }

  setLineBlock = (block = true) => {
    const { setCellsBlocking, cellPointer } = this.props
    const { x, y } = cellPointer
    const direction = this.state.blockingSide
    const oppositeDirection = getOppositeDirection(direction)
    const cell: ICell = { position: { x, y }, blocking: { [direction]: block } }
    const nearCellPosition: IPosition = Object.assign({}, cell.position)

    switch (direction) {
      case c.LEFT:
        nearCellPosition.x -= 1
        break
      case c.RIGHT:
        nearCellPosition.x += 1
        break
      case c.TOP:
        nearCellPosition.y += 1
        break
      case c.BOTTOM:
        nearCellPosition.y -= 1
        break
      default:
        break
    }

    const nearCell: ICell = { position: nearCellPosition, blocking: { [oppositeDirection]: block } }

    if (block) {
      setCellsBlocking([cell, nearCell], block)
    } else {
      this.removeCellBlocking(cell, direction)
    }
  }

  setCornerBlock = (block: boolean) => {
    const { setCellsBlocking } = this.props
    const angle = this.getAngle()

    if (!angle) {
      return
    }

    if (angle === c.LEFT_BOTTOM) {
      let lTop = this.getCellByAngle(c.LEFT_TOP)
      let rBottom = this.getCellByAngle(c.RIGHT_BOTTOM)

      lTop = {
        ...lTop,
        blocking: {
          ...lTop.blocking,
          rightBottom: block
        }
      }
      rBottom = {
        ...rBottom,
        blocking: {
          ...rBottom.blocking,
          leftTop: block
        }
      }

      setCellsBlocking([lTop, rBottom], block)
    } else if (angle === c.LEFT_TOP) {
      let lBottom = this.getCellByAngle(c.LEFT_BOTTOM)
      let rTop = this.getCellByAngle(c.RIGHT_TOP)

      lBottom = {
        ...lBottom,
        blocking: {
          ...lBottom.blocking,
          rightTop: block
        }
      }
      rTop = {
        ...rTop,
        blocking: {
          ...rTop.blocking,
          leftBottom: block
        }
      }

      setCellsBlocking([lBottom, rTop], block)
    } else if (angle === c.RIGHT_TOP) {
      let lTop = this.getCellByAngle(c.LEFT_TOP)
      let rBottom = this.getCellByAngle(c.RIGHT_BOTTOM)

      lTop = {
        ...lTop,
        blocking: {
          ...lTop.blocking,
          rightBottom: block
        }
      }
      rBottom = {
        ...rBottom,
        blocking: {
          ...rBottom.blocking,
          leftTop: block
        }
      }

      setCellsBlocking([lTop, rBottom], block)
    } else if (angle === c.RIGHT_BOTTOM) {
      let lBottom = { ...this.getCellByAngle(c.LEFT_BOTTOM) }
      let rTop = { ...this.getCellByAngle(c.RIGHT_TOP) }

      lBottom = {
        ...lBottom,
        blocking: {
          ...lBottom.blocking,
          rightTop: block
        }
      }
      rTop = {
        ...rTop,
        blocking: {
          ...rTop.blocking,
          leftBottom: block
        }
      }

      setCellsBlocking([lBottom, rTop], block)
    }
  }

  setLineEndBlock = (block = true) => {
    const { setCellsBlocking } = this.props
    const blockAllowed = this.checkLineEndBlock()

    if (!blockAllowed) {
      return
    }

    let lTop = this.getCellByAngle(c.LEFT_TOP)
    let rTop = this.getCellByAngle(c.RIGHT_TOP)
    let lBottom = this.getCellByAngle(c.LEFT_BOTTOM)
    let rBottom = this.getCellByAngle(c.RIGHT_BOTTOM)

    lTop = {
      ...lTop,
      blocking: {
        ...lTop.blocking,
        rightBottom: block
      }
    }
    rTop = {
      ...rTop,
      blocking: {
        ...rTop.blocking,
        leftBottom: block
      }
    }
    lBottom = {
      ...lBottom,
      blocking: {
        ...lBottom.blocking,
        rightTop: block
      }
    }
    rBottom = {
      ...rBottom,
      blocking: {
        ...rBottom.blocking,
        leftTop: block
      }
    }

    setCellsBlocking([lTop, rTop, lBottom, rBottom], block)
  }

  getCellByAngle = (angle: string): ICell => {
    const { cellPointer, cells } = this.props
    let cell: ICell
    const position: IPosition = { x: cellPointer.x, y: cellPointer.y }

    switch (angle) {
      case c.LEFT_TOP:
        position.x -= 1
        break
      case c.RIGHT_TOP:
        break
      case c.LEFT_BOTTOM:
        position.x -= 1
        position.y -= 1
        break
      case c.RIGHT_BOTTOM:
        position.y -= 1
        break
      default:
        break
    }

    cell = cells.find(c => positionsEqual(position, c.position))

    return (
      cell || {
        ...emptyCell,
        position
      }
    )
  }

  getAngle = (): string | null => {
    const lTop = this.getCellByAngle(c.LEFT_TOP)
    const rTop = this.getCellByAngle(c.RIGHT_TOP)
    const lBottom = this.getCellByAngle(c.LEFT_BOTTOM)
    const rBottom = this.getCellByAngle(c.RIGHT_BOTTOM)
    let angle = null

    if (lTop.blocking.right && lTop.blocking.bottom && !rTop.blocking.bottom && !lBottom.blocking.right) {
      angle = c.RIGHT_BOTTOM
    } else if (rTop.blocking.left && rTop.blocking.bottom && !rBottom.blocking.left && !lBottom.blocking.top) {
      angle = c.LEFT_BOTTOM
    } else if (rBottom.blocking.left && rBottom.blocking.top && !rTop.blocking.left && !lBottom.blocking.top) {
      angle = c.LEFT_TOP
    } else if (lBottom.blocking.top && lBottom.blocking.right && !lTop.blocking.right && !rBottom.blocking.top) {
      angle = c.RIGHT_TOP
    }

    return angle
  }

  checkLineEndBlock = (): boolean => {
    const lTop = this.getCellByAngle(c.LEFT_TOP)
    const rTop = this.getCellByAngle(c.RIGHT_TOP)
    const lBottom = this.getCellByAngle(c.LEFT_BOTTOM)
    const rBottom = this.getCellByAngle(c.RIGHT_BOTTOM)

    return (
      (lBottom.blocking.right && !lBottom.blocking.top && !rBottom.blocking.top && !lTop.blocking.right)
      || (lTop.blocking.right && !lTop.blocking.bottom && !rTop.blocking.bottom && !lBottom.blocking.right)
      || (rTop.blocking.bottom && !rTop.blocking.left && !rBottom.blocking.left && !lTop.blocking.bottom)
      || (lTop.blocking.bottom && !lTop.blocking.right && !lBottom.blocking.right && !rTop.blocking.bottom)
    )
  }

  render() {
    const { blockingTool, onContextMenu } = this.props

    if (!blockingTool) {
      return <div />
    }

    const styles = this.getCellPointerStyles()
    const block = this.getBlock()

    return (
      <div
        className={cn(s.cellPointer, s[block], s[blockingTool], { [s.unblocking]: this.state.unblocking })}
        style={styles}
        onClick={this.handleClick}
        onMouseMove={this.handleMouseMove}
        onContextMenu={onContextMenu}
        onMouseDown={this.handleDragEraseStart}
        onMouseUp={this.handleDragEraseEnd}
      />
    )
  }
}

export default connect(
  (state: any) => ({
    blockingTool: state.mapeditor.mapData.blocking.blockingTool,
    cellPointer: state.mapeditor.mapData.cellPointer,
    cells: state.mapeditor.mapData.blocking.cells
  }),
  {
    setCellsBlocking,
    removeBlocksLocally
  }
)(BlockingPointer)
