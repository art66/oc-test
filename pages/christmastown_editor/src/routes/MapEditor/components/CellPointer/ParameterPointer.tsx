import React, { Component } from 'react'
import { connect } from 'react-redux'
import IParameter from '../../interfaces/IParameter'
import { addParameterToMap } from '../../modules/mapeditor'
import { getColor } from '../../../../utils'
import ICellPointer from '../../interfaces/ICellPointer'

interface IProps {
  selectedParameter: IParameter,
  cellPointer: ICellPointer,
  addParameterToMap: () => void,
  onRightClick: (e: React.MouseEvent<Element>) => void,
  getCellPointerStyles: () => {left: number, top: number, width: number, height: number}
}

class ParameterPointer extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps) {
    const { selectedParameter, cellPointer } = this.props
    const parameterChanged = selectedParameter.parameter_id !== nextProps.selectedParameter.parameter_id
    const { show, x, y } = cellPointer
    const pointerVisibilityChanged = show !== nextProps.cellPointer.show
    const pointerPositionChanged = x !== nextProps.cellPointer.x || y !== nextProps.cellPointer.y

    return parameterChanged || pointerVisibilityChanged || pointerPositionChanged
  }

  render() {
    const { selectedParameter, addParameterToMap, getCellPointerStyles, onRightClick } = this.props

    if (!selectedParameter.parameter_id) {
      return <div />
    }

    return (
      <span
        className='cell-pointer'
        style={getCellPointerStyles()}
        onClick={addParameterToMap}
        onContextMenu={onRightClick}
      >
        <span
          className='parameter-cell-pointer'
          style={{ color: getColor(selectedParameter.color) }}
        >
          {selectedParameter.code}
        </span>
      </span>
    )
  }
}

export default connect(
  (state: any) => ({
    selectedParameter: state.mapeditor.mapData.selectedParameter,
    cellPointer: state.mapeditor.mapData.cellPointer
  }),
  {
    addParameterToMap
  }
)(ParameterPointer)
