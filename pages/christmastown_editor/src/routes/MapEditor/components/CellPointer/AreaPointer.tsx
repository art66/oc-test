import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addAreaOnMap } from '../../modules/mapeditor'
import ICellPointer from '../../interfaces/ICellPointer'
import IArea from '../../interfaces/IArea'

interface IProps {
  cellPointer: ICellPointer
  getCellPointerStyles: () => { left: number; top: number; width: number; height: number }
  addAreaOnMap: () => void
  selectedArea?: IArea
}

class AreaPointer extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps) {
    const { selectedArea, cellPointer } = this.props
    const areaChanged = selectedArea._id !== nextProps.selectedArea._id
    const { show, x, y } = cellPointer
    const pointerVisibilityChanged = show !== nextProps.cellPointer.show
    const pointerPositionChanged = x !== nextProps.cellPointer.x || y !== nextProps.cellPointer.y

    return areaChanged || pointerVisibilityChanged || pointerPositionChanged
  }

  _handleAddArea = () => this.props.addAreaOnMap()

  render() {
    const { getCellPointerStyles, selectedArea } = this.props

    if (!selectedArea.name) {
      return <div />
    }

    return (
      // eslint-disable-next-line jsx-a11y/click-events-have-key-events
      <span className='cell-pointer' style={getCellPointerStyles()} onClick={this._handleAddArea} />
    )
  }
}

export default connect(
  (state: any) => ({
    cellPointer: state.mapeditor.mapData.cellPointer,
    selectedArea: state.mapeditor.library.areas.selectedArea
  }),
  {
    addAreaOnMap
  }
)(AreaPointer)
