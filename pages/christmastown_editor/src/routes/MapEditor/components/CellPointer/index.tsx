import React, { Component } from 'react'
import { connect } from 'react-redux'
import ICellPointer from '../../interfaces/ICellPointer'
import { clearBuffer, unSelectOnMap } from '../../modules/mapeditor'
import { TAB_AREAS, TAB_OBJECTS, TAB_PARAMETERS, TAB_BLOCKING, TAB_NPC } from '../../../../constants'
import BlockingPointer from './BlockingPointer'
import ObjectsPointer from './ObjectsPointer'
import ParameterPointer from './ParameterPointer'
import NpcPointer from './NpcPointer'
import AreaPointer from './AreaPointer'

interface IProps {
  mapWidth: number,
  mapHeight: number,
  cellPointer: ICellPointer,
  currentTab: string,
  unSelectOnMap: () => void,
  clearBuffer: () => void,
}

class CellPointer extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps) {
    const { currentTab, mapHeight, mapWidth } = this.props
    const tabChanged = currentTab !== nextProps.currentTab
    const mapHeightChanged = mapHeight !== nextProps.mapHeight
    const mapWidthChanged = mapWidth !== nextProps.mapWidth

    return tabChanged || mapHeightChanged || mapWidthChanged
  }

  getCellPointerStyles = () => {
    const { cellPointer } = this.props
    const { left, top, width, height } = cellPointer

    return { left, top, width, height }
  }

  getCellPointer = () => {
    const { currentTab, mapHeight, mapWidth } = this.props
    let result = <div />

    switch (currentTab) {
      case TAB_OBJECTS:
        result = (
          <ObjectsPointer
            onRightClick={this._handleRightClick}
            getCellPointerStyles={this.getCellPointerStyles}
          />
        )
        break
      case TAB_PARAMETERS:
        result = (
          <ParameterPointer
            getCellPointerStyles={this.getCellPointerStyles}
            onRightClick={this._handleRightClick}
          />
        )
        break
      case TAB_NPC:
        result = (
          <NpcPointer
            getCellPointerStyles={this.getCellPointerStyles}
            onRightClick={this._handleRightClick}
          />
        )
        break
      case TAB_AREAS:
        result = (
          <AreaPointer
            getCellPointerStyles={this.getCellPointerStyles}
          />
        )
        break
      case TAB_BLOCKING:
        result = (
          <BlockingPointer
            mapWidth={mapWidth}
            mapHeight={mapHeight}
            onContextMenu={this._handleRightClick}
            getCellPointerStyles={this.getCellPointerStyles}
          />
        )
        break
      default:
        result = <div />
        break
    }

    return result
  }

  _handleRightClick = e => {
    e.preventDefault()
    const { unSelectOnMap, clearBuffer } = this.props

    unSelectOnMap()
    clearBuffer()
  }

  render() {
    return this.getCellPointer()
  }
}

export default connect(
  (state: any) => ({
    currentTab: state.mapeditor.library.currentTab,
    cellPointer: state.mapeditor.mapData.cellPointer
  }),
  {
    clearBuffer,
    unSelectOnMap
  }
)(CellPointer)
