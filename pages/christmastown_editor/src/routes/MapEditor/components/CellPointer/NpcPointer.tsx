import React, { Component } from 'react'
import { connect } from 'react-redux'
import INpc from '../../../../interfaces/INpc'
import IPosition from '../../interfaces/IPosition'
import ICellPointer from '../../interfaces/ICellPointer'
import { addNpc } from '../../modules/npc'
import NpcIcon from '../../../../components/NpcIcon'

interface IProps {
  selectedNpc?: INpc,
  cellPointer: ICellPointer,
  onRightClick: (e: React.MouseEvent<Element>) => void,
  getCellPointerStyles: () => { left: number, top: number, width: number, height: number },
  addNpc: (selectedNpc: INpc, position: IPosition) => void
}

class ParameterPointer extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps) {
    const { selectedNpc, cellPointer } = this.props
    const npcChanged = selectedNpc.libraryId !== nextProps.selectedNpc.libraryId
    const { show, x, y } = cellPointer
    const pointerVisibilityChanged = show !== nextProps.cellPointer.show
    const pointerPositionChanged = x !== nextProps.cellPointer.x || y !== nextProps.cellPointer.y

    return npcChanged || pointerVisibilityChanged || pointerPositionChanged
  }

  render() {
    const { selectedNpc, addNpc, cellPointer, getCellPointerStyles, onRightClick } = this.props
    const position = { x: cellPointer.x, y: cellPointer.y }

    if (!selectedNpc.libraryId) {
      return <div />
    }

    return (
      <span
        className='cell-pointer'
        style={getCellPointerStyles()}
        onClick={() => addNpc(selectedNpc, position)}
        onContextMenu={onRightClick}
      >
        <NpcIcon npcType={selectedNpc.type.name} color={selectedNpc.color} isMapNpc={true} />
      </span>
    )
  }
}

export default connect(
  (state: any) => ({
    selectedNpc: state.mapeditor.mapData.selectedNpc,
    cellPointer: state.mapeditor.mapData.cellPointer
  }),
  {
    addNpc
  }
)(ParameterPointer)
