import React, { Component } from 'react'
import { connect } from 'react-redux'
import orderBy from 'lodash.orderby'
import { getObjectImagePath } from '../../../../utils'
import { findSideObjectInGroup } from '../../utils'
import { CELL_SIZE } from '../../../../constants'
import IBuffer from '../../interfaces/IBuffer'
import IMapObject from '../../interfaces/IMapObject'
import { putObjectsFromBufferOnMap, addObjectToMap } from '../../modules/mapeditor'
import IPosition from '../../interfaces/IPosition'
import ICellPointer from '../../interfaces/ICellPointer'

interface IProps {
  buffer?: IBuffer,
  selectedObjects?: IMapObject[],
  libraryObjects: Object,
  cellPointer: ICellPointer,
  getCellPointerStyles: () => {left: number, top: number, width: number, height: number},
  putObjectsFromBufferOnMap: (position: IPosition) => void,
  addObjectToMap: () => void,
  onRightClick: (e: React.MouseEvent<Element>) => void
}

class ObjectsPointer extends Component<IProps> {
  renderObjects = (leftTopPosition) => {
    const { buffer, selectedObjects, libraryObjects } = this.props
    const objects = buffer.objects.length ? buffer.objects : selectedObjects
    const sortedObjects = orderBy(objects, ['order'], ['asc'])

    return sortedObjects.map(obj => {
      const styles = {
        left: (obj.position.x - leftTopPosition.x) * CELL_SIZE,
        top: (leftTopPosition.y - obj.position.y) * CELL_SIZE,
        position: 'absolute',
        display: 'block'
      } as React.CSSProperties

      return (
        <img
          key={`cellPointerObject-${obj.hash}`}
          src={getObjectImagePath(libraryObjects[obj.object_id])}
          style={styles}
          alt=''
        />
      )
    })
  }

  getObjectsParams = () => {
    const {
      selectedObjects,
      libraryObjects,
      buffer,
      cellPointer,
      putObjectsFromBufferOnMap,
      addObjectToMap
    } = this.props
    const objects = buffer.objects.length ? buffer.objects : selectedObjects
    const fullInfoObjects = objects.map(obj => ({ ...libraryObjects[obj.object_id], ...obj }))

    if (buffer.objects.length) {
      const leftObject = findSideObjectInGroup(fullInfoObjects, 'left')
      const rightObject = findSideObjectInGroup(fullInfoObjects, 'right')
      const topObject = findSideObjectInGroup(fullInfoObjects, 'top')
      const bottomObject = findSideObjectInGroup(fullInfoObjects, 'bottom')
      const xDiffBetweenObjects = rightObject.position.x - leftObject.position.x
      const yDiffBetweenObjects = topObject.position.y - bottomObject.position.y

      return {
        width: xDiffBetweenObjects * CELL_SIZE + rightObject.width,
        height: yDiffBetweenObjects * CELL_SIZE + bottomObject.height,
        leftTopPosition: { x: leftObject.position.x, y: topObject.position.y },
        onClick: () => putObjectsFromBufferOnMap({ x: cellPointer.x, y: cellPointer.y })
      }
    }

    if (!selectedObjects[0].hash) {
      const selectedObjectLibraryID = selectedObjects[0].object_id

      return {
        width: libraryObjects[selectedObjectLibraryID].width,
        height: libraryObjects[selectedObjectLibraryID].height,
        leftTopPosition: selectedObjects[0].position.x !== undefined ? selectedObjects[0].position : { x: 0, y: 0 },
        onClick: addObjectToMap
      }
    }
  }

  render() {
    const {
      getCellPointerStyles,
      buffer,
      selectedObjects,
      onRightClick
    } = this.props

    if (buffer.objects.length === 0 && selectedObjects.length === 0 ||
      !buffer.objects.length && selectedObjects[0].hash) {
      return <div />
    }

    const params = this.getObjectsParams()
    const { width, height, leftTopPosition, onClick } = params

    const styles = {
      ...getCellPointerStyles(),
      width,
      height
    }

    return (
      <span
        className='cell-pointer'
        style={styles}
        onClick={onClick}
        onContextMenu={onRightClick}
      >
        {this.renderObjects(leftTopPosition)}
      </span>
    )
  }
}

export default connect(
  (state: any) => ({
    selectedObjects: state.mapeditor.mapData.selectedObjects,
    buffer: state.mapeditor.mapData.buffer,
    libraryObjects: state.mapeditor.library.objects,
    cellPointer: state.mapeditor.mapData.cellPointer
  }),
  {
    putObjectsFromBufferOnMap,
    addObjectToMap
  }
)(ObjectsPointer)
