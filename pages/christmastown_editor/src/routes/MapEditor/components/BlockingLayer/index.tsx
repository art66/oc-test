import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { getDirectionToPosition } from '@torn/christmas-town/src/routes/ChristmasTown/utils'
import ICell from '../../interfaces/ICell'
import ICellPointer from '../../interfaces/ICellPointer'
import { positionsEqual } from '../../../../utils'
import { findNearCellByDirection } from '../../utils'
import * as c from '../../../../constants'
import { ERASER, BLOCK_ERASER } from '../../constants/blocks'
import s from './styles.cssmodule.scss'

interface IProps {
  cells?: ICell[]
  cellPointer: ICellPointer
  blockingTool: string
}

export class BlockingLayer extends Component<IProps> {
  shouldComponentUpdate(newProps: IProps) {
    return (
      JSON.stringify(newProps.cells) !== JSON.stringify(this.props.cells)
      || (newProps.blockingTool === ERASER || newProps.blockingTool === BLOCK_ERASER
        && !positionsEqual(newProps.cellPointer, this.props.cellPointer))
    )
  }

  getEraserSideRelativeToCell = (cell: ICell): string => {
    if (this.props.blockingTool !== ERASER) {
      return ''
    }

    const { cellPointer } = this.props
    const { x, y } = cell.position
    const diffX = Math.abs(cellPointer.x - x)
    const diffY = Math.abs(cellPointer.y - y)

    if (diffX > 1 || diffY > 1) {
      return ''
    }

    if (diffX === 0 && diffY === 0) {
      return 'center'
    }

    return getDirectionToPosition(cell.position, { x: cellPointer.x, y: cellPointer.y })
  }

  getCells = () => this.props.cells.map(cell => {
    const { position, blocking } = cell
    const { left, top, right, bottom, leftTop, leftBottom, rightTop } = blocking
    const cLeft = findNearCellByDirection(this.props.cells, position, c.LEFT)
    const cTop = findNearCellByDirection(this.props.cells, position, c.TOP)
    const cRight = findNearCellByDirection(this.props.cells, position, c.RIGHT)
    const cBottom = findNearCellByDirection(this.props.cells, position, c.BOTTOM)
    const ltBlock = left && top && !cLeft.blocking.top && !cTop.blocking.left && cLeft.blocking.rightTop
    const rtBlock = top && right && !cTop.blocking.right && !cRight.blocking.top && cTop.blocking.rightBottom
    const rbBlock = right && bottom && !cBottom.blocking.right && !cRight.blocking.bottom && cBottom.blocking.rightTop
    const lbBlock = left && bottom && !cLeft.blocking.bottom && !cBottom.blocking.left && cLeft.blocking.rightBottom
    const ltEnd = (
      (left && !top && leftTop && cLeft.blocking.rightTop && !cLeft.blocking.top && !cTop.blocking.left)
      || (top && !left && leftTop && cTop.blocking.leftBottom && !cTop.blocking.left && !cLeft.blocking.top)
    )
    const lbEnd = (
      (left && !bottom && leftBottom && cLeft.blocking.rightBottom && !cLeft.blocking.bottom && !cBottom.blocking.left)
      || (bottom && !left && leftBottom && cBottom.blocking.leftTop && !cLeft.blocking.bottom && !cBottom.blocking.left)
    )
    const rtEnd = (
      (top && !right && rightTop && cTop.blocking.rightBottom && !cTop.blocking.right && !cRight.blocking.top)
    )
    const eraserSide = this.getEraserSideRelativeToCell(cell)

    return (
      <div
        key={`${cell.position.x}-${cell.position.y}`}
        className={cn(s.cell, s[`eraserSide_${eraserSide}`], {
          [s.borderTop]: cell.blocking.top,
          [s.borderRight]: cell.blocking.right,
          [s.borderBottom]: cell.blocking.bottom,
          [s.borderLeft]: cell.blocking.left
        })}
        style={{
          width: c.CELL_SIZE,
          height: c.CELL_SIZE,
          left: cell.position.x * c.CELL_SIZE,
          top: cell.position.y * -c.CELL_SIZE
        }}
      >
        {ltBlock && <div className={cn(s.cornerBlock, s.leftTop)} />}
        {rtBlock && <div className={cn(s.cornerBlock, s.rightTop)} />}
        {rbBlock && <div className={cn(s.cornerBlock, s.rightBottom)} />}
        {lbBlock && <div className={cn(s.cornerBlock, s.leftBottom)} />}
        {ltEnd && <div className={cn(s.lineEndBlock, s.leftTop)} />}
        {lbEnd && <div className={cn(s.lineEndBlock, s.leftBottom)} />}
        {rtEnd && <div className={cn(s.lineEndBlock, s.rightTop)} />}
      </div>
    )
  })

  render() {
    const cells = this.getCells()

    return (
      <div className={s.blockingLayer}>{cells}</div>
    )
  }
}

export default connect(
  (state: any) => ({
    cells: state.mapeditor.mapData.blocking.cells,
    cellPointer: state.mapeditor.mapData.cellPointer,
    blockingTool: state.mapeditor.mapData.blocking.blockingTool
  }),
  {}
)(BlockingLayer)
