import React, { Component } from 'react'
import cn from 'classnames'
import { connect } from 'react-redux'
import { setBlockingTool } from '../../modules/blocking'
import * as blocks from '../../constants/blocks'
import s from './style.cssmodule.scss'

interface IProps {
  blockingTool: string
  setBlockingTool(tool: string): void
}

export class BlockingLibrary extends Component<IProps> {
  handleSelectBlockingTool = (blockingTool: string) => {
    const tool = this.props.blockingTool !== blockingTool ? blockingTool : ''
    this.props.setBlockingTool(tool)
  }

  render() {
    const { blockingTool } = this.props

    return (
      <div className={s.blocking}>
        <div className={s.tools}>
          <div className={s.item}>
            <h5 className={s.title}>Line</h5>
            <figure
              onClick={() => this.handleSelectBlockingTool(blocks.LINE_BLOCK)}
              className={cn(s.tool, s.line, { [s.selected]: blockingTool === blocks.LINE_BLOCK })}
            >
              <figure className={cn(s.part, s.part1)} />
              <figure className={cn(s.part, s.part2)} />
            </figure>
          </div>
          <div className={s.item}>
            <h5 className={s.title}>Corner</h5>
            <figure
              onClick={() => this.handleSelectBlockingTool(blocks.CORNER_BLOCK)}
              className={cn(s.tool, s.cornerBlock, { [s.selected]: blockingTool === blocks.CORNER_BLOCK })}
            >
              <figure className={cn(s.part, s.part1)} />
              <figure className={cn(s.part, s.part2)} />
              <figure className={cn(s.part, s.part3)} />
              <figure className={cn(s.part, s.part4)} />
            </figure>
          </div>
          <div className={s.item}>
            <h5 className={s.title}>Line end</h5>
            <figure
              onClick={() => this.handleSelectBlockingTool(blocks.LINE_END_BLOCK)}
              className={cn(s.tool, s.lineEndBlock, { [s.selected]: blockingTool === blocks.LINE_END_BLOCK })}
            >
              <figure className={cn(s.part, s.part1)} />
              <figure className={cn(s.part, s.part2)} />
              <figure className={cn(s.part, s.part3)} />
              <figure className={cn(s.part, s.part4)} />
            </figure>
          </div>
        </div>
        <div className={s.tools}>
          <div className={s.item}>
            <h5 className={s.title}>Line eraser</h5>
            <figure
              onClick={() => this.handleSelectBlockingTool(blocks.ERASER)}
              className={cn(s.tool, s.eraser, { [s.selected]: blockingTool === blocks.ERASER })}
            >
              <figure className={cn(s.part, s.part1)} />
              <figure className={cn(s.part, s.part2)} />
            </figure>
          </div>
          <div className={s.item}>
            <h5 className={s.title}>Cell eraser</h5>
            <figure
              onClick={() => this.handleSelectBlockingTool(blocks.BLOCK_ERASER)}
              className={cn(s.tool, s.blockEraser, { [s.selected]: blockingTool === blocks.BLOCK_ERASER })}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    blockingTool: state.mapeditor.mapData.blocking.blockingTool
  }),
  {
    setBlockingTool
  }
)(BlockingLibrary)
