import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import ILibraryObject from '../../interfaces/ILibraryObject'
import IMapObject from '../../interfaces/IMapObject'
import { fetchUrl } from '../../../../utils'
import { selectObjectOnMap } from '../../modules/mapeditor'
import { LAYERS_NAMES_ARR } from '../../constants/layers'
import { ACCESS_LEVEL_1 } from '../../../../constants/accessLevels'
import {
  addTagsToObject,
  removeObjectTags,
  showLibraryObjectPopup,
  changeDefaultLayer
} from '../../modules/library'
import { capitalizeString } from '../../utils'
import checkPermission from '../../../../utils/checkPermission'
import s from './styles.cssmodule.scss'

const PADDING_LEFT = 10
const MIN_TAG_LENGTH = 2
const MAX_TAG_LENGTH = 30

interface IProps {
  object: ILibraryObject
  objectElement: HTMLDivElement
  userAccessLevel: number
  userRole: string
  availableMapRoles: string[]
  adminMapComponent?: any
  searchTags: string
  filterObjectsConditions: { category: string }
  changeDefaultLayer: (layerName: string) => void
  selectObjectOnMap(object: IMapObject): void
  addTagsToObject(objectLibraryId: string, tags: string[]): void
  removeObjectTags(objectLibraryId: string, tags: string[]): void
  showLibraryObjectPopup(object: ILibraryObject): void
}

interface IState {
  error?: string
  tagInputValue: string
  objectsOnMap: IMapObject[]
  selectedObjectIndex: number
}

class Popup extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      tagInputValue: '',
      objectsOnMap: [],
      selectedObjectIndex: -1
    }
  }

  componentDidMount() {
    this.fetchObjects()
  }

  handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    const { value } = e.target

    if (value.length <= MAX_TAG_LENGTH) {
      this.setState({ tagInputValue: value, error: '' })
    }
  }

  fetchObjects = async () => {
    const response = await fetchUrl('findObjectsOnMap', { objectLibraryId: this.props.object.object_id })

    this.setState({ objectsOnMap: response.objects })
  }

  handleSearchObjectClick = (direction: 'next' | 'prev') => {
    const { selectedObjectIndex, objectsOnMap } = this.state
    const { adminMapComponent } = this.props

    if (objectsOnMap.length === 0) {
      return
    }

    const modifier = direction === 'next' ? 1 : -1
    let nextIndex = selectedObjectIndex + modifier

    if (nextIndex < 0) {
      nextIndex = objectsOnMap.length - 1
    } else {
      nextIndex = objectsOnMap.length < nextIndex + 1 || nextIndex <= 0 ? 0 : nextIndex
    }
    this.setState({ selectedObjectIndex: nextIndex })
    const object = objectsOnMap[nextIndex]
    const { x, y } = object.position

    adminMapComponent.setMapPosition(x, y)
    this.props.selectObjectOnMap({ ...object, object_id: object.objectLibraryId })
  }

  validateTag = (tag: string): boolean => tag && tag.length > MIN_TAG_LENGTH && tag.length < MAX_TAG_LENGTH

  handleCreateTagButtonClick = () => {
    const { tagInputValue } = this.state

    if (!this.validateTag(tagInputValue)) {
      this.setState({
        error: `The value must be at least ${MIN_TAG_LENGTH} characters but not more than ${MAX_TAG_LENGTH}`
      })

      return
    }

    const { object } = this.props
    const tags = [tagInputValue]

    this.props.addTagsToObject(object.object_id, tags)
    this.props.showLibraryObjectPopup({ ...object, tags: (object.tags || []).concat(tags) })
  }

  handleRemoveTagButtonClick = (tag: string) => {
    const { object } = this.props
    const tags = [tag]

    this.props.removeObjectTags(object.object_id, tags)
    this.props.showLibraryObjectPopup({ ...object, tags: object.tags.filter(t => t !== tag) })
  }

  getObjectTags = () => {
    const list = (this.props.object.tags || []).map(tag => (
      <li key={tag} className={s.tag}>
        <button
          type='button'
          onClick={() => this.handleRemoveTagButtonClick(tag)}
          className={cn(s.button, s.removeTagButton)}
        >
          &#x2715;
        </button>
        #{tag}
      </li>
    ))

    return <ul className={s.tagList}>{list}</ul>
  }

  _handleChangingDefaultLayer = (name) => {
    const { object } = this.props

    if (object.layer.name !== name) {
      this.props.changeDefaultLayer(name)
    }
  }

  getLayersButtons = () => {
    const { object, userAccessLevel, userRole, availableMapRoles } = this.props

    return LAYERS_NAMES_ARR.map(name => {
      const capitalizedName = capitalizeString(name)

      return checkPermission(userAccessLevel, ACCESS_LEVEL_1, userRole, availableMapRoles) ? (
        <button
          key={name}
          type='button'
          className={cn(s.layerButton, { [s.active]: object.layer.name === name })}
          onClick={() => this._handleChangingDefaultLayer(name)}
        >
          {capitalizedName}
        </button>
      ) : (
        <span
          key={name}
          className={cn(s.layerSpan, { [s.active]: object.layer.name === name })}
        >
          {capitalizedName}
        </span>
      )
    })
  }

  getPagination = () => {
    const { objectsOnMap, selectedObjectIndex } = this.state

    return objectsOnMap.length ?
      (
        <div className={s.counter}>
          <button
            type='button'
            className={cn(s.button, s.searchObjectsButton)}
            onClick={() => this.handleSearchObjectClick('prev')}
          >
            &lsaquo;
          </button>
          {selectedObjectIndex + 1} / {objectsOnMap.length}
          <button
            type='button'
            className={cn(s.button, s.searchObjectsButton)}
            onClick={() => this.handleSearchObjectClick('next')}
          >
            &rsaquo;
          </button>
        </div>
      ) : ''
  }

  render() {
    const { object, searchTags, filterObjectsConditions, objectElement } = this.props
    const { tagInputValue, error } = this.state
    const offsetLeft = (searchTags.length > 0 || filterObjectsConditions.category === 'Recently Used') ?
      objectElement.offsetLeft :
      object.position.left
    const style = { left: -offsetLeft + PADDING_LEFT, top: object.height }
    const tags = this.getObjectTags()

    return (
      <div className={s.popup} style={style}>
        <div className={s.section}>
          <div className={s.counterWrapper}>
            <span>View all</span>
            {this.getPagination()}
          </div>
        </div>
        <div className={s.section}>
          <div className={s.defaultLayerTitle}>
            <div className={s.left}>
              <span>
                <SVGIconGenerator
                  iconName='CTLayers'
                  preset={{ type: 'christmasTown', subtype: 'DEFAULTS' }}
                  customClass={s.layersIcon}
                />
              </span>
              <span className={s.title}>Default layer:</span>
            </div>
            <div className={s.right}>
              {this.getLayersButtons()}
            </div>
          </div>
        </div>
        <div className={s.section}>
          <div className={s.createTag}>
            <input
              className={cn(s.tagInput, { [s.error]: error && error.length > 0 })}
              onChange={this.handleInputChange}
              value={tagInputValue}
            />
            <button
              type='button'
              className={cn(s.button, s.createTagButton)}
              onClick={this.handleCreateTagButtonClick}
            >
              +
            </button>
          </div>
          {tags}
        </div>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    object: state.mapeditor.library.objects[state.mapeditor.library.popupObject.object_id],
    filterObjectsConditions: state.mapeditor.library.filterObjectsConditions,
    userAccessLevel: state.mapeditor.user.accessLevel,
    userRole: state.mapeditor.user.role,
    availableMapRoles: state.mapeditor.user.availableMapRoles
  }),
  {
    selectObjectOnMap,
    addTagsToObject,
    removeObjectTags,
    showLibraryObjectPopup,
    changeDefaultLayer
  }
)(Popup)
