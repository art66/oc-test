import React, { Component } from 'react'
import { connect } from 'react-redux'
import Draggable from 'react-draggable'
import cn from 'classnames'
import { unselectObject, selectLibraryObject, saveObjectPositionInLibrary } from '../../modules/mapeditor'
import { showLibraryObjectPopup, hideLibraryObjectPopup } from '../../modules/library'
import { CELL_SIZE } from '../../../../constants'
import ILibraryObject from '../../interfaces/ILibraryObject'
import Popup from './Popup'
import sAnim from '../../../../../../christmastown/src/routes/ChristmasTown/components/ObjectsLayer/animations.cssmodule.scss'

interface IProps {
  object: ILibraryObject
  selectedObjects: ILibraryObject[]
  editObjectsMode: boolean
  popupObject?: ILibraryObject
  adminMapComponent: any
  searchTags: string
  selectLibraryObject: (object: ILibraryObject) => void
  unselectObject: (object: ILibraryObject) => void
  saveObjectPositionInLibrary: (object: ILibraryObject, position: { left: number; top: number }) => void
  setListHeight: (height: number) => void
  getListHeight: () => number
  getObjectsMaxTopValue: () => number
  showLibraryObjectPopup: (object: ILibraryObject) => void
  hideLibraryObjectPopup: () => void
}

interface IState {
  mouseHolding: boolean
}

class ObjectsLibraryItem extends Component<IProps, IState> {
  objectElement: HTMLDivElement

  constructor(props: IProps) {
    super(props)
    this.state = { mouseHolding: false }
  }

  handleClick = () => {
    const { selectedObjects, object, editObjectsMode } = this.props

    if (editObjectsMode) {
      return
    }

    if (selectedObjects.find(obj => obj.object_id === object.object_id)) {
      this.props.unselectObject(object)
      this.props.hideLibraryObjectPopup()
    } else {
      this.props.selectLibraryObject(object)
    }
  }

  handleContextMenu = e => {
    e.preventDefault()

    if (this.props.editObjectsMode) {
      return
    }

    const { object, popupObject } = this.props

    if (object.object_id === popupObject.object_id) {
      this.props.hideLibraryObjectPopup()
    } else {
      this.props.selectLibraryObject(object)
      this.props.showLibraryObjectPopup(object)
    }
  }

  handleDrag = (_, object) => {
    this.checkAndSetListHeight(object.y)
  }

  handleDragStop = (_, object) => {
    const pos = {
      top: Math.round(object.y / CELL_SIZE) * CELL_SIZE,
      left: Math.round(object.x / CELL_SIZE) * CELL_SIZE
    }

    this.props.saveObjectPositionInLibrary(this.props.object, pos)
    this.checkAndSetListHeight(object.y)
  }

  checkAndSetListHeight = objectY => {
    const listHeight = this.props.getListHeight()

    if (listHeight < objectY + 200 || this.props.getObjectsMaxTopValue() < listHeight - 400) {
      this.props.setListHeight(objectY + 200)
    }
  }

  getPopup = popupOpened => {
    const { searchTags, adminMapComponent } = this.props

    return popupOpened && (
      <Popup
        adminMapComponent={adminMapComponent}
        objectElement={this.objectElement}
        searchTags={searchTags}
      />
    )
  }

  render() {
    const {
      object: { object_id, width, height, category, type, inexpressive, position, animation },
      popupObject,
      selectedObjects,
      editObjectsMode
    } = this.props
    const isSelected = selectedObjects.some(obj => obj.object_id === object_id)
    const animationName = animation ? animation.name : ''
    const popupOpened =
      popupObject.object_id === object_id && selectedObjects[0] && selectedObjects[0].object_id === object_id
    const positionInLibrary = {
      x: (position && position.left) || 0,
      y: (position && position.top) || 0
    }
    const className = cn(
      'ct-object',
      { selected: isSelected },
      { inexpressive },
      { [sAnim.animation]: animation },
      { [sAnim[animationName]]: animation }
    )
    const objectStyles = {
      width,
      height,
      cursor: editObjectsMode
        ? this.state.mouseHolding
          ? '-webkit-grabbing'
          : '-webkit-grab'
        : 'pointer'
    }

    return (
      <div
        className='object-library-item'
        style={{ zIndex: popupOpened ? 100 : 'auto' }}
        ref={el => this.objectElement = el}
      >
        <Draggable
          grid={[CELL_SIZE, CELL_SIZE]}
          disabled={!editObjectsMode}
          onDrag={this.handleDrag}
          onStop={this.handleDragStop}
          position={positionInLibrary}
        >
          <div>
            <div
              className={className}
              style={objectStyles}
              onClick={this.handleClick}
              onContextMenu={this.handleContextMenu}
              onMouseDown={() => this.setState({ mouseHolding: true })}
              onMouseUp={() => this.setState({ mouseHolding: false })}
            >
              <img src={`/images/v2/christmas_town/library/${category}/${type}.png`} draggable={false} alt='' />
            </div>
            {this.getPopup(popupOpened)}
          </div>
        </Draggable>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    popupObject: state.mapeditor.library.popupObject,
    selectedObjects: state.mapeditor.mapData.selectedObjects,
    editObjectsMode: state.mapeditor.library.editObjectsMode
  }),
  {
    showLibraryObjectPopup,
    hideLibraryObjectPopup,
    unselectObject,
    selectLibraryObject,
    saveObjectPositionInLibrary
  }
)(ObjectsLibraryItem)
