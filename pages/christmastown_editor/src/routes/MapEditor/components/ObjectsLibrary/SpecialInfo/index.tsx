import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import DefaultLayerLabel from './DefaultLayerLabel'
import EditLib from './EditLib'
import IObjectCategory from '../../../interfaces/IObjectCategory'
import { RECENTLY_USED_CATEGORY as recentlyUsedCategory } from '../../../constants/library'
import checkPermission from '../../../../../utils/checkPermission'
import { ACCESS_LEVEL_0 } from '../../../../../constants/accessLevels'
import s from '../styles.cssmodule.scss'

interface IProps {
  filterObjectsConditions: {
    category: string
  }
  filteringByTags: boolean
  categoryLayer: string
  objectCategories: IObjectCategory[]
  currentCategory: string
  userAccessLevel: number
  userRole: string
  availableMapRoles: string[]
  categorySelected: (category: string) => boolean
}

class SpecialInfo extends Component<IProps> {
  getModifiedCategorySign = () => {
    const { objectCategories, currentCategory } = this.props
    const categories = [recentlyUsedCategory].concat(objectCategories)
    const categoryAsObj = categories.find(item => item.name.toLowerCase() === currentCategory) as IObjectCategory

    return categoryAsObj && categoryAsObj.categoryLayerIsModified ? '*' : ''
  }

  renderEditLibBlock = () => {
    const { userAccessLevel, userRole, availableMapRoles } = this.props
    const canEdit = checkPermission(userAccessLevel, ACCESS_LEVEL_0, userRole, availableMapRoles)

    return (
      canEdit && (
        <div className={s.right}>
          <EditLib />
        </div>
      )
    )
  }

  renderDefaultLayerLabel = () => {
    const {
      filterObjectsConditions: { category },
      filteringByTags,
      categoryLayer
    } = this.props
    const signOfModifiedCategory = this.getModifiedCategorySign()

    return this.props.categorySelected(category.toLowerCase()) && !filteringByTags ? (
      <DefaultLayerLabel categoryLayer={categoryLayer} signOfModifiedCategory={signOfModifiedCategory} />
    ) : null
  }

  render() {
    return (
      <div className={s.specialInfo}>
        <div className={cn(s.left, s.defaultLayerTitle)}>{this.renderDefaultLayerLabel()}</div>
        {this.renderEditLibBlock()}
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    filterObjectsConditions: state.mapeditor.library.filterObjectsConditions,
    objectCategories: state.mapeditor.library.objectCategories,
    userAccessLevel: state.mapeditor.user.accessLevel,
    userRole: state.mapeditor.user.role,
    availableMapRoles: state.mapeditor.user.availableMapRoles
  }),
  {}
)(SpecialInfo)
