import React, { Component } from 'react'
import { connect } from 'react-redux'
import Tooltip from '@torn/shared/components/Tooltip'
import cn from 'classnames'
import { toggleLibraryMode, parseAndUpdateObjectsLibrary } from '../../../modules/mapeditor'

import s from '../styles.cssmodule.scss'

interface IProps {
  editObjectsMode: boolean
  toggleLibraryMode: () => void
  parseAndUpdateObjectsLibrary: () => void
}

class EditLib extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps) {
    const { editObjectsMode } = this.props

    return nextProps.editObjectsMode !== editObjectsMode
  }

  render() {
    const { editObjectsMode } = this.props

    return (
      <span className={s.editLib}>
        <i
          onClick={this.props.toggleLibraryMode}
          className={cn('edit-icon', { active: editObjectsMode })}
          id='editLibBtn'
        />
        <Tooltip parent='editLibBtn' arrow='center' position='top'>
          <span className='tipContent'>Edit mode</span>
        </Tooltip>
        <button className='parse btn btn-blue' onClick={this.props.parseAndUpdateObjectsLibrary}>
          Parse
        </button>
      </span>
    )
  }
}

export default connect(
  (state: any) => ({
    editObjectsMode: state.mapeditor.library.editObjectsMode
  }),
  {
    toggleLibraryMode,
    parseAndUpdateObjectsLibrary
  }
)(EditLib)
