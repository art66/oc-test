import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import s from '../styles.cssmodule.scss'

interface IProps {
  categoryLayer: string
  signOfModifiedCategory: string
}

export default function DefaultLayerLabel(props: IProps) {
  const { categoryLayer, signOfModifiedCategory } = props

  return (
    <>
      <span>
        <SVGIconGenerator iconName='CTLayers' preset={{ type: 'christmasTown', subtype: 'DEFAULTS' }} />
      </span>
      <span className={s.title}>
        Default layer: {categoryLayer}
        {signOfModifiedCategory}
      </span>
    </>
  )
}
