import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import uniq from 'lodash.uniq'
import ObjectsLibraryList from './ObjectsLibraryList'
import {
  saveObjectPositionInLibrary,
  saveObjectZIndex,
  setFilterCondition,
  selectLibraryObject
} from '../../modules/mapeditor'
import { capitalizeString } from '../../utils'
import checkPermission from '../../../../utils/checkPermission'
import { ACCESS_LEVEL_0 } from '../../../../constants/accessLevels'
import ILibraryObject from '../../interfaces/ILibraryObject'
import IObjectCategory from '../../interfaces/IObjectCategory'
import SpecialInfo from './SpecialInfo'
import { DEFAULT_CATEGORY_BG, RECENTLY_USED_CATEGORY as recentlyUsedCategory } from '../../constants/library'
import FiltersWrapper from './FiltersWrapper'
import s from './styles.cssmodule.scss'

interface IProps {
  objects: {
    [libraryObjectId: string]: ILibraryObject
  }
  userAccessLevel: number
  userRole?: string
  availableMapRoles?: string[]
  objectCategories: IObjectCategory[]
  filterObjectsConditions: { category: string }
  selectedObject: ILibraryObject
  popupObject: ILibraryObject
  adminMapComponent: any
  setFilterCondition: (name: string, value: string) => void
  selectLibraryObject: (object: ILibraryObject) => void
  saveObjectPositionInLibrary: (object: ILibraryObject, position: { left: number; top: number }) => void
}

interface IState {
  searchTags: string
  categoryLayer: string
  currentCategory: string
}

export class ObjectsLibrary extends Component<IProps, IState> {
  objectsTags: string[]

  constructor(props: IProps) {
    super(props)
    // this.scrollArea = React.createRef()
    this.objectsTags = []
    this.state = {
      searchTags: '',
      categoryLayer: '',
      currentCategory: this.props.filterObjectsConditions.category.toLowerCase()
    }
  }

  componentDidMount() {
    const { filterObjectsConditions } = this.props
    const { category } = filterObjectsConditions

    this.updateDefaultLayer(category.toLowerCase())
  }

  componentDidUpdate(prevProps: IProps) {
    const { popupObject, filterObjectsConditions, objects } = this.props

    if (
      ((!prevProps.popupObject && popupObject) || prevProps.popupObject.object_id !== popupObject.object_id)
      && filterObjectsConditions.category !== recentlyUsedCategory.name
    ) {
      const object = objects[popupObject.object_id]

      if (object) {
        this.props.setFilterCondition('category', object.category)
      }
    }

    if (this.objectsTags.length <= 0 && Object.keys(objects).length > 0) {
      this.objectsTags = uniq(Object.keys(objects).reduce((acc, objID) => acc.concat(objects[objID].tags), []))
    }
  }

  updateDefaultLayer = category => {
    const { objectCategories } = this.props
    const categories = [recentlyUsedCategory].concat(objectCategories)
    const categoryObj = categories.find(item => item.name.toLowerCase() === category) as IObjectCategory

    if (categoryObj) {
      const defaultLayerIsSetted = !!categoryObj.layer
      const categoryLayerTitle = defaultLayerIsSetted ? (categoryObj as IObjectCategory).layer : 'Not set'

      this.setState({
        categoryLayer: capitalizeString(categoryLayerTitle)
      })
    }
  }

  handleDropdownChange = (value, props) => {
    this.props.setFilterCondition(props.name, value.name)
  }

  getCategoryBackground = () => {
    const categories = this.props.objectCategories
    const selectedCategory = this.props.filterObjectsConditions.category

    if (!categories) {
      return DEFAULT_CATEGORY_BG
    }

    for (let i = 0; i < categories.length; i += 1) {
      if (selectedCategory.toLowerCase() === categories[i].name.toLowerCase() && categories[i].background) {
        return categories[i].background
      }
    }

    return DEFAULT_CATEGORY_BG
  }

  getFilteredCategories = categoryName => {
    const { objectCategories } = this.props
    const categories = [recentlyUsedCategory].concat(objectCategories)

    return categories.filter(c => c.name.toLowerCase().indexOf(categoryName) !== -1)
  }

  categorySelected = categoryName => {
    const categories = this.getFilteredCategories(categoryName)

    return categories.length === 1 && categoryName === categories[0].name.toLowerCase()
  }

  setCurrentCategory = (categoryName: string) => {
    this.setState({
      currentCategory: categoryName
    })
  }

  handleCategoryAutocompleteInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const categoryName = e.target.value.toLowerCase()

    this.props.setFilterCondition('category', e.target.value)

    if (this.categorySelected(categoryName)) {
      this.setCurrentCategory(categoryName)
      this.updateDefaultLayer(categoryName)
    }
  }

  _handleCategoryAutocompleteFocus = () => {
    this.props.setFilterCondition('category', '')
  }

  handleCategoryAutocompleteSelect = selected => {
    this.props.setFilterCondition('category', selected.name)
    this.setCurrentCategory(selected.name.toLowerCase())
    this.updateDefaultLayer(selected.name.toLowerCase())
  }

  handleTagsAutocompleteInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchTags: e.target.value })
  }

  handleTagsAutocompleteSelect = selected => {
    this.setState({ searchTags: selected.value })
  }

  _handleTagsAutocompleteFocus = () => {
    this.setState({ searchTags: '' })
  }

  getTagsList = () =>
    this.objectsTags
      .filter(tag => tag && tag.indexOf(this.state.searchTags) !== -1)
      .map(tag => ({ name: tag, value: tag }))

  render() {
    const {
      filterObjectsConditions: { category },
      userAccessLevel,
      userRole,
      availableMapRoles
    } = this.props
    const { categoryLayer, currentCategory, searchTags } = this.state
    const isRecentlyUsedCategory = category.toLowerCase() === recentlyUsedCategory.name.toLowerCase()
    const categoryBackground = this.getCategoryBackground()
    const canEdit = checkPermission(userAccessLevel, ACCESS_LEVEL_0, userRole, availableMapRoles)
    const tags = this.getTagsList()
    const filteringByTags = searchTags && searchTags.length > 0
    const objectsWrapCn = cn({
      objects: true,
      [categoryBackground]: true,
      [s.filteringByTags]: filteringByTags || isRecentlyUsedCategory
    })

    return (
      <div className='objects-container'>
        <div className='filter-wrap'>
          <FiltersWrapper
            getFilteredCategories={this.getFilteredCategories}
            searchTags={searchTags}
            tags={tags}
            handleCategoryAutocompleteInputChange={this.handleCategoryAutocompleteInputChange}
            handleCategoryAutocompleteSelect={this.handleCategoryAutocompleteSelect}
            handleCategoryAutocompleteFocus={this._handleCategoryAutocompleteFocus}
            handleTagsAutocompleteFocus={this._handleTagsAutocompleteFocus}
            handleTagsAutocompleteInputChange={this.handleTagsAutocompleteInputChange}
            handleTagsAutocompleteSelect={this.handleTagsAutocompleteSelect}
          />
          <SpecialInfo
            categorySelected={this.categorySelected}
            filteringByTags={filteringByTags}
            categoryLayer={categoryLayer}
            currentCategory={currentCategory}
          />
        </div>
        <div className={objectsWrapCn}>
          <div className='scrollarea scroll-area scrollbar-thin'>
            <div className='scrollarea-content content'>
              <ObjectsLibraryList
                adminMapComponent={this.props.adminMapComponent}
                searchTags={searchTags}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  popupObject: state.mapeditor.library.popupObject,
  objects: state.mapeditor.library.objects,
  objectCategories: state.mapeditor.library.objectCategories,
  filterObjectsConditions: state.mapeditor.library.filterObjectsConditions,
  userAccessLevel: state.mapeditor.user.accessLevel,
  userRole: state.mapeditor.user.role,
  availableMapRoles: state.mapeditor.user.availableMapRoles
})

const mapActionsToProps = {
  saveObjectPositionInLibrary,
  saveObjectZIndex,
  setFilterCondition,
  selectLibraryObject
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(ObjectsLibrary)
