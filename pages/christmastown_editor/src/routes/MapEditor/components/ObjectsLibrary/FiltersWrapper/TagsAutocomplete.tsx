import React, { Component } from 'react'
import Autocomplete from '@torn/shared/components/Autocomplete'
import cn from 'classnames'
import s from '../styles.cssmodule.scss'
import icons from '../../../../../styles/icons.cssmodule.scss'

interface IProps {
  searchTags: string
  tags: { name: string; value: string }[]
  handleTagsAutocompleteFocus: () => void
  handleTagsAutocompleteInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  handleTagsAutocompleteSelect: (selected: { value: string }) => void
}

class TagsAutocomplete extends Component<IProps> {
  render() {
    const { searchTags, tags } = this.props

    return (
      <div className={s.autocompleteWrapper}>
        <Autocomplete
          name='tag'
          value={searchTags || ''}
          placeholder='Type tag name'
          className={s.tagsAutocomplete}
          inputClassName={s.tagsAutocompleteInput}
          autoFocus={false}
          list={tags}
          onFocus={this.props.handleTagsAutocompleteFocus}
          onInput={this.props.handleTagsAutocompleteInputChange}
          onSelect={this.props.handleTagsAutocompleteSelect}
          noItemsFoundMessage={'No items found'}
        />
        <i className={cn(s.searchIcon, icons.search)} />
      </div>
    )
  }
}

export default TagsAutocomplete
