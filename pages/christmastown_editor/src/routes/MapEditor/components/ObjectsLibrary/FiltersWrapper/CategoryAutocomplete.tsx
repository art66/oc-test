import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Autocomplete from '@torn/shared/components/Autocomplete'
import ILibraryObject from '../../../interfaces/ILibraryObject'
import icons from '../../../../../styles/icons.cssmodule.scss'
import s from '../styles.cssmodule.scss'

interface IProps {
  filterObjectsConditions: {
    category: string
  }
  handleCategoryAutocompleteInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  handleCategoryAutocompleteSelect: (selected: { name: string }) => void
  handleCategoryAutocompleteFocus: () => void
  getFilteredCategories: (category: string) => (ILibraryObject | { name: string })[]
}

class CategoryAutocomplete extends Component<IProps> {
  render() {
    const {
      filterObjectsConditions: { category }
    } = this.props
    const categories = this.props.getFilteredCategories(category.toLowerCase())

    return (
      <div className={s.autocompleteWrapper}>
        <Autocomplete
          name='category'
          value={category || ''}
          placeholder='Enter category'
          className={s.tagsAutocomplete}
          inputClassName={s.tagsAutocompleteInput}
          autoFocus={false}
          list={categories}
          onInput={this.props.handleCategoryAutocompleteInputChange}
          onSelect={this.props.handleCategoryAutocompleteSelect}
          onFocus={this.props.handleCategoryAutocompleteFocus}
          noItemsFoundMessage='No items found'
        />
        <i className={cn(s.searchIcon, icons.search)} />
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    filterObjectsConditions: state.mapeditor.library.filterObjectsConditions
  }),
  {}
)(CategoryAutocomplete)
