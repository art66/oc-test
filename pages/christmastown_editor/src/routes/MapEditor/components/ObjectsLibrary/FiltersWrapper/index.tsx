import React, { Component } from 'react'
import CategoryAutocomplete from './CategoryAutocomplete'
import ILibraryObject from '../../../interfaces/ILibraryObject'
import TagsAutocomplete from './TagsAutocomplete'

interface IProps {
  searchTags: string
  tags: { name: string; value: string }[]
  getFilteredCategories: (category: string) => (ILibraryObject | { name: string })[]
  handleCategoryAutocompleteInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  handleCategoryAutocompleteSelect: (selected: { name: string }) => void
  handleCategoryAutocompleteFocus: () => void
  handleTagsAutocompleteFocus: () => void
  handleTagsAutocompleteInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  handleTagsAutocompleteSelect: (selected: { value: string }) => void
}

class FiltersWrapper extends Component<IProps> {
  render() {
    const { searchTags, tags } = this.props

    return (
      <>
        <CategoryAutocomplete
          handleCategoryAutocompleteInputChange={this.props.handleCategoryAutocompleteInputChange}
          handleCategoryAutocompleteSelect={this.props.handleCategoryAutocompleteSelect}
          handleCategoryAutocompleteFocus={this.props.handleCategoryAutocompleteFocus}
          getFilteredCategories={this.props.getFilteredCategories}
        />
        <TagsAutocomplete
          searchTags={searchTags}
          tags={tags}
          handleTagsAutocompleteFocus={this.props.handleTagsAutocompleteFocus}
          handleTagsAutocompleteInputChange={this.props.handleTagsAutocompleteInputChange}
          handleTagsAutocompleteSelect={this.props.handleTagsAutocompleteSelect}
        />
      </>
    )
  }
}

export default FiltersWrapper
