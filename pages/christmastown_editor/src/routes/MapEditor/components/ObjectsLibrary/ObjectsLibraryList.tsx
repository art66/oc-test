import React, { Component } from 'react'
import { connect } from 'react-redux'
import orderBy from 'lodash.orderby'
import ObjectsLibraryItem from './ObjectsLibraryItem'
import ILibraryObject from '../../interfaces/ILibraryObject'
import { selectLibraryObject, saveObjectPositionInLibrary } from '../../modules/mapeditor'
import { RECENTLY_USED } from '../../constants'

const SPARE_HEIGHT = 400
const SPARE_HEIGHT_FULLSCREEN = 255

interface IProps {
  fullscreenMode: boolean
  objects: {
    [libraryObjectId: string]: ILibraryObject
  },
  selectedObjects: ILibraryObject[]
  filterObjectsConditions: {
    category: string
  },
  searchTags: string
  editObjectsMode: boolean
  recentlyUsedObjects: ILibraryObject[]
  adminMapComponent: Component
  selectLibraryObject: (object: ILibraryObject) => void
  saveObjectPositionInLibrary: (object: ILibraryObject, position: { left: number; top: number }) => void
}

interface IState {
  listHeight: number
}

export class ObjectsLibraryList extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      listHeight: props.fullscreenMode ? SPARE_HEIGHT_FULLSCREEN : SPARE_HEIGHT + this.getObjectsMaxTopValue()
    }
  }

  shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    const { objects, selectedObjects, filterObjectsConditions, searchTags, editObjectsMode } = this.props
    const { listHeight } = this.state

    return Object.keys(objects).length !== Object.keys(nextProps.objects).length
      || filterObjectsConditions.category !== nextProps.filterObjectsConditions.category
      || searchTags !== nextProps.searchTags
      || JSON.stringify(selectedObjects) !== JSON.stringify(nextProps.selectedObjects)
      || editObjectsMode
      || nextProps.editObjectsMode
      || listHeight !== nextState.listHeight
  }

  componentDidMount() {
    const { filterObjectsConditions } = this.props

    this.setMaxListHeight(filterObjectsConditions.category)
  }

  componentDidUpdate(prevProps: IProps) {
    const { filterObjectsConditions: { category } } = this.props

    if (category !== prevProps.filterObjectsConditions.category) {
      this.setMaxListHeight(category)
    }
  }

  getObjectsMaxTopValue = (category = this.props.filterObjectsConditions.category) => {
    const { objects } = this.props

    return Math.max.apply(
      Math,
      Object.keys(objects)
        .filter(objID => objects[objID].category.toLowerCase() === category.toLowerCase())
        .map(objectID => objects[objectID].position.top)
    )
  }

  getListHeight = () => {
    const { listHeight } = this.state

    return listHeight
  }

  getObjectsList = () => {
    const { searchTags, recentlyUsedObjects, filterObjectsConditions, objects } = this.props
    const category = filterObjectsConditions.category.toLowerCase()

    let list = Object.keys(objects)
      .filter(objectID => (searchTags.length === 0
          && (category === objects[objectID].category.toLowerCase() || (category === 'recently used'
            && recentlyUsedObjects.some(obj => obj.object_id === objects[objectID].object_id)))
      )
        || (searchTags.length > 0 && objects[objectID].tags
          && objects[objectID].tags.some(tag => searchTags.indexOf(tag) !== -1 || tag.indexOf(searchTags) !== -1)))
      .map(objectID => ({
        ...objects[objectID],
        order: (recentlyUsedObjects.find(rObj => rObj.object_id === objects[objectID].object_id) || {}).order
          || objects[objectID].order
      }))

    if (category === RECENTLY_USED) {
      list = orderBy(list, ['order'], ['desc'])
    }

    return list
  }

  renderObjectsList = () => {
    const { searchTags, adminMapComponent } = this.props
    const list = this.getObjectsList()

    return list.map(object => (
      <ObjectsLibraryItem
        key={object.object_id}
        object={object}
        setListHeight={this.setListHeight}
        getListHeight={this.getListHeight}
        getObjectsMaxTopValue={this.getObjectsMaxTopValue}
        adminMapComponent={adminMapComponent}
        searchTags={searchTags}
      />
    ))
  }

  setListHeight = height => {
    this.setState({ listHeight: height })
  }

  setMaxListHeight = category => {
    const { fullscreenMode } = this.props
    const height = this.getObjectsMaxTopValue(category)
    const spaceHeight = fullscreenMode ? SPARE_HEIGHT_FULLSCREEN : SPARE_HEIGHT

    this.setListHeight(height + spaceHeight)
  }

  render() {
    const { listHeight } = this.state
    const objectsList = this.renderObjectsList()

    return (
      <div className='objects-list' style={{ height: listHeight }}>
        {objectsList}
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    recentlyUsedObjects: state.mapeditor.library.recentlyUsedObjects,
    objects: state.mapeditor.library.objects,
    filterObjectsConditions: state.mapeditor.library.filterObjectsConditions,
    selectedObjects: state.mapeditor.mapData.selectedObjects,
    fullscreenMode: state.mapeditor.mapData.fullscreenMode,
    editObjectsMode: state.mapeditor.library.editObjectsMode
  }),
  {
    selectLibraryObject,
    saveObjectPositionInLibrary
  }
)(ObjectsLibraryList)
