import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { getCookie } from '@torn/shared/utils'
import Map from './Map'
import Coordinates from './Coordinates'
import MapControls from './MapControls'
import { getViewportWidthInCells, getViewportHeightInCells } from '../utils/index'
import {
  CELL_SIZE,
  ADMIN_MAP_VIEWPORT_X,
  ADMIN_MAP_VIEWPORT_Y,
  MAP_VIEWPORT_CELLS_STOCK,
  ADMIN_POSITION_MIN_X,
  ADMIN_POSITION_MIN_Y,
  MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_Y,
  MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_X
} from '../../../constants'
import {
  TIME_TO_UPDATE_MAP_FULLSCREEN,
  VIEW_TOGGLE,
  DEFAULT_COOKIE_VIEW_TOGGLE
} from '../constants'

export class AdminMap extends Component {
  constructor(props) {
    super(props)
    const { mapData, updateMapEditorState } = props
    const { mapSize, mapPosition } = mapData

    this.timerId = 0
    this.mapHeight = mapSize.mapHeight
    this.mapWidth = mapSize.mapWidth
    this.mapWidthInCells = this.mapWidth / CELL_SIZE
    this.mapHeightInCells = this.mapHeight / CELL_SIZE
    this.halfHeight = this.mapHeight / 2

    const yInPix = mapPosition.y * CELL_SIZE - this.halfHeight
    const xInPix = mapPosition.x * -CELL_SIZE

    this.state = {
      dragX: xInPix <= 0 ? xInPix : 0,
      dragY: yInPix,
      widthViewportInCells: getViewportWidthInCells(),
      heightViewportInCells: getViewportHeightInCells()
    }

    updateMapEditorState(this.setMapPosition)
  }

  componentDidMount() {
    const { onRef } = this.props

    onRef(this)
    window.adminMapComponent = this
    window.addEventListener('focus', this._handleTabChange)
  }

  componentDidUpdate() {
    const { mapData } = this.props

    if (mapData.fullscreenMode) {
      window.addEventListener('resize', this.updateDimensions)
    } else {
      window.removeEventListener('resize', this.updateDimensions)
    }
  }

  componentWillUnmount() {
    const { onRef } = this.props

    onRef(this)
    window.adminMapComponent = null
    window.removeEventListener('focus', this._handleTabChange)
    clearTimeout(this.timerId)
  }

  _handleTabChange = () => {
    const { mapData, checkTabsSync, setViewToggleLayers, toggleViewLayers } = this.props
    const { widthViewportInCells, heightViewportInCells } = this.state
    const viewToggleCookie = getCookie(VIEW_TOGGLE) && JSON.parse(getCookie(VIEW_TOGGLE)) || DEFAULT_COOKIE_VIEW_TOGGLE

    const mapSize = mapData.fullscreenMode ?
      { width: widthViewportInCells, height: heightViewportInCells } :
      { width: ADMIN_MAP_VIEWPORT_X, height: ADMIN_MAP_VIEWPORT_Y }

    mapData.mapId && checkTabsSync(mapData.mapId, mapSize)
    setViewToggleLayers(viewToggleCookie.tools)

    if (mapData.viewToggle !== viewToggleCookie.open) {
      toggleViewLayers()
    }
  }

  updateDimensions = () => {
    const { dragX, dragY } = this.state
    const widthViewportInCells = getViewportWidthInCells()
    const heightViewportInCells = getViewportHeightInCells()

    this.setState({
      widthViewportInCells,
      heightViewportInCells
    })

    const drag = this.getViewportPoints(dragX, dragY)
    const { leftTop, rightBottom } = drag
    const centerX = (rightBottom.x + leftTop.x) / 2
    const centerY = (rightBottom.y + leftTop.y) / 2 + MAP_VIEWPORT_CELLS_STOCK

    if (this.timerId) {
      clearTimeout(this.timerId)
    }

    this.timerId = setTimeout(() => {
      this.setMapPosition(centerX, centerY)
    }, TIME_TO_UPDATE_MAP_FULLSCREEN)
  }

  handleDrag = (_, object) => {
    const { mapData } = this.props
    const { x, y } = object

    let dragX = this.handleDragValue(x, this.mapWidth, ADMIN_MAP_VIEWPORT_X)
    let dragY = this.handleDragValue(y, this.mapHeight, ADMIN_MAP_VIEWPORT_Y)

    if (mapData.fullscreenMode) {
      dragX = this.handleDragFullscreen({
        coordinate: x,
        mapSize: this.mapWidth,
        sizeScreen: getViewportWidthInCells(),
        stockCell: MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_X
      })
      dragY = this.handleDragFullscreen({
        coordinate: y,
        mapSize: this.mapHeight,
        sizeScreen: getViewportHeightInCells(),
        stockCell: MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_Y
      })
    }

    this.setState({
      dragX,
      dragY
    })
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  handleDragValue = (coordinate, mapSize, ADMIN_MAP_VIEWPORT) => {
    let drag = coordinate
    const compareValue = -mapSize + ADMIN_MAP_VIEWPORT * CELL_SIZE

    if (coordinate > 0) {
      drag = 0
    } else if (coordinate < compareValue) {
      drag = compareValue
    }

    return drag
  }

  handleDragFullscreen = params => {
    const { coordinate, mapSize, sizeScreen, stockCell } = params

    let drag = coordinate
    const compareValue = -mapSize + sizeScreen * CELL_SIZE - stockCell * CELL_SIZE

    if (coordinate > 0) {
      drag = 0
    } else if (coordinate < compareValue) {
      drag = compareValue
    }

    return drag
  }

  handleStartDrag = (_, object) => {
    this.setState({
      dragXStart: object.x <= 0 ? object.x : 0,
      dragYStart: object.y,
      cursorType: 'move'
    })
  }

  handleStopDrag = (_, object) => {
    const x = object.x <= 0 ? object.x : 0
    const { y } = object

    this.setState({ cursorType: 'default' })
    this.updateMap(x, y)
  }

  getViewportPoints = (dragX = this.state.dragX, dragY = this.state.dragY) => {
    const { mapData } = this.props
    const { widthViewportInCells, heightViewportInCells } = this.state
    const viewportX = mapData.fullscreenMode ? widthViewportInCells : ADMIN_MAP_VIEWPORT_X
    const viewportY = mapData.fullscreenMode ? heightViewportInCells : ADMIN_MAP_VIEWPORT_Y
    const cellsStockX = mapData.fullscreenMode ? MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_X : MAP_VIEWPORT_CELLS_STOCK
    const cellsStockY = mapData.fullscreenMode ? MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_Y : 0

    const leftTop = {
      x: Math.round(dragX / -CELL_SIZE),
      y: Math.round((-dragY - this.halfHeight) / -CELL_SIZE) + MAP_VIEWPORT_CELLS_STOCK
    }

    const rightBottom = {
      x: Math.round(
        dragX / -CELL_SIZE
        + viewportX
        + cellsStockX
      ),
      y: Math.round(
        (-dragY - this.halfHeight) / -CELL_SIZE
        - viewportY
        + cellsStockY
      )
    }

    return {
      leftTop,
      rightBottom: {
        x: rightBottom.x > this.mapWidthInCells ? this.mapWidthInCells : rightBottom.x,
        y: rightBottom.y
      }
    }
  }

  updateMap = (dragX = this.state.dragX, dragY = this.state.dragY) => {
    const { dragXStart, dragYStart } = this.state
    const { updateMap } = this.props

    if (dragXStart !== dragX || dragYStart !== dragY) {
      updateMap(this.getViewportPoints(dragX, dragY))
      this.setState({ dragXStart: null, dragYStart: null })
    }
  }

  setMapPosition = (x, y) => {
    const { mapData } = this.props
    const adminPositionFullscreenMinX = Math.floor(getViewportWidthInCells() / 2)
    const adminPositionFullscreenMinY = Math.floor(getViewportHeightInCells() / 2)
    const adminPositionX = mapData.fullscreenMode ? adminPositionFullscreenMinX : ADMIN_POSITION_MIN_X
    const adminPositionY = mapData.fullscreenMode ? adminPositionFullscreenMinY : ADMIN_POSITION_MIN_Y

    const intX = parseInt(x, 10)
    const intY = parseInt(y, 10)

    return this.handleMapPositionCoordinates(intX, intY, { adminPositionX, adminPositionY })
  }

  handleMapPositionX = (intX, validX, adminPositionX) => {
    const { mapData } = this.props
    const cellsStock = mapData.fullscreenMode ? MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_X : MAP_VIEWPORT_CELLS_STOCK
    const val = this.mapWidthInCells - adminPositionX - cellsStock

    let x = validX

    if (intX) {
      if (intX < adminPositionX) {
        x = adminPositionX
      } else {
        x = intX >= this.mapWidthInCells ? this.mapWidthInCells - adminPositionX - MAP_VIEWPORT_CELLS_STOCK : intX
      }

      if (intX >= val) {
        x = val
      }
    }

    if (validX % 2 !== 0 && mapData.fullscreenMode) {
      x += MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_X
    }

    return x
  }

  handleMapPositionY = (intY, validY, adminPositionY) => {
    const { mapData } = this.props
    const cellsStock = mapData.fullscreenMode ? MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_Y : 0

    let y = validY

    if (intY || intY === 0) {
      y = intY >= 0 ? intY + adminPositionY - cellsStock : intY - adminPositionY
      if (Math.abs(intY) > this.mapWidthInCells / 2 - adminPositionY) {
        y = intY >= 0 ? this.mapWidthInCells / 2 : -(this.mapHeightInCells / 2)
      }
    }

    return y
  }

  handleMapPositionXInPix = validX => {
    const { mapData } = this.props
    const mapViewport = mapData.fullscreenMode ?
      Math.ceil(getViewportWidthInCells() / 2 + MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_X) :
      Math.floor(ADMIN_MAP_VIEWPORT_X / 2) + MAP_VIEWPORT_CELLS_STOCK

    return validX * -CELL_SIZE + mapViewport * CELL_SIZE
  }

  handleMapPositionYInPix = (intY, validY) => {
    const { mapData } = this.props
    const mapViewport = mapData.fullscreenMode ?
      getViewportHeightInCells() - MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_Y :
      ADMIN_MAP_VIEWPORT_Y

    return intY >= 0 ?
      CELL_SIZE * validY - this.halfHeight :
      CELL_SIZE * (validY + mapViewport) - this.halfHeight
  }

  handleMapPositionCoordinates = (intX, intY, adminPosition) => {
    const { adminPositionX, adminPositionY } = adminPosition

    let validX = adminPositionX
    let validY = -adminPositionY

    validX = this.handleMapPositionX(intX, validX, adminPositionX)
    validY = this.handleMapPositionY(intY, validY, adminPositionY)

    const xInPix = this.handleMapPositionXInPix(validX)
    const yInPix = this.handleMapPositionYInPix(intY, validY)

    this.setState({ dragX: xInPix <= 0 ? xInPix : 0, dragY: yInPix })
    this.updateMap(xInPix, yInPix)
  }

  render() {
    const { dragX, dragY, cursorType } = this.state
    const {
      mapData,
      addParameterToMap,
      selectParameterOnMap,
      unSelectOnMap,
      removeParameterOnMap,
      setPointerPos,
      toggleCoordinates,
      addObjectToMap,
      selectObjectOnMap,
      selectObjectsOnMap,
      addSelectedObject,
      unselectObject,
      removeObjectOnMap,
      changeObjectPosition,
      toggleFullscreenMode,
      toggleViewLayers,
      setViewToggleLayers,
      setScale,
      currentTab,
      prevTab,
      selectedArea,
      addAreaOnMap
    } = this.props

    return (
      <div className='admin-map'>
        <MapControls
          toggleCoordinates={toggleCoordinates}
          setMapPosition={this.setMapPosition}
          fullscreenMode={mapData.fullscreenMode}
          toggleFullscreenMode={toggleFullscreenMode}
          updateMapFullscreenMode={this.updateDimensions}
          toggleViewLayers={toggleViewLayers}
          setViewToggleLayers={setViewToggleLayers}
          infoText={mapData.infoText}
          setScale={setScale}
          scale={mapData.scale}
          currentTab={currentTab}
          prevTab={prevTab}
        />
        {mapData.scale === 1 && (
          <Coordinates
            dragX={dragX}
            dragY={dragY}
            show={mapData.showCoordinates}
            mapSize={mapData.mapSize}
            fullscreenMode={mapData.fullscreenMode}
          />
        )}
        <Map
          mapData={mapData}
          currentTab={currentTab}
          cursorType={cursorType}
          setPointerPos={setPointerPos}
          addParameterToMap={addParameterToMap}
          selectParameterOnMap={selectParameterOnMap}
          unSelectOnMap={unSelectOnMap}
          removeParameterOnMap={removeParameterOnMap}
          handleDrag={this.handleDrag}
          handleStartDrag={this.handleStartDrag}
          handleStopDrag={this.handleStopDrag}
          dragX={dragX}
          dragY={dragY}
          addObjectToMap={addObjectToMap}
          selectObjectOnMap={selectObjectOnMap}
          selectObjectsOnMap={selectObjectsOnMap}
          addSelectedObject={addSelectedObject}
          unselectObject={unselectObject}
          removeObjectOnMap={removeObjectOnMap}
          changeObjectPosition={changeObjectPosition}
          updateMap={this.updateMap}
          getViewportPoints={this.getViewportPoints}
          selectedArea={selectedArea}
          addAreaOnMap={addAreaOnMap}
        />
      </div>
    )
  }
}

AdminMap.propTypes = {
  mapData: PropTypes.object.isRequired,
  currentTab: PropTypes.string,
  prevTab: PropTypes.string,
  setPointerPos: PropTypes.func.isRequired,
  addParameterToMap: PropTypes.func.isRequired,
  selectParameterOnMap: PropTypes.func.isRequired,
  unSelectOnMap: PropTypes.func.isRequired,
  removeParameterOnMap: PropTypes.func.isRequired,
  toggleCoordinates: PropTypes.func.isRequired,
  addObjectToMap: PropTypes.func.isRequired,
  selectObjectOnMap: PropTypes.func.isRequired,
  removeObjectOnMap: PropTypes.func.isRequired,
  changeObjectPosition: PropTypes.func,
  toggleViewLayers: PropTypes.func.isRequired,
  setViewToggleLayers: PropTypes.func.isRequired,
  toggleFullscreenMode: PropTypes.func.isRequired,
  updateMap: PropTypes.func.isRequired,
  selectedArea: PropTypes.object,
  addAreaOnMap: PropTypes.func,
  setScale: PropTypes.func,
  updateMapEditorState: PropTypes.func,
  checkTabsSync: PropTypes.func,
  ref: PropTypes.func,
  onRef: PropTypes.func,
  selectObjectsOnMap: PropTypes.func,
  addSelectedObject: PropTypes.func,
  unselectObject: PropTypes.func
}

export default AdminMap
