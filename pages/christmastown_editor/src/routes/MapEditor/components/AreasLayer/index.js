import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'
import { addAreaOnMap, selectAreaOnMap } from '../../modules/mapeditor'
import { CELL_SIZE } from '../../../../constants'
import s from './style.cssmodule.scss'

class AreasLayer extends Component {
  getBorders(area) {
    const { areas } = this.props
    const border = { top: true, right: true, bottom: true, left: true }

    areas.list.map(item => {
      if (item._id !== area._id) {
        return border
      }

      if (item.position.x === area.position.x) {
        if (item.position.y === area.position.y + 1) {
          border.top = false
        } else if (item.position.y === area.position.y - 1) {
          border.bottom = false
        }
      }

      if (item.position.y === area.position.y) {
        if (item.position.x === area.position.x + 1) {
          border.right = false
        } else if (item.position.x === area.position.x - 1) {
          border.left = false
        }
      }
    })

    return border
  }

  getAreas() {
    const { areas, currentTab, selectAreaOnMap } = this.props

    return areas.list.map(area => {
      const borders = this.getBorders(area)

      return (
        <div
          key={area.hash}
          style={{
            width: CELL_SIZE,
            height: CELL_SIZE,
            left: area.position.x * CELL_SIZE,
            top: area.position.y * -CELL_SIZE,
            pointerEvents: currentTab === 'areas' ? 'auto' : 'none'
          }}
          onClick={() => selectAreaOnMap(area)}
          className={cn(
            s.ctArea,
            { [s.top]: borders.top },
            { [s.right]: borders.right },
            { [s.bottom]: borders.bottom },
            { [s.left]: borders.left },
            { [s.selected]: area.selected },
            { [s.notSaved]: !area.saved }
          )}
        >
          <div className={s.border} style={{ borderColor: area.color }}>
            <i className={cn(s.ctAreaIcon)} style={{ backgroundColor: area.color }} />
          </div>
        </div>
      )
    })
  }

  render() {
    const areas = this.getAreas()

    return <div className={s.areasLayer}>{areas}</div>
  }
}

AreasLayer.propTypes = {
  areas: PropTypes.object,
  currentTab: PropTypes.string,
  selectedArea: PropTypes.object,
  selectAreaOnMap: PropTypes.func
}

const mapStateToProps = state => ({
  areas: state.mapeditor.mapData.areas,
  currentTab: state.mapeditor.library.currentTab,
  selectedArea: state.mapeditor.library.areas.selectedArea
})

const mapActionsToProps = {
  addAreaOnMap,
  selectAreaOnMap
}

export default connect(mapStateToProps, mapActionsToProps)(AreasLayer)
