import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ParametersLayerItem from './ParametersLayerItem'

export class ParametersLayer extends Component {
  shouldComponentUpdate(newProps) {
    const { currentTab, selectedParameter, highlightedCells, parameters } = this.props
    const hash = newProps.parameters.reduce((hash, item) => hash + item.hash, '')
    const needUpdate =
      this.hash !== hash
      || newProps.currentTab !== currentTab
      || newProps.selectedParameter.hash !== undefined
      || newProps.selectedParameter.hash !== selectedParameter.hash
      || (newProps.highlightedCells.type === 'parameter' && newProps.highlightedCells.hash !== highlightedCells.hash)
      || JSON.stringify(newProps.parameters) !== JSON.stringify(parameters)

    this.hash = hash

    return needUpdate
  }

  buildParameterList = parameters => {
    const {
      selectParameterOnMap,
      selectedParameter,
      removeParameterOnMap,
      currentTab,
      unSelectOnMap,
      highlightedCells
    } = this.props

    let parameterList = []

    parameters.map(parameter => {
      parameterList.push(
        <ParametersLayerItem
          key={parameter.hash}
          parameter={parameter}
          selectParameterOnMap={selectParameterOnMap}
          selectedParameter={selectedParameter}
          removeParameterOnMap={removeParameterOnMap}
          currentTab={currentTab}
          unSelectOnMap={unSelectOnMap}
          highlightedCells={highlightedCells}
        />
      )
    })

    return parameterList
  }

  render() {
    const { parameters } = this.props
    const parameterList = this.buildParameterList(parameters)

    return <div className='parameters-layer'>{parameterList}</div>
  }
}

ParametersLayer.propTypes = {
  currentTab: PropTypes.string,
  selectParameterOnMap: PropTypes.func.isRequired,
  selectedParameter: PropTypes.object.isRequired,
  removeParameterOnMap: PropTypes.func.isRequired,
  parameters: PropTypes.array.isRequired,
  highlightedCells: PropTypes.object,
  unSelectOnMap: PropTypes.func
}

export default ParametersLayer
