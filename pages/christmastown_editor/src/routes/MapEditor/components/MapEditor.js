import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import DebugBox from '@torn/shared/components/DebugBox'
import Preloader from '@torn/shared/components/Preloader'
import ParametersLibrary from '../../../components/ParametersLibrary'
import NpcLibrary from '../../../components/NpcLibrary'
import ObjectsLibrary from './ObjectsLibrary'
import AreasLibrary from './AreasLibrary'
import AdminMap from './AdminMap'
import ObjectsLayers from './ObjectsLayers'
import BlockingLibrary from './BlockingLibrary'
import History from './History'
import VersionSystem from './VersionSystem'
import MapTitle from './MapTitle'
import Overlay from './Overlay'
import '../../../styles/christmas_town.css'
import '../../../styles/christmas_town_admin.css'
import ToolsPanel from './ToolsPanel/index'
import TabsPanel from './TabsPanel'
import checkPermission from '../../../utils/checkPermission'
import {
  TOOLS_BUTTONS,
  BACKUPS_TAB,
  HISTORY_TAB,
  OBJECTS_TAB,
  LAYERS_TAB,
  PARAMETERS_TAB,
  BLOCKING_TAB, AREAS_TAB
} from '../constants/index'
import { ACCESS_LEVEL_3 } from '../../../constants/accessLevels'

export class MapEditor extends Component {
  constructor(props) {
    super(props)
    this.adminMapComponent = React.createRef()
    this.refEditorWrap = React.createRef()
    this.state = {
      goToLocation: () => {}
    }
    const { clearLocalStorage, setFilterCondition, initMovement, fetchMapEditorDataWithBlockingCheck } = this.props

    clearLocalStorage()
    fetchMapEditorDataWithBlockingCheck()
    setFilterCondition()
    initMovement()
  }

  componentDidMount() {
    this.observer = new MutationObserver(this.validationFullscreenModeClass)
    const config = {
      attributes: true,
      attributeFilter: ['class'],
      childList: false,
      subtree: false
    }
    const target = this.refEditorWrap.current

    target && this.observer.observe(target, config)
  }

  componentWillUnmount() {
    this.observer.disconnect()
  }

  getSelectedLibrary = () => {
    const {
      library,
      selectLibraryParameter,
      reorderObjects,
      setOrderToLayers,
      unselectObject,
      mapData,
      user
    } = this.props
    const { goToLocation } = this.state

    let result

    switch (library.currentTab) {
      case HISTORY_TAB:
        result = (<History setMapPosition={goToLocation} />)
        break
      case BACKUPS_TAB:
        result = (<VersionSystem />)
        break
      case OBJECTS_TAB:
        result = (
          <ObjectsLibrary adminMapComponent={this.adminMapComponent} />
        )
        break
      case LAYERS_TAB:
        result = (
          <ObjectsLayers
            reorderObjects={reorderObjects}
            setOrderToLayers={setOrderToLayers}
            unselectObject={unselectObject}
          />
        )
        break
      case PARAMETERS_TAB:
        result = (
          <ParametersLibrary
            parameterCategories={library.parameterCategories}
            selectLibraryParameter={selectLibraryParameter}
            userAccessLevel={user.accessLevel}
          />
        )
        break
      case BLOCKING_TAB:
        result = <BlockingLibrary />
        break
      case 'npc':
        result = (
          <NpcLibrary
            libraryNpc={library.libraryNpc}
            mapNpc={mapData.npc}
            selectedNpc={mapData.selectedNpc}
            userAccessLevel={user.accessLevel}
            userRole={user.role}
            availableMapRoles={user.availableMapRoles}
          />
        )
        break
      case AREAS_TAB:
        result = <AreasLibrary fullscreenMode={mapData.fullscreenMode} />
        break
      default:
        break
    }

    return result
  }

  getStateFromAdminMap = value => {
    this.setState({
      goToLocation: value
    })
  }

  setFullscreenTitleLibrary = currentTab => {
    return (
      <div className='title-wrap clearfix'>
        {
          TOOLS_BUTTONS.map(item => {
            if (item.libraryTab === currentTab) {
              return (
                <span>
                  <SVGIconGenerator
                    iconName={item.iconName}
                    preset={{
                      type: 'christmasTown',
                      subtype: 'DEFAULTS_FULLSCREEN'
                    }}
                  />
                  <span>{item.label}</span>
                </span>
              )
            }
            return null
          })
        }
      </div>
    )
  }

  validationFullscreenModeClass = () => {
    const { mapData } = this.props
    const fullscreenClass = 'fullscreen-mode'
    const node = this.refEditorWrap.current

    if (node && !node.classList.contains(fullscreenClass) && mapData.fullscreenMode) {
      node.classList.add(fullscreenClass)
    }
  }

  renderEditor = () => {
    const {
      mapData,
      library,
      setPointerPos,
      addParameterToMap,
      selectParameterOnMap,
      unSelectOnMap,
      removeParameterOnMap,
      toggleCoordinates,
      setCurrentTab,
      addObjectToMap,
      selectLibraryObject,
      selectObjectOnMap,
      selectObjectsOnMap,
      addSelectedObject,
      unselectObject,
      removeObjectOnMap,
      changeObjectPosition,
      setMapPosition,
      toggleFullscreenMode,
      toggleViewLayers,
      setViewToggleLayers,
      updateMap,
      addAreaOnMap,
      setScale,
      overlay,
      checkTabsSync,
      user
    } = this.props
    const selectedLibrary = this.getSelectedLibrary()
    const cnEditorWrap = cn({
      'ct-wrap': true,
      'ct-admin-wrap': true,
      'map-editor': true,
      'fullscreen-mode': mapData.fullscreenMode,
      overlayActive: overlay.active
    })

    return (
      <div>
        <div className={cnEditorWrap} ref={this.refEditorWrap}>
          <div className='map-container'>
            <MapTitle
              mapData={mapData}
              library={library}
              userAccessLevel={user.accessLevel}
              toggleTab={setCurrentTab}
            />
            <div className='ct-map'>
              {mapData.mapEditorDataFetched && (
                <AdminMap
                  mapData={mapData}
                  setPointerPos={setPointerPos}
                  addParameterToMap={addParameterToMap}
                  selectParameterOnMap={selectParameterOnMap}
                  unSelectOnMap={unSelectOnMap}
                  removeParameterOnMap={removeParameterOnMap}
                  toggleCoordinates={toggleCoordinates}
                  addObjectToMap={addObjectToMap}
                  selectLibraryObject={selectLibraryObject}
                  selectObjectOnMap={selectObjectOnMap}
                  selectObjectsOnMap={selectObjectsOnMap}
                  addSelectedObject={addSelectedObject}
                  unselectObject={unselectObject}
                  removeObjectOnMap={removeObjectOnMap}
                  changeObjectPosition={changeObjectPosition}
                  setMapPosition={setMapPosition}
                  toggleFullscreenMode={toggleFullscreenMode}
                  toggleViewLayers={toggleViewLayers}
                  setViewToggleLayers={setViewToggleLayers}
                  updateMap={updateMap}
                  currentTab={library.currentTab}
                  prevTab={library.prevTab}
                  selectedArea={library.areas.selectedArea}
                  addAreaOnMap={addAreaOnMap}
                  setScale={setScale}
                  checkTabsSync={checkTabsSync}
                  updateMapEditorState={this.getStateFromAdminMap}
                  onRef={component => this.adminMapComponent = component}
                />
              )}
            </div>
          </div>
          {(mapData.mapEditorDataFetched || !mapData.fullscreenMode)
          && <div className={`tools-container ${
            mapData.fullscreenMode && library.currentTab === library.prevTab ? ' hide' : ' show'
          }${library.currentTab === 'blocking' ? ' blocking-tools' : ''} ${library.currentTab}`}
          >
            {mapData.fullscreenMode ? this.setFullscreenTitleLibrary(library.currentTab) : <TabsPanel />}
            {mapData.mapEditorDataFetched && selectedLibrary}
             </div>}
          {(mapData.fullscreenMode && mapData.mapEditorDataFetched)
          && <ToolsPanel
            setCurrentTab={setCurrentTab}
            activeTab={library.currentTab}
            user={user}
          />}
          {overlay.active ? <Overlay /> : ''}
        </div>
      </div>
    )
  }

  renderDebugBox = () => {
    const { mapData } = this.props

    return <DebugBox debugMessage={mapData.loadStatus.message} isBeatifyError={true} />
  }

  render() {
    const { user, mapData } = this.props

    if (mapData.loadingInitData) {
      return <Preloader />
    }

    return (mapData.mapId && checkPermission(user.accessLevel, ACCESS_LEVEL_3, user.role, user.availableMapRoles)) ?
      this.renderEditor() :
      this.renderDebugBox()
  }
}

MapEditor.propTypes = {
  fetchMapEditorData: PropTypes.func.isRequired,
  initMovement: PropTypes.func,
  mapData: PropTypes.object.isRequired,
  library: PropTypes.object.isRequired,
  setPointerPos: PropTypes.func.isRequired,
  selectLibraryParameter: PropTypes.func.isRequired,
  selectParameterOnMap: PropTypes.func.isRequired,
  unSelectOnMap: PropTypes.func.isRequired,
  addParameterToMap: PropTypes.func.isRequired,
  removeParameterOnMap: PropTypes.func.isRequired,
  toggleCoordinates: PropTypes.func.isRequired,
  setCurrentTab: PropTypes.func.isRequired,
  setFilterCondition: PropTypes.func.isRequired,
  selectLibraryObject: PropTypes.func.isRequired,
  addObjectToMap: PropTypes.func.isRequired,
  selectObjectOnMap: PropTypes.func.isRequired,
  removeObjectOnMap: PropTypes.func.isRequired,
  changeObjectPosition: PropTypes.func,
  setMapPosition: PropTypes.func.isRequired,
  toggleFullscreenMode: PropTypes.func.isRequired,
  toggleViewLayers: PropTypes.func.isRequired,
  setViewToggleLayers: PropTypes.func.isRequired,
  updateMap: PropTypes.func.isRequired,
  addAreaOnMap: PropTypes.func,
  clearLocalStorage: PropTypes.func,
  setScale: PropTypes.func,
  reorderObjects: PropTypes.func,
  setOrderToLayers: PropTypes.func,
  unselectObject: PropTypes.func,
  selectObjectsOnMap: PropTypes.func,
  addSelectedObject: PropTypes.func,
  overlay: PropTypes.object,
  user: PropTypes.object,
  checkTabsSync: PropTypes.func,
  fetchMapEditorDataWithBlockingCheck: PropTypes.func.isRequired
}

export default MapEditor
