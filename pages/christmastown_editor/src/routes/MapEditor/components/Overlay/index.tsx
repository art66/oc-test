import React from 'react'
import s from './styles.cssmodule.scss'

export const Overlay = () => {
  return (
    <div className={s.overlay}>
      <div className={s.messageWrapper}>
        <span className={s.title}>Processing, please do not refresh the page.</span>
      </div>
    </div>
  )
}

export default Overlay
