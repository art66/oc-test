import React, { Component, Fragment } from 'react'
import IBackup from '../../interfaces/IBackup'
import { LOAD_BACKUP_CONFIRM_TYPE, REMOVE_BACKUP_CONFIRM_TYPE } from '../../constants/backups'
import s from './ConfirmBlock.cssmodule.scss'

interface IProps {
  backup: IBackup
  toggleConfirmation: (ID: string, confirmation: { enabled: boolean; type: string }) => void
  removeBackup: (ID: string) => void
  loadBackup: (ID: string) => void
}

export class ConfirmBlock extends Component<IProps> {
  handleBackupLoading = () => {
    const { loadBackup, backup } = this.props

    loadBackup(backup.backupId)
  }

  getButtons = () => {
    const { backup, toggleConfirmation, removeBackup } = this.props

    if (backup.confirmation.type === LOAD_BACKUP_CONFIRM_TYPE) {
      return (
        <Fragment>
          <button className={s.btn} onClick={this.handleBackupLoading}>Load Backup</button>
          <button
            className={s.btn}
            onClick={() => toggleConfirmation(backup.backupId, { enabled: false, type: LOAD_BACKUP_CONFIRM_TYPE })}
          >
            Cancel
          </button>
        </Fragment>
      )
    }

    if (backup.confirmation.type === REMOVE_BACKUP_CONFIRM_TYPE) {
      return (
        <Fragment>
          <button
            className={s.btn}
            onClick={() => removeBackup(backup.backupId)}
          >
            Yes
          </button>
          <button
            className={s.btn}
            onClick={() => toggleConfirmation(backup.backupId, { enabled: false, type: REMOVE_BACKUP_CONFIRM_TYPE })}
          >
            No
          </button>
        </Fragment>
      )
    }
  }

  getMessage = () => {
    const { backup } = this.props

    if (backup.confirmation.type === LOAD_BACKUP_CONFIRM_TYPE) {
      return (
        <Fragment>
          <p>Are you sure you want to load this backup?</p>
          <p>Any unsaved progress will be lost.</p>
        </Fragment>
      )
    }

    if (backup.confirmation.type === REMOVE_BACKUP_CONFIRM_TYPE) {
      return (
        <Fragment>
          <p>Are you sure you would like to delete this backup?</p>
        </Fragment>
      )
    }
  }

  render() {
    return (
      <div className={s.confirmation}>
        <div className={s.message}>{this.getMessage()}</div>
        <div className={s.buttons}>{this.getButtons()}</div>
      </div>
    )
  }
}

export default ConfirmBlock
