import React, { Component, RefObject } from 'react'
import { connect } from 'react-redux'
import Controls from './Controls'
import Backup from './Backup'
import IBackup from '../../interfaces/IBackup'
import { fetchBackupsData } from '../../modules/backups'

import s from './style.cssmodule.scss'

interface IProps {
  fetchBackupsData: () => void
  backups: {
    backupsList: IBackup[]
  }
  isFullscreenMode: boolean
}

export class VersionSystem extends Component<IProps> {
  backupsRef: RefObject<HTMLUListElement>
  constructor(props) {
    super(props)
    this.backupsRef = React.createRef()
  }

  componentDidMount() {
    this.props.fetchBackupsData()
  }

  getBackupsList = () => {
    const { backups } = this.props

    return (backups.backupsList || []).map(item => {
      return <Backup key={item.backupId} backup={item} listOffset={this.backupsRef?.current?.offsetTop || 0} />
    })
  }

  render() {
    return (
      <div className='backups-container'>
        <Controls />
        <div className='scrollarea scroll-area scrollbar-thin'>
          <div className='scrollarea-content content'>
            <ul ref={this.backupsRef} className={s.backupsList}>
              {this.getBackupsList()}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  backups: state.mapeditor.backups,
  isFullscreenMode: state.mapeditor.mapData.fullscreenMode
})

const mapDispatchToProps = {
  fetchBackupsData
}

export default connect(mapStateToProps, mapDispatchToProps)(VersionSystem)
