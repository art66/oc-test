import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  SAVING,
  DEFAULT,
  EXISTED_NAME_ERROR,
  EMPTY_NAME_ERROR,
  MIN_BACKUP_NAME_LENGTH,
  MAX_BACKUP_NAME_LENGTH,
  LENGTH_NAME_ERROR
} from '../../constants/backups'
import { changeControlsStatus, addBackup, changeControlsState } from '../../modules/backups'
import cn from 'classnames'
import IBackup from '../../interfaces/IBackup'
import { stringLengthIsInInterval } from '../../utils'
import s from './Controls.cssmodule.scss'

interface IProps {
  changeControlsStatus: (status: string) => void
  addBackup: (name: string) => void
  backups: {
    controlsStatus: string
    controls: {
      canSave: boolean
      message: string
    }
    backupsList: IBackup[]
  },
  changeControlsState: (canSave: boolean, message?: string) => void
}

class Controls extends Component<IProps> {
  nameInput: HTMLInputElement
  constructor(props: IProps) {
    super(props)
  }

  _handleSaveBackup = () => {
    const { addBackup, backups, changeControlsState } = this.props
    const name = this.nameInput.value.trim()
    const isPresent = backups.backupsList.some(backup => backup.name === name)
    const isValidLength = stringLengthIsInInterval(name, MIN_BACKUP_NAME_LENGTH, MAX_BACKUP_NAME_LENGTH)

    if (!isPresent && name && isValidLength) {
      addBackup(name)
    }

    if (isPresent) {
      changeControlsState(false, EXISTED_NAME_ERROR)
    }
    if (!name) {
      changeControlsState(false, EMPTY_NAME_ERROR)
    }
    if (!isValidLength) {
      changeControlsState(false, LENGTH_NAME_ERROR)
    }
  }

  _handleCancelSavingBackup = () => {
    const { changeControlsStatus } = this.props

    this.enableSaving()
    changeControlsStatus(DEFAULT)
  }

  enableSaving = () => {
    const { changeControlsState } = this.props

    changeControlsState(true)
  }

  getErrorTooltip = () => {
    const { backups } = this.props

    return !backups.controls.canSave ? (
      <div className={s.errorTooltip}>
        <span>{backups.controls.message}</span>
      </div>
    ) : (
      ''
    )
  }

  getControls = controlsStatus => {
    const { changeControlsStatus, backups } = this.props

    if (controlsStatus === SAVING) {
      return (
        <div className={s.savingBackup}>
          {this.getErrorTooltip()}
          <input
            placeholder='Name version'
            className={cn(s.backupName, { [s.error]: !backups.controls.canSave })}
            type='text'
            ref={el => (this.nameInput = el)}
            onClick={this.enableSaving}
          />
          <button
            className={`btn-blue ${cn(s.btn, s.saveBtn)} ${!backups.controls.canSave ? 'locked' : ''}`}
            onClick={this._handleSaveBackup}
          >
            Save
          </button>
          <button className={cn(s.btn, s.cancelBtn)} onClick={this._handleCancelSavingBackup}>
            Cancel
          </button>
        </div>
      )
    }

    return (
      <div className={s.creatingBackup}>
        <div className={s.description}>You can create backups and return to them at any time.</div>
        <div className={s.buttonWrapper}>
          <button className={`btn-blue ${s.button}`} onClick={() => changeControlsStatus(SAVING)}>
            CREATE BACKUP
          </button>
        </div>
      </div>
    )
  }

  render() {
    const { backups } = this.props

    return <div className={s.controlsWrapper}>{this.getControls(backups.controlsStatus)}</div>
  }
}

const mapStateToProps = state => ({
  backups: state.mapeditor.backups
})

const mapDispatchToProps = {
  changeControlsStatus,
  addBackup,
  changeControlsState
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Controls)
