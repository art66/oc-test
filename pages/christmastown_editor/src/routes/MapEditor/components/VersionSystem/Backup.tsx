import React, { Component } from 'react'
import cn from 'classnames'
import { connect } from 'react-redux'
import IBackup from '../../interfaces/IBackup'
import { getDate, getFormattedDate, stringLengthIsInInterval } from '../../utils'
import {
  editBackupName,
  renameBackup,
  removeBackup,
  toggleConfirmation,
  isWrongName,
  loadBackup
} from '../../modules/backups'
import {
  LOAD_BACKUP_CONFIRM_TYPE,
  REMOVE_BACKUP_CONFIRM_TYPE,
  EXISTED_NAME_ERROR,
  EMPTY_NAME_ERROR,
  LENGTH_NAME_ERROR,
  MIN_BACKUP_NAME_LENGTH,
  MAX_BACKUP_NAME_LENGTH
} from '../../constants/backups'
import ConfirmBlock from './ConfirmBlock'
import ErrorTooltip from './ErrorTooltip'
import s from './style.cssmodule.scss'

interface IProps {
  backup: IBackup
  backups: IBackup[]
  listOffset: number
  editBackupName: (ID: string) => void
  renameBackup: (ID: string, name: string) => void
  removeBackup: (ID: string) => void
  toggleConfirmation: (ID: string, confirmation: { enabled: boolean; type: string }) => void
  isWrongName: (ID: string, wrongName: boolean, message: string) => void
  loadBackup: (ID: string) => void
}

export class Backup extends Component<IProps> {
  backupNameEl: HTMLElement

  componentDidUpdate(prevProps: IProps) {
    const { backup } = this.props

    if (backup.editing && backup.editing !== prevProps.backup.editing) {
      this[`nameInput${backup.backupId}`].focus()
    }
  }

  checkNameUniqueness = name => {
    const { backups, backup } = this.props

    return backups.every(item => item.name !== name || backup.backupId === item.backupId)
  }

  isValidName = (name: string) => {
    const isValidLength = stringLengthIsInInterval(name, MIN_BACKUP_NAME_LENGTH, MAX_BACKUP_NAME_LENGTH)

    return this.checkNameUniqueness(name) && name && isValidLength
  }

  handleChangeName = () => {
    const { backup } = this.props
    const newName = this[`nameInput${backup.backupId}`].value.trim()
    const isUniqName = this.checkNameUniqueness(newName)
    const isValidLength = stringLengthIsInInterval(newName, MIN_BACKUP_NAME_LENGTH, MAX_BACKUP_NAME_LENGTH)

    this.props.isWrongName(backup.backupId, !isUniqName, EXISTED_NAME_ERROR)

    if (this.isValidName(newName)) {
      this.props.renameBackup(backup.backupId, newName)
      return
    }
    if (!newName) {
      this.props.isWrongName(backup.backupId, true, EMPTY_NAME_ERROR)
    }
    if (!isValidLength) {
      this.props.isWrongName(backup.backupId, true, LENGTH_NAME_ERROR)
    }
  }

  handleEnablingEditMode = () => this.props.editBackupName(this.props.backup.backupId)

  getErrorTooltip = () => {
    const { backup, listOffset } = this.props

    if (backup.wrongName && this.backupNameEl) {
      return <ErrorTooltip message={backup.message} topOffset={this.backupNameEl.offsetTop - listOffset} />
    }
  }

  getBackupName = () => {
    const { backup } = this.props

    if (backup.editing) {
      return (
        <div ref={el => (this.backupNameEl = el)} className={cn(s.col, s.nameCol)}>
          {this.getErrorTooltip()}
          <input
            type='text'
            className={s.nameInput}
            defaultValue={backup.name}
            ref={el => (this[`nameInput${backup.backupId}`] = el)}
            onFocus={() => this.props.isWrongName(backup.backupId, false, '')}
          />
          <button
            type='button'
            className={cn(s.btn, s.saveNameBtn)}
            onClick={this.handleChangeName}
            aria-label='Save'
          />
        </div>
      )
    }

    return (
      <div className={cn(s.col, s.nameCol)}>
        <span className={s.name}>{backup.name}</span>
        <button
          type='button'
          className={cn(s.btn, s.editNameBtn)}
          onClick={this.handleEnablingEditMode}
          aria-label='Edit'
        />
      </div>
    )
  }

  getConfirmation = () => {
    const { backup } = this.props

    if (backup.confirmation.enabled) {
      return (
        <ConfirmBlock
          backup={backup}
          toggleConfirmation={this.props.toggleConfirmation}
          removeBackup={this.props.removeBackup}
          loadBackup={this.props.loadBackup}
        />
      )
    }
  }

  handleToggleConfirmation = (enabled, type) => {
    const { backup } = this.props

    this.props.toggleConfirmation(backup.backupId, { enabled, type })
  }

  confirmationEnabled = type => {
    const { backup } = this.props

    return backup.confirmation.enabled && backup.confirmation.type === type
  }

  render() {
    const { backup } = this.props
    const date = getFormattedDate(getDate(backup.timestamp))
    const removeConfirmationEnabled = this.confirmationEnabled(REMOVE_BACKUP_CONFIRM_TYPE)
    const loadConfirmationEnabled = this.confirmationEnabled(LOAD_BACKUP_CONFIRM_TYPE)

    return (
      <li>
        <div className={cn(s.backup, { [s.active]: backup.active, [s.wrongName]: backup.wrongName })}>
          {this.getBackupName()}
          <div className={cn(s.col, s.deleteCol)}>
            <button
              aria-label='Remove'
              type='button'
              className={cn(s.btn, s.deleteBackupBtn)}
              onClick={() => this.handleToggleConfirmation(!removeConfirmationEnabled, REMOVE_BACKUP_CONFIRM_TYPE)}
            />
          </div>
          <div className={cn(s.col, s.dateCol)}>
            <span className={s.date}>{date}</span>
          </div>
          <div className={cn(s.col, s.loadCol)}>
            <button
              type='button'
              className={cn(s.btn, s.loadBackupBtn)}
              onClick={() => this.handleToggleConfirmation(!loadConfirmationEnabled, LOAD_BACKUP_CONFIRM_TYPE)}
            >
              Load
            </button>
          </div>
        </div>
        {this.getConfirmation()}
      </li>
    )
  }
}

const mapStateToProps = state => ({
  backups: state.mapeditor.backups.backupsList
})

const mapDispatchToProps = {
  editBackupName,
  renameBackup,
  removeBackup,
  toggleConfirmation,
  isWrongName,
  loadBackup
}

export default connect(mapStateToProps, mapDispatchToProps)(Backup)
