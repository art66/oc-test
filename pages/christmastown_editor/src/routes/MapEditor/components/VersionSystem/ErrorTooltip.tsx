import React, { Component } from 'react'
import cn from 'classnames'
import s from './style.cssmodule.scss'

interface IProps {
  message: string
  topOffset: number
}

interface IState {
  bottom: boolean
}

class ErrorTooltip extends Component<IProps, IState> {
  errorTooltip: HTMLElement
  constructor(props: IProps) {
    super(props)
    this.state = {
      bottom: false
    }
  }

  componentDidMount() {
    const { topOffset } = this.props
    const minTopOffset = Math.abs(this.errorTooltip.offsetTop)

    if (topOffset < minTopOffset) {
      this.setState({
        bottom: true
      })
    }
  }

  render() {
    const { message } = this.props
    const { bottom } = this.state

    return (
      <div ref={el => (this.errorTooltip = el)} className={cn(s.errorTooltip, { [s.bottom]: bottom })}>
        <span>{message}</span>
      </div>
    )
  }
}

export default ErrorTooltip
