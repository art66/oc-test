import React, { Component } from 'react'
import IToolsPanelItem from '../../interfaces/IToolsPanelItem'
import { SVGIconGenerator } from '@torn/shared/SVG'
import Tooltip from '@torn/shared/components/Tooltip'
import s from './styles.cssmodule.scss'

interface IProps {
  item: IToolsPanelItem
  setCurrentTab:(x: string) => {}
  activeTab: string
}

export class ToolsPanelItem extends Component<IProps> {
  setTab = () => {
    const { item, setCurrentTab } = this.props
    setCurrentTab(item.libraryTab)
  }

  render() {
    const { item, activeTab } = this.props
    const preset = {
      type: 'christmasTown',
      subtype: item.customPreset || 'DEFAULTS_BLUE'
    }

    return (
      <button
        id={item.libraryTab}
        className={`${s.tool} ${activeTab === item.libraryTab ? s.active : ''}`}
        type='button'
        onClick={this.setTab}
      >
        <div className={s.iconWrapper}>
          <SVGIconGenerator
            iconName={item.iconName}
            preset={preset}
          />
        </div>
        <Tooltip
          parent={item.libraryTab}
          arrow='center'
          position='left'
        >
          <span className='tipContent'>{item.label}</span>
        </Tooltip>
      </button>
    )
  }
}

export default ToolsPanelItem
