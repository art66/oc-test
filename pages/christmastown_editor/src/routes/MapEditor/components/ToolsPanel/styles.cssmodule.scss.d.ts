// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'fullscreenTool': string;
  'fullscreenTools': string;
  'globalSvgShadow': string;
  'iconWrapper': string;
  'tool': string;
}
export const cssExports: CssExports;
export default cssExports;
