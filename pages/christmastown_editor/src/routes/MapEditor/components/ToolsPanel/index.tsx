import React, { Component } from 'react'
import ToolsPanelItem from './ToolsPanelItem'
import { TOOLS_BUTTONS, BACKUPS_TAB } from '../../constants'
import checkPermission from '../../../../utils/checkPermission'
import { ACCESS_LEVEL_2 } from '../../../../constants/accessLevels'
import s from './styles.cssmodule.scss'

interface IProps {
  setCurrentTab:(x: string) => {}
  activeTab: string
  user: {
    role: string,
    accessLevel: number,
    availableMapRoles: string[]
  }
}

export class ToolsPanel extends Component<IProps> {
  componentDidMount() {
    const html = document.documentElement
    html.classList.add('hide-scroll')
  }

  componentWillUnmount() {
    const html = document.documentElement
    html.classList.remove('hide-scroll')
  }

  getButtons = () => {
    const { user } = this.props

    if (checkPermission(user.accessLevel, ACCESS_LEVEL_2, user.role, user.availableMapRoles)) {
      return TOOLS_BUTTONS
    }

    return TOOLS_BUTTONS.filter(item => item.libraryTab !== BACKUPS_TAB)
  }

  renderButtons = () => {
    const { setCurrentTab, activeTab } = this.props
    const toolsButtons = this.getButtons()

    return toolsButtons.map(item => {
      return (
        <li key={item.iconName} id={item.libraryTab} className={s.fullscreenTool}>
          <ToolsPanelItem item={item} setCurrentTab={setCurrentTab} activeTab={activeTab} />
        </li>
      )
    })
  }

  render() {
    return (
      <ul className={s.fullscreenTools}>
        {this.renderButtons()}
      </ul>
    )
  }
}

export default ToolsPanel
