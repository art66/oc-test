import React, { Component } from 'react'
import { connect } from 'react-redux'
import Draggable from 'react-draggable'
import orderBy from 'lodash.orderby'
import IMapObject from '../../interfaces/IMapObject'
import { CELL_SIZE } from '../../../../constants'
import { findSideObjectInGroup } from '../../utils'
import {
  moveObjectsAndSave,
  showMapObjectContextMenu,
  hideMapObjectContextMenu,
  removeObjectsOnMapAndSave,
  unselectObject,
  selectObjectOnMap
} from '../../modules/mapeditor'
import { positionsEqual } from '../../../../utils'
import IPosition from '../../interfaces/IPosition'
import SelectedObject from './SelectedObject'
import ContextMenu from './ContextMenu'
import s from './styles.cssmodule.scss'

interface IMoveObjectPayload {
  hash: string
  position: IPosition
  oldPosition: IPosition
}

interface IProps {
  selectedObjects: IMapObject[]
  objectsOnMap: IMapObject[]
  contextMenuObject?: IMapObject
  getCellPositionByClick: (e: React.MouseEvent<Element>) => IPosition
  libraryObjects: Object
  removeObjectsOnMapAndSaveAction: (objects: IMapObject[]) => void
  moveObjectsAndSaveAction: (moveObjects: IMoveObjectPayload[], selectedObjects: any) => void
  showMapObjectContextMenuAction: (object: IMapObject) => void
  hideMapObjectContextMenuAction: () => void
  unselectObjectAction: (object: IMapObject) => void
  selectObjectOnMapAction: (object: IMapObject) => void
}

class SelectedGroupRectangle extends Component<IProps> {
  position: IPosition
  width: number
  height: number
  mouseDownCellPosition: IPosition

  constructor(props: IProps) {
    super(props)
    this.setPosition()
  }

  getObjectsOnCell = cellPosition => {
    const { objectsOnMap, libraryObjects } = this.props

    return objectsOnMap.filter(obj => {
      const rightCellPositionX = obj.position.x + libraryObjects[obj.object_id].width / CELL_SIZE
      const bottomCellPositionY = obj.position.y - libraryObjects[obj.object_id].height / CELL_SIZE

      for (let { x } = obj.position; x < rightCellPositionX; x++) {
        for (let y = bottomCellPositionY; y < obj.position.y; y++) {
          const position = { x, y: y + 1 }

          // eslint-disable-next-line max-depth
          if (positionsEqual(position, cellPosition)) {
            return true
          }
        }
      }

      return false
    })
  }

  getObjects = () => {
    const { libraryObjects, selectedObjects } = this.props
    const selectedObjectsFullInfo = orderBy(
      selectedObjects.map(item => ({ ...libraryObjects[item.object_id], ...item })),
      ['order'],
      ['asc']
    )
    const leftObject = findSideObjectInGroup(selectedObjectsFullInfo, 'left')
    const rightObject = findSideObjectInGroup(selectedObjectsFullInfo, 'right')
    const topObject = findSideObjectInGroup(selectedObjectsFullInfo, 'top')
    const bottomObject = findSideObjectInGroup(selectedObjectsFullInfo, 'bottom')

    return {
      leftObject,
      rightObject,
      topObject,
      bottomObject
    }
  }

  setPosition = () => {
    const { selectedObjects, libraryObjects } = this.props
    const selectedObjectsFullInfo = selectedObjects.map(item => ({ ...libraryObjects[item.object_id], ...item }))
    const leftObject = findSideObjectInGroup(selectedObjectsFullInfo, 'left')
    const topObject = findSideObjectInGroup(selectedObjectsFullInfo, 'top')

    if (!leftObject || !topObject) {
      this.position = { x: 0, y: 0 }
    } else {
      this.position = { x: leftObject.position.x, y: topObject.position.y }
    }
  }

  _handleDragStart = () => {
    this.setPosition()
  }

  _handleDragStop = (_, eventObject) => {
    const { selectedObjects, moveObjectsAndSaveAction } = this.props
    const newPos = {
      x: Math.round(eventObject.x / CELL_SIZE),
      y: Math.round(eventObject.y / -CELL_SIZE)
    }

    if (this.position.x === newPos.x && this.position.y === newPos.y) {
      return
    }

    const moveObjects = selectedObjects.map(obj => ({
      hash: obj.hash,
      position: {
        x: newPos.x + (obj.position.x - this.position.x),
        y: newPos.y - (this.position.y - obj.position.y)
      },
      oldPosition: obj.position
    }))

    moveObjectsAndSaveAction(moveObjects, selectedObjects)
    this.setPosition()
  }

  _handleDeleteObjectsClick = () => {
    const { removeObjectsOnMapAndSaveAction, hideMapObjectContextMenuAction, selectedObjects } = this.props

    removeObjectsOnMapAndSaveAction(selectedObjects)
    hideMapObjectContextMenuAction()
  }

  _handleContextMenu = e => {
    e.preventDefault()
    const {
      contextMenuObject,
      hideMapObjectContextMenuAction,
      showMapObjectContextMenuAction,
      selectedObjects
    } = this.props

    Object.keys(contextMenuObject).length ?
      hideMapObjectContextMenuAction() :
      showMapObjectContextMenuAction(selectedObjects[0])
  }

  _handleObjectMouseDown = (e: React.MouseEvent<Element>) => {
    const { getCellPositionByClick } = this.props

    this.mouseDownCellPosition = getCellPositionByClick(e)
  }

  _handleObjectMouseUp = (e: React.MouseEvent<Element>, object: IMapObject) => {
    const {
      unselectObjectAction,
      getCellPositionByClick,
      selectObjectOnMapAction,
      hideMapObjectContextMenuAction
    } = this.props

    if (e.ctrlKey || e.metaKey) {
      return unselectObjectAction(object)
    }

    if (e.button !== 0) return null

    const cellPosition = getCellPositionByClick(e)

    if (!positionsEqual(this.mouseDownCellPosition, cellPosition)) {
      return null
    }

    const objectsOnCell = this.getObjectsOnCell(cellPosition)
    const currObjIndex = objectsOnCell.findIndex(obj => obj.hash === object.hash)
    const nextObject = objectsOnCell.length > currObjIndex + 1 ? objectsOnCell[currObjIndex + 1] : objectsOnCell[0]

    if (!nextObject) {
      return null
    }

    selectObjectOnMapAction(nextObject)
    return hideMapObjectContextMenuAction()
  }

  renderContextMenu = (marginLeft: number) => {
    const { selectedObjects, contextMenuObject } = this.props

    if (!contextMenuObject || selectedObjects[0].hash !== contextMenuObject.hash || selectedObjects.length > 1) {
      return ''
    }

    return <ContextMenu marginLeft={marginLeft} />
  }

  renderSelectedObjects = () => {
    const { libraryObjects, selectedObjects } = this.props
    const selectedObjectsFullInfo = orderBy(
      selectedObjects.map(item => ({ ...libraryObjects[item.object_id], ...item })),
      ['order'],
      ['asc']
    )

    return selectedObjectsFullInfo.map(obj => (
      <SelectedObject
        key={`selectedObject-${obj.hash || obj.object_id}`}
        object={obj}
        onObjectMouseDown={this._handleObjectMouseDown}
        onObjectMouseUp={this._handleObjectMouseUp}
      />
    ))
  }

  render() {
    const { selectedObjects } = this.props
    const { leftObject, rightObject, topObject, bottomObject } = this.getObjects()

    if (!selectedObjects.length || !selectedObjects[0].hash) {
      return <div />
    }

    const width = (rightObject.position.x - leftObject.position.x) * CELL_SIZE + rightObject.width
    const height = (topObject.position.y - bottomObject.position.y) * CELL_SIZE + bottomObject.height
    const styles = { height, width }
    const contextMenu = this.renderContextMenu(width / 2)
    const position = {
      x: leftObject.position.x * CELL_SIZE,
      y: topObject.position.y * -CELL_SIZE
    }

    this.width = width
    this.height = height

    return (
      <Draggable
        grid={[CELL_SIZE, CELL_SIZE]}
        onStart={this._handleDragStart}
        onStop={this._handleDragStop}
        disabled={false}
        position={position}
      >
        <div className={s.selectedGroupRectangle} style={styles} onContextMenu={this._handleContextMenu}>
          <i
            aria-label='button'
            role='button'
            tabIndex={0}
            className={s.delete}
            onMouseDown={this._handleDeleteObjectsClick}
          />
          {this.renderSelectedObjects()}
          {contextMenu}
        </div>
      </Draggable>
    )
  }
}

export default connect(
  (state: any) => ({
    selectedObjects: state.mapeditor.mapData.selectedObjects,
    objectsOnMap: state.mapeditor.mapData.objects,
    contextMenuObject: state.mapeditor.mapData.contextMenuObject,
    libraryObjects: state.mapeditor.library.objects
  }),
  {
    removeObjectsOnMapAndSaveAction: removeObjectsOnMapAndSave,
    moveObjectsAndSaveAction: moveObjectsAndSave,
    showMapObjectContextMenuAction: showMapObjectContextMenu,
    hideMapObjectContextMenuAction: hideMapObjectContextMenu,
    unselectObjectAction: unselectObject,
    selectObjectOnMapAction: selectObjectOnMap
  }
)(SelectedGroupRectangle)
