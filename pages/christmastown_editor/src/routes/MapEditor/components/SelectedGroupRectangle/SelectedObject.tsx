import React, { Component } from 'react'
import { connect } from 'react-redux'
import orderBy from 'lodash.orderby'
import IMapObject from '../../interfaces/IMapObject'
import { getObjectImagePath } from '../../../../utils'
import { CELL_SIZE } from '../../../../constants'
import { findSideObjectInGroup } from '../../utils'

interface IProps {
  object: IMapObject,
  selectedObjects: IMapObject[],
  libraryObjects: Object,
  onObjectMouseDown: (e: React.MouseEvent<Element>) => void,
  onObjectMouseUp: (e: React.MouseEvent<Element>, object: IMapObject) => void
}

class SelectedObject extends Component<IProps> {
  _handleMouseUp = (e: React.MouseEvent<Element>) => {
    const { onObjectMouseUp, object } = this.props

    onObjectMouseUp(e, object)
  }

  getLeftTopPosition = () => {
    const { selectedObjects, libraryObjects } = this.props
    const selectedObjectsFullInfo = orderBy(
      selectedObjects.map(item => ({ ...libraryObjects[item.object_id], ...item })), ['order'], ['asc']
    )
    const leftObject = findSideObjectInGroup(selectedObjectsFullInfo, 'left')
    const topObject = findSideObjectInGroup(selectedObjectsFullInfo, 'top')
    const { x } = leftObject.position
    const { y } = topObject.position

    return { x, y }
  }

  render() {
    const { object, onObjectMouseDown } = this.props
    const leftTopPosition = this.getLeftTopPosition()
    const style = {
      left: (object.position.x - leftTopPosition.x) * CELL_SIZE,
      top: (leftTopPosition.y - object.position.y) * CELL_SIZE,
      position: 'absolute',
      display: 'block'
    } as React.CSSProperties

    return (
      <div
        onMouseDown={onObjectMouseDown}
        onMouseUp={this._handleMouseUp}
        className='ct-object'
        style={style}
      >
        <img src={getObjectImagePath(object)} alt='' />
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    selectedObjects: state.mapeditor.mapData.selectedObjects,
    libraryObjects: state.mapeditor.library.objects
  }),
  {}
)(SelectedObject)
