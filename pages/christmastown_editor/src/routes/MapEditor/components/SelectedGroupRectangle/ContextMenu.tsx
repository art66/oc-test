import React, { Component } from 'react'
import { connect } from 'react-redux'
import { SVGIconGenerator } from '@torn/shared/SVG'
import cn from 'classnames'
import IMapObject from '../../interfaces/IMapObject'
import {
  selectLibraryObject,
  hideMapObjectContextMenu,
  putObjectsIntoBuffer,
  removeObjectsOnMapAndSave
} from '../../modules/mapeditor'
import { showLibraryObjectPopup } from '../../modules/library'
import { changeObjectLayer } from '../../modules/layers'
import { LAYERS_NAMES_ARR } from '../../constants/layers'
import { capitalizeString } from '../../utils'
import * as c from '../../constants'
import s from './styles.cssmodule.scss'
import icons from '../../../../styles/icons.cssmodule.scss'

interface IProps {
  marginLeft: number,
  selectedObjects: IMapObject[],
  libraryObjects: Object,
  selectLibraryObject: (object: IMapObject) => void,
  showLibraryObjectPopup: (object: IMapObject) => void,
  hideMapObjectContextMenu: () => void,
  changeObjectLayer: (object: IMapObject, layerName: string) => void,
  putObjectsIntoBuffer: (action: string, objects: IMapObject[]) => void,
  removeObjectsOnMapAndSave: (objects: IMapObject[]) => void
}

class ContextMenu extends Component<IProps> {
  _handleShowObjectInLibraryClick = e => {
    e.stopPropagation()
    const { selectedObjects, selectLibraryObject, showLibraryObjectPopup, hideMapObjectContextMenu } = this.props
    const object = selectedObjects[0]

    selectLibraryObject({ ...object, hash: null })
    showLibraryObjectPopup(object)
    hideMapObjectContextMenu()
  }

  _handleChangeLayer = layerName => {
    const { selectedObjects, changeObjectLayer } = this.props

    if (selectedObjects[0].layer.name === layerName) {
      return
    }

    changeObjectLayer(selectedObjects[0], layerName)
  }

  _handleCopyObjectsClick = () => {
    const { putObjectsIntoBuffer, hideMapObjectContextMenu, selectedObjects } = this.props

    putObjectsIntoBuffer(c.COPY, selectedObjects)
    hideMapObjectContextMenu()
  }

  _handleCutObjectsClick = () => {
    const { putObjectsIntoBuffer, hideMapObjectContextMenu, selectedObjects } = this.props

    putObjectsIntoBuffer(c.CUT, selectedObjects)
    hideMapObjectContextMenu()
  }

  _handleDeleteObjectsClick = () => {
    const { removeObjectsOnMapAndSave, hideMapObjectContextMenu, selectedObjects } = this.props

    removeObjectsOnMapAndSave(selectedObjects)
    hideMapObjectContextMenu()
  }

  renderShowInLibraryContextMenuSection = () => {
    const { selectedObjects, libraryObjects } = this.props

    return selectedObjects.length === 1 ?
      (
        <li onClick={this._handleShowObjectInLibraryClick}>
          <span className={s.black}>Show in library:</span> {libraryObjects[selectedObjects[0].object_id].category}
        </li>
      ) : ''
  }

  renderLayersButtons = () => {
    const { selectedObjects } = this.props

    return LAYERS_NAMES_ARR.map(name => {
      const capitalizedName = capitalizeString(name)

      return (
        <button
          key={name}
          type='button'
          className={cn(s.layerButton, { [s.active]: selectedObjects[0].layer.name === name })}
          onClick={() => this._handleChangeLayer(name)}
        >
          {capitalizedName}
        </button>
      )
    })
  }

  render() {
    const { marginLeft } = this.props

    return (
      <div className={s.popup} style={{ marginLeft }}>
        <ul className={s.list}>
          {this.renderShowInLibraryContextMenuSection()}
          <li className={s.noHover}>
            <div className={s.defaultLayerTitle}>
              <div className={s.left}>
                <span className={s.layersIconWrapper}>
                  <SVGIconGenerator
                    iconName='CTLayers'
                    preset={{ type: 'christmasTown', subtype: 'DEFAULTS' }}
                  />
                </span>
                <span className={s.title}>Layer:</span>
              </div>
              <div className={s.right}>
                {this.renderLayersButtons()}
              </div>
            </div>
          </li>
          <li onClick={this._handleCopyObjectsClick}>
            <i className={icons.copy} />Copy <span className={s.tip}>Ctrl+C</span>
          </li>
          <li onClick={this._handleCutObjectsClick}>
            <i className={icons.cut} />Cut <span className={s.tip}>Ctrl+X</span>
          </li>
          <li onClick={this._handleDeleteObjectsClick}>
            <i className={icons.cross} />Delete <span className={s.tip}>Delete</span>
          </li>
        </ul>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    selectedObjects: state.mapeditor.mapData.selectedObjects,
    libraryObjects: state.mapeditor.library.objects
  }),
  {
    selectLibraryObject,
    showLibraryObjectPopup,
    hideMapObjectContextMenu,
    changeObjectLayer,
    putObjectsIntoBuffer,
    removeObjectsOnMapAndSave
  }
)(ContextMenu)
