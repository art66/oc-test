import React, { Component } from 'react'

interface IProps {
  mapData: {
    loading: boolean,
    loadStatus: any
  }
  styleClass: string
}

export class LoaderStatus extends Component<IProps> {
  render() {
    const { mapData, styleClass } = this.props as IProps

    return (
      <span className={styleClass}>
        {mapData.loading ? <i className='saving-icon' /> : mapData.loadStatus || 'All changes saved'}
      </span>
    )
  }
}

export default LoaderStatus
