import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { BLUE_FILL, DISABLE_FILL } from '@torn/shared/SVG/presets/icons/christmastown'
import { isInt } from '../../../../utils'
import s from './MapControls.cssmodule.scss'

interface IProps {
  mapData: {
    fullscreenMode: boolean
  },
  setMapPosition: (x: string, y: string) => void
}

interface IState {
  coordinatesInputX: string,
  coordinatesInputY: string
}

class Coords extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      coordinatesInputX: '',
      coordinatesInputY: ''
    }
  }

  isValidCoords = () => {
    const { coordinatesInputX, coordinatesInputY } = this.state

    return isInt(coordinatesInputX) && isInt(coordinatesInputY)
  }

  _handleCoordinatesInputChange = e => {
    const newInputValue = e.target.value
    const { state } = this
    const prevInputValue = state[e.target.name]
    const containOnlyNums = /^-?\d*$/.test(newInputValue)

    this.setState({
      [e.target.name]: newInputValue.length && !containOnlyNums ? prevInputValue : newInputValue
    } as IState)
  }

  _handleChangingMapPosition = () => {
    const { setMapPosition } = this.props
    const { coordinatesInputX, coordinatesInputY } = this.state
    const isValidCoords = this.isValidCoords()

    if (isValidCoords) {
      setMapPosition(coordinatesInputX, coordinatesInputY)
    }
  }

  _handleKeyPressInCoordsInputs = ({ key }) => {
    if (key === 'Enter') {
      this._handleChangingMapPosition()
    }
  }

  render() {
    const { mapData } = this.props
    const { coordinatesInputX, coordinatesInputY } = this.state
    const isValidCoords = this.isValidCoords()

    return (
      <>
        <label className='coordinates-input-label' htmlFor='coordinatesInputX'>
          x:
        </label>
        <input
          id='coordinatesInputX'
          type='text'
          className='coordinates-input'
          name='coordinatesInputX'
          autoComplete='off'
          onChange={this._handleCoordinatesInputChange}
          onKeyPress={this._handleKeyPressInCoordsInputs}
          value={coordinatesInputX}
        />
        <label className='coordinates-input-label' htmlFor='coordinatesInputY'>
          y:
        </label>
        <input
          id='coordinatesInputY'
          type='text'
          className='coordinates-input'
          name='coordinatesInputY'
          autoComplete='off'
          onChange={this._handleCoordinatesInputChange}
          onKeyPress={this._handleKeyPressInCoordsInputs}
          value={coordinatesInputY}
        />
        <button
          type='button'
          id='goToCoords'
          className={cn('search-icon', s.searchBtn, { [s.disabled]: !isValidCoords })}
          onClick={this._handleChangingMapPosition}
        >
          <SVGIconGenerator
            iconName='Search'
            preset={{ type: 'christmasTown', subtype: 'SEARCH' }}
            fill={isValidCoords ? BLUE_FILL : DISABLE_FILL}
          />
          <Tooltip
            parent='goToCoords'
            arrow='center'
            position={mapData.fullscreenMode ? 'bottom' : 'top'}
          >
            <span className='tipContent'>Go to location</span>
          </Tooltip>
        </button>
      </>
    )
  }
}

export default connect(
  (state: any) => ({
    mapData: state.mapeditor.mapData
  }),
  {}
)(Coords)
