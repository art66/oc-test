import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getCookie } from '@torn/shared/utils'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { redoAction, undoAction } from '../../modules/history'
import { toggleCoordinates, toggleFullscreenMode } from '../../modules/mapeditor'
import IUserAction from '../../interfaces/IUserAction'
import { REDO_ACTION, UNDO_ACTION } from '../../constants/history'
import Coords from './Coords'
import { BLUE_FILL, DISABLE_FILL } from '../../../../../../../shared/SVG/presets/icons/christmastown'
import ViewToggle from './ViewToggle'
import IToggleLayer from '../../interfaces/IToggleLayer'
import { MAX_LENGTH_INFO_MSG_STATUS_PANEL } from '../../constants/index'
import s from './MapControls.cssmodule.scss'

interface IProps {
  history: {
    actions: IUserAction[]
  }
  infoText: string
  mapData: {
    loading: boolean
    fullscreenMode: boolean
    showCoordinates: boolean
    viewToggle: boolean
    viewToggleLayers: { [name: string]: IToggleLayer }
  }
  currentTab: string
  prevTab: string
  undoAction: (action: IUserAction) => void
  redoAction: (action: IUserAction) => void
  setMapPosition: (x: string, y: string) => void
  toggleCoordinates: () => void
  toggleFullscreenMode: () => void
  updateMapFullscreenMode: () => void
  toggleViewLayers: () => void
  setViewToggleLayers: (active: { [name: string]: IToggleLayer }) => void
}

export class MapControls extends Component<IProps> {
  getOwnerActions = () => {
    const { history } = this.props

    return history.actions.filter(item => item.user.userID === getCookie('uid'))
  }

  checkAccessToActionOfHistory = actionType => {
    const { mapData } = this.props
    const ownerActions = this.getOwnerActions()
    const undoRedoIsDisabled = !ownerActions.length || mapData.loading

    switch (actionType) {
      case UNDO_ACTION:
        return undoRedoIsDisabled || ownerActions[ownerActions.length - 1].isReverted
      case REDO_ACTION:
        return undoRedoIsDisabled || !ownerActions[0].isReverted
      default:
        return undoRedoIsDisabled
    }
  }

  _handleUndoAction = () => {
    const { history, undoAction } = this.props
    const { actions } = history
    const undoIsDisabled = this.checkAccessToActionOfHistory(UNDO_ACTION)

    if (undoIsDisabled) {
      return false
    }

    const action = [...actions].find(item => item.user.userID === getCookie('uid') && !item.isReverted)

    undoAction(action)
  }

  _handleRedoAction = () => {
    const { history, redoAction } = this.props
    const { actions } = history
    const redoIsDisabled = this.checkAccessToActionOfHistory(REDO_ACTION)

    if (redoIsDisabled) {
      return false
    }

    const tempActions = [...actions].filter(item => item.user.userID === getCookie('uid'))
    const actionIndex = tempActions.findIndex(item => !item.isReverted)
    const action = actionIndex === -1 ? tempActions[tempActions.length - 1] : tempActions[actionIndex - 1]

    redoAction(action)
  }

  _handleToggleFullscreenMode = () => {
    const { toggleFullscreenMode, updateMapFullscreenMode } = this.props

    toggleFullscreenMode()
    updateMapFullscreenMode()
  }

  _handleViewToggle = () => {
    const { toggleViewLayers } = this.props

    toggleViewLayers()
  }

  renderViewToggle = () => {
    const { mapData } = this.props

    return mapData.viewToggle && <ViewToggle />
  }

  renderInfoTextTooltip = text => {
    return (
      <Tooltip parent='infoText' arrow='center' position='top'>
        <span className='tipContent' dangerouslySetInnerHTML={{ __html: text }} />
      </Tooltip>
    )
  }

  renderInfoText = () => {
    const { infoText } = this.props
    const infoMsg = infoText || 'Select object from the library and place it on the map.'
    const isVisibleTooltip = infoMsg.length > MAX_LENGTH_INFO_MSG_STATUS_PANEL

    return (
      <>
        <span
          id='infoText'
          className={cn('info-text', s.infoText)}
          dangerouslySetInnerHTML={{ __html: infoMsg }}
        />
        {isVisibleTooltip && this.renderInfoTextTooltip(infoMsg)}
      </>
    )
  }

  render() {
    const {
      toggleCoordinates,
      mapData, setMapPosition,
      currentTab,
      prevTab
    } = this.props
    const undoIsDisabled = this.checkAccessToActionOfHistory(UNDO_ACTION)
    const redoIsDisabled = this.checkAccessToActionOfHistory(REDO_ACTION)
    const openPanelStyles = mapData.fullscreenMode && currentTab === prevTab ? '' : ' open-panel'

    return (
      <div className={`info-container ${openPanelStyles}`}>
        {this.renderInfoText()}
        <div className='actions'>
          <button
            type='button'
            id='undoAction'
            className={cn(s.undoBtn, { [s.disabled]: undoIsDisabled })}
            onClick={this._handleUndoAction}
          >
            <div>
              <SVGIconGenerator
                iconName='Undo'
                preset={{ type: 'christmasTown', subtype: 'UNDO' }}
                fill={undoIsDisabled ? DISABLE_FILL : BLUE_FILL}
              />
            </div>
            <Tooltip
              parent='undoAction'
              arrow='center'
              position={mapData.fullscreenMode ? 'bottom' : 'top'}
            >
              <span className='tipContent'>Undo</span>
            </Tooltip>
          </button>
          <button
            type='button'
            id='redoAction'
            className={cn(s.redoBtn, { [s.disabled]: redoIsDisabled })}
            onClick={this._handleRedoAction}
          >
            <div>
              <SVGIconGenerator
                iconName='Redo'
                preset={{ type: 'christmasTown', subtype: 'REDO' }}
                fill={redoIsDisabled ? DISABLE_FILL : BLUE_FILL}
              />
            </div>
            <Tooltip
              parent='redoAction'
              arrow='center'
              position={mapData.fullscreenMode ? 'bottom' : 'top'}
            >
              <span className='tipContent'>Redo</span>
            </Tooltip>
          </button>
          <Coords setMapPosition={setMapPosition} />
          <button
            type='button'
            id='showCoordinates'
            className='switch-coordinates-icon'
            onClick={toggleCoordinates}
          >
            <div>
              <SVGIconGenerator
                iconName='CTCoordinates'
                preset={{ type: 'christmasTown', subtype: 'DEFAULTS_BLUE' }}
              />
            </div>
            <Tooltip
              parent='showCoordinates'
              arrow='center'
              position={mapData.fullscreenMode ? 'bottom' : 'top'}
            >
              <span className='tipContent'>{mapData.showCoordinates ? 'Hide' : 'Show'} coordinates</span>
            </Tooltip>
          </button>
          <button
            type='button'
            id='toggleFullscreenMode'
            className='fullscreen-mode-icon'
            onClick={this._handleToggleFullscreenMode}
          >
            <div>
              <SVGIconGenerator
                iconName='CTFullscreen'
                preset={{ type: 'christmasTown', subtype: 'FULLSCREEN_MODE' }}
              />
            </div>
            <Tooltip
              parent='toggleFullscreenMode'
              arrow='center'
              position={mapData.fullscreenMode ? 'bottom' : 'top'}
            >
              <span className='tipContent'>{mapData.fullscreenMode ? 'Normal' : 'Fullscreen'} mode</span>
            </Tooltip>
          </button>
          <button
            type='button'
            id='toggleViewLayers'
            className={`view-toggle-layers-icon ${mapData.viewToggle ? 'view-toggle-active' : ''}`}
            onClick={this._handleViewToggle}
          >
            <div>
              <SVGIconGenerator
                customClass={s.viewToggleIcon}
                iconName='CTViewToggle'
                preset={{ type: 'christmasTown', subtype: 'VIEW_TOGGLE' }}
              />
            </div>
            <Tooltip
              parent='toggleViewLayers'
              arrow='center'
              active={true}
              position={mapData.fullscreenMode ? 'bottom' : 'top'}
            >
              <span className='tipContent'>{mapData.viewToggle ? 'Hide' : 'Show'} view toggle</span>
            </Tooltip>
          </button>
        </div>
        {this.renderViewToggle()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  history: state.mapeditor.history,
  mapData: state.mapeditor.mapData
})

const mapDispatchToProps = {
  undoAction,
  redoAction,
  toggleCoordinates,
  toggleFullscreenMode
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapControls)
