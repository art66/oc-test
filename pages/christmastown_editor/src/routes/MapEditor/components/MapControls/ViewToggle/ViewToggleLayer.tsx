import React, { Component } from 'react'
import IToggleLayer from '../../../interfaces/IToggleLayer'
import { SVGIconGenerator } from '@torn/shared/SVG'
import Tooltip from '@torn/shared/components/Tooltip'
import { HISTORY_TAB } from '../../../constants'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

interface IProps {
  item: IToggleLayer
  viewToggleLayers: { [name: string]: IToggleLayer }
  setViewToggleLayers: (active: { [name: string]: IToggleLayer }) => void
  currentTab: string
}

export class ViewToggleLayer extends Component<IProps> {
  _handleDisable = () => {
    return
  }

  _handleClick = () => {
    const { viewToggleLayers } = this.props
    const { item, setViewToggleLayers } = this.props
    const activeLocal = {}

    Object.keys(viewToggleLayers).forEach(name => {
      activeLocal[name] = name === item.layer ? !viewToggleLayers[name] : viewToggleLayers[name]
    })

    setViewToggleLayers(activeLocal)
  }

  render() {
    const { item, viewToggleLayers, currentTab } = this.props
    const isCurrentTab = currentTab === item.tab
    const isHistoryTab = currentTab === HISTORY_TAB
    const layerIconPreset = viewToggleLayers[item.layer]
    || isCurrentTab || isHistoryTab ? 'ACTIVE_VIEW_TOGGLE' : 'INACTIVE_VIEW_TOGGLE'
    const showIconPreset = viewToggleLayers[item.layer]
    || isCurrentTab || isHistoryTab ? 'ACTIVE_VIEW_TOGGLE_SHOW' : 'INACTIVE_VIEW_TOGGLE_SHOW'
    const stylesButton = cn({
      [s.layer]: true,
      [s.defaultLayer]: isCurrentTab || isHistoryTab
    })
    const idItem = `toggle-${item.layer}`

    return (
      <button
        id={idItem}
        className={stylesButton}
        type='button'
        onClick={isCurrentTab || isHistoryTab ? this._handleDisable : this._handleClick}
      >
        <div className={s.iconWrapper}>
          <SVGIconGenerator
            iconName='ViewShow'
            customClass={s.viewShowIcon}
            preset={{ type: 'christmasTown', subtype: showIconPreset }}
          />
          <SVGIconGenerator
            iconName={item.iconName}
            customClass={s.layerIcon}
            preset={{ type: 'christmasTown', subtype: layerIconPreset }}
          />
        </div>
        <Tooltip
          parent={idItem}
          arrow='center'
          position='left'
        >
          <span className='tipContent'>{item.label}</span>
        </Tooltip>
      </button>
    )
  }
}

export default ViewToggleLayer
