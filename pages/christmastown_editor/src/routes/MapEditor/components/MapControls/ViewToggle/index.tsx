import React, { Component } from 'react'
import { connect } from 'react-redux'
import ViewToggleLayer from './ViewToggleLayer'
import { TOGGLE_LAYERS } from '../../../constants'
import { setViewToggleLayers } from '../../../modules/mapeditor'
import IToggleLayer from '../../../interfaces/IToggleLayer'
import s from './styles.cssmodule.scss'

interface IProps {
  viewToggleLayers: { [name: string]: IToggleLayer }
  setViewToggleLayers: (active: { [name: string]: IToggleLayer }) => void
  currentTab: string
}

export class ViewToggle extends Component<IProps> {
  renderButtons = () => {
    const { viewToggleLayers, setViewToggleLayers, currentTab } = this.props

    return TOGGLE_LAYERS.map(item => {
      return (
        <li key={`toggle-${item.iconName}`} id={`toggle-${item.layer}`} className={s.toggleLayer}>
          <ViewToggleLayer
            item={item}
            currentTab={currentTab}
            setViewToggleLayers={setViewToggleLayers}
            viewToggleLayers={viewToggleLayers}
          />
        </li>
      )
    })
  }

  render() {
    return (
      <ul className={s.toggleLayers}>
        {this.renderButtons()}
      </ul>
    )
  }
}

const mapStateToProps = state => ({
  viewToggleLayers: state.mapeditor.mapData.viewToggleLayers,
  currentTab: state.mapeditor.library.currentTab
})

const mapDispatchToProps = {
  setViewToggleLayers
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewToggle)
