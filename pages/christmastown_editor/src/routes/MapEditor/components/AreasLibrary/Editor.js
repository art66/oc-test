import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import uniq from 'lodash.uniq'
import cn from 'classnames'
import { saveAreasOnMap, hideAreasEditor, deleteAreasOnMap } from '../../modules/mapeditor'
import s from './style.cssmodule.scss'

class Editor extends Component {
  render() {
    const {
      selectedArea,
      canDeleteAreaOnMap,
      canSaveAreaOnMap,
      hideAreasEditor,
      saveAreasOnMap,
      deleteAreasOnMap,
      areaNames
    } = this.props

    return (
      <div className={s.areasEditor}>
        <div className={s.areaName}>{areaNames || selectedArea.name}</div>
        <div className={s.actions}>
          {canDeleteAreaOnMap && (
            <button onClick={() => deleteAreasOnMap()} className='btn-blue big'>
              DELETE AREA
            </button>
          )}
          {canSaveAreaOnMap && (
            <button onClick={() => saveAreasOnMap()} className='btn-blue big'>
              SAVE
            </button>
          )}
          <span onClick={hideAreasEditor} className={cn(s.cancel, 'link-blue')}>
            Cancel
          </span>
        </div>
      </div>
    )
  }
}

Editor.propTypes = {
  selectedArea: PropTypes.object,
  hideAreasEditor: PropTypes.func,
  saveAreasOnMap: PropTypes.func,
  deleteAreasOnMap: PropTypes.func,
  canDeleteAreaOnMap: PropTypes.bool,
  canSaveAreaOnMap: PropTypes.bool,
  areaNames: PropTypes.string
}

const mapStateToProps = state => {
  const canDeleteAreaOnMap = state.mapeditor.mapData.areas.list.filter(area => area.selected).length > 0
  const areaNames = state.mapeditor.mapData.areas.list.filter(area => area.selected).map(area => area.name)

  return {
    selectedArea: state.mapeditor.library.areas.selectedArea,
    areaNames: uniq(areaNames).join(', '),
    canDeleteAreaOnMap: canDeleteAreaOnMap,
    canSaveAreaOnMap: !canDeleteAreaOnMap
  }
}

const mapActionsToProps = {
  saveAreasOnMap,
  hideAreasEditor,
  deleteAreasOnMap
}

export default connect(mapStateToProps, mapActionsToProps)(Editor)
