import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import AreasList from './AreasList'
import Form from './Form'
import Editor from './Editor'
import Dialog from './Dialog'
import {
  removeAreaFromLibrary,
  showAreaForm,
  selectArea,
  showAreasDialog,
  hideAreasDialog
} from '../../modules/mapeditor'

class AreasLibrary extends Component {
  getContent() {
    const { areas, showAreaForm, showAreasDialog, hideAreasDialog, removeAreaFromLibrary, selectArea, fullscreenMode } = this.props
    let content

    if (areas.editingArea) {
      content = <Form />
    } else if (areas.selectedArea && areas.selectedArea._id) {
      content = <Editor />
    } else if (areas.dialog && areas.dialog.text) {
      content = <Dialog dialog={areas.dialog} confirmAction={removeAreaFromLibrary} cancelAction={hideAreasDialog} />
    } else {
      content = (
        <AreasList
          areas={areas}
          showAreaForm={showAreaForm}
          removeAreaFromLibrary={removeAreaFromLibrary}
          selectArea={selectArea}
          showAreasDialog={showAreasDialog}
          fullscreenMode={fullscreenMode}
        />
      )
    }

    return content
  }

  render() {
    const content = this.getContent()

    return (
      <div className='areas-container'>
        {content}
      </div>
    )
  }
}

AreasLibrary.propTypes = {
  areas: PropTypes.object,
  showAreaForm: PropTypes.func,
  hideAreaForm: PropTypes.func,
  removeAreaFromLibrary: PropTypes.func,
  selectArea: PropTypes.func,
  showAreasDialog: PropTypes.func,
  hideAreasDialog: PropTypes.func,
  fullscreenMode: PropTypes.bool
}

const mapStateToProps = state => ({
  areas: state.mapeditor.library.areas
})

const mapActionsToProps = {
  removeAreaFromLibrary,
  showAreaForm,
  selectArea,
  showAreasDialog,
  hideAreasDialog
}

export default connect(mapStateToProps, mapActionsToProps)(AreasLibrary)
