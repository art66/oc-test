import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { hexToRgb } from '../../../../utils'
import s from './style.cssmodule.scss'

/* eslint-disable jsx-a11y/click-events-have-key-events */
class AreasList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      searchValue: ''
    }
  }

  setFilterValue(value) {
    this.setState({ searchValue: value })
  }

  getAreasList() {
    const { areas, showAreaForm, showAreasDialog, selectArea } = this.props

    return areas.list
      .filter(area => area.name && area.name
        .toString()
        .toLowerCase()
        .indexOf(this.state.searchValue.toLowerCase()) !== -1)
      .map(area => (
        <div
          key={area._id}
          className={cn(s.listItem, { [s.selected]: areas.selectedArea._id === area._id })}
          style={{ backgroundColor: hexToRgb(area.color, 0.2) || 'transparent' }}
        >
          <div onClick={() => selectArea(area)}>
            <div className={cn(s.col, s.quantity)}>{area.quantity || 0}</div>
            <div className={cn(s.col, s.name)}>{area.name}</div>
          </div>
          <i onClick={() => showAreaForm(area._id)} className={cn(s.icon, 'edit-icon')} />
          <i
            onClick={() =>
              showAreasDialog({
                text: `Are you sure you would like to remove ${area.name} area form library?`,
                confirmData: area._id
              })}
            className={cn(s.icon, 'remove-icon')}
          />
        </div>
      ))
  }

  render() {
    const areas = this.getAreasList()
    const { showAreaForm } = this.props

    return (
      <div>
        <div className='filter-wrap'>
          <input
            type='text'
            className={`input-text ${s.filerAreas}`}
            placeholder='Filter by name'
            onInput={e => this.setFilterValue(e.target.value)}
          />
        </div>
        <div onClick={showAreaForm} className={s.newAreaLink}>
          <i className={s.plusIcon}>+</i>
          <span className='link-blue'>New area</span>
        </div>
        <div className='scrollarea scroll-area scrollbar-thin'>
          <div className='scrollarea-content content'>
            <div className={s.areasList}>{areas}</div>
          </div>
        </div>
      </div>
    )
  }
}

AreasList.propTypes = {
  areas: PropTypes.object,
  showAreaForm: PropTypes.func,
  removeAreaFromLibrary: PropTypes.func,
  selectArea: PropTypes.func,
  showAreasDialog: PropTypes.func,
  fullscreenMode: PropTypes.bool
}

export default AreasList
