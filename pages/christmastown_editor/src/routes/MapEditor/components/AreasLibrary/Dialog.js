import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import s from './style.cssmodule.scss'

class Dialog extends Component {
  render() {
    const { dialog, confirmAction, cancelAction } = this.props

    return (
      <div className={s.areasDialog}>
        <div>
          <div
            className={s.text + (dialog.color ? `bold t-${dialog.color}` : '')}
            dangerouslySetInnerHTML={{ __html: dialog.text }}
          />
          <div className={s.confirmBlock}>
            {dialog.confirmed && (
              <span onClick={cancelAction} className='confirm-action okay t-blue h c-pointer bold'>
                Okay
              </span>
            )}
            {!dialog.confirmed && (
              <div>
                <span
                  onClick={() => confirmAction(dialog.confirmData)}
                  className={cn('t-blue h c-pointer', s.confirmAction)}
                >
                  Yes
                </span>
                <span onClick={() => cancelAction()} className={cn('t-blue h c-pointer', s.confirmAction)}>
                  No
                </span>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }
}

Dialog.propTypes = {
  dialog: PropTypes.object,
  confirmAction: PropTypes.func,
  cancelAction: PropTypes.func
}

export default Dialog
