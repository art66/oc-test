import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { saveAreaInLibrary, changeEditingArea, hideAreaForm } from '../../modules/mapeditor'
import { toggleICS, setICSInitParams } from '../../../../store/actions'
import editorsStyles from '../../../../styles/editors.cssmodule.scss'
import sTip from '../../../../styles/tooltips.cssmodule.scss'
import s from './style.cssmodule.scss'

const MAX_AREA_NAME_LENGTH = 25

class Form extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isValid: true,
      errorMsg: ''
    }
  }

  componentDidMount() {
    tooltipsSubscriber.subscribe(this.getImagePreviewTooltip())
    tooltipsSubscriber.subscribe({
      child: 'Custom image will be replaced by default one',
      ID: 'remove-area-image'
    })
  }

  componentWillUnmount() {
    tooltipsSubscriber.unsubscribe(this.getImagePreviewTooltip())
  }

  componentDidUpdate(prevProps) {
    this.updateTooltips()
  }

  getImagePreviewTooltip = () => {
    const { editingArea } = this.props

    return {
      ID: 'replace-area-image',
      child: (
        <div className={cn(sTip.imagePreviewWrap, sTip.withFilter)}>
          <img className={sTip.imagePreview} src={editingArea.imageData || editingArea.imageUrl} />
        </div>
      ),
      customConfig: {
        overrideStyles: {
          container: cn(sTip.imagePreviewTooltipContainer, sTip.container),
          title: sTip.imagePreviewTooltipTitle
        }
      }
    }
  }

  updateTooltips = () => {
    tooltipsSubscriber.render({
      tooltipsList: [this.getImagePreviewTooltip()],
      type: 'update'
    })
  }

  _handleUploadImage = (data) => this.props.changeEditingArea({ imageData: data })

  _handleOpenICS = () => {
    const { toggleICS: openICS, setICSInitParams: setInitParams, editingArea } = this.props
    const initParams = {
      imageType: 'christmasTownArea',
      callback: this._handleUploadImage,
      parameters: {
        areaId: editingArea._id || null
      }
    }

    openICS(true)
    setInitParams(initParams)
  }

  _handleRemoveImage = () => {
    this.props.changeEditingArea({
      imageData: null,
      imagePath: null,
      imageUrl: null
    })
  }

  _handleInputChange = event => {
    const { changeEditingArea } = this.props

    changeEditingArea({ name: event.target.value })
  }

  _handleSave = () => {
    const { editingArea, saveAreaInLibrary, hideAreaForm, areas } = this.props
    const { isValid } = this.state
    const errorMsg = {
      emptyString: 'Please add name',
      sameName: `The area with name ${editingArea.name} already exists, please choose another one`,
      maxLength: `Area name can not be more than ${MAX_AREA_NAME_LENGTH} characters`
    }

    if (!isValid) return

    if (areas.find(area => area.name === editingArea.name && area._id === editingArea._id
      && !editingArea.imageData && editingArea.imageData !== null)) {
      hideAreaForm()
      return
    }

    if (areas.find(area => area.name === editingArea.name && area._id !== editingArea._id)) {
      this.setState({ isValid: false, errorMsg: errorMsg.sameName })
      return
    }

    const emptyStringRegExp = new RegExp('^\\s*$')

    if (!editingArea.name || emptyStringRegExp.test(editingArea.name)) {
      this.setState({ isValid: false, errorMsg: errorMsg.emptyString })
      editingArea.name = ''
    } else if (editingArea.name.length > MAX_AREA_NAME_LENGTH) {
      this.setState({ isValid: false, errorMsg: errorMsg.maxLength })
    } else {
      saveAreaInLibrary(editingArea)
    }
  }

  renderActionsButtons = () => {
    const { editingArea } = this.props
    const removeEditImageIconPreset = {
      type: 'christmasTown',
      subtype: 'REMOVE_EDIT_IMAGE_DEFAULT'
    }
    const removeEditImageIconHoverPreset = {
      active: true,
      preset: {
        type: 'christmasTown',
        subtype: 'REMOVE_EDIT_IMAGE_HOVERED'
      }
    }

    return editingArea.imageData || editingArea.imageUrl ? (
      <>
        <button
          id='replace-area-image'
          type='button'
          className={cn(editorsStyles.spanBtn, editorsStyles.replaceImageBtn)}
          onClick={this._handleOpenICS}
        >
          <SVGIconGenerator
            iconName='Image'
            type='edit'
            preset={removeEditImageIconPreset}
            onHover={removeEditImageIconHoverPreset}
            customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
          />
          Replace background image
        </button>
        <button
          type='button'
          id='remove-area-image'
          className={editorsStyles.spanBtn}
          onClick={this._handleRemoveImage}
        >
          <SVGIconGenerator
            iconName='Image'
            type='remove'
            preset={removeEditImageIconPreset}
            onHover={removeEditImageIconHoverPreset}
            customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
          />
          Remove background image
        </button>
      </>
    ) : (
      <button
        type='button'
        className={editorsStyles.spanBtn}
        onClick={this._handleOpenICS}
      >
        <SVGIconGenerator
          iconName='Image'
          preset={{ type: 'christmasTown', subtype: 'UPLOAD_IMAGE_DEFAULT' }}
          onHover={{ active: true, preset: { type: 'christmasTown', subtype: 'UPLOAD_IMAGE_HOVERED' } }}
          customClass={cn(editorsStyles.icon, editorsStyles.imageIcon)}
        />
        Upload background image
      </button>
    )
  }

  render() {
    const { editingArea, hideAreaForm } = this.props
    const { isValid, errorMsg } = this.state
    const saveButtonClasses = cn({ 'btn-blue': true, big: true, locked: !isValid })

    return (
      <div className={s.areaForm}>
        {!isValid && (
          <div className={s.errorTooltip}>
            <span>
              {errorMsg}
            </span>
          </div>
        )}
        <input
          onChange={this._handleInputChange}
          onClick={() => this.setState({ isValid: true })}
          value={editingArea.name || ''}
          className={cn(s.areaNameInput, { [s.error]: !isValid })}
          placeholder='Name'
        />
        <div className={s.actions}>
          <div className={s.actionsWrap}>
            {this.renderActionsButtons()}
          </div>
          <div className={s.buttonsWrap}>
            <button
              type='button'
              onClick={this._handleSave}
              className={saveButtonClasses}
            >
              SAVE
            </button>
            <button
              type='button'
              onClick={hideAreaForm}
              className={cn(s.cancel, editorsStyles.spanBtn, 'link-blue')}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    )
  }
}

Form.propTypes = {
  editingArea: PropTypes.object,
  areas: PropTypes.array,
  hideAreaForm: PropTypes.func,
  changeEditingArea: PropTypes.func,
  saveAreaInLibrary: PropTypes.func,
  toggleICS: PropTypes.func,
  setICSInitParams: PropTypes.func
}

const mapStateToProps = state => ({
  editingArea: state.mapeditor.library.areas.editingArea,
  areas: state.mapeditor.library.areas.list
})

const mapActionsToProps = {
  saveAreaInLibrary,
  changeEditingArea,
  hideAreaForm,
  toggleICS,
  setICSInitParams
}

export default connect(mapStateToProps, mapActionsToProps)(Form)
