import React, { Component } from 'react'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { getCookie } from '@torn/shared/utils'
import IUserAction from '../../interfaces/IUserAction'
import {
  CREATE_OBJECTS_ACTION_TYPE,
  DELETE_OBJECTS_ACTION_TYPE,
  MOVE_OBJECTS_ACTION_TYPE,
  SET_BLOCK_ACTION_TYPE,
  TITLES,
  ACTION_TYPES
} from '../../constants/history'
import s from './Action.cssmodule.scss'

interface IProps {
  action: IUserAction
  fullscreenMode: boolean
  setMapPosition(x: number, y: number): void
}

export class Action extends Component<IProps> {
  getTitle = action => {
    if (action.type === CREATE_OBJECTS_ACTION_TYPE) {
      return {
        type: ACTION_TYPES.create,
        message: action.payload.objectsCreated.length > 1 ? 'Create group' : 'Create object'
      }
    }

    if (action.type === DELETE_OBJECTS_ACTION_TYPE) {
      return {
        type: ACTION_TYPES.delete,
        message: action.payload.objectsRemoved.length > 1 ? 'Delete group' : 'Delete object'
      }
    }

    if (action.type === MOVE_OBJECTS_ACTION_TYPE) {
      return {
        type: ACTION_TYPES.move,
        message: action.payload.objectsMoved.length > 1 ? 'Move group' : 'Move object'
      }
    }

    if (action.type === SET_BLOCK_ACTION_TYPE) {
      const isRemove = !action.payload.blockAdded

      return {
        type: isRemove ? ACTION_TYPES.delete : ACTION_TYPES.create,
        message: isRemove ? 'Delete block' : 'Create block'
      }
    }

    return TITLES[action.type]
  }

  render() {
    const { action, setMapPosition, fullscreenMode } = this.props
    const title = this.getTitle(action)

    return (
      <li
        className={cn(s.action, { [s.reverted]: action.isReverted, [s[action.status]]: action.status })}
        key={action.actionId}
      >
        <span className={cn(s.statusIcon, { [s[action.status]]: true })} />
        <span className={cn(s.actionIcon, { [s[title.type]]: true })} />
        <span className={s.actionName}>{title.message}</span>
        <span className={s.username}>
          {getCookie('sso_wiki_user') === action.user.playername ? <span className={s.owner}>(you) </span> : ''}
          {action.user.playername}
        </span>
        <button
          className={s.location}
          id={`action-position-${action.actionId}`}
          onClick={() => setMapPosition(action.position.x, action.position.y)}
        />
        <Tooltip
          parent={`action-position-${action.actionId}`}
          arrow='center'
          position={fullscreenMode ? 'left' : 'top'}
        >
          <span>
            Go to location x {action.position.x} / y {action.position.y}
          </span>
        </Tooltip>
      </li>
    )
  }
}

export default Action
