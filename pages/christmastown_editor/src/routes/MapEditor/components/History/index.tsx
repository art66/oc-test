import React, { Component } from 'react'
import { connect } from 'react-redux'
import IUserAction from '../../interfaces/IUserAction'
import Action from './Action'
import s from './style.cssmodule.scss'

interface IProps {
  history: {
    actions: IUserAction[]
  }
  setMapPosition(x: number, y: number): void
  mapData: any
}

interface IState {
  currPosition: number
}

export class History extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      currPosition: 0
    }
  }

  groupActionsByDate = (actions) => {
    const groupedActions = []
    let tempArr = []

    for (let i = 0; i < actions.length; i += 1) {
      tempArr.push(actions[i])
      if (i === actions.length - 1) {
        groupedActions.push(tempArr)
        break
      }

      if (actions[i].date.day !== actions[i + 1].date.day
        || actions[i].date.month !== actions[i + 1].date.month
        || actions[i].date.year !== actions[i + 1].date.year) {
        groupedActions.push(tempArr)
        tempArr = []
      }
    }
    return groupedActions
  }

  getDate = (date) => {
    const currentDate = new Date()
    const currentDay = currentDate.getUTCDate()
    const currentMonth = currentDate.getUTCMonth() + 1
    const currentYear = currentDate.getUTCFullYear()

    if (date.day !== currentDay || date.month !== currentMonth || date.year !== currentYear) {
      return (
        <div className={s.date}>
          {date.weekday} {date.day}/{date.month}/{date.year}
        </div>
      )
    }
  }

  render() {
    const { history, setMapPosition, mapData } = this.props
    const groupedActions = this.groupActionsByDate([...history.actions])

    return (
      <div className='history-container'>
        <div className={s.infoContainer}>
          <span className={s.title}>History</span>
        </div>
        <div className='scrollarea scroll-area scrollbar-thin'>
          <div className='scrollarea-content content'>
            <ul className={s.history}>
              {
                groupedActions.map((group, index) => {
                  return <li key={index}>
                    {this.getDate(group[0].date)}
                    <ul key={group[0].date}>
                      {
                        group.map((action) => {
                          return <Action
                            key={action.actionId}
                            action={action}
                            setMapPosition={setMapPosition}
                            fullscreenMode={mapData.fullscreenMode}
                          />
                        })
                      }
                    </ul>
                  </li>
                })
              }
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  history: state.mapeditor.history,
  mapData: state.mapeditor.mapData
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(History)
