import React from 'react'
import cn from 'classnames'

import { getColor } from '../../../utils'
import { CELL_SIZE, TAB_PARAMETERS } from '../../../constants'

const SPAWN_BLOCKER_ID = 3

interface IParam {
  // eslint-disable-next-line camelcase
  parameter_id: number
  hash: string
  color: string
  code: string
  position: {
    y: number
    x: number
  }
}

interface IProps {
  currentTab: string
  parameter: IParam
  removeParameterOnMap: (params: IParam) => void
  selectedParameter: {}
  selectParameterOnMap: (params: IParam) => void
  highlightedCells: {
    type: string
    hash: string
    id: number
  }
  unSelectOnMap: () => void
}

export const ParametersLayerItem = (props: IProps) => {
  const { parameter, selectedParameter, currentTab, highlightedCells } = props
  const isSelected = JSON.stringify(selectedParameter) === JSON.stringify(parameter)
  const radiusCells = parameter.parameter_id === SPAWN_BLOCKER_ID ? 6 : 0
  const radiusSize = radiusCells * CELL_SIZE
  const highlightAllowed = highlightedCells.type === 'parameter' && highlightedCells.hash
  const fullHighlight = highlightAllowed && parameter.hash === highlightedCells.hash
  const halfHighlight = highlightAllowed && parameter.parameter_id === highlightedCells.id && !fullHighlight
  const parameterItemClasses = cn({
    'parameter-layer-item': true,
    selected: isSelected,
    'half-highlight': halfHighlight,
    'full-highlight': fullHighlight
  })

  return (
    <div
      role='none'
      className={parameterItemClasses}
      style={{
        left: parameter.position.x * CELL_SIZE,
        top: parameter.position.y * -CELL_SIZE,
        color: getColor(parameter.color),
        pointerEvents: currentTab === TAB_PARAMETERS ? 'auto' : 'none'
      }}
      onClick={() => props.selectParameterOnMap(parameter)}
      onContextMenu={e => {
        e.preventDefault()
        props.unSelectOnMap()
      }}
    >
      {parameter.code}
      {isSelected && (
        <i role='none' className='delete-parameter-icon' onClick={() => props.removeParameterOnMap(parameter)} />
      )}
      {radiusCells > 0 && (
        <div
          className='radius'
          style={{
            width: radiusSize * 2 + CELL_SIZE,
            height: radiusSize * 2 + CELL_SIZE,
            left: -radiusSize,
            top: -radiusSize
          }}
        />
      )}
    </div>
  )
}

export default ParametersLayerItem
