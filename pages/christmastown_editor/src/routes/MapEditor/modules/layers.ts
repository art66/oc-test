import * as actionTypes from '../actionTypes'
import { UPDATE_OBJECTS_ACTION_TYPE } from '../constants/history'
import { LAYERS_MAP } from '../constants/layers'
import {comparator, getActionMock, getPosition, handleQuotaExceededError} from '../utils'
import { saveToHistory, updateAction } from './history'
import { fetchUrl } from '../../../utils'
import { getAjaxError, loading } from './mapeditor'

export const changeObjectsOrder = selectedObjects => {
  return {
    selectedObjects,
    type: actionTypes.CHANGE_OBJECTS_ORDER
  }
}

export const changeLayer = (object, layerName, newLayer) => {
  return {
    object,
    layerName,
    newLayer,
    type: actionTypes.CHANGE_LAYER
  }
}

export const updateObjectsOrder = objects => {
  return {
    objects,
    type: actionTypes.UPDATE_OBJECTS_ORDER
  }
}

const updateLayersInLS = modifiedSelectedObjects => {
  try {
    let lsObjects = JSON.parse(localStorage.getItem('mapObjects'))

    lsObjects = lsObjects.map(object => {
      const changedObject = modifiedSelectedObjects.find(item => item.hash === object.hash)

      if (changedObject) {
        return {
          ...object,
          ...changedObject,
          layer: changedObject.layer,
          order: changedObject.layer.number * changedObject.createdAt,
          onTopOfUser: changedObject.onTopOfUser
        }
      }
      return object
    })
    localStorage.setItem('mapObjects', JSON.stringify(lsObjects))
  } catch (e) {
    handleQuotaExceededError(e)
  }
}

const generateReqDataForUpdatingObjects = changedObjects => {
  return changedObjects.map(item => {
    return {
      hash: item.hash,
      position: { ...item.position },
      newState: {
        layer: item.layer,
        createdAt: item.createdAt
      }
    }
  })
}

const getReorderedObjects = (updatedObjects, oldObjects) => {
  return updatedObjects.filter(item => {
    const oldObj = oldObjects.find(obj => item.hash === obj.hash)

    return oldObj.createdAt !== item.createdAt || oldObj.layer.name !== item.layer.name
  })
}

const getUpdatedLayer = (object, layerName, layersInState) => {
  const objectsTimeInMicroSecs = [...layersInState[layerName].map(obj => obj.createdAt), object.createdAt]
    .sort(comparator)

  return [object, ...layersInState[layerName]].map((obj, index) => {
    return {
      ...obj,
      layer: {
        name: layerName,
        number: LAYERS_MAP[layerName]
      },
      createdAt: objectsTimeInMicroSecs[index],
      order: objectsTimeInMicroSecs[index] * LAYERS_MAP[layerName]
    }
  })
}

export const changeObjectLayer = (object, layerName) => (dispatch, getState) => {
  const layersInState = getState().mapeditor.mapData.layers
  const { selectedObjects } = getState().mapeditor.mapData
  const newLayer = getUpdatedLayer(object, layerName, layersInState)
  const position = getPosition(selectedObjects)
  const payload = { oldObjects: [object, ...layersInState[layerName]], newObjects: newLayer }
  const action = getActionMock(payload, UPDATE_OBJECTS_ACTION_TYPE, position)

  dispatch(loading(true))
  dispatch(changeLayer(object, layerName, newLayer))
  dispatch(saveToHistory(action))
  updateLayersInLS(newLayer)

  const changedObjects = getReorderedObjects(newLayer, selectedObjects)
  const reqData = generateReqDataForUpdatingObjects(changedObjects)

  return fetchUrl('updateObjects', { position, updateObjects: reqData }).then(
    json => {
      dispatch(updateAction(action.actionId, json.action))
      dispatch(loading(false))
    },
    error => dispatch(getAjaxError(error))
  )
}

export const reorderObjects = (selectedObjects, timestampArray) => (dispatch, getState) => {
  const oldObjects = getState().mapeditor.mapData.selectedObjects
    .filter(item => selectedObjects.some(obj => obj.hash === item.hash))
  const modifiedSelectedObjects = [...selectedObjects].map((item, index) => {
    return {
      ...item,
      createdAt: timestampArray.length ? timestampArray[index] : item.createdAt
    }
  })
  const position = getPosition(selectedObjects)
  const payload = { oldObjects, newObjects: modifiedSelectedObjects }
  const action = getActionMock(payload, UPDATE_OBJECTS_ACTION_TYPE, position)

  dispatch(loading(true))
  dispatch(changeObjectsOrder(modifiedSelectedObjects))
  dispatch(saveToHistory(action))
  updateLayersInLS(modifiedSelectedObjects)

  const changedObjects = getReorderedObjects(modifiedSelectedObjects, oldObjects)
  const reqData = generateReqDataForUpdatingObjects(changedObjects)

  return fetchUrl('updateObjects', { position, updateObjects: reqData }).then(
    json => {
      dispatch(updateAction(action.actionId, json.action))
      dispatch(loading(false))
    },
    error => dispatch(getAjaxError(error))
  )
}
