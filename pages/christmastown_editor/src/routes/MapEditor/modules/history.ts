import { createAction } from 'redux-actions'
import * as actionTypes from '../actionTypes/history'

export const saveToHistory = createAction(actionTypes.SAVE_TO_HISTORY, action => ({ action }))
export const undoAction = createAction(actionTypes.UNDO_ACTION, action => ({ action }))
export const redoAction = createAction(actionTypes.REDO_ACTION, action => ({ action }))
export const historyDataFetched = createAction(actionTypes.HISTORY_DATA_FETCHED, json => ({ json }))
export const updateAction = createAction(actionTypes.UPDATE_ACTION, (id, action) => ({ id, action }))
export const removeFromHistory = createAction(actionTypes.REMOVE_FROM_HISTORY, IDs => ({ IDs }))
