import { createAction } from 'redux-actions'
import * as a from '../actionTypes/overlay'

export const activateOverlay = createAction(a.ACTIVATE_OVERLAY)
export const deactivateOverlay = createAction(a.DEACTIVATE_OVERLAY)
