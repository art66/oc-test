import { createAction } from 'redux-actions'
import * as a from '../actionTypes/library'

export const showLibraryObjectPopup = createAction(a.SHOW_LIBRARY_OBJECT_POPUP, object => ({ object }))
export const hideLibraryObjectPopup = createAction(a.HIDE_LIBRARY_OBJECT_POPUP)
export const addTagsToObject = createAction(a.ADD_TAGS_TO_OBJECT, (objectLibraryId: string, tags: string[]) => ({
  objectLibraryId,
  tags
}))
export const removeObjectTags = createAction(a.REMOVE_OBJECT_TAGS, (objectLibraryId: string, tags: string[]) => ({
  objectLibraryId,
  tags
}))
export const setDefaultLayer = createAction(a.SET_DEFAULT_LAYER, layerName => ({ layerName }))
export const changeDefaultLayer = createAction(a.CHANGE_DEFAULT_LAYER, layerName => ({ layerName }))
export const updateParametersCount = createAction(a.UPDATE_PARAMETERS_COUNT, (parameterID, count) => ({
  parameterID,
  count
}))
