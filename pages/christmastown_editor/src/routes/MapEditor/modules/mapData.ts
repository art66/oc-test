import { createAction } from 'redux-actions'
import * as a from '../actionTypes'
import IParameter from '../interfaces/IParameter'
import ISize from '../interfaces/ISize'

export const searchParameterOnMap = createAction(
  a.SEARCH_PARAMETER_ON_MAP, (id: string, modifier: 1 | -1) => ({ id, modifier })
)

export const highlightParameterOnMap = createAction(
  a.HIGHLIGHT_PARAMETER_ON_MAP, (id: string, hash: string, index: number) => ({ id, hash, index })
)

export const fetchParametersOnMapById =
  createAction(a.FETCH_PARAMETERS_ON_MAP_BY_ID, (id: number, modifier: 1 | -1) => ({ id, modifier }))
export const fetchParametersOnMapByIdSuccess = createAction(
  a.FETCH_PARAMETERS_ON_MAP_BY_ID_SUCCESS,
  (id: string, parameters: IParameter[], searchIndex: number) => ({ id, parameters, searchIndex })
)

export const checkTabsSync =
  createAction(a.CHECK_TABS_SYNC, (mapId: string, mapSize: ISize) => ({ mapId, mapSize }))

export const synchronizeMapData =
  createAction(a.SYNCHRONIZE_MAP_DATA, (mapData: {}) => ({ mapData }))

export const renameMap = createAction(a.RENAME_MAP, name => name)

export const disableDataLoading = createAction(a.DISABLE_DATA_LOADING, allowed => ({ allowed }))

export const setSuccessfulLoadStatus = createAction(a.SET_SUCCESSFUL_LOAD_STATUS)
