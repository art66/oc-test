import { createAction } from 'redux-actions'
import * as actionTypes from '../actionTypes/npc'
import INpc from '../../../interfaces/INpc'
import IPosition from '../interfaces/IPosition'

export const selectNpc = createAction(
  actionTypes.SELECT_NPC,
  (npc: INpc, showPointer: boolean) => ({ npc, showPointer })
)
export const addNpc = createAction(
  actionTypes.ADD_NPC,
  (selectedNpc: INpc, position: IPosition) => ({ selectedNpc, position })
)
export const placeNpcOnMap = createAction(
  actionTypes.PLACE_NPC_ON_MAP,
  (npc: INpc, websocket: boolean) => ({ npc, websocket })
)
export const removeNpc = createAction(actionTypes.REMOVE_NPC, (npc: INpc) => ({ npc }))
export const removeNpcFromMap = createAction(
  actionTypes.REMOVE_NPC_FROM_MAP,
  (npc: INpc, websocket: boolean) => ({ npc, websocket })
)
export const highlightNpcOnMap = createAction(
  actionTypes.HIGHLIGHT_NPC_ON_MAP,
  (id: string, hash: string) => ({ id, hash })
)

export const addLibraryNpc = createAction(actionTypes.ADD_LIBRARY_NPC, (npc: INpc) => ({ npc }))
export const updateLibraryNpc = createAction(actionTypes.UPDATE_LIBRARY_NPC, (npc: INpc) => ({ npc }))
export const removeLibraryNpc = createAction(actionTypes.REMOVE_LIBRARY_NPC, (id: string) => ({ id }))
export const updateNpcQuantity = createAction(
  actionTypes.UPDATE_LIBRARY_NPC_QUANTITY,
  (id: string, value: 1|-1) => ({ id, value })
)
