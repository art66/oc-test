import { createAction } from 'redux-actions'
import * as actionTypes from '../actionTypes/backups'

export const changeControlsStatus = createAction(actionTypes.CHANGE_CONTROLS_STATUS, status => ({ status }))
export const editBackupName = createAction(actionTypes.EDIT_BACKUP_NAME, ID => ({ ID }))
export const saveBackupName = createAction(actionTypes.SAVE_BACKUP_NAME, (ID, name) => ({ ID, name }))
export const addBackup = createAction(actionTypes.ADD_BACKUP, name => ({ name }))
export const saveBackup = createAction(actionTypes.SAVE_BACKUP, backup => ({ backup }))
export const removeBackup = createAction(actionTypes.REMOVE_BACKUP, ID => ({ ID }))
export const removeBackupFromList = createAction(actionTypes.REMOVE_BACKUP_FROM_LIST, ID => ({ ID }))
export const toggleConfirmation =
  createAction(actionTypes.TOGGLE_BACKUP_CONFIRMATION, (ID, confirmation) => ({ ID, confirmation }))
export const isWrongName = createAction(actionTypes.IS_WRONG_NAME, (ID, wrongName, message) => ({
  ID,
  wrongName,
  message
}))
export const fetchBackupsData = createAction(actionTypes.FETCH_BACKUPS_DATA)
export const setBackups = createAction(actionTypes.SET_BACKUPS, backups => ({ backups }))
export const renameBackup = createAction(actionTypes.RENAME_BACKUP, (ID, name) => ({ ID, name }))
export const loadBackup = createAction(actionTypes.LOAD_BACKUP, ID => ({ ID }))
export const changeControlsState = createAction(actionTypes.CHANGE_CONTROLS_STATE, (canSave, message = '') => ({
  canSave,
  message
}))
