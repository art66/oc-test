import { createAction } from 'redux-actions'
import md5 from 'js-md5'
import { getCookie } from '@torn/shared/utils'
// eslint-disable-next-line max-len
import {
  fetchUrl,
  isInViewport,
  mergeMapElements,
  positionsEqual,
  applyScaleEndpoints,
  getErrorMessage
} from '../../../utils'
import * as actionTypes from '../actionTypes'
import { updateCells } from './blocking'
import { renameMap, disableDataLoading, setSuccessfulLoadStatus } from './mapData'
import { historyDataFetched, saveToHistory, removeFromHistory } from './history'
import { getActionMock, getPosition, getMapSizeInitMap, setCookie, handleQuotaExceededError } from '../utils'
// eslint-disable-next-line import/no-cycle
import { updateObjectsOrder } from './layers'
import { activateOverlay, deactivateOverlay } from './overlay'
import * as npcActions from './npc'
import {
  OBJECTS_KEY_IN_PAYLOAD as payloadKeys,
  CREATE_OBJECTS_ACTION_TYPE,
  DELETE_OBJECTS_ACTION_TYPE,
  MOVE_OBJECTS_ACTION_TYPE,
  ADD_PARAMETER_ACTION_TYPE,
  REMOVE_PARAMETER_ACTION_TYPE,
  DELETE_AREAS_ACTION_TYPE,
  ADD_AREAS_ACTION_TYPE,
  SET_BLOCK_ACTION_TYPE,
  ADD_NPC_ACTION_TYPE,
  REMOVE_NPC_ACTION_TYPE
} from '../constants/history'
import {
  FULLSCREEN_MODE,
  NORMAL_MODE,
  FULLSCREEN_MODE_COOKIE,
  COORDINATES_COOKIE,
  VIEW_TOGGLE,
  YEAR,
  DEFAULT_COOKIE_VIEW_TOGGLE
} from '../constants'
import { getRouteHeader } from '../../../../../christmastown/src/controller/actions'

// eslint-disable-next-line no-redeclare
/* global WebsocketHandler */

export const loading = createAction(actionTypes.LOADING, (value = true) => value)

export const mapEditorDataFetched = json => ({
  type: actionTypes.MAP_EDITOR_DATA_FETCHED,
  json
})

export const mapEditorDataFetchedWithError = () => ({
  type: actionTypes.MAP_EDITOR_DATA_FETCHED_WITH_ERROR
})

export const getAjaxError = error => {
  return {
    type: actionTypes.GET_AJAX_ERROR,
    error
  }
}

export const updateMapData = mapData => ({
  type: actionTypes.UPDATE_MAP_DATA,
  mapData
})

export const startFetchingAdminMapData = endpoints => ({
  type: actionTypes.START_FETCHING_ADMIN_MAP_DATA,
  endpoints
})

export const setFilterCondition = (name = '', value = '') => ({
  type: actionTypes.SET_FILTER_CONDITION,
  name,
  value
})

export const fetchMapEditorData = () => dispatch =>
  fetchUrl('mapEditor', getMapSizeInitMap())
    .then(json => {
      dispatch(mapEditorDataFetched(json))
      dispatch(historyDataFetched(json))
    })
    .then(() => dispatch(setFilterCondition()))
    .catch(error => {
      dispatch(getAjaxError(getErrorMessage(error)))
      dispatch(mapEditorDataFetchedWithError())
    })
    .finally(() => dispatch(deactivateOverlay()))

export const fetchMapEditorDataWithBlockingCheck = () => dispatch => {
  fetchUrl('isMapLocked')
    .then(data => {
      dispatch(disableDataLoading(data.status))

      if (!data.status) {
        dispatch(fetchMapEditorData())
      }
    })
    .catch(error => {
      dispatch(mapEditorDataFetchedWithError())
      dispatch(getAjaxError(getErrorMessage(error)))
    })
    .finally(() => dispatch(getRouteHeader()))
}

export const parseAndUpdateObjectsLibrary = () => dispatch => {
  fetchUrl('updateObjectsLibrary')
    .then(() => dispatch(fetchMapEditorData()))
    .catch(error => dispatch(getAjaxError(getErrorMessage(error))))
}

export const updateMap = endpoints => (dispatch, getState) => {
  const { mapData } = getState().mapeditor

  endpoints = endpoints || mapData.endpoints
  const scale = 1

  dispatch(loadMapDataFromLS(endpoints))
  dispatch(startFetchingAdminMapData(endpoints))
  return fetchUrl('updateMap', applyScaleEndpoints(endpoints, scale))
    .then(json => dispatch(adminMapDataFetched(json, endpoints)))
    .catch(error => dispatch(getAjaxError(getErrorMessage(error))))
}

export const loadMapDataFromLS = endpoints => (dispatch, getState) => {
  const { mapData } = getState().mapeditor
  const mapPosition = endpoints.leftTop

  let lsObjects = []
  let lsParameters = []

  try {
    lsObjects = JSON.parse(localStorage.getItem('mapObjects')) || []
    lsParameters = JSON.parse(localStorage.getItem('mapParameters')) || []
  } catch (e) {
    console.log(e)
  }

  lsObjects = lsObjects.filter(item => isInViewport(item.position, mapPosition, mapData.scale))
  lsParameters = lsParameters.filter(item => isInViewport(item.position, mapPosition))
  const mapDataToUpdate = {
    objects: mergeMapElements(mapData.objects, lsObjects),
    parameters: mergeMapElements(mapData.parameters, lsParameters)
  }

  dispatch(updateMapData(mapDataToUpdate))
}

export const clearLocalStorage = () => () => {
  try {
    localStorage.setItem('mapObjects', '[]')
    localStorage.setItem('mapParameters', '[]')
  } catch (e) {
    handleQuotaExceededError(e)
  }
}

// eslint-disable-next-line max-statements
export const adminMapDataFetched = (json, endpoints) => (dispatch, getState) => {
  const { mapData } = getState().mapeditor
  const mapPosition = endpoints.leftTop
  let lsObjects = []
  let lsParameters = []

  try {
    lsObjects = (JSON.parse(localStorage.getItem('mapObjects')) || []).filter(
      obj => !isInViewport(obj.position, mapPosition)
    )
    lsParameters = (JSON.parse(localStorage.getItem('mapParameters')) || []).filter(
      parameter => !isInViewport(parameter.position, mapPosition)
    )
  } catch (e) {
    console.log(e)
  }

  const areas = mapData.areas.list.filter(area => isInViewport(area.position, endpoints.leftTop))
  const storeObjects = mapData.objects.filter(obj => !isInViewport(obj.position, mapPosition))
  const objects = mergeMapElements(storeObjects, lsObjects, json.mapObjects)
  const parameters = mergeMapElements(mapData.parameters, lsParameters, json.mapParameters)

  try {
    localStorage.setItem('mapObjects', JSON.stringify(objects))
    localStorage.setItem('mapParameters', JSON.stringify(parameters))
  } catch (e) {
    handleQuotaExceededError(e)
  }

  json.mapAreas = mergeMapElements(
    areas,
    json.mapAreas.map(area => ({ ...area, saved: true }))
  )
  json.mapObjects = objects.filter(item => isInViewport(item.position, mapPosition, mapData.scale))
  json.mapParameters = parameters.filter(item => isInViewport(item.position, mapPosition))
  dispatch({ type: actionTypes.ADMIN_MAP_DATA_FETCHED, json, endpoints })
}

export const setPointerPos = pointerPos => ({
  type: actionTypes.SET_POINTER_POS,
  pointerPos
})

export const selectLibraryParameter = parameter => ({
  type: actionTypes.SELECT_LIBRARY_PARAMETER,
  parameter
})

export const addParameterToMap = () => {
  return (dispatch, getState) => {
    const { mapData } = getState().mapeditor
    const selectedParameter = Object.assign({}, mapData.selectedParameter)

    selectedParameter.hash = md5(selectedParameter.parameter_id.toString() + Date.now().toString())
    selectedParameter.position = {
      x: mapData.cellPointer.x,
      y: mapData.cellPointer.y
    }
    const isCellBusy = mapData.parameters.some(p => positionsEqual(p.position, selectedParameter.position))

    if (isCellBusy) {
      return false
    }

    dispatch(showParameterOnMap(selectedParameter))
    const parameterToSend = {
      parameter_id: selectedParameter.parameter_id,
      position: selectedParameter.position,
      hash: selectedParameter.hash
    }

    return fetchUrl('putParameter', parameterToSend).then(
      () => {
        dispatch(parameterAddedOnMap(selectedParameter))
      },
      error => dispatch(getAjaxError(getErrorMessage(error)))
    )
  }
}

export const showParameterOnMap = (parameter, websocket = false) => ({
  type: actionTypes.SHOW_PARAMETER_ON_MAP,
  parameter,
  websocket
})

export const parameterAddedOnMap = parameter => ({
  type: actionTypes.PARAMETER_ADDED_ON_MAP,
  parameter
})

export const selectParameterOnMap = parameter => ({
  type: actionTypes.SELECT_PARAMETER_ON_MAP,
  parameter
})

export const unSelectOnMap = () => ({
  type: actionTypes.UNSELECT_ON_MAP
})

export const removeParameterOnMap = () => {
  return (dispatch, getState) => {
    const { selectedParameter } = getState().mapeditor.mapData

    dispatch(showRemovingParameterOnMap(selectedParameter))

    const parameterToSend = {
      parameter_id: selectedParameter.parameter_id,
      position: selectedParameter.position,
      hash: selectedParameter.hash
    }

    return fetchUrl('removeParameterFromMap', parameterToSend).then(
      json => {
        dispatch(parameterRemovedOnMap(json.action.payload.deletedParameter))
        dispatch(unSelectOnMap())
      },
      error => dispatch(getAjaxError(getErrorMessage(error)))
    )
  }
}

export const showRemovingParameterOnMap = (parameter, websocket = false) => dispatch => {
  dispatch({ type: actionTypes.SHOW_REMOVING_PARAMETER_ON_MAP, parameter, websocket })

  try {
    let lsParameters = JSON.parse(localStorage.getItem('mapParameters'))

    lsParameters = JSON.stringify(lsParameters.filter(item => item.hash !== parameter.hash))
    localStorage.setItem('mapParameters', lsParameters)
  } catch (e) {
    handleQuotaExceededError(e)
  }
}

export const parameterRemovedOnMap = parameter => ({
  type: actionTypes.PARAMETER_REMOVED_ON_MAP,
  parameter
})

export const toggleCoordinates = () => (dispatch, getState) => {
  const { showCoordinates } = getState().mapeditor.mapData

  dispatch({ type: actionTypes.TOGGLE_COORDINATES })
  setCookie(COORDINATES_COOKIE, JSON.stringify(!showCoordinates))
}

export const setCurrentTab = tab => ({
  type: actionTypes.SET_CURRENT_TAB,
  tab
})

export const addObjectToMap = () => {
  return (dispatch, getState) => {
    const { x, y } = getState().mapeditor.mapData.cellPointer
    const selectedObject = Object.assign({}, getState().mapeditor.mapData.selectedObjects[0])

    selectedObject.hash = md5(selectedObject.object_id.toString() + Date.now().toString())
    selectedObject.position = { x, y }
    dispatch(showObjectOnMap(selectedObject))
    dispatch(loading(true))

    const position = getPosition([selectedObject])

    const objectToSend = {
      object_id: selectedObject.object_id,
      hash: selectedObject.hash,
      position: selectedObject.position
    }

    return fetchUrl('putObjects', { position, objects: [objectToSend] }).then(
      json => {
        dispatch(objectAddedToMap([selectedObject], json.action.payload.objectsCreated))
        dispatch(loading(false))
      },
      error => dispatch(getAjaxError(getErrorMessage(error)))
    )
  }
}

export const showObjectOnMap = object => ({
  type: actionTypes.SHOW_OBJECT_ON_MAP,
  object
})

export const objectAddedToMap = (objects, createdObjects) => ({
  type: actionTypes.OBJECT_ADDED_TO_MAP,
  objects,
  createdObjects
})

export const selectLibraryObject = object => ({
  type: actionTypes.SELECT_LIBRARY_OBJECT,
  object
})

export const saveObjectPositionInLibrary = (object, pos) => dispatch => {
  return fetchUrl('saveObjectPositionInLibrary', { object_id: object.object_id, position: pos })
    .then(() =>
      dispatch({
        type: actionTypes.SAVE_OBJECT_POSITION_IN_LIBRARY,
        object,
        position: pos
      }))
    .catch(error => dispatch(getAjaxError(getErrorMessage(error))))
}

export const saveObjectZIndex = (object, onTopOfUser) => dispatch => {
  return fetchUrl('saveObjectPositioning', { object_id: object.object_id, onTopOfUser })
    .then(() =>
      dispatch({
        type: actionTypes.SAVE_OBJECT_Z_INDEX,
        object,
        onTopOfUser
      }))
    .catch(error => dispatch(getAjaxError(getErrorMessage(error))))
}

export const toggleLibraryMode = () => ({
  type: actionTypes.TOGGLE_LIBRARY_MODE
})

export const selectObjectOnMap = object => ({
  type: actionTypes.SELECT_OBJECT_ON_MAP,
  object
})

export const selectObjectsOnMap = createAction(actionTypes.SELECT_OBJECTS_ON_MAP, objects => ({ objects }))
export const addSelectedObject = createAction(actionTypes.ADD_SELECTED_OBJECT, object => ({ object }))
export const unselectObject = createAction(actionTypes.UNSELECT_OBJECT, object => ({ object }))
export const unselectLayer = createAction(actionTypes.UNSELECT_LAYER, layerName => ({ layerName }))
export const removeObjectsOnMap = createAction(actionTypes.REMOVE_OBJECTS_ON_MAP, objects => ({ objects }))
export const removeObjectsOnMapAndSave = createAction(actionTypes.REMOVE_OBJECTS_ON_MAP_AND_SAVE, objects => ({
  objects
}))
export const putObjectsIntoBuffer = createAction(actionTypes.PUT_OBJECTS_INTO_BUFFER, (action, objects) => ({
  action,
  objects
}))
export const putObjectsFromBufferOnMap = createAction(actionTypes.PUT_OBJECTS_FROM_BUFFER_ON_MAP, position => ({
  position
}))
export const clearBuffer = createAction(actionTypes.CLEAR_BUFFER)
export const addObjects = createAction(actionTypes.ADD_OBJECTS, objects => ({ objects }))
export const showMapObjectContextMenu = createAction(actionTypes.SHOW_MAP_OBJECT_CONTEXT_MENU, object => ({ object }))
export const hideMapObjectContextMenu = createAction(actionTypes.HIDE_MAP_OBJECT_CONTEXT_MENU)

export const removeObjectOnMap = object => {
  return (dispatch, getState) => {
    const selectedObject = object || getState().mapeditor.mapData.selectedObjects[0]

    dispatch(showRemovingObjectOnMap(selectedObject))

    const objectToSend = {
      object_id: selectedObject.object_id,
      hash: selectedObject.hash,
      position: selectedObject.position
    }

    return fetchUrl('removeObject', objectToSend).then(
      json => {
        dispatch(objectRemovedOnMap(json.object))
      },
      error => dispatch(getAjaxError(getErrorMessage(error)))
    )
  }
}

export const showRemovingObjectOnMap = object => dispatch => {
  try {
    let lsObjects = JSON.parse(localStorage.getItem('mapObjects'))

    lsObjects = JSON.stringify(lsObjects.filter(obj => obj.hash !== object.hash))
    localStorage.setItem('mapObjects', lsObjects)
  } catch (e) {
    handleQuotaExceededError(e)
  }

  dispatch({ type: actionTypes.SHOW_REMOVING_OBJECT_ON_MAP, object })
}

export const objectRemovedOnMap = object => ({
  type: actionTypes.OBJECT_REMOVED_ON_MAP,
  object
})

export const changeObjectPosition = (object, newPos) => dispatch => {
  dispatch({ type: actionTypes.CHANGE_OBJECT_POSITION, object, newPos })

  try {
    let lsObjects = JSON.parse(localStorage.getItem('mapObjects'))

    lsObjects = lsObjects.map(obj => ({
      ...obj,
      position: obj.hash === object.hash ? newPos : obj.position
    }))
    localStorage.setItem('mapObjects', JSON.stringify(lsObjects))
  } catch (e) {
    handleQuotaExceededError(e)
  }

  const objectToSend = {
    object_id: object.object_id,
    hash: object.hash,
    position: object.position
  }

  return fetchUrl('changeObjectPosition', { object: objectToSend, newPos }).catch(error =>
    dispatch(getAjaxError(getErrorMessage(error))))
}

export const moveObjects = createAction(actionTypes.MOVE_OBJECTS, (moveObj, oldObjects) => ({
  moveObjects: moveObj,
  oldObjects
}))

export const moveObjectsAndSave = createAction(actionTypes.MOVE_OBJECTS_AND_SAVE, (moveObj, oldObjects) => ({
  moveObjects: moveObj,
  oldObjects
}))

export const setMapPosition = (x, y) => ({
  type: actionTypes.SET_MAP_POSITION,
  x,
  y
})

export const toggleFullscreenMode = () => (dispatch, getState) => {
  const { fullscreenMode } = getState().mapeditor.mapData

  dispatch({ type: actionTypes.TOGGLE_FULLSCREEN_MODE })
  setCookie(FULLSCREEN_MODE_COOKIE, !fullscreenMode ? FULLSCREEN_MODE : NORMAL_MODE)
}

export const toggleViewLayers = () => (dispatch, getState) => {
  const { viewToggle } = getState().mapeditor.mapData
  const viewToggleCookie = (getCookie(VIEW_TOGGLE) && JSON.parse(getCookie(VIEW_TOGGLE))) || DEFAULT_COOKIE_VIEW_TOGGLE
  const newViewToggleCookie = {
    ...viewToggleCookie,
    open: !viewToggle
  }

  dispatch({ type: actionTypes.TOGGLE_VIEW_LAYERS })
  setCookie(VIEW_TOGGLE, JSON.stringify(newViewToggleCookie), YEAR)
}

export const setViewToggleLayers = layers => dispatch => {
  const viewToggleCookie = (getCookie(VIEW_TOGGLE) && JSON.parse(getCookie(VIEW_TOGGLE))) || DEFAULT_COOKIE_VIEW_TOGGLE
  const newViewToggleCookie = {
    ...viewToggleCookie,
    tools: layers
  }

  dispatch({ type: actionTypes.SET_TOGGLE_VIEW_LAYERS, layers })
  setCookie(VIEW_TOGGLE, JSON.stringify(newViewToggleCookie), YEAR)
}

export const updateInfoText = text => ({
  type: actionTypes.UPDATE_INFO_TEXT,
  text
})

export const updateParameterList = payload => ({
  type: actionTypes.UPDATE_PARAMETER_LIST,
  payload
})

export const hidePreloader = () => ({
  type: actionTypes.HIDE_PRELOADER
})

export const showAreaForm = (_id = null) => ({
  type: actionTypes.SHOW_AREA_FORM,
  _id
})

export const hideAreaForm = () => ({
  type: actionTypes.HIDE_AREA_FORM
})

export const removeAreaFromLibrary = _id => dispatch => {
  fetchUrl('removeAreaFromLibrary', { _id })
    .then(() => {
      dispatch({ type: actionTypes.REMOVE_AREA_FROM_LIBRARY, _id })
      dispatch(hideAreasDialog())
    })
    .catch(error => dispatch(getAjaxError(getErrorMessage(error))))
}

export const changeEditingArea = area => ({
  type: actionTypes.CHANGE_EDITING_AREA,
  area
})

export const saveAreaInLibrary = area => dispatch => {
  fetchUrl('saveAreaInLibrary', { area }).then(
    data => {
      dispatch({ type: actionTypes.SAVE_AREA_IN_LIBRARY, area: data })
      dispatch(setSuccessfulLoadStatus())
    },
    error => dispatch(getAjaxError(getErrorMessage(error)))
  )
}

export const selectArea = area => ({
  type: actionTypes.SELECT_AREA,
  area
})

export const addAreaOnMap = (area = null) => (dispatch, getState) => {
  const selectedArea = Object.assign({}, area || getState().mapeditor.library.areas.selectedArea)
  const { cellPointer } = getState().mapeditor.mapData
  const cellIsBusy = getState().mapeditor.mapData.areas.list.some(
    areaItem => areaItem.position.x === cellPointer.x && areaItem.position.y === cellPointer.y
  )

  if (cellIsBusy) {
    return
  }

  selectedArea.saved = false
  selectedArea.hash = md5(selectedArea._id.toString() + Date.now().toString())
  selectedArea.position = {
    x: cellPointer.x,
    y: cellPointer.y
  }

  dispatch({ type: actionTypes.ADD_AREA_ON_MAP, area: selectedArea })
}

export const selectAreaOnMap = area => ({
  type: actionTypes.SELECT_AREA_ON_MAP,
  area
})

export const saveAreasOnMap = () => (dispatch, getState) => {
  const newAreas = getState().mapeditor.mapData.areas.list.filter(area => !area.saved)

  dispatch(loading(true))
  dispatch({ type: actionTypes.SAVE_AREAS_ON_MAP, areas: newAreas })

  if (newAreas.length === 0) {
    return
  }

  const position = getPosition(newAreas)

  fetchUrl('saveAreasOnMap', { position, areas: newAreas }).then(
    () => dispatch(loading(false)),
    error => dispatch(getAjaxError(getErrorMessage(error)))
  )
}

export const addAreasOnMap = areas => ({
  type: actionTypes.ADD_AREAS_ON_MAP,
  areas
})

export const deleteAreasOnMap = () => (dispatch, getState) => {
  const areas = getState().mapeditor.mapData.areas.list.filter(area => area.selected)
  const position = getPosition(areas)

  dispatch(loading(true))
  dispatch({ type: actionTypes.DELETE_AREAS_ON_MAP, areas })
  fetchUrl('deleteAreasOnMap', { position, areas }).then(
    () => dispatch(loading(false)),
    error => dispatch(getAjaxError(getErrorMessage(error)))
  )
}

export const deleteAreasOnMapByHashes = hashes => ({
  type: actionTypes.DELETE_AREAS_ON_MAP_BY_HASHES,
  hashes
})

export const hideAreasEditor = () => ({
  type: actionTypes.HIDE_AREAS_EDITOR
})

export const showAreasDialog = dialog => ({
  type: actionTypes.SHOW_AREAS_DIALOG,
  dialog
})

export const hideAreasDialog = () => ({
  type: actionTypes.HIDE_AREAS_DIALOG
})

export const setScale = scale => dispatch => {
  dispatch({ type: actionTypes.SET_SCALE, scale })
  dispatch(updateMap())
}

const handleActionsOnMapByWS = (data, updateText) => dispatch => {
  const tempAction = getActionMock(data.data.action.payload, data.data.action.type, data.data.action.position)
  const { discardedActions } = data.data

  dispatch(saveToHistory({ ...tempAction, ...data.data.action }))
  updateText(data.message, data.user)

  if (discardedActions && discardedActions.length) {
    dispatch(removeFromHistory(discardedActions))
  }
}

export const initMovement = () => (dispatch, getState) => {
  const handler = new WebsocketHandler('cTownAdmin')
  const mapsManagementHandler = new WebsocketHandler('MapsManagement')
  const credentials = handler.getCredentials()

  function checkUser(user) {
    return credentials && credentials.userID !== user.user_id
  }

  function updateText(message, user) {
    if (checkUser(user)) {
      dispatch(updateInfoText(message))
    }
  }

  mapsManagementHandler.setActions({
    // eslint-disable-next-line @typescript-eslint/naming-convention
    MapRenamed: data => {
      dispatch(renameMap(data.data.newName))
    }
  })

  handler.setActions({
    placeObjectsOnTheMap: data => {
      dispatch(handleActionsOnMapByWS(data, updateText))
      dispatch(addObjects(data.data.action.payload[payloadKeys[CREATE_OBJECTS_ACTION_TYPE]]))
    },
    removeObjects: data => {
      dispatch(handleActionsOnMapByWS(data, updateText))
      dispatch(removeObjectsOnMap(data.data.action.payload[payloadKeys[DELETE_OBJECTS_ACTION_TYPE]]))
    },
    moveObjects: data => {
      const objects = data.data.action.payload[payloadKeys[MOVE_OBJECTS_ACTION_TYPE]].map(obj => ({
        hash: obj.hash,
        position: obj.newPosition
      }))

      dispatch(handleActionsOnMapByWS(data, updateText))
      dispatch(moveObjects(objects))
    },
    putParameter: data => {
      const { addedParameter } = data.data.action.payload

      if (addedParameter && addedParameter.name) {
        dispatch(showParameterOnMap(data.data.action.payload[payloadKeys[ADD_PARAMETER_ACTION_TYPE]], true))
      }
      dispatch(handleActionsOnMapByWS(data, updateText))
    },
    removeParameterFromMap: data => {
      dispatch(handleActionsOnMapByWS(data, updateText))
      dispatch(showRemovingParameterOnMap(data.data.action.payload[payloadKeys[REMOVE_PARAMETER_ACTION_TYPE]], true))
    },
    createParameter: data => {
      dispatch(updateParameterList(data.data))
      updateText(data.message, data.user)

      if (data.data.discardedActions && data.data.discardedActions.length) {
        dispatch(removeFromHistory(data.data.discardedActions))
      }
    },
    removeParameter: data => {
      dispatch(updateParameterList(data.data))
      updateText(data.message, data.user)

      if (data.data.discardedActions && data.data.discardedActions.length) {
        dispatch(removeFromHistory(data.data.discardedActions))
      }
    },
    saveAreaInLibrary: data => {
      dispatch({ type: actionTypes.SAVE_AREA_IN_LIBRARY, area: data.data, websocket: true })
      updateText(data.message, data.user)

      if (data.data.discardedActions && data.data.discardedActions.length) {
        dispatch(removeFromHistory(data.data.discardedActions))
      }
    },
    removeAreaFromLibrary: data => {
      dispatch(dispatch({ type: actionTypes.REMOVE_AREA_FROM_LIBRARY, _id: data.data._id, websocket: true }))
      updateText(data.message, data.user)

      if (data.data.discardedActions && data.data.discardedActions.length) {
        dispatch(removeFromHistory(data.data.discardedActions))
      }
    },
    saveAreasOnMap: data => {
      dispatch(handleActionsOnMapByWS(data, updateText))
      dispatch(addAreasOnMap(data.data.action.payload[payloadKeys[ADD_AREAS_ACTION_TYPE]]))
    },
    deleteAreasOnMap: data => {
      const hashes = data.data.action.payload[payloadKeys[DELETE_AREAS_ACTION_TYPE]].map(area => area.hash)

      dispatch(handleActionsOnMapByWS(data, updateText))
      dispatch(deleteAreasOnMapByHashes(hashes))
    },
    setBlocks: data => {
      try {
        const cells = data.data.action.payload[payloadKeys[SET_BLOCK_ACTION_TYPE]]

        dispatch(handleActionsOnMapByWS(data, updateText))
        dispatch(updateCells(cells))
      } catch (e) {
        console.error(e)
      }
    },
    updateObjects: data => {
      dispatch(updateObjectsOrder(data.data.action.payload.newObjects))
    },
    undo: data => {
      if (data.data.discardedActions && data.data.discardedActions.length) {
        dispatch(removeFromHistory(data.data.discardedActions))
      }
    },
    redo: data => {
      if (data.data.discardedActions && data.data.discardedActions.length) {
        dispatch(removeFromHistory(data.data.discardedActions))
      }
    },
    startBackupCreation: () => {
      dispatch(activateOverlay())
    },
    endBackupCreation: () => {
      const { dataLoadingIsDisabled } = getState().mapeditor.mapData

      dispatch(deactivateOverlay())
      if (dataLoadingIsDisabled) {
        dispatch(fetchMapEditorData())
      }
    },
    startLoadingBackup: () => {
      dispatch(activateOverlay())
    },
    endLoadingBackup: () => {
      dispatch(fetchMapEditorData())
    },
    saveLibraryNpc: data => {
      dispatch(npcActions.addLibraryNpc(data.actionData.savedLibraryNpc))
      updateText(data.message, data.user)
    },
    updateLibraryNpc: data => {
      dispatch(npcActions.updateLibraryNpc(data.actionData.updatedLibraryNpc))
      updateText(data.message, data.user)
    },
    removeLibraryNpc: data => {
      dispatch(npcActions.removeLibraryNpc(data.actionData.removedLibraryNpcId))
      updateText(data.message, data.user)
    },
    saveMapNpc: data => {
      const payload = data.data.action.payload[payloadKeys[ADD_NPC_ACTION_TYPE]]

      dispatch(npcActions.placeNpcOnMap(payload))
      dispatch(npcActions.updateNpcQuantity(payload.libraryId, 1))
      dispatch(handleActionsOnMapByWS(data, updateText))
    },
    removeMapNpc: data => {
      const payload = data.data.action.payload[payloadKeys[REMOVE_NPC_ACTION_TYPE]]

      dispatch(npcActions.removeNpcFromMap(payload))
      dispatch(npcActions.updateNpcQuantity(payload.libraryId, -1))
      dispatch(handleActionsOnMapByWS(data, updateText))
    }
  })
}
