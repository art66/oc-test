import { createAction } from 'redux-actions'
import * as a from '../actionTypes/blocking'
import ICell from '../interfaces/ICell'

export const setBlockingTool = createAction(a.SET_BLOCKING_TOOL, (blockingTool: string) => ({ blockingTool }))
export const setCellsBlocking =
  createAction(a.SET_CELLS_BLOCKING, (cells: ICell[], blockAdded: boolean) => ({ cells, blockAdded }))
export const removeBlocksLocally =
  createAction(a.REMOVE_BLOCKS_LOCALLY, (cells: ICell[]) => ({ cells }))
export const updateCells = createAction(a.UPDATE_CELLS, (cells: ICell[]) => ({ cells }))
