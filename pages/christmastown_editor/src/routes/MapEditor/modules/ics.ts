import { createAction } from 'redux-actions'
import * as a from '../actionTypes/ics'

export const toggleICS = createAction(a.TOGGLE_ICS, (show) => ({ show }))
export const setICSInitParams = createAction(a.SET_ICS_INIT_PARAMS, (initParams) => ({ initParams }))
