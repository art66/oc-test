import IMapObject from '../interfaces/IMapObject'
import { handleQuotaExceededError } from '../utils'

export const MAP_OBJECTS = 'mapObjects'

export default class CacheService {

  static removeMapObjectsFromCache(objects: IMapObject[]): void {
    try {
      const lsObjects = (JSON.parse(localStorage.getItem(MAP_OBJECTS)) || []).filter(lsObj =>
        objects.every(obj => obj.hash !== lsObj.hash)
      )

      localStorage.setItem(MAP_OBJECTS, JSON.stringify(lsObjects))
    } catch (e) {
      handleQuotaExceededError(e)
    }
  }
}
