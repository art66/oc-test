import { positionsEqual } from '../../../utils'
import ICell from '../interfaces/ICell'

export const getCellByPosition = (state, position): ICell => state.mapeditor.mapData.blocking.cells
  .find(cell => positionsEqual(position, cell.position)) || {}
export const getOldCells = (state, cells) => state.mapeditor.mapData.blocking.cells
  .filter(item => cells
    .findIndex(cell => cell.position.x === item.position.x && cell.position.y === item.position.y) !== -1)
