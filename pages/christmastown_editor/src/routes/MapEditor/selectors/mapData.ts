import IBuffer from '../interfaces/IBuffer'
import ICellPointer from '../interfaces/ICellPointer'

export const getBuffer = (state): IBuffer => state.mapeditor.mapData.buffer
export const getCellPointer = (state): ICellPointer => state.mapeditor.mapData.cellPointer
export const getCurrentTab = (state): string => state.mapeditor.library.currentTab
