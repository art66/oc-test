export const getPopupObject = state => state.mapeditor.library.popupObject

export const getParameter = (state, parameterID) => {
  return state.mapeditor.library.parameterCategories.find(category => {
    return category.parameters.find(param => param.parameter_id === parameterID)
  })
}

export const getArea = (state, areaID) => {
  return state.mapeditor.library.areas.list.find(area => area._id === areaID)
}

export const getNPC = (state, npcID) => {
  return state.mapeditor.library.libraryNpc.find(npc => npc.libraryId === npcID)
}
