import { put, takeEvery, select } from 'redux-saga/effects'
import md5 from 'js-md5'
import { fetchUrl, setMapPosition, getErrorMessage } from '../../../utils'
import {
  loading,
  removeObjectsOnMap,
  moveObjects,
  addObjects,
  objectAddedToMap,
  moveObjectsAndSave as moveObjectsAndSaveAction,
  unSelectOnMap,
  getAjaxError
} from '../modules/mapeditor'
import {
  fetchParametersOnMapByIdSuccess,
  highlightParameterOnMap,
  searchParameterOnMap,
  synchronizeMapData
} from '../modules/mapData'
import { deactivateOverlay } from '../modules/overlay'
import * as actionTypes from '../actionTypes'
import * as c from '../constants'
import { TAB_BACKUPS, PARAMETER_ABSENT_MESSAGE } from '../../../constants'
import IMapObject from '../interfaces/IMapObject'
import IAction from '../interfaces/IAction'
import IPosition from '../interfaces/IPosition'
import IBuffer from '../interfaces/IBuffer'
import ISize from '../interfaces/ISize'
import { getBuffer, getCurrentTab } from '../selectors/mapData'
import { findSideObjectInGroup, /*getActionMock, */getPosition, clearLocalStorage } from '../utils'
import CacheService from '../services/CacheService'

function* removeObjectsOnMapAndSave(action: IAction<{ objects: IMapObject[] }>) {
  try {
    const { objects } = action.payload
    yield put(loading(true))
    const position = getPosition(objects)
    yield put(removeObjectsOnMap(objects))
    yield fetchUrl('removeObjects', { position, objects })
    CacheService.removeMapObjectsFromCache(objects)
    yield put(loading(false))
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* putObjectsFromBufferOnMap(action: IAction<{ position: IPosition }>) {
  try {
    const { position } = action.payload
    const buffer: IBuffer = yield select(getBuffer)
    const leftObject: IMapObject = findSideObjectInGroup(buffer.objects, 'left')
    const topObject: IMapObject = findSideObjectInGroup(buffer.objects, 'top')
    const leftTopPosition: IPosition = { x: leftObject.position.x, y: topObject.position.y }

    if (buffer.action === c.COPY) {
      const newObjects: IMapObject[] = buffer.objects.map(obj => ({
        ...obj,
        hash: md5(obj.object_id.toString() + Date.now().toString() + Math.random().toString()),
        position: {
          x: position.x + (obj.position.x - leftTopPosition.x),
          y: position.y - (leftTopPosition.y - obj.position.y)
        }
      }))
      const positionOfAction = getPosition(newObjects)

      yield put(loading(true))
      yield put(addObjects(newObjects))
      const json = yield fetchUrl('putObjects', { position: positionOfAction, objects: newObjects })

      yield put(objectAddedToMap(newObjects, json.action.payload.objectsCreated))
      yield put(loading(false))
    } else if (buffer.action === c.CUT) {
      const moveObjects = buffer.objects.map(obj => ({
        hash: obj.hash,
        position: {
          x: position.x + (obj.position.x - leftTopPosition.x),
          y: position.y - (leftTopPosition.y - obj.position.y)
        },
        oldPosition: obj.position
      }))

      yield put(moveObjectsAndSaveAction(moveObjects, buffer.objects))
      yield put(unSelectOnMap())
    }
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

interface IMoveObjectPayload {
  hash: string,
  position: IPosition,
  oldPosition: IPosition
}

function* moveObjectsAndSave(action: IAction<{ moveObjects: IMoveObjectPayload[], oldObjects: any }>) {
  try {
    yield put(loading(true))
    yield put(moveObjects(action.payload.moveObjects))
    const objects = action.payload.moveObjects.map(obj => ({
      hash: obj.hash,
      targetPosition: obj.position,
      position: obj.oldPosition
    }))
    const positionOfAction = getPosition(objects)

    yield fetchUrl('moveObjects', { moveObjects: objects, position: positionOfAction })
    yield put(loading(false))
  } catch (error) {
    console.log(error)
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* fetchParametersOnMapById(action: IAction<{ id: string, modifier: 1 | -1 }>) {
  try {
    yield put(loading(true))

    const { id, modifier } = action.payload
    const { parameters } = yield fetchUrl('findParametersOnMap', { parameterId: id })

    yield put(fetchParametersOnMapByIdSuccess(id, parameters))
    yield put(searchParameterOnMap(id, modifier))
  } catch (e) {
    yield put(getAjaxError(getErrorMessage(e)))
  }
}

function* searchParameter(action: IAction<{ id: string, modifier: 1 | -1 }>) {
  try {
    const { id, modifier } = action.payload
    const store = yield select()
    const { parameters, searchIndex } = store.mapeditor.mapData.parametersSearch[id]

    let nextIndex = searchIndex + modifier

    if (nextIndex < 1) {
      nextIndex = parameters.length
    } else {
      nextIndex = nextIndex > parameters.length || nextIndex <= 1 ? 1 : nextIndex
    }

    const nextParameter = parameters[nextIndex - 1]

    if (!nextParameter) {
      throw new Error(PARAMETER_ABSENT_MESSAGE)
    }

    yield put(highlightParameterOnMap(nextParameter.id, nextParameter.hash, nextIndex))
    setMapPosition(nextParameter.position)
  } catch (e) {
    yield put(getAjaxError(getErrorMessage(e)))
  }
}

function* checkTabsSync(action: IAction<{ mapId: string, mapSize: ISize }>) {
  try {
    const currentTab = yield select(getCurrentTab)
    const { mapId, mapSize } = action.payload
    const { mapChanged, mapEditorData } = yield fetchUrl('checkMapEditorSynchronization', {
      isBackupsRequired: currentTab === TAB_BACKUPS,
      mapID: mapId,
      mapSize
    })

    if (mapChanged) {
      clearLocalStorage()
      yield put(synchronizeMapData(mapEditorData))
    } else {
      yield put(deactivateOverlay())
    }
  } catch (e) {
    yield put(getAjaxError(e))
    yield put(deactivateOverlay())
  }
}

export default function* mapData() {
  yield takeEvery(actionTypes.REMOVE_OBJECTS_ON_MAP_AND_SAVE, removeObjectsOnMapAndSave)
  yield takeEvery(actionTypes.PUT_OBJECTS_FROM_BUFFER_ON_MAP, putObjectsFromBufferOnMap)
  yield takeEvery(actionTypes.MOVE_OBJECTS_AND_SAVE, moveObjectsAndSave)
  yield takeEvery(actionTypes.FETCH_PARAMETERS_ON_MAP_BY_ID, fetchParametersOnMapById)
  yield takeEvery(actionTypes.SEARCH_PARAMETER_ON_MAP, searchParameter)
  yield takeEvery(actionTypes.CHECK_TABS_SYNC, checkTabsSync)
}
