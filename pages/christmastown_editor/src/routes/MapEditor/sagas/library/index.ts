import { put, takeEvery, select } from 'redux-saga/effects'
import * as a from '../../actionTypes/library'
import { fetchUrl, getErrorMessage } from '../../../../utils'
import { setDefaultLayer } from '../../modules/library'
import { getAjaxError } from '../../modules/mapeditor'
import { getPopupObject } from '../../selectors/library'
import IAction from '../../interfaces/IAction'
import { setSuccessfulLoadStatus } from '../../modules/mapData'

function* addTagsToObject(action: IAction<{ objectLibraryId: string; tags: string[] }>) {
  try {
    const { objectLibraryId, tags } = action.payload

    yield fetchUrl('addTags', { objectLibraryId, tags })
    yield put(setSuccessfulLoadStatus())
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* removeObjectTags(action: IAction<{ objectLibraryId: string; tags: string[] }>) {
  try {
    const { objectLibraryId, tags } = action.payload

    yield fetchUrl('removeTags', { objectLibraryId, tags })
    yield put(setSuccessfulLoadStatus())
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* changeDefaultLayer(action: IAction<{ layerName: string }>) {
  try {
    const { layerName } = action.payload
    const object = yield select(getPopupObject)
    const json = yield fetchUrl('updateLibraryObjectsLayer', {
      objects: [{ object_id: object.object_id, layer: layerName }]
    })

    if (json.success) {
      yield put(setDefaultLayer(layerName))
    }
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

export default function* library() {
  yield takeEvery(a.ADD_TAGS_TO_OBJECT, addTagsToObject)
  yield takeEvery(a.REMOVE_OBJECT_TAGS, removeObjectTags)
  yield takeEvery(a.CHANGE_DEFAULT_LAYER, changeDefaultLayer)
}
