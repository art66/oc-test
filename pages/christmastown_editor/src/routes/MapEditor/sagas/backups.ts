import { put, takeEvery } from 'redux-saga/effects'
import { fetchUrl, getErrorMessage } from '../../../utils'
import * as actionTypes from '../actionTypes/backups'
import {
  saveBackup,
  changeControlsStatus,
  removeBackupFromList,
  setBackups,
  saveBackupName,
  toggleConfirmation,
  changeControlsState,
  isWrongName
} from '../modules/backups'
import { getAjaxError } from '../modules/mapeditor'
import { LOAD_BACKUP_CONFIRM_TYPE } from '../constants/backups'
import { setSuccessfulLoadStatus } from '../modules/mapData'

function* fetchBackupsData() {
  try {
    const json = yield fetchUrl('getBackups')

    yield put(setBackups(json.versions))
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* addBackup(action: any) {
  try {
    const json = yield fetchUrl('createBackup', { name: action.payload.name })

    yield put(setSuccessfulLoadStatus())

    if (json.success) {
      yield put(saveBackup(json.version))
      yield put(changeControlsStatus('default'))
    } else {
      yield put(changeControlsState(false, json.message))
    }
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* removeBackup(action: any) {
  try {
    const json = yield fetchUrl('deleteBackup', { backupId: action.payload.ID })

    yield put(setSuccessfulLoadStatus())

    if (json.success) {
      yield put(removeBackupFromList(action.payload.ID))
    }
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* renameBackup(action: any) {
  try {
    const json = yield fetchUrl('renameBackup', { backupId: action.payload.ID, name: action.payload.name })

    yield put(setSuccessfulLoadStatus())

    if (json.success) {
      yield put(saveBackupName(action.payload.ID, json.version.name))
    } else {
      yield put(isWrongName(action.payload.ID, true, json.message))
    }
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* loadBackup(action: any) {
  try {
    const json = yield fetchUrl('loadBackup', { backupId: action.payload.ID })

    yield put(setSuccessfulLoadStatus())

    if (json.success) {
      yield put(toggleConfirmation(action.payload.ID, { enabled: false, type: LOAD_BACKUP_CONFIRM_TYPE }))
    }
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

export default function* backups() {
  yield takeEvery(actionTypes.ADD_BACKUP, addBackup)
  yield takeEvery(actionTypes.REMOVE_BACKUP, removeBackup)
  yield takeEvery(actionTypes.FETCH_BACKUPS_DATA, fetchBackupsData)
  yield takeEvery(actionTypes.RENAME_BACKUP, renameBackup)
  yield takeEvery(actionTypes.LOAD_BACKUP, loadBackup)
}
