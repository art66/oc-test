import { put, takeEvery, select } from 'redux-saga/effects'
import md5 from 'js-md5'
import { fetchUrl, positionsEqual, getErrorMessage } from '../../../utils'
import { getAjaxError } from '../modules/mapeditor'
import { setSuccessfulLoadStatus } from '../modules/mapData'
import { placeNpcOnMap, removeNpcFromMap } from '../modules/npc'
import * as actionTypes from '../actionTypes/npc'
import {
  MESSAGE_NPC_PARAMETER_SAME_CELL,
  MAX_GRINCH_PER_MAP,
  MAX_SANTA_PER_MAP,
  MESSAGE_MAX_GRINCH_PER_MAP,
  MESSAGE_MAX_SANTA_PER_MAP
} from '../constants'
import { NPC_TYPE_SANTA, NPC_TYPE_GRINCH } from '../../../constants'
import IAction from '../interfaces/IAction'
import INpc from '../../../interfaces/INpc'
import IPosition from '../interfaces/IPosition'

function* addNpcSaga(action: IAction<{ selectedNpc: INpc, position: IPosition }>) {
  try {
    const state = yield select()
    const { npc, parameters, npcLimit } = state.mapeditor.mapData
    const { selectedNpc, position } = action.payload
    const newNpc = {
      libraryId: selectedNpc.libraryId,
      name: selectedNpc.name,
      hash: md5(selectedNpc.libraryId.toString() + Date.now().toString() + Math.random().toString()),
      radius: selectedNpc.radius,
      speed: selectedNpc.speed,
      type: selectedNpc.type,
      color: selectedNpc.color,
      position
    }
    const movingNpc = npc.filter(npcUser => npcUser.radius > 0 || npcUser.speed > 0)
    const selectedNpcIsMoving = selectedNpc.radius > 0 || selectedNpc.speed > 0

    const cellBusy = parameters.some(p => positionsEqual(p.position, position))
      || npc.some(n => positionsEqual(n.position, position))

    if (cellBusy) {
      return yield put(getAjaxError(MESSAGE_NPC_PARAMETER_SAME_CELL))
    }

    if (movingNpc.length >= npcLimit && selectedNpcIsMoving) {
      return yield put(getAjaxError(`Maximum ${npcLimit} NPC allowed per map`))
    }

    if (
      newNpc.type.id === NPC_TYPE_SANTA
      && npc.filter(item => item.type.id === NPC_TYPE_SANTA).length >= MAX_SANTA_PER_MAP
    ) {
      return yield put(getAjaxError(MESSAGE_MAX_SANTA_PER_MAP))
    }

    if (
      newNpc.type.id === NPC_TYPE_GRINCH
      && npc.filter(item => item.type.id === NPC_TYPE_GRINCH).length >= MAX_GRINCH_PER_MAP
    ) {
      return yield put(getAjaxError(MESSAGE_MAX_GRINCH_PER_MAP))
    }

    yield put(placeNpcOnMap(newNpc))
    yield fetchUrl('saveMapNpc', { npc: newNpc })
    yield put(setSuccessfulLoadStatus())
  } catch (e) {
    yield put(getAjaxError(getErrorMessage(e)))
  }
}

function* removeNpcSaga(action: IAction<{ npc: INpc }>) {
  try {
    const { npc } = action.payload

    yield put(removeNpcFromMap(npc))
    yield fetchUrl('removeMapNpc', { hash: npc.hash })

  } catch (e) {
    yield put(getAjaxError(getErrorMessage(e)))
  }
}

export default function* npc() {
  yield takeEvery(actionTypes.ADD_NPC, addNpcSaga)
  yield takeEvery(actionTypes.REMOVE_NPC, removeNpcSaga)
}
