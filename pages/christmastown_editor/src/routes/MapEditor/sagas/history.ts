import { put, takeEvery, select } from 'redux-saga/effects'
import * as actionTypes from '../actionTypes/history'
import { fetchUrl } from '../../../utils'
import { isOnTopOfUser, handleQuotaExceededError } from '../utils'
import { SHOW_REMOVING_PARAMETER_ON_MAP, DELETE_AREAS_ON_MAP_BY_HASHES, MOVE_OBJECTS } from '../actionTypes'
import { updateParametersCount } from '../modules/library'
import * as types from '../constants/history'
import {
  loading,
  showObjectOnMap,
  showParameterOnMap,
  addAreasOnMap,
  removeObjectsOnMap,
  addObjects
} from '../modules/mapeditor'
import { updateObjectsOrder } from '../modules/layers'
import { updateCells } from '../modules/blocking'
import { placeNpcOnMap, removeNpcFromMap, updateNpcQuantity } from '../modules/npc'
import { getArea, getNPC, getParameter } from '../selectors/library'
import IMapObject from '../interfaces/IMapObject'
import IHistoryObject from '../interfaces/IHistoryObject'
import IParameter from '../interfaces/IParameter'
import IBlocking from '../interfaces/IBlocking'
import IPosition from '../interfaces/IPosition'
import IArea from '../interfaces/IArea'
import IAction from '../interfaces/IAction'
import INpc from '../../../interfaces/INpc'

function* deleteObjectsOnMap(objects: IMapObject[]) {
  yield put(removeObjectsOnMap(objects))
  yield put(loading(false))
}

function* createObjectsOnMap(objects: IMapObject[]) {
  if (objects.length === 1) {
    yield put(showObjectOnMap(objects[0]))
  } else {
    yield put(addObjects(objects))
  }
  yield put(loading(false))
}

function* moveObjectsOnMap(objects: IHistoryObject[], actionType: string) {
  const movedObjects = objects.map(item => ({
    hash: item.hash,
    position: actionType === 'undo' ? item.oldPosition : item.newPosition
  }))

  yield put({ type: MOVE_OBJECTS, payload: { moveObjects: movedObjects } })
}

function* removeParameterOnMap(parameter: IParameter) {
  const libParameter = yield select(getParameter, parameter.parameter_id)

  if (libParameter) {
    yield put({ parameter, type: SHOW_REMOVING_PARAMETER_ON_MAP })
    yield put(updateParametersCount(parameter.parameter_id, libParameter.quantity - 1))
  }
  yield put(loading(false))
  try {
    let lsParameters = JSON.parse(localStorage.getItem('mapParameters'))

    lsParameters = JSON.stringify(lsParameters.filter(item => item.hash !== parameter.hash))
    localStorage.setItem('mapParameters', lsParameters)
  } catch (e) {
    handleQuotaExceededError(e)
  }
}

function* changeObjectsOrderOnMap(objects: IMapObject[]) {
  yield put(updateObjectsOrder(objects))

  try {
    let lsObjects = JSON.parse(localStorage.getItem('mapObjects'))

    lsObjects = lsObjects.map(object => {
      const changedObject = objects.find(item => item.hash === object.hash)

      if (changedObject) {
        return {
          ...object,
          layer: changedObject.layer,
          createdAt: changedObject.createdAt,
          order: changedObject.createdAt * changedObject.layer.number,
          onTopOfUser: isOnTopOfUser(changedObject.layer.name)
        }
      }
      return object
    })
    localStorage.setItem('mapObjects', JSON.stringify(lsObjects))
  } catch (e) {
    handleQuotaExceededError(e)
  }
}

function* unsetBlockOnMap(blocks: { oldBlocking: IBlocking; blocking: IBlocking; position: IPosition }[]) {
  const blocksArr = blocks.map(item => {
    let copyBlock = { ...item.blocking }
    const newBlockProps = Object.keys(copyBlock)
    const oldBlockProps = Object.keys(item.oldBlocking)
    const props = newBlockProps.filter(
      prop => oldBlockProps.indexOf(prop) === -1 || copyBlock[prop] !== item.oldBlocking[prop]
    )

    props.map(prop => {
      copyBlock = { ...copyBlock, [prop]: !copyBlock[prop] }
      return prop
    })
    return {
      blocking: copyBlock,
      position: item.position
    }
  })

  yield put(updateCells(blocksArr))
}

function* setBlockOnMap(blocks: { blocking: IBlocking; position: IPosition }[]) {
  const blocksArr = blocks.map(item => {
    return {
      blocking: item.blocking,
      position: item.position
    }
  })

  yield put(updateCells(blocksArr))
}

function* addAreaOnMap(areas: IArea[]) {
  const areaID = areas.length ? areas[0]._id : null
  const libraryArea = yield select(getArea, areaID)

  if (libraryArea) {
    yield put(addAreasOnMap(areas))
  }
}

function* addParameterOnMap(parameter: IParameter) {
  const libParameter = yield select(getParameter, parameter.parameter_id)

  if (libParameter) {
    yield put(showParameterOnMap(parameter))
    yield put(updateParametersCount(parameter.parameter_id, libParameter.quantity + 1))
  }
  yield put(loading(false))
}

function* addNPCOnMap(NPC: INpc) {
  const libraryNPC = yield select(getNPC, NPC.libraryId)

  if (libraryNPC) {
    yield put(placeNpcOnMap(NPC))
    yield put(updateNpcQuantity(NPC.libraryId, 1))
  }
}

function* undoAction(action: IAction<any>) {
  const { type, payload } = action.payload.action

  switch (type) {
    case types.CREATE_OBJECTS_ACTION_TYPE:
      yield deleteObjectsOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.CREATE_OBJECTS_ACTION_TYPE]])
      break
    case types.MOVE_OBJECTS_ACTION_TYPE:
      yield moveObjectsOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.MOVE_OBJECTS_ACTION_TYPE]], 'undo')
      break
    case types.DELETE_OBJECTS_ACTION_TYPE:
      yield createObjectsOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.DELETE_OBJECTS_ACTION_TYPE]])
      break
    case types.ADD_PARAMETER_ACTION_TYPE:
      yield removeParameterOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.ADD_PARAMETER_ACTION_TYPE]])
      break
    case types.ADD_AREAS_ACTION_TYPE:
      const hashes = payload[types.OBJECTS_KEY_IN_PAYLOAD[types.ADD_AREAS_ACTION_TYPE]].map(item => item.hash)

      yield put({ hashes, type: DELETE_AREAS_ON_MAP_BY_HASHES })
      break
    case types.UPDATE_OBJECTS_ACTION_TYPE:
      yield changeObjectsOrderOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.UPDATE_OBJECTS_ACTION_TYPE]])
      break
    case types.DELETE_AREAS_ACTION_TYPE:
      yield addAreaOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.DELETE_AREAS_ACTION_TYPE]])
      break
    case types.REMOVE_PARAMETER_ACTION_TYPE:
      yield addParameterOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.REMOVE_PARAMETER_ACTION_TYPE]])
      break
    case types.SET_BLOCK_ACTION_TYPE:
      yield unsetBlockOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.SET_BLOCK_ACTION_TYPE]])
      break
    case types.ADD_NPC_ACTION_TYPE:
      const payloadAddNpcUndo = payload[types.OBJECTS_KEY_IN_PAYLOAD[types.ADD_NPC_ACTION_TYPE]]

      yield put(removeNpcFromMap(payloadAddNpcUndo))
      yield put(updateNpcQuantity(payloadAddNpcUndo.libraryId, -1))
      break
    case types.REMOVE_NPC_ACTION_TYPE:
      yield addNPCOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.REMOVE_NPC_ACTION_TYPE]])
      break
  }

  yield fetchUrl('undo', { actionId: action.payload.action.actionId })
}

function* redoAction(action: IAction<any>) {
  const { type, payload } = action.payload.action

  switch (type) {
    case types.CREATE_OBJECTS_ACTION_TYPE:
      yield createObjectsOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.CREATE_OBJECTS_ACTION_TYPE]])
      break
    case types.MOVE_OBJECTS_ACTION_TYPE:
      yield moveObjectsOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.MOVE_OBJECTS_ACTION_TYPE]], 'redo')
      break
    case types.DELETE_OBJECTS_ACTION_TYPE:
      yield deleteObjectsOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.DELETE_OBJECTS_ACTION_TYPE]])
      break
    case types.ADD_PARAMETER_ACTION_TYPE:
      yield addParameterOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.ADD_PARAMETER_ACTION_TYPE]])
      break
    case types.ADD_AREAS_ACTION_TYPE:
      yield addAreaOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.ADD_AREAS_ACTION_TYPE]])
      break
    case types.UPDATE_OBJECTS_ACTION_TYPE:
      yield changeObjectsOrderOnMap(payload.newObjects)
      break
    case types.DELETE_AREAS_ACTION_TYPE:
      const hashes = payload[types.OBJECTS_KEY_IN_PAYLOAD[types.DELETE_AREAS_ACTION_TYPE]].map(item => item.hash)

      yield put({ hashes, type: DELETE_AREAS_ON_MAP_BY_HASHES })
      break
    case types.REMOVE_PARAMETER_ACTION_TYPE:
      yield removeParameterOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.REMOVE_PARAMETER_ACTION_TYPE]])
      break
    case types.SET_BLOCK_ACTION_TYPE:
      yield setBlockOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.SET_BLOCK_ACTION_TYPE]])
      break
    case types.ADD_NPC_ACTION_TYPE:
      yield addNPCOnMap(payload[types.OBJECTS_KEY_IN_PAYLOAD[types.ADD_NPC_ACTION_TYPE]])
      break
    case types.REMOVE_NPC_ACTION_TYPE:
      const payloadRemoveNpcRedo = payload[types.OBJECTS_KEY_IN_PAYLOAD[types.REMOVE_NPC_ACTION_TYPE]]

      yield put(removeNpcFromMap(payloadRemoveNpcRedo))
      yield put(updateNpcQuantity(payloadRemoveNpcRedo.libraryId, -1))
      break
  }

  yield fetchUrl('redo', { actionId: action.payload.action.actionId })
}

export default function* history() {
  yield takeEvery(actionTypes.REDO_ACTION, redoAction)
  yield takeEvery(actionTypes.UNDO_ACTION, undoAction)
}
