import { put, takeEvery } from 'redux-saga/effects'
import { fetchUrl, getErrorMessage } from '../../../utils'
import { updateCells } from '../modules/blocking'
import * as a from '../actionTypes/blocking'
import ICell from '../interfaces/ICell'
import IAction from '../interfaces/IAction'
import { getPosition } from '../utils'
import { loading, getAjaxError } from '../modules/mapeditor'

function* setCellsBlocking(action: IAction<{ cells: ICell[], blockAdded: boolean }>) {
  try {
    const { cells, blockAdded } = action.payload
    const position = getPosition(cells)

    yield put(loading(true))
    yield put(updateCells(cells))
    yield fetchUrl('setBlocks', { blockAdded, position, blocks: cells })
    yield put(loading(false))
  } catch (error) {
    yield put(getAjaxError(getErrorMessage(error)))
  }
}

function* removeBlocksLocally(action: IAction<{ cells: ICell[] }>) {
  const { cells } = action.payload

  yield put(updateCells(cells))
}

export default function* blocking() {
  yield takeEvery(a.SET_CELLS_BLOCKING, setCellsBlocking)
  yield takeEvery(a.REMOVE_BLOCKS_LOCALLY, removeBlocksLocally)
}
