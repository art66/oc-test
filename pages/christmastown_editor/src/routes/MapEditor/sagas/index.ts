import { all } from 'redux-saga/effects'
import mapData from './mapData'
import blocking from './blocking'
import history from './history'
import library from './library'
import backups from './backups'
import npc from './npc'

export default function* rootSaga() {
  yield all([mapData(), blocking(), history(), library(), backups(), npc()])
}
