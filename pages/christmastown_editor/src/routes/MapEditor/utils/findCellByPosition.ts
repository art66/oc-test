import IPosition from '../interfaces/IPosition'
import ICell from '../interfaces/ICell'
import { positionsEqual } from '../../../utils'

const emptyCell: ICell = {
  position: { x: 0, y: 0 },
  blocking: {},
  empty: true
}

const findCellByPosition = (cells: ICell[], position: IPosition): ICell => {
  return cells.find(cell => positionsEqual(position, cell.position)) || emptyCell
}

export default findCellByPosition
