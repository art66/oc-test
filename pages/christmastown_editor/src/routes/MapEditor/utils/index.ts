import orderBy from 'lodash.orderby'
import md5 from 'js-md5'
import { getCookie } from '@torn/shared/utils'
import IPosition from '../interfaces/IPosition'
import IRectangle from '../interfaces/IRectangle'
import IMapObject from '../interfaces/IMapObject'
import { CELL_SIZE, ADMIN_MAP_VIEWPORT_X, ADMIN_MAP_VIEWPORT_Y } from '../../../constants'
import { HIGHEST_LAYER, HIGH_LAYER } from '../constants/layers'
import {
  FULLSCREEN_MODE,
  FULLSCREEN_MODE_COOKIE,
  LOCAL_STORAGE_MAP_OBJECTS,
  LOCAL_STORAGE_MAP_PARAMETERS,
  QUOTA_EXCEEDED_ERR_CODE,
  QUOTA_EXCEEDED_ERR_CODE_FF,
  QUOTA_EXCEEDED_ERR_NAME_FF
} from '../constants'

export { default as findCellByPosition } from './findCellByPosition'
export { default as findNearCellByDirection } from './findNearCellByDirection'

export function isInRectangle(position: IPosition, rectangle: IRectangle) {
  const { x, y } = position
  const { leftTop, rightBottom } = rectangle

  return x >= leftTop.x && y <= leftTop.y && x <= rightBottom.x && y >= rightBottom.y
}

type TSide = 'left' | 'top' | 'right' | 'bottom'

export function findSideObjectInGroup(objects: IMapObject[], side: TSide): IMapObject {
  let orderType, fieldBy

  if (side === 'left') {
    orderType = 'asc'
    fieldBy = 'position.x'
  }
  if (side === 'top') {
    orderType = 'desc'
    fieldBy = 'position.y'
  }
  if (side === 'right') {
    orderType = 'desc'
    fieldBy = 'rightPoint'
  }
  if (side === 'bottom') {
    orderType = 'asc'
    fieldBy = 'bottomPoint'
  }
  const objectsArr = objects.map(obj => ({
    ...obj,
    rightPoint: obj.position.x + obj.width / CELL_SIZE,
    bottomPoint: obj.position.y - obj.height / CELL_SIZE
  }))

  return orderBy(objectsArr, [fieldBy], [orderType])[0]
}

export const isOnTopOfUser = layerName => {
  return layerName === HIGH_LAYER || layerName === HIGHEST_LAYER
}

export const comparator = (first, second) => {
  return first > second ? -1 : 1
}

export const getDate = timestamp => {
  const date = new Date(timestamp * 1000)
  const hoursAmount = date.getUTCHours()
  const minutesAmount = date.getUTCMinutes()
  const day = date.getUTCDate()
  const month = date.getUTCMonth()
  const year = date.getUTCFullYear()
  const weekday = date.getUTCDay()
  const hours = hoursAmount < 10 ? `0${hoursAmount}` : hoursAmount
  const minutes = minutesAmount < 10 ? `0${minutesAmount}` : minutesAmount

  return {
    minutes,
    hours,
    day,
    year,
    month: month + 1,
    weekday: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][weekday]
  }
}

export const getActionMock = (payload, actionType, position) => {
  const timestamp = Math.round(new Date().getTime() / 1000)

  return {
    timestamp,
    position,
    payload,
    actionId: md5(JSON.stringify(payload) + Date.now().toString()),
    type: actionType,
    user: {
      userID: getCookie('uid'),
      playername: getCookie('sso_wiki_user')
    },
    date: getDate(timestamp),
    status: 'pending',
    isReverted: false
  }
}

export const getPosition = items => {
  const fieldName = items[0].position ? 'position' : 'newPosition'
  const maxX = Math.max(...items.map(item => item[fieldName].x))
  const maxY = Math.max(...items.map(item => item[fieldName].y))
  const minX = Math.min(...items.map(item => item[fieldName].x))
  const minY = Math.min(...items.map(item => item[fieldName].y))

  return {
    x: Math.round(minX + (maxX - minX) / 2),
    y: Math.round(minY + (maxY - minY) / 2)
  }
}

export const getFormattedDate = date => {
  const currentDate = new Date()
  const currentDay = currentDate.getUTCDate()
  const currentMonth = currentDate.getUTCMonth() + 1
  const currentYear = currentDate.getUTCFullYear()
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

  if (date.day !== currentDay || date.month !== currentMonth || date.year !== currentYear) {
    return `${monthNames[date.month - 1]} ${date.day}`
  }
  return `${date.hours}:${date.minutes}`
}

export const capitalizeString = str => {
  return str && str.charAt(0).toUpperCase() + str.substr(1)
}

export const getElementPositionRelativePage = el => {
  const rect = el.getBoundingClientRect()
  const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop

  return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

export const getViewportWidthInCells = () => Math.floor(window.innerWidth / CELL_SIZE)

export const getViewportHeightInCells = () => Math.floor(window.innerHeight / CELL_SIZE)

export const getMapSizeInitMap = () => {
  const CTMode = getCookie(FULLSCREEN_MODE_COOKIE)

  return CTMode === FULLSCREEN_MODE ?
    { width: getViewportWidthInCells(), height: getViewportHeightInCells() } :
    { width: ADMIN_MAP_VIEWPORT_X, height: ADMIN_MAP_VIEWPORT_Y }
}

export const setCookie = (name, value, days) => {
  let expires = ''

  if (days) {
    const date = new Date()

    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
    expires = `; ${expires}=${date.toUTCString()}`
  }

  document.cookie = `${name}=${value || ''}${expires}; path=/`
}

export const clearLocalStorage = () => {
  try {
    localStorage.removeItem(LOCAL_STORAGE_MAP_PARAMETERS)
    localStorage.removeItem(LOCAL_STORAGE_MAP_OBJECTS)
  } catch (e) {
    console.log(e)
  }
}

export const isQuotaExceeded = e => {
  if (e && e.code) {
    switch (e.code) {
      case QUOTA_EXCEEDED_ERR_CODE:
        return true
      case QUOTA_EXCEEDED_ERR_CODE_FF:
        if (e.name === QUOTA_EXCEEDED_ERR_NAME_FF) {
          return true
        }
        break
      default:
        return false
    }
  }
}

export const handleQuotaExceededError = e => {
  if (isQuotaExceeded(e)) {
    clearLocalStorage()
  }
}

export const stringLengthIsInInterval = (text: string, minLength: number, maxLength: number) => {
  return text.length >= minLength && text.length <= maxLength
}
