import orderBy from 'lodash.orderby'
import { isOnTopOfUser } from './index'
import { LAYERS_NAMES_ARR } from '../constants/layers'

export const updateObjectsOrderInList = (oldObjects, newObjects) => {
  return orderBy(oldObjects.map(object => {
    const updatedObject = newObjects.find(newObj => object.hash === newObj.hash)

    if (updatedObject) {
      return {
        ...object,
        ...updatedObject,
        onTopOfUser: isOnTopOfUser(updatedObject.layer.name),
        order: updatedObject.layer.number * updatedObject.createdAt
      }
    }
    return object
  }), ['order'], ['desc'])
}

export const sortLayers = (first, second) => {
  return first.layer.number * first.createdAt > second.layer.number * second.createdAt ? -1 : 1
}

export const updateLayers = (oldLayers, newObjects) => {
  const layersObj = {}

  let objectsInLayers = []

  LAYERS_NAMES_ARR.forEach(name => {
    objectsInLayers = [...objectsInLayers, ...oldLayers[name]]
  })

  const updatedObjects = updateObjectsOrderInList(objectsInLayers, newObjects)

  LAYERS_NAMES_ARR.forEach(name => {
    layersObj[name] = orderBy(updatedObjects.filter(obj => obj.layer.name === name), ['order'], ['desc'])
  })

  return layersObj
}
