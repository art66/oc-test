import ICell from '../interfaces/ICell'
import { positionsEqual } from '../../../utils'

const replaceCells = (cells: ICell[], newCells: ICell[]): ICell[] => {
  newCells.forEach(cell => {
    const cellIndex = cells.findIndex(c => positionsEqual(c.position, cell.position))

    if (cellIndex === -1) {
      cells.push(cell)
    } else {
      cells[cellIndex] = { ...cells[cellIndex], ...cell }
    }
  })

  return cells
}

export default replaceCells
