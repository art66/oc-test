import { findCellByPosition } from './'
import { getNextPositionByDirection } from '@torn/christmas-town/src/routes/ChristmasTown/utils'
import ICell from '../interfaces/ICell'
import IPosition from '../interfaces/IPosition'

const emptyCell: ICell = {
  position: { x: 0, y: 0 },
  blocking: {},
  empty: true
}

const findNearCellByDirection = (cells: ICell[], position: IPosition, direction: string): ICell => {
  const nearPosition = getNextPositionByDirection(position, direction)

  return findCellByPosition(cells, nearPosition) || { ...emptyCell, position: nearPosition }
}

export default findNearCellByDirection
