import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import reducer from './reducers/index'
import rootStore from '../../store/createStore'
import Preloader from '@torn/shared/components/Preloader'
import CTStore from '../../../../christmastown/src/store/createStore'
import { injectReducer as CTInjectReducer } from '../../../../christmastown/src/store/helpers/rootReducer'

const PreloaderComponent = () => <Preloader />

const MapEditorRoute = Loadable({
  loader: async() => {
    // tslint:disable-next-line:space-in-parens
    const MapEditor = await import(/* webpackChunkName: "mapEditor" */ './containers/MapEditorContainer')

    return MapEditor
  },
  render(asyncComponent: any, { isSPA }: { isSPA: boolean }) {
    const { default: MapEditor } = asyncComponent
    const store = isSPA ? CTStore : rootStore
    const reducerInjector = isSPA ? CTInjectReducer : injectReducer

    reducerInjector(store, { reducer, key: 'mapeditor' })

    return <MapEditor />
  },
  loading: PreloaderComponent
})

export default MapEditorRoute
