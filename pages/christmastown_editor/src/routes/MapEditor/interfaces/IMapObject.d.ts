import IObject from './IObject'
import IPosition from './IPosition'

interface IMapObject extends IObject {
  hash: string,
  position: IPosition,
  onTopOfUser: boolean,
  created: number,
  layer: {
    name: string,
    number: number
  },
  createdAt: number
}

export default IMapObject
