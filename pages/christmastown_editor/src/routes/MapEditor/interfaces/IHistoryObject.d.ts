import IPosition from './IPosition'
import IMapObject from './IMapObject'

export default interface IHistoryObject extends IMapObject {
  oldPosition: IPosition,
  newPosition: IPosition
}
