export default interface IUserAction {
  actionId: string | number
  type: string
  user: {
    userID: string,
    playername: string
  }
  timestamp: number
  position: {
    x: number
    y: number
  }
  date: any
  status: string
  isReverted: boolean
  payload: any
}
