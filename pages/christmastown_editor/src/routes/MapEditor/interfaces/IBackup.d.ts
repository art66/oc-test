export default interface IBackup {
  backupId: string
  name: string
  timestamp: number
  editing?: boolean
  confirmation?: {
    enabled: boolean
    type: string
  },
  active?: boolean,
  wrongName?: boolean,
  message?: string
}
