import IPosition from './IPosition'

export default interface IParameter {
  code: string
  color: string
  created: number | null
  hash: string
  parameter_id: number
  position: IPosition
}
