export default interface IToggleLayer {
  iconName: string,
  layer: string,
  label: string,
  tab: string
}
