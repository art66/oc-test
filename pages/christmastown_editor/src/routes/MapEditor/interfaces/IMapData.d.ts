import ICellPointer from './ICellPointer'
import IPosition from './IPosition'
import IEndpoints from './IEndpoints'
import IParameter from './IParameter'
import IMapObject from './IMapObject'
import IArea from './IArea'
import IBuffer from './IBuffer'
import IBlocking from './IBlocking'

export default interface IMapData {
  viewToggleLayers: object,
  scale: number
  loading: boolean
  loadStatus: string
  cellPointer: ICellPointer
  mapPosition: IPosition
  mapSize: {
    mapWidth: number
    mapHeight: number
  }
  endpoints: IEndpoints
  fullscreenMode: boolean,
  showCoordinates: boolean
  selectedParameter: IParameter
  selectedObjects: IMapObject[]
  objects: IMapObject[]
  parameters: IParameter[]
  buffer: IBuffer
  areas: {
    list: IArea[]
  }
  blocking: IBlocking
}
