import IObject from './IObject'

export default interface ILibraryObject extends IObject {
  quantity: number
  tags: string[]
  position: {
    left: number
    top: number
  },
  layer: {
    name: string,
    number: number
  },
  order: number
}
