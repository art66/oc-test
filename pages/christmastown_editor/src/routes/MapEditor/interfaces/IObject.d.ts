export default interface IObject {
  object_id: string
  objectLibraryId?: string
  category: string
  type: number
  width: number
  height: number
  quantity?: number
  inexpressive?: boolean
  animation?: {
    name: string
  }
}
