import IPosition from './IPosition'

export default interface IArea {
  _id: number
  color: string
  created: number
  hash: string
  name: string
  position: IPosition
}
