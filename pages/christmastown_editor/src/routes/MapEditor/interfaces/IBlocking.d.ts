import ICell from './ICell'

export default interface IBlocking {
  blockingTool: string
  cells: ICell[]
}
