import IPosition from './IPosition'

export default interface ICell {
  position: IPosition
  blocking: {
    top?: boolean
    right?: boolean
    bottom?: boolean
    left?: boolean
    leftTop?: boolean
    rightTop?: boolean
    leftBottom?: boolean
    rightBottom?: boolean
  }
  empty?: boolean
}
