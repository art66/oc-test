export default interface IMapSize {
  width: number,
  height: number
}
