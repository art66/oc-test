export default interface IObjectCategory {
  name: string
  background: string
  layer?: string
  categoryLayerIsModified?: boolean
}
