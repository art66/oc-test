export default interface IPositionInPixels {
  top: number
  left: number
}
