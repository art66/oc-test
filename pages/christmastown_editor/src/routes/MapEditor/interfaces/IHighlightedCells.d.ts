export default interface IHighlightedCells {
  type: 'parameter' | 'npc',
  id: string | number,
  hash: string
}
