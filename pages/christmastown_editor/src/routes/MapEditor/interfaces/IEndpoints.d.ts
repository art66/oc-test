import IPosition from './IPosition'

export default interface IEndpoints {
  leftTop: IPosition
  rightBottom: IPosition
}
