import IMapObject from './IMapObject'

export default interface IBuffer {
  action?: 'copy' | 'cut'
  objects?: IMapObject[]
}
