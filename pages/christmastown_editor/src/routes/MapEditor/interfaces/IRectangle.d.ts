import IPosition from './IPosition'

export default interface IRectangle {
  leftTop: IPosition
  rightBottom: IPosition
}
