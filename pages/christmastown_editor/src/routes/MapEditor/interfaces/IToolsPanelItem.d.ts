export default interface IToolsPanelItem {
  iconName: string,
  libraryTab: string,
  customPreset?: string,
  label: string
}
