interface ILayer {
  hash: string,
  object_id: string,
  category?: string,
  type: number | string,
  path?: string,
  onTopOfUser?: boolean,
  created?: number,
  order?: number,
  layer?: {
    number: number,
    name: string
  }
}

export default ILayer
