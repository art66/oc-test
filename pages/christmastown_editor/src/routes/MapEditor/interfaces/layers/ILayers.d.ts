import { HIGHEST_LAYER, HIGH_LAYER, LOW_LAYER, LOWEST_LAYER } from '../../constants/layers'
import ILayer from './ILayer'

interface ILayers {
  [HIGHEST_LAYER]: ILayer[],
  [HIGH_LAYER]: ILayer[],
  [LOW_LAYER]: ILayer[],
  [LOWEST_LAYER]: ILayer[]
}

export default ILayers
