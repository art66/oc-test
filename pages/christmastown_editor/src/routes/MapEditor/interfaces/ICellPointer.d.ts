export default interface ICellPointer {
  show: boolean
  top: number
  left: number
  x: number
  y: number
  width: number
  height: number
}
