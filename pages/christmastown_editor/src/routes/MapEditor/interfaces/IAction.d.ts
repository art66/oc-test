export default interface IAction<TPayload> {
  type: string
  payload: TPayload
}
