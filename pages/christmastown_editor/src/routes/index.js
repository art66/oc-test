import MapEditorRoute from './MapEditor'
import ParameterEditorRoute from './ParameterEditor'
import NpcEditorRoute from './NpcEditor'

export { MapEditorRoute, ParameterEditorRoute, NpcEditorRoute }
