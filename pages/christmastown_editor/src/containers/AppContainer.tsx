import React, { Component } from 'react'
import { Store } from 'redux'
import { HashRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import CoreLayout from '../layouts/CoreLayout'
import { MapEditorRoute, ParameterEditorRoute, NpcEditorRoute } from '../routes'

interface IProps {
  store: Store
}

class AppContainer extends Component<IProps, any> {
  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <HashRouter>
          <CoreLayout>
            <Switch>
              <Route exact={true} path='/' component={MapEditorRoute} />
              <Route path='/parametereditor' component={ParameterEditorRoute} />
              <Route path='/npceditor' component={NpcEditorRoute} />
            </Switch>
          </CoreLayout>
        </HashRouter>
      </Provider>
    )
  }
}

export default AppContainer
