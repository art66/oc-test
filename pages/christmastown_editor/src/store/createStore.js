import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import { reduxLogger } from './middleware'
import makeRootReducer from './rootReducer'
import rootSaga from './rootSaga'
const sagaMiddleware = createSagaMiddleware()

const rootStore = (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [thunk, sagaMiddleware]

  if (__DEV__ && (!window.getCookie || !['1990187', '2313413'].includes(window.getCookie('uid')))) {
    middleware.push(reduxLogger)
  }

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  if (__DEV__) {
    const { __REDUX_DEVTOOLS_EXTENSION__ } = window

    if (__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(
      responsiveStoreEnhancer,
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  sagaMiddleware.run(rootSaga)
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }

  return store
}

export default rootStore()
