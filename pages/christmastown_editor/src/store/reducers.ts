import { LOCATION_CHANGE } from 'react-router-redux'
import initialState, { IInitialState } from './initialState'
import { TYPE_IDS, SHOW_DEBUG_BOX, HIDE_DEBUG_BOX, SHOW_INFO_BOX, HIDE_INFO_BOX } from '../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SHOW_DEBUG_BOX]: (state: IInitialState, action: any) => ({
    ...state,
    debug: action.msg
  }),
  [HIDE_DEBUG_BOX]: (state: IInitialState) => ({
    ...state,
    debug: null
  }),
  [SHOW_INFO_BOX]: (state: IInitialState, action: any) => ({
    ...state,
    info: action.msg
  }),
  [HIDE_INFO_BOX]: (state: IInitialState) => ({
    ...state,
    info: null
  }),
  [LOCATION_CHANGE]: (state: IInitialState, action: any) => {
    if (!action || !action.payload || !action.payload.hash) {
      return {
        ...state
      }
    }

    const currentPath = action.payload.hash.substr(2, action.payload.hash.length)

    return {
      ...state,
      debug: null,
      info: null,
      locationCurrent: currentPath,
      pageID: TYPE_IDS[currentPath]
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
