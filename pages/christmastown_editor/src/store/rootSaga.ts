import { all } from 'redux-saga/effects'
import mapEditor from '../routes/MapEditor/sagas'
import npcEditor from '../routes/NpcEditor/sagas'

export default function* rootSaga() {
  yield all([mapEditor(), npcEditor()])
}
