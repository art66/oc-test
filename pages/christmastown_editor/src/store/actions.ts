import { createAction } from 'redux-actions'
import { SHOW_DEBUG_BOX, HIDE_DEBUG_BOX, SHOW_INFO_BOX, HIDE_INFO_BOX } from '../constants'
import { TOGGLE_ICS, SET_ICS_INIT_PARAMS } from './actionTypes'

export const setICSInitParams = createAction(SET_ICS_INIT_PARAMS, (initParams) => ({ initParams }))
export const toggleICS = createAction(TOGGLE_ICS, (show) => ({ show }))

export const debugShow = error => ({
  msg: error,
  type: SHOW_DEBUG_BOX
})

export const debugHide = () => ({
  type: HIDE_DEBUG_BOX
})

export const infoShow = info => ({
  msg: info,
  type: SHOW_INFO_BOX
})

export const infoHide = () => ({
  type: HIDE_INFO_BOX
})
