import IInitParams from '@torn/ics/src/models/IInitParams'
import currentLocation from '../utils/getWindowLocation'
import { TYPE_IDS } from '../constants'

export interface IInitialState {
  locationCurrent?: any
  appTitle?: string
  appID?: string
  pageID?: number
  debug?: {
    msg: string | object
  }
  info?: string
  debugCloseAction?: () => void
  ics?: {
    show: boolean
    initParams?: IInitParams
  }
}

const initialState: IInitialState = {
  locationCurrent: currentLocation,
  appTitle: 'ChristmasTown Editor',
  appID: 'ChristmasTownEditor',
  pageID: TYPE_IDS[currentLocation],
  debug: null,
  info: null,
  ics: {
    show: false
  }
}

export default initialState
