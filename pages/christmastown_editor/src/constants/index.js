export const CELL_SIZE = 30
export const ADMIN_MAP_VIEWPORT_X = 21
export const ADMIN_MAP_VIEWPORT_Y = 20
export const USER_MAP_VIEWPORT = 9
export const MOVING_INTERVAL = 1000
export const MAX_STEP_DURATION = 60
export const CELL_MIN_RANGE = 1
export const CELL_MAX_RANGE = 10
export const MAP_VIEWPORT_CELLS_STOCK = 1
export const MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_Y = 3
export const MAP_VIEWPORT_CELLS_STOCK_FULLSCREEN_X = 1
export const USER_POSITION_MIN_X = 5
export const USER_POSITION_MIN_Y = 0
export const ADMIN_POSITION_MIN_X = 10
export const ADMIN_POSITION_MIN_Y = 10
export const TAB_OBJECTS = 'objects'
export const TAB_PARAMETERS = 'parameters'
export const TAB_AREAS = 'areas'
export const TAB_BLOCKING = 'blocking'
export const TAB_LAYERS = 'layers'
export const TAB_HISTORY = 'history'
export const TAB_BACKUPS = 'backups'
export const TAB_NPC = 'npc'

export const JAIL = 'jail'
export const HOSPITAL = 'hospital'
export const DEFAULT_CATEGORY_NAME = 'default'
export const DEFAULT_TRIGGER_NAME = 'no trigger'

export const MAIN_URL = '/christmas_town.php?q='

export const INVENTORY_SLIDER_CHUNK_SIZE = 10
export const PIXELS = 'px'
export const ITEMS_URL = '/images/items/'
export const CT_ITEMS_URL = ITEMS_URL + 'christmas_town/'
export const CT_ITEMS_CATEGORY_KEYS = 'keys'
export const CT_ITEMS_CATEGORY_ITEMS = 'ctItems'
export const CT_ITEMS_CATEGORY_NOTES = 'notes'
export const CT_ITEMS_CATEGORY_CHESTS = 'chests'
export const TORN_CATEGORY_ITEMS = 'tornItems'
export const ITEMS_IMG_EXTENSION = '.png'
export const OFFSET_ITEMS_SIZE = 0
export const OFFSET_CT_ITEMS_SIZE = 0
export const OFFSET_KEYS_SIZE = 10
export const OFFSET_NOTES_SIZE = 10
export const OFFSET_CHESTS_SIZE = 5

export const CT_DEVELOPERS = [
  '2055008',
  '1748546',
  '2072301',
  '2117366',
  '1637698',
  '2147691',
  '2307949',
  '2307957',
  '2313413',
  '2272005'
]

export const LEFT_TOP = 'leftTop'
export const RIGHT_TOP = 'rightTop'
export const LEFT_BOTTOM = 'leftBottom'
export const RIGHT_BOTTOM = 'rightBottom'
export const TOP = 'top'
export const RIGHT = 'right'
export const BOTTOM = 'bottom'
export const LEFT = 'left'
export const ALL = 'all'

export const TYPE_IDS = {
  '': 1,
  parametereditor: 2
}

export const SHOW_DEBUG_BOX = 'SHOW_DEBUG_BOX'
export const HIDE_DEBUG_BOX = 'HIDE_DEBUG_BOX'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_INFO_BOX = 'HIDE_INFO_BOX'

export const GATE_CONTROL = 'gateControl'
export const TELEPORT = 'teleport'
export const SPEED = 'speed'
export const GAME_GIFT_SHOP = 'gameGiftShop'

export const SERVER_ERROR_MESSAGE = 'Internal server error'
export const UNSUPPORTED_DEVICE_MESSAGE = "Your device isn't supported"
export const PARAMETER_ABSENT_MESSAGE = "Parameter doesn't exist"

export const FORBIDDEN_ERROR_CODE = 403
export const FORBIDDEN_ERROR_TEXT = 'Access Forbidden'

export const TELEPORT_ANIMATIONS = {
  default: 'Fade effect',
  fall: 'Fall effect'
}

export const NPC_TYPE_SANTA = 0
export const NPC_TYPE_GRINCH = 10
