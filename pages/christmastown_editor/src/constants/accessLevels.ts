// Lower access level = more permissions

export const ACCESS_LEVEL_0 = 0 // ['Developer']
export const ACCESS_LEVEL_1 = 1 // ['TornAdmin', 'ChristmasTownAdmin']
export const ACCESS_LEVEL_2 = 2 // ['StaffCommittee', 'MapsManager', 'Owner', 'Exclusive']
export const ACCESS_LEVEL_3 = 3 // ['Editor']
export const ACCESS_LEVEL_4 = 4 // ['Tester']
