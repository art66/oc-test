import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'

import puzzles from './puzzles'

export default combineReducers({
  browser: createResponsiveStateReducer({
    small: 600,
    medium: 1000,
    large: 2000
  }),
  puzzles
})
