import { handleActions } from 'redux-actions'
import { PUZZLES } from '../constants'

const INITIAL_SECTION_ID = window.location.hash.replace('#', '') || 'LoadingScreen'
// const INITIAL_SECTION_ID = 'MyFirstIsIn'

const initialState = {
  enabled: false,
  loadProgress: 0,
  subTestID: 1,
  bootup: true
}

export default handleActions(
  {
    'puzzle loaded'(state, action) {
      const puzzleName = action.payload.puzzleName || INITIAL_SECTION_ID
      const puzzleModule = action.payload.puzzleModule.default
      const newSection =
        PUZZLES.find(function(item) {
          return item.id === puzzleName
        }) || {}
      const subTestID = action.payload.subTestID ? Number(action.payload.subTestID) : initialState.subTestID
      return {
        ...state,
        puzzleModule: puzzleModule,
        puzzleName: newSection.name,
        subTestID: subTestID,
        puzzleLoaded: true,
        keyboard: null
      }
    },

    'device enabled'(state, action) {
      const subTestID = action.payload.subTestID ? Number(action.payload.subTestID) : initialState.subTestID
      return {
        ...state,
        enabled: !state.enabled,
        subjectNumber: action.payload.subjectNumber,
        timeElapsed: action.payload.timeElapsed,
        subTestID: subTestID
      }
    },

    'set subject number'(state, action) {
      return {
        ...state,
        subjectNumber: action.payload.subjectNumber
      }
    },

    'set puzzle'(state, action) {
      const puzzleName = action.payload.puzzleName || INITIAL_SECTION_ID
      const puzzleModule = action.payload.puzzleModule.default
      const newSection =
        PUZZLES.find(function(item) {
          return item.id === puzzleName
        }) || {}

      return {
        ...state,
        puzzleModule: puzzleModule,
        puzzleName: newSection.name,
        keyboard: null
      }
    },

    'next subtest'(state) {
      return {
        ...state,
        subTestID: state.subTestID + 1,
        keyboard: null
      }
    },

    'show keyboard'(state, action) {
      return {
        ...state,
        keyboard: {
          target: action.payload.target,
          returnAction: action.payload.returnAction
        }
      }
    },

    'hide keyboard'(state, action) {
      return {
        ...state,
        keyboard: null
      }
    },

    'show info box'(state, action) {
      console.log(action.payload.msg)
      return {
        ...state,
        infoBox: {
          msg: action.payload.msg,
          color: action.payload.color
        }
      }
    },

    'set load progress'(state, action) {
      return {
        ...state,
        loadProgress: action.payload.loadProgress
      }
    },

    'show sticker'(state, action) {
      return {
        ...state,
        deviceNumber: action.payload.deviceNumber
      }
    },

    'set puzzle status'(state, action) {
      return {
        ...state,
        status: action.payload.status,
        statusMessage: action.payload.statusMessage
      }
    }
  },
  initialState
)
