export const MAIN_URL = '/loader.php?sid=missionsPuzzleDeviceData&mode=json'
export const PUZZLES = [
  { id: 1, name: 'Calibration' },
  { id: 2, name: 'CaesarShift' },
  { id: 3, name: 'LocateArea' },
  { id: 4, name: 'Maze' },
  { id: 5, name: 'MyFirstIsIn' },
  { id: 6, name: 'OddOneOut' },
  { id: 7, name: 'Scrabble' },
  { id: 8, name: 'Word' }
]
