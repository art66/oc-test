import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'

const CELL_SIZE = 34
class Char extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      character: props.children,
      origin: props.origin,
      children,
      isAMemberOfLetters
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { children: newChildren, origin: newOrigin } = nextProps
    const { origin, children, isAMemberOfLetters } = prevState

    return {
      origin: isAMemberOfLetters && children !== newChildren ? origin : newOrigin,
      children,
      isAMemberOfLetters
    }
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps(nextProps) {
  //   const { children: newChildren, origin: newOrigin } = nextProps
  //   const { origin, children, isAMemberOfLetters } = this.props

  //   if (isAMemberOfLetters && children !== newChildren) {
  //     this.setState({
  //       origin
  //     })
  //   } else {
  //     this.setState({
  //       origin: newOrigin
  //     })
  //   }
  // }
  getBounds = () => {
    return this.root.getBoundingClientRect()
  }
  render() {
    const { children, ...rest } = this.props
    const skewAngle = Math.atan(this.state.origin[0] / this.state.origin[1])
    const scaleY = this.state.origin[1] / CELL_SIZE
    return (
      <li
        {...rest}
        ref={node => {
          this.root = node
        }}
      >
        {this.props.children ? (
          <div className="scrabble__tracing-wrap">
            <div className="scrabble__scrabble__trace--outering-box">
              <div className="scrabble__trace--outer" style={{ transform: 'skewX(' + skewAngle + 'rad)' }}>
                <div className="scrabble__trace--inner" style={{ transform: 'scaleY(' + scaleY + ')' }} />
              </div>
              <div className="scrabble__trace--outer" style={{ transform: 'skewX(' + skewAngle + 'rad)' }}>
                <div className="scrabble__trace--inner" style={{ transform: 'scaleY(' + scaleY + ')' }} />
              </div>
            </div>
            <div
              className="scrabble__letter"
              style={{ left: this.state.origin[0] + 'px', top: this.state.origin[1] + 'px' }}
            >
              {children}
            </div>
          </div>
        ) : null}
      </li>
    )
  }
}

Char.propTypes = {
  children: PropTypes.node.isRequired,
  origin: PropTypes.array.isRequired,
  isAMemberOfLetters: PropTypes.bool
}

class Scrabble extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      letters: [
        'd',
        'e',
        'g',
        'b',
        's',
        'i',
        'r',
        'd',
        'a',
        'g',
        'n',
        'r',
        'e',
        'd',
        'o',
        'p',
        'n',
        'r',
        'p',
        'e',
        'd',
        'i'
      ],
      words: [3, 2, 8, 9],
      usedLetters: {
        // a map of 'positions in placeholders' -> 'letters.index'
        5: 4,
        11: 9
      }
    }
    this.wordsRefs = []
    this.lettersRefs = []
  }
  // WORDS METHODTS
  renderWords = () => {
    let start = 0
    let end = 0
    const uls = []
    this.state.words.map((lettersCount, i) => {
      end += lettersCount
      uls.push(this.getWord(start, end))
      start = end
    })
    return uls
  }
  getWord = (start, end) => {
    const lis = []
    for (let i = start; i < end; i++) {
      const c = classnames({
        filled: true,
        pressed: this.state.selectedPlaceHolderIndex === i
      })
      lis.push(
        <Char
          ref={this.registerNode}
          origin={this.getOrigin(i, false)}
          className={c}
          key={i}
          isAMemberOfLetters={false}
          onClick={() => this.wordClick(i)}
        >
          {this.state.letters[this.state.usedLetters[i]]}
        </Char>
      )
    }
    return (
      <ul key={start} className="places-list clearfix uppercase">
        {lis}
      </ul>
    )
  }
  wordClick = index => {
    // remove the letter on second click
    if (Object.keys(this.state.usedLetters).indexOf(index.toString()) !== -1) {
      const newUsedLetters = { ...this.state.usedLetters }
      delete newUsedLetters[index]
      this.setState({
        usedLetters: newUsedLetters
      })
    }

    this.setState(
      {
        selectedPlaceHolderIndex: index
      },
      this.moveLetter
    )
  }
  // WORDS METHODTS END

  // LETTERS METHODTS
  renderLetters = () => {
    const lis = []
    this.state.letters.map((letter, i) => {
      const c = classnames({
        uppercase: this.state.selectedLetterIndex === i,
        pressed: this.state.selectedLetterIndex === i,
        used: Object.values(this.state.usedLetters).indexOf(i) !== -1
      })
      lis.push(
        <Char
          ref={this.registerNode}
          origin={this.getOrigin(i, true)}
          key={i}
          className={c}
          isAMemberOfLetters
          onClick={() => this.letterClick(i)}
        >
          {this.getLettersChar(letter, i)}
        </Char>
      )
    })
    return lis
  }
  getLettersChar = (letter, i) => {
    for (const key of Object.keys(this.state.usedLetters)) {
      if (this.state.usedLetters[key] === i) {
        return null
      }
    }
    return letter
  }
  letterClick = index => {
    if (Object.values(this.state.usedLetters).indexOf(index) === -1) {
      this.setState(
        {
          selectedLetterIndex: this.state.selectedLetterIndex === index ? null : index
        },
        this.moveLetter
      )
    } else {
      this.setState({
        selectedLetterIndex: null
      })
    }
  }
  // LETTERS METHODTS END
  getOrigin = (i, isAMemberOfLetters) => {
    let from, to
    if (!isAMemberOfLetters) {
      const k = Object.keys(this.state.usedLetters).indexOf(i.toString())
      if (this.wordsRefs.length === 0 || k === -1) {
        return [0, 0]
      }
      from = this.wordsRefs[i]
      to = this.lettersRefs[Object.values(this.state.usedLetters)[k]]
    } else {
      const k = Object.values(this.state.usedLetters).indexOf(i)
      if (this.lettersRefs.length === 0 || k === -1) {
        return [0, 0]
      }
      from = this.lettersRefs[i]
      to = this.wordsRefs[Object.keys(this.state.usedLetters)[k]]
    }

    const { left: leftFrom, top: topFrom } = from.getBounds()
    const { left: leftTo, top: topTo } = to.getBounds()
    const left = leftTo - leftFrom
    const top = topTo - topFrom
    return [left, top]
  }
  registerNode = node => {
    if (node == null) return

    if (node.props.isAMemberOfLetters) {
      this.lettersRefs.push(node)
    } else {
      this.wordsRefs.push(node)
    }
  }
  moveLetter = () => {
    if (this.state.selectedLetterIndex != null && this.state.selectedPlaceHolderIndex != null) {
      this.setState({
        usedLetters: {
          ...this.state.usedLetters,
          [this.state.selectedPlaceHolderIndex]: this.state.selectedLetterIndex
        },
        selectedLetterIndex: null,
        selectedPlaceHolderIndex: null
      })
    }
  }
  clearHandler = () => {
    this.setState({
      usedLetters: {},
      selectedLetterIndex: null,
      selectedPlaceHolderIndex: null
    })
  }
  submitHandler = () => {
    const { actions } = this.props
    if (event) event.preventDefault()
    actions.nextSubtest()
  }
  render() {
    const totalLetters = this.state.words.reduce((sum, word) => sum + word, 0)
    const readyToSubmit = Object.keys(this.state.usedLetters).length === totalLetters
    const submitClass = classnames({
      'pd-button': true,
      locked: !readyToSubmit
    })
    const clearClass = classnames({
      'pd-button': true,
      locked: Object.keys(this.state.usedLetters).length === 0
    })
    return (
      <div className="content-50h content-top scrabble">
        <div className="content-inner text-center">
          <div className="pd-counter">0/5</div>
          <span className="task-title-small">
            Dont let the looks deceive you, these chocolate covered raisins will melt in your mouth and boost your
            happiness!<br />
            Provides a large amount of happiness when eaten.
          </span>
          <div className="pd-item pd-border pd-border-rad-6">
            <img src="/images/items/24/large.png" />
          </div>
          <div className="places-for-buttons">{this.renderWords()}</div>
          <div className="available-buttons">
            <ul className="buttons-list light clearfix uppercase">{this.renderLetters()}</ul>
          </div>
          <button type="button" className={submitClass} onClick={readyToSubmit ? this.submitHandler : null}>
            SUBMIT
          </button>
          <button type="button" className={clearClass} onClick={this.clearHandler}>
            Clear all
          </button>
        </div>
      </div>
    )
  }
}

Scrabble.propTypes = {
  actions: PropTypes.object
}

export default Scrabble
