import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'

const TOTAL_LETTERS = 26

class CaesarShift extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false,
      shift: 0
    }
  }

  componentDidMount() {
    const { actions } = this.props

    actions.showKeyboard({
      target: this.refs.answer,
      returnAction: this.submit
    })
  }

  changeHangler = event => {
    const input = event.target
    this.setState({ readyToSubmit: !!input.value })
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      fetchUrl('&step=enable&guess=' + this.refs.answer.value)
        .then(json => {
          // if (json.result && json.result.correct)
          // {
          actions.nextSubtest()
          // }
        })
        .catch(error => actions.showInfoBox({ msg: error.toString(), color: 'red' }))
    }
  }

  spin = direction => {
    if (direction === 'left') {
      this.spinCW()
    } else {
      this.spinCCW()
    }
  }

  scroll = event => {
    if (event.deltaY < 0) {
      this.spinCW()
    } else {
      this.spinCCW()
    }
  }

  spinCW = () => {
    this.setState({ shift: this.state.shift - 1 })
  }

  spinCCW = () => {
    this.setState({ shift: this.state.shift + 1 })
  }

  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })

    // not sure we ever need to disable it
    const leftArrow = classnames({
      'cs-pd-arrow-left': true
      // 'disabled': this.state.shift < 0
    })

    const rightArrow = classnames({
      'cs-pd-arrow-right': true
      // 'disabled': this.state.shift === 0
    })

    const angle = this.state.shift * (360 / TOTAL_LETTERS)

    return (
      <div className="content-75h content-middle caesar-shift" onWheel={this.scroll}>
        <div className="content-inner text-center">
          <span className="task-title">{'< L VDZ D FORXG, ZLWK LYB FLPFOHG URXQG >'}</span>
          <div className="text-center">
            <div className="wheels-container">
              <div className="wheel-outer" />
              <div className="wheel-inner" style={{ transform: 'rotateZ(' + angle + 'deg)' }} />
              <span className="pd-counter cs-counter">{this.state.shift % TOTAL_LETTERS}</span>
              <i className={leftArrow} onClick={() => this.spin('left')} />
              <i className={rightArrow} onClick={() => this.spin('rigth')} />
            </div>
          </div>
          <form className="pd-form pd-form-inline">
            <div className="pd-input-group">
              <label htmlFor="subject-number" className="pd-label">
                Decipher message:
              </label>
              <input
                ref="answer"
                id="subject-number"
                type="text"
                className="pd-input-text uppercase"
                onChange={this.changeHangler}
              />
            </div>
            <button type="submit" className={submitClass} onClick={this.submit}>
              SUBMIT
            </button>
          </form>
        </div>
      </div>
    )
  }
}

CaesarShift.propTypes = {
  actions: PropTypes.object
}

export default CaesarShift
