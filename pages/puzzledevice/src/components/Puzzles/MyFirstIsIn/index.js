import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../utils'

class MyFirstIsIn extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      answer: '',
      currentChallengeID: 0,
      readyToSubmit: false,
      challenges: [
        'My first is in <span class="lighter">duress</span> but not in <span class="lighter">udder</span>',
        'My second is in <span class="lighter">nightmare</span> but not in <span class="lighter">arrangement</span>',
        'My third is in <span class="lighter">adsorption</span> but not in <span class="lighter">standpoint</span>',
        'My fourth is in <span class="lighter">arrogant</span> but not in <span class="lighter">grant</span>',
        'My fifth is in <span class="lighter">interrogate</span> but not in <span class="lighter">grate</span>',
        'My sixth is in <span class="lighter">meantime</span> but not in <span class="lighter">innate</span>',
        'My seventh is in <span class="lighter">ensnarement</span> but not in <span class="lighter">treatment</span>'
      ]
    }
  }

  componentDidMount() {
    const { actions } = this.props
    fetchUrl('&puzzle=myfirstisin&step=getData')
      .then(json => {
        // if (json.result)
        // {
        //   this.setState({
        //     challenges: json.result.challenges
        //   })
        // }
      })
      .catch(error => actions.showInfoBox({ msg: error.toString(), color: 'red' }))

    setTimeout(this.start, 3000)
  }

  start = () => {
    const { actions } = this.props

    if (this.state.started) return

    this.setState({
      started: true
    })

    actions.showKeyboard({
      target: this.refs.answer,
      returnAction: this.submit
    })
  }

  changeHandler = event => {
    const input = event.target

    if (input.value.length === this.state.challenges.length) {
      this.checkUserInput(input.value)
    } else if (input.value.length > this.state.challenges.length) {
      input.value = input.value.substr(0, this.state.challenges.length)
    }

    this.setState({
      answer: input.value,
      currentChallengeID: input.value.length,
      readyToSubmit: !!input.value
    })
  }

  checkUserInput = userInput => {
    const { actions } = this.props

    fetchUrl('&puzzle=myfirstisin&step=check&guess=' + userInput)
      .then(json => {
        // if (json.result)
        // {
        //   this.setState({
        //     challenge: json.result.challenge
        //   })
        // }
      })
      .catch(error => actions.showInfoBox({ msg: error.toString(), color: 'red' }))
  }

  getDots = () => {
    const lis = []
    for (let i = 0; i < this.state.challenges.length; i++) {
      const c = classnames({
        completed: i < this.state.currentChallengeID,
        active: i === this.state.currentChallengeID
      })
      lis.push(<li key={i} className={c} />)
    }
    return lis
  }

  getCurrentChallenge = () => {
    if (this.state.currentChallengeID >= this.state.challenges.length) {
      return { __html: '&nbsp;' }
    }
    return { __html: this.state.challenges[this.state.currentChallengeID] }
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      actions.nextSubtest()
    }
  }

  render() {
    return (
      <div className="content-75h content-top my-first-is-in playing" onClick={this.start}>
        <div className="content-inner text-center">
          {this.state.started ? (
            <ul className="steps-marks">{this.getDots()}</ul>
          ) : (
            <span className="task-title initial">Guess item name.</span>
          )}
          <div className="pd-item pd-border pd-border-rad-6">
            <img id="test-item-img" src="/images/items/24/large.png" />
          </div>
          <div className={'slideup' + (this.state.started ? ' active' : '')}>
            <span className="task-title" dangerouslySetInnerHTML={this.getCurrentChallenge()} />
            <div action="" className="pd-form">
              <div className="pd-input-group">
                <label htmlFor="item-name" className="pd-label">
                  Type item name:
                </label>
                <input
                  ref="answer"
                  id="item-name"
                  type="text"
                  className="pd-input-text"
                  onChange={this.changeHandler}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

MyFirstIsIn.propTypes = {
  actions: PropTypes.object
}

export default MyFirstIsIn
