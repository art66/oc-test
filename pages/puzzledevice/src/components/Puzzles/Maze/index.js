import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'

const LABYRINTH_MAP = [
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1],
  [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1],
  [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
  [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1],
  [1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1],
  [1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1],
  [1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
  [1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1],
  [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1],
  [0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0],
  [1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1],
  [1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1],
  [1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
  [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1],
  [1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
  [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1],
  [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
  [1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1],
  [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]
const CELL_HEIGHT = 14
const CELL_WIDTH = 14

class Maze extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      mazeY: 10,
      mazeX: 0
    }
  }

  componentDidMount() {
    window.addEventListener('keydown', this.keyDownHandler)
    window.addEventListener('keyup', this.keypup)
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.keyDownHandler)
    window.removeEventListener('keyup', this.keypup)
  }

  moveMaze = direction => {
    const { mazeY, mazeX } = this.state
    const { actions } = this.props
    let toY, toX
    switch (direction) {
      case 'up':
        toY = mazeY - 1
        toX = mazeX
        break
      case 'down':
        toY = mazeY + 1
        toX = mazeX
        break
      case 'left':
        toY = mazeY
        toX = mazeX - 1
        break
      case 'right':
        toY = mazeY
        toX = mazeX + 1
        break
    }

    if (LABYRINTH_MAP[toY] && LABYRINTH_MAP[toY][toX] === 0) {
      this.setState({
        mazeY: toY,
        mazeX: toX
      })
      if (toY === 10 && toX === 20) {
        actions.nextSubtest()
      }
    } else {
      // this.shake()
    }
  }

  keyDownHandler = event => {
    if (event.code.indexOf('Arrow') !== -1) {
      this.setState({
        keyPressed: event.code
      })
    }

    switch (event.code) {
      case 'ArrowUp':
        this.moveMaze('up')
        break
      case 'ArrowDown':
        this.moveMaze('down')
        break
      case 'ArrowLeft':
        this.moveMaze('left')
        break
      case 'ArrowRight':
        this.moveMaze('right')
        break
    }
  }

  keypup = () => {
    this.setState({
      keyPressed: null
    })
  }

  shake = () => {
    this.setState({
      shake: true
    })
    setTimeout(() => {
      this.setState({
        shake: false
      })
    }, 1000)
  }

  render() {
    const cUp = classnames({
      'btn-cursor': true,
      'pd-arrow-up': true,
      pressed: this.state.keyPressed === 'ArrowUp'
    })
    const cDown = classnames({
      'btn-cursor': true,
      'pd-arrow-down': true,
      pressed: this.state.keyPressed === 'ArrowDown'
    })
    const cLeft = classnames({
      'btn-cursor': true,
      'pd-arrow-left': true,
      pressed: this.state.keyPressed === 'ArrowLeft'
    })
    const cRight = classnames({
      'btn-cursor': true,
      'pd-arrow-right': true,
      pressed: this.state.keyPressed === 'ArrowRight'
    })

    return (
      <div className={'content-100h content-bottom' + (this.state.shake ? ' shakeit' : '')}>
        <div className="content-inner text-center">
          <span className="task-title">Move the ball out of a maze using the arrow keys</span>
          <div className="pd-maze-container">
            <i
              className="pd-maze-ball"
              style={{ left: this.state.mazeX * CELL_WIDTH, top: this.state.mazeY * CELL_HEIGHT }}
            />
          </div>
          <div className="pd-maze-controls">
            <ul className="buttons-list light clearfix">
              <li className={cUp} onClick={() => this.moveMaze('up')} />
              <li className={cLeft} onClick={() => this.moveMaze('left')} />
              <li className={cDown} onClick={() => this.moveMaze('down')} />
              <li className={cRight} onClick={() => this.moveMaze('right')} />
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

Maze.propTypes = {
  actions: PropTypes.object
}

export default Maze
