import PropTypes from 'prop-types'
import React, { Component } from 'react'
// import paper from 'paper';
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'

const MIN_PATH_LENGTH = 500

class Calibration7 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false
    }
    require.ensure(
      ['paper'],
      require => {
        const paper = require('paper')
        this.init(paper)
      },
      'paper'
    )
    this.puzzleID = 1
    this.subTestID = 7
  }

  init(paper) {
    paper.setup(this.refs.canvas)
    const tool = new paper.Tool()
    let totalLength = 0
    let path

    tool.onMouseDown = function(event) {
      if (path) {
        path.selected = false
      }

      path = new paper.Path({
        segments: [event.point],
        strokeColor: '#0c4848',
        strokeWidth: 3
      })
    }

    tool.onMouseDrag = function(event) {
      path.add(event.point)
    }

    tool.onMouseUp = function(event) {
      path.simplify(2)
      totalLength += path.length
      if (totalLength > MIN_PATH_LENGTH) {
        this.setState({
          readyToSubmit: true
        })
      }
    }.bind(this)
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })
      fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID).then(json => {
        actions.nextSubtest()
      })
    }
  }

  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })

    return (
      <div className="content-100h content-middle">
        <div className="content-inner text-center">
          <span className="task-title">Draw a cat</span>
          <div className="text-center">
            <div className="pd-draw">
              <canvas ref="canvas" width="350px" height="350px" />
            </div>
            <button type="submit" className={submitClass} onClick={this.submit}>
              SUBMIT
            </button>
          </div>
        </div>
      </div>
    )
  }
}

Calibration7.propTypes = {
  actions: PropTypes.object
}

export default Calibration7
