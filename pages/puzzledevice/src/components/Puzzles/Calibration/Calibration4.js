import PropTypes from 'prop-types'
import React, { Component } from 'react'
import * as _ from 'lodash'
import { fetchUrl } from '../../../utils'

class Calibration4 extends Component {
  constructor(props, context) {
    super(props, context)
    this.hits = 0
    this.state = {
      activeFace: 'faceA'
    }
    this.state = {
      ...this.state,
      colors: this.randomizeColors()
    }
    this.animateColors()
    this.puzzleID = 1
    this.subTestID = 4
  }

  animateColors = () => {
    this.interval = setInterval(() => {
      this.allowClick = true
      this.setState({
        colors: this.smartRandomizeColors()
      })
    }, 1000)
  }

  componentWillUnmount() {
    this.stopAnimation()
  }

  stopAnimation() {
    clearInterval(this.interval)
  }

  clickHandler = index => {
    if (!this.allowClick) return
    this.allowClick = false
    this.hits++
    if (this.hits >= 5) {
      this.stopAnimation()
      this.submit()
    }
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID).then(json => {
      actions.nextSubtest()
    })
  }

  getColorBoxes = () => {
    return this.state.colors.map((color, index) => (
      <li className="flip-container" key={index}>
        <div className={'flipper ' + color}>
          <label className="red" onClick={() => this.clickHandler(index)} />
          <label className="turquoise" />
          <label className="yellow" />
          <label className="blue" />
        </div>
      </li>
    ))
  }

  randomizeColors = () => _.shuffle(['turquoise', 'yellow', 'red', 'blue'])

  smartRandomizeColors = () => {
    let colors
    let redIndex = this.state.colors.indexOf('red')
    do {
      colors = this.randomizeColors()
    } while (redIndex === colors.indexOf('red'))
    return colors
  }

  render() {
    return (
      <div className="content-100h content-middle">
        <div className="content-inner text-center">
          <span className="task-title">Press red buttons as they appear</span>
          <ul className="colored-buttons clearfix">{this.getColorBoxes()}</ul>
        </div>
      </div>
    )
  }
}

Calibration4.propTypes = {
  actions: PropTypes.object
}

export default Calibration4
