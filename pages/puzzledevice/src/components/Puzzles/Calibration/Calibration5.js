import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'

class Calibration5 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false,
      map: [
        [1, 1],
        [1, 2],
        [1, 3],
        [1, 4],
        [1, 5],
        [2, 1],
        [2, 2],
        [2, 3],
        [2, 4],
        [2, 5],
        [3, 1],
        [3, 2],
        [3, 3],
        [3, 4],
        [3, 5],
        [4, 1],
        [4, 2],
        [4, 3],
        [4, 4],
        [4, 5],
        [5, 1],
        [5, 2],
        [5, 4],
        [5, 3],
        [5, 5]
      ]
    }
    this.puzzleID = 1
    this.subTestID = 5
  }

  handleClick = index => {
    if (this.state.readyToSubmit) return
    if (!this.state.selectedTile) {
      this.setState({ selectedTile: index })
    } else {
      const newMap = this.state.map.splice(0)
      let swap = newMap[this.state.selectedTile]
      newMap[this.state.selectedTile] = newMap[index]
      newMap[index] = swap
      this.setState({ map: newMap, selectedTile: null })
      if (this.isSorted(newMap)) {
        this.setState({
          readyToSubmit: true
        })
      }
    }
  }

  getTiles = () => {
    const { selectedTile, map: itemsArray } = this.state

    const getClasses = (index, i, j) => {
      const classes = classnames({
        [`tile tile-id-${index} tile-location-${i}-${j}`]: true,
        selected: selectedTile === index
      })

      return {
        classes
      }
    }

    const itemsToRender = itemsArray.map((itemArray, index) => {
      const [i, j] = itemArray
      const { classes } = getClasses(index, i, j)

      return (
        <div
          key={`${i}_${j}`}
          role='button'
          tabIndex={0}
          aria-label='piece-button'
          className={classes}
          onClick={() => this.handleClick(index)}
          onKeyDown={undefined}
        />
      )
    })

    return itemsToRender
  }

  isSorted = map => {
    let oldi, oldj
    for (let [i, j] of map) {
      if (oldi && oldj && (i < oldi || (j < oldj && !(j === 1 && oldj === 5)))) {
        return false
      }
      oldi = i
      oldj = j
    }
    return true
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })
      fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID).then(json => {
        actions.nextSubtest()
      })
    }
  }

  render() {
    const submitButton = this.state.readyToSubmit ? (
      <button type="submit" className="pd-button" onClick={this.submit}>
        NEXT
      </button>
    ) : null

    return (
      <div className="content-100h content-middle locate-area">
        <div className="content-inner text-center">
          <div className="heigh-regulator">
            <span className="task-title">Select two pieces at a time to swap and complete the image</span>
            <div className="text-center">
              <div className="pd-map pd-border">{this.getTiles()}</div>
            </div>
            {submitButton}
          </div>
        </div>
      </div>
    )
  }
}

Calibration5.propTypes = {
  actions: PropTypes.object
}

export default Calibration5
