import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import ScrollArea from 'react-scrollbar'

import { fetchUrl } from '../../../utils'

class Calibration2 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false
    }
    this.puzzleID = 1
    this.subTestID = 2
  }

  componentDidMount() {
    // ugly trick, had to use jQuery plugin
    /* global $ */
    $(this.refs['user-agreement-checkbox'])
      .on('change', this.changeHandler)
      .prettyCheckable()
  }

  changeHandler = event => {
    this.setState({
      readyToSubmit: !this.state.readyToSubmit
    })
  }

  submit = event => {
    const { actions } = this.props
    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })

      fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID).then(json => {
        actions.nextSubtest()
      })
    }
  }

  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })
    return (
      <div className="content-75h content-middle pd-terms">
        <div className="content-inner text-center">
          <ScrollArea speed={0.8} className="scroll-area" contentClassName="content" horizontal={false} smoothScrolling>
            <div className="pd-text">
              <p>
                <strong>Lorem ipsum</strong> dolordolordolordoloasdf asdf asdfsad asd fs asdf asdf
                asdfrdolor!!!!!!!!!!!!1 sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam
                felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede
                justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>
              <p>
                <strong>Lipsum</strong>, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
                pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend
                tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus
                in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. penatibus et
                magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
                pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                vulputate eget, arcu.
              </p>
              <p>
                <strong>Lorem ipsum</strong> Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum
                rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel,
                luctus pulvinar, hendrerit id, lorem. dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>
              <p>
                <strong>Lorem ipsum</strong> iis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>
              <p>
                <strong>Lorem ipsum</strong> dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>
              <p>
                <strong>Lorem ipsum</strong> dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>
              <p>
                <strong>Lipsum</strong>, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
                pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend
                tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus
                in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. penatibus et
                magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
                pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                vulputate eget, arcu.
              </p>
              <p>
                <strong>Lorem ipsum</strong> Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum
                rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel,
                luctus pulvinar, hendrerit id, lorem. dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>
              <p>
                <strong>Lorem ipsum</strong> iis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>
              <p>
                <strong>Lorem ipsum</strong> dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis
                enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </p>

              <form action="" className="pd-form clearfix">
                <input
                  className="check"
                  type="checkbox"
                  ref="user-agreement-checkbox"
                  name="user-agreement"
                  data-label="I agree to terms and conditions."
                />
                <button type="submit" className={submitClass} onClick={this.submit}>
                  SUBMIT
                </button>
              </form>
            </div>
          </ScrollArea>
        </div>
      </div>
    )
  }
}

Calibration2.propTypes = {
  actions: PropTypes.object
}

export default Calibration2
