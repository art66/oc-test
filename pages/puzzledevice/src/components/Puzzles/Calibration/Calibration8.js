import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'

class Calibration8 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false
    }
    this.puzzleID = 1
    this.subTestID = 8
    fetchUrl('&step=getPuzzle&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID).then(json => {
      this.setState({
        question: json.question
      })
    })
  }

  componentDidMount() {
    const { actions } = this.props

    actions.showKeyboard({
      target: this.refs.answer,
      returnAction: this.submit
    })
  }

  changeHangler = event => {
    const input = event.target
    this.setState({ readyToSubmit: !!input.value })
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })
      const answer = this.refs.answer.value
      fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID + '&answer=' + answer).then(
        json => {
          if (json.result === true) {
            actions.nextSubtest()
          } else {
            this.shake()
          }
        }
      )
    }
  }

  shake = () => {
    this.setState({
      shake: true
    })
    setTimeout(() => {
      this.setState({
        shake: false
      })
    }, 1000)
  }

  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })

    return (
      <div
        className={
          'content-75h content-middle' + (this.state.shake ? ' shakeit' : '') + (this.state.question ? '' : ' hide')
        }
      >
        <div className="content-inner text-center">
          <blockquote className="pd-quote" dangerouslySetInnerHTML={{ __html: this.state.question }} />
          <form action="" className="pd-form pd-form-inline">
            <div className="pd-input-group">
              <label htmlFor="answer" className="pd-label">
                Answer:
              </label>
              <input id="answer" ref="answer" type="text" className="pd-input-text" onChange={this.changeHangler} />
            </div>
            <button type="submit" className={submitClass} onClick={this.submit}>
              SUBMIT
            </button>
          </form>
        </div>
      </div>
    )
  }
}

Calibration8.propTypes = {
  actions: PropTypes.object
}

export default Calibration8
