import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'

class Calibration3 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false
    }
    this.puzzleID = 1
    this.subTestID = 3
  }
  clicHandler = event => {
    this.setState({
      readyToSubmit: true
    })
  }
  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })
      fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID).then(json => {
        actions.nextSubtest()
      })
    }
  }
  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })

    const getItems = () => {
      const items = []
      for (let i = 1; i <= 8; i++) {
        items.push(
          <li key={i}>
            <label>
              <input type="checkbox" className="to-hide" />
              <i className={'edible-things-image-' + i} onClick={this.clicHandler} />
            </label>
          </li>
        )
      }
      return items
    }
    return (
      <div className="content-100h content-middle">
        <div className="content-inner text-center">
          <span className="task-title">Click all the edible things</span>
          <ul className="edible-things clearfix">{getItems()}</ul>
          <button type="submit" className={submitClass} onClick={this.submit}>
            SUBMIT
          </button>
        </div>
      </div>
    )
  }
}

Calibration3.propTypes = {
  actions: PropTypes.object
}

export default Calibration3
