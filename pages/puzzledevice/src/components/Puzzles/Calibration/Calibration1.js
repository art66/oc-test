import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { fetchUrl } from '../../../utils'
import classnames from 'classnames'

class Calibration1 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false
    }
  }

  componentDidMount() {
    const { actions } = this.props

    actions.showKeyboard({
      target: this.refs.subjectnumber,
      returnAction: this.submit
    })
  }

  changeHangler = event => {
    const input = event.target
    this.setState({ readyToSubmit: !!input.value })
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })
      const subjectNumber = this.refs.subjectnumber.value
      fetchUrl('&step=checkSubject&subjectNumber=' + subjectNumber)
        .then(json => {
          const dataRoot = json
          if (dataRoot.result === true) {
            actions.setSubjectNumber({
              subjectNumber: Number(subjectNumber)
            })
            actions.nextSubtest()
          } else {
            this.shake()
          }
        })
        .catch(error => actions.showInfoBox({ msg: error.toString(), color: 'red' }))
    }
  }

  shake = () => {
    this.setState({
      shake: true
    })
    setTimeout(() => {
      this.setState({
        shake: false
      })
    }, 1000)
  }

  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })
    return (
      <div className={'content-75h content-middle' + (this.state.shake ? ' shakeit' : '')}>
        <div className="content-inner text-center">
          <form action="" className="pd-form pd-form-inline">
            <div className="pd-input-group">
              <label htmlFor="subject-number" className="pd-label">
                Subject Number:
              </label>
              <input
                id="subject-number"
                ref="subjectnumber"
                type="text"
                className="pd-input-text"
                onChange={this.changeHangler}
              />
            </div>
            <button type="submit" className={submitClass} onClick={this.submit}>
              SUBMIT
            </button>
          </form>
        </div>
      </div>
    )
  }
}

Calibration1.propTypes = {
  actions: PropTypes.object
}

export default Calibration1
