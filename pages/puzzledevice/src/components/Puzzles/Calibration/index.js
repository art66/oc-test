import PropTypes from 'prop-types'
import React, { Component } from 'react'

import Calibration1 from './Calibration1'
import Calibration2 from './Calibration2'
import Calibration3 from './Calibration3'
import Calibration4 from './Calibration4'
import Calibration5 from './Calibration5'
import Calibration6 from './Calibration6'
import Calibration7 from './Calibration7'
import Calibration8 from './Calibration8'
import Calibration9 from './Calibration9'

const SUBTESTS = {
  1: Calibration1,
  2: Calibration2,
  3: Calibration3,
  4: Calibration4,
  5: Calibration5,
  6: Calibration6,
  7: Calibration7,
  8: Calibration8,
  9: Calibration9
}

class Calibration extends Component {
  render() {
    const { subTestID } = this.props.puzzles
    const Section = SUBTESTS[subTestID]
    if (Section) {
      return <Section {...this.props} />
    }
    return null
  }
}

Calibration.propTypes = {
  puzzles: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  browser: PropTypes.object.isRequired
}

export default Calibration
