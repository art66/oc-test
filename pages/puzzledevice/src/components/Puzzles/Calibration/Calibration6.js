import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'

class Calibration6 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false
    }
    this.puzzleID = 1
    this.subTestID = 6
  }

  componentDidMount() {
    const { actions } = this.props

    actions.showKeyboard({
      target: this.refs.answer,
      returnAction: this.submit
    })
  }

  changeHangler = event => {
    const input = event.target
    this.setState({ readyToSubmit: !!input.value })
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })
      const answer = this.refs.answer.value
      fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID + '&answer=' + answer).then(
        json => {
          if (json.result === true) {
            actions.nextSubtest()
          } else {
            this.shake()
          }
        }
      )
    }
  }

  shake = () => {
    this.setState({
      shake: true
    })
    setTimeout(() => {
      this.setState({
        shake: false
      })
    }, 1000)
  }

  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })
    return (
      <div className={'content-75h content-middle' + (this.state.shake ? ' shakeit' : '')}>
        <div className="content-inner">
          <span className="task-title m-bottom65">24 - 13 + 1 = ...</span>
          <form className="pd-form pd-form-inline">
            <div className="pd-input-group">
              <label htmlFor="subject-number" className="pd-label">
                Enter the answer:
              </label>
              <input
                id="subject-number"
                type="text"
                ref="answer"
                className="pd-input-text"
                onChange={this.changeHangler}
              />
            </div>
            <button type="submit" className={submitClass} onClick={this.submit}>
              SUBMIT
            </button>
          </form>
        </div>
      </div>
    )
  }
}

Calibration6.propTypes = {
  actions: PropTypes.object
}

export default Calibration6
