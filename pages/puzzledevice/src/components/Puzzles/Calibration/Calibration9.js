import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'

class Calibration9 extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      readyToSubmit: false,
      invalid: false
    }
    this.puzzleID = 1
    this.subTestID = 9
  }

  componentDidMount() {
    const { actions } = this.props

    actions.showKeyboard({
      target: this.refs.answer,
      returnAction: this.submit
    })
  }

  changeHangler = event => {
    const input = event.target
    if (this.validate(input.value)) {
      this.setState({
        readyToSubmit: true,
        invalid: false
      })
    } else {
      this.setState({
        readyToSubmit: false,
        invalid: true
      })
    }
  }

  validate(value) {
    if (isNaN(value)) return false
    if (value < 1) return false
    if (value > 100) return false
    return true
  }

  submit = event => {
    const { actions } = this.props

    if (event) event.preventDefault()

    if (this.state.readyToSubmit) {
      this.setState({
        readyToSubmit: false
      })
      const answer = this.refs.answer.value
      fetchUrl('&step=solve&puzzleID=' + this.puzzleID + '&subTestID=' + this.subTestID + '&answer=' + answer).then(
        json => {
          actions.checkStatus(json)
          actions.hideKeyboard()
        }
      )
    }
  }

  render() {
    const submitClass = classnames({
      'pd-button': true,
      locked: !this.state.readyToSubmit
    })

    return (
      <div className="content-75h content-middle">
        <div className="content-inner">
          <form action="" className="pd-form pd-form-inline">
            <div className="pd-input-group">
              <label htmlFor="answer" className="pd-label">
                Please estimate your IQ:
              </label>
              <input
                id="answer"
                ref="answer"
                type="text"
                className={'pd-input-text' + (this.state.invalid ? ' pd-input-text--invalid' : '')}
                onChange={this.changeHangler}
              />
            </div>
            <button type="submit" className={submitClass} onClick={this.submit}>
              SUBMIT
            </button>
          </form>
        </div>
      </div>
    )
  }
}

Calibration9.propTypes = {
  actions: PropTypes.object
}

export default Calibration9
