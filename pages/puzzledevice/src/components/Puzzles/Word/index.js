import React, { Component } from 'react'
import classnames from 'classnames'

class Word extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      letters: ['Q', 'a', 's', 'd', 'f', 'l', 'Q', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'],
      selectedLetters: []
    }
  }

  selectLetter(index) {
    index = Number(index)
    if (this.state.selectedLetters.indexOf(index) === -1) {
      this.setState({
        selectedLetters: [...this.state.selectedLetters, index]
      })
    }
  }

  indexToCoordinates(index) {
    const SIZE = 4
    return {
      x: index % SIZE,
      y: Math.floor(index / SIZE)
    }
  }

  canSelectLetter(index) {
    const isANeighbourCell = index => {
      if (this.state.selectedLetters.length === 0) {
        return true
      }
      const lastCellIndex = this.state.selectedLetters[this.state.selectedLetters.length - 1]
      const { x: x1, y: y1 } = this.indexToCoordinates(index)
      const { x: x2, y: y2 } = this.indexToCoordinates(lastCellIndex)
      if (Math.abs(x1 - x2) > 1 || Math.abs(y1 - y2) > 1) {
        return false
      }
      return true
    }

    if (this.state.selectedLetters.length >= 5) {
      return false
    }
    if (!isANeighbourCell(index)) {
      return false
    }

    return true
  }

  handleStart(event) {
    const index = event.target.dataset.index
    this.canSelectLetter(index) && this.selectLetter(index)
  }

  handleEnd(event) {
    console.log('Drag End')
  }

  handleDrag(event) {
    if (event.buttons === 0) return
    const index = Number(event.target.dataset.index)
    this.canSelectLetter(index) && this.selectLetter(index)
  }

  getLetters() {
    const letters = []
    this.state.letters.map((letter, i) => {
      const c = classnames({
        pressed: this.state.selectedLetters.indexOf(i) !== -1
      })
      const traceDirectionClass = this.getTraceDirection(i)
      letters.push(
        <li
          key={i}
          onMouseDown={event => {
            this.handleStart(event)
          }}
          onMouseUp={event => {
            this.handleEnd(event)
          }}
          data-index={i}
          className={c + ' ' + traceDirectionClass}
        >
          <div
            className="word__cells"
            onMouseMove={event => {
              this.handleDrag(event)
            }}
            data-index={i}
          >
            {letter}
          </div>
        </li>
      )
    })
    return letters
  }

  getTraceDirection(i) {
    if (this.state.selectedLetters.length < 2) {
      // no traces for 1 selection
      return false
    }
    const index = this.state.selectedLetters.indexOf(i)
    const from = this.state.selectedLetters[index]
    const to = this.state.selectedLetters[index + 1]

    if (from === undefined || to === undefined) {
      return false
    }
    const { x: x1, y: y1 } = this.indexToCoordinates(from)
    const { x: x2, y: y2 } = this.indexToCoordinates(to)

    const dx = x2 - x1
    const dy = y2 - y1
    let dir = ''
    switch (dy) {
      case 1:
        dir += 's'
        break
      case -1:
        dir += 'n'
        break
    }
    switch (dx) {
      case 1:
        dir += 'e'
        break
      case -1:
        dir += 'w'
        break
    }
    return 'trace-it trace-to-' + dir
  }

  // top letters logic
  getSlectedLetters() {
    const LETTERS_COUNT = 5
    const lis = []
    for (let i = 0; i < LETTERS_COUNT; i++) {
      lis.push(
        <li key={i}>
          {this.state.selectedLetters[i] !== null ? this.state.letters[this.state.selectedLetters[i]] : null}
        </li>
      )
    }
    return lis
  }

  render() {
    return (
      <div className="content-50h content-top word">
        <div className="content-inner text-center">
          <div className="pd-counter">0/5</div>
          <span className="task-title">Item clue: Said o relieve a large amount of pain.</span>
          <div className="pd-item pd-border pd-border-rad-6">
            <img src="/images/items/24/large.png" />
          </div>
          <div className="places-for-buttons">
            <ul className="places-list uppercase clearfix">{this.getSlectedLetters()}</ul>
          </div>
          <div className="available-buttons">
            <ul className="buttons-list light clearfix uppercase">{this.getLetters()}</ul>
          </div>
        </div>
      </div>
    )
  }
}

export default Word
