import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { CSSTransitionGroup } from 'react-transition-group'

const CHALLENGES = [
  {
    img: 'somepath',
    hints: ["Big Al's Gun Shop", 'Docks', 'My Gym', 'City Park'],
    answer: ["Big Al's Gun Shop"]
  },
  {
    img: 'somepath1',
    hints: ["Big Al's Gun Shop", 'Race', 'Casino', 'City Park'],
    answer: ['Race']
  }
]

class LocateArea extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      currentChallenge: 0
    }
  }

  guess = hint => {
    const c = this.state.currentChallenge
    const { actions } = this.props
    if (hint === CHALLENGES[c].answer) {
      let newChallengeIndex = this.state.currentChallenge + 1
      if (newChallengeIndex >= CHALLENGES.length) {
        actions.nextSubtest()
      } else {
        this.setState({
          currentChallenge: newChallengeIndex
        })
      }
    } else {
      this.shake()
    }
  }

  shake = () => {
    this.setState({
      shake: true
    })
    setTimeout(() => {
      this.setState({
        shake: false
      })
    }, 1000)
  }

  getHints = () => {
    const c = this.state.currentChallenge
    return CHALLENGES[c].hints.reduce(
      (allHints, nextHint, i) => [
        ...allHints,
        <li key={i} onClick={() => this.guess(nextHint)}>
          <a href="#">{nextHint}</a>
        </li>
      ],
      []
    )
  }

  render() {
    return (
      <CSSTransitionGroup transitionName="fade" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
        <div
          key={this.state.currentChallenge}
          className={'content-100h content-top locate-area' + (this.state.shake ? ' shakeit' : '')}
        >
          <div className="content-inner text-center">
            <div className="pd-counter">
              {this.state.currentChallenge + 1}/{CHALLENGES.length}
            </div>
            <span className="task-title">To which part of Torn City does this area belong?</span>
            <div className="pd-map pd-border" />
            <ul className="pd-horizontal-list">{this.getHints()}</ul>
          </div>
        </div>
      </CSSTransitionGroup>
    )
  }
}

LocateArea.propTypes = {
  actions: PropTypes.object
}

export default LocateArea
