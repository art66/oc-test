import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { fetchUrl } from '../../utils'

class OddOneOut extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      currentChallenge: 0,
      challenges: [[24, 569, 700, 231], [25, 568, 701, 230], [26, 567, 702, 232], [27, 566, 703, 233]]
    }
  }

  componentDidMount() {}

  guess = challenge => {
    const { actions } = this.props

    this.setState({
      currentChoise: challenge
    })

    fetchUrl('&puzzle=oddoneout&step=guess&challenge=' + this.state.currentChallenge + '&choise=' + challenge)
      .then(json => this.dataLoaded(json.result))
      .catch(error => actions.showInfoBox({ msg: error.toString(), color: 'red' }))

    setTimeout(() => {
      this.dataLoaded({
        guessed: Boolean(Math.round(Math.random()))
      })
    }, 200)
  }

  dataLoaded = result => {
    const { actions } = this.props

    if (!result) return

    this.setState({
      guessed: result.guessed
    })

    if (result.guessed) {
      // delay transision to next challengt
      setTimeout(() => {
        if (this.state.currentChallenge === this.state.challenges.length - 1) {
          actions.nextSubtest()
        } else {
          this.setState({
            currentChallenge: this.state.currentChallenge + 1
          })
        }
      }, 500)
    } else {
      this.shake()
    }

    setTimeout(() => {
      this.setState({
        currentChoise: null
      })
    }, 500)
  }

  shake = () => {
    this.setState({
      shake: true
    })
    setTimeout(() => {
      this.setState({
        shake: false
      })
    }, 1000)
  }

  getItems = () => {
    const items = []
    this.state.challenges[this.state.currentChallenge].map((challenge, i) => {
      const liClass = classnames({
        checking: challenge === this.state.currentChoise,
        success: this.state.currentChoise === challenge && this.state.guessed === true,
        failure: this.state.currentChoise === challenge && this.state.guessed === false
      })
      items.push(
        <li key={i} className={liClass} onClick={() => this.guess(challenge)}>
          <img src={'/images/items/' + challenge + '/large.png'} />
        </li>
      )
    })
    return items
  }

  render() {
    return (
      <div className={'content-50h content-top odd-one-out' + (this.state.shake ? ' shakeit' : '')}>
        <div className="content-inner text-center">
          <div className="pd-counter">
            {this.state.currentChallenge + 1}/{this.state.challenges.length}
          </div>
          <span className="task-title">
            One of these items does not belong here...<br />Select the odd one!
          </span>
          <ReactCSSTransitionGroup transitionName="fade" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
            <ul className="pd-items pd-borders pd-borders-rad-6" key={this.state.currentChallenge}>
              {this.getItems()}
            </ul>
          </ReactCSSTransitionGroup>
        </div>
      </div>
    )
  }
}

OddOneOut.propTypes = {
  actions: PropTypes.object
}

export default OddOneOut
