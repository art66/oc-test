import React from 'react'

const GIF_SRC = '/images/v2/puzzle_devices/misc/bootup.gif'

export const GifAnimation = () => (
  <div className="content-75h content-middle">
    <div className="content-inner text-center">
      <div className="device-logo-wrap">
        <img className="device-logo" src={GIF_SRC} />
      </div>
    </div>
  </div>
)

export default GifAnimation
