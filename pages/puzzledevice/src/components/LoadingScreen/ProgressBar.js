import PropTypes from 'prop-types'
import React from 'react'

const ProgressBar = props => {
  const { opacity, load, deviceNumber, subjectNumber = 'Uncalibrated' } = props
  return (
    <div className="content-75h content-middle">
      <div className="content-inner text-center">
        <div className="bootup-title" style={{ opacity: opacity }}>
          <i className="amanda-logo" />
          <p className="pd-text amanda-text">
            Cognitive Testing Device #{deviceNumber}
            <br />
            Licensed to Ravenscroft Industries<br />
            Calibrated for subject {subjectNumber}
            <br />
          </p>
        </div>
        <div className="pd-progress-bar-wrapper">
          <div className="bar-grey">
            <div className="bar-green" style={{ width: load + '%' }} />
            <span className="bar-value" style={{ opacity: opacity }}>
              {load}%
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}

ProgressBar.propTypes = {
  opacity: PropTypes.number,
  load: PropTypes.number,
  deviceNumber: PropTypes.number,
  subjectNumber: PropTypes.number
}

export default ProgressBar
