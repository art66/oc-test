import PropTypes from 'prop-types'
import React, { Component } from 'react'
import GifAnimation from './GifAnimation'
import ProgressBar from './ProgressBar'

class LoadingScreen extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      phase: 0,
      opacity: 0
    }

    // delay appearance of gif
    setTimeout(() => {
      this.setState({
        phase: 1
      })
    }, 1000)

    // move to progress bar
    setTimeout(() => {
      this.setState({
        phase: 2
      })

      this.animate()
    }, 5000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  animate() {
    this.interval = setInterval(() => {
      const delta = !this.props.puzzles.puzzleLoaded ? (100 - this.props.puzzles.loadProgress) / 100 : 2
      let nextLoad = this.props.puzzles.loadProgress + delta
      if (nextLoad >= 100) {
        this.props.actions.setLoadProgress({ loadProgress: 100 })
        clearInterval(this.interval)
        return
      }

      let nextOpacity = nextLoad / 70
      if (nextOpacity > 1) nextOpacity = 1

      this.setState({
        opacity: nextOpacity
      })
      this.props.actions.setLoadProgress({ loadProgress: nextLoad })
    }, 50)
  }

  render() {
    const { phase, opacity } = this.state
    const { loadProgress, deviceNumber, subjectNumber } = this.props.puzzles
    if (phase === 1) {
      return <GifAnimation />
    } else if (phase === 2) {
      return (
        <ProgressBar
          opacity={opacity}
          load={Math.floor(loadProgress)}
          subjectNumber={subjectNumber}
          deviceNumber={deviceNumber}
        />
      )
    }
    return null
  }
}

LoadingScreen.propTypes = {
  puzzles: PropTypes.object.isRequired,
  actions: PropTypes.object
}

export default LoadingScreen
