import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'

const KEYS = [
  { keyMain: 'q', keyAlt: '!' },
  { keyMain: 'w', keyAlt: '@' },
  { keyMain: 'e', keyAlt: '#' },
  { keyMain: 'r', keyAlt: '$' },
  { keyMain: 't', keyAlt: '%' },
  { keyMain: 'y', keyAlt: '^' },
  { keyMain: 'u', keyAlt: '&' },
  { keyMain: 'i', keyAlt: '*' },
  { keyMain: 'o', keyAlt: '(' },
  { keyMain: 'p', keyAlt: ')' },
  { action: 'ArrowUp', classname: 'btn-cursor pd-arrow-up' },
  { keyMain: '.?', keyAlt: 'Aa', action: 'Alt', classname: 'btn-special characters' },
  { keyMain: '7' },
  { keyMain: '8' },
  { keyMain: '9' },
  { keyMain: 'a', keyAlt: '-' },
  { keyMain: 's', keyAlt: '+' },
  { keyMain: 'd', keyAlt: '=' },
  { keyMain: 'f', keyAlt: '/' },
  { keyMain: 'g', keyAlt: '\\' },
  { keyMain: 'h', keyAlt: ';' },
  { keyMain: 'j', keyAlt: ':' },
  { keyMain: 'k', keyAlt: '\'' },
  { keyMain: 'l', keyAlt: '\'' },
  { action: 'ArrowLeft', classname: 'btn-cursor pd-arrow-left' },
  { action: 'ArrowDown', classname: 'btn-cursor pd-arrow-down' },
  { action: 'ArrowRight', classname: 'btn-cursor pd-arrow-right' },
  { keyMain: '4' },
  { keyMain: '5' },
  { keyMain: '6' },
  { action: 'Shift', classname: 'btn-special shift' },
  { keyMain: 'z', keyAlt: '[' },
  { keyMain: 'x', keyAlt: ']' },
  { keyMain: 'c', keyAlt: '{' },
  { keyMain: 'v', keyAlt: '}' },
  { keyMain: 'b', keyAlt: '<' },
  { keyMain: 'n', keyAlt: '>' },
  { keyMain: 'm', keyAlt: '?' },
  { action: 'Backspace', classname: 'btn-special ctrl' },
  { keyMain: ' ', classname: 'space' },
  { keyMain: '1' },
  { keyMain: '2' },
  { keyMain: '3' }
]

const Key = props => {
  const { keyMain, keyAlt, alt, classname, action, pressed, children } = props
  const key = alt && keyAlt ? keyAlt : keyMain
  const keyCode = action || key

  const cn = classnames({
    pressed: pressed,
    [classname]: classname
  })

  return (
    <li data-key-code={keyCode} className={cn}>
      {key}
      {children}
    </li>
  )
}

Key.propTypes = {
  keyMain: PropTypes.string,
  keyAlt: PropTypes.string,
  alt: PropTypes.bool,
  classname: PropTypes.string,
  action: PropTypes.string,
  pressed: PropTypes.bool,
  children: PropTypes.node
}

class Keyboard extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      shift: false,
      alt: false
    }
  }

  componentDidMount() {
    const { target, returnAction } = this.props

    this.target = target
    this.returnAction = returnAction

    this.target.addEventListener('keypress', this.keypress)
    this.target.addEventListener('keyup', this.keypup)
    // this.target.addEventListener('blur', this.blur);

    this.focusTarget()
  }

  getKey = (index, options) => {
    const { keyPressed, shift, alt, tempAlt } = this.state
    const { action, keyMain, keyAlt } = options

    const isEnter = action === 'Enter' && keyPressed === 13
    const isShift = action === 'Shift' && shift
    const isAlt = action === 'Alt' && (alt || tempAlt)
    const isKeyPressed = keyPressed && (keyPressed.toLowerCase() === keyMain || keyPressed.toLowerCase() === keyAlt)

    const pressed = isEnter || isShift || isAlt || isKeyPressed

    return (
      <Key
        key={index}
        {...options}
        alt={alt || tempAlt}
        pressed={pressed}
      />
    )
  }

  getKeys = () => {
    return KEYS.map((keyCode, index) => {

      return this.getKey(index, keyCode)
    })
  }

  keypress = event => {
    const letter = event.keyCode === 13 ? '13' : String.fromCharCode(event.keyCode)

    if (letter === '13') {
      this.returnAction()
    }

    this.setState({
      keyPressed: letter
    })

    if (
      KEYS.find(key => {
        return key.keyAlt === letter
      })
    ) {
      this.setState({
        tempAlt: true
      })
    } else if (
      KEYS.find(key => {
        return key.keyMain === letter.toLowerCase()
      })
    ) {
      this.setState({
        tempAlt: false,
        alt: false
      })
    }
  }

  keypup = () => {
    this.setState({
      keyPressed: null,
      tempAlt: false
    })
  }

  // TODO: listen to blur event to
  // remove 'focus' class form target
  // blur = (event) => {
  //   setTimeout(function() {
  //       console.log(document.activeElement);
  //   }, 1);
  // }
  focusTarget = () => {
    this.target.focus()
    this.target.classList.add('focus')
  }

  // trigger this action so that listeners of
  // 'change' event of input can fire
  triggerOnChange = () => {
    const evt = new Event('input', { bubbles: true })

    this.target.dispatchEvent(evt)
  }

  // eslint-disable-next-line max-statements
  tap = event => {
    const { target } = this
    const { alt, shift } = this.state

    const { selectionStart, selectionEnd } = target
    const key = event.target
    let code = key.dataset.keyCode

    if (code === undefined) return

    switch (code) {
      case 'ArrowLeft':
        target.selectionStart -= 1
        target.selectionEnd = target.selectionStart
        break
      case 'ArrowRight':
        target.selectionStart += 1
        break
      case 'ArrowUp':
        target.selectionStart = 0
        target.selectionEnd = 0
        break
      case 'ArrowDown':
        target.selectionStart = target.value.length
        break
      case 'Shift':
        this.setState({
          shift: !shift,
          alt: false
        })
        break
      case 'Alt':
        this.setState({
          alt: !alt,
          shift: false
        })
        break
      case 'Enter':
        this.returnAction()
        break
      case 'Backspace':
        if (selectionStart === selectionEnd) {
          target.value = target.value.substring(0, selectionStart - 1) + target.value.substring(selectionEnd)
          target.selectionStart = selectionStart - 1
          target.selectionEnd = selectionStart - 1
        } else {
          target.value = target.value.substring(0, selectionStart) + target.value.substring(selectionEnd)
          target.selectionStart = selectionStart
          target.selectionEnd = selectionStart
        }
        break
      default:
        if (shift) {
          code = key.dataset.codeAlternative ? key.dataset.codeAlternative : code.toUpperCase()
        }
        target.value = target.value.substring(0, selectionStart) + code + target.value.substring(selectionEnd)
        target.selectionStart = selectionStart + 1
        target.selectionEnd = selectionStart + 1
    }
    this.focusTarget()
    this.triggerOnChange()
  }

  render() {
    const { shift } = this.state

    const getKeysFirst = this.getKey(1, { keyMain: '0', classname: 'zero' })
    const getKeysSecond = this.getKey(2, {
      action: 'Enter',
      classname: 'enter',
      children: (
        <div>
          <span className='la-shadow' />
          <span className='ra-shadow' />
        </div>
      )
    })

    return (
      <div
        tabIndex={0}
        role='button'
        className={`keyboard clearfix${shift ? 'uppercase' : ''}`}
        onClick={this.tap}
        onKeyDown={undefined}
      >
        <ul className='buttons-list clearfix'>{this.getKeys()}</ul>
        <ul className='buttons-list clearfix'>
          {getKeysFirst}
          {getKeysSecond}
        </ul>
      </div>
    )
  }
}

Keyboard.propTypes = {
  target: PropTypes.object.isRequired,
  returnAction: PropTypes.func
}

export default Keyboard
