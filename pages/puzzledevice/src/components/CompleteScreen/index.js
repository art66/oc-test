import PropTypes from 'prop-types'
import React from 'react'

export const CompleteScreen = props => (
  <div className="content-100h content-middle">
    <div className="content-inner text-center">
      <span className="task-title concluded" dangerouslySetInnerHTML={{ __html: props.statusMessage }} />
    </div>
  </div>
)

CompleteScreen.propTypes = {
  statusMessage: PropTypes.string.isRequired
}

export default CompleteScreen
