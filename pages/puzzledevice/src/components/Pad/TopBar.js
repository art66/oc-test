import PropTypes from 'prop-types'
import React, { Component } from 'react'
import * as _ from 'lodash'

let timer = false

class TopBar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      ticker: false
    }
  }

  generateTestNumber() {
    const { subTestID, deviceNumber } = this.props
    return (subTestID * 105).toString().substr(1, 2) + '_' + (deviceNumber * 3051).toString().substr(1, 3)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { timeElapsed } = nextProps
    const { ticker } = prevState

    if (!timer && timeElapsed !== undefined) {
      timer = true

      setInterval(() => {
        return {
          ticker: ticker + 1
        }
      }, 1000)

      return {
        ticker: timeElapsed + 1
      }
    }

    return null
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps(nextProps) {
  //   const { timeElapsed } = nextProps
  //   if (!this.timerStarted && timeElapsed !== undefined) {
  //     this.timerStarted = true
  //     this.setState({
  //       ticker: timeElapsed + 1
  //     })
  //     setInterval(() => {
  //       this.setState({
  //         ticker: this.state.ticker + 1
  //       })
  //     }, 1000)
  //   }
  // }
  getTime(secs) {
    const hours = Math.floor(secs / 3600)
    const minutes = Math.floor((secs - hours * 3600) / 60)
    const seconds = secs % 60

    let formatedTime = _.padStart(minutes, 2, '0') + ':' + _.padStart(seconds, 2, '0')
    if (hours > 0) {
      formatedTime = _.padStart(hours, 2, '0') + ':' + formatedTime
    }
    return formatedTime
  }
  render() {
    const { timeElapsed, bootup, subjectNumber } = this.props

    if (timeElapsed === undefined || !subjectNumber || bootup) return null

    return (
      <div className="pd-top-info">
        <span>
          <span>Subject #</span>
          <span>{subjectNumber}</span>
          <span> / Test #</span>
          <span>{this.generateTestNumber()} </span>
          <span> / Time elapsed: </span>
          <span style={{ display: 'inline-block', width: '55px', textAlign: 'left' }}>
            {this.getTime(this.state.ticker)}
          </span>
          <i className="pd-battery-icon" />
        </span>
      </div>
    )
  }
}

TopBar.propTypes = {
  timeElapsed: PropTypes.number,
  bootup: PropTypes.bool,
  subjectNumber: PropTypes.number,
  subTestID: PropTypes.number.isRequired,
  deviceNumber: PropTypes.number
}

export default TopBar
