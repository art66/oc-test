import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { CSSTransitionGroup } from 'react-transition-group'

import TopBar from './TopBar'
import Keyboard from '../Keyboard'

class Pad extends Component {
  _renderResetButton = () => {
    // getAction is global
    const fetch = () => getAction(
      {
        action: '/loader.php?sid=missionsPuzzleDeviceData&step=reset&mode=json',
        success: () => {
          window.location.reload()
        }
      }
    )

    return (
      <button
        type='button'
        className='reset-button'
        onClick={fetch}
        style={{ margin: '40px 0' }}
      >
        reset
      </button>
    )
  }

  render() {
    const { actions, children, browser, bootup, puzzles } = this.props
    const { enabled, keyboard, puzzleName, deviceNumber, subTestID, subjectNumber, timeElapsed } = puzzles

    const classPuzzleDevice = classnames({
      'puzzle-device': true,
      enabled: enabled,
      bootup: bootup
    })

    const classUIWrapper = classnames({
      'device-ui': true,
      bootup: bootup
    })

    const keyboardMarkup =
      keyboard && browser.mediaType === 'large' ? <Keyboard {...keyboard} key={'' + puzzleName + subTestID} /> : null

    return (
      <div className={classPuzzleDevice}>
        <div className='frame frame-1'>
          <div className='pads pads-1'>
            <div className='scratches scratches-1'>
              <div className='device-indicators'>
                <i className='power-control' onClick={actions.enableDevice} />
                <i className='camera' />
                <i className='power-indicator-1' />
                <i className='power-indicator-2' />
                {deviceNumber ? (
                  <span className={'sticker sticker-' + deviceNumber + ' sticker-rc'}>Device #{deviceNumber}</span>
                ) : null}
              </div>
              <div className={classUIWrapper}>
                <TopBar
                  subjectNumber={subjectNumber}
                  timeElapsed={timeElapsed}
                  bootup={bootup}
                  deviceNumber={deviceNumber}
                  subTestID={subTestID}
                />
                <CSSTransitionGroup transitionName='fade' transitionEnterTimeout={500} transitionLeaveTimeout={500}>
                  {children}
                </CSSTransitionGroup>
                <CSSTransitionGroup transitionName='slide' transitionEnterTimeout={500} transitionLeaveTimeout={500}>
                  {keyboardMarkup}
                </CSSTransitionGroup>
              </div>
              <div className='screen-scratches screen-scratches-1' />
            </div>
          </div>
        </div>
        {this._renderResetButton()}
      </div>
    )
  }
}

Pad.propTypes = {
  actions: PropTypes.object,
  children: PropTypes.node,
  browser: PropTypes.object,
  bootup: PropTypes.bool,
  puzzles: PropTypes.object
}

export default Pad
