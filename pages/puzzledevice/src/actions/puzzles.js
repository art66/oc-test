import { createAction } from 'redux-actions'
import _ from 'lodash'
import { fetchUrl } from '../utils'
import { PUZZLES } from '../constants'

export function getState() {
  return dispatch => {
    return fetchUrl('&step=getState')
      .then(json => {
        const dataRoot = json && json.DB && json.DB.missionsPuzzle
        dispatch(checkStatus(json))
        if (dataRoot.puzzleID) {
          dispatch(loadPuzzle(dataRoot.puzzleID, dataRoot.subTestID))
        }
        if (dataRoot.timeElapsed !== undefined) {
          dispatch(
            deviceEnabled({
              subjectNumber: dataRoot.subjectNumber,
              timeElapsed: dataRoot.timeElapsed
            })
          )
        }
        if (dataRoot.DeviceNumber) {
          dispatch(
            showSticker({
              deviceNumber: dataRoot.DeviceNumber
            })
          )
        }
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString() + '. At getState', color: 'red' })))
  }
}

export function checkStatus(json) {
  return dispatch => {
    const status = _.get(json, 'DB.missionsPuzzle.status')
    if (status === 'puzzleComplete') {
      dispatch(
        setPuzzleStatus({
          status: 'complete',
          statusMessage: _.get(json, 'DB.missionsPuzzle.puzzleComplete')
        })
      )
    }
  }
}

export function loadPuzzle(puzzleID, subTestID) {
  const puzzleName = PUZZLES.find(puzzle => puzzle.id === puzzleID).name
  return dispatch => {
    return new Promise((resolve, reject) => {
      try {
        require.ensure([], require => {
          require('bundle-loader!../containers/Puzzles/' + puzzleName)(function(module) {
            dispatch(
              puzzleLoaded({
                puzzleName: puzzleName,
                subTestID: subTestID,
                puzzleModule: module
              })
            )
          })
        })
      } catch (e) {
        reject(e)
      }
    })
  }
}

export function enableDevice() {
  return dispatch => {
    return fetchUrl('&step=enable')
      .then(json => {
        const dataRoot = json && json.DB && json.DB.missionsPuzzle
        if (dataRoot.timeElapsed !== undefined) {
          dispatch(
            deviceEnabled({
              subjectNumber: dataRoot.SubjectNumber,
              subTestID: dataRoot.subTestID,
              timeElapsed: dataRoot.timeElapsed
            })
          )
        }
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const puzzleLoaded = createAction('puzzle loaded')
export const deviceEnabled = createAction('device enabled')
export const nextSubtest = createAction('next subtest')
export const setPuzzle = createAction('set puzzle')
export const showKeyboard = createAction('show keyboard')
export const hideKeyboard = createAction('hide keyboard')
export const showInfoBox = createAction('show info box')
export const setLoadProgress = createAction('set load progress')
export const showSticker = createAction('show sticker')
export const setSubjectNumber = createAction('set subject number')
export const setPuzzleStatus = createAction('set puzzle status')
