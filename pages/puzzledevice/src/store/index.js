import { createStore, applyMiddleware } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunkMiddleware from 'redux-thunk'
// import createLogger from 'redux-logger'
// import { logger } from '../middleware'
import rootReducer from '../reducers'

export default function configure(initialState) {
  const create = window.devToolsExtension ? window.devToolsExtension()(createStore) : createStore

  const createStoreWithMiddleware = responsiveStoreEnhancer(
    applyMiddleware(
      thunkMiddleware
      // createLogger(),
      // logger
    )(create)
  )

  const store = createStoreWithMiddleware(rootReducer, initialState)

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers')
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
