import { MAIN_URL } from '../constants'

const fetchUrl = (url) => fetch(
  MAIN_URL + url,
  {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
  }
)
  .then(response => {
    return response.text().then(text => {
      let json = null

      try {
        json = JSON.parse(text)

        if (json.result && json.result.error) {
          throw new Error(json.result.error)
        }
      } catch (e) {
        console.error(`Server responded with: ${text}`)
      }

      return json
    })
  })

export { fetchUrl }
