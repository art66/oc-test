import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Pad from '../../components/Pad'
import * as puzzlesActons from '../../actions/puzzles'
import LoadingScreen from '../../components/LoadingScreen'
import CompleteScreen from '../../components/CompleteScreen'

class App extends Component {
  constructor(props, context) {
    super(props, context)
    props.actions.getState()
  }
  render() {
    let CurrentSection
    let bootup = true
    const { puzzles, browser, actions } = this.props
    const { status, statusMessage } = puzzles
    const showLoadingScreen = !puzzles.puzzleLoaded || puzzles.loadProgress < 100

    if (!puzzles.enabled) {
    } else if (status === 'complete') {
      CurrentSection = <CompleteScreen statusMessage={statusMessage} />
    } else if (showLoadingScreen) {
      CurrentSection = <LoadingScreen key="1" puzzles={puzzles} actions={actions} />
    } else {
      const Section = puzzles.puzzleModule
      const key = !showLoadingScreen ? '' + puzzles.puzzleName + puzzles.subTestID : 'same'
      CurrentSection = <Section key={key} puzzles={puzzles} actions={actions} browser={browser} />
      if (puzzles.subTestID !== 1) bootup = false
    }

    // const CurrentSection = LoadingScreen

    // const bootup = CurrentSectio  === LoadingScreen
    return (
      <Pad puzzles={puzzles} browser={browser} actions={actions} bootup={bootup}>
        {CurrentSection}
      </Pad>
    )
  }
}

App.propTypes = {
  puzzles: PropTypes.object,
  browser: PropTypes.object,
  actions: PropTypes.object
}

function mapStateToProps(state) {
  return {
    puzzles: state.puzzles,
    browser: state.browser
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(puzzlesActons, dispatch),
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
