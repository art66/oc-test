// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'delete': string;
  'disabled': string;
  'edit': string;
  'globalSvgShadow': string;
  'icon': string;
  'publish': string;
  'restore': string;
  'unpublish': string;
}
export const cssExports: CssExports;
export default cssExports;
