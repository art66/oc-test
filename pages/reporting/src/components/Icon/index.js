import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

class Icon extends Component {
  static defaultProps = {
    disabled: false
  }

  render() {
    const { name, disabled } = this.props

    return <span className={cn(s.icon, s[name], { [s.disabled]: disabled })} />
  }
}

Icon.propTypes = {
  name: PropTypes.oneOf(['edit', 'publish', 'unpublish', 'delete', 'restore']),
  disabled: PropTypes.bool
}

export default Icon
