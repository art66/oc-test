import React, { Component } from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'
import Tooltip from './Tooltip'
import IInputField from '../../interfaces/IInputField'

interface IProps {
  label: string
  field: string
  state: {
    title: IInputField
    subtitle: IInputField
  }
  getCharsLeft: number
  onChange: (e: any) => void
  onFocus: () => void
  onBlur: () => void
  isVisible: boolean
  checkValidTitleLines?: (x: boolean) => void
}

class InputField extends Component<IProps> {
  render() {
    const {
      label,
      field,
      state,
      getCharsLeft,
      onChange,
      onFocus,
      onBlur,
      isVisible,
      checkValidTitleLines
    } = this.props as IProps

    return (
      <div className={s.group}>
        <label className={s.label}>{`${label}:`}</label>
        <span className={s.charsLeft}>({getCharsLeft} characters left)</span>
        <input
          className={cn(s.input, s[field])}
          value={state[field].value}
          onChange={onChange}
          onFocus={onFocus}
          onBlur={onBlur}
        />
        <Tooltip
          title={state.title.value}
          subtitle={field === 'subtitle' ? state.subtitle.value : ''}
          isVisible={isVisible}
          field={`${field}`}
          checkValidTitleLines={checkValidTitleLines}
        />
      </div>
    )
  }
}

export default InputField
