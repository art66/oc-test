import React, { Component } from 'react'
import cn from 'classnames'
import { fetchUrl } from '../../utils'
import {
  BYTES_IN_MEGABYTES,
  TITLE_MAX_LENGTH,
  SUBTITLE_MAX_LENGTH,
  LEAD_IMAGE_MAX_LENGTH,
  LEAD_IMAGE_MAX_WIDTH,
  LEAD_IMAGE_MAX_HEIGHT,
  LEAD_IMAGE_MAX_FILE_SIZE,
  LEAD_IMAGE_EXTENSION
} from '../../constants'
import s from './styles.cssmodule.scss'
import Tooltip from './Tooltip'
import ICustomWindow from '../../interfaces/ICustomWindow'
import IInputField from '../../interfaces/IInputField'
import InputField from './InputField'
/* global $ */

declare let window: ICustomWindow

interface IState {
  title: IInputField
  subtitle: IInputField
  leadImage: {
    maxLength: number
    maxWidth: number
    maxHeight: number
    maxFileSize: number
    extension: string[]
    value: string
    tooltipVisible: boolean
  }
  text: {
    value: string
  }
}

interface IProps {
  articleID: string
  description?: string
  withSubtitle: boolean
  withLeadImage: boolean
  formTitle?: string
  textLabel?: string
  smallControls: boolean
  submissionId?: string
  onSubmit: Function
  checkValidTitleLines: (x: boolean) => void
  notValidTitleLines: boolean
}

class Form extends Component<IProps, IState> {
  static defaultProps = {
    withSubtitle: true,
    withLeadImage: true,
    smallControls: false
  }
  submitted: boolean
  textInput: HTMLElement
  textArea: HTMLTextAreaElement

  constructor(props: IProps) {
    super(props)
    this.submitted = false
    this.state = {
      title: {
        maxLength: TITLE_MAX_LENGTH,
        value: '',
        tooltipVisible: false
      },
      subtitle: {
        maxLength: SUBTITLE_MAX_LENGTH,
        value: '',
        tooltipVisible: false
      },
      leadImage: {
        maxLength: LEAD_IMAGE_MAX_LENGTH,
        maxWidth: LEAD_IMAGE_MAX_WIDTH,
        maxHeight: LEAD_IMAGE_MAX_HEIGHT,
        maxFileSize: LEAD_IMAGE_MAX_FILE_SIZE,
        extension: LEAD_IMAGE_EXTENSION,
        value: '',
        tooltipVisible: false
      },
      text: {
        value: ''
      }
    }
  }

  componentDidMount() {
    if (this.props.articleID && this.props.articleID !== 'new') {
      this.loadFormData()
    } else {
      this.initBBCodeEditor()
    }
  }

  handleSubmit = _e => {}

  loadFormData = () => {
    const { articleID } = this.props

    return fetchUrl('step=getEditPanel', { articleID })
      .then(data => {
        this.setFormField('title', data.title)
        this.setFormField('subtitle', data.subtitle)
        this.setFormField('leadImage', data.leadImage ? data.leadImage : '')
        this.setFormField('text', data.text)
        this.initBBCodeEditor(data, `newspaper_edit_article_${articleID}`)
      })
      .catch(error =>
        window.showReportingInfoBox({
          msg: error.message,
          color: 'red'
        })
      )
  }

  initBBCodeEditor = (data: any = {}, id: string = this.props.submissionId || 'newspaper_new_article') => {
    window.$('#bbc-editor-wrapper').editorPlugin({
      action: 'setContent',
      content: data.text || '',
      uniqueId: id,
      disableAutoDelete: true,
      editorSubmitHandler: (e, clearEditorCacheCallback) => {
        if (this.submitted || !this.props.onSubmit) {
          return
        }

        const data = {
          title: this.state.title.value,
          subtitle: this.state.subtitle.value,
          leadImage: this.state.leadImage.value,
          text: this.textArea.value
        }

        const promise = this.props.onSubmit(e, data)
        if (promise && promise.then) {
          promise.then(resp => {
            this.submitted = false

            if (resp && resp.success) {
              this.setFormField('title', '')
              this.setFormField('subtitle', '')
              this.setFormField('leadImage', '')
              this.setFormField('text', '')
              this.textArea.value = ''
              this.textInput.innerHTML = ''
              clearEditorCacheCallback()
              this.forceUpdate()
            }
          })
          this.submitted = true
        }
      }
    })
    this.textArea = document.getElementById('editor-textarea') as HTMLTextAreaElement
    this.textInput = document.getElementById('main-input')
  }

  setFormField = (fieldName: string, value: string = '') => {
    // notValidTitleCondition, preventEnteringNotValidTitle - adding for check validation on lines for title
    const { notValidTitleLines } = this.props
    const notValidTitleCondition = fieldName === 'title' && notValidTitleLines
    const preventEnteringNotValidTitle = notValidTitleCondition && value.length > this.state[fieldName].value.length
    const fieldValue = notValidTitleCondition ? this.state[fieldName].value.slice(0, -1) : value

    if (this.state[fieldName] && this.state[fieldName].maxLength && this.state[fieldName].maxLength < value.length
      || preventEnteringNotValidTitle) {
      return
    }

    this.setState({
      [fieldName]: {
        ...this.state[fieldName],
        value: fieldValue
      }
    } as IState)
  }

  isInputTooltipVisible = (field: string = 'title') => {
    if (location.href.indexOf('articles') === -1) {
      return false
    }

    return this.state[field].tooltipVisible
  }

  showInputTooltip = (field: string = 'title') => {
    this.setState({
      [field]: {
        ...this.state[field],
        tooltipVisible: true
      }
    } as IState)
  }

  hideInputTooltip = (field: string = 'title') => {
    this.setState({
      [field]: {
        ...this.state[field],
        tooltipVisible: false
      }
    } as IState)
  }

  getCharsLeft = fieldName => this.state[fieldName].maxLength - this.state[fieldName].value.length

  fileChangedHandler = e => {
    e.preventDefault()
    const file = e.target.files[0]
    this.handleImageLoad(file)
  }

  handleImageLoad = file => {
    const reader = new FileReader()
    const fileName = file.name
    const fileSize = file.size
    const fileExtension = fileName.replace(/^.*\./, '')
    let fileWidth, fileHeight

    reader.onload = () => {
      const img = new Image()
      img.src = reader.result as string
      img.onload = () => {
        fileWidth = img.width
        fileHeight = img.height
        if (this.validateImage({ fileSize, fileWidth, fileHeight, fileExtension })) {
          this.setFormField('leadImage', img.src)
          window.showReportingInfoBox({
            msg: '',
            color: ''
          })
        } else {
          this.setFormField('leadImage', '')
          window.showReportingInfoBox({
            msg: 'Please choose file with necessary requirements',
            color: 'red'
          })
        }
      }
    }
    reader.readAsDataURL(file)
  }

  validateImage = infoImage => {
    const { fileSize, fileWidth, fileHeight, fileExtension } = infoImage
    const { maxWidth, maxHeight, maxFileSize, extension } = this.state.leadImage
    return (
      fileSize <= maxFileSize * BYTES_IN_MEGABYTES * BYTES_IN_MEGABYTES &&
      fileWidth === maxWidth &&
      fileHeight === maxHeight &&
      extension.indexOf(fileExtension) > -1
    )
  }

  renderSubtitle = () => {
    return (
      <InputField
        label='Subtitle'
        field='subtitle'
        state={this.state}
        getCharsLeft={this.getCharsLeft('subtitle')}
        onChange={e => this.setFormField('subtitle', e.target.value)}
        onFocus={() => this.showInputTooltip('subtitle')}
        onBlur={() => this.hideInputTooltip('subtitle')}
        isVisible={this.isInputTooltipVisible('subtitle')}
      />
    )
  }

  renderTitle = () => {
    const { checkValidTitleLines } = this.props

    return (
      <InputField
        label='Title'
        field='title'
        state={this.state}
        getCharsLeft={this.getCharsLeft('title')}
        onChange={e => this.setFormField('title', e.target.value)}
        onFocus={() => this.showInputTooltip('title')}
        onBlur={() => this.hideInputTooltip('title')}
        isVisible={this.isInputTooltipVisible('title')}
        checkValidTitleLines={checkValidTitleLines}
      />
    )
  }

  renderLeadImage = () => {
    return (
      <div className={cn(s.group, s.groupUploadImage)}>
        <div
          className={cn(s.leftGroupUploadImage, this.state.leadImage.value ? s.cursorPointer : '')}
          onMouseEnter={() => (this.state.leadImage.value ? this.showInputTooltip('leadImage') : null)}
          onMouseLeave={() => this.hideInputTooltip('leadImage')}
        >
          <span className={s.label}>Lead article image </span>
          <span className={s.fileSize}>(PNG, 264x198px)</span>
        </div>
        <div className={s.rightGroupUploadImage}>
          <label className={`torn-btn silver ${s.uploadBtn}`}>
            <span>Upload</span>
            <input
              type='file'
              className={s.uploadInput}
              onChange={this.fileChangedHandler}
              onFocus={() => this.showInputTooltip('leadImage')}
              onBlur={() => this.hideInputTooltip('leadImage')}
            />
          </label>
        </div>
        <Tooltip
          title={this.state.title.value}
          subtitle={this.state.subtitle.value}
          leadImage={this.state.leadImage.value}
          isVisible={this.isInputTooltipVisible('leadImage')}
          field='leadImage'
        />
      </div>
    )
  }

  renderLeadArticleMsg = () => {
    return <span className={s.notification}>This article will become the lead article</span>
  }

  renderArticleDescription = description => {
    return (
      <div>
        <div className={s.description} dangerouslySetInnerHTML={{ __html: description }} />
        <span className={s.divider} />
      </div>
    )
  }

  render() {
    const {
      description,
      withLeadImage,
      withSubtitle,
      formTitle = 'New Article',
      textLabel = 'Article',
      smallControls
    } = this.props

    return (
      <div className={s.form}>
        <div className='title-black top-round'>{formTitle}</div>
        <div className={cn(s.content, 'bottom-round')}>
          {description && this.renderArticleDescription(description)}
          <form onSubmit={e => this.handleSubmit(e)} id='editor-form'>
            {this.renderTitle()}
            {withSubtitle && this.renderSubtitle()}
            {withLeadImage && this.renderLeadImage()}
            <label className={cn(s.label, s.articleLabel)}>{textLabel}:</label>
            {this.state.subtitle.value.length > 0 && this.renderLeadArticleMsg()}
            <div id='bbc-editor-wrapper' className={cn({ 'controls-small': smallControls })} />
          </form>
        </div>
      </div>
    )
  }
}

export default Form
