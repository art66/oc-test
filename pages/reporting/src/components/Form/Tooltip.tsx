import React, { Component } from 'react'
import cn from 'classnames'
import ICustomWindow from '../../interfaces/ICustomWindow'
import s from './tooltip.cssmodule.scss'

interface IProps {
  title: string
  subtitle?: string
  leadImage?: string
  field: string
  isVisible: boolean
  checkValidTitleLines?: (x: boolean) => void
}

interface IState {
  errorTitle: boolean
}

declare let window: ICustomWindow

const HEIGHT_TITLE = 58

class Tooltip extends Component<IProps, IState> {
  tooltip: any

  constructor(props: IProps) {
    super(props)
    this.tooltip = React.createRef()
    this.state = {
      errorTitle: false
    }
  }

  componentDidUpdate() {
    const { field, checkValidTitleLines } = this.props
    const { errorTitle } = this.state

    if (field === 'title' && checkValidTitleLines) {
      const isCorrectHeightTooltip = this.tooltip.current && (this.tooltip.current.clientHeight > HEIGHT_TITLE)
      let informMessage = { msg: '', color: '' }

      if (isCorrectHeightTooltip) {
        informMessage = { msg: 'The title of the article more than 2 lines', color: 'red' }
      }

      if (errorTitle !== isCorrectHeightTooltip) {
        this.setState({ errorTitle: isCorrectHeightTooltip })
        checkValidTitleLines(isCorrectHeightTooltip)
      }

      window.showReportingInfoBox(informMessage)
    }
  }

  renderLeadImageTooltip = leadImage => {
    return (
      <div className={s.leadImage}>
        <img src={leadImage} alt='Lead image' />
      </div>
    )
  }

  render() {
    const { title, subtitle, leadImage, field, isVisible } = this.props as IProps
    const { errorTitle } = this.state
    const tooltipClasses = cn({
      [s.tooltip]: true,
      [s[field]]: true,
      [s.error]: errorTitle
    })

    if (!isVisible || (!title && !subtitle && !leadImage)) {
      return <div />
    }

    return (
      <div className={tooltipClasses} ref={this.tooltip}>
        <div className={s.title}>{title}</div>
        {<div className={s.subtitle}>{subtitle}</div>}
        {field === 'leadImage' && leadImage && this.renderLeadImageTooltip(leadImage)}
      </div>
    )
  }
}

export default Tooltip
