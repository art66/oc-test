import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

class TabControls extends Component {
  getTabs = () =>
    this.props.tabs.map((tab, index) => (
      <span className={cn(s.tabControl, { [s.active]: tab.active })} onClick={() => tab.onClick(tab.name)}>
        {tab.name}
      </span>
    ))

  render() {
    return <div className={s.tabControls}>{this.getTabs()}</div>
  }
}

TabControls.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      active: PropTypes.bool,
      onClick: PropTypes.func
    })
  )
}

export default TabControls
