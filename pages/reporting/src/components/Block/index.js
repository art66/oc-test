import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'

class Block extends Component {
  render() {
    const { children, loading, loadingDefault, className } = this.props

    return (
      <div className={className || ''}>
        {loading || (loadingDefault && loading === 'undefined') ? <Preloader /> : children}
      </div>
    )
  }
}

Block.propTypes = {
  children: PropTypes.node,
  loading: PropTypes.bool,
  loadingDefault: PropTypes.bool,
  className: PropTypes.string
}

const mapStateToProps = state => {
  let route = 'frontpage'
  if (location.hash.indexOf('articles') !== -1) {
    route = 'article'
  } else if (location.hash.indexOf('archive') !== -1) {
    route = 'archive'
  }

  return {
    loadingDefault: state[route].loading
  }
}

export default connect(mapStateToProps)(Block)
