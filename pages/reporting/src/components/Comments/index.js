import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Button from '@torn/shared/components/Button'
import s from './styles.cssmodule.scss'
import { fetchUrl } from '../../utils'

class Comments extends Component {
  constructor(props) {
    super(props)
    this.state = {
      comments: [],
      user: {}
    }
  }

  componentDidMount() {
    this.props.onRef && this.props.onRef(this)
    this.fetchComments()
  }

  componentWillUnmount() {
    this.props.onRef && this.props.onRef(undefined)
  }

  fetchComments = () => {
    fetchUrl('step=getComments', { articleID: this.props.articleID })
      .then(data => this.setState({
          comments: data.comments || [],
          user: data.user || {}
        }))
      .catch(error => window.showReportingInfoBox({
          msg: error.message,
          color: 'red'
        }))
  }

  getComments = () => {
    return this.state.comments
      .slice()
      .reverse()
      .map((comment, index) => (
        <li key={index}>
          {comment.author.userID !== 0 && (
            <a className={s.authorLink} href={`/profiles.php?XID=${comment.author.userID}`}>
              {comment.author.playername}:
            </a>
          )}
          <span dangerouslySetInnerHTML={{ __html: comment.text }} />
        </li>
      ))
  }

  handleSubmit = e => {
    e.preventDefault()
    const { user } = this.state

    this.saveComment({
      text: this.textInput.value,
      author: {
        userID: user.userID,
        playername: user.playername
      }
    })
  }

  saveComment = comment => {
    if (!comment || !comment.text || comment.text.length === 0) {
      window.showReportingInfoBox({
        msg: 'You cannot send an empty comment',
        color: 'red'
      })

      return
    }

    this.setState({
      comments: this.state.comments.concat([comment])
    })

    return fetchUrl('step=addComment', {
      articleID: parseInt(this.props.articleID),
      text: comment.text
    })
      .then(response => {
        if (response && response.success) {
          this.textInput.value = ''
        }
      })
      .catch(error => window.showReportingInfoBox({
          msg: error.message,
          color: 'red'
        }))
  }

  render() {
    const comments = this.getComments()

    return (
      <div className={cn(s.comments, 'cont-gray')}>
        <div className='title-black top-round'>Comment</div>
        <div className={s.content}>
          <form onSubmit={e => this.handleSubmit(e)}>
            <label className={s.label}>Your comment:</label>
            <textarea
              className={s.textInput}
              ref={el => {
                this.textInput = el
              }}
            />
            <Button type='submit'>SUBMIT</Button>
          </form>
          <ul className={s.list}>{comments}</ul>
        </div>
      </div>
    )
  }
}

Comments.propTypes = {
  articleID: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onRef: PropTypes.func
}

export default Comments
