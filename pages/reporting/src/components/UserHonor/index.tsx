import React from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

type TOnlineStatus = 'online' | 'offline' | 'idle' | 'Online' | 'Offline' | 'Idle' | ''

interface IProps {
  userID: number
  playername: string
  awardImage: string
  onlineStatus: TOnlineStatus
  smallOnTablet: boolean
  smallOnMobile: boolean
}
/* eslint-disable jsx-a11y/img-redundant-alt */
const getUserOnlineStatus = (onlineStatus) => onlineStatus
  && <span className={cn('onlineStatus', s.onlineStatus, s[onlineStatus.toLowerCase()])} title={onlineStatus} />

const getUser = (userID, playername, awardImage) => {
  return (
    <a
      href={`/profiles.php?XID=${userID}`}
      title={`${playername} [${userID}]`}
      className={cn('link user name', s.link)}
    >
      {awardImage ? <img src={awardImage} alt='Honor bar image' /> : playername}
    </a>
  )
}

const UserHonor = ({ userID, playername, awardImage, onlineStatus = '', smallOnTablet, smallOnMobile }: IProps) => {
  return (
    <div className={cn({ [s.smallOnTablet]: smallOnTablet, [s.smallOnMobile]: smallOnMobile })}>
      {getUserOnlineStatus(onlineStatus)}
      {getUser(userID, playername, awardImage)}
    </div>
  )
}

export default UserHonor
