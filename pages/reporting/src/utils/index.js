import { MAIN_URL } from '../constants'
/* global addRFC */

export const fetchUrl = (url, data) => {
  let fullUrl = url.startsWith('/') ? url : MAIN_URL + url

  return fetch(addRFC(fullUrl), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      try {
        var json = JSON.parse(text)
        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = 'Server responded with: ' + text
      }

      if (error) {
        throw new Error(error.message || (typeof error === 'object' ? Object.values(error) : error))
      } else {
        return json
      }
    })
  })
}
