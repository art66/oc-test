import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link, hashHistory } from 'react-router'
import InfoBox from '@torn/shared/components/InfoBox'
import { SVGIconGenerator } from '@torn/shared/SVG'
import cn from 'classnames'
import s from './styles.cssmodule.scss'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'

class CoreLayout extends Component {
  constructor(props) {
    super(props)
    this.state = { infoBox: {} }
  }

  componentDidMount() {
    window.showReportingInfoBox = (infoBox = {}) => {
      this.setState({ infoBox })
    }
    hashHistory.listen(() => {
      this.setState({ infoBox: {} })
    })
  }

  isReportingSuperUser = () => getCookie('reportingSuperUser') === '1'

  render() {
    const { children } = this.props
    const reportingSuperUser = this.isReportingSuperUser()

    return (
      <div>
        <div className={cn(s.header, 'clearfix')}>
          <h4>Newspaper</h4>
          <ul className={s.links}>
            <li>
              <a href='/newspaper.php' className={cn(s.link, s.reporters, 'back-to')}>
                <SVGIconGenerator
                  customClass={s.icon}
                  iconName='Back'
                  preset={{
                    type: 'topPageLinks',
                    subtype: 'Back'
                  }}
                />
                <span className={s.linkText}>Newspaper</span>
              </a>
            </li>
            {reportingSuperUser && this.props.location.pathname !== '/submissions' && (
              <li>
                <Link to='/submissions' className={cn(s.link, s.playerGossip, 'player-gossip')}>
                  <SVGIconGenerator
                    customClass={s.icon}
                    iconName='AllSubmissions'
                    preset={{
                      type: 'topPageLinks',
                      subtype: 'AllSubmissions'
                    }}
                  />
                  <span className={s.linkText}>Player Gossip</span>
                </Link>
              </li>
            )}
            {this.props.location.pathname !== '/headlines'
              && reportingSuperUser && (
              <li>
                <Link to='/headlines' className={cn(s.link, s.newsTicker, 'news-ticker')} onlyActiveOnIndex>
                  <SVGIconGenerator
                    customClass={s.icon}
                    iconName='NewsTicker'
                    preset={{
                      type: 'topPageLinks',
                      subtype: 'NewsTicker'
                    }}
                  />
                  <span className={s.linkText}>News ticker</span>
                </Link>
              </li>
            )}
            {this.props.location.pathname !== '/' && (
              <li>
                <Link to='/' className={cn(s.link, s.reporters, 'reporting')}>
                  <SVGIconGenerator
                    customClass={s.icon}
                    iconName='ReportingPanel'
                    preset={{
                      type: 'topPageLinks',
                      subtype: 'ReportingPanel'
                    }}
                  />
                  <span className={s.linkText}>Reporting</span>
                </Link>
              </li>
            )}
            {this.props.location.pathname !== '/newsletter' && (
              <li>
                <Link to='/newsletter' className={cn(s.link, s.reporters, 'newsletter')}>
                  <SVGIconGenerator
                    customClass={s.icon}
                    iconName='Newsletter'
                    preset={{
                      type: 'topPageLinks',
                      subtype: 'Newsletter'
                    }}
                  />
                  <span className={s.linkText}>Newsletter</span>
                </Link>
              </li>
            )}
            {this.props.location.pathname !== '/reporters' && (
              <li>
                <Link to='/reporters' className={cn(s.link, s.reporters, 'people')}>
                  <SVGIconGenerator
                    customClass={s.icon}
                    iconName='Reporters'
                    preset={{
                      type: 'topPageLinks',
                      subtype: 'Reporters'
                    }}
                  />
                  <span className={s.linkText}>Reporters</span>
                </Link>
              </li>
            )}
          </ul>
        </div>
        <hr className='delimiter-999 m-top5 m-bottom10' />
        {this.state.infoBox && <InfoBox msg={this.state.infoBox.msg} color={this.state.infoBox.color} />}
        <div className='core-layout__viewport'>{children}</div>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
  location: PropTypes.object
}

export default CoreLayout
