interface IInputField {
  maxLength: number
  value: string
  tooltipVisible: boolean
}

export default IInputField
