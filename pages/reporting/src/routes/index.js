import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import Articles from './Articles'
import Article from './Article'
import NewArticle from './NewArticle'
import Submissions from './Submissions'
import Reporters from './Reporters'
import Newsletter from './Newsletter'
import Headlines from './Headlines'

export const createRoutes = store => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Articles(store),
  childRoutes: [
    Article(store),
    NewArticle(store),
    Submissions(store),
    Reporters(store),
    Newsletter(store),
    Headlines(store)
  ]
})

export default createRoutes
