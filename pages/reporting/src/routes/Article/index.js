import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'articles/:articleID',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Article = require('./containers/ArticleContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'article', reducer })
        cb(null, Article)
      },
      'article'
    )
  }
})
