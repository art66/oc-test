import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'

const initialState = {
  article: {
    articleID: 0,
    title: '',
    status: '',
    author: {
      userID: 0,
      playername: ''
    },
    text: '',
    actions: {}
  },
  openedArticle: 0,
  loading: false,
  user: {},
  tab: 'article'
}

export const loading = createAction('loading', (loading = true) => ({ loading }))
export const setPreviewState = createAction('set preview state', data => ({ data }))
export const resetPreviewState = createAction('reset preview state')
export const openArticle = createAction('open article', (articleID = 0) => ({ articleID }))
export const publishArticle = createAction('publish article')
export const unpublishArticle = createAction('unpublish article')
export const removeArticle = createAction('remove article', articleID => ({ articleID }))
export const restoreArticle = createAction('restore article', articleID => ({ articleID }))
export const confirmArticle = createAction('confirm article', articleID => ({ articleID }))
export const addComment = createAction('add comment', comment => ({ comment }))
export const setTab = createAction('set tab', tab => ({ tab }))
export const showDialog = createAction('show dialog', dialog => ({ dialog }))
export const hideDialog = createAction('hide dialog')

export const loadPreview = articleID => (dispatch, getState) => {
  return fetchUrl('step=getPreviewPanel', { articleID })
    .then(data => {
      dispatch(setPreviewState(data))
    })
    .catch(error =>
      window.showReportingInfoBox({
        msg: error.message,
        color: 'red'
      })
    )
}

export const publish = articleID => dispatch => {
  dispatch(publishArticle(articleID))

  return fetchUrl('step=publishArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const unpublish = articleID => dispatch => {
  dispatch(unpublishArticle(articleID))

  return fetchUrl('step=unpublishArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const remove = articleID => dispatch => {
  dispatch(removeArticle(articleID))

  return fetchUrl('step=deleteArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const restore = articleID => dispatch => {
  dispatch(restoreArticle(articleID))

  return fetchUrl('step=restoreArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const confirm = articleID => dispatch => {
  dispatch(confirmArticle(articleID))

  return fetchUrl('step=confirmArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const saveComment = comment => dispatch => {
  dispatch(addComment(comment))
}

export default handleActions(
  {
    'set preview state'(state, action) {
      return {
        ...state,
        ...action.payload.data
      }
    },
    'reset preview state'() {
      return {
        ...initialState
      }
    },
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'open article'(state, action) {
      return {
        ...state,
        openedArticle: state.openedArticle === action.payload.articleID ? 0 : action.payload.articleID
      }
    },
    'publish article'(state) {
      return {
        ...state,
        article: {
          ...state.article,
          published: true
        },
        dialog: ''
      }
    },
    'unpublish article'(state) {
      return {
        ...state,
        article: {
          ...state.article,
          published: false
        },
        dialog: ''
      }
    },
    'remove article'(state) {
      return {
        ...state,
        article: {
          ...state.article,
          deleted: true
        },
        dialog: ''
      }
    },
    'restore article'(state) {
      return {
        ...state,
        article: {
          ...state.article,
          deleted: false
        },
        dialog: ''
      }
    },
    'confirm article'(state) {
      return {
        ...state,
        article: {
          ...state.article,
          confirmed: true
        },
        dialog: ''
      }
    },
    'set tab'(state, action) {
      return {
        ...state,
        tab: action.payload.tab
      }
    },
    'show dialog'(state, action) {
      return {
        ...state,
        dialog: action.payload.dialog
      }
    },
    'hide dialog'(state, action) {
      return {
        ...state,
        dialog: ''
      }
    }
  },
  initialState
)
