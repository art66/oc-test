import { connect } from 'react-redux'
import {
  resetPreviewState,
  loadPreview,
  publish,
  unpublish,
  openArticle,
  remove,
  restore,
  confirm,
  saveComment,
  setTab,
  showDialog,
  hideDialog
} from '../modules'
import Article from '../components/Article'

const mapDispatchToProps = {
  resetPreviewState,
  loadPreview,
  publish,
  unpublish,
  openArticle,
  remove,
  restore,
  confirm,
  saveComment,
  setTab,
  showDialog,
  hideDialog
}

const mapStateToProps = state => ({
  article: state.article.article,
  user: state.article.user,
  tab: state.article.tab,
  dialog: state.article.dialog,
  mediaType: state.browser.mediaType
})

export default connect(mapStateToProps, mapDispatchToProps)(Article)
