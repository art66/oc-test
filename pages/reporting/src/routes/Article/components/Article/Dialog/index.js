import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'

class Dialog extends Component {
  handleYesClick = () => {
    const { dialog, article } = this.props
    const action = dialog

    try {
      this.props[action](article.articleID)
    } catch (error) {
      console.error(error)
    }
  }

  getMessage = () => {
    const { article, dialog } = this.props
    let message

    if (dialog === 'confirm' || dialog === 'remove' || dialog === 'restore' || dialog === 'unpublish') {
      message = (
        <span>
          Are you sure you want to {dialog} this article written by
          <a href={`/profiles.php?XID=${article.author.userID}`} className="t-blue h c-pointer bold m-left5">
            {article.author.playername}
          </a>
        </span>
      )
    } else if (dialog === 'publish') {
      message = (
        <span>
          Are you sure you want to publish this article written by
          <a href={`/profiles.php?XID=${article.author.userID}`} className="t-blue h c-pointer bold m-left5">
            {article.author.playername}
          </a>
          <div>It will appear in the newspaper for all players to read.</div>
        </span>
      )
    }

    return message
  }

  render() {
    const { hideDialog } = this.props

    return (
      <div className={s.dialog}>
        {this.getMessage()}
        <div className={s.actions}>
          <span onClick={this.handleYesClick} className="t-blue h c-pointer bold m-right10">
            Yes
          </span>
          <span onClick={hideDialog} className="t-blue h c-pointer bold">
            No
          </span>
        </div>
      </div>
    )
  }
}

Dialog.propTypes = {
  dialog: PropTypes.string,
  article: PropTypes.object,
  hideDialog: PropTypes.func,
  confirm: PropTypes.func,
  unconfirm: PropTypes.func,
  remove: PropTypes.func,
  restore: PropTypes.func,
  publish: PropTypes.func,
  unpublish: PropTypes.func,
}

export default Dialog
