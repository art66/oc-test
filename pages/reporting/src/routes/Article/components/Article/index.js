import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import cn from 'classnames'
import Button from '@torn/shared/components/Button'
import Comments from '../../../../components/Comments'
import Dialog from './Dialog'
import s from './styles.cssmodule.scss'

class Article extends Component {
  constructor(props) {
    super(props)
    this.props.resetPreviewState()
    this.props.loadPreview(this.props.params.articleID)
  }

  componentDidUpdate(prevProps) {
    const { article } = this.props

    const isArticleChanged = article.confirmed !== prevProps.article.confirmed
    const isArticleDeleted = article.deleted !== prevProps.article.deleted
    const isArticlePublished = article.published !== prevProps.article.published

    if (isArticleChanged || isArticleDeleted || isArticlePublished) {
      this.comments && this.comments.fetchComments()
    }
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps(newProps) {
  //   const { article } = this.props

  //   if (
  //     article.confirmed !== newProps.article.confirmed ||
  //     article.deleted !== newProps.article.deleted ||
  //     article.published !== newProps.article.published
  //   ) {
  //     this.comments && this.comments.fetchComments()
  //   }
  // }

  getActions = () => {
    const { article, dialog, showDialog, hideDialog, confirm, remove, restore, publish, unpublish } = this.props
    const { actions } = article

    if (dialog) {
      return (
        <Dialog
          dialog={dialog}
          article={article}
          hideDialog={hideDialog}
          confirm={confirm}
          remove={remove}
          restore={restore}
          publish={publish}
          unpublish={unpublish}
        />
      )
    }

    return (
      <div className={s.actions}>
        <Button
          className={s.action}
          onClick={() => showDialog('confirm')}
          disabled={article.confirmed || !actions.confirm || article.deleted}
        >
          CONFIRM
        </Button>
        <Link
          className={s.action}
          to={`/articles/edit/${article.articleID}`}
          onClick={e => (article.deleted || !actions.edit) && e.preventDefault()}
        >
          <Button disabled={article.deleted || !actions.edit}>EDIT</Button>
        </Link>
        {article.deleted ? (
          <Button className={s.action} onClick={() => showDialog('restore')} disabled={!actions.restore}>
            RESTORE
          </Button>
        ) : (
          <Button className={s.action} onClick={() => showDialog('remove')} disabled={!actions.delete}>
            DELETE
          </Button>
        )}
        {article.published ? (
          <Button className={s.action} onClick={() => showDialog('unpublish')} disabled={!actions.unpublish}>
            UNPUBLISH
          </Button>
        ) : (
          <Button
            className={s.action}
            onClick={() => showDialog('publish')}
            disabled={!actions.publish || article.deleted}
          >
            PUBLISH
          </Button>
        )}
      </div>
    )
  }

  getArticle = () => {
    const { title, subtitle, text, author, createdDate, createdTimeAgo, showDialog, hideDialog } = this.props.article

    return (
      <div className={s.article}>
        <div className="title-black top-round">Article</div>
        <div>
          <div className={s.header}>
            <h5 className={s.title}>{title}</h5>
            <div className={s.info}>
              <a href={`/profiles.php?XID=${author.userID}`} className={s.author}>
                {author.playername}
              </a>
              <span className={s.date}>
                on {createdDate} ({createdTimeAgo})
              </span>
            </div>
          </div>
          {subtitle && <div className={s.subtitle} dangerouslySetInnerHTML={{ __html: subtitle }} />}
          <div className={s.text} dangerouslySetInnerHTML={{ __html: text }} />
          {this.getActions()}
        </div>
      </div>
    )
  }

  render() {
    const article = this.getArticle()
    const { user, tab, mediaType, saveComment, setTab } = this.props
    const isDesktopMode = !document.body.classList.contains('r')

    return (
      <div className={s.preview}>
        <div className={s.tabControls}>
          <span className={cn(s.tabControl, { [s.active]: tab === 'article' })} onClick={() => setTab('article')}>
            ARTICLE
          </span>
          <span className={cn(s.tabControl, { [s.active]: tab === 'comments' })} onClick={() => setTab('comments')}>
            DISCUSSION
          </span>
        </div>
        <div className={s.tabs}>
          {tab !== 'article' && mediaType !== 'desktop' && !isDesktopMode ? '' : article}
          {tab !== 'comments' && mediaType !== 'desktop' && !isDesktopMode ? (
            ''
          ) : (
            <Comments
              articleID={this.props.params.articleID}
              user={user}
              saveComment={saveComment}
              onRef={ref => (this.comments = ref)}
            />
          )}
        </div>
      </div>
    )
  }
}

Article.propTypes = {
  params: PropTypes.shape({
    articleID: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  }),
  article: PropTypes.shape({
    articleID: PropTypes.number,
    title: PropTypes.string,
    text: PropTypes.string,
    status: PropTypes.string,
    author: PropTypes.shape({
      userID: PropTypes.number,
      playername: PropTypes.string
    }),
    createdDate: PropTypes.string,
    createdTimeAgo: PropTypes.string,
    actions: PropTypes.object
  }),
  user: PropTypes.object,
  tab: PropTypes.string,
  mediaType: PropTypes.string,
  dialog: PropTypes.string,
  loadPreview: PropTypes.func,
  openedArticle: PropTypes.number,
  openArticle: PropTypes.func,
  publish: PropTypes.func,
  unpublish: PropTypes.func,
  remove: PropTypes.func,
  restore: PropTypes.func,
  confirm: PropTypes.func,
  setTab: PropTypes.func,
  saveComment: PropTypes.func,
  showDialog: PropTypes.func,
  hideDialog: PropTypes.func,
  resetPreviewState: PropTypes.func
}

export default Article
