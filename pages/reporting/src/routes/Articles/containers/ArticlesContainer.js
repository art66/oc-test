import { connect } from 'react-redux'
import { loadArticles, publish, unpublish, openArticle, remove, restore, lazyLoad, unsetArticles } from '../modules'
import Articles from '../components/Articles'

const mapDispatchToProps = {
  loadArticles,
  publish,
  unpublish,
  openArticle,
  remove,
  restore,
  lazyLoad,
  unsetArticles
}

const mapStateToProps = state => ({
  articles: state.articles.articles,
  openedArticle: state.articles.openedArticle,
  loading: state.articles.loading,
  allArticlesLoaded: state.articles.allArticlesLoaded
})

export default connect(mapStateToProps, mapDispatchToProps)(Articles)
