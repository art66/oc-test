import { injectReducer } from '../../store/reducers'

export default store => ({
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Articles = require('./containers/ArticlesContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'articles', reducer })
        cb(null, Articles)
      },
      'articles'
    )
  }
})
