import { createAction, handleActions } from 'redux-actions'
import { unionBy, orderBy } from 'lodash'
import { fetchUrl } from '../../../utils'
const LAZY_LOAD_ARTICLES_LENGTH = 20

const initialState = {
  articles: [],
  openedArticle: 0,
  loading: false,
  allAritclesLoaded: false
}

export const loading = createAction('loading', (loading = true) => ({ loading }))
export const setArticles = createAction('set articles', articles => ({ articles }))
export const openArticle = createAction('open article', (articleID = 0) => ({ articleID }))
export const publishArticle = createAction('publish article', articleID => ({ articleID }))
export const unpublishArticle = createAction('unpublish article', articleID => ({ articleID }))
export const removeArticle = createAction('remove article', articleID => ({ articleID }))
export const restoreArticle = createAction('restore article', articleID => ({ articleID }))
export const allArticlesLoaded = createAction('all articles loaded')

export const loadArticles = (range = [0, 30]) => (dispatch, getState) => {
  dispatch(loading(true))
  const articles = getState().articles.articles

  fetchUrl('step=getStatusPanel', { range })
    .then(resp => {
      dispatch(setArticles(orderBy(unionBy(resp, articles, 'articleID'), 'articleID', 'desc')))
      dispatch(loading(false))
      if (resp.length === 0) {
        dispatch(allArticlesLoaded())
      }
    })
    .catch(error =>
      window.showReportingInfoBox({
        msg: error.message,
        color: 'red'
      })
    )
}

export const unsetArticles = () => dispatch => {
  dispatch(setArticles([]))
}

export const lazyLoad = () => (dispatch, getState) => {
  let articlesLength = getState().articles.articles.length
  dispatch(loadArticles([articlesLength, articlesLength + LAZY_LOAD_ARTICLES_LENGTH]))
}

export const publish = articleID => (dispatch, getState) => {
  const article = getState().articles.articles.find(article => article.articleID === articleID)
  if (article.deleted || !article.actions.publish) {
    return
  }

  dispatch(publishArticle(articleID))

  return fetchUrl('step=publishArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const unpublish = articleID => (dispatch, getState) => {
  const article = getState().articles.articles.find(article => article.articleID === articleID)
  if (!article.actions.unpublish) {
    return
  }

  dispatch(unpublishArticle(articleID))

  return fetchUrl('step=unpublishArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const remove = articleID => (dispatch, getState) => {
  const article = getState().articles.articles.find(article => article.articleID === articleID)
  if (!article.actions.delete) {
    return
  }

  dispatch(removeArticle(articleID))

  return fetchUrl('step=deleteArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const restore = articleID => (dispatch, getState) => {
  const article = getState().articles.articles.find(article => article.articleID === articleID)
  if (!article.actions.restore) {
    return
  }

  dispatch(restoreArticle(articleID))

  return fetchUrl('step=restoreArticle', { articleID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export default handleActions(
  {
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'set articles'(state, action) {
      return {
        ...state,
        articles: action.payload.articles
      }
    },
    'update article state'(state, action) {
      return {
        ...state,
        articles: state.articles.map(article => ({
          ...article,
          status: article.articleID === action.payload.articleID ? action.payload.status : article.status
        }))
      }
    },
    'open article'(state, action) {
      return {
        ...state,
        openedArticle: state.openedArticle === action.payload.articleID ? 0 : action.payload.articleID
      }
    },
    'publish article'(state, action) {
      return {
        ...state,
        articles: state.articles.map(article => ({
          ...article,
          published: article.articleID === action.payload.articleID ? true : article.published
        }))
      }
    },
    'unpublish article'(state, action) {
      return {
        ...state,
        articles: state.articles.map(article => ({
          ...article,
          published: article.articleID === action.payload.articleID ? false : article.published
        }))
      }
    },
    'remove article'(state, action) {
      return {
        ...state,
        articles: state.articles.map(article => ({
          ...article,
          deleted: article.articleID === action.payload.articleID ? true : article.deleted
        }))
      }
    },
    'restore article'(state, action) {
      return {
        ...state,
        articles: state.articles.map(article => ({
          ...article,
          deleted: article.articleID === action.payload.articleID ? false : article.deleted
        }))
      }
    },
    'all articles loaded'(state) {
      return {
        ...state,
        allArticlesLoaded: true
      }
    }
  },
  initialState
)
