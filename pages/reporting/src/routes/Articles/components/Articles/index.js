import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import cn from 'classnames'
import Button from '@torn/shared/components/Button'
import Icon from '../../../../components/Icon'
import s from './styles.cssmodule.scss'
import Preloader from '../../../../../../../shared/components/Preloader/index'

class Articles extends Component {
  componentDidMount() {
    this.props.unsetArticles()
    this.props.loadArticles([0, 30])
    window.addEventListener('scroll', this.lazyLoad)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.lazyLoad)
  }

  lazyLoad = () => {
    const { loading, allArticlesLoaded, lazyLoad } = this.props

    if (!loading && !allArticlesLoaded && window.scrollY + window.outerHeight > this.reportingElement.clientHeight) {
      lazyLoad()
    }
  }

  getStatus = article => {
    if (article.deleted) {
      return 'deleted'
    } else if (article.published) {
      return 'published'
    }

    return article.confirmed ? 'confirmed' : 'unpublished'
  }

  getArticles = () =>
    this.props.articles.map(article => {
      const { articleID, title, author, deleted, published, actions, isArticleNew, isCurrentUserAuthor } = article
      const { openedArticle, openArticle, publish, unpublish, remove, restore } = this.props
      const isOpened = articleID === openedArticle
      const status = this.getStatus(article)

      return (
        <li
          key={articleID}
          className={cn({
            [s.opened]: isOpened,
            [s.published]: published,
            [s.unpublished]: !published,
            active: isOpened,
            [s.deleted]: deleted,
            [s.new]: isArticleNew,
            [s.your]: isCurrentUserAuthor
          })}
        >
          <div className={cn(s.col, s.title)}>
            <Link to={`/articles/${articleID}`} className={s.titleLink}>
              {title}
            </Link>
            <span className={s.arrowAccordion} onClick={() => openArticle(articleID)}>
              <i className="arrow-999" />
            </span>
          </div>
          <div className={cn(s.col, s.status, s[status])}>
            <span className={s.label}>Status:</span>
            {status}
          </div>
          <div className={cn(s.col, s.author)}>
            <span className={s.label}>Author:</span>
            <a href={`/profiles.php?XID=${author.userID}`} className="t-blue h">
              {author.playername}
            </a>
          </div>
          <div className={cn(s.col, s.actions)}>
            <Link
              to={`/articles/edit/${articleID}`}
              className={s.action}
              onClick={e => (article.deleted || !actions.edit) && e.preventDefault()}
            >
              <Icon name="edit" disabled={article.deleted || !actions.edit} />
              <span className={cn(s.actionText, { [s.disabled]: article.deleted || !actions.edit })}>Edit</span>
            </Link>
            {article.published ? (
              <span className={s.action} onClick={() => unpublish(article.articleID)}>
                <i className={cn(s.icon, s.unpublish, { [s.disabled]: !actions.unpublish })} />
                <span className={cn(s.actionText, { [s.disabled]: !actions.unpublish })}>Unpublish</span>
              </span>
            ) : (
              <span className={s.action} onClick={() => publish(article.articleID)}>
                <i className={cn(s.icon, s.publish, { [s.disabled]: deleted || !actions.publish })} />
                <span className={cn(s.actionText, { [s.disabled]: deleted || !actions.publish })}>Publish</span>
              </span>
            )}
            {article.deleted ? (
              <span className={s.action} onClick={() => restore(article.articleID)}>
                <Icon name="restore" disabled={!actions.restore} />
                <span className={cn(s.actionText, { [s.disabled]: !actions.restore })}>Restore</span>
              </span>
            ) : (
              <span className={s.action} onClick={() => remove(article.articleID)}>
                <Icon name="delete" disabled={!actions.delete} />
                <span className={cn(s.actionText, { [s.disabled]: !actions.delete })}>Delete</span>
              </span>
            )}
          </div>
        </li>
      )
    })

  render() {
    const articles = this.getArticles()

    return (
      <div
        className={s.reportingPanel}
        ref={el => {
          this.reportingElement = el
        }}
      >
        <div>
          <div className="new-article-wrap">
            <div className="title-black m-top10 top-round" role="heading" aria-level="5">
              New Article
            </div>
            <div className="cont-gray10  bottom-round msg">
              <p className="m-bottom10">
                Would you like to create a new article? When an article is published you will receive{' '}
                <span className="bold m-right5">250 points</span>
                and will be credited the
                <a className="t-blue h m-right5 m-left5" href="awards.php">
                  'Journalist'
                </a>
                award.
              </p>
              <Link to="/articles/edit/new">
                <Button value="WRITE NEW ARTICLE" />
              </Link>
            </div>
          </div>
        </div>
        <hr className="delimiter-999 m-top10 m-bottom10" />
        <ul className={cn('title-black', s.titles)}>
          <li className={cn(s.title, s.articles)}>Articles</li>
          <li className={cn(s.title, s.status)}>Status</li>
          <li className={cn(s.title, s.author)}>Author</li>
          <li className={cn(s.title, s.action)}>Action</li>
        </ul>
        <ul className={s.list}>{articles}</ul>
        {this.props.loading && <Preloader />}
      </div>
    )
  }
}

Articles.propTypes = {
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      articleID: PropTypes.number,
      title: PropTypes.string,
      status: PropTypes.string,
      author: PropTypes.shape({
        userID: PropTypes.number,
        playername: PropTypes.string
      })
    })
  ),
  openedArticle: PropTypes.number,
  loading: PropTypes.bool,
  allArticlesLoaded: PropTypes.bool,
  loadArticles: PropTypes.func,
  openArticle: PropTypes.func,
  publish: PropTypes.func,
  unpublish: PropTypes.func,
  remove: PropTypes.func,
  restore: PropTypes.func,
  lazyLoad: PropTypes.func,
  unsetArticles: PropTypes.func
}

export default Articles
