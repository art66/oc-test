import React, { Component } from 'react'
import cn from 'classnames'
import Form from '../../../../components/Form'
import Comments from '../../../../components/Comments'
import TabControls from '../../../../components/TabControls'
import s from './styles.cssmodule.scss'
import { fetchUrl } from '../../../../utils'
import ICustomWindow from '../../../../interfaces/ICustomWindow'

interface IProps {
  params: {
    articleID: string
  }
  history: object
  tab: string
  mediaType: string
  notValidTitleLines: boolean
  prevNotValidTitleLines: boolean
  loadFormData: () => void
  setTab: () => void
  checkValidTitleLines: (x: boolean) => void
}

declare let window: ICustomWindow

class NewArticle extends Component<IProps> {
  static contextTypes = {
    router: () => {}
  }

  handleSubmit = (_e, data) => {
    const { articleID } = this.props.params
    const { prevNotValidTitleLines, notValidTitleLines } = this.props
    const article = {
      ...data,
      articleID: articleID && articleID !== 'new' ? articleID : null
    }

    if (!data.title || !data.text || data.title.length < 2 || data.text.length < 2) {
      window.showReportingInfoBox({
        msg: 'You cannot create an empty article',
        color: 'red'
      })

      return
    }

    // check notValidTitleLines when title tooltip isn't showing
    if (notValidTitleLines === null && prevNotValidTitleLines) {
      window.showReportingInfoBox({
        msg: 'You cannot create an article. The title of the article more than 2 lines',
        color: 'red'
      })

      return
    }

    return fetchUrl('step=saveArticle', { ...article })
      .then(resp => {
        this.context.router.push('/')
        window.showReportingInfoBox({
          msg: 'Article was saved',
          color: 'green'
        })

        return resp
      })
      .catch(error =>
        window.showReportingInfoBox({
          msg: error.message,
          color: 'red'
        })
      )
  }

  renderTabControls = (tab, setTab) => {
    const tabs = [
      { name: 'ARTICLE', active: tab === 'ARTICLE', onClick: setTab },
      { name: 'DISCUSSION', active: tab === 'DISCUSSION', onClick: setTab }
    ]

    return <TabControls tabs={tabs} />
  }

  renderForm = articleID => {
    const { checkValidTitleLines, notValidTitleLines } = this.props

    return (
      <Form
        articleID={articleID}
        onSubmit={(e, data) => this.handleSubmit(e, data)}
        smallControls={articleID !== 'new'}
        checkValidTitleLines={checkValidTitleLines}
        notValidTitleLines={notValidTitleLines}
      />
    )
  }

  renderComments = articleID => {
    return !articleID || articleID === 'new' ? '' : <Comments articleID={articleID} />
  }

  render() {
    const { tab, mediaType, setTab } = this.props
    const { articleID } = this.props.params
    const isDesktopMode = !document.body.classList.contains('r')

    return (
      <div className={cn(s.newArticle, { [s.withComments]: articleID !== 'new' })}>
        {(!articleID || articleID !== 'new') && this.renderTabControls(tab, setTab)}
        <div className={s.tabs}>
          {tab !== 'ARTICLE' && mediaType !== 'desktop' && !isDesktopMode ? '' : this.renderForm(articleID)}
          {(tab !== 'DISCUSSION' && mediaType !== 'desktop' && !isDesktopMode) || this.renderComments(articleID)}
        </div>
      </div>
    )
  }
}

export default NewArticle
