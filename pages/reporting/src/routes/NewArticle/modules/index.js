import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'

const initialState = {
  form: {
    title: '',
    subtitle: '',
    text: ''
  },
  user: {
    userID: 1,
    playername: 'Chedburn'
  },
  tab: 'ARTICLE',
  notValidTitleLines: false,
  prevNotValidTitleLines: false
}

export const loading = createAction('loading', (loading = true) => ({ loading }))
export const setFormField = createAction('set form field', (field, value) => ({ field, value }))
export const setTab = createAction('set tab', tabName => ({ tabName }))
export const checkValidTitleLines = createAction('check valid title lines', valid => ({ valid }))

export default handleActions(
  {
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'set tab'(state, action) {
      return {
        ...state,
        tab: action.payload.tabName
      }
    },
    'check valid title lines'(state, action) {
      return {
        ...state,
        prevNotValidTitleLines: state.notValidTitleLines,
        notValidTitleLines: action.payload.valid
      }
    }
  },
  initialState
)
