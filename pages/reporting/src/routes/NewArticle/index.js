import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'articles/edit/:articleID',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const NewArticle = require('./containers/NewArticleContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'newArticle', reducer })
        cb(null, NewArticle)
      },
      'newArticle'
    )
  }
})
