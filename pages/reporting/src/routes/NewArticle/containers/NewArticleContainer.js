import { connect } from 'react-redux'
import { setTab, checkValidTitleLines } from '../modules'
import NewArticle from '../components/NewArticle'

const mapDispatchToProps = {
  setTab,
  checkValidTitleLines
}

const mapStateToProps = state => ({
  form: state.newArticle.form,
  comments: state.newArticle.comments,
  tab: state.newArticle.tab,
  mediaType: state.browser.mediaType,
  notValidTitleLines: state.newArticle.notValidTitleLines,
  prevNotValidTitleLines: state.newArticle.prevNotValidTitleLines
})

export default connect(mapStateToProps, mapDispatchToProps)(NewArticle)
