import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'newsletter',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Newsletter = require('./containers/NewsletterContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'newsletter', reducer })
        cb(null, Newsletter)
      },
      'newsletter'
    )
  }
})
