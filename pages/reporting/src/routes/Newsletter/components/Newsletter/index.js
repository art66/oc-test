import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Button from '@torn/shared/components/Button'
import { fetchUrl } from '../../../../utils'
import s from './styles.cssmodule.scss'
/* global $ */

class Newsletter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      subject: {
        maxLength: 35,
        value: 'Reporting Newsletter'
      },
      text: {
        value: ''
      }
    }
  }

  componentDidMount() {
    this.initBBCodeEditor()
  }

  initBBCodeEditor = (data = {}) => {
    $('#bbc-editor-wrapper').editorPlugin({
      action: 'setContent',
      content: data.text || '',
      editorSubmitHandler: e => {
        let data = {
          subject: `[t-span color=t-green-d]${this.state.subject.value}[/t-span]`,
          text: this.textArea.value
        }

        if (
          !this.state.subject.value ||
          !this.textArea.value ||
          this.state.subject.value.length < 2 ||
          this.textArea.value.length < 2
        ) {
          window.showReportingInfoBox({
            msg: 'Please fill all the fields',
            color: 'red'
          })

          return
        }

        fetchUrl('step=massMailReporters', { ...data })
          .then(() => {
            window.showReportingInfoBox &&
              window.showReportingInfoBox({
                msg: 'Newsletter was sent',
                color: 'green'
              })
            this.setFormField('subject', '')
            this.setFormField('text', '')
            this.textArea.value = ''
            this.textInput.innerHTML = ''
          })
          .catch(errors => alert(errors))
      }
    })
    this.textArea = document.getElementById('editor-textarea')
    this.textInput = document.getElementById('main-input')
  }

  setFormField = (fieldName, value = '') => {
    if (this.state[fieldName] && this.state[fieldName].maxLength && this.state[fieldName].maxLength < value.length) {
      return
    }

    this.setState({
      [fieldName]: {
        ...this.state[fieldName],
        value: value
      }
    })
  }

  getCharsLeft = fieldName => this.state[fieldName].maxLength - this.state[fieldName].value.length

  render() {
    return (
      <div className={s.form}>
        <div className="title-black top-round">Reporting newsletter</div>
        <div className="cont-gray bottom-round">
          <form id="editor-form">
            <div className={s.group}>
              <label className={s.label}>Subject:</label>
              <span className={s.charsLeft}>({this.getCharsLeft('subject')} characters left)</span>
              <input
                className={cn(s.input, s.title)}
                value={this.state.subject.value}
                onChange={e => this.setFormField('subject', e.target.value)}
              />
            </div>
            <span className={s.divider} />
            <label className={cn(s.label, s.articleLabel)}>Text:</label>
            <div id="bbc-editor-wrapper" />
          </form>
        </div>
      </div>
    )
  }
}

Newsletter.propTypes = {
  articleID: PropTypes.string,
  description: PropTypes.string,
  onSubmit: PropTypes.func.isRequired
}

export default Newsletter
