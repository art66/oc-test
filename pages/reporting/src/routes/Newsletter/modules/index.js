import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'

const initialState = {
  form: {
    title: '',
    subtitle: '',
    text: ''
  },
  user: {
    userID: 1,
    playername: 'Chedburn'
  },
  tab: 'form'
}

export const loading = createAction('loading', (loading = true) => ({ loading }))
export const setFormField = createAction('set form field', (field, value) => ({ field, value }))
export const setTab = createAction('set tab', tabName => ({ tabName }))

export const loadPreview = articleID => (dispatch, getState) => {
  return fetchUrl('step=getPreviewPanel', { articleID: 342 }).then(data => {
    // dispatch(setPreviewState(data))
  })
}

export default handleActions(
  {
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'set form field'(state, action) {
      return {
        ...state,
        form: {
          ...state.form,
          [action.payload.field]: action.payload.value
        }
      }
    }
  },
  initialState
)
