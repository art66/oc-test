import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Icon from '../../../../components/Icon'
import UserHonor from '../../../../components/UserHonor'
import s from './styles.cssmodule.scss'

class Submissions extends Component {
  componentDidMount() {
    const { loadSubmissions } = this.props

    loadSubmissions()
  }

  getSubmissions = () => {
    const { submissions, loading } = this.props

    if (!submissions.length && !loading) {
      return (
        <div className='cont-gray bottom-round p10' style={{ textAlign: 'center' }}>
          There are no any public submissions
        </div>
      )
    }

    const submissionsList = submissions.map(submission => {
      const { articleID, title, isArticleNew, deleted, author } = submission
      const { toggleSubmission } = this.props

      return (
        <li key={articleID} className={cn(s.item, { [s.new]: isArticleNew }, { [s.deleted]: deleted })}>
          <div className={s.row}>
            <div className={cn(s.author, !author?.awardImageLink && s.withoutImage)}>
              <UserHonor
                userID={author.userID}
                playername={author.playername}
                awardImage={author.awardImageLink}
                onlineStatus={author.onlineStatus}
              />
            </div>
            <div className={s.title} onClick={() => toggleSubmission(articleID)}>
              {title}
            </div>
            {this.renderActions(submission)}
          </div>
          {this.renderSubmissionContent(submission)}
        </li>
      )
    })

    return <ul className={s.list}>{submissionsList}</ul>
  }

  renderActions = submission => {
    const { articleID, deleted } = submission
    const { restore, remove } = this.props

    return deleted ? (
      <div className={cn(s.action, { [s.disabled]: !submission.actions.restore })} onClick={() => restore(articleID)}>
        <Icon name='restore' disabled={!submission.actions.restore} />
      </div>
    ) : (
      <div className={cn(s.action, { [s.disabled]: !submission.actions.delete })} onClick={() => remove(articleID)}>
        <Icon name='delete' disabled={!submission.actions.delete} />
      </div>
    )
  }

  renderSubmissionContent = submission => {
    const { openedSubmission } = this.props
    const { articleID, title, text, author } = submission

    return (
      openedSubmission === articleID && (
        <div className={s.text}>
          <span className={s.mobileInfo}>
            <div className={s.mobileTitle}>{title}</div>
            Written by:
            <a href={`/profiles.php?XID=${author.userID}`} className='t-blue h bold authorLink'>
              {author.playername}
            </a>
          </span>
          <div dangerouslySetInnerHTML={{ __html: text }} />
        </div>
      )
    )
  }

  render() {
    const submissions = this.getSubmissions()

    return (
      <div className={s.submissions}>
        <div className='title-black top-round'>Public Submissions</div>
        <div className={s.titles}>
          <span className={s.name}>Name</span>
          <span className={s.article}>Article</span>
        </div>
        {submissions}
      </div>
    )
  }
}

Submissions.propTypes = {
  submissions: PropTypes.arrayOf(
    PropTypes.shape({
      articleID: PropTypes.number,
      title: PropTypes.string,
      text: PropTypes.string,
      status: PropTypes.string,
      author: PropTypes.shape({
        userID: PropTypes.number,
        playername: PropTypes.string
      })
    })
  ),
  openedSubmission: PropTypes.number,
  loading: PropTypes.bool,
  loadSubmissions: PropTypes.func,
  toggleSubmission: PropTypes.func,
  remove: PropTypes.func,
  restore: PropTypes.func
}

export default Submissions
