import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'

const initialState = {
  submissions: [],
  openedSubmission: 0,
  loading: false
}

export const setSubmissions = createAction('set submissions', submissions => ({ submissions }))
export const loading = createAction('loading', (isLoading = true) => ({ loading: isLoading }))
export const updateArticleStatus = createAction('update article state', (articleID, status) => ({ articleID, status }))
export const toggleSubmission = createAction('toggle submission', (articleID = 0) => ({ articleID }))
export const removeSubmission = createAction('remove submission', articleID => ({ articleID }))
export const restoreSubmission = createAction('restore submission', articleID => ({ articleID }))

export const loadSubmissions = (range = [0, 300]) => dispatch => {
  dispatch(loading(true))
  fetchUrl('step=getSubmissionsPanel', { range })
    .then(submissions => {
      dispatch(setSubmissions(submissions))
      dispatch(loading(false))
    })
    .catch(error => alert(error))
}

export const remove = articleID => (dispatch, getState) => {
  const submission = getState().submissions.submissions.find(s => s.articleID === articleID)

  if (submission && !submission.actions.delete) {
    return
  }

  dispatch(removeSubmission(articleID))
  fetchUrl('step=deleteSubmission', { articleID }).catch(error => alert(error))
}

export const restore = articleID => (dispatch, getState) => {
  const submission = getState().submissions.submissions.find(s => s.articleID === articleID)

  if (submission && !submission.actions.restore) {
    return
  }

  dispatch(restoreSubmission(articleID))
  fetchUrl('step=restoreSubmission', { articleID }).catch(error => alert(error))
}

export default handleActions(
  {
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'set submissions'(state, action) {
      return {
        ...state,
        submissions: action.payload.submissions
      }
    },
    'toggle submission'(state, action) {
      return {
        ...state,
        openedSubmission: state.openedSubmission === action.payload.articleID ? 0 : action.payload.articleID
      }
    },
    'remove submission'(state, action) {
      return {
        ...state,
        submissions: state.submissions.map(submission => ({
          ...submission,
          deleted: submission.articleID === action.payload.articleID ? true : submission.deleted
        }))
      }
    },
    'restore submission'(state, action) {
      return {
        ...state,
        submissions: state.submissions.map(submission => ({
          ...submission,
          deleted: submission.articleID === action.payload.articleID ? false : submission.deleted
        }))
      }
    }
  },
  initialState
)
