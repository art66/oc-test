import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'submissions',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Submissions = require('./containers/SubmissionsContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'submissions', reducer })
        cb(null, Submissions)
      },
      'submissions'
    )
  }
})
