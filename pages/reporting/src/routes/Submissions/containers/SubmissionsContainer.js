import { connect } from 'react-redux'
import { loadSubmissions, toggleSubmission, remove, restore } from '../modules'
import Submissions from '../components/Submissions'

const mapDispatchToProps = {
  loadSubmissions,
  toggleSubmission,
  remove,
  restore
}

const mapStateToProps = state => ({
  submissions: state.submissions.submissions,
  openedSubmission: state.submissions.openedSubmission,
  loading: state.submissions.loading
})

export default connect(mapStateToProps, mapDispatchToProps)(Submissions)
