import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'reporters',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Reporters = require('./containers/ReportersContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'reporters', reducer })
        cb(null, Reporters)
      },
      'reporters'
    )
  }
})
