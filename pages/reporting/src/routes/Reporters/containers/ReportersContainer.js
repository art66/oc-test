import { connect } from 'react-redux'
import { add, remove, restore, loadReporters, setAutocompleteValue } from '../modules'
import Reporters from '../components/Reporters'

const mapDispatchToProps = {
  add,
  remove,
  restore,
  loadReporters,
  setAutocompleteValue
}

const mapStateToProps = state => ({
  reporters: state.reporters.reporters,
  user: state.reporters.user,
  autocompleteValue: state.reporters.autocompleteValue
})

export default connect(mapStateToProps, mapDispatchToProps)(Reporters)
