import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'

const initialState = {
  reporters: [],
  user: {},
  loading: false,
  autocompleteValue: ''
}

export const setReporters = createAction('set reporters', reporters => ({ reporters }))
export const setUser = createAction('set user', user => ({ user }))
export const loading = createAction('loading', (loading = true) => ({ loading }))
export const addReporter = createAction('add reporter', reporter => ({ reporter }))
export const removeReporter = createAction('remove reporter', userID => ({ userID }))
export const restoreReporter = createAction('restore reporter', userID => ({ userID }))
export const setAutocompleteValue = createAction('set autocomplete value', value => ({ value }))

export const loadReporters = () => dispatch => {
  fetchUrl('step=getReportersPanel', { range: [0, 300] })
    .then(resp => {
      dispatch(setReporters(resp.reporters))
      dispatch(setUser(resp.user))
    })
    .catch(error =>
      window.showReportingInfoBox({
        msg: error.message,
        color: 'red'
      })
    )
}

export const add = userID => dispatch => {
  fetchUrl('step=addReporter', { userID })
    .then(reporter => {
      dispatch(addReporter(reporter))
      dispatch(setAutocompleteValue(''))
      window.showReportingInfoBox({
        msg: 'Reporter was added',
        color: 'green'
      })
    })
    .catch(error =>
      window.showReportingInfoBox({
        msg: error.message,
        color: 'red'
      })
    )
}

export const remove = userID => dispatch => {
  dispatch(removeReporter(userID))
  fetchUrl('step=removeReporter', { userID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export const restore = userID => dispatch => {
  dispatch(restoreReporter(userID))
  fetchUrl('step=restoreReporter', { userID }).catch(error =>
    window.showReportingInfoBox({
      msg: error.message,
      color: 'red'
    })
  )
}

export default handleActions(
  {
    'set reporters'(state, action) {
      return {
        ...state,
        reporters: action.payload.reporters
      }
    },
    'set user'(state, action) {
      return {
        ...state,
        user: action.payload.user
        // user: { hasPrivileges: false }
      }
    },
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'add reporter'(state, action) {
      return {
        ...state,
        reporters: state.reporters.concat([action.payload.reporter])
      }
    },
    'remove reporter'(state, action) {
      return {
        ...state,
        reporters: state.reporters.map(reporter => ({
          ...reporter,
          removed: reporter.userID === action.payload.userID ? true : reporter.removed
        }))
      }
    },
    'restore reporter'(state, action) {
      return {
        ...state,
        reporters: state.reporters.map(reporter => ({
          ...reporter,
          removed: reporter.userID === action.payload.userID ? false : reporter.removed
        }))
      }
    },
    'set autocomplete value'(state, action) {
      return {
        ...state,
        autocompleteValue: action.payload.value
      }
    }
  },
  initialState
)
