import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Autocomplete from '@torn/shared/components/Autocomplete'
import Button from '@torn/shared/components/Button'
import s from './styles.cssmodule.scss'
/* global $ */
/* global addRFC */

class AddReporter extends Component {
  constructor(props) {
    super(props)
    this.state = { fetchingAutocomplete: false }
  }

  loadAutocompleteData = query => {
    let action = window.location.protocol + '//' + window.location.hostname + '/' + 'autocompleteUserAjaxAction.php'
    this.setState({
      fetchingAutocomplete: true
    })
    $.ajax({
      url: addRFC(action),
      dataType: 'json',
      data: { q: query, option: 'ac-all' },
      success: data => {
        if (!data) {
          return
        }

        this.setState({
          autocompleteUsers: data,
          fetchingAutocomplete: false
        })

        if (query !== this.props.autocompleteValue) {
          this.loadAutocompleteData(this.props.autocompleteValue)
        }
      }
    })
  }

  handleAutocompleteInputChange = e => {
    this.props.setAutocompleteValue(e.target.value)

    if (!this.state.fetchingAutocomplete) {
      this.loadAutocompleteData(e.target.value)
    }

    // this.props.searchForAccount(e.target.value)
  }

  handleAutocompleteSelect = selected => {
    this.props.setAutocompleteValue(`${selected.name} [${selected.id}]`)
  }

  handleSubmit = e => {
    e.preventDefault()
    const { autocompleteValue, add } = this.props
    let userID = autocompleteValue.replace(/.*\[|\]/gi, '')
    if (!parseInt(userID)) {
      window.showReportingInfoBox({
        msg: 'Select user from the list',
        color: 'red'
      })

      return
    }

    add(userID)
  }

  render() {
    return (
      <div className={s.addReporter}>
        <div className="title-black top-round">Add reporters</div>
        <div className="cont-gray bottom-round p10">
          <form onSubmit={e => this.handleSubmit(e)}>
            <label className={s.label}>User's ID:</label>
            <Autocomplete
              name="searchAccount"
              value={this.props.autocompleteValue}
              inputClassName={s.input}
              autoFocus="true"
              list={this.state.autocompleteUsers || []}
              targets="users"
              onInput={this.handleAutocompleteInputChange}
              onSelect={this.handleAutocompleteSelect}
            />
            <Button>ADD</Button>
          </form>
        </div>
      </div>
    )
  }
}

AddReporter.propTypes = {
  autocompleteValue: PropTypes.string,
  setAutocompleteValue: PropTypes.func,
  add: PropTypes.func
}

export default AddReporter
