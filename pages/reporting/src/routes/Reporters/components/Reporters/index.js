import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import UserHonor from '../../../../components/UserHonor'
import AddReporter from './AddReporter'
import InfoBox from '@torn/shared/components/InfoBox'
import s from './styles.cssmodule.scss'

class Reporters extends Component {
  componentDidMount() {
    this.props.loadReporters()
  }

  handleRemoveClick = reporter => {
    if (reporter.removed) {
      this.props.restore(reporter.userID)
    } else {
      this.props.remove(reporter.userID)
    }
  }

  getReportersList = () => {
    const list = this.props.reporters.map(reporter => {
      const { userID, playername, onlineStatus, awardImageLink, status, lastPost, articlesAmount, removed } = reporter

      return (
        <li key={userID} className={cn(s.reporter, { [s.removed]: removed })}>
          <div className={cn(s.col, s.name, !awardImageLink?.length && s.withoutImage)}>
            <UserHonor
              userID={userID}
              playername={playername}
              awardImage={awardImageLink}
              onlineStatus={onlineStatus}
              smallOnTablet
            />
          </div>
          <div className={cn(s.col, s.status)}>{status}</div>
          <div className={cn(s.col, s.lastPost)}>{lastPost}</div>
          <div className={cn(s.col, s.articles)}>{articlesAmount}</div>
          {this.props.user.hasPrivileges && (
            <div className={cn(s.col, s.action, 't-red h')} onClick={() => this.handleRemoveClick(reporter)}>
              {reporter.removed ? 'undo' : 'remove'}
            </div>
          )}
        </li>
      )
    })

    return <ul className={s.list}>{list}</ul>
  }

  render() {
    const reporters = this.getReportersList()
    const { user, add, autocompleteValue, setAutocompleteValue } = this.props

    return (
      <div className={cn({ [s.noPrivileges]: !user.hasPrivileges, [s.hasPrivileges]: user.hasPrivileges })}>
        <div>
          <InfoBox
            msg={`Below are the reporters and editors of TC Times. Currently there are
              ${this.props.reporters.length} members.`}
            color="grey"
          />
        </div>
        {user.hasPrivileges && (
          <AddReporter autocompleteValue={autocompleteValue} setAutocompleteValue={setAutocompleteValue} add={add} />
        )}
        <div className={s.reporters}>
          <ul className={cn(s.titles, 'title-black top-round')}>
            <li className={cn(s.title, s.name)}>Name</li>
            <li className={cn(s.title, s.status)}>Status</li>
            <li className={cn(s.title, s.lastPost)}>Last post</li>
            <li className={cn(s.title, s.articles)}>Articles</li>
            {user.hasPrivileges && <li className={cn(s.title, s.action)}>Action</li>}
          </ul>
          {reporters}
        </div>
      </div>
    )
  }
}

Reporters.propTypes = {
  reporters: PropTypes.array,
  user: PropTypes.shape({
    hasPrivileges: PropTypes.bool
  }),
  autocompleteValue: PropTypes.string,
  setAutocompleteValue: PropTypes.func,
  loadReporters: PropTypes.func,
  add: PropTypes.func,
  restore: PropTypes.func,
  remove: PropTypes.func
}

export default Reporters
