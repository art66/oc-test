import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'

const initialState = {
  form: {
    headline: '',
    headlineMaxLength: 140,
    url: '',
    duration: '24',
    countdownDate: {
      name: 'Disabled',
      timestamp: null
    },
    countdownHours: '',
    countdownMins: '',
    isGlobal: false
  },
  headlines: [],
  openedHeadline: 0,
  loading: false,
  insertedHeadlines: 0,
  updatedHeadlines: 0,
  generateStatus: false,
  startGenerationAccess: false
}

export const setHeadlines = createAction('set headlines', headlines => ({ headlines }))
export const loading = createAction('loading', (loading = true) => ({ loading }))
export const updateArticleStatus = createAction('update article state', (articleID, status) => ({ articleID, status }))
export const toggleHeadline = createAction('toggle headline', (ID = 0) => ({ ID }))
export const removeHeadline = createAction('remove headline', ID => ({ ID }))
export const restoreHeadline = createAction('restore headline', ID => ({ ID }))
export const updateFormField = createAction('update form field', (field, value) => ({ field, value }))
export const resetForm = createAction('reset form')
export const decrementCountdowns = createAction('decrement countdowns')
export const startAutomaticGeneration = createAction(
  'start generation',
  ({ insertedHeadlines, updatedHeadlines }) => ({ insertedHeadlines, updatedHeadlines })
)
export const checkAccessStartGeneration = createAction(
  'check access generation',
  ({ headlineGenerationAccess, maxHeadlineLength }) => ({ headlineGenerationAccess, maxHeadlineLength })
)

export const loadHeadlines = (range = [0, 300]) => dispatch => {
  dispatch(loading(true))
  fetchUrl('/page.php?sid=newsTickersAdmin&step=getHeadlines', { range })
    .then(resp => {
      dispatch(setHeadlines(resp.headlines))
      dispatch(loading(false))
    })
    .catch(error => {
      dispatch(loading(false))
      window.showReportingInfoBox({ msg: `${error}`, color: 'red' })
    })
}

export const saveHeadline = () => (dispatch, getState) => {
  let form = getState().headlines.form
  let date = new Date(form.countdownDate.timestamp)
  date.setUTCHours(form.countdownHours || 0)
  date.setUTCMinutes(form.countdownMins || 0)
  let headline = {
    headline: form.headline,
    url: form.url,
    duration: form.duration,
    countdown: form.countdownDate.timestamp ? parseInt(date.getTime() / 1000) : null,
    isGlobal: form.isGlobal
  }
  dispatch(loading(true))

  return fetchUrl('/page.php?sid=newsTickersAdmin&step=saveHeadline', headline)
    .then(resp => {
      dispatch(setHeadlines(resp.headlines))
      dispatch(resetForm())
      dispatch(loading(false))
      window.showReportingInfoBox({ msg: 'Headline was saved', color: 'green' })
    })
    .catch(error => {
      dispatch(loading(false))
      window.showReportingInfoBox({ msg: `${error}`, color: 'red' })
    })
}

export const remove = ID => dispatch => {
  dispatch(removeHeadline(ID))
  fetchUrl('/page.php?sid=newsTickersAdmin&step=deleteHeadline', { ID })
    .catch(error => window.showReportingInfoBox({ msg: `${error}`, color: 'red' }))
}

export const restore = ID => dispatch => {
  dispatch(restoreHeadline(ID))
  fetchUrl('/page.php?sid=newsTickersAdmin&step=restoreHeadline', { ID })
    .catch(error => window.showReportingInfoBox({ msg: `${error}`, color: 'red' }))
}

export const startHeadlines = () => dispatch => {
  fetchUrl('/page.php?sid=newsTickersAdmin&step=runHeadlineGenerators')
    .then(data => {
      dispatch(startAutomaticGeneration(data))
      dispatch(setHeadlines(data.headlines))
    })
    .catch(error => window.showReportingInfoBox({ msg: `${error}`, color: 'red' }))
}

export const checkAccessStartHeadlines = () => dispatch => {
  fetchUrl('/page.php?sid=newsTickersAdmin&step=checkAccessForHeadlineManagement')
    .then(data => dispatch(checkAccessStartGeneration(data)))
    .catch(error => window.showReportingInfoBox({ msg: `${error}`, color: 'red' }))
}

export default handleActions(
  {
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'set headlines'(state, action) {
      return {
        ...state,
        headlines: action.payload.headlines
      }
    },
    'toggle headline'(state, action) {
      return {
        ...state,
        openedHeadline: state.openedHeadline === action.payload.ID ? 0 : action.payload.ID
      }
    },
    'remove headline'(state, action) {
      return {
        ...state,
        headlines: state.headlines.map(headline => ({
          ...headline,
          deleted: headline.ID === action.payload.ID ? true : headline.deleted
        })),
        generateStatus: false
      }
    },
    'restore headline'(state, action) {
      return {
        ...state,
        headlines: state.headlines.map(headline => ({
          ...headline,
          deleted: headline.ID === action.payload.ID ? false : headline.deleted
        })),
        generateStatus: false
      }
    },
    'update form field'(state, action) {
      return {
        ...state,
        form: {
          ...state.form,
          [action.payload.field]: action.payload.value
        }
      }
    },
    'reset form'(state) {
      return {
        ...state,
        form: {
          ...initialState.form
        },
        generateStatus: false
      }
    },
    'decrement countdowns'(state) {
      return {
        ...state,
        headlines: state.headlines.map(headline => ({
          ...headline,
          countdown: headline.countdown && headline.countdown > 0 ? headline.countdown - 1 : headline.countdown
        }))
      }
    },
    'start generation'(state, action) {
      return {
        ...state,
        insertedHeadlines: action.payload.insertedHeadlines,
        updatedHeadlines: action.payload.updatedHeadlines,
        generateStatus: true
      }
    },
    'check access generation'(state, action) {
      return {
        ...state,
        startGenerationAccess: action.payload.headlineGenerationAccess,
        form: {
          ...state.form,
          headlineMaxLength: action.payload.maxHeadlineLength
        }
      }
    }
  },
  initialState
)
