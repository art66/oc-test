import { connect } from 'react-redux'
import {
  loadHeadlines,
  toggleHeadline,
  remove,
  restore,
  updateFormField,
  saveHeadline,
  decrementCountdowns,
  startHeadlines,
  checkAccessStartHeadlines
} from '../modules'
import Headlines from '../components/Headlines'

const mapDispatchToProps = {
  loadHeadlines,
  toggleHeadline,
  remove,
  restore,
  updateFormField,
  saveHeadline,
  decrementCountdowns,
  startHeadlines,
  checkAccessStartHeadlines
}

const mapStateToProps = state => ({
  form: state.headlines.form,
  headlines: state.headlines.headlines,
  openedHeadline: state.headlines.openedHeadline,
  loading: state.headlines.loading,
  startGenerationAccess: state.headlines.startGenerationAccess
})

export default connect(mapStateToProps, mapDispatchToProps)(Headlines)
