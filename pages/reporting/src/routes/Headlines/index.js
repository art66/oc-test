import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'headlines',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Headlines = require('./containers/HeadlinesContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'headlines', reducer })
        cb(null, Headlines)
      },
      'headlines'
    )
  }
})
