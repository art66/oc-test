import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Dropdown from '@torn/shared/components/Dropdown'
import Button from '@torn/shared/components/Button'
import s from './styles.cssmodule.scss'
/* global getCurrentTimestamp */

const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

class Form extends Component {
  getDropdownList = () => {
    let date = new Date(getCurrentTimestamp())
    const list = [{ name: 'Disabled', timestamp: null }, { name: 'Today', timestamp: date.getTime() }]

    for (let i = 1; i <= 20; i++) {
      date.setUTCDate(date.getUTCDate() + 1)
      list.push({
        name: date.getUTCDate() + ' ' + monthNames[date.getUTCMonth()],
        timestamp: date.getTime()
      })
    }

    return list
  }

  getHeadlineCharsLeft = () => {
    const { headline, headlineMaxLength } = this.props.form

    return headlineMaxLength - headline.length
  }

  handleCountdownHoursInputChange = e => {
    let stringVal = e.target.value
    const { updateFormField } = this.props

    stringVal = parseInt(stringVal) > 23 ? '23' : stringVal
    if (!e.target.value.length || (parseInt(stringVal) >= 0 && e.target.value.length <= 2)) {
      updateFormField('countdownHours', stringVal || e.target.value)
    }
  }

  handleCountdownMinsInputChange = e => {
    let stringVal = e.target.value
    const { updateFormField } = this.props

    stringVal = parseInt(stringVal) > 59 ? '59' : stringVal
    if (!e.target.value.length || (parseInt(stringVal) >= 0 && e.target.value.length <= 2)) {
      updateFormField('countdownMins', stringVal || e.target.value)
    }
  }

  handleDurationInputChange = e => {
    let numberValue = parseInt(e.target.value)
    const { updateFormField } = this.props

    numberValue = numberValue > 175200 ? 175200 : numberValue
    if (!e.target.value.length || numberValue > 0) {
      updateFormField('duration', numberValue || e.target.value)
    }
  }

  render() {
    const { form, updateFormField, onSubmit, loading } = this.props
    const { headline, url, duration, countdownDate, countdownHours, countdownMins, isGlobal } = form
    const dropdownList = this.getDropdownList()

    return (
      <div className={s.createHeadline}>
        <form
          onSubmit={e => {
            e.preventDefault()
            onSubmit()
          }}
        >
          <div className='title-black top-round'>Create Headline</div>
          <div className='cont-gray bottom-round'>
            <div className={s.group}>
              <label className={s.label} htmlFor='headlineInput'>Headline</label>
              <span className={s.tip}>({this.getHeadlineCharsLeft()} characters left)</span>
              <input
                id='headlineInput'
                className={cn(s.input, s.headline)}
                maxLength={form.headlineMaxLength}
                onChange={e => {
                  if (this.getHeadlineCharsLeft() > 0 || e.target.value.length < headline.length) {
                    updateFormField('headline', e.target.value)
                  }
                }}
                value={headline}
              />
            </div>
            <div className={s.group}>
              <label className={s.label} htmlFor='urlInput'>URL</label>
              <span className={s.tip}>(insert URL)</span>
              <input
                id='urlInput'
                className={cn(s.input, s.url)}
                onChange={e => updateFormField('url', e.target.value)}
                value={url}
              />
            </div>
            <div className={s.bottomGroup}>
              <div className={s.part}>
                <label className={cn(s.label, s.duration)} htmlFor='durationInput'>Duration* (h)</label>
                <input
                  id='durationInput'
                  className={cn(s.input, s.duration)}
                  onChange={e => this.handleDurationInputChange(e)}
                  value={duration}
                  disabled={countdownDate.name !== 'Disabled'}
                />
              </div>
              <div className={s.part}>
                <label className={cn(s.label, s.countdown)}>Countdown:</label>
                <Dropdown
                  list={dropdownList}
                  onChange={value => updateFormField('countdownDate', value)}
                  selected={countdownDate}
                />
              </div>
              <input
                className={cn(s.input, s.countdown)}
                placeholder='00'
                onChange={e => this.handleCountdownHoursInputChange(e)}
                value={countdownHours}
                disabled={countdownDate.name === 'Disabled'}
              />
              <span className={s.colon}>:</span>
              <input
                className={cn(s.input, s.countdown)}
                placeholder='00'
                onChange={e => this.handleCountdownMinsInputChange(e)}
                value={countdownMins}
                disabled={countdownDate.name === 'Disabled'}
              />
              <div className={cn('choice-container', s.checkbox, s.global)}>
                <input
                  className='checkbox-css'
                  type='checkbox'
                  id='headlineGlobal'
                  name='headlineGlobal'
                  onChange={e => updateFormField('isGlobal', e.target.checked)}
                  checked={isGlobal}
                />
                <label htmlFor='headlineGlobal' className='marker-css'>
                  Urgent announcement
                </label>
              </div>
              <div className={s.submitBlock}>
                <Button
                  className={s.submit}
                  loading={loading}
                  disabled={!headline.trim().length}
                >
                  CREATE
                </Button>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

Form.propTypes = {
  form: PropTypes.shape({
    headline: PropTypes.string,
    headlineMaxLength: PropTypes.number,
    url: PropTypes.string,
    duration: PropTypes.string,
    countdownDate: PropTypes.object,
    countdownHours: PropTypes.string,
    countdownMins: PropTypes.string,
    isGlobal: PropTypes.bool
  }),
  loading: PropTypes.bool,
  updateFormField: PropTypes.func,
  onSubmit: PropTypes.func
}

export default Form
