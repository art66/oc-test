import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { formatTimeWithDays } from '@torn/shared/utils/formatTimeWithDays'
import s from './styles.cssmodule.scss'

class HeadlinesList extends Component {
  componentDidMount() {
    this.startCountdowns()
  }

  componentWillUnmount() {
    this.clearCountdowns()
  }

  getHeadlineText = headline => {
    let countdownText = ''

    if (typeof headline.countdown === 'number') {
      countdownText = `[${formatTimeWithDays(headline.countdown)}]`
    }

    return <span
      className={cn(s.headlineText, { bold: headline.isGlobal })}
      dangerouslySetInnerHTML={{ __html: `${headline.headline} ${countdownText}` }}
    />
  }

  getHeadlines = () =>
    this.props.headlines.map(headline => (
      <li key={headline.ID} className={s.row}>
        <div className={s.headline} onClick={() => this.props.toggleHeadline(headline.ID)}>
          <div className={cn(s.col, s.text, 't-overflow')}>{this.getHeadlineText(headline)}</div>
          <div className={cn(s.col, s.time)}>{headline.time}</div>
        </div>
        {this.props.openedHeadline === headline.ID && (
          <div className={s.dropdown}>
            {this.getHeadlineText(headline)}
            <div className={s.info}>
              {
                headline.author ?
                  <div className={s.author}>
                    <span className='bold'>Created by: </span>
                    <a className='t-blue h c-pointer' href={`/profiles.php?XID=${headline.author.userID}`}>
                      {headline.author.playername}
                    </a>
                  </div> :
                  <div className={s.author}>
                    <span className='bold'>Created automatically</span>
                  </div>
              }
              {headline.deleted ? (
                <a
                  href='#'
                  onClick={e => this.handleRestoreClick(e, headline.ID)}
                  className={cn(s.delete, 't-blue h c-pointer')}
                >
                  Restore
                </a>
              ) : (
                <a
                  href='#'
                  onClick={e => this.handleDeleteClick(e, headline.ID)}
                  className={cn(s.delete, 't-blue h c-pointer')}
                >
                  Delete
                </a>
              )}
            </div>
          </div>
        )}
      </li>
    ))

  getInformMessage = () => {
    return (
      <p className={s.informMsg}>There is no active headlines for now.</p>
    )
  }

  handleDeleteClick = (event, ID) => {
    event.preventDefault()
    this.props.remove(ID)
  }

  handleRestoreClick = (event, ID) => {
    event.preventDefault()
    this.props.restore(ID)
  }

  clearCountdowns = () => {
    clearInterval(this.countdownInterval)
  }

  startCountdowns = () => {
    this.countdownInterval = setInterval(this.props.decrementCountdowns, 1000)
  }

  render() {
    const { headlines } = this.props
    const headlinesList = headlines.length ? this.getHeadlines() : this.getInformMessage()

    return (
      <div className={s.headlinesList}>
        <div className='title-black top-round'>Current Headlines</div>
        <ul className='cont-gray bottom-round'>{headlinesList}</ul>
      </div>
    )
  }
}

HeadlinesList.propTypes = {
  headlines: PropTypes.array,
  openedHeadline: PropTypes.number,
  toggleHeadline: PropTypes.func,
  remove: PropTypes.func,
  restore: PropTypes.func,
  decrementCountdowns: PropTypes.func
}

export default HeadlinesList
