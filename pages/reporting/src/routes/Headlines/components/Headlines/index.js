import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Form from './Form'
import HeadlinesList from './HeadlinesList'
import ManagingPanel from './ManagingPanel'
import s from './styles.cssmodule.scss'
/* global getCookie */

class Headlines extends Component {
  componentDidMount() {
    const { router, checkAccessStartHeadlines, loadHeadlines } = this.props

    if (getCookie('reportingSuperUser') !== '1' && getCookie('uid') !== '2272005' && getCookie('uid') !== '2072301') {
      router.push('/')
    }

    checkAccessStartHeadlines()
    loadHeadlines()
  }

  getManagingPanel = () => {
    const { startGenerationAccess } = this.props

    if (startGenerationAccess) {
      return (
        <>
          <hr className='delimiter-999 m-top10 m-bottom10' />
          <ManagingPanel />
        </>
      )
    }

    return null
  }

  render() {
    const {
      headlines,
      openedHeadline,
      form,
      loading,
      updateFormField,
      saveHeadline,
      toggleHeadline,
      remove,
      restore,
      decrementCountdowns
    } = this.props
    const managingPanel = this.getManagingPanel()

    return (
      <div className={s.headlines}>
        <Form form={form} updateFormField={updateFormField} onSubmit={saveHeadline} loading={loading} />
        {managingPanel}
        <hr className='delimiter-999 m-top10 m-bottom10' />
        <HeadlinesList
          headlines={headlines}
          openedHeadline={openedHeadline}
          toggleHeadline={toggleHeadline}
          remove={remove}
          restore={restore}
          decrementCountdowns={decrementCountdowns}
        />
      </div>
    )
  }
}

Headlines.propTypes = {
  form: PropTypes.object,
  headlines: PropTypes.array,
  openedHeadline: PropTypes.number,
  loading: PropTypes.bool,
  updateFormField: PropTypes.func,
  saveHeadline: PropTypes.func,
  loadHeadlines: PropTypes.func,
  toggleHeadline: PropTypes.func,
  remove: PropTypes.func,
  restore: PropTypes.func,
  decrementCountdowns: PropTypes.func,
  router: PropTypes.object,
  startGenerationAccess: PropTypes.bool,
  checkAccessStartHeadlines: PropTypes.func
}

export default Headlines
