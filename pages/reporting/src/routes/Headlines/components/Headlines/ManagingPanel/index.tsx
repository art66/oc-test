import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { startHeadlines } from '../../../modules'
import s from './styles.cssmodule.scss'

interface IProps {
  startHeadlines: () => void
  headlines: [object]
  insertedHeadlines: number
  updatedHeadlines: number
  generateStatus: boolean
}

class ManagingPanel extends Component<IProps> {
  _handleStartHeadlines = () => {
    const { startHeadlines } = this.props

    startHeadlines()
  }

  getGenerateStatus = () => {
    const { generateStatus } = this.props

    return generateStatus && <p className={s.infoStatus}>Automatic headline is successfully generated</p>
  }

  render() {
    const { insertedHeadlines, updatedHeadlines, headlines } = this.props
    const status = this.getGenerateStatus()
    const isDisabled = headlines.length < 1

    return (
      <div className={s.managingPanel}>
        <div className='title-black top-round'>Managing automatic headlines generation</div>
        <div className='cont-gray bottom-round'>
          <div className={s.wrapper}>
            <div className={s.infoBlock}>
              <p className={s.infoBlockRow}>
                Inserted Headlines: <span className={s.infoValue}>{insertedHeadlines}</span>
              </p>
              <p className={s.infoBlockRow}>
                Updated Headlines: <span className={s.infoValue}>{updatedHeadlines}</span>
              </p>
              {status}
            </div>
            <button
              type='button'
              className={cn('torn-btn', s.startHeadlines)}
              onClick={this._handleStartHeadlines}
              disabled={isDisabled}
            >
              Run Headline Generators
            </button>
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = {
  startHeadlines
}

const mapStateToProps = state => ({
  headlines: state.headlines.headlines,
  insertedHeadlines: state.headlines.insertedHeadlines,
  updatedHeadlines: state.headlines.updatedHeadlines,
  generateStatus: state.headlines.generateStatus
})

export default connect(mapStateToProps, mapDispatchToProps)(ManagingPanel)
