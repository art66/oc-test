import ITransactions from './ITransactions'

export default interface IOwned {
  sharesAmount: number
  transactions: ITransactions[]
  transactionsAmount: number
}
