import IStockMarket from './IStockMarket'

export default interface IStore {
  stockMarket: IStockMarket
  browser: {
    mediaType: string
  }
}
