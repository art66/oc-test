export default interface IWindow {
  getUserMoney: () => number
  WebsocketHandler?: {
    addEventListener: (namespace: string, action: string, callback: (data?: any) => any) => any
  }
}
