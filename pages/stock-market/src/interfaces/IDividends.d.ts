export default interface IDividends {
  type: string
  status: string
  requirements: {
    forOne: number
    nextIncrement: number
  }
  bonus: {
    default: string
    current: string
  }
  increment: {
    possible: number
    current: number
  }
  progress: {
    total: number
    current: number
  }
}
