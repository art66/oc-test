export default interface IChart {
  time: number
  value: number
}
