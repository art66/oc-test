export default interface ISettings {
  transactionsFee: number
  isTravelling: boolean
  isInJail: boolean
  isInHospital: boolean
  charts: {
    selectedHeaderOption: number
    optionsList: {
      availabilityTime: {
        0: number,
        1: number,
        2: number,
        3: number,
        4: number,
        5: number,
        6: number
      },
      header: {
        0: string,
        1: string,
        2: string,
        3: string,
        4: string,
        5: string
      }
      dropdown: {
        1: string,
        2: string,
        3: string,
        4: string,
        5: string,
        6: string
      }
    }
  }
}
