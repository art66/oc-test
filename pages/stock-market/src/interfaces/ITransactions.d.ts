export default interface ITransactions {
  id?: number
  date?: string
  amount?: number
  boughtPrice?: number
  timestamp?: number
}
