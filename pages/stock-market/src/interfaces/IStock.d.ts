import IProfile from './IProfile'
import IChart from './IChart'
import ITransactions from './ITransactions'
import IDividends from './IDividends'

export default interface IStock {
  id: number
  profile: IProfile
  sharesPrice: {
    chartDataTab: IChart[]
    chartData: IChart[]
    transactionsChart: IChart[]
    chartImageData: string
    limitaryValues: {
      min: number,
      max: number
    }
  }
  userOwned: {
    sharesAmount: number
    transactionsAmount: number
    transactions: ITransactions[]
  }
  dividends: IDividends
  selectedTabPeriod: string
  stepBuyDialog: number
  stepSellDialog: number
  sharesValueToBuy: any
  sharesValueToSell: any
  prevSharesValueToBuy: number
  prevSharesValueToSell: number
  prevCurrentPrice: number
  sellPrice: number,
  buyPrice: number,
  mergeTransactionsIds: number[]
  activeTransaction: ITransactions
  transactionChart: IChart[]
  isShowTransaction: boolean
  isCollected: boolean
  isFetchTabChartData: boolean
  error: {
    buy: string,
    sell: string,
    withdrawDividend: string
  }
}
