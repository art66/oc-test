export default interface IDialogData {
  amountShares: number
  totalCoast: number
  diff: number
  totalCoastWithFee: number
  isBuy: boolean
  stepDialog: number
  maxValue: number
}
