import ITransactions from './ITransactions'
import IDividends from './IDividends'
import IChart from './IChart'

type TStocksDividendsWS = {
  id: number
  dividends: IDividends
}

export default TStocksDividendsWS

type TStocksPriceWS = {
  id: number
  chartsData: IChart[]
}

type TStocksGraphImagesWS = {
  id: number
  imagesData: {
    0: string
    1: string
    2: string
    3: string
    4: string
    5: string
    6: string
  }[]
}

export interface IUpdateMoneyWS {
  money: string
}

export interface ICreateTransactionWS {
  data: {
    stockId: number
    transaction: ITransactions
  }
}

export interface IRemoveTransactionsWS {
  data: {
    stockId: number
    transactions: number[]
  }
}

export interface IUpdateTransactionWS {
  data: {
    stockId: number
    transaction: ITransactions
  }
}

export interface IUpdateUserOwnedWS {
  data: {
    stockId: number
    sharesAmount: number
  }
}

export interface IRefreshStocksDividendsWS {
  data: {
    stocks: TStocksDividendsWS[]
  }
}

export interface IRefreshStocksPriceWS {
  data: {
    stocks: TStocksPriceWS[]
  }
}

export interface IRefreshStocksGraphImagesWS {
  data: {
    stocks: TStocksGraphImagesWS[]
  }
}
