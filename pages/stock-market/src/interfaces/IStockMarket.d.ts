import IStock from './IStock'
import ISettings from './ISettings'
import IChooseStock from './IChooseStock'

export default interface IStockMarket {
  chooseStock: IChooseStock
  stocks: IStock[]
  settings: ISettings
  errorApp: string
  userMoney: number
  isPriceUpdated: boolean
  withdrawDividends: string
  sortedBy: string
}
