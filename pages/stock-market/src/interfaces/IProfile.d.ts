export default interface IProfile {
  name: string
  acronym: string
  description: string
  smallLogo: {
    light: string
    dark: string
  }
  bigLogo: {
    light: string
    dark: string
  }
}
