export default {
  height: 170,
  width: '92.5%',
  hAxis: {
    maxAlternation: 1,
    maxTextLines: 2,
    baselineColor: 'transparent',
    textStyle: {
      color: '#999999',
      fontSize: 10,
      fontName: 'Fjalla One, serif'
    },
    gridlines: {
      count: 12,
      color: '#E6E6E6'
    },
    format: 'MMM d\n HH:mm',
    timeZoneName: 'GMT'
  },
  vAxis: {
    maxAlternation: 1,
    maxTextLines: 1,
    baselineColor: 'transparent',
    fractionDigits: 2,
    textStyle: {
      color: '#999999',
      fontSize: 10,
      fontName: 'Fjalla One, serif'
    },
    format: '$#.##',
    gridlines: {
      count: 4,
      color: '#E6E6E6'
    },
    viewWindow: {
      max: 'auto',
      min: 'auto'
    }
  },
  colors: ['#4DABF7'],
  style: {
    color: '#dddddd'
  },
  crosshair: {
    color: '#4DABF7',
    trigger: 'selection'
  },
  series: {
    0: {
      type: 'area',
      areaOpacity: 0.05
    }
  },
  backgroundColor: 'transparent',
  legend: {
    position: 'none'
  },
  chartArea: {
    left: 40,
    top: 10,
    width: '92%',
    height: 130,
    backgroundColor: {
      fill: '#F2F2F2',
      stroke: '#DDDDDD',
      strokeWidth: '1'
    },
    trigger: 'none'
  },
  tooltip: {
    isHtml: true
  }
}
