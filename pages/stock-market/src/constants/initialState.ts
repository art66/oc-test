import { checkDarkMode } from '@torn/sidebar/src/utils'
import IWindow from '../interfaces/IWindow'

declare let window: IWindow

export default {
  chooseStock: {
    id: 1,
    currentTab: ''
  },
  isPriceUpdated: false,
  withdrawDividends: '',
  isDarkMode: checkDarkMode(),
  sortedBy: 'price',
  errorApp: '',
  userMoney: window.getUserMoney(),
  stocks: [],
  settings: {
    transactionsFee: 0.1,
    isTravelling: false,
    isInJail: false,
    isInHospital: false,
    charts: {
      selectedHeaderOption: 2,
      optionsList: {
        availabilityTime: {
          0: 0,
          1: 0,
          2: 0,
          3: 0,
          4: 0,
          5: 0,
          6: 0
        },
        header: {
          0: 'live',
          1: '1h',
          2: '24h',
          3: '7d',
          4: '1m',
          5: '1y'
        },
        dropdown: {
          1: 'Last hour',
          2: 'Last day',
          3: 'Last week',
          4: 'Last month',
          5: 'Last year',
          6: 'All time'
        }
      }
    }
  }
}
