export const PERIODS = [
  { id: 'live', name: 'Live' },
  { id: '1h', name: 'Last hour' },
  { id: '24h', name: 'Last day' },
  { id: '7d', name: 'Last week' },
  { id: '1m', name: 'Last month' },
  { id: '1y', name: 'Last year' }
]

export const SVG_BASE_URL = 'url(data:image/png;base64,'

export const SORT_FIELD_COOKIE = 'selectedSortedBy'
export const SELECTED_HEADER_PERIOD_COOKIE = 'selectedHeaderOption'
export const COOKIE_EXPIRES_IN_DAYS = 3650 // 10 years

export const DARK_THEME = 'dark'

export const DEFAULT_SELECTED_TAB_PERIOD = 'Last hour'

export const DESKTOP_SCREEN = 'desktop'
export const MOBILE_SCREEN = 'mobile'

export const BUY_DIALOG = 'buy'
export const SELL_DIALOG = 'sell'

export const MIN_TABLET_HEIGHT_MANAGE_BLOCK = 360
export const MAX_TABLET_HEIGHT_MANAGE_BLOCK = 572
export const MIN_MOBILE_HEIGHT_MANAGE_BLOCK = 350
export const MAX_MOBILE_HEIGHT_MANAGE_BLOCK = 592
export const DEFAULT_HEIGHT_MANAGE_BLOCK = 210
export const MIN_HEIGHT_DIALOG_MOBILE = 220
export const MIN_HEIGHT_DIALOG = 200

export const PERCENTS_MULTIPLIER = 100

export const TRANSACTION_PERIOD = 'Transaction'

export const READY_STATUS = 'Ready'
export const INACTIVE_STATUS = 'Inactive'
export const IN_PROGRESS_STATUS = 'In progress'

export const ACTIVE_TYPE = 'active'
export const PASSIVE_TYPE = 'passive'

export const ACTIVE_ICON = 'ActiveBonus'
export const PASSIVE_ICON = 'PassiveBonus'
export const IN_PROGRESS_ICON = 'InProgressBonus'

export const INFO_TAB = 'nameTab'
export const DIVIDEND_TAB = 'dividendTab'
export const PRICE_TAB = 'priceTab'
export const MANAGER_TAB = 'ownedTab'

export const CHART_TAB = 'dropdown'
export const CHART_DOWN = 'down'
export const CHART_UP = 'up'
export const CHART_PAUSE = 'pause'

export const STOCK_PRESET = 'stockMarket'
export const INACTIVE_PRESET = 'INACTIVE_PROPERTIES'
export const READY_PRESET = 'READY_PROPERTIES'
export const DEFAULT_PRESET = 'DEFAULT_PROPERTIES'

export const LAST_HOUR = 'Last hour'
export const LAST_DAY = 'Last day'
export const LAST_WEEK = 'Last week'
export const LAST_MONTH = 'Last month'
export const LAST_YEAR = 'Last year'
export const ALL_TIME = 'All time'

export const CONFIRM_DIALOG_ID = 1
export const CONFIRMED_DIALOG_ID = 2

export const TRANSACTION_LOAD_LIMIT = 30

export const X_AXIS_FORMATTING = {
  'Last hour': 'HH:mm',
  'Last day': 'HH:mm',
  'Last week': 'd',
  'Last month': 'MMM d',
  'Last year': 'MMM d',
  'All time': 'YYYY'
}

export const STOCKS_ACRONYM_IS_ONES = ['EVL', 'MCS', 'TSB', 'CNC', 'TCT', 'GRN', 'TMI', 'IOU']

export const SORTING_IDS = ['price', 'name', 'owned', 'dividend']
