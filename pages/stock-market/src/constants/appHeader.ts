export default {
  titles: {
    default: {
      ID: 0,
      title: 'Stock Market'
    }
  },
  links: {
    default: {
      ID: 0,
      items: [
        {
          id: '0',
          title: 'City',
          href: '/city.php',
          icon: 'City',
          label: 'City-link',
          isSPA: true
        }
      ]
    }
  },
  tutorials: {
    activateOnLoad: true,
    hideByDefault: true,
    default: {
      ID: 0,
      items: [
        {
          title: 'Stock Market Tutorial',
          text: `Here you can invest your hard earned money in the different Torn corporations under the TCSE. Each
          corporation will offer a special bonus if you own a certain amount of shares in them; however this is often
          hundreds of millions of dollars worth.`
        }
      ]
    }
  }
}
