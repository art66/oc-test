export default {
  height: 170,
  width: '92.5%',
  hAxis: {
    maxAlternation: 1,
    maxTextLines: 2,
    baselineColor: 'transparent',
    textStyle: {
      color: '#999999',
      fontSize: 10,
      fontName: 'Fjalla One, serif'
    },
    gridlines: {
      count: 12,
      color: '#000000'
    },
    format: 'MMM d\n HH:mm'
  },
  vAxis: {
    maxAlternation: 1,
    maxTextLines: 1,
    baselineColor: 'transparent',
    fractionDigits: 2,
    textStyle: {
      color: '#999999',
      fontSize: 10,
      fontName: 'Fjalla One, serif'
    },
    format: '$',
    gridlines: {
      count: 4,
      color: '#000000',
      shadowColor: '#FFFFFF1F',
      shadowOffsetY: 1
    },
    viewWindow: {
      max: 'auto',
      min: 'auto'
    }
  },
  colors: ['#4DABF7'],
  style: {
    color: '#dddddd'
  },
  crosshair: {
    color: '#4DABF7',
    trigger: 'selection'
  },
  series: {
    0: {
      type: 'area',
      areaOpacity: 0.05
    }
  },
  backgroundColor: 'transparent',
  legend: {
    position: 'none'
  },
  chartArea: {
    left: 40,
    top: 10,
    width: '92%',
    height: 130,
    backgroundColor: {
      fill: '#242424',
      stroke: '#000000',
      strokeWidth: '1'
    },
    trigger: 'none'
  },
  tooltip: {
    isHtml: true
  }
}
