import { put, select } from 'redux-saga/effects'
import { fetchUrl, getCookie } from '@torn/shared/utils'
import { setStockMarketData, setAppErrorMessage, changeSorting, setCurrentTab, setTabPeriod } from '../actions'
import { getAvailabilityTimePeriods, getTabOptionsList } from '../selectors'
import { checkIsDisabled, getOptionId } from '../utils'

function* fetchStockMarketData() {
  try {
    const url = window.location.search
    const params = url
      .slice(1)
      .split('&')
      .map(p => p.split('='))
      .reduce(
        (obj, pair) => {
          const [key, value] = pair.map(decodeURIComponent)

          obj[key] = value
          return obj
        },
        { stockID: null, tab: '', period: '' }
      )

    const data = yield fetchUrl(
      'page.php?sid=StockMarket&step=getInitialData',
      getCookie('selectedHeaderOption') && { chartPeriod: +getCookie('selectedHeaderOption') }
    )

    if (data.success) {
      yield put(setStockMarketData(data))
      yield put(setAppErrorMessage(''))
      yield put(changeSorting(getCookie('selectedSortedBy')))

      if (params && params?.stockID) {
        const element = document.getElementById(params.stockID)

        if (element.offsetTop + 210 > window.innerHeight) {
          window.scrollTo({
            top: element.offsetTop,
            behavior: 'auto'
          })
        }

        const periodValue = params.period === 'alltime' ? 'All time' : `Last ${params.period}`
        const tabOptionList = yield select(getTabOptionsList)
        const availabilityTime = yield select(getAvailabilityTimePeriods)
        const chartPeriod = getOptionId(tabOptionList, periodValue)
        const isDisabled = checkIsDisabled(chartPeriod, availabilityTime)

        if (!isDisabled && !isNaN(chartPeriod)) {
          yield params?.period && put(setTabPeriod(periodValue))
        }

        if (params.tab === 'price' || params.tab === 'owned' || params.tab === 'dividend' || params.tab === 'name') {
          yield put(setCurrentTab(+params.stockID, `${params.tab}Tab`))
        }
      }
    } else {
      yield put(setAppErrorMessage(data.message))
    }
  } catch (e) {
    console.error(e)
  }
}

export default fetchStockMarketData
