import { all, put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  getChooseStockId,
  getCurrentStockPrice,
  getSharesAmount,
  getSharesValueToBuy,
  getStepBuyDialog
} from '../selectors'
import { setBuyPrice, setBuyStepDialog, setErrorMessage, updateUserOwned, setSharesValueToSell } from '../actions'
import { BUY_DIALOG } from '../constants'

function* buyShares() {
  try {
    const stepBuyDialog = yield select(getStepBuyDialog)
    const [stockId, amount] = yield all([select(getChooseStockId), select(getSharesValueToBuy)])
    const currentPrice = yield select(getCurrentStockPrice)
    const currentUserOwned = yield select(getSharesAmount)
    const data = yield fetchUrl('page.php?sid=StockMarket&step=buyShares', { stockId, amount })

    if (data.success) {
      yield put(setBuyStepDialog(stepBuyDialog + 1))
      yield put(setBuyPrice(currentPrice))
      yield put(updateUserOwned(stockId, currentUserOwned + amount))
      yield put(setSharesValueToSell(stockId, currentUserOwned + amount))
      yield put(setErrorMessage(BUY_DIALOG, ''))
    } else {
      yield put(setErrorMessage(BUY_DIALOG, data.message))
      yield put(setBuyStepDialog(stepBuyDialog + 1))
    }
  } catch (e) {
    console.error(e)
  }
}

export default buyShares
