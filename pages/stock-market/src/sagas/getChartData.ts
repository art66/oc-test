import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { setChartData } from '../actions'

function* getChartData(action: {
  type: string
  payload: {
    id: number
    period: number
  }
}) {
  try {
    const { id, period } = action.payload
    const data = yield fetchUrl('page.php?sid=StockMarket&step=getChartData', {
      stockId: id,
      chartPeriod: period
    })

    if (data.success) {
      yield put(setChartData(data.chart))
    }
  } catch (e) {
    console.error(e)
  }
}

export default getChartData
