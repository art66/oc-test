import { all, put, select } from 'redux-saga/effects'
import { getStepBuyDialog, getStepSellDialog } from '../selectors'
import { setBuyStepDialog, setSellStepDialog } from '../actions'
import { BUY_DIALOG } from '../constants'
import IType from '../interfaces/IType'

function* nextClickSaga(action: { type: IType; payload: { type: string } }) {
  const { type } = action.payload
  const [stepBuyDialog, stepSellDialog] = yield all([select(getStepBuyDialog), select(getStepSellDialog)])

  type === BUY_DIALOG ?
    yield put(setBuyStepDialog(stepBuyDialog + 1)) :
    yield put(setSellStepDialog(stepSellDialog + 1))
}

export default nextClickSaga
