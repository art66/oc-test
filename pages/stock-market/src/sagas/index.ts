import { takeEvery } from 'redux-saga/effects'
import * as actionTypes from '../actions/actionsTypes'
import fetchStockMarketData from './fetchStockMarketData'
import getAllChartData from './getAllChartData'
import getChartData from './getChartData'
import buyShares from './buyShares'
import sellTransaction from './sellTransaction'
import sellShares from './sellShares'
import mergeTransactions from './mergeTransactions'
import getChartDataByTransactionId from './getChartDataByTransactionId'
import withdrawDividend from './withdrawDividend'
import backClick from './backClick'
import nextClickSaga from './nextClick'
import getMoreTransactions from './getMoreTransactions'
import changeStock from './changeStock'
import removeTransactions from './removeTransactions'
import changeSortingStocks from './changeSortingStocks'
import { updateRequestURL } from './updateURL'

export default function* stockMarket() {
  yield takeEvery(actionTypes.FETCH_STOCK_MARKET_DATA, fetchStockMarketData)
  yield takeEvery(actionTypes.GET_ALL_CHART_DATA, getAllChartData)
  yield takeEvery(actionTypes.GET_CHART_DATA, getChartData)
  yield takeEvery(actionTypes.BUY_SHARES, buyShares)
  yield takeEvery(actionTypes.SELL_SHARES, sellShares)
  yield takeEvery(actionTypes.SET_TAB_PERIOD, updateRequestURL)
  yield takeEvery(actionTypes.SET_ALL_TAB_PERIOD, updateRequestURL)
  yield takeEvery(actionTypes.MERGE_TRANSACTIONS, mergeTransactions)
  yield takeEvery(actionTypes.VIEW_TRANSACTION, getChartDataByTransactionId)
  yield takeEvery(actionTypes.VIEW_TRANSACTION, updateRequestURL)
  yield takeEvery(actionTypes.WITHDRAW_DIVIDEND, withdrawDividend)
  yield takeEvery(actionTypes.GET_MORE_TRANSACTIONS, getMoreTransactions)
  yield takeEvery(actionTypes.BACK_CLICK, backClick)
  yield takeEvery(actionTypes.NEXT_CLICK, nextClickSaga)
  yield takeEvery(actionTypes.SELL_TRANSACTION, sellTransaction)
  yield takeEvery(actionTypes.SET_CURRENT_TAB, changeStock)
  yield takeEvery(actionTypes.SET_CURRENT_TAB, updateRequestURL)
  yield takeEvery(actionTypes.SET_USER_MONEY, removeTransactions)
  yield takeEvery(actionTypes.CHANGE_SORTING, changeSortingStocks)
}
