import { select, put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getMergeTransactionsIds } from '../selectors'
import { refreshMergeTransactions } from '../actions'

function* mergeTransactions() {
  try {
    const transactionsIds = yield select(getMergeTransactionsIds)

    const data = yield fetchUrl('page.php?sid=StockMarket&step=mergeTransactions', { transactionsIds })

    if (data.success) {
      yield put(refreshMergeTransactions())
    }
  } catch (e) {
    console.error(e)
  }
}

export default mergeTransactions
