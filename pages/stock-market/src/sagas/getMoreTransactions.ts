import { all, put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getChooseStockId, getTransactionOffset } from '../selectors'
import { setMoreTransactions } from '../actions'
import IType from '../interfaces/IType'

function* getMoreTransactions(action: { type: IType; payload: { count: number } }) {
  try {
    const [stockId, offset] = yield all([select(getChooseStockId), select(getTransactionOffset)])
    const data = yield fetchUrl(
      'page.php?sid=StockMarket&step=getTransactions',
      { stockId, offset, limit: action.payload.count }
    )

    if (data.success) {
      yield put(setMoreTransactions(data.transactions))
    }
  } catch (e) {
    console.error(e)
  }
}

export default getMoreTransactions
