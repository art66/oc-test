import { put, select } from 'redux-saga/effects'
import { getChooseStockId, getTransactions } from '../selectors'
import { setActiveTransaction, setSellStepDialog, setSharesValueToSell } from '../actions'
import IType from '../interfaces/IType'

function* sellTransaction(action: { type: IType; payload: { transactionId: number } }) {
  const chooseStockId = yield select(getChooseStockId)
  const transactions = yield select(getTransactions)
  const activeTransaction = transactions.find(item => item.id === action.payload.transactionId)

  yield put(setSellStepDialog(1))
  yield put(setActiveTransaction(activeTransaction))
  yield put(setSharesValueToSell(chooseStockId, activeTransaction.amount))
}

export default sellTransaction
