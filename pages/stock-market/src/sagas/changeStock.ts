import { put, all, select } from 'redux-saga/effects'
import { setSharesValueToSell, setSharesValueToBuy } from '../actions'
import {
  getChooseStockId,
  getMaxSharesValueForBuy,
  getMaxSharesValueForSell,
  getStepBuyDialog,
  getStepSellDialog
} from '../selectors'

function* changeStock() {
  const chooseStockId = yield select(getChooseStockId)
  const [sharesBuy, sharesSell] = yield all([select(getMaxSharesValueForBuy), select(getMaxSharesValueForSell)])
  const [stepBuyDialog, stepSellDialog] = yield all([select(getStepBuyDialog), select(getStepSellDialog)])

  yield stepBuyDialog === 0 && put(setSharesValueToBuy(chooseStockId, sharesBuy))
  yield stepSellDialog === 0 && put(setSharesValueToSell(chooseStockId, sharesSell))
}

export default changeStock
