import { put, select } from 'redux-saga/effects'
import { getChooseStockId, getMaxSharesValueForBuy } from '../selectors'
import { setSharesValueToBuy } from '../actions'

function* removeTransactions() {
  try {
    const chooseStockId = yield select(getChooseStockId)
    const maxSharesValue = yield select(getMaxSharesValueForBuy)

    yield put(setSharesValueToBuy(chooseStockId, maxSharesValue))
  } catch (e) {
    console.error(e)
  }
}

export default removeTransactions
