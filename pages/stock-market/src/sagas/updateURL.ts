import { put, select } from 'redux-saga/effects'
import { setRequestUrl } from '../actions'
import { getChooseStockCurrentTab, getChooseStockId, getSelectedTabPeriod } from '../selectors'
import { ALL_TIME, PRICE_TAB } from '../constants'

const prepareDataForURL = (
  id: number,
  tab: string,
  period: string
): {
  stockID: number | string
  tab: string
  period: string
} => {
  const periodValue = period === ALL_TIME ? 'alltime' : period.replace('Last ', '')

  return {
    stockID: tab ? id : '',
    tab: tab.replace('Tab', ''),
    period: tab === PRICE_TAB ? periodValue : ''
  }
}

export function* updateRequestURL() {
  const id = yield select(getChooseStockId)
  const tab = yield select(getChooseStockCurrentTab)
  const period = yield select(getSelectedTabPeriod)
  const dataURL = prepareDataForURL(id, tab, period)
  const url = Object.entries(dataURL)
    .map(([key, val]) => (val ? `${key}=${val}` : null))
    .filter(item => item)
    .join('&')
  const urlToSave = url.length ? `&${url}` : url

  yield put(setRequestUrl(urlToSave))

  const newUrl = `?sid=stocks${urlToSave}`

  if (window.location.search !== newUrl) {
    window.history.pushState(null, null, newUrl)
  }
}
