import { orderBy } from 'lodash'
import { put, select } from 'redux-saga/effects'
import { setChangeSorting } from '../actions'
import { getStocks } from '../selectors'
import { ACTIVE_TYPE, IN_PROGRESS_STATUS, PASSIVE_TYPE, READY_STATUS } from '../constants'
import IType from '../interfaces/IType'

function* changeSortingStocks(action: {type: IType; payload: { field: string }}) {
  const { field } = action.payload
  const stocks = yield select(getStocks)

  let stocksSorted,
    prepareStocks

  switch (field) {
    case 'name':
      stocksSorted = orderBy(stocks, 'profile.name', 'asc')
      break
    case 'dividend':
      prepareStocks = stocks.map(stock => {
        if (stock.dividends.type === ACTIVE_TYPE && stock.dividends.status === READY_STATUS) {
          return {
            ...stock,
            dividendPosition: 1,
            dividendProgress: stock.dividends.progress.total - stock.dividends.progress.current
          }
        }

        if (stock.dividends.type === PASSIVE_TYPE && stock.dividends.status === READY_STATUS) {
          return {
            ...stock,
            dividendPosition: 2,
            dividendProgress: stock.dividends.progress.total - stock.dividends.progress.current
          }
        }

        if (stock.dividends.status === IN_PROGRESS_STATUS) {
          return {
            ...stock,
            dividendPosition: 3,
            dividendProgress: stock.dividends.progress.total - stock.dividends.progress.current
          }
        }

        return {
          ...stock,
          dividendPosition: 4,
          dividendProgress: stock.dividends.progress.total - stock.dividends.progress.current
        }
      })

      stocksSorted = orderBy(prepareStocks, ['dividendPosition', 'dividendProgress'], ['asc', 'asc'])
      break
    case 'owned':
      prepareStocks = stocks.map(stock => ({
        ...stock,
        moneyValue: stock.sharesPrice.chartData[stock.sharesPrice.chartData.length - 1].value * stock.userOwned.sharesAmount
      }))

      stocksSorted = orderBy(prepareStocks, 'moneyValue', 'desc')
      break
    default:
      prepareStocks = stocks.map(stock => ({
        ...stock,
        currentPrice: stock.sharesPrice.chartData[stock.sharesPrice.chartData.length - 1].value
      }))

      stocksSorted = orderBy(prepareStocks, 'currentPrice', 'desc')
      break
  }

  yield put(setChangeSorting(stocksSorted, field || 'price'))
}

export default changeSortingStocks
