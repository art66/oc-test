import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getChooseStockId } from '../selectors'
import { refreshStocksDividends, setErrorMessage, setIsCollected, setWithdrawDividend } from '../actions'

function* withdrawDividend() {
  try {
    const stockId = yield select(getChooseStockId)

    const data = yield fetchUrl('page.php?sid=StockMarket&step=withdrawDividend', { stockId })

    if (data.success) {
      yield put(setWithdrawDividend(data.withdrawnBonus))
      yield put(setErrorMessage('withdrawDividend', ''))
      yield put(refreshStocksDividends(data.stocks))
      yield put(setIsCollected(true))
    } else {
      yield put(setErrorMessage('withdrawDividend', data.message))
      yield put(setIsCollected(true))
    }
  } catch (e) {
    console.error(e)
  }
}

export default withdrawDividend
