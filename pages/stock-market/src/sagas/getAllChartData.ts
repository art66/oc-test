import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { setAllChartData } from '../actions'
import IType from '../interfaces/IType'

function* getAllChartData(action: { type: IType; payload: { period: number } }) {
  try {
    const { period } = action.payload
    const data = yield fetchUrl('page.php?sid=StockMarket&step=getAllChartData', {
      chartPeriod: period
    })

    if (data.success) {
      yield put(setAllChartData(data))
    }
  } catch (e) {
    console.error(e)
  }
}

export default getAllChartData
