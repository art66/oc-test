import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { setCurrentTab, setTabPeriod, setTransactionChartData } from '../actions'
import { getChooseStockId } from '../selectors'
import IType from '../interfaces/IType'
import { PRICE_TAB } from '../constants/index'

function* getChartDataByTransactionId(action: { type: IType; payload: { transactionId: number } }) {
  try {
    const { transactionId } = action.payload
    const currentStockId = yield select(getChooseStockId)
    const data = yield fetchUrl('page.php?sid=StockMarket&step=getChartDataByTransactionID', { transactionId })

    if (data.success) {
      yield put(setTabPeriod(''))
      yield put(setCurrentTab(currentStockId, PRICE_TAB))
      yield put(setTransactionChartData(data.charts))
    }
  } catch (e) {
    console.error(e)
  }
}

export default getChartDataByTransactionId
