import { all, put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  getActiveTransaction,
  getChooseStockId,
  getCurrentStockPrice,
  getSharesAmount,
  getSharesValueToSell,
  getStepSellDialog
} from '../selectors'
import { setErrorMessage, setSellPrice, setSellStepDialog, updateUserOwned } from '../actions'
import { SELL_DIALOG } from '../constants'

function* sellShares() {
  try {
    const stepSellDialog = yield select(getStepSellDialog)
    const [stockId, amount, activeTransaction] = yield all([
      select(getChooseStockId),
      select(getSharesValueToSell),
      select(getActiveTransaction)
    ])
    const currentPrice = yield select(getCurrentStockPrice)
    const currentUserOwned = yield select(getSharesAmount)
    const payload = activeTransaction ? { stockId, amount, transactionId: activeTransaction.id } : { stockId, amount }
    const data = yield fetchUrl('page.php?sid=StockMarket&step=sellShares', payload)

    if (data.success) {
      yield put(setSellStepDialog(stepSellDialog + 1))
      yield put(setSellPrice(currentPrice))
      yield put(updateUserOwned(stockId, currentUserOwned - amount))
      yield put(setErrorMessage(SELL_DIALOG, ''))
    } else {
      yield put(setErrorMessage(SELL_DIALOG, data.message))
      yield put(setSellStepDialog(stepSellDialog + 1))
    }
  } catch (e) {
    console.error(e)
  }
}

export default sellShares
