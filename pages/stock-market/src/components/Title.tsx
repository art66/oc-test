import React from 'react'
import cn from 'classnames'
import Dropdown from '@torn/shared/components/Dropdown'
import { setCookie } from '@torn/shared/utils'
import { useDispatch, useSelector } from 'react-redux'
import {
  getAvailabilityTimePeriods,
  getHeaderOptionsList,
  getSelectedHeaderPeriod,
  getSortedBy,
  getTabOptionsList
} from '../selectors'
import {
  PERIODS,
  SORTING_IDS,
  SELECTED_HEADER_PERIOD_COOKIE,
  DEFAULT_SELECTED_TAB_PERIOD,
  SORT_FIELD_COOKIE,
  COOKIE_EXPIRES_IN_DAYS
} from '../constants'
import { checkIsDisabled, getOptionId } from '../utils'
import { setHeaderPeriod, getAllChartData, setAllTabPeriod, changeSorting } from '../actions'
import s from './styles/Title.cssmodule.scss'

const Title = () => {
  const dispatch = useDispatch()
  const optionList = useSelector(getHeaderOptionsList)
  const tabOptionList = useSelector(getTabOptionsList)
  const selectedHeaderOption = useSelector(getSelectedHeaderPeriod)
  const availabilityTime = useSelector(getAvailabilityTimePeriods)
  const sortedBy = useSelector(getSortedBy)

  const handleChangePeriod = period => {
    const chartPeriod = getOptionId(optionList, period.id)
    const isDisabled = checkIsDisabled(chartPeriod, availabilityTime)

    if (!isDisabled) {
      dispatch(setHeaderPeriod(chartPeriod))
      setCookie(SELECTED_HEADER_PERIOD_COOKIE, `${chartPeriod}`, COOKIE_EXPIRES_IN_DAYS)
      dispatch(getAllChartData(chartPeriod))
      dispatch(setAllTabPeriod(chartPeriod ? tabOptionList[chartPeriod] : DEFAULT_SELECTED_TAB_PERIOD))
    }
  }

  const periods = PERIODS.map((item, index) => {
    return {
      ...item,
      id: checkIsDisabled(index, availabilityTime) ? PERIODS[selectedHeaderOption].id : item.id,
      customClass: checkIsDisabled(index, availabilityTime) ? 'disabledPeriod' : ''
    }
  })

  const sorting = e => {
    if (SORTING_IDS.includes(e.target.id)) {
      dispatch(changeSorting(e.target.id))
      setCookie(SORT_FIELD_COOKIE, `${e.target.id}`, COOKIE_EXPIRES_IN_DAYS)
    }
  }

  const sortingByEnter = e => {
    if (e.key === 'Enter') {
      sorting(e)
    }
  }

  return (
    <ul className={cn('title-black', s.titles)}>
      <li
        id='name'
        className={cn(s.title, s.stockName, { [s.sorted]: sortedBy === 'name' })}
        onClick={sorting}
        onKeyPress={sortingByEnter}
      >
        Stock <span className={s.showDesktop}>Name</span>
        <span className={s.sortedIcon} />
      </li>
      <li
        id='price'
        className={cn(s.title, s.price, { [s.sorted]: sortedBy === 'price' })}
        onClick={sorting}
        onKeyPress={sortingByEnter}
      >
        <div id='price' className={s.priceContainer}>
          Price
          <Dropdown
            id='0'
            className='react-dropdown-default'
            list={periods}
            selected={periods[selectedHeaderOption]}
            name='chartPeriod'
            onChange={handleChangePeriod}
            selectedId={true}
          />
          <span className={s.sortedIcon} />
        </div>
      </li>
      <li
        id='owned'
        className={cn(s.title, s.owned, { [s.sorted]: sortedBy === 'owned' })}
        onClick={sorting}
        onKeyPress={sortingByEnter}
      >
        Owned
        <span className={s.sortedIcon} />
      </li>
      <li
        id='dividend'
        className={cn(s.title, s.dividend, { [s.sorted]: sortedBy === 'dividend' })}
        onClick={sorting}
        onKeyPress={sortingByEnter}
      >
        Dividend
        <span className={s.sortedIcon} />
      </li>
    </ul>
  )
}

export default Title
