import React from 'react'
import { useSelector } from 'react-redux'
import IDividends from '../interfaces/IDividends'
import ActiveBonusMessage from './DividendMessages/ActiveBonusMessage'
import PassiveBonusMessage from './DividendMessages/PassiveBonusMessage'
import InProgressBonusMessage from './DividendMessages/InProgressBonusMessage'
import ConfirmedActiveBonusMessage from './DividendMessages/ConfirmedActiveBonusMessage'
import { ACTIVE_TYPE, PASSIVE_TYPE, READY_STATUS } from '../constants'
import IProfile from '../interfaces/IProfile'
import { getIsCollected, getIsHospital, getIsJail, getIsTravelling } from '../selectors'
import BonusMessage from './DividendMessages/BonusMessage'

interface IProps {
  dividends: IDividends
  profile: IProfile
}

const DividendTab = ({ dividends, profile: { name, acronym } }: IProps) => {
  const { status, type } = dividends
  const isCollected = useSelector(getIsCollected)
  const isTravelling = useSelector(getIsTravelling)
  const isJail = useSelector(getIsJail)
  const isHospital = useSelector(getIsHospital)
  const activeDividend = isCollected ?
    <ConfirmedActiveBonusMessage acronym={acronym} /> :
    <ActiveBonusMessage acronym={acronym} dividends={dividends} />

  if (status === READY_STATUS) {
    if (type === ACTIVE_TYPE) {
      return isTravelling || isJail || isHospital ? <BonusMessage acronym={acronym} /> : activeDividend
    } else if (type === PASSIVE_TYPE) {
      return <PassiveBonusMessage stockName={name} acronym={acronym} dividends={dividends} />
    }
  }

  return type === ACTIVE_TYPE && isCollected ?
    <ConfirmedActiveBonusMessage acronym={acronym} /> :
    <InProgressBonusMessage stockName={name} acronym={acronym} dividends={dividends} />
}

export default DividendTab
