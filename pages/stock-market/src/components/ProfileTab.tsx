import React from 'react'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import IProfile from '../interfaces/IProfile'
import { DARK_THEME } from '../constants'
import s from './styles/ProfileTab.cssmodule.scss'

interface IProps {
  profile: IProfile
}

const ProfileTab = ({ profile, theme }: TWithThemeInjectedProps & IProps) => {
  const { description, bigLogo, name } = profile
  const isDarkMode = theme === DARK_THEME

  return (
    <div className={s.infoDropdown}>
      <div className={s.logo}>
        <img src={isDarkMode ? bigLogo.dark : bigLogo.light} alt={`${name} stock logo`} />
      </div>
      <div className={s.description}>{description}</div>
    </div>
  )
}

export default withTheme(ProfileTab)
