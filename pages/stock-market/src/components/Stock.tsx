import React, { useState } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolderDividend from '@torn/shared/SVG/helpers/iconsHolder/stockMarket'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import { toMoney } from '@torn/shared/utils'
import { useDispatch, useSelector } from 'react-redux'
import { getChooseStockCurrentTab, getChooseStockId, getMediaType, getChooseStockInfo } from '../selectors'
import ProgressBar from './ProgressBar'
import StockTabs from './StockTabs'
import DividendTooltip from './DividendTooltip'
import {
  DIVIDEND_TAB,
  PRICE_TAB,
  INFO_TAB,
  MANAGER_TAB,
  STOCK_PRESET,
  DESKTOP_SCREEN,
  SVG_BASE_URL,
  DARK_THEME
} from '../constants'
import { setCurrentTab } from '../actions'
import IStock from '../interfaces/IStock'
import { getChangesPrice, getPriceChangeIcon, getDividendStatusMsg } from '../utils'
import s from './styles/Stock.cssmodule.scss'
import gs from '../styles/global.cssmodule.scss'

interface IProps {
  stock: IStock
}

// eslint-disable-next-line complexity
const Stock = ({ stock, theme }: TWithThemeInjectedProps & IProps) => {
  const dispatch = useDispatch()
  const chooseStock = useSelector(getChooseStockInfo)
  const mediaType = useSelector(getMediaType)
  const currentTab = useSelector(getChooseStockCurrentTab)
  const id = useSelector(getChooseStockId)
  const isDarkMode = theme === DARK_THEME
  const [isShowTooltip, setIsShowTooltip] = useState(false)
  const { profile, userOwned, dividends, sharesPrice } = stock
  const stockPrice = sharesPrice.chartData[sharesPrice.chartData.length - 1].value
  const { dollars, percents, changeType } = getChangesPrice(sharesPrice.chartData, stockPrice)
  const dividendIcon = getPriceChangeIcon(dividends.type, dividends.status, isDarkMode)
  const priceStyles =
    id === stock.id && currentTab === PRICE_TAB ?
      {
        background: `${SVG_BASE_URL}${sharesPrice.chartImageData})`,
        backgroundColor: isDarkMode ? '#222' : '#fff'
      } :
      { background: `${SVG_BASE_URL}${sharesPrice.chartImageData})` }

  const renderFormattedPrice = () => {
    const arrayOfDigits = Array.from(String(stockPrice.toFixed(2)))

    return arrayOfDigits.map((item, index) => {
      return (
        <span key={index} className={s.number}>
          {item}
        </span>
      )
    })
  }

  const renderTab = () => stock.id === id && currentTab && <StockTabs stock={stock} chooseStock={chooseStock} />

  const handleOpenTab = e => {
    const tabName = e.currentTarget.getAttribute('data-name')

    dispatch(setCurrentTab(stock.id, stock.id === id && tabName === currentTab ? '' : tabName))
  }

  const handleOpenTabByKey = e => {
    if (e.keyCode === 'Tab') {
      handleOpenTab(e)
    }
  }

  const handleDoubleClick = e => e.preventDefault()
  const handleMouseEnter = () => setIsShowTooltip(true)
  const handleMouseLeave = () => setIsShowTooltip(false)

  return (
    <>
      <ul id={`${stock.id}`} className={cn(s.stock, s[stock.profile.acronym])} role='tablist'>
        <li
          className={cn(s.stockName, id === stock.id && currentTab === INFO_TAB ? s.active : '')}
          data-name={INFO_TAB}
          role='tab'
          id={INFO_TAB}
          aria-controls={`panel-${INFO_TAB}`}
          aria-label={`Stock: ${profile.name}`}
          tabIndex={0}
          onClick={handleOpenTab}
          onKeyDown={handleOpenTabByKey}
        >
          <div className={s.logoContainer}>
            <img
              src={isDarkMode ? profile.smallLogo.dark : profile.smallLogo.light}
              alt={`${profile.name} small logo`}
            />
          </div>
          <div className={s.nameContainer}>{profile.name}</div>
        </li>
        <li
          className={cn(s.stockPrice, id === stock.id && currentTab === PRICE_TAB ? s.active : '')}
          data-name={PRICE_TAB}
          id={PRICE_TAB}
          role='tab'
          tabIndex={0}
          aria-label={`Share stock price: $${stockPrice}`}
          aria-controls={`panel-${PRICE_TAB}`}
          onClick={handleOpenTab}
          onKeyDown={handleOpenTabByKey}
          style={priceStyles}
        >
          <div className={s.price}>{renderFormattedPrice()}</div>
          <div className={cn(s.changePrice)}>
            <p className={gs[changeType]}>
              {mediaType === DESKTOP_SCREEN && (
                <>
                  ${dollars} <span className={gs.icon} />
                </>
              )}
              {mediaType !== DESKTOP_SCREEN && (
                <>
                  <span className={cn(gs.icon, gs.tabIcon)} /> ${dollars}
                </>
              )}
            </p>
            <p className={gs[changeType]}>
              {mediaType === DESKTOP_SCREEN && (
                <>
                  {percents}% <span className={gs.icon} />
                </>
              )}
              {mediaType !== DESKTOP_SCREEN && (
                <>
                  <span className={cn(gs.icon, gs.tabIcon)} /> {percents}%
                </>
              )}
            </p>
          </div>
        </li>
        <li
          className={cn(s.stockOwned, id === stock.id && currentTab === MANAGER_TAB ? s.active : '')}
          data-name={MANAGER_TAB}
          role='tab'
          id={MANAGER_TAB}
          aria-label={`Owned: ${userOwned.sharesAmount} shares`}
          aria-controls={`panel-${MANAGER_TAB}`}
          tabIndex={0}
          onClick={handleOpenTab}
          onKeyDown={handleOpenTabByKey}
        >
          <p className={s.value}>{toMoney(Math.ceil(stockPrice * stock.userOwned.sharesAmount), '$')}</p>
          <p className={s.count}>{toMoney(userOwned.sharesAmount ? userOwned.sharesAmount : 'None', '')}</p>
        </li>
        <li
          className={cn(s.stockDividend, id === stock.id && currentTab === DIVIDEND_TAB ? s.active : '')}
          data-name={DIVIDEND_TAB}
          id={DIVIDEND_TAB}
          role='tab'
          tabIndex={0}
          aria-label={`Dividend: ${dividends.bonus.current}. Status: ${getDividendStatusMsg(dividends)}`}
          aria-controls={`panel-${DIVIDEND_TAB}`}
          onClick={handleOpenTab}
          onKeyDown={handleOpenTabByKey}
        >
          <div className={s.dividendInfo}>
            <p className={s.dividend}>{dividends.bonus.current}</p>
            <p className={cn(s[dividends.type], s[dividends.status])}>{getDividendStatusMsg(dividends)}</p>
          </div>
          <div
            className={s.dividendStatus}
            id={`dividendStatus${profile.acronym}`}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
            onDoubleClick={handleDoubleClick}
          >
            <ProgressBar
              totalSegments={dividends.progress.total}
              filledSegments={dividends.progress.current}
              offset={dividends.progress.total === 1 ? 0 : 7}
              radius={170}
              type='progress'
            />
            <ProgressBar
              totalSegments={Math.ceil(dividends.increment.possible)}
              filledSegments={dividends.increment.current}
              filledPartSegments={dividends.increment.possible}
              offset={Math.ceil(dividends.increment.possible) === 1 ? 0 : 7}
              radius={140}
              type='increment'
            />
            <SVGIconGenerator
              customClass={s.dividendIcon}
              iconName={dividendIcon.icon}
              iconsHolder={iconsHolderDividend}
              preset={{ type: STOCK_PRESET, subtype: dividendIcon.preset }}
            />
            <DividendTooltip isShowTooltip={isShowTooltip} acronym={profile.acronym} dividends={dividends} />
          </div>
        </li>
      </ul>
      {renderTab()}
    </>
  )
}

export default withTheme(Stock)
