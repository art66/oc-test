import React, { useEffect } from 'react'
import cn from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import AnimationLoad from '@torn/shared/components/AnimationLoad'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import IChart from '../interfaces/IChart'
import Chart from './Chart'
import {
  getChooseStock,
  getCurrentStockPrice,
  getIsShowTransaction,
  getSelectedTabPeriod,
  getTabOptionsList,
  getIsFetchTabChartData,
  getAvailabilityTimePeriods,
  getLimitaryValuesChartData
} from '../selectors'
import { checkIsDisabled, getOptionId, getPreparedDataForChart, getChangesPrice } from '../utils'
import { getChartData, setAllTabPeriod } from '../actions'
import { TRANSACTION_PERIOD, DARK_THEME } from '../constants'
import s from './styles/PriceTab.cssmodule.scss'
import gs from '../styles/global.cssmodule.scss'

interface IProps {
  sharesPrice: {
    chartData: IChart[]
    chartDataTab: IChart[]
    transactionsChart: IChart[]
  }
}

const PriceTab = ({ sharesPrice, theme }: TWithThemeInjectedProps & IProps) => {
  const dispatch = useDispatch()
  const isDarkMode = theme === DARK_THEME
  const chooseStock = useSelector(getChooseStock)
  const tabPeriod = useSelector(getSelectedTabPeriod)
  const availabilityTime = useSelector(getAvailabilityTimePeriods)
  const currentPrice = useSelector(getCurrentStockPrice)
  const isShowTransaction = useSelector(getIsShowTransaction)
  const optionsList = useSelector(getTabOptionsList)
  const isFetchTabChartData = useSelector(getIsFetchTabChartData)
  const limitaryValues = useSelector(getLimitaryValuesChartData)
  const chartData = isShowTransaction ? sharesPrice.transactionsChart : sharesPrice.chartDataTab
  const { dollars, percents, changeType } = getChangesPrice(chartData, currentPrice)
  const servers = isShowTransaction ?
    getPreparedDataForChart(sharesPrice.transactionsChart) :
    getPreparedDataForChart(sharesPrice.chartDataTab)
  const selectedTabPeriod = isShowTransaction || !tabPeriod ? TRANSACTION_PERIOD : tabPeriod
  const currentPriceValue = isShowTransaction ?
    sharesPrice.transactionsChart[sharesPrice.transactionsChart.length - 1].value.toFixed(2) :
    sharesPrice.chartDataTab[sharesPrice.chartDataTab.length - 1].value.toFixed(2)
  const startPrice = isShowTransaction ?
    sharesPrice.transactionsChart[0].value.toFixed(2) :
    sharesPrice.chartDataTab[0].value.toFixed(2)
  const endPrice = isShowTransaction ?
    sharesPrice.transactionsChart[sharesPrice.transactionsChart.length - 1].value.toFixed(2) :
    sharesPrice.chartDataTab[sharesPrice.chartDataTab.length - 1].value.toFixed(2)

  const handleChangeTabPeriod = e => {
    if (checkIsDisabled(getOptionId(optionsList, e.currentTarget.id), availabilityTime)) {
      return
    }

    dispatch(setAllTabPeriod(e.currentTarget.id))
  }

  useEffect(() => {
    if (!isShowTransaction) {
      tabPeriod && dispatch(getChartData(chooseStock.id, getOptionId(optionsList, selectedTabPeriod)))
    }
  }, [chooseStock.id, isShowTransaction, selectedTabPeriod])

  const renderPeriods = () => {
    return Object.values(optionsList).map(item => {
      return (
        <li
          key={item}
          className={s.period}
          data-name={item}
          role='tab'
          tabIndex={0}
          aria-controls={item}
          aria-label={`Price changes for period: ${item}`}
        >
          <label
            className={cn(
              s.radioButton,
              checkIsDisabled(getOptionId(optionsList, item), availabilityTime) ? s.disabledButton : ''
            )}
            htmlFor={item}
          >
            <input
              type='radio'
              data-lpignore='true'
              id={item}
              name='periodChartTab'
              value={item}
              disabled={checkIsDisabled(getOptionId(optionsList, item), availabilityTime)}
              checked={item === selectedTabPeriod}
              onChange={handleChangeTabPeriod}
              className={s.radioButtonInput}
            />
            <span className={s.radioButtonControl} />
            <span>{item}</span>
          </label>
        </li>
      )
    })
  }

  const renderPriceTabInfo = () => {
    if (isFetchTabChartData) {
      return (
        <div className={s.priceInfo}>
          <div className={s.chartContainer}>
            <Chart isDarkMode={isDarkMode} selectedTabPeriod={selectedTabPeriod} servers={servers} />
          </div>
          <div className={s.priceContainer}>
            <div className={s.infoPriceContainer}>
              <ul className={s.priceInfoList}>
                <li className={s.title}>Current Price:</li>
                <li className={s.title}>{selectedTabPeriod} change:</li>
                <li className={s.title}>{selectedTabPeriod} change:</li>
                <li className={s.title}>{selectedTabPeriod} start:</li>
              </ul>
              <ul className={s.priceInfoValueList}>
                <li className={s.value}>${currentPriceValue}</li>
                <li className={cn(s.value, gs[changeType])}>
                  <span className={cn(gs.icon, gs.tabIcon)} />${dollars}
                </li>
                <li className={cn(s.value, gs[changeType])}>
                  <span className={cn(gs.icon, gs.tabIcon)} />
                  {percents}%
                </li>
                <li className={s.value}>${startPrice}</li>
              </ul>
            </div>
            <div className={s.infoPriceContainer}>
              <ul className={s.priceInfoList}>
                <li className={s.title}>{selectedTabPeriod} end:</li>
                <li className={s.title}>{selectedTabPeriod} high:</li>
                <li className={s.title}>{selectedTabPeriod} low:</li>
              </ul>
              <ul className={s.priceInfoValueList}>
                <li className={s.value}>${endPrice}</li>
                <li className={cn(s.value, s.up)}>${limitaryValues.max.toFixed(2)}</li>
                <li className={cn(s.value, s.low)}>${limitaryValues.min.toFixed(2)}</li>
              </ul>
            </div>
          </div>
        </div>
      )
    }

    return (
      <div className={s.priceLoader}>
        <AnimationLoad />
      </div>
    )
  }

  return (
    <div className={s.priceDropdown}>
      <ul role='tablist' className={s.periods}>
        {renderPeriods()}
      </ul>
      {renderPriceTabInfo()}
    </div>
  )
}

export default withTheme(PriceTab)
