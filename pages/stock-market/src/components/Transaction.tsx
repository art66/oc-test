import React, { useEffect, useState } from 'react'
import cn from 'classnames'
import { toMoney } from '@torn/shared/utils'
import { useDispatch, useSelector } from 'react-redux'
import ITransactions from '../interfaces/ITransactions'
import s from './styles/Transaction.cssmodule.scss'
import gs from '../styles/global.cssmodule.scss'
import { getCurrentStockPrice, getIsTravelling, getMergeTransactionsIds, getTransactions } from '../selectors'
import { setMergeTransactionsIds, viewTransaction, sellTransaction } from '../actions'

interface IProps {
  transaction: ITransactions
}

const Transaction = (props: IProps) => {
  const dispatch = useDispatch()
  const currentPrice = useSelector(getCurrentStockPrice)
  const transactions = useSelector(getTransactions)
  const mergeTransactionsIds = useSelector(getMergeTransactionsIds)
  const isTraveling = useSelector(getIsTravelling)
  const { transaction } = props
  const currentDate = new Date()
  const currentTime = currentDate.getTime()
  const [isDisabledView, setIsDisabledView] = useState(currentTime - transaction?.timestamp * 1000 < 180000)

  useEffect(() => {
    setTimeout(() => setIsDisabledView(false), 180000 - (currentTime - transaction.timestamp * 1000))
  }, [isDisabledView, transaction])

  const getChangePrice = () => {
    const diffPrice = currentPrice - transaction.boughtPrice
    const percents = (diffPrice * 100) / transaction.boughtPrice
    const profit = Math.floor(transaction.amount * currentPrice - transaction.amount * transaction.boughtPrice)

    return {
      dollars: +diffPrice,
      percents: +percents,
      profit: +profit.toFixed(2)
    }
  }

  const getChangeIcon = () => {
    const { percents } = getChangePrice()
    let icon

    if (percents === 0) {
      icon = 'pause'
    } else if (percents > 0) {
      icon = 'up'
    } else {
      icon = 'down'
    }

    return icon
  }

  const changePrice = getChangePrice()
  const dollarsABS = Math.abs(changePrice.dollars).toFixed(2)
  const percentsABS = Math.abs(changePrice.percents).toFixed(2)
  const profitABS = Math.abs(changePrice.profit)
  const handleSellTransaction = () => dispatch(sellTransaction(transaction.id))
  const handleChooseMergeTransaction = () => dispatch(setMergeTransactionsIds(transaction.id))
  const handleViewTransaction = () => dispatch(viewTransaction(transaction.id))

  return (
    <ul className={cn(s.transaction, { [s.one]: transactions.length === 1, [s.more]: transactions.length > 1 })}>
      <li className={s.date}>{transaction.date}</li>
      <li className={s.shares}>{toMoney(transaction.amount, '')}</li>
      <li className={s.value}>{toMoney(Math.floor(transaction.amount * currentPrice), '$')}</li>
      <li className={s.bought}>{toMoney(transaction.boughtPrice.toFixed(2), '$')}</li>
      <li className={s.current}>{toMoney(currentPrice.toFixed(2), '$')}</li>
      <li className={s.change}>
        <span className={gs[getChangeIcon()]}>
          <span className={cn(gs.icon, gs.tabIcon)} />${dollarsABS} ({percentsABS}%)
        </span>
      </li>
      <li className={s.profit}>
        <span className={gs[getChangeIcon()]}>
          {`${changePrice.profit >= 0 ? '+' : '-'} ${toMoney(profitABS, '$')}`}
        </span>
      </li>
      <li className={s.view}>
        <button type='button' disabled={isDisabledView} className={s.viewButton} onClick={handleViewTransaction}>
          View
        </button>
      </li>
      <li className={s.sell}>
        <button type='button' disabled={isTraveling} className={s.sellButton} onClick={handleSellTransaction}>
          Sell
        </button>
      </li>
      <li className={s.merge}>
        <label className={s.checkboxButton}>
          <input
            type='checkbox'
            checked={mergeTransactionsIds.includes(transaction.id)}
            data-lpignore='true'
            className={s.checkboxButtonInput}
            onChange={handleChooseMergeTransaction}
          />
          <span className={s.checkboxButtonControl} />
        </label>
      </li>
    </ul>
  )
}

export default Transaction
