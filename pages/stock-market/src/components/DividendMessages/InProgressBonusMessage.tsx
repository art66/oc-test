import React from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { toMoney } from '@torn/shared/utils'
import IDividends from '../../interfaces/IDividends'
import { ACTIVE_TYPE, IN_PROGRESS_STATUS, INACTIVE_STATUS, PASSIVE_TYPE } from '../../constants'
import { getCurrentStockPrice } from '../../selectors'
import { getSharesIncrementRequirements, toCardinal, getArticleForStockDividend, addPlural } from '../../utils'
import s from '../styles/DividendTab.cssmodule.scss'

interface IProps {
  dividends: IDividends
  stockName: string
  acronym: string
}

const InProgressBonusMessage = ({ dividends, stockName, acronym }: IProps) => {
  const { progress, bonus, requirements, increment } = dividends
  const currentPrice = useSelector(getCurrentStockPrice)
  const sharesValueForIncrement =
    dividends.increment.possible >= 2 ?
      getSharesIncrementRequirements(dividends.increment.current, dividends.requirements.forOne) :
      requirements.forOne

  const renderInProgressMessage = () => {
    return dividends.type === ACTIVE_TYPE ? (
      <p className={s.greenText}>
        You will be able to collect {bonus.current} in {progress.total - progress.current}
        {` day${addPlural(progress.total - progress.current)}`}
      </p>
    ) : (
      <p className={s.blueText}>
        You will receive {getArticleForStockDividend(dividends.bonus.current)}
        {bonus.current} in {progress.total - progress.current} days
      </p>
    )
  }

  const renderNextIncrementMessage = () => {
    if (dividends.type === PASSIVE_TYPE && requirements.nextIncrement) {
      return (
        <p>
          Buy <span className={s.grayText}>{toMoney(requirements.nextIncrement, '')}</span> more shares for
          <span className={s.grayText}> {toMoney(Math.ceil(requirements.nextIncrement * currentPrice), '$')} </span>
          to unlock the <span className={s.grayText}>benefit</span>
        </p>
      )
    }

    return dividends.type === ACTIVE_TYPE && requirements.nextIncrement ? (
      <p>
        Buy <span className={s.grayText}>{toMoney(requirements.nextIncrement, '')}</span> more shares for
        <span className={s.grayText}> {toMoney(Math.ceil(requirements.nextIncrement * currentPrice), '$')} </span>
        to unlock the <span className={s.grayText}>{toCardinal(Math.floor(increment.possible) + 1)}</span> increment
        {dividends.progress.current > 1 ? ' for the next cycle' : ''}
      </p>
    ) : null
  }

  return (
    <div className={cn(s.message, s[acronym], dividends.status === INACTIVE_STATUS ? s.inactive : s[dividends.type])}>
      <p>
        <span className={s.grayText}>{stockName} </span>
        provides <span className={s.grayText}>{bonus.default}</span>
      </p>
      <p>
        {dividends.type === ACTIVE_TYPE ? 'Every' : 'After'} <span className={s.grayText}>{progress.total} days </span>
        when holding at least
        <span className={s.grayText}> {toMoney(sharesValueForIncrement, '')}</span> shares currently valued at
        <span className={s.grayText}>{toMoney(Math.ceil(sharesValueForIncrement * currentPrice), ' $')}</span>
      </p>
      {dividends.status === IN_PROGRESS_STATUS && renderInProgressMessage()}
      {renderNextIncrementMessage()}
    </div>
  )
}

export default InProgressBonusMessage
