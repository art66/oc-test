import React from 'react'
import cn from 'classnames'
import { useSelector } from 'react-redux'
import { getPlace } from '../../utils'
import s from '../styles/DividendTab.cssmodule.scss'
import { getIsHospital, getIsJail } from '../../selectors'

interface IProps {
  acronym: string
}

const BonusMessage = ({ acronym }: IProps) => {
  const isJail = useSelector(getIsJail)
  const isHospital = useSelector(getIsHospital)

  return (
    <div className={cn(s.message, s[acronym], s.active)}>
      <p>You cannot collect a dividend while {getPlace(isJail, isHospital)}</p>
    </div>
  )
}

export default BonusMessage
