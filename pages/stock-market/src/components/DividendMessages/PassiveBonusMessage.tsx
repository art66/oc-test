import React from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { toMoney } from '@torn/shared/utils'
import IDividends from '../../interfaces/IDividends'
import { getCurrentStockPrice } from '../../selectors'
import { getArticleForStockDividend } from '../../utils'
import s from '../styles/DividendTab.cssmodule.scss'

interface IProps {
  dividends: IDividends
  stockName: string
  acronym: string
}

const PassiveBonusMessage = ({ dividends, stockName, acronym }: IProps) => {
  const currentPrice = useSelector(getCurrentStockPrice)

  return (
    <div className={cn(s.message, s[acronym], s.passive)}>
      <p>
        <span className={s.grayText}>{stockName} </span>
        provides <span className={s.grayText}>{dividends.bonus.default}</span>
      </p>
      <p>
        After <span className={s.grayText}>{dividends.progress.total} days </span>
        when holding at least
        <span className={s.grayText}> {toMoney(dividends.requirements.forOne, '')}</span> shares currently valued at
        <span className={s.grayText}>{toMoney(Math.ceil(dividends.requirements.forOne * currentPrice), ' $')}</span>
      </p>
      <p className={s.blueText}>
        You currently have {getArticleForStockDividend(dividends.bonus.current)}{dividends.bonus.current}
      </p>
    </div>
  )
}

export default PassiveBonusMessage
