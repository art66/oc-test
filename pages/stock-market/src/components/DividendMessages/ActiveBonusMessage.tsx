import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import cn from 'classnames'
import IDividends from '../../interfaces/IDividends'
import { withdrawDividend } from '../../actions'
import { getIsOrAre } from '../../utils'
import s from '../styles/DividendTab.cssmodule.scss'

interface IProps {
  dividends: IDividends
  acronym: string
}

const ActiveBonusMessage = ({ acronym, dividends }: IProps) => {
  const dispatch = useDispatch()
  const [isDisabled, setIsDisabled] = useState(false)

  const handleCollect = () => {
    setIsDisabled(true)
    dispatch(withdrawDividend())
  }

  return (
    <div className={cn(s.message, s[acronym], s.active)}>
      <p className={s.collectInfoMessage}>
        {`Your ${dividends.bonus.current} ${getIsOrAre(dividends.bonus.current, acronym)} ready for collection`}
      </p>
      <button type='button' disabled={isDisabled} className='torn-btn green' onClick={handleCollect}>
        Collect
      </button>
    </div>
  )
}

export default ActiveBonusMessage
