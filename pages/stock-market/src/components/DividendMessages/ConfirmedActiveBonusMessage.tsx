import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { setIsCollected } from '../../actions'
import { getError, getWithdrawDividend } from '../../selectors'
import s from '../styles/DividendTab.cssmodule.scss'

interface IProps {
  acronym: string
}

const ConfirmedActiveBonusMessage = ({ acronym }: IProps) => {
  const dispatch = useDispatch()
  const withdrawBonus = useSelector(getWithdrawDividend)
  const error = useSelector(getError)
  const handleCancel = () => dispatch(setIsCollected(false))

  const renderMessage = () => {
    return error.withdrawDividend && error.withdrawDividend.length ?
      <p className={s.error}>{error.withdrawDividend}</p> :
      <p className={s.collectedInfoMessage}>You have collected your {withdrawBonus}</p>
  }

  return (
    <div className={cn(s.message, s[acronym], s.collected)}>
      {renderMessage()}
      <button type='button' className={cn('torn-btn', s.continueButton)} onClick={handleCancel}>
        Continue
      </button>
    </div>
  )
}

export default ConfirmedActiveBonusMessage
