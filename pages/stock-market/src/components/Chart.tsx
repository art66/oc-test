import React, { useEffect } from 'react'
import { GoogleCharts } from 'google-charts'
import OPTION_CHART_TAB from '../constants/tabChartOptions'
import OPTION_CHART_TAB_DARK from '../constants/tabChartOptionsDark'
import s from './styles/PriceTab.cssmodule.scss'
import { X_AXIS_FORMATTING, LAST_HOUR, LAST_WEEK, LAST_YEAR, LAST_DAY, LAST_MONTH, ALL_TIME } from '../constants'

interface IProps {
  servers: object
  selectedTabPeriod: string
  isDarkMode: boolean
}

const Chart = ({ isDarkMode, servers, selectedTabPeriod }: IProps) => {
  const chartPlaceholder = React.useRef(null)
  let chartInstance = null
  let chartLoaded = false

  const getOption = () => {
    const option = isDarkMode ? OPTION_CHART_TAB_DARK : OPTION_CHART_TAB
    let hAxisFormat = ''

    switch (selectedTabPeriod) {
      case LAST_HOUR:
        hAxisFormat = X_AXIS_FORMATTING[selectedTabPeriod]
        break
      case LAST_DAY:
        hAxisFormat = X_AXIS_FORMATTING[selectedTabPeriod]
        break
      case LAST_WEEK:
        hAxisFormat = X_AXIS_FORMATTING[selectedTabPeriod]
        break
      case LAST_MONTH:
        hAxisFormat = X_AXIS_FORMATTING[selectedTabPeriod]
        break
      case LAST_YEAR:
        hAxisFormat = X_AXIS_FORMATTING[selectedTabPeriod]
        break
      case ALL_TIME:
        hAxisFormat = X_AXIS_FORMATTING[selectedTabPeriod]
        break
      default:
        hAxisFormat = option.hAxis.format
        break
    }

    return {
      ...option,
      hAxis: {
        ...option.hAxis,
        format: hAxisFormat
      }
    }
  }

  const options = getOption()

  // eslint-disable-next-line max-statements
  const drawChart = () => {
    const data = new GoogleCharts.api.visualization.DataTable(servers)
    const requiredValuesInRow = Object.keys(servers).length

    let rows = []

    data.addColumn('datetime', 'time')

    Object.keys(servers).forEach((serverName, serverIndex) => {
      const server = servers[serverName]

      data.addColumn('number', serverName)
      Object.keys(server)
        .reverse()
        .forEach((recordTimestamp, recordIndex) => {
          const value = parseFloat(server[recordTimestamp])

          if (!rows[recordIndex]) {
            rows[recordIndex] = []
          }
          if (serverIndex === 0) {
            // @ts-ignore
            rows[recordIndex][0] = new Date(recordTimestamp * 1000)
          }
          rows[recordIndex][serverIndex + 1] = value
        })
    })

    rows = rows.reverse()

    if (rows.some(row => row.length < requiredValuesInRow)) {
      rows.forEach((row, index) => {
        if (row.length < requiredValuesInRow) {
          const lack = requiredValuesInRow - row.length

          rows[index] = rows[index].concat(new Array(lack).fill(0))
        }
      })
    }

    data.addRows(rows)

    const formatterPattern =
      selectedTabPeriod === LAST_HOUR || selectedTabPeriod === ALL_TIME ? 'YYYY, MMM d, HH:mm' : 'MMM d, HH:mm'
    const formatter = new GoogleCharts.api.visualization.DateFormat({ pattern: formatterPattern })
    const formatNumber = new GoogleCharts.api.visualization.NumberFormat({ pattern: '0.00' })

    formatter.format(data, 0)
    formatNumber.format(data, 1)

    if (chartInstance) {
      chartInstance.clearChart()
    } else {
      chartInstance = new GoogleCharts.api.visualization.AreaChart(chartPlaceholder.current)
    }

    chartInstance.draw(data, options)
  }

  const loadChartAndDraw = () => {
    try {
      if (chartLoaded) {
        drawChart()
      } else {
        chartLoaded = true
        GoogleCharts.load(drawChart)
      }
      // eslint-disable-next-line no-empty
    } catch (e) {}
  }

  loadChartAndDraw()

  useEffect(() => {
    window.addEventListener('resize', drawChart)

    return () => {
      window.removeEventListener('resize', drawChart)
    }
  }, [])

  return <div className={s.tabPriceChart} ref={chartPlaceholder} />
}

export default Chart
