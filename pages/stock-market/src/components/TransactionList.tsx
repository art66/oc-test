import React from 'react'
import cn from 'classnames'
import { useSelector } from 'react-redux'
import Transaction from './Transaction'
import s from './styles/TransactionList.cssmodule.scss'
import { getTransactions } from '../selectors'

const TransactionList = () => {
  const transactions = useSelector(getTransactions)
  const withScroll = transactions.length >= 3

  const renderTransactions = () => {
    const transactionList =
      transactions.length
      && transactions.map(item => {
        return <Transaction key={item?.id} transaction={item} />
      })

    return transactions.length === 1 ? (
      <div>{transactionList}</div>
    ) : (
      <div className={cn(s.categoriesValues, { [s.withScroll]: withScroll })}>
        <div className={cn(s.scrollArea)}>{transactionList}</div>
      </div>
    )
  }

  return (
    <div className={cn(s.transactionsList, { [s.one]: transactions.length === 1, [s.more]: transactions.length > 1 })}>
      <ul className={s.titles}>
        <li className={s.date}>Buy date</li>
        <li className={s.shares}>Shares</li>
        <li className={s.value}>Value</li>
        <li className={s.bought}>Bought</li>
        <li className={s.current}>Current</li>
        <li className={s.change}>Change</li>
        <li className={s.profit}>Profit</li>
        <li className={s.view}>View</li>
        <li className={s.sell}>Sell</li>
        <li className={s.merge}>Merge</li>
      </ul>
      {renderTransactions()}
    </div>
  )
}

export default TransactionList
