import React from 'react'
import cn from 'classnames'
import IStock from '../interfaces/IStock'
import IChooseStock from '../interfaces/IChooseStock'
import ProfileTab from './ProfileTab'
import PriceTab from './PriceTab'
import ManagerTab from './ManagerTab'
import DividendTab from './DividendTab'
import { DIVIDEND_TAB, PRICE_TAB, INFO_TAB, MANAGER_TAB } from '../constants'
import s from './styles/StockTabs.cssmodule.scss'

interface IProps {
  stock: IStock
  chooseStock: IChooseStock
}

const StockTabs = ({ stock, chooseStock }: IProps) => {
  const getDropdown = () => {
    switch (chooseStock.currentTab) {
      case INFO_TAB:
        return <ProfileTab profile={stock.profile} />
      case PRICE_TAB:
        return <PriceTab sharesPrice={stock.sharesPrice} />
      case MANAGER_TAB:
        return <ManagerTab />
      case DIVIDEND_TAB:
        return <DividendTab profile={stock.profile} dividends={stock.dividends} />
      default:
        return null
    }
  }

  return (
    <div
      className={cn(s.stockDropdown, s[chooseStock.currentTab])}
      role='tabpanel'
      id={`panel-${chooseStock.currentTab}`}
      tabIndex={0}
      aria-labelledby={chooseStock.currentTab}
    >
      {getDropdown()}
    </div>
  )
}

export default StockTabs
