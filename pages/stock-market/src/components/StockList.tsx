import React from 'react'
import Stock from './Stock'
import IStock from '../interfaces/IStock'
import s from './styles/StockList.cssmodule.scss'

interface IProps {
  stocks: IStock[]
}

const StockList = ({ stocks }: IProps) => {
  const renderStocks = () => stocks && stocks.map((item) => <Stock key={item.id} stock={item} />)

  return <div className={s.stockMarket}>{renderStocks()}</div>
}

export default StockList
