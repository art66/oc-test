import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AnimationLoad from '@torn/shared/components/AnimationLoad'
import InfoBox from '@torn/shared/components/InfoBox'
import AppHeader from '@torn/shared/components/AppHeader'
import Title from './Title'
import StockList from './StockList'
import { fetchStockMarketData } from '../actions'
import { getErrorApp, getStocks } from '../selectors'
import APP_HEADER_DATA from '../constants/appHeader'
import './styles/DM_vars.scss'
import './styles/vars.scss'

const App = () => {
  const dispatch = useDispatch()
  const stocks = useSelector(getStocks)
  const error = useSelector(getErrorApp)

  useEffect(() => {
    dispatch(fetchStockMarketData())
  }, [])

  const renderStockMarket = () => {
    if (stocks && stocks.length) {
      return (
        <>
          <Title />
          <StockList stocks={stocks} />
        </>
      )
    }

    return <AnimationLoad />
  }

  const renderError = () => <InfoBox msg={error} color='red' />

  return error && error.length ? (
    renderError()
  ) : (
    <>
      <AppHeader clientProps={APP_HEADER_DATA} appID='0' />
      {renderStockMarket()}
    </>
  )
}

export default App
