import React from 'react'
import { useSelector } from 'react-redux'
import ActionDialog from './DialogSteps/ActionDialog'
import ConfirmDialog from './DialogSteps/ConfirmDialog'
import ConfirmedDialog from './DialogSteps/ConfirmedDialog'
import MessageDialog from './DialogSteps/MessageDialog'
import { getIsHospital, getIsJail, getIsTravelling } from '../selectors'
import { CONFIRM_DIALOG_ID, CONFIRMED_DIALOG_ID, BUY_DIALOG } from '../constants'

interface IProps {
  type: string
  stepBuyDialog?: number
  stepSellDialog?: number
}

const Dialog = ({ type, stepBuyDialog, stepSellDialog }: IProps) => {
  const stepDialog = type === BUY_DIALOG ? stepBuyDialog : stepSellDialog
  const isTraveling = useSelector(getIsTravelling)
  const isJail = useSelector(getIsJail)
  const isHospital = useSelector(getIsHospital)

  const renderDialogStep = () => {
    switch (stepDialog) {
      case CONFIRM_DIALOG_ID:
        return <ConfirmDialog type={type} />
      case CONFIRMED_DIALOG_ID:
        return <ConfirmedDialog type={type} />
      default:
        return <ActionDialog type={type} />
    }
  }

  return isTraveling || isJail || isHospital ? <MessageDialog type={type} /> : renderDialogStep()
}

export default Dialog
