import React, { useEffect, useRef } from 'react'
import cn from 'classnames'
import s from './styles/ProgressBar.cssmodule.scss'

interface IProps {
  totalSegments: number
  filledSegments: number
  filledPartSegments?: number
  offset: number
  radius: number
  type: string
}

const ProgressBar = ({ totalSegments, filledSegments, filledPartSegments, offset, radius, type }: IProps) => {
  const svgCircle = useRef(null)

  const getProgressBarFilledClass = (i: number): string => {
    let styles = filledPartSegments >= i && filledSegments <= 1 ? 'filled filledPartSegments' : 'empty'

    if (filledSegments >= i) {
      styles = `filled ${type}`
    }

    if (!Number.isInteger(filledPartSegments) && totalSegments <= 10 && i === Math.ceil(filledPartSegments)) {
      styles = 'filled filledPartSegments'
    }

    if (
      !Number.isInteger(filledPartSegments)
      && totalSegments <= 10
      && i <= Math.floor(filledPartSegments)
      && i > filledSegments
    ) {
      styles = 'filled filledPartSegments'
    }

    return styles
  }

  const getSegment = () => {
    const partFilled = Math.round((filledPartSegments % 1) * 100)
    const filledSeg = (360 * partFilled) / 100

    const segment = [
      <circle key={radius} cx='160' cy='160' r={radius} className={filledSegments === 1 ? `filled ${type}` : 'empty'} />
    ]

    if (filledPartSegments) {
      segment.push(<path d={describeArc(160, 160, 0, filledSeg)} className={getProgressBarFilledClass(1)} />)
    }

    return segment
  }

  // eslint-disable-next-line max-statements
  const getNSegments = () => {
    const len = offset === 0 ? 360 / totalSegments : (360 - offset * totalSegments) / totalSegments
    let prevStartAngle = 0
    let prevEndAngle = 0
    const segment = []

    for (let i = 1; i <= totalSegments; i++) {
      prevStartAngle = prevEndAngle + offset
      prevEndAngle = len * i + offset * i
      const path = describeArc(160, 160, prevStartAngle, prevEndAngle)

      if (
        totalSegments <= 10
        && !Number.isInteger(filledPartSegments)
        && filledPartSegments
        && i === Math.ceil(filledPartSegments)
      ) {
        let filledAngle = prevEndAngle - (100 - Math.round((filledPartSegments % 1) * 100))

        if (Math.round((filledPartSegments % 1) * 100) < 50) {
          filledAngle = prevStartAngle + Math.round((filledPartSegments % 1) * 100)
        }

        if (filledAngle > prevEndAngle) {
          filledAngle = prevEndAngle
        }

        segment.push(<path className='empty' d={path} />)
        segment.push(
          <path
            className={getProgressBarFilledClass(i)}
            d={describeArc(160, 160, prevStartAngle, Math.round(filledAngle))}
          />
        )
      } else {
        segment.push(<path className={getProgressBarFilledClass(i)} d={path} />)
      }
    }

    return segment
  }

  const polarToCartesian = (centerX: number, centerY: number, angleInDegrees: number): { x: number; y: number } => {
    const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0

    return {
      x: centerX + radius * Math.cos(angleInRadians),
      y: centerY + radius * Math.sin(angleInRadians)
    }
  }

  const describeArc = (x: number, y: number, startAngle: number, endAngle: number): string => {
    const start = polarToCartesian(x, y, endAngle)
    const end = polarToCartesian(x, y, startAngle)
    const arcSweep = endAngle - startAngle <= 180 ? 0 : 1

    return ['M', start.x, start.y, 'A', radius, radius, 0, arcSweep, 0, end.x, end.y].join(' ')
  }

  const draw = () => (totalSegments <= 1 ? getSegment() : getNSegments())

  useEffect(() => {
    draw()
  }, [totalSegments, filledSegments, filledPartSegments])

  return (
    <div className={cn(s.svgContainer, s[type])}>
      <svg
        id='circle'
        className={s.svgProgress}
        viewBox='0 0 320 320'
        version='1.1'
        xmlns='http://www.w3.org/2000/svg'
        ref={svgCircle}
      >
        {draw()}
      </svg>
    </div>
  )
}

export default ProgressBar
