import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toMoney } from '@torn/shared/utils'
import { buyShares, sellShares, backClick, setIsPriceUpdated } from '../../actions'
import {
  getCurrentStockPrice,
  getSharesValueToBuy,
  getSharesValueToSell,
  getTransactionsFee,
  getIsPriceUpdated,
  getStockDividends,
  getMaxSharesValueForSell,
  getMaxSharesValueForBuy
} from '../../selectors'
import {
  ACTIVE_TYPE,
  IN_PROGRESS_STATUS,
  PASSIVE_TYPE,
  READY_STATUS,
  BUY_DIALOG,
  PERCENTS_MULTIPLIER
} from '../../constants'
import { getBuySellDialogsValues, getSharesIncrementRequirements } from '../../utils'
import s from '../styles/Dialog.cssmodule.scss'

interface IProps {
  type: string
}

const ConfirmDialog = ({ type }: IProps) => {
  const dispatch = useDispatch()
  const currentPrice = useSelector(getCurrentStockPrice)
  const sharesValueToSell = useSelector(getSharesValueToSell)
  const sharesValueToBuy = useSelector(getSharesValueToBuy)
  const sellSharesMaxValue = useSelector(getMaxSharesValueForSell)
  const buySharesMaxValue = useSelector(getMaxSharesValueForBuy)
  const fee = useSelector(getTransactionsFee)
  const isPriceUpdated = useSelector(getIsPriceUpdated)
  const dividends = useSelector(getStockDividends)
  const isBuy = type === BUY_DIALOG
  const [isDisabledButton, setIsDisabledButton] = useState(false)
  const dialogValues = getBuySellDialogsValues({
    currentPrice,
    sharesValueToSell,
    sharesValueToBuy,
    fee,
    sellSharesMaxValue,
    buySharesMaxValue,
    isBuy
  })

  const handleClick = () => {
    setIsDisabledButton(true)
    isBuy ? dispatch(buyShares()) : dispatch(sellShares())
  }

  const handleBack = () => dispatch(backClick(type))

  const currentDate = new Date()

  useEffect(() => {
    const timer = setTimeout(() => dispatch(setIsPriceUpdated(true)), (57 - currentDate.getSeconds()) * 1000)

    return () => clearTimeout(timer)
  }, [currentPrice])

  const getBuySellConfirmMessage = () => {
    if (isBuy) {
      return (
        <p>
          For a total of <strong>{dialogValues.formattedCoastWithFee}</strong>
        </p>
      )
    }

    return (
      <p>
        For a total of <strong>{dialogValues.formattedCoastWithFee}</strong> after the
        <strong> {fee * PERCENTS_MULTIPLIER}%</strong> fee of
        <strong> {dialogValues.formattedDiff}</strong>
      </p>
    )
  }

  const renderActionsButtons = () => {
    return (
      <>
        <button type='button' disabled={isDisabledButton} className='torn-btn gray' onClick={handleBack}>
          Back
        </button>
        <button type='button' disabled={isDisabledButton} className={`torn-btn gray ${s[type]}`} onClick={handleClick}>
          Confirm Transaction
        </button>
      </>
    )
  }

  const renderPriceUpdatingButton = () => (
    <button type='button' className='torn-btn gray disabled' onClick={handleBack}>
      Price updating...
    </button>
  )

  const renderDividendInfo = () => {
    if (dividends.type === PASSIVE_TYPE && dividends.progress.current > 0) {
      if (
        dividends.status === READY_STATUS
        && dialogValues.maxValue - dialogValues.amountShares < dividends.requirements.forOne
      ) {
        return 'The active benefit this stock is providing will be lost'
      } else if (
        dividends.status === IN_PROGRESS_STATUS
        && dialogValues.maxValue - dialogValues.amountShares < dividends.requirements.forOne
      ) {
        return 'The progress towards this stock\'s benefit will be lost'
      }
    }

    const conditionForLoseAllProgress =
      dialogValues.amountShares === dialogValues.maxValue
      || (dialogValues.maxValue - dialogValues.amountShares
        < getSharesIncrementRequirements(dividends.increment.current, dividends.requirements.forOne)
        && dividends.increment.current === 1)
    const conditionForLoseIncrement =
      dialogValues.maxValue - dialogValues.amountShares
        < getSharesIncrementRequirements(dividends.increment.current, dividends.requirements.forOne)
      && dividends.increment.current > 1

    if (dividends.type === ACTIVE_TYPE && dividends.progress.current > 0) {
      if (dividends.status === IN_PROGRESS_STATUS) {
        if (conditionForLoseAllProgress) {
          return 'All progress towards this stock\'s dividend payout will be lost'
        }

        if (conditionForLoseIncrement) {
          return 'At least one increment of this stock\'s dividend payout will be lost'
        }
      }
    }

    return null
  }

  return (
    <div className={s.manageBlock}>
      <div className={s.message}>
        <p>
          Do you want to {type} <strong>{dialogValues.formattedAmountShares}</strong> shares at
          <strong> {toMoney(currentPrice.toFixed(2), '$')}</strong> each?
        </p>
        {getBuySellConfirmMessage()}
      </div>
      {!isBuy && <p className={s.dividendsDialogMsg}>{renderDividendInfo()}</p>}
      <div className={s.actions}>{isPriceUpdated ? renderPriceUpdatingButton() : renderActionsButtons()}</div>
    </div>
  )
}

export default ConfirmDialog
