import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { toMoney } from '@torn/shared/utils'
import MoneyInput from '@torn/shared/components/MoneyInput'
import { getBuySellDialogsValues } from '../../utils'
import {
  getChooseStockId,
  getCurrentStockPrice,
  getMaxSharesValueForBuy,
  getMaxSharesValueForSell,
  getSharesValueToBuy,
  getSharesValueToSell,
  getStepBuyDialog,
  getStepSellDialog,
  getTransactionsFee
} from '../../selectors'
import { nextClick, setSharesValueToBuy, setSharesValueToSell } from '../../actions'
import { BUY_DIALOG } from '../../constants'
import s from '../styles/Dialog.cssmodule.scss'

interface IProps {
  type: string
}

const ActionDialog = (props: IProps) => {
  const { type } = props
  const dispatch = useDispatch()
  const currentPrice = useSelector(getCurrentStockPrice)
  const sharesValueToSell = useSelector(getSharesValueToSell)
  const sharesValueToBuy = useSelector(getSharesValueToBuy)
  const fee = useSelector(getTransactionsFee)
  const stepBuyDialog = useSelector(getStepBuyDialog)
  const stepSellDialog = useSelector(getStepSellDialog)
  const sellSharesMaxValue = useSelector(getMaxSharesValueForSell)
  const buySharesMaxValue = useSelector(getMaxSharesValueForBuy)
  const isBuy = type === BUY_DIALOG
  const dialogValues = getBuySellDialogsValues({
    currentPrice,
    sharesValueToSell,
    sharesValueToBuy,
    fee,
    stepBuyDialog,
    stepSellDialog,
    sellSharesMaxValue,
    buySharesMaxValue,
    isBuy
  })
  const chooseStockId = useSelector(getChooseStockId)

  const handleClick = () => {
    dispatch(nextClick(props.type))
    isBuy ?
      dispatch(setSharesValueToBuy(chooseStockId, Math.floor(sharesValueToBuy))) :
      dispatch(setSharesValueToSell(chooseStockId, Math.floor(sharesValueToSell)))
  }

  const handleOnChange = e =>
    isBuy ? dispatch(setSharesValueToBuy(chooseStockId, e)) : dispatch(setSharesValueToSell(chooseStockId, e))

  const renderBuySellDefaultMessage = () => {
    if (isBuy) {
      return (
        <p>
          <strong>{dialogValues.formattedCoastWithFee}</strong> will buy you
          <strong> {dialogValues.formattedAmountShares}</strong> shares
        </p>
      )
    }

    return (
      <p>
        Your <strong>{dialogValues.formattedAmountShares}</strong> shares are worth
        <strong> {dialogValues.formattedTotalCoast}</strong>
      </p>
    )
  }

  const renderMessage = () => {
    return !dialogValues.stepDialog ? (
      <>
        <p>How many shares would you like to {props.type}?</p>
        {renderBuySellDefaultMessage()}
      </>
    ) : (
      <>
        <p>
          {isBuy ? 'Buying' : 'Selling'} <strong>{dialogValues.formattedAmountShares}</strong> shares at
          <strong> {toMoney(currentPrice.toFixed(2), '$')}</strong>
        </p>
        <p>
          {isBuy ? (
            <>
              For a total of <strong>{dialogValues.formattedCoastWithFee}</strong>
            </>
          ) : (
            <>
              <strong>{dialogValues.formattedCoastWithFee}</strong> after
              <strong> {dialogValues.formattedDiff}</strong> fee
            </>
          )}
        </p>
      </>
    )
  }

  return (
    <div className={s.manageBlock}>
      <div className={s.message}>{renderMessage()}</div>
      <div className={cn(s.actions, s.withInput)}>
        <MoneyInput
          userMoney={dialogValues.maxValue}
          onChange={handleOnChange}
          value={`${dialogValues.amountShares}`}
          disableReadOnly={!!isBuy}
          onClick={handleClick}
          customStyles={{ signButton: s.singButton }}
          disableOnMountInputFocus={true}
        />
        <button
          type='button'
          className={`torn-btn gray ${s[props.type]}`}
          disabled={isNaN(dialogValues.amountShares) || !dialogValues.amountShares}
          onClick={handleClick}
        >
          {props.type}
        </button>
      </div>
    </div>
  )
}

export default ActionDialog
