import React from 'react'
import { useSelector } from 'react-redux'
import { getPlace } from '../../utils'
import s from '../styles/Dialog.cssmodule.scss'
import { getIsHospital, getIsJail } from '../../selectors'
import { BUY_DIALOG, SELL_DIALOG } from '../../constants'

interface IProps {
  type: string
}

const MessageDialog = ({ type }: IProps) => {
  const isJail = useSelector(getIsJail)
  const isHospital = useSelector(getIsHospital)

  return (
    <div className={s.manageBlock}>
      <div>
        {`You cannot ${type === BUY_DIALOG ? BUY_DIALOG : SELL_DIALOG} shares while ${getPlace(isJail, isHospital)}`}
      </div>
    </div>
  )
}

export default MessageDialog
