import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toMoney } from '@torn/shared/utils'
import {
  getBuyPrice,
  getChooseStockId,
  getError,
  getPrevSharesValueToBuy,
  getPrevSharesValueToSell,
  getSellPrice,
  getSharesAmount,
  getTransactionsFee
} from '../../selectors'
import { refreshDialogBuy, refreshDialogSell, setSharesValueToSell } from '../../actions'
import { getBuySellDialogsValues } from '../../utils'
import { BUY_DIALOG, PERCENTS_MULTIPLIER } from '../../constants'
import s from '../styles/Dialog.cssmodule.scss'

interface IProps {
  type: string
}

const ConfirmedDialog = (props: IProps) => {
  const dispatch = useDispatch()
  const { type } = props
  const errors = useSelector(getError)
  const buyPrice = useSelector(getBuyPrice)
  const sellPrice = useSelector(getSellPrice)
  const sharesAmount = useSelector(getSharesAmount)
  const sharesValueToSell = useSelector(getPrevSharesValueToSell)
  const sharesValueToBuy = useSelector(getPrevSharesValueToBuy)
  const fee = useSelector(getTransactionsFee)
  const isBuy = type === BUY_DIALOG
  const currentPrice = isBuy ? buyPrice : sellPrice
  const error = isBuy ? errors.buy : errors.sell
  const dialogValues = getBuySellDialogsValues({
    currentPrice,
    sharesValueToSell,
    sharesValueToBuy,
    fee,
    isBuy
  })
  const chooseStockId = useSelector(getChooseStockId)

  const actionName = isBuy ? 'bought' : 'sold'
  const transactionInfo = isBuy ?
    `${dialogValues.formattedCoastWithFee}` :
    `${dialogValues.formattedCoastWithFee} after the ${fee * PERCENTS_MULTIPLIER}% fee of ${
      dialogValues.formattedDiff
    }`

  const renderMessage = () => {
    return error ? (
      <p className={s.error}>{error}</p>
    ) : (
      <>
        <p>
          You have {actionName} <strong>{dialogValues.formattedAmountShares}</strong> shares at
          <strong> {toMoney(currentPrice.toFixed(2), '$')}</strong> each
        </p>
        <p>For a total of {transactionInfo}</p>
      </>
    )
  }

  const handleRefreshDialog = () => {
    !isBuy && dispatch(setSharesValueToSell(chooseStockId, sharesAmount))
    isBuy ? dispatch(refreshDialogBuy()) : dispatch(refreshDialogSell())
  }

  return (
    <div className={s.manageBlock}>
      <div className={s.message}>{renderMessage()}</div>
      <div className={s.actions}>
        <button type='button' className='torn-btn gray' onClick={handleRefreshDialog}>
          Back
        </button>
      </div>
    </div>
  )
}

export default ConfirmedDialog
