import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import TransactionList from './TransactionList'
import Dialog from './Dialog'
import s from './styles/ManagerTab.cssmodule.scss'
import { getMoreTransactions, mergeTransactions, refreshDialogBuy, refreshDialogSell } from '../actions'
import {
  getMediaType,
  getMergeTransactionsIds,
  getStepBuyDialog,
  getStepSellDialog,
  getTransactionMore,
  getTransactions
} from '../selectors'
import {
  DESKTOP_SCREEN,
  TRANSACTION_LOAD_LIMIT,
  MOBILE_SCREEN,
  BUY_DIALOG,
  SELL_DIALOG,
  DARK_THEME
} from '../constants'

const ManagerTab = ({ theme }: TWithThemeInjectedProps) => {
  const transactions = useSelector(getTransactions)
  const moreTransactions = useSelector(getTransactionMore)
  const dispatch = useDispatch()
  const currentTransactions = useRef(null)
  const actionsBlock = useRef(null)
  const [isMerge, setMerge] = useState(false)
  const [height, setHeight] = useState(0)
  const mergeTransactionsIds = useSelector(getMergeTransactionsIds)
  const stepBuyDialog = useSelector(getStepBuyDialog)
  const stepSellDialog = useSelector(getStepSellDialog)
  const mediaType = useSelector(getMediaType)
  const isDarkMode = theme === DARK_THEME

  useEffect(() => {
    const resizeListener = () => {
      let actionsBlockHeight = 0

      actionsBlockHeight =
        transactions.length && transactions.length > 1 && actionsBlock ?
          actionsBlock.current.offsetHeight :
          actionsBlockHeight

      currentTransactions ? setHeight(currentTransactions.current.offsetHeight + actionsBlockHeight) : setHeight(0)
    }

    resizeListener()
    window.addEventListener('resize', resizeListener)

    return () => {
      window.removeEventListener('resize', resizeListener)
    }
  }, [currentTransactions, actionsBlock, transactions])

  useEffect(() => {
    const visibilityChangeHandler = () => {
      if (document.visibilityState === 'hidden' && (stepBuyDialog === 2 || stepSellDialog === 2)) {
        dispatch(refreshDialogSell())
        dispatch(refreshDialogBuy())
      }
    }

    window.addEventListener('visibilitychange', visibilityChangeHandler)

    return () => {
      window.removeEventListener('visibilitychange', visibilityChangeHandler)
    }
  }, [stepBuyDialog, stepSellDialog])

  const handleMerge = () => setMerge(true)
  const handleCancel = () => setMerge(false)
  const handleGetMoreTransactions = () => dispatch(getMoreTransactions(TRANSACTION_LOAD_LIMIT))
  const handleMergeTransactions = () => {
    dispatch(mergeTransactions())
    setMerge(false)
  }

  const renderMergeDialog = () => {
    return (
      isMerge && (
        <>
          <p>Are you sure you&apos;d like to merge the selected into one?</p>
          <button type='button' onClick={handleMergeTransactions}>
            Yes
          </button>
          <button type='button' onClick={handleCancel}>
            Cancel
          </button>
        </>
      )
    )
  }

  const renderMergeBlock = () => {
    if (mergeTransactionsIds.length >= 2) {
      return (
        <li className={cn(s.mergeBlock, moreTransactions ? s.withShow : '')}>
          {!isMerge && (
            <button type='button' onClick={handleMerge}>
              Merge
            </button>
          )}
          {renderMergeDialog()}
        </li>
      )
    }

    return null
  }

  // eslint-disable-next-line complexity
  const getManageBlockStyles = () => {
    const tabletHeight = transactions.length > 1 ? 572 : 360
    const mobileHeight = transactions.length > 1 ? 592 : 350
    const tabletMobileHeight = mediaType === MOBILE_SCREEN ? mobileHeight : tabletHeight
    const minHeightDialog = mediaType === MOBILE_SCREEN ? 220 : 200
    const borderStyles = isDarkMode ? '1px solid #222' : '1px solid #ddd'
    const boxShadow = transactions.length === 0 && isDarkMode ? '0px 1px 0px #FFFFFF1A' : 'unset'
    let containerHeight = mediaType === DESKTOP_SCREEN ? 210 : tabletMobileHeight
    let minContainerHeight =
      containerHeight - height < minHeightDialog && mediaType !== DESKTOP_SCREEN ?
        minHeightDialog :
        containerHeight - height

    minContainerHeight = containerHeight - height < 100 && mediaType === DESKTOP_SCREEN ? 100 : minContainerHeight

    minContainerHeight = transactions.length === 0 ? minContainerHeight : minContainerHeight - 1
    containerHeight = transactions.length === 0 ? containerHeight : containerHeight - 1

    return {
      height: `${currentTransactions ? minContainerHeight : containerHeight}px`,
      borderBottom: transactions.length === 0 ? 'none' : borderStyles,
      boxShadow
    }
  }

  const renderShowMoreTransactions = () => {
    const showMoreText =
      moreTransactions <= TRANSACTION_LOAD_LIMIT ?
        `Show ${moreTransactions} more` :
        `Show ${TRANSACTION_LOAD_LIMIT} more of ${moreTransactions}`

    return moreTransactions ? (
      <button type='button' onClick={handleGetMoreTransactions}>
        {showMoreText}
      </button>
    ) : null
  }

  const renderShowMoreBlock = () => {
    return <li className={cn(s.showMore, isMerge ? s.isMerge : '')}>{renderShowMoreTransactions()}</li>
  }

  const renderTransactionsList = () => {
    return (
      <>
        <div className={cn(s.transactionsContainer, { [s.one]: transactions.length === 1 })} ref={currentTransactions}>
          <div className={cn({ [s.scrollAreaContainer]: transactions.length === 1 })}>
            {transactions.length ? <TransactionList /> : null}
          </div>
        </div>
        {transactions.length && transactions.length > 1 ? (
          <ul className={s.actions} ref={actionsBlock}>
            {renderShowMoreBlock()}
            {renderMergeBlock()}
          </ul>
        ) : null}
      </>
    )
  }

  return (
    <div className={s.manageTab}>
      <div className={s.manageContainer} style={getManageBlockStyles()}>
        <div className={s.buyBlock}>
          <Dialog type={BUY_DIALOG} stepBuyDialog={stepBuyDialog} />
        </div>
        <div className={s.sellBlock}>
          <Dialog type={SELL_DIALOG} stepSellDialog={stepSellDialog} />
        </div>
      </div>
      {renderTransactionsList()}
    </div>
  )
}

export default withTheme(ManagerTab)
