import React from 'react'
import { useSelector } from 'react-redux'
import Tooltip from '@torn/shared/components/Tooltip'
import { MOBILE_SCREEN, PASSIVE_TYPE, READY_STATUS } from '../constants'
import { getMediaType } from '../selectors'
import IDividends from '../interfaces/IDividends'

interface IProps {
  isShowTooltip: boolean
  acronym: string
  dividends: IDividends
}

const DividendTooltip = ({ isShowTooltip, acronym, dividends }: IProps) => {
  const mediaType = useSelector(getMediaType)
  const tooltipStyles = {
    style: {
      boxShadow: '0 0 2px #00000073',
      background: 'var(--tooltip-bg-color)'
    },
    arrowStyle: {
      borderTopColor: 'var(--tooltip-bg-color)',
      filter: 'var(--white-tooltip-arrow-filter)',
      top: '-1px'
    }
  }

  return (
    isShowTooltip && (
      <Tooltip
        style={tooltipStyles}
        position='top'
        arrow={mediaType === MOBILE_SCREEN ? 'right' : 'center'}
        parent={`dividendStatus${acronym}`}
      >
        <p className={`statusDividend ${dividends.type} ${dividends.status}`}>
          {dividends.type === PASSIVE_TYPE && dividends.status === READY_STATUS ? 'Active' : dividends.status}
        </p>
        <p className='incrementDividend'>Increment: {dividends.increment.current}</p>
        <p className='progressDividend'>
          Progress: {dividends.progress.current} / {dividends.progress.total} days
        </p>
      </Tooltip>
    )
  )
}

export default DividendTooltip
