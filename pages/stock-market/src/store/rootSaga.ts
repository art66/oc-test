import { all } from 'redux-saga/effects'
import stockMarket from '../sagas'

export default function* rootSaga() {
  yield all([stockMarket()])
}
