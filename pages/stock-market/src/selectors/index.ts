import { createSelector } from 'reselect'
import IStore from '../interfaces/IStore'
import { getOptionId } from '../utils'

export const getHeaderOptionsList = (state: IStore) => state.stockMarket.settings.charts.optionsList.header
export const getTabOptionsList = (state: IStore) => state.stockMarket.settings.charts.optionsList.dropdown
export const getAvailabilityTimePeriods = (state: IStore) =>
  state.stockMarket.settings.charts.optionsList.availabilityTime
export const getSelectedHeaderPeriod = (state: IStore) => state.stockMarket.settings.charts.selectedHeaderOption
export const getStocks = (state: IStore) => state.stockMarket.stocks
export const getErrorApp = (state: IStore) => state.stockMarket.errorApp
export const getTransactionsFee = (state: IStore) => state.stockMarket.settings.transactionsFee
export const getIsTravelling = (state: IStore) => state.stockMarket.settings.isTravelling
export const getIsJail = (state: IStore) => state.stockMarket.settings.isInJail
export const getIsHospital = (state: IStore) => state.stockMarket.settings.isInHospital
export const getMediaType = (state: IStore) => state.browser.mediaType
export const getUserMoney = (state: IStore) => state.stockMarket.userMoney
export const getIsPriceUpdated = (state: IStore) => state.stockMarket.isPriceUpdated
export const getWithdrawDividend = (state: IStore) => state.stockMarket.withdrawDividends
export const getSortedBy = (state: IStore) => state.stockMarket.sortedBy

export const getChooseStockInfo = (state: IStore) => state.stockMarket.chooseStock
export const getChooseStockId = (state: IStore) => state.stockMarket.chooseStock?.id
export const getChooseStockCurrentTab = (state: IStore) => state.stockMarket.chooseStock?.currentTab

export const getChooseStock = createSelector([getChooseStockId, getStocks], (chooseStockId, stocks) =>
  stocks.find(stock => stock.id === chooseStockId))
export const getHeaderChartDataForPeriod = createSelector([getChooseStock], stock => stock.sharesPrice.chartData)
export const getTabChartDataForPeriod = createSelector([getChooseStock], stock => stock.sharesPrice.chartDataTab)
export const getTransactionsChartData = createSelector([getChooseStock], stock => stock.sharesPrice.transactionsChart)
export const getSelectedTabPeriod = createSelector([getChooseStock], chooseStock => chooseStock.selectedTabPeriod)
export const getCurrentStockPrice = createSelector(
  [getHeaderChartDataForPeriod],
  chartData => chartData[chartData.length - 1].value
)
export const getSharesAmount = createSelector([getChooseStock], stock => stock.userOwned.sharesAmount)
export const getBuyPrice = createSelector([getChooseStock], stock => stock.buyPrice)
export const getSellPrice = createSelector([getChooseStock], stock => stock.sellPrice)
export const getError = createSelector([getChooseStock], stock => stock.error)
export const getHeaderPeriodId = createSelector(
  [getSelectedHeaderPeriod, getHeaderOptionsList],
  (period, optionsList) => getOptionId(optionsList, period)
)
export const getTabPeriodId = createSelector([getSelectedTabPeriod, getTabOptionsList], (period, optionsList) =>
  getOptionId(optionsList, period))
export const getMaxSharesValueForBuy = createSelector([getCurrentStockPrice, getUserMoney], (currentPrice, userMoney) =>
  Math.floor(userMoney / currentPrice))
export const getMaxSharesValueForSell = createSelector([getSharesAmount], sharesAmount => sharesAmount)
export const getTransactions = createSelector([getChooseStock], stock => stock.userOwned.transactions)
export const getTransactionMore = createSelector(
  [getChooseStock, getTransactions],
  (stock, transactions) => stock.userOwned.transactionsAmount - transactions.length
)
export const getTransactionOffset = createSelector(
  [getChooseStock, getTransactionMore],
  (stock, moreTransactions) => stock.userOwned.transactionsAmount - moreTransactions
)
export const getStepBuyDialog = createSelector([getChooseStock], stock => stock.stepBuyDialog)
export const getStepSellDialog = createSelector([getChooseStock], stock => stock.stepSellDialog)
export const getSharesValueToBuy = createSelector([getChooseStock], stock => stock.sharesValueToBuy)
export const getSharesValueToSell = createSelector([getChooseStock], stock => stock.sharesValueToSell)
export const getPrevSharesValueToBuy = createSelector([getChooseStock], stock => stock.prevSharesValueToBuy)
export const getPrevSharesValueToSell = createSelector([getChooseStock], stock => stock.prevSharesValueToSell)
export const getMergeTransactionsIds = createSelector([getChooseStock], stock => stock.mergeTransactionsIds)
export const getActiveTransaction = createSelector([getChooseStock], stock => stock.activeTransaction)
export const getIsShowTransaction = createSelector([getChooseStock], stock => stock.isShowTransaction)
export const getChartTransaction = createSelector([getChooseStock], stock => stock.transactionChart)
export const getStockDividends = createSelector([getChooseStock], stock => stock.dividends)
export const getIsCollected = createSelector([getChooseStock], stock => stock.isCollected)
export const getIsFetchTabChartData = createSelector([getChooseStock], stock => stock.isFetchTabChartData)
export const getLimitaryValuesChartData = createSelector([getChooseStock], stock => stock.sharesPrice.limitaryValues)
