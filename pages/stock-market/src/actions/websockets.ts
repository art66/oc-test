import { Dispatch } from 'react'
import { ActionType } from 'typesafe-actions'
import * as Actions from './index'
import IWindow from '../interfaces/IWindow'
import {
  ICreateTransactionWS,
  IUpdateTransactionWS,
  IRemoveTransactionsWS,
  IUpdateMoneyWS,
  IUpdateUserOwnedWS,
  IRefreshStocksDividendsWS,
  IRefreshStocksPriceWS,
  IRefreshStocksGraphImagesWS
} from '../interfaces/IWS'

declare let window: IWindow

declare function WebsocketHandler(namespace: string, options?: { channel: string }): void

type TAction = ActionType<typeof Actions>

const initWS = (dispatch: Dispatch<TAction>) => {
  window.WebsocketHandler.addEventListener('sidebar', 'updateMoney', (payload: IUpdateMoneyWS) => {
    dispatch(Actions.setUserMoney(+payload.money))
  })

  window.WebsocketHandler.addEventListener('sidebar', 'onJail', () => {
    dispatch(Actions.setIsJail(true))
  })

  window.WebsocketHandler.addEventListener('sidebar', 'onHospital', () => {
    dispatch(Actions.setIsHospital(true))
  })

  window.WebsocketHandler.addEventListener('sidebar', 'onRevive', () => {
    dispatch(Actions.setIsHospital(false))
  })

  window.WebsocketHandler.addEventListener('sidebar', 'onLeaveFromJail', () => {
    dispatch(Actions.setIsJail(false))
  })

  const userHandler = new WebsocketHandler('stockMarket')
  const globalHandler = new WebsocketHandler('stockMarket', { channel: 'stockMarket' })

  userHandler.setActions({
    createTransaction: (payload: ICreateTransactionWS) => {
      dispatch(Actions.createTransaction(payload.data.stockId, payload.data.transaction))
    },
    removeTransactions: (payload: IRemoveTransactionsWS) => {
      dispatch(Actions.removeTransactions(payload.data.stockId, payload.data.transactions))
    },
    updateTransaction: (payload: IUpdateTransactionWS) => {
      dispatch(Actions.updateTransaction(payload.data.stockId, payload.data.transaction))
    },
    updateUserOwnedAction: (payload: IUpdateUserOwnedWS) => {
      if (document.visibilityState === 'hidden') {
        dispatch(Actions.updateUserOwned(payload.data.stockId, payload.data.sharesAmount))
        dispatch(Actions.setSharesValueToSell(payload.data.stockId, payload.data.sharesAmount))
      }
    },
    refreshStocksDividends: (payload: IRefreshStocksDividendsWS) => {
      if (document.visibilityState === 'hidden') {
        dispatch(Actions.refreshStocksDividends(payload.data.stocks))
      }
    }
  })

  globalHandler.setActions({
    refreshStocksPrice: (payload: IRefreshStocksPriceWS) => {
      dispatch(Actions.refreshStocksPrice(payload.data.stocks))
    },
    refreshStocksGraphImages: (payload: IRefreshStocksGraphImagesWS) => {
      dispatch(Actions.refreshStocksGraphImages(payload.data.stocks))
    }
  })
}

export default initWS
