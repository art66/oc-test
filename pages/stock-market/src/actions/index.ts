import { createAction } from 'typesafe-actions'
import * as actionTypes from './actionsTypes'
import ITransactions from '../interfaces/ITransactions'
import IStore from '../interfaces/IStore'
import IStock from '../interfaces/IStock'
import IChart from '../interfaces/IChart'
import TStocksDividendsWS from '../interfaces/IWS'

export const fetchStockMarketData = createAction(actionTypes.FETCH_STOCK_MARKET_DATA)()
export const setStockMarketData = createAction(actionTypes.SET_STOCK_MARKET_DATA, (data: IStore) => ({ data }))()
export const getAllChartData = createAction(actionTypes.GET_ALL_CHART_DATA, (period: number) => ({ period }))()
export const setAllChartData = createAction(actionTypes.SET_ALL_CHART_DATA, (data: IChart[]) => ({ data }))()
export const getChartData = createAction(actionTypes.GET_CHART_DATA, (id: number, period: number) => ({ id, period }))()
export const setChartData = createAction(actionTypes.SET_CHART_DATA, (data: IChart[]) => ({ data }))()
export const getMoreTransactions = createAction(actionTypes.GET_MORE_TRANSACTIONS, (count: number) => ({ count }))()
export const setHeaderPeriod = createAction(actionTypes.SET_HEADER_PERIOD, (period: number) => ({ period }))()
export const refreshStocksPrice = createAction(actionTypes.REFRESH_STOCKS_PRICE, stocks => ({ stocks }))()
export const refreshStocksDividends = createAction(
  actionTypes.REFRESH_STOCKS_DIVIDENDS,
  (stocks: TStocksDividendsWS[]) => ({ stocks })
)()
export const withdrawDividend = createAction(actionTypes.WITHDRAW_DIVIDEND)()
export const setBuyStepDialog = createAction(actionTypes.SET_STEP_BUY_DIALOG, (step: number) => ({ step }))()
export const setSellStepDialog = createAction(actionTypes.SET_STEP_SELL_DIALOG, (step: number) => ({ step }))()
export const refreshDialogBuy = createAction(actionTypes.REFRESH_DIALOG_BUY)()
export const refreshDialogSell = createAction(actionTypes.REFRESH_DIALOG_SELL)()
export const cancelMergeTransactions = createAction(actionTypes.CANCEL_MERGE_TRANSACTION)()
export const refreshMergeTransactions = createAction(actionTypes.REFRESH_MERGE_TRANSACTIONS)()
export const setRequestUrl = createAction(actionTypes.SET_REQUEST_URL, (url: string) => ({ url }))()
export const setBuyPrice = createAction(actionTypes.SET_BUY_PRICE, (price: number) => ({ price }))()
export const setSellPrice = createAction(actionTypes.SET_SELL_PRICE, (price: number) => ({ price }))()
export const setIsJail = createAction(actionTypes.SET_IS_JAIL, (isInJail: boolean) => ({ isInJail }))()
export const setIsHospital = createAction(actionTypes.SET_IS_HOSPITAL, (isInHospital: boolean) => ({ isInHospital }))()
export const changeSorting = createAction(actionTypes.CHANGE_SORTING, (field: string) => ({ field }))()
export const setTransactionChartData = createAction(actionTypes.SET_TRANSACTIONS_CHART_DATA, (data: IChart[]) => ({
  data
}))()
export const setMoreTransactions = createAction(actionTypes.SET_MORE_TRANSACTIONS, (transactions: ITransactions[]) => ({
  transactions
}))()
export const buyShares = createAction(actionTypes.BUY_SHARES)()
export const sellShares = createAction(actionTypes.SELL_SHARES)()
export const mergeTransactions = createAction(actionTypes.MERGE_TRANSACTIONS)()
export const setMergeTransactionsIds = createAction(
  actionTypes.SET_MERGE_TRANSACTIONS_IDS,
  (transactionId: number) => ({ transactionId })
)()
export const setTabPeriod = createAction(actionTypes.SET_TAB_PERIOD, (selectedTabPeriod: string) => ({
  selectedTabPeriod
}))()
export const setAllTabPeriod = createAction(actionTypes.SET_ALL_TAB_PERIOD, (selectedTabPeriod: string) => ({
  selectedTabPeriod
}))()
export const setCurrentTab = createAction(actionTypes.SET_CURRENT_TAB, (id: number, currentTab: string) => ({
  id,
  currentTab
}))()
export const createTransaction = createAction(
  actionTypes.CREATE_TRANSACTION,
  (stockId: number, transaction: ITransactions) => ({ stockId, transaction })
)()
export const removeTransactions = createAction(
  actionTypes.REMOVE_TRANSACTIONS,
  (stockId: number, transactions: number[]) => ({ stockId, transactions })
)()
export const updateTransaction = createAction(
  actionTypes.UPDATE_TRANSACTION,
  (stockId: number, transaction: ITransactions) => ({ stockId, transaction })
)()
export const updateUserOwned = createAction(actionTypes.UPDATE_USER_OWNED, (stockId: number, sharesAmount: number) => ({
  stockId,
  sharesAmount
}))()
export const refreshStocksGraphImages = createAction(actionTypes.REFRESH_STOCKS_GRAPH_IMAGES, stocks => ({ stocks }))()
export const setWithdrawDividend = createAction(actionTypes.SET_WITHDRAW_DIVIDEND, (dividend: string) => ({
  dividend
}))()
export const setSharesValueToBuy = createAction(
  actionTypes.SET_SHARES_VALUE_TO_BUY,
  (stockId: number, sharesBuy: number) => ({ stockId, sharesBuy })
)()
export const setSharesValueToSell = createAction(
  actionTypes.SET_SHARES_VALUE_TO_SELL,
  (stockId: number, sharesSell: number) => ({ stockId, sharesSell })
)()
export const backClick = createAction(actionTypes.BACK_CLICK, (type: string) => ({ type }))()
export const nextClick = createAction(actionTypes.NEXT_CLICK, (type: string) => ({ type }))()
export const setActiveTransaction = createAction(actionTypes.SET_ACTIVE_TRANSACTION, (transaction: ITransactions) => ({
  transaction
}))()
export const viewTransaction = createAction(actionTypes.VIEW_TRANSACTION, (transactionId: number) => ({
  transactionId
}))()
export const sellTransaction = createAction(actionTypes.SELL_TRANSACTION, (transactionId: number) => ({
  transactionId
}))()
export const setIsCollected = createAction(actionTypes.SET_IS_COLLECTED, (value: boolean) => ({ value }))()
export const setIsFetchTabChartData = createAction(actionTypes.SET_IS_FETCH_TAB_CHART_DATA, (value: boolean) => ({
  value
}))()
export const setAppErrorMessage = createAction(actionTypes.SET_APP_ERROR_MESSAGE, (error: string) => ({ error }))()
export const setErrorMessage = createAction(actionTypes.SET_ERROR_MESSAGE, (type: string, error: string) => ({
  type,
  error
}))()
export const setUserMoney = createAction(actionTypes.SET_USER_MONEY, (money: number) => ({ money }))()
export const setIsPriceUpdated = createAction(actionTypes.SET_PRICE_UPDATING, (value: boolean) => ({ value }))()
export const setChangeSorting = createAction(actionTypes.SET_CHANGE_SORTING, (stocks: IStock[], field: string) => ({
  stocks,
  field
}))()
