import { getCookie } from '@torn/shared/utils'
import * as actionTypes from '../actions/actionsTypes'
import initialState from '../constants/initialState'
import IType from '../interfaces/IType'
import IStockMarket from '../interfaces/IStockMarket'
import IWindow from '../interfaces/IWindow'
import { CONFIRMED_DIALOG_ID, SORT_FIELD_COOKIE, DEFAULT_SELECTED_TAB_PERIOD, PRICE_TAB } from '../constants'

declare let window: IWindow

const ACTION_HANDLERS = {
  [actionTypes.SET_STOCK_MARKET_DATA]: (state: IStockMarket, action) => {
    const { settings, stocks } = action.payload.data

    return {
      ...state,
      settings,
      userMoney: window.getUserMoney(),
      sortedBy: getCookie(SORT_FIELD_COOKIE) || state.sortedBy,
      stocks: stocks.map(stock => {
        return {
          ...stock,
          sharesPrice: {
            ...stock.sharesPrice,
            chartDataTab: stock.sharesPrice.chartData,
            transactionsChart: stock.sharesPrice.chartData
          },
          stockOwned: {
            ...stock.userOwned,
            transactions:
              stock.userOwned.transactionsAmount === stock.userOwned.transactions.length ?
                stock.userOwned.transactions :
                stock.userOwned.transactions.pop()
          },
          stepBuyDialog: 0,
          stepSellDialog: 0,
          buyPrice: 0,
          sellPrice: 0,
          selectedTabPeriod:
            settings.charts.optionsList.dropdown[settings.charts.selectedHeaderOption] || DEFAULT_SELECTED_TAB_PERIOD,
          isShowTransaction: false,
          isCollected: false,
          isFetchTabChartData: false,
          mergeTransactionsIds: [],
          activeTransaction: null,
          sharesValueToBuy: 0,
          sharesValueToSell: 0,
          prevSharesValueToBuy: 0,
          prevSharesValueToSell: 0,
          transactionChart: [],
          error: {
            buy: '',
            sell: '',
            withdrawDividend: ''
          }
        }
      })
    }
  },
  [actionTypes.SET_HEADER_PERIOD]: (state: IStockMarket, action) => {
    return {
      ...state,
      settings: {
        ...state.settings,
        charts: {
          ...state.settings.charts,
          selectedHeaderOption: action.payload.period
        }
      }
    }
  },
  [actionTypes.SET_TAB_PERIOD]: (state: IStockMarket, action) => {
    const { selectedTabPeriod } = action.payload

    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          selectedTabPeriod: stock.id === state.chooseStock.id ? selectedTabPeriod : stock.selectedTabPeriod,
          isShowTransaction: false
        }
      })
    }
  },
  [actionTypes.SET_ALL_TAB_PERIOD]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          selectedTabPeriod: action.payload.selectedTabPeriod,
          isShowTransaction: false
        }
      })
    }
  },
  [actionTypes.SET_CURRENT_TAB]: (state: IStockMarket, action) => {
    const { id, currentTab } = action.payload
    const tabPeriod = state.stocks.find(item => item.selectedTabPeriod !== '')

    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          userOwned: {
            ...stock.userOwned,
            transactions:
              id !== state.chooseStock.id ?
                stock.userOwned.transactions.filter(
                  (item, index) =>
                    (stock.userOwned.transactionsAmount === stock.userOwned.transactions.length ?
                      index < 4 :
                      index < 3) && item
                ) :
                stock.userOwned.transactions
          },
          stepSellDialog:
            id !== state.chooseStock.id && stock.stepSellDialog !== CONFIRMED_DIALOG_ID ? 0 : stock.stepSellDialog,
          stepBuyDialog:
            id !== state.chooseStock.id && stock.stepBuyDialog !== CONFIRMED_DIALOG_ID ? 0 : stock.stepBuyDialog,
          mergeTransactionsIds: id !== state.chooseStock.id ? [] : stock.mergeTransactionsIds,
          isShowTransaction: false,
          selectedTabPeriod:
            id === state.chooseStock.id && currentTab === PRICE_TAB ?
              stock.selectedTabPeriod :
              tabPeriod.selectedTabPeriod
        }
      }),
      chooseStock: {
        id,
        currentTab
      }
    }
  },
  [actionTypes.SET_CHART_DATA]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        if (stock.id === state.chooseStock.id) {
          return {
            ...stock,
            sharesPrice: {
              ...stock.sharesPrice,
              chartDataTab: action.payload.data.chartData,
              limitaryValues: action.payload.data.limitaryValues
            },
            isFetchTabChartData: true
          }
        }

        return stock
      })
    }
  },
  [actionTypes.SET_ALL_CHART_DATA]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map((stock, index) => {
        const newStockData = action.payload.data.charts.find(chart => chart.stockId === stock.id)

        return {
          ...stock,
          sharesPrice: {
            ...state.stocks[index].sharesPrice,
            chartData: newStockData.chartData,
            chartImageData: newStockData.chartImageData
          }
        }
      })
    }
  },
  [actionTypes.SET_TRANSACTIONS_CHART_DATA]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        if (stock.id === state.chooseStock.id) {
          return {
            ...stock,
            sharesPrice: {
              ...stock.sharesPrice,
              transactionsChart: action.payload.data.chartData,
              limitaryValues: action.payload.data.limitaryValues
            },
            isShowTransaction: true,
            isFetchTabChartData: true
          }
        }

        return stock
      })
    }
  },
  [actionTypes.SET_MERGE_TRANSACTIONS_IDS]: (state: IStockMarket, action) => {
    const currentStock = state.stocks.find(stock => stock.id === state.chooseStock.id)
    const newMergeTransactionsIds = currentStock.mergeTransactionsIds
    const checkIfExist = newMergeTransactionsIds.includes(action.payload.transactionId)

    return {
      ...state,
      stocks: state.stocks.map(stock => {
        if (stock.id === state.chooseStock.id) {
          return {
            ...stock,
            mergeTransactionsIds: checkIfExist ?
              newMergeTransactionsIds.filter(item => item !== action.payload.transactionId) :
              [...newMergeTransactionsIds, action.payload.transactionId]
          }
        }

        return stock
      })
    }
  },
  [actionTypes.CREATE_TRANSACTION]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        const countTransactions = stock.userOwned.transactionsAmount === stock.userOwned.transactions.length ? 4 : 3
        const transactions =
          stock.userOwned.transactions.length >= countTransactions ?
            stock.userOwned.transactions.filter(
              (item, index) => index < stock.userOwned.transactions.length - (countTransactions ? 2 : 1) && item
            ) :
            stock.userOwned.transactions

        return {
          ...stock,
          userOwned: {
            ...stock.userOwned,
            transactions:
              stock.id === action.payload.stockId ?
                [action.payload.transaction, ...transactions] :
                stock.userOwned.transactions,
            transactionsAmount:
              stock.id === action.payload.stockId ?
                stock.userOwned.transactionsAmount + 1 :
                stock.userOwned.transactionsAmount
          }
        }
      })
    }
  },
  [actionTypes.UPDATE_TRANSACTION]: (state: IStockMarket, action) => {
    const { transaction: updatedTransaction } = action.payload

    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          userOwned: {
            ...stock.userOwned,
            transactions:
              stock.id === action.payload.stockId ?
                stock.userOwned.transactions.map(transaction =>
                  transaction.id === updatedTransaction.id ? updatedTransaction : transaction) :
                stock.userOwned.transactions
          }
        }
      })
    }
  },
  [actionTypes.REMOVE_TRANSACTIONS]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        const filteredTransactions = stock.userOwned.transactions.filter(
          transaction => !action.payload.transactions.includes(transaction.id)
        )

        return {
          ...stock,
          userOwned: {
            ...stock.userOwned,
            transactions: filteredTransactions[0] ? filteredTransactions : [],
            transactionsAmount:
              stock.id === action.payload.stockId ?
                stock.userOwned.transactionsAmount - action.payload.transactions.length :
                stock.userOwned.transactionsAmount
          }
        }
      })
    }
  },
  [actionTypes.REFRESH_STOCKS_DIVIDENDS]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        const updatedStock = action.payload.stocks.find(item => stock.id === item.id)

        return {
          ...stock,
          dividends: updatedStock ? updatedStock.dividends : stock.dividends
        }
      })
    }
  },
  [actionTypes.REFRESH_STOCKS_PRICE]: (state: IStockMarket, action) => {
    return {
      ...state,
      isPriceUpdated: false,
      stocks: state.stocks.map(stock => {
        const updatedStock = action.payload.stocks.find(item => stock.id === item.id)
        const headerPeriod = state.settings.charts.selectedHeaderOption
        const chartData = updatedStock ? updatedStock.chartsData[headerPeriod] : stock.sharesPrice.chartData

        return {
          ...stock,
          sharesPrice: {
            ...stock.sharesPrice,
            chartDataTab: [...stock.sharesPrice.chartDataTab, chartData[chartData.length - 1]],
            transactionsChart: [...stock.sharesPrice.transactionsChart, chartData[chartData.length - 1]],
            chartData
          }
        }
      })
    }
  },
  [actionTypes.REFRESH_STOCKS_GRAPH_IMAGES]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        const updatedStock = action.payload.stocks.find(item => stock.id === item.id)
        const headerPeriod = state.settings.charts.selectedHeaderOption
        const chartImageData = updatedStock ? updatedStock.imagesData[headerPeriod] : stock.sharesPrice.chartImageData

        return {
          ...stock,
          sharesPrice: {
            ...stock.sharesPrice,
            chartImageData
          }
        }
      })
    }
  },
  [actionTypes.UPDATE_USER_OWNED]: (state: IStockMarket, action) => {
    const { stockId, sharesAmount } = action.payload

    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return stock.id === stockId ?
          {
            ...stock,
            userOwned: { ...stock.userOwned, sharesAmount },
            prevSharesValueToSell:
                stock.userOwned.sharesAmount - sharesAmount < 0 ?
                  stock.prevSharesValueToSell :
                  stock.userOwned.sharesAmount - sharesAmount,
            prevSharesValueToBuy:
                sharesAmount - stock.userOwned.sharesAmount < 0 ?
                  stock.prevSharesValueToBuy :
                  sharesAmount - stock.userOwned.sharesAmount
            // sharesValueToSell: sharesAmount
          } :
          stock
      })
    }
  },
  [actionTypes.SET_STEP_BUY_DIALOG]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          stepBuyDialog: stock.id === state.chooseStock.id ? action.payload.step : stock.stepBuyDialog
        }
      })
    }
  },
  [actionTypes.SET_STEP_SELL_DIALOG]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          stepSellDialog: stock.id === state.chooseStock.id ? action.payload.step : stock.stepSellDialog
        }
      })
    }
  },
  [actionTypes.SET_SHARES_VALUE_TO_BUY]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        if (stock.id === action.payload.stockId) {
          return {
            ...stock,
            prevSharesValueToBuy:
              stock.stepBuyDialog !== CONFIRMED_DIALOG_ID ? stock.sharesValueToBuy : stock.prevSharesValueToBuy,
            sharesValueToBuy: action.payload.sharesBuy
          }
        }

        return stock
      })
    }
  },
  [actionTypes.SET_SHARES_VALUE_TO_SELL]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        if (stock.id === action.payload.stockId) {
          return {
            ...stock,
            prevSharesValueToSell: stock.sharesValueToSell,
            sharesValueToSell: action.payload.sharesSell
          }
        }

        return stock
      })
    }
  },
  [actionTypes.SET_ACTIVE_TRANSACTION]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          activeTransaction: stock.id === state.chooseStock.id ? action.payload.transaction : stock.activeTransaction
        }
      })
    }
  },
  [actionTypes.REFRESH_DIALOG_BUY]: (state: IStockMarket) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          stepBuyDialog: stock.id === state.chooseStock.id ? 0 : stock.stepBuyDialog
        }
      })
    }
  },
  [actionTypes.REFRESH_DIALOG_SELL]: (state: IStockMarket) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          stepSellDialog: stock.id === state.chooseStock.id ? 0 : stock.stepSellDialog,
          sharesValueToSell: stock.userOwned.sharesAmount
        }
      })
    }
  },
  [actionTypes.SET_IS_COLLECTED]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          isCollected: stock.id === state.chooseStock.id ? action.payload.value : stock.isCollected
        }
      })
    }
  },
  [actionTypes.SET_MORE_TRANSACTIONS]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return stock.id === state.chooseStock.id && action.payload.transactions.length ?
          {
            ...stock,
            userOwned: {
              ...stock.userOwned,
              transactions:
                  stock.userOwned.transactions.length < 3 ?
                    [...stock.userOwned.transactions, action.payload.transactions[0]] :
                    [...stock.userOwned.transactions, ...action.payload.transactions]
            }
          } :
          stock
      })
    }
  },
  [actionTypes.REFRESH_MERGE_TRANSACTIONS]: (state: IStockMarket) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          mergeTransactionsIds: stock.id === state.chooseStock.id ? [] : stock.mergeTransactionsIds
        }
      })
    }
  },
  [actionTypes.SET_APP_ERROR_MESSAGE]: (state: IStockMarket, action) => {
    return {
      ...state,
      errorApp: action.payload.error
    }
  },
  [actionTypes.SET_USER_MONEY]: (state: IStockMarket, action) => {
    return {
      ...state,
      userMoney: action.payload.money
    }
  },
  [actionTypes.SET_ERROR_MESSAGE]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          error:
            stock.id === state.chooseStock.id ?
              {
                ...stock.error,
                [action.payload.type]: action.payload.error
              } :
              stock.error
        }
      })
    }
  },
  [actionTypes.SET_PRICE_UPDATING]: (state: IStockMarket, action) => {
    return {
      ...state,
      isPriceUpdated: action.payload.value
    }
  },
  [actionTypes.SET_WITHDRAW_DIVIDEND]: (state: IStockMarket, action) => {
    return {
      ...state,
      withdrawDividends: action.payload.dividend
    }
  },
  [actionTypes.SET_IS_JAIL]: (state: IStockMarket, action) => {
    return {
      ...state,
      settings: {
        ...state.settings,
        isInJail: action.payload.isInJail
      }
    }
  },
  [actionTypes.SET_IS_HOSPITAL]: (state: IStockMarket, action) => {
    return {
      ...state,
      settings: {
        ...state.settings,
        isInHospital: action.payload.isInHospital
      }
    }
  },
  [actionTypes.SET_CHANGE_SORTING]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: action.payload.stocks,
      sortedBy: action.payload.field
    }
  },
  [actionTypes.SET_REQUEST_URL]: (state: IStockMarket, action) => {
    return {
      ...state,
      url: action.payload.url
    }
  },
  [actionTypes.SET_BUY_PRICE]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          buyPrice: stock.id === state.chooseStock.id ? action.payload.price : stock.buyPrice
        }
      })
    }
  },
  [actionTypes.SET_SELL_PRICE]: (state: IStockMarket, action) => {
    return {
      ...state,
      stocks: state.stocks.map(stock => {
        return {
          ...stock,
          sellPrice: stock.id === state.chooseStock.id ? action.payload.price : stock.sellPrice
        }
      })
    }
  }
}

const reducer = (state: IStockMarket = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
