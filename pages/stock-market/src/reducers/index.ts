import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import stockMarketReducer from './stockMarket'

export const makeRootReducer = (): any => {
  return combineReducers({
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    }),
    stockMarket: stockMarketReducer
  })
}

export default makeRootReducer
