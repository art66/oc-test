import { toMoney } from '@torn/shared/utils'
import {
  ACTIVE_ICON,
  ACTIVE_TYPE,
  DEFAULT_PRESET,
  IN_PROGRESS_ICON,
  IN_PROGRESS_STATUS,
  INACTIVE_PRESET,
  INACTIVE_STATUS,
  PASSIVE_ICON,
  PASSIVE_TYPE,
  READY_PRESET,
  READY_STATUS,
  STOCKS_ACRONYM_IS_ONES
} from '../constants'
import IChart from '../interfaces/IChart'
import IDividends from '../interfaces/IDividends'

export const getPlace = (isJail: boolean, isHospital: boolean): string => {
  if (isJail) {
    return 'in jail'
  }

  if (isHospital) {
    return 'in hospital'
  }

  return 'traveling'
}

export const getSharesIncrementRequirements = (inc: number, sharesRequired: number): number => {
  let lastIncrementCost = sharesRequired
  let possibleIncrement = 1
  let totalPrice = sharesRequired

  while (possibleIncrement < inc) {
    lastIncrementCost *= 2
    totalPrice += lastIncrementCost
    possibleIncrement += 1
  }

  return totalPrice
}

export const toCardinal = (num: number): string => {
  const ones = num % 10
  const tens = num % 100

  if (tens < 11 || tens > 13) {
    switch (ones) {
      case 1:
        return `${num}st`
      case 2:
        return `${num}nd`
      case 3:
        return `${num}rd`
    }
  }

  return `${num}th`
}

export const addPlural = (num: number): string => (num === 1 ? '' : 's')

export const getIsOrAre = (currentBonus: string, acronym: string): string => {
  let value = 'is'

  if (+currentBonus.replace(/\D/g, '') !== 1) {
    value = 'are'
  }

  if (STOCKS_ACRONYM_IS_ONES.includes(acronym)) {
    value = 'is'
  }

  return value
}

export const getOptionId = (options, value: number | string): number => {
  return +Object.keys(options).find(key => options[key] === value)
}

export const checkIsDisabled = (period: number, availabilityTime: object): boolean => {
  const periodTime = availabilityTime[period]
  const date = new Date()
  const currentTime = date.getTime()

  return periodTime * 1000 > currentTime
}

export const getChangesPrice = (chart: IChart[], currentPrice: number) => {
  const diffPrice = currentPrice - chart[0].value
  const percents = 100 - (100 / chart[0].value) * currentPrice
  let changeType = 'pause'

  if (diffPrice > 0) {
    changeType = 'up'
  } else if (diffPrice < 0) {
    changeType = 'down'
  }

  return {
    dollars: Math.abs(diffPrice).toFixed(2),
    percents: Math.abs(percents).toFixed(2),
    changeType
  }
}

export const getPreparedDataForChart = (chart: IChart[]): object => {
  const servers = {}
  const price = {}

  chart.map(item => {
    const date = new Date(item.time * 1000)
    const utcTime = (date.getTime() + date.getTimezoneOffset() * 60 * 1000) / 1000

    price[utcTime] = item.value

    return price
  })

  servers['price'] = price

  return servers
}

export const getPriceChangeIcon = (
  dividendType: string,
  dividendStatus: string,
  isDarkMode: boolean
): {
  icon: string
  preset: string
} => {
  let icon = dividendType === ACTIVE_TYPE ? ACTIVE_ICON : PASSIVE_ICON
  let preset = INACTIVE_PRESET

  if (dividendStatus === IN_PROGRESS_STATUS) {
    icon = IN_PROGRESS_ICON
    preset = DEFAULT_PRESET
  }

  if (dividendStatus !== INACTIVE_STATUS) {
    if (dividendType === PASSIVE_TYPE) {
      preset = READY_PRESET
    } else if (dividendType === ACTIVE_TYPE) {
      preset = DEFAULT_PRESET
    }
  }

  return {
    icon,
    preset: isDarkMode ? `${preset}_DARK_MODE` : preset
  }
}

export const getBuySellDialogsValues = (values: {
  isBuy: boolean
  fee: number
  currentPrice?: number
  stepBuyDialog?: number
  stepSellDialog?: number
  buySharesMaxValue?: number
  sellSharesMaxValue?: number
  sharesValueToBuy: number
  sharesValueToSell: number
}): {
  stepDialog: number
  maxValue: number
  amountShares: number
  totalCoast: number
  diff: number
  formattedDiff: string
  formattedTotalCoast: string
  formattedAmountShares: string
  formattedCoastWithFee: string
} => {
  const {
    isBuy,
    fee,
    currentPrice,
    stepBuyDialog,
    stepSellDialog,
    buySharesMaxValue,
    sellSharesMaxValue,
    sharesValueToBuy,
    sharesValueToSell
  } = values
  const stepDialog = isBuy ? stepBuyDialog : stepSellDialog
  const maxValue = isBuy ? buySharesMaxValue : sellSharesMaxValue
  const amountShares = isBuy ? sharesValueToBuy : sharesValueToSell
  const formattedAmountShares = toMoney(amountShares, '')
  const totalCoast = isBuy ? amountShares * currentPrice : amountShares * currentPrice
  const formattedTotalCoast = toMoney(totalCoast, '$')
  const diff = Math.ceil(totalCoast * fee)
  const formattedDiff = toMoney(diff, '$')
  const totalCoastWithFee = isBuy ? Math.ceil(totalCoast) : Math.floor(totalCoast - diff)
  const formattedCoastWithFee = toMoney(totalCoastWithFee, '$')

  return {
    stepDialog,
    maxValue,
    amountShares,
    totalCoast,
    diff,
    formattedDiff: isNaN(diff) ? '$0' : formattedDiff,
    formattedTotalCoast: isNaN(totalCoast) ? '$0' : formattedTotalCoast,
    formattedAmountShares: isNaN(amountShares) ? '0' : formattedAmountShares,
    formattedCoastWithFee: isNaN(totalCoastWithFee) ? '$0' : formattedCoastWithFee
  }
}

export const getDividendStatusMsg = (dividends: IDividends): string => {
  let msg = dividends.status
  const daysToGet = dividends.progress.total - dividends.progress.current

  if (dividends.status === READY_STATUS) {
    msg = dividends.type === ACTIVE_TYPE ? 'Ready for collection' : 'Benefit active'
  }

  if (dividends.status === IN_PROGRESS_STATUS) {
    msg = `Ready in ${daysToGet} day${addPlural(daysToGet)}`
  }

  return msg
}

export const getArticleForStockDividend = (dividend: string): string => (dividend === 'Advanced Firewall' ? 'an ' : '')
