import { connect } from 'react-redux'
import {
  getInitialData,
  changeHairstyleColor,
  unlockHairstyle,
  unlockHonor,
  selectHairstyle,
  unlockBackdrop
} from '../modules'

import {
  getAjaxError,
  setActiveHairstyle,
  resetHairstylesTouchStates,
  setRemovedBackdrop,
  setSelectedBackdrop,
  toggleAwardsConfirmation
} from '../actions'

import TokenShop from '../components/TokenShop'

const mapStateToProps = state => ({
  shop: state.shop,
  browser: state.browser
})

const mapDispatchToProps = {
  getInitialData,
  setActiveHairstyle,
  resetHairstylesTouchStates,
  changeHairstyleColor,
  unlockHairstyle,
  unlockHonor,
  selectHairstyle,
  unlockBackdrop,
  setSelectedBackdrop,
  getAjaxError,
  setRemovedBackdrop,
  toggleAwardsConfirmation
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TokenShop)
