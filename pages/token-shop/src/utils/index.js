export function groupArray(groupSize, arr) {
  let resArr = []
  for (let i = 0; i < arr.length; i += groupSize) {
    resArr.push(arr.slice(i, i + groupSize))
  }
  let lastGroupLength = resArr[resArr.length - 1].length
  if (lastGroupLength < groupSize) {
    for (let i = 0; i < groupSize - lastGroupLength; i++) {
      resArr[resArr.length - 1].push({ status: 'empty' })
    }
  }
  return resArr
}

export function fetchUrl(url, data) {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      try {
        var json = JSON.parse(text)
        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = 'Server responded with: ' + text
      }

      if (error) {
        throw new Error(error.message)
      } else {
        return json
      }
    })
  })
}
