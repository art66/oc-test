// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'actionWrapper': string;
  'available': string;
  'backdrop': string;
  'backdropActions': string;
  'backdropInfo': string;
  'backdropSlide': string;
  'backdrops': string;
  'button': string;
  'cost': string;
  'empty': string;
  'emptyBottom': string;
  'emptyPlaceholder': string;
  'globalSvgShadow': string;
  'lockIcon': string;
  'locked': string;
  'lockedLabel': string;
  'placeholder': string;
  'previewImage': string;
  'previewImageWrapper': string;
  'selectLabel': string;
  'selected': string;
  'title': string;
  'topPanel': string;
}
export const cssExports: CssExports;
export default cssExports;
