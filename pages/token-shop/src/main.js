import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-dom'

import createStore from './store/createStore'
import TokenShop from './containers/TokenShop'

// ========================================================
// Store Instantiation
// ========================================================
const initialState = window.___INITIAL_STATE__
const store = createStore(initialState)

render(
  <Provider store={store}>
    <TokenShop />
  </Provider>,
  document.getElementById('tokenshoproot')
)
