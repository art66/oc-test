import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import shop from '../modules'

const browser = createResponsiveStateReducer({
  mobile: 600,
  tablet: 1000,
  desktop: 5000
})

export const makeRootReducer = asyncReducers => {
  return combineReducers({
    shop,
    browser,
    ...asyncReducers
  })
}

export default makeRootReducer
