import React, { Component } from 'react'
import s from '../styles/Backdrops.cssmodule.scss'
import { connect } from 'react-redux'
import { unlockBackdrop } from '../modules'
import IBackdrop from '../models/Backdrop'
import { NEXT_UNLOCKED_STATUS, BACKDROP_CONFIRM_MESSAGE, BACKDROP_KEY } from '../constants'
import { toggleAwardsConfirmation } from '../actions'
import ConfirmationBuying from './ConfirmationBuying'

interface IProp {
  unlockBackdrop: (position: number) => void
  toggleAwardsConfirmation: (position: number, showConfirmation: boolean, awardType: string) => void
  item: IBackdrop
  tabIndex: number
  shop: {
    tokens: number
    backdrop: {
      cost: number
      backdrops: IBackdrop[]
    }
  }
}

class LockedBackdrop extends Component<IProp> {
  showConfirmation = (position, disabled) => {

    if (!disabled) {
      this.props.toggleAwardsConfirmation(position, true, BACKDROP_KEY)
    }
  }

  _handleUnlockBackdrop = (item) => {

    if (!item.onHold) {
      this.props.unlockBackdrop(item.position)
    }
  }

  render () {
    const { item, shop, tabIndex, toggleAwardsConfirmation } = this.props
    return (
      <div className={s.locked}>
        <div className={`locked-placeholder ${s.placeholder}`}>
          {item.status === NEXT_UNLOCKED_STATUS && !item.showConfirmation ? (
            <div className={s.actionWrapper}>
              <span className={s.cost}>{shop.backdrop.cost}</span>
              <button
                className={`torn-btn gold ${s.button} ${shop.tokens < shop.backdrop.cost ? 'disabled' : ''}`}
                onClick={() => this.showConfirmation(item.position, shop.tokens < shop.backdrop.cost)}
                aria-label={`Unlock backdrop. Cost ${shop.backdrop.cost} tokens`}
                tabIndex={tabIndex}
              >
                UNLOCK
              </button>
            </div>
          ) : item.status === NEXT_UNLOCKED_STATUS && item.showConfirmation ?
            <ConfirmationBuying
              confirmationMessage={BACKDROP_CONFIRM_MESSAGE}
              unlockItem={() => this._handleUnlockBackdrop(item)}
              cancelUnlockingItem={() => toggleAwardsConfirmation(item.position, false, BACKDROP_KEY)}
            /> : (
            <div className={s.lockIcon} />
          )}
        </div>
        <div className={`label ${s.lockedLabel}`}>Locked</div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  shop: state.shop
})

const mapDispatchToProps = {
  unlockBackdrop,
  toggleAwardsConfirmation
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LockedBackdrop)
