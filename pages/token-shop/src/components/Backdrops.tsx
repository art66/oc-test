import React, { Component } from 'react'
import { Swiper, Slide } from 'react-dynamic-swiper'
import 'react-dynamic-swiper/lib/styles.css'
import { connect } from 'react-redux'
import { AMOUNT_BACKDROPS_PER_SLIDE, SELECTED_STATUS, AVAILABLE_STATUS, EMPTY_STATUS } from '../constants'
import { groupArray } from '../utils'
import Backdrop from '../models/Backdrop'
import AvailableBackdrop from './AvailableBackdrop'
import LockedBackdrop from './LockedBackdrop'
import s from '../styles/Backdrops.cssmodule.scss'

type Props = {
  shop: {
    backdrop: {
      backdrops: Backdrop[]
    }
  }
}

type State = {
  activeSlide: number
}

class Backdrops extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      activeSlide: 0
    }
  }

  getBackdrop = (item, index) => {
    const tabIndex = index === this.state.activeSlide ? 0 : -1

    if (item.status === SELECTED_STATUS || item.status === AVAILABLE_STATUS) {
      return <AvailableBackdrop item={item} tabIndex={tabIndex} />
    }
    if (item.status === EMPTY_STATUS) {
      return (
        <div className={s.empty}>
          <div className={`empty-placeholder ${s.emptyPlaceholder}`} />
          <div className={`empty-placeholder ${s.emptyBottom}`} />
        </div>
      )
    }
    return <LockedBackdrop item={item} tabIndex={tabIndex} />
  }

  isDisabled = (swiper, param) => {
    return swiper && swiper[param] ? 'swiper-button-disabled' : ''
  }

  setActiveIndex = (param) => {
    this.setState((state: State) => {
      return { activeSlide: state.activeSlide + param }
    })
  }

  render() {
    const { shop } = this.props
    const options = {
      slidesPerView: 1,
      slidesPerGroup: 1,
      spaceBetween: 2,
      shortSwipes: false,
      longSwipesMs: 100
    }
    let groupedArr = shop.backdrop ? groupArray(AMOUNT_BACKDROPS_PER_SLIDE, shop.backdrop.backdrops) : []
    let amountSlides = groupedArr
      .findIndex((group) => group.every(item => (item.status === 'locked' || item.status === 'empty')))

    if (amountSlides === -1) {
      amountSlides = groupedArr.length
    }
    groupedArr = groupedArr.slice(0, amountSlides)

    return (
      <div className={s.backdrops}>
        <div className={`panel ${s.topPanel}`}>
          <h3 className={s.title}>BACKDROPS</h3>
        </div>
        <Swiper
          swiperOptions={options}
          className='swiper'
          pagination={false}
          prevButton={swiper => <button
            className={`swiper-button-prev ${this.isDisabled(swiper, 'isBeginning')}`}
            onClick={() => this.setActiveIndex(-1)}
            aria-label='Move to previous slide'
          />}
          nextButton={swiper => <button
            className={`swiper-button-next ${this.isDisabled(swiper, 'isEnd')}`}
            onClick={() => this.setActiveIndex(1)}
            aria-label='Move to next slide'
          />}
        >
          {groupedArr.map((item, index) => {
            return (
              <Slide key={index}>
                <div className={s.backdropSlide}>
                  {item.map((item, itemIndex) => {
                    return (
                      <div className={s.backdrop} key={item.position ? item.position : item.status + itemIndex}>
                        {this.getBackdrop(item, index)}
                      </div>
                    )
                  })}
                </div>
              </Slide>
            )
          })}
        </Swiper>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  shop: state.shop
})

export default connect(
  mapStateToProps,
  {}
)(Backdrops)
