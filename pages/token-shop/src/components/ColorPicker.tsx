import React, { Component } from 'react';
import { connect } from 'react-redux';
import s from '../styles/Hairstyles.cssmodule.scss';
import { changeHairstyleColor } from '../modules';
import Hairstyle from '../models/Hairstyle';

type Props = {
  item: Hairstyle;
  tabIndex: number;
  changeHairstyleColor: (ID: number, color: string) => void;
};

class ColorPicker extends Component<Props> {
  render() {
    const { item, changeHairstyleColor, tabIndex } = this.props;
    const colors = ['black', 'brown', 'blond', 'ginger', 'white'];

    return (
      <ul className={s.colorPicker}>
        {
          colors.map(color => {
            return (
              <li key={color} className={`${item.color === color ? `active-color ${s.active}` : ''}`}>
                <button
                  className={`${s.pickerButton} ${s[color]}`}
                  onClick={() => {changeHairstyleColor(item.ID, color)}}
                  aria-label={`Select ${color} color for ${item.name}`}
                  tabIndex={tabIndex}
                />
              </li>
            )
          })
        }
    </ul>
    )
  }
};

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  changeHairstyleColor
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ColorPicker);
