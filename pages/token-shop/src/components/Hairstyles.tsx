import React, { Component } from 'react'
import { Swiper, Slide } from 'react-dynamic-swiper'
import 'react-dynamic-swiper/lib/styles.css'
import { connect } from 'react-redux'
import s from '../styles/Hairstyles.cssmodule.scss'
import { AMOUNT_HAIRSTYLES_PER_SLIDE, EMPTY_STATUS, AVAILABLE_STATUS, SELECTED_STATUS } from '../constants'
import { groupArray } from '../utils'
import { setActiveHairstyle, resetHairstylesTouchStates } from '../actions'
import Hairstyle from '../models/Hairstyle'
import AvailableHairstyle from './AvailableHairstyle'
import LockedHairstyle from './LockedHairstyle'

interface IProps {
  setActiveHairstyle: (item: Hairstyle) => void
  resetHairstylesTouchStates: () => void
  shop: {
    tokens: number
    hairstyle: {
      cost: number
      hairstyles: Hairstyle[]
    }
  }
}

interface IState {
  activeSlide: number
}

class Hairstyles extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      activeSlide: 0
    }
  }

  componentDidMount() {
    document.addEventListener('click', this.resetItemsTouchStatesByCLick)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.resetItemsTouchStatesByCLick)
  }

  handleActiveItem = (e, item) => {
    if(!e.target.closest('.hairstyle-actions')) this.props.setActiveHairstyle(item)
  }

  isDisabled = (swiper, param) => {
    return swiper && swiper[param] ? 'swiper-button-disabled' : ''
  }

  setActiveIndex = (param) => {
    this.setState((state: IState) => {
      return { activeSlide: state.activeSlide + param }
    })
  }

  determineTouchState = item => {
    return item.touchStatus && (item.touchStatus === 'active' || item.touchStatus === 'notActive')
      ? s[item.touchStatus]
      : ''
  }

  resetItemsTouchStatesByCLick = e => {
    if (!e.target.closest('.hairstyle-slide')) {
      this.props.resetHairstylesTouchStates()
    }
  }

  getHairstyle = (item, index) => {
    const tabIndex = index === this.state.activeSlide ? 0 : -1
    if (item.status === AVAILABLE_STATUS || item.status === SELECTED_STATUS) {
      return <AvailableHairstyle item={item} tabIndex={tabIndex} />
    }
    if (item.status === EMPTY_STATUS) {
      return <div className={`empty-hair ${s.empty}`} />
    }
    return <LockedHairstyle item={item} tabIndex={tabIndex} />
  }

  render() {
    const { shop } = this.props
    const options = {
      slidesPerView: 1,
      slidesPerGroup: 1,
      spaceBetween: 2,
      shortSwipes: false,
      longSwipesMs: 100
    }
    const that = this
    let groupedArr = shop.hairstyle ? groupArray(AMOUNT_HAIRSTYLES_PER_SLIDE, shop.hairstyle.hairstyles) : []
    let amountSlides = groupedArr
      .findIndex((group) => group.every(item => (item.status === 'locked' || item.status === 'empty')))
    if (amountSlides === -1) {
      amountSlides = groupedArr.length
    }
    groupedArr = groupedArr.slice(0, amountSlides)

    return (
      <div className={s.hairstyles}>
        <div className={`panel ${s.topPanel}`}>
          <h3 className={s.title}>HAIRSTYLES</h3>
        </div>
        <Swiper
          swiperOptions={options}
          className={'swiper'}
          pagination={false}
          prevButton={swiper => <button
            className={`swiper-button-prev ${this.isDisabled(swiper, 'isBeginning')}`}
            onClick={() => this.setActiveIndex(-1)}
            aria-label='Move to previous slide'
          />}
          nextButton={swiper => <button
            className={`swiper-button-next ${this.isDisabled(swiper, 'isEnd')}`}
            onClick={() => this.setActiveIndex(1)}
            aria-label='Move to next slide'
          />}
        >
          {groupedArr.map((item, index) => {
            return (
              <Slide key={index}>
                <div className={`hairstyle-slide ${s.hairstylesSlide}`}>
                  {item.map((item, indexItem) => {
                    return (
                      <div
                        className={`hairstyle-item ${s.hairstyle} ${that.determineTouchState(item)}`}
                        key={item.position ? item.position : item.status + indexItem}
                        onClick={(e) => {this.handleActiveItem(e, item)}}
                      >
                        {this.getHairstyle(item, index)}
                      </div>
                    )
                  })}
                </div>
              </Slide>
            )
          })}
        </Swiper>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  shop: state.shop
})

const mapDispatchToProps = {
  setActiveHairstyle,
  resetHairstylesTouchStates
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hairstyles)
