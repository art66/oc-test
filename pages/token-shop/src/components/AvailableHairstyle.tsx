import React, { Component } from 'react';
import { connect } from 'react-redux';
import s from '../styles/Hairstyles.cssmodule.scss';
import { removeHairstyle, selectHairstyle } from '../modules';
import Hairstyle from '../models/Hairstyle';
import ColorPicker from './ColorPicker';
import { SELECTED_STATUS } from '../constants';

type Props = {
  item: Hairstyle;
  tabIndex: number;
  removeHairstyle: (ID: number) => void;
  selectHairstyle: (ID: number) => void;
};

class AvailableHairstyle extends Component<Props> {

  render() {
    const { item, removeHairstyle, selectHairstyle, tabIndex } = this.props;
    return (
      <div
        className={`available-hair ${s.available}
        ${item.status === SELECTED_STATUS ? s.selected : ''} ${s[item.rarity]}`}
        tabIndex={tabIndex}
        aria-label={item.name}
      >
        <div
          className={`${s.hairstyleWrapper} ${s[item.color]}`}
          style={{ backgroundImage: `url(${item.imageLink})` }}
        />
        <div className={s.hairstyleInfo}>
          <span className={s.name}>{item.name}</span>
          {item.status === SELECTED_STATUS ? <span className={s.selectLabel}>SELECTED</span> : ''}
        </div>
        <div className={`hairstyle-actions ${s.hairstyleActions}`}>
          <ColorPicker item={item} tabIndex={tabIndex} />
          <div className={s.btnWrapper}>
            {item.status === SELECTED_STATUS ? (
              <button
                className={`torn-btn gold ${s.button}`}
                onClick={() => removeHairstyle(item.ID)}
                aria-label={`Remove ${item.name}`}
                tabIndex={tabIndex}
              >REMOVE</button>
            ) : (
              <button
                className={`torn-btn gold ${s.button}`}
                onClick={() => selectHairstyle(item.ID)}
                aria-label={`Select ${item.name}`}
                tabIndex={tabIndex}
              >SELECT</button>
            )}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  selectHairstyle,
  removeHairstyle
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AvailableHairstyle);
