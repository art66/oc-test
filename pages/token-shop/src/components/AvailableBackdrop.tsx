import React, { Component } from 'react'
import { connect } from 'react-redux'
import s from '../styles/Backdrops.cssmodule.scss'
import { fetchUrl } from '../utils'
import { getAjaxError, setRemovedBackdrop, setSelectedBackdrop } from '../actions'
import IBackdrop from '../models/Backdrop'
import { SELECTED_STATUS } from '../constants'

interface IProp {
  setRemovedBackdrop: (ID: number) => void
  setSelectedBackdrop: (ID: number) => void
  getAjaxError: (error: any) => void
  item: IBackdrop
  tabIndex: number
  browser: {
    lessThan: {
      desktop: boolean
    }
  }
}

class AvailableBackdrop extends Component<IProp> {
  changeBackdrop = (className, backdropUrl) => {
    const bg = document.getElementsByClassName(className)

    if (bg.length === 0) {
      const el = document.createElement('div')

      el.style.cssText = `background-image: url(${backdropUrl});`
      el.classList.add(className)
      document.body.appendChild(el)
    } else {
      (bg[0] as HTMLElement).style.cssText = `background-image: url(${backdropUrl});`
    }
  }

  selectBackdropHandler = (item) => {
    return fetchUrl('/token_shop.php?step=selectBackdrop', { ID: item.ID }).then(
      data => {
        if (data.success) {
          this.changeBackdrop('custom-bg-desktop', item.linkDesktopImage)
          this.changeBackdrop('custom-bg-mobile', item.linkMobileImage)
          this.props.setSelectedBackdrop(item.ID)
        }
      },
      error => {
        this.props.getAjaxError(error)
      }
    )
  }

  removeBackdropHandler = (ID) => {
    return fetchUrl('/token_shop.php?step=removeBackdrop', { ID: ID }).then(
      data => {
        if (data.success) {
          const desktopBg = document.getElementsByClassName('custom-bg-desktop')[0]
          const mobileBg = document.getElementsByClassName('custom-bg-mobile')[0]

          this.props.setRemovedBackdrop(ID);
          (desktopBg as HTMLElement).style.cssText = '';
          (mobileBg as HTMLElement).style.cssText = ''
        }
      },
      error => {
        this.props.getAjaxError(error)
      }
    )
  }

  getButton = (buttonName, item, tabIndex, callback) => {
    const isTouchDevice = 'ontouchstart' in document.documentElement
    const isMobile = this.props.browser.lessThan.desktop

    return isMobile && isTouchDevice ?
      (
        <button
          className={`torn-btn gold ${s.button}`}
          onTouchStart={callback}
          aria-label={`${buttonName} ${item.name} backdrop`}
          tabIndex={tabIndex}
        >
          {buttonName}
        </button>
      ) :
      (
        <button
          className={`torn-btn gold ${s.button}`}
          onClick={callback}
          aria-label={`${buttonName} ${item.name} backdrop`}
          tabIndex={tabIndex}
        >
          {buttonName}
        </button>
      )
  }

  render() {
    const { item, tabIndex } = this.props

    return (
      <div
        className={`${s.available} ${item.status === SELECTED_STATUS ? s.selected : ''}`}
        tabIndex={tabIndex}
        aria-label={`${item.name} backdrop`}
      >
        <div className={`image-wrapper ${s.previewImageWrapper}`}>
          <img className={s.previewImage} src={item.linkPreviewImage} alt={item.name} />
        </div>
        <div className={`backdrop-name ${s.backdropInfo}`}>
          <span>{item.name}</span>
          {item.status === SELECTED_STATUS ? <span className={s.selectLabel}>SELECTED</span> : ''}
        </div>
        <div className={`actions ${s.backdropActions}`}>
          {
            item.status === SELECTED_STATUS ?
              this.getButton('REMOVE', item, tabIndex, () => this.removeBackdropHandler(item.ID)) :
              this.getButton('SELECT', item, tabIndex, () => this.selectBackdropHandler(item))
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  shop: state.shop,
  browser: state.browser
})

const mapDispatchToProps = {
  setRemovedBackdrop,
  setSelectedBackdrop,
  getAjaxError
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AvailableBackdrop)
