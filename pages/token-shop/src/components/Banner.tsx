import React, { Component } from 'react'
import s from '../styles/Banner.cssmodule.scss'
import { connect } from 'react-redux'

type Props = {
  shop: {
    tokens: number;
  };
}

class Banner extends Component<Props> {
  render() {
    const { shop } = this.props

    return (
      <div className={`banner-block ${s.banner}`}>
        <div className={s.bannerBg} />
        <div className={`tokens-panel ${s.tokens}`}>
          <span className={s.tokensAmount}>{shop.tokens}</span>
          <span>{shop.tokens === 1 ? 'TOKEN' : 'TOKENS'} AVAILABLE</span>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  shop: state.shop
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Banner)
