import React, { Component } from 'react'
import AppHeader from '@torn/shared/components/AppHeader'
import { IClientProps } from '@torn/shared/components/AppHeader/container/types'
import Banner from './Banner'
import Honors from './Honors'
import Hairstyles from './Hairstyles'
import Backdrops from './Backdrops'
import '../styles/TokenShop.scss'

type TProps = {
  getInitialData: () => void
  shop: {
    appHeader: IClientProps
  }
}

class TokenShop extends Component<TProps> {
  componentDidMount() {
    this.props.getInitialData()
  }

  render() {
    const { shop: { appHeader } } = this.props

    return (
      <>
        <AppHeader appID='TokenShop' clientProps={appHeader} />
        <div>
          <Banner />
          <hr className='page-head-delimiter' />
          <Honors />
          <hr className='page-head-delimiter' />
          <Hairstyles />
          <hr className='page-head-delimiter' />
          <Backdrops />
        </div>
      </>
    )
  }
}

export default TokenShop
