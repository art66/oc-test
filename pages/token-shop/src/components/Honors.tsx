import React, { Component } from 'react'
import 'react-dynamic-swiper/lib/styles.css'
import { Swiper, Slide } from 'react-dynamic-swiper'
import { connect } from 'react-redux'
import s from '../styles/Honnors.cssmodule.scss'
import { groupArray } from '../utils'
import { AMOUNT_HONORS_PER_SLIDE, HONORS_CONFIRM_MESSAGE } from '../constants'
import { unlockHonor } from '../modules'
import { toggleHonorsConfirmation } from '../actions'
import Honor from '../models/Honor'
import ConfirmationBuying from './ConfirmationBuying'

interface IProps {
  unlockHonor: (id: number) => void
  toggleHonorsConfirmation: (ID: number, showConfirmation: boolean) => void
  shop: {
    tokens: number
    honors: Honor[]
  }
  browser: {
    lessThan: {
      desktop: boolean
    }
  }
}

interface IState {
  activeSlide: number
}

class Honors extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      activeSlide: 0
    }
  }

  showConfirmation = (item, disabled) => {
    if (!disabled) this.props.toggleHonorsConfirmation(item.id, true)
  }

  _handleUnlockHonor = (item) => {
    if (!item.onHold) this.props.unlockHonor(item)
  }

  isDisabled = (swiper, param) => {
    return swiper && swiper[param] ? 'swiper-button-disabled' : ''
  }

  setActiveIndex = (param) => {
    this.setState((state: IState) => {
      return { activeSlide: state.activeSlide + param }
    })
  }

  toggleConfirmation = (item, isMobile, isTouchDevice, index, shop) => {
    return item.showConfirmation ?
      (
        <ConfirmationBuying
          confirmationMessage={HONORS_CONFIRM_MESSAGE}
          unlockItem={() => this._handleUnlockHonor(item)}
          cancelUnlockingItem={() => this.props.toggleHonorsConfirmation(item.id, false)}
          inlineButtons={true}
        />
      ) :
      (
        <div className={s.wrapper}>
          <div className={s.hover}>
            <span className={s.cost}>{item.tokens}</span>
            {isMobile && isTouchDevice ? (
              <button
                className={`torn-btn gold ${s.button} ${shop.tokens < item.tokens ? 'disabled' : ''}`}
                onTouchStart={() => this.showConfirmation(item, shop.tokens < item.tokens)}
                aria-label={`Unlock ${item.name} honor. Cost ${item.tokens} tokens.`}
                tabIndex={this.state.activeSlide === index ? 0 : -1}
              >
                UNLOCK
              </button>
            ) : (
              <button
                className={`torn-btn gold ${s.button} ${shop.tokens < item.tokens ? 'disabled' : ''}`}
                onClick={() => this.showConfirmation(item, shop.tokens < item.tokens)}
                aria-label={`Unlock ${item.name} honor. Cost ${item.tokens} tokens.`}
                tabIndex={this.state.activeSlide === index ? 0 : -1}
              >
                UNLOCK
              </button>
            )}
          </div>
          <div className={s.defaultWrapper}>
            <div className={s.default}>{item.name}</div>
          </div>
        </div>
      )
  }

  render() {
    const { shop, browser } = this.props
    const options = {
      slidesPerView: 1,
      slidesPerGroup: 1,
      spaceBetween: 2,
      shortSwipes: false,
      longSwipesMs: 100
    }
    const isTouchDevice = 'ontouchstart' in document.documentElement
    const isMobile = browser.lessThan.desktop
    const groupedArr = shop.honors ? groupArray(AMOUNT_HONORS_PER_SLIDE, shop.honors) : []

    return (
      <div className={s.honors}>
        <div className={`panel ${s.topPanel}`}>
          <h3 className={s.title}>HONORS</h3>
        </div>
        <Swiper
          swiperOptions={options}
          className='swiper'
          pagination={false}
          prevButton={swiper => <button
            className={`swiper-button-prev ${this.isDisabled(swiper, 'isBeginning')}`}
            onClick={() => this.setActiveIndex(-1)}
            aria-label='Move to previous slide'
          />}
          nextButton={swiper => <button
            className={`swiper-button-next ${this.isDisabled(swiper, 'isEnd')}`}
            onClick={() => this.setActiveIndex(1)}
            aria-label='Move to next slide'
          />}
        >
          {groupedArr.map((item, index) => {
            return (
              <Slide key={index}>
                {item.map((item) => {
                  return (
                    <div className={`${s.honor} honor-item`} key={item.id}>
                      {!item.imageLink ? (
                        <div
                          className={s.locked}
                          tabIndex={this.state.activeSlide === index ? 0 : -1}
                          aria-label={`${item.name} Honor`}
                        >
                          {!item.status ?
                            this.toggleConfirmation(item, isMobile, isTouchDevice, index, shop) :
                            (
                              <div />
                            )}
                        </div>
                      ) : (
                        <div
                          className={s.available}
                          tabIndex={this.state.activeSlide === index ? 0 : -1}
                          aria-label={`${item.name} Honor`}
                        >
                          <img className={s.honorImg} src={item.imageLink} alt={item.name} />
                        </div>
                      )}
                    </div>
                  )
                })}
              </Slide>
            )
          })}
        </Swiper>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  shop: state.shop,
  browser: state.browser
})

const mapDispatchToProps = {
  unlockHonor,
  toggleHonorsConfirmation
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Honors)
