import React, { Component } from 'react'
import { connect } from 'react-redux'
import s from '../styles/Hairstyles.cssmodule.scss'
import { unlockHairstyle } from '../modules'
import IHairstyle from '../models/Hairstyle'
import { HAIRSTYLE_CONFIRM_MESSAGE, HAIRSTYLE_KEY, NEXT_UNLOCKED_STATUS } from '../constants'
import { toggleAwardsConfirmation } from '../actions'
import ConfirmationBuying from './ConfirmationBuying'

interface IProps {
  item: IHairstyle
  tabIndex: number
  unlockHairstyle: (ID: number) => void
  toggleAwardsConfirmation: (position: number, showConfirmation: boolean, awardType: string) => void
  shop: {
    tokens: number
    hairstyle: {
      cost: number
      hairstyles: IHairstyle[]
    }
  }
  browser: {
    lessThan: {
      tablet: boolean
    }
  }
}

class LockedHairstyle extends Component<IProps> {
  showConfirmation = (position, disabled) => {
    const cannotUnlock = this.props.shop.hairstyle.hairstyles
      .filter(item => item.touchStatus === 'active').length

    if (!disabled && !cannotUnlock) {
      this.props.toggleAwardsConfirmation(position, true, HAIRSTYLE_KEY)
    }
  }

  _handleUnlockHairstyle = (item) => {

    if (!item.onHold) {
      this.props.unlockHairstyle(item.position)
    }
  }

  render() {
    const { item, shop, tabIndex, toggleAwardsConfirmation } = this.props

    return (
      <div className={`locked-hair ${s.locked}`}>
        {item.status === NEXT_UNLOCKED_STATUS && !item.showConfirmation ? (
          <div className={s.actionWrapper}>
            <div className={s.cost}>
              <span className={s.costValue}>{shop.hairstyle.cost}</span>
            </div>
            <button
              className={`torn-btn gold ${s.button} ${shop.tokens < shop.hairstyle.cost ? 'disabled' : ''}`}
              onClick={() => this.showConfirmation(item.position, shop.tokens < shop.hairstyle.cost)}
              aria-label={`Unlock hairstyle. Cost ${shop.hairstyle.cost} tokens`}
              tabIndex={tabIndex}
            >
              UNLOCK
            </button>
          </div>
        ) : item.status === NEXT_UNLOCKED_STATUS && item.showConfirmation ?
          <ConfirmationBuying
            confirmationMessage={HAIRSTYLE_CONFIRM_MESSAGE}
            unlockItem={() => this._handleUnlockHairstyle(item)}
            cancelUnlockingItem={() => toggleAwardsConfirmation(item.position, false, HAIRSTYLE_KEY)}
          /> : (
          <div className={s.lockIcon} />
        )}
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  shop: state.shop,
  browser: state.browser
})

const mapDispatchToProps = {
  unlockHairstyle,
  toggleAwardsConfirmation
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LockedHairstyle)
