import React, { Component } from 'react'
import s from '../styles/ConfirmationBuying.cssmodule.scss'
import cn from 'classnames'

interface IProp {
  confirmationMessage: string
  unlockItem: () => void
  cancelUnlockingItem: () => void
  inlineButtons?: boolean
}

class ConfirmationBuying extends Component<IProp> {
  render() {
    const { confirmationMessage, unlockItem, cancelUnlockingItem, inlineButtons } = this.props

    return (
      <div className={s.confirmationWrapper}>
        <div className={cn(s.confirmation, { [s.inlineButtons]: inlineButtons })}>
          <div className={s.title}>{confirmationMessage}</div>
          <div className={s.buttonsWrapper}>
            <button className={s.confirmBtn} onClick={unlockItem}>Yes</button>
            <button className={s.cancelBtn} onClick={cancelUnlockingItem}>No</button>
          </div>
        </div>
      </div>
    )
  }
}

export default ConfirmationBuying
