export default interface Honor {
  id: number
  name: string
  tokens: number
  imageLink: string
  showConfirmation: boolean
}
