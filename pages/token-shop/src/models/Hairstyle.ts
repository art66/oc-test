export default interface IHairstyle {
  ID: number
  name: string
  imageLink: string
  status: string
  color: string
  rarity: string
  position: number
  touchStatus: string
  onHold: boolean
  showConfirmation: boolean
}
