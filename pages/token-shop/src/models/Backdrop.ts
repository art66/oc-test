export default interface IBackdrop {
  status: string
  position: number
  ID?: number
  name?: string
  linkMobileImage?: string
  linkDesktopImage?: string
  linkPreviewImage?: string
  rarity?: string
  onHold: boolean
  showConfirmation: boolean
}
