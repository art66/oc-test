import {
  HAIRSTYLES_COST,
  BACKDROPS_COST,
  AVAILABLE_STATUS,
  SELECTED_STATUS,
  NEXT_UNLOCKED_STATUS,
  LOCKED_STATUS,
  HAIRSTYLE_KEY,
  BACKDROP_KEY
} from '../constants'
import { fetchUrl } from '../utils'
import {
  setInitialData,
  getAjaxError,
  resetHairstylesTouchStates,
  setHairstyleColor,
  setUnlockedHairstyle,
  setUnlockedHonor,
  setSelectedHairstyle,
  setRemovedHairstyle,
  setUnlockedBackdrop,
  setAwardOnHold,
  setHonorOnHold
} from '../actions'
import * as actionTypes from '../actions/actionTypes'

const ACTION_HANDLERS = {
  [actionTypes.SET_INITIAL_DATA]: (state, action) => {
    return {
      ...state,
      appHeader: action.payload.appHeader,
      tokens: action.payload.tokens,
      honors: action.payload.honors.map(item => {
        return {
          ...item,
          onHold: false,
          showConfirmation: false
        }
      }),
      hairstyle: {
        cost: HAIRSTYLES_COST,
        hairstyles: action.payload.hairstyles.map((item, index) => {
          if (
            item.status === LOCKED_STATUS
            && (index === 0
              || action.payload.hairstyles[index - 1].status === AVAILABLE_STATUS
              || action.payload.hairstyles[index - 1].status === SELECTED_STATUS)
          ) {
            return {
              ...item,
              position: index + 1,
              status: NEXT_UNLOCKED_STATUS,
              onHold: false,
              showConfirmation: false
            }
          }
          return {
            ...item,
            position: index + 1,
            onHold: false,
            showConfirmation: false
          }
        })
      },
      backdrop: {
        cost: BACKDROPS_COST,
        backdrops: action.payload.backdrops.map((item, index) => {
          if (item.status === LOCKED_STATUS && (index === 0
              || action.payload.backdrops[index - 1].status === AVAILABLE_STATUS
              || action.payload.backdrops[index - 1].status === SELECTED_STATUS)) {
            return {
              ...item,
              position: index + 1,
              status: NEXT_UNLOCKED_STATUS,
              onHold: false,
              showConfirmation: false
            }
          }
          return {
            ...item,
            position: index + 1,
            onHold: false,
            showConfirmation: false
          }
        })
      }
    }
  },
  [actionTypes.SET_ACTIVE_HAIRSTYLE]: (state, action) => {
    const isNotTouched = state.hairstyle.hairstyles.every(item => !item.touchStatus)

    return {
      ...state,
      hairstyle: {
        ...state.hairstyle,
        hairstyles: state.hairstyle.hairstyles.map(item => {
          if ((action.payload.item.status === SELECTED_STATUS
            || action.payload.item.status === AVAILABLE_STATUS) && isNotTouched) {
            return (item.status === SELECTED_STATUS || item.status === AVAILABLE_STATUS)
            && item.ID === action.payload.item.ID ?
              { ...item, touchStatus: 'active' } :
              { ...item, touchStatus: 'notActive' }
          }
          return {
            ...item,
            touchStatus: ''
          }
        })
      }
    }
  },
  [actionTypes.RESET_HAIRSTYLES_TOUCH_STATES]: state => {
    return {
      ...state,
      hairstyle: {
        ...state.hairstyle,
        hairstyles: state.hairstyle.hairstyles.map(item => ({
          ...item,
          touchStatus: ''
        }))
      }
    }
  },
  [actionTypes.SET_HAIRSTYLE_COLOR]: (state, action) => {
    return {
      ...state,
      hairstyle: {
        ...state.hairstyle,
        hairstyles: state.hairstyle.hairstyles.map(item => {
          return action.payload.ID === item.ID ? { ...item, color: action.payload.color } : { ...item }
        })
      }
    }
  },
  [actionTypes.SET_UNLOCKED_HAIRSTYLE]: (state, action) => {
    return {
      ...state,
      tokens: action.payload.tokensAmount,
      hairstyle: {
        ...state.hairstyle,
        hairstyles: state.hairstyle.hairstyles.map(item => {
          if (item.position === action.payload.position) {
            return {
              ...item,
              ...action.payload.item,
              showConfirmation: false
            }
          }
          if (action.payload.position + 1 === item.position) {
            return {
              ...item,
              status: NEXT_UNLOCKED_STATUS
            }
          }
          return { ...item }
        })
      }
    }
  },
  [actionTypes.SET_UNLOCKED_BACKDROP]: (state, action) => {
    return {
      ...state,
      tokens: action.payload.tokensAmount,
      backdrop: {
        ...state.backdrop,
        backdrops: state.backdrop.backdrops.map(item => {
          if (item.position === action.payload.position) {
            return {
              ...item,
              ...action.payload.item,
              showConfirmation: false
            }
          }
          if (action.payload.position + 1 === item.position) {
            return {
              ...item,
              status: NEXT_UNLOCKED_STATUS
            }
          }
          return { ...item }
        })
      }
    }
  },
  [actionTypes.SET_SELECTED_HAIRSTYLE]: (state, action) => {
    return {
      ...state,
      hairstyle: {
        ...state.hairstyle,
        hairstyles: state.hairstyle.hairstyles.map(item => {
          if (action.payload.ID === item.ID) {
            return {
              ...item,
              status: SELECTED_STATUS
            }
          }
          if (action.payload.removedHairstyleID && action.payload.removedHairstyleID === item.ID
            && item.status === SELECTED_STATUS) {
            return {
              ...item,
              status: AVAILABLE_STATUS
            }
          }
          return { ...item }
        })
      }
    }
  },
  [actionTypes.GET_AJAX_ERROR]: state => ({ ...state }),
  [actionTypes.SET_UNLOCKED_HONOR]: (state, action) => ({
    ...state,
    tokens: action.payload.tokens,
    honors: state.honors.map(item =>
      (item.id === action.payload.honor.id ?
        { ...item, ...action.payload.honor, showConfirmation: false } :
        { ...item }))
  }),
  [actionTypes.SET_REMOVED_HAIRSTYLE]: (state, action) => {
    return {
      ...state,
      hairstyle: {
        ...state.hairstyle,
        hairstyles: state.hairstyle.hairstyles.map(item => {
          return action.payload.ID === item.ID ? { ...item, status: AVAILABLE_STATUS } : { ...item }
        })
      }
    }
  },
  [actionTypes.SET_SELECTED_BACKDROP]: (state, action) => {
    return {
      ...state,
      backdrop: {
        ...state.backdrop,
        backdrops: state.backdrop.backdrops.map(item => {
          if (action.payload.ID === item.ID) {
            return {
              ...item,
              status: SELECTED_STATUS
            }
          }
          if (action.payload.ID !== item.ID && item.status === SELECTED_STATUS) {
            return {
              ...item,
              status: AVAILABLE_STATUS
            }
          }
          return { ...item }
        })
      }
    }
  },
  [actionTypes.SET_REMOVED_BACKDROP]: (state, action) => {
    return {
      ...state,
      backdrop: {
        ...state.backdrop,
        backdrops: state.backdrop.backdrops.map(item => {
          return action.payload.ID === item.ID ? { ...item, status: AVAILABLE_STATUS } : { ...item }
        })
      }
    }
  },
  [actionTypes.SET_AWARD_ON_HOLD]: (state, action) => {
    return {
      ...state,
      [action.payload.awardType]: {
        ...state[action.payload.awardType],
        [`${action.payload.awardType}s`]: state[action.payload.awardType][`${action.payload.awardType}s`].map(item => {
          return action.payload.position === item.position ? { ...item, onHold: action.payload.onHold } : { ...item }
        })
      }
    }
  },
  [actionTypes.SET_HONOR_ON_HOLD]: (state, action) => {
    return {
      ...state,
      honors: state.honors.map(item => {
        return item.id === action.payload.ID ? { ...item, onHold: action.payload.onHold } : { ...item }
      })
    }
  },
  [actionTypes.TOGGLE_AWARDS_CONFIRMATION]: (state, action) => {
    return {
      ...state,
      [action.payload.awardType]: {
        ...state[action.payload.awardType],
        [`${action.payload.awardType}s`]: state[action.payload.awardType][`${action.payload.awardType}s`].map(item => {
          return action.payload.position === item.position ?
            { ...item, showConfirmation: action.payload.showConfirmation } : { ...item }
        })
      }
    }
  },
  [actionTypes.TOGGLE_HONORS_CONFIRMATION]: (state, action) => {
    return {
      ...state,
      honors: state.honors.map(item => {
        return item.id === action.payload.ID ?
          { ...item, showConfirmation: action.payload.showConfirmation } : { ...item }
      })
    }
  }
}

export const getInitialData = () => dispatch => {
  return fetchUrl('/token_shop.php?step=indexAction').then(
    data => {
      dispatch(setInitialData(data))
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const changeHairstyleColor = (ID, color) => dispatch => {
  fetchUrl('/token_shop.php?step=updateHairstyleColor', { ID, color }).then(
    data => {
      if (data.success) dispatch(setHairstyleColor(ID, color))
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const unlockHonor = item => dispatch => {
  dispatch(setHonorOnHold(item.id, true))
  return fetchUrl('/token_shop.php?step=unlockHonor', item).then(
    data => {
      dispatch(setUnlockedHonor(data.tokens, data.honor))
      dispatch(setHonorOnHold(item.id, false))
    },
    error => {
      dispatch(getAjaxError(error))
      dispatch(setHonorOnHold(item.id, false))
    }
  )
}

export const unlockHairstyle = position => (dispatch, getState) => {
  dispatch(setAwardOnHold(position, true, HAIRSTYLE_KEY))
  return fetchUrl('/token_shop.php?step=unlockHairstyle', { tokens: getState().shop.hairstyle.cost }).then(
    data => {
      dispatch(resetHairstylesTouchStates())
      dispatch(setUnlockedHairstyle(position, data.hairstyle, data.tokens))
      dispatch(setAwardOnHold(position, false, HAIRSTYLE_KEY))
    },
    error => {
      dispatch(getAjaxError(error))
      dispatch(setAwardOnHold(position, false, HAIRSTYLE_KEY))
    }
  )
}

export const selectHairstyle = ID => dispatch => {
  return fetchUrl('/token_shop.php?step=selectHairstyle', { ID }).then(
    data => {
      if (data.success) {
        dispatch(setSelectedHairstyle(ID, data.removedHairstyleID))
        dispatch(resetHairstylesTouchStates())
      }
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const removeHairstyle = ID => dispatch => {
  return fetchUrl('/token_shop.php?step=removeHairstyle', { ID }).then(
    data => {
      if (data.success) {
        dispatch(setRemovedHairstyle(ID))
        dispatch(resetHairstylesTouchStates())
      }
    },
    error => {
      dispatch(getAjaxError(error))
    }
  )
}

export const unlockBackdrop = position => (dispatch, getState) => {
  dispatch(setAwardOnHold(position, true, BACKDROP_KEY))
  return fetchUrl('/token_shop.php?step=unlockBackdrop', { tokens: getState().shop.backdrop.cost }).then(
    data => {
      dispatch(setUnlockedBackdrop(position, data.backdrop, data.tokens))
      dispatch(setAwardOnHold(position, false, BACKDROP_KEY))
    },
    error => {
      dispatch(getAjaxError(error))
      dispatch(setAwardOnHold(position, false, BACKDROP_KEY))
    }
  )
}

const initialState = {}

export default function tokenShopReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
