import { createAction } from 'redux-actions'
import * as actionTypes from './actionTypes'

export const getAjaxError = createAction(actionTypes.GET_AJAX_ERROR, error => error)

export const setInitialData = createAction(actionTypes.SET_INITIAL_DATA, initialData => initialData)

export const setActiveHairstyle = createAction(actionTypes.SET_ACTIVE_HAIRSTYLE, item => ({ item }))

export const resetHairstylesTouchStates = createAction(actionTypes.RESET_HAIRSTYLES_TOUCH_STATES)

export const setHairstyleColor = createAction(actionTypes.SET_HAIRSTYLE_COLOR, (ID, color) => ({ ID, color }))

export const setUnlockedHairstyle = (createAction(actionTypes.SET_UNLOCKED_HAIRSTYLE, (position, item, tokensAmount) =>
  ({ position, item, tokensAmount })))

export const setUnlockedHonor = createAction(actionTypes.SET_UNLOCKED_HONOR, (tokens, honor) => ({ tokens, honor }))

export const setSelectedHairstyle = createAction(actionTypes.SET_SELECTED_HAIRSTYLE, (ID, removedHairstyleID) =>
  ({ ID, removedHairstyleID }))

export const setRemovedHairstyle = createAction(actionTypes.SET_REMOVED_HAIRSTYLE, ID => ({ ID }))

export const setUnlockedBackdrop = createAction(actionTypes.SET_UNLOCKED_BACKDROP, (position, item, tokensAmount) =>
  ({ position, item, tokensAmount }))

export const setRemovedBackdrop = createAction(actionTypes.SET_REMOVED_BACKDROP, ID => ({ ID }))

export const setSelectedBackdrop = createAction(actionTypes.SET_SELECTED_BACKDROP, ID => ({ ID }))

export const setAwardOnHold = createAction(actionTypes.SET_AWARD_ON_HOLD, (position, onHold, awardType) =>
  ({ position, onHold, awardType }))

export const setHonorOnHold = createAction(actionTypes.SET_HONOR_ON_HOLD, (ID, onHold) => ({ ID, onHold }))

export const toggleAwardsConfirmation =
  createAction(actionTypes.TOGGLE_AWARDS_CONFIRMATION, (position, showConfirmation, awardType) =>
    ({ position, showConfirmation, awardType }))

export const toggleHonorsConfirmation = createAction(actionTypes.TOGGLE_HONORS_CONFIRMATION, (ID, showConfirmation) =>
  ({ ID, showConfirmation }))
