# RUSSIAN-ROULETTE - Torn

## 3.2.0
 * Added live user money update (WS).
 * Added ability to set/unset honor bars to appear.
 * Fixed sounds toggling in Safari.
 * Fixed list update.
 * Fixed start value for bet.
 * Fixed timer synchronization.
 * Fixed sounds mute on the page without gestures.
 * Fixed 1b shortcut on the bet input.
 * Fixed phantom games duplication in the lobby list.

## 3.1.0
 * Improved routing logic.
 * Created skeletons and preloader for Lobby.

## 3.0.0
 * Created first stable Lobby UI.

## 2.0.0
 * Created first stable structure.
 * Added assets and data preloaders.
 * Added app header.

## 1.0.0
 * Initial release.
