import AppHeader from '@torn/shared/components/AppHeader'
import DebugBox from '@torn/shared/components/DebugBox'
import InfoBox from '@torn/shared/components/InfoBox'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'
import React, { Dispatch } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Store } from 'redux'
import ChipLoader from '../components/ChipLoader'
import Tooltips from '../components/Tooltips'
import { APP_HEADER } from '../constants'
import { IMG_ASSETS } from '../constants/assets'
import { notifySoundRun } from '../controller/actions/audio'
import { checkManualDesktopMode, debugHide, infoShow } from '../controller/actions/common'
import { initApp, loadInitAssetsSaved } from '../controller/actions/main'
import { IProps } from '../interfaces/ILayout'
import { IState } from '../interfaces/IState'
import styles from './index.cssmodule.scss'

class AppLayout extends React.Component<IProps> {
  componentDidMount() {
    this.props.initApp()
    this._checkManualDesktopMode()

    this._startSoundInitHandlers()
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()

    this._removeSoundInitHandlers()
  }

  _checkManualDesktopMode = () => {
    const { checkManualMode } = this.props

    subscribeOnDesktopLayout((payload: boolean) => checkManualMode(payload))
  }

  _soundInitHandlers = () => {
    if (!this.props.isSoundsNativeRun) {
      this.props.notifySoundRun()
    }
  }

  _startSoundInitHandlers = () => {
    if (!this.props.isSoundsNativeRun) {
      document.addEventListener('touchstart', this._soundInitHandlers, true)
      document.addEventListener('click', this._soundInitHandlers, true)
      document.addEventListener('keydown', this._soundInitHandlers, true)
    }
  }

  _removeSoundInitHandlers = () => {
    document.removeEventListener('touchstart', this._soundInitHandlers)
    document.removeEventListener('click', this._soundInitHandlers)
    document.removeEventListener('keydown', this._soundInitHandlers)
  }

  _renderHeader = () => {
    const { appID, pageID } = this.props

    return <AppHeader appID={appID} pageID={pageID} clientProps={APP_HEADER} />
  }

  _renderInfoDebugAreas = () => {
    const { debug, debugCloseAction, info } = this.props

    return (
      <React.Fragment>
        {debug && <DebugBox debugMessage={debug} close={debugCloseAction} isBeatifyError={true} />}
        {info && <InfoBox msg={info} />}
      </React.Fragment>
    )
  }

  _renderBody = () => {
    const { children, location } = this.props

    // use React.cloneElement and location.hash
    // to switch routes on internal push() redirects
    // otherwise redirects will work only on <Link to='' /> clicks
    const routerCustomConfig = {
      location: {
        ...location,
        pathname: location.hash.replace(/^#/i, '')
      }
    }

    return <div className={styles.bodyWrap}>{React.cloneElement(children, routerCustomConfig)}</div>
  }

  _renderTooltips = () => {
    return <Tooltips />
  }

  _renderPreloader = () => {
    const { assetsLoaded } = this.props

    return <ChipLoader assets={IMG_ASSETS} onLoadFinish={assetsLoaded} />
  }

  render() {
    const { isDataLoaded, isAssetsLoaded } = this.props

    if (!isDataLoaded || !isAssetsLoaded) {
      return (
        <React.Fragment>
          {this._renderHeader()}
          {this._renderPreloader()}
        </React.Fragment>
      )
    }

    return (
      <div className={styles.appContainer}>
        {this._renderTooltips()}
        {this._renderHeader()}
        {this._renderInfoDebugAreas()}
        {this._renderBody()}
      </div>
    )
  }
}

const mapStateToProps = (state: IState & Store<any>) => ({
  appID: state.common.appID,
  pageID: state.common.pageID,
  debug: state.common.debug,
  info: state.common.info,
  mediaType: state.browser.mediaType,
  isDataLoaded: state.main.isDataLoaded,
  isAssetsLoaded: state.main.isAssetsLoaded,
  location: state.router.location,
  isSoundsNativeRun: state.audio.isSoundsNativeRun
})

const mapDispatchToState = (dispatch: Dispatch<any>) => ({
  initApp: () => dispatch(initApp()),
  assetsLoaded: () => dispatch(loadInitAssetsSaved()),
  checkManualMode: (payload: boolean) => dispatch(checkManualDesktopMode({ status: payload })),
  debugCloseAction: () => dispatch(debugHide()),
  infoBoxShow: (msg: string) => dispatch(infoShow(msg)),
  notifySoundRun: () => dispatch(notifySoundRun())
})

const connectAppToReduxStore = connect(mapStateToProps, mapDispatchToState)(AppLayout)

export default withRouter(connectAppToReduxStore)
