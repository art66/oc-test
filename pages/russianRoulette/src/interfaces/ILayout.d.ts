import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import { match, RouteComponentProps } from 'react-router'
import { TAppID } from '../controller/interfaces/ICommon'

export interface IProps extends RouteComponentProps {
  initApp: () => void
  assetsLoaded: () => void
  checkManualMode: (status: boolean) => void
  infoBoxShow: (msg: string) => void
  notifySoundRun: () => void
  isSoundsNativeRun: boolean
  mediaType: TMediaType
  children?: any
  location: any
  match: match<{ myParam: string }>
  isDataLoaded: boolean
  isAssetsLoaded: boolean
  appID: TAppID
  pageID: number
  debug: {
    msg: string | object
  }
  debugCloseAction: () => void
  info: string
}
