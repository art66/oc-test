import { Store } from 'redux'
import { History } from 'history'

export interface IProps {
  store: Store
  history: History<any>
}
