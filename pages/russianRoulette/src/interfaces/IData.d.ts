
export type TGameTypes = 'lobby' | 'acting'
export type TUserTypes = 'creator' | 'onlooker'
