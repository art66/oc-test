import { IInitialState as IAudio } from '../controller/interfaces/IAudio'
import { IInitialState as IMain } from '../controller/interfaces/IMain'
import { IInitialState as ICommon } from '../controller/interfaces/ICommon'
import { IInitialState as IBrowser } from '../controller/interfaces/IBrowser'
import { IInitialState as ILobby } from '../routes/Lobby/interfaces/IModules'
import { IInitialState as IGame } from '../routes/Game/interfaces/IModules'

export interface IState {
  audio: IAudio
  main: IMain
  lobby: ILobby
  game: IGame
  common: ICommon
  browser: IBrowser
  router: any
}
