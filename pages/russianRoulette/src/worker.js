const TIMER_FREQ = 30000

const startWorkerThread = () => {
  setInterval(() => postMessage('legacy poll ticker'), TIMER_FREQ)
}

startWorkerThread()
