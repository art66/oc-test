export const LEAVE_LAYOUT_SWITCH = 'game/LEAVE_LAYOUT_SWITCH'
export const LEAVE_FIGHT_ATTEMPT = 'game/LEAVE_FIGHT_ATTEMPT'
export const LEAVE_FIGHT_SAVED = 'game/LEAVE_FIGHT_SAVED'
export const FIGHT_LEAVED = 'game/FIGHT_LEAVED'
export const TICKER = 'game/TICKER'
export const UPDATE_TIMER = 'game/UPDATE_TIMER'
export const AUTO_LEAVE_FIGHT_ATTEMPT = 'game/AUTO_LEAVE_FIGHT_ATTEMPT'
export const AUTO_LEAVE_FIGHT_SAVED = 'game/AUTO_LEAVE_FIGHT_SAVED'
export const OPPONENT_JOINED = 'game/OPPONENT_JOINED'
export const FIGHT_INITIATED = 'game/FIGHT_INITIATED'
export const SHOT_ATTEMPT = 'game/SHOT_ATTEMPT'
export const SHOT_SAVED = 'game/SHOT_SAVED'
export const TAKE_ACTION_ATTEMPT = 'game/TAKE_ACTION_ATTEMPT'
export const TAKE_ACTION_SAVED = 'game/TAKE_ACTION_SAVED'
export const SET_ANIM_STAGE = 'game/SET_ANIM_STAGE'
export const UPDATE_PATRONS_BEFORE_TAKE_ACTION = 'game/UPDATE_PATRONS_BEFORE_TAKE_ACTION'
export const FIGHT_FINISHED_AFTER_ACTION = 'game/FIGHT_FINISHED_AFTER_ACTION'
export const ANIM_SHOT = 'game/ANIM_SHOT'
export const FORCE_FETCH_ATTEMPT = 'game/FORCE_FETCH_ATTEMPT'
export const FORCE_FETCH_SAVED = 'game/FORCE_FETCH_SAVED'

export const MAX_FIGHT_TIME = 720
export const MAX_ACTION_TIME = 180

export const ANIM_TYPE_BARREL_INIT = 'barrelInit'
export const ANIM_TYPE_BARREL_MISS = 'barrelMiss'
export const ANIM_TYPE_BARREL_FORCE = 'barrelForce'
export const ANIM_TYPE_SHOT_GUN_CLICK = 'shotGunClick'
export const ANIM_TYPE_SHOT_GUN_FORCE = 'shotGunForce'

export const ANIM_STAGE_START = 'start'
export const ANIM_STAGE_PROGRESS = 'progress'
export const ANIM_STAGE_FINISH = 'finish'
export const ANIM_STAGE_RETAKE = 'retake'

export const STEP_STARTED = 'started'
export const STEP_SHOT = 'click'
export const STEP_WON = 'won'
export const STEP_LOSE = 'lost'
export const STEP_RETAKE = 'retake'

export const SIDES = {
  right: 'right',
  left: 'left'
}

export const REVERSE_SIDE = {
  left: SIDES.right,
  right: SIDES.left
}

export const ANIM_COLORS = {
  yellow: 'yellow',
  red: 'red',
  green: 'green'
}

export const TAKE_ACTION_MESSAGE = 'appears to be a little anxious...'
