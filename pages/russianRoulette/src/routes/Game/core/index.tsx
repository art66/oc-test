import numberToArray from '@torn/shared/utils/numberToArray'
import React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { playSound } from '../../../controller/services/sound'
import webworker from '../../../controller/webworkers'
import { IState } from '../../../interfaces/IState'
import {
  ANIM_STAGE_FINISH,
  ANIM_STAGE_PROGRESS,
  ANIM_STAGE_START,
  ANIM_TYPE_BARREL_FORCE,
  ANIM_TYPE_BARREL_INIT,
  ANIM_TYPE_BARREL_MISS,
  ANIM_TYPE_SHOT_GUN_CLICK,
  ANIM_TYPE_SHOT_GUN_FORCE
} from '../constants'
import { GameLayout } from '../GameLayout'
import { TAnimStages, TAnimTypes, TFightResults } from '../interfaces/IModules'
import {
  animateShot,
  autoLeaveFightAttempt,
  finishFightAfterAction,
  forceFetchAttempt,
  setTransitionStage,
  updatePatronsOnTakeAction
} from '../modules/actions'
import checkFightStep from '../utils/checkFightStep'
import styles from './index.cssmodule.scss'

export interface IProps {
  time: number
  step: TFightResults
  whoseMove: number
  animType: TAnimTypes
  forceFetch: () => void
  autoLeaveFight: () => void
  finishFight: () => void
  setAnimStage: (animStage: TAnimTypes, animType: TAnimStages) => void
  updatePatrons: () => void
  makeShoot: (movesNext: number) => void
  result: TFightResults
  movesTemp: {
    before: number
    diff: number
    next: number
  }
  moves: {
    before: number
    diff: number
    next: number
  }
  missClicks: number
  isSound: boolean
  isSoundsNativeRun: boolean
  isPreFinish: boolean
}

class Core extends React.Component<IProps> {
  private _isTimerRun: boolean
  private _isFightLeaved: boolean
  private _timerInitID: any
  private _timerShotID: any
  private _timerIterateID: any
  private _timerFinish: any
  private _webworker: any
  private _ref: React.RefObject<HTMLDivElement>

  constructor(props: IProps) {
    super(props)

    this._ref = React.createRef()
  }

  componentDidMount() {
    const { step } = this.props
    const { isStarted } = checkFightStep(step)

    isStarted && this._initTransition()

    this._startLegacySavePolling()

    this._ref.current?.focus()

    // ensure we don't lost anything by leaving the page
    // and returning back after a short/while
    window.addEventListener('focus', this._startForceFetch)
  }

  componentDidUpdate(prevProps: IProps) {
    this._manageAutoLeave()
    this._manageFightAnimations(prevProps)
    this._manageShots()
    this._stopPooling()
  }

  componentWillUnmount() {
    this._timerInitID && clearInterval(this._timerInitID)
    this._timerShotID && clearInterval(this._timerShotID)
    this._timerIterateID && clearInterval(this._timerIterateID)
    this._timerFinish && clearInterval(this._timerFinish)

    this._webworker && this._removeLegacySavePolling()

    window.removeEventListener('focus', this._startForceFetch)
  }

  _startForceFetch = () => {
    const { forceFetch } = this.props

    forceFetch()
  }

  _startLegacySavePolling = () => {
    const { forceFetch } = this.props

    this._webworker = webworker({ callback: forceFetch })
  }

  _stopPooling = () => {
    const { time } = this.props

    if (this._webworker && time <= 0) {
      this._removeLegacySavePolling()
    }
  }

  _removeLegacySavePolling = () => {
    this._webworker.terminate()
    this._webworker = null
  }

  _manageAutoLeave = () => {
    const { time, autoLeaveFight } = this.props

    // in case no one take action for
    // the last 15min the fight should be over
    if (time <= 0 && !this._isFightLeaved) {
      autoLeaveFight()

      this._isFightLeaved = true
    }
  }

  _manageFightAnimations = (prevProps: IProps) => {
    const { step, whoseMove, isPreFinish } = this.props
    const { isStarted, isShoot, isRetake } = checkFightStep(step)

    const isStepChanged = prevProps.step !== step
    const isPlayerChanged = prevProps.whoseMove !== whoseMove
    const isPreFinishChanged = prevProps.isPreFinish !== isPreFinish

    if (isStepChanged && isRetake) {
      // otherwise if force shot commit make its animation as well
      this._takeActionTransition()
    } else if ((isPlayerChanged || isStepChanged || isPreFinishChanged) && isShoot) {
      // each shot animation or force shot on take action
      this._shotTransition()
    } else if (isStepChanged && isStarted) {
      // initial animation
      this._initTransition()
    }
  }

  _manageShots = () => {
    const { moves, step } = this.props
    const { isFinished } = checkFightStep(step)

    const movesBefore = moves?.before
    const movesNext = moves?.next
    const isShotMade = movesNext - movesBefore > 0

    if (isShotMade && !this._isTimerRun && !isFinished) {
      const shotsArray = numberToArray(moves?.diff)

      shotsArray.forEach(this._shotsIterator)

      this._isTimerRun = true
    }
  }

  _shotsIterator = (key: number) => {
    this._timerIterateID = setTimeout(() => {
      const { makeShoot, moves, isPreFinish, step, finishFight } = this.props

      const { isRetake, isFinished } = checkFightStep(step)

      const isLastShot = moves?.diff === key
      const isGameOver = isFinished || isRetake || isPreFinish

      if (isLastShot) {
        this._isTimerRun = false
      }

      if (isLastShot && isGameOver) {
        finishFight()
        playSound('bang')

        return
      }

      makeShoot(1)
      playSound('blank')
    }, 250 * (key ? key - 1 : key))
  }

  _initTransition = () => {
    const { setAnimStage } = this.props

    setAnimStage('', ANIM_STAGE_START)
    playSound('join')

    this._timerInitID = setTimeout(() => {
      setAnimStage(ANIM_TYPE_BARREL_INIT, ANIM_STAGE_PROGRESS)

      setTimeout(() => {
        setAnimStage(ANIM_TYPE_SHOT_GUN_CLICK, ANIM_STAGE_FINISH)
      }, 3000)
    }, 5000)
  }

  _shotTransition = () => {
    const { setAnimStage, isPreFinish, moves } = this.props

    const finishTransition = () => {
      if (moves?.next - moves?.before > 1) {
        setAnimStage(ANIM_TYPE_BARREL_FORCE, ANIM_STAGE_START)

        return
      }

      setAnimStage(ANIM_TYPE_SHOT_GUN_FORCE, ANIM_STAGE_FINISH)
    }

    const shotTransition = () => {
      setAnimStage(ANIM_TYPE_BARREL_MISS, ANIM_STAGE_START)

      this._timerShotID = setTimeout(() => {
        setAnimStage(ANIM_TYPE_BARREL_MISS, ANIM_STAGE_PROGRESS)

        this._timerShotID = setTimeout(() => {
          setAnimStage(ANIM_TYPE_SHOT_GUN_CLICK, ANIM_STAGE_FINISH)
        }, 3000)
      }, 3000)
    }

    if (isPreFinish) {
      finishTransition()

      return
    }

    shotTransition()
  }

  _takeActionTransition = () => {
    const { setAnimStage, updatePatrons, finishFight, movesTemp } = this.props

    setAnimStage(ANIM_TYPE_SHOT_GUN_FORCE, 'start')

    this._timerShotID = setTimeout(() => {
      if (movesTemp?.before !== movesTemp?.next && movesTemp?.diff > 1) {
        updatePatrons() // update moves on the client side before barrel clicks
        setAnimStage(ANIM_TYPE_BARREL_FORCE, 'progress')
      } else {
        finishFight()
        playSound('bang')
        setAnimStage(ANIM_TYPE_SHOT_GUN_FORCE, 'finish')
      }
    }, 3000)
  }

  _renderSoundsWarningPopup = () => {
    const { isSoundsNativeRun } = this.props

    if (isSoundsNativeRun) {
      return null
    }

    const LABEL = 'Please click or tap anywhere to enable sound!'

    return (
      <div ref={this._ref} tabIndex={0} aria-label={LABEL} className={styles.soundsInitPopup}>
        <span className={styles.soundsInitPopupLabel}>{LABEL}</span>
      </div>
    )
  }

  render() {
    return (
      <React.Fragment>
        {this._renderSoundsWarningPopup()}
        <GameLayout />
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({ main, game, audio }: IState) => ({
  time: main.gameData?.timeLeft,
  step: main.gameData?.result,
  whoseMove: main.gameData?.whoseMove,
  animType: game.animation.type,
  result: main.gameData?.result,
  moves: main.gameData?.moves,
  movesTemp: main.gameData?.temp?.moves,
  isPreFinish: !!main.gameData?.temp?.winner,
  isSoundsNativeRun: audio.isSoundsNativeRun
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  forceFetch: () => dispatch(forceFetchAttempt()),
  autoLeaveFight: () => dispatch(autoLeaveFightAttempt()),
  setAnimStage: (type: TAnimTypes, stage: TAnimStages) => dispatch(setTransitionStage(type, stage)),
  updatePatrons: () => dispatch(updatePatronsOnTakeAction()),
  finishFight: () => dispatch(finishFightAfterAction()),
  makeShoot: (movesNext: number) => dispatch(animateShot(movesNext))
})

export default connect(mapStateToProps, mapDispatchToState)(Core)
