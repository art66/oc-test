import { STEP_LOSE, STEP_RETAKE, STEP_SHOT, STEP_STARTED, STEP_WON } from '../constants'
import { TFightResults } from '../interfaces/IModules'

const checkFightStep = (step: TFightResults) => {
  return {
    isStarted: step === STEP_STARTED,
    isShoot: step === STEP_SHOT,
    isWon: step === STEP_WON,
    isLose: step === STEP_LOSE,
    isFinished: step === STEP_LOSE || step === STEP_WON,
    isRetake: step === STEP_RETAKE
  }
}

export default checkFightStep
