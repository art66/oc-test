import isArray from '@torn/shared/utils/isArray'
import isObject from '@torn/shared/utils/isObject'
import { ILobbyJoinRowData } from '../../Lobby/interfaces/IModules'

const enhancedGameListUpdate = (stateGameData: ILobbyJoinRowData[], actionGameData: ILobbyJoinRowData) => {
  const isSomeGameOverlapped = isArray(stateGameData) ?
    stateGameData?.some(game => game.creator.name === actionGameData.creator.name) :
    false

  const updateOverlappedGame = (game: ILobbyJoinRowData) =>
    (game.creator.name === actionGameData.creator.name ?
      {
        ...actionGameData
      } :
      game)

  const createNewGameList = () => {
    const newGameList = [...(isArray(stateGameData) ? stateGameData : [])]

    if (isObject(actionGameData)) {
      newGameList.push(actionGameData)
    }

    return newGameList
  }

  return isSomeGameOverlapped ? stateGameData.map(updateOverlappedGame) : createNewGameList()
}

export default enhancedGameListUpdate
