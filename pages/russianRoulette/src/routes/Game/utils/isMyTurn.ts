import getUserID from '@torn/shared/utils/getUserID'

const isMyTurn = (activeUserID: number) => {
  return activeUserID === Number(getUserID())
}

export default isMyTurn
