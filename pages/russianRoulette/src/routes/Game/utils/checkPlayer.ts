import getUserID from '@torn/shared/utils/getUserID'

const checkPlayer = (creatorID: number) => {
  return {
    isCreator: Number(getUserID()) === creatorID,
    isOpponent: Number(getUserID()) !== creatorID
  }
}

export default checkPlayer
