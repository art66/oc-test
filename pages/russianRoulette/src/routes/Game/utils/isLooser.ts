import getUserID from '@torn/shared/utils/getUserID'

const isLooser = (winner: number) => winner && winner !== Number(getUserID())

export default isLooser
