export interface IMoves {
  before: number
  diff: number
  next: number
}

const normalizeMoves = (movesPrev: IMoves, movesNext: number): IMoves => {
  const movesBeforeTemp = movesPrev?.before || 0
  const movesNextTemp = movesNext || 0

  return {
    before: movesBeforeTemp,
    diff: movesNextTemp - movesBeforeTemp,
    next: movesNextTemp
  }
}

export default normalizeMoves
