import { SIDES } from '../constants'
import checkPlayer from './checkPlayer'
import whoseTurn from './isMyTurn'

export interface ISide {
  activeUserID: number
  creatorID: number
  winner?: number
}

const getSide = (config: ISide) => {
  const { activeUserID, creatorID, winner } = config

  const { isCreator } = checkPlayer(creatorID)
  const isMyTurn = whoseTurn(activeUserID)

  const calcShot = () => {
    if (winner && winner === creatorID) {
      // fight is over and creator is lose
      return SIDES.left
    } else if (winner && winner !== creatorID) {
      // fight is over and creator is lose
      return SIDES.right
    } else if (!isCreator && !isMyTurn) {
      // creator shall make a shot
      return SIDES.right
    } else if (isCreator && isMyTurn) {
      // creator shall made a shot
      return SIDES.right
    }

    return SIDES.left
  }

  return {
    model: isCreator ? SIDES.right : SIDES.left,
    shot: calcShot(),
    buttons: isCreator || (isCreator && !isMyTurn) ? SIDES.right : SIDES.left
  }
}

export default getSide
