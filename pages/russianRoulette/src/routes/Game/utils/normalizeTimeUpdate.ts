const normalizeTimeUpdate = (timeLeft: number, timeLeftTillTakeAction: number) => {
  return {
    timeLeft: timeLeft > 0 ? timeLeft - 1 : 0,
    timeLeftTillTakeAction: timeLeftTillTakeAction > 0 ? timeLeftTillTakeAction - 1 : 0
  }
}

export default normalizeTimeUpdate
