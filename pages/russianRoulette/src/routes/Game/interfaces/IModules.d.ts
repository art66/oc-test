import { TUserTypes } from '../../../interfaces/IData'

export interface IUser {
  ID: number
  name: string
  img: string
}

export interface IFightCommonData {
  ID: number
  betAmount: number
  creator: IUser
  opponent: IUser
  message: string
  lastAction: number
  skullPosition: number
  missClicks: number
  shotsLeft: number
  shotsAtLastTurn: number
  shotsAvailable: TShootCount
  timeLeft: number
  timeLeftTillTakeAction: number
  timeCreated: number
  timeStarted: number
  whoseMove: number
  winner: number
  result: TFightResults
}

export interface IFightPayloadData extends IFightCommonData {
  moves: number
}

export interface IFightData extends IFightCommonData {
  moves: {
    before: number
    diff: number
    next: number
  }
  temp?: {
    result: TFightResults
    winner: number
    moves: {
      before: number
      diff: number
      next: number
    }
  }
}

export interface IFightInitiated {
  data: IFightPayloadData
}

export interface ITimerUpdate {
  newMaxTime: number
  newActionTime: number
}

export interface IOpponentData {
  opponent: {
    ID: number
    name: string
    img: string
  }
  moves: number
  skullPosition: number
  timeLeft: number
  timeLeftTillTakeAction: number
  message: string[]
  result: TFightResults
}

export interface IInitialState {
  lastCommit: number
  isLeaveConfirmation: boolean
  isLeave: boolean
  isShoot: boolean
  isTakeAction: boolean
  animation: {
    type: TAnimTypes
    stage: TAnimStages
  }
}

export interface ISetAnimation {
  animType: TAnimTypes
  animStage: TAnimStages
}

export type TShootCount = 1 | 2 | 3
export type TFightResults = 'started' | 'click' | 'won' | 'lost' | 'retake'
export type TAnimStages = '' | 'start' | 'progress' | 'finish'
export type TAnimTypes = '' | 'barrelInit' | 'barrelMiss' | 'barrelForce' | 'shotGunClick' | 'shotGunForce'
export type TAnimColors = '' | 'yellow' | 'red' | 'green'
export type TAnimSides = 'left' | 'right'

export interface IShootCount {
  count: TShootCount
}

export interface IFightJoined {
  data: IFightPayloadData
}

export interface IGameJoinedWS {
  data: {
    error?: string
    data: IOpponentData & IFightPayloadData
  }
}

export interface IFightInitializedWS {
  data: {
    error?: string
    type: TUserTypes
    data: IFightPayloadData
  }
}

export interface ITakeActionSaved extends ITime {
  moves: number
  message: string[]
  result: TFightResults
  whoseMove: number
  winner: number
  shotsLeft: number
}

export interface ITakeActionWS {
  data: {
    data: ITakeActionSaved
    error?: string
  }
}

export interface IShotSaved extends ITime {
  moves: number
  messages: string[]
  result: TFightResults
  shotsAtLastTurn: number
  shotsAvailable: number
  winner?: number
  whoseMove: number
}

export interface IShotFiredWS {
  data: {
    data: IShotSaved
    error?: string
  }
}

export interface ITime {
  timeLeft: number
  timeLeftTillTakeAction: number
}

export interface ITimeData {
  data: ITime
}
