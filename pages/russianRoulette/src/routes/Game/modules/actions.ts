import { IType } from '../../../controller/interfaces'
import { IInitData } from '../../../controller/interfaces/IMain'
import { ILeaveFightSaved } from '../../Lobby/interfaces/IModules'
import {
  ANIM_SHOT,
  AUTO_LEAVE_FIGHT_ATTEMPT,
  AUTO_LEAVE_FIGHT_SAVED,
  FIGHT_FINISHED_AFTER_ACTION,
  FIGHT_INITIATED,
  FIGHT_LEAVED,
  FORCE_FETCH_ATTEMPT,
  FORCE_FETCH_SAVED,
  LEAVE_FIGHT_ATTEMPT,
  LEAVE_LAYOUT_SWITCH,
  OPPONENT_JOINED,
  SET_ANIM_STAGE,
  SHOT_ATTEMPT,
  SHOT_SAVED,
  TAKE_ACTION_ATTEMPT,
  TAKE_ACTION_SAVED,
  TICKER,
  UPDATE_PATRONS_BEFORE_TAKE_ACTION,
  UPDATE_TIMER
} from '../constants'
import {
  IFightInitiated,
  IOpponentData,
  ISetAnimation,
  IShootCount,
  IShotSaved,
  ITakeActionSaved,
  ITime,
  ITimeData,
  ITimerUpdate,
  TAnimStages,
  TAnimTypes,
  TShootCount
} from '../interfaces/IModules'

export const leaveLayoutSwitch = (): IType => ({
  type: LEAVE_LAYOUT_SWITCH
})

export const leaveFightAttempt = (): IType => ({
  type: LEAVE_FIGHT_ATTEMPT
})

export const fightLeaved = ({ data, step }: ILeaveFightSaved): ILeaveFightSaved & IType => ({
  data,
  step,
  type: FIGHT_LEAVED
})

export const updateTime = (newTime: ITime): ITimeData & IType => ({
  data: {
    ...newTime
  },
  type: TICKER
})

export const setTime = (newMaxTime: number, newActionTime: number): ITimerUpdate & IType => ({
  newMaxTime,
  newActionTime,
  type: UPDATE_TIMER
})

export const autoLeaveFightAttempt = (): IType => ({
  type: AUTO_LEAVE_FIGHT_ATTEMPT
})

export const autoLeaveFightSaved = (payload: ILeaveFightSaved): ILeaveFightSaved & IType => ({
  data: payload.data,
  step: payload.step,
  type: AUTO_LEAVE_FIGHT_SAVED
})

export const opponentJoined = (data: IOpponentData): { payload: IOpponentData } & IType => ({
  payload: data,
  type: OPPONENT_JOINED
})

export const fightInitiated = (payload: IFightInitiated): IFightInitiated & IType => ({
  data: payload.data,
  type: FIGHT_INITIATED
})

export const makeShotAttempt = (count: TShootCount): IShootCount & IType => ({
  count,
  type: SHOT_ATTEMPT
})

export const makeShotSaved = (payload: IShotSaved): { payload: IShotSaved } & IType => ({
  payload,
  type: SHOT_SAVED
})

export const takeActionAttempt = (): IType => ({
  type: TAKE_ACTION_ATTEMPT
})

export const takeActionSaved = (payload: ITakeActionSaved): { payload: ITakeActionSaved } & IType => ({
  payload,
  type: TAKE_ACTION_SAVED
})

export const setTransitionStage = (animType: TAnimTypes, animStage: TAnimStages): ISetAnimation & IType => ({
  animType,
  animStage,
  type: SET_ANIM_STAGE
})

export const finishFightAfterAction = (): IType => ({
  type: FIGHT_FINISHED_AFTER_ACTION
})

export const updatePatronsOnTakeAction = (): IType => ({
  type: UPDATE_PATRONS_BEFORE_TAKE_ACTION
})

export const animateShot = (movesNext: number): { movesNext: number } & IType => ({
  movesNext,
  type: ANIM_SHOT
})

export const forceFetchAttempt = (): IType => ({
  type: FORCE_FETCH_ATTEMPT
})

export const forceFetchSaved = (payload: IInitData): IInitData & IType => ({
  ...payload,
  type: FORCE_FETCH_SAVED
})
