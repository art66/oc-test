import { IType } from '../../../controller/interfaces'
import {
  ANIM_STAGE_FINISH,
  ANIM_TYPE_SHOT_GUN_FORCE,
  AUTO_LEAVE_FIGHT_ATTEMPT,
  AUTO_LEAVE_FIGHT_SAVED,
  FIGHT_FINISHED_AFTER_ACTION,
  FIGHT_LEAVED,
  LEAVE_FIGHT_ATTEMPT,
  LEAVE_LAYOUT_SWITCH,
  SET_ANIM_STAGE,
  SHOT_ATTEMPT,
  SHOT_SAVED,
  TAKE_ACTION_ATTEMPT,
  TAKE_ACTION_SAVED
} from '../constants'
import { IInitialState, ISetAnimation } from '../interfaces/IModules'
import initialState from './initialState'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [LEAVE_LAYOUT_SWITCH]: (state: IInitialState) => ({
    ...state,
    isLeaveConfirmation: !state.isLeaveConfirmation
  }),
  [LEAVE_FIGHT_ATTEMPT]: (state: IInitialState) => ({
    ...state,
    isLeave: true
  }),
  [FIGHT_LEAVED]: (state: IInitialState) => ({
    ...state,
    isLeave: false,
    isLeaveConfirmation: false
  }),
  [SHOT_ATTEMPT]: (state: IInitialState) => ({
    ...state,
    isShoot: true
  }),
  [SHOT_SAVED]: (state: IInitialState) => ({
    ...state,
    isShoot: false,
    lastCommit: Date.now() // temp solution. TODO: remove after pooling will be removed
  }),
  [TAKE_ACTION_ATTEMPT]: (state: IInitialState) => ({
    ...state,
    isTakeAction: true
  }),
  [TAKE_ACTION_SAVED]: (state: IInitialState) => ({
    ...state,
    isTakeAction: false,
    lastCommit: Date.now() // temp solution. TODO: remove after pooling will be removed
  }),
  [AUTO_LEAVE_FIGHT_ATTEMPT]: (state: IInitialState) => ({
    ...state,
    isLeave: true
  }),
  [AUTO_LEAVE_FIGHT_SAVED]: (state: IInitialState) => ({
    ...state,
    isLeave: false
  }),
  [SET_ANIM_STAGE]: (state: IInitialState, action: ISetAnimation & IType) => ({
    ...state,
    animation: {
      type: action.animType,
      stage: action.animStage
    }
  }),
  [FIGHT_FINISHED_AFTER_ACTION]: (state: IInitialState) => ({
    ...state,
    animation: {
      ...state.animation,
      type: ANIM_TYPE_SHOT_GUN_FORCE,
      stage: ANIM_STAGE_FINISH
    }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
