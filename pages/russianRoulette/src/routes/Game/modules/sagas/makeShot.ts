import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { GENERIC_ERROR } from '../../../../constants/errors'
import { debugShow } from '../../../../controller/actions/common'

function* makeShot({ count }: any) {
  let payload = null

  try {
    const dataConfig = {
      sid: 'russianRouletteData',
      step: 'makeTurn',
      shotsAmount: count
    }

    payload = yield fetchUrl('/page.php', dataConfig)
  } catch (e) {
    yield put(debugShow(GENERIC_ERROR))

    throw new Error(`${GENERIC_ERROR}: ${e}`)
  }

  if (payload.error) {
    yield put(debugShow(payload.error))

    throw new Error(payload.error)
  }
}

export default makeShot
