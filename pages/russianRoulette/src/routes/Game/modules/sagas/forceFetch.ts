import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { ROOT_URL } from '../../../../constants'
import { GENERIC_ERROR } from '../../../../constants/errors'
import { debugShow } from '../../../../controller/actions/common'
import getState from '../../../../controller/helpers/getState'
import { IState } from '../../../../interfaces/IState'
import { forceFetchSaved } from '../actions'

const normalizeTime = (time: number) => Number((time / 1000).toFixed(0))

function* forceFetch() {
  const { game }: IState = yield getState()

  const { lastCommit: datePrev } = game
  const dateNow = normalizeTime(Date.now())

  if (dateNow - normalizeTime(datePrev) < 30) {
    // too early to start a force request if some one was made before!
    return
  }

  let payload = null

  try {
    payload = yield fetchUrl(`${ROOT_URL}&log=true`)
  } catch (e) {
    yield put(debugShow(GENERIC_ERROR))

    throw new Error(`${GENERIC_ERROR}: ${e}`)
  }

  if (payload.error) {
    yield put(debugShow(payload.error))

    throw new Error(payload.error)
  }

  yield put(forceFetchSaved(payload))
}

export default forceFetch
