import { takeLatest } from 'redux-saga/effects'
import {
  AUTO_LEAVE_FIGHT_ATTEMPT,
  FORCE_FETCH_ATTEMPT,
  LEAVE_FIGHT_ATTEMPT,
  SHOT_ATTEMPT,
  TAKE_ACTION_ATTEMPT
} from '../../constants'
import autoLeaveFight from './autoLeaveFight'
import forceFetch from './forceFetch'
import leaveFight from './leaveFight'
import makeShot from './makeShot'
import takeAction from './takeAction'

export default function* rootSaga() {
  yield takeLatest(LEAVE_FIGHT_ATTEMPT, leaveFight)
  yield takeLatest(AUTO_LEAVE_FIGHT_ATTEMPT, autoLeaveFight)
  yield takeLatest(SHOT_ATTEMPT, makeShot)
  yield takeLatest(TAKE_ACTION_ATTEMPT, takeAction)
  yield takeLatest(FORCE_FETCH_ATTEMPT, forceFetch)
}
