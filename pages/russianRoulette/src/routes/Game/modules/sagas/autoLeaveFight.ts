import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { GENERIC_ERROR } from '../../../../constants/errors'
import { debugShow } from '../../../../controller/actions/common'
import { autoLeaveFightSaved } from '../actions'

function* autoLeaveFight() {
  let payload = null

  try {
    payload = yield fetchUrl('/page.php', { sid: 'russianRouletteData' })
  } catch (e) {
    yield put(debugShow(GENERIC_ERROR))

    throw new Error(`${GENERIC_ERROR}: ${e}`)
  }

  if (payload.data?.timeLeft === 0) {
    yield autoLeaveFight()

    return
  }

  if (payload.error) {
    yield put(debugShow(payload.error))

    throw new Error(payload.error)
  }

  yield put(autoLeaveFightSaved(payload))
}

export default autoLeaveFight
