import { Dispatch } from 'react'
import { debugShow } from '../../../controller/actions/common'
import { IFightLeavedWS } from '../../Lobby/interfaces/IModules'
import { IShotFiredWS, ITakeActionWS } from '../interfaces/IModules'
import { fightLeaved, makeShotSaved, takeActionSaved } from './actions'

// you can subscribe on the user channel only once per page.
// use global in-game WS initialization and throw the callback below there
const gameLeft = (payload: IFightLeavedWS, dispatch: Dispatch<any>) => {
  if (payload.data.error) {
    dispatch(debugShow(payload.data.error))

    return
  }

  dispatch(fightLeaved(payload.data))
}

const shotFired = (payload: IShotFiredWS, dispatch: Dispatch<any>) => {
  if (payload.data.error) {
    dispatch(debugShow(payload.data.error))

    return
  }

  dispatch(makeShotSaved(payload.data.data))
}

const actionTaken = (payload: ITakeActionWS, dispatch: Dispatch<any>) => {
  if (payload.data.error) {
    dispatch(debugShow(payload.data.error))

    return
  }

  dispatch(takeActionSaved(payload.data.data))
}

export { gameLeft, shotFired, actionTaken }

// export default initWS
