import { IInitialState } from '../interfaces/IModules'

const initialState: IInitialState = {
  lastCommit: null,
  isLeaveConfirmation: false,
  isLeave: false,
  isShoot: false,
  isTakeAction: false,
  animation: {
    stage: '',
    type: ''
  }
}

export default initialState
