import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import Body from '../components/Body'

const mapStateToProps = ({ main, browser, common }: IState) => ({
  isDesktop: browser.is.desktop || common.isDesktopManualLayout,
  creator: main.gameData?.creator,
  opponent: main.gameData?.opponent,
  step: main.gameData?.result,
  winner: main.gameData?.winner,
  tempWinner: main.gameData?.temp?.winner,
  activeUserID: main.gameData?.whoseMove
})

export default connect(mapStateToProps, null)(Body)
