import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IState } from '../../../interfaces/IState'
import Leave from '../components/Leave'
import { leaveFightAttempt, leaveLayoutSwitch } from '../modules/actions'

const mapStateToProps = ({ game }: IState) => ({
  isLeaveConfirmation: game.isLeaveConfirmation,
  isLeave: game.isLeave
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  switchLeaveLayout: () => dispatch(leaveLayoutSwitch()),
  leaveFight: () => dispatch(leaveFightAttempt())
})

export default connect(mapStateToProps, mapDispatchToState)(Leave)
