import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import FightProgress from '../components/FightProgress'
import { finishFightAfterAction } from '../modules/actions'

const mapStateToProps = ({ main, game }: IState) => ({
  step: main.gameData?.result,
  animType: game.animation.type,
  whoseMove: main.gameData?.whoseMove,
  opponent: main.gameData?.opponent,
  shotsCount: main.gameData?.moves?.before,
  missClicks: main.gameData?.missClicks,
  activeUserID: main.gameData?.whoseMove,
  creatorID: main.gameData?.creator?.ID,
  winner: main.gameData?.winner,
  winnerTemp: main.gameData?.temp?.winner,
  skullPosition: main.gameData?.skullPosition
})

const mapDispatchToState = dispatch => ({
  finishFight: () => dispatch(finishFightAfterAction())
})

export default connect(mapStateToProps, mapDispatchToState)(FightProgress)
