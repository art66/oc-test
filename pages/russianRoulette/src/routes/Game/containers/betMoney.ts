import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import BetMoney from '../components/BetMoney'

const mapStateToProps = ({ main }: IState) => ({
  betAmount: main.gameData?.betAmount
})

export default connect(mapStateToProps, null)(BetMoney)
