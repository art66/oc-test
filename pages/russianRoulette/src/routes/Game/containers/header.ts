import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import Header from '../components/Header'

const mapStateToProps = ({ browser, main, common }: IState) => ({
  isDesktop: browser.is.desktop || common.isDesktopManualLayout,
  shotsCount: main.gameData?.moves?.diff ? main.gameData?.moves?.before : main.gameData?.moves?.next,
  opponent: main.gameData?.opponent,
  winner: main.gameData?.winner,
  winnerTemp: main.gameData?.temp?.winner
})

export default connect(mapStateToProps, null)(Header)
