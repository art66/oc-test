import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import UserCard from '../components/UserCard'

const mapStateToProps = ({ main, game }: IState) => ({
  animStage: game.animation.stage,
  step: main.gameData?.result
})

export default connect(mapStateToProps, null)(UserCard)
