import { Dispatch } from 'react'
import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import ShootButtons from '../components/ShootButtons'
import { TShootCount } from '../interfaces/IModules'
import { autoLeaveFightAttempt, makeShotAttempt, takeActionAttempt } from '../modules/actions'

const mapStateToProps = ({ main, game, browser, common }: IState) => ({
  isDesktop: browser.is.desktop || common.isDesktopManualLayout,
  opponent: main.gameData?.opponent,
  animStage: game.animation.stage,
  count: main.gameData?.shotsAvailable,
  activeUserID: main.gameData?.whoseMove,
  timeLeftTillTakeAction: main.gameData?.timeLeftTillTakeAction,
  creatorID: main.gameData?.creator?.ID,
  winner: main.gameData?.winner,
  isTakeAction: game.isTakeAction,
  isShoot: game.isShoot,
  isLeave: game.isLeave
})

const mapDispatchToState = (dispatch: Dispatch<any>) => ({
  makeShot: (count: TShootCount) => dispatch(makeShotAttempt(count)),
  leaveFight: () => dispatch(autoLeaveFightAttempt()),
  takeAction: () => dispatch(takeActionAttempt())
})

export default connect(mapStateToProps, mapDispatchToState)(ShootButtons)
