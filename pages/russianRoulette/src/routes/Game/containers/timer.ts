import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IState } from '../../../interfaces/IState'
import Timer from '../components/Header/Timer'
import { ITime } from '../interfaces/IModules'
import { updateTime } from '../modules/actions'

const mapStateToProps = ({ main }: IState) => ({
  time: main.gameData?.timeLeft,
  actionTime: main.gameData?.timeLeftTillTakeAction,
  opponent: main.gameData?.opponent,
  winner: main.gameData?.winner
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  tickerUpdate: (newTime?: ITime) => dispatch(updateTime(newTime))
})

export default connect(mapStateToProps, mapDispatchToState)(Timer)
