import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import Footer from '../components/Footer'

const mapStateToProps = ({ browser, main, common }: IState) => ({
  isDesktop: browser.is.desktop || common.isDesktopManualLayout,
  opponent: main.gameData?.opponent
})

export default connect(mapStateToProps, null)(Footer)
