import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import Message from '../components/Header/Message'

const mapStateToProps = ({ main, game }: IState) => ({
  message: main.gameData?.message,
  result: main.gameData?.result,
  animStage: game.animation.stage,
  activeUserID: main.gameData?.whoseMove,
  timeLeftTillTakeAction: main.gameData?.timeLeftTillTakeAction,
  creatorID: main.gameData?.creator?.ID,
  creatorName: main.gameData?.creator?.name,
  opponentName: main.gameData?.opponent?.name
})

export default connect(mapStateToProps, null)(Message)
