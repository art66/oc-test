import React from 'react'
import Loadable from 'react-loadable'
import Preloader from '../../components/Preloader'
import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/services/rootReducer'
import reducer from './modules/reducers'

// import initWS from './modules/websockets'

const Preload = () => <Preloader />

const GameRoot = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const GameRootComponent = await import(/* webpackChunkName: "game" */ './core/index')

    injectReducer(rootStore, { reducer, key: 'game' })

    // disabled temporary because of
    // the global initialization on user channel
    // initWS(rootStore.dispatch)

    return GameRootComponent
  },
  loading: Preload
})

export default GameRoot
