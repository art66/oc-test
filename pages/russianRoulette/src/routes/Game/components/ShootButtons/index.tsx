import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import numberToArray from '@torn/shared/utils/numberToArray'
import classnames from 'classnames'
import React from 'react'
import { TAnimStages, TShootCount } from '../../interfaces/IModules'
import getSide from '../../utils/getSide'
import isMyTurn from '../../utils/isMyTurn'
// import { REVERSE_SIDE } from '../../constants'
import styles from './index.cssmodule.scss'

export interface IProps {
  opponent: object
  isDesktop: string
  animStage: TAnimStages
  count: TShootCount
  winner: number
  makeShot: (count: TShootCount) => void
  leaveFight: () => void
  takeAction: () => void
  timeLeftTillTakeAction: number
  activeUserID: number
  creatorID: number
  isTakeAction: boolean
  isShoot: boolean
  isLeave: boolean
}

const SHOOT_LABELS = ['shoot', 'x2', 'x3']
const CLASS_LABELS = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6']

class ShootButtons extends React.PureComponent<IProps> {
  private _ref: React.RefObject<HTMLButtonElement>

  constructor(props: IProps) {
    super(props)

    this._ref = React.createRef()
  }

  componentDidMount() {
    this._ref && this._ref.current && this._ref.current.focus()
  }

  _retakeAction = () => {
    const { timeLeftTillTakeAction, winner, opponent, activeUserID } = this.props

    return timeLeftTillTakeAction <= 0 && !isMyTurn(activeUserID) && !winner && opponent
  }

  _classHolder = () => {
    const { isDesktop, count, winner, activeUserID, creatorID, animStage } = this.props
    const isRetakeAction = this._retakeAction()

    const classes = classnames({
      [styles.buttonsWrap]: true,
      [styles.buttonInAnim]: animStage === 'finish',
      [styles[getSide({ activeUserID, creatorID }).buttons]]: isDesktop,
      [styles[`${CLASS_LABELS[count - 1 || 0]}`]]: isDesktop && !isRetakeAction && count && !winner,
      [styles.retakeActionWrap]: isRetakeAction,
      [styles.leaveWrap]: !!winner
    })

    return classes
  }

  _handleClick = (e: React.MouseEvent<HTMLButtonElement> & React.KeyboardEvent<HTMLButtonElement>) => {
    const {
      dataset: { id: ID }
    } = e.target as HTMLButtonElement
    const { takeAction, makeShot, winner, leaveFight, isTakeAction, isLeave, isShoot } = this.props

    if (winner) {
      !isLeave && leaveFight()

      return
    }

    if (this._retakeAction()) {
      !isTakeAction && takeAction()

      return
    }

    !isShoot && makeShot(Number(ID) as TShootCount)
  }

  _handleShotKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleClick
        },
        {
          type: 'space',
          onEvent: this._handleClick
        }
      ]
    })
  }

  // in case the fight is finished
  _renderLeaveButton = () => {
    return (
      <button
        ref={this._ref}
        type='button'
        onClick={this._handleClick}
        onKeyDown={this._handleShotKeydown}
        className='torn-btn'
      >
        leave
      </button>
    )
  }

  // in case opponent do nothing with his gun
  _renderTakeAction = () => {
    return (
      <button
        ref={this._ref}
        type='button'
        onClick={this._handleClick}
        onKeyDown={this._handleShotKeydown}
        className='torn-btn'
      >
        take action
      </button>
    )
  }

  _renderShootButtons = () => {
    const { count } = this.props

    const iteratableButtonsCount = numberToArray(count)

    return iteratableButtonsCount.map((key: number, index: number) => (
      <button
        ref={index === 0 && this._ref}
        data-id={key}
        key={key}
        type='button'
        onClick={this._handleClick}
        onKeyDown={this._handleShotKeydown}
        className={`torn-btn ${styles.commitButton}`}
      >
        {SHOOT_LABELS[index]}
      </button>
    ))
  }

  _getButtonsToRender = () => {
    const { winner } = this.props

    if (winner) {
      return this._renderLeaveButton()
    } else if (this._retakeAction()) {
      return this._renderTakeAction()
    }

    return this._renderShootButtons()
  }

  render() {
    const { animStage, count, winner, activeUserID } = this.props

    const isNotMyTurn = !count || !isMyTurn(activeUserID)
    const isRetakeAction = this._retakeAction()
    const isAnimInProgress = ['start', 'progress'].includes(animStage)

    // no need to render button if empty
    if (isAnimInProgress || (isNotMyTurn && !isRetakeAction && !winner)) {
      return null
    }

    return <div className={this._classHolder()}>{this._getButtonsToRender()}</div>
  }
}

export default ShootButtons
