import React from 'react'
import classnames from 'classnames'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import numberToArray from '@torn/shared/utils/numberToArray'

import getSide from '../../utils/getSide'
import checkFightStep from '../../utils/checkFightStep'

import styles from './index.cssmodule.scss'
import './transition.scss'
import { TFightResults, TAnimTypes } from '../../interfaces/IModules'

export interface IProps {
  step: TFightResults
  animType: TAnimTypes
  opponent: object
  whoseMove: number
  shotsCount: number
  missClicks: number
  activeUserID: number
  creatorID: number
  winner: number
  winnerTemp: number
  skullPosition: number
  finishFight: () => void
}

class FightProgress extends React.PureComponent<IProps> {
  _renderMiss = () => {
    const { shotsCount: missesAmount } = this.props

    if (!missesAmount || missesAmount <= 0) {
      return null
    }

    const missesArr = []

    numberToArray(missesAmount).forEach(key => {
      if (key >= 7) {
        return
      }

      missesArr.push(<i key={key} className={`${styles.miss} ${styles[`pos${key}`]}`} />)
    })

    return missesArr
  }

  _renderSkull = () => {
    const { step, skullPosition } = this.props
    const { isStarted } = checkFightStep(step)

    if (!isStarted) {
      return null
    }

    return <div className={`${styles.skull} ${isStarted ? styles.skullSpin : ''} ${styles[`pos${skullPosition}`]}`} />
  }

  _renderBarrel = () => {
    const { step } = this.props
    const { isStarted } = checkFightStep(step)

    return (
      <div className={styles.charger}>
        <div className={styles.smokeBackdrop} />
        <div className={`${styles.barrel} ${isStarted ? styles.barrelSpin : ''}`}>
          {this._renderSkull()}
          {this._renderMiss()}
        </div>
      </div>
    )
  }

  _renderShotGun = () => {
    const { step, activeUserID, creatorID, winner, winnerTemp } = this.props
    const { isLose, isWon } = checkFightStep(step)
    const { shot: shotSide } = getSide({
      activeUserID,
      creatorID,
      winner: winner || winnerTemp
    })

    const classes = classnames({
      [styles.shotGun]: true,
      [styles[shotSide]]: true,
      [styles.shotGunFired]: isLose || isWon
    })

    return <div className={classes} />
  }

  _renderAnimations = () => {
    const { animType } = this.props

    if (!animType) {
      return null
    }

    const transitionModels = {
      barrelInit: {
        classes: 'barrelInitTransition',
        timing: { appear: 2000, enter: 2000, exit: 500 },
        component: this._renderBarrel,
        onExited: undefined
      },
      barrelMiss: {
        classes: 'barrelMissTransition',
        timing: { appear: 0, enter: 0, exit: 500 },
        component: this._renderBarrel,
        onExited: undefined
      },
      barrelForce: {
        classes: 'barrelForceTransition',
        timing: { appear: 0, enter: 0, exit: 0 },
        component: this._renderBarrel,
        onExited: undefined
      },
      shotGunClick: {
        classes: 'shotClickTransition',
        timing: { appear: 500, enter: 500, exit: 0 },
        component: this._renderShotGun,
        onExited: undefined
      },
      shotGunForce: {
        classes: 'shotForceTransition',
        timing: { appear: 0, enter: 0, exit: 0 },
        component: this._renderShotGun,
        onExited: undefined
      }
    }

    const { classes, timing, component, onExited } = transitionModels[animType] || {}

    return (
      <TransitionGroup>
        <CSSTransition appear={true} classNames={classes} key={animType} timeout={timing} onExited={onExited}>
          {component()}
        </CSSTransition>
      </TransitionGroup>
    )
  }

  _renderFightStep = () => {
    const { step } = this.props
    const { isStarted, isShoot, isRetake, isFinished } = checkFightStep(step)

    if (isStarted || isShoot || isRetake || isFinished) {
      return this._renderAnimations()
    }

    return this._renderShotGun()
  }

  render() {
    const { opponent } = this.props

    if (!opponent) {
      return null
    }

    return <div className={styles.fightProgressWrap}>{this._renderFightStep()}</div>
  }
}

export default FightProgress
