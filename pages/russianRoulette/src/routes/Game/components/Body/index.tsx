import classnames from 'classnames'
import React from 'react'
import { ANIM_COLORS, SIDES } from '../../constants'
import FightProgress from '../../containers/fightProgress'
import Leave from '../../containers/leave'
import ShootButtons from '../../containers/shootButtons'
import UserCard from '../../containers/userCard'
import { IUser, TAnimColors, TAnimSides, TFightResults } from '../../interfaces/IModules'
import checkFightStep from '../../utils/checkFightStep'
import styles from './index.cssmodule.scss'

export interface IProps {
  isDesktop: boolean
  step: TFightResults
  activeUserID: number
  winner: number
  tempWinner: number
  opponent: IUser
  creator: IUser
}

class Body extends React.Component<IProps> {
  _getCardColor = (user: IUser) => {
    const { winner, activeUserID, step, tempWinner } = this.props
    const { isRetake } = checkFightStep(step)

    if (winner && winner === user?.ID) {
      return ANIM_COLORS.green as TAnimColors
    } else if (winner && winner !== user?.ID) {
      return ANIM_COLORS.red as TAnimColors
    } else if (activeUserID === user?.ID || (isRetake && user?.ID === tempWinner)) {
      return ANIM_COLORS.yellow as TAnimColors
    }

    return '' as TAnimColors
  }

  _renderTitle = (userName: string, profileID: number, cardColor: TAnimColors) => {
    const title = userName || 'Waiting for player...'

    const classes = classnames({
      [styles.title]: true,
      [styles[cardColor || ANIM_COLORS.yellow]]: userName,
      [styles.white]: !userName
    })

    return (
      <div className={styles.titleWrap}>
        <a
          aria-label={`User name: ${title}`}
          href={profileID ? `/profiles.php?XID=${profileID}` : '/#'}
          className={classes}
        >
          {title}
        </a>
      </div>
    )
  }

  _renderCompetitorBlock = () => {
    const { opponent } = this.props

    const cardColor = this._getCardColor(opponent)

    return (
      <div className={styles.userCardBlock}>
        {this._renderTitle(opponent?.name, opponent?.ID, cardColor)}
        <UserCard userImage={opponent?.img} cardColor={cardColor} cardSide={SIDES.left as TAnimSides} />
      </div>
    )
  }

  _renderCreatorBlock = () => {
    const { isDesktop, creator, opponent } = this.props

    const cardColor = this._getCardColor(creator)

    return (
      <div className={styles.userCardBlock}>
        {this._renderTitle(creator?.name, creator?.ID, cardColor)}
        <UserCard userImage={creator?.img} cardColor={cardColor} cardSide={SIDES.right as TAnimSides} />
        {isDesktop && !opponent && <Leave />}
      </div>
    )
  }

  _renderFightProgressBlock = () => {
    return <FightProgress />
  }

  _renderShotButtonsBlock = () => {
    const { isDesktop } = this.props

    if (!isDesktop) {
      return null
    }

    return <ShootButtons />
  }

  render() {
    return (
      <div className={styles.bodyWrap}>
        {this._renderCompetitorBlock()}
        {this._renderFightProgressBlock()}
        {this._renderCreatorBlock()}
        {this._renderShotButtonsBlock()}
      </div>
    )
  }
}

export default Body
