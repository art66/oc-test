import { fetchUrl } from '@torn/shared/utils'
import React from 'react'
import { ITime } from '../../interfaces/IModules'
import styles from './timer.cssmodule.scss'

export interface IProps {
  tickerUpdate: (payload?: ITime) => void
  opponent: object
  actionTime: number
  time: number
  winner: number
}

class Timer extends React.PureComponent<IProps> {
  private _timerID: any

  componentDidMount() {
    const { tickerUpdate } = this.props

    this._timerID = setInterval(() => tickerUpdate(), 1000)

    window.addEventListener('focus', this._synchronizeTimers)
  }

  componentDidUpdate() {
    const { time, winner } = this.props

    if ((time <= 0 || winner) && this._timerID) {
      clearInterval(this._timerID)
    }
  }

  componentWillUnmount() {
    clearInterval(this._timerID)

    window.removeEventListener('focus', this._synchronizeTimers)
  }

  _synchronizeTimers = async () => {
    const { tickerUpdate } = this.props

    const timePayload = await fetchUrl('/page.php?sid=russianRouletteData&step=timers')

    tickerUpdate(timePayload)
  }

  _getTime = () => {
    const { opponent, actionTime, time } = this.props

    const timeToRender = opponent && actionTime ? actionTime : time

    if (!timeToRender) {
      return '00:00'
    }

    const minute = Math.floor(timeToRender / 60)
    const seconds = timeToRender - minute * 60

    const normalizedTime = `${minute < 10 ? `0${minute}` : `${minute}`}:${seconds < 10 ? `0${seconds}` : seconds}`

    return normalizedTime
  }

  _renderTime = () => {
    return (
      <>
        <span className={styles.label}>Starting:</span>
        <span className={styles.counter}>{this._getTime()}</span>
      </>
    )
  }

  render() {
    return <div className={styles.timer}>{this._renderTime()}</div>
  }
}

export default Timer
