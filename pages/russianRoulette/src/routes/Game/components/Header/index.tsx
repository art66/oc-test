import numberToArray from '@torn/shared/utils/numberToArray'
import React from 'react'
import BetMoney from '../../containers/betMoney'
import Message from '../../containers/message'
import Timer from '../../containers/timer'
import styles from './index.cssmodule.scss'
import Pot from './Pot'

const BARREL_STATES = ['idle', 'active', 'finished']
const BULLET_ORDER = {
  1: 'first',
  2: 'second',
  3: 'third',
  4: 'four',
  5: 'five',
  6: 'six'
}

export interface IProps {
  isDesktop: boolean
  shotsCount: number
  opponent: object
  winner: number
  winnerTemp: number
}

class Header extends React.PureComponent<IProps> {
  _getMisses = () => {
    const { opponent, shotsCount } = this.props

    const missesAmount = shotsCount

    if (!missesAmount || missesAmount <= 0) {
      return null
    }

    const misses = opponent ?
      numberToArray(missesAmount).map(key => (
        <i key={key} className={`${styles.cell} ${styles[BULLET_ORDER[key]]} ${styles.miss}`} />
      )) :
      null

    return misses
  }

  _getShot = () => {
    const { shotsCount, winner } = this.props

    const shotsAmount = winner && shotsCount ? shotsCount + 1 : shotsCount

    const shot = winner ? (
      <i className={`${styles.cell} ${styles[BULLET_ORDER[shotsAmount || 1]]} ${styles.shot}`} />
    ) : null

    return shot
  }

  _renderWonLabel = () => {
    return <span className={styles.finishLabel}>BANG!</span>
  }

  _renderTopSection = () => {
    const { isDesktop, opponent, winner } = this.props

    const barrelStage = (winner && 2) || (opponent && 1) || 0

    return (
      <div className={styles.topSection}>
        <div className={`${styles.barrel} ${styles[BARREL_STATES[barrelStage]]}`}>
          {this._getMisses()}
          {this._getShot()}
        </div>
        <div className={styles.infoWrap}>
          {!winner ? <Timer /> : this._renderWonLabel()}
          {!isDesktop && <BetMoney />}
        </div>
      </div>
    )
  }

  _renderBottomSection = () => {
    const { isDesktop } = this.props

    return (
      <div className={styles.bottomSection}>
        {isDesktop && <Pot />}
        <Message />
      </div>
    )
  }

  render() {
    return (
      <div className={styles.headerWrap}>
        {this._renderTopSection()}
        {this._renderBottomSection()}
      </div>
    )
  }
}

export default Header
