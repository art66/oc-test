import isArray from '@torn/shared/utils/isArray'
import classnames from 'classnames'
import React from 'react'
import { TAKE_ACTION_MESSAGE } from '../../constants'
import { TAnimStages, TFightResults } from '../../interfaces/IModules'
import checkFightStep from '../../utils/checkFightStep'
import checkPlayer from '../../utils/checkPlayer'
import whoseTurn from '../../utils/isMyTurn'
import styles from './message.cssmodule.scss'

export interface IProps {
  message: string | string[]
  result: TFightResults
  animStage: TAnimStages
  activeUserID: number
  timeLeftTillTakeAction: number
  creatorID: number
  creatorName: string
  opponentName: string
}

const MESSAGES_NORMALIZED = {
  start: 0,
  progress: 1,
  finish: 2
}

class Message extends React.PureComponent<IProps> {
  _getMessage = () => {
    const {
      message,
      animStage,
      activeUserID,
      creatorID,
      creatorName,
      opponentName,
      timeLeftTillTakeAction,
      result
    } = this.props

    const { isCreator } = checkPlayer(creatorID)
    const isMyTurn = whoseTurn(activeUserID)

    if (timeLeftTillTakeAction <= 0 && opponentName && !isMyTurn && !['retake', 'won', 'lose'].includes(result)) {
      // specific scenario in case user doesn't active for last 3 min
      return `<em>${isCreator ? opponentName : creatorName}</em> ${TAKE_ACTION_MESSAGE}`
    } else if (isArray(message) && message[MESSAGES_NORMALIZED[animStage]]) {
      // show message based on current anim
      return message[MESSAGES_NORMALIZED[animStage]]
    } else if (isArray(message) && !message[MESSAGES_NORMALIZED[animStage]]) {
      // on reload show always last message
      return message[message.length - 1]
    }

    return message
  }

  _renderSSRMessage = () => {
    const { result, animStage, activeUserID } = this.props
    const { isShoot, isLose, isWon, isRetake } = checkFightStep(result)

    const isMyTurn = whoseTurn(activeUserID)
    const isMainHighlight = isMyTurn && ['', 'start', 'finish'].includes(animStage)
    const isShootHighlight = isShoot && ['', 'start'].includes(animStage)

    const classes = classnames({
      [styles.message]: true,
      [styles.yellow]: isMainHighlight || isShootHighlight || (isRetake && isMyTurn),
      [styles.green]: isWon,
      [styles.red]: isLose
    })

    const matchPattern = new RegExp('(<br>)|(<br/>)|(<br />)|(<br >)', 'gi')
    const normalizedMessage = (this._getMessage() as string)?.replace(matchPattern, '')

    return <span className={classes} dangerouslySetInnerHTML={{ __html: normalizedMessage }} />
  }

  render() {
    return <div className={styles.messageWrap}>{this._renderSSRMessage()}</div>
  }
}

export default Message
