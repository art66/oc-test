import React from 'react'
import BetMoney from '../../containers/betMoney'
import SoundBtn from '../SoundBtn'
import styles from './pot.cssmodule.scss'

class Pot extends React.PureComponent {
  render() {
    return (
      <div className={styles.potWrap}>
        <SoundBtn />
        <BetMoney />
      </div>
    )
  }
}

export default Pot
