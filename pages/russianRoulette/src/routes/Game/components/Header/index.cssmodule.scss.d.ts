// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'barrel': string;
  'bottomSection': string;
  'cell': string;
  'finishLabel': string;
  'finished': string;
  'first': string;
  'five': string;
  'four': string;
  'globalSvgShadow': string;
  'headerWrap': string;
  'infoWrap': string;
  'miss': string;
  'second': string;
  'shot': string;
  'six': string;
  'third': string;
  'topSection': string;
}
export const cssExports: CssExports;
export default cssExports;
