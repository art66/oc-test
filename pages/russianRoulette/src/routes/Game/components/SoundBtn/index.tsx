import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { setAudioState } from '../../../../controller/actions/audio'
import { IState } from '../../../../interfaces/IState'
import styles from './index.cssmodule.scss'

type TProps = TReduxProps & {
  noLabel?: boolean
}
class SoundBtn extends React.PureComponent<TProps> {
  handleSoundKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this.toggleSoundState,
          isPreventBubbling: true
        },
        {
          type: 'space',
          onEvent: this.toggleSoundState,
          isPreventBubbling: true
        }
      ]
    })
  }

  getTitle = () => {
    const { isSound, noLabel } = this.props

    if (noLabel) {
      return ''
    }

    return (isSound && 'SOUND ON') || 'SOUND OFF'
  }

  toggleSoundState = () => {
    this.props.setAudioState(!this.props.isSound)
  }

  render() {
    const { isSound } = this.props

    return (
      <button
        type='button'
        onClick={this.toggleSoundState}
        onKeyDown={this.handleSoundKeydown}
        className={`${styles.soundToggler} ${!isSound ? styles.disabled : ''}`}
      >
        {this.getTitle()}
      </button>
    )
  }
}

const connector = connect(
  (state: IState) => ({
    isSound: state.audio.isSound
  }),
  {
    setAudioState
  }
)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(SoundBtn)
