import classnames from 'classnames'
import React from 'react'
import { ANIM_COLORS } from '../../constants'
import { TAnimColors, TAnimSides, TAnimStages, TFightResults } from '../../interfaces/IModules'
import checkFightStep from '../../utils/checkFightStep'
import styles from './index.cssmodule.scss'

export interface IProps {
  cardColor: TAnimColors
  cardSide: TAnimSides
  userImage: string
  animStage: TAnimStages
  step: TFightResults
}

class UserCard extends React.PureComponent<IProps> {
  _isAnimFinished = () => {
    const { animStage } = this.props

    return !['start', 'progress'].includes(animStage)
  }

  _renderLoseBackdrop = () => {
    const { cardColor, cardSide } = this.props

    const isLose = cardColor === ANIM_COLORS.red

    if (!isLose) {
      return null
    }

    return <i className={`${styles.loseBackdrop} ${styles[cardSide]}`} />
  }

  render() {
    const { cardColor, cardSide, userImage, step } = this.props
    const { isRetake } = checkFightStep(step)

    const classes = classnames({
      [styles.cardWrap]: true,
      [styles[cardSide]]: cardSide,
      [styles[cardColor]]: (isRetake && cardColor) || (cardColor && this._isAnimFinished())
    })

    return (
      <div className={classes}>
        {this._renderLoseBackdrop()}
        {userImage && <img alt='' className={styles.userImage} src={userImage} />}
      </div>
    )
  }
}

export default UserCard
