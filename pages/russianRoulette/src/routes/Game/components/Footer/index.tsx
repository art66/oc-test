import React from 'react'
import SoundBtn from '../SoundBtn'
import Leave from '../../containers/leave'
import ShootButtons from '../../containers/shootButtons'
import styles from './index.cssmodule.scss'

export interface IProps {
  opponent: object
  isDesktop: boolean
}

class Footer extends React.PureComponent<IProps> {
  render() {
    const { isDesktop, opponent } = this.props

    if (isDesktop) {
      return null
    }

    return (
      <div className={styles.footerWrap}>
        <SoundBtn noLabel={true} />
        {!opponent ? <Leave /> : <ShootButtons />}
      </div>
    )
  }
}

export default Footer
