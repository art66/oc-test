import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import React from 'react'
import Confirmation from '../../../../components/Confirmation'
import styles from './index.cssmodule.scss'

export interface IProps {
  isLeave: boolean
  isLeaveConfirmation: boolean
  switchLeaveLayout: () => void
  leaveFight: () => void
}

class Leave extends React.PureComponent<IProps> {
  private _ref: React.RefObject<HTMLButtonElement>

  constructor(props: IProps) {
    super(props)

    this._ref = React.createRef()
  }

  componentDidMount() {
    this._ref && this._ref.current && this._ref.current.focus()
  }

  _handleLayoutToggle = () => {
    const { switchLeaveLayout } = this.props

    switchLeaveLayout()
  }

  _handleLeave = () => {
    const { leaveFight } = this.props

    leaveFight()
  }

  _handleLeaveKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleLayoutToggle
        },
        {
          type: 'space',
          onEvent: this._handleLayoutToggle
        }
      ]
    })
  }

  _renderLeaveButton = () => {
    return (
      <button
        ref={this._ref}
        type='button'
        onClick={this._handleLayoutToggle}
        onKeyDown={this._handleLeaveKeydown}
        className='torn-btn'
      >
        LEAVE
      </button>
    )
  }

  _renderLeaveConfirmation = () => {
    const { isLeave } = this.props

    return (
      <Confirmation
        betAmount='0'
        inProgress={isLeave}
        type='leave'
        onStart={this._handleLeave}
        onCancel={this._handleLayoutToggle}
      />
    )
  }

  render() {
    const { isLeaveConfirmation } = this.props

    const content = isLeaveConfirmation ? this._renderLeaveConfirmation() : this._renderLeaveButton()

    return <div className={styles.leaveWrap}>{content}</div>
  }
}

export default Leave
