import { toMoney } from '@torn/shared/utils'
import React from 'react'
import styles from './index.cssmodule.scss'

export interface IProps {
  betAmount: number
}

class BetMoney extends React.PureComponent<IProps> {
  render() {
    const { betAmount } = this.props

    return (
      <div className={styles.money}>
        <span className={styles.label}>POT MONEY:</span>
        <span className={styles.count}>{`$${toMoney(betAmount * 2)}`}</span>
      </div>
    )
  }
}

export default BetMoney
