import React from 'react'
import Body from '../containers/body'
import Footer from '../containers/footer'
import Header from '../containers/header'
import styles from './index.cssmodule.scss'

export const GameLayout = () => (
  <div className={styles.appWrapper}>
    <Header />
    <Body />
    <Footer />
  </div>
)
