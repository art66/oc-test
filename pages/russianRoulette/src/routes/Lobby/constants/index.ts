export const JOIN_GAME_ATTEMPT = 'lobby/JOIN_GAME_ATTEMPT'
export const JOIN_GAME_START = 'lobby/JOIN_GAME_START'
export const JOIN_GAME_SAVED = 'lobby/JOIN_GAME_SAVED'
export const JOIN_GAME_CANCEL = 'lobby/JOIN_GAME_CANCEL'
export const JOIN_GAME_FAILED = 'lobby/JOIN_GAME_FAILED'
export const JOIN_GAME_CLEAR = 'lobby/JOIN_GAME_CLEAR'

export const CREATE_GAME_START = 'lobby/CREATE_GAME_START'
export const CREATE_GAME_SAVED = 'lobby/CREATE_GAME_SAVED'
export const CREATE_GAME_CANCEL = 'lobby/CREATE_GAME_CANCEL'
export const CREATE_GAME_FAILED = 'lobby/CREATE_GAME_FAILED'
export const CREATE_GAME_CLEAR = 'lobby/CREATE_GAME_CLEAR'

export const LOAD_INIT_DATA_SAVED = 'lobby/LOAD_INIT_DATA_SAVED'

export const FIGHT_CREATED = 'lobby/FIGHT_CREATED'
export const FIGHT_EXPIRED = 'lobby/FIGHT_EXPIRED'
export const FIGHT_JOINED = 'lobby/FIGHT_JOINED'

export const TITLE_START = 'Start a new game'
export const TITLE_JOIN = 'List of available games'

export const LABEL_BET = 'Please enter bet amount:'
export const LABEL_PASSWORD = 'Password (optional)'
export const LABEL_START = 'start'

export const NO_GAMES_PLACEHOLDER = 'Currently no games...'
export const COLUMN_STARTED = 'Started by'
export const COLUMN_STATUS = 'Status'
export const COLUMN_BET = 'Bet Amount'
export const COLUMN_JOIN = 'Join'

export const STATUS = 'Waiting for an opponent...'
export const STATUS_SHORT = 'Waiting...'
