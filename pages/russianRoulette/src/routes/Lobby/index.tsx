import React from 'react'
import Loadable from 'react-loadable'
import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/services/rootReducer'
import Skeleton from './components/Skeleton'
import reducer from './modules/reducers'
import initWS from './modules/websockets'

// use whole layout as skeleton because it consisted almost of static html data
const Preload = () => <Skeleton />

const LobbyRoute = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const LobbyRouteComponent = await import(/* webpackChunkName: "lobby" */ './layout/index')

    initWS(rootStore.dispatch, rootStore)
    injectReducer(rootStore, { reducer, key: 'lobby' })

    return LobbyRouteComponent
  },
  loading: Preload
})

export default LobbyRoute
