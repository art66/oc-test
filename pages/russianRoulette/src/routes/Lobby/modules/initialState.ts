import { IInitialState } from '../interfaces/IModules'

const initialState: IInitialState = {
  activeRowID: null,
  startFightAttempt: false,
  joinFightAttempt: false,
  joinError: null,
  createError: null
}

export default initialState
