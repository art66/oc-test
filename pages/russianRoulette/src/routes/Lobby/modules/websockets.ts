import getUserID from '@torn/shared/utils/getUserID'
import { Dispatch } from 'react'
import { Store } from 'redux'
import { STEP_ACTING } from '../../../constants'
import { debugShow } from '../../../controller/actions/common'
import { setGamesData } from '../../../controller/actions/main'
import { IGameCreatedWS, IGameRemovedWS, ITimeoutWS } from '../interfaces/IModules'
import { gameExpired, newGameCreated } from './actions'

const initWS = (dispatch: Dispatch<any>, state: Store) => {
  // @ts-ignore global Websocket & Centrifuge handler!!!
  const handler = new WebsocketHandler('russianRoulette', { channel: 'rrLobby' })

  handler.setActions({
    gameCreated: (payload: IGameCreatedWS) => {
      const {
        main: { step }
      } = state.getState()
      const {
        data: { creator }
      } = payload.data

      // prevent update on creator
      if (creator.ID === Number(getUserID()) || step === STEP_ACTING) {
        return
      }

      if (payload.data.error) {
        dispatch(debugShow(payload.data.error))

        return
      }

      dispatch(newGameCreated(payload.data))
    },
    gameRemoved: (payload: IGameRemovedWS) => {
      const {
        main: { step }
      } = state.getState()

      if (step === STEP_ACTING) {
        return
      }

      if (payload.data.error) {
        dispatch(debugShow(payload.data.error))

        return
      }

      dispatch(gameExpired(payload.data))
    },
    gameRemovedFromList: (payload: IGameRemovedWS) => {
      const {
        main: { step }
      } = state.getState()

      if (step === STEP_ACTING) {
        return
      }

      if (payload.data.error) {
        dispatch(debugShow(payload.data.error))

        return
      }

      dispatch(gameExpired(payload.data))
    },
    timeout: (payload: ITimeoutWS) => {
      dispatch(setGamesData(payload.data.data))
    }
  })
}

export default initWS
