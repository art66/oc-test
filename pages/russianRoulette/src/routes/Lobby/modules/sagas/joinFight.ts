import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { GENERIC_ERROR } from '../../../../constants/errors'
import { debugShow } from '../../../../controller/actions/common'
import { joinGameCancel, joinGameFailed, joinGameSaved } from '../actions'

// import { IInitData } from '../interfaces/IMain'

function* joinFight({ gameID, password }: { gameID: number; password: string }) {
  let payload = null

  try {
    const requestData = {
      sid: 'russianRouletteData',
      step: 'joinGame',
      ID: gameID,
      ...(password ? { password } : {})
    }

    payload = yield fetchUrl('/page.php', requestData)
  } catch (e) {
    yield put(debugShow(GENERIC_ERROR))
    yield put(joinGameCancel())

    throw new Error(`${GENERIC_ERROR}: ${e}`)
  }

  if (payload.error) {
    yield put(joinGameFailed(payload.error))

    return
  }

  yield put(joinGameSaved())
}

export default joinFight
