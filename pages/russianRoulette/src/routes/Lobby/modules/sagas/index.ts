// @ts-nocheck
import { takeLatest } from 'redux-saga/effects'
import { CREATE_GAME_START, JOIN_GAME_START } from '../../constants'
import createFight from './createFight'
import joinFight from './joinFight'

export default function* rootSaga() {
  yield takeLatest(CREATE_GAME_START, createFight)
  yield takeLatest(JOIN_GAME_START, joinFight)
}
