import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { GENERIC_ERROR } from '../../../../constants/errors'
import { debugShow } from '../../../../controller/actions/common'
import { createGameCancel, createGameFailed } from '../actions'

function* createFight({ betAmount, password }: { betAmount: string; password: string }) {
  let payload = null

  try {
    const requestData = {
      sid: 'russianRouletteData',
      step: 'createGame',
      betAmount,
      ...(password ? { password } : {})
    }

    payload = yield fetchUrl('/page.php', requestData)
  } catch (e) {
    yield put(debugShow(GENERIC_ERROR))
    yield put(createGameCancel())

    throw new Error(`${GENERIC_ERROR}: ${e}`)
  }

  if (payload.error) {
    yield put(createGameFailed(payload.error))
  }
}

export default createFight
