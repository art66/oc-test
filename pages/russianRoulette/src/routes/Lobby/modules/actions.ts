import { IType } from '../../../controller/interfaces'
import { TUserTypes } from '../../../interfaces/IData'
import { IFightJoined, IFightPayloadData } from '../../Game/interfaces/IModules'
import {
  CREATE_GAME_CANCEL,
  CREATE_GAME_CLEAR,
  CREATE_GAME_FAILED,
  CREATE_GAME_SAVED,
  CREATE_GAME_START,
  FIGHT_CREATED,
  FIGHT_EXPIRED,
  FIGHT_JOINED,
  JOIN_GAME_ATTEMPT,
  JOIN_GAME_CANCEL,
  JOIN_GAME_CLEAR,
  JOIN_GAME_FAILED,
  JOIN_GAME_SAVED,
  JOIN_GAME_START
} from '../constants'
import {
  IFightExpired,
  IGameJoin,
  IGameStart,
  IJoinGameAttempt,
  ILobbyJoinRowData,
  INewGameCreated
} from '../interfaces/IModules'

export const joinGameAttempt = (rowID: number): IJoinGameAttempt & IType => ({
  rowID,
  type: JOIN_GAME_ATTEMPT
})

export const joinGameStart = (gameID: number, password?: string): IGameJoin & IType => ({
  gameID,
  password,
  type: JOIN_GAME_START
})

export const joinGameSaved = (): IType => ({
  // payload,
  type: JOIN_GAME_SAVED
})

export const joinGameCancel = (): IType => ({
  type: JOIN_GAME_CANCEL
})

export const joinGameFailed = (errorText: string): { errorText: string } & IType => ({
  errorText,
  type: JOIN_GAME_FAILED
})

export const clearFailedJoin = (): IType => ({
  type: JOIN_GAME_CLEAR
})

export const createGameStart = (betAmount: number, password: string): IGameStart & IType => ({
  betAmount,
  password,
  type: CREATE_GAME_START
})

export const createGameSaved = (): IType => ({
  // payload,
  type: CREATE_GAME_SAVED
})

export const createGameCancel = (): IType => ({
  type: CREATE_GAME_CANCEL
})

export const createGameFailed = (errorText: string): { errorText: string } & IType => ({
  errorText,
  type: CREATE_GAME_FAILED
})

export const clearFailedCreate = (): IType => ({
  type: CREATE_GAME_CLEAR
})

export const newGameCreated = (payload: { data: ILobbyJoinRowData; type: TUserTypes }): INewGameCreated & IType => ({
  data: payload.data,
  status: payload.type,
  type: FIGHT_CREATED
})

export const gameExpired = (payload: IFightExpired): IFightExpired & IType => ({
  expiredGame: payload.expiredGame,
  type: FIGHT_EXPIRED
})

export const fightJoined = (payload: { data: IFightPayloadData }): IFightJoined & IType => ({
  data: payload.data,
  type: FIGHT_JOINED
})
