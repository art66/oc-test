import { IType } from '../../../controller/interfaces'
import { FIGHT_INITIATED } from '../../Game/constants'
import {
  CREATE_GAME_CANCEL,
  CREATE_GAME_CLEAR,
  CREATE_GAME_FAILED,
  CREATE_GAME_START,
  JOIN_GAME_ATTEMPT,
  JOIN_GAME_CANCEL,
  JOIN_GAME_CLEAR,
  JOIN_GAME_FAILED,
  JOIN_GAME_SAVED,
  JOIN_GAME_START
} from '../constants'
import { IInitialState, IJoinGameAttempt } from '../interfaces/IModules'
import initialState from './initialState'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [JOIN_GAME_ATTEMPT]: (state: IInitialState, action: IJoinGameAttempt) => ({
    ...state,
    activeRowID: action.rowID,
    incorrectJoinPassword: false,
    joinError: null
  }),
  [JOIN_GAME_START]: (state: IInitialState) => ({
    ...state,
    joinFightAttempt: true
  }),
  [JOIN_GAME_CANCEL]: (state: IInitialState) => ({
    ...state,
    activeRowID: null,
    joinError: null,
    joinFightAttempt: false
  }),
  [JOIN_GAME_SAVED]: (state: IInitialState) => ({
    ...state,
    // ...action.payload,
    joinFightAttempt: false
  }),
  [JOIN_GAME_FAILED]: (state: IInitialState, action: { errorText: string }) => ({
    ...state,
    joinFightAttempt: false,
    joinError: action.errorText
  }),
  [JOIN_GAME_CLEAR]: (state: IInitialState) => ({
    ...state,
    joinError: null
  }),
  [CREATE_GAME_START]: (state: IInitialState) => ({
    ...state,
    startFightAttempt: true
  }),
  [CREATE_GAME_CANCEL]: (state: IInitialState) => ({
    ...state,
    startFightAttempt: false
  }),
  [FIGHT_INITIATED]: (state: IInitialState) => ({
    ...state,
    // payload: action.payload,
    startFightAttempt: false
  }),
  [CREATE_GAME_FAILED]: (state: IInitialState, action: { errorText: string }) => ({
    ...state,
    startFightAttempt: false,
    createError: action.errorText
  }),
  [CREATE_GAME_CLEAR]: (state: IInitialState) => ({
    ...state,
    createError: null
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
