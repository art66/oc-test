import React from 'react'
import Create from '../containers/create'
import Join from '../containers/join'
import styles from './index.cssmodule.scss'

class AppLayout extends React.Component {
  render() {
    return (
      <div className={styles.appWrapper}>
        <Create />
        <hr className='delimiter-999 m-top10 m-bottom10' />
        <Join />
      </div>
    )
  }
}

export default AppLayout
