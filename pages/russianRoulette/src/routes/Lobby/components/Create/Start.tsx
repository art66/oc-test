import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import React from 'react'
import WarningTooltip from '../../../../components/WarnintTooltip'
import { LABEL_PASSWORD, LABEL_START } from '../../constants'
import styles from './styles.cssmodule.scss'

interface IProps {
  readOnly?: boolean
  inputBetValue?: string
  inputPasswordValue?: string
  onChange?: ({ value, type }: { value: string; type: 'Password' }) => void
  onClick?: ({ startConfirm }: { startConfirm: boolean }) => void
}

class Start extends React.PureComponent<IProps, { isMaxInputLength: boolean }> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      isMaxInputLength: false
    }
  }

  _preventInteraction = () => {
    const { readOnly, inputBetValue } = this.props

    return readOnly || !inputBetValue || isNaN(Number(inputBetValue))
  }

  _maxLengthPassword = (password: string) => {
    return password.length >= 20
  }

  _handleClick = () => {
    const { isMaxInputLength } = this.state
    const { onClick } = this.props

    if (isMaxInputLength) {
      return
    }

    onClick({ startConfirm: true })
  }

  _handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { onChange } = this.props
    const { value } = e.target

    if (this._maxLengthPassword(value)) {
      this.setState({
        isMaxInputLength: true
      })

      return
    }

    this.setState({ isMaxInputLength: false })

    onChange({
      value: value,
      type: 'Password'
    })
  }

  _handleInputCancel = () => {
    const { onChange } = this.props

    onChange({
      value: '',
      type: 'Password'
    })
  }

  _handleInputBlur = () => {
    this.setState({ isMaxInputLength: false })
  }

  _handleButtonKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleClick
        },
        {
          type: 'space',
          onEvent: this._handleClick
        }
      ]
    })
  }

  _handleInputKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleClick
        },
        {
          type: 'esc',
          onEvent: this._handleInputCancel
        }
      ]
    })
  }

  _renderInput = () => {
    const { isMaxInputLength } = this.state
    const { readOnly, inputPasswordValue: value } = this.props

    return (
      <input
        aria-label='Input password for the fight (optional)'
        className={`${styles.input} ${styles.password} ${isMaxInputLength ? styles.inputWarning : ''}`}
        type='text'
        readOnly={readOnly}
        value={value}
        onChange={this._handleInputChange}
        onBlur={this._handleInputBlur}
        onKeyDown={this._handleInputKeydown}
        data-lpignore='true'
      />
    )
  }

  _renderButton = () => {
    return (
      <button
        type='button'
        className={`${styles.submit} torn-btn`}
        disabled={this._preventInteraction()}
        onClick={this._handleClick}
        onKeyDown={this._handleButtonKeydown}
      >
        {LABEL_START}
      </button>
    )
  }

  render() {
    const { isMaxInputLength } = this.state

    return (
      <div className={styles.startBlock}>
        <span className={styles.label}>{LABEL_PASSWORD}</span>
        <div className={styles.passwordInputBlock}>
          <WarningTooltip isWarning={isMaxInputLength} />
          {this._renderInput()}
          {this._renderButton()}
        </div>
      </div>
    )
  }
}

export default Start
