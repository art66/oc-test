import MoneyInput from '@torn/shared/components/MoneyInput'
import React from 'react'
import { LABEL_BET } from '../../constants'
import styles from './styles.cssmodule.scss'

interface IProps {
  readOnly?: boolean
  inputBetValue?: string
  userMoney?: number
  onChange?: ({ value, type }: { value: string; type: 'Bet' }) => void
  onClick?: ({ startConfirm }: { startConfirm: boolean }) => void
  isMobile: boolean
  isManualDesktopMode: boolean
}

// interface IState {
//   isTypeStarted: boolean
// }

class Bet extends React.PureComponent<IProps> {
  _handleInputClick = (_value?: string) => {
    const { onClick } = this.props

    onClick({ startConfirm: true })
  }

  _handleChange = (value: string) => {
    const { onChange } = this.props

    // this.setState({
    //   isTypeStarted: true
    // })

    onChange({
      value,
      type: 'Bet'
    })
  }

  _renderBetBlock = () => {
    // const { isTypeStarted } = this.state
    const { isMobile, isManualDesktopMode, readOnly, userMoney, inputBetValue } = this.props

    return (
      <div className={styles.betBlock}>
        <span className={styles.label} aria-label={LABEL_BET}>
          {LABEL_BET}
        </span>
        <MoneyInput
          value={inputBetValue}
          userMoney={userMoney}
          onClick={this._handleInputClick}
          onChange={this._handleChange}
          readOnly={readOnly}
          disableOnMountInputFocus={isMobile && !isManualDesktopMode}
        />
      </div>
    )
  }

  render() {
    return this._renderBetBlock()
  }
}

export default Bet
