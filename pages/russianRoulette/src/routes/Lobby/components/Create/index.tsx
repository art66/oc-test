import React from 'react'
import Confirmation from '../../../../components/Confirmation'
import ContainerWrap from '../../../../components/ContainerWrap'
import Fallback from '../../../../components/Fallback'
import HeadingLine from '../../../../components/HeadingLine'
import { TITLE_START } from '../../constants'
import Bet from '../../containers/bet'
import Start from './Start'
import styles from './styles.cssmodule.scss'

interface IProps {
  createError: string
  startFightAttempt: boolean
  createFightStart: (betAmount: number, password: string) => void
  clearFailedCreate: () => void
}

interface IState {
  inputBetValue: string
  inputPasswordValue: string
  isConfirmStage: boolean
}

interface IToggle {
  startConfirm?: boolean
}

class Create extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      inputBetValue: '10',
      inputPasswordValue: '',
      isConfirmStage: false
    }
  }

  _handleStartFight = () => {
    const { inputBetValue, inputPasswordValue } = this.state
    const { createFightStart } = this.props

    createFightStart(Number(inputBetValue), inputPasswordValue)
  }

  _handleLayoutToggle = (config?: IToggle) => {
    const { startConfirm = false } = config || {}

    this.setState({
      isConfirmStage: startConfirm
    })
  }

  _handleInputChange = ({ value, type }: { value: string; type: 'Bet' | 'Password' }) => {
    this.setState({
      [`input${type}Value`]: value
    } as any)
  }

  _handleFailedCreate = () => {
    const { clearFailedCreate } = this.props

    clearFailedCreate()
  }

  _renderInitFightBlock = () => {
    const { inputBetValue, inputPasswordValue } = this.state

    return (
      <div className={styles.contentWrap}>
        <Bet inputBetValue={inputBetValue} onChange={this._handleInputChange} onClick={this._handleLayoutToggle} />
        <Start
          inputBetValue={inputBetValue}
          inputPasswordValue={inputPasswordValue}
          onChange={this._handleInputChange}
          onClick={this._handleLayoutToggle}
        />
      </div>
    )
  }

  _renderConfirmationBlock = () => {
    const { inputBetValue } = this.state
    const { startFightAttempt } = this.props

    return (
      <div className={styles.contentWrap}>
        <Confirmation
          type='create'
          inProgress={startFightAttempt}
          onStart={this._handleStartFight}
          onCancel={this._handleLayoutToggle}
          betAmount={inputBetValue}
        />
      </div>
    )
  }

  render() {
    const { isConfirmStage } = this.state
    const { createError } = this.props

    if (createError) {
      return (
        <div className={styles.createWrap}>
          <Fallback onClick={this._handleFailedCreate} errorContent={createError} />
        </div>
      )
    }

    const content = isConfirmStage ? this._renderConfirmationBlock() : this._renderInitFightBlock()

    return (
      <div className={styles.createWrap}>
        <HeadingLine title={TITLE_START} />
        <ContainerWrap>{content}</ContainerWrap>
      </div>
    )
  }
}

export default Create
