import React from 'react'
import CreateSkeleton from './Create'
import styles from './index.cssmodule.scss'
import JoinSkeleton from './Join'

class Skeleton extends React.PureComponent {
  _renderCreateSkeleton = () => {
    return (
      <div className={styles.createWrap}>
        <CreateSkeleton />
        <hr />
        <JoinSkeleton />
      </div>
    )
  }

  render() {
    return this._renderCreateSkeleton()
  }
}

export default Skeleton
