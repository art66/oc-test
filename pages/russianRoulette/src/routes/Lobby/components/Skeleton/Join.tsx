import React from 'react'
import ContainerWrap from '../../../../components/ContainerWrap'
import HeadingLine from '../../../../components/HeadingLine'
import { TITLE_JOIN } from '../../constants'
import Columns from '../Join/Columns'
import styles from './join.cssmodule.scss'
import Rows from './Rows'

class JoinSkeleton extends React.PureComponent {
  _renderColumns = () => <Columns />

  _renderJoinSkeleton = () => {
    return (
      <div className={styles.joinWrap}>
        <HeadingLine title={TITLE_JOIN} />
        <ContainerWrap>
          <div className={styles.contentWrap}>
            <Columns />
            <Rows />
          </div>
        </ContainerWrap>
      </div>
    )
  }

  render() {
    return this._renderJoinSkeleton()
  }
}

export default JoinSkeleton
