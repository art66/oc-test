import React from 'react'
import ContainerWrap from '../../../../components/ContainerWrap'
import HeadingLine from '../../../../components/HeadingLine'
import { TITLE_START } from '../../constants'
import styles from './create.cssmodule.scss'

class CreateSkeleton extends React.PureComponent {
  _renderCreateSkeleton = () => {
    return (
      <div className={styles.createWrap}>
        <HeadingLine title={TITLE_START} />
        <ContainerWrap>
          <div className={styles.contentWrap}>
            <span className={styles.label} />
            <span className={styles.input} />
            <span className={`${styles.label} ${styles.labelSecond}`} />
            <span className={`${styles.input} ${styles.inputSecond}`} />
            <span className={styles.button} />
          </div>
        </ContainerWrap>
      </div>
    )
  }

  render() {
    return this._renderCreateSkeleton()
  }
}

export default CreateSkeleton
