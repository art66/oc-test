import numberToArray from '@torn/shared/utils/numberToArray'
import React from 'react'
import styles from './rows.cssmodule.scss'

class RowsSkeleton extends React.PureComponent {
  _rowsSkeleton = () => {
    const rowUI = (ID: number) => (
      <div key={ID} className={styles.rowWrap}>
        <div className={styles.topSection}>
          <div className={styles.userInfoBlock}>
            <span className={styles.status} />
            <span className={styles.tag} />
            <span className={styles.image} />
          </div>
          <div className={styles.statusBlock} />
          <div className={styles.betBlock} />
          <div className={styles.joinBlock} />
        </div>
      </div>
    )

    return (
      <div className={styles.skeletonRowsWrap}>
        {numberToArray(2).map((_ID: number, index: number) => rowUI(index))}
      </div>
    )
  }

  render() {
    return this._rowsSkeleton()
  }
}

export default RowsSkeleton
