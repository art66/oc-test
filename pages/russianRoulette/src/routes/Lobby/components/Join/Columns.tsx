import React from 'react'
import { COLUMN_BET, COLUMN_JOIN, COLUMN_STARTED, COLUMN_STATUS } from '../../constants'
import styles from './styles.cssmodule.scss'

class Columns extends React.PureComponent {
  _renderColumns = () => {
    return (
      <div className={`${styles.columnsWrap} white-grad`}>
        <span className={styles.column}>{COLUMN_STARTED}</span>
        <span className={styles.column}>{COLUMN_STATUS}</span>
        <span className={styles.column}>{COLUMN_BET}</span>
        <span className={styles.column}>{COLUMN_JOIN}</span>
      </div>
    )
  }

  render() {
    return this._renderColumns()
  }
}

export default Columns
