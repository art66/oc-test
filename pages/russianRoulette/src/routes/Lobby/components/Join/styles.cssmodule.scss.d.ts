// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'activeSubmit': string;
  'betBlock': string;
  'bottomSection': string;
  'column': string;
  'columnItem': string;
  'columnsWrap': string;
  'contentWrap': string;
  'globalSvgShadow': string;
  'hackBottomAppear': string;
  'joinBlock': string;
  'joinWrap': string;
  'placeholder': string;
  'row': string;
  'rowsWrap': string;
  'statusBlock': string;
  'submit': string;
  'text': string;
  'topSection': string;
  'userInfoBlock': string;
  'userStatusWrap': string;
  'withPassword': string;
}
export const cssExports: CssExports;
export default cssExports;
