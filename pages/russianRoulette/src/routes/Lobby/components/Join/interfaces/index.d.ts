export interface IProps {
  joinError: string
  gameList: object[]
  mediaType: 'desktop'
  isManualDesktopMode: boolean
  joinFightAttempt: boolean
  isDataLoaded: boolean
  activeRowID: number
  showImages: boolean
  setActiveRow: (rowID: number) => void
  joinFightStart: (gameID: number, password?: string) => void
  clearActiveRow: () => void
  clearFailedJoin: () => void
}
