import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import UserInfo from '@torn/shared/components/UserInfo'
import { SVGIconGenerator } from '@torn/shared/SVG'
import JoinSVGIcon from '@torn/shared/SVG/icons/global/Join'
import { toMoney } from '@torn/shared/utils'
import isArray from '@torn/shared/utils/isArray'
import isEmpty from '@torn/shared/utils/isEmpty'
import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import React from 'react'
import Confirmation from '../../../../components/Confirmation'
import ContainerWrap from '../../../../components/ContainerWrap'
import Fallback from '../../../../components/Fallback'
import HeadingLine from '../../../../components/HeadingLine'
import { NO_GAMES_PLACEHOLDER, STATUS, STATUS_SHORT, TITLE_JOIN } from '../../constants'
import { ILobbyJoinCreator, ILobbyJoinRowData } from '../../interfaces/IModules'
import RowsSkeleton from '../Skeleton/Rows'
import Columns from './Columns'
import { IProps } from './interfaces'
import styles from './styles.cssmodule.scss'

class Join extends React.PureComponent<IProps> {
  componentDidMount() {
    this._mountTooltips()
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: [{ ID: 'join', child: 'Join' }], type: 'mount' })

  _isNotDesktop = () => {
    const { mediaType, isManualDesktopMode } = this.props

    const isNotDesktop = mediaType !== 'desktop' && !isManualDesktopMode

    return isNotDesktop
  }

  _isActiveRow = (rowID: number) => {
    const { activeRowID } = this.props

    return rowID === activeRowID
  }

  _handleJoinAttempt = (e: React.KeyboardEvent<HTMLButtonElement> & React.MouseEvent<HTMLButtonElement>) => {
    const { setActiveRow, activeRowID } = this.props
    const {
      dataset: { id }
    } = e.target as HTMLButtonElement

    const isAlreadyActiveRow = Number(id) === activeRowID

    setActiveRow(isAlreadyActiveRow ? null : Number(id))
  }

  _handleJoin = ({ ID, password }: { ID: number; password?: string }) => {
    const { joinFightStart } = this.props

    joinFightStart(ID, password)
  }

  _handleJoinCancel = () => {
    const { clearActiveRow } = this.props

    clearActiveRow()
  }

  _handleFailedJoin = () => {
    const { clearFailedJoin } = this.props

    clearFailedJoin()
  }

  _handleJoinKeydown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleJoinAttempt
        },
        {
          type: 'space',
          onEvent: this._handleJoinAttempt
        },
        {
          type: 'esc',
          onEvent: this._handleJoinCancel
        }
      ]
    })
  }

  _renderPlaceholder = () => {
    return (
      <div className={styles.placeholder}>
        <span className={styles.text}>{NO_GAMES_PLACEHOLDER}</span>
      </div>
    )
  }

  _renderColumns = () => <Columns />

  _renderRowUserInfo = (creator: ILobbyJoinCreator) => {
    const { showImages } = this.props

    const userConfig = {
      useProgressiveImage: true,
      isActiveTooltips: true,
      showImages,
      status: {
        isActive: true,
        addTooltip: true,
        mode: creator.status as 'online'
      },
      user: {
        isActive: true,
        ID: creator.ID,
        name: creator.name,
        imageUrl: creator.imageURL,
        isSmall: this._isNotDesktop()
      },
      faction: {
        isActive: true,
        ID: creator.factionID,
        name: creator.factionName,
        imageUrl: creator.factionImageURL,
        rank: creator.factionRank
      },
      customStyles: {
        status: styles.userStatusWrap
      }
    }

    return (
      <div className={styles.userInfoBlock}>
        <UserInfo {...userConfig} />
      </div>
    )
  }

  _renderRowJoinButton = (rowID: number, isPassword: boolean) => {
    return (
      <div className={styles.joinBlock}>
        <button
          id='join'
          data-id={String(rowID)}
          type='button'
          className={`${styles.submit} ${this._isActiveRow(rowID) ? styles.activeSubmit : ''} ${
            isPassword ? styles.withPassword : ''
          }`}
          onClick={this._handleJoinAttempt}
          onKeyDown={this._handleJoinKeydown}
        >
          <SVGIconGenerator
            iconName='Join'
            iconsHolder={{ Join: JoinSVGIcon }}
            dimensions={{ viewbox: '2 0 24 24', width: 20, height: 20 }}
            fill={{
              name: (isPassword && 'ORANGE_DIMMED') || (this._isActiveRow(rowID) && 'GREY_HOVERED') || 'GREY_COMMON'
            }}
            onHover={{ active: true, fill: { name: (isPassword && 'ORANGE_HOVERED') || 'GREY_HOVERED' } }}
          />
        </button>
      </div>
    )
  }

  _renderConfirmation = (rowID: number, betAmount: number, password: boolean) => {
    const { joinError, joinFightAttempt } = this.props

    if (!this._isActiveRow(rowID)) {
      return null
    }

    if (joinError) {
      return (
        <div className={styles.bottomSection}>
          <Fallback onClick={this._handleFailedJoin} errorContent={joinError} />
        </div>
      )
    }

    return (
      <div className={styles.bottomSection}>
        <Confirmation
          type='join'
          ID={rowID}
          isPassword={password}
          inProgress={joinFightAttempt}
          betAmount={String(betAmount)}
          onStart={this._handleJoin}
          onCancel={this._handleJoinCancel}
        />
      </div>
    )
  }

  _renderRow = ({ ID, betAmount, creator, password }: ILobbyJoinRowData) => {
    return (
      <div id={String(ID)} key={ID} className={`${styles.row} ${this._isActiveRow(ID) ? styles.active : ''}`}>
        <div className={styles.topSection}>
          {this._renderRowUserInfo(creator)}
          <div className={styles.statusBlock} aria-label={`Fight status: ${STATUS}`}>
            <span className={styles.text}>{this._isNotDesktop() ? STATUS_SHORT : STATUS}</span>
          </div>
          <div className={styles.betBlock} aria-label={`Bet amount: ${betAmount}`}>
            <span className={styles.text}>$</span>
            <span className={styles.text}>{toMoney(betAmount)}</span>
          </div>
          {this._renderRowJoinButton(ID, password)}
        </div>
        {this._renderConfirmation(ID, betAmount, password)}
      </div>
    )
  }

  _renderList = () => {
    const { isDataLoaded, gameList } = this.props

    if (!isDataLoaded) {
      return <RowsSkeleton />
    }

    if (isEmpty(gameList) || !isArray(gameList)) {
      return this._renderPlaceholder()
    }

    return <div className={styles.rowsWrap}>{gameList.map((row: ILobbyJoinRowData) => this._renderRow(row))}</div>
  }

  render() {
    return (
      <div className={styles.joinWrap}>
        <HeadingLine title={TITLE_JOIN} />
        <ContainerWrap>
          {this._renderColumns()}
          {this._renderList()}
        </ContainerWrap>
      </div>
    )
  }
}

export default Join
