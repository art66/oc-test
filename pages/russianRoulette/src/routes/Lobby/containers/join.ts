import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IState } from '../../../interfaces/IState'
import Join from '../components/Join'
import { clearFailedJoin, joinGameAttempt, joinGameCancel, joinGameStart } from '../modules/actions'

const mapStateToProps = ({ lobby, main, common, browser }: IState) => ({
  joinError: lobby.joinError,
  isDataLoaded: main.isDataLoaded,
  isManualDesktopMode: common.isDesktopManualLayout,
  mediaType: browser.mediaType,
  gameList: main.gameData,
  showImages: main.showImages,
  activeRowID: lobby.activeRowID,
  joinFightAttempt: lobby.joinFightAttempt
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  setActiveRow: (rowID: number) => dispatch(joinGameAttempt(rowID)),
  joinFightStart: (gameID: number, password?: string) => dispatch(joinGameStart(gameID, password)),
  clearActiveRow: () => dispatch(joinGameCancel()),
  clearFailedJoin: () => dispatch(clearFailedJoin())
})

export default connect(mapStateToProps, mapDispatchToProps)(Join)
