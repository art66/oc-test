import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IState } from '../../../interfaces/IState'
import Create from '../components/Create'
import { clearFailedCreate, createGameCancel, createGameStart } from '../modules/actions'

const mapStateToProps = ({ lobby }: IState) => ({
  startFightAttempt: lobby.startFightAttempt,
  createError: lobby.createError
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  createFightCancel: () => dispatch(createGameCancel()),
  createFightStart: (betAmount: number, password: string) => dispatch(createGameStart(betAmount, password)),
  clearFailedCreate: () => dispatch(clearFailedCreate())
})

export default connect(mapStateToProps, mapDispatchToProps)(Create)
