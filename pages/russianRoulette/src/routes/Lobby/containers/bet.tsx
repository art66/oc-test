import { connect } from 'react-redux'
import { IState } from '../../../interfaces/IState'
import Bet from '../components/Create/Bet'

const mapStateToProps = ({ main, browser, common }: IState) => ({
  userMoney: main.userMoney,
  isMobile: browser.is.mobile,
  isManualDesktopMode: common.isDesktopManualLayout
})

export default connect(mapStateToProps, null)(Bet)
