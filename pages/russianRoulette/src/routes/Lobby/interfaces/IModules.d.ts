import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'
import { TGameTypes, TUserTypes } from '../../../interfaces/IData'

export type TStatusTypes = 'online' | 'offline' | 'idle'

export interface ILobbyJoinCreator {
  status: TStatusTypes
  ID: number
  name: string
  imageURL: string
  factionID: number
  factionName: string
  factionImageURL: string
  factionRank: TFactionRank
}

export interface ILobbyJoinRowData {
  ID: number
  betAmount: number
  password: boolean
  timeCreated: number
  creator: ILobbyJoinCreator
}

export interface INewGameCreated {
  data: ILobbyJoinRowData
  status: TUserTypes
}

export interface IInitialState {
  activeRowID: number
  startFightAttempt: boolean
  joinFightAttempt: boolean
  joinError: string
  createError: string
}

export interface IJoinGameAttempt {
  rowID: number
}

export interface IFightCreated {
  status: TUserTypes
  data: ILobbyJoinRowData
}

export interface IFightExpired {
  expiredGame: number
}

export interface IGameStart {
  betAmount: number
  password: string
}

export interface IGameJoin {
  gameID: number
  password: string
}

export interface IGameCreatedWS {
  data: {
    error?: string
    type: TUserTypes
    data: ILobbyJoinRowData
  }
}

export interface IGameRemovedWS {
  data: {
    error?: string
    expiredGame: number
  }
}

export interface IFightLeavedWS {
  data: {
    data: ILobbyJoinRowData[]
    step: TGameTypes
    error?: string
  }
}

export interface ILeaveFightSaved {
  data: ILobbyJoinRowData[]
  step: TGameTypes
}

export interface ITimeoutWS {
  action: 'timeout'
  data: {
    data: ILobbyJoinRowData[]
    step: TGameTypes
  }
}
