// Here you can place any global constants.
// Please, note that for constant used in a separate folders only
// you probably would like to create its own constants folder either.

// --------------------------
// REDUX NAMESPACES
// --------------------------
export const INIT_APP = 'INIT_APP'
export const SHOW_DEBUG_BOX = 'SHOW_DEBUG_BOX'
export const HIDE_DEBUG_BOX = 'HIDE_DEBUG_BOX'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_INFO_BOX = 'HIDE_INFO_BOX'
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE'
export const MEDIA_SCREEN_CHANGED = 'redux-responsive/CALCULATE_RESPONSIVE_STATE'
export const ROUTE_HEADER_SET = 'ROUTE_HEADER_SET'
export const MANUAL_DESKTOP_MODE = 'MANUAL_DESKTOP_MODE'
export const LOAD_INIT_DATA_ATTEMPT = 'LOAD_INIT_DATA_ATTEMPT'
export const LOAD_INIT_DATA_SAVED = 'LOAD_INIT_DATA_SAVED'
export const SET_GAMES_DATA = 'SET_GAMES_DATA'
export const INIT_ASSETS_LOADED = 'INIT_ASSETS_LOADED'
export const SET_AUDIO_STATE = 'SET_AUDIO_STATE'
export const USER_MONEY_UPDATED = 'USER_MONEY_UPDATED'
export const SOUNDS_INIT_NOTIFY = 'SOUNDS_INIT_NOTIFY'

export const ROOT_URL = '/page.php?sid=russianRouletteData'
export const STEP_ACTING = 'acting'
export const STEP_LOBBY = 'lobby'
export const USER_CREATOR = 'creator'
export const USER_ONLOOKER = 'onlooker'

// --------------------------
// APP HEADER PATHS IDs
// --------------------------
export const PATHS_ID = {
  '': 1,
  game: 2
}

export const APP_HEADER = {
  active: true,
  titles: {
    default: {
      ID: 0,
      title: 'Russian Roulette'
    }
  },
  links: {
    default: {
      ID: 0,
      items: [
        {
          title: 'Last Games',
          href: '/loader.php?sid=viewRussianRouletteLastGames',
          label: 'last-games',
          icon: 'LastRolls'
        },
        {
          title: 'Statistics',
          href: '/loader.php?sid=viewRussianRouletteStats',
          label: 'statistics',
          icon: 'Statistics'
        },
        // {
        //   title: 'Log',
        //   href: '/loader.php?sid=manageRussianRouletteLogs',
        //   label: 'log',
        //   icon: 'Log'
        // },
        {
          title: 'Back to Casino',
          href: '/casino.php',
          label: 'back',
          icon: 'Back'
        }
      ]
    }
  }
}
