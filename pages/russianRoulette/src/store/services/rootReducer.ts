import { connectRouter } from 'connected-react-router'
import { combineReducers, Store } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import audio from '../../controller/reducers/audio'
import common from '../../controller/reducers/common'
import main from '../../controller/reducers/main'
import history from '../history'
import { IProps, IStore, TAsyncReducers } from '../interfaces/IServices'

export const makeRootReducer = (asyncReducers?: TAsyncReducers) => {
  return combineReducers({
    ...asyncReducers,
    common,
    main,
    audio,
    router: connectRouter(history),
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    })
  })
}

export const injectReducer = (store: IStore & Store<any>, { key, reducer }: IProps) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) {
    return
  }

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
