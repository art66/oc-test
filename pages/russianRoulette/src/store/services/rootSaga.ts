import { all } from 'redux-saga/effects'
import mainSaga from '../../controller/sagas'
import gameRouteSaga from '../../routes/Game/modules/sagas'
import lobbyRouteSaga from '../../routes/Lobby/modules/sagas'

export default function* rootSaga() {
  yield all([mainSaga(), lobbyRouteSaga(), gameRouteSaga()])
}
