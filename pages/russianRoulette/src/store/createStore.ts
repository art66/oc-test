import { routerMiddleware } from 'connected-react-router'
import { applyMiddleware, compose, createStore, Store } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import createSagaMiddleware from 'redux-saga'
import reduxLogger from '../controller/middleware/reduxLogger'
import initRussianRouletteWS, { initUserMoneyWS } from '../controller/websockets'
import history from './history'
import { IAsyncReducersStore } from './interfaces/IServices'
import makeRootReducer from './services/rootReducer'
import rootSaga from './services/rootSaga'
import activateStoreHMR from './services/storeHMR'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    getCookie: any
    devToolsExtension: any
  }
}

// creating saga middleware for observation
const sagaMiddleware = createSagaMiddleware()

const rootStore = (initialState: object = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [routerMiddleware(history), sagaMiddleware]
  const enhancers = []

  if (__DEV__) {
    middleware.push(reduxLogger)

    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store: Store<any> & IAsyncReducersStore = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer, applyMiddleware(...middleware), ...enhancers)
  )

  sagaMiddleware.run(rootSaga) // starting middleware run
  store.asyncReducers = {} // activating async reducers replacement

  activateStoreHMR(store)

  initRussianRouletteWS(store.dispatch)
  initUserMoneyWS(store.dispatch)

  return store
}

export default rootStore()
