import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import AppContainer from './core'
import rootStore from './store/createStore'
import history from './store/history'

const MOUNT_NODE = document.getElementById('react-root')

let render = () => {
  ReactDOM.render(<AppContainer store={rootStore} history={history} />, MOUNT_NODE)
}

if (__DEV__) {
  if (module.hot) {
    const renderApp = render
    const renderError = (error: any) => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    module.hot.accept('./core/index', render)
  }
}

render()
