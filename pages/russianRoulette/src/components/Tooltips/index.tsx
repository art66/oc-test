import TooltipGlobal from '@torn/shared/components/TooltipNew'
import React from 'react'
import './anim.cssmodule.scss'
import styles from './index.cssmodule.scss'

const ANIM_PROPS = {
  className: 'ammoTooltip',
  timeExit: 100,
  timeIn: 300
}

const POSITION = {
  x: 'center',
  y: 'top'
}

const STYLES = {
  wrap: styles.wrap,
  container: styles.tooltipContainer,
  title: styles.tooltipTitle,
  arrowWrap: styles.arrowWrap,
  tooltipArrow: styles.tooltipArrow
}

class Tooltip extends React.PureComponent {
  render() {
    return (
      <TooltipGlobal manualCoodsFix={{ fixX: -3 }} overrideStyles={STYLES} position={POSITION} animProps={ANIM_PROPS} />
    )
  }
}

export default Tooltip
