import { SVGIconGenerator } from '@torn/shared/SVG'
import CloseSVGIcon from '@torn/shared/SVG/icons/global/Close'
import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import React from 'react'
import styles from './styles.cssmodule.scss'

interface IProps {
  errorContent: string
  onClick: () => void
}

const ERROR_TEXT_PASSWORD = 'Incorrect Password! Please, try again.'

class Fallback extends React.PureComponent<IProps> {
  private _ref: React.RefObject<HTMLInputElement>

  constructor(props: IProps) {
    super(props)

    this._ref = React.createRef()
  }

  componentDidMount() {
    this._forceButtonFocus()
  }

  _forceButtonFocus = () => {
    this._ref && this._ref.current && this._ref.current.focus()
  }

  _handleClickStart = () => {
    const { onClick } = this.props

    onClick()
  }

  _handleSaveKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleClickStart
        },
        {
          type: 'space',
          onEvent: this._handleClickStart
        },
        {
          type: 'esc',
          onEvent: this._handleClickStart
        }
      ]
    })
  }

  _renderText = () => {
    const { errorContent } = this.props

    return (
      <div className={styles.textWrap}>
        <span aria-label={ERROR_TEXT_PASSWORD} className={styles.text}>
          {errorContent || ERROR_TEXT_PASSWORD}
        </span>
      </div>
    )
  }

  _renderButton = () => {
    return (
      <div className={styles.buttonWrap}>
        <button
          aria-label='Confirm'
          ref={this._ref}
          data-type='confirm'
          className={styles.button}
          type='button'
          onClick={this._handleClickStart}
          onKeyDown={this._handleSaveKeydown}
        >
          <SVGIconGenerator
            iconName='Close'
            iconsHolder={{ Close: CloseSVGIcon }}
            dimensions={{ width: 20, height: 20 }}
            fill={{ name: 'GREY_COMMON' }}
            onHover={{ active: true, fill: { name: 'GREY_HOVERED' } }}
          />
        </button>
      </div>
    )
  }

  render() {
    return (
      <div className={styles.closeWrap}>
        {this._renderText()}
        {this._renderButton()}
      </div>
    )
  }
}

export default Fallback
