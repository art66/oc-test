import React, { memo } from 'react'

interface IContainerWrap {
  children: JSX.Element | JSX.Element[]
}

const ContainerWrap = ({ children }: IContainerWrap) => <div className='cont-gray bottom-round'>{children}</div>

export default memo(ContainerWrap)
