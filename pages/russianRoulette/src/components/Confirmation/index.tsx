import AnimationLoad from '@torn/shared/components/AnimationLoad'
import { toMoney } from '@torn/shared/utils'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import classnames from 'classnames'
import React from 'react'
import WarningTooltip from '../WarnintTooltip'
import styles from './confirm.cssmodule.scss'

interface IProps {
  inProgress: boolean
  type: 'create' | 'join' | 'leave'
  isPassword?: boolean
  ID?: number
  betAmount: string
  onStart: ({ ID, password }: { ID?: number; password?: string }) => void
  onCancel: () => void
}

interface IState {
  isMaxInputLength: boolean
  inputValue: string
}

class Confirm extends React.PureComponent<IProps, IState> {
  private _refConfirm: React.RefObject<HTMLInputElement>
  private _refPassword: React.RefObject<HTMLInputElement>

  constructor(props: IProps) {
    super(props)

    this.state = {
      inputValue: '',
      isMaxInputLength: false
    }

    this._refConfirm = React.createRef()
    this._refPassword = React.createRef()
  }

  componentDidMount() {
    // it's a hack here! Because of one-by-one triggering onKeyDown events.
    setTimeout(() => this._forceFocus(), 100)
  }

  componentDidUpdate(prevProps: IProps) {
    const { inProgress } = this.props

    if (prevProps.inProgress !== inProgress) {
      this._forceFocus()
    }
  }

  _preventAction = () => {
    const { inputValue } = this.state
    const { isPassword } = this.props

    return isPassword && !inputValue
  }

  _maxLengthPassword = (password: string) => {
    return password.length >= 20
  }

  _forceFocus = () => {
    const { isPassword } = this.props

    if (isPassword) {
      this._refPassword && this._refPassword.current && this._refPassword.current.focus()

      return
    }

    this._refConfirm && this._refConfirm.current && this._refConfirm.current.focus()
  }

  _handleClickStart = () => {
    const { inputValue } = this.state
    const { ID, onStart } = this.props

    if (this._preventAction()) {
      return
    }

    onStart({ ID, password: inputValue })
  }

  _handleClickCancel = () => {
    const { onCancel } = this.props

    onCancel()
  }

  _handleSaveKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleClickStart
        },
        {
          type: 'space',
          onEvent: this._handleClickStart
        },
        {
          type: 'esc',
          onEvent: this._handleClickCancel
        }
      ]
    })
  }

  _handleCancelKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleClickCancel
        },
        {
          type: 'space',
          onEvent: this._handleClickCancel
        },
        {
          type: 'esc',
          onEvent: this._handleClickCancel
        }
      ]
    })
  }

  _handleInputKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleClickStart
        },
        {
          type: 'esc',
          onEvent: this._handleClickCancel
        }
      ]
    })
  }

  _handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target as HTMLInputElement

    if (this._maxLengthPassword(value)) {
      this.setState({
        isMaxInputLength: true
      })

      return
    }

    this.setState({
      isMaxInputLength: false,
      inputValue: (event.target as HTMLInputElement).value
    })
  }

  _handleInputBlur = () => {
    this.setState({ isMaxInputLength: false })
  }

  _renderPasswordInput = () => {
    const { inputValue, isMaxInputLength } = this.state
    const { isPassword } = this.props

    if (!isPassword) {
      return null
    }

    return (
      <div className={styles.inputPasswordWrap}>
        <WarningTooltip isWarning={isMaxInputLength} type='join' />
        <span className={styles.text}>Password:</span>
        <input
          ref={this._refPassword}
          aria-label='Input password for the fight'
          className={`${styles.password} ${isMaxInputLength ? styles.maxInputLength : ''}`}
          type='text'
          value={inputValue}
          onChange={this._handleInputChange}
          onKeyDown={this._handleInputKeydown}
          onBlur={this._handleInputBlur}
        />
      </div>
    )
  }

  _confirmationText = () => {
    const { type, betAmount } = this.props

    const firstLine =
      type === 'join' ? `${firstLetterUpper({ value: type })} this game` : `Are you sure you want to ${type} this game`

    return (
      <React.Fragment>
        <div className={styles.firstLine}>
          <span className={styles.text}>{firstLine}</span>
        </div>
        <div className={styles.secondLine}>
          <span className={styles.text}>of Foot Russian Roulette for</span>
          <span className={styles.text}>${toMoney(betAmount)}</span>
          <span className={styles.text}>?</span>
        </div>
      </React.Fragment>
    )
  }

  _leaveText = () => {
    const { type } = this.props

    return (
      <React.Fragment>
        <div className={styles.firstLine}>
          <span className={styles.text}>Are you sure you want to {type}</span>
        </div>
        <div className={styles.secondLine}>
          <span className={styles.text}>this game</span>
          <span className={styles.text}>?</span>
          <span className={styles.text}>Yes</span>
          <span className={styles.text}>No</span>
        </div>
      </React.Fragment>
    )
  }

  _renderText = () => {
    const { type } = this.props

    const content = type !== 'leave' ? this._confirmationText() : this._leaveText()

    return (
      <div className={styles.textWrap}>
        {content}
        {this._renderPasswordInput()}
      </div>
    )
  }

  _renderButtons = () => {
    const { inProgress, type } = this.props

    const labels = {
      action: type === 'join' ? 'Join' : 'Yes',
      cancel: type === 'join' ? 'Cancel' : 'No'
    }

    if (inProgress) {
      return (
        <div className={styles.buttonsWrap}>
          <AnimationLoad dotsCount={5} dotsColor={type === 'leave' ? 'white' : 'black'} isAdaptive={true} />
        </div>
      )
    }

    return (
      <div className={styles.buttonsWrap}>
        <button
          ref={this._refConfirm}
          data-type='confirm'
          className={styles.button}
          type='button'
          disabled={this._preventAction()}
          onClick={this._handleClickStart}
          onKeyDown={this._handleSaveKeydown}
        >
          {labels.action}
        </button>
        <button
          data-type='cancel'
          className={styles.button}
          type='button'
          onClick={this._handleClickCancel}
          onKeyDown={this._handleCancelKeydown}
        >
          {labels.cancel}
        </button>
      </div>
    )
  }

  render() {
    const { inProgress, type } = this.props

    const classes = classnames({
      [styles.confirmWrap]: true,
      [styles.joinLayout]: type === 'join',
      [styles.leaveLayout]: type === 'leave',
      [styles.active]: inProgress
    })

    return (
      <div className={classes}>
        {this._renderText()}
        {this._renderButtons()}
      </div>
    )
  }
}

export default Confirm
