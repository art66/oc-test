import React, { memo } from 'react'

interface IHeadingLine {
  title: string
}

const HeadingLine = ({ title }: IHeadingLine) => {
  return <div className='title-black top-round'>{title}</div>
}

export default memo(HeadingLine)
