import { getTheme } from '@torn/shared/utils/getTheme'
import numberToArray from '@torn/shared/utils/numberToArray'
import React from 'react'
import styles from './index.cssmodule.scss'
import { IProps, IState, TMaxSections } from './interfaces'

const ITERATOR_FIXER = 1
const COLOR_PROGRESS_SCHEME = {
  lead: 'lead',
  second: 'second',
  third: 'third',
  last: 'last'
}

class ChipLoader extends React.PureComponent<IProps, IState> {
  private _isMounted = false
  private _isLoaded = false
  private _sectionsMax: TMaxSections = 16 // this is a constant here!!!

  constructor(props: IProps) {
    super(props)

    this.state = {
      iterationCounter: 0,
      chunksLoadedCounter: 0,
      loadPerSection: 0,
      loadProgress: 0
    }
  }

  componentDidMount() {
    this._isMounted = true
    this._init()
    this._loadAssets()
  }

  componentDidUpdate() {
    const { chunksLoadedCounter } = this.state

    // all images should be loaded
    const isLoadFinished = chunksLoadedCounter === this._sectionsMax

    if (isLoadFinished && !this._isLoaded) {
      this._finalizeLoad()

      this._isLoaded = true
    }
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  _finalizeLoad = () => {
    const { onLoadFinish } = this.props

    setTimeout(() => {
      // use timeout to show all the progress
      onLoadFinish() //   before callback fire and remove all the progress
    }, 200)
  }

  _init = async () => {
    const { assets } = this.props
    const sectionLoad = assets.length / this._sectionsMax

    // loading preloader image itself first!!!
    await this._loadImage(`/casino/loader/images/chip_${getTheme()}.svg`)

    if (!this._isMounted) {
      return
    }

    this.setState({
      loadPerSection: sectionLoad,
      loadProgress: sectionLoad // initial load progress
    })
  }

  _loadAssets = () => {
    const { assets } = this.props

    // synchronous running use this for smooth animation loader effect
    assets.reduce(
      (prevImageLoad, imageURL) => prevImageLoad.then(() => this._loadImage(imageURL)),
      // .then(() => console.log('IMAGE LOADED!!!!!!'))),
      Promise.resolve()
      // .then(() => console.log('ALL LOADED!!!!!!'))
    )

    // parallel running can give us more speed,
    // besides it hard to configure with smooth progress effect
    // Promise.all(assets.map((img: IImage) => {
    //   return this._loadImage(img.src)
    // }))
  }

  _loadImage = async (img: string) =>
    new Promise((resolve, reject) => {
      const image = new Image()

      image.onload = () => resolve(this._runProgressAnimation())

      image.onerror = (e: Event) => reject(e)

      image.src = img
    })

  _updateIteration = () => {
    if (!this._isMounted) {
      return
    }

    this.setState(prevState => ({
      iterationCounter: prevState.iterationCounter + ITERATOR_FIXER
    }))
  }

  _updateLoadProgress = () => {
    const { assets } = this.props

    if (!this._isMounted) {
      return
    }

    const updateChunks = (iterationCounter: number, loadProgress: number, loadPerSection: number) => {
      const isLastIteration = iterationCounter + ITERATOR_FIXER === assets.length

      return isLastIteration ? this._sectionsMax : loadProgress / loadPerSection
    }

    this.setState(prevState => ({
      iterationCounter: prevState.iterationCounter + ITERATOR_FIXER,
      loadProgress: prevState.loadProgress + prevState.loadPerSection,
      chunksLoadedCounter: updateChunks(prevState.iterationCounter, prevState.loadProgress, prevState.loadPerSection)
    }))
  }

  _runProgressAnimation = async () => {
    const { iterationCounter, loadProgress } = this.state

    if (iterationCounter >= loadProgress) {
      // once new chunk is fully loaded
      this._updateLoadProgress()
    } else {
      this._updateIteration()
    }
  }

  _renderUIProgress = () => {
    const { chunksLoadedCounter } = this.state

    const progressArray = chunksLoadedCounter ? numberToArray(chunksLoadedCounter) : []

    const calcColorScheme = (iteration: number) => {
      let fillColor = COLOR_PROGRESS_SCHEME.last

      if (chunksLoadedCounter - iteration === 0) {
        fillColor = COLOR_PROGRESS_SCHEME.lead
      } else if (chunksLoadedCounter - iteration === 1) {
        fillColor = COLOR_PROGRESS_SCHEME.second
      } else if (chunksLoadedCounter - iteration === 2) {
        fillColor = COLOR_PROGRESS_SCHEME.third
      }

      return fillColor
    }

    return progressArray.map((iteration: number) => (
      <i
        key={iteration}
        className={`${styles.chipLoader_chunk} ${styles[`chipLoader_chunk__${iteration}`]} ${
          styles[calcColorScheme(iteration)]
        }`}
      />
    ))
  }

  render() {
    return <div className={styles.chipLoader}>{this._renderUIProgress()}</div>
  }
}

export default ChipLoader
