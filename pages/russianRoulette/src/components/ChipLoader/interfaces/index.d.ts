export type TMaxSections = 16

export type TImage = string

export interface IProps {
  onLoadFinish: () => void
  assets: string[]
}

export interface IState {
  iterationCounter: number
  chunksLoadedCounter: number
  loadPerSection: number
  loadProgress: number
}
