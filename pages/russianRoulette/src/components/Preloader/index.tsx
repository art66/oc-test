import React, { memo } from 'react'
import styles from './index.cssmodule.scss'

const Preloader = () => {
  return (
    <div className={styles.appContainer}>
      <div className={styles.preloaderChip} />
    </div>
  )
}

export default memo(Preloader)
