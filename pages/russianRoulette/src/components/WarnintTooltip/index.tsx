import React from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import styles from './index.cssmodule.scss'

export const ANIM_PROPS = {
  className: 'inputMaxLengthTooltip',
  timeIn: 1000,
  timeExit: 300
}

class WarningTooltip extends React.PureComponent<{ isWarning: boolean; type?: 'join' }> {
  _renderInputTooltip = () => {
    const { isWarning, type } = this.props
    const { className, timeExit, timeIn } = ANIM_PROPS

    if (!isWarning) {
      return null
    }

    return (
      <TransitionGroup>
        <CSSTransition
          appear={true}
          key={isWarning && 1}
          classNames={className}
          timeout={{ enter: timeIn, exit: timeExit }}
          unmountOnExit={true}
        >
          <div className={`${styles.inputWarningTooltip} ${type ? styles[`${type}Wrap`] : ''}`}>
            20 symbols limit for password
          </div>
        </CSSTransition>
      </TransitionGroup>
    )
  }

  render() {
    return this._renderInputTooltip()
  }
}

export default WarningTooltip
