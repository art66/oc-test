import { ConnectedRouter } from 'connected-react-router'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { HashRouter, Route, Switch } from 'react-router-dom'
import { IProps } from '../interfaces/ICore'
import AppLayout from '../layout'
import { Game, Lobby } from '../routes'

class AppContainer extends Component<IProps> {
  render() {
    const { store, history } = this.props

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <HashRouter>
            <AppLayout>
              <Switch>
                <Route exact={true} path='/' component={Lobby} />
                <Route path='/game' component={Game} />
              </Switch>
            </AppLayout>
          </HashRouter>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default AppContainer
