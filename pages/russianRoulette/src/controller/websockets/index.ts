import { Dispatch } from 'react'
import { IFightInitializedWS, IGameJoinedWS, IShotFiredWS, ITakeActionWS } from '../../routes/Game/interfaces/IModules'
import { fightInitiated, opponentJoined } from '../../routes/Game/modules/actions'
import { actionTaken, gameLeft, shotFired } from '../../routes/Game/modules/websockets'
import { IFightLeavedWS } from '../../routes/Lobby/interfaces/IModules'
import { fightJoined } from '../../routes/Lobby/modules/actions'
import { debugShow } from '../actions/common'
import initUserMoneyWS from './userMoney'

const initWS = (dispatch: Dispatch<any>) => {
  // @ts-ignore global Websocket & Centrifuge handler!!!
  const handler = new WebsocketHandler('russianRoulette')

  handler.setActions({
    gameCreated: (payload: IFightInitializedWS) => {
      if (payload.data.error) {
        dispatch(debugShow(payload.data.error))

        return
      }

      dispatch(fightInitiated(payload.data))
    },
    gameJoined: (payload: IGameJoinedWS) => {
      if (payload.data.error) {
        dispatch(debugShow(payload.data.error))

        return
      }

      // in case someone joined our own fight
      if ([null, undefined].includes(payload.data.data.creator)) {
        dispatch(opponentJoined(payload.data.data))

        return
      }

      // in case we join someone other fight
      dispatch(fightJoined(payload.data))
    },
    gameLeft: (payload: IFightLeavedWS) => gameLeft(payload, dispatch),
    turnMade: (payload: IShotFiredWS) => shotFired(payload, dispatch),
    actionTaken: (payload: ITakeActionWS) => actionTaken(payload, dispatch)
  })
}

export default initWS
export { initUserMoneyWS }
