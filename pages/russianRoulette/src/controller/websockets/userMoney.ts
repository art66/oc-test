import { Dispatch } from 'react'
import { updateUserMoney } from '../actions/main'
import { IUserMoneyWS } from '../interfaces/IMain'

const initWS = (dispatch: Dispatch<any>) => {
  interface ITornWindow extends Window {
    WebsocketHandler?: {
      addEventListener: (namespace: string, action: string, callback: (data?: any) => any) => any
    }
  }

  const win = window as ITornWindow

  win.WebsocketHandler.addEventListener('sidebar', 'updateMoney', (payload: IUserMoneyWS) => {
    dispatch(updateUserMoney(String(payload.money)))
  })
}

export default initWS
