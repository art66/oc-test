import currentLocation from '../../utils/getWindowLocation'
import { IInitialState } from '../interfaces/ICommon'

const initialState: IInitialState = {
  debugCloseAction: () => {},
  locationCurrent: currentLocation,
  isDesktopManualLayout: false,
  appID: 'russian-roulette',
  pageID: null,
  debug: null,
  info: null
}

export default initialState
