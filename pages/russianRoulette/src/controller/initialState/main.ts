import { IInitialState } from '../interfaces/IMain'

const initialState: IInitialState = {
  isAssetsLoaded: false,
  isDataLoaded: false,
  isForceFetch: false,
  gameData: null,
  step: undefined,
  userMoney: null,
  showImages: true
}

export default initialState
