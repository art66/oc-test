import { IInitialState } from '../interfaces/IAudio'

// @ts-ignore
const AudioContext = window.AudioContext || window.webkitAudioContext

const initialState: IInitialState = {
  isSoundsNativeRun: new AudioContext().state === 'running'
}

export default initialState
