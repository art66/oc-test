import LocalStorageManager from '@torn/shared/utils/localStorageManager'

const { addStore, updateStore, getStore } = new LocalStorageManager()

export { addStore, updateStore, getStore }
