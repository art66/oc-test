import { push } from 'connected-react-router'
import { put } from 'redux-saga/effects'
import { TGameTypes } from '../../interfaces/IData'

const REDIRECT_PATHS = {
  lobby: {
    match: '#/',
    redirect: '?sid=russianRoulette#/'
  },
  acting: {
    match: '#/game',
    redirect: '?sid=russianRoulette#/game'
  }
}

function* pushLocation(step: TGameTypes) {
  if (!step || !REDIRECT_PATHS[step] || REDIRECT_PATHS[step].match === window.location.hash) {
    return
  }

  yield put(push(REDIRECT_PATHS[step].redirect))
}

export default pushLocation
