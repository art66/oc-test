import { select } from 'redux-saga/effects'
import { IState } from '../../interfaces/IState'

function* getState() {
  const state = yield select((store: IState) => store)

  return state
}

export default getState
