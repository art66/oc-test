import { SET_AUDIO_STATE, SOUNDS_INIT_NOTIFY } from '../../constants'
import { setAudioState } from '../actions/audio'
import initialState from '../initialState/audio'
import { IType } from '../interfaces'
import { IInitialState } from '../interfaces/IAudio'

const ACTION_HANDLERS = {
  [SET_AUDIO_STATE]: (state: IInitialState, action: ReturnType<typeof setAudioState>) => ({
    ...state,
    isSound: action.isSound
  }),
  [SOUNDS_INIT_NOTIFY]: (state: IInitialState) => ({
    ...state,
    isSoundsNativeRun: true
  })
}

const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
