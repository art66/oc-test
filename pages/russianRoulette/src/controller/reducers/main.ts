import isArray from '@torn/shared/utils/isArray'
import isEmpty from '@torn/shared/utils/isEmpty'
import isObject from '@torn/shared/utils/isObject'
import {
  INIT_ASSETS_LOADED,
  LOAD_INIT_DATA_ATTEMPT,
  LOAD_INIT_DATA_SAVED,
  SET_GAMES_DATA,
  STEP_ACTING,
  STEP_LOBBY,
  USER_MONEY_UPDATED
} from '../../constants'
import {
  ANIM_SHOT,
  ANIM_STAGE_RETAKE,
  AUTO_LEAVE_FIGHT_SAVED,
  FIGHT_FINISHED_AFTER_ACTION,
  FIGHT_INITIATED,
  FIGHT_LEAVED,
  FORCE_FETCH_ATTEMPT,
  FORCE_FETCH_SAVED,
  OPPONENT_JOINED,
  SHOT_SAVED,
  STEP_SHOT,
  TAKE_ACTION_SAVED,
  TICKER,
  UPDATE_PATRONS_BEFORE_TAKE_ACTION
} from '../../routes/Game/constants'
import {
  IFightInitiated,
  IFightJoined,
  IOpponentData,
  IShotSaved,
  ITakeActionSaved,
  ITimeData
} from '../../routes/Game/interfaces/IModules'
import enhancedGameListUpdate from '../../routes/Game/utils/enhancedGameListUpdate'
import normalizeMoves from '../../routes/Game/utils/normalizeMoves'
import normalizeTimeUpdate from '../../routes/Game/utils/normalizeTimeUpdate'
import { FIGHT_CREATED, FIGHT_EXPIRED, FIGHT_JOINED } from '../../routes/Lobby/constants'
import {
  IFightCreated,
  IFightExpired,
  ILeaveFightSaved,
  ILobbyJoinRowData
} from '../../routes/Lobby/interfaces/IModules'
import initialState from '../initialState/main'
import { IType } from '../interfaces'
import { IInitData, IInitialState, IUserMoney } from '../interfaces/IMain'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [LOAD_INIT_DATA_ATTEMPT]: (state: IInitialState) => ({
    ...state,
    isDataLoaded: false
  }),
  [LOAD_INIT_DATA_SAVED]: (state: IInitialState, action: IInitData) => ({
    ...state,
    isDataLoaded: true,
    showImages: action.user.showImages,
    gameData: isObject(action.data) ?
      {
        // if fight is started
        ...action.data,
        moves: {
          ...normalizeMoves(state.gameData?.moves, action.data.moves),
          before: action.data.moves || 0
        }
      } :
      action.data,
    step: action.step,
    userMoney: action.user.money
  }),
  [SET_GAMES_DATA]: (state: IInitialState, action) => ({
    ...state,
    gameData: action.payload
  }),
  [INIT_ASSETS_LOADED]: (state: IInitialState) => ({
    ...state,
    isAssetsLoaded: true
  }),
  [FIGHT_CREATED]: (state: IInitialState, action: IFightCreated) => ({
    ...state,
    gameData: enhancedGameListUpdate(state?.gameData, action.data)
  }),
  [FIGHT_JOINED]: (state: IInitialState, action: IFightJoined) => ({
    ...state,
    gameData: {
      ...action.data,
      moves: normalizeMoves(state.gameData?.moves, action.data.moves)
    },
    step: STEP_ACTING
  }),
  [FIGHT_EXPIRED]: (state: IInitialState, action: IFightExpired) => ({
    ...state,
    gameData: isArray(state.gameData) ?
      state.gameData?.filter((game: ILobbyJoinRowData) => game.ID !== action.expiredGame) :
      state.gameData
  }),
  [FIGHT_LEAVED]: (state: IInitialState, action: ILeaveFightSaved) => ({
    ...state,
    gameData: action.data,
    step: STEP_LOBBY
  }),
  [AUTO_LEAVE_FIGHT_SAVED]: (state: IInitialState, action: ILeaveFightSaved) => ({
    ...state,
    gameData: action.data,
    step: action.step
  }),
  [FIGHT_INITIATED]: (state: IInitialState, action: IFightInitiated) => ({
    ...state,
    gameData: {
      ...action.data,
      moves: normalizeMoves(state.gameData?.moves, action.data.moves)
    },
    step: STEP_ACTING
  }),
  [OPPONENT_JOINED]: (state: IInitialState, action: { payload: IOpponentData }) => ({
    ...state,
    gameData: {
      ...state.gameData,
      ...action.payload
    }
  }),
  [SHOT_SAVED]: (state: IInitialState, action: { payload: IShotSaved }) => ({
    ...state,
    gameData: {
      ...state.gameData,
      ...action.payload,
      whoseMove: action.payload.whoseMove || state.gameData.whoseMove,
      moves: normalizeMoves(state.gameData?.moves, action.payload.moves),
      ...(action.payload.winner && {
        // in case we won
        result: STEP_SHOT,
        winner: null,
        temp: {
          result: action.payload.result,
          winner: action.payload.winner
        }
      })
    }
  }),
  [TAKE_ACTION_SAVED]: (state: IInitialState, action: { payload: ITakeActionSaved }) => ({
    ...state,
    gameData: {
      ...state.gameData,
      ...action.payload,
      moves: state.gameData?.moves,
      message: [
        action.payload.message[0], // this is a hack for particular take action move,
        action.payload.message[0], // because for now we have there non optimized scenario
        action.payload.message[1]
      ],
      result: ANIM_STAGE_RETAKE,
      winner: null,
      temp: {
        result: action.payload.result,
        winner: action.payload.winner,
        moves: normalizeMoves(state.gameData?.moves, action.payload.moves)
      }
    }
  }),
  [UPDATE_PATRONS_BEFORE_TAKE_ACTION]: (state: IInitialState) => ({
    ...state,
    gameData: {
      ...state.gameData,
      moves: state.gameData.temp.moves
    }
  }),
  [FIGHT_FINISHED_AFTER_ACTION]: (state: IInitialState) => ({
    ...state,
    gameData: {
      ...state.gameData,
      winner: state.gameData.temp.winner,
      result: state.gameData.temp.result,
      temp: null
    }
  }),
  [TICKER]: (state: IInitialState, action: ITimeData) => ({
    ...state,
    gameData: {
      ...state.gameData,
      ...(!isEmpty(action.data) ?
        action.data :
        normalizeTimeUpdate(state.gameData.timeLeft, state.gameData.timeLeftTillTakeAction))
    }
  }),
  [ANIM_SHOT]: (state: IInitialState, action: { movesNext: number }) => ({
    ...state,
    gameData: {
      ...state.gameData,
      moves: {
        ...state.gameData.moves,
        before: state.gameData.moves.before + action.movesNext
      }
    }
  }),
  [USER_MONEY_UPDATED]: (state: IInitialState, action: IUserMoney) => ({
    ...state,
    userMoney: action.money
  }),
  [FORCE_FETCH_ATTEMPT]: (state: IInitialState) => ({
    ...state,
    isForceFetch: true
  }),
  [FORCE_FETCH_SAVED]: (state: IInitialState, action: IInitData) => ({
    ...state,
    isForceFetch: false,
    gameData: {
      ...state.gameData,
      ...action.data,
      moves: normalizeMoves(state.gameData?.moves, action.data.moves)
    },
    step: action.step,
    userMoney: action.user.money,
    showImages: action.user.showImages
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
