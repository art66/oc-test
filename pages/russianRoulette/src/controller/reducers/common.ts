import {
  HIDE_DEBUG_BOX,
  HIDE_INFO_BOX,
  LOCATION_CHANGE,
  MANUAL_DESKTOP_MODE,
  PATHS_ID,
  SHOW_DEBUG_BOX,
  SHOW_INFO_BOX
} from '../../constants'
import locationPath from '../helpers/locationPath'
import initialState from '../initialState/common'
import { IType } from '../interfaces'
import { IDebugShow, IInfoShow, IInitialState, ILocationChange, IManualDesktopStatus } from '../interfaces/ICommon'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SHOW_DEBUG_BOX]: (state: IInitialState, action: IDebugShow) => ({
    ...state,
    debug: action.msg
  }),
  [HIDE_DEBUG_BOX]: (state: IInitialState) => ({
    ...state,
    debug: null
  }),
  [SHOW_INFO_BOX]: (state: IInitialState, action: IInfoShow) => ({
    ...state,
    info: action.msg
  }),
  [HIDE_INFO_BOX]: (state: IInitialState) => ({
    ...state,
    info: null
  }),
  [MANUAL_DESKTOP_MODE]: (state: IInitialState, action: IManualDesktopStatus) => ({
    ...state,
    isDesktopManualLayout: action.status
  }),
  [LOCATION_CHANGE]: (state: IInitialState, action: ILocationChange) => {
    const currentPath = locationPath(action.payload.location.hash)

    return {
      ...state,
      debug: null,
      info: null,
      locationCurrent: currentPath,
      pageID: PATHS_ID[currentPath]
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
