export interface IInitialState {
  mediaType: 'desktop' | 'tablet' | 'mobile'
  is: {
    desktop: boolean
    tablet: boolean
    mobile: boolean
  }
}
