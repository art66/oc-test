import { TGameTypes } from '../../interfaces/IData'
import { IFightData, IFightPayloadData } from '../../routes/Game/interfaces/IModules'
import { ILobbyJoinRowData } from '../../routes/Lobby/interfaces/IModules'

export interface IInitialState {
  isAssetsLoaded: boolean
  isDataLoaded: boolean
  isForceFetch: boolean
  step: TGameTypes
  gameData: ILobbyJoinRowData[] & IFightData
  userMoney: number
  showImages: boolean
}

export interface IInitData {
  data: ILobbyJoinRowData[] & IFightPayloadData
  step: TGameTypes
  user: {
    money: number
    showImages: boolean
  }
}

export interface IUserMoneyWS {
  money: number
}

export interface IUserMoney {
  money: string
}
