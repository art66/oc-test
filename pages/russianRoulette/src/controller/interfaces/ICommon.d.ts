import { IType } from '.'

export type TDebugMessage = string | object

export interface IDebugShow extends IType {
  msg: TDebugMessage
}

export interface IInfoShow extends IType {
  msg: TDebugMessage
}

export interface ILocationChange extends IType {
  payload: {
    location: {
      hash: string
    }
  }
}

export interface IManualDesktopStatus extends IType {
  status: boolean
}

export type TAppID = 'russian-roulette'

export interface IInitialState {
  isDesktopManualLayout?: any
  locationCurrent?: string
  appID: TAppID
  pageID: number
  info: string
  debug: {
    msg: string | object
  }
  debugCloseAction: () => void
}
