import isValue from '@torn/shared/utils/isValue'
import { select } from 'redux-saga/effects'
import { addStore, getStore, updateStore } from '../helpers/localStorage'
import { mute, unmute } from '../services/sound'
import { IState } from '../../interfaces/IState'

export function* setAudioSaga() {
  const soundOn = yield select((state: IState) => state.audio.isSound)

  const localStorageValue = { storeKey: 'rrSound', storeData: !!soundOn }

  isValue(getStore({ storeKey: 'rrSound' })) ? updateStore(localStorageValue) : addStore(localStorageValue)

  soundOn ? unmute() : mute()
}
