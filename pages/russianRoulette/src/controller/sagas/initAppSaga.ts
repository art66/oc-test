import { call, put } from 'redux-saga/effects'
import { setAudioState } from '../actions/audio'
import { loadInitData } from '../actions/main'
import { getStore } from '../helpers/localStorage'
import { initSounds } from '../services/sound'

export function* initAppSaga() {
  yield put(loadInitData())

  yield call(initSounds)

  yield put(setAudioState(getStore({ storeKey: 'rrSound' })))
}
