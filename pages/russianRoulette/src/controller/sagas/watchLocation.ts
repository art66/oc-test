import { IState } from '../../interfaces/IState'
import getState from '../helpers/getState'
import interceptInGameRedirect from '../helpers/pushLocation'

function* watchLocation() {
  const state: IState = yield getState()

  yield interceptInGameRedirect(state.main?.step)
}

export default watchLocation
