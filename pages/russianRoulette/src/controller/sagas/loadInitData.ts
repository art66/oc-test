import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { ROOT_URL } from '../../constants'
import { GENERIC_ERROR } from '../../constants/errors'
import { debugShow } from '../actions/common'
import { loadInitDataSaved } from '../actions/main'
import interceptInGameRedirect from '../helpers/pushLocation'
import { IInitData } from '../interfaces/IMain'

function* loadInitData() {
  let payload: IInitData = null

  try {
    payload = yield fetchUrl(ROOT_URL)
  } catch (e) {
    yield put(debugShow(GENERIC_ERROR))

    throw new Error(`${GENERIC_ERROR}: ${e}`)
  }

  // in case game already started navigate user in game
  yield interceptInGameRedirect(payload?.step)

  yield put(loadInitDataSaved({ ...payload }))
}

export default loadInitData
