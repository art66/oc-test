import { LOCATION_CHANGE } from 'react-router-redux'
import { takeLatest } from 'redux-saga/effects'
import { INIT_APP, LOAD_INIT_DATA_ATTEMPT, SET_AUDIO_STATE } from '../../constants'
import { AUTO_LEAVE_FIGHT_SAVED, FIGHT_INITIATED, FIGHT_LEAVED, FORCE_FETCH_SAVED } from '../../routes/Game/constants'
import { FIGHT_JOINED } from '../../routes/Lobby/constants'
import { initAppSaga } from './initAppSaga'
import loadInitData from './loadInitData'
import { setAudioSaga } from './setAudioSaga'
import watchLocation from './watchLocation'

export default function* rootSaga() {
  yield takeLatest(INIT_APP, initAppSaga)
  yield takeLatest(SET_AUDIO_STATE, setAudioSaga)
  yield takeLatest(LOAD_INIT_DATA_ATTEMPT, loadInitData)
  yield takeLatest(LOCATION_CHANGE, watchLocation)
  yield takeLatest(FIGHT_INITIATED, watchLocation)
  yield takeLatest(FIGHT_JOINED, watchLocation)
  yield takeLatest(FIGHT_LEAVED, watchLocation)
  yield takeLatest(AUTO_LEAVE_FIGHT_SAVED, watchLocation)
  yield takeLatest(FORCE_FETCH_SAVED, watchLocation)
}
