import { Howl } from 'howler'

let sounds

const tracksList = ['bang', 'join', 'blank'] as const

export type TSoundID = typeof tracksList[number]

const loadSounds = (list, basePath: string) =>
  list.map(track => ({
    soundID: track,
    sound: new Howl({ src: [`${basePath}/${track}.mp3`, `${basePath}/${track}.ogg`] })
  }))

export const playSound = (soundID: TSoundID) => {
  try {
    sounds?.find(snd => snd.soundID === soundID)?.sound?.play()
  } catch (e) {
    console.error(`Error occurred when playing sound ${soundID}`)
  }
}

export const mute = () => {
  sounds?.forEach(({ sound }) => {
    sound.mute(true)
  })
}

export const unmute = () => {
  sounds?.forEach(({ sound }) => {
    sound.mute(false)
  })
}

export const initSounds = () => {
  sounds = loadSounds(tracksList, '/casino/russianRoulette/audio/')
}
