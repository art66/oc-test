const WORKER_PATH = __DEV__ ? 'worker.js' : '/js/webWorkers/russianRoulette.js'

const startWebWorker = ({ callback }: { callback: (e: MessageEvent) => void }) => {
  if (!window || !window.Worker) {
    console.error('Webworkers are not allowed by tye browser!')

    return null
  }

  const rrWorker = new Worker(WORKER_PATH)

  rrWorker.onmessage = e => {
    callback(e)
  }

  rrWorker.onerror = e => {
    rrWorker.terminate()

    console.log(`Some error happen during work: ${e}`)
  }

  return rrWorker
}

export default startWebWorker
