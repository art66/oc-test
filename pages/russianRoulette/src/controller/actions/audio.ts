import { SET_AUDIO_STATE, SOUNDS_INIT_NOTIFY } from '../../constants'

export const setAudioState = (isSound: boolean) => ({
  isSound,
  type: SET_AUDIO_STATE
})

export const notifySoundRun = () => ({
  status,
  type: SOUNDS_INIT_NOTIFY
})
