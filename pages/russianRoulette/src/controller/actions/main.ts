import {
  INIT_APP,
  INIT_ASSETS_LOADED,
  LOAD_INIT_DATA_ATTEMPT,
  LOAD_INIT_DATA_SAVED,
  SET_GAMES_DATA,
  USER_MONEY_UPDATED
} from '../../constants'
import { ILobbyJoinRowData } from '../../routes/Lobby/interfaces/IModules'
import { IType } from '../interfaces'
import { IInitData, IUserMoney } from '../interfaces/IMain'

export const initApp = (): IType => ({
  type: INIT_APP
})

export const loadInitData = (): IType => ({
  type: LOAD_INIT_DATA_ATTEMPT
})

export const loadInitDataSaved = ({ data, step, user }: IInitData): IInitData & IType => ({
  data,
  step,
  user,
  type: LOAD_INIT_DATA_SAVED
})

export const setGamesData = (payload: ILobbyJoinRowData[]) => ({
  payload,
  type: SET_GAMES_DATA
})

export const loadInitAssetsSaved = (): IType => ({
  type: INIT_ASSETS_LOADED
})

export const updateUserMoney = (money: string): IUserMoney & IType => ({
  type: USER_MONEY_UPDATED,
  money
})
