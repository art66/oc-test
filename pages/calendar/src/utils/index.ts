import { getCookie } from '@torn/shared/utils'
import { AMOUNT_DAYS_IN_WEEK, SUNDAY } from '../constants'

export function fetchUrl(url: string, data?: any) {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      let json

      try {
        json = JSON.parse(text)

        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = `Server responded with: ${text}`
      }

      if (error) {
        throw new Error(error.message)
      } else {
        return json
      }
    })
  })
}

export function convertDateToObject(date: Date): any {
  return {
    day: date.getUTCDate(),
    month: date.getUTCMonth() + 1,
    year: date.getUTCFullYear(),
    weekday: date.getUTCDay() === 0 ? AMOUNT_DAYS_IN_WEEK : date.getUTCDay()
  }
}

export const checkEqualityOfDaysWithYear = (firstDate, secondDate) => {
  const { day, month, year } = firstDate
  const { day: controlDay, month: controlMonth, year: controlYear } = secondDate

  return day === controlDay && month === controlMonth && year === controlYear
}

export const checkEqualityOfDaysWithoutYear = (firstDate, secondDate) => {
  const { day, month } = firstDate
  const { day: controlDay, month: controlMonth } = secondDate

  return day === controlDay && month === controlMonth
}

export const getWeekday = date => {
  const { year, month, day } = date

  return new Date(Date.UTC(year, month - 1, day)).getUTCDay() || SUNDAY
}

export const addLeadZero = num => {
  return num < 10 ? `0${num}` : `${num}`
}

export const checkDarkMode = () => {
  const DMCookieValue = getCookie('darkModeEnabled')

  return DMCookieValue === 'true'
}
