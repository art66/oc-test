import React from 'react'
import { connect } from 'react-redux'
import AppHeader from '@torn/shared/components/AppHeader'
import { TOGGLE_DARK_MODE_EVENT } from '@torn/header/src/constants'
import { IClientProps } from '@torn/shared/components/AppHeader/container/types'
import { fetchCalendarData, setCurrentDate, setDarkModeState } from '../actions'
import { FULL_YEAR_VIEW } from '../constants'
import Calendar from './Calendar'
import MonthView from './MonthView'
import Banner from './Banner'
import { checkDarkMode } from '../utils'
import IEvent from '../interfaces/IEvent'
import './vars.scss'
import './DM_vars.scss'
import './base.scss'

interface IProps {
  fetchCalendarData: () => void
  setCurrentDate: () => void
  setDarkModeState: (enabled: boolean) => void
  calendar: {
    viewMode: string
    events: IEvent[]
    appHeader: IClientProps
  }
}

class App extends React.Component<IProps> {
  componentDidMount() {
    this.props.setCurrentDate()
    this.props.fetchCalendarData()
    window.addEventListener(TOGGLE_DARK_MODE_EVENT, this.updateDarkModeState)
    this.updateDarkModeState()
  }

  componentWillUnmount() {
    window.removeEventListener(TOGGLE_DARK_MODE_EVENT, this.updateDarkModeState)
  }

  updateDarkModeState = () => {
    const darkModeEnabled = checkDarkMode()

    this.props.setDarkModeState(darkModeEnabled)
  }

  render() {
    const {
      calendar: { events, viewMode, appHeader }
    } = this.props
    const isFullYearView = viewMode === FULL_YEAR_VIEW

    return events ? (
      <div className='calendar-container'>
        <AppHeader appID='Calendar' clientProps={appHeader} />
        <Banner />
        {isFullYearView ? <Calendar /> : <MonthView />}
      </div>
    ) : null
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar
})

const mapDispatchToProps = {
  fetchCalendarData,
  setCurrentDate,
  setDarkModeState
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
