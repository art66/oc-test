import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import MonthNav from './MonthNav'
import MonthGrid from '../MonthGrid'
import { setFullYearMode } from '../../actions'
import IDate from '../../interfaces/IDate'

import s from './styles.cssmodule.scss'

interface IProps {
  calendar: {
    currentDate: IDate
    viewedMonth: number
  }
  browser: {
    mediaType: string
  }
  setFullYearMode: () => void
}

class MonthView extends React.Component<IProps> {
  componentDidUpdate() {
    const { browser } = this.props

    if (browser.mediaType === 'desktop') {
      this.props.setFullYearMode()
    }
  }

  render() {
    const { calendar } = this.props
    const isPassedMonth = calendar.viewedMonth < calendar.currentDate.month

    return (
      <div className={`month-view ${cn(s.monthView, { [s.passed]: isPassedMonth })}`}>
        <MonthNav isPassedMonth={isPassedMonth} />
        <MonthGrid monthNumber={calendar.viewedMonth} isPassedMonth={isPassedMonth} customClass='monthView' />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar,
  browser: state.browser
})

const mapDispatchToProps = {
  setFullYearMode
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MonthView)
