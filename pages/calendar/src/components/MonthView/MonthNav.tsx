import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import {
  DISABLE_FILL,
  DEFAULT_GRADIENT_FILL,
  DEFAULT_FILL,
  DM_DISABLED_GRADIENT
} from '@torn/shared/SVG/presets/icons/calendar'
import { MONTHS, JANUARY, DECEMBER } from '../../constants'
import { setActiveMonth, setFullYearMode } from '../../actions'
import { ICON_PRESETS, ICON_TYPE } from '../../constants/icon'
import s from './styles.cssmodule.scss'

interface IProps {
  setActiveMonth: (monthNumber: number) => void
  setFullYearMode: () => void
  isPassedMonth: boolean
  calendar: {
    monthsMetadata: any
    viewedMonth?: number
    darkModeEnabled?: boolean
  }
}

class MonthNav extends React.Component<IProps> {
  getArrowsGradient = month => {
    const {
      calendar: { viewedMonth }
    } = this.props

    return viewedMonth === month ? { gradient: DM_DISABLED_GRADIENT } : {}
  }

  goToNextMonth = () => {
    const { calendar } = this.props

    if (calendar.viewedMonth < DECEMBER) {
      this.props.setActiveMonth(calendar.viewedMonth + 1)
    }
  }

  goToPrevMonth = () => {
    const { calendar } = this.props

    if (calendar.viewedMonth > JANUARY) {
      this.props.setActiveMonth(calendar.viewedMonth - 1)
    }
  }

  renderMonthEventTitleIcon = monthEventTitle => {
    const { isPassedMonth } = this.props

    return monthEventTitle ? (
      <div className={cn(s.icon, s[monthEventTitle.icon])}>
        <SVGIconGenerator
          iconName={monthEventTitle.icon}
          preset={{ type: ICON_TYPE, subtype: ICON_PRESETS[monthEventTitle.icon].subtype }}
          fill={isPassedMonth ? DISABLE_FILL : DEFAULT_FILL}
          customClass={`disableFilter calendarIcons ${s.eventIcons} ${s[monthEventTitle.icon]}`}
        />
      </div>
    ) : null
  }

  renderMonthEventTitle = (monthEventTitle, monthNumber) => {
    return (
      <>
        <span className={s.monthTitle}>{`${MONTHS[monthNumber - 1]}${monthEventTitle ? ':' : ''}`}</span>
        {monthEventTitle ? <span className={s.eventTitle}>{monthEventTitle.title}</span> : ''}
      </>
    )
  }

  render() {
    const { calendar } = this.props
    const { monthsMetadata } = calendar
    const monthNumber = calendar.viewedMonth
    const monthKey = MONTHS[monthNumber - 1].toLowerCase()
    const monthEventTitle = monthsMetadata && monthsMetadata[monthKey]

    return (
      <div className={s.monthNav}>
        <div className={s.monthHeader}>
          <button
            type='button'
            className={cn(s.prevMonth, s.navButton)}
            onClick={this.goToPrevMonth}
            aria-label='Go to previous month'
          >
            <SVGIconGenerator
              iconName='ArrowLeftThin'
              preset={{ type: ICON_TYPE, subtype: ICON_PRESETS['ArrowLeftThin'].subtype }}
              customClass={cn('disableFilter', 'calendarIcons', { [s.disabled]: calendar.viewedMonth === JANUARY })}
              fill={calendar.viewedMonth === JANUARY ? DISABLE_FILL : DEFAULT_GRADIENT_FILL}
              {...this.getArrowsGradient(JANUARY)}
            />
          </button>
          {this.renderMonthEventTitleIcon(monthEventTitle)}
          {this.renderMonthEventTitle(monthEventTitle, monthNumber)}
          <button
            type='button'
            className={cn(s.nextMonth, s.navButton)}
            onClick={this.goToNextMonth}
            aria-label='Go to next month'
          >
            <SVGIconGenerator
              iconName='ArrowRightThin'
              preset={{ type: ICON_TYPE, subtype: ICON_PRESETS['ArrowRightThin'].subtype }}
              customClass={cn('disableFilter', 'calendarIcons', { [s.disabled]: calendar.viewedMonth === DECEMBER })}
              fill={calendar.viewedMonth === DECEMBER ? DISABLE_FILL : DEFAULT_GRADIENT_FILL}
              {...this.getArrowsGradient(DECEMBER)}
            />
          </button>
        </div>
        <div className={s.backToYearWrapper}>
          <button
            type='button'
            className={s.navButton}
            onClick={this.props.setFullYearMode}
            aria-label='Back to full year'
          >
            <SVGIconGenerator
              iconName='Back'
              preset={{ type: ICON_TYPE, subtype: ICON_PRESETS['Back'].subtype }}
              customClass='disableFilter calendarIcons'
            />
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar
})

const mapDispatchToProps = {
  setActiveMonth,
  setFullYearMode
}

export default connect(mapStateToProps, mapDispatchToProps)(MonthNav)
