import React, { PureComponent } from 'react'
import s from './styles.cssmodule.scss'

class Banner extends PureComponent {
  render() {
    return <div className={s.banner} />
  }
}

export default Banner
