import React, { Component } from 'react'
import cn from 'classnames'
import { connect } from 'react-redux'
import s from './styles.cssmodule.scss'
const ARROW_HEIGHT = 10
const DEFAULT_OFFSET = 10
const ARROW_OFFSET = 16

interface IProps {
  parent: string
  position: string
  arrow: string
  isSingleDayEvent: boolean
  bindTo: string
  children: any
  browser: {
    is: {
      mobile: boolean
    }
    lessThan: {
      desktop: boolean
    }
  }
}

interface IState {
  isTooltipActive: boolean
  left: number
  top: number
  arrowTop: number
  arrowLeft: number
}

class Tooltip extends Component<IProps, IState> {
  parent: NodeListOf<HTMLElement>
  tooltip: HTMLElement

  constructor(props: IProps) {
    super(props)
    this.state = {
      isTooltipActive: false,
      left: 0,
      top: 0,
      arrowLeft: 0,
      arrowTop: 0
    }
  }

  componentDidMount() {
    const { parent } = this.props
    const selector = `div[data-tooltipid=${parent}]`

    this.parent = document.querySelectorAll(selector)

    if (!this.parent) {
      return
    }

    this.calcTooltipPosition()
    window.addEventListener('resize', this.hideTooltip)
    window.addEventListener('resize', this.calcTooltipPosition)
    window.addEventListener('load', this.calcTooltipPosition)
    window.addEventListener('click', this.hideContextMenuOnContentClick)
    this.parent.forEach(element => {
      element.addEventListener('blur', this.hideTooltip)
      element.addEventListener('focus', this.showTooltip)
      element.addEventListener('click', this.showTooltip)
      element.addEventListener('mouseleave', this.hideTooltip)
      element.addEventListener('mouseenter', this.showTooltip)
    })
  }

  componentWillUnmount() {
    if (!this.parent) {
      return
    }

    window.removeEventListener('click', this.hideContextMenuOnContentClick)
    window.removeEventListener('resize', this.calcTooltipPosition)
    window.removeEventListener('load', this.calcTooltipPosition)
    window.removeEventListener('resize', this.hideTooltip)
    this.parent.forEach(element => {
      element.removeEventListener('blur', this.hideTooltip)
      element.removeEventListener('focus', this.showTooltip)
      element.removeEventListener('click', this.showTooltip)
      element.removeEventListener('mouseleave', this.hideTooltip)
      element.removeEventListener('mouseenter', this.showTooltip)
    })
  }

  getCoords = elem => {
    const box = elem.getBoundingClientRect()

    return {
      top: box.top + pageYOffset,
      left: box.left + pageXOffset,
      width: box.width,
      height: box.height
    }
  }

  calcLeftPosition = (rect, width) => {
    const { browser } = this.props
    const defaultOffset = browser.is.mobile ? 0 : DEFAULT_OFFSET
    const screenWidth = document.body.offsetWidth
    const leftPos = rect.left + rect.width / 2 - width / 2
    const maxLeftPos = screenWidth - width - defaultOffset

    if (leftPos < defaultOffset) {
      return defaultOffset
    }

    if (leftPos > maxLeftPos) {
      return leftPos - (leftPos - maxLeftPos)
    }

    return leftPos
  }

  calcTooltipPosition = () => {
    const { bindTo, position } = this.props
    const bindToEl = document.getElementById(bindTo)

    if (!bindToEl || !this.tooltip) {
      return
    }

    const rect = this.getCoords(bindToEl)
    const { width, height } = this.tooltip.getBoundingClientRect()

    if (position === 'bottom') {
      const left = this.calcLeftPosition(rect, width)

      this.setState({
        left,
        top: rect.top + rect.height + ARROW_HEIGHT,
        arrowTop: rect.top + rect.height - ARROW_OFFSET,
        arrowLeft: rect.left + rect.width / 2
      })
    }

    if (position === 'top') {
      const left = this.calcLeftPosition(rect, width)

      this.setState({
        left,
        top: rect.top - height - ARROW_HEIGHT,
        arrowTop: rect.top - ARROW_HEIGHT,
        arrowLeft: rect.left + rect.width / 2
      })
    }
  }

  hideContextMenuOnContentClick = e => {
    const closestParent = e.target.closest('#calendar-root')

    if (e && e.target && closestParent && closestParent.length === 0) {
      this.hideTooltip()
    }
  }

  showTooltip = () => {
    const { isTooltipActive } = this.state

    this.calcTooltipPosition()
    if (!isTooltipActive) {
      this.setState({ isTooltipActive: true })
    }
  }

  hideTooltip = () => {
    const { isTooltipActive } = this.state

    this.calcTooltipPosition()

    if (isTooltipActive) {
      if ('ontouchstart' in document.documentElement) {
        setTimeout(() => this.setState({ isTooltipActive: false }), 10)
      } else {
        this.setState({ isTooltipActive: false })
      }
    }
  }

  toggleTooltip = () => {
    const { isTooltipActive } = this.state

    this.calcTooltipPosition()
    this.setState({ isTooltipActive: !isTooltipActive })
  }

  render() {
    const { children, position } = this.props
    const { isTooltipActive, top, left, arrowLeft, arrowTop } = this.state
    const style = {
      top,
      left
    }
    const arrowStyle = {
      left: arrowLeft,
      top: arrowTop
    }

    return (
      <div className={cn(s.tooltipWrapper, s[position], { [s.show]: isTooltipActive })}>
        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
        <div
          className={cn(s.calendarTooltip)}
          style={style}
          ref={el => (this.tooltip = el)}
          onClick={this.hideTooltip}
          onMouseEnter={this.hideTooltip}
        >
          <div>{children}</div>
        </div>
        <div className={s.firstArrow} style={arrowStyle} />
        <div className={s.secondArrow} style={arrowStyle} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  browser: state.browser
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Tooltip)
