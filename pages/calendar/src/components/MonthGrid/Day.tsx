import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { AMOUNT_DAYS_IN_WEEK, SUNDAY, MONDAY, MONTHS, THURSDAY, MONTH_VIEW, FULL_YEAR_VIEW } from '../../constants'
import {
  DM_MULTI_DAY_EVENT_OPACITY_PASSED,
  DM_SINGLE_DAY_EVENT_OPACITY_PASSED,
  MULTI_DAY_EVENT_OPACITY,
  MULTI_DAY_EVENT_OPACITY_PASSED,
  SINGLE_DAY_EVENT_OPACITY,
  SINGLE_DAY_EVENT_OPACITY_PASSED
} from '../../constants/styles'
import Tooltip from '../Tooltip'
import IEvent from '../../interfaces/IEvent'
import IDate from '../../interfaces/IDate'
import IBrowser from '../../interfaces/IBrowser'
import { addLeadZero, checkEqualityOfDaysWithoutYear, getWeekday } from '../../utils'
import IColor from '../../interfaces/IColor'
import s from './styles.cssmodule.scss'
import sTip from '../Tooltip/styles.cssmodule.scss'

interface IProps {
  calendar: {
    viewMode: string
    currentDate: IDate
    darkModeEnabled?: boolean
  }
  browser: IBrowser
  dayNumber: number
  events?: IEvent[]
  isActiveMonth: boolean
  monthNumber: number
  isPassedMonth: boolean
  dayAfterEvent?: {
    isDayAfterEvent: boolean
    eventColor: IColor | IEvent
    isPassedEvent: boolean
    isEarless: boolean
  }
  dayBeforeEvent?: {
    isDayBeforeEvent: boolean
    eventColor: IColor | IEvent
    isPassedEvent: boolean
    isEarless: boolean
  }
}

interface IState {
  allowTooltipsInFullYearView: boolean
}

class Day extends React.Component<IProps, IState> {
  element: any

  constructor(props: IProps) {
    super(props)
    this.state = {
      allowTooltipsInFullYearView: true
    }
  }

  componentDidMount = () => {
    this.checkAllowingTooltipsInFullYearViewMode()
    window.addEventListener('load', this.checkAllowingTooltipsInFullYearViewMode)
    window.addEventListener('resize', this.checkAllowingTooltipsInFullYearViewMode)
  }

  componentWillUnmount = () => {
    window.removeEventListener('load', this.checkAllowingTooltipsInFullYearViewMode)
    window.removeEventListener('resize', this.checkAllowingTooltipsInFullYearViewMode)
  }

  getElementIDForEvtEndedInNextMonth = eventStartDate => {
    const {
      calendar: {
        currentDate: { year }
      },
      monthNumber
    } = this.props

    if (eventStartDate.weekday < THURSDAY) {
      return this.getElementIDForEvtWithShift(eventStartDate)
    }

    return `active-date-${addLeadZero(eventStartDate.day)}${addLeadZero(monthNumber)}${year}`
  }

  getElementIDForEvtStartedInPrevMonth = eventEndDate => {
    const {
      calendar: {
        currentDate: { year }
      },
      monthNumber
    } = this.props
    const eventStartWeekday = getWeekday({ day: 1, month: monthNumber, year })
    const eventEndWeekday = eventEndDate.day > SUNDAY - eventStartWeekday ? SUNDAY : eventEndDate.weekday

    if (eventStartWeekday < THURSDAY) {
      if (eventEndWeekday <= THURSDAY) {
        return `active-date-${addLeadZero(eventEndDate.day)}${addLeadZero(monthNumber)}${year}`
      }
      const diff = THURSDAY - eventStartWeekday

      return `active-date-${addLeadZero(1 + diff)}${addLeadZero(monthNumber)}${year}`
    }

    return `active-date-${addLeadZero(1)}${addLeadZero(monthNumber)}${year}`
  }

  getElementIDForEvtWithShift = eventStartDate => {
    const {
      calendar: {
        currentDate: { year }
      },
      monthNumber
    } = this.props
    const diff = THURSDAY - eventStartDate.weekday

    return `active-date-${addLeadZero(eventStartDate.day + diff)}${addLeadZero(monthNumber)}${year}`
  }

  getDefaultElementID = eventStartDate => {
    const {
      calendar: {
        currentDate: { year }
      },
      monthNumber
    } = this.props

    return `active-date-${addLeadZero(eventStartDate.day)}${addLeadZero(monthNumber)}${year}`
  }

  getElementIDForSingleDayEvt = eventEndDate => {
    const {
      calendar: {
        currentDate: { year }
      },
      monthNumber
    } = this.props

    return `active-date-${addLeadZero(eventEndDate.day)}${addLeadZero(monthNumber)}${year}`
  }

  getElementIDForBindingTooltip = event => {
    const { monthNumber } = this.props
    const { end: eventEndDate, start: eventStartDate } = event.date
    const isSingleDayEvent = this.isSingleDayEvent()

    if (isSingleDayEvent) {
      return this.getElementIDForSingleDayEvt(eventEndDate)
    }

    if (eventEndDate.month !== monthNumber) {
      return this.getElementIDForEvtEndedInNextMonth(eventStartDate)
    }

    if (eventStartDate.month !== monthNumber) {
      return this.getElementIDForEvtStartedInPrevMonth(eventEndDate)
    }

    if (eventStartDate.weekday < THURSDAY) {
      return this.getElementIDForEvtWithShift(eventStartDate)
    }

    return this.getDefaultElementID(eventStartDate)
  }

  getEvent = () => {
    const { events } = this.props
    const isSingleDayEvent = this.isSingleDayEvent()
    const singleDayEvt = events?.find(event => event.date.start.day === event.date.end.day)
    const multiDayEvt = events?.find(
      event => event.date.start.day < event.date.end.day || event.date.start.month < event.date.end.month
    )

    return isSingleDayEvent ? singleDayEvt : multiDayEvt
  }

  getFormattedEventDateString = (isSingleDayEvent, eventDate) => {
    const { start, end } = eventDate

    if (isSingleDayEvent) {
      return `Event date: ${start.day} ${MONTHS[start.month - 1]} ${start.year}.`
    }

    const startDate = `Event start date: ${start.day} ${MONTHS[start.month - 1]} ${start.year}.`
    const endDate = `Event end date: ${end.day} ${MONTHS[end.month - 1]} ${end.year}.`

    return `${startDate} ${endDate}`
  }

  getParamsForWAI = event => {
    const {
      dayNumber,
      monthNumber,
      calendar: {
        currentDate: { year }
      }
    } = this.props
    const isSingleDayEvent = this.isSingleDayEvent()
    let tabIndex = -1
    let label = ''

    if (event) {
      const { day: startDay, month: startMonth, year: startYear } = event.date.start
      const dateMsg = this.getFormattedEventDateString(isSingleDayEvent, event.date)

      if (dayNumber === startDay && monthNumber === startMonth && year === startYear) {
        tabIndex = 0
        label = `${event.title}. ${dateMsg} ${event.description}`
      }
    }
    return {
      tabIndex,
      label
    }
  }

  getClassForDayNumber = () => {
    const {
      dayNumber,
      monthNumber,
      calendar: {
        currentDate: { year }
      }
    } = this.props
    const weekday = getWeekday({ year, month: monthNumber, day: dayNumber })
    const isSingleDayEvent = this.isSingleDayEvent()
    const competition = this.findCompetition()
    const startCompDay = competition && competition.date.start.day
    const endCompDay = competition && competition.date.end.day

    return cn({
      [s.dayNumber]: true,
      [s.singleDayEvent]: isSingleDayEvent,
      [s.competition]: competition,
      [s.top]: competition && this.competitionHasTopBorder(startCompDay),
      [s.bottom]: competition && this.competitionsHasBottomBorder(endCompDay),
      [s.right]: competition && this.competitionHasRightBorder(endCompDay, weekday),
      [s.left]: competition && this.competitionHasLeftBorder(startCompDay, weekday)
    })
  }

  getClassForDay = (competition, event) => {
    const { isActiveMonth } = this.props

    return cn({
      [s.day]: true,
      [s.notActive]: !isActiveMonth,
      [s.competition]: competition,
      [s.current]: this.isCurrentDay(),
      [s.passedEvent]: event?.isPassed
    })
  }

  getColor = (color, opacity) => `rgba(${color.r}, ${color.g}, ${color.b}, ${opacity})`

  getDayGradient = (color, extremeOpacity, middleOpacity) => {
    const extremeColor = this.getColor(color, extremeOpacity)
    const middleColor = this.getColor(color, middleOpacity)

    return `linear-gradient(90deg, ${extremeColor} 0%, ${middleColor} 50%, ${extremeColor} 100%)`
  }

  getGradient = (color, startOpacity, endOpacity) => {
    const startColor = this.getColor(color, startOpacity)
    const endColor = this.getColor(color, endOpacity)

    return `linear-gradient(90deg, ${startColor} 0%, ${endColor} 100%)`
  }

  getStyles = event => {
    const isSingleDayEvent = this.isSingleDayEvent()
    const competition = this.findCompetition()

    if (!event) {
      return null
    }

    if (isSingleDayEvent) {
      const { dayGradientBg } = this.getOpacityParamsForSingleDayEvent(event.isPassed)
      const borderColor = competition ? competition.color : event.color
      const { border } = this.getOpacityParamsForMultiDayEvent(competition ? competition.isPassed : event.isPassed)

      return {
        borderColor: this.getColor(borderColor, border),
        background: event.isEarless ?
          'transparent' :
          this.getDayGradient(event.color, dayGradientBg.middle, dayGradientBg.end)
      }
    }

    const { border: borderOpacity } = this.getOpacityParamsForMultiDayEvent(event.isPassed)

    return {
      borderColor: this.getColor(event.color, borderOpacity)
    }
  }

  getCompetitionStyles = event => {
    const competition = this.findCompetition()

    if (!event || !competition) {
      return null
    }

    const { background } = this.getOpacityParamsForMultiDayEvent(competition.isPassed)

    return {
      backgroundColor: this.getColor(competition.color, background)
    }
  }

  getNumberWrapperStyles = event => {
    const isSingleDayEvent = this.isSingleDayEvent()
    const opacity = this.isCurrentDay() ? SINGLE_DAY_EVENT_OPACITY.currentDayBg : SINGLE_DAY_EVENT_OPACITY.numberBg

    if (!event || !isSingleDayEvent) {
      return null
    }

    if (event.isPassed) {
      const {
        calendar: { darkModeEnabled }
      } = this.props

      const borderOpacity = darkModeEnabled ?
        DM_SINGLE_DAY_EVENT_OPACITY_PASSED.numberBorder :
        SINGLE_DAY_EVENT_OPACITY_PASSED.numberBorder

      return {
        border: `1px solid ${this.getColor(event.color, borderOpacity)}`
      }
    }

    const eventOpacity = event.isEarless ? opacity + SINGLE_DAY_EVENT_OPACITY.dayGradientBg.middle : opacity

    return {
      backgroundColor: this.getColor(event.color, eventOpacity)
    }
  }

  getOpacityParamsForMultiDayEvent = isPassed => {
    const {
      calendar: { darkModeEnabled }
    } = this.props

    if (isPassed) {
      return darkModeEnabled ? DM_MULTI_DAY_EVENT_OPACITY_PASSED : MULTI_DAY_EVENT_OPACITY_PASSED
    }

    return MULTI_DAY_EVENT_OPACITY
  }

  getOpacityParamsForSingleDayEvent = isPassed => {
    const {
      calendar: { darkModeEnabled }
    } = this.props

    if (isPassed) {
      return darkModeEnabled ? DM_SINGLE_DAY_EVENT_OPACITY_PASSED : SINGLE_DAY_EVENT_OPACITY_PASSED
    }

    return SINGLE_DAY_EVENT_OPACITY
  }

  getStylesForSideGradient = (daySettings, side) => {
    if (!daySettings || !daySettings.eventColor) {
      return null
    }

    const {
      dayGradientBg: { middle, start }
    } = this.getOpacityParamsForSingleDayEvent(daySettings.isPassedEvent)

    return {
      background:
        side === 'right' ?
          this.getGradient(daySettings.eventColor, middle, start) :
          this.getGradient(daySettings.eventColor, start, middle)
    }
  }

  getIDForTooltipAttribute = (competition, event) => {
    if (this.isSingleDayEvent()) {
      return this.getElementIDForBindingTooltip(event)
    }

    if (competition) {
      return this.getElementIDForBindingTooltip(competition)
    }

    return null
  }

  competitionHasTopBorder = startCompDay => {
    const { dayNumber } = this.props

    return dayNumber === startCompDay || dayNumber - startCompDay < AMOUNT_DAYS_IN_WEEK
  }

  competitionsHasBottomBorder = endCompDay => {
    const { dayNumber } = this.props

    return dayNumber === endCompDay || endCompDay - dayNumber < AMOUNT_DAYS_IN_WEEK
  }

  competitionHasRightBorder = (endCompDay, weekday) => {
    const {
      dayNumber,
      monthNumber,
      calendar: {
        currentDate: { year }
      }
    } = this.props
    const amountDaysInMonth = new Date(Date.UTC(year, monthNumber, 0)).getUTCDate()

    return dayNumber === endCompDay || weekday === SUNDAY || dayNumber === amountDaysInMonth
  }

  competitionHasLeftBorder = (startCompDay, weekday) => {
    const { dayNumber } = this.props

    return dayNumber === startCompDay || weekday === MONDAY || dayNumber === 1
  }

  isCurrentDay = () => {
    const {
      isActiveMonth,
      calendar: { currentDate },
      dayNumber,
      monthNumber
    } = this.props
    const daysIsEqual = checkEqualityOfDaysWithoutYear(currentDate, { day: dayNumber, month: monthNumber })

    return isActiveMonth && daysIsEqual
  }

  isSingleDayEvent = () => this.props.events?.some(event => event.date.start.day === event.date.end.day)

  findCompetition = () =>
    this.props.events?.find(
      event => event.date.start.day < event.date.end.day || event.date.start.month < event.date.end.month
    )

  checkAllowingTooltipsInFullYearViewMode = () => {
    const { calendar, browser } = this.props
    const allow =
      calendar.viewMode === FULL_YEAR_VIEW && (browser.greaterThan.tablet || !document.body.classList.contains('r'))

    this.setState({
      allowTooltipsInFullYearView: allow
    })
  }

  isMonthView = () => this.props.calendar.viewMode === MONTH_VIEW

  renderTooltip = (event, enableTooltipForThisDay) => {
    const {
      dayNumber,
      monthNumber,
      calendar: {
        currentDate: { year }
      }
    } = this.props
    const { allowTooltipsInFullYearView } = this.state
    const isSingleDayEvent = this.isSingleDayEvent()
    const isMonthView = this.isMonthView()
    const tooltipsEnabled = isMonthView || allowTooltipsInFullYearView

    return event && tooltipsEnabled && enableTooltipForThisDay ? (
      <Tooltip
        position='top'
        arrow='center'
        parent={`active-date-${addLeadZero(dayNumber)}${addLeadZero(monthNumber)}${year}`}
        isSingleDayEvent={isSingleDayEvent}
        bindTo={this.getElementIDForBindingTooltip(event)}
      >
        <div className={sTip.tooltip}>
          <img src={event.imageBannerLink} alt={event.title} />
          <div className={sTip.textWrapper}>
            <div className={sTip.tooltipTitle}>{event.title}</div>
            <div className={sTip.tooltipDurationDescription}>{event.durationDescription}</div>
            <div className={sTip.tooltipDescription}>{event.description}</div>
          </div>
        </div>
      </Tooltip>
    ) : null
  }

  renderLeftGradient = () => {
    const { dayBeforeEvent } = this.props
    const leftGradient = this.getStylesForSideGradient(dayBeforeEvent, 'left')

    return dayBeforeEvent && dayBeforeEvent.isDayBeforeEvent && !dayBeforeEvent.isEarless ? (
      <div className={s.leftGradient} style={leftGradient} />
    ) : null
  }

  renderRightGradient = () => {
    const { dayAfterEvent } = this.props
    const rightGradient = this.getStylesForSideGradient(dayAfterEvent, 'right')

    return dayAfterEvent && dayAfterEvent.isDayAfterEvent && !dayAfterEvent.isEarless ? (
      <div className={s.rightGradient} style={rightGradient} />
    ) : null
  }

  render() {
    const {
      dayNumber,
      isActiveMonth,
      monthNumber,
      calendar: {
        currentDate: { year }
      }
    } = this.props
    const competition = this.findCompetition()
    const c = this.getClassForDayNumber()
    const event = this.getEvent()
    const cDay = this.getClassForDay(competition, event)
    const waiParams = this.getParamsForWAI(event)
    const styles = this.getStyles(event)
    const numberWrapperStyles = this.getNumberWrapperStyles(event)
    const elIDPrefix = `${isActiveMonth ? 'active' : 'notActive'}`
    const dayID = `${elIDPrefix}-date-${addLeadZero(dayNumber)}${addLeadZero(monthNumber)}${year}`
    const idForTooltip = this.getIDForTooltipAttribute(competition, event)
    const enableTooltipForThisDay = idForTooltip === dayID

    return (
      <div
        className={cDay}
        id={dayID}
        tabIndex={waiParams.tabIndex}
        aria-label={waiParams.label}
        style={this.getCompetitionStyles(event)}
        {...(idForTooltip ? { 'data-tooltipid': idForTooltip } : {})}
      >
        <div className={c} style={styles}>
          {this.renderLeftGradient()}
          <div className={s.numberWrapper} style={numberWrapperStyles}>
            {dayNumber}
          </div>
          {this.renderRightGradient()}
        </div>
        {this.renderTooltip(event, enableTooltipForThisDay)}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar,
  browser: state.browser
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Day)
