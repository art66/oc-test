import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { AMOUNT_DAYS_IN_WEEK, AMOUNT_MONTH_IN_YEAR, AMOUNT_ROWS_IN_MONTH } from '../../constants'
import Day from './Day'
import IEvent from '../../interfaces/IEvent'
import IDate from '../../interfaces/IDate'
import { checkEqualityOfDaysWithoutYear } from '../../utils'
import s from './styles.cssmodule.scss'

interface IProps {
  monthNumber: number
  customClass: string
  isPassedMonth: boolean
  calendar: {
    events: IEvent[]
    competitions: IEvent[]
    currentDate: IDate
  }
}

class MonthGrid extends React.Component<IProps> {
  getDaysFromPrevMonth = (monthNumber, year, weekdayFirstDay) => {
    const { isPassedMonth } = this.props
    const days = []
    const prevMonth = monthNumber - 1 || AMOUNT_MONTH_IN_YEAR
    const lastDayPrevMonth = new Date(Date.UTC(year, prevMonth, 0)).getUTCDate()
    const startMonthBlockDay = lastDayPrevMonth - (weekdayFirstDay - 2)

    for (let i = startMonthBlockDay; i <= lastDayPrevMonth; i += 1) {
      days.push(
        <Day
          key={`${i}/${prevMonth}`}
          dayNumber={i}
          isActiveMonth={false}
          monthNumber={monthNumber}
          isPassedMonth={isPassedMonth}
        />
      )
    }

    return days
  }

  getEvents = (dayNumber, monthNumber) => {
    const { events, competitions } = this.props.calendar

    const eventsThisDay = events.filter(event => {
      return this.checkDayOccurrenceInEvent(event, dayNumber, monthNumber)
    })

    const competitionsThisDay = competitions.filter(competition => {
      return this.checkDayOccurrenceInEvent(competition, dayNumber, monthNumber)
    })

    return [...eventsThisDay, ...competitionsThisDay]
  }

  getDaysFromNextMonth = (year, monthNumber, amountDaysInMonth, weekdayFirstDay) => {
    const { isPassedMonth } = this.props
    const days = []
    const weekdayLastMonthDay =
      new Date(Date.UTC(year, monthNumber - 1, amountDaysInMonth)).getUTCDay() || AMOUNT_DAYS_IN_WEEK
    const currRow = Math.ceil((weekdayFirstDay - 1 + amountDaysInMonth) / AMOUNT_DAYS_IN_WEEK)
    const amountDaysFromNextMonth =
      AMOUNT_DAYS_IN_WEEK - weekdayLastMonthDay + AMOUNT_DAYS_IN_WEEK * (AMOUNT_ROWS_IN_MONTH - currRow)

    for (let i = 0; i < amountDaysFromNextMonth; i += 1) {
      days.push(
        <Day
          key={`${i + 1}/${monthNumber + 1}`}
          dayNumber={i + 1}
          isActiveMonth={false}
          monthNumber={monthNumber}
          isPassedMonth={isPassedMonth}
        />
      )
    }

    return days
  }

  getSettingsForDayAfterEvent = dayNumber => {
    const {
      monthNumber,
      calendar: {
        currentDate: { year }
      }
    } = this.props
    const prevMonth = monthNumber - 1 || AMOUNT_MONTH_IN_YEAR
    const prevDay = dayNumber || new Date(Date.UTC(year, prevMonth, 0)).getUTCDate()
    const prevDayMonth = dayNumber ? monthNumber : monthNumber - 1
    const prevEvent = this.findOneDayEventThisDay(prevDay, prevDayMonth)

    return {
      isDayAfterEvent: !!prevEvent,
      eventColor: prevEvent && prevEvent.color,
      isPassedEvent: prevEvent && prevEvent.isPassed,
      isEarless: prevEvent && prevEvent.isEarless
    }
  }

  getSettingsForDayBeforeEvent = (dayNumber, amountDaysInMonth) => {
    const { monthNumber } = this.props
    const nextDayMonth = dayNumber === amountDaysInMonth ? monthNumber + 1 : monthNumber
    const nextDay = dayNumber === amountDaysInMonth ? 1 : dayNumber + 1
    const nextEvent = this.findOneDayEventThisDay(nextDay, nextDayMonth)

    return {
      isDayBeforeEvent: !!nextEvent,
      eventColor: nextEvent && nextEvent.color,
      isPassedEvent: nextEvent && nextEvent.isPassed,
      isEarless: nextEvent && nextEvent.isEarless
    }
  }

  getDaysFromCurrentMonth = amountDaysInMonth => {
    const { monthNumber, isPassedMonth } = this.props
    const days = []

    for (let i = 0; i < amountDaysInMonth; i += 1) {
      const dayNumber = i + 1
      const events = this.getEvents(dayNumber, monthNumber)
      const dayAfterEvent = this.getSettingsForDayAfterEvent(i)
      const dayBeforeEvent = this.getSettingsForDayBeforeEvent(dayNumber, amountDaysInMonth)

      days.push(
        <Day
          key={`${dayNumber}/${monthNumber}`}
          dayNumber={dayNumber}
          isActiveMonth={true}
          monthNumber={monthNumber}
          events={events}
          isPassedMonth={isPassedMonth}
          dayAfterEvent={dayAfterEvent}
          dayBeforeEvent={dayBeforeEvent}
        />
      )
    }

    return days
  }

  getDays = () => {
    const { monthNumber, calendar } = this.props
    const year = calendar.currentDate && calendar.currentDate.year
    let days = []
    const amountDaysInMonth = new Date(Date.UTC(year, monthNumber, 0)).getUTCDate()
    const weekdayFirstDay = new Date(Date.UTC(year, monthNumber - 1, 1)).getUTCDay() || AMOUNT_DAYS_IN_WEEK
    const daysFromCurrentMonth = this.getDaysFromCurrentMonth(amountDaysInMonth)
    const daysFromNextMonth = this.getDaysFromNextMonth(year, monthNumber, amountDaysInMonth, weekdayFirstDay)

    if (weekdayFirstDay !== 1) {
      days = [...this.getDaysFromPrevMonth(monthNumber, year, weekdayFirstDay)]
    }

    return [...days, ...daysFromCurrentMonth, ...daysFromNextMonth]
  }

  findOneDayEventThisDay = (dayNumber, monthNumber) => {
    const {
      calendar: { events }
    } = this.props

    return events.find(event => {
      return (
        checkEqualityOfDaysWithoutYear(event.date.start, event.date.end)
        && this.checkDayOccurrenceInEvent(event, dayNumber, monthNumber)
      )
    })
  }

  checkDayOccurrenceInEvent = (event, dayNumber, monthNumber) => {
    const {
      calendar: { currentDate }
    } = this.props
    const eventDate = event.date
    const isCurrentYear = currentDate.year === eventDate.start.year
    const moreThanStartDate =
      (dayNumber >= eventDate.start.day && monthNumber >= eventDate.start.month) || monthNumber > eventDate.start.month
    const lessThanEndDate =
      (dayNumber <= eventDate.end.day && monthNumber <= eventDate.end.month) || monthNumber < eventDate.end.month

    return isCurrentYear && moreThanStartDate && lessThanEndDate
  }

  render() {
    const { calendar, customClass, isPassedMonth } = this.props

    return calendar.currentDate ? (
      <div className={cn(s.monthGrid, s[customClass], { [s.passed]: isPassedMonth })}>{this.getDays()}</div>
    ) : null
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(MonthGrid)
