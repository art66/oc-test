import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { DISABLE_FILL, DEFAULT_FILL } from '@torn/shared/SVG/presets/icons/calendar'
import { setMonthViewMode } from '../../actions'
import MonthGrid from '../MonthGrid'
import { FULL_YEAR_VIEW, MONTHS } from '../../constants'
import { ICON_TYPE, ICON_PRESETS } from '../../constants/icon'
import IDate from '../../interfaces/IDate'
import IBrowser from '../../interfaces/IBrowser'
import s from './styles.cssmodule.scss'

interface IProps {
  monthNumber: number
  browser: IBrowser
  calendar: {
    monthsMetadata: any
    viewMode: string
    currentDate: IDate
  }
  setMonthViewMode: (monthNumber: number) => void
}

interface IState {
  isDesktopMode: boolean
}

class Month extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      isDesktopMode: true
    }
  }

  componentDidMount = () => {
    this.isDesktopMode()
    window.addEventListener('load', this.isDesktopMode)
    window.addEventListener('resize', this.isDesktopMode)
  }

  componentWillUnmount = () => {
    window.removeEventListener('load', this.isDesktopMode)
    window.removeEventListener('resize', this.isDesktopMode)
  }

  isDesktopMode = () => {
    const { calendar, browser } = this.props
    const allow =
      calendar.viewMode === FULL_YEAR_VIEW && (browser.greaterThan.tablet || !document.body.classList.contains('r'))

    this.setState({
      isDesktopMode: allow
    })
  }

  handleEnablingMonthViewMode = () => {
    const { monthNumber, browser } = this.props

    if (browser.lessThan.desktop && !this.state.isDesktopMode) {
      this.props.setMonthViewMode(monthNumber)
    }
  }

  renderMonthEventTitle = isPassedMonth => {
    const {
      monthNumber,
      calendar: { monthsMetadata }
    } = this.props
    const monthKey = MONTHS[monthNumber - 1].toLowerCase()
    const monthEventTitle = monthsMetadata && monthsMetadata[monthKey]

    return monthEventTitle ? (
      <div className={s.event}>
        <div className={cn(s.icon, s[monthEventTitle.icon])}>
          <SVGIconGenerator
            iconName={monthEventTitle.icon}
            preset={{ type: ICON_TYPE, subtype: ICON_PRESETS[monthEventTitle.icon].subtype }}
            fill={isPassedMonth ? DISABLE_FILL : DEFAULT_FILL}
            customClass={`disableFilter calendarIcons ${s[monthEventTitle.icon]}`}
          />
        </div>
        <div className={s.eventTitle}>{monthEventTitle.title}</div>
      </div>
    ) : null
  }

  render() {
    const {
      monthNumber,
      calendar: { currentDate }
    } = this.props
    const isPassedMonth = monthNumber < currentDate.month

    return (
      <div className={cn(s.month, { [s.passed]: isPassedMonth })} onClick={this.handleEnablingMonthViewMode}>
        <div className={s.title}>
          {this.renderMonthEventTitle(isPassedMonth)}
          <div className={s.monthName}>{MONTHS[monthNumber - 1]}</div>
        </div>
        <MonthGrid monthNumber={monthNumber} isPassedMonth={isPassedMonth} customClass='fullYearView' />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar,
  browser: state.browser
})

const mapDispatchToProps = {
  setMonthViewMode
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Month)
