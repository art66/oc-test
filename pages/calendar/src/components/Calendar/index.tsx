import React, { PureComponent } from 'react'
import Month from './Month'
import { AMOUNT_MONTH_IN_YEAR } from '../../constants'
import s from './styles.cssmodule.scss'

class Calendar extends PureComponent {
  generateGrid = () => {
    const months = []

    for (let i = 0; i < AMOUNT_MONTH_IN_YEAR; i += 1) {
      months.push(<Month key={i} monthNumber={i + 1} />)
    }
    return months
  }

  render() {
    return <div className={s.calendar}>{this.generateGrid()}</div>
  }
}

export default Calendar
