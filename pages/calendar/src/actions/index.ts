import { createAction } from 'redux-actions'
import * as actionTypes from './actionTypes'

export const fetchCalendarData = createAction(actionTypes.FETCH_CALENDAR_DATA)
export const setCalendarData = createAction(actionTypes.SET_CALENDAR_DATA, data => data)
export const setCurrentDate = createAction(actionTypes.SET_CURRENT_DATE)
export const setMonthViewMode = createAction(actionTypes.SET_MONTH_VIEW_MODE, monthNumber => ({ monthNumber }))
export const setFullYearMode = createAction(actionTypes.SET_FULL_YEAR_MODE)
export const setActiveMonth = createAction(actionTypes.SET_ACTIVE_MONTH, monthNumber => ({ monthNumber }))
export const setDarkModeState = createAction(actionTypes.SET_DARK_MODE_STATE, darkModeEnabled => ({ darkModeEnabled }))
