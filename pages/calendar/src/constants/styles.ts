export const MULTI_DAY_EVENT_OPACITY = {
  border: 0.4,
  background: 0.05
}

export const SINGLE_DAY_EVENT_OPACITY = {
  numberBg: 0.2,
  currentDayBg: 0.7,
  dayGradientBg: {
    start: 0.07,
    middle: 0.18,
    end: 0.3
  }
}

export const SINGLE_DAY_EVENT_OPACITY_PASSED = {
  numberBg: 0,
  numberBorder: 0.2,
  currentDayBg: 0.7,
  dayGradientBg: {
    start: 0.05,
    middle: 0.1,
    end: 0.15
  }
}

export const DM_SINGLE_DAY_EVENT_OPACITY_PASSED = {
  numberBg: 0,
  numberBorder: 0.7,
  currentDayBg: 0.7,
  dayGradientBg: {
    start: 0.1,
    middle: 0.2,
    end: 0.3
  }
}

export const MULTI_DAY_EVENT_OPACITY_PASSED = {
  border: 0.12,
  background: 0.05
}

export const DM_MULTI_DAY_EVENT_OPACITY_PASSED = {
  border: 0.3,
  background: 0.2
}
