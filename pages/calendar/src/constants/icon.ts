export const ICON_PRESETS = {
  ChristmasTown: {
    subtype: 'CHRISTMAS_TOWN'
  },
  DogTags: {
    subtype: 'DOG_TAGS'
  },
  Easter: {
    subtype: 'EASTER'
  },
  Elimination: {
    subtype: 'ELIMINATION'
  },
  MrMissTorn: {
    subtype: 'MR_MISS_TORN'
  },
  ArrowLeftThin: {
    subtype: 'LEFT_ARROW_THIN'
  },
  ArrowRightThin: {
    subtype: 'RIGHT_ARROW_THIN'
  },
  Back: {
    subtype: 'BACK'
  }
}
export const ICON_TYPE = 'calendar'
