export const AMOUNT_DAYS_IN_WEEK = 7
export const AMOUNT_MONTH_IN_YEAR = 12
export const AMOUNT_ROWS_IN_MONTH = 6
export const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]
export const MONDAY = 1
export const THURSDAY = 4
export const SUNDAY = 7
export const FULL_YEAR_VIEW = 'FULL_YEAR_VIEW'
export const MONTH_VIEW = 'MONTH_VIEW'
export const JANUARY = 1
export const DECEMBER = 12
