import { put, takeEvery } from 'redux-saga/effects'
import * as actionTypes from '../actions/actionTypes'
import { setCalendarData } from '../actions'
import { fetchUrl } from '../utils'

export function* fetchCalendarData() {
  const data = yield fetchUrl('calendar.php?step=indexAction')

  yield put(setCalendarData(data.calendar))
}

export default function* chart() {
  yield takeEvery(actionTypes.FETCH_CALENDAR_DATA, fetchCalendarData)
}
