import { handleActions } from 'redux-actions'
import * as actionTypes from '../actions/actionTypes'
import { convertDateToObject } from '../utils'
import { FULL_YEAR_VIEW, MONTH_VIEW } from '../constants'

const initialState = {
  viewMode: FULL_YEAR_VIEW
}

const ACTION_HANDLERS = {
  [actionTypes.SET_CURRENT_DATE]: state => {
    return {
      ...state,
      currentDate: convertDateToObject(new Date((window as any).getCurrentTimestamp()))
    }
  },
  [actionTypes.SET_CALENDAR_DATA]: (state, action) => {
    return {
      ...state,
      ...action.payload,
      competitions: action.payload.competitions.map(item => ({
        ...item,
        date: {
          start: convertDateToObject(new Date(item.date.start * 1000)),
          end: convertDateToObject(new Date(item.date.end * 1000))
        }
      })),
      events: action.payload.events.map(item => ({
        ...item,
        date: {
          start: convertDateToObject(new Date(item.date.start * 1000)),
          end: convertDateToObject(new Date(item.date.end * 1000))
        }
      }))
    }
  },
  [actionTypes.SET_MONTH_VIEW_MODE]: (state, action) => {
    return {
      ...state,
      viewMode: MONTH_VIEW,
      viewedMonth: action.payload.monthNumber
    }
  },
  [actionTypes.SET_ACTIVE_MONTH]: (state, action) => {
    return {
      ...state,
      viewedMonth: action.payload.monthNumber
    }
  },
  [actionTypes.SET_FULL_YEAR_MODE]: state => {
    return {
      ...state,
      viewMode: FULL_YEAR_VIEW
    }
  },
  [actionTypes.SET_DARK_MODE_STATE]: (state, action) => {
    return {
      ...state,
      darkModeEnabled: action.payload.darkModeEnabled
    }
  }
}

export default handleActions(ACTION_HANDLERS, initialState)
