import IDate from './IDate'
import IColor from './IColor'

export default interface IEvent {
  title: string
  description: string
  durationDescription: string
  imageBannerLink: string
  date: {
    start: IDate
    end: IDate
  }
  color: IColor
  isPassed: boolean
  isEarless?: boolean
}
