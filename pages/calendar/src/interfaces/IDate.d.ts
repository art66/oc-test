export default interface IDate {
  day: number
  month: number
  year: number
  weekday: number
}
