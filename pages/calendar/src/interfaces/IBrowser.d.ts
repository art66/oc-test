import { IBrowser as IReduxResponsiveBrowser, IBreakPoints } from 'redux-responsive'

export default interface IBrowser extends IReduxResponsiveBrowser<IBreakPoints<'desktop' | 'tablet'>> {}
