import { createAction } from 'redux-actions'
import { fetchUrl } from '../utils'
import { BASE_URL } from '../constants'

export function getInitialState() {
  return dispatch => {
    return fetchUrl(BASE_URL + '&step=getInitialData&page=questionnaire')
      .then(json => {
        dispatch(dataLoaded(json))
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const dataLoaded = createAction('data loaded', json => ({ json, meta: 'ajax' }))

export const goToPage = createAction('go to page')

export function submit(step, data) {
  return dispatch => {
    return fetchUrl(BASE_URL + '&step=' + step, data)
      .then(json => {
        dispatch(submited(json))
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const submited = createAction('submited', json => ({ json, meta: 'ajax' }))

export const selectAnswer = createAction('select anwer')

export const bringInfoBox = createAction('bring info box')

export const hideInfoBox = createAction('hide info box')

export function showInfoBox(argsObj) {
  return dispatch => {
    console.error(argsObj.msg)
    dispatch(bringInfoBox(argsObj))

    if (!argsObj.persistent) {
      setTimeout(() => dispatch(hideInfoBox()), 5000)
    }
  }
}
