import { createStore, applyMiddleware, compose } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
// import createLogger from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import { fetchInterceptor } from './middleware/fetchInterceptor'
import rootReducer from '../reducers'

export default function configureStore(preloadedState) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  const store = createStore(
    rootReducer,
    preloadedState,
    composeEnhancers(
      responsiveStoreEnhancer,
      applyMiddleware(
        thunkMiddleware,
        fetchInterceptor
        // createLogger()
      )
    )
  )

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
