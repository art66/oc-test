import { handleActions } from 'redux-actions'

const initialState = {
  gotContent: false,
  pageNumber: 1,
  answers: {}
}

const _filter = data => ({
  questions: data.questions,
  questionsPerPage: data.questionsPerPage,
  status: data.status,
  notValid: data.notValid
})

const pageWithFirstInvalidQuestion = (questionsPerPage, notValid = {}) => {
  const ks = Object.keys(notValid)
  for (let i in ks) {
    let k = ks[i]
    if (notValid[k]) {
      if (k <= questionsPerPage[1]['to']) {
        return 1
      } else {
        return 2
      }
    }
  }
  return 1
}
const isCompleted = action => {
  const status = action.payload.json.status
  if (status === 'deny' || status === 'solved') {
    return true
  }
  return false
}
export default handleActions(
  {
    'data loaded'(state, action) {
      if (!isCompleted(action)) {
        return {
          ...state,
          ..._filter(action.payload.json),
          gotContent: true
        }
      } else {
        return {
          ...state,
          completed: true
        }
      }
    },
    'bring info box'(state, action) {
      return {
        ...state,
        infoBox: {
          msg: action.payload.msg,
          color: action.payload.color
        }
      }
    },
    'hide info box'(state, action) {
      return {
        ...state,
        infoBox: null
      }
    },
    'go to page'(state, action) {
      return {
        ...state,
        pageNumber: action.payload || state.pageNumber
      }
    },
    'select anwer'(state, action) {
      return {
        ...state,
        answers: {
          ...state.answers,
          [action.payload.question]: action.payload.answer
        },
        notValid: {
          ...state.notValid,
          [action.payload.question]: false
        }
      }
    },
    submited(state, action) {
      if (!isCompleted(action)) {
        return {
          ...state,
          ..._filter(action.payload.json),
          pageNumber: pageWithFirstInvalidQuestion(state.questionsPerPage, action.payload.json.notValid)
        }
      } else {
        return {
          ...state,
          gotContent: false,
          completed: true
        }
      }
    }
  },
  initialState
)
