import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import app from './app'

const rootReducer = combineReducers({
  browser: createResponsiveStateReducer(
    {
      phone: 600,
      tablet: 1024
    },
    'desktop'
  ),
  app
})

export default rootReducer
