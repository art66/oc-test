import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { selectAnswer, submit } from '../../actions'
import QuestionBlock from '../QuestionBlock'
import { CSSTransitionGroup } from 'react-transition-group'
import Footer from '../Footer'

export class Questionnaire extends Component {
  _getQuestionsList() {
    const { questions, pageNumber, questionsPerPage, mediaType } = this.props
    if (mediaType !== 'desktop') {
      return questions
    }
    const paging = questionsPerPage[pageNumber]
    return this.props.questions.slice(paging.from, paging.to + 1)
  }
  _questions() {
    const { answers, pageNumber, questionsPerPage, selectAnswer, mediaType, notValid } = this.props
    const paging = questionsPerPage[pageNumber]
    const questions = this._getQuestionsList()
    const divs = []
    questions.map((q, i) => {
      const index = mediaType === 'desktop' ? paging.from + i : i
      divs.push(
        <QuestionBlock
          {...q}
          index={index}
          key={index}
          invalid={notValid && notValid[index]}
          selectAnswer={selectAnswer}
          selectedAnswer={answers[index]}
        />
      )
    })
    return (
      <CSSTransitionGroup
        transitionName="fade"
        className="questions-wrap"
        component="div"
        transitionEnterTimeout={400}
        transitionLeaveTimeout={400}
      >
        {divs}
      </CSSTransitionGroup>
    )
  }
  _submit = () => {
    const { submit, answers } = this.props
    submit('solve', { answers })
  }
  render() {
    return (
      <div>
        {this._questions()}
        <Footer submit={this._submit} type="questionnaire" />
      </div>
    )
  }
}

Questionnaire.propTypes = {
  questions: PropTypes.array.isRequired,
  answers: PropTypes.object,
  questionsPerPage: PropTypes.object,
  notValid: PropTypes.object,
  pageNumber: PropTypes.number,
  mediaType: PropTypes.string,
  submit: PropTypes.func,
  selectAnswer: PropTypes.func
}

const mapStateToProps = state => ({
  questions: state.app.questions,
  answers: state.app.answers,
  pageNumber: state.app.pageNumber,
  questionsPerPage: state.app.questionsPerPage,
  notValid: state.app.notValid,
  mediaType: state.browser.mediaType
})

const mapActionsToProps = {
  selectAnswer,
  submit
}

export default connect(mapStateToProps, mapActionsToProps)(Questionnaire)
