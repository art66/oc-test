import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'

const LETTERS = ['a', 'b', 'c', 'd', 'e', 'f']

export class QuestionBlock extends Component {
  _getAnswers() {
    const { answers, index, selectedAnswer = '', userInputPlaceholder } = this.props
    if (userInputPlaceholder) {
      return (
        <input
          type="text"
          className="question-block__input"
          placeholder={userInputPlaceholder}
          onChange={event => this._changeHandler(event.target.value)}
          value={selectedAnswer}
        />
      )
    }
    const lis = []
    answers.map((a, i) => {
      const checked = selectedAnswer === i
      const c = classnames({
        'question-block__radio-label': true,
        checked
      })
      lis.push(
        <li key={i}>
          <label className={c}>
            <input
              className="question-block__radio"
              type="radio"
              name={'q' + index}
              value={i}
              onChange={() => this._changeHandler(i)}
              checked={checked}
            />
            <em>{LETTERS[i]})</em> {a}
          </label>
        </li>
      )
    })
    return <ul className="question-block__answers">{lis}</ul>
  }
  _changeHandler(answer) {
    const { selectAnswer, index } = this.props
    selectAnswer({ question: index, answer })
  }
  render() {
    const { question, index, invalid } = this.props
    const c = classnames({
      'question-block': true,
      invalid
    })
    return (
      <div className={c}>
        <div className="question-block__question">
          {index + 1}) {question}
        </div>
        {this._getAnswers()}
      </div>
    )
  }
}

QuestionBlock.propTypes = {
  question: PropTypes.string.isRequired,
  answers: PropTypes.array,
  invalid: PropTypes.bool,
  userInputPlaceholder: PropTypes.string,
  index: PropTypes.number.isRequired,
  selectAnswer: PropTypes.func,
  selectedAnswer: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

export default QuestionBlock
