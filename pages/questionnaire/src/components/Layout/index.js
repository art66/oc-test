import PropTypes from 'prop-types'
import React from 'react'
import Header from '../Header'
import './styles.scss'

export const Layout = ({ children }) => (
  <div className="questionnaire-wrap">
    <Header />
    {children}
  </div>
)

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout
