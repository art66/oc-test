import React from 'react'

export const Header = () => (
  <div className="questionaarie-header">
    <h1>
      <img className="logo" src="images/v2/missions/questionnaire/amanda_logo_sign.png" />
      <div className="logo-devider" />
      <img className="logo-wording" src="images/v2/missions/questionnaire/amanda_logo_writing.png" />
    </h1>
  </div>
)

export default Header
