import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { connect } from 'react-redux'
import { goToPage } from '../../actions'

const NEXT = 'NEXT PAGE >'
const PREV = '< PREVIOUS PAGE'

export class Footer extends Component {
  _goToPage(event, pageNbr) {
    event.preventDefault()
    const { goToPage } = this.props
    goToPage(pageNbr)
  }
  _getHint = () => {
    const { status, pageNumber, type } = this.props
    if (status === 'error' && pageNumber === 2) {
      if (type === 'questionnaire') {
        return <div className="hint-message fadein">Please select your answers</div>
      } else if (type === 'casusfoederis') {
        return <div className="hint-message fadein">Please add your signature</div>
      }
    }
  }
  render() {
    const { pageNumber, mediaType, submit, disabled } = this.props
    const showLinks = mediaType === 'desktop'
    const showSubmit = mediaType !== 'desktop' || pageNumber === 2
    const submitClass = classnames({
      submit: true,
      'btn-gold': true,
      fadein: true,
      disabled
    })
    let pagelink

    if (pageNumber === 1) {
      pagelink = (
        <a href="#" className={'pagelink go-right'} onClick={event => this._goToPage(event, 2)}>
          {NEXT}
        </a>
      )
    } else {
      pagelink = (
        <a href="#" className={'pagelink go-left'} onClick={event => this._goToPage(event, 1)}>
          {PREV}
        </a>
      )
    }
    return (
      <div className="q-footer">
        {showLinks && pagelink}
        {showSubmit && (
          <button className={submitClass} onClick={!disabled && submit}>
            SUBMIT
          </button>
        )}
        {this._getHint()}
      </div>
    )
  }
}

Footer.propTypes = {
  pageNumber: PropTypes.number,
  submit: PropTypes.func,
  goToPage: PropTypes.func,
  mediaType: PropTypes.string,
  status: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string.isRequired
}

Footer.defaultProps = {
  pageNumber: 1
}

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType,
  pageNumber: state.app.pageNumber,
  status: state.app.status
})

const mapActionsToProps = {
  goToPage
}

export default connect(mapStateToProps, mapActionsToProps)(Footer)
