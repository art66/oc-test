import { ISkimmingState } from '../routes/CardSkimming/interfaces/IState'

export interface IState {
  common: any
  browser: any
  skimming: ISkimmingState,
  burglary: any
}
