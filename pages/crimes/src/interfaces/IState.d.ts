import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export type TAction = (attemptURL: string, ID: string | number) => void

export interface IOutcome {
  ID: number
  outcomeDesc: string
  customDesc?: string
  result: string
  image?: string
  rewardsGive: {
    userRewards?: {
      money: number
    }
    itemsRewards?: {
      count: number
      label: string
    }
    ammoRewards?: {
      count: number
      image: string
      title: string
      catShort: string
      titleShort: string
    }
  }
  story: string[] | string
}

export interface IAttemptProgress {
  itemId: number
  inProgress?: boolean
}

export interface IState {
  outcome: IOutcome
  showPlaceholder: boolean
  attemptProgress: IAttemptProgress
}

export interface IAdditionalInfo {
  itemId: number
}

export type TCloseOutcome = () => void

export interface ICloseOutcome {
  isActive: boolean
  callback: TCloseOutcome
}

export interface ICommonRowProps {
  additionalInfo: object
  available: boolean
  action: TAction
  button: any
  className: string
  attemptProgress: IAttemptProgress
  crimesByType: any[]
  closeOutcome: ICloseOutcome
  crimeID: number
  currentRowID: number | string
  isCurrentRow: boolean
  isManualLayoutSetted: boolean
  isRowActive: boolean
  isRowHaveOutcome: boolean
  isRowNotInProgress: boolean
  itemId: number | string
  jailed: boolean
  lastSubCrimeID: number
  mediaType: TMediaType
  nerve: number
  outcome: IOutcome
  showOutcome: boolean
  title: string
  iconClass: string // controlled via crime props
  result: string
  rowState: string
  story: object
  typeID: number
  buttonLabel: string
  user: {
    nerve: number
  }
  isDesktopLayoutSetted: boolean
  crimeInfo: object
  subID?: string
  error?: string
}
