export type TRfcv = string
export type TIsAjaxRequest = boolean

export interface ICurrentType {
  _id: {
    $oid: string
  },
  ID: number
  typeID: number
  crimeRoute: string
  // - operated by each independent crime route
  // title: 'Skimming' | 'Pickpocketing' | 'Bootlegging' | 'Search for Cash' | 'Graffiti' | 'Shoplifting'
  // crimeRoute: 'skimming' | 'pickpocketing' | 'bootlegging' | 'search-for-cash' | 'graffiti' | 'shoplifting'
  active: number
  img: string
  expForLevel100: number
  notForTest: number
}

export interface ICurrentUserStats {
  failedTotal: number
  attemptsTotal: number
  crimesByIDAttempts: {
      [x: string]: number
  }
  successesTotal: number
  crimesByIDAttemptsSucc: {
    [x: string]: number
  }
  critFailedTotal: number
  moneyFoundTotal: number
  statsByType: {
      [x: string]: {
        successesTotal: number
      }
  },
  skill: number
  skillLevel: number
  enhancer: string
}

export interface ICurrentUserStatistics {
  label: 'Level & EXP' | 'Enhancer' | 'Successes' | 'Fails' | 'Critical fails'
  value: '1 (29%)' | 'None' | '27' | '60' | '74'
}

export interface ITutorial {
  text: string
}

export interface IUser {
  playername: string
}

export interface IDebugInfo {
  time: number
}

export interface IResponseData {
  error?: string
  hideTestPanel: boolean
  hubRedirect: boolean
  // crimesByType: [any] // - operated by each independent crime route
  additionalInfo: object
  currentType: ICurrentType
  skillLevelTotal: number
  currentLevel: number
  lastSubCrimeID: number
  lastSubCrimeIDByType: number
  currentUserStats: ICurrentUserStats
  currentUserStatistics: ICurrentUserStatistics[]
  exp: number
  tutorial: ITutorial
  user: IUser
  time: number
  debugInfo: IDebugInfo
}

export interface ICoreResponseData {
  DB: IResponseData,
  rfcv: TRfcv
  isAjaxRequest: TIsAjaxRequest
}
