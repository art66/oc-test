export type TType = string

export interface IType {
  type: TType
}

export interface IAttemptStart {
  itemId: number
  isCurrentRow: boolean
}
