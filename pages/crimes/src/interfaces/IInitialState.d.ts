export interface IInitialCrimeState {
  attemptProgress: {
    inProgress: boolean
  }
  additionalInfo: object
  isCurrentRow: boolean
  showPlaceholder: boolean
  exp: number
  hideTestPanel: boolean
  hubRedirect: boolean
  lastSubCrimeID: number
  lastSubCrimeIDByType: number
  skillLevelTotal: number
  isAjaxRequest: boolean
  rfcv: string
  time: number
  tutorial: {
    text: string
  }
  user: {
    playername: string
  }
  currentUserStats: {
    enhancer: string
    skill: number
    skillLevel: number
    statsByType: {
      6: {
        successesTotal: number
      }
    }
  }
}
