// ------------------------------------
// APP ROOT URL
// ------------------------------------
export const ROOT_URL = '/loader.php?sid=crimesData'

export const TYPE_IDS = {
  default: 0,
  searchforcash: 1,
  bootlegging: 2,
  graffiti: 3,
  shoplifting: 4,
  pickpocketing: 5,
  skimming: 6,
  burglary: 7
}

// ------------------------------------
// Action Constants
// ------------------------------------
export const AJAX = 'ajax'
export const FETCH_DATA = 'FETCH_DATA'
export const DATA_FETCHED = 'DATA_FETCHED'
export const ATTEMPT = 'ATTEMPT'
export const ATTEMPT_START = 'ATTEMPT_START'
export const ATTEMPT_FETCHED = 'ATTEMPT_FETCHED'
export const POOLING = 'POOLING'
export const POOLING_FETCHED = 'POOLING_FETCHED'
export const LEVEL_UP = 'LEVEL_UP'
export const LOAD_HUB_DATA = 'LOAD_HUB_DATA'
export const HUB_DATA_LOADED = 'HUB_DATA_LOADED'
export const SHOW_DEBUG_INFO = 'SHOW_DEBUG_INFO'
export const HIDE_DEBUG_INFO = 'HIDE_DEBUG_INFO'
export const SHOW_INFOBOX = 'SHOW_INFOBOX'
export const CRIME_IS_READY = 'CRIME_IS_READY'
export const CRIME_IS_NOT_READY = 'CRIME_IS_NOT_READY'
export const UPDATE_EXP = 'UPDATE_EXP'
export const SET_EXP = 'SET_EXP'
export const RESET_STATS = 'RESET_STATS'
export const RESET_STATS_DONE = 'RESET_STATS_DONE'
export const SET_CRIME_LEVEL = 'SET_CRIME_LEVEL'
export const HIDE_ARROWS = 'HIDE_ARROWS'
export const SHOW_ARROWS = 'SHOW_ARROWS'
export const TOGGLE_ARROWS = 'TOGGLE_ARROWS'
export const GET_CURRENT_OUTCOME = 'GET_CURRENT_OUTCOME'
export const CURRENT_ARROW = 'CURRENT_ARROW'
export const CURRENT_ARROW_CLICK = 'CURRENT_ARROW_CLICK'
export const CURRENT_ARROW_HOVER = 'CURRENT_ARROW_HOVER'
export const FREEZER_SLIDER = 'FREEZER_SLIDER'
export const CLEAR_OUTDATED_OUTCOME = 'CLEAR_OUTDATED_OUTCOME'
export const CRIME_MOUNTED = 'CRIME_MOUNTED'
export const CRIME_UNMOUNTED = 'CRIME_UNMOUNTED'
export const SHOW_DROPDOWN_PANEL = 'SHOW_DROPDOWN_PANEL'
export const TOGGLE_DROPDOWN_PANEL = 'TOGGLE_DROPDOWN_PANEL'
export const HIDE_DROPDOWN_PANEL = 'HIDE_DROPDOWN_PANEL'
export const EDITOR_SWITCHER = 'EDITOR_SWITCHER'
export const USER_JAILED = 'USER_JAILED'
export const DATA_RESETTED = 'DATA_FETCHED'
export const CURRENT_PAGE_DATA = 'CURRENT_PAGE_DATA'
export const MANUAL_LAYOUT_SET = 'MANUAL_LAYOUT_SET'
export const MEDIA_SCREEN_CHANGED = 'MEDIA_SCREEN_CHANGED'
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE'
export const CLOSE_OUTCOME = 'CLOSE_OUTCOME'
export const DARK_MODE_CHECK = 'DARK_MODE_CHECK'

// -------------------------------------
// LAYOUT STYLE TITLE CRIMEROW OPTIONS
// -------------------------------------
export const UNDERTITLE = true

// // -------------------------------------
// // APP HEADER
// // -------------------------------------
// export const ROOT_TITLE = 'Crimes'

// // -------------------------------------
// // SVG STAR ICONS
// // -------------------------------------
export const DEFAULT_SVG_STAR_ICON_PROPS = {
  iconName: 'CrimeStars',
  fill: {
    strokeWidth: 0
  },
  dimensions: {
    width: 25,
    height: 25,
    viewbox: '0 0 25 25'
  },
  filter: {
    active: false
  }
}

export const STARS_DIMENSIONS = {
  default: DEFAULT_SVG_STAR_ICON_PROPS.dimensions,
  romb: {
    width: 25,
    height: 25,
    viewbox: '0 0 25 25'
  },
  metal: {
    width: 23,
    height: 21.89,
    viewbox: '0 0 23 21.89'
  },
  bronze: {
    width: 25,
    height: 24.37,
    viewbox: '0 0 25 24.37'
  },
  silver: {
    width: 25,
    height: 24.62,
    viewbox: '0 0 25 24.62'
  },
  gold: {
    width: 25,
    height: 24.75,
    viewbox: '0 0 25 24.75'
  },
  platinum: {
    width: 25,
    height: 24.86,
    viewbox: '0 0 25 24.86'
  }
}

export const STARS_FILTERS = {
  romb: {
    active: true,
    ID: 'romb',
    scheme: [
      {
        step: '0',
        color: '#fff'
      },
      {
        step: '1',
        color: '#ccc'
      }
    ],
    transform: 'translate(1.24 -0.14)',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: 20.1,
      x2: 2.42,
      y1: 3.8,
      y2: 21.48
    }
  },
  metal: {
    active: true,
    ID: 'metal',
    scheme: [
      {
        step: '0',
        color: '#686859'
      },
      {
        step: '1',
        color: '#eeeeea'
      }
    ],
    transform: '',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: 11.5,
      y1: 2.42,
      y2: 21.89
    }
  },
  bronze: {
    active: true,
    ID: 'bronze',
    scheme: [
      {
        step: '0',
        color: '#fdb'
      },
      {
        step: '1',
        color: '#b25900'
      }
    ],
    transform: 'translate(39.25 28.51) rotate(180)',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: 26.75,
      x2: 26.75,
      y1: 28.51,
      y2: 4.14
    }
  },
  silver: {
    active: true,
    ID: 'silver',
    scheme: [
      {
        step: '0',
        color: '#fff'
      },
      {
        step: '1',
        color: '#8e8e9b'
      }
    ],
    transform: 'translate(-18.46 11.1) rotate(180)',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: -30.96,
      x2: -30.96,
      y1: 11.1,
      y2: -13.52
    }
  },
  gold: {
    active: true,
    ID: 'gold',
    scheme: [
      {
        step: '0',
        color: '#ff0'
      },
      {
        step: '1',
        color: '#d9a300'
      }
    ],
    transform: 'translate(-25.69 22.96) rotate(180)',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: -38.19,
      x2: -38.19,
      y1: 22.96,
      y2: -1.78
    }
  },
  platinum: {
    active: false
  }
}

export const APP_HEADER = {
  active: true,
  titles: {
    default: {
      ID: 0,
      title: 'Crimes'
    },
    list: [
      {
        ID: 1,
        subTitle: 'Search for Cash'
      },
      {
        ID: 2,
        subTitle: 'Bootlegging'
      },
      {
        ID: 3,
        subTitle: 'Graffiti'
      },
      {
        ID: 4,
        subTitle: 'Shoplifting'
      },
      {
        ID: 5,
        subTitle: 'Pickpocket'
      },
      {
        ID: 6,
        subTitle: 'Skimming'
      }
    ]
  },
  links: {
    default: {
      ID: 0,
      items: [
        {
          href: '/',
          icon: 'CrimesHub',
          label: 'hub-link',
          title: 'Back to HUB',
          isSPALink: true
        }
      ]
    },
    list: [
      {
        ID: 0,
        items: [
          // {
          //   href: '/searchforcash',
          //   icon: 'SearchForCash',
          //   label: 'searchforcash-link',
          //   title: 'Search for cash',
          //   isSPALink: true
          // },
          // {
          //   href: '/bootlegging',
          //   icon: 'Bootlegging',
          //   label: 'bootlegging-link',
          //   title: 'Bootlegging',
          //   isSPALink: true
          // },
          // {
          //   href: '/graffiti',
          //   icon: 'Graffiti',
          //   label: 'graffiti',
          //   title: 'Graffiti',
          //   isSPALink: true
          // },
          // {
          //   href: '/shoplifting',
          //   icon: 'Shoplifting',
          //   label: 'shoplifting-link',
          //   title: 'Shoplifting',
          //   isSPALink: true
          // }
          // {
          //   href: '/pickpocketing',
          //   icon: 'Pickpocket',
          //   label: 'pickpocket-link',
          //   title: 'Pickpocket',
          //   isSPALink: true
          // }
        ]
      },
      {
        ID: 1,
        items: [
          // {
          //   href: '/bootlegging',
          //   icon: 'Bootlegging',
          //   label: 'bootlegging-link',
          //   title: 'Bootlegging',
          //   isSPALink: true
          // },
          // {
          //   href: '/graffiti',
          //   icon: 'Graffiti',
          //   label: 'graffiti',
          //   title: 'Graffiti',
          //   isSPALink: true
          // },
          // {
          //   href: '/shoplifting',
          //   icon: 'Shoplifting',
          //   label: 'shoplifting-link',
          //   title: 'Shoplifting',
          //   isSPALink: true
          // },
          // {
          //   href: '/pickpocketing',
          //   icon: 'Pickpocket',
          //   label: 'pickpocket-link',
          //   title: 'Pickpocket',
          //   isSPALink: true
          // },
          {
            href: '/',
            icon: 'CrimesHub',
            label: 'hub-link',
            title: 'Back to HUB',
            isSPALink: true
          }
        ]
      },
      {
        ID: 2,
        items: [
          // {
          //   href: '/searchforcash',
          //   icon: 'SearchForCash',
          //   label: 'searchforcash-link',
          //   title: 'Search for cash',
          //   isSPALink: true
          // },
          // {
          //   href: '/graffiti',
          //   icon: 'Graffiti',
          //   label: 'graffiti',
          //   title: 'Graffiti',
          //   isSPALink: true
          // },
          // {
          //   href: '/shoplifting',
          //   icon: 'Shoplifting',
          //   label: 'shoplifting-link',
          //   title: 'Shoplifting',
          //   isSPALink: true
          // },
          // {
          //   href: '/pickpocketing',
          //   icon: 'Pickpocket',
          //   label: 'pickpocket-link',
          //   title: 'Pickpocket',
          //   isSPALink: true
          // },
          {
            href: '/',
            icon: 'Hub',
            label: 'hub-link',
            title: 'Back to HUB',
            isSPALink: true
          }
        ]
      },
      {
        ID: 3,
        items: [
          // {
          //   href: '/searchforcash',
          //   icon: 'SearchForCash',
          //   label: 'searchforcash-link',
          //   title: 'Search for cash',
          //   isSPALink: true
          // },
          // {
          //   href: '/bootlegging',
          //   icon: 'Bootlegging',
          //   label: 'bootlegging-link',
          //   title: 'Bootlegging',
          //   isSPALink: true
          // },
          // {
          //   href: '/shoplifting',
          //   icon: 'Shoplifting',
          //   label: 'shoplifting-link',
          //   title: 'Shoplifting',
          //   isSPALink: true
          // },
          // {
          //   href: '/pickpocketing',
          //   icon: 'Pickpocket',
          //   label: 'pickpocket-link',
          //   title: 'Pickpocket',
          //   isSPALink: true
          // },
          {
            href: '/',
            icon: 'Hub',
            label: 'hub-link',
            title: 'Back to HUB',
            isSPALink: true
          }
        ]
      },
      {
        ID: 4,
        items: [
          // {
          //   href: '/searchforcash',
          //   icon: 'SearchForCash',
          //   label: 'searchforcash-link',
          //   title: 'Search for cash',
          //   isSPALink: true
          // },
          // {
          //   href: '/bootlegging',
          //   icon: 'Bootlegging',
          //   label: 'bootlegging-link',
          //   title: 'Bootlegging',
          //   isSPALink: true
          // },
          // {
          //   href: '/graffiti',
          //   icon: 'Graffiti',
          //   label: 'graffiti',
          //   title: 'Graffiti',
          //   isSPALink: true
          // },
          // {
          //   href: '/pickpocketing',
          //   icon: 'Pickpocket',
          //   label: 'pickpocket-link',
          //   title: 'Pickpocket',
          //   isSPALink: true
          // },
          {
            href: '/',
            icon: 'Hub',
            label: 'hub-link',
            title: 'Back to HUB',
            isSPALink: true
          }
        ]
      },
      {
        ID: 5,
        items: [
          // {
          //   href: '/searchforcash',
          //   icon: 'SearchForCash',
          //   label: 'searchforcash-link',
          //   title: 'Search for cash',
          //   isSPALink: true
          // },
          // {
          //   href: '/bootlegging',
          //   icon: 'Bootlegging',
          //   label: 'bootlegging-link',
          //   title: 'Bootlegging',
          //   isSPALink: true
          // },
          // {
          //   href: '/graffiti',
          //   icon: 'Graffiti',
          //   label: 'graffiti',
          //   title: 'Graffiti',
          //   isSPALink: true
          // },
          // {
          //   href: '/shoplifting',
          //   icon: 'Shoplifting',
          //   label: 'shoplifting-link',
          //   title: 'Shoplifting',
          //   isSPALink: true
          // },
          {
            href: '/',
            icon: 'Hub',
            label: 'hub-link',
            title: 'Back to HUB',
            isSPALink: true
          }
        ]
      },
      {
        ID: 4,
        items: [
          {
            href: '/',
            icon: 'Hub',
            label: 'hub-link',
            title: 'Back to HUB',
            isSPALink: true
          }
        ]
      }
    ]
  },
  tutorials: {
    activateOnLoad: true,
    hideByDefault: true,
    default: {
      ID: 0,
      items: [
        {
          title: 'Crimes tutorial',
          text:
            // eslint-disable-next-line max-len
            'Here you can bet on real-world sports matches. Simply select a match and place your bet. Winnings will usually be credited to you within 24 hours of the match ending. Winnings or returned money are sent to you in the form of a cashiers check which can be accessed from your bank.'
        }
      ]
    }
  }
}
