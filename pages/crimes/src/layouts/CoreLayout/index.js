import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'
import { subscribeOnDarkMode, unsubscribeFromDarkMode } from '@torn/shared/utils/darkModeChecker'

import InfoBox from '@torn/shared/components/InfoBox'
import DebugInfo from '@torn/shared/components/DebugBox'

import { Tooltip } from '../../components'
import AppHeader from '../../../../../shared/components/AppHeader'

import { hideDebugInfo, manualLayoutChecker, darkModeChecker } from '../../modules/actions'

import { APP_HEADER } from '../../constants'

import '../../styles/dark-mode.scss'
import '../../styles/light-mode.scss'

import styles from './index.cssmodule.scss'

class CoreLayout extends PureComponent {
  componentDidMount() {
    this._checkManualDesktopMode()
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
    unsubscribeFromDarkMode()
  }

  _checkManualDesktopMode = () => {
    const { checkManualDesktopMode } = this.props

    subscribeOnDesktopLayout(payload => checkManualDesktopMode(payload))
  }

  _checkManualDesktopMode = () => {
    const { checkDarkMode } = this.props

    subscribeOnDarkMode(payload => checkDarkMode(payload))
  }

  render() {
    const {
      children,
      hideDebug,
      common: { dbg, infoBox, appID, pageID }
    } = this.props

    return (
      <div className={`${styles['core-layout']} crimes-app`}>
        <Tooltip />
        <AppHeader appID={appID} pageID={pageID} clientProps={APP_HEADER} />
        {dbg && <DebugInfo debugMessage={dbg?.msg || dbg} close={hideDebug} />}
        {infoBox && <InfoBox {...infoBox} />}
        {children}
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
  common: PropTypes.object,
  tutorial: PropTypes.object,
  location: PropTypes.object,
  hideDebug: PropTypes.func,
  checkManualDesktopMode: PropTypes.func,
  checkDarkMode: PropTypes.func
}

CoreLayout.defaultProps = {
  hideDebug: () => {},
  checkManualDesktopMode: () => {},
  checkDarkMode: () => {},
  common: {},
  tutorial: {},
  location: {
    pathname: ''
  }
}

const mapStateToProps = state => {
  return {
    common: state.common
  }
}

const mapDispatchToState = dispatch => ({
  hideDebug: () => dispatch(hideDebugInfo()),
  checkManualDesktopMode: payload => dispatch(manualLayoutChecker(payload)),
  checkDarkMode: payload => dispatch(darkModeChecker(payload))
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToState
  )(CoreLayout)
)
