import { takeLatest } from 'redux-saga/effects'
import { loadHubData, locationChange, resetStats, updateExp } from './sagas'
import { LOAD_HUB_DATA, UPDATE_EXP, RESET_STATS, LOCATION_CHANGE } from '../constants'

export default function* watchHubData() {
  yield takeLatest(LOAD_HUB_DATA, loadHubData)
  yield takeLatest(UPDATE_EXP, updateExp)
  yield takeLatest(RESET_STATS, resetStats)
  yield takeLatest(LOCATION_CHANGE, locationChange)
}
