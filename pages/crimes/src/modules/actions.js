import {
  LOAD_HUB_DATA,
  HUB_DATA_LOADED,
  SHOW_DEBUG_INFO,
  HIDE_DEBUG_INFO,
  SHOW_INFOBOX,
  CRIME_IS_READY,
  CRIME_IS_NOT_READY,
  UPDATE_EXP,
  RESET_STATS,
  RESET_STATS_DONE,
  SET_EXP,
  SET_CRIME_LEVEL,
  GET_CURRENT_OUTCOME,
  HIDE_ARROWS,
  SHOW_ARROWS,
  CURRENT_PAGE_DATA,
  CURRENT_ARROW_CLICK,
  CURRENT_ARROW_HOVER,
  FREEZER_SLIDER,
  SHOW_DROPDOWN_PANEL,
  TOGGLE_DROPDOWN_PANEL,
  HIDE_DROPDOWN_PANEL,
  EDITOR_SWITCHER,
  USER_JAILED,
  DATA_RESETTED,
  FETCH_DATA,
  DATA_FETCHED,
  ATTEMPT,
  ATTEMPT_START,
  ATTEMPT_FETCHED,
  POOLING,
  POOLING_FETCHED,
  CLEAR_OUTDATED_OUTCOME,
  LEVEL_UP,
  AJAX,
  MANUAL_LAYOUT_SET,
  DARK_MODE_CHECK,
  TOGGLE_ARROWS,
  CLOSE_OUTCOME
  // MEDIA_SCREEN_CHANGED
} from '../constants'

// ------------------------------------
// Actions
// ------------------------------------
export const fetchData = (crimeName, url) => ({
  type: `${crimeName}/${FETCH_DATA}`,
  url
})

export const dataFetched = (crimeName, json) => ({
  type: `${crimeName}/${DATA_FETCHED}`,
  json,
  meta: 'ajax'
})

export const attempt = (crimeName, url, itemId, isCurrentRow) => ({
  type: `${crimeName}/${ATTEMPT}`,
  url,
  itemId,
  isCurrentRow
})

export const attemptStart = (crimeName, itemId, isCurrentRow) => ({
  type: `${crimeName}/${ATTEMPT_START}`,
  itemId,
  isCurrentRow
})

export const attemptFetched = (crimeName, json) => ({
  type: `${crimeName}/${ATTEMPT_FETCHED}`,
  json,
  meta: 'ajax'
})

export const pooling = crimeName => ({
  type: `${crimeName}/${POOLING}`
})

export const poolingFetched = (crimeName, json) => ({
  type: `${crimeName}/${POOLING_FETCHED}`,
  json,
  meta: 'ajax'
})

export function levelUp(crimeName, data) {
  return {
    type: `${crimeName}/${LEVEL_UP}`,
    payload: data
  }
}

export const clearOutdatedOutcome = crimeName => ({
  type: `${crimeName}/${CLEAR_OUTDATED_OUTCOME}`
})

export const showInfoBox = data => {
  return {
    type: SHOW_INFOBOX,
    payload: data
  }
}

export const loadHubData = () => ({
  type: LOAD_HUB_DATA
})

export const hubDataLoaded = json => ({
  type: HUB_DATA_LOADED,
  json,
  meta: AJAX
})

export const showDebugInfo = msg => ({
  type: SHOW_DEBUG_INFO,
  msg
})

export const hideDebugInfo = () => ({
  type: HIDE_DEBUG_INFO
})

export const crimeReady = bundle => ({
  type: CRIME_IS_READY,
  bundle
})

export const crimeIsNotReady = bundle => ({
  type: CRIME_IS_NOT_READY,
  bundle
})

export const updateExp = data => ({
  type: UPDATE_EXP,
  data
})

export const resetStats = () => ({
  type: RESET_STATS
})

export const statsResetted = (crimeName, json) => ({
  type: `${crimeName}/${RESET_STATS_DONE}`,
  json
})

export const dataResetted = (crimeName, json) => ({
  type: `${crimeName}/${DATA_RESETTED}`,
  json
})

export const setExp = result => ({
  type: SET_EXP,
  result
})

export const setCrimeLevel = (result, currentCrime) => ({
  type: `${currentCrime}/${SET_CRIME_LEVEL}`,
  result
})

export const getCurrentOutcome = (outcomeHeight, outcomeOffestTop) => ({
  type: GET_CURRENT_OUTCOME,
  outcomeHeight,
  outcomeOffestTop
})

export const showArrow = () => ({
  type: SHOW_ARROWS
})

export const hideArrow = () => ({
  type: HIDE_ARROWS
})

export const toggleArrows = () => ({
  type: TOGGLE_ARROWS
})

export const showDropDownPanel = () => ({
  type: SHOW_DROPDOWN_PANEL
})

export const toggleDropDownPanel = () => ({
  type: TOGGLE_DROPDOWN_PANEL
})

export const hideDropDownPanel = () => ({
  type: HIDE_DROPDOWN_PANEL
})

export const getCurrentPageData = (crimeName, pageID) => ({
  type: CURRENT_PAGE_DATA,
  crimeName,
  pageID
})

export const prevNextArrowsClick = currentArrow => ({
  type: CURRENT_ARROW_CLICK,
  currentArrow
})

export const prevNextArrowsHover = currentArrow => ({
  type: CURRENT_ARROW_HOVER,
  currentArrow
})

export const freezerSlider = payload => ({
  type: FREEZER_SLIDER,
  payload
})

export const editorSwitcher = () => ({
  type: EDITOR_SWITCHER
})

export const userJailed = status => ({
  type: USER_JAILED,
  status
})

export const manualLayoutChecker = status => ({
  type: MANUAL_LAYOUT_SET,
  status
})

export const darkModeChecker = status => ({
  type: DARK_MODE_CHECK,
  status
})

export const closeOutcome = () => ({
  type: `${window.location.hash.replace(/^(#\/)/i, '')}/${CLOSE_OUTCOME}`
})

export const actions = {
  crimeReady,
  hideDebugInfo,
  hubDataLoaded,
  loadHubData,
  showDebugInfo,
  showInfoBox,
  setExp,
  closeOutcome
}
