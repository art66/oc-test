import { LOCATION_CHANGE } from 'react-router-redux'
import {
  HUB_DATA_LOADED,
  SHOW_DEBUG_INFO,
  HIDE_DEBUG_INFO,
  SHOW_INFOBOX,
  CRIME_IS_READY,
  CRIME_IS_NOT_READY,
  SET_EXP,
  GET_CURRENT_OUTCOME,
  HIDE_ARROWS,
  SHOW_ARROWS,
  TOGGLE_ARROWS,
  CURRENT_PAGE_DATA,
  CURRENT_ARROW_CLICK,
  CURRENT_ARROW_HOVER,
  FREEZER_SLIDER,
  SHOW_DROPDOWN_PANEL,
  TOGGLE_DROPDOWN_PANEL,
  HIDE_DROPDOWN_PANEL,
  EDITOR_SWITCHER,
  USER_JAILED,
  MANUAL_LAYOUT_SET,
  DARK_MODE_CHECK
  // MEDIA_SCREEN_CHANGED
} from '../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [HUB_DATA_LOADED]: (state, action) => ({
    ...state,
    ...action.json.DB
  }),
  [SHOW_DEBUG_INFO]: (state, action) => ({
    ...state,
    dbg: action.msg
  }),
  [HIDE_DEBUG_INFO]: state => ({
    ...state,
    dbg: null
  }),
  [LOCATION_CHANGE]: (state, action) => ({
    ...state,
    nextPathname: action.payload.pathname
  }),
  [SHOW_INFOBOX]: (state, action) => ({
    ...state,
    infoBox: action.payload
  }),
  [CRIME_IS_READY]: state => ({
    ...state,
    crimeIsReady: true
  }),
  [CRIME_IS_NOT_READY]: state => ({
    ...state,
    crimeIsReady: false
  }),
  [SET_EXP]: (state, action) => ({
    ...state,
    exp: action.result.DB.exp
  }),
  [GET_CURRENT_OUTCOME]: (state, action) => ({
    ...state,
    outcomeHeight: action.outcomeHeight,
    outcomeOffestTop: action.outcomeOffestTop
  }),
  [HIDE_ARROWS]: state => ({
    ...state,
    showArrows: false
  }),
  [SHOW_ARROWS]: state => ({
    ...state,
    showArrows: true
  }),
  [TOGGLE_ARROWS]: state => ({
    ...state,
    showArrows: !state.showArrows
  }),
  [SHOW_DROPDOWN_PANEL]: state => ({
    ...state,
    showDropDown: true
  }),
  [TOGGLE_DROPDOWN_PANEL]: state => ({
    ...state,
    showDropDown: !state.showDropDown
  }),
  [HIDE_DROPDOWN_PANEL]: state => ({
    ...state,
    showDropDown: false
  }),
  [CURRENT_PAGE_DATA]: (state, action) => ({
    ...state,
    crimeName: action.crimeName,
    pageID: action.pageID,
    outcomeHeight: 0,
    outcomeOffestTop: 0
  }),
  [CURRENT_ARROW_CLICK]: (state, action) => ({
    ...state,
    currentArrow: action.currentArrow
  }),
  [CURRENT_ARROW_HOVER]: (state, action) => ({
    ...state,
    currentArrow: action.currentArrow
  }),
  [FREEZER_SLIDER]: (state, action) => ({
    ...state,
    freeze: action.payload
  }),
  [EDITOR_SWITCHER]: state => ({
    ...state,
    editorSwitch: !state.editorSwitch
  }),
  [USER_JAILED]: (state, action) => ({
    ...state,
    jailed: action.status
  }),
  [MANUAL_LAYOUT_SET]: (state, action) => ({
    ...state,
    isDesktopLayoutSetted: action.status
  }),
  [DARK_MODE_CHECK]: (state, action) => ({
    ...state,
    isDarkMode: action.status
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  loadedBundles: [],
  showArrows: true,
  currentOutcome: ''
}

export default (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
