import { put, select } from 'redux-saga/effects'
import { getCurrentPageData, fetchData } from '../actions'
import { TYPE_IDS } from '../../constants'

export function* locationChange() {
  try {
    const getState = yield select(state => state)
    const {
      routing: { locationBeforeTransitions }
    } = getState

    if (locationBeforeTransitions) {
      const { hash } = locationBeforeTransitions
      const crimeName = hash.substr(2, hash.length)

      // get crime main data update for each re-layout
      if (getState[crimeName]) {
        yield put(fetchData(crimeName, `&step=crimesList&typeID=${TYPE_IDS[crimeName]}`))
      }

      yield put(getCurrentPageData(crimeName, TYPE_IDS[crimeName]))
    }
  } catch (error) {
    throw new Error('Wrong URL name -', error)
  }
}

export default locationChange
