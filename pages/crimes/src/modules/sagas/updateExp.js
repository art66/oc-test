import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { setExp, setCrimeLevel, showDebugInfo } from '../actions'
import fetchUpdate from './fetchUpdate'
import { ROOT_URL } from '../../constants'

export function* updateExp(data) {
  try {
    const getState = yield select(state => state)

    const {
      common: { crimeName }
    } = getState

    const currentCrime = crimeName
    const currentCrimeID = getState.common.crimesTypes.find(({ crimeRoute }) => crimeRoute === currentCrime).typeID
    const json = yield fetchUrl(`${ROOT_URL}&step=setExp`, {
      data: {
        ...data,
        typeID: currentCrimeID
      }
    })

    if (json.DB.error) {
      throw new Error(json.DB.error)
    }
    yield put(setExp(json))
    yield put(setCrimeLevel(json, currentCrime))

    yield fetchUpdate(json, crimeName)
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default updateExp
