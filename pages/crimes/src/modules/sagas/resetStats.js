import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { statsResetted, showDebugInfo } from '../actions'
import fetchUpdate from './fetchUpdate'
import { ROOT_URL, TYPE_IDS } from '../../constants'

export function* resetStats() {
  try {
    const getState = yield select(state => state)
    const {
      common: { crimeName }
    } = getState

    if (!crimeName || !TYPE_IDS[crimeName]) {
      throw new Error(`Crimes if not found - ${crimeName} or TYPE_ID is missed - ${TYPE_IDS[crimeName]}`)
    }

    const json = yield fetchUrl(`${ROOT_URL}&step=removeDebug&typeID=${TYPE_IDS[crimeName]}`)

    if (json.DB.error) {
      throw new Error(json.DB.error)
    }

    yield put(statsResetted(crimeName, json))

    yield fetchUpdate(json, crimeName)
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default resetStats
