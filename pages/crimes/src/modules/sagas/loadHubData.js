import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { hubDataLoaded, showDebugInfo } from '../actions'
import { ROOT_URL } from '../../constants'

export function* loadHubData() {
  try {
    const json = yield fetchUrl(`${ROOT_URL}&step=hub`)

    if (json.DB.error) {
      throw new Error(json.DB.error)
    }
    yield put(hubDataLoaded(json))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default loadHubData
