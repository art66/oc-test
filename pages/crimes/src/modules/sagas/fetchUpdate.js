import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { dataResetted } from '../actions'
import { TYPE_IDS, ROOT_URL } from '../../constants'

function* fetchUpdate(json, crimeName) {
  // extra block for update crime data in live mode [START]
  const responceData = yield fetchUrl(`${ROOT_URL}&step=crimesList&typeID=${TYPE_IDS[crimeName]}`)

  if (json.DB.error) {
    throw new Error(json.DB.error)
  }

  yield put(dataResetted(crimeName, responceData))
  // extra block for update crime data in live mode [END]
}

export default fetchUpdate
