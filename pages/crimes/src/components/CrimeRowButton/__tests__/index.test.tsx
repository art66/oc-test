import React from 'react'
import { shallow, mount } from 'enzyme'
import initialState, { disabledButton, animationButton, goldStarButton } from './mocks'
import CrimeRowButton from '..'

jest.useFakeTimers()

describe('<CrimeRowButton />', () => {
  it('should return active main crime row button with onClick action event', () => {
    const Component = shallow(<CrimeRowButton {...initialState} />)

    expect(Component).toMatchSnapshot()

    expect(Component.find('.torn-btn.gold').length).toBe(1)
    Component.find('.torn-btn.gold').simulate('click')

    expect(Component).toMatchSnapshot()
  })
  it('should return disabled main crime row button', () => {
    const Component = shallow(<CrimeRowButton {...disabledButton} />)

    expect(Component.find('.torn-btn.gold').length).toBe(1)
    expect(Component.find('.torn-btn.gold').prop('disabled')).toBe(true)

    expect(Component).toMatchSnapshot()
  })
  it('should return load crime row button animation in case of inProgress === true', () => {
    const Component = shallow(<CrimeRowButton {...animationButton} />)

    expect(Component.find('.torn-btn.gold.mobileArrow').length).toBe(0)
    expect(Component.find('.torn-btn.gold').length).toBe(0)

    expect(Component.find('.pseudoBtnGoldContainer').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should return Gold Star and after 3 sec switch on regular button (rare outcome)', async () => {
    const Component = await mount(<CrimeRowButton {...goldStarButton} />)

    expect(Component.find('.torn-btn.gold.mobileArrow').length).toBe(0)
    expect(Component.find('.torn-btn.gold').length).toBe(0)
    expect(Component.find('.pseudoBtnGoldContainer').length).toBe(1)

    expect(Component.find('CooldownStar').length).toBe(2)
    expect(Component.state('goldStarAllowed')).toBe(true)

    expect(Component).toMatchSnapshot()

    jest.runAllTimers()
    expect(setTimeout).toHaveBeenCalledTimes(3)
    expect(Component.state('goldStarAllowed')).toBe(false)
    Component.update()

    expect(Component.find('CooldownStar').length).toBe(0)
    expect(Component.find('.torn-btn.gold').length).toBe(1)
    expect(Component.find('.torn-btn.gold').prop('disabled')).toBe(false)

    expect(Component).toMatchSnapshot()
  })
})
