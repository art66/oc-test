const initialState = {
  action: () => '1',
  crimeID: 0,
  button: {
    disabled: false,
    label: 'ATTEMPT'
  },
  isButtonDisabled: () => false
}

export const disabledButton = {
  ...initialState,
  button: {
    ...initialState.button,
    disabled: true
  },
  isButtonDisabled: () => false
}

export const animationButton = {
  ...initialState,
  isButtonDisabled: () => true
}

export const goldStarButton = {
  ...initialState,
  isRowActive: true,
  special: 'rare',
  isButtonDisabled: () => false
}

export default initialState
