import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import AnimationLoad from '@torn/shared/components/AnimationLoad'
import { SVGIconGenerator } from '@torn/shared/SVG'

import CooldownStar from '../CooldownStar'
import styles from './index.cssmodule.scss'

const DIMENSIONS = {
  width: '100%',
  height: '100%',
  viewbox: '-4 -4 30 52'
}

class CrimeRowButton extends PureComponent {
  static propTypes = {
    action: PropTypes.func,
    isDisabled: PropTypes.bool,
    isButtonDisabled: PropTypes.func,
    crimeID: PropTypes.number,
    button: PropTypes.object,
    special: PropTypes.string,
    isRowActive: PropTypes.bool,
    isDesktop: PropTypes.bool,
    mediaType: PropTypes.string,
    isManualDesktop: PropTypes.bool,
    isDarkMode: PropTypes.bool,
    nerve: PropTypes.number
  }

  static defaultProps = {
    action: () => {},
    crimeID: 0,
    button: {},
    special: '',
    isDisabled: false,
    isButtonDisabled: () => {},
    isRowActive: false,
    isDesktop: false,
    isManualDesktop: false,
    isDarkMode: false,
    mediaType: 'desktop',
    nerve: 0
  }

  constructor(props) {
    super(props)

    this.state = {
      greyStarAllowed: true
    }
  }

  _btnAnimationHolder = () => {
    const { isDesktop, button, isDarkMode } = this.props
    const dotsCount = !isDesktop ? 3 : 7

    return (
      <div
        className={styles.pseudoBtnGreyContainer}
        style={{ width: `${button.width}px`, height: `${button.height}px` }}
      >
        <AnimationLoad dotsColor={isDarkMode ? 'white' : 'black'} dotsCount={dotsCount} isAdaptive={true} />
      </div>
    )
  }

  _btnAction = () => {
    const { action, crimeID, button, mediaType, isManualDesktop, isDarkMode, nerve } = this.props
    const isTouchScreen = !isManualDesktop && mediaType !== 'desktop'

    const btnClass = cn({
      'torn-btn': true,
      grey: true,
      'btn-dark-bg': isDarkMode,
      [styles.crimeButton]: true,
      [styles.mobileArrow]: isTouchScreen
    })

    const label = !isTouchScreen ? (
      <>
        <span className={styles.btnLabel}>{button.label}</span>
        <span className={styles.btnDlm} />
        <span className={styles.btnNerve}>{nerve || 0}</span>
      </>
    ) : ''

    const icon = isTouchScreen ? (
      <SVGIconGenerator
        iconName={button.iconName}
        dimensions={DIMENSIONS}
      />
    ) : ''

    return (
      <button
        type='button'
        aria-label={isTouchScreen ? button.label : ''}
        href='#'
        className={btnClass}
        onClick={action}
        id={crimeID}
        disabled={button.disabled}
        style={{ width: `${button.width}px`, height: `${button.height}px` }}
      >
        {icon}
        {label}
      </button>
    )
  }

  _greyStarButtonDisabler = () => {
    this.setState({
      greyStarAllowed: false
    })
  }

  _btnGreyStar = () => {
    const { greyStarAppearAllowed } = this.state
    const { button } = this.props

    return (
      <div
        className={styles.pseudoBtnGreyContainer}
        style={{ width: `${button.width}px`, height: `${button.height}px` }}
      >
        <CooldownStar hide={this._greyStarButtonDisabler} isStarShown={greyStarAppearAllowed} />
      </div>
    )
  }

  _checkGreyStarLayout = () => {
    const { isRowActive, special = '' } = this.props
    const { greyStarAllowed } = this.state
    const isSpecial = special === 'rare'

    return isRowActive && greyStarAllowed && isSpecial
  }

  render() {
    const { isDisabled, isButtonDisabled } = this.props

    if (isDisabled || isButtonDisabled && isButtonDisabled()) {
      return this._btnAnimationHolder()
    }

    if (this._checkGreyStarLayout()) {
      return this._btnGreyStar()
    }

    return this._btnAction()
  }
}

const mapStateToProps = state => ({
  isDarkMode: state.common.isDarkMode
})

export default connect(mapStateToProps)(CrimeRowButton)
