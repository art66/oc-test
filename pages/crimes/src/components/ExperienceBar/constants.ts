export const ANIMATION_DURATION = 600
export const MAX_PROGRESS = 100
export const TOOLTIP_ID = 'expBar'

export const DARK_MODE_GRADIENT = [
  {
    step: 0,
    color: '#000'
  },
  {
    step: 1,
    color: '#333'
  }
]

// SVG ICONS PROPS
export const SVG_CONTAINER_DEFAULT = {
  fill: {
    stroke: '#FFF',
    strokeWidth: '1',
    color: 'url(\'#star_container_gradient_one\')'
  },
  dimensions: {
    width: 42,
    height: 32,
    viewbox: '0.5 1 43.5 30'
  },
  filter: {
    ID: 'star_container_shadow_one',
    active: false,
    shadow: {
      x: 0,
      y: 1,
      blur: 1,
      color: '#4a444494'
    }
  },
  gradient: {
    scheme: [
      {
        step: 0,
        color: '#CCC'
      },
      {
        step: 1,
        color: '#EEE'
      }
    ],
    ID: 'star_container_gradient_one',
    transform: 'rotate(90)'
  }
}

export const SVG_CONTAINER_LAST = {
  ...SVG_CONTAINER_DEFAULT,
  dimensions: {
    ...SVG_CONTAINER_DEFAULT.dimensions,
    viewbox: '0.5 -1.5 42 33'
  }
}

export const SVG_CONTAINER_FULL = {
  ...SVG_CONTAINER_DEFAULT,
  filter: {
    ID: null,
    active: false
  },
  fill: {
    stroke: 'transparent',
    strokeWidth: '0',
    color: 'url(\'#star_container_gradient_one\')'
  },
  gradient: {
    scheme: [
      {
        step: 0,
        color: '#b7b7a6'
      },
      {
        step: 1,
        color: '#e8e8d7'
      }
    ],
    ID: 'star_container_gradient_one',
    transform: 'rotate(90)'
  },
  dimensions: {
    width: 45,
    height: 41,
    viewbox: '0 1.2 44 39'
  }
}
