import React from 'react'
import { mount } from 'enzyme'
import initialState, {
  secondState,
  thirthState,
  fourthState,
  fifthState,
  finalState,
  animationPrepare,
  animationStart,
  animationInProgressOneTime
} from './mocks'
import { ExperienceBar } from '..'

describe('<ExperienceBar />', () => {
  it('should render with lvl 5 achived and metal stars apper ExperienceBar', () => {
    const Component = mount(<ExperienceBar {...initialState} />)

    expect(Component.find('ExperienceBar').length).toBe(1)
    expect(Component.find('.experienceBarWrap').length).toBe(1)
    expect(Component).toMatchSnapshot()
    expect(
      Component.find('SVGIcon')
        .at(0)
        .prop('iconProps').type
    ).toBe('metal')

    expect(Component).toMatchSnapshot()
  })
  it('should render with lvl 25 achived and bronze stars apper ExperienceBar', () => {
    const Component = mount(<ExperienceBar {...secondState} />)

    expect(Component.find('ExperienceBar').length).toBe(1)
    expect(Component.find('.experienceBarWrap').length).toBe(1)
    expect(
      Component.find('SVGIcon')
        .at(0)
        .prop('iconProps').type
    ).toBe('bronze')

    expect(Component).toMatchSnapshot()
  })
  it('should render with lvl 50 achived and gold stars apper ExperienceBar', () => {
    const Component = mount(<ExperienceBar {...thirthState} />)

    expect(Component.find('ExperienceBar').length).toBe(1)
    expect(Component.find('.experienceBarWrap').length).toBe(1)
    expect(
      Component.find('SVGIcon')
        .at(0)
        .prop('iconProps').type
    ).toBe('silver')

    expect(Component).toMatchSnapshot()
  })
  it('should render with lvl 50 achived and magic stars apper ExperienceBar', () => {
    const Component = mount(<ExperienceBar {...fourthState} />)

    expect(Component.find('ExperienceBar').length).toBe(1)
    expect(Component.find('.experienceBarWrap').length).toBe(1)
    expect(
      Component.find('SVGIcon')
        .at(0)
        .prop('iconProps').type
    ).toBe('silver')

    expect(Component).toMatchSnapshot()
  })
  it('should render with lvl 75 achived and platinum stars apper ExperienceBar', () => {
    const Component = mount(<ExperienceBar {...fifthState} />)

    expect(Component.find('ExperienceBar').length).toBe(1)
    expect(Component.find('.experienceBarWrap').length).toBe(1)
    expect(
      Component.find('SVGIcon')
        .at(0)
        .prop('iconProps').type
    ).toBe('gold')

    expect(Component).toMatchSnapshot()
  })
  it('should render with lvl 100 achived and brilliant one star apper ExperienceBar', () => {
    const Component = mount(<ExperienceBar {...finalState} />)

    expect(Component.find('ExperienceBar').length).toBe(1)
    expect(Component.find('.experienceBarWrap').length).toBe(1)
    expect(
      Component.find('SVGIcon')
        .at(0)
        .prop('iconProps').type
    ).toBe('platinum')

    expect(Component).toMatchSnapshot()
  })
  // eslint-disable-next-line max-statements
  it('should render animation progress from 5% to 25% after one click and remove it after 0.350s', async () => {
    jest.useFakeTimers()

    // initialization
    const Component = mount(<ExperienceBar {...animationPrepare} />)

    expect(Component.find('.experienceBarWrap').length).toBe(1)
    expect(Component.find('.progress').length).toBe(1)
    expect(Component.find('.barHighlighterWrap').length).toBe(1)

    // [START] estimating values of the bars
    expect(Component.find('.progress').width).toBe(undefined)
    expect(Component.find('.barHighlighterWrap').prop('style').width).toBe('0%')
    expect(Component.find('.barHighlighterWrap').prop('style').right).toBe('unset')
    expect(Component.find('.barHighlighterWrap').prop('style').left).toBe('0%')

    expect(Component).toMatchSnapshot()

    Component.setProps(animationStart)
    Component.update()

    expect(Component.find('.progress').prop('style').width).toBe('5%')
    expect(Component.find('.barHighlighterWrap').prop('style').width).toBe('0%')
    expect(Component.find('.barHighlighterWrap').prop('style').right).toBe('95%')
    expect(Component.find('.barHighlighterWrap').prop('style').left).toBe('unset')

    expect(Component).toMatchSnapshot()

    Component.setProps(animationInProgressOneTime)
    Component.update()

    expect(Component.find('.progress').prop('style').width).toBe('25%')
    expect(Component.find('.barHighlighterWrap').prop('style').width).toBe('20%')
    expect(Component.find('.barHighlighterWrap').prop('style').right).toBe('unset')
    expect(Component.find('.barHighlighterWrap').prop('style').left).toBe('5%')
    // [END] estimating values of the bars

    // [START] decreasing highlight bar
    jest.advanceTimersByTime(3000)

    await setTimeout(() => {
      const prevState = Component.state()

      Component.setState({
        updateType: 'state',
        progressAmountDifferenceSum: 0,
        tempAnimProgress: prevState.progressAmountDifferenceSum
      })
    }, 3000)

    Component.update()

    expect(Component.find('.barHighlighterWrap').prop('style').width).toBe('0%')

    expect(Component).toMatchSnapshot()
    // [END] decreasing highlight bar
  }, 5000)
})
