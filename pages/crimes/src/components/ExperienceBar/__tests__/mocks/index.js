export const initialState = {
  progress: 5,
  current: 5
}

export const secondState = {
  progress: 25,
  current: 25
}

export const thirthState = {
  progress: 50,
  current: 50
}

export const fourthState = {
  progress: 50,
  current: 50
}

export const fifthState = {
  progress: 75,
  current: 75
}

export const finalState = {
  progress: 100,
  current: 100
}

export const preNewLevel = {
  progress: 99,
  current: 56
}

export const newLevel = {
  progress: 5,
  current: 57
}

export const animationPrepare = {
  ...initialState,
  progress: 0
}

export const animationStart = {
  ...initialState
}

export const animationInProgressOneTime = {
  ...initialState,
  progress: 25
}

export default initialState
