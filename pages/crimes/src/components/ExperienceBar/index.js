import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import cn from 'classnames'

import { SVGIconGenerator } from '@torn/shared/SVG/'
import { debounce } from '@torn/shared/utils/debounce'
import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'
import isValue from '@torn/shared/utils/isValue'

import StarLVL from '../StarLVL'

import {
  ANIMATION_DURATION,
  MAX_PROGRESS,
  TOOLTIP_ID,
  SVG_CONTAINER_DEFAULT,
  SVG_CONTAINER_LAST,
  SVG_CONTAINER_FULL,
  DARK_MODE_GRADIENT
} from './constants'

import styles from './styles.cssmodule.scss'
import './animations.scss'

export class ExperienceBar extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      updateType: '',
      isPropsMounted: false,
      levelChanged: false,
      startedHighlightPoint: 0,
      progressAmountDifferenceSum: null,
      tempProgress: null,
      tempAnimProgress: 0 // required for smooth reverse animation of the highlight bar
    }

    this._progressRef = React.createRef()
    this._highlightBar = React.createRef()
  }

  // used for anim transition manipulation
  static getDerivedStateFromProps(nextProps, prevState) {
    const { updateType, tempProgress, progressAmountDifferenceSum } = prevState
    const { progress } = nextProps

    if (!updateType || updateType === 'props') {
      const progressAmountDiff = progressAmountDifferenceSum + (progress - tempProgress)
      const levelChanged = progress < tempProgress
      const isEmptyProgress = !isValue(tempProgress) || levelChanged

      return {
        updateType: 'props',
        tempProgress: progress,
        levelChanged,
        progressAmountDifferenceSum: isEmptyProgress ? progress : progressAmountDiff
      }
    }

    if (prevState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    return null
  }

  componentDidMount() {
    this._runBarHighlightingDebounce = this._createBarDebounceUpdate()

    this._setStartPointForAnimBar()
    this._renderTooltip()
  }

  componentDidUpdate(prevProps, prevState) {
    const { updateType, isPropsMounted, levelChanged } = this.state
    const propsReceived = updateType === 'props' && isPropsMounted

    this._setStartPointForAnimBar()

    if (propsReceived) {
      this._runBarHighlightingDebounce()
    }

    if (levelChanged) {
      this._resetHighlightingBar()
    }

    this._renderTooltip('update')
  }

  componentWillUnmount() {
    this._runBarHighlightingDebounce({ clearTimer: true })
    clearInterval(this._setStartedTimedID)
  }

  // we need to wait on fetch response to be sure that
  // we're received actual data, instead of a mock one.
  _setStartPointForAnimBar = () => {
    const { isPropsMounted } = this.state
    const { progress } = this.props

    if (!isPropsMounted && progress > 0) {
      this.setState({
        updateType: 'state',
        isPropsMounted: true,
        startedHighlightPoint: progress,
        progressAmountDifferenceSum: 0
      })
    }
  }

  _createBarDebounceUpdate = () => {
    this._runDebounce = debounce({
      callback: this._checkProgressAmountDifferenceSum,
      delay: 3000
    })

    return this._runDebounce
  }

  _checkProgressAmountDifferenceSum = () => {
    const { updateType } = this.state
    const { progress } = this.props

    if (updateType === 'state') {
      return
    }

    // removing animation width once no more actions fired!
    this.setState(prevState => ({
      updateType: 'state',
      progressAmountDifferenceSum: 0,
      tempAnimProgress: prevState.progressAmountDifferenceSum
    }))

    // we should update starting point after animation
    // finish to be ready for the new one.
    this._setStartedTimedID = setTimeout(() => {
      this.setState({
        updateType: 'state',
        startedHighlightPoint: progress,
        tempAnimProgress: 0
      })
    }, 350)
  }

  _resetHighlightingBar = () => {
    const setBarsTransition = transitionType => {
      const transitionHolder = { disable: 'unset', active: 'width .35s ease-out' }
      const bars = [this._progressRef.current, this._highlightBar.current]

      bars.forEach(bar => {
        bar.style.transition = transitionHolder[transitionType]
      })
    }

    this._setLeaveTimedID = setTimeout(() => {
      setBarsTransition('disable')

      this.setState({
        levelChanged: false,
        startedHighlightPoint: 0
      })

      this._setEndTimedID = setTimeout(() => {
        setBarsTransition('active')
      }, 50)
    }, 350)
  }

  _getMainProps = () => {
    const { current, progress } = this.props
    const final = current === MAX_PROGRESS
    const finalProgress = final ? MAX_PROGRESS : progress
    const next = !final && current + 1

    return {
      final,
      finalProgress,
      next
    }
  }

  _getSVGSchema = (svg = SVG_CONTAINER_DEFAULT) => {
    const { isDarkMode } = this.props

    const svgDataSchema = {
      ...svg,
      fill: {
        ...svg.fill,
        stroke: '#505050',
        color: 'url(\'#star_container_gradient_dark_one\')'
      },
      gradient: {
        ...svg.gradient,
        ID: 'star_container_gradient_dark_one',
        scheme: DARK_MODE_GRADIENT
      }
    }

    return isDarkMode ? svgDataSchema : SVG_CONTAINER_DEFAULT
  }

  _getExperienceBar = () => {
    const { final } = this._getMainProps()

    const fullbar = (
      <div className={styles.barBgFullWrap}>
        <div className={cn(styles.barBg, styles.barBgFull)} />
        <SVGIconGenerator
          iconName='StarContainer'
          type='romb'
          customClass={styles.barMiddleSVG}
          {...SVG_CONTAINER_FULL}
        />
      </div>
    )

    const defaultBar = (
      <div className={styles.mainBackground}>
        <SVGIconGenerator
          iconName='StarContainer'
          type='default'
          customClass={styles.l}
          {...this._getSVGSchema(SVG_CONTAINER_DEFAULT)}
        />
        <div className={styles.barBg} />
        <SVGIconGenerator
          iconName='StarContainer'
          type='rotated'
          customClass={styles.r}
          {...this._getSVGSchema(SVG_CONTAINER_LAST)}
        />
      </div>
    )

    const key = final ? 1 : 2
    const currentBar = final ? fullbar : defaultBar

    return (
      <TransitionGroup className='expBarContainerAnim'>
        <CSSTransition key={key} classNames='fadeStyles' timeout={ANIMATION_DURATION}>
          {currentBar}
        </CSSTransition>
      </TransitionGroup>
    )
  }

  _animateStar = (child, name, key) => {
    if (!child || child === null) return null

    return (
      <TransitionGroup className='sidesStarContainerAnim'>
        <CSSTransition classNames={name} timeout={ANIMATION_DURATION} key={key}>
          {child}
        </CSSTransition>
      </TransitionGroup>
    )
  }

  _renderStar = (starType, starClass, isFinal) => {
    if (!starType) return null

    return (
      <div className={cn(styles.lvlWrap, starClass, { [styles.last]: isFinal })}>
        <StarLVL lvl={starType} />
      </div>
    )
  }

  _getStars = () => {
    const { current } = this.props
    const { final, finalProgress, next } = this._getMainProps()

    if (!current) return null

    const animProps = {
      key: next ? `${current}_r` : `${finalProgress}_l`,
      name: final ? 'fadeStyles' : 'expStar'
    }

    return (
      <React.Fragment>
        {this._animateStar(this._renderStar(current, styles.current, final), animProps.name, animProps.key)}
        {this._animateStar(this._renderStar(next, styles.next), animProps.name, animProps.key)}
      </React.Fragment>
    )
  }

  _renderTooltip = stage => {
    const { finalProgress, next } = this._getMainProps()

    const title = `Crime skill level: ${next} (${(finalProgress && finalProgress.toFixed(2)) || 0}%)`
    const payload = { child: title, ID: TOOLTIP_ID }

    if (stage === 'update') {
      tooltipSubscriber.update(payload)
    } else {
      tooltipSubscriber.subscribe(payload)
    }
  }

  _renderBarHighlighter = () => {
    const {
      isPropsMounted,
      progressAmountDifferenceSum,
      startedHighlightPoint,
      tempAnimProgress,
      levelChanged
    } = this.state

    const styleConfig = {
      width: `${progressAmountDifferenceSum}%`
    }

    const styleConfigAdapter = () => {
      if (levelChanged) {
        styleConfig.width = '100%'
        styleConfig.left = `${startedHighlightPoint}%`
        styleConfig.right = 'unset'
      } else if (isPropsMounted && progressAmountDifferenceSum === 0) {
        styleConfig.right = `${Math.abs(100 - (startedHighlightPoint + tempAnimProgress))}%` || ''
        styleConfig.left = 'unset'
      } else {
        styleConfig.left = `${startedHighlightPoint}%`
        styleConfig.right = 'unset'
      }
    }

    styleConfigAdapter()

    return (
      <div ref={this._highlightBar} className={styles.barHighlighterWrap} style={styleConfig}>
        <div className={styles.barHighlighter} />
      </div>
    )
  }

  _renderProgress = () => {
    const { finalProgress } = this._getMainProps()
    const { levelChanged } = this.state

    return (
      <div
        className={styles.progressWrapper}
        ref={this._progressRef}
        style={{ width: `${levelChanged ? '100' : finalProgress}%` }}
      >
        <div className={styles.progress} />
        <div className={styles.progressBcg} />
      </div>
    )
  }

  _renderExpDividers = () => {
    return (
      <div className={styles.expDividers}>
        <i className={styles.expDivider} />
        <i className={styles.expDivider} />
        <i className={styles.expDivider} />
      </div>
    )
  }

  render() {
    const { final } = this._getMainProps()

    return (
      <div className={styles.experienceBarWrap}>
        {this._getExperienceBar()}
        <div id={TOOLTIP_ID} className={cn(styles.barWrap, { [styles.final]: final })}>
          {this._renderBarHighlighter()}
          {this._renderProgress()}
          <div className={styles.bar} />
          {this._renderExpDividers()}
        </div>
        {this._getStars()}
      </div>
    )
  }
}

ExperienceBar.propTypes = {
  isDarkMode: PropTypes.bool,
  progress: PropTypes.number,
  current: PropTypes.number
}

ExperienceBar.defaultProps = {
  progress: null,
  current: null
}

const mapStateToProps = ({ common }) => ({
  isDarkMode: common.isDarkMode
})

export default connect(mapStateToProps, null)(ExperienceBar)
