import React from 'react'

import Cooldown from './Cooldown'
import Star from './Star'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class CooldownStar extends React.PureComponent<IProps, any> {
  static defaultProps = {
    isStarShown: false,
    hide: () => {}
  }

  timer: any

  componentDidMount() {
    const { hide } = this.props

    this.timer = setTimeout(hide, 3000)
  }

  componentWillUnmount() {
    clearTimeout(this.timer)
  }

  _renderCooldownStar = () => {
    return (
      <div className={styles.cooldownStarWrap}>
        <Cooldown />
        <Star />
      </div>
    )
  }

  render() {
    return this._renderCooldownStar()
  }
}

export default CooldownStar
