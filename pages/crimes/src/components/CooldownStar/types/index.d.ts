export interface IProps {
  isStarShown: boolean
  hide: () => void
}
