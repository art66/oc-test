import React from 'react'

import CooldownStar from './index'

export const Default = () => <CooldownStar />

Default.story = {
  name: 'default'
}

export default {
  title: 'Crimes/CooldownStar'
}
