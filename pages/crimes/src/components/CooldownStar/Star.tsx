import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'

import styles from './star.cssmodule.scss'

const FILL = {
  color: 'url(#cooldownStar)',
  stroke: '0',
  strokeWidth: '0'
}
const DIMENSIONS = {
  width: '100%',
  height: '100%',
  viewbox: '0 0 5 5'
}
const GRADIENT = {
  active: true,
  ID: 'cooldownStar',
  scheme: [
    {
      step: '0',
      color: '#b28500'
    },
    {
      step: '1',
      color: '#ffbf00'
    }
  ],
  transform: 'rotate(90)'
}
const FILTER = {
  active: false
}

class Star extends React.PureComponent {
  _renderStar = () => {
    return <SVGIconGenerator iconName='Star' fill={FILL} dimensions={DIMENSIONS} filter={FILTER} gradient={GRADIENT} />
  }

  render() {
    return <div className={styles.starWrap}>{this._renderStar()}</div>
  }
}

export default Star
