import React from 'react'

import styles from './cooldown.cssmodule.scss'

const FILL = '#d9a301'
const HEIGHT = 17
const WIDTH = 4
const X = 48.5
const Y = 0
const RX = 0
const RY = 0
const POSITION_STEP = 15
const ANIM_TIME = 3
const ANIM_DELEY = 0.125
const LINE_COUNT = 24

class CooldownStar extends React.PureComponent {
  _renderRect = (rectStep, rectDeley) => {
    return (
      <g key={rectStep + rectDeley} transform={`rotate(${rectStep} 50 50)`}>
        <rect
          className={styles.diamondFi}
          x={X}
          y={Y}
          rx={RX}
          ry={RY}
          width={WIDTH}
          height={HEIGHT}
          fill={FILL}
          style={{ animationDelay: `${rectDeley}s` }}
        />
      </g>
    )
  }

  _renderCooldownCircle = () => {
    const circleLength = Array.from(Array(LINE_COUNT).keys())

    const rects = circleLength.map(rectNumber => {
      const rectStep = POSITION_STEP * rectNumber
      const rectDeley = ANIM_TIME - ANIM_DELEY * rectNumber

      return this._renderRect(rectStep, rectDeley)
    })

    return (
      <svg
        width='100%'
        height='100%'
        xmlns='http://www.w3.org/2000/svg'
        viewBox='0 0 100 100'
        preserveAspectRatio='xMidYMid'
      >
        {rects}
      </svg>
    )
  }

  render() {
    return <div className={styles.cooldownWrap}>{this._renderCooldownCircle()}</div>
  }
}

export default CooldownStar
