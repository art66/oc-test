import React from 'react'
import styles from '../../../../styles/searchforcash.cssmodule.scss'

const childNode = <div className='test-mock-children'>Time to test!</div>

const initialState = {
  rowShouldUpdate: () => {},
  showOutcome: false,
  attemptsTotal: 0,
  nonBorder: false,
  button: {
    action: () => {},
    disabled: false,
    label: ''
  },
  crimeID: 0,
  children: childNode,
  lastSubCrimeID: 0,
  currentRowID: 0,
  outcome: {},
  isCurrentRow: false,
  className: '',
  iconClass: '',
  nerve: 0,
  styles,
  itemId: 0,
  state: '',
  underTitle: false,
  dlmForTitle: true,
  title: '<span class="t-hide">Search the </span> Trash',
  id: 0,
  click: () => {},
  getCurrentOutcome: () => {},
  isRowActive: true,
  isButtonDisabled: () => true
}

export const activeButton = {
  ...initialState,
  button: {
    ...initialState.button,
    disabled: false
  }
}

export const disabledButton = {
  ...initialState,
  button: {
    ...initialState.button,
    disabled: true
  }
}

export const rowWihOutcome = {
  ...initialState,
  showOutcome: true,
  outcome: {
    result: 'success',
    outcomeDesc: '$36!',
    rewardsGive: {
      userRewards: {
        money: 36
      },
      itemsRewards: null
    },
    story: ['You search around the Post Office trash can.', 'You find a handful of greasy dollar bills!']
  }
}

export default initialState
