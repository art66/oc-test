import React from 'react'
import { mount } from 'enzyme'
import initialState, { activeButton, disabledButton, rowWihOutcome } from './mocks'

import CrimeRow from '..'

describe('<CrimeRow Global />', () => {
  it('should render basic crime row with children crimeRow included and crime button', () => {
    const Component = mount(<CrimeRow {...initialState} />)

    expect(Component.find('.crimeRow').length).toBe(1)
    expect(Component.find('.title.dlm').length).toBe(1)
    expect(
      Component.find('.title.dlm')
        .first()
        .html()
    ).toEqual('<div class="title dlm"><span class="t-hide">Search the </span> Trash</div>')
    expect(Component.find('.test-mock-children').length).toBe(1)
    expect(Component.find('.action').length).toBe(1)

    expect(Component.find('.torn-btn.gold').length).toBe(1)
    expect(Component.find('.torn-btn.gold').prop('disabled')).toBe(false)

    expect(Component).toMatchSnapshot()
  })
  it('should render row with active button', () => {
    const Component = mount(<CrimeRow {...activeButton} />)

    expect(Component.find('.crimeRow').length).toBe(1)
    expect(Component.find('.test-mock-children').length).toBe(1)

    expect(Component.find('.torn-btn.gold').length).toBe(1)
    expect(Component.find('.torn-btn.gold').prop('disabled')).toBe(false)

    expect(Component).toMatchSnapshot()
  })
  it('should render row with disable button', () => {
    const Component = mount(<CrimeRow {...disabledButton} />)

    expect(Component.find('.crimeRow').length).toBe(1)
    expect(Component.find('.test-mock-children').length).toBe(1)

    expect(Component.find('.torn-btn.gold').length).toBe(1)
    expect(Component.find('.torn-btn.gold').prop('disabled')).toBe(true)

    expect(Component).toMatchSnapshot()
  })
  it('should render row with inProgress === true button', () => {
    const Component = mount(<CrimeRow {...initialState} />)

    Component.setProps({ isRowActive: false })
    Component.setState({ inProgress: true })
    Component.setProps({ isRowActive: true })

    expect(Component).toMatchSnapshot()

    expect(Component.find('.crimeRow').length).toBe(1)
    expect(Component.find('.test-mock-children').length).toBe(1)
    expect(Component.find('.torn-btn').length).toBe(0)
    expect(Component.find('.pseudoBtnGoldContainer').length).toBe(1)
    expect(Component.find('.loader').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render row with outcome result', () => {
    const Component = mount(<CrimeRow {...rowWihOutcome} />)

    expect(Component.find('.crimeRow').length).toBe(1)

    expect(Component).toMatchSnapshot()

    expect(Component.find('div.outcome').length).toBe(1)
    expect(Component.find('.outcomeContainer').length).toBe(1)
    expect(Component.find('.outcomeWrap').length).toBe(1)
    expect(Component.find('.story').length).toBe(1)
    expect(Component.find('.result').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
