// if row has some elemnt under the title in mobile layout
// provide the "underTitle" attribute and give that element
// "underTitle" class
import pt from 'prop-types'
import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group'
import cn from 'classnames'
import isEmpty from '@torn/shared/utils/isEmpty'
import isValue from '@torn/shared/utils/isValue'

import globalCrimeRowCheck from '../../utils/shouldRowsUpdate'
import CrimeRowButton from '../CrimeRowButton'
import Outcome from '../Outcome'
import localStyles from './styles.cssmodule.scss'
import './anim.scss'

export class CrimeRow extends Component {
  static propTypes = {
    showOutcome: pt.bool,
    attemptsTotal: pt.number,
    nonBorder: pt.bool,
    button: pt.shape({
      action: pt.func,
      disabled: pt.bool,
      width: pt.number,
      height: pt.number,
      label: pt.string,
      iconName: pt.string
    }),
    getCurrentOutcome: pt.func,
    crimeID: pt.number,
    subID: pt.oneOfType([pt.number, pt.string]),
    lastSubCrimeID: pt.number,
    currentRowID: pt.oneOfType([pt.number, pt.string]),
    children: pt.node.isRequired,
    outcome: pt.object,
    isRowActive: pt.bool,
    isCurrentRow: pt.bool,
    isCDSelected: pt.bool,
    spraySelected: pt.string,
    genre: pt.oneOfType([pt.number, pt.string]),
    className: pt.string,
    dlmForTitle: pt.bool,
    iconClass: pt.string,
    nerve: pt.number,
    styles: pt.object,
    itemId: pt.oneOfType([pt.number, pt.string]),
    state: pt.string,
    title: pt.string,
    customImage: pt.oneOfType([pt.element, pt.string]),
    subTitle: pt.string,
    underTitle: pt.bool,
    isRowNotInProgress: pt.bool,
    isManualLayoutSetted: pt.bool,
    mediaType: pt.string,
    click: pt.func,
    id: pt.number,
    additionalInfo: pt.object,
    moneyToCollect: pt.number,
    rowShouldUpdate: pt.func,
    closeOutcome: pt.shape({
      isActive: pt.bool,
      callback: pt.func
    })
  }

  static defaultProps = {
    showOutcome: false,
    attemptsTotal: 0,
    nonBorder: false,
    button: {
      action: () => {},
      disabled: false,
      label: '',
      iconName: ''
    },
    crimeID: 0,
    subID: null,
    lastSubCrimeID: 0,
    currentRowID: 0,
    outcome: {},
    isRowActive: false,
    isCurrentRow: false,
    isCDSelected: false,
    spraySelected: '',
    genre: null,
    className: '',
    iconClass: '',
    nerve: 0,
    styles: '',
    itemId: 0,
    state: '',
    underTitle: false,
    dlmForTitle: true,
    title: '',
    subTitle: '',
    customImage: '',
    id: 0,
    additionalInfo: {},
    moneyToCollect: 0,
    isRowNotInProgress: false,
    mediaType: 'desktop',
    isManualLayoutSetted: false,
    click: () => {},
    getCurrentOutcome: () => {},
    rowShouldUpdate: () => {},
    closeOutcome: {
      isActive: false,
      callback: () => {}
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      inProgress: false,
      counter: 0,
      height: null
    }

    this.ref = React.createRef()
  }

  shouldComponentUpdate(nextProps, prevState) {
    const { inProgress, height, counter } = this.state
    const { rowShouldUpdate, mediaType } = this.props

    const isCrimeCheck = rowShouldUpdate(this.props, nextProps)
    const isGlobalCheck = globalCrimeRowCheck(this.props, nextProps)
    const isProgressCheck = inProgress !== prevState.inProgress
    const isHeightCheck = height !== prevState.height
    const isMediaCheck = mediaType !== prevState.mediaType
    const isCounterCheck = counter !== prevState.counter

    return isCrimeCheck || isGlobalCheck || isProgressCheck || isHeightCheck || isCounterCheck || isMediaCheck
  }

  componentDidUpdate(nextProps) {
    const { mediaType } = this.props

    // get node throw native method because react does recognize dom changes under the CSSTransition Animation logic.
    const isViewportChanged = mediaType !== nextProps.mediaType

    if (isViewportChanged) {
      this._calcHeight()
    }
  }

  _calcHeight = () => {
    const rowRef = this.ref && this.ref.current

    if (rowRef === null) {
      return
    }

    const newHeight = rowRef.lastChild?.firstChild?.firstChild?.firstChild?.scrollHeight || 0

    this.setState(prevState => ({
      ...prevState,
      height: newHeight
    }))
  }

  _isButtonDisabled = () => {
    const { inProgress } = this.state
    const { isRowActive } = this.props

    let buttonDisabled = false

    if (inProgress && isRowActive) {
      buttonDisabled = true
    }

    return buttonDisabled
  }

  _action = event => {
    event.preventDefault()
    const { button, isRowNotInProgress, isRowActive } = this.props

    if (button.disabled || isRowActive && !isRowNotInProgress) return

    button.action()
    this._countIncrementor()
    this._disableButton()
  }

  _countIncrementor = () => {
    this.setState(prevState => ({
      ...prevState,
      counter: prevState.counter + 1
    }))
  }

  _enableButton = () => {
    this.setState(prevState => ({
      ...prevState,
      inProgress: false
    }))
  }

  _disableButton = () => {
    this.setState({
      inProgress: true
    })
  }

  _isSameRow = () => {
    const { counter } = this.state
    const { currentRowID, subID, crimeID } = this.props
    const rowID = subID ? `${crimeID}_${subID}` : crimeID

    return counter >= 2 && currentRowID === rowID
  }

  _isRowCommitted = () => {
    const { isRowActive, outcome } = this.props

    return (isRowActive && !isEmpty(outcome)) || this._isSameRow()
  }

  _setHeight = height => this.setState({ height })

  _titleClassAdder = () => {
    const { styles, underTitle, dlmForTitle, nonBorder } = this.props

    return cn({
      [styles.title]: true,
      [styles.withUnderneath]: underTitle,
      [styles.dlm]: dlmForTitle,
      [styles.nonBorder]: nonBorder
    })
  }

  _throwOverOutcomeProps = () => {
    const { counter } = this.state
    const {
      outcome,
      crimeID,
      itemId,
      showOutcome,
      getCurrentOutcome,
      state,
      className,
      styles,
      isRowActive,
      isRowNotInProgress,
      mediaType,
      closeOutcome
    } = this.props

    return {
      state,
      itemId,
      crimeID,
      counter,
      outcome,
      className: styles[className],
      mediaType,
      isRowActive,
      showOutcome,
      isCurrentRow: this._isSameRow(),
      getCurrentOutcome,
      isRowNotInProgress,
      activeButton: this._enableButton,
      getOutcomeHeight: this._setHeight,
      closeOutcome
    }
  }

  _throwOverButtonProps = () => {
    const {
      crimeID,
      button,
      styles,
      mediaType,
      isManualLayoutSetted,
      isRowNotInProgress,
      isRowActive,
      outcome: { rarity, special }
    } = this.props

    const { inProgress } = this.state

    return {
      inProgress,
      crimeID,
      button,
      styles,
      special,
      outcomeRarity: rarity,
      isDisabled: isRowActive && !isRowNotInProgress,
      isRowActive,
      isDesktop: isManualLayoutSetted || (mediaType !== 'mobile' && mediaType !== 'tablet'),
      isManualDesktop: isManualLayoutSetted,
      mediaType,
      action: this._action
    }
  }

  _getImage = () => {
    const { customImage, iconClass, styles } = this.props
    const imageBCG = <i className={iconClass} />
    const imgToRender = customImage || imageBCG

    return <div className={styles['image']}>{imgToRender}</div>
  }

  _getTitle = () => {
    const { title, subTitle, styles } = this.props

    if (subTitle) {
      return (
        <div className={this._titleClassAdder()}>
          <span className={styles.titleText} dangerouslySetInnerHTML={{ __html: title }} />
          <span className={styles.descText} dangerouslySetInnerHTML={{ __html: subTitle }} />
        </div>
      )
    }

    return <div className={this._titleClassAdder()} dangerouslySetInnerHTML={{ __html: title }} />
  }

  _getCommitButton = () => {
    const { styles, nerve } = this.props

    return (
      <div className={styles.action}>
        <CrimeRowButton {...this._throwOverButtonProps()} nerve={nerve} />
      </div>
    )
  }

  _getOutcome = () => {
    const { isRowActive } = this.props

    return (
      <CSSTransition
        in={isRowActive}
        classNames='rowOutcome'
        timeout={{ enter: 0, exit: 350 }}
        onExit={() => this.setState({ counter: 0 })}
        unmountOnExit
      >
        <Outcome {...this._throwOverOutcomeProps()} />
      </CSSTransition>
    )
  }

  _getHeight = () => {
    const { height } = this.state
    const isHeightAllowed = isValue(height) && !isNaN(height)
    const rowHeight = isHeightAllowed ? `${parseInt(height, 10) + 50}px` : ''

    return rowHeight ? { height: rowHeight } : {}
  }

  render() {
    const { children, className, styles } = this.props

    // state values -> selected / locked / success / failure / jailed
    const rowClass = cn({
      [styles.crimeRow]: true,
      [styles[className]]: className
    })

    return (
      <div
        className={rowClass}
        ref={this.ref}
        style={this._isRowCommitted() ? this._getHeight() : {}}
      >
        <div className={localStyles.topSection}>
          {this._getImage()}
          {this._getTitle()}
          {children}
          {this._getCommitButton()}
        </div>
        <div className={localStyles.bottomSection}>{this._getOutcome()}</div>
      </div>
    )
  }
}

export default CrimeRow
