/* eslint-disable max-len */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import classnames from 'classnames'

import { onEntered, onEntering, onExit } from './OutcomeSlider'
import OutcomeContainer from './OutcomeContainer'
import styles from './outcome.cssmodule.scss'
import './anim.scss'

class Outcome extends Component {
  static propTypes = {
    state: PropTypes.string,
    isCurrentRow: PropTypes.bool,
    counter: PropTypes.number,
    activeButton: PropTypes.func,
    getCurrentOutcome: PropTypes.func,
    getOutcomeHeight: PropTypes.func,
    isRowNotInProgress: PropTypes.bool,
    outcome: PropTypes.object,
    showOutcome: PropTypes.bool,
    mediaType: PropTypes.string,
    closeOutcome: PropTypes.shape({
      isActive: PropTypes.bool,
      callback: PropTypes.func
    })
  }

  static defaultProps = {
    state: '',
    counter: 0,
    isCurrentRow: false,
    closeOutcome: {
      isActive: false,
      callback: () => {}
    },
    activeButton: () => {},
    getCurrentOutcome: () => {},
    getOutcomeHeight: () => {},
    isRowNotInProgress: false,
    outcome: {},
    showOutcome: false,
    mediaType: ''
  }

  constructor(props) {
    super(props)

    this.ref = React.createRef()

    this.state = {
      animPhase: null
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { animPhase } = this.state
    const { outcome, mediaType } = this.props

    const isOutcomeDataChanged = JSON.stringify(outcome) !== JSON.stringify(nextProps.outcome)
    const isMediaChanged = mediaType !== nextProps.mediaType
    const isFetchNotInProgress = nextProps.isRowNotInProgress
    const isAnimationInProgress = animPhase !== nextState.animPhase
    const isOutcomeChanged = (isOutcomeDataChanged || isMediaChanged) && isFetchNotInProgress

    return isOutcomeChanged || isAnimationInProgress
  }

  _getNativeOutcomeDOMContainer = () => document.body.querySelector('[class*="outcomeContainer"]')

  _getClassStatus = () => {
    const { state } = this.props

    let status = ''

    if (state === 'success') {
      status = 'success'
    } else if (state === 'failed') {
      status = 'failure'
    } else {
      status = 'jailed'
    }

    return styles[status]
  }

  _setAnimPhase = stage => {
    this.setState({
      animPhase: stage
    })
  }

  _isOutcomeToShow = () => {
    const { outcome, showOutcome } = this.props

    return outcome && outcome.story && showOutcome
  }

  _renderOutcome = () => {
    const {
      activeButton,
      closeOutcome,
      outcome,
      counter,
      isCurrentRow,
      getCurrentOutcome,
      getOutcomeHeight,
      mediaType
    } = this.props

    if (!this._isOutcomeToShow()) {
      return null
    }

    const animProps = {
      isCurrentRow,
      activeButton,
      counter,
      getCurrentOutcome,
      getOutcomeHeight,
      setAnimPhase: this._setAnimPhase,
      outcomeRef: this.ref
    }

    const closeBtn = {
      isActive: closeOutcome?.isActive,
      callback: closeOutcome?.callback,
      setHeight: getOutcomeHeight
    }

    return (
      <CSSTransition
        classNames={isCurrentRow ? 'currentRowOutcome' : 'newRowOutcome'}
        key={counter}
        timeout={{ enter: 500, exit: 500 }}
        onEntering={() => onEntering(animProps)}
        onEntered={() => onEntered(animProps)}
        onExit={() => onExit(animProps)}
        unmountOnExit
      >
        <div
          ref={this.ref}
          style={{ zIndex: counter }}
          className={`${styles.outcomeContainer} ${
            isCurrentRow ? styles.sameRowOutcome : ''
          } ${this._getClassStatus()}`}
        >
          <OutcomeContainer outcome={outcome} mediaType={mediaType} close={closeBtn} />
        </div>
      </CSSTransition>
    )
  }

  render() {
    const { isCurrentRow = false } = this.props

    const classes = classnames({
      [styles.outcome]: true
    })

    return (
      <TransitionGroup className={classes} data-set={isCurrentRow} component='div'>
        {this._renderOutcome()}
      </TransitionGroup>
    )
  }
}

export default Outcome
