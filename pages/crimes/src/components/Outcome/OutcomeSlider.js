const onEntering = props => {
  const { setAnimPhase, outcomeRef, getOutcomeHeight } = props

  const { height } = outcomeRef?.current?.firstChild?.getBoundingClientRect() || {}

  setAnimPhase('entering')
  getOutcomeHeight(height)
}

const onEntered = props => {
  const { activeButton, getCurrentOutcome, outcomeRef, setAnimPhase } = props
  const { current } = outcomeRef

  setAnimPhase('entered')
  activeButton()

  if (current === null) {
    return
  }

  const { height: outcomeHeight, top } = current.firstChild?.getBoundingClientRect() || {}
  const outcomeOffsetTop = top + pageYOffset

  getCurrentOutcome && getCurrentOutcome(outcomeHeight, outcomeOffsetTop)
}

const onExit = props => {
  const { setAnimPhase } = props

  setAnimPhase('exit')
}

export { onEntering, onEntered, onExit }
