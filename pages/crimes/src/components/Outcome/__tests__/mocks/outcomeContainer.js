const initialState = {
  status: 'sucess',
  mediaType: 'desktop',
  outcome: {
    ID: 0,
    outcomeDesc: 'Desc Test',
    result: 'success',
    image: null,
    rewardsGive: null,
    story: ['Story Test']
  }
}

export default initialState
