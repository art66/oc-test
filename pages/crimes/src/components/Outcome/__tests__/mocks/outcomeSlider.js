import React from 'react'

const onEnteringState = {
  getOutcomeHeight: () => {},
  setAnimPhase: () => {},
  outcomeRef: {
    current: <div>Test Node</div>
  }
}

const onEnteredState = {
  activeButton: () => {},
  getCurrentOutcome: () => {},
  setAnimPhase: () => {},
  outcomeRef: {
    current: null
  }
}

const onExitState = {
  setAnimPhase: () => {}
}

export { onEnteringState, onEnteredState, onExitState }
