import React from 'react'
import { shallow } from 'enzyme'
import OutcomeContainer from '../OutcomeContainer'
import initialState from './mocks/outcomeContainer'

// TODO Add new tests for covering close button along with all three rewards types layout
describe('<OutcomeContainer Global />', () => {
  it('should render basic OutcomeContainer structure', () => {
    const Component = shallow(<OutcomeContainer {...initialState} />)

    expect(Component.debug()).toMatchSnapshot()

    expect(Component.find('.outcomeWrap').length).toBe(1)
    expect(Component.find('.story')).toBeTruthy()
    expect(Component.find('.result')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  // TODO: solve the Enzyme problem corresponding
  // to https://github.com/airbnb/enzyme/issues/1533 isuue to run tests below

  // it('should render OutcomeContainer with rewards given', () => {
  //   const Component = shallow(<OutcomeContainer {...initialState} />)

  //   expect(Component.find('.outcomeWrap').length).toBe(1)
  //   expect(Component.find('.story')).toBe(1)
  //   expect(Component.find('.result')).toBe(1)

  //   expect(Component).toMatchSnapshot()
  // })
  // it('should render OutcomeContainer with iteam image given', () => {
  //   const Component = shallow(<OutcomeContainer {...initialState} />)

  //   expect(Component.find('.outcomeWrap').length).toBe(1)
  //   expect(Component.find('.story')).toBe(1)
  //   expect(Component.find('.result')).toBe(1)

  //   expect(Component).toMatchSnapshot()
  // })
})
