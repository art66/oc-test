import { onEnteredState, onEnteringState, onExitState } from './mocks/outcomeSlider'
import { onEntered, onEntering, onExit } from '../OutcomeSlider'

describe('Slider Outcome onEntered / onEntering callbacks', () => {
  it('should response OutcomeSlider', () => {
    expect(onEntering(onEnteringState)).toBeUndefined()
    expect(onEntered(onEnteredState)).toBeUndefined()
    expect(onExit(onExitState)).toBeUndefined()
  })
})
