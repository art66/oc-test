import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import SVGCloseBtn from '@torn/shared/SVG/icons/global/Close'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import styles from './outcome.cssmodule.scss'
import './ammoRewards.scss'

class OutcomeContainer extends PureComponent {
  static propTypes = {
    status: PropTypes.string,
    mediaType: PropTypes.string,
    close: PropTypes.shape({
      isActive: PropTypes.bool,
      callback: PropTypes.func,
      setHeight: PropTypes.func
    }),
    outcome: PropTypes.shape({
      ID: PropTypes.number,
      outcomeDesc: PropTypes.string,
      customDesc: PropTypes.string,
      result: PropTypes.string,
      image: PropTypes.string,
      rewardsGive: PropTypes.shape({
        userRewards: PropTypes.shape({
          money: PropTypes.number
        }),
        itemsRewards: PropTypes.objectOf(
          PropTypes.shape({
            count: PropTypes.number,
            label: PropTypes.string
          })
        ),
        ammoRewards: PropTypes.shape({
          count: PropTypes.number,
          image: PropTypes.string,
          title: PropTypes.string,
          catShort: PropTypes.string,
          titleShort: PropTypes.string
        })
      }),
      story: PropTypes.array
    })
  }

  static defaultProps = {
    status: '',
    mediaType: '',
    close: {
      isActive: false,
      callback: () => {},
      setHeight: () => {}
    },
    outcome: {
      ID: null,
      outcomeDesc: '',
      result: '',
      image: '',
      rewardsGive: {
        userRewards: {
          money: 0
        },
        itemsRewards: {
          1: {
            count: 0,
            label: ''
          }
        },
        ammoRewards: {
          count: 0,
          image: '',
          title: '',
          catShort: '',
          titleShort: ''
        }
      },
      story: []
    }
  }

  componentDidMount() {
    tooltipsSubscriber.render({
      tooltipsList: [
        {
          ID: 'closeOutcomeBtn',
          child: 'Collapse'
        }
      ],
      type: 'mount'
    })
  }

  _handleClose = () => {
    const { close: { setHeight, callback } = {} } = this.props

    if (!setHeight) {
      return
    }

    setHeight({ height: '' })
    callback && callback()
  }

  _renderCloseButton = () => {
    const { close: { isActive } = {} } = this.props

    if (!isActive) {
      return null
    }

    return (
      <div className={styles.closeWrap}>
        <button
          id='closeOutcomeBtn'
          type='button'
          className={styles.closeButton}
          onClick={this._handleClose}
        >
          <SVGIconGenerator
            iconName='Close'
            preset={{ type: 'crimes', subtype: 'PICKPOCKETING' }}
            iconsHolder={{ Close: SVGCloseBtn }}
            dimensions={{ viewbox: '-2 -2 30 30' }}
          />
        </button>
      </div>
    )
  }

  _storyText = () => {
    const {
      outcome: { story = [] },
      mediaType
    } = this.props

    const getTextLayout = () => {
      if (!story.length || story.length <= 0) {
        return ''
      }

      const desktopLayout = story.map((stories, i) => <p key={i} dangerouslySetInnerHTML={{ __html: stories }} />)
      const mobileLayout = story.map((stories, i) => (
        <span key={i} dangerouslySetInnerHTML={{ __html: `${stories} ` }} />
      ))

      if (mediaType !== 'desktop') {
        return mobileLayout
      }

      return desktopLayout
    }

    return getTextLayout()
  }

  _renderEntityRewards = () => {
    const { outcome: { rewardsGive } = {} } = this.props
    const { userRewards, itemsRewards, ammoRewards } = rewardsGive || {}

    if (!userRewards && !itemsRewards && !ammoRewards) {
      return null
    }

    // in case we receive complex reward data
    if (itemsRewards) {
      const isObject = typeof itemsRewards === 'object'

      const objectRewardItem = () => {
        return Object.keys(itemsRewards).map(key => {
          const { label, count } = itemsRewards[key]

          return (
            <div key={label} className={styles.itemsRewards}>
              <div className={styles.imageWrap} title={label}>
                <img alt='outcome_rewards' src={`/images/items/${key}/medium.png`} />
              </div>
              <div className={styles.count}>{count > 1 && <span>x{count}</span>}</div>
            </div>
          )
        })
      }

      return isObject ? objectRewardItem() : <span>Rewards given: {itemsRewards}</span>
    }

    if (ammoRewards) {
      const { titleShort, catShort, image, count } = ammoRewards

      return (
        <div key={titleShort} className={styles.ammoRewards}>
          <div className={`${styles.imageWrap} ${styles.ammoImageWrap}`}>
            <div className={`ammo-reward-image ${image}`} />
            <div className={styles.ammoInfoWrap}>
              <span className={`${styles.label} ${styles.ammoLabel}`}>{titleShort}</span>
              {count && <span className={`${styles.count} ${styles.ammoCount}`}>{catShort}x{count}</span> || null}
            </div>
          </div>
        </div>
      )
    }

    if (userRewards) {
      return (
        <div className={styles.moneyRewards}>
          <span>Money reward: {userRewards.money}</span>
        </div>
      )
    }

    return null
  }

  _getRewards = () => {
    return (
      <div className={styles.rewardsWrap}>
        {this._renderEntityRewards()}
      </div>
    )
  }

  _getMainText = () => {
    const {
      outcome: { outcomeDesc, result, rewardsGive } = {}
    } = this.props
    const { userRewards, itemsRewards, ammoRewards } = rewardsGive || {}

    const classesMainText = classnames({
      [styles.mainText]: true,
      [styles.noMainTextArrow]: !outcomeDesc && !userRewards && !itemsRewards && !ammoRewards
    })

    return <span className={classesMainText}>{result}</span>
  }

  _getDescription = () => {
    const {
      outcome: { result, outcomeDesc, customDesc } = {},
      mediaType
    } = this.props

    if (!outcomeDesc) return null

    const tooLongDescForMobileLayout = result?.length > 8 && outcomeDesc?.length > 8
    const cutTextFont = tooLongDescForMobileLayout && mediaType === 'mobile' ? styles.infoTextCutted : ''
    const specTextFont = outcomeDesc && !outcomeDesc.match(/\$/gi) ? styles.specResultFont : ''

    if (customDesc) {
      return (
        <span
          className={`${styles.infoText} ${cutTextFont} ${specTextFont}`}
          dangerouslySetInnerHTML={{ __html: outcomeDesc }}
        />
      )
    }

    return <span className={`${styles.infoText} ${cutTextFont} ${specTextFont}`}>{outcomeDesc}</span>
  }

  // DEV container! Should display only on dev-www.torn.com!
  _getOutcomeID = () => {
    const {
      outcome: { ID }
    } = this.props

    // this hack is made just for dev purposes and would not affect on Prod Server
    const isDevServer = window.location.href.match(/https?:\/\/(www.)?torn\.com/g)

    if (!ID || isDevServer) return null

    return <span className={styles.outcomeID}>{`#${ID}`}</span>
  }

  render() {
    const { status } = this.props

    return (
      <div className={`${styles.outcomeWrap} ${status}`}>
        {this._renderCloseButton()}
        <div className={styles.story}>{this._storyText()}</div>
        <div className={styles.result}>
          {this._getMainText()}
          {this._getDescription()}
          {this._getRewards()}
          {this._getOutcomeID()}
        </div>
      </div>
    )
  }
}

export default OutcomeContainer
