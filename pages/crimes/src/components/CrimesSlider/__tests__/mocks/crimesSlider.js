const initialState = {
  crimesTypes: [
    {
      name: 'searchforcash',
      crimeRoute: '/searchforcash'
    },
    {
      name: 'bootlegging',
      crimeRoute: '/bootlegging'
    },
    {
      name: 'graffiti',
      crimeRoute: '/graffiti'
    },
    {
      name: 'shoplifting',
      crimeRoute: '/shoplifting'
    }
  ],
  crimeIsNotReady: () => {},
  loadHubData: () => {},
  prevNextArrows: () => {},
  freezerSlider: () => {},
  location: {
    pathname: '/bootlegging'
  },
  showArrows: false
}

export const leftArrow = {
  ...initialState,
  location: {
    pathname: '/shoplifting'
  }
}

export const rightArrow = {
  ...initialState,
  location: {
    pathname: '/searchforcash'
  }
}

export const userJailed = {
  ...initialState,
  jailed: true
}

export default initialState
