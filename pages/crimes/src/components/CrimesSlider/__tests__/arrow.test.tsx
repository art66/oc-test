import React from 'react'
import { mount } from 'enzyme'

import Arrow from '../Arrow'

describe('<Arrow Crimes Slider />', () => {
  it('should render left arrow', async done => {
    const Component = mount(<Arrow arrowType='left' />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Arrow').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('className')).toBe('default bloomOnHover')
    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render right arrow', async done => {
    const Component = mount(<Arrow arrowType='right' />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Arrow').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('className')).toBe('default bloomOnHover')
    expect(Component).toMatchSnapshot()

    done()
  })
})
