import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter } from 'react-router-dom'
import { shape } from 'prop-types'
import initialState, { leftArrow, rightArrow, userJailed } from './mocks/crimesSlider'
import CrimesSlider from '../CrimesSlider'

// Instantiate routes context for react-router-dom components testing
const mainRouter = {
  history: new BrowserRouter().history,
  route: {
    location: {
      pathname: '/bootlegging'
    },
    match: {}
  }
}

const routerRight = {
  ...mainRouter,
  route: {
    ...mainRouter.route,
    location: {
      pathname: '/searchforcash'
    }
  }
}

const routerLeft = {
  ...mainRouter,
  route: {
    ...mainRouter.route,
    location: {
      pathname: '/shoplifting'
    }
  }
}

// Creating the context for testing Routes
const createContext = router => ({
  context: { router },
  childContextTypes: { router: shape({}) }
})

// Mount wrapped Component

describe('<CrimesSlider />', () => {
  it('should render basic Component with Bootlegging Crime and both Arrow (Left, Right) for navigation', () => {
    function mountWrap(node) {
      return mount(node, createContext(mainRouter))
    }

    const Component = mountWrap(
      <BrowserRouter>
        <CrimesSlider {...initialState}>
          <div>TEST</div>
        </CrimesSlider>
      </BrowserRouter>
    )

    expect(Component.find('.carouselWrap').length).toBe(1)
    expect(Component.find('Link').length).toBe(2)
    expect(Component.find('a').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render basic Component with SearchForCash Crime and only right Arrow (Right) for navigation', () => {
    function mountWrap(node) {
      return mount(node, createContext(routerRight))
    }

    const Component = mountWrap(
      <BrowserRouter>
        <CrimesSlider {...rightArrow}>
          <div>TEST</div>
        </CrimesSlider>
      </BrowserRouter>
    )

    expect(Component).toMatchSnapshot()
    expect(Component.find('.carouselWrap').length).toBe(1)
    expect(Component.find('Link').length).toBe(1)
    expect(Component.find('a').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render basic Component with Shoplifting Crime and only left Arrow (Right) for navigation', () => {
    function mountWrap(node) {
      return mount(node, createContext(routerLeft))
    }

    const Component = mountWrap(
      <BrowserRouter>
        <CrimesSlider {...leftArrow}>
          <div>TEST</div>
        </CrimesSlider>
      </BrowserRouter>
    )

    expect(Component.find('.carouselWrap').length).toBe(1)
    expect(Component.find('Link').length).toBe(1)
    expect(Component.find('a').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render Component without Arrows in case of Jailed user status', () => {
    function mountWrap(node) {
      return mount(node, createContext(routerLeft))
    }

    const Component = mountWrap(
      <CrimesSlider {...userJailed}>
        <div>TEST</div>
      </CrimesSlider>
    )

    expect(Component.find('.carouselWrap').length).toBe(1)
    expect(Component.find('Link').length).toBe(0)
    expect(Component.find('a').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
