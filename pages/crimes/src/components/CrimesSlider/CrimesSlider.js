import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'

import Arrow from './Arrow'
import { Editor } from '..'

import styles from './styles.cssmodule.scss'
import './anim.scss'

export class CrimesSlider extends PureComponent {
  static propTypes = {
    children: PropTypes.element.isRequired,
    crimeIsNotReady: PropTypes.func,
    crimesTypes: PropTypes.array,
    loadHubData: PropTypes.func,
    prevNextArrowsHover: PropTypes.func,
    prevNextArrowsClick: PropTypes.func,
    freezerSlider: PropTypes.func,
    currentArrow: PropTypes.string,
    location: PropTypes.object,
    showArrows: PropTypes.bool,
    crimeName: PropTypes.string,
    jailed: PropTypes.bool,
    hideTestPanel: PropTypes.bool,
    isSlidesChange: PropTypes.bool
  }

  static defaultProps = {
    crimesTypes: [],
    crimeIsNotReady: () => {},
    loadHubData: () => {},
    prevNextArrowsHover: () => {},
    prevNextArrowsClick: () => {},
    freezerSlider: () => {},
    currentArrow: '',
    location: {
      pathname: ''
    },
    showArrows: false,
    crimeName: '',
    jailed: false,
    hideTestPanel: false,
    isSlidesChange: false
  }

  componentDidMount() {
    const { crimesTypes, loadHubData } = this.props

    if (!crimesTypes || (crimesTypes && crimesTypes.length === 0)) {
      loadHubData()
    }
  }

  _handleMouseEnterPrev = () => {
    const { prevNextArrowsHover, currentArrow } = this.props

    if (currentArrow === 'prev') return

    prevNextArrowsHover('prev')
  }

  _handleMouseEnterNext = () => {
    const { prevNextArrowsHover, currentArrow } = this.props

    if (currentArrow === 'next') return

    prevNextArrowsHover('next')
  }

  _handleCLickPrev = () => {
    const { crimeIsNotReady, prevNextArrowsClick, freezerSlider } = this.props

    prevNextArrowsClick('prev')
    freezerSlider(true)
    crimeIsNotReady()
  }

  _handleCLickNext = () => {
    const { crimeIsNotReady, prevNextArrowsClick, freezerSlider } = this.props

    prevNextArrowsClick('next')
    freezerSlider(true)
    crimeIsNotReady()
  }

  _getPrevArrow = currentIndex => {
    const { crimesTypes, showArrows, jailed } = this.props
    // const { showArrow } = this.state

    const hideOrshowArrow = showArrows ? '' : styles.hideArrow

    if (jailed || currentIndex - 1 < 0) return null

    const arrowNavigatePrev = `/${crimesTypes[currentIndex - 1].crimeRoute}`

    return (
      <Link
        to={arrowNavigatePrev}
        className={hideOrshowArrow}
        onClick={this._handleCLickPrev}
        onMouseEnter={this._handleMouseEnterPrev}
        onTouchStart={this._handleMouseEnterPrev}
      >
        <Arrow arrowType='left' />
      </Link>
    )
  }

  _getNextArrow = currentIndex => {
    const { crimesTypes, showArrows, jailed } = this.props
    // const { showArrow } = this.state

    const hideOrshowArrow = showArrows ? '' : styles.hideArrow

    if (jailed || currentIndex + 1 >= crimesTypes.length) return null

    const arrowNavigateNext = `/${crimesTypes[currentIndex + 1].crimeRoute}`

    return (
      <Link
        to={arrowNavigateNext}
        className={hideOrshowArrow}
        onClick={this._handleCLickNext}
        onMouseEnter={this._handleMouseEnterNext}
        onTouchStart={this._handleMouseEnterNext}
      >
        <Arrow arrowType='right' />
      </Link>
    )
  }

  render() {
    const {
      children,
      crimesTypes,
      location: { pathname },
      crimeName,
      hideTestPanel,
      isSlidesChange
    } = this.props

    if (!children) return null

    const currentIndex = crimesTypes.findIndex(item => pathname.search(item.crimeRoute) !== -1)

    return (
      <div className={`${styles.carouselWrap} ${isSlidesChange ? styles.crimesInChange : ''}`}>
        {this._getPrevArrow(currentIndex)}
        {this._getNextArrow(currentIndex)}
        {children}
        {!hideTestPanel && crimeName && <Editor />}
      </div>
    )
  }
}

export default CrimesSlider
