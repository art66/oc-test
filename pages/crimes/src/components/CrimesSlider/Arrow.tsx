import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { IProps } from './types'
import styles from './arrow.cssmodule.scss'

const DIMENSIONS = {
  width: '100%',
  height: '100%',
  viewbox: '-4 -4 30 52'
}

const FILTER = {
  active: false
}

class Arrow extends React.PureComponent<IProps> {
  render() {
    const { arrowType = 'left' } = this.props

    return (
      <span className={`${styles.arrowWrap} ${styles[arrowType]}`}>
        <SVGIconGenerator
          iconName={'ArrowCrimes'}
          type={arrowType}
          customClass={styles.bloomOnHover}
          dimensions={DIMENSIONS}
          filter={FILTER}
        />
      </span>
    )
  }
}

export default Arrow
