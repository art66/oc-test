import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  loadHubData,
  crimeIsNotReady,
  crimeReady,
  prevNextArrowsHover,
  prevNextArrowsClick,
  freezerSlider
} from '../../modules/actions'
import CrimesSlider from './CrimesSlider'

const mapStateToProps = state => {
  return {
    crimesTypes: state.common.crimesTypes,
    crimeIsReady: state.common.crimeIsReady,
    loadedBundles: state.common.loadedBundles,
    nextPathname: state.common.nextPathname,
    showArrows: state.common.showArrows,
    currentType: state.common.currentType,
    crimeName: state.common.crimeName,
    dbg: state.common.dbg,
    infoBox: state.common.infoBox,
    currentArrow: state.common.currentArrow,
    freeze: state.common.freeze,
    jailed: state.common.jailed,
    hideTestPanel: state.common.hideTestPanel
  }
}

const mapActionCreators = dispatch => ({
  crimeIsNotReady: () => dispatch(crimeIsNotReady()),
  crimeIsReady: () => dispatch(crimeReady()),
  loadHubData: () => dispatch(loadHubData()),
  prevNextArrowsClick: currentArrow => dispatch(prevNextArrowsClick(currentArrow)),
  prevNextArrowsHover: currentArrow => dispatch(prevNextArrowsHover(currentArrow)),
  freezerSlider: payload => dispatch(freezerSlider(payload))
})

export default withRouter(
  connect(
    mapStateToProps,
    mapActionCreators
  )(CrimesSlider)
)
