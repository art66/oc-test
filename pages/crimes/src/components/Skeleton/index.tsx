import React from 'react'
import styles from './index.cssmodule.scss'

interface IProps {
  type: string
}

class Placeholder extends React.PureComponent<IProps> {
  _renderTopSection = () => {
    return (
      <div className={styles.topSection}>
        <div className={styles.header} />
        <div className={styles.banner} />
        <div className={styles.delimier} />
        <div className={styles.leftSquare} />
        <div className={styles.rightSquere} />
      </div>
    )
  }

  render() {
    const { children, type } = this.props

    if (type !== 'full') {
      return children
    }

    return (
      <div className={styles.placeholderWrap}>
        {this._renderTopSection()}
        <div className={styles.bottomSection}>{children}</div>
      </div>
    )
  }
}

export default Placeholder
