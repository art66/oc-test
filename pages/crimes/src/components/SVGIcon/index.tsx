import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { IProps } from './types'

class SVGIcon extends React.PureComponent<IProps> {
  render() {
    const { iconProps } = this.props

    return <SVGIconGenerator {...iconProps} />
  }
}

export default SVGIcon
