import React from 'react'
import { mount } from 'enzyme'
import initialState, { noIconName } from './mocks'
import SVGIcon from '..'

describe('<SVGIcon />', () => {
  it('should return CrimeRowIcons Component', async done => {
    const Component = mount(<SVGIcon iconProps={initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Trash')
    expect(Component.find('svg').length).toBe(1)
    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return Error if icon is not found', () => {
    const Component = mount(<SVGIcon iconProps={noIconName} />)

    expect(() => {
      throw new Error()
    }).toThrow()

    expect(Component).toMatchSnapshot()
  })
})
