import { createSelector } from 'reselect'
import { IProps } from './types/selectors'
import { FULL_STROKE, CIRCLE_RADIUS_DOUBLER, BASE_COLOR, SECURITY_COLOR_RED, FULL_WIDTH } from './constants'

const getRadius = (props: IProps) => props.radius
const getProgress = (props: IProps) => props.progress
const getStroke = (props: IProps) => props.stroke
const getSecurityStatus = (props: IProps) => props.disableSecurity
const getProgressID = (props: IProps) => props.progressID
const getAnimateStatus = (props: IProps) => props.isAnimate

const calcCircleDimensions = createSelector(
  [getRadius],
  radius => {
    const circleDimensions = radius * CIRCLE_RADIUS_DOUBLER

    return circleDimensions
  }
)

const calcNormalizedRadius = createSelector(
  [getRadius, getStroke],
  (radius, stroke) => {
    const normalizedRadius = radius - stroke * CIRCLE_RADIUS_DOUBLER

    return normalizedRadius
  }
)

const calcCircumference = createSelector(
  [calcNormalizedRadius],
  normalizedRadius => {
    const circumference = normalizedRadius * CIRCLE_RADIUS_DOUBLER * Math.PI

    return circumference
  }
)

const calcSecurityStrokeDashOffset = createSelector(
  [calcCircumference, getProgress],
  circumFerence => {
    const strokeDashoffset = circumFerence - (FULL_STROKE / FULL_WIDTH) * circumFerence

    return strokeDashoffset
  }
)

const calcBaseStrokeDashOffset = createSelector(
  [calcCircumference, getProgress],
  (circumFerence, progress) => {
    const strokeDashoffset = circumFerence - (progress / FULL_WIDTH) * circumFerence

    return strokeDashoffset
  }
)

const calcCircleColor = createSelector(
  [getSecurityStatus, getProgressID],
  (disableSecurity, progressID) => {
    const circleColor = disableSecurity
      ? SECURITY_COLOR_RED
      : `url(#${BASE_COLOR}${progressID ? `_${progressID}` : ''})`

    return circleColor
  }
)

const checkAnimate = createSelector(
  [getAnimateStatus],
  isAnimate => isAnimate || false
)

const progressCircleOwnProps = props => ({
  circleDimensions: calcCircleDimensions(props),
  normalizedRadius: calcNormalizedRadius(props),
  circumFerence: calcCircumference(props),
  baseStrokeDashOffset: calcBaseStrokeDashOffset(props),
  securityStrokeDashOffset: calcSecurityStrokeDashOffset(props),
  circleColor: calcCircleColor(props),
  isAnimate: checkAnimate(props)
})

export default progressCircleOwnProps
