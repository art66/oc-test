export interface IProps {
  stroke: number
  radius: number
  progress: number
  disableSecurity: boolean
  progressID?: number
  isAnimate?: boolean
  forceEmpty?: boolean
  startValue?: number
}

export interface ICheckMethods {
  _svgDefs(): JSX.Element
  _svgDisableLine(): JSX.Element
  _svgCircleMaker(strokeColor: string, strokeDashOffset: number, isAnimation?: boolean): JSX.Element
  _svgBaseCircle(): JSX.Element
  _svgSecurityCircle(): JSX.Element
}

export interface IBaseCircle {
  isAnimate: boolean
  circleColor: string
  baseStrokeDashOffset: number
}

export interface ISecurityCircle {
  securityStrokeDashOffset: number
}

export interface ICircleDimensions {
  circleDimensions: number
}

export interface ICircleMaker {
  circumFerence: number
  normalizedRadius: number
}

export interface IDefaultProps extends IProps {}
