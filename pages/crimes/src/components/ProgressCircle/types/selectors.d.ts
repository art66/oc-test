export interface IProps {
  radius: number
  progress: number
  stroke: number
  disableSecurity: boolean
  progressID?: number
  isAnimate?: boolean
}
