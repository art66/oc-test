import { IProps } from '../../types/selectors'

const initialState: IProps = {
  radius: 21,
  progress: 80,
  stroke: 3,
  disableSecurity: false,
  isAnimate: true
}

const redColor: IProps = {
  ...initialState,
  disableSecurity: true
}

export { redColor }
export default initialState
