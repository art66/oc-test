import { IProps } from '../../types/progressCircle'

const initialState: IProps = {
  radius: 20,
  stroke: 3,
  progress: 50,
  disableSecurity: true
}

const statusON: IProps = {
  radius: 20,
  stroke: 3,
  progress: 50,
  disableSecurity: true
}

const statusOFF: IProps = {
  radius: 20,
  stroke: 3,
  progress: 50,
  disableSecurity: false
}

const statusONAnim: IProps = {
  ...statusON,
  isAnimate: true
}

export { statusON, statusOFF, statusONAnim }
export default initialState
