import React from 'react'
import { mount } from 'enzyme'
import ProgressCircle from '..'
import initialState, { statusON, statusOFF, statusONAnim } from './mocks/progressCircle'

describe('<ProgressCircle />', () => {
  it('should render the ProgressCircle svg icon', () => {
    const Component = mount(<ProgressCircle {...initialState} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('defs').length).toBe(1)
    expect(Component.find('clipPath').length).toBe(1)
    expect(Component.find('linearGradient').length).toBe(1)
    expect(Component.find('circle').length).toBe(2)
    expect(Component.find('line').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })

  it('should render icon without red line and grey circle', () => {
    const Component = mount(<ProgressCircle {...statusOFF} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('defs').length).toBe(1)
    expect(Component.find('clipPath').length).toBe(1)
    expect(Component.find('linearGradient').length).toBe(1)
    expect(Component.find('circle').length).toBe(2)
    expect(
      Component.find('circle')
        .first()
        .prop('stroke')
    ).toEqual('#E1E1D4')
    expect(Component.find('line').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render icon with red line and red circle', () => {
    const Component = mount(<ProgressCircle {...statusON} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('defs').length).toBe(1)
    expect(Component.find('clipPath').length).toBe(1)
    expect(Component.find('linearGradient').length).toBe(1)
    expect(Component.find('circle').length).toBe(2)
    expect(
      Component.find('circle')
        .at(1)
        .prop('stroke')
    ).toEqual('#DB5E34')
    expect(Component.find('line').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render icon with red line and red circle and animation', () => {
    const Component = mount(<ProgressCircle {...statusONAnim} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('defs').length).toBe(1)
    expect(Component.find('clipPath').length).toBe(1)
    expect(Component.find('linearGradient').length).toBe(1)
    expect(Component.find('circle').length).toBe(2)
    expect(Component.find('circle').at(1).prop('style')).toEqual({ transition: 'all 1s linear' })
    expect(Component.find('circle').at(1).prop('strokeDasharray')).toBe('87.96459430051421 87.96459430051421')
    expect(Component.find('line').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
})
