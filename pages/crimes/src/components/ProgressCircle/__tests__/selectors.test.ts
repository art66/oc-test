import progressCircleOwnProps from '../selectors'
import initialState, { redColor } from './mocks/selectors'

describe('ProgressCircle selector', () => {
  it('should return circleDimensions value', () => {
    const { circleDimensions } = progressCircleOwnProps(initialState)

    expect(circleDimensions).toBe(42)
  })
  it('should return normalizedRadius value', () => {
    const { normalizedRadius } = progressCircleOwnProps(initialState)

    expect(normalizedRadius).toBe(15)
  })
  it('should return circumFerence value', () => {
    const { circumFerence } = progressCircleOwnProps(initialState)

    expect(circumFerence).toBe(94.24777960769379)
  })
  it('should return baseStrokeDashOffset value', () => {
    const { baseStrokeDashOffset } = progressCircleOwnProps(initialState)

    expect(baseStrokeDashOffset).toBe(18.84955592153875)
  })
  it('should return securityStrokeDashOffset value', () => {
    const { securityStrokeDashOffset } = progressCircleOwnProps(initialState)

    expect(securityStrokeDashOffset).toBe(0)
  })
  it('should return red circleColor value', () => {
    const { circleColor } = progressCircleOwnProps(redColor)

    expect(circleColor).toBe('#DB5E34')
  })
  it('should return base circleColor value', () => {
    const { circleColor } = progressCircleOwnProps(initialState)

    expect(circleColor).toBe('url(#gradient)')
  })
  it('should return base isAnimate value', () => {
    const { isAnimate } = progressCircleOwnProps(initialState)

    expect(isAnimate).toBeTruthy()
  })
})
