// import PropTypes from 'prop-types'
import React, {PureComponent} from 'react'
import progressCircleOwnProps from './selectors'
import {
  CIRCLE_FILL,
  COORD_X1,
  COORD_X2,
  COORD_Y1,
  COORD_Y2,
  FINISH_GRADIENT_COLOR,
  FULL_CIRCLE_DIM,
  RED_LINE_COLOR,
  SECURITY_COLOR,
  START_GRADIENT_COLOR,
  SVG_CLIP_ID,
  SVG_COORD_X,
  SVG_COORD_Y,
  SVG_DEFS_HEIGHT,
  SVG_DEFS_WIDTH,
  SVG_GRADIENT_ID,
  SVG_OFFSET_FINISH,
  SVG_OFFSET_START,
  SVG_OPACITY
} from './constants'
import {
  IBaseCircle,
  ICheckMethods,
  ICircleDimensions,
  ICircleMaker,
  IDefaultProps,
  IProps,
  ISecurityCircle
} from './types/progressCircle'

// import styles from './index.cssmodule.scss'

class ProgressCircle extends PureComponent<IProps> implements ICheckMethods {
  // public _animationID: any
  // private _circleRef: React.LegacyRef<SVGCircleElement>
  // private _updateTimerID: any

  static defaultProps: IDefaultProps = {
    radius: 21,
    stroke: 2,
    progress: 0,
    disableSecurity: false,
    progressID: null
  }

  // constructor(props: IProps) {
  //   super(props)

  //   this.state = {
  //     startValue: 0,
  //     progress: 0
  //   }

  //   this._circleRef = React.createRef()
  // }

  // componentDidMount() {
  //   const { startValue, progress } = this.props

  //   this.setState({
  //     startValue,
  //     progress
  //   })

  //   // this._runAnimationFrame()

  //   window.addEventListener('focus', this._updateProgress)
  // }

  // componentWillUnmount() {
  //   window.removeEventListener('focus', this._updateProgress)
  // }

  // _updateProgress = () => {
  //   const { progress: newProgress } = this.props

  //   if (this._updateTimerID) {
  //     clearInterval(this._updateTimerID)
  //   }

  //   this.setState({
  //     progress: newProgress
  //   })

  //   this._updateTimerID = setInterval(() => {
  //     const { startValue } = this.state

  //     this.setState({
  //       progress: startValue
  //     })
  //   }, 50)
  // }

  // _updateStartValue = () => {
  //   const { startValue } = this.state
  //   const { progress } = this.props

  //   if (startValue === progress) {
  //     return
  //   }

  //   this.setState({
  //     startValue: progress
  //   })
  // }

  // // solution for animation update while tab is changed and the outer timer is stopped
  // _runAnimationFrame = () => {
  //     // @ts-ignore
  //     const { current } = this._circleRef

  //     const animTimeLoopInterval = 60
  //     const fullCircleDim = 106.8
  //     // const fpsInterval = 1000 / animTimeLoopInterval
  //     // let timeStart = Date.now()

  //     const step = () => {
  //       // const timeNow = Date.now()

  //       // console.log(timeNow, timeStart, timeStart - fpsInterval, timeStart - fpsInterval <= timeStart)
  //       if (current.style.strokeDashoffset >= fullCircleDim) {
  //         return
  //       }

  //       // if (timeNow - fpsInterval > timeStart) {
  //         const { startValue, progress } = this.state

  //         const pieceValueByTimeProvided = fullCircleDim / (startValue * animTimeLoopInterval)

  //         const baseValue = current.style.strokeDashoffset
  //         const newValue = Number(baseValue) + pieceValueByTimeProvided

  //         current.style.strokeDashoffset = String(newValue)

  //         if (startValue - progress > 0) {
  //           const timeGapBetweenTimestamps = startValue - progress

  //           const onRunningBeforeUpdateCurrentValueShouldBe = pieceValueByTimeProvided * (timeGapBetweenTimestamps * animTimeLoopInterval)

  //           if (Number(onRunningBeforeUpdateCurrentValueShouldBe) > Number(current.style.strokeDashoffset)) {
  //             const getAnimProgressDiff = Number(onRunningBeforeUpdateCurrentValueShouldBe) - Number(current.style.strokeDashoffset)

  //             console.log(pieceValueByTimeProvided.toFixed(2), startValue, progress, 'startValue, progress')
  //             console.log(onRunningBeforeUpdateCurrentValueShouldBe, current.style.strokeDashoffset, getAnimProgressDiff, 'onRunningBeforeUpdateCurrentValueShouldBe')

  //             current.style.strokeDashoffset = String(Number(current.style.strokeDashoffset) + getAnimProgressDiff)
  //           }
  //         }

  //         // timeStart = timeNow
  //       // }

  //       window.requestAnimationFrame(step)
  //     }

  //     window.requestAnimationFrame(step)
  // }

  _svgDefs = () => {
    const { progressID } = this.props

    const clipPathID = progressID ? `${SVG_CLIP_ID}_${progressID}` : SVG_CLIP_ID
    const gradientID = progressID ? `${SVG_GRADIENT_ID}_${progressID}` : SVG_GRADIENT_ID

    return (
      <defs>
        <clipPath id={clipPathID}>
          <rect x={SVG_COORD_X} y={SVG_COORD_Y} width={SVG_DEFS_WIDTH} height={SVG_DEFS_HEIGHT} />
        </clipPath>
        <linearGradient id={gradientID}>
          <stop offset={SVG_OFFSET_START} stopColor={START_GRADIENT_COLOR} />
          <stop offset={SVG_OFFSET_FINISH} stopColor={FINISH_GRADIENT_COLOR} stopOpacity={SVG_OPACITY} />
        </linearGradient>
      </defs>
    )
  }

  _svgDisableLine = () => {
    const { stroke, disableSecurity } = this.props

    if (!disableSecurity) return null

    return <line x1={COORD_X1} y1={COORD_Y1} x2={COORD_X2} y2={COORD_Y2} stroke={RED_LINE_COLOR} strokeWidth={stroke} />
  }

  _svgCircleMaker = (strokeColor, strokeDashOffset, isAnimation?: boolean) => {
    const { radius, stroke, forceEmpty } = this.props
    const { circumFerence, normalizedRadius }: ICircleMaker = progressCircleOwnProps(this.props)
    const strokeDashArray = `${circumFerence} ${circumFerence}`

    // const animation = (
    //   <animate attributeName='stroke-dashoffset' from='0' to='106.8' dur={`${startValue}s`} repeatCount='1' />
    // )

    const animationConfig = isAnimation ? {
      transition: 'all 1s linear'
    } : {}

    return (
      <circle
        stroke={strokeColor}
        fill={CIRCLE_FILL}
        strokeWidth={stroke}
        strokeDasharray={strokeDashArray}
        strokeDashoffset={isAnimation && forceEmpty ? FULL_CIRCLE_DIM : strokeDashOffset}
        r={normalizedRadius}
        cx={radius}
        cy={radius}
        style={{ ...animationConfig }}
      />
    )
  }

  _svgSecurityCircle = () => {
    const { circleColor, baseStrokeDashOffset, isAnimate = false }: IBaseCircle = progressCircleOwnProps(this.props)

    return this._svgCircleMaker(circleColor, baseStrokeDashOffset, isAnimate)
  }

  _svgBaseCircle = () => {
    const { securityStrokeDashOffset }: ISecurityCircle = progressCircleOwnProps(this.props)

    const securityCircle = this._svgCircleMaker(SECURITY_COLOR, securityStrokeDashOffset)

    return securityCircle
  }

  render() {
    const { circleDimensions }: ICircleDimensions = progressCircleOwnProps(this.props)

    return (
      <svg
        height={circleDimensions}
        width={circleDimensions}
        // className={styles.progressCircle}
      >
        {this._svgDefs()}
        {this._svgBaseCircle()}
        {this._svgSecurityCircle()}
        {this._svgDisableLine()}
      </svg>
    )
  }
}

export default ProgressCircle
