// -------------------------------------
// LAYOUT STYLE SECURITY CIRCLE OPTIONS
// -------------------------------------
export const START_GRADIENT_COLOR = '#C0C0B1'
export const FINISH_GRADIENT_COLOR = '#8E8E7F'
export const BASE_COLOR = 'gradient'
export const SECURITY_COLOR = '#E1E1D4'
export const SECURITY_COLOR_RED = '#DB5E34'
export const RED_LINE_COLOR = '#DB5E34'
export const CIRCLE_RADIUS_DOUBLER = 2
export const FULL_STROKE = 100
export const REVERSE_CIRLCE_DECREES = 106.8
export const FULL_CIRCLE_DIM = REVERSE_CIRLCE_DECREES
export const FULL_WIDTH = 100
export const COORD_X1 = 9.85
export const COORD_Y1 = 32.5
export const COORD_X2 = 32.5
export const COORD_Y2 = 9.85
export const SVG_DEFS_WIDTH = 100
export const SVG_DEFS_HEIGHT = 100
export const SVG_CLIP_ID = 'cut-off'
export const SVG_GRADIENT_ID = 'gradient'
export const SVG_COORD_X = 0
export const SVG_COORD_Y = 50
export const SVG_OFFSET_START = 0
export const SVG_OFFSET_FINISH = '100%'
export const SVG_OPACITY = '100%'
export const CIRCLE_FILL = 'transparent'
