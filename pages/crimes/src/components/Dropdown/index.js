/*
  a wrapper for react-select with custom styles
*/
import React, { PureComponent } from 'react'
import Select from 'react-select'
import './styles.scss'

class Dropdown extends PureComponent {
  render() {
    return <Select clearable={false} {...this.props} />
  }
}

export default Dropdown
