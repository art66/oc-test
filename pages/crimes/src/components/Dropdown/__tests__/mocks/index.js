const initialState = {
  name: 'selected-state',
  onChange: () => {},
  options: [
    {
      label: 'Pop',
      value: '1'
    },
    {
      label: 'Rock',
      value: '2'
    },
    {
      label: 'Urban',
      value: '3'
    },
    {
      label: 'Country',
      value: '4'
    },
    {
      label: 'Classical',
      value: '5'
    },
    {
      label: 'Dance',
      value: '6'
    },
    {
      label: 'Rap',
      value: '7'
    },
    {
      label: 'Jazz',
      value: '8'
    }
  ],
  placeholder: 'Pop',
  simpleValue: true,
  value: '3'
}

export default initialState
