import React from 'react'
import { mount } from 'enzyme'
import initialState from './mocks'
import DropDown from '..'

describe('<DropDown />', () => {
  it('should render DropDown component with Pop music genre selected and value === 3', () => {
    const Component = mount(<DropDown {...initialState} />)

    expect(Component.find('Select').length).toBe(1)
    expect(Component.find('Value').length).toBe(1)
    expect(Component.find('Value').props('value').value).toEqual(initialState.options[2])
    expect(Component.find('.Select-value-label').props('text').children).toBe('Urban')
    expect(Component).toMatchSnapshot()
  })
})
