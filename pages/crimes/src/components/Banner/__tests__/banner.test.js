import React from 'react'
import { mount } from 'enzyme'
import initialState, {
  nextBanner,
  onePropChanged,
  eventClicked,
  mouseDownMock,
  mouseUpMock,
  mouseUpWrongMock
} from './mocks'
import Banner from '..'

describe('<Banner Global />', () => {
  it('should render basic Banner with children Banner included', () => {
    const Component = mount(<Banner {...initialState} />)

    expect(Component.find('.bannerWrap').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render basic Banner with children Banner & custom className included', () => {
    const Component = mount(<Banner {...initialState} />)

    expect(Component.find('.bannerWrap.searchforcash').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should update Banner if children Banner is changed', () => {
    const Component = mount(<Banner {...initialState} />)

    expect(Component.find('.bannerWrap.searchforcash').length).toBe(1)
    expect(Component.find('.test-mock-children').length).toBe(1)
    expect(Component).toMatchSnapshot()

    Component.setProps(nextBanner)

    expect(Component.find('.bannerWrap.searchforcash').length).toBe(1)
    expect(Component.find('.next-test-mock-children').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should not update Banner if children Banner is not changed', () => {
    const Component = mount(<Banner {...initialState} />)

    expect(Component.find('.bannerWrap.searchforcash').length).toBe(1)
    expect(Component).toMatchSnapshot()

    Component.setProps(onePropChanged)

    expect(Component.find('.bannerWrap.searchforcash').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should close DropDownPanel in case if click is made right', () => {
    const Component = mount(<Banner {...initialState} />)

    Component.find('.bannerWrap').simulate('mousedown', eventClicked)

    Component.setState({
      coordsStart: {
        X: 100,
        Y: 200
      }
    })

    expect(Component.state()).toEqual(mouseDownMock)
    Component.find('.bannerWrap').simulate('mouseup', eventClicked)
    const isCoordsSame = JSON.stringify(mouseDownMock) === JSON.stringify(mouseUpMock)

    expect(isCoordsSame).toBeTruthy()
  })
  it('should not to close DropDownPanel in case if click is have different coords on Down and Up phases', () => {
    const Component = mount(<Banner {...initialState} />)

    Component.find('.bannerWrap').simulate('mousedown', eventClicked)

    Component.setState({
      coordsStart: {
        X: 100,
        Y: 200
      }
    })

    Component.find('.bannerWrap').simulate('mouseup', eventClicked)
    const isCoordsSame = JSON.stringify(Component.state()) === JSON.stringify(mouseUpWrongMock)

    expect(isCoordsSame).toBeFalsy()
  })
})
