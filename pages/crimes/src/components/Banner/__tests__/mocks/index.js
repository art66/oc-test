import React from 'react'

const childNode = <div className='test-mock-children'>Time to test!</div>

const nextChildNode = <div className='next-test-mock-children'>Next time to test!</div>

const initialState = {
  dropDownPanelToShow: () => {},
  arrowToHide: () => {},
  children: childNode,
  customClass: 'searchforcash'
}

export const nextBanner = {
  ...initialState,
  children: nextChildNode
}

export const onePropChanged = {
  ...initialState,
  customClass: 'bootlegging'
}

export const eventClicked = {
  clientX: 100,
  clientY: 200
}

export const mouseDownMock = {
  coordsStart: {
    X: 100,
    Y: 200
  }
}

export const mouseUpMock = {
  coordsStart: {
    X: 100,
    Y: 200
  }
}

export const mouseUpWrongMock = {
  coordsStart: {
    X: 200,
    Y: 200
  }
}

export default initialState
