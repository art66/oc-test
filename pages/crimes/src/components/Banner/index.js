import PropTypes from 'prop-types'
import React, { Component } from 'react'

import './index.cssmodule.scss'

export class Banners extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    dropDownPanelToShow: PropTypes.func,
    toggleArrows: PropTypes.func,
    customClass: PropTypes.string
  }

  static defaultProps = {
    dropDownPanelToShow: () => {},
    toggleArrows: () => {},
    customClass: ''
  }

  constructor(props) {
    super(props)

    this.state = {
      coordsStart: null
    }
  }

  shouldComponentUpdate(nextProps) {
    const { children } = this.props

    if (children === nextProps.children) {
      return false
    }

    return true
  }

  _expandPanel = coordsFinish => {
    const { coordsStart } = this.state
    const { toggleArrows, dropDownPanelToShow } = this.props

    const isCoordsSame = JSON.stringify(coordsStart) === JSON.stringify(coordsFinish)

    if (!isCoordsSame) return

    dropDownPanelToShow()
    toggleArrows()
  }

  _handleDown = e => {
    e.persist()

    this.setState(prevState => ({
      ...prevState,
      coordsStart: {
        X: e.clientX,
        Y: e.clientY
      }
    }))
  }

  _handleUp = e => {
    const coordsFinish = {
      X: e.clientX,
      Y: e.clientY
    }

    this._expandPanel(coordsFinish, e.target)
  }

  render() {
    const { children, customClass } = this.props

    return (
      <div
        role='button'
        tabIndex={0}
        className={`bannerWrap ${customClass}`}
        onMouseDown={this._handleDown}
        onMouseUp={this._handleUp}
      >
        {children}
      </div>
    )
  }
}

export default Banners
