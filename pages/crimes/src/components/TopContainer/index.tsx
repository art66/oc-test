import React from 'react'

import PanelTitle from '../PanelTitle'
import InfoPanel from '../InfoPanel'
import ExperienceBar from '../ExperienceBar'

export interface IProps {
  title: any
  stats: any
  current: any
  progress: any
  children: any
}

class TopContainer extends React.Component<IProps> {
  render() {
    const { title, stats, current, progress, children: BannerComponent } = this.props

    return (
      <div className='top-container'>
        <PanelTitle title={title} stats={stats} />
        {stats && <InfoPanel panelItems={stats} />}
        {BannerComponent}
        <ExperienceBar current={current} progress={progress} />
      </div>
    )
  }
}

export default TopContainer
