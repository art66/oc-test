const initialState = {
  title: 'TEST',
  showTooltip: true,
  parent: {
    active: true,
    ID: 'expTooltip'
  }
}

export const disableTooltip = {
  ...initialState,
  showTooltip: false
}

export default initialState
