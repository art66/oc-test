import PropTypes from 'prop-types'
import React, { Component } from 'react'
import s from './styles.cssmodule.scss'

const HEADER_HEIGHT = 235
const POPUP_CLASSNAME = 'ui-tooltip ui-widget ui-corner-all ui-widget-content white-tooltip'

class Popup extends Component {
  constructor(props) {
    super(props)

    this.ref = React.createRef()
  }

  componentDidMount() {
    this.ref.current.focus()
  }

  _handleBlurClose = () => {
    const { closePopUp } = this.props

    closePopUp()
  }

  _getCoords = () => {
    const {
      outcomeHeight,
      outcomeOffestTop,
      pos: { top, left }
    } = this.props
    let topCoord = 0

    if (top + HEADER_HEIGHT >= outcomeOffestTop) {
      topCoord = top + outcomeHeight
    } else {
      topCoord = top
    }

    return {
      topCoord,
      left
    }
  }

  render() {
    const { children } = this.props

    const { topCoord, left } = this._getCoords()

    const styleConfig = {
      left: `${left}px`,
      top: `${topCoord}px`,
      position: 'absolute'
    }

    return (
      <div
        role='button'
        tabIndex='0'
        style={styleConfig}
        className={`${s.popup} ${POPUP_CLASSNAME}`}
        onBlur={this._handleBlurClose}
        ref={this.ref}
      >
        {children}
      </div>
    )
  }
}

Popup.propTypes = {
  children: PropTypes.node.isRequired,
  closePopUp: PropTypes.func,
  pos: PropTypes.object,
  outcomeHeight: PropTypes.number,
  outcomeOffestTop: PropTypes.number
}

Popup.defaultProps = {
  pos: {},
  outcomeHeight: 0,
  outcomeOffestTop: 0,
  closePopUp: () => {}
}

export default Popup
