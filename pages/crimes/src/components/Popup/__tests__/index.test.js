import React from 'react'
import { mount } from 'enzyme'
import initialState, { belowOutcome } from './mocks'
import Popup from '..'

describe('<Popup />', () => {
  it('should render regular Popup Component with on Outcome component or without it', () => {
    const Component = mount(<Popup {...initialState} />)

    expect(Component.find('Popup').length).toBe(1)
    expect(Component.find('.popup').length).toBe(1)
    expect(Component.find('.popup').prop('style').top).toBe('160px')

    expect(Component).toMatchSnapshot()
  })
  it('should render regular Popup Component below Outcome component', () => {
    const Component = mount(<Popup {...belowOutcome} />)

    expect(Component.find('Popup').length).toBe(1)
    expect(Component.find('.popup').length).toBe(1)
    expect(Component.find('.popup').prop('style').top).toBe('100px')

    expect(Component).toMatchSnapshot()
  })
  it('should fire onBlur function on Popup Component', () => {
    const Component = mount(<Popup {...initialState} />)

    expect(Component.find('Popup').length).toBe(1)
    expect(Component.find('.popup').length).toBe(1)
    Component.find('.popup').simulate('blur')
    expect(Component.find('Popup').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
