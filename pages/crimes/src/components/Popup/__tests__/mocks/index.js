import React from 'react'

const child = <div>Test child</div>

const initialState = {
  children: child,
  pos: {
    top: 100,
    left: 431
  },
  outcomeHeight: 60,
  outcomeOffestTop: 60,
  closePopUp: () => {}
}

export const belowOutcome = {
  ...initialState,
  outcomeOffestTop: 760
}

export default initialState
