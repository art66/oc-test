const initialState = {
  browser: {
    mediaType: 'desktop'
  },
  panelItems: [
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    },
    {
      label: 'Level & EXP',
      value: '95 (81%)'
    }
  ]
}

export const desktopLayout = {
  ...initialState,
  browser: {
    mediaType: 'desktop'
  }
}

export const tabletLayout = {
  ...initialState,
  browser: {
    mediaType: 'tablet'
  }
}

export const mobileLayout = {
  ...initialState,
  browser: {
    mediaType: 'mobile'
  }
}

export default initialState
