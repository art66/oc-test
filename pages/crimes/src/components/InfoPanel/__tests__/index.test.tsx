import React from 'react'
import { shallow } from 'enzyme'
import initialState, { desktopLayout, tabletLayout, mobileLayout } from './mocks'
import { InfoPanel } from '..'

describe('<InfoPanel />', () => {
  it('should render InfoPanel component', () => {
    const Component = shallow(<InfoPanel {...initialState} />)

    expect(Component.find('SlickSlider').length).toBe(1)
    expect(Component.find('SlickSlider').prop('slidesToShow')).toBe(4)
    expect(Component.find('ul').length).toBe(5)
    expect(Component.find('li').length).toBe(23)
    expect(Component).toMatchSnapshot()
  })
  it('should render InfoPanel in desktop layout', () => {
    const Component = shallow(<InfoPanel {...desktopLayout} />)

    expect(Component.find('SlickSlider').length).toBe(1)
    expect(Component.find('SlickSlider').prop('slidesToShow')).toBe(4)
    expect(Component).toMatchSnapshot()
  })
  it('should render InfoPanel in tablet layout', () => {
    const Component = shallow(<InfoPanel {...tabletLayout} />)

    expect(Component.find('SlickSlider').length).toBe(1)
    expect(Component.find('SlickSlider').prop('slidesToShow')).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render InfoPanel in mobile layout', () => {
    const Component = shallow(<InfoPanel {...mobileLayout} />)

    expect(Component.find('SlickSlider').length).toBe(1)
    expect(Component.find('SlickSlider').prop('slidesToShow')).toBe(2)
    expect(Component).toMatchSnapshot()
  })
})
