import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import isEmpty from '@torn/shared/utils/isEmpty'

import DropDownPanel from '../DropDownPanel'
import SlickSlider from '../SlickSlider'
import styles from './styles.cssmodule.scss'

const ITEMS_PER_COLUMN = 5
const COUNT_INCREMENT = 1
const FIXER = 1
const LAYOUTS = {
  desktop: 4,
  tablet: 2,
  mobile: 2
}

export class InfoPanel extends React.PureComponent {
  static propTypes = {
    crimesName: PropTypes.string,
    mediaType: PropTypes.string,
    panelItems: PropTypes.array.isRequired,
    browser: PropTypes.object,
    isDesktopManualLayout: PropTypes.bool
  }

  static defaultProps = {
    browser: {}
  }

  constructor(props) {
    super(props)

    this.state = {
      tempMediaType: null,
      isMediaChanged: false
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { browser: { mediaType } = {} } = nextProps

    if (mediaType !== prevState.tempMediaType) {
      return {
        tempMediaType: mediaType,
        isMediaChanged: true
      }
    }

    return {
      isMediaChanged: false
    }
  }

  componentDidMount() {
    this._fixCarouselFlexLayout()
  }

  componentDidUpdate(prevProps) {
    const { mediaType, crimesName } = this.props

    if (prevProps.mediaType !== mediaType) {
      this._hackForSlickSliderCaretReturn()
    }

    if (prevProps.crimesName !== crimesName) {
      this._fixCarouselFlexLayout()
    }
  }

  componentWillUnmount() {
    clearInterval(this._timerID)
  }

  // helper for proper Carousel layout because of bad abstraction inside the DOM structure
  // for centering all the elements at once (list/dots/buttons/etc)
  _fixCarouselFlexLayout = () => {
    const slidesList = this._buildList()

    const setaStyleValues = (display = 'block', margin = 0) => {
      this._timerID = setInterval(() => {
        const slickList = document.querySelector('.slick-slider')
        const slickTrack = document.querySelector('.slick-track')

        slickList.style.display = display
        slickTrack.style.margin = margin

        if (slickList && slickTrack) {
          clearInterval(this._timerID)
        }
      }, 1000)
    }

    slidesList.length <= 2 ? setaStyleValues() : setaStyleValues('flex', 'inherit')
  }

  _buildList() {
    const { panelItems } = this.props

    const columnCount = Math.ceil(panelItems.length / ITEMS_PER_COLUMN)
    const columnIndexes = Array.from(Array(columnCount).keys())
    const columnHolder = []
    let counter = 0

    columnIndexes.forEach((columnIndex) => {
      const itemsHolder = []

      panelItems.map((item, itemIndex) => {
        const itemNotYetIn = counter < itemIndex + FIXER
        const itemsCurrentColumn = itemIndex < ITEMS_PER_COLUMN * (columnIndex + FIXER)

        if (itemNotYetIn && itemsCurrentColumn) {
          counter += COUNT_INCREMENT

          return itemsHolder.push(
            <li key={itemIndex}>
              <span className={styles.label}>{item.label}: </span>
              <span>{item.value}</span>
            </li>
          )
        }

        return itemsHolder
      })

      return columnHolder.push(
        <ul className={styles.itemWrap} key={columnIndex}>
          {itemsHolder}
        </ul>
      )
    })

    return columnHolder
  }

  _getListCount = () => {
    const { isDesktopManualLayout, browser: { mediaType } = {} } = this.props

    if (isDesktopManualLayout) {
      return LAYOUTS['desktop']
    }

    return LAYOUTS[mediaType]
  }

  _getSlickSliderConfig = () => {
    const { isMediaChanged } = this.state

    const slidesToShow = this._getListCount()

    const sliderSettings = {
      className: 'slick--arrows-next-to-dots',
      slidesToShow,
      slidesToScroll: slidesToShow,
      arrows: false
    }

    // if resolution is changed shall go to default slide
    // TODO: fix the bug with buck onca have a time by updateing and
    // reconfigurating the slick-slider lib v.3.0.0 (v.2.0.0 currently in use)!!!
    if (isMediaChanged) {
      sliderSettings.slickGoTo = 0 // not working in v.2.0, shall be tested in react-slick v3.0.0
    }

    return sliderSettings
  }

  // HACK below, see prev func, please!!!
  _hackForSlickSliderCaretReturn = () => {
    const slickTrackNode = document.querySelector('.slick-track')

    if (slickTrackNode && slickTrackNode.style) {
      setTimeout(() => {
        slickTrackNode.style.transform = 'translate3d(0px, 0px, 0px)'
      }, 100)
    }
  }

  render() {
    const { panelItems, browser: { mediaType } = {} } = this.props

    if (isEmpty(panelItems)) {
      return null
    }

    const slidesList = this._buildList()

    return (
      <DropDownPanel panelItems={panelItems} mediaType={mediaType}>
        <SlickSlider {...this._getSlickSliderConfig()}>{slidesList}</SlickSlider>
      </DropDownPanel>
    )
  }
}

const mapStateToProps = (state) => ({
  crimesName: state.common.crimeName,
  browser: state.browser,
  isDesktopLayoutSetted: state.common.isDesktopLayoutSetted
})

export default connect(mapStateToProps, null)(InfoPanel)
