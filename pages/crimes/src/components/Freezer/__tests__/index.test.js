import React from 'react'
import { mount } from 'enzyme'
import Freezer from '..'

describe('<Freezer />', () => {
  it('should render Freezer', () => {
    const Component = mount(<Freezer />)

    expect(Component.find('.freezer').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
