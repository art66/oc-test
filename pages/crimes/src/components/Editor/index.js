import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { CSSTransition } from 'react-transition-group'

import { updateExp, resetStats } from '../../modules/actions'
import styles from './styles.cssmodule.scss'
import './anim.scss'

const MAX_LEVEL = 100

export class Editor extends React.Component {
  static propTypes = {
    currentLevel: PropTypes.number,
    exp: PropTypes.number,
    runUpdateExp: PropTypes.func,
    runResetStats: PropTypes.func,
    editorSwitch: PropTypes.bool,
    isDarkMode: PropTypes.bool
  }

  static defaultProps = {
    currentLevel: 0,
    exp: 0,
    editorSwitch: true,
    runUpdateExp: () => {},
    runResetStats: () => {},
    isDarkMode: false
  }

  constructor(props) {
    super(props)

    this.state = {
      updateType: 'props',
      currentLevel: 0,
      exp: 0
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!prevState.updateType || prevState.updateType === 'props') {
      return {
        updateType: 'props',
        currentLevel: nextProps.currentLevel || 0,
        exp: nextProps.exp || 0
      }
    }

    if (prevState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    return null
  }

  _handleInputChange = event => {
    const { name, value } = event.target

    this.setState({
      updateType: 'state',
      [name]: name === 'currentLevel' && Number(value) > MAX_LEVEL ? MAX_LEVEL : Number(value)
    })
  }

  _handleKeyDown = event => {
    if (event.keyCode === 13) {
      this._save()
    }
  }

  _handleReset = () => {
    const { runResetStats } = this.props

    runResetStats()
  }

  _handleSave = () => {
    const { runUpdateExp } = this.props

    runUpdateExp(this.state)
  }

  _renderInputLevel = () => {
    const { currentLevel = 0 } = this.state

    return (
      <input
        placeholder={0}
        id='currentLevel'
        value={currentLevel}
        name='currentLevel'
        onChange={this._handleInputChange}
        onKeyDown={this._handleKeyDown}
        className={`${styles.input} ${styles.inputMaxWidth}`}
        type='text'
      />
    )
  }

  _renderInputExp = () => {
    const { exp = 0 } = this.state

    return (
      <input
        placeholder={0}
        id='exp'
        value={exp && exp.toFixed(0)}
        name='exp'
        onChange={this._handleInputChange}
        onKeyDown={this._handleKeyDown}
        className={`${styles.input} ${styles.inputMaxWidth}`}
        type='text'
      />
    )
  }

  _renderInputReset = () => {
    const { isDarkMode } = this.props

    return (
      <button
        type='button'
        onClick={this._handleReset}
        className={`torn-btn grey ${isDarkMode ? 'btn-dark-bg' : ''} ${styles.marginRight} ${styles.crimesButton}`}
      >
        RESET
      </button>
    )
  }

  _renderInputSave = () => {
    const { isDarkMode } = this.props

    return (
      <button
        onClick={this._handleSave}
        type='button'
        className={`torn-btn grey ${isDarkMode ? 'btn-dark-bg' : ''} right ${styles.crimesButton}`}
      >
        SAVE
      </button>
    )
  }

  _renderEditor = () => {
    return (
      <div className={styles.wrapper}>
        <label htmlFor='currentLevel'>
          <span>Level </span>
          {this._renderInputLevel()}
        </label>
        <label htmlFor='exp'>
          <span>Exp </span>
          {this._renderInputExp()}
        </label>
        <div className={styles.buttonContainer}>
          {this._renderInputReset()}
          {this._renderInputSave()}
        </div>
      </div>
    )
  }

  render() {
    const { editorSwitch } = this.props

    return (
      <CSSTransition in={editorSwitch} classNames='editor' timeout={{ enter: 450, exit: 450 }} unmountOnExit>
        {this._renderEditor()}
      </CSSTransition>
    )
  }
}

const getThisCrimeData = state => {
  const {
    routing: { locationBeforeTransitions }
  } = state
  const thisCrimeName =
    (locationBeforeTransitions && locationBeforeTransitions.hash.substr(2)) || state.common.crimeName

  const crime = state[thisCrimeName]

  if (!crime) {
    return {}
  }

  return {
    exp: state[thisCrimeName].exp,
    currentLevel: state[thisCrimeName].currentLevel,
    editorSwitch: state.common.editorSwitch,
    isDarkMode: state.common.isDarkMode
  }
}

const mapStateToProps = state => ({
  ...getThisCrimeData(state)
})

const mapDispatchToProps = dispatch => ({
  runUpdateExp: data => dispatch(updateExp(data)),
  runResetStats: () => dispatch(resetStats())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Editor)
