import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

import { editorSwitcher } from '../../modules/actions'
import styles from './button.cssmodule.scss'

const TOOLTIP_ID = 'debug-swicher'

export class EditorSwitcher extends PureComponent {
  static propTypes = {
    switchEditor: PropTypes.func,
    isEditorAppear: PropTypes.bool
  }

  static defaultProps = {
    switchEditor: () => {},
    isEditorAppear: true
  }

  componentDidMount() {
    this._renderTooltip()
  }

  _handlerClick = () => {
    const { switchEditor } = this.props

    switchEditor()
  }

  _renderTooltip = () => tooltipSubscriber.subscribe({ child: 'Editor Switcher', ID: TOOLTIP_ID })

  render() {
    const { isEditorAppear } = this.props
    const checkStatus = isEditorAppear ? '✓' : ' '

    return (
      <div className={styles.debugSwitcherWrap}>
        <button id={TOOLTIP_ID} type='button' className={styles.debugSwitcher} onClick={this._handlerClick}>
          {checkStatus}
        </button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isEditorAppear: state.common.editorSwitch
})

const mapDispatchToState = dispatch => ({
  switchEditor: () => dispatch(editorSwitcher())
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(EditorSwitcher)
