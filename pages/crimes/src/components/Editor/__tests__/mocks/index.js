const initialState = {
  currentLevel: 0,
  exp: 0,
  editorSwitch: true,
  runUpdateExp: () => {},
  runResetStats: () => {}
}

export const disabledState = {
  ...initialState,
  editorSwitch: false
}

export const mockUpdatedState = {
  currentLevel: 98,
  exp: 18,
  updateType: 'state'
}

export const mockUpdatedStateExpect = {
  currentLevel: 98,
  exp: 18,
  updateType: ''
}

export default initialState
