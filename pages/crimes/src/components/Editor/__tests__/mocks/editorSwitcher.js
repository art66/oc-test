const initialState = {
  switchEditor: () => {},
  isEditorAppear: true
}

export const disabledState = {
  switchEditor: () => {},
  isEditorAppear: false
}

export default initialState
