import React from 'react'
import { mount } from 'enzyme'
import initialState, { disabledState, mockUpdatedState, mockUpdatedStateExpect } from './mocks'
import { Editor } from '..'

describe('<Editor />', () => {
  it('should render activated Editor', () => {
    const Component = mount(<Editor {...initialState} />)

    expect(Component.find('Editor').length).toBe(1)
    expect(Component.find('.wrapper').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render deactivated Editor', () => {
    const Component = mount(<Editor {...disabledState} />)

    expect(Component.find('Editor').length).toBe(1)
    expect(Component.find('.wrapper').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should make Reset button click', () => {
    const Component = mount(<Editor {...initialState} />)

    expect(Component.find('Editor').length).toBe(1)
    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('.torn-btn.gold').length).toBe(2)
    expect(Component.find('.torn-btn.gold.marginRight').length).toBe(1)
    Component.find('.torn-btn.gold.marginRight').simulate('click')

    expect(Component).toMatchSnapshot()
  })
  it('should make Save button click', () => {
    const Component = mount(<Editor {...initialState} />)

    expect(Component.find('Editor').length).toBe(1)
    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('.torn-btn.gold').length).toBe(2)
    Component.find('.torn-btn.gold')
      .last()
      .simulate('click')

    expect(Component).toMatchSnapshot()
  })
  it('should change state input value by interaction with InputLVL amd InputEXP input fields', () => {
    const Component = mount(<Editor {...initialState} />)

    expect(Component.find('Editor').length).toBe(1)
    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('#currentLevel').length).toBe(1)
    expect(Component.find('#exp').length).toBe(1)

    Component.find('#currentLevel').simulate('change', {
      target: { name: Component.find('#currentLevel').prop('name'), value: 98 }
    })
    Component.find('#exp').simulate('change', { target: { name: Component.find('#exp').prop('name'), value: 18 } })

    Component.setState(mockUpdatedState)

    expect(Component.state()).toEqual(mockUpdatedStateExpect)

    expect(Component).toMatchSnapshot()
  })
})
