import React from 'react'
import { mount } from 'enzyme'
import initialState, { disabledState } from './mocks/editorSwitcher'
import { EditorSwitcher } from '../EditorSwitcher'

describe('<EditorSwitcher />', () => {
  it('should render active EditorSwitcher', () => {
    const Component = mount(<EditorSwitcher {...initialState} />)

    expect(Component.find('.debugSwitcherWrap').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    Component.find('button').simulate('click')
    expect(Component.find('button').text()).toBe('✓')

    expect(Component).toMatchSnapshot()
  })
  it('should render deactivated EditorSwitcher', () => {
    const Component = mount(<EditorSwitcher {...disabledState} />)

    expect(Component.find('.debugSwitcherWrap').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    Component.find('button').simulate('click')
    expect(Component.find('button').text()).toBe(' ')

    expect(Component).toMatchSnapshot()
  })
})
