import React from 'react'

const child = <div>Text Child</div>

const initialState = {
  children: child,
  dots: true,
  arrows: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1
}

export default initialState
