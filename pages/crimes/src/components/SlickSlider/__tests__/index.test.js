import React from 'react'
import { mount } from 'enzyme'
import initialState from './mocks'
import SlickSlider from '..'

describe('<SlickSlider />', () => {
  it('should render regular SlickSlider Component', () => {
    const Component = mount(<SlickSlider {...initialState} />)

    expect(Component.find('SlickSlider').length).toBe(1)
    expect(Component.find('.slick-prev-wrap').length).toBe(0)
    expect(Component.find('.slick-next-wrap').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
