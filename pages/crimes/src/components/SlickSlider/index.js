import React from 'react'
import PropTypes from 'prop-types'
import Slider from 'react-slick'

import './slick-slider.scss'
import 'slick-carousel/slick/slick.css'

export const SlickSlider = props => {
  const { children = [], ...rest } = props

  const defaults = {
    dots: true,
    arrows: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
    // variableWidth: true,
    // prevArrow: <PrevArrow />,
    // nextArrow: <NextArrow />
  }

  const merged = {
    ...defaults,
    ...rest
  }

  return <Slider {...merged}>{children}</Slider>
}

SlickSlider.propTypes = {
  children: PropTypes.node.isRequired
}

export default SlickSlider
