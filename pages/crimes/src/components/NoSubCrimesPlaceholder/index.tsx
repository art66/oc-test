import React from 'react'

import styles from './styles.cssmodule.scss'

export interface IProps {
  text?: string
  isBackdrop?: boolean
}

const MOCK = 'No potential targets are within range currently. Please wait...'

class NoSubCrimesPlaceholder extends React.PureComponent<IProps> {
  render() {
    const { text, isBackdrop } = this.props

    return (
      <div className={`${styles.noRowsPlaceholder} ${isBackdrop ? styles.backdrop : ''}`}>
        <span className={styles.textPlaceholder}>
          {text || MOCK}
        </span>
      </div>
    )
  }
}

export default NoSubCrimesPlaceholder
