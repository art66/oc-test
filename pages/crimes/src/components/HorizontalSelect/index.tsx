import React, { PureComponent } from 'react'
import Slider from 'react-slick'
import Arrow from './Arrow'

import './slick-slider-hor.scss'
import 'slick-carousel/slick/slick.css'

export interface IProps {
  onChange: (genre: number) => void
  value: number
  options: {
    label: any
    value: string
  }[]
}

export class HorizontalSelect extends PureComponent<IProps> {
  private _myRef: React.RefObject<HTMLButtonElement>

  static defaultProps = {
    onChange: () => {},
    options: []
  }

  constructor(props: IProps) {
    super(props)

    this._myRef = React.createRef()
  }

  _handleChange = index => {
    const { onChange, options } = this.props
    const genre = Number(options[index].value)

    if (!genre) return

    onChange(genre)
  }

  _values = options => {
    return options.map(elem => {
      return <div key={elem.value}>{elem.label}</div>
    })
  }

  _sliderConfig = () => {
    const sliderConfig = {
      afterChange: this._handleChange,
      className: 'slick--horizontal',
      dots: false,
      variableWidth: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: <Arrow />,
      nextArrow: <Arrow />
    }

    return sliderConfig
  }

  render() {
    const { options } = this.props

    if (!options || options.length === 0) return null

    return (
      <Slider {...this._sliderConfig()} ref={this._myRef}>
        {this._values(options)}
      </Slider>
    )
  }
}

export default HorizontalSelect
