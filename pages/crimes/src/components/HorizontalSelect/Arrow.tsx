import React, { memo } from 'react'

import './slick-slider-hor.scss'

export interface IProps {
  className: string
  onClick: () => void
}

const Arrow = (props: IProps) => {
  const { onClick, className } = props

  const handleClick = e => {
    e.preventDefault()
    e.stopPropagation()

    onClick && onClick()
  }

  return (
    <button
      aria-label='set CDs gender'
      type='button'
      className={className}
      onClick={handleClick}
    />
  )
}

Arrow.defaultProps = {
  className: '',
  onClick: () => {}
}

export default memo(Arrow)
