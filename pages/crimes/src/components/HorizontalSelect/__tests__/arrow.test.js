import React from 'react'
import { mount } from 'enzyme'
import initialState from './mocks/arrow'
import Arrow from '../Arrow'

describe('<Arrow />', () => {
  it('should render Arrow woth cusom class and received click event', () => {
    const Component = mount(<Arrow {...initialState} />)

    expect(Component.find('Arrow').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    Component.find('button').simulate('click')
    expect(Component.find('Arrow').prop('classname')).toBe('slick-arrow slick-prev slick-disabled')

    expect(Component).toMatchSnapshot()
  })
})
