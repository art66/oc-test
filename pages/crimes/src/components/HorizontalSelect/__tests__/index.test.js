import React from 'react'
import { mount } from 'enzyme'
import initialState, { emptyList } from './mocks'
import HorizontalSelect from '..'

describe('<HorizontalSelect />', () => {
  it('should render HorizontalSelect with 3 items list', () => {
    const Component = mount(<HorizontalSelect {...initialState} />)

    expect(Component.find('HorizontalSelect').length).toBe(1)
    expect(Component.find('Slider').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should not render HorizontalSelect with 0 items list', () => {
    const Component = mount(<HorizontalSelect {...emptyList} />)

    expect(Component.find('HorizontalSelect').length).toBe(1)
    expect(Component.find('Slider').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
