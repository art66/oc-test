const initialState = {
  onChange: () => {},
  options: [
    {
      value: 0,
      label: 'test_0'
    },
    {
      value: 1,
      label: 'test_1'
    },
    {
      value: 2,
      label: 'test_2'
    }
  ]
}

export const emptyList = {
  ...initialState,
  options: []
}

export default initialState
