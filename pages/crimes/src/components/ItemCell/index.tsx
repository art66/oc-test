import React from 'react'
import { toMoney } from '@torn/shared/utils'
import isValue from '@torn/shared/utils/isValue'
import cn from 'classnames'

import styles from './index.cssmodule.scss'

export interface IProps {
  ID?: string
  callback?: () => void
  isButton?: boolean
  count?: number
  imgAlt?: string
  imgSrc?: string
  isMock?: boolean
}

export type TButtonTypes = 'button' | 'reset' | 'submit'

const ItemCell = ({ ID, callback, isButton, isMock, imgAlt, imgSrc, count }: IProps) => {
  const TabType = isButton ? 'button' : 'div'
  const extraProps = isButton ?
    {
      type: 'button' as TButtonTypes,
      onClick: callback
    } :
    {}

  if (isMock) {
    return (
      <div className={styles.itemWrap}>
        <div className={styles.itemMock} />
      </div>
    )
  }

  return (
    <div className={styles.itemWrap}>
      <TabType id={ID} className={cn(styles.itemContainer, { [styles.clickable]: isButton })} {...extraProps}>
        <img alt={imgAlt} className={styles.img} src={imgSrc} />
      </TabType>
      <div className={styles.countersContainer}>
        {isValue(count) && <span className={styles.counter}>{toMoney(count)}</span>}
      </div>
    </div>
  )
}

export default ItemCell
