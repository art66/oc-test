import React from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class StarLVL extends React.PureComponent<IProps> {
  _renderStarIcon = lvl => {
    return (
      <div className={styles.svgStarWrap}>
        <i className={`${styles.star} ${styles[`star${lvl}`]}`} />
      </div>
    )
  }

  render() {
    const { lvl } = this.props

    return this._renderStarIcon(lvl)
  }
}

export default StarLVL
