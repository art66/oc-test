import React from 'react'
import { mount } from 'enzyme'
import initialState, { customTitle, disableDefs, disabledTitle, customStyles } from './mocks'
import StarLVL from '..'

describe('<StarLVL />', () => {
  it('should render regular StarLVL Component with romb icon', () => {
    const Component = mount(<StarLVL {...initialState} />)

    expect(Component.find('StarLVL').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('type')).toBe('romb')

    expect(Component).toMatchSnapshot()
  })
  it('should render with custom title', () => {
    const Component = mount(<StarLVL {...customTitle} />)

    expect(Component.find('StarLVL').length).toBe(1)
    expect(Component.find('span').text()).toBe('Title')

    expect(Component).toMatchSnapshot()
  })
  it('should render with customProps, e.g. with grey icon background', () => {
    const Component = mount(<StarLVL {...disableDefs} />)

    expect(Component.find('StarLVL').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('activeDefs')).toBeFalsy()
    expect(Component.find('SVGIconGenerator').prop('fill')).toEqual({ color: '#bdbdaf', strokeWidth: 0 })

    expect(Component).toMatchSnapshot()
  })
  it('should render with disabled title', () => {
    const Component = mount(<StarLVL {...disabledTitle} />)

    expect(Component.find('StarLVL').length).toBe(1)
    expect(Component.find('span').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render with custom title styles', () => {
    const Component = mount(<StarLVL {...customStyles} />)

    expect(Component.find('StarLVL').length).toBe(1)
    expect(Component.find('span').prop('className')).toBe('crimes-icon-text testStyle')

    expect(Component).toMatchSnapshot()
  })
})
