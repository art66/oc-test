const initialState = {
  lvl: 1
}

export const customTitle = {
  ...initialState,
  customTitle: 'Title'
}

export const disableDefs = {
  ...initialState,
  customProps: {
    activeDefs: false,
    fill: {
      color: '#bdbdaf',
      strokeWidth: 0
    }
  }
}

export const disabledTitle = {
  ...initialState,
  disableTitle: true
}

export const customStyles = {
  ...initialState,
  customStyles: {
    text: 'testStyle'
  }
}

export default initialState
