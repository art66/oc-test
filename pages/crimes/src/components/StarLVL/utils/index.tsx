import SVG_ICONS_LABELS from '../constants'

const getStarLVL = level => {
  const { PLATINUM, GOLD, SILVER, BRONZE, METAL, ROMB } = SVG_ICONS_LABELS

  if (level === 100) {
    return PLATINUM
  }

  if (level > 74) {
    return GOLD
  }

  if (level > 49) {
    return SILVER
  }

  if (level > 24) {
    return BRONZE
  }

  if (level > 2) {
    return METAL
  }

  return ROMB
}

export default getStarLVL
