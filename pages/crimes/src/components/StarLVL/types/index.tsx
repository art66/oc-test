export interface IProps {
  lvl: number
  customTitle?: string
  disableTitle?: boolean
  customProps?: object
  customStyles?: {
    text: string
  }
}
