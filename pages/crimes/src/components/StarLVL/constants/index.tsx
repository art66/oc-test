const SVG_ICONS_LABELS = {
  PLATINUM: 'platinum',
  GOLD: 'gold',
  SILVER: 'silver',
  BRONZE: 'bronze',
  METAL: 'metal',
  ROMB: 'romb'
}

export default SVG_ICONS_LABELS
