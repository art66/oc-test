import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group'
import { connect } from 'react-redux'
import { toggleArrows, toggleDropDownPanel } from '../../modules/actions'
import s from './styles.cssmodule.scss'
import './anim.scss'

export class DropDownPanel extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    toggleDropDownToShow: PropTypes.func,
    toggleArrowsToShow: PropTypes.func,
    expanded: PropTypes.bool,
    panelItems: PropTypes.array,
    mediaType: PropTypes.string,
    isMediaChanged: PropTypes.bool
  }

  static defaultProps = {
    panelItems: [],
    toggleDropDownToShow: () => {},
    toggleArrowsToShow: () => {},
    expanded: false,
    mediaType: 'desktop'
  }

  constructor(props) {
    super(props)

    this.state = {
      coordsStart: null
    }
  }

  shouldComponentUpdate(nextProps) {
    const { expanded, panelItems, mediaType } = this.props

    const statisticIsNotEqual = panelItems.some((item, index) => item !== nextProps.panelItems[index])
    const orientationChanged = mediaType !== nextProps.mediaType

    if (statisticIsNotEqual || orientationChanged || nextProps.isMediaChanged) {
      return true
    }

    return expanded !== nextProps.expanded
  }

  _expandPanel = (coordsFinish, target) => {
    const { coordsStart } = this.state
    const { toggleArrowsToShow, toggleDropDownToShow } = this.props

    const isCoordsSame = JSON.stringify(coordsStart) === JSON.stringify(coordsFinish)
    const isElementSlick = target.className.match(/slick-arrow/)
    const isElementButton = target.tagName === 'BUTTON'

    if (!isCoordsSame || isElementSlick || isElementButton) return

    toggleDropDownToShow()
    toggleArrowsToShow()
  }

  _handleDown = e => {
    this.setState({
      coordsStart: {
        X: e.clientX,
        Y: e.clientY
      }
    })
  }

  _handleUp = e => {
    const coordsFinish = {
      X: e.clientX,
      Y: e.clientY
    }

    this._expandPanel(coordsFinish, e.target)
  }

  _renderDropDown = () => {
    const { children, expanded } = this.props

    return (
      <div className={s.supportAnimWrap}>
        <div className={`${s.panelContent} ${expanded ? s.open : s.closed}`}>
          {children}
        </div>
        <div className={s.arrow} />
      </div>
    )
  }

  render() {
    const { expanded } = this.props

    return (
      <div
        role='button'
        aria-label='Panel Log DropDown Handler'
        tabIndex={0}
        className={s.panelWrap}
        onMouseDown={this._handleDown}
        onMouseUp={this._handleUp}
      >
        <div className={s.panelWrapInner}>
          <CSSTransition in={expanded} classNames='dropDown' timeout={{ enter: 350, exit: 350 }}>
            {this._renderDropDown()}
          </CSSTransition>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  expanded: state.common.showDropDown
})

const mapDispatchToProps = dispatch => ({
  toggleDropDownToShow: () => dispatch(toggleDropDownPanel()),
  toggleArrowsToShow: () => dispatch(toggleArrows())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DropDownPanel)
