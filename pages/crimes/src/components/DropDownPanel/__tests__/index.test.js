import React from 'react'
import { mount } from 'enzyme'
import initialState,
{
  deactivatedDropDown,
  eventClicked,
  mouseDownMock,
  mouseUpMock,
  mouseUpWrongMock
} from './mocks'
import { DropDownPanel } from '..'

describe('<DropDownPanel />', () => {
  it('should render DropDown component with activated DropDown', () => {
    const Component = mount(
      <DropDownPanel {...initialState}>
        <div>TEST</div>
      </DropDownPanel>
    )

    expect(Component.find('.panelWrap').length).toBe(1)
    expect(Component.find('.panelWrapInner').length).toBe(1)
    expect(Component.find('.panelContent').length).toBe(1)
    expect(Component.find('.panelContent').childAt(0).props('html').children).toEqual('TEST')
    expect(Component).toMatchSnapshot()
  })
  it('should render DropDown component with deactivated DropDown', () => {
    const Component = mount(
      <DropDownPanel {...deactivatedDropDown}>
        <div>TEST</div>
      </DropDownPanel>
    )

    expect(Component.find('.panelWrap').length).toBe(1)
    expect(Component.find('.panelWrapInner').length).toBe(1)
    expect(Component.find('.panelContent').length).toBe(1)
    expect(Component.find('.panelContent').childAt(0).hasClass('slick-initialized')).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('should close DropDownPanel in case if click is made right', () => {
    const Component = mount(
      <DropDownPanel {...initialState}>
        <div>TEST</div>
      </DropDownPanel>
    )

    Component.find('.panelWrap').simulate('mousedown', eventClicked)
    expect(Component.state()).toEqual(mouseDownMock)
    Component.find('.panelWrap').simulate('mouseup', eventClicked)

    const isCoordsSame = JSON.stringify(mouseDownMock) === JSON.stringify(mouseUpMock)
    const isElementSlick = Component.find('.panelWrap').hasClass('slick-arrow')
    const isElementButton = Component.find('.panelWrap').type() === 'BUTTON'

    expect(isCoordsSame && !isElementSlick && !isElementButton).toBeTruthy()
  })
  it('should not to close DropDownPanel in case if click is have different coords on Down and Up phases', () => {
    const Component = mount(
      <DropDownPanel {...initialState}>
        <div>TEST</div>
      </DropDownPanel>
    )

    Component.find('.panelWrap').simulate('mousedown', eventClicked)
    expect(Component.state()).toEqual(mouseDownMock)
    Component.find('.panelWrap').simulate('mouseup', eventClicked)

    const isCoordsSame = JSON.stringify(mouseDownMock) === JSON.stringify(mouseUpWrongMock)
    const isElementSlick = Component.find('.panelWrap').hasClass('slick-arrow')
    const isElementButton = Component.find('.panelWrap').type() === 'BUTTON'

    expect(isCoordsSame && !isElementSlick && !isElementButton).toBeFalsy()
  })
  it('should not to close DropDownPanel in case if clicked node contains \'slick-arrow\' className', () => {
    const Component = mount(
      <DropDownPanel {...initialState}>
        <div className='slick-arrow'>TEST</div>
      </DropDownPanel>
    )

    Component.find('.slick-arrow').simulate('mousedown', eventClicked)
    expect(Component.state()).toEqual(mouseDownMock)
    Component.find('.slick-arrow').simulate('mouseup', eventClicked)

    const isCoordsSame = JSON.stringify(mouseDownMock) === JSON.stringify(mouseUpWrongMock)
    const isElementSlick = Component.find('.slick-arrow').hasClass('slick-arrow')
    const isElementButton = Component.find('.slick-arrow').type() === 'BUTTON'

    expect(isCoordsSame && !isElementSlick && !isElementButton).toBeFalsy()
  })
  it('should not to close DropDownPanel in case if clicked node is BUTTON', () => {
    const Component = mount(
      <DropDownPanel {...initialState}>
        <button type='button'>TEST</button>
      </DropDownPanel>
    )

    Component.find('button').first().simulate('mousedown', eventClicked)
    expect(Component.state()).toEqual(mouseDownMock)
    Component.find('button').first().simulate('mouseup', eventClicked)

    const isCoordsSame = JSON.stringify(mouseDownMock) === JSON.stringify(mouseUpWrongMock)
    const isElementSlick = Component.find('.panelWrap').hasClass('slick-arrow')
    const isElementButton = Component.find('button').first().type() === 'BUTTON'

    expect(isCoordsSame && !isElementSlick && !isElementButton).toBeFalsy()
  })
})
