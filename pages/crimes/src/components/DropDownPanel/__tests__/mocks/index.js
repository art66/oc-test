const initialState = {
  dropDownPanelToHide: () => {},
  arrowToShow: () => {},
  expanded: true
}

export const deactivatedDropDown = {
  ...initialState,
  expanded: false
}

export const eventClicked = {
  clientX: 100,
  clientY: 200
}

export const mouseDownMock = {
  coordsStart: {
    X: 100,
    Y: 200
  }
}

export const mouseUpMock = {
  coordsStart: {
    X: 100,
    Y: 200
  }
}

export const mouseUpWrongMock = {
  coordsStart: {
    X: 200,
    Y: 200
  }
}

export default initialState
