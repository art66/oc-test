import React from 'react'
import { shallow } from 'enzyme'
import initialState, { noValue } from './mocks'
import { PanelTitle } from '..'

describe('<PanelTitle />', () => {
  it('should render PanelTitle', () => {
    const Component = shallow(<PanelTitle {...initialState} />)

    expect(Component).toMatchSnapshot()

    expect(Component.find('.titleCrimes').length).toBe(1)
    expect(
      Component.find('.success')
        .childAt(0)
        .text()
    ).toBe('12')
    expect(
      Component.find('.failure')
        .childAt(0)
        .text()
    ).toBe('1')
    expect(
      Component.find('.jailed')
        .childAt(0)
        .text()
    ).toBe('1')

    expect(Component).toMatchSnapshot()
  })
  it('should render PanelTitle with default values if some does not come from store', () => {
    const Component = shallow(<PanelTitle {...noValue} />)

    expect(Component.find('.titleCrimes').length).toBe(1)
    expect(
      Component.find('.success')
        .childAt(0)
        .text()
    ).toBe('0')
    expect(
      Component.find('.failure')
        .childAt(0)
        .text()
    ).toBe('0')
    expect(
      Component.find('.jailed')
        .childAt(0)
        .text()
    ).toBe('0')

    expect(Component).toMatchSnapshot()
  })
})
