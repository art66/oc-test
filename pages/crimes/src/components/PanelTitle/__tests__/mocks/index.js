const initialState = {
  title: 'Test Title',
  stats: [
    {
      label: 'Successes',
      value: 12
    },
    {
      label: 'Fails',
      value: 1
    },
    {
      label: 'Critical fails',
      value: 1
    }
  ]
}

export const noValue = {
  ...initialState,
  stats: [
    {
      label: 'Successes'
    },
    {
      label: 'Fails'
    },
    {
      label: 'Critical fails'
    }
  ]
}

export default initialState
