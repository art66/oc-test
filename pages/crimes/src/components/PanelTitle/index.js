import PropTypes from 'prop-types'
import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'

import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { toggleDropDownPanel, toggleArrows } from '../../modules/actions'

import EditorSwitcher from '../Editor/EditorSwitcher'
import styles from './styles.cssmodule.scss'

const NO_VALUE = 0
const STATUSES_TO_RENDER = ['Successes', 'Fails', 'Critical fails']
const TOOLTIP_ID = 'stats'
const TYPES = {
  0: 'Success',
  1: 'Failure',
  2: 'Critical'
}

const ICON_DIMENSIONS = {
  width: '25px',
  height: '25px',
  viewbox: '-5 -3 25 25'
}

export class PanelTitle extends PureComponent {
  static propTypes = {
    stats: PropTypes.array,
    hideTestPanel: PropTypes.bool,
    isDarkMode: PropTypes.bool,
    title: PropTypes.string.isRequired,
    toggleBanner: PropTypes.func,
    toggleArrows: PropTypes.func
  }

  static defaultProps = {
    stats: [],
    hideTestPanel: true,
    isDarkMode: false,
    toggleBanner: () => {},
    toggleArrows: () => {}
  }

  componentDidMount() {
    this._renderTooltip()
  }

  _getStatuses = () => {
    const { stats = [] } = this.props

    const statusesValues = []

    if (!stats) {
      return null
    }

    STATUSES_TO_RENDER.forEach(stat => {
      const findStat = stats.find(item => item.label === stat)
      const valueNotFound = !findStat || !findStat.value

      if (valueNotFound) {
        statusesValues.push(NO_VALUE)

        return
      }

      statusesValues.push(findStat.value)
    })

    return statusesValues
  }

  _getStatTooltipsPayload = index => ({
    type: TYPES[index],
    ID: `${TOOLTIP_ID}-${TYPES[index]}`,
    isLastChild: index <= 1
  })

  _renderTooltip = () => {
    [0, 1, 2].forEach(key => {
      const { type, ID } = this._getStatTooltipsPayload(key)

      tooltipSubscriber.subscribe({ child: type, ID })
    })
  }

  _renderStats = () => {
    const statusesList = this._getStatuses()

    const divider = <span> / </span>

    if (!statusesList) {
      return null
    }

    return statusesList.map((count, index) => {
      const { type, ID, isLastChild } = this._getStatTooltipsPayload(index)

      return (
        <Fragment key={count + index}>
          <span id={ID} className={styles[type.toLowerCase()]}>
            <span className={styles.count}>{count}</span>
          </span>
          {isLastChild && divider}
        </Fragment>
      )
    })
  }

  render() {
    const { title, hideTestPanel, toggleBanner, isDarkMode } = this.props

    // TODO: delete EditorSwitcher once the project will be done.
    return (
      <div className={styles.titleCrimes}>
        <span>{title}</span>
        <span className={styles.counters}>{this._renderStats()}</span>
        <button className={styles.toggleIcon} type='button' onClick={() => { toggleBanner(); toggleArrows() }}>
          <SVGIconGenerator
            iconName='Statistics'
            fill={{
              color: isDarkMode ? '#333' : '#888',
              stroke: isDarkMode ? '#666' : '#888'
            }}
            dimensions={ICON_DIMENSIONS}
          />
        </button>
        {!hideTestPanel && <EditorSwitcher />}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  hideTestPanel: state.common.hideTestPanel,
  isDarkMode: state.common.isDarkMode
})

const mapDispatchToState = {
  toggleBanner: toggleDropDownPanel
}

export default connect(
  mapStateToProps,
  mapDispatchToState
)(PanelTitle)
