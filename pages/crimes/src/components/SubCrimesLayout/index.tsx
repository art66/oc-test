import React from 'react'

import TopContainer from '../TopContainer'
import Freezer from '../Freezer'

interface IProps {
  title: string
  stats: object[]
  current: number
  progress: number
  showPlaceholder: boolean
  skeleton: any
  banners: JSX.Element
  customWrapClass: string
}

class SubCrimesLayout extends React.Component<IProps> {
  render() {
    const {
      title,
      stats,
      current,
      progress,
      showPlaceholder,
      skeleton: Skeleton,
      children: CrimesList,
      banners: Banners,
      customWrapClass
    } = this.props

    return (
      <div className={`subcrimes-wrap ${customWrapClass}`}>
        <TopContainer title={title} stats={stats} current={current} progress={progress}>
          {Banners}
        </TopContainer>
        {showPlaceholder ? <Skeleton type='' /> : CrimesList}
        {showPlaceholder && <Freezer />}
      </div>
    )
  }
}

export default SubCrimesLayout
