import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { HashRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import CoreLayout from '../layouts/CoreLayout'
import Hub from '../routes/Hub'

import CrimesRoutesContainer from '../routes'

export class AppContainer extends PureComponent {
  render() {
    const { store } = this.props

    return (
      <Provider store={store} history={history}>
        <HashRouter>
          <CoreLayout>
            <Switch>
              <Route exact path='/' component={Hub} />
              <CrimesRoutesContainer />
            </Switch>
          </CoreLayout>
        </HashRouter>
      </Provider>
    )
  }
}

AppContainer.propTypes = {
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
}

export default AppContainer
