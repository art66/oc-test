self.addEventListener('install', e => {
  e.waitUntil(
    caches.open('sprays-cache').then(cache => {
      return cache.addAll([
        '/images/items/862/medium.png',
        '/images/items/856/medium.png',
        '/images/items/857/medium.png',
        '/images/items/859/medium.png',
        '/images/items/863/medium.png',
        '/images/items/861/medium.png',
        '/images/items/860/medium.png',
        '/images/items/858/medium.png'
      ])
    })
  )
})

// self.addEventListener('fetch', function(event) {
//   if (event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') {
//     return
//   }
//   event.respondWith(
//     caches.match(event.request).then(function(response) {
//       return response || fetch(event.request, { headers: { 'Content-Type' : 'text/plain' } })
//     })
//   )
// })
