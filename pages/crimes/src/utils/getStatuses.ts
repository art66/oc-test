import { IAttemptProgress, IOutcome } from '../interfaces/IState'

export interface IGetStatuses {
  isRowActive: boolean
  isRowNotInProgress: boolean
  isRowHaveOutcome: boolean
  isCurrentRow: boolean
}

const getRowID = row => {
  if (row.commitID) {
    return `${row.crimeID}_${row.commitID}`
  } else if (row.subID) {
    return `${row.crimeID}_${row.subID}`
  }

  return row.crimeID
}

const getStatuses = (attemptProgress: IAttemptProgress, row: any, outcome: IOutcome): IGetStatuses => {
  const ID = getRowID(row)

  const isRowActive = attemptProgress.itemId === ID
  const isRowNotInProgress = !attemptProgress.inProgress
  const isRowHaveOutcome = outcome && outcome.story && isRowActive

  return {
    isRowActive,
    isRowNotInProgress,
    isRowHaveOutcome,
    isCurrentRow: isRowActive
  }
}

export default getStatuses
