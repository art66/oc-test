import { IRowGlobalSelector } from './IGlobalSelectors'

export interface IRowSelector<T> {
  ownCrimeRowProps: T
  throwGlobalCrimeRowProps: IRowGlobalSelector
}
