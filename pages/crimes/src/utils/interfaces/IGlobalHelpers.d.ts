import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import { IAttemptProgress, ICommonRowProps, IOutcome, TCloseOutcome } from '../../interfaces/IState'

export type TGetTitle = (state: ICommonRowProps) => string
export type TGetCrimeId = (state: ICommonRowProps) => number
export type TGetIconClass = (state: ICommonRowProps) => string
export type TGetAdditionalInfo = (state: ICommonRowProps) => object
export type TGetMediaType = (state: ICommonRowProps) => TMediaType
export type TGetJailed = (state: ICommonRowProps) => boolean
export type TGetOutcome = (state: ICommonRowProps) => IOutcome
export type TGetAttemptProgress = (state: ICommonRowProps) => IAttemptProgress
export type TGetCrimes = (state: ICommonRowProps) => any[]
export type TGetCurrentRowID = (state: ICommonRowProps) => number | string
export type TCheckCurrentRow = (state: ICommonRowProps) => boolean
export type TGetLastSubCrimeID = (state: ICommonRowProps) => number
export type TGetResult = (state: ICommonRowProps) => string
export type TGetStory = (state: ICommonRowProps) => object
export type TGetTypeID = (state: ICommonRowProps) => number
export type TGetButtonLabel = (state: ICommonRowProps) => string
export type TGetUserNerve = (state: ICommonRowProps) => number
export type TGetDesktopManualLayout = (state: ICommonRowProps) => boolean
export type TGetCrimeInfo = (state: ICommonRowProps) => object

export type TGetManualLayout = (state: ICommonRowProps) => boolean
export type TGetAvailable = (state: ICommonRowProps) => boolean
export type TGetCloseOutcomeCallback = (state: ICommonRowProps) => TCloseOutcome
export type TGetShowOutcome = (state: ICommonRowProps) => boolean
export type TGetState = (state: ICommonRowProps) => string
export type TGetItemId = (state: ICommonRowProps) => number | string
export type TGetNerve = (state: ICommonRowProps) => number
export type TCheckAvailable = (state: ICommonRowProps) => boolean
export type TCheckRowIsNotInProgress = (state: ICommonRowProps) => boolean
export type TCheckRowActive = (state: ICommonRowProps) => boolean
export type TCheckRowHaveOutcome = (state: ICommonRowProps) => boolean
export type TGetAction = (state: ICommonRowProps) => (attemptURL: string, ID: string | number) => void
export type TGetSubID = (state: ICommonRowProps) => string
export type TGetError = (state: ICommonRowProps) => string

export interface IGlobalHelpers {
  getTitle: TGetTitle
  getCrimeId: TGetCrimeId
  getIconClass: TGetIconClass
  getAdditionalInfo: TGetAdditionalInfo
  getMediaType: TGetMediaType
  getJailed: TGetJailed
  getOutcome: TGetOutcome
  getAttemptProgress: TGetAttemptProgress
  getCrimes: TGetCrimes
  checkCurrentRow: TCheckCurrentRow
  getCurrentRowID: TGetCurrentRowID
  getLastSubCrimeID: TGetLastSubCrimeID
  getResult: TGetResult
  getStory: TGetStory
  getTypeID: TGetTypeID
  getButtonLabel: TGetButtonLabel
  getUserNerve: TGetUserNerve
  getDesktopManualLayout: TGetDesktopManualLayout
  getCrimeInfo: TGetCrimeInfo
  getManualLayout: TGetManualLayout
  getAvailable: TGetAvailable
  getCloseOutcomeCallback: TGetCloseOutcomeCallback
  getShowOutcome: TGetShowOutcome
  getState: TGetState
  getItemId: TGetItemId
  getNerve: TGetNerve
  checkAvailable: TCheckAvailable
  checkRowIsNotInProgress: TCheckRowIsNotInProgress
  checkRowActive: TCheckRowActive
  checkRowHaveOutcome: TCheckRowHaveOutcome
  getAction: TGetAction
  getSubID: TGetSubID
  getError: TGetError
}
