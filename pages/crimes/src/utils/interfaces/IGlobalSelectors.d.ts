import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import { IOutcome } from '../../interfaces/IState';
import { ICrimeInfo } from '../../routes/CardSkimming/interfaces/IPayload';

export interface IButtonActionProps {
  crimeIDCustom?: string | number
  subURL?: string
  subID?: number | string
}

export interface IButtonAction {
  label: string
  disabled: boolean
  width?: number
  height?: number
  action: ({ subURL, subID }?: IButtonActionProps) => void
}

export interface IRowGlobalSelector {
  additionalInfo: object
  available: boolean
  button: IButtonAction
  className: string
  crimeInfo: object
  closeOutcome: {
    isActive: boolean
    callback: () => void
  }
  iconClass: string
  crimeID: number
  currentRowID: number | string
  isCurrentRow: boolean
  isManualLayoutSetted: boolean
  isRowActive: boolean
  isRowHaveOutcome: boolean
  isRowNotInProgress: boolean
  itemId: number | string
  jailed: boolean
  lastSubCrimeID: number
  mediaType: TMediaType
  nerve: number
  outcome: IOutcome
  showOutcome: boolean
  state: string
  title: string
  subID?: string
}
