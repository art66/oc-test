import { TMediaType } from "@torn/shared/utils/checkMediaType/interfaces";
import { IAttemptProgress, IOutcome } from "../../interfaces/IState";

export interface IGlobalListSelector {
  outcome: IOutcome
  isCurrentRow: boolean
  currentRowID: number
  lastSubCrimeID: number
  attemptProgress: IAttemptProgress
  crimes: any
  result: string
  story: object
  typeID: number
  buttonLabel: string
  userNerve: number
  jailed: boolean
  mediaType: TMediaType
  isDesktopLayoutSetted: boolean
}
