const crimeIDNormalizer = (crimeID: string) => {
  return /_/i.test(crimeID) ? Number(crimeID.match(/\d+/)[0]) : crimeID
}

export default crimeIDNormalizer
