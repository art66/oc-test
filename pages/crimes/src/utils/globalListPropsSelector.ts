import { createSelector } from 'reselect'
import { IOutcome } from '../interfaces/IState'
import {
  getMediaType,
  getError,
  getJailed,
  getOutcome,
  getAttemptProgress,
  getCrimes,
  checkCurrentRow,
  getCurrentRowID,
  getLastSubCrimeID,
  getResult,
  getStory,
  getTypeID,
  getButtonLabel,
  getUserNerve,
  getDesktopManualLayout
} from './globalSelectorsHelpers'

import { IGlobalListSelector } from './interfaces/IGlobalListSelector'

import rowEnhancer from './rowEnhancer'

// eslint-disable-next-line max-statements
const getGlobalSelector = (...args): IGlobalListSelector => {
  const [route, browser, common] = args

  // reselect functions
  const mediaTypeState = createSelector(
    [getMediaType],
    mediaType => mediaType
  )
  const jailedState = createSelector(
    [getJailed],
    jailed => jailed
  )
  const errorState = createSelector(
    [getError],
    error => error
  )
  const outcomeState = createSelector(
    [getOutcome],
    (outcome: IOutcome): IOutcome => outcome
  )
  const isCurrentRowState = createSelector(
    [checkCurrentRow],
    isCurrentRow => isCurrentRow
  )
  const currentRowIDState = createSelector(
    [getCurrentRowID],
    currentRowID => currentRowID
  )
  const lastSubCrimeIDState = createSelector(
    [getLastSubCrimeID],
    lastSubCrimeID => lastSubCrimeID
  )
  const attemptProgressState = createSelector(
    [getAttemptProgress],
    attemptProgress => attemptProgress
  )
  const outcomeResult = createSelector(
    [getResult],
    result => result
  )
  const outcomeStory = createSelector(
    [getStory],
    story => story
  )
  const typeIDState = createSelector(
    [getTypeID],
    typeID => typeID
  )
  const buttonLabelState = createSelector(
    [getButtonLabel],
    buttonLabel => buttonLabel
  )
  const userNerveState = createSelector(
    [getUserNerve],
    nerve => nerve
  )
  const desktopManualLayoutState = createSelector(
    [getDesktopManualLayout],
    isDesktopLayoutSetted => isDesktopLayoutSetted
  )

  const globalListPropsSelector = createSelector(
    [getAttemptProgress, getOutcome, getCrimes],
    (...props) => {
      const [attemptProgress, outcome, crimesByType] = props

      return (
        crimesByType && crimesByType.map(row => rowEnhancer(attemptProgress, row, outcome)) || []
      )
    }
  )

  return {
    outcome: outcomeState(route),
    isCurrentRow: isCurrentRowState(route),
    currentRowID: currentRowIDState(route),
    lastSubCrimeID: lastSubCrimeIDState(route),
    attemptProgress: attemptProgressState(route),
    crimes: globalListPropsSelector(route),
    result: outcomeResult(route),
    story: outcomeStory(route),
    typeID: typeIDState(route),
    buttonLabel: buttonLabelState(route),
    userNerve: userNerveState(common),
    jailed: jailedState(common),
    error: errorState(common),
    mediaType: mediaTypeState(browser),
    isDesktopLayoutSetted: desktopManualLayoutState(common)
  } as IGlobalListSelector
}

export default getGlobalSelector
