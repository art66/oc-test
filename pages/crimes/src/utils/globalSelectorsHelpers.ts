import { ICommonRowProps } from '../interfaces/IState'

import {
  TCheckCurrentRow,
  TGetAdditionalInfo,
  TGetAttemptProgress,
  TGetButtonLabel, TGetCrimeId,
  TGetCrimeInfo, TGetCrimes,
  TGetCurrentRowID, TGetDesktopManualLayout,
  TGetIconClass, TGetJailed,
  TGetLastSubCrimeID,
  TGetMediaType, TGetOutcome,
  TGetResult,
  TGetStory,
  TGetTitle,
  TGetTypeID,
  TGetUserNerve,
  TGetManualLayout,
  TGetAvailable,
  TGetCloseOutcomeCallback,
  TGetShowOutcome,
  TGetState,
  TGetItemId,
  TGetNerve,
  TCheckAvailable,
  TCheckRowIsNotInProgress,
  TCheckRowActive,
  TCheckRowHaveOutcome,
  TGetAction,
  TGetSubID,
  TGetError
} from './interfaces/IGlobalHelpers'

const getTitle: TGetTitle = (state: ICommonRowProps) => state.title
const getCrimeId: TGetCrimeId = (state: ICommonRowProps) => state.crimeID
const getIconClass: TGetIconClass = (state: ICommonRowProps) => state.iconClass
const getAdditionalInfo: TGetAdditionalInfo = (state: ICommonRowProps) => state.additionalInfo
const getMediaType: TGetMediaType = (state: ICommonRowProps) => state.mediaType
const getJailed: TGetJailed = (state: ICommonRowProps) => state.jailed
const getOutcome: TGetOutcome = (state: ICommonRowProps) => state.outcome
const getAttemptProgress: TGetAttemptProgress = (state: ICommonRowProps) => state.attemptProgress
const getCrimes: TGetCrimes = (state: ICommonRowProps) => state.crimesByType
const getCurrentRowID: TGetCurrentRowID = (state: ICommonRowProps) => state.currentRowID
const checkCurrentRow: TCheckCurrentRow = (state: ICommonRowProps) => state.isCurrentRow
const getLastSubCrimeID: TGetLastSubCrimeID = (state: ICommonRowProps) => state.lastSubCrimeID
const getResult: TGetResult = (state: ICommonRowProps) => state.result
const getStory: TGetStory = (state: ICommonRowProps) => state.story
const getTypeID: TGetTypeID = (state: ICommonRowProps) => state.typeID
const getButtonLabel: TGetButtonLabel = (state: ICommonRowProps) => state.buttonLabel || 'Attempt'
const getUserNerve: TGetUserNerve = (state: ICommonRowProps) => state.nerve || (state.user && state.user.nerve) || 0
const getDesktopManualLayout: TGetDesktopManualLayout = (state: ICommonRowProps) => state.isDesktopLayoutSetted
const getCrimeInfo: TGetCrimeInfo = (state: ICommonRowProps) => state.crimeInfo
const getAction: TGetAction = (state: ICommonRowProps) => state.action
const checkRowHaveOutcome: TCheckRowHaveOutcome = (state: ICommonRowProps) => state.isRowHaveOutcome
const checkRowActive: TCheckRowActive = (state: ICommonRowProps) => state.isRowActive
const checkRowIsNotInProgress: TCheckRowIsNotInProgress = (state: ICommonRowProps) => state.isRowNotInProgress
const checkAvailable: TCheckAvailable = (state: ICommonRowProps) => state.available
const getNerve: TGetNerve = (state: ICommonRowProps) => state.nerve
const getItemId: TGetItemId = (state: ICommonRowProps) => state.itemId
const getState: TGetState = (state: ICommonRowProps) => state.rowState
const getShowOutcome: TGetShowOutcome = (state: ICommonRowProps) => state.showOutcome
const getManualLayout: TGetManualLayout = (state: ICommonRowProps) => state.isDesktopLayoutSetted || state.isManualLayoutSetted
const getAvailable: TGetAvailable = (state: ICommonRowProps) => state.available
const getCloseOutcomeCallback: TGetCloseOutcomeCallback = (state: ICommonRowProps) => state.closeOutcome?.callback
const getSubID: TGetSubID = (state: ICommonRowProps) => state.subID
const getError: TGetError = (state: any) => state.infoBox?.msg

export {
  getTitle,
  getCrimeId,
  getIconClass,
  getAdditionalInfo,
  getMediaType,
  getJailed,
  getOutcome,
  getAttemptProgress,
  getCrimes,
  checkCurrentRow,
  getCurrentRowID,
  getLastSubCrimeID,
  getResult,
  getStory,
  getTypeID,
  getButtonLabel,
  getUserNerve,
  getDesktopManualLayout,
  getCrimeInfo,
  getManualLayout,
  getAvailable,
  getCloseOutcomeCallback,
  getShowOutcome,
  getState,
  getItemId,
  getNerve,
  checkAvailable,
  checkRowIsNotInProgress,
  checkRowActive,
  checkRowHaveOutcome,
  getAction,
  getSubID,
  getError
}
