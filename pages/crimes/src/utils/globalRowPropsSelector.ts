import { createSelector } from 'reselect'
import {
  checkAvailable,
  checkCurrentRow,
  checkRowActive,
  checkRowHaveOutcome,
  checkRowIsNotInProgress,
  getAction,
  getAdditionalInfo,
  getAvailable,
  getButtonLabel,
  getCloseOutcomeCallback,
  getLastSubCrimeID,
  getCurrentRowID,
  getIconClass,
  getItemId,
  getJailed,
  getManualLayout,
  getMediaType,
  getNerve,
  getOutcome,
  getShowOutcome,
  getState,
  getTitle,
  getTypeID,
  getCrimeId,
  getCrimeInfo,
  getSubID,
  getError
} from './globalSelectorsHelpers'

import { IButtonActionProps, IRowGlobalSelector } from './interfaces/IGlobalSelectors'

// reselect functions
const rowClassName = createSelector([checkRowActive], isRowActive => {
  return isRowActive ? 'active' : ''
})

const buttonConfig = createSelector(
  [getButtonLabel, getAction, getTypeID, getCrimeId, checkAvailable, getJailed, getError],
  (...props) => {
    const [buttonLabel, action, typeID, crimeID, available, jailed, error] = props

    const actionEnhanced = (extraActionProps: IButtonActionProps = {}) => {
      const { crimeIDCustom, subURL, subID } = extraActionProps || {}

      let attemptURL = `&step=attempt&typeID=${typeID}&crimeID=${crimeIDCustom || crimeID}`

      let ID = crimeID as string | number

      if (subURL) {
        attemptURL += subURL
      }

      if (subID) {
        ID += `_${subID}`
      }

      return action(attemptURL, ID)
    }

    return {
      label: buttonLabel,
      action: actionEnhanced,
      disabled: !available || jailed || !!error
    }
  }
)

const closeOutcomeConfig = createSelector([getCloseOutcomeCallback], closeOutcome => ({
  isActive: true,
  callback: closeOutcome
}))

const globalRowPropsSelectorFirstHalf = createSelector(
  [
    getCrimeId,
    getLastSubCrimeID,
    getCurrentRowID,
    checkCurrentRow,
    checkRowHaveOutcome,
    checkRowActive,
    checkRowIsNotInProgress,
    getNerve,
    getItemId,
    getState,
    getOutcome,
    getShowOutcome
  ],
  (
    crimeID,
    lastSubCrimeID,
    currentRowID,
    isCurrentRow,
    isRowHaveOutcome,
    isRowActive,
    isRowNotInProgress,
    nerve,
    itemId,
    state,
    outcome,
    showOutcome
  ) => ({
    crimeID,
    lastSubCrimeID,
    currentRowID,
    isCurrentRow,
    isRowHaveOutcome,
    isRowActive,
    isRowNotInProgress,
    nerve,
    itemId,
    state,
    outcome,
    showOutcome
  })
)

const globalRowPropsSelectorSecondHalf = createSelector(
  [
    getAdditionalInfo,
    getMediaType,
    getJailed,
    getManualLayout,
    getAvailable,
    rowClassName,
    buttonConfig,
    closeOutcomeConfig,
    getIconClass,
    getCrimeInfo,
    getTitle,
    getSubID
  ],
  (
    additionalInfo,
    mediaType,
    jailed,
    isManualLayoutSetted,
    available,
    className,
    button,
    closeOutcome,
    iconClass,
    crimeInfo,
    title,
    subID
  ) => ({
    additionalInfo,
    mediaType,
    jailed,
    isManualLayoutSetted,
    available,
    className,
    button,
    closeOutcome,
    iconClass,
    crimeInfo,
    title,
    subID
  })
)

const globalRowSelector = (state: any): IRowGlobalSelector => {
  return {
    ...globalRowPropsSelectorFirstHalf(state),
    ...globalRowPropsSelectorSecondHalf(state)
  }
}

export default globalRowSelector
