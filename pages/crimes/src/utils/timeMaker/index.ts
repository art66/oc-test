const timeMaker = (params?: { initTime?: number; isShort?: boolean; mountTime: number }) => {
  const { isShort, initTime, mountTime } = params || {}

  if (!initTime) {
    return null
  }

  const before = initTime * 1000
  const now = mountTime
  const diff = now - before

  const seconds = diff / 1000
  const minutes = seconds / 59
  const hours = minutes / 60
  const days = hours / 24

  const timeCases = {
    seconds: () => {
      return `${Math.ceil(seconds)} seconds`
    },
    minutes: () => {
      const minutesPure = Math.floor(minutes)
      const secondsPure = Math.ceil((minutesPure - minutes) * 60)

      return `${minutesPure} minutes, ${Math.abs(secondsPure)} seconds`
    },
    hours: () => {
      const hoursPure = Math.floor(hours)
      const minutesPure = Math.ceil((hoursPure - hours) * 60)

      return `${hoursPure} hours, ${Math.abs(minutesPure)} minutes`
    },
    days: () => {
      const daysPure = Math.floor(days)
      const hoursPure = Math.ceil((days - daysPure) * 24)

      return `${daysPure} days, ${hoursPure} hours`
    },
    month: () => {
      return `${Math.ceil(days)} days`
    }
  }

  const getNormalizedPrefix = (value, description) => {
    const dateReplacePattern = new RegExp('s$', 'i')

    if (Number(value) === 1) {
      return description.replace(dateReplacePattern, '')
    }

    return description
  }

  const postfixFixer = (timeString: string) => {
    const dateMatchPattern = new RegExp('([^,]+)', 'g')
    const dateToDescSlicePattern = new RegExp('([^\\s]+)', 'g')

    const [mainString, secondaryString] = timeString.match(dateMatchPattern)
    const [mainValue, mainDescription] = mainString?.match(dateToDescSlicePattern) || []

    const mainNormalizedPrefix = getNormalizedPrefix(mainValue, mainDescription)

    // in case we have a double value over the string (e.g. "1 minute, 30 seconds")
    if (secondaryString && dateToDescSlicePattern.test(secondaryString)) {
      const [secondaryValue, secondaryDescription] = secondaryString.match(dateToDescSlicePattern) || []
      const secondaryNormalizedPrefix = getNormalizedPrefix(secondaryValue, secondaryDescription)

      return `${mainValue} ${mainNormalizedPrefix}, ${secondaryValue} ${secondaryNormalizedPrefix}`
    }

    return `${mainValue} ${mainNormalizedPrefix}`
  }

  const timeShorter = (timeString: string) => {
    const pattern = new RegExp('(\\d*) (\\w)', 'i')

    const shortTime = timeString.match(pattern)[0]
    const shortTimeWithTruncatedSpace = shortTime.replace(pattern, '$1$2')

    return shortTimeWithTruncatedSpace
  }

  const timeController = () => {
    let currentCase = null

    if (seconds < 59) {
      currentCase = timeCases.seconds()
    } else if (minutes < 60) {
      currentCase = timeCases.minutes()
    } else if (hours < 24) {
      currentCase = timeCases.hours()
    } else if (days < 8) {
      currentCase = timeCases.days()
    } else {
      currentCase = timeCases.month()
    }

    return isShort ? timeShorter(currentCase) : postfixFixer(currentCase)
  }

  return timeController()
}

export default timeMaker
