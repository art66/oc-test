import { IAttemptProgress, IOutcome } from '../interfaces/IState'
import getStatuses from './getStatuses'

export interface IReturnEnhancer {
  isRowActive: boolean
  isRowNotInProgress: boolean
  isRowHaveOutcome: boolean
  isCurrentRow: boolean
  currentRowID: number | string
  showOutcome: boolean
  rowState: string
  outcome: IOutcome
}

const rowEnhancer = <T, >(attemptProgress: IAttemptProgress, row: T, outcome: IOutcome): T & IReturnEnhancer => {
  const { isRowActive, isRowNotInProgress, isRowHaveOutcome, isCurrentRow } = getStatuses(attemptProgress, row, outcome)

  const showOutcome = isRowNotInProgress && isRowHaveOutcome
  const rowState = showOutcome ? outcome.result : 'locked'
  const getOutcomes = showOutcome && outcome && outcome.story ? outcome : {} as IOutcome

  return {
    ...row,
    isRowActive,
    isRowNotInProgress,
    isRowHaveOutcome,
    isCurrentRow,
    currentRowID: attemptProgress.itemId,
    showOutcome,
    rowState,
    outcome: getOutcomes
  }
}

export default rowEnhancer
