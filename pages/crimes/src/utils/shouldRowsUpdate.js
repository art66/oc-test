const globalCrimeRowCheck = (prevProps, nextProps) => {
  const { mediaType, isRowActive } = prevProps

  // update row once user has been jailed/hospitalized
  if (nextProps.jailed) {
    return true
  }

  // update row if viewport is changed
  if (mediaType !== nextProps.mediaType) {
    return true
  }

  // update row that has left focus (another one is clicked)
  if (isRowActive !== nextProps.isRowActive) {
    return true
  }

  // update only active row once new outcome added
  if (nextProps.isRowHaveOutcome && nextProps.isRowActive) {
    return true
  }

  return false
}

export default globalCrimeRowCheck
