const getCurrentRootName = (): string => {
  const rootName = window.location.hash.toString().substr(2, window.location.hash.toString().length)

  if (rootName === '') {
    return 'default'
  }

  return rootName
}

export default getCurrentRootName
