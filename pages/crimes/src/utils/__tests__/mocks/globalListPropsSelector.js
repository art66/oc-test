const initialState = {
  searchforcash: {
    buttonLabel: 'Attempt',
    typeID: 1,
    startAttempt: () => {},
    statusTimersAction: () => {},
    attemptProgress: {
      inProgress: false,
      itemId: 1
    },
    currentRowID: 1,
    isCurrentRow: false,
    isDesktopLayoutSetted: false,
    showPlaceholder: true,
    outcome: {
      outcomeDesc: null,
      result: 'outcome is there',
      rewardsGive: {
        itemsRewards: null,
        userRewards: {
          money: 0
        }
      },
      story: []
    },
    result: 'result_there',
    story: ['some_story'],
    crimesByType: [
      {
        currentTemplate: 'searchforcash',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'searchforcash',
        crimeID: 1,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'searchforcash',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'searchforcash',
        crimeID: 2,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'searchforcash',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'searchforcash',
        crimeID: 3,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'searchforcash',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'searchforcash',
        crimeID: 4,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'searchforcash',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'searchforcash',
        crimeID: 5,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'searchforcash',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'searchforcash',
        crimeID: 6,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      }
    ],
    additionalInfo: null,
    currentType: {
      _id: {
        $oid: ''
      },
      ID: 0,
      typeID: 0,
      title: 'searchforcash',
      crimeRoute: 'searchforcash',
      active: 0,
      img: '',
      expForLevel100: 0
    },
    skillLevelTotal: 0,
    currentLevel: 0,
    lastSubCrimeID: 6,
    currentUserStats: {
      failedTotal: 0,
      attemptsTotal: 0,
      crimesByIDAttempts: {
        1: 0,
        2: 0,
        3: 0
      },
      successesTotal: 0,
      moneyFoundTotal: 0,
      jailedTotal: 0,
      itemsFound: {
        1: 0
      },
      critFailedTotal: 0,
      statsByType: [
        {
          successesTotal: 0
        }
      ],
      skill: 0,
      skillLevel: 0
    },
    currentUserStatistics: [
      {
        label: '',
        value: ''
      }
    ]
  },
  browser: {
    mediaType: 'desktop'
  },
  common: {
    jailed: false,
    user: {
      nerve: 2
    }
  }
}

export const stateCrime2 = {
  searchforcash: {
    ...initialState.searchforcash,
    attemptProgress: {
      inProgress: false,
      itemId: 2
    }
  },
  browser: {
    mediaType: 'desktop'
  },
  common: {
    jailed: false,
    user: {
      nerve: 2
    }
  }
}

export const stateCrime3 = {
  searchforcash: {
    ...initialState.searchforcash,
    attemptProgress: {
      inProgress: false,
      itemId: 3
    }
  },
  browser: {
    mediaType: 'desktop'
  },
  common: {
    jailed: false,
    user: {
      nerve: 2
    }
  }
}

export default initialState
