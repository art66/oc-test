const initialState = {
  title: 'Test',
  crimeID: 1,
  iconClass: 'icon',
  additionalInfo: {},
  mediaType: 'desktop',
  jailed: true,
  outcome: {},
  attemptProgress: {},
  crimesByType: [{}],
  isCurrentRow: true,
  currentRowID: 1,
  lastSubCrimeID: 1,
  result: 'result',
  story: ['story'],
  typeID: 1,
  buttonLabel: 'Label',
  user: {
    nerve: 2
  },
  crimeInfo: {
    gender: 'male',
    age: '20 - 35'
  },
  isDesktopLayoutSetted: true
}

export default initialState
