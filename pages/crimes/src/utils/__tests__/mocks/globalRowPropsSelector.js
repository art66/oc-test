const initialState = {
  crimeID: 1,
  isCurrentRow: true,
  isRowHaveOutcome: true,
  isRowActive: true,
  isRowNotInProgress: true,
  nerve: 1,
  itemId: 1,
  state: 'state',
  outcome: {},
  showOutcome: true,
  title: 'title',
  iconClass: 'icon',
  additionalInfo: {},
  mediaType: 'desktop',
  jailed: true,
  attemptProgress: {},
  crimesByType: [{}],
  currentRowID: 1,
  lastSubCrimeID: 1,
  result: 'result',
  story: ['story'],
  typeID: 1,
  buttonLabel: 'Label',
  user: {
    nerve: 2
  }
}

export default initialState
