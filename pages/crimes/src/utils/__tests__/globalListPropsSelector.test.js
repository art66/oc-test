import initialState, { stateCrime2, stateCrime3 } from './mocks/globalListPropsSelector'
import getGlobalSelector from '../globalListPropsSelector'

describe('selectors', () => {
  it('should update 1st crime of crimesList with outcome result obtained', () => {
    const { searchforcash, browser, common } = initialState
    const { crimes: newCrimesState } = getGlobalSelector(searchforcash, browser, common)

    expect(newCrimesState[0].isRowHaveOutcome).toBeTruthy()
    expect(newCrimesState[0].rowState).toBe('outcome is there')
    expect(newCrimesState).toMatchSnapshot()
  })
  it('should throw outcome to 1st crime and then, after state update, throw it to the 3thd', () => {
    const { searchforcash, browser, common } = stateCrime2
    const { crimes: newCrimesState } = getGlobalSelector(searchforcash, browser, common)

    expect(newCrimesState[0].isRowHaveOutcome).toBeFalsy()
    expect(newCrimesState[0].rowState).toBe('locked')

    expect(newCrimesState[1].isRowHaveOutcome).toBeTruthy()
    expect(newCrimesState[1].rowState).toBe('outcome is there')
    expect(newCrimesState).toMatchSnapshot()

    const { searchforcash: one, browser: two, common: three } = stateCrime3
    const { crimes: newCrimesState1 } = getGlobalSelector(one, two, three)

    expect(newCrimesState1[1].isRowHaveOutcome).toBeFalsy()
    expect(newCrimesState1[1].rowState).toBe('locked')
    expect(newCrimesState1[2].isRowHaveOutcome).toBeTruthy()
    expect(newCrimesState1[2].rowState).toBe('outcome is there')
    expect(newCrimesState1).toMatchSnapshot()
  })
  // eslint-disable-next-line max-statements
  it('should run and return all selectors', () => {
    const { searchforcash, browser, common } = stateCrime2
    const {
      outcome,
      isCurrentRow,
      currentRowID,
      lastSubCrimeID,
      attemptProgress,
      crimes,
      result,
      story,
      typeID,
      buttonLabel,
      userNerve,
      jailed,
      mediaType
    } = getGlobalSelector(searchforcash, browser, common)

    expect(typeof outcome === 'object').toBeTruthy()
    expect(typeof isCurrentRow === 'boolean').toBeTruthy()
    expect(typeof currentRowID === 'number').toBeTruthy()
    expect(typeof lastSubCrimeID === 'number').toBeTruthy()
    expect(typeof attemptProgress === 'object').toBeTruthy()
    expect(typeof crimes === 'object').toBeTruthy()
    expect(typeof result === 'string').toBeTruthy()
    expect(typeof story === 'object').toBeTruthy()
    expect(typeof typeID === 'number').toBeTruthy()
    expect(typeof buttonLabel === 'string').toBeTruthy()
    expect(typeof userNerve === 'number').toBeTruthy()
    expect(typeof jailed === 'boolean').toBeTruthy()
    expect(typeof mediaType === 'string').toBeTruthy()
    expect(typeof isDesktopLayoutSetted === 'boolean').toBeFalsy()
  })
})
