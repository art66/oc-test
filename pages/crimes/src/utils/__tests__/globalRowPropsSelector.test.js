import initialState from './mocks/globalRowPropsSelector'
import globalRowPropsSelector from '../globalRowPropsSelector'

describe('globalRowPropsSelector', () => {
  it('should return first-half global row Selector with all calculated props', () => {
    const globalSelector = globalRowPropsSelector(initialState)

    expect(globalSelector).toMatchSnapshot()
  })
  it('should return second-half global row Selector with all calculated props', () => {
    const globalSelector = globalRowPropsSelector(initialState)

    expect(globalSelector).toMatchSnapshot()
  })
})
