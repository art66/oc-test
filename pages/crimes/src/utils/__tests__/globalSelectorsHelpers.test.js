import initialState from './mocks/globalSelectorsHelpers'
import {
  getCrimeId,
  getIconClass,
  getAdditionalInfo,
  getMediaType,
  getJailed,
  getOutcome,
  getAttemptProgress,
  getCrimes,
  checkCurrentRow,
  getCurrentRowID,
  getLastSubCrimeID,
  getResult,
  getStory,
  getTypeID,
  getButtonLabel,
  getUserNerve,
  getTitle,
  getDesktopManualLayout,
  getCrimeInfo
} from '../globalSelectorsHelpers'

describe('globalSelectorsHelpers', () => {
  it('should return first-half selectors', () => {
    expect(getTitle(initialState)).toBeTruthy()
    expect(getCrimeId(initialState)).toBeTruthy()
    expect(getIconClass(initialState)).toBeTruthy()
    expect(getAdditionalInfo(initialState)).toBeTruthy()
    expect(getMediaType(initialState)).toBeTruthy()
    expect(getJailed(initialState)).toBeTruthy()
    expect(getOutcome(initialState)).toBeTruthy()
    expect(getAttemptProgress(initialState)).toBeTruthy()
    expect(getCrimes(initialState)).toBeTruthy()
  })
  it('should return second-half selectors', () => {
    expect(checkCurrentRow(initialState)).toBeTruthy()
    expect(getCurrentRowID(initialState)).toBeTruthy()
    expect(getLastSubCrimeID(initialState)).toBeTruthy()
    expect(getResult(initialState)).toBeTruthy()
    expect(getStory(initialState)).toBeTruthy()
    expect(getTypeID(initialState)).toBeTruthy()
    expect(getButtonLabel(initialState)).toBeTruthy()
    expect(getUserNerve(initialState)).toBeTruthy()
    expect(getDesktopManualLayout(initialState)).toBeTruthy()
    expect(getCrimeInfo(initialState)).toBeTruthy()
  })
})
