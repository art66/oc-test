import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { hideArrow, showArrow } from '../modules/actions'
import '../components/CrimesSlider/anim.scss'
import '../styles/main.scss'
import styles from './index.cssmodule.scss'

import SearchForCash from './SearchForCash'
import Bootlegging from './Bootlegging'
import Graffiti from './Graffiti'
import Shoplifting from './Shoplifting'
import Pickpocketing from './Pickpocketing'
import CardSkimming from './CardSkimming'
import Burglary from './Burglary'

import CrimesSlider from '../components/CrimesSlider'

const ANIMAITION_DURATION = 750

const onEntering = arrowSwitcher => {
  arrowSwitcher()
}

const onEntered = arrowSwitcher => {
  arrowSwitcher()
}

class CrimesRoutesContainer extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      isSlidesChange: false
    }

    this.ref = React.createRef()
  }

  _handleStart = () => {
    this.setState({
      isSlidesChange: true
    })
  }

  _handleFinish = () => {
    const { getShowArrows } = this.props

    this.setState({
      isSlidesChange: false
    })

    onEntered(getShowArrows)
  }

  render() {
    const { isSlidesChange } = this.state
    const { state, location, getHideArrows } = this.props
    const {
      common: { currentArrow }
    } = state
    const { pathname } = location

    return (
      <CrimesSlider isSlidesChange={isSlidesChange}>
        <TransitionGroup className={styles.carouselContainer}>
          <CSSTransition
            component='div'
            key={pathname}
            classNames={currentArrow}
            timeout={ANIMAITION_DURATION}
            onEnter={() => this._handleStart()}
            onEntering={() => onEntering(getHideArrows)}
            onExited={() => this._handleFinish()}
          >
            <div ref={this.ref} className={styles.crimeContainer}>
              <Switch location={location}>
                <Route exact path='/searchforcash' component={SearchForCash} />
                <Route exact path='/bootlegging' component={Bootlegging} />
                <Route exact path='/graffiti' component={Graffiti} />
                <Route exact path='/shoplifting' component={Shoplifting} />
                <Route exact path='/pickpocketing' component={Pickpocketing} />
                <Route exact path='/skimming' component={CardSkimming} />
                <Route exact path='/burglary' component={Burglary} />
              </Switch>
            </div>
          </CSSTransition>
        </TransitionGroup>
      </CrimesSlider>
    )
  }
}

CrimesRoutesContainer.propTypes = {
  state: PropTypes.object,
  location: PropTypes.object,
  getHideArrows: PropTypes.func,
  getShowArrows: PropTypes.func
}

CrimesRoutesContainer.defaultProps = {
  state: {},
  location: {},
  getHideArrows: () => {},
  getShowArrows: () => {}
}

const mapStateToProps = state => ({
  state
})

const mapDispatchToState = dispatch => ({
  getHideArrows: () => dispatch(hideArrow()),
  getShowArrows: () => dispatch(showArrow())
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(CrimesRoutesContainer)
