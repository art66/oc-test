import { IType } from '../../../interfaces/IModules'
import { SET_INSTALL_AREA, EXPIRED_ROW_REMOVED, FETCH_LOG_ATTEMPT, FETCH_LOG_SAVED, CLEAR_LOG, CRIME_DATA_UPDATE_ATTEMPT, CRIME_DATA_UPDATE_SAVED } from '../constants'
import { ISetInstallArea } from '../interfaces/IModules'
import { ILogItem, ISubCrime } from '../interfaces/IPayload'

// -----------------------------
// CARDSKIMMING ACTIONS
// -----------------------------
export const setInstallArea = (ID: number, available: boolean): ISetInstallArea => ({
  type: SET_INSTALL_AREA,
  available,
  ID
})

export const removeExpiredRow = (crimeID, subID) => ({
  type: EXPIRED_ROW_REMOVED,
  crimeID,
  subID
})

export const fetchLogAttempt = (crimeID: string, subID: string) => ({
  type: FETCH_LOG_ATTEMPT,
  subID,
  crimeID
})

export const fetchLogSaved = (log: ILogItem[]): { log: ILogItem[] } & IType => ({
  log,
  type: FETCH_LOG_SAVED
})

export const clearLog = () => ({
  type: CLEAR_LOG
})

export const subCrimeDataUpdateAttempt = (crimeID: string, subID: string) => ({
  type: CRIME_DATA_UPDATE_ATTEMPT,
  subID,
  crimeID
})

export const subCrimeDataUpdateSaved = (subCrimeData: ISubCrime) => ({
  subCrimeData,
  type: CRIME_DATA_UPDATE_SAVED
})
