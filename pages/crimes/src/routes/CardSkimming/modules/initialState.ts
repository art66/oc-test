import { IInitialState } from '../interfaces/IInitialState'

const initialState: IInitialState = {
  attemptProgress: {
    inProgress: false
  },
  typeID: 6,
  isCurrentRow: false,
  showPlaceholder: true,
  crimesByType: {
    installSkimming: {
      areas: [
        { title: 'Bus Station', nerve: 1, crimeID: 49, available: false },
        { title: 'Subway Station', nerve: 1, crimeID: 52, available: false },
        { title: 'College Campus', nerve: 1, crimeID: 53, available: false },
        { title: 'Gas Station', nerve: 1, crimeID: 54, available: false },
        { title: 'Post Office', nerve: 1, crimeID: 55, available: false },
        { title: 'Airport Terminal', nerve: 1, crimeID: 56, available: false },
        { title: 'Casino Lobby', nerve: 1, crimeID: 57, available: false },
        { title: 'Bank Branch', nerve: 1, crimeID: 58, available: false }
      ],
      buttonTitle: 'Install',
      crimeID: 49,
      iconClass: 'jewelry_shop',
      nerve: 1,
      requirements: {
        items: [
          { label: 'Spy Camera', value: '1123', count: 0, available: false },
          { label: 'Card Skimmer', value: '1125', count: 0, available: false }
        ]
      },
      title: 'Install Card Skimmer'
    },
    sellCards: {
      additionalInfo: {
        collectedCards: 0
      },
      available: true,
      buttonTitle: 'Sell',
      crimeID: 51,
      iconClass: 'jewelry_shop',
      nerve: 1,
      requirements: {
        items: [
          { label: 'Personal Computer', value: '61', count: 2, available: true },
          { label: 'Laptop', value: '154', count: 1, available: true }
        ]
      },
      title: 'Sell credit cards'
    }
  },
  additionalInfo: null,
  exp: null,
  hideTestPanel: false,
  hubRedirect: false,
  lastSubCrimeID: null,
  lastSubCrimeIDByType: null,
  skillLevelTotal: null,
  isAjaxRequest: true,
  rfcv: null,
  time: null,
  tutorial: {
    text: 'Doing crimes can earn you money and items. The more crimes you do, the more experience you will gain, making it easier for you to do harder crimes. Make sure you understand that being caught for your crimes could greatly reduce your crime experience, making it even harder to do crimes. As you advance, you will be able to do even more crimes, earning you big money.'
  },
  user: {
    playername: null
  },
  currentUserStats: {
    enhancer: null,
    skill: 1,
    skillLevel: 0,
    statsByType: {
      6: {
        successesTotal: 0
      }
    }
  },
  currentUserStatistics: [
    { label: 'Level & EXP', value: null },
    { label: 'Enhancer', value: null },
    { label: 'Successes', value: null },
    { label: 'Fails', value: null },
    { label: 'Critical fails', value: null },
    { label: 'Money made', value: null },
    { label: 'Skimmers installed', value: null },
    { label: 'Skimmers lost', value: null },
    { label: 'Skimmers recovered', value: null },
    { label: 'Card details sold', value: null },
    { label: 'Card details lost', value: null },
    { label: 'Card details recovered', value: null },
    { label: 'Oldest recovered skimmer', value: null },
    { label: 'Most lucrative skimmer', value: null },
    { label: 'Bus station skims', value: null },
    { label: 'Subway station skims', value: null },
    { label: 'College campus skims', value: null },
    { label: 'Gas station skims', value: null },
    { label: 'Post office skims', value: null },
    { label: 'Airport terminal skims', value: null },
    { label: 'Casino lobby skims', value: null },
    { label: 'Back branch skims', value: null }
  ]
}

export default initialState
