import isValue from '@torn/shared/utils/isValue'

import initialState from './initialState'

import {
  DATA_FETCHED, ATTEMPT_START, ATTEMPT_FETCHED, CRIME_DATA_UPDATE_ATTEMPT, CRIME_DATA_UPDATE_SAVED, SET_INSTALL_AREA, EXPIRED_ROW_REMOVED, FETCH_LOG_ATTEMPT, FETCH_LOG_SAVED, CLEAR_LOG
} from '../constants'

import { IFetchAction, ISetInstallArea } from '../interfaces/IModules'
import { ISkimmingState } from '../interfaces/IState'
import { IAttemptStart } from '../../../interfaces/IModules'

import checkInstallCommitAvailability from '../utils/checkInstallCommitAvailability'
import crimesSeparator from '../utils/crimesSeparator'

import addNewCrimes from '../utils/addNewCrimes'
import updatePrevCrimes from '../utils/updatePrevCrimes'
// import attemptAdapter from '../utils/attemptAdapter'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [DATA_FETCHED]: (state: ISkimmingState, action: IFetchAction): ISkimmingState => ({
    ...state,
    ...action.json.DB,
    showPlaceholder: false,
    attemptProgress: {
      ...state.attemptProgress,
      inProgress: false
    },
    ...crimesSeparator(action.json.DB.crimesByType, {
      installSkimming: state.installSkimming,
      sellCards: state.sellCards,
      crimesByType: (state.crimesByType as any).subCrimes
    }, true),
    installSkimming: {
      ...action.json.DB.crimesByType.installSkimming,
      available: checkInstallCommitAvailability(action.json.DB.crimesByType.installSkimming.areas)
    }
  }),
  [ATTEMPT_START]: (state: ISkimmingState, action: IAttemptStart) => ({
    ...state,
    attemptProgress: {
      itemId: action.itemId,
      inProgress: true
    },
    currentRowID: action.itemId
  }),
  [ATTEMPT_FETCHED]: (state: ISkimmingState, action: any) => ({
    ...state,
    ...action.json.DB,
    attemptProgress: {
      ...state.attemptProgress,
      inProgress: false
    },
    installSkimming: {
      ...state.installSkimming,
      requirements: action.json.DB.additionalInfo?.items ? {
        ...state.installSkimming.requirements,
        items: action.json.DB.additionalInfo.items
      } : state.installSkimming.requirements,
      ...action.json.DB.crimesByType?.installSkimming || {}
    },
    sellCards: {
      ...state.sellCards,
      additionalInfo: isValue(action.json.DB.additionalInfo?.collectedCards) ? {
        ...state.sellCards.additionalInfo,
        collectedCards: action.json.DB.additionalInfo.collectedCards
      } : state.sellCards.additionalInfo,
      ...action.json.DB.crimesByType?.sellCards || {}
    },
    crimesByType: action.json.DB.crimesByType?.subCrimes ?
      addNewCrimes(state.crimesByType, action.json.DB.crimesByType?.subCrimes) :
      updatePrevCrimes({
        crimes: state.crimesByType,
        outcome: action.json.DB.outcome,
        lastActiveID: state.attemptProgress.itemId
      })
  }),
  [CRIME_DATA_UPDATE_ATTEMPT]: (state: ISkimmingState, action: any) => ({
    ...state,
    crimesByType: state.crimesByType.map(crime => {
      const crimeID = `${crime.crimeID}_${crime.subID}`
      const activeCrimeID = `${action.crimeID}_${action.subID}`

      return crimeID === activeCrimeID ? {
        ...crime,
        pollInProgress: true
      } : crime
    })
  }),
  [CRIME_DATA_UPDATE_SAVED]: (state: ISkimmingState, action: any) => ({
    ...state,
    crimesByType: state.crimesByType.map(crime => {
      const prevCrimeID = `${crime.crimeID}_${crime.subID}`
      const nextCrimeID = `${action.subCrimeData.crimeID}_${action.subCrimeData.subID}`

      return prevCrimeID === nextCrimeID ? {
        ...action.subCrimeData,
        pollInProgress: false
      } : crime
    })
  }),
  [SET_INSTALL_AREA]: (state: ISkimmingState, action: ISetInstallArea) => ({
    ...state,
    installSkimming: {
      ...state.installSkimming,
      lastActiveArea: action.ID,
      available: action.available
    }
  }),
  [EXPIRED_ROW_REMOVED]: (state: ISkimmingState, action: any) => {
    const findExpiredRow = crime => `${crime.crimeID}_${crime.subID}` !== `${action.crimeID}_${action.subID}`

    return {
      ...state,
      crimesByType: state.crimesByType.filter(findExpiredRow)
    }
  },
  [FETCH_LOG_ATTEMPT]: (state: ISkimmingState) => ({
    ...state,
    isLogFetch: true,
    log: null
  }),
  [FETCH_LOG_SAVED]: (state: ISkimmingState, action: any) => ({
    ...state,
    isLogFetch: false,
    log: action.log
  }),
  [CLEAR_LOG]: (state: ISkimmingState) => ({
    ...state,
    log: null
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
export default (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
