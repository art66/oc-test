import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { attemptStart, userJailed, attemptFetched, showDebugInfo } from '../../../../modules/actions'
import { ROOT_URL } from '../../../../constants'

export function* attempt({ url, itemId, isCurrentRow }: any) {
  try {
    yield put(attemptStart('skimming', itemId, isCurrentRow))
    const json = yield fetchUrl(`${ROOT_URL}${url}`, null, false)

    if (json?.DB?.error) {
      throw new Error(json?.DB?.error)
    }

    if (!['success', 'failed'].includes(json?.DB?.outcome?.result)) {
      yield put(userJailed(true))
    }

    yield put(attemptFetched('skimming', json))

    // yield put(updateExp())
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default attempt
