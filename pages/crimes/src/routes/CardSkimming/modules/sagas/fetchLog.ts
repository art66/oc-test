import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { showDebugInfo } from '../../../../modules/actions'
import { fetchLogSaved } from '../actions'
import { ILogItem } from '../../interfaces/IPayload'

export interface ILogPayload {
  DB: {
    error?: string
    log: ILogItem[]
  }
}

function* fetchLog({ crimeID, subID }: any) {
  try {
    const url = `sid=crimesData&step=crimesList&crimeID=50&type=info&typeID=6&crimeID=${crimeID}&subID=${subID}`

    const json: ILogPayload = yield fetchUrl(`/loader2.php?${url}`, null, false)

    if (json?.DB?.error) {
      throw new Error(json.DB.error)
    }

    yield put(fetchLogSaved(json.DB.log))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default fetchLog
