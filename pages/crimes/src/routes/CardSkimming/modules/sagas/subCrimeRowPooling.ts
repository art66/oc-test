import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { ROOT_URL } from '../../../../constants'

import { showDebugInfo } from '../../../../modules/actions'
import { subCrimeDataUpdateSaved } from '../actions'
import getState from './helpers/getState'

export function* subCrimeRowPooling({ crimeID, subID }) {
  try {
    const json = yield fetchUrl(`${ROOT_URL}&step=crimesList&typeID=6&type=poll&crimeID=${crimeID}&subID=${subID}`, null, false)

    if (json.DB?.error) {
      yield put(showDebugInfo({ msg: json.DB?.error }))

      return
    }

    const { skimming } = yield getState()
    const { attemptProgress } = skimming

    if (attemptProgress.inProgress) {
      return
    }

    yield put(subCrimeDataUpdateSaved(json.DB.subCrimeData))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default subCrimeRowPooling
