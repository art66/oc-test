import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import { fetchData } from '../../modules/actions'
import { typeID } from './constants'
import reducer from './modules/reducers'
import saga from './modules/sagas'
import rootStore from '../../store/createStore'
import Placeholder from './components/Skeleton'
import '../../styles/shoplifting.cssmodule.scss'

const Preloader = () => <Placeholder type='full' />

const CardSkimmingContainer = Loadable({
  loader: async () => {
    const CardSkimming = await import(/* webpackChunkName: "skimming" */ './containers/App')

    injectReducer(rootStore, { key: 'skimming', reducer })
    injectSaga({ key: 'skimming', saga })

    const coreData = fetchData('skimming', `&step=crimesList&typeID=${typeID}`) as never

    rootStore.dispatch(coreData)

    return CardSkimming
  },
  loading: Preloader
})

export default CardSkimmingContainer
