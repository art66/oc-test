import React from 'react'
import { connect } from 'react-redux'
import { showDropDownPanel, toggleArrows } from '../../../../modules/actions'

import BannerGlobal from '../../../../components/Banner'

import stylesCS from './index.cssmodule.scss'
import '../../../../styles/animations.scss'

import generateCardNumber from '../../utils/generateCardNumber'

export interface IBannersProps {
  dropDownPanelToShow: Function
  toggleArrow: Function
}

export class Banner extends React.Component<IBannersProps> {
  static defaultProps = {
    dropDownPanelToShow: () => {},
    toggleArrow: () => {}
  }

  _innerAnimation = () => {
    const numbersToRender = Array.from(Array(50).keys()).map(key => {
      return (
        <li key={key}>{generateCardNumber()}</li>
      )
    })

    return (
      <>
        <div className={stylesCS.numbersWrap}>
          <ul className={stylesCS.numberList}>
            {numbersToRender}
          </ul>
        </div>
        <div className={stylesCS.cashMachineLight} />
      </>
    )
  }

  render() {
    const { dropDownPanelToShow, toggleArrow } = this.props

    return (
      <BannerGlobal dropDownPanelToShow={dropDownPanelToShow} toggleArrows={toggleArrow}>
        <div className={stylesCS.crimesCardSkimming}>{this._innerAnimation()}</div>
      </BannerGlobal>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  dropDownPanelToShow: () => dispatch(showDropDownPanel()),
  toggleArrow: () => dispatch(toggleArrows())
})

export default connect(null, mapDispatchToProps)(Banner)
