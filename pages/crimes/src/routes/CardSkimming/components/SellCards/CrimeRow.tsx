import React from 'react'
import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

import { CrimeRow as CrimeRowGlobal } from '../../../../components'
import ItemCell from '../../../../components/ItemCell'
import Arrow from '../Arrow'

import selectorProps from './selectors'

import globalStyles from '../../../../styles/card-skimming.cssmodule.scss'
import styles from './index.cssmodule.scss'

import { IProps, IState } from './interfaces'

class SellCards extends React.Component<IProps, IState> {
  private _animateFrameId: any

  constructor(props: IProps) {
    super(props)

    this.state = {
      cards: 0,
      cardsFull: 0,
      sign: 'sum',
      updateType: '',
      isInitDone: false
    }
  }

  static getDerivedStateFromProps(nextProps: IProps, nextState: IState) {
    if (nextState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    if (nextState.cardsFull !== nextProps.additionalInfo.collectedCards) {
      return {
        cards: nextState.cardsFull,
        cardsFull: nextProps.additionalInfo.collectedCards,
        sign: nextProps.additionalInfo.collectedCards > nextState.cardsFull ? 'sum' : 'minus',
        updateType: 'props',
        isInitDone: true
      }
    }

    return null
  }

  componentDidMount() {
    this._initializeTooltips()
  }

  componentDidUpdate(_, prevState) {
    if (this.state.isInitDone && prevState.cardsFull !== this.state.cardsFull) {
      this.animate()
    }
  }

  componentWillUnmount() {
    cancelAnimationFrame(this._animateFrameId)
  }

  _getFPSInterval = () => {
    const { cardsFull, cards, sign } = this.state

    const cardsToSlice = sign === 'minus' ? Math.abs(cardsFull - cards) : (cardsFull - cards)

    return 300 / cardsToSlice
  }

  animate = () => {
    let now = 0
    let then = window.performance.now()
    let elapsed = 0

    const step = newtime => {
      const { cardsFull, cards, sign } = this.state

      if (cards === cardsFull) {
        return
      }

      now = newtime
      elapsed = now - then

      if (elapsed > this._getFPSInterval()) {
        then = now - (elapsed % this._getFPSInterval())


        this.setState(prevState => ({
          cards: sign === 'minus' ? prevState.cards - 1 : prevState.cards + 1,
          updateType: 'state'
        }))
      }

      this._animateFrameId = requestAnimationFrame(step)
    }

    this._animateFrameId = requestAnimationFrame(step)
  }

  _initializeTooltips = () => {
    const { crimeID, requirements } = this.props
    const { items } = requirements || {}

    const gadget = items.find(item => item.available)

    const tooltipsItems = []

    tooltipsItems.push({
      child: `${gadget.available ? 'Requires: ' : 'Required: '}${gadget.label}`,
      ID: `${crimeID}-${gadget.value}`
    })

    this._renderTooltips(tooltipsItems)
  }

  _renderTooltips = (tooltips = []) => {
    tooltips.forEach(tooltipItem => tooltipSubscriber.subscribe(tooltipItem))
  }

  _renderCollectCardsSection = () => {
    const { cards, cardsFull } = this.state

    return (
      <div className={`${globalStyles.firstSection} ${styles.collectWrap} ${globalStyles.dlm}`}>
        <span className={styles.textCount}>{cards === 0 ? cardsFull : cards}</span>
        <span className={styles.textDesc}>Card Details</span>
      </div>
    )
  }

  _renderItemsSection = () => {
    const { crimeID, requirements } = this.props

    const gadget = requirements.items.find(item => item.available)

    return (
      <div className={`${globalStyles.secondSection} ${globalStyles.dlm}`}>
        <div className={globalStyles.cellWrap}>
          <ItemCell isMock={true} />
        </div>
        <div className={globalStyles.cellWrap}>
          <Arrow type={!gadget.available ? 'shadow' : 'full'} shape='checked' />
          <ItemCell
            imgAlt={gadget.value}
            ID={`${crimeID}-${gadget.value}`}
            imgSrc={`/images/items/${gadget.value}/${!gadget ? 'blank' : 'large'}.png`}
          />
        </div>
      </div>
    )
  }

  _renderRow = () => {
    const { error } = this.props
    const { throwGlobalCrimeRowProps: props } = selectorProps(this.props)

    const modifiedGlobalProps = {
      ...props,
      button: {
        ...props.button,
        disabled: props.button.disabled || !!error
      }
    }

    return (
      <CrimeRowGlobal {...modifiedGlobalProps} styles={globalStyles}>
        {this._renderCollectCardsSection()}
        {this._renderItemsSection()}
      </CrimeRowGlobal>
    )
  }

  render() {
    return this._renderRow()
  }
}

export default SellCards
