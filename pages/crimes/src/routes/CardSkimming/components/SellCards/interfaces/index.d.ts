import { ISellCards } from '../../../interfaces/IPayload'

export interface IProps extends ISellCards {
  collectedCards?: number
}

export interface IState {
  isInitDone: boolean
  updateType: '' | 'state' | 'props'
  sign: 'sum' | 'minus'
  cards: number
  cardsFull: number
  forceRemove?: boolean
  attemptDisabled?: boolean
  isRowCommitted?: boolean
  isRowInstalled?: boolean
}

export interface ISellCardsOwnSelector {
  crimeID: number
}

