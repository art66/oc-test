import { connect } from 'react-redux'
import SellCards from './CrimeRow'
import { closeOutcome } from '../../../../modules/actions'

const mapDispatchToProps = dispatch => ({
  collapseOutcome: () => dispatch(closeOutcome())
})

export default connect(null, mapDispatchToProps)(SellCards)
