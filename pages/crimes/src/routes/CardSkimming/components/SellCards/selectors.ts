import { createSelector } from 'reselect'

import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getCrimeId } from '../../../../utils/globalSelectorsHelpers'
import { IRowSelector } from '../../../../utils/interfaces/IGlobalRowSelector'
import { IRowGlobalSelector } from '../../../../utils/interfaces/IGlobalSelectors'
import { ISellCardsAdditionalInfo } from '../../interfaces/IPayload'
import { IProps, ISellCardsOwnSelector } from './interfaces'

const BUTTON_LABEL = 'Sell'

const ownCrimeRowProps = createSelector([getCrimeId], crimeID => ({
  crimeID
}))

const globalCrimeRowPropsModified = createSelector(
  [globalRowPropsSelector],
  (globalProps: IRowGlobalSelector) => {
    const { collectedCards } = globalProps.additionalInfo as ISellCardsAdditionalInfo

    return {
      ...globalProps,
      button: {
        ...globalProps.button,
        label: BUTTON_LABEL,
        disabled: !collectedCards || Number(collectedCards) === 0 || !collectedCards && globalProps.button.disabled
      }
    }
  }
)

const selectorProps = (props: IProps): IRowSelector<ISellCardsOwnSelector> => ({
  ownCrimeRowProps: ownCrimeRowProps(props),
  throwGlobalCrimeRowProps: globalCrimeRowPropsModified(props)
})

export default selectorProps
