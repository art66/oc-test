import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

import { IAttemptProgress, ICommonRowProps } from '../../../../../interfaces/IState'
import { IInstallSkimming, ISellCards, ISubCrime } from '../../../interfaces/IPayload'

export interface IDispatchProps {
  startAttempt: Function
}

export interface IMapToStateProps {
  error: string
  jailed: boolean
  typeID: number
  attemptProgress: IAttemptProgress
  lastSubCrimeID: number
  isCurrentRow: boolean
  currentRowID: number
  mediaType: TMediaType
  isDesktopLayoutSetted: boolean
  installSkimming: IInstallSkimming & ICommonRowProps
  sellCards: ISellCards & ICommonRowProps
  crimesByType: (ISubCrime & ICommonRowProps)[]
  startPooling: () => void
}

export interface IProps extends IMapToStateProps, IDispatchProps {}
