import React, { PureComponent } from 'react'

import NoSubCrimesPlaceholder from '../NoSubCrimesPlaceholder'
import InstallSkimming from '../InstallSkimming'
import SellCards from '../SellCards'
import Areas from '../Areas'

import styles from './index.cssmodule.scss'
import '../../../../styles/subcrimes.scss'
import { IProps } from './interfaces'

export class CrimesList extends PureComponent<IProps, any> {
  _attempt = (url: string, itemId: string | number) => {
    const { startAttempt, currentRowID } = this.props
    const onCurrentRow = String(currentRowID) === String(itemId)

    startAttempt(url, itemId, onCurrentRow)
  }

  _buildStaticSubCrimes = () => {
    const { attemptProgress: { itemId }, installSkimming, sellCards, mediaType, isDesktopLayoutSetted, typeID, jailed, error } = this.props

    if (!installSkimming || !sellCards) {
      return null
    }

    return (
      <div className={styles.staticRoutesWrap}>
        <InstallSkimming
          {...installSkimming}
          action={this._attempt}
          mediaType={mediaType}
          isManualLayoutSetted={isDesktopLayoutSetted}
          itemId={itemId}
          typeID={typeID}
          jailed={jailed}
          error={error}
        />
        <SellCards
          {...sellCards}
          // collectedCards={this.state.cards}
          action={this._attempt}
          mediaType={mediaType}
          isManualLayoutSetted={isDesktopLayoutSetted}
          itemId={itemId}
          typeID={typeID}
          jailed={jailed}
          error={error}
        />
      </div>
    )
  }

  _buildDynamicSubCrimes = () => {
    const { attemptProgress, crimesByType, typeID, jailed, mediaType, error } = this.props

    if (!crimesByType || crimesByType.length === 0) {
      return <NoSubCrimesPlaceholder />
    }

    return (
      <Areas
        attempt={this._attempt}
        itemId={attemptProgress.itemId}
        crimesByType={crimesByType}
        mediaType={mediaType}
        typeID={typeID}
        jailed={jailed}
        error={error}
      />
    )
  }

  render() {
    return (
      <div className='main-container'>
        <div className={`subcrimes-list ${styles.subcrimesList}`}>
          <div>{this._buildStaticSubCrimes()}</div>
          <hr className={styles.delimiter} />
          <div className={styles.dynamicRoutesWrap}>{this._buildDynamicSubCrimes()}</div>
        </div>
      </div>
    )
  }
}

export default CrimesList
