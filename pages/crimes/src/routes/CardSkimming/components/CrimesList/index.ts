import { connect } from 'react-redux'
import CrimesList from './CrimesList'

import { attempt } from '../../../../modules/actions'
import rowEnhancer from '../../../../utils/rowEnhancer'

import getGlobalSelector from '../../../../utils/globalListPropsSelector'
import { IState } from '../../../../interfaces/IController'
import { IMapToStateProps } from './interfaces'
import { ISellCards, IInstallSkimming, ISubCrime } from '../../interfaces/IPayload'

const mapStateToProps = ({ skimming, browser, common }: IState): IMapToStateProps => ({
  ...getGlobalSelector(skimming, browser, common) as any,
  installSkimming: rowEnhancer<IInstallSkimming>(skimming.attemptProgress, skimming.installSkimming, skimming.outcome),
  sellCards: rowEnhancer<ISellCards>(skimming.attemptProgress, skimming.sellCards, skimming.outcome),
  crimesByType: skimming.crimesByType?.map(crime => rowEnhancer<ISubCrime>(skimming.attemptProgress, crime, skimming.outcome))
})

const mapDispatchToProps = dispatch => ({
  startAttempt: (url, itemId, isCurrentRow) => dispatch(attempt('skimming', url, itemId, isCurrentRow))
})

export default connect(mapStateToProps, mapDispatchToProps)(CrimesList)
