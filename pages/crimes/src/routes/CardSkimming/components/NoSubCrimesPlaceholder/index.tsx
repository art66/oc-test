import React from 'react'

import NoSubCrimesPlaceholder from '../../../../components/NoSubCrimesPlaceholder'

interface IProps {
  isBackdrop?: boolean
}

class NoSubCrimes extends React.PureComponent<IProps> {
  render() {
    const { isBackdrop } = this.props

    return (
      <NoSubCrimesPlaceholder
        isBackdrop={isBackdrop}
        text='You currently have no card skimmers installed'
      />
    )
  }
}

export default NoSubCrimes
