import Banner from './Banner'
import CrimesList from './CrimesList/index'
import Skeleton from './Skeleton'

export { Banner, CrimesList, Skeleton }
