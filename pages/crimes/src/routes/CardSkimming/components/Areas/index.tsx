import React from 'react'
import { connect } from 'react-redux'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

import Area from './CrimeRow'
import Log from './Log'

import { closeOutcome } from '../../../../modules/actions'

import { ICommonRowProps } from '../../../../interfaces/IState'
import { ILogItem, ISubCrime } from '../../interfaces/IPayload'
import { TAttempt } from '../../interfaces/IModules'
import { clearLog, fetchLogAttempt, subCrimeDataUpdateAttempt } from '../../modules/actions'

export interface IProps {
  runPooling: (crimeID: number, subID: string) => void
  runFetchLogAttempt: (crimeID: string, subID: string) => void
  runClearLog: () => void
  isManualLayoutSetted: boolean
  mediaType: TMediaType
  error: string
  jailed: boolean
  attempt: TAttempt
  typeID: number
  itemId: number
  log: ILogItem[]
  crimesByType: (ISubCrime & ICommonRowProps)[]
}

export interface IState {
  isLogActive: boolean
  currentRowID: string
  timeMount: number
  timeMountStatic: number
}

class Areas extends React.Component<IProps, IState> {
  private _timerID: any

  constructor(props: IProps) {
    super(props)

    this.state = {
      currentRowID: null,
      isLogActive: false,
      timeMount: new Date().getTime(),
      timeMountStatic: new Date().getTime()
    }
  }

  componentDidMount() {
    this._runUpdateTimer()
    document.addEventListener('click', this._handleLogClick)

    this.setState({
      timeMount: new Date().getTime()
    })
  }

  componentWillUnmount() {
    this._removeUpdateTimer()
    document.removeEventListener('click', this._handleLogClick)
  }

  _runUpdateTimer = () => {
    this._timerID = setInterval(() => {
      const { runPooling, crimesByType } = this.props

      const timeNow = new Date().getTime()

      this.setState({
        timeMount: new Date().getTime()
      })

      // update crime by its timestamp (e.g. personal subCrime row pooling)
      crimesByType.forEach(({ pollInProgress, crimeInfo, crimeID, subID }) => {
        if (pollInProgress || timeNow < crimeInfo.timeWhenUpdated * 1000 || crimeInfo.progress === 'found') {
          return
        }

        runPooling(crimeID, subID)
      })
    }, 1000)
  }

  _removeUpdateTimer = () => {
    clearInterval(this._timerID)
  }

  _handleLogClick = ({ target }) => {
    const { log, runFetchLogAttempt, runClearLog } = this.props

    if (target.id === 'areaLog') {
      this.setState(prevState => ({
        currentRowID: target.dataset.subid,
        isLogActive: prevState.currentRowID !== target.dataset.subid ? true : !prevState.isLogActive
      }))

      runFetchLogAttempt(target.dataset.id, target.dataset.subid)

      return
    }

    this.setState({
      isLogActive: false
    })

    log && runClearLog()
  }

  render() {
    const { isLogActive, currentRowID, timeMount, timeMountStatic } = this.state
    const { log, attempt, crimesByType, typeID, jailed, error, mediaType, isManualLayoutSetted } = this.props

    return crimesByType.map(row => {
      const isLogAppear = isLogActive && currentRowID === row.subID

      return (
        <Area
          {...row}
          log={log}
          mediaType={mediaType}
          isExpired={row.isExpired}
          isDisabledButton={row.isDisabledButton}
          isManualLayoutSetted={isManualLayoutSetted}
          key={`${row.crimeID}_${row.subID}`}
          action={attempt}
          itemId={`${row.crimeID}_${row.subID}`}
          cards={row.crimeInfo?.cards}
          progress={row.crimeInfo?.progress}
          timeMount={timeMount}
          timeMountStatic={timeMountStatic}
          typeID={typeID}
          jailed={jailed}
          error={error}
        >
          {isLogAppear && <Log log={log} timeMount={timeMount} timeFound={row.crimeInfo.timeFound} timeMountStatic={timeMountStatic} />}
        </Area>
      )
    })
  }
}

const mapStateToProps = state => ({
  log: state.skimming.log,
  isManualLayoutSetted: state.common.isDesktopLayoutSetted
})

const mapDispatchToProps = {
  runClearLog: clearLog,
  runFetchLogAttempt: fetchLogAttempt,
  collapseOutcome: closeOutcome,
  runPooling: subCrimeDataUpdateAttempt
}

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Areas)
