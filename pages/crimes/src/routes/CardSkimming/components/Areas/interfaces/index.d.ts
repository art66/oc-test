import { ILogItem, ISubCrime } from '../../../interfaces/IPayload'

export interface IProps extends ISubCrime {
  log: ILogItem[]
  error?: string
  isFresh?: boolean
  children: JSX.Element
  timeMount: number
  timeMountStatic: number
  clearExpiredRow: (crimeID: string | number, subID: string) => void
}

export interface IState {
  isAppear: boolean
  attemptDisabled: boolean
}
