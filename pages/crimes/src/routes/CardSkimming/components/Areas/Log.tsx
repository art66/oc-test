import React from 'react'
import classnames from 'classnames'

import Loader from '@torn/shared/components/AnimationLoad'

import timeMaker from '../../../../utils/timeMaker'

import { ILogItem } from '../../interfaces/IPayload'

import styles from './log.cssmodule.scss'

export interface IProps {
  log: ILogItem[]
  timeFound: number
  timeMount: number
  timeMountStatic: number
}

class Log extends React.Component<IProps, { isReverseAppear: boolean }> {
  private _ref: React.RefObject<HTMLDivElement>

  constructor(props: IProps) {
    super(props)

    this._ref = React.createRef()
  }

  _renderLog = () => {
    const { log, timeMount, timeFound, timeMountStatic } = this.props

    const getLogItems = log?.map(({ label, color, time }, index) => {
      const timeData = {
        initTime: time,
        mountTime: timeFound ? timeMountStatic : timeMount,
        isShort: true
      }

      return (
        <div key={`${index}_${label}`} className={styles.logItem}>
          <span className={styles.index}>#{log.length - index}</span>
          <span className={`${styles.label} ${styles[color]}`}>{label}</span>
          <span className={styles.time}>{timeMaker(timeData)}</span>
        </div>
      )
    })

    return getLogItems
  }

  render() {
    const logData = this._renderLog()

    const isNoLogData = !logData || logData?.length === 0
    const body = isNoLogData ? <Loader dotsColor='black' isAdaptive={true} dotsCount={5} /> : logData

    const classes = classnames({
      [styles.noScrollBar]: !logData || logData?.length < 8,
      [styles.logWrap]: true
    })

    return (
      <div ref={this._ref} className={classes}>
        <div className={styles.logItems}>
          <div className={`${styles.logScrollHelper} ${isNoLogData ? styles.placeholder : ''}`}>
            {body}
          </div>
        </div>
      </div>
    )
  }
}

export default Log
