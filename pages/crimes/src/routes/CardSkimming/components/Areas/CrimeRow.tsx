import React from 'react'
import { connect } from 'react-redux'
import { CSSTransition } from 'react-transition-group'

import isValue from '@torn/shared/utils/isValue'

import { CrimeRow as CrimeRowGlobal } from '../../../../components'
import { removeExpiredRow } from '../../modules/actions'

import selectorProps from './selectors'

import timeMaker from '../../../../utils/timeMaker'

import styles from './index.cssmodule.scss'

import { IProps, IState } from './interfaces'

const ROW_ANIMATION_TIMING = 500

class Area extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      isAppear: true,
      attemptDisabled: false
    }
  }

  // prevent clicks while the row is expired but being in the transition phase
  _handleExit = () => this.setState({ attemptDisabled: true })
  _handleExited = () => this._removeExpiredRow()

  _removeExpiredRow = () => {
    const { clearExpiredRow, crimeID, subID } = this.props

    clearExpiredRow(crimeID, subID)
  }

  _renderTimeAgo = () => {
    const { log, crimeInfo: { time, timeFound }, timeMount, timeMountStatic } = this.props

    const pureAreaTime = time || log[0]?.time

    const timeToRender = timeMaker({
      initTime: pureAreaTime,
      mountTime: timeFound ? timeMountStatic : timeMount
    })

    return (
      <div className={`${styles.timeAgo} ${styles.firstSection} ${styles.dlm}`}>
        {timeToRender}
      </div>
    )
  }

  _renderProgress = () => {
    const { cards, subID, crimeID, progress, isManualLayoutSetted, mediaType } = this.props

    const label = progress === 'active' ? 'Active' : 'Found'
    const isDesktop = isManualLayoutSetted || mediaType === 'desktop'

    const makeSpinner = Array.from(Array(9).keys()).map(key => {
      return (
        <div key={key} className={styles.spinner} />
      )
    })

    return (
      <div
        id='areaLog'
        data-id={crimeID}
        data-subid={subID}
        className={`${styles.secondSection} ${styles.progressContainer} ${styles.dlm}`}
        role='button'
        tabIndex={0}
        aria-label='Crime Area Log. Log is opened'

      >
        <div className={styles.progressWrap}>
          <span className={styles.textCount}>{cards || 0}</span>
          <div className={styles.spinnerWrap}>
            {makeSpinner}
          </div>
          {isDesktop && <span className={styles.textStatus}>{label}</span>}
        </div>
      </div>
    )
  }

  _renderLog = () => {
    const { children } = this.props

    return children
  }

  _renderRow = () => {
    const { attemptDisabled, isAppear } = this.state
    const { isFresh, isExpired, error } = this.props
    const { throwGlobalCrimeRowProps: props } = selectorProps(this.props)

    const modifiedGlobalProps = {
      ...props,
      button: {
        ...props.button,
        disabled: attemptDisabled || props.button.disabled || !!error
      },
      closeOutcome: {
        ...props.closeOutcome,
        callback: () => this.setState({
          isAppear: false
        })
      }
    }

    return (
      <CSSTransition
        appear={isFresh}
        in={isAppear && !(isValue(isExpired) && isExpired) || isAppear && props.isRowActive}
        timeout={ROW_ANIMATION_TIMING}
        classNames='rowDisappear'
        onExit={this._handleExit}
        onExited={this._handleExited}
        unmountOnExit={true}
      >
        <CrimeRowGlobal {...modifiedGlobalProps} styles={styles}>
          {this._renderTimeAgo()}
          {this._renderProgress()}
          {this._renderLog()}
        </CrimeRowGlobal>
      </CSSTransition>
    )
  }

  render() {
    return this._renderRow()
  }
}

const mapDispatchToProps = {
  clearExpiredRow: removeExpiredRow
}

export default connect(null, mapDispatchToProps)(Area)
