import { createSelector } from 'reselect'

import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getCrimeId } from '../../../../utils/globalSelectorsHelpers'

import { IRowSelector } from '../../../../utils/interfaces/IGlobalRowSelector'
import { IRowGlobalSelector } from '../../../../utils/interfaces/IGlobalSelectors'
import { ICrimeInfo } from '../../interfaces/IPayload'
import { IProps } from './interfaces'

const BUTTON_LABEL = 'Recover'

const getButtonStatus = state => state.isDisabledButton
const getExpiredStatus = state => state.isExpired

const ownCrimeRowProps = createSelector(
  [getCrimeId, getExpiredStatus],
  (commitID: number, isExpired: boolean) => ({
    commitID,
    isExpired
  })
)

const globalCrimeRowPropsModified = createSelector(
  [globalRowPropsSelector, getButtonStatus, getExpiredStatus],
  (globalProps: IRowGlobalSelector, isDisabledButton: boolean, isExpired: boolean) => {
    const { outcome, crimeID, subID, currentRowID, crimeInfo, button } = globalProps
    const action = () => globalProps.button.action({ subURL: `&value1=${globalProps.subID}`, subID })

    const isRowRecovered = outcome.result === 'success' && `${crimeID}_${subID}` === currentRowID
    const isFailureAttempt = outcome.result === 'failed'
    const isRowFound = (crimeInfo as ICrimeInfo).progress === 'found'

    const props = {
      ...globalProps,
      className: (crimeInfo as ICrimeInfo).progress,
      button: {
        ...button,
        action,
        disabled: isDisabledButton || isRowRecovered || isFailureAttempt || isRowFound || button.disabled || isExpired,
        label: BUTTON_LABEL
      }
    }

    return props
  }
)

const selectorProps = (props: IProps): IRowSelector<any> => ({
  ownCrimeRowProps: ownCrimeRowProps(props),
  throwGlobalCrimeRowProps: globalCrimeRowPropsModified(props)
})

export default selectorProps
