import React, { memo } from 'react'

import Placeholder from '../../../../components/Skeleton'

import globalSkeleton from '../../../../styles/skeleton.cssmodule.scss'
import styles from './styles.cssmodule.scss'

const CrimeRowPlaceholder = ({ type }: { type: string }) => {
  const rowsList = (count, isStatic?) =>
    Array.from(Array(count).keys()).map(key => (
      <div className={`${globalSkeleton.rowWrap} ${styles.row} ${isStatic ? styles.static : ''}`} key={key}>
        <div className={globalSkeleton.titleImg} />
        <div className={`${globalSkeleton.titleText} ${styles.titleText}`} />
        <div className={styles.statusContainer}>
          <div className={styles.text} />
        </div>
        <div className={styles.itemContainer} />
        <div className={styles.nerveContainer} />
        <div className={`${globalSkeleton.buttonAction} ${styles.actionButton}`} />
      </div>
    ))

  return (
    <Placeholder type={type}>
      <div className={globalSkeleton.rowsWrap}>
        {rowsList(2, true)}
        <hr className={styles.delimiter} />
        {rowsList(8)}
      </div>
    </Placeholder>
  )
}

export default memo(CrimeRowPlaceholder)
