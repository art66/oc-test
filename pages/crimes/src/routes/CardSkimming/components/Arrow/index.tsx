import React, { memo } from 'react'

import styles from './arrow.cssmodule.scss'

export interface IProps {
  type: 'shadow' | 'full'
  shape: 'checked' | 'forward'
}

const Arrow = memo(({ type = 'full', shape = 'checked' }: IProps) => {
  const arrowType = type === 'shadow' ? styles.shadow : styles.full

  if (shape === 'checked') {
    return (
      <svg
        className={`${styles.arrowChecked} ${arrowType}`}
        xmlns='http://www.w3.org/2000/svg'
        viewBox='0 0 12 9.5'
      >
        <path
          d='M10.142,2,4.5,7.784,1.857,5.278,0,7.136,4.5,11.5,12,3.858Z'
          transform='translate(0 -2)'
        />
      </svg>
    )
  }

  return (
    <svg
      className={`${styles.arrowForward} ${arrowType}`}
      xmlns='http://www.w3.org/2000/svg'
      viewBox='0 0 7.5 11'
    >
      <path
        d='M8.142,4,4.5,7.784.857,4.278-1,6.136,4.5,11.5,10,5.858Z'
        transform='translate(-4 10) rotate(-90)'
      />
    </svg>
  )
})

export default Arrow
