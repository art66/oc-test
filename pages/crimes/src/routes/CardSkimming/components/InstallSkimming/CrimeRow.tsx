import React from 'react'
import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'
import cn from 'classnames'

import { CrimeRow as CrimeRowGlobal } from '../../../../components'
import ItemCell from '../../../../components/ItemCell'
import DropDown from './DropDown'
import Arrow from '../Arrow'

import selectorProps from './selectors'

import globalStyles from '../../../../styles/card-skimming.cssmodule.scss'

import { IProps } from './interfaces'

class InstallSkimming extends React.Component<IProps> {
  componentDidMount() {
    this._initializeTooltips()
  }

  _initializeTooltips = () => {
    const { crimeID, requirements } = this.props
    const { items } = requirements || {}

    const tooltipsItems = []

    items.forEach(item => {
      tooltipsItems.push({
        child: `${item.label ? 'Uses: ' : ''}${item.label}`,
        ID: `${crimeID}-${item.value}`
      })
    })

    this._renderTooltips(tooltipsItems)
  }

  _renderTooltips = (tooltips = []) => {
    tooltips.forEach(tooltipItem => tooltipSubscriber.subscribe(tooltipItem))
  }

  _renderDropDownSection = () => {
    const { lastActiveArea, areas } = this.props

    return (
      <DropDown
        crimeID={lastActiveArea}
        areas={areas}
      />
    )
  }

  _renderItemsSection = () => {
    const { requirements, crimeID } = this.props
    const { items } = requirements
    const [spyCamera, laptop] = items

    return (
      <div className={cn(globalStyles.secondSection, globalStyles.dlm)}>
        <div className={globalStyles.cellWrap}>
          <Arrow type={!spyCamera.available ? 'shadow' : 'full'} shape='forward' />
          <ItemCell
            imgAlt={spyCamera.value}
            ID={`${crimeID}-${spyCamera.value}`}
            count={spyCamera.count}
            imgSrc={`/images/items/${spyCamera.value}/${!spyCamera.available ? 'blank' : 'large'}.png`}
          />
        </div>
        <div className={globalStyles.cellWrap}>
          <Arrow type={!laptop.available ? 'shadow' : 'full'} shape='forward' />
          <ItemCell
            imgAlt={laptop.value}
            ID={`${crimeID}-${laptop.value}`}
            count={laptop.count}
            imgSrc={`/images/items/${laptop.value}/${!laptop.available ? 'blank' : 'large'}.png`}
          />
        </div>
      </div>
    )
  }

  _renderRow = () => {
    const { error } = this.props
    const { throwGlobalCrimeRowProps: props } = selectorProps(this.props)

    const modifiedGlobalProps = {
      ...props,
      button: {
        ...props.button,
        disabled: props.button.disabled || !!error
      }
    }

    return (
      <CrimeRowGlobal {...modifiedGlobalProps} styles={globalStyles}>
        {this._renderDropDownSection()}
        {this._renderItemsSection()}
      </CrimeRowGlobal>
    )
  }

  render() {
    return this._renderRow()
  }
}

export default InstallSkimming
