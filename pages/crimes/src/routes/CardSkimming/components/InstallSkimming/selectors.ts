import { createSelector } from 'reselect'

import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getCrimeId } from '../../../../utils/globalSelectorsHelpers'
import { IRowSelector } from '../../../../utils/interfaces/IGlobalRowSelector'
import { IRowGlobalSelector } from '../../../../utils/interfaces/IGlobalSelectors'
import { IProps, IInstallSkimmingOwnSelector } from './interfaces'

const BUTTON_LABEL = 'Install'

const ownCrimeRowProps = createSelector([getCrimeId], commitID => ({
  commitID
}))

const getLastActiveArea = state => state.lastActiveArea

const globalCrimeRowPropsModified = createSelector(
  [globalRowPropsSelector, getLastActiveArea],
  (globalProps: IRowGlobalSelector, lastActiveArea: number) => {
    return {
      ...globalProps,
      button: {
        ...globalProps.button,
        label: BUTTON_LABEL,
        action: () => globalProps.button.action({ crimeIDCustom: lastActiveArea }),
        iconName: 'test'
      }
    }
  }
)

const selectorProps = (props: IProps): IRowSelector<IInstallSkimmingOwnSelector> => ({
  ownCrimeRowProps: ownCrimeRowProps(props),
  throwGlobalCrimeRowProps: globalCrimeRowPropsModified(props)
})

export default selectorProps
