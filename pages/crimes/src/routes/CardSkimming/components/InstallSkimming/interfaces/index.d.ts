import { IInstallSkimming } from '../../../interfaces/IPayload'

export interface IProps extends IInstallSkimming {}

export interface IState {
  isDropDownOpened: boolean
}

export interface IInstallSkimmingOwnSelector {
  commitID: number
}
