import { IArea } from '../../../interfaces/IPayload'

export interface IProps {
  isTouchScreen: boolean
  crimeID: number
  installArea: (ID: number, available: boolean) => void
  areas: IArea[]
  crimeLevel: number
}

export interface IState {
  isDropDownOpened: boolean
}

