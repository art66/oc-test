import React from 'react'
import cn from 'classnames'

import { connect } from 'react-redux'

import StarLVL from '../../../../components/StarLVL'

import { setInstallArea } from '../../modules/actions'

import { IProps, IState } from './interfaces/IDropDown'

import styles from './dropdown.cssmodule.scss'
import globalStyles from '../../../../styles/card-skimming.cssmodule.scss'

interface IAreaRender {
  title: string
  nerve: number
  areaCrimeID: number
  isLastActiveArea?: boolean
  isAvailable?: boolean
  isSelected?: boolean
}

const STARS_PROGRESS = {
  'post office': 25,
  'airport terminal': 50,
  'casino lobby': 75,
  'bank branch': 100
}

class DropDown extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      isDropDownOpened: false
    }
  }

  componentDidMount() {
    document.addEventListener('click', this._handleDropDownCloseGlobal)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this._handleDropDownCloseGlobal)
  }

  _handleDropDownCloseGlobal = ({ target }) => {
    const { isTouchScreen } = this.props

    const dropDownWrap = document.querySelector('#installSkimmingDropDown')

    if (isTouchScreen && target.getAttribute('id') === 'installSkimmingDropDownMobile') {
      return
    }

    if (!dropDownWrap?.contains(target)) {
      this.setState({
        isDropDownOpened: false
      })
    }
  }

  _handleDropDownClick = ({ target }) => {
    const { dataset } = target
    const { crimeID: lastActiveAreaID, installArea, areas } = this.props

    this.setState(prevState => ({
      isDropDownOpened: !prevState.isDropDownOpened
    }))

    const isActiveAreaClicked = lastActiveAreaID === Number(dataset.id)
    const isAreaMissed = !dataset.id
    const isAreaNonAvailable = !areas?.find(area => area.crimeID === Number(dataset.id))?.available

    if (isActiveAreaClicked || isAreaMissed || isAreaNonAvailable) {
      return
    }

    installArea(Number(dataset.id), !!dataset.available)
  }

  _handleBlur = () => this.setState({ isDropDownOpened: false })

  _handleTouchScreenClickHelper = e => {
    e.preventDefault()
    e.stopPropagation()

    this.setState({
      isDropDownOpened: true
    })
  }

  _renderArea = ({ title, nerve, areaCrimeID, isLastActiveArea, isAvailable, isSelected }: IAreaRender) => {
    const { crimeLevel } = this.props

    let starLevel = STARS_PROGRESS[title.toLowerCase()]

    if (crimeLevel === 100) {
      starLevel = null
    } else if (crimeLevel >= 25 && crimeLevel <= 49 && [25].includes(starLevel)) {
      starLevel = null
    } else if (crimeLevel >= 50 && crimeLevel <= 74 && [25, 50].includes(starLevel)) {
      starLevel = null
    } else if (crimeLevel >= 75 && crimeLevel <= 99 && [25, 50, 75].includes(starLevel)) {
      starLevel = null
    }

    const classNames = cn({
      [styles.area]: true,
      [styles.lastActiveArea]: isLastActiveArea,
      [styles.selectedArea]: isSelected
    })

    return (
      <div
        key={areaCrimeID}
        className={classNames}
        data-nerve={nerve}
        data-id={areaCrimeID}
        data-available={isAvailable}
      >
        <span className={cn({ [styles.nonAvailable]: !isAvailable })}>{title}</span>
        {!isLastActiveArea && (
          <span className={starLevel ? styles.star : styles.text}>
            {starLevel ? <StarLVL lvl={starLevel} /> : '--'}
          </span>
        )}
      </div>
    )
  }

  _renderDropDownSection = () => {
    const { isDropDownOpened } = this.state
    const { areas, crimeID, isTouchScreen } = this.props

    let lastActiveAreaCrime = null
    const areasToRender = []

    areas.forEach(({ title, nerve, crimeID: areaCrimeID, available }) => {
      const isSelected = crimeID && crimeID === areaCrimeID

      if (isSelected) {
        lastActiveAreaCrime = this._renderArea({
          title,
          nerve,
          areaCrimeID,
          isLastActiveArea: true,
          isAvailable: available
        })
      }

      areasToRender.push(this._renderArea({ title, nerve, areaCrimeID, isAvailable: available, isSelected }))
    })

    return (
      <div
        id='installSkimmingDropDown'
        tabIndex={0}
        aria-label='button'
        role='button'
        className={cn(globalStyles.firstSection, globalStyles.dlm, { [styles.activeDropDownAreas]: isDropDownOpened })}
        onClick={this._handleDropDownClick}
        onKeyDown={undefined}
      >
        <div className={styles.areasContainer}>
          {lastActiveAreaCrime}
          {isDropDownOpened && areasToRender}
        </div>
        {isTouchScreen && !isDropDownOpened && (
          <button
            id='installSkimmingDropDownMobile'
            aria-label='dropDown button'
            type='button'
            className={styles.mobileHoverButton}
            onClick={this._handleTouchScreenClickHelper}
          />
        )}
      </div>
    )
  }

  render() {
    return this._renderDropDownSection()
  }
}

const mapStateToProps = ({ browser, common, skimming }) => ({
  crimeLevel: skimming.currentLevel,
  mediaType: browser.mediaType,
  isDesktopLayoutSetted: common.isDesktopLayoutSetted,
  isTouchScreen: browser.mediaType !== 'desktop' && !common.isDesktopLayoutSetted
})

const mapDispatchToProps = dispatch => ({
  installArea: (ID, available) => dispatch(setInstallArea(ID, available))
})

export default connect(mapStateToProps, mapDispatchToProps)(DropDown)
