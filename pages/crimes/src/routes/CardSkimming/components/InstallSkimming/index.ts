import { connect } from 'react-redux'
import InstallSkimming from './CrimeRow'
import { closeOutcome } from '../../../../modules/actions'

const mapDispatchToProps = dispatch => ({
  collapseOutcome: () => dispatch(closeOutcome())
})

export default connect(null, mapDispatchToProps)(InstallSkimming)
