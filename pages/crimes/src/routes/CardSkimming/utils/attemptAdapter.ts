export interface IAttemptAdapter {
  outcome: any
}

const attemptAdapter = ({ outcome }: IAttemptAdapter) => {
  return outcome
}

export default attemptAdapter
