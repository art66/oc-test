import { IArea } from '../interfaces/IPayload'

const checkInstallCommitAvailability = (installRowAreas: IArea[]) => {
  if (!installRowAreas[0].available) {
    return false
  }

  return installRowAreas?.some(area => area.available)
}

export default checkInstallCommitAvailability
