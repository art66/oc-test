const updatePrevCrimes = ({ crimes, outcome, lastActiveID }: any) => {
  const updatedCrimes = !crimes ? [] : crimes.map(crime => {
    return lastActiveID === `${crime.crimeID}_${crime.subID}` ? {
      ...crime,
      isExpired: true,
      isDisabledButton: outcome.result === 'success'
    } : crime
  })

  return updatedCrimes
}

export default updatePrevCrimes
