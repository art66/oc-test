const generate = count => {
  const max = 10 ** count
  const min = max / 10

  return Math.floor(Math.random() * (max - min + 1)) + min
}

const generateCardNumber = () => `${generate(4)} ${generate(4)} ${generate(4)} ${generate(4)}`

export default generateCardNumber
