import addNewCrimes from './addNewCrimes'

const crimesSeparator = (crimesByTypeNext: any, crimesByTypePrev: any, isGlobalCrimeMount?: boolean) => {
  const { installSkimming = {}, sellCards = {}, crimesByType = [] } = crimesByTypePrev

  return {
    installSkimming: {
      ...installSkimming,
      ...crimesByTypeNext.installSkimming,
      ...crimesByTypePrev.installSkimming?.lastActiveArea ? {
        lastActiveArea: crimesByTypePrev.installSkimming?.lastActiveArea
      } : {}
    },
    sellCards: {
      ...sellCards,
      ...crimesByTypeNext.sellCards
    },
    crimesByType: isGlobalCrimeMount ? crimesByTypeNext.subCrimes : [
      ...addNewCrimes(crimesByType, crimesByTypeNext.subCrimes)
    ]
  }
}

export default crimesSeparator
