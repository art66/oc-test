const addNewCrimes = (prevCrimes, newCrimes) => {
  let updatedCrimes = []

  const findMatch = newCrime => oldCrime => {
    const oldCrimeID = `${oldCrime.crimeID}_${oldCrime.subID}`
    const newCrimeID = `${newCrime.crimeID}_${newCrime.subID}`

    return oldCrimeID === newCrimeID
  }

  const reorderCrimes = () => {
    const updatedCrimesShakeOrder = [...updatedCrimes]
      .sort((a, b) => {
        if (b.crimeInfo.progress !== 'active') {
          return -1
        }

        return b.crimeInfo.time - a.crimeInfo.time
      })

    updatedCrimes = [
      ...updatedCrimesShakeOrder
    ]
  }

  const keepExpiredCrimes = () => {
    (prevCrimes || []).forEach(prevCrime => {
      const isNotExpired = updatedCrimes?.some(findMatch(prevCrime))

      if (isNotExpired) {
        return
      }

      updatedCrimes.push({
        ...prevCrime,
        isExpired: true
      })
    })
  }

  const updateOngoingCrimes = newCrime => {
    (prevCrimes || []).forEach(oldCrime => {
      const isMatch = findMatch(newCrime)(oldCrime)

      if (!isMatch) {
        return
      }

      updatedCrimes.push({
        ...oldCrime,
        ...newCrime
      })
    })
  }

  const addNewCrime = newCrime => {
    const isFresh = !prevCrimes?.some(findMatch(newCrime))

    if (!isFresh) {
      return
    }

    updatedCrimes.push({
      ...newCrime,
      isFresh: true
    })
  }

  const run = () => {
    // updating fresh crimes configuration
    newCrimes?.forEach(newCrime => {
      updateOngoingCrimes(newCrime)
      addNewCrime(newCrime)
    })

    // they will be removed under animation transition
    // or full page reload/route change
    keepExpiredCrimes()

    // while we have several dedicated operation on a single
    // array we need to make it consistent in order with array kept
    // in the Redux state
    reorderCrimes()

    return updatedCrimes
  }

  return run()
}

export default addNewCrimes
