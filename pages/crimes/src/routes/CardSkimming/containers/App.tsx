import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { fetchData, clearOutdatedOutcome } from '../../../modules/actions'

import { CrimesList, Skeleton, Banner } from '../components'
import SubCrimesLayout from '../../../components/SubCrimesLayout'

import styles from './app.cssmodule.scss'
import '../../../styles/card-skimming.cssmodule.scss'

export interface ICardSkimmingContainerProps {
  state: {
    skimming: {
      outcome: {
        story: string[]
      }
    }
  }
  showPlaceholder: boolean
  current: number
  progress: number
  stats: object[]
  fetchDataAction: Function
  startClearOutdatedOutcome: Function
}

class CardSkimmingContainer extends PureComponent<ICardSkimmingContainerProps> {
  componentDidMount() {
    window.addEventListener('focus', this._fetchData)
  }

  componentWillUnmount() {
    const {
      startClearOutdatedOutcome,
      state
    } = this.props
    const { skimming } = state || {}
    const { outcome: { story = [] } = {} } = skimming || {}

    if (skimming && (!story || (skimming && story.length !== 0))) {
      startClearOutdatedOutcome()
    }

    window.removeEventListener('focus', this._fetchData)
  }

  _fetchData = () => {
    const { fetchDataAction } = this.props

    fetchDataAction()
  }

  render() {
    const { stats, current, progress, showPlaceholder } = this.props

    return (
      <SubCrimesLayout
        customWrapClass={styles.skimming}
        showPlaceholder={showPlaceholder}
        title='Card Skimming'
        skeleton={Skeleton}
        banners={<Banner />}
        stats={stats}
        current={current}
        progress={progress}
      >
       <CrimesList />
      </SubCrimesLayout>
    )
  }
}

const mapStateToProps = state => ({
  stat: state.skimming,
  current: state.skimming.currentLevel,
  progress: state.skimming.skillLevelTotal,
  stats: state.skimming.currentUserStatistics,
  showPlaceholder: state.skimming.showPlaceholder
})

const mapDispatchToProps = dispatch => ({
  fetchDataAction: () => dispatch(fetchData('skimming', '&step=crimesList&typeID=6')),
  startClearOutdatedOutcome: () => dispatch(clearOutdatedOutcome('skimming'))
})

export default connect(mapStateToProps, mapDispatchToProps)(CardSkimmingContainer)
