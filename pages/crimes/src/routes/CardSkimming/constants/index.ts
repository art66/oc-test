export const typeID = 6
export const CRIMES_COUNT = 24

// ------------------------------------
// Action Constants
// ------------------------------------
export const ATTEMPT = 'skimming/ATTEMPT'
export const FETCH_DATA = 'skimming/FETCH_DATA'
export const DATA_FETCHED = 'skimming/DATA_FETCHED'
export const ATTEMPT_START = 'skimming/ATTEMPT_START'
export const ATTEMPT_FETCHED = 'skimming/ATTEMPT_FETCHED'
export const CRIME_IS_READY = 'skimming/CRIME_IS_READY'
export const CRIME_DATA_UPDATE_ATTEMPT = 'skimming/CRIME_DATA_UPDATE_ATTEMPT'
export const CRIME_DATA_UPDATE_SAVED = 'skimming/CRIME_DATA_UPDATE_SAVED'
export const SET_INSTALL_AREA = 'skimming/SET_INSTALL_AREA'
export const EXPIRED_ROW_REMOVED = 'skimming/EXPIRED_ROW_REMOVED'
export const FETCH_LOG_ATTEMPT = 'skimming/FETCH_LOG_ATTEMPT'
export const FETCH_LOG_SAVED = 'skimming/FETCH_LOG_SAVED'
export const CLEAR_LOG = 'skimming/CLEAR_LOG'
