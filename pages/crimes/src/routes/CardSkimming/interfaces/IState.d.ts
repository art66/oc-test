import { IState } from '../../../interfaces/IState'
import { IInstallSkimming, ILogItem, ISellCards, ISubCrime } from './IPayload'

export interface ISkimmingState extends IState {
  log: ILogItem[]
  installSkimming: IInstallSkimming
  sellCards: ISellCards
  crimesByType: ISubCrime[]
}
