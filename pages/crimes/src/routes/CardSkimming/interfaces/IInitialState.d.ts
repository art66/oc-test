import { IInitialCrimeState } from '../../../interfaces/IInitialState'

export interface IInitialState extends IInitialCrimeState {
  typeID: 6
  crimesByType: {
    installSkimming: {
      areas: {
        title: 'Bus Station' | 'Subway Station' | 'College Campus' | 'Gas Station' | 'Post Office' | 'Airport Terminal' | 'Casino Lobby' | 'Bank Branch'
        nerve: 1
        crimeID: 49 | 52 | 53 | 54 | 55 | 56 | 57 | 58
        available: boolean
      }[]
      buttonTitle: 'Install'
      crimeID: 49 | 52 | 53 | 54 | 55 | 56 | 57 | 58
      iconClass: 'jewelry_shop'
      nerve: 1
      requirements: {
        items: {
          label: 'Spy Camera' | 'Card Skimmer'
          value: '1123' | '1125'
          count: number
          available: boolean
        }[]
      }
      title: 'Install Card Skimmer'
    }
    sellCards: {
      additionalInfo: {
        collectedCards: number
      }
      available: boolean
      buttonTitle: 'Sell'
      crimeID: 51
      iconClass: 'jewelry_shop'
      nerve: number
      requirements: {
        items: {
          label: 'Personal Computer' | 'Laptop'
          value: '61' | '154'
          count: number
          available: boolean
        }[]
      }
      title: 'Sell credit cards'
    }
  }
  currentUserStatistics: {
    label: 'Level & EXP' | 'Enhancer' | 'Successes' | 'Fails' | 'Critical fails' | 'Money made' |  'Skimmers installed' | 'Skimmers lost' | 'Skimmers recovered' | 'Card details sold' | 'Card details lost' | 'Card details recovered' | 'Oldest recovered skimmer' | 'Most lucrative skimmer' | 'Bus station skims' | 'Subway station skims' | 'College campus skims' | 'Gas station skims' | 'Post office skims' | 'Airport terminal skims' | 'Casino lobby skims' | 'Back branch skims'
    value: number
  }[]
}
