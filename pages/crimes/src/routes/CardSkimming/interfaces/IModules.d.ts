import { TType } from '../../../interfaces/IModules'
import { ISkimmingPayload } from './IPayload'

export type TAttempt = (url: string, itemId: string | number) => void

export interface IFetchAction {
  type: TType
  json: ISkimmingPayload
}

export interface ISetInstallArea {
  type: TType
  available: boolean
  ID: number
}

