import { ICurrentType, IResponseData, TIsAjaxRequest, TRfcv } from '../../../interfaces/IActionResponce'
import { ICommonRowProps, IOutcome } from '../../../interfaces/IState'

export type TResult = 'info'
export type TCrimeRoute = 'skimming'
export type TCrimeTitle = 'Skimming'
export type TAreaTitles = 'Bus Station' | 'Subway Station' | 'College Campus' | 'Gas Station' | 'Post Office' | 'Airport Terminal' | 'Casino Lobby' | 'Bank Branch'

export interface IPanelItem {
  removeLink: boolean
  label: 'Crime enhancer' | 'Level & EXP'
  value: 'Bag of Reinddddd' | '1 100 & 100%'
}

export interface IArea {
  title: TAreaTitles
  nerve: number
  crimeID: number
  available: boolean
}

export interface IRequiredItemInstallSkimming {
  label: 'Spy Camera' | 'Card Skimmer'
  value: '1123' | '1125'
  count: number
  available: boolean
}

export interface IRequiredItemsInstallSkimming {
  items: IRequiredItemInstallSkimming[]
}

export interface IInstallSkimming extends ICommonRowProps {
  crimeID: number
  lastActiveArea: number
  title: 'Install Skimming'
  iconClass: 'jewelry_shop'
  nerve: number
  requirements: IRequiredItemsInstallSkimming
  buttonTitle: 'install'
  outcome: IOutcome
  areas: IArea[]
}

export interface IRequiredItemSellCards {
  label: 'Personal Computer' | 'Laptop'
  value: '61' | '154'
  count: number
  available: boolean
}

export interface IRequiredItemsSellCards {
  items: IRequiredItemSellCards[]
}

export interface ISellCardsAdditionalInfo {
  collectedCards: number
}

export interface ISellCards extends ICommonRowProps {
  available: boolean
  title: 'Sell credit cards'
  iconClass: 'jewelry_shop'
  buttonTitle: 'sell'
  nerve: number
  crimeID: number
  requirements: IRequiredItemsSellCards
  additionalInfo: ISellCardsAdditionalInfo
}

export interface ILogItem {
  label: string
  color: 'green' | 'red' | 'grey'
  time: number
}

export interface ICrimeInfo {
  time: number
  cards: number
  timeFound?: number
  timeActive: number
  timeWhenUpdated: number
  progress: 'active' | 'found'
}

export interface ISubCrime extends ICommonRowProps {
  title: TAreaTitles
  timeActive: number
  buttonTitle: 'recover'
  crimeInfo: ICrimeInfo
  nerve: number
  iconClass: string
  crimeID: number
  subID: string
  available: boolean
  progress: 'active' | 'found'
  cards: number
  isDisabledButton?: boolean
  isExpired?: boolean
  pollInProgress?: boolean
}

export interface ICrimesByType {
  installSkimming: IInstallSkimming
  sellCards: ISellCards
  subCrimes: ISubCrime[]
}

export interface ISkimmingCurrentType extends ICurrentType {
  title: 'Skimming'
  crimeRoute: TCrimeRoute
}

export interface ISkimmingData extends IResponseData, ISkimmingCurrentType {
  title: TCrimeTitle
  crimesByType: ICrimesByType
}

export interface ISkimmingPayload {
  DB: ISkimmingData
  rfcv: TRfcv
  isAjaxRequest: TIsAjaxRequest
}

export interface IAttemptSkimmingPayload {
  json: {
    DB: {
      ID: 1
      infoByTypeGroup: string
      isInfoByTypeArray: boolean
      outcome: {
        ID: number
        outcomeDesc: string
        rarity: string
        result: string
        rewardsGive: string
        special: string
        story: string | string[]
        timestamp: number
      }
      time: number
      tutorial: {
        text: string
      }
      typeGroupSubs: false
      user: {
        playername: string
      }
    }
    error: string
    isAjaxRequest: true
    rfcv: null
  }
  meta: string
}
