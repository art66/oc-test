export const typeID = 6
export const CRIMES_COUNT = 24

// ------------------------------------
// Action Constants
// ------------------------------------
export const ATTEMPT = 'burglary/ATTEMPT'
export const FETCH_DATA = 'burglary/FETCH_DATA'
export const DATA_FETCHED = 'burglary/DATA_FETCHED'
export const ATTEMPT_START = 'burglary/ATTEMPT_START'
export const ATTEMPT_FETCHED = 'burglary/ATTEMPT_FETCHED'
export const CRIME_IS_READY = 'burglary/CRIME_IS_READY'
export const EXPIRED_ROW_REMOVED = 'burglary/EXPIRED_ROW_REMOVED'

export const btnActionDimensions = {
  desktop: {
    width: 66,
    height: 31
  },
  tablet: {
    width: 30,
    height: 30
  },
  mobile: {
    width: 30,
    height: 30
  }
}
