import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import { fetchData } from '../../modules/actions'
import { typeID } from './constants'
import reducer from './modules/reducers'
import saga from './modules/sagas'
import rootStore from '../../store/createStore'

import '../../styles/shoplifting.cssmodule.scss'

const Preloader = () => null

const BurglaryContainer = Loadable({
  loader: async () => {
    const Burglary = await import(/* webpackChunkName: "burglary" */ './containers/App')

    injectReducer(rootStore, { key: 'burglary', reducer })
    injectSaga({ key: 'burglary', saga })

    const coreData = fetchData('burglary', `&step=crimesList&typeID=${typeID}`) as never

    rootStore.dispatch(coreData)

    return Burglary
  },
  loading: Preloader
})

export default BurglaryContainer
