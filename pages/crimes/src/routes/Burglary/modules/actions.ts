import { EXPIRED_ROW_REMOVED } from '../constants'

// -----------------------------
// BURGLARY ACTIONS
// -----------------------------
export const removeExpiredRow = (crimeID, subID) => ({
  type: EXPIRED_ROW_REMOVED,
  crimeID,
  subID
})
