import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { dataFetched, crimeReady, getCurrentPageData, showDebugInfo } from '../../../../modules/actions'
import { ROOT_URL, TYPE_IDS } from '../../../../constants'

function* fetchData({ url }: any) {
  try {
    const json: any = yield fetchUrl(ROOT_URL + url, null, false)

    if (json?.DB?.error) {
      throw new Error(json.DB.error)
    }

    yield put(dataFetched('burglary', json))

    yield put(getCurrentPageData(json.DB.currentType.crimeRoute, TYPE_IDS[json.DB.currentType.crimeRoute]))
    yield put(crimeReady('/burglary'))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default fetchData
