// @ts-nocheck
import { takeLatest, takeEvery } from 'redux-saga/effects'

import fetchData from './fetchData'
import attempt from './attempt'
import subCrimeRowPooling from './subCrimeRowPooling'
import fetchLog from './fetchLog'

import { ATTEMPT, FETCH_DATA } from '../../constants'

export default function* watchCardSkimming() {
  yield takeLatest(FETCH_DATA, fetchData)
  yield takeLatest(ATTEMPT, attempt)
}
