import initialState from './initialState'

import {
  DATA_FETCHED, ATTEMPT_START, ATTEMPT_FETCHED
} from '../constants'

import { IFetchAction } from '../interfaces/IModules'
import { ISkimmingState } from '../interfaces/IState'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [DATA_FETCHED]: (state: ISkimmingState, action: IFetchAction): ISkimmingState => ({
    ...state,
    ...action.json.DB
  }),
  [ATTEMPT_START]: (state: ISkimmingState, action: any) => ({
    ...state,
    ...action.json.DB
  }),
  [ATTEMPT_FETCHED]: (state: ISkimmingState, action: any) => ({
    ...state,
    ...action.json.DB
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
export default (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
