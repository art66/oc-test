import { IInitialCrimeState } from '../../../interfaces/IInitialState'

export interface IInitialState extends IInitialCrimeState {
  typeID: 7
  crimesByType: any
  currentUserStatistics: {
    label: 'Level & EXP' | 'Enhancer' | 'Successes' | 'Fails' | 'Critical fails' | 'Money made' |  'Skimmers installed' | 'Skimmers lost' | 'Skimmers recovered' | 'Card details sold' | 'Card details lost' | 'Card details recovered' | 'Oldest recovered skimmer' | 'Most lucrative skimmer' | 'Bus station skims' | 'Subway station skims' | 'College campus skims' | 'Gas station skims' | 'Post office skims' | 'Airport terminal skims' | 'Casino lobby skims' | 'Back branch skims'
    value: number
  }[]
}
