import React from 'react'
import { connect } from 'react-redux'
import { showDropDownPanel, toggleArrows } from '../../../../modules/actions'

import BannerGlobal from '../../../../components/Banner'

import stylesCS from './index.cssmodule.scss'
import '../../../../styles/animations.scss'

export interface IBannersProps {
  dropDownPanelToShow: Function
  toggleArrow: Function
}

export class Banner extends React.Component<IBannersProps> {
  static defaultProps = {
    dropDownPanelToShow: () => {},
    toggleArrow: () => {}
  }

  render() {
    const { dropDownPanelToShow, toggleArrow } = this.props

    return (
      <BannerGlobal dropDownPanelToShow={dropDownPanelToShow} toggleArrows={toggleArrow}>
        <div className={stylesCS.burglaryBanner}>
          <div className={stylesCS.bannerBcg} />
          <div className={stylesCS.bannerCloudUp} />
          <div className={stylesCS.bannerCloudDown} />
        </div>
      </BannerGlobal>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  dropDownPanelToShow: () => dispatch(showDropDownPanel()),
  toggleArrow: () => dispatch(toggleArrows())
})

export default connect(null, mapDispatchToProps)(Banner)
