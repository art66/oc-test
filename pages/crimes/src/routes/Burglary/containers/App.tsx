import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { fetchData, clearOutdatedOutcome } from '../../../modules/actions'

import { Banner } from '../components'
import SubCrimesLayout from '../../../components/SubCrimesLayout'

import styles from './app.cssmodule.scss'
import '../../../styles/card-skimming.cssmodule.scss'

export interface IBurglaryContainerProps {
  state: any
  stats: any
  current: any
  progress: any
  showPlaceholder: any
  fetchDataAction: any
}

class BurglaryContainer extends PureComponent<IBurglaryContainerProps> {
  _fetchData = () => {
    const { fetchDataAction } = this.props

    fetchDataAction()
  }

  render() {
    const { stats, current, progress, showPlaceholder } = this.props

    return (
      <SubCrimesLayout
        customWrapClass={styles.burglary}
        showPlaceholder={showPlaceholder}
        title='Burglary'
        skeleton={() => <span />}
        banners={<Banner />}
        stats={stats}
        current={current}
        progress={progress}
      >
        <div />
      </SubCrimesLayout>
    )
  }
}

const mapStateToProps = state => ({
  stat: state.burglary,
  current: state.burglary.currentLevel,
  progress: state.burglary.skillLevelTotal,
  stats: state.burglary.currentUserStatistics,
  showPlaceholder: state.burglary.showPlaceholder
})

const mapDispatchToProps = dispatch => ({
  fetchDataAction: () => dispatch(fetchData('burglary', '&step=crimesList&typeID=7')),
  startClearOutdatedOutcome: () => dispatch(clearOutdatedOutcome('burglary'))
})

export default connect(mapStateToProps, mapDispatchToProps)(BurglaryContainer)
