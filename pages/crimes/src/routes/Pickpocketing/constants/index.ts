export const typeID = 5
export const CRIMES_COUNT = 24

// ------------------------------------
// Action Constants
// ------------------------------------
export const ATTEMPT = 'pickpocketing/ATTEMPT'
export const FETCH_DATA = 'pickpocketing/FETCH_DATA'
export const DATA_FETCHED = 'pickpocketing/DATA_FETCHED'
export const ATTEMPT_START = 'pickpocketing/ATTEMPT_START'
export const ATTEMPT_FETCHED = 'pickpocketing/ATTEMPT_FETCHED'
export const CRIME_IS_READY = 'pickpocketing/CRIME_IS_READY'
export const LEVEL_UP = 'pickpocketing/LEVEL_UP'
export const SET_CRIME_LEVEL = 'pickpocketing/SET_CRIME_LEVEL'
export const CLEAR_OUTDATED_OUTCOME = 'pickpocketing/CLEAR_OUTDATED_OUTCOME'
export const POOLING = 'pickpocketing/POOLING'
export const POOLING_FETCHED = 'pickpocketing/POOLING_FETCHED'
export const RESET_STATS_DONE = 'pickpocketing/RESET_STATS_DONE'
export const CRIMES_TICKER_UPDATER = 'pickpocketing/CRIMES_TICKER_UPDATER'
export const EXPIRED_ROW_REMOVED = 'pickpocketing/EXPIRED_ROW_REMOVED'
export const SYNCHRONIZE_TARGETS = 'pickpocketing/SYNCHRONIZE_TARGETS'
export const SYNCHRONIZE_TARGETS_FINISH = 'pickpocketing/SYNCHRONIZE_TARGETS_FINISH'
