const getDate = () => Number((Date.now() / 1000).toFixed(0))

export default getDate
