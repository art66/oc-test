const pickpocketingRowsUpdate = (prevProps: any, nextProps: any) => {
  const isAddInfoChanged = JSON.stringify(prevProps.additionalInfo) !== JSON.stringify(nextProps.additionalInfo)
  const lifecycleUpdated = prevProps.rowTimeLifecycle !== nextProps.rowTimeLifecycle
  const timeChanged = prevProps.timeToLeave !== nextProps.timeToLeave
  const tickerChanged = prevProps.timeLeft !== nextProps.timeLeft
  const isInitMountedChanged = prevProps.isRowInstalled !== nextProps.isRowInstalled
  const attemptDisabledChanged = prevProps.attemptDisabled !== nextProps.attemptDisabled
  const isExpiredChanged = prevProps.expired !== nextProps.expired

  // update only rows that received updated statistics data
  if (isAddInfoChanged || lifecycleUpdated || timeChanged || tickerChanged || isInitMountedChanged || attemptDisabledChanged || isExpiredChanged) {
    return true
  }

  return false
}

export { pickpocketingRowsUpdate }
