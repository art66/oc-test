const updateCrimeTargets = (oldCrimesList, newCrimesList) => {
  const updatedCrimesList = [
    ...oldCrimesList
  ]

  const rowsIteratorChecker = (oldCrime, nextCrimesList) => {
    return nextCrimesList.some(newCrime => {
      if (`${oldCrime.crimeID}_${oldCrime.commitID}` === `${newCrime.crimeID}_${newCrime.commitID}`) {
        oldCrime.timeLeft = newCrime.timeLeft

        return true
      }

      return false
    })
  }

  // const checkExpiredRows = () => {
  //   oldCrimesList.forEach((oldCrime, index) => {
  //     const isCurrentCrimeExpired = !rowsIteratorChecker(oldCrime, newCrimesList)

  //     if (isCurrentCrimeExpired) {
  //       updatedCrimesList[index] = {
  //         ...updatedCrimesList[index],
  //         expired: true
  //       }
  //     }
  //   })
  // }

  const checkIncomingRows = () => {
    newCrimesList.forEach(newCrime => {
      const isNewCrimeMissed = !rowsIteratorChecker(newCrime, oldCrimesList)

      if (isNewCrimeMissed) {
        updatedCrimesList.push({
          ...newCrime,
          totalTime: newCrime.timeLeft
        })
      }
    })
  }

  // checkExpiredRows()
  checkIncomingRows()

  return updatedCrimesList
}

export default updateCrimeTargets
