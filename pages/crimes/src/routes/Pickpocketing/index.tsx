import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import { fetchData } from '../../modules/actions'
import { typeID } from './constants'
import reducer from './modules/reducers'
import saga from './modules/sagas'
import rootStore from '../../store/createStore'

import Placeholder from './components/Skeleton'

const Preloader = () => <Placeholder type='full' />

const PickpocketContainer = Loadable({
  loader: async () => {
    const Pickpocket = await import(/* webpackChunkName: "pickpocket" */ './containers/Pickpocketing')

    injectReducer(rootStore, { key: 'pickpocketing', reducer })
    injectSaga({ key: 'pickpocketing', saga })

    // @ts-ignore
    rootStore.dispatch(fetchData('pickpocketing', `&step=crimesList&typeID=${typeID}`))

    return Pickpocket
  },
  loading: Preloader
})

export default PickpocketContainer
