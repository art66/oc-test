import { connect } from 'react-redux'
import CrimeRow from './CrimeRow'
import { closeOutcome } from '../../../../modules/actions'
import { removeExpiredRow } from '../../modules/actions'

const mapDispatchToProps = dispatch => ({
  closeOutcome: () => dispatch(closeOutcome()),
  removeExpiredRow: (crimeID, commitID) => dispatch(removeExpiredRow(crimeID, commitID))
})

export default connect(
  null,
  mapDispatchToProps
)(CrimeRow)
