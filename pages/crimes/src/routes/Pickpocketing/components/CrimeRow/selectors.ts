import { createSelector } from 'reselect'

import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getCrimeInfo } from '../../../../utils/globalSelectorsHelpers'
import { IRowGlobalSelector } from '../../../../utils/interfaces/IGlobalSelectors'

const BUTTON_LABEL = 'Pickpocket'

interface IPickpocketingCrimeInfo {
  muscular: string
  stats: string
}

const getSubID = props => props.subID
const getCommitID = props => props.commitID

const getTargetData = createSelector(
  [getCrimeInfo],
  crimeInfo => {
    const { muscular, stats } = crimeInfo as unknown as IPickpocketingCrimeInfo

    return `${muscular} ${stats}`
  }
)

const ownCrimeRowProps = createSelector(
  [getCommitID, getSubID, getTargetData],
  (commitID, subID, targetData) => ({
    commitID,
    subID,
    targetData
  })
)

const globalCrimeRowProps = createSelector(
  [globalRowPropsSelector, getCommitID, getTargetData],
  (globalProps: IRowGlobalSelector, commitID, targetData) => {
    return {
      ...globalProps,
      subTitle: targetData,
      button: {
        ...globalProps.button,
        label: BUTTON_LABEL,
        disabled: globalProps.button.disabled,
        action: () => globalProps.button.action({ subURL: `&value1=${commitID}`, subID: commitID })
      }
    }
  }
)

const selectorProps = props => ({
  ownCrimeRowProps: ownCrimeRowProps(props),
  throwGlobalCrimeRowProps: globalCrimeRowProps(props)
})

export default selectorProps
