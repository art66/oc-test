export interface ICrimeInfo {
  age: string
  gender: string
  height: string
  muscular: string
  status: {
    icon: string
    title: string
  }
}

export interface IProps {
  action: (url: any, itemId: any) => void
  removeExpiredRow: (crimeID: number, commitID: number) => void
  isRowActive: boolean
  itemId: number
  state: object
  story: object
  totalTime: number
  iconClass: string
  rowTimeLifecycle: number
  timeLeft: number
  crimeID: number
  commitID: string
  sortID: number
  currentRowID: number | string
  // isCrimesListMounted: boolean
  expired: boolean
  crimeInfo: ICrimeInfo
}

export interface IState {
  forceRemove: boolean
  attemptDisabled: boolean
  isRowCommitted: boolean
  isRowInstalled: boolean
}
