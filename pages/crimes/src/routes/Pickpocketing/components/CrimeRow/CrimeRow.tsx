import React, { Component, Fragment } from 'react'
import { CSSTransition } from 'react-transition-group'

import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import { pickpocketingRowsUpdate } from '../../utils/shouldRowsUpdate'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'

import { CrimeRow as CrimeRowGlobal } from '../../../../components'
import ProgressCircle from '../../../../components/ProgressCircle'

import selectorProps from './selectors'

import globalStyles from '../../../../styles/pickpocketing.cssmodule.scss'
import styles from './index.cssmodule.scss'
import { IProps, IState, ICrimeInfo } from './interfaces'

const ROW_ID = 'row_'
const IMG_ROOT = '/images/v2/2sef32sdfr422svbfr2/crimes/targets_faces/'
const ROW_ANIMATION_TIMING = 500

const ICONS_CONFIG = {
  distracted: {
    viewbox: '0 -4 24 24'
  },
  phone: {
    viewbox: '0 -3 18 18'
  },
  walking: {
    viewbox: '-1 0 17 18'
  },
  cycling: {
    viewbox: '2 -3 21 25'
  },
  conversation: {
    viewbox: '1 -2 16 20'
  },
  music: {
    viewbox: '1.5 -2 16 20'
  },
  loitering: {
    viewbox: '-3.5 0 16 20'
  },
  running: {
    viewbox: '0 -1 19 19'
  },
  soliciting: {
    viewbox: '0 0 13 22'
  },
  stumbling: {
    viewbox: '1 -1 19 19'
  },
  begging: {
    viewbox: '0 -.5 15 16'
  }
}

class CrimeRow extends Component<IProps, IState> {
  static defaultProps: IProps = {
    action: () => {},
    removeExpiredRow: () => {},
    isRowActive: false,
    itemId: null,
    state: null,
    story: null,
    totalTime: 0,
    iconClass: '',
    timeLeft: 0,
    rowTimeLifecycle: 0,
    crimeID: null,
    commitID: '',
    sortID: null,
    currentRowID: null,
    // isCrimesListMounted: false,
    expired: false,
    crimeInfo: ({} as ICrimeInfo)
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      forceRemove: false,
      attemptDisabled: false,
      isRowInstalled: false,
      isRowCommitted: false
    }
  }

  componentDidMount() {
    this._mountTooltips()
  }

  shouldComponentUpdate(nextProps, nextState) {
    const tempPrevProps = {
      ...this.props,
      ...this.state
    }

    const tempNextProps = {
      ...nextProps,
      ...nextState
    }

    const rowCheck = pickpocketingRowsUpdate(tempPrevProps, tempNextProps)
    const globalCheck = globalCrimeRowCheck(tempPrevProps, tempNextProps)

    return rowCheck || globalCheck || false
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _setInitMount = () => {
    this.setState({
      isRowInstalled: true
    })
  }

  _tooltipID = () => {
    const { crimeID, commitID, sortID } = this.props

    const rowTooltipID = `pickpocket__${ROW_ID}${crimeID}_${sortID}_${commitID}`

    return rowTooltipID
  }

  _getTooltip = () => {
    const { timeLeft } = this.props

    const childTitle = `Time left: ${timeLeft >= 0 ? timeLeft : '0'}s`

    return [{
      ID: this._tooltipID(),
      child: childTitle
    }]
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltip(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltip(), type: 'update' })

  // prevent clicks while the row is expired but being in the transition phase
  _handleExit = () => this.setState({ attemptDisabled: true })
  _handleExited = () => this._removeExpiredRow()

  _removeExpiredRow = () => {
    const { removeExpiredRow, crimeID, commitID } = this.props

    removeExpiredRow(crimeID, Number(commitID))
  }

  _isRowShouldExist = () => {
    const { isRowCommitted } = this.state
    const { timeLeft } = this.props

    const progress = this._getRowProgress()

    return !isRowCommitted && (timeLeft >= 0 || progress >= 0)
  }

  _getRowProgress = () => {
    const { totalTime, timeLeft } = this.props

    const progress = Number(((timeLeft * 100) / totalTime).toFixed(2))

    return progress
  }

  _rowImg = () => {
    const { iconClass } = this.props

    return (
      <img alt='' className={globalStyles.rowImg} src={`${IMG_ROOT}${iconClass}.png`} />
    )
  }

  _renderSpecialIcon = () => {
    const { isRowCommitted } = this.state
    const { commitID, crimeInfo } = this.props
    const { status } = crimeInfo || {}

    // WE DON'T NEED THIS IF TIMELEFT SHOULD OUTGOING SYNCHRONOUSLY BTW MULTIPLE TABS
    const progress = this._getRowProgress()

    return (
      <Fragment>
        <div className={styles.targetIconStatus}>
          <SVGIconGenerator
            iconName='TargetCondition'
            type={status.icon}
            preset={{ type: 'crimes', subtype: 'PICKPOCKETING' }}
            dimensions={{ viewbox: ICONS_CONFIG[status.icon].viewbox }}
          />
        </div>
        <div className={styles.targetIconCircle}>
          <ProgressCircle
            progress={this._isRowShouldExist() ? progress : 0}
            progressID={Number(commitID)}
            isAnimate={true}
            forceEmpty={isRowCommitted}
          />
        </div>
      </Fragment>
    )
  }

  _renderSpecialText = () => {
    const { crimeInfo } = this.props
    const { status } = crimeInfo || {}

    return (
      <div className={styles.targetStatus}>
        <span>{status.title}</span>
      </div>
    )
  }

  _callback = () => {
    this.setState({
      isRowCommitted: true
    })
  }

  _renderTargetStatus = () => {
    const { isRowCommitted } = this.state
    const { timeLeft } = this.props

    return (
      <div className={styles.targetWrap}>
        <div id={isRowCommitted || timeLeft < 0 ? '' : this._tooltipID()} className={styles.targetProgress}>
          {this._renderSpecialIcon()}
        </div>
        {this._renderSpecialText()}
      </div>
    )
  }

  _renderRow = () => {
    const { attemptDisabled, isRowCommitted } = this.state

    const {
      throwGlobalCrimeRowProps: props
    } = selectorProps(this.props)

    const modifiedGlobalProps = {
      ...props,
      button: {
        ...props.button,
        action: () => {
          props.button.action()
          this._callback()
        },
        disabled: isRowCommitted || attemptDisabled || props.button.disabled
      },
      closeOutcome: {
        ...props.closeOutcome,
        callback: () => this.setState({
          forceRemove: true
        })
      }
    }

    return (
      <CrimeRowGlobal
        {...this.props}
        {...modifiedGlobalProps}
        customImage={this._rowImg()}
        styles={globalStyles}
        rowShouldUpdate={pickpocketingRowsUpdate}
      >
        {this._renderTargetStatus()}
      </CrimeRowGlobal>
    )
  }

  _renderRowWithAnimation = () => {
    const { forceRemove, isRowCommitted } = this.state
    const { timeLeft, crimeID, currentRowID, commitID } = this.props

    const rowID = `${crimeID}_${commitID}`
    const isActiveRow = rowID === currentRowID

    return (
      <CSSTransition
        appear={true}
        in={!forceRemove && (isRowCommitted && isActiveRow || !isRowCommitted && timeLeft >= 0)}
        timeout={ROW_ANIMATION_TIMING}
        classNames='rowDisappear'
        onExit={this._handleExit}
        onExited={this._handleExited}
        unmountOnExit={true}
      >
        {this._renderRow()}
      </CSSTransition>
    )
  }

  render() {
    return this._renderRowWithAnimation()
  }
}

export default CrimeRow
