import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { showDropDownPanel, toggleArrows } from '../../../../modules/actions'
import BannersHolder from './BannersHolder'
import BannerGlobal from '../../../../components/Banner'
import '../../../../styles/searchforcash-banners.scss'
import '../../../../styles/animations.scss'
import styles from './index.cssmodule.scss'

const ANIMATION_DURATION = 1000

export interface IBannersProps {
  bannerID: number
  dropDownPanelToShow: Function
  toggleArrow: Function
}

export class Banners extends Component<IBannersProps> {
  static defaultProps = {
    bannerID: 1,
    dropDownPanelToShow: () => {},
    toggleArrow: () => {}
  }

  shouldComponentUpdate(nextProps) {
    const { bannerID } = this.props

    if (bannerID === nextProps.bannerID) {
      return false
    }

    return true
  }

  render() {
    const { bannerID, dropDownPanelToShow, toggleArrow } = this.props

    return (
      <BannerGlobal dropDownPanelToShow={dropDownPanelToShow} toggleArrows={toggleArrow}>
        <TransitionGroup className={styles.animationWrap}>
          <CSSTransition
            classNames='fade'
            key={bannerID}
            timeout={{ enter: ANIMATION_DURATION, exit: ANIMATION_DURATION }}
          >
            <BannersHolder bannerID={bannerID} />
          </CSSTransition>
        </TransitionGroup>
      </BannerGlobal>
    )
  }
}

const mapStateToProps = state => ({
  bannerID: state.pickpocketing.bannerID
})

const mapDispatchToProps = dispatch => ({
  dropDownPanelToShow: () => dispatch(showDropDownPanel()),
  toggleArrow: () => dispatch(toggleArrows())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Banners)
