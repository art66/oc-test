/* eslint-disable @typescript-eslint/naming-convention */
import React, { Component } from 'react'
import classnames from 'classnames'

import styles from './index.cssmodule.scss'

export interface IBannersHolderProps {
  bannerID: number
}

class BannersHolder extends Component<IBannersHolderProps> {
  static defaultProps = {
    bannerID: null
  }

  shouldComponentUpdate() {
    return true
  }

  _classNameHolder = currentBannerClass => classnames(styles.crimesPickpocketing, styles[currentBannerClass])

  _innerAnimation = () => {
    return (
      <div className={styles.scene}>
        <div className={styles.parallaxBG} />
        <div className={styles.parallaxModel} />
      </div>
    )
  }

  _wrapper = bannerName => {
    return <div className={this._classNameHolder(bannerName)}>{this._innerAnimation()}</div>
  }

  _BIKE = () => this._wrapper('bike')

  _CROSSWALK = () => this._wrapper('crosswalk')

  _UNDERGROUND = () => this._wrapper('underground')

  _ALLEY = () => this._wrapper('alley')

  _BENCH = () => this._wrapper('bench')

  _getCurrentBanner = () => ({
    1: this._BIKE(),
    2: this._CROSSWALK(),
    3: this._UNDERGROUND(),
    4: this._ALLEY(),
    5: this._BENCH()
  })

  render() {
    const { bannerID } = this.props

    const getBanner = this._getCurrentBanner()
    const Banner = !getBanner[bannerID] ? getBanner[1] : getBanner[bannerID]

    return Banner
  }
}

export default BannersHolder
