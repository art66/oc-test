import React from 'react'

import NoSubCrimesPlaceholder from '../../../../components/NoSubCrimesPlaceholder'

interface IProps {
  isBackdrop?: boolean
}

class NoTargetsPlaceholder extends React.PureComponent<IProps> {
  render() {
    const { isBackdrop } = this.props

    return (
      <NoSubCrimesPlaceholder
        isBackdrop={isBackdrop}
        text='No potential targets are within range currently. Please wait...'
      />
    )
  }
}

export default NoTargetsPlaceholder
