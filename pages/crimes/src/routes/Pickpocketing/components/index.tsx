import Banners from './Banners'
import CrimesList from './CrimesList/index'
import Skeleton from './Skeleton'

export { Banners, CrimesList, Skeleton }
