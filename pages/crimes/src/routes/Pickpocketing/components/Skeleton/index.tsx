import React from 'react'

import Placeholder from '../../../../components/Skeleton'

import globalStyles from '../../../../styles/skeleton.cssmodule.scss'
import styles from './styles.cssmodule.scss'

const CrimeRowPlaceholder = ({ type }: { type: string }) => {
  const rowsList = () => Array.from(Array(7).keys()).map(key => (
    <div className={globalStyles.rowWrap} key={key}>
      <div className={`${globalStyles.titleImg} ${styles.titleImg}`} />
      <div className={`${globalStyles.titleText} ${globalStyles.titleTextUp} ${styles.titleText}`} />
      <div className={`${globalStyles.titleSubText} ${globalStyles.titleSlim} ${styles.subTitle}`} />
      <div className={`${globalStyles.circlesPseudo} ${styles.crimeStatus}`} />
      <div className={`${globalStyles.nerveContainer} ${styles.nerve}`} />
      <div className={globalStyles.buttonAction} />
    </div>
  ))

  return (
    <Placeholder type={type}>
      <div className={globalStyles.rowsWrap}>
        {rowsList()}
      </div>
    </Placeholder>
  )
}

export default CrimeRowPlaceholder
