import React, { PureComponent } from 'react'

import CrimeRow from '../CrimeRow'
import NoTargetsPlaceholder from '../NoTargetsPlaceholder'

import styles from './index.cssmodule.scss'
import '../../../../styles/subcrimes.scss'

import { IOutcome } from '../../../../interfaces/IState'

interface ICrimesListProps {
  outcome: IOutcome
  attemptProgress: {
    itemId: number
  }
  startAttempt: Function
  lastSubCrimeID: number
  isCurrentRow: boolean
  crimes: {
    isRowActive: boolean
    crimeID: number
    commitID: string
    rowStory: object
    rowState: object
  }[]
  mediaType: string
  result: string
  story: string[]
  currentRowID: string | number
  updateCrimeTickers: Function
  runTargetsSync: Function
}

export class CrimesList extends PureComponent<ICrimesListProps> {
  private _timedID: any

  static defaultProps: ICrimesListProps = {
    outcome: {} as IOutcome,
    lastSubCrimeID: 0,
    startAttempt: () => {},
    attemptProgress: {
      itemId: null
    },
    isCurrentRow: false,
    crimes: [],
    mediaType: 'desktop',
    result: '',
    story: [],
    currentRowID: 0,
    updateCrimeTickers: () => {},
    runTargetsSync: () => {}
  }

  // constructor(props) {
  //   super(props)

  //   // this.state = {
  //   //   isInitMounted: false
  //   // }
  // }

  componentDidMount() {
    this._runRowsTimerUpdater()

    // updating target while user enter the screen
    window.addEventListener('focus', this._synchronizeTargets)
  }

  componentWillUnmount() {
    this._clearRowsTimerUpdater(this._timedID)

    window.removeEventListener('focus', this._synchronizeTargets)
  }

  _synchronizeTargets = () => {
    const { runTargetsSync } = this.props

    runTargetsSync()
  }

  _runRowsTimerUpdater = () => {
    const { updateCrimeTickers } = this.props

    this._timedID = setInterval(updateCrimeTickers, 1000)
  }

  _clearRowsTimerUpdater = timerID => {
    clearInterval(timerID)
  }

  // _setIniMountFromNestedRow = () => {
  //   const { isInitMounted } = this.state

  //   !isInitMounted && this.setState({ isInitMounted: true })
  // }

  _attempt = (url, itemId) => {
    const { startAttempt, currentRowID } = this.props
    const onCurrentRow = currentRowID === itemId

    startAttempt(url, itemId, onCurrentRow)
  }

  _buildCrimesList = () => {
    const list = []
    // const { isInitMounted } = this.state
    const {
      attemptProgress: { itemId },
      crimes
    } = this.props

    if (!crimes || crimes.length === 0) {
      return null
    }

    crimes.map(row => {
      const { crimeID, commitID } = row

      return list.push(
        <CrimeRow
          key={`${crimeID}_${commitID}`}
          crimeID={row.crimeID}
          action={this._attempt}
          {...row}
          {...this.props}
          // isCrimesListMounted={isInitMounted}
          // setInitMount={this._setIniMountFromNestedRow}
          isRowActive={row.isRowActive}
          itemId={itemId}
          state={row.rowState}
          story={row.rowStory}
        />
      )
    })

    return list
  }

  render() {
    const { crimes } = this.props

    return (
      <div className='main-container'>
        <div className={`subcrimes-list ${styles.subcrimesList}`}>
          {(!crimes || [0, 1].includes(crimes.length)) && <NoTargetsPlaceholder isBackdrop={true} />}
          <div>{this._buildCrimesList()}</div>
        </div>
      </div>
    )
  }
}

export default CrimesList
