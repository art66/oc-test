import { connect } from 'react-redux'
import CrimesList from './CrimesList'

import { updateCrimeTickers, runTargetsSync } from '../../modules/actions'
import { attempt } from '../../../../modules/actions'

import getGlobalSelector from '../../../../utils/globalListPropsSelector'

const mapStateToProps = ({ pickpocketing, browser, common }) => {
  return getGlobalSelector(pickpocketing, browser, common)
}

const mapDispatchToProps = dispatch => ({
  startAttempt: (url, itemId, isCurrentRow) => dispatch(attempt('pickpocketing', url, itemId, isCurrentRow)),
  updateCrimeTickers: () => dispatch(updateCrimeTickers()),
  runTargetsSync: () => dispatch(runTargetsSync())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrimesList)
