import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { clearOutdatedOutcome, pooling } from '../../../modules/actions'

import { Banners, CrimesList, Skeleton } from '../components'
import SubCrimesLayout from '../../../components/SubCrimesLayout'

import styles from '../styles/container.cssmodule.scss'

export interface IPickpocketingContainerProps {
  state: {
    searchforcash: {
      outcome: {
        story: string[]
      }
    }
  }
  showPlaceholder: boolean
  current: number
  progress: number
  stats: object[]
  startPooling: Function
  startClearOutdatedOutcome: Function
}

class PickpocketingContainer extends PureComponent<IPickpocketingContainerProps> {
  private _poollingTimerId: any

  static defaultProps: IPickpocketingContainerProps = {
    state: {
      searchforcash: {
        outcome: {
          story: null
        }
      }
    },
    startPooling: () => {},
    startClearOutdatedOutcome: () => {},
    showPlaceholder: true,
    current: null,
    progress: null,
    stats: []
  }

  componentDidMount() {
    this._poollingTimerId = setInterval(this._startPoolingTimer, 5000)
  }

  componentWillUnmount() {
    this._clearPoolingTimer(this._poollingTimerId)

    const {
      startClearOutdatedOutcome,
      state: { searchforcash }
    } = this.props

    const { outcome: { story = [] } = {} } = searchforcash || {}

    if (!story || (searchforcash && story.length !== 0)) {
      startClearOutdatedOutcome()
    }
  }

  _startPoolingTimer = () => {
    const { startPooling } = this.props

    startPooling()
  }

  _clearPoolingTimer = timer => {
    clearInterval(timer)
  }

  render() {
    const { stats, current, progress, showPlaceholder } = this.props

    return (
      <SubCrimesLayout
        customWrapClass={styles.pickpocketing}
        showPlaceholder={showPlaceholder}
        title='Pickpocket'
        skeleton={Skeleton}
        banners={<Banners />}
        stats={stats}
        current={current}
        progress={progress}
      >
        <CrimesList />
      </SubCrimesLayout>
    )
  }
}

const mapStateToProps = state => ({
  stat: state.pickpocketing,
  current: state.pickpocketing.currentLevel,
  progress: state.pickpocketing.skillLevelTotal,
  stats: state.pickpocketing.currentUserStatistics,
  showPlaceholder: state.pickpocketing.showPlaceholder
})

const mapDispatchToProps = dispatch => ({
  startClearOutdatedOutcome: () => dispatch(clearOutdatedOutcome('pickpocketing')),
  startPooling: () => dispatch(pooling('pickpocketing'))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PickpocketingContainer)
