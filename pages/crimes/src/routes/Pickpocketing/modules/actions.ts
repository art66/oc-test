// -----------------------------
// PICKPOCKETING ACTIONS
// -----------------------------
import { CRIMES_TICKER_UPDATER, EXPIRED_ROW_REMOVED, SYNCHRONIZE_TARGETS, SYNCHRONIZE_TARGETS_FINISH } from '../constants'

// ------------------------------------
// Actions
// ------------------------------------
export const updateCrimeTickers = () => ({
  type: CRIMES_TICKER_UPDATER
})

export const removeExpiredRow = (crimeID, commitID) => ({
  type: EXPIRED_ROW_REMOVED,
  crimeID,
  commitID
})

export const runTargetsSync = () => ({
  type: SYNCHRONIZE_TARGETS
})

export const finishTargetsSync = json => ({
  type: SYNCHRONIZE_TARGETS_FINISH,
  payload: json.DB
})
