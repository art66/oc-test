import initialState from './initialState'
import updateCrimeTargets from '../utils/updateCrimeTargets'

import {
  DATA_FETCHED,
  ATTEMPT_START,
  ATTEMPT_FETCHED,
  LEVEL_UP,
  SET_CRIME_LEVEL,
  CLEAR_OUTDATED_OUTCOME,
  POOLING_FETCHED,
  RESET_STATS_DONE,
  CRIMES_TICKER_UPDATER,
  EXPIRED_ROW_REMOVED,
  SYNCHRONIZE_TARGETS,
  SYNCHRONIZE_TARGETS_FINISH
} from '../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [DATA_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    crimesByType: action.json.DB.crimesByType,
    // crimesByType: action.json.DB.crimesByType.map(crime => ({
    //   ...crime,
    //   rowTimeLifecycle: crime.timeLeft
    // })),
    showPlaceholder: false
  }),
  [ATTEMPT_START]: (state, action) => ({
    ...state,
    // bannerID: crimeIDNormalizer(action.itemId),
    attemptProgress: {
      itemId: action.itemId,
      inProgress: true
    },
    currentRowID: action.itemId,
    isCurrentRow: action.isCurrentRow
  }),
  [ATTEMPT_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    attemptProgress: {
      ...state.attemptProgress,
      inProgress: false
    }
  }),
  [LEVEL_UP]: (state, action) => ({
    ...state,
    level: action.payload.level
  }),
  [SET_CRIME_LEVEL]: (state, action) => ({
    ...state,
    ...action?.result?.DB
  }),
  [CLEAR_OUTDATED_OUTCOME]: state => ({
    ...state,
    outcome: {},
    isCurrentRow: false,
    currentRowID: -1,
    attemptProgress: {
      ...state.attemptProgress,
      itemId: -1
    }
  }),
  [POOLING_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    crimesByType: updateCrimeTargets(state.crimesByType, action.json.DB.crimesByType)
  }),
  [SYNCHRONIZE_TARGETS]: state => ({
    ...state,
    attemptProgress: {
      ...state.attemptProgress,
      inProgress: true
    }
  }),
  [SYNCHRONIZE_TARGETS_FINISH]: (state, action) => ({
    ...state,
    attemptProgress: {
      ...state.attemptProgress,
      inProgress: false
    },
    crimesByType: updateCrimeTargets(state.crimesByType, action.payload.crimesByType)
  }),
  [RESET_STATS_DONE]: (state, action) => ({
    ...state,
    ...action.json.DB
  }),
  [CRIMES_TICKER_UPDATER]: state => {
    return {
      ...state,
      crimesByType: state.crimesByType.map(crime => ({
        ...crime,
        timeLeft: crime.timeLeft - 1
      }))
    }
  },
  [EXPIRED_ROW_REMOVED]: (state, action) => {
    const findExpiredRow = crime => `${crime.crimeID}_${crime.commitID}` !== `${action.crimeID}_${action.commitID}`

    return {
      ...state,
      crimesByType: state.crimesByType.filter(findExpiredRow)
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
export default (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
