import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { ROOT_URL } from '../../../../constants'
import { finishTargetsSync } from '../actions'
import { showDebugInfo } from '../../../../modules/actions'
// import getState from './helpers/getState'

export function* synchronizeTargets() {
  try {
    const json = yield fetchUrl(`${ROOT_URL}&step=crimesList&typeID=5&type=poll`, null, false)

    if (json.DB.error) {
      throw new Error(json.DB.error)
    }

    // const { pickpocketing } = yield getState()
    // const { attemptProgress } = pickpocketing

    // if (attemptProgress.inProgress) {
    //   return
    // }

    yield put(finishTargetsSync(json))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default synchronizeTargets
