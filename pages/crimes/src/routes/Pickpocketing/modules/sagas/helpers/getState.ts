import { select } from 'redux-saga/effects'

function* getState() {
  const currentState = yield select(state => state)

  return currentState
}

export default getState
