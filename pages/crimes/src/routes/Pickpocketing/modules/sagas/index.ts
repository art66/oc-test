import { takeLatest } from 'redux-saga/effects'

import fetchData from './fetchData'
import attempt from './attempt'
import pooling from './pooling'
import updateCrimeTargets from './synchronizeTargets'

import { ATTEMPT, FETCH_DATA, POOLING, SYNCHRONIZE_TARGETS } from '../../constants'

export default function* watchPickpocketing() {
  yield takeLatest(FETCH_DATA, fetchData)
  yield takeLatest(ATTEMPT, attempt)
  yield takeLatest(POOLING, pooling)
  yield takeLatest(SYNCHRONIZE_TARGETS, updateCrimeTargets)
}
