import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { ROOT_URL } from '../../../../constants'
import { poolingFetched, showDebugInfo } from '../../../../modules/actions'
import getState from './helpers/getState'

export function* pooling() {
  try {
    const json = yield fetchUrl(`${ROOT_URL}&step=crimesList&typeID=5&type=poll`, null, false)

    if (json.DB?.error) {
      yield put(showDebugInfo({ msg: json.DB.error }))

      return
      // throw new Error(json.DB.error)
    }

    const { pickpocketing } = yield getState()
    const { attemptProgress } = pickpocketing

    if (attemptProgress.inProgress) {
      return
    }

    yield put(poolingFetched('pickpocketing', json))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default pooling
