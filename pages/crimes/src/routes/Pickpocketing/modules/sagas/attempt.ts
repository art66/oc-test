import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { attemptStart, userJailed, attemptFetched,  showDebugInfo } from '../../../../modules/actions'
import { ROOT_URL } from '../../../../constants'

export function* attempt({ url, itemId, isCurrentRow }: any) {
  try {
    yield put(attemptStart('pickpocketing', itemId, isCurrentRow))
    const json = yield fetchUrl(`${ROOT_URL}${url}`, null, false)

    const {
      DB: { error, outcome }
    } = json

    if (error) {
      throw new Error(error)
    }

    if (!['success', 'failed'].includes(outcome?.result)) {
      yield put(userJailed(true))
    }

    yield put(attemptFetched('pickpocketing', json))

    // yield put(updateExp())
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default attempt
