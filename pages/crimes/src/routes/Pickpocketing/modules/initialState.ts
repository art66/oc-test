import getRandomIntInclusive from '@torn/shared/utils/getRandomValue'

export default {
  attemptProgress: {
    inProgress: false,
    itemId: 0
  },
  isMounted: true,
  typeID: 5,
  buttonLabel: 'Attempt',
  isCurrentRow: false,
  showPlaceholder: true,
  bannerID: getRandomIntInclusive(1, 5),
  crimesByType: [
    {
      currentTemplate: 'pickpocket',
      skillLevel: 0,
      skillLevelTotal: null,
      title: 'PickPock',
      nerve: 0,
      iconClass: 'PickPock',
      crimeID: 0,
      subID: 0,
      commitID: null,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        barValue: 0,
        title: '',
        progressIconState: 0,
        currentSearchers: 0
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    }
  ],
  additionalInfo: null,
  currentType: {
    _id: {
      $oid: ''
    },
    ID: 0,
    typeID: 0,
    title: 'Pickpocket',
    crimeRoute: 'pickpocket',
    active: 0,
    img: '',
    expForLevel100: 0
  },
  skillLevelTotal: 0,
  currentLevel: null,
  lastSubCrimeID: 0,
  currentUserStats: {
    failedTotal: 0,
    attemptsTotal: 0,
    crimesByIDAttempts: {
      1: 0,
      2: 0,
      3: 0
    },
    successesTotal: 0,
    moneyFoundTotal: 0,
    jailedTotal: 0,
    itemsFound: {
      1: 0
    },
    critFailedTotal: 0,
    statsByType: [
      {
        successesTotal: 0
      }
    ],
    skill: 0,
    skillLevel: 0
  },
  currentUserStatistics: []
}
