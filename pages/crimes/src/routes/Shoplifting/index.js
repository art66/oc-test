import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import { fetchData } from '../../modules/actions'
import { typeID } from './constants'
import reducer from './modules/reducers'
import saga from './modules/saga'
import rootStore from '../../store/createStore'

import Placeholder from './components/Skeleton'

const Preloader = () => <Placeholder type='full' />

const ShopliftingContainer = Loadable({
  loader: async () => {
    const Shoplifting = await import(/* webpackChunkName: "shoplifting" */ './containers/Shoplifting')

    injectReducer(rootStore, { key: 'shoplifting', reducer })
    injectSaga({ key: 'shoplifting', saga })
    rootStore.dispatch(fetchData('shoplifting', `&step=crimesList&typeID=${typeID}`))

    return Shoplifting
  },
  loading: Preloader
})

export default ShopliftingContainer
