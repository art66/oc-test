import React from 'react'
import SVGIcon from '../../../../components/SVGIcon'

import iconsSchema from './iconsSchemas'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

const SECURITY_HOLDER = ['camera', 'guard', 'checkpoint']
const SECURITY_LEVEL = ['one', 'two', 'three', 'four']

class CrimeRowIcon extends React.PureComponent<IProps> {
  _getCurrentSecurityType = () => {
    const { iconName = '' } = this.props

    const currentSecurity = SECURITY_HOLDER.filter(secType => {
      const regExpVar = new RegExp(secType, 'i')

      if (iconName.match(regExpVar)) {
        return secType
      }

      return
    })

    return currentSecurity[0]
  }

  _getCurrentSecurityLevel = () => {
    const { iconName = '' } = this.props

    const securityLevel = SECURITY_LEVEL.filter(secType => {
      const regExpVar = new RegExp(secType, 'i')

      if (iconName.match(regExpVar)) {
        return secType
      }

      return
    })

    return securityLevel[0]
  }

  render() {
    const currentSecurity = this._getCurrentSecurityType()
    const securityLevel = this._getCurrentSecurityLevel()

    const { iconName: SVGIconName = '', layoutTypes = '' } = iconsSchema[currentSecurity] || {}

    if (!SVGIconName) {
      return null
    }

    const iconProps = {
      iconName: SVGIconName,
      ...layoutTypes[securityLevel],
      customClass: styles.customSVGIcon,
      preset: { type: 'crimes', subtype: 'SHOPLIFTING' }
    }

    return (
      <div className={styles.svgIconWrap}>
        <SVGIcon iconProps={iconProps} />
      </div>
    )
  }
}

export default CrimeRowIcon
