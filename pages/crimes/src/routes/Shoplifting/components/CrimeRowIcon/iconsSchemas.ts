const iconsSchema = {
  camera: {
    iconName: 'SecurityCamera',
    layoutTypes: {
      one: {
        dimensions: {
          width: '20px',
          height: '16px',
          viewbox: '0 0 20 17'
        }
      },
      two: {
        type: 'cameraTwo',
        dimensions: {
          width: '24px',
          height: '16px',
          viewbox: '0 0 24 16'
        }
      },
      three: {
        type: 'cameraThree',
        dimensions: {
          width: '24px',
          height: '24px',
          viewbox: '0 0 24 24'
        }
      },
      four: {
        type: 'cameraFour',
        dimensions: {
          width: '24px',
          height: '27px',
          viewbox: '0 0 24 27'
        }
      }
    }
  },
  guard: {
    iconName: 'Guard',
    layoutTypes: {
      one: {
        dimensions: {
          width: '13px',
          height: '15px',
          viewbox: '0 0 13 15'
        }
      },
      two: {
        type: 'twoGuards',
        dimensions: {
          width: '21px',
          height: '11px',
          viewbox: '0 0 21 11'
        }
      },
      three: {
        type: 'threeGuards',
        dimensions: {
          width: '17px',
          height: '17px',
          viewbox: '0 0 18 17'
        }
      }
    }
  },
  checkpoint: {
    iconName: 'Checkpoint',
    layoutTypes: {
      undefined: {
        dimensions: {
          width: '12px',
          height: '16px',
          viewbox: '0 0 12 16'
        }
      }
    }
  }
}

export default iconsSchema
