import React, { PureComponent } from 'react'
import CrimeRow from '../CrimeRow/CrimeRow'
import defineStatTimerInterval from '../../modules/statusTimers'
import propTypes from './propTypes'
import '../../../../styles/subcrimes.scss'

export class CrimesList extends PureComponent {
  componentDidMount() {
    const { statusTimersAction, crimes = [] } = this.props
    const crimesArray = Array.from(Array(crimes.length).keys())

    this.timer = defineStatTimerInterval(crimesArray, statusTimersAction)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  _buildCrimesList = () => {
    const crimesList = []
    const {
      attemptProgress = {},
      crimes = [],
      mediaType,
      lastSubCrimeID,
      currentRowID,
      isCurrentRow,
      isDesktopLayoutSetted,
      typeID,
      jailed,
      buttonLabel,
      closeOutcome
    } = this.props

    crimes
      && crimes.map((row, i) => crimesList.push(
        <CrimeRow
          {...row}
          key={i}
          isRowActive={row.isRowActive}
          action={this._attempt}
          lastSubCrimeID={lastSubCrimeID}
          currentRowID={currentRowID}
          isCurrentRow={isCurrentRow}
          itemId={attemptProgress.itemId}
          crimeID={row.crimeID}
          buttonLabel={buttonLabel}
          typeID={typeID}
          mediaType={mediaType}
          jailed={jailed}
          isDesktopLayoutSetted={isDesktopLayoutSetted}
          state={row.rowState}
          closeOutcome={closeOutcome}
        />
      ))

    return crimesList
  }

  _attempt = (url, itemId) => {
    const { startAttempt, currentRowID } = this.props
    const onCurrentRow = currentRowID === Number(itemId)

    startAttempt(url, itemId, onCurrentRow)
  }

  render() {
    return (
      <div className='main-container'>
        <div className='subcrimes-list'>
          <div>{this._buildCrimesList()}</div>
        </div>
      </div>
    )
  }
}

CrimesList.propTypes = propTypes

export default CrimesList
