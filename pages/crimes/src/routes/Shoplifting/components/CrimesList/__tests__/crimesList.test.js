import React from 'react'
import { mount } from 'enzyme'
import initialState, { emptyCrimeList } from './mocks/crimesList'
import CrimesList from '../CrimesList'

describe('<CrimesList />', () => {
  const {
    attemptProgress: { itemId, inProgress },
    outcome: { story }
  } = initialState

  it('should render whole crimes list', () => {
    const Component = mount(<CrimesList {...initialState} />)
    const crimes = Component.instance()._buildCrimesList()

    expect(crimes.length).toBe(6)
    expect(Component).toMatchSnapshot()
  })
  it('should not render crimes list', () => {
    const Component = mount(<CrimesList {...emptyCrimeList} />)
    const crimes = Component.instance()._buildCrimesList()

    expect(crimes.length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should throw outcome to the 2nd subcrime', () => {
    const Component = mount(<CrimesList {...initialState} />)
    const getCrimesList = Component.instance()._buildCrimesList()
    const otcomeToSubcrime = getCrimesList.filter(crime => itemId === crime.props.crimeID) || false
    const isOutomeMustBeShowed = story && otcomeToSubcrime && !inProgress

    expect(isOutomeMustBeShowed).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
})
