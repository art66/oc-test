const initialState = {
  jailed: false,
  buttonLabel: 'Shoplift',
  startAttempt: () => {},
  statusTimersAction: () => {},
  attemptProgress: {
    inProgress: false
  },
  isCurrentRow: false,
  showPlaceholder: true,
  outcome: {
    outcomeDesc: null,
    result: '',
    rewardsGive: {
      itemsRewards: null,
      userRewards: {
        money: 0
      }
    },
    story: []
  },
  crimes: [
    {
      currentTemplate: 'shoplifting',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'shoplifting',
      crimeID: 19,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        notoriety: 100,
        statusEffects: [
          {
            disabled: false,
            reduceRFC: 0,
            timePercent: 0,
            timeTillNextCycle: 0,
            title: 'oneCamera'
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    },
    {
      currentTemplate: 'shoplifting',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'shoplifting',
      crimeID: 20,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        notoriety: 100,
        statusEffects: [
          {
            disabled: false,
            reduceRFC: 0,
            timePercent: 0,
            timeTillNextCycle: 0,
            title: 'oneCamera'
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    },
    {
      currentTemplate: 'shoplifting',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'shoplifting',
      crimeID: 21,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        notoriety: 100,
        statusEffects: [
          {
            disabled: false,
            reduceRFC: 0,
            timePercent: 0,
            timeTillNextCycle: 0,
            title: 'oneCamera'
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    },
    {
      currentTemplate: 'shoplifting',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'shoplifting',
      crimeID: 22,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        notoriety: 100,
        statusEffects: [
          {
            disabled: false,
            reduceRFC: 0,
            timePercent: 0,
            timeTillNextCycle: 0,
            title: 'oneCamera'
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    },
    {
      currentTemplate: 'shoplifting',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'shoplifting',
      crimeID: 23,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        notoriety: 100,
        statusEffects: [
          {
            disabled: false,
            reduceRFC: 0,
            timePercent: 0,
            timeTillNextCycle: 0,
            title: 'oneCamera'
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    },
    {
      currentTemplate: 'shoplifting',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'shoplifting',
      crimeID: 24,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        notoriety: 100,
        statusEffects: [
          {
            disabled: false,
            reduceRFC: 0,
            timePercent: 0,
            timeTillNextCycle: 0,
            title: 'oneCamera'
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    }
  ],
  additionalInfo: null,
  currentType: {
    _id: {
      $oid: ''
    },
    ID: 0,
    typeID: 0,
    title: 'shoplifting',
    crimeRoute: 'shoplifting',
    active: 0,
    img: '',
    expForLevel100: 0
  },
  skillLevelTotal: 0,
  currentLevel: 0,
  lastSubCrimeID: 0,
  currentUserStats: {
    failedTotal: 0,
    attemptsTotal: 0,
    crimesByIDAttempts: {
      1: 0,
      2: 0,
      3: 0
    },
    successesTotal: 0,
    moneyFoundTotal: 0,
    jailedTotal: 0,
    itemsFound: {
      1: 0
    },
    critFailedTotal: 0,
    statsByType: [
      {
        successesTotal: 0
      }
    ],
    skill: 0,
    skillLevel: 0
  },
  currentUserStatistics: [
    {
      label: '',
      value: ''
    }
  ]
}

export const emptyCrimeList = {
  ...initialState,
  crimes: []
}

export default initialState
