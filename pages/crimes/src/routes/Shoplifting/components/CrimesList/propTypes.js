import pt from 'prop-types'

const propTypes = {
  mediaType: pt.string,
  currentRowID: pt.number,
  isCurrentRow: pt.bool,
  statusTimersAction: pt.func.isRequired,
  startAttempt: pt.func.isRequired,
  lastSubCrimeID: pt.number,
  attemptProgress: pt.shape({
    inProgress: pt.bool,
    itemId: pt.number
  }),
  outcome: pt.shape({
    outcomeDesc: pt.string,
    result: pt.string,
    rewardsGive: pt.shape({
      itemsRewards: null,
      userRewards: pt.shape({
        money: pt.number
      })
    }),
    story: pt.arrayOf(pt.string)
  }),
  crimes: pt.arrayOf(
    pt.shape({
      additionalInfo: pt.shape({
        notoriety: pt.number,
        statusEffects: pt.arrayOf(
          pt.shape({
            disabled: pt.bool,
            reduceRFC: pt.nubmer,
            timeDisabled: pt.nubmer,
            timePercent: pt.nubmer
          })
        )
      }),
      available: pt.bool,
      crimeID: pt.number,
      iconClass: pt.string,
      nerve: pt.number,
      panelItems: pt.arrayOf(
        pt.shape({
          label: pt.string,
          removeLink: pt.bool,
          value: pt.string
        })
      ),
      requirements: pt.object,
      result: pt.string,
      skillLevel: pt.number,
      skillLevelTotal: pt.number,
      subID: pt.number,
      title: pt.string
    })
  )
}

export default propTypes
