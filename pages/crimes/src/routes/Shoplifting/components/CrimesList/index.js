import { connect } from 'react-redux'
import CrimesList from './CrimesList'
import { closeOutcome, attempt } from '../../../../modules/actions'
import { statusTimersUpdater } from '../../modules/actions'
import getGlobalSelector from '../../../../utils/globalListPropsSelector'

const mapStateToProps = ({ shoplifting, browser, common }) => {
  return getGlobalSelector(shoplifting, browser, common)
}

const mapDispatchToProps = dispatch => ({
  closeOutcome: () => dispatch(closeOutcome()),
  startAttempt: (url, itemId, isCurrentRow) => dispatch(attempt('shoplifting', url, itemId, isCurrentRow)),
  statusTimersAction: statTimersRowPayload => dispatch(statusTimersUpdater(statTimersRowPayload))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrimesList)
