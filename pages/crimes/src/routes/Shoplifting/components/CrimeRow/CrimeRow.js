import React, { Component } from 'react'
import classnames from 'classnames'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import { shopliftingRowsUpdate } from '../../utils/shouldRowsUpdate'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'
import propTypes from './propTypes'
import ownCrimeRowProps from './selectors'

import { CrimeRow as CrimeRowGlobal } from '../../../../components'
import CrimeRowIcon from '../CrimeRowIcon'
import ProgressCircle from '../../../../components/ProgressCircle'

import { CRIMES_COUNT, CAMERA_CLASSES_HOLDER, GUARD_CLASSES_HOLDER } from '../../constants'
import styles from '../../../../styles/shoplifting.cssmodule.scss'

class CrimeRow extends Component {
  componentDidMount() {
    this._mountTooltips()
  }

  shouldComponentUpdate(nextProps) {
    const rowCheck = shopliftingRowsUpdate(this.props, nextProps)
    const globalCheck = globalCrimeRowCheck(this.props, nextProps)

    return rowCheck || globalCheck || false
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _classNameHolder = () => {
    const { mediaType, isDesktopLayoutSetted } = this.props

    return {
      wrapClassName: classnames([styles['pb-wrap']], [styles.dlm], [styles.center], [styles.underTitle]),
      statusEffectClassName: classnames([styles.statusEffects], [styles.dlm], [styles.center]),
      progressBarClassName: classnames([styles['pb']], {
        [styles['pb--short']]: !isDesktopLayoutSetted && mediaType !== 'desktop'
      })
    }
  }

  _getRealRowID = () => {
    const { crimeID } = this.props

    return crimeID - CRIMES_COUNT
  }

  _tooltipIDs = () => {
    const { crimeID } = this.props

    return {
      progressRow: `progressRow_notoriety_${crimeID}`,
      progressCamera: `progressRow_camera_${crimeID}`,
      progressGuard: `progressRow_guard_${crimeID}`
    }
  }

  _getTooltipsList = () => {
    const { additionalInfo: { notoriety } = {} } = this.props
    const {
      checkCameraProgress: { statusCamera },
      checkGuardProgress: { statusCheckpointOrGuard }
    } = ownCrimeRowProps(this.props)
    const { progressRow, progressCamera, progressGuard } = this._tooltipIDs()
    const rowID = this._getRealRowID()

    const itemsToTooltips = [
      {
        ID: progressRow,
        child: `Notoriety: ${notoriety}%`
      },
      {
        ID: progressCamera,
        child: statusCamera
      }
    ]

    if (GUARD_CLASSES_HOLDER[rowID] !== 'noneCheck') {
      itemsToTooltips.push({
        ID: progressGuard,
        child: statusCheckpointOrGuard
      })
    }

    return itemsToTooltips
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltipsList(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltipsList(), type: 'update' })

  render() {
    const { additionalInfo = {} } = this.props
    const rowID = this._getRealRowID()

    const { notoriety } = additionalInfo
    const {
      checkCameraProgress: { isCameraDisabled, progressOffCamera },
      checkGuardProgress: { isGuardsDisabled, progressOffGuard },
      throwGlobalCrimeRowProps: props
    } = ownCrimeRowProps(this.props)

    const { wrapClassName, progressBarClassName, statusEffectClassName } = this._classNameHolder()

    const { progressRow, progressCamera, progressGuard } = this._tooltipIDs()

    return (
      <CrimeRowGlobal {...props} rowShouldUpdate={shopliftingRowsUpdate}>
        <div id={progressRow} className={wrapClassName}>
          <div className={progressBarClassName}>
            <div className={styles['bar-line']} style={{ width: `${notoriety}%` }} />
          </div>
        </div>
        <div className={statusEffectClassName}>
          <div id={progressCamera} className={styles.cameraStatus}>
            <CrimeRowIcon iconName={CAMERA_CLASSES_HOLDER[rowID]} />
            <div className={styles.roundCircle}>
              <ProgressCircle progress={progressOffCamera} disableSecurity={isCameraDisabled} />
            </div>
          </div>
          <div id={progressGuard} className={styles.guardsStatus}>
            <CrimeRowIcon iconName={GUARD_CLASSES_HOLDER[rowID]} />
            <div className={styles.roundCircle}>
              <ProgressCircle progress={progressOffGuard} disableSecurity={isGuardsDisabled} />
            </div>
          </div>
        </div>
      </CrimeRowGlobal>
    )
  }
}

CrimeRow.propTypes = propTypes

CrimeRow.defaultProps = {
  additionalInfo: {
    notoriety: 0,
    statusEffects: [{}, {}]
  }
}

export default CrimeRow
