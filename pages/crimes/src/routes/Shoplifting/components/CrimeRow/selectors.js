import { createSelector } from 'reselect'
import { CRIMES_COUNT } from '../../constants'
import { UNDERTITLE } from '../../../../constants'
import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getCrimeId, getIconClass, getAdditionalInfo } from '../../../../utils/globalSelectorsHelpers'
import styles from '../../../../styles/shoplifting.cssmodule.scss'

// we need to make a multi name layout for the camera and guards text,
// because with the current abstraction it would be to hard.
// perhaps we would refactor with hack somehow in future.
const getType = props => props.type
const getID = props => props.ID
const ROWS_TO_HACK = {
  camera: [22, 23, 24],
  guard: [24]
}
const LABELS = {
  camera: {
    multi: 'Cameras',
    single: 'Camera'
  },
  guard: {
    multi: 'Guards',
    single: 'Guard'
  }
}

// reselect functions
const rowIcon = createSelector(
  [getIconClass],
  iconClass => `${styles.subcrimeImgSprite} ${styles[iconClass]}`
)

const globalCrimeRowProps = createSelector(
  [rowIcon, globalRowPropsSelector],
  (iconClass, globalProps) => {
    return {
      ...globalProps,
      underTitle: UNDERTITLE,
      styles,
      iconClass
    }
  }
)

const securityLabelState = createSelector(
  [getType, getID],
  (type, ID) => {
    const { multi, single } = LABELS[type]
    const rowsToHack = ROWS_TO_HACK[type]

    return rowsToHack.includes(ID) ? multi : single
  }
)

const cameraProgress = createSelector(
  [getCrimeId, getAdditionalInfo],
  (crimeID, additionalInfo) => {
    const { statusEffects = [] } = additionalInfo
    const secLabel = securityLabelState({ type: 'camera', ID: crimeID })

    const { timePercent: timeDisabledCamera = 0 } = statusEffects[0]
    const isCameraDisabled = statusEffects[0].disabled
    const progressOffCamera = parseInt(timeDisabledCamera.toFixed(), 10)
    const statusCamera = isCameraDisabled ?
      `Status: ${secLabel} disabled (${progressOffCamera}%)` :
      `Status: ${secLabel} recording (${progressOffCamera}%)`

    return {
      isCameraDisabled,
      progressOffCamera,
      statusCamera
    }
  }
)

const guardProgress = createSelector(
  [getCrimeId, getAdditionalInfo],
  (crimeID, additionalInfo) => {
    const { statusEffects = [] } = additionalInfo
    const secLabel = securityLabelState({ type: 'guard', ID: crimeID })

    const { timePercent: timeDisabledGuard = 0 } = statusEffects[1] || {}
    const rowID = crimeID - CRIMES_COUNT
    const inactiveRows = rowID === 1 || rowID === 2
    const statusDisabled = (statusEffects[1] && statusEffects[1].disabled) || false

    const isGuardsDisabled = inactiveRows ? false : statusDisabled
    const isCheckpointsStatus = (statusEffects[1] && statusEffects[1].title) === 'Checkpoint'
    const progressOffGuard = inactiveRows ? 0 : parseInt(timeDisabledGuard.toFixed(), 10)

    const checkStatusInfo = () => {
      const statusCheckpoint = isGuardsDisabled ?
        `Status: Checkpoints broken (${progressOffGuard}%)` :
        `Status: Checkpoints enabled (${progressOffGuard}%)`
      const statusGuard = isGuardsDisabled ?
        `Status: ${secLabel} on break ${progressOffGuard}%` :
        `Status: ${secLabel} patrolling (${progressOffGuard}%)`

      const statusCheckpointOrGuard = isCheckpointsStatus ? statusCheckpoint : statusGuard

      return statusCheckpointOrGuard
    }

    return {
      isGuardsDisabled,
      progressOffGuard,
      statusCheckpointOrGuard: checkStatusInfo()
    }
  }
)

const ownCrimeRowProps = props => ({
  checkCameraProgress: cameraProgress(props),
  checkGuardProgress: guardProgress(props),
  throwGlobalCrimeRowProps: globalCrimeRowProps(props)
})

export default ownCrimeRowProps
