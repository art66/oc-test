import pt from 'prop-types'

const propTypes = {
  outcome: pt.object,
  itemId: pt.number,
  closeOutcome: pt.func,
  lastSubCrimeID: pt.number,
  currentRowID: pt.number,
  isCurrentRow: pt.bool,
  action: pt.func,
  additionalInfo: pt.object,
  attemptProgress: pt.object,
  available: pt.bool,
  crimeID: pt.number,
  iconClass: pt.string,
  mediaType: pt.string,
  nerve: pt.number,
  requirements: pt.object,
  showOutcome: pt.bool,
  state: pt.string,
  title: pt.string.isRequired
}

export default propTypes
