const initialState = {
  currentTemplate: 'shoplifting',
  skillLevel: 0,
  skillLevelTotal: 0,
  title: 'Search the Trash',
  nerve: 0,
  iconClass: 'shoplifting',
  crimeID: 3,
  subID: 0,
  requirements: {
    items: {},
    minCrimeLevel: {}
  },
  available: true,
  additionalInfo: {
    notoriety: 0,
    statusEffects: [
      {
        disabled: false,
        reduceRFC: 0,
        timePercent: 50,
        timeTillNextCycle: 10,
        title: 'oneCamera'
      }
    ]
  },
  panelItems: [
    {
      removeLink: true,
      label: '',
      value: ''
    },
    {
      removeLink: false,
      label: '',
      value: ''
    }
  ],
  result: 'info'
}

export const oneGuardStatus = {
  ...initialState
}

export const twoGuardStatuses = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    statusEffects: [
      ...initialState.additionalInfo.statusEffects,
      {
        disabled: false,
        reduceRFC: 0,
        timePercent: 70,
        timeTillNextCycle: 10,
        title: 'oneGuard'
      }
    ]
  }
}

export const guardStatusesActive = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    statusEffects: [
      {
        ...initialState.additionalInfo.statusEffects[0],
        disabled: false
      },
      {
        ...initialState.additionalInfo.statusEffects[1],
        disabled: false
      }
    ]
  }
}

export const guardStatusesDisabled = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    statusEffects: [
      {
        ...initialState.additionalInfo.statusEffects[0],
        disabled: true
      },
      {
        ...initialState.additionalInfo.statusEffects[1],
        disabled: true
      }
    ]
  }
}

export const secondStatusGuard = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    statusEffects: [
      ...initialState.additionalInfo.statusEffects,
      {
        ...initialState.additionalInfo.statusEffects[1],
        disabled: false,
        reduceRFC: 0,
        timePercent: 70,
        timeTillNextCycle: 10,
        title: 'oneGuard'
      }
    ]
  }
}

export const secondStatusCheckpoint = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    statusEffects: [
      ...initialState.additionalInfo.statusEffects,
      {
        ...initialState.additionalInfo.statusEffects[1],
        disabled: false,
        reduceRFC: 0,
        timePercent: 70,
        timeTillNextCycle: 10,
        title: 'Checkpoint'
      }
    ]
  }
}

export default initialState
