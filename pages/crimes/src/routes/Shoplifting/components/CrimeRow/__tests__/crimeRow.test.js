import React from 'react'
import { shallow } from 'enzyme'
import initialState, { zeroPersent, fiftyPercent, fullPersent } from './mocks/crimeRow'
import CrimeRow from '../CrimeRow'

describe('<CrimeRow />', () => {
  it('should render crimeRow', () => {
    const Component = shallow(<CrimeRow {...initialState} />)

    expect(Component.find('.bar-line').length).toBe(1)
    expect(Component.find('ProgressCircle').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render crimeRow with 0% progress bar', () => {
    const Component = shallow(<CrimeRow {...zeroPersent} />)
    const { additionalInfo } = Component.instance().props

    expect(Component.find('.bar-line').length).toBe(1)
    expect(additionalInfo.notoriety).toBe(0)
    expect(Component.find('ProgressCircle').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render crimeRow with 50% progress bar', () => {
    const Component = shallow(<CrimeRow {...fiftyPercent} />)
    const { additionalInfo } = Component.instance().props

    expect(Component.find('.bar-line').length).toBe(1)
    expect(additionalInfo.notoriety).toBe(50)
    expect(Component.find('ProgressCircle').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render crimeRow with 100% progress bar', () => {
    const Component = shallow(<CrimeRow {...fullPersent} />)
    const { additionalInfo } = Component.instance().props

    expect(Component.find('.bar-line').length).toBe(1)
    expect(additionalInfo.notoriety).toBe(100)
    expect(Component.find('ProgressCircle').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
})
