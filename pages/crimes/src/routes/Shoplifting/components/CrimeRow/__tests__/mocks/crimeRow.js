const initialState = {
  jailed: false,
  buttonLabel: 'Shoplift',
  currentTemplate: 'shoplifting',
  skillLevel: 0,
  skillLevelTotal: 0,
  title: 'Search the Trash',
  nerve: 0,
  iconClass: 'shoplifting',
  crimeID: 0,
  subID: 0,
  requirements: {
    items: {},
    minCrimeLevel: {}
  },
  available: true,
  additionalInfo: {
    notoriety: 0,
    statusEffects: [
      {
        disabled: false,
        reduceRFC: 0,
        timePercent: 0,
        timeTillNextCycle: 0,
        title: 'oneCamera'
      }
    ]
  },
  panelItems: [
    {
      removeLink: true,
      label: '',
      value: ''
    },
    {
      removeLink: false,
      label: '',
      value: ''
    }
  ],
  result: 'info'
}

export const zeroPersent = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    notoriety: 0
  }
}

export const fiftyPercent = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    notoriety: 50
  }
}

export const fullPersent = {
  ...initialState,
  additionalInfo: {
    ...initialState.additionalInfo,
    notoriety: 100
  }
}

export default initialState
