import initialState, {
  oneGuardStatus,
  twoGuardStatuses,
  guardStatusesActive,
  guardStatusesDisabled,
  secondStatusGuard,
  secondStatusCheckpoint
} from './mocks/selectors'
import ownCrimeRowProps from '../selectors'

describe('selectors', () => {
  it('should have only first guardStatus', () => {
    const {
      checkCameraProgress: { isCameraDisabled, progressOffCamera, statusCamera },
      checkGuardProgress: { isGuardsDisabled, progressOffGuard, statusCheckpointOrGuard }
    } = ownCrimeRowProps(oneGuardStatus)

    expect(isCameraDisabled).toBeFalsy()
    expect(progressOffCamera).toBe(50)
    expect(statusCamera).toBe('Status: Camera recording (50%)')
    expect(statusCamera).toMatchSnapshot()

    expect(isGuardsDisabled).toBeFalsy()
    expect(progressOffGuard).toBe(0)
    expect(statusCheckpointOrGuard).toBe('Status: Guard patrolling (0%)')
    expect(statusCheckpointOrGuard).toMatchSnapshot()
  })
  it('should have two guardStatuses', () => {
    const {
      checkCameraProgress: { isCameraDisabled, progressOffCamera, statusCamera },
      checkGuardProgress: { isGuardsDisabled, progressOffGuard, statusCheckpointOrGuard }
    } = ownCrimeRowProps(twoGuardStatuses)

    expect(isCameraDisabled).toBeFalsy()
    expect(progressOffCamera).toBe(50)
    expect(statusCamera).toBe('Status: Camera recording (50%)')
    expect(statusCamera).toMatchSnapshot()

    expect(isGuardsDisabled).toBeFalsy()
    expect(progressOffGuard).toBe(70)
    expect(statusCheckpointOrGuard).toBe('Status: Guard patrolling (70%)')
    expect(statusCheckpointOrGuard).toMatchSnapshot()
  })
  it('should have activated guardStatuses', () => {
    const {
      checkCameraProgress: { isCameraDisabled },
      checkGuardProgress: { isGuardsDisabled }
    } = ownCrimeRowProps(guardStatusesActive)

    expect(isCameraDisabled).toBeFalsy()
    expect(isGuardsDisabled).toBeFalsy()
    expect(isCameraDisabled).toMatchSnapshot()
    expect(isGuardsDisabled).toMatchSnapshot()
    // expect(Component).toMatchSnapshot()
  })
  it('should have deactivated guardStatuses', () => {
    const {
      checkCameraProgress: { isCameraDisabled },
      checkGuardProgress: { isGuardsDisabled }
    } = ownCrimeRowProps(guardStatusesDisabled)

    expect(isCameraDisabled).toBeTruthy()
    expect(isGuardsDisabled).toBeTruthy()
    expect(isCameraDisabled).toMatchSnapshot()
    expect(isGuardsDisabled).toMatchSnapshot()
  })
  it('should return guard in second status', () => {
    const {
      checkGuardProgress: { statusCheckpointOrGuard }
    } = ownCrimeRowProps(secondStatusGuard)

    expect(statusCheckpointOrGuard).toBe('Status: Guard patrolling (70%)')
    expect(statusCheckpointOrGuard).toMatchSnapshot()
  })
  it('should return checkpoint in second status', () => {
    const {
      checkGuardProgress: { statusCheckpointOrGuard }
    } = ownCrimeRowProps(secondStatusCheckpoint)

    expect(statusCheckpointOrGuard).toBe('Status: Checkpoints enabled (70%)')
    expect(statusCheckpointOrGuard).toMatchSnapshot()
  })
  it('should run and return all selectors', () => {
    const { checkCameraProgress, checkGuardProgress, throwGlobalCrimeRowProps } = ownCrimeRowProps(initialState)

    expect(typeof checkCameraProgress === 'object').toBeTruthy()
    expect(typeof checkGuardProgress === 'object').toBeTruthy()
    expect(typeof throwGlobalCrimeRowProps === 'object').toBeTruthy()
  })
})
