import { connect } from 'react-redux'
import Banner from './Banner'
import bannerToRender, { cameraStatus } from './selectors'
import { showDropDownPanel, toggleArrows } from '../../../../modules/actions'

const mapStateToProps = state => ({
  currentBannerToRender: bannerToRender(state),
  isCameraActive: cameraStatus(state)
})

const mapDispatchToProps = dispatch => ({
  dropDownPanelToShow: () => dispatch(showDropDownPanel()),
  toggleArrow: () => dispatch(toggleArrows())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Banner)
