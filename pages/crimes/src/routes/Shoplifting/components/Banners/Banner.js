import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import BannerGlobal from '../../../../components/Banner'
import { ANIMATION_DURATION, ANIM_EFFECT } from '../../constants'
import styles from './styles.cssmodule.scss'

export class Banner extends Component {
  static propTypes = {
    dropDownPanelToShow: PropTypes.func,
    toggleArrow: PropTypes.func,
    isCameraActive: PropTypes.bool,
    currentBannerToRender: PropTypes.shape({
      bannerID: PropTypes.number,
      bannerClassWrap: PropTypes.string,
      currentBannerClass: PropTypes.string,
      // eslint-disable-next-line react/no-typos
      BannerAnimationHolder: PropTypes.ReactNode
    })
  }

  static defaultProps = {
    dropDownPanelToShow: () => {},
    toggleArrow: () => {},
    currentBannerToRender: {},
    isCameraActive: false
  }

  constructor(props) {
    super(props)

    this.state = {
      date: '00/00/00 00:00:00'
    }
  }

  componentDidMount() {
    this._setBannerDate()
  }

  shouldComponentUpdate(nextProps, nextState) {
    const {
      currentBannerToRender: { bannerID }
    } = this.props
    const { date } = this.state

    // allow rerenders if banners clocks is ticking
    if (date !== nextState.date) {
      return true
    }

    // prevent rerenders if bannerID explicitly does not changed
    if (bannerID === nextProps.currentBannerToRender.bannerID) {
      return false
    }

    return true
  }

  componentWillUnmount() {
    this._clearBannerDate()
  }

  _setBannerDate = () => {
    this.date = setInterval(() => {
      const date = new Date()
      const nowUtcDate = Date.UTC(
        date.getUTCFullYear(),
        date.getUTCMonth(),
        date.getUTCDate(),
        date.getUTCHours(),
        date.getUTCMinutes(),
        date.getUTCSeconds()
      )
      const realUTCDate = new Date(nowUtcDate)
      const dateOptions = { day: '2-digit', month: '2-digit', year: '2-digit' }
      const localDate = realUTCDate.toLocaleDateString('en-US', dateOptions)

      const hours = realUTCDate.getUTCHours()
      const minutes = realUTCDate.getUTCMinutes()
      const seconds = realUTCDate.getUTCSeconds()

      const realHours = hours < 10 ? `0${hours}` : hours
      const realMinutes = minutes < 10 ? `0${minutes}` : minutes
      const realSeconds = seconds < 10 ? `0${seconds}` : seconds

      this.setState({
        date: `${localDate} ${realHours}:${realMinutes}:${realSeconds}`
      })
    }, 1000)
  }

  _clearBannerDate = () => {
    clearInterval(this.date)
  }

  _bannersHelper = () => {
    const { date, isCameraActive } = this.state

    return (
      <Fragment>
        {!isCameraActive && <div className={styles.cameraLines} />}
        <div className={styles.time} id='time'>
          {date}
        </div>
      </Fragment>
    )
  }

  render() {
    const {
      currentBannerToRender: { bannerClassWrap, BannerAnimationHolder, bannerID },
      isCameraActive,
      dropDownPanelToShow,
      toggleArrow
    } = this.props

    return (
      <BannerGlobal
        customClass={styles.bannerWrap}
        dropDownPanelToShow={dropDownPanelToShow}
        toggleArrows={toggleArrow}
      >
        <TransitionGroup component='span'>
          <CSSTransition key={bannerID} classNames={ANIM_EFFECT} timeout={ANIMATION_DURATION}>
            <div className={bannerClassWrap}>
              {!isCameraActive && <BannerAnimationHolder />}
              {this._bannersHelper()}
            </div>
          </CSSTransition>
        </TransitionGroup>
      </BannerGlobal>
    )
  }
}

export default Banner
