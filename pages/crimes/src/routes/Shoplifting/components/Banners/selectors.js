import cn from 'classnames'
import { createSelector } from 'reselect'
import { CRIMES_COUNT, BANNERS_CLASSNAMES } from '../../constants'
import BannersContainer from './BannersHolder'
import styles from './styles.cssmodule.scss'

const FIRST_BANNER_ID = 1

const getLastSubCrimeID = state => state.shoplifting.bannerID
const getCrimeList = state => state.shoplifting.crimesByType

// reselect function
const bannerToRender = createSelector(
  [getLastSubCrimeID],
  crimeID => {
    const fixBannerID = crimeID - CRIMES_COUNT
    const bannerID = fixBannerID < 0 ? FIRST_BANNER_ID : fixBannerID
    const bannerClassName = !BannersContainer[bannerID] ? BANNERS_CLASSNAMES[1] : BANNERS_CLASSNAMES[bannerID]
    const bannerClassWrap = cn([styles.crimesShoplifting], styles[bannerClassName])
    const BannerAnimationHolder = !BannersContainer[bannerID] ? BannersContainer[1] : BannersContainer[bannerID]

    return {
      bannerClassWrap,
      BannerAnimationHolder,
      bannerID
    }
  }
)

export const cameraStatus = createSelector(
  [getLastSubCrimeID, getCrimeList],
  (crimeID, crimesByType) => {
    const bannerID = crimeID - CRIMES_COUNT
    const currentCrimeRow = crimesByType[bannerID - 1] || []
    const { additionalInfo: { statusEffects } = [] } = currentCrimeRow
    const isCameraActive = (statusEffects && statusEffects[0].disabled) || false

    return isCameraActive
  }
)

export default bannerToRender
