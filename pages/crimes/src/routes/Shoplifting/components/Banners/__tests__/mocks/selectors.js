export default {
  shoplifting: {
    bannerID: 19
  }
}

export const lastBannerID = {
  shoplifting: {
    bannerID: 24
  }
}

export const illegalBannerID = {
  shoplifting: {
    bannerID: 31
  }
}

export const cameraActive = {
  shoplifting: {
    bannerID: 19,
    crimesByType: [
      {
        currentTemplate: 'shoplifting',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'shoplifting',
        crimeID: 0,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: true,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'shoplifting',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'shoplifting',
        crimeID: 0,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'shoplifting',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'shoplifting',
        crimeID: 0,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'shoplifting',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'shoplifting',
        crimeID: 0,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'shoplifting',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'shoplifting',
        crimeID: 0,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'shoplifting',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        nerve: 0,
        iconClass: 'shoplifting',
        crimeID: 0,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      }
    ]
  }
}

export const cameraDisabled = {
  shoplifting: {
    ...cameraActive.shoplifting,
    crimesByType: cameraActive.shoplifting.crimesByType.map((crime, index) => (
      index === 0 ? {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          statusEffects: [
            {
              ...crime.additionalInfo.statusEffects,
              disabled: false
            }
          ]
        }
      } : crime
    ))
  }
}
