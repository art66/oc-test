import React from 'react'
import { mount } from 'enzyme'
import initialState, { thirthdBanner, withoutAnimBanner } from './mocks/banner'
import Banner from '../Banner'

describe('<Banner />', () => {
  it('should render 1st banner', () => {
    const Component = mount(<Banner {...initialState} />)

    expect(Component.find('.sweet-shop').length).toBe(1)
    expect(Component.find('.cameraLines').length).toBe(1)
    expect(Component.find('.time').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render 1st banner and after state change - 3thd', () => {
    let Component = mount(<Banner {...initialState} />)

    expect(Component.find('.sweet-shop').length).toBe(1)
    expect(Component.find('.cameraLines').length).toBe(1)
    expect(Component.find('.time').length).toBe(1)
    expect(Component).toMatchSnapshot()

    Component = mount(<Banner {...thirthdBanner} />)

    expect(Component.find('.clothing').length).toBe(1)
    expect(Component.find('.cameraLines').length).toBe(1)
    expect(Component.find('.time').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should does not place animation over the banner', () => {
    const Component = mount(<Banner {...withoutAnimBanner} />)

    expect(Component.find('.sweet-shop').length).toBe(1)
    expect(Component.find('.clothingGlitch1').length).toBe(0)
    expect(Component.find('.clothingGlitch2').length).toBe(0)
    expect(Component.find('.time').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
})
