import state, { lastBannerID, illegalBannerID, cameraActive, cameraDisabled } from './mocks/selectors'
import bannerToRender, { cameraStatus } from '../selectors'

describe('<Banner />', () => {
  it('should return the first banner with ID = 1', () => {
    const { bannerClassWrap, BannerAnimationHolder, bannerID } = bannerToRender(state)

    expect(bannerClassWrap).toEqual('crimesShoplifting sweets')
    expect(BannerAnimationHolder).toEqual(expect.any(Function))
    expect(bannerID).toEqual(1)
  })
  it('should return the last banner with ID = 6', () => {
    const { bannerClassWrap, BannerAnimationHolder, bannerID } = bannerToRender(lastBannerID)

    expect(bannerClassWrap).toEqual('crimesShoplifting jewelry')
    expect(BannerAnimationHolder).toEqual(expect.any(Function))
    expect(bannerID).toEqual(6)
  })
  it('should return the first banner in case of undefined bannerID', () => {
    const { bannerClassWrap, BannerAnimationHolder, bannerID } = bannerToRender(illegalBannerID)

    expect(bannerClassWrap).toEqual('crimesShoplifting sweets')
    expect(BannerAnimationHolder).toEqual(expect.any(Function))
    expect(bannerID).toEqual(13)
  })
  it('should return true in case of deactivated cameraStatus', () => {
    const isCameraDisabled = cameraStatus(cameraActive)

    expect(isCameraDisabled).toBeTruthy()
  })
  it('should return false in case of activated cameraStatus', () => {
    const isCameraDisabled = cameraStatus(cameraDisabled)

    expect(isCameraDisabled).toBeFalsy()
  })
})
