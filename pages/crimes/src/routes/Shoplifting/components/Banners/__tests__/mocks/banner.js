export default {
  dropDownPanelToShow: () => {},
  arrowToHide: () => {},
  isCameraActive: false,
  currentBannerToRender: {
    BannerAnimationHolder: 'div',
    bannerClassWrap: 'sweet-shop',
    bannerID: 1
  }
}

export const thirthdBanner = {
  isCameraActive: false,
  currentBannerToRender: {
    BannerAnimationHolder: 'div',
    bannerClassWrap: 'clothing',
    bannerID: 1
  }
}

export const withoutAnimBanner = {
  isCameraActive: true,
  currentBannerToRender: {
    BannerAnimationHolder: 'div',
    bannerClassWrap: 'sweet-shop',
    bannerID: 1
  }
}

export const eventClicked = {
  clientX: 100,
  clientY: 200
}

export const mouseDownMock = {
  coordsStart: {
    X: 100,
    Y: 200
  },
  date: '00/00/00 00:00:00'
}

export const mouseUpMock = {
  coordsStart: {
    X: 100,
    Y: 200
  },
  date: '00/00/00 00:00:00'
}

export const mouseUpWrongMock = {
  coordsStart: {
    X: 200,
    Y: 200
  },
  date: '00/00/00 00:00:00'
}
