/* eslint-disable max-statements */
import { mount, render } from 'enzyme'
import BannersHolder from '../BannersHolder'

describe('<Banner />', () => {
  it('should callback and render all bannersHolders each by each', () => {
    const BannerSweets = mount(BannersHolder[1]())
    const BannerBobs = mount(BannersHolder[2]())
    const BannerClothing = render(BannersHolder[3]())
    const BannerStore = mount(BannersHolder[4]())
    const BannerGuns = mount(BannersHolder[5]())
    const BannerJewelry = mount(BannersHolder[6]())

    expect(BannerSweets.find('.sweetsCameraGlitch').length).toBe(1)
    expect(BannerSweets).toMatchSnapshot()

    expect(BannerBobs.find('.bitsNbobCameraGlitch').length).toBe(1)
    expect(BannerBobs).toMatchSnapshot()

    // Enzyme does not work with React Fragments, used typeof checking for BannerStore
    expect(typeof BannerClothing === 'object').toBe(true)
    expect(BannerClothing).toMatchSnapshot()

    expect(BannerStore.find('.superstoreCameraGlitch').length).toBe(1)
    expect(BannerStore).toMatchSnapshot()

    expect(BannerGuns.find('.gunShopCameraGlitch').length).toBe(1)
    expect(BannerGuns).toMatchSnapshot()

    expect(BannerJewelry.find('.jewelryCameraGlitch').length).toBe(1)
    expect(BannerJewelry).toMatchSnapshot()
  })
})
