import React, { Fragment } from 'react'
import styles from './styles.cssmodule.scss'

const sallysSweetShop = () => (
  <div className={styles.sweetsCameraGlitch}>
    <div className={styles.sweetsGlitchBg} />
  </div>
)

const bitsAndBobs = () => (
  <div className={styles.bitsNbobCameraGlitch}>
    <div className={styles.bitsNbobGlitchBg} />
  </div>
)

const tcClothing = () => (
  <Fragment>
    <div className={styles.clothingGlitch1} />
    <div className={styles.clothingGlitch2} />
  </Fragment>
)

const superStore = () => (
  <div className={styles.superstoreCameraGlitch}>
    <div className={styles.superstoreGlitchBg} />
  </div>
)

const bigAlsGunShop = () => (
  <div className={styles.gunShopCameraGlitch}>
    <div className={styles.gunShopGlitchBg} />
  </div>
)

const jewelryShop = () => (
  <div className={styles.jewelryCameraGlitch}>
    <div className={styles.jewelryGlitchBg} />
  </div>
)

const BannersContainer = {
  1: sallysSweetShop,
  2: bitsAndBobs,
  3: tcClothing,
  4: superStore,
  5: bigAlsGunShop,
  6: jewelryShop
}

export default BannersContainer
