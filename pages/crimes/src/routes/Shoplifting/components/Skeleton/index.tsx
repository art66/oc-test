import React from 'react'

import Placeholder from '../../../../components/Skeleton'

import globalSkeleton from '../../../../styles/skeleton.cssmodule.scss'
import styles from './styles.cssmodule.scss'

const Skeleton = ({ type }: { type: string }) => {
  const rowsList = () =>
    Array.from(Array(6).keys()).map(key => (
      <div className={globalSkeleton.rowWrap} key={key}>
        <div className={globalSkeleton.titleImg} />
        <div className={`${globalSkeleton.titleText} ${globalSkeleton.titleTextUp}`} />
        <div className={`${globalSkeleton.progress} ${styles.progress}`} />
        <div className={globalSkeleton.circlesPseudo} />
        <div className={`${globalSkeleton.nerveContainer} ${styles.nerve}`} />
        <div className={globalSkeleton.buttonAction} />
      </div>
    ))

  return (
    <Placeholder type={type}>
      <div className={globalSkeleton.rowsWrap}>{rowsList()}</div>
    </Placeholder>
  )
}

export default Skeleton
