import {
  TIMERS_DUARATION,
  TIMERS_HOLDER,
  PERCENT_IMPLIFIER,
  NOT_REDUCED_RFC,
  REDUCED_RFC,
  SECURITY_IS_ACTIVE,
  SECURITY_IS_DISABLED
} from '../constants'

const EXTRA_CRIME_COUNT = 1

const calcTimersOffPayload = (...props) => {
  const [title, disableInterval, timeLeftInInterval] = props
  const disabled = SECURITY_IS_ACTIVE
  const reducedRFC = REDUCED_RFC
  const timeLeftBeforeNextCicle = disableInterval - timeLeftInInterval
  const timeLeftInPercentage = (timeLeftBeforeNextCicle / disableInterval) * PERCENT_IMPLIFIER

  return {
    title,
    disabled,
    timeTillNextCycle: timeLeftBeforeNextCicle,
    timePercent: timeLeftInPercentage,
    reducedRFC
  }
}

const calcTimerOnPayload = (...props) => {
  const [title, interval, disableInterval, timeLeftInInterval] = props

  const disabled = SECURITY_IS_DISABLED
  const reducedRFC = NOT_REDUCED_RFC
  const timeLeftBeforeNextCicle = interval - timeLeftInInterval
  const timeLeftForDisableInterval = interval - disableInterval
  const timeLeftInPercentage = (timeLeftBeforeNextCicle / timeLeftForDisableInterval) * PERCENT_IMPLIFIER

  return {
    title,
    disabled,
    timeTillNextCycle: timeLeftBeforeNextCicle,
    timePercent: timeLeftInPercentage,
    reducedRFC
  }
}

// Timers updation logic for all crimeRows in CrimeList.
// Activates timer action and updates crimeRow status values in the store
const defineStatTimerInterval = (crimesArray = [], statusTimersAction) => {
  const startTimers = setInterval(() => {
    const time = Date.now() / 1000
    const globalCrimestimersData = []

    crimesArray.map(crimekey => {
      const rowID = crimekey + EXTRA_CRIME_COUNT
      const collectedPayload = []
      const statTimersRowPayload = {
        rowID,
        rowPayload: collectedPayload
      }

      TIMERS_HOLDER[rowID].map(timer => {
        if (timer === null) return {}

        const { title, interval, shift = 0, disableInterval } = timer
        const timeLeftInInterval = (time + shift) % interval
        let payload = null

        if (timer.disableInterval >= timeLeftInInterval) {
          // Security is Disabled
          payload = calcTimersOffPayload(title, disableInterval, timeLeftInInterval)
        } else {
          // Security is Active
          payload = calcTimerOnPayload(title, interval, disableInterval, timeLeftInInterval)
        }
        collectedPayload.push(payload)
        return collectedPayload
      })

      globalCrimestimersData.push(statTimersRowPayload)

      return globalCrimestimersData
    })

    // callback our redux action with collected info about all status timers
    // and puts them in the redux store
    statusTimersAction(globalCrimestimersData)
  }, TIMERS_DUARATION)

  return startTimers
}

export default defineStatTimerInterval
