import { put, takeLatest } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  dataFetched,
  attemptStart,
  attemptFetched,
  crimeReady,
  showDebugInfo,
  updateExp,
  getCurrentPageData,
  userJailed
} from '../../../modules/actions'
import { TYPE_IDS, ROOT_URL } from '../../../constants'
import { ATTEMPT, FETCH_DATA } from '../constants'

export function* fetchData({ url }) {
  try {
    const json = yield fetchUrl(ROOT_URL + url)

    if (json.DB && json.DB.error) {
      throw new Error(json.DB.error)
    }

    yield put(dataFetched('shoplifting', json))
    yield put(getCurrentPageData(json.DB.currentType.crimeRoute, TYPE_IDS[json.DB.currentType.crimeRoute]))
    yield put(crimeReady('/shoplifting'))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export function* attempt({ url, itemId, isCurrentRow }) {
  try {
    yield put(attemptStart('shoplifting', itemId, isCurrentRow))
    const json = yield fetchUrl(ROOT_URL + url)

    const {
      DB: { error, outcome }
    } = json

    if (error) {
      throw new Error(error)
    }

    if (!['success', 'failed'].includes(outcome.result)) {
      yield put(userJailed(true))
    }

    yield put(attemptFetched('shoplifting', json))
    yield put(updateExp())
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default function* watchSearchForCash() {
  yield takeLatest(FETCH_DATA, fetchData)
  yield takeLatest(ATTEMPT, attempt)
}
