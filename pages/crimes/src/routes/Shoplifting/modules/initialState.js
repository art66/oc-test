export default {
  attemptProgress: {
    inProgress: false
  },
  buttonLabel: 'Shoplift',
  typeID: 4,
  bannerID: 1,
  isCurrentRow: false,
  showPlaceholder: true,
  outcome: {
    outcomeDesc: null,
    result: '',
    rewardsGive: {
      itemsRewards: null,
      userRewards: {
        money: 0
      }
    },
    story: []
  },
  crimesByType: [
    {
      currentTemplate: 'shoplifting',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'shoplifting',
      crimeID: 0,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        notoriety: 100,
        statusEffects: [
          {
            disabled: false,
            reduceRFC: 0,
            timePercent: 0,
            shift: 0,
            timeTillNextCycle: 0,
            title: 'oneCamera'
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    }
  ],
  additionalInfo: null,
  currentType: {
    _id: {
      $oid: ''
    },
    ID: 0,
    typeID: 0,
    title: 'shoplifting',
    crimeRoute: 'shoplifting',
    active: 0,
    img: '',
    expForLevel100: 0
  },
  skillLevelTotal: 0,
  currentLevel: null,
  lastSubCrimeID: 0,
  currentUserStats: {
    failedTotal: 0,
    attemptsTotal: 0,
    crimesByIDAttempts: {
      1: 0,
      2: 0,
      3: 0
    },
    successesTotal: 0,
    moneyFoundTotal: 0,
    jailedTotal: 0,
    itemsFound: {
      1: 0
    },
    critFailedTotal: 0,
    statsByType: [
      {
        successesTotal: 0
      }
    ],
    skill: 0,
    skillLevel: 0
  },
  currentUserStatistics: []
}
