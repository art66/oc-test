import initialState from './initialState'
import {
  ATTEMPT,
  DATA_FETCHED,
  ATTEMPT_START,
  ATTEMPT_FETCHED,
  LEVEL_UP,
  SET_CRIME_LEVEL,
  STATS_TIMERS_UPDATER,
  CRIMES_COUNT,
  CLEAR_OUTDATED_OUTCOME,
  RESET_STATS_DONE,
  CLOSE_OUTCOME
} from '../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [DATA_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    showPlaceholder: false
  }),
  [ATTEMPT]: state => ({
    ...state,
    attemptProgress: {
      ...state.attemptProgress,
      inProgress: true
    }
  }),
  [ATTEMPT_START]: (state, action) => ({
    ...state,
    bannerID: action.itemId,
    attemptProgress: {
      itemId: action.itemId,
      inProgress: true
    },
    currentRowID: action.itemId,
    isCurrentRow: action.isCurrentRow
  }),
  [ATTEMPT_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    attemptProgress: {
      itemId: state.attemptProgress.itemId,
      inProgress: false
    },
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === state.attemptProgress.itemId ?
      {
        ...crime,
        additionalInfo: action.json.DB.additionalInfo
      } :
      crime))
  }),
  [LEVEL_UP]: (state, action) => ({
    ...state,
    level: action.payload.level
  }),
  [SET_CRIME_LEVEL]: (state, action) => ({
    ...state,
    ...action.result.DB
  }),
  [STATS_TIMERS_UPDATER]: (state, action) => ({
    ...state,
    crimesByType: state.crimesByType.map(crime => {
      const localCrimeID = crime.crimeID - CRIMES_COUNT
      const [rowPayloadID] = action.rowsPayload.filter(payload => payload.rowID === localCrimeID)

      // temporary check for dev purposes only
      if (!rowPayloadID) {
        throw new Error('Oops, there is now row payload found. Check reducer:', rowPayloadID)
      }

      return {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          statusEffects: rowPayloadID.rowPayload
        }
      }
    })
  }),
  [CLEAR_OUTDATED_OUTCOME]: state => ({
    ...state,
    outcome: {},
    isCurrentRow: false,
    currentRowID: -1,
    attemptProgress: {
      ...state.attemptProgress,
      itemId: -1
    }
  }),
  [RESET_STATS_DONE]: (state, action) => ({
    ...state,
    ...action.json.DB
  }),
  [CLOSE_OUTCOME]: state => ({
    ...state,
    attemptProgress: {
      itemId: 0
    }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
export default function (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
