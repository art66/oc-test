import { STATS_TIMERS_UPDATER } from '../constants'

// ------------------------------------
// Actions
// ------------------------------------
export const statusTimersUpdater = statTimersRowsPayload => ({
  type: STATS_TIMERS_UPDATER,
  rowsPayload: statTimersRowsPayload
})
