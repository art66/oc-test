import { searchforcashRowsUpdate } from '../../SearchForCash/utils/shouldRowsUpdate'

const shopliftingRowsUpdate = (prevProps, nextProps) => {
  // extends searchforcash update checking
  return searchforcashRowsUpdate(prevProps, nextProps)
}

export { shopliftingRowsUpdate }
