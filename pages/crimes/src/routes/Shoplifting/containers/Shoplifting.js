import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { clearOutdatedOutcome } from '../../../modules/actions'

import { CrimesList, Skeleton, Banners } from '../components'
import SubCrimesLayout from '../../../components/SubCrimesLayout'

class Shoplifting extends PureComponent {
  static propTypes = {
    state: PropTypes.object,
    showPlaceholder: PropTypes.bool,
    current: PropTypes.number,
    progress: PropTypes.number,
    stats: PropTypes.array,
    startClearOutdatedOutcome: PropTypes.func
  }

  static defaultProps = {
    state: {},
    startClearOutdatedOutcome: () => {},
    showPlaceholder: true,
    current: 0,
    progress: 0,
    stats: []
  }

  componentWillUnmount() {
    const {
      startClearOutdatedOutcome,
      state: { shoplifting }
    } = this.props
    const { outcome: { story = [] } = {} } = shoplifting || {}

    if (!story || (shoplifting && story.length !== 0)) {
      startClearOutdatedOutcome()
    }
  }

  render() {
    const { stats, current, progress, showPlaceholder } = this.props

    return (
      <SubCrimesLayout
        customWrapClass='shoplifting'
        showPlaceholder={showPlaceholder}
        title='shoplifting'
        skeleton={Skeleton}
        banners={<Banners />}
        stats={stats}
        current={current}
        progress={progress}
      >
        <CrimesList />
      </SubCrimesLayout>
    )
  }
}

const mapStateToProps = state => ({
  stat: state.shoplifting,
  current: state.shoplifting.currentLevel,
  progress: state.shoplifting.skillLevelTotal,
  stats: state.shoplifting.currentUserStatistics,
  showPlaceholder: state.shoplifting.showPlaceholder
})

const mapDispatchToProps = dispatch => ({
  startClearOutdatedOutcome: () => dispatch(clearOutdatedOutcome('shoplifting'))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shoplifting)
