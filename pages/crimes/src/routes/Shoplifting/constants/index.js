// ------------------------------------
// CRIME TYPE
// ------------------------------------
export const typeID = 4

// ------------------------------------
// Action Constants
// ------------------------------------
export const ATTEMPT = 'shoplifting/ATTEMPT'
export const FETCH_DATA = 'shoplifting/FETCH_DATA'
export const DATA_FETCHED = 'shoplifting/DATA_FETCHED'
export const ATTEMPT_START = 'shoplifting/ATTEMPT_START'
export const ATTEMPT_FETCHED = 'shoplifting/ATTEMPT_FETCHED'
export const CRIME_IS_READY = 'shoplifting/CRIME_IS_READY'
export const LEVEL_UP = 'shoplifting/LEVEL_UP'
export const SET_CRIME_LEVEL = 'shoplifting/SET_CRIME_LEVEL'
export const STATS_TIMERS_UPDATER = 'shoplifting/STATS_TIMERS_UPDATER'
export const CLEAR_OUTDATED_OUTCOME = 'shoplifting/CLEAR_OUTDATED_OUTCOME'
export const RESET_STATS_DONE = 'shoplifting/RESET_STATS_DONE'
export const CLOSE_OUTCOME = 'shoplifting/CLOSE_OUTCOME'

// -------------------------------------
// BANNERS
// -------------------------------------
export const ANIMATION_DURATION = 1000
export const CRIMES_COUNT = 18
export const ANIM_EFFECT = 'fade'
export const BANNERS_CLASSNAMES = {
  1: 'sweets',
  2: 'bitsNbob',
  3: 'clothing',
  4: 'superstore',
  5: 'gunShop',
  6: 'jewelry'
}

// -------------------------------------
// STATUS CLASSNAMES
// -------------------------------------
export const CAMERA_CLASSES_HOLDER = {
  1: 'cameraOne',
  2: 'cameraOne',
  3: 'cameraOne',
  4: 'cameraTwo',
  5: 'cameraThree',
  6: 'cameraFour'
}

export const GUARD_CLASSES_HOLDER = {
  1: 'noneCheck',
  2: 'noneCheck',
  3: 'checkpoint',
  4: 'checkpoint',
  5: 'guardOne',
  6: 'guardTwo'
}

// -------------------------------------
// STATUS DUARATIONS TIME PROPS
// -------------------------------------
export const TIMERS_DUARATION = 20000
export const PERCENT_IMPLIFIER = 100
export const SECURITY_IS_ACTIVE = true
export const SECURITY_IS_DISABLED = false
export const REDUCED_RFC = 80
export const NOT_REDUCED_RFC = 0

export const ONE_CAMERA_TIMER_ROW1 = {
  title: 'oneCamera',
  interval: 75600,
  shift: 2000,
  disableInterval: 21600
}

export const ONE_CAMERA_TIMER_ROW2 = {
  title: 'oneCamera',
  interval: 75600,
  shift: 1000,
  disableInterval: 21600
}

export const ONE_CAMERA_TIMER_ROW3 = {
  title: 'oneCamera',
  interval: 75600,
  shift: 3000,
  disableInterval: 21600
}

export const TWO_CAMERA_TIMER = {
  title: 'twoCamera',
  interval: 97200,
  disableInterval: 14400
}

export const THREE_CAMERA_TIMER = {
  title: 'threeCamera',
  interval: 118800,
  disableInterval: 7200
}

export const FOUR_CAMERA_TIMER = {
  title: 'fourCamera',
  interval: 140400,
  disableInterval: 3600
}

export const CHECKPOINTS_ROW3 = {
  title: 'Checkpoint',
  interval: 788400,
  shift: 1000,
  disableInterval: 86400
}

export const CHECKPOINTS_ROW4 = {
  title: 'Checkpoint',
  interval: 788400,
  shift: 16000,
  disableInterval: 86400
}

export const ONE_GUARD = {
  title: 'oneGuard',
  interval: 28800,
  disableInterval: 900
}

export const TWO_GUARD = {
  title: 'twoGuard',
  interval: 28800,
  disableInterval: 180
}

export const TIMERS_HOLDER = {
  1: [ONE_CAMERA_TIMER_ROW1, null],
  2: [ONE_CAMERA_TIMER_ROW2, null],
  3: [ONE_CAMERA_TIMER_ROW3, CHECKPOINTS_ROW3],
  4: [TWO_CAMERA_TIMER, CHECKPOINTS_ROW4],
  5: [THREE_CAMERA_TIMER, ONE_GUARD],
  6: [FOUR_CAMERA_TIMER, TWO_GUARD]
}
