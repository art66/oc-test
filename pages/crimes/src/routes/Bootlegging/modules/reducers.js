import initialState from './initialState'
import {
  DATA_FETCHED,
  ATTEMPT_START,
  ATTEMPT_FETCHED,
  BREAK_BLANK_SELECTED_CDS,
  POOLING_FETCHED,
  LEVEL_UP,
  REMOVE_CD_FROM_QUEUE,
  SET_TIME_FOR_NEXT_CD,
  TOGGLE_CDS_SELECTOR,
  SELECT_CDS,
  SET_CRIME_LEVEL,
  BLANK_SELECTED_CDS,
  CLEAR_OUTDATED_OUTCOME,
  CD_QUEUE_EMPTY,
  RESET_STATS_DONE,
  CLOSE_OUTCOME
} from '../constants'

const COPY_CD_ROW_ID = 8
const SETUP_STORE_ID = 10
const COLLECT_FUNDS_ID = 11

// const EXTRA_PACK = 100

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [DATA_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    showPlaceholder: false
  }),
  [ATTEMPT_START]: (state, action) => ({
    ...state,
    attemptProgress: {
      itemId: action.itemId,
      inProgress: true
    },
    currentRowID: action.itemId,
    isCurrentRow: action.isCurrentRow
  }),
  [ATTEMPT_FETCHED]: (state, action) => {
    const fetchedStore = {
      ...state,
      ...action.json.DB,
      additionalInfo: {
        ...state.additionalInfo,
        ...action.json.DB.additionalInfo
      },
      blankSelectedCDs: false,
      attemptProgress: {
        ...state.attemptProgress,
        inProgress: false
      },
      crimesByType: state.crimesByType.map(crime => (crime.crimeID === COPY_CD_ROW_ID ?
        {
          ...crime,
          additionalInfo: {
            ...crime.additionalInfo,
            ...action.json.DB.additionalInfo,
            tempCDsUsed: null
          }
        } :
        crime))
    }

    // replace SetupStore on CollectFunds crimes once first is completed
    if (action.json.DB.crimesByType) {
      const currentCrimes = state.crimesByType.filter(crime => crime.crimeID !== SETUP_STORE_ID) || []
      const collectFundsCrime = action.json.DB.crimesByType.filter(funds => funds.crimeID === COLLECT_FUNDS_ID) || []

      return {
        ...fetchedStore,
        crimesByType: [...currentCrimes, ...collectFundsCrime]
      }
    }

    return fetchedStore
  },
  [POOLING_FETCHED]: (state, action) => ({
    ...state,
    additionalInfo: {
      ...state.additionalInfo,
      CDs: action.json.DB.additionalInfo.CDs,
      moneyToCollect: action.json.DB.additionalInfo.moneyToCollect
    }
  }),
  [LEVEL_UP]: (state, action) => ({
    ...state,
    level: action.payload.level
  }),
  [REMOVE_CD_FROM_QUEUE]: (state, action) => ({
    // move first CD from queue to copied cds list
    ...state,
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === COPY_CD_ROW_ID ?
      {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          currentQueue: crime.additionalInfo.currentQueue.slice(1),
          timeLeftForNextCD: 0
        }
      } :
      crime)),
    additionalInfo: {
      ...state.additionalInfo,
      CDs: action.queue
    }
  }),
  [SET_TIME_FOR_NEXT_CD]: (state, action) => ({
    ...state,
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === COPY_CD_ROW_ID ?
      {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          timeLeftForNextCD: action.time
        }
      } :
      crime))
  }),
  [TOGGLE_CDS_SELECTOR]: (state, action) => ({
    ...state,
    showCDsSelector: action.value
  }),
  [SELECT_CDS]: (state, action) => ({
    ...state,
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === COPY_CD_ROW_ID ?
      {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          CDsLeft: true,
          blankCDs: crime.additionalInfo.blankCDs + action.countCDs,
          tempCDsUsed: {
            ...crime.additionalInfo.tempCDsUsed,
            [action.value]:
                  ((crime.additionalInfo.tempCDsUsed && crime.additionalInfo.tempCDsUsed[action.value]) || 0) + 1
          }
        },
        requirements: {
          ...crime.requirements,
          items: crime.requirements.items.map(reqItem => (reqItem.value === action.value ?
            {
              ...reqItem,
              count: reqItem.count - 1
              // tempCountUsed: reqItem.tempCountUsed ? reqItem.tempCountUsed + 1 : 1
            } :
            reqItem))
        }
      } :
      crime))
  }),
  [SET_CRIME_LEVEL]: (state, action) => ({
    ...state,
    ...action.result.DB
  }),
  [BLANK_SELECTED_CDS]: (state, action) => ({
    ...state,
    blankSelectedCDs: action.value
  }),
  [BREAK_BLANK_SELECTED_CDS]: state => ({
    ...state,
    blankSelectedCDs: false
  }),
  [CLEAR_OUTDATED_OUTCOME]: state => ({
    ...state,
    outcome: {},
    isCurrentRow: false,
    currentRowID: -1,
    attemptProgress: {
      ...state.attemptProgress,
      itemId: -1
    }
  }),
  [CD_QUEUE_EMPTY]: state => ({
    ...state,
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === COPY_CD_ROW_ID ?
      {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          currentQueue: null,
          СDsBootStage: 'finished'
        }
      } :
      crime))
  }),
  [RESET_STATS_DONE]: (state, action) => ({
    ...state,
    ...action.json.DB
  }),
  [CLOSE_OUTCOME]: state => ({
    ...state,
    attemptProgress: {
      itemId: 0
    }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
export default function (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
