import { put, takeLatest } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  dataFetched,
  attemptStart,
  attemptFetched,
  poolingFetched,
  crimeReady,
  showDebugInfo,
  // updateExp,
  getCurrentPageData,
  userJailed
} from '../../../modules/actions'
import { TYPE_IDS, ROOT_URL } from '../../../constants'
import { ATTEMPT, FETCH_DATA, POOLING } from '../constants'

export function* fetchData({ url }) {
  try {
    const json = yield fetchUrl(ROOT_URL + url)

    yield put(dataFetched('bootlegging', json))
    yield put(getCurrentPageData(json.DB.currentType.crimeRoute, TYPE_IDS[json.DB.currentType.crimeRoute]))
    yield put(crimeReady('/bootlegging'))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export function* attempt({ itemId, url, isCurrentRow }) {
  try {
    yield put(attemptStart('bootlegging', itemId, isCurrentRow))
    const json = yield fetchUrl(ROOT_URL + url)

    const {
      DB: { error, outcome }
    } = json

    if (error) {
      throw new Error(error)
    }

    if (!['success', 'failed'].includes(outcome.result)) {
      yield put(userJailed(true))
    }

    yield put(attemptFetched('bootlegging', json))
    // yield put(updateExp()) // temporary removed for testing without attemp exp update
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export function* pooling() {
  try {
    const json = yield fetchUrl(`${ROOT_URL}&step=attempt&crimeID=11&typeID=2&type=poll`)

    if (json.DB?.error) {
      yield put(showDebugInfo({ msg: json.DB?.error }))

      return
      // throw new Error(json.DB.error)
    }

    yield put(poolingFetched('bootlegging', json))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default function* watchBootlegging() {
  yield takeLatest(FETCH_DATA, fetchData)
  yield takeLatest(ATTEMPT, attempt)
  yield takeLatest(POOLING, pooling)
}
