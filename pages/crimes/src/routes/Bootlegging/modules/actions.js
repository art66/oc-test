import {
  BREAK_BLANK_SELECTED_CDS,
  REMOVE_CD_FROM_QUEUE,
  SET_TIME_FOR_NEXT_CD,
  TOGGLE_CDS_SELECTOR,
  SELECT_CDS,
  BLANK_SELECTED_CDS,
  CLEAR_OUTDATED_OUTCOME,
  CD_QUEUE_EMPTY
} from '../constants'

// ------------------------------------
// Actions
// ------------------------------------
export function removeCdFromQueue(queue) {
  return {
    type: REMOVE_CD_FROM_QUEUE,
    queue
  }
}

export function setTimeForNextCd(time) {
  return {
    type: SET_TIME_FOR_NEXT_CD,
    time
  }
}

export const toggleCDsSelector = value => ({
  type: TOGGLE_CDS_SELECTOR,
  value
})

export const selectCDs = (packID, countCDs, value) => ({
  type: SELECT_CDS,
  packID,
  countCDs,
  value
})

export const blankSelectedCDs = value => ({
  type: BLANK_SELECTED_CDS,
  value
})

export const breakSelectedCDs = () => ({
  type: BREAK_BLANK_SELECTED_CDS
})

export const clearOutdatedOutcome = () => ({
  type: CLEAR_OUTDATED_OUTCOME
})

export const cdQueueEmpty = () => ({
  type: CD_QUEUE_EMPTY
})
