import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { toMoney } from '@torn/shared/utils'
import { bootleggingRow4CollectUpdate } from '../../utils/shouldRowsUpdate'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'
import { CrimeRow } from '../../../../components'
import ownCrimeRowProps from './selectors'
import styles from '../../../../styles/bootlegging.cssmodule.scss'
import './anim.scss'

export class Row4Collect extends Component {
  static propTypes = {
    closeOutcome: PropTypes.func,
    lastSubCrimeID: PropTypes.number,
    currentRowID: PropTypes.number,
    startPooling: PropTypes.func,
    state: PropTypes.string,
    showOutcome: PropTypes.bool,
    itemId: PropTypes.number,
    callback: PropTypes.func,
    attempt: PropTypes.func,
    outcome: PropTypes.object,
    isCurrentRow: PropTypes.bool,
    available: PropTypes.bool,
    crimeID: PropTypes.number,
    iconClass: PropTypes.string,
    additionalInfo: PropTypes.object,
    nerve: PropTypes.number,
    title: PropTypes.string.isRequired
  }

  static defaultProps = {
    closeOutcome: () => {},
    lastSubCrimeID: 0,
    currentRowID: 0,
    startPooling: () => {},
    state: '',
    showOutcome: false,
    itemId: 0,
    callback: () => {},
    attempt: () => {},
    outcome: {},
    isCurrentRow: false,
    available: false,
    crimeID: 0,
    iconClass: '',
    additionalInfo: {},
    nerve: 0
  }

  shouldComponentUpdate(nextProps) {
    const globalCheck = globalCrimeRowCheck(this.props, nextProps)
    const rowCheck = bootleggingRow4CollectUpdate(this.props, nextProps)

    return rowCheck || globalCheck || false
  }

  componentDidMount() {
    this.poollingTimerId = setInterval(this._startPoolingTimer, 20000)
  }

  componentWillUnmount() {
    this._clearPoolingTimer(this.poollingTimerId)
  }

  _startPoolingTimer = () => {
    const { startPooling, crimeID } = this.props

    // clear pooling once crime was reverted to setup online store (it seems to be only why debuggig)
    if (crimeID !== 11) {
      this._clearPoolingTimer(this.poollingTimerId)

      return
    }

    startPooling()
  }

  _clearPoolingTimer = timer => {
    clearInterval(timer)
  }

  render() {
    const {
      additionalInfo: { moneyToCollect = 0 }
    } = this.props

    const { containerWidth, throwGlobalCrimeRowProps: props } = ownCrimeRowProps(this.props)

    const crimeRowClasses = `${styles.inner_cell_1} ${styles.underTitle}
     ${styles.dlm} ${styles.center} ${styles.amountWrap}`

    return (
      <CrimeRow {...props} underTitle rowShouldUpdate={bootleggingRow4CollectUpdate}>
        <div className={crimeRowClasses}>
          <span key={moneyToCollect} className={styles['amount-dollars']}>
            $
          </span>
          <TransitionGroup className='amount-wrap'>
            <CSSTransition
              component='div'
              key={moneyToCollect}
              style={{ width: `${containerWidth}px` }}
              classNames='amount'
              timeout={{ enter: 400, exit: 1000 }}
            >
              <span className={styles['amount']}>{toMoney(moneyToCollect)}</span>
            </CSSTransition>
          </TransitionGroup>
        </div>
      </CrimeRow>
    )
  }
}

export default Row4Collect
