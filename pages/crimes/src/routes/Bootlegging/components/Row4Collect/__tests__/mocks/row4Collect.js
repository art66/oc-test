export default {
  closeOutcome: () => {},
  isRowActive: false,
  lastSubCrimeID: 0,
  currentRowID: 0,
  startPooling: () => {},
  state: '',
  showOutcome: true,
  itemId: 0,
  callback: () => {},
  attempt: () => {},
  outcome: {},
  isCurrentRow: false,
  available: false,
  crimeID: 0,
  iconClass: '',
  additionalInfo: {
    moneyToCollect: 1
  },
  nerve: 0,
  title: ''
}

export const attemptButtonActive = {
  isRowActive: true,
  available: true
}

export const attemptButtonDisable = {
  available: false
}

export const countStateIncrement = {
  count: 9
}

export const countIncrement = {
  additionalInfo: {
    moneyToCollect: 10
  }
}
