import initialState, { tenCounts, hundryCounts, thosendCounts } from './mocks/selectors'
import ownCrimeRowProps from '../selectors'

describe('Row4Collect', () => {
  it('should return width of 17 for 10 counts', () => {
    const { containerWidth } = ownCrimeRowProps(tenCounts)

    expect(containerWidth).toBe(17)
  })
  it('should return width of 25 for 100 counts', () => {
    const { containerWidth } = ownCrimeRowProps(hundryCounts)

    expect(containerWidth).toBe(25)
  })
  it('should return width of 33 for 1000 counts', () => {
    const { containerWidth } = ownCrimeRowProps(thosendCounts)

    expect(containerWidth).toBe(33)
  })
  it('should return whole props for global crimeRow', () => {
    const { throwGlobalCrimeRowProps } = ownCrimeRowProps(initialState)

    expect(typeof throwGlobalCrimeRowProps === 'object').toBeTruthy()
  })
})
