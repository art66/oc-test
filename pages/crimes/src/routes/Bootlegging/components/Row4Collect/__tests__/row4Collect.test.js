import React from 'react'
import { shallow, mount } from 'enzyme'
import initialState, { attemptButtonActive, countIncrement } from './mocks/row4Collect'
import Row4Collect from '../Row4Collect'

describe('<Row4Collect />', () => {
  it('should render the row', () => {
    const Component = mount(<Row4Collect {...initialState} />)

    expect(Component.find('CrimeRow').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('attempt button should be disabled', () => {
    const Component = shallow(<Row4Collect {...initialState} />)
    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('attempt button should be active', () => {
    const buttonIsActive = Object.assign(initialState, attemptButtonActive)
    const Component = shallow(<Row4Collect {...buttonIsActive} />)
    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('row should have \'active\' className if clicked his attempt button', () => {
    const Component = shallow(<Row4Collect {...initialState} />)
    const buttonIsDisabled = Component.instance().render().props.className

    expect(buttonIsDisabled).toEqual('active')
    expect(Component).toMatchSnapshot()
  })
  it('count with value 1-10 should have container width equal to 9', () => {
    const Component = mount(<Row4Collect {...initialState} />)

    const { moneyToCollect } = initialState.additionalInfo
    const countContainerWidth = (moneyToCollect.toString().length - moneyToCollect.toString().length / 5) * 10 + 1

    expect(countContainerWidth).toEqual(9)
    expect(Component).toMatchSnapshot()
  })
  it('Component should hange state \'count\'. After 20000 mc make pool request again and change state', () => {
    jest.useFakeTimers()

    const Component = mount(<Row4Collect {...initialState} />)
    const { moneyToCollect } = initialState.additionalInfo
    const { poollingTimerId } = Component.instance()

    expect(moneyToCollect).toBe(1)
    expect(poollingTimerId).toBeTruthy()
    expect(setInterval).toHaveBeenCalledTimes(1)
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 20000)

    Component.setProps(countIncrement)
    expect(Component.prop('additionalInfo')).toEqual({ moneyToCollect: 10 })
    expect(Component).toMatchSnapshot()
  })
})
