const initialState = {
  count: 100,
  additionalInfo: {
    moneyToCollect: 9
  }
}

export const tenCounts = {
  count: 10,
  additionalInfo: {
    moneyToCollect: 16
  }
}

export const hundryCounts = {
  count: 100,
  additionalInfo: {
    moneyToCollect: 200
  }
}

export const thosendCounts = {
  count: 1000,
  additionalInfo: {
    moneyToCollect: 1001
  }
}

export default initialState
