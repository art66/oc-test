import { createSelector } from 'reselect'
import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getIconClass, getAdditionalInfo } from '../../../../utils/globalSelectorsHelpers'

import styles from '../../../../styles/bootlegging.cssmodule.scss'

const FIXER = 1
const DISABLE_BORDER = false
const BUTTON_LABEL = 'Collect'

// reselect functions
const rowIcon = createSelector([getIconClass], iconClass => styles[iconClass])
const containerWidth = createSelector([getAdditionalInfo], additionalInfo => {
  const { moneyToCollect = 0 } = additionalInfo
  const disksCountLength = moneyToCollect.toString().length
  const disksCountOptimizedLength = moneyToCollect.toString().length / 5
  const disksWidth = (disksCountLength - disksCountOptimizedLength) * 10 + FIXER || null

  return disksWidth
})

const globalCrimeRowProps = createSelector([globalRowPropsSelector, rowIcon, getAdditionalInfo], (...props) => {
  const [globalProps, iconClass, additionalInfo] = props

  return {
    ...globalProps,
    button: {
      ...globalProps.button,
      label: BUTTON_LABEL,
      disabled: globalProps.button.disabled || !additionalInfo.moneyToCollect
    },
    styles,
    iconClass,
    additionalInfo,
    nonBorder: DISABLE_BORDER
  }
})

const ownCrimeRowProps = props => ({
  containerWidth: containerWidth(props),
  throwGlobalCrimeRowProps: globalCrimeRowProps(props)
})

export default ownCrimeRowProps
