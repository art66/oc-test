const initialState = {
  callback: () => {},
  showOutcome: false,
  isRowActive: false,
  jailed: false,
  minCrimeLevel: {
    value: 25
  },
  minCrimeLVL: {
    available: true
  },
  itemId: 0,
  lastSubCrimeID: 0,
  closeOutcome: () => {},
  currentRowID: 0,
  state: '',
  isShopComplete: false,
  attempt: () => {},
  available: false,
  crimeID: 0,
  iconClass: '',
  maxProgress: 0,
  isCurrentRow: false,
  mediaType: '',
  nerve: 0,
  outcome: {},
  progress: 0,
  requirements: {
    minCrimeLevel: {
      value: 50,
      available: false
    }
  },
  title: ''
}

export const attemptButtonActive = {
  isRowActive: true,
  available: true
}

export const minCrimeLevel = {
  ...initialState,
  minCrimeLVL: {
    available: false
  },
  available: false
}

export const attemptButtonDisable = {
  available: false
}

export const educationIcon = {
  requirements: {
    minCrimeLevel: {
      available: true
    }
  }
}

export const mustShowActive = {
  ...initialState,
  isRowActive: true,
  showOutcome: true
}

export default initialState
