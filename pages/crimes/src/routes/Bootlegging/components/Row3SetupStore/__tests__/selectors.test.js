import initialState from './mocks/selectors'
import ownCrimeRowProps from '../selectors'

describe('Row3SetupStore selectors', () => {
  it('should return locked className if row is not available', () => {
    const { rowAvaibility } = ownCrimeRowProps(initialState)

    expect(rowAvaibility).toBe('locked')
  })
  it('should return blank image if user does not have enaught crime LVL', () => {
    const { minCrimeLVL } = ownCrimeRowProps(initialState)

    expect(minCrimeLVL).toBe('lvlBlank')
  })
  it('should return short level name if screen is less then desktop', () => {
    const { deviceTypeLVL } = ownCrimeRowProps(initialState)

    expect(deviceTypeLVL).toBe('Lvl')
  })
  it('should return current progress of SetupOnline Shop progress', () => {
    const { barProgress } = ownCrimeRowProps(initialState)

    expect(barProgress).toBe(50)
  })
  it('should return Programming education required title in case of missing education achivment', () => {
    const { eduTitle } = ownCrimeRowProps(initialState)

    expect(eduTitle).toBe('Programming education required')
  })
  it('should return props for global CrimeRow', () => {
    const { throwGlobalCrimeRowProps } = ownCrimeRowProps(initialState)

    expect(typeof throwGlobalCrimeRowProps === 'object').toBeTruthy()
  })
})
