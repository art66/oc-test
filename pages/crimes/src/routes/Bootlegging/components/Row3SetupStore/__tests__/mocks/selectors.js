const initialState = {
  minCrimeLVL: {
    available: false
  },
  requirements: {
    minCrimeLevel: {
      available: false,
      value: 0
    }
  },
  progress: 50,
  maxProgress: 100,
  available: false,
  mediaType: 'Tablet'
}

export default initialState
