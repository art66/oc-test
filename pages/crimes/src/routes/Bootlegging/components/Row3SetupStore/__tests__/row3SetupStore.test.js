import React from 'react'
import { shallow, mount } from 'enzyme'
import initialState, {
  attemptButtonActive,
  attemptButtonDisable,
  educationIcon,
  minCrimeLevel,
  mustShowActive
} from './mocks/row3SetupStore'
import Row3SetupStore from '../Row3SetupStore'

describe('<Row3SetupStore />', () => {
  it('should render the row', () => {
    const Component = shallow(<Row3SetupStore {...initialState} />)

    expect(Component.find('CrimeRow').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('attempt button should be disabled', () => {
    const Component = shallow(<Row3SetupStore {...initialState} />)
    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('attempt button should be active', () => {
    const buttonIsActive = Object.assign(initialState, attemptButtonActive)
    const Component = shallow(<Row3SetupStore {...buttonIsActive} />)
    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('row should have \'active\' className if clicked his attempt button', () => {
    const Component = shallow(<Row3SetupStore {...mustShowActive} />)
    const buttonIsDisabled = Component.instance().render().props.className

    expect(buttonIsDisabled).toEqual('active')
    expect(Component).toMatchSnapshot()
  })
  it('minLvl icon should be locked', () => {
    const Component = mount(<Row3SetupStore {...minCrimeLevel} />)

    expect(Component.find('StarLVL').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('activeDefs')).toBeFalsy()
    expect(Component.find('SVGIconGenerator').prop('fill')).toEqual({ color: '#bdbdaf', strokeWidth: 0 })

    expect(Component).toMatchSnapshot()
  })
  it('minLvl icon should be shown', () => {
    const eduIcon = Object.assign(initialState, educationIcon)
    const Component = shallow(<Row3SetupStore {...eduIcon} />)

    expect(Component.find('.lvlBlank').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('education icon should be locked', () => {
    const eduIcon = Object.assign(initialState, attemptButtonDisable)
    const Component = shallow(<Row3SetupStore {...eduIcon} />)

    expect(Component.find('.education-icon.locked').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('education icon should be shown', () => {
    const eduIcon = Object.assign(initialState, attemptButtonActive)
    const Component = shallow(<Row3SetupStore {...eduIcon} />)

    expect(Component.find('.education-icon.locked').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
