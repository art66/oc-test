import { createSelector } from 'reselect'
import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getIconClass, getTitle, getMediaType } from '../../../../utils/globalSelectorsHelpers'

import styles from '../../../../styles/bootlegging.cssmodule.scss'

const getIsShopComplete = props => props.isShopComplete
const getAvailability = props => props.available
const getMinCrimeLVL = props => props.requirements.minCrimeLevel
const getProgress = props => props.progress
const getMaxProgress = props => props.maxProgress

// reselect functions
const rowIcon = createSelector([getIconClass], iconClass => styles[iconClass])
const rowAvaibility = createSelector([getAvailability], available => (available ? '' : styles['locked']))
const minCrimeLVL = createSelector([getMinCrimeLVL], ({ available }) => (available ? 'lvlReady' : 'lvlBlank'))
const deviceTypeLVL = createSelector([getMediaType], mediaType => (mediaType === 'desktop' ? 'Level' : 'Lvl'))
const barProgress = createSelector(
  [getProgress, getMaxProgress],
  (progress, maxProgress) => (100 * progress) / maxProgress
)

const eduTitleState = createSelector(
  [getAvailability],
  available => (available ? 'Programming education' : 'Programming education required')
)
const rowTitle = createSelector([getMediaType, getTitle], (mediaType, title) => {
  if (mediaType === 'mobile') {
    return 'Setup E-Store'
  }

  return title
})

const globalCrimeRowProps = createSelector(
  [globalRowPropsSelector, eduTitleState, rowIcon, getIsShopComplete, getAvailability, rowTitle],
  (...props) => {
    const [globalProps, eduTitle, iconClass, isShopComplete, available, title] = props

    return {
      ...globalProps,
      styles,
      eduTitle,
      iconClass,
      isShopComplete,
      available,
      title
    }
  }
)

const ownCrimeRowProps = props => ({
  rowAvaibility: rowAvaibility(props),
  minCrimeLVL: minCrimeLVL(props),
  deviceTypeLVL: deviceTypeLVL(props),
  barProgress: barProgress(props),
  eduTitle: eduTitleState(props),
  throwGlobalCrimeRowProps: globalCrimeRowProps(props)
})

export default ownCrimeRowProps
