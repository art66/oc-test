import PropTypes from 'prop-types'
import React, { Component } from 'react'
import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

import { bootleggingRow23SetupStore } from '../../utils/shouldRowsUpdate'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'

import { CrimeRow } from '../../../../components'
import StarLVL from '../../../../components/StarLVL'

import ownCrimeRowProps from './selectors'
import rowStyles from './styles.cssmodule.scss'
import styles from '../../../../styles/bootlegging.cssmodule.scss'

const PLATINUM_STAR_ID = 100
const GREY_STAR_CONF = {
  activeDefs: false,
  fill: {
    color: '#bdbdaf',
    strokeWidth: 0
  }
}

export class Row3SetupStore extends Component {
  static propTypes = {
    callback: PropTypes.func,
    showOutcome: PropTypes.bool,
    itemId: PropTypes.number,
    lastSubCrimeID: PropTypes.number,
    closeOutcome: PropTypes.func,
    currentRowID: PropTypes.number,
    state: PropTypes.string,
    isShopComplete: PropTypes.bool,
    attempt: PropTypes.func,
    available: PropTypes.bool,
    crimeID: PropTypes.number,
    iconClass: PropTypes.string,
    maxProgress: PropTypes.number,
    isCurrentRow: PropTypes.bool,
    mediaType: PropTypes.string,
    nerve: PropTypes.number,
    outcome: PropTypes.object,
    progress: PropTypes.number,
    requirements: PropTypes.object,
    title: PropTypes.string.isRequired
  }

  static defaultProps = {
    attempt: () => {},
    callback: () => {},
    closeOutcome: () => {},
    showOutcome: false,
    itemId: 0,
    lastSubCrimeID: 0,
    currentRowID: 0,
    state: '',
    isShopComplete: false,
    available: false,
    crimeID: 0,
    iconClass: '',
    maxProgress: 0,
    isCurrentRow: false,
    mediaType: '',
    nerve: 0,
    outcome: {},
    progress: 0,
    requirements: {}
  }

  componentDidMount() {
    this._initializeTooltrips()
  }

  shouldComponentUpdate(nextProps, nextState) {
    const rowCheck = bootleggingRow23SetupStore(this.props, nextProps)
    const globalCheck = globalCrimeRowCheck(this.props, nextProps)

    return rowCheck || globalCheck || false
  }

  componentDidUpdate() {
    this._initializeTooltrips({ update: true })
  }

  _initializeTooltrips = ({ update = false } = {}) => {
    const { barProgress, eduTitle } = ownCrimeRowProps(this.props)

    if (update) {
      tooltipSubscriber.update({ child: `${barProgress}% complete`, ID: 'row_3-bar-line' })
    }

    tooltipSubscriber.subscribe({ child: eduTitle, ID: `row_3-${eduTitle}` })
    tooltipSubscriber.subscribe({ child: `${barProgress}% complete`, ID: 'row_3-bar-line' })
  }

  _renderLVLStar = () => {
    const {
      requirements: { minCrimeLevel = {} }
    } = this.props

    const { minCrimeLVL } = ownCrimeRowProps(this.props)
    const isLowLVL = minCrimeLVL === 'lvlBlank'

    return <StarLVL lvl={PLATINUM_STAR_ID} customTitle={minCrimeLevel.value} customProps={isLowLVL && GREY_STAR_CONF} />
  }

  render() {
    const { rowAvaibility, deviceTypeLVL, barProgress, eduTitle, throwGlobalCrimeRowProps: props } = ownCrimeRowProps(
      this.props
    )

    const rowClassnames = `${styles.inner_cell_1} ${styles.underTitle} ${styles.dlm}
      ${styles.center} ${styles.shopBarLine}`

    return (
      <CrimeRow {...props} rowShouldUpdate={bootleggingRow23SetupStore} underTitle>
        <div className={rowClassnames}>
          <div id='row_3-bar-line' className={`${styles['pb']} ${styles['green']} ${rowStyles.withPointerEvent}`}>
            <div className={styles['bar-line']} style={{ width: `${barProgress}%` }}>
              <div className={styles['highlight']} />
            </div>
          </div>
        </div>
        <div className={`${styles.inner_cell_2} ${styles.dlm}`}>
          <div className={`${styles['required-item-1']} ${rowStyles.reqItem1}`}>
            <div className={`${styles.imageWrap} ${styles.responsive}`}>
              <i id={`row_3-${eduTitle}`} className={`${styles['education-icon']} ${rowAvaibility}`} />
            </div>
          </div>
          <div className={`${styles['required-item-2']} ${rowStyles.reqItem2}`}>
            <div className={`${rowStyles.lvlWrap} ${styles['level-wrap']}`}>
              <span className={`${rowStyles.lvlText} ${styles['level']}`}>{deviceTypeLVL}</span>
              <div className={rowStyles.iconContainer}>{this._renderLVLStar()}</div>
            </div>
          </div>
        </div>
      </CrimeRow>
    )
  }
}

export default Row3SetupStore
