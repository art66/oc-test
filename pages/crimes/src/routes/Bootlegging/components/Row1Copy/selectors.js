import { createSelector } from 'reselect'
import classnames from 'classnames'
import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getIconClass } from '../../../../utils/globalSelectorsHelpers'
import styles from '../../../../styles/bootlegging.cssmodule.scss'

const BUTTON_LABEL = 'Copy'
const DISABLE_BORDER = false
const UNDERTITLE = true
const CD_ON = true
const CD_OFF = false

const getGenre = props => props.currentGenre
const getGenres = props => props.genres
const checkCDSelected = props => props.cdSelected
const getBlankCDs = props => props.blankCDs
// const getCDSelected = props => props.cdSelected
const getBlankSelectedCDs = props => props.blankSelectedCDs
const getRequirements = props => props.requirements
const getTempCountUsed = props => props.tempCountUsed

// reselect functions
const rowIcon = createSelector([getIconClass], iconClass => styles[iconClass])
const currentGenre = createSelector([getGenre], genre => genre || 1)
const blankCDs = createSelector([getBlankCDs], gettedblankCDs => gettedblankCDs)
const tempCountUsed = createSelector([getTempCountUsed], tempCountUsedGetted => tempCountUsedGetted)

const mapGenres = createSelector([getGenres], genres => {
  return Object.keys(genres).map(id => ({ label: genres[id], value: id }))
})

const checkCDsEnabled = createSelector(
  [getBlankSelectedCDs, getRequirements],
  (...props) => {
    const [blankSelectedCDs, requirements] = props
    const items = requirements && requirements.items
    const imgToggleEnabled = blankSelectedCDs

    return {
      items,
      imgToggleEnabled,
      completeEnabled: true
    }
  }
)

const classNameHolder = createSelector([checkCDsEnabled], ({ items, completeEnabled }) => {
  const ri0 = classnames({
    [styles.imageWrap]: true,
    [styles.responsive]: true,
    [styles.imageWrapForCDs]: true,
    [styles.cdsAddButton]: true,
    [styles['selector']]: true,
    [styles.cdSelected]: completeEnabled,
    [styles['locked']]: false
  })

  const ri1 = classnames({
    [styles.imageWrap]: true,
    [styles.responsive]: true,
    [styles['locked']]: !items[1].available
  })

  return {
    ri0,
    ri1
  }
})

const cdSelected = createSelector([checkCDSelected, checkCDsEnabled], (someCDSelected, CDsEnabled) => {
  const { imgToggleEnabled } = CDsEnabled

  const isCDSelected = someCDSelected || imgToggleEnabled ? CD_ON : CD_OFF

  return isCDSelected
})

const checkIsCDSelected = createSelector([checkCDSelected, checkCDsEnabled], (someCDSelected, CDsEnabled) => {
  const { imgToggleEnabled } = CDsEnabled

  return someCDSelected || imgToggleEnabled ? CD_ON : CD_OFF
})

const globalCrimeRowProps = createSelector(
  [globalRowPropsSelector, checkIsCDSelected, rowIcon, currentGenre, checkCDsEnabled, blankCDs, tempCountUsed],
  (...props) => {
    const [globalProps, isCDSelected, iconClass, genre, isCDsEnabled, gettedBlankCDs, tempCountUsedGetted] = props

    const hasValue2Props = tempCountUsedGetted && Object.keys(tempCountUsedGetted).length >= 1

    const valueToAction = hasValue2Props ?
      `&value1=${genre}&value2=${JSON.stringify(tempCountUsedGetted)}` :
      `&value1=${genre}`

    const a = {
      ...globalProps,
      button: {
        ...globalProps.button,
        label: BUTTON_LABEL,
        disabled: globalProps.button.disabled || !isCDsEnabled.completeEnabled,
        action: () => globalProps.button.action({ subURL: valueToAction })
      },
      nonBorder: DISABLE_BORDER,
      underTitle: UNDERTITLE,
      blankCDs: gettedBlankCDs,
      isCDSelected,
      iconClass,
      genre,
      styles
    }

    return a
  }
)

const ownCrimeRowProps = props => ({
  genre: currentGenre(props),
  cdSelected: cdSelected(props),
  mapGenres: mapGenres(props),
  blankCDs: blankCDs(props),
  classNameHolder: classNameHolder(props),
  checkCDsEnabled: checkCDsEnabled(props),
  throwGlobalCrimeRowProps: globalCrimeRowProps(props)
})

export default ownCrimeRowProps
