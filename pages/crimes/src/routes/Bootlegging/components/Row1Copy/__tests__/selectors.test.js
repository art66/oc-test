import initialState, { genresExpectedResult, CDsExpectedResult, disabledButton } from './mocks/selectors'
import ownCrimeRowProps from '../selectors'

describe('Row1Copy selector', () => {
  it('should return mapGenres selecter with collected genres info', () => {
    const { mapGenres } = ownCrimeRowProps(initialState)

    expect(mapGenres).toEqual(genresExpectedResult)
  })
  it('should return checkCDsEnabled selector with info about icons in placeholders of crimeRow', () => {
    const { checkCDsEnabled } = ownCrimeRowProps(initialState)

    expect(checkCDsEnabled).toEqual(CDsExpectedResult)
  })
  it('should return current count of blankCDs in state', () => {
    const { blankCDs } = ownCrimeRowProps(initialState)

    expect(blankCDs).toEqual(100)
  })
  it('should return classNameHolder with collected info about classes in crimeRow. Secound icon must be locked', () => {
    const { classNameHolder } = ownCrimeRowProps(initialState)

    expect(classNameHolder.ri0).toEqual('imageWrap responsive imageWrapForCDs cdsAddButton selector cdSelected')
    expect(classNameHolder.ri1).toEqual('imageWrap responsive locked')
  })
  it('should return locked button in case of jailed status', () => {
    const { throwGlobalCrimeRowProps } = ownCrimeRowProps(disabledButton)

    expect(throwGlobalCrimeRowProps.button.disabled).toBeTruthy()
  })
  it('should return whole props of the row that must be trasmitted to rhe Global CrimeRow component', () => {
    const { throwGlobalCrimeRowProps } = ownCrimeRowProps(initialState)

    expect(typeof throwGlobalCrimeRowProps === 'object').toBeTruthy()
  })
})
