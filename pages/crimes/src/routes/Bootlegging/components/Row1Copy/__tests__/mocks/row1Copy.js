export default {
  state: '',
  breakSelectedCDs: () => {},
  blankCDsCount: 0,
  isRowActive: true,
  jailed: false,
  isCurrentRow: true,
  showOutcome: true,
  itemId: 0,
  lastSubCrimeID: 0,
  currentRowID: 0,
  closeOutcome: () => {},
  blankSelectedCDs: false,
  additionalInfo: {},
  attempt: () => {},
  available: true,
  blankCDs: 0,
  cdSelected: false,
  crimeID: 8,
  genres: {},
  iconClass: '',
  mediaType: 'desktop',
  nerve: 0,
  outcome: {},
  requirements: {
    items: [
      {
        available: true
      },
      {}
    ]
  },
  showCDsSelector: false,
  title: '',
  toggleCDsSelector: () => {}
}

export const mobileMode = {
  mediaType: 'mobile'
}

export const tabletMode = {
  mediaType: 'tablet'
}

export const desktopMode = {
  mediaType: 'desktop'
}

export const popUpShowed = {
  blankSelectedCDs: true,
  showCDsSelector: true,
  cdSelected: true
}

export const cdDoesNotSelected = {
  blankSelectedCDs: false,
  showCDsSelector: false,
  cdSelected: false
}
