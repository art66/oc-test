const initialState = {
  lastSubCrimeID: 0,
  currentRowID: 0,
  className: '',
  crimeID: 0,
  isCurrentRow: false,
  closeOutcome: () => {},
  nonBorder: false,
  itemId: 0,
  showOutcome: false,
  underTitle: true,
  iconClass: 'undefined',
  nerve: 0,
  outcome: {},
  styles: {},
  state: '',
  title: '',
  isRowNotInProgress: false,
  IsRowHaveOutcome: false,
  blankCDs: 100,
  cdSelected: true,
  genres: {
    1: 'Pop',
    2: 'Rock',
    3: 'Urban',
    4: 'Country',
    5: 'Classical',
    6: 'Dance',
    7: 'Rap',
    8: 'Jazz'
  },
  requirements: {
    items: [
      {
        available: false,
        blankCDsCount: 0,
        count: 0,
        label: 'Pack of Blank CDs',
        value: 0
      },
      {
        available: false,
        count: 0,
        label: 'Personal Computer',
        value: 0
      }
    ]
  }
}

export const genresExpectedResult = [
  { label: 'Pop', value: '1' },
  { label: 'Rock', value: '2' },
  { label: 'Urban', value: '3' },
  { label: 'Country', value: '4' },
  { label: 'Classical', value: '5' },
  { label: 'Dance', value: '6' },
  { label: 'Rap', value: '7' },
  { label: 'Jazz', value: '8' }
]

export const CDsExpectedResult = {
  items: [
    {
      available: false,
      blankCDsCount: 0,
      count: 0,
      label: 'Pack of Blank CDs',
      value: 0
    },
    {
      available: false,
      count: 0,
      label: 'Personal Computer',
      value: 0
    }
  ],
  imgToggleEnabled: true,
  completeEnabled: true
}

export const disabledButton = {
  ...initialState,
  jailed: true
}

export default initialState
