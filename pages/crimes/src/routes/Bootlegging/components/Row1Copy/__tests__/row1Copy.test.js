import React from 'react'
import { shallow, mount } from 'enzyme'
import initialState, { mobileMode, tabletMode, desktopMode, popUpShowed, cdDoesNotSelected } from './mocks/row1Copy'
import Row1Copy from '../Row1Copy'

describe('<Row1Copy />', () => {
  it('should render this row', () => {
    const Component = shallow(<Row1Copy {...initialState} />)

    expect(Component.find('.inner_cell_1.underTitle.dlm').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render in desktop mode', () => {
    const Component = shallow(<Row1Copy {...initialState} />)
    const toBeDesktop = Component.instance().props.mediaType

    expect(toBeDesktop).toEqual('desktop')
    expect(Component.find('.inner_cell_1.underTitle.dlm').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render in tablet mode', () => {
    const newState = Object.assign(initialState, tabletMode)
    const Component = shallow(<Row1Copy {...newState} />)
    const toBeTablet = Component.instance().props.mediaType

    expect(toBeTablet).toEqual('tablet')
    expect(Component.find('.inner_cell_1.underTitle.dlm').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render in mobile mode', () => {
    const newState = Object.assign(initialState, mobileMode)
    const Component = shallow(<Row1Copy {...newState} />)
    const toBeMobile = Component.instance().props.mediaType

    expect(toBeMobile).toEqual('mobile')
    expect(Component.find('.inner_cell_1.underTitle.dlm').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('Dropdown is shown', () => {
    const newState = Object.assign(initialState, desktopMode)
    const Component = shallow(<Row1Copy {...newState} />)

    expect(Component.find('Dropdown').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('HorizontalSelect is shown', () => {
    const newState = Object.assign(initialState, mobileMode)
    const Component = shallow(<Row1Copy {...newState} />)

    expect(Component.find('HorizontalSelect').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('CDs must be selected and it\'s image should be placed in the row placeholder', () => {
    const newState = Object.assign(initialState, desktopMode)
    let Component = mount(<Row1Copy {...newState} />)

    // Popup should Be shown based on click and updated prop 'showCDsSelector'
    Component.find('.imageWrap.responsive.imageWrapForCDs.selector').simulate('click')
    const newStateWithPopup = Object.assign(newState, popUpShowed)

    Component = mount(<Row1Copy {...newStateWithPopup} />)

    // After Popup click must be placed CDs image in the row placeholder by prop 'cdSelected' or 'blankSelectedCDs'
    const {
      requirements: { items },
      blankCDs,
      blankSelectedCDs,
      cdSelected
    } = newStateWithPopup
    const imgToggleEnabled = items[0].available && (blankCDs > 0 || blankSelectedCDs)

    expect(cdSelected && imgToggleEnabled).toBeTruthy()
    expect(Component.find('.amountOfCDs').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('Dropdown should reflect on click and then change \'genre\' in the state of row', () => {
    const newState = Object.assign(initialState, desktopMode)
    const Component = mount(<Row1Copy {...newState} />)

    Component.setState({ currentGenre: 1 })

    expect(
      Component.find({ name: 'selected-state' })
        .first()
        .prop('value')
    ).toEqual(1)

    Component.find('Dropdown').simulate('change')
    Component.setState({ currentGenre: 2 })

    expect(
      Component.find({ name: 'selected-state' })
        .first()
        .prop('value')
    ).toEqual(2)
    expect(Component).toMatchSnapshot()
  })
  it('HorizontalSelect should reflect on scroll and then change \'genre\' in the row state', () => {
    const newState = Object.assign(initialState, tabletMode)
    const Component = shallow(<Row1Copy {...newState} />)

    expect(Component.find('HorizontalSelect').prop('value')).toEqual(1)

    Component.find('HorizontalSelect').simulate('change')
    Component.setState({ currentGenre: 3 })

    expect(Component.find('HorizontalSelect').prop('value')).toEqual(3)
    expect(Component).toMatchSnapshot()
  })
  it('Attempt button should be active', () => {
    const cdsSelected = Object.assign(initialState, popUpShowed)
    const Component = shallow(<Row1Copy {...cdsSelected} />)
    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('Attempt button should be disabled', () => {
    const cdsNotSelected = Object.assign(initialState, cdDoesNotSelected)
    const Component = shallow(<Row1Copy {...cdsNotSelected} />)
    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
})
