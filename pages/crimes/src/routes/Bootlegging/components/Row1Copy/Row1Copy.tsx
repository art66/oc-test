import React, { Component, Fragment } from 'react'
import { toMoney } from '@torn/shared/utils'
import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

import firstLetterUpper from '../../../../utils/firstLetterUpper'
import { bootleggingRow1CopyUpdate } from '../../utils/shouldRowsUpdate'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'

import { Dropdown, HorizontalSelect, CrimeRow } from '../../../../components'
import ItemCell from '../../../../components/ItemCell'

import ownCrimeRowProps from './selectors'

import localStyles from './index.cssmodule.scss'
import styles from '../../../../styles/bootlegging.cssmodule.scss'

const ROW_ID = 'row_copy'
const CD_PACK_LABEL = 'cd_pack'
const CD_TYPES = ['Pop', 'Rock', 'Urban', 'Country', 'Classical', 'Dance', 'Rap', 'Jazz']

export interface IProps {
  state: string
  isCurrentRow: boolean
  showOutcome: boolean
  itemId: number
  lastSubCrimeID: number
  currentRowID: number
  blankSelectedCDs: boolean
  additionalInfo: object
  available: boolean
  blankCDs: number
  cdSelected: boolean
  crimeID: number
  genres: object
  iconClass: string
  mediaType: string
  nerve: number
  outcome: object
  requirements: object
  showCDsSelector: boolean
  title: string
  toggleCDsSelector: (status: boolean) => void
  breakSelectedCDs: () => void
  closeOutcome: () => void
  attempt: () => void
}

export interface IState {
  currentGenre: number
}

export class Row1Copy extends Component<IProps, IState> {
  static defaultProps = {
    breakSelectedCDs: () => {},
    toggleCDsSelector: () => {},
    closeOutcome: () => {},
    attempt: () => {},
    state: '',
    isCurrentRow: false,
    showOutcome: false,
    itemId: 0,
    lastSubCrimeID: 0,
    currentRowID: 0,
    blankSelectedCDs: false,
    additionalInfo: {},
    available: false,
    blankCDs: 0,
    cdSelected: false,
    genres: {},
    iconClass: '',
    mediaType: '',
    nerve: 0,
    outcome: {},
    requirements: {},
    showCDsSelector: false
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      currentGenre: 1
    }
  }

  componentDidMount() {
    this._initializeTooltips()
  }

  shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    const { crimeID } = this.props
    const { currentGenre: genre } = this.state
    const { cdSelected: isCDSelected, blankCDs } = ownCrimeRowProps(this.props)
    const { cdSelected: nextPropsCDSelected, blankCDs: blankCDsNext } = ownCrimeRowProps(nextProps)

    const prevProps = {
      crimeID,
      isCDSelected,
      genre,
      blankCDs
    }

    const newProps = {
      isCDSelected: nextPropsCDSelected,
      genre: nextState.currentGenre,
      blankCDs: blankCDsNext
    }

    const rowCheck = bootleggingRow1CopyUpdate(prevProps, newProps)
    const globalCheck = globalCrimeRowCheck(this.props, nextProps)

    return rowCheck || globalCheck || false
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _handleChange = currentGenre => {
    this.setState({
      currentGenre
    })
  }

  _toggleSelector = () => {
    const { toggleCDsSelector } = this.props

    toggleCDsSelector(true)
  }

  _getCDsSelector = () => {
    const { mediaType } = this.props
    const { currentGenre } = this.state
    const { mapGenres: genres } = ownCrimeRowProps(this.props)

    if (mediaType !== 'desktop') {
      return <HorizontalSelect value={currentGenre} options={genres} onChange={this._handleChange} />
    }

    return (
      <Dropdown
        options={genres}
        simpleValue={true}
        value={currentGenre}
        placeholder='Pop'
        name='selected-state'
        onChange={this._handleChange}
      />
    )
  }

  _getCDsImage = () => {
    const {
      checkCDsEnabled: { completeEnabled }
    } = ownCrimeRowProps(this.props)

    if (!completeEnabled) return null

    return (
      <Fragment>
        <img alt='disk' className={styles.disk} src='/images/items/44/disk.png' />
      </Fragment>
    )
  }

  _makeCustomOutcome = props => {
    const { mediaType, outcome } = props

    if (outcome.result !== 'success') {
      return outcome
    }

    const [CDsCount] = (props.outcome && props.outcome.outcomeDesc && props.outcome.outcomeDesc.match(/\d+/g)) || []
    const CDsType = (props.outcome && props.outcome.outcomeDesc && props.outcome.outcomeDesc.match(/[^\d\s]\w+/g)) || []

    const currentCDsType = CD_TYPES.find(item => CDsType.find(cdType => item === cdType))
    const currentCDsTypeWithLowerCase = currentCDsType && currentCDsType.toLowerCase()

    const label = mediaType !== 'mobile' ? firstLetterUpper(currentCDsType) : ''
    const resultText = `Copying ${CDsCount} ${label} CDs`

    // TODO: lazy hack below! Replace this part of code by correct back-end outcome responce!
    // write to Pavel about it first!
    return {
      ...props.outcome,
      customDesc: true,
      outcomeDesc: `
        <div class="${localStyles.disksWrap}">
          <i class="${localStyles['disk-image-tiny']} ${localStyles[currentCDsTypeWithLowerCase]}"></i>
          <span>${resultText}</span>
        </div>
      `
    }
  }

  _initializeTooltips = () => {
    const { items } = this._getRowProps()
    const firstItem = this._findFirstIReqItem(items)

    const tooltipsItems = [
      {
        child: firstItem.label,
        ID: `${ROW_ID}-${firstItem.label}`
      },
      {
        child: 'Pack of Blank CDs',
        ID: `${ROW_ID}-${CD_PACK_LABEL}`
      }
    ]

    this._renderTooltips(tooltipsItems)
  }

  _updateTooltips = () => {
    const tooltipsItems = [
      {
        child: 'Pack of Blank CDs',
        ID: `${ROW_ID}-${CD_PACK_LABEL}`
      }
    ]

    this._renderTooltips(tooltipsItems)
  }

  _renderTooltips = (tooltips = []) => {
    tooltips.forEach(tooltipItem => tooltipSubscriber.subscribe(tooltipItem))
  }

  _findFirstIReqItem = (items = []) => {
    const itemFounded = items.find(item => item.value === '61') || {}

    return itemFounded
  }

  _renderItemsSection = (items) => {
    const {
      blankCDs
    } = ownCrimeRowProps(this.props)

    const { label, value, available } = this._findFirstIReqItem(items)

    return (
      <div className={`${styles.inner_cell_2} ${styles.dlm}`}>
        <ItemCell
          imgAlt='computer'
          ID={`${ROW_ID}-${label}`}
          count={null}
          imgSrc={`/images/items/${value}/${available ? 'large' : 'blank'}.png`}
        />
        <ItemCell
          imgAlt='disk'
          ID={`${ROW_ID}-${CD_PACK_LABEL}`}
          count={toMoney(blankCDs) as unknown as number}
          imgSrc='/images/items/44/disk.png'
          isButton={true}
          callback={this._toggleSelector}
        />
      </div>
    )
  }

  _getRowProps = () => {
    const { ...rowProps } = this.props
    const { currentGenre } = this.state

    const selectorProps = {
      ...rowProps,
      currentGenre
    }

    const {
      checkCDsEnabled: { items },
      classNameHolder: { ri0, ri1 },
      throwGlobalCrimeRowProps: props
    } = ownCrimeRowProps(selectorProps)

    return {
      items,
      ri0,
      ri1,
      props
    }
  }

  render() {
    const { items, props } = this._getRowProps()

    const outcome = this._makeCustomOutcome(props)

    const propsWithOutcomeHook = {
      ...props,
      outcome
    }

    return (
      <CrimeRow {...propsWithOutcomeHook} rowShouldUpdate={bootleggingRow1CopyUpdate}>
        <div className={`${styles.inner_cell_1} ${styles.underTitle} ${styles.dlm}`}>{this._getCDsSelector()}</div>
        <div className={`${styles.inner_cell_2} ${styles.dlm}`}>
          {this._renderItemsSection(items)}
        </div>
      </CrimeRow>
    )
  }
}

export default Row1Copy
