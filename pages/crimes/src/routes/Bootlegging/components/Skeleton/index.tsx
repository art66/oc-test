import React from 'react'

import Placeholder from '../../../../components/Skeleton'

import globalStyles from '../../../../styles/skeleton.cssmodule.scss'
import styles from './styles.cssmodule.scss'

const DisksPlaceholder = () => {
  const disksArr = Array.from(Array(8).keys()).map(key => (
    <div className={styles.diskContainer} key={key}>
      <span className={styles.title} />
      <span className={styles.disk} />
      <span className={styles.queue} />
      <div className={styles.progress} />
      <span className={styles.total} />
    </div>
  ))

  return (
    <div className={styles.disksWrap}>
      {disksArr}
    </div>
  )
}

const CrimeRowPlaceholder = ({ type }: {type: string}) => {
  const row1Copy = () => {
    return (
      <div className={globalStyles.rowWrap}>
        <div className={globalStyles.titleImg} />
        <div className={`${globalStyles.titleText} ${globalStyles.titleTextUp} ${styles.titleText}`} />
        <div className={styles.selectGenre}>
          <div className={`${globalStyles.titleText} ${styles.genreTitle}`} />
          <div className={styles.genderButton} />
        </div>
        <div className={styles.itemsWrap}>
          <div className={`${globalStyles.itemContainer} ${globalStyles.itemContainerMini}`} />
          <div className={`${globalStyles.itemContainer} ${globalStyles.itemContainerMini}`} />
        </div>
        <div className={globalStyles.nerveContainer} />
        <div className={globalStyles.buttonAction} />
      </div>
    )
  }

  const row2Sell = () => {
    return (
      <div className={globalStyles.rowWrap}>
        <div className={globalStyles.titleImg} />
        <div className={`${globalStyles.titleText} ${styles.titleText}`} />
        <div className={styles.emptySellRowSpace} />
        <div className={globalStyles.nerveContainer} />
        <div className={globalStyles.buttonAction} />
      </div>
    )
  }

  const row3SetupStore = () => {
    return (
      <div className={globalStyles.rowWrap}>
        <div className={globalStyles.titleImg} />
        <div className={`${globalStyles.titleText} ${globalStyles.titleTextUp} ${styles.titleText}`} />
        <div className={`${globalStyles.progress} ${styles.setupProgress}`} />
        <div className={styles.itemsWrap}>
          <div className={`${globalStyles.itemContainer} ${globalStyles.itemContainerMini}`} />
          <div className={`${globalStyles.itemContainer} ${globalStyles.itemContainerMini}`} />
        </div>
        <div className={globalStyles.nerveContainer} />
        <div className={globalStyles.buttonAction} />
      </div>
    )
  }

  const bootleggingPlaceholder = (
    <>
      <div className={globalStyles.rowsWrap}>
        {row1Copy()}
        {row2Sell()}
        {row3SetupStore()}
      </div>
      {type === 'full' && DisksPlaceholder()}
    </>
  )

  return <Placeholder type={type}>{bootleggingPlaceholder}</Placeholder>
}

export default CrimeRowPlaceholder
