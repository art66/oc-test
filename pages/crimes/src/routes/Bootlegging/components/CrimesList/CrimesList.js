import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Row1Copy from '../Row1Copy/Row1Copy'
import Row2Sell from '../Row2Sell/Row2Sell'
import Row3SetupStore from '../Row3SetupStore/Row3SetupStore'
import Row4Collect from '../Row4Collect/Row4Collect'
import '../../../../styles/subcrimes.scss'

class CrimesList extends Component {
  static propTypes = {
    stat: PropTypes.object,
    callback: PropTypes.func,
    startBreakAttempt: PropTypes.func,
    startPooling: PropTypes.func,
    additionalInfo: PropTypes.object,
    blankSelectedCDs: PropTypes.bool,
    current: PropTypes.number,
    outcome: PropTypes.object,
    isCurrentRow: PropTypes.bool,
    lastSubCrimeID: PropTypes.number,
    currentRowID: PropTypes.number,
    startAttempt: PropTypes.func.isRequired,
    startToggleCDs: PropTypes.func,
    pushCurrentRow: PropTypes.func,
    attemptProgress: PropTypes.object,
    cdSelected: PropTypes.bool,
    copiedCDs: PropTypes.number,
    blankCDsCount: PropTypes.number,
    blankCDs: PropTypes.number,
    crimes: PropTypes.array,
    genres: PropTypes.object,
    progress: PropTypes.number,
    maxProgress: PropTypes.number,
    mediaType: PropTypes.string,
    showCDsSelector: PropTypes.bool,
    userNerve: PropTypes.number,
    startBreakSelectedCDs: PropTypes.bool,
    jailed: PropTypes.bool,
    typeID: PropTypes.number,
    tempCountUsed: PropTypes.number,
    closeOutcome: PropTypes.bool
  }

  static defaultProps = {
    stat: {},
    callback: () => {},
    startBreakAttempt: () => {},
    startPooling: () => {},
    additionalInfo: {},
    blankSelectedCDs: false,
    current: 0,
    outcome: {},
    isCurrentRow: false,
    lastSubCrimeID: 0,
    currentRowID: 0,
    startToggleCDs: () => {},
    pushCurrentRow: () => {},
    attemptProgress: {},
    cdSelected: false,
    copiedCDs: 0,
    blankCDsCount: 0,
    blankCDs: 0,
    crimes: [],
    genres: {},
    progress: 0,
    maxProgress: 0,
    mediaType: '',
    showCDsSelector: false,
    userNerve: 0
  }

  constructor(props) {
    super(props)

    this.state = {
      activeFundsCrime: false
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      additionalInfo: { progress, maxProgress }
    } = nextProps

    const setupOnlineStoreCompleted = progress === maxProgress && maxProgress !== undefined

    // revert 3rd crime to Setup store once crime lvl is decreased.
    if (!setupOnlineStoreCompleted && prevState.activeFundsCrime) {
      return {
        activeFundsCrime: false
      }
    }

    return null
  }

  componentDidUpdate() {
    const {
      additionalInfo: { progress, maxProgress }
    } = this.props
    const { activeFundsCrime } = this.state

    const setupOnlineStoreCompleted = progress === maxProgress && maxProgress !== undefined

    if (setupOnlineStoreCompleted && activeFundsCrime !== true) {
      setTimeout(() => {
        this.setState({
          activeFundsCrime: true
        })
      }, 1000)
    }
  }

  _attempt = (url, itemId) => {
    const { startAttempt, currentRowID } = this.props
    const onCurrentRow = currentRowID === Number(itemId)

    startAttempt(url, itemId, onCurrentRow)
  }

  _renderRow1Copy = outcomeState => {
    const {
      crimes,
      // blankCDsCount,
      attemptProgress,
      blankCDs,
      blankSelectedCDs,
      cdSelected,
      genres,
      mediaType,
      showCDsSelector,
      startToggleCDs,
      lastSubCrimeID,
      currentRowID,
      isCurrentRow,
      startBreakSelectedCDs,
      jailed,
      typeID,
      tempCountUsed,
      closeOutcome
    } = this.props

    const { isRowNotInProgress, outcome, isRowActive, isRowHaveOutcome, showOutcome, rowState } = outcomeState

    return (
      <Row1Copy
        {...crimes[0]}
        typeID={typeID}
        jailed={jailed}
        action={this._attempt}
        tempCountUsed={tempCountUsed}
        breakSelectedCDs={startBreakSelectedCDs}
        // blankCDsCount={blankCDsCount}
        blankCDs={blankCDs}
        blankSelectedCDs={blankSelectedCDs}
        cdSelected={cdSelected}
        genres={genres}
        mediaType={mediaType}
        itemId={attemptProgress.itemId}
        showCDsSelector={showCDsSelector}
        toggleCDsSelector={startToggleCDs}
        outcome={outcome}
        currentRowID={currentRowID}
        isCurrentRow={isCurrentRow}
        lastSubCrimeID={lastSubCrimeID}
        isRowActive={isRowActive}
        isRowNotInProgress={isRowNotInProgress}
        isRowHaveOutcome={isRowHaveOutcome}
        showOutcome={showOutcome}
        state={rowState}
        closeOutcome={closeOutcome}
      />
    )
  }

  _renderRow2Sell = outcomeState => {
    const {
      crimes,
      attemptProgress,
      mediaType,
      lastSubCrimeID,
      currentRowID,
      userNerve,
      isCurrentRow,
      copiedCDs,
      jailed,
      typeID,
      closeOutcome
    } = this.props

    const { isRowNotInProgress, isRowActive, outcome, isRowHaveOutcome, showOutcome, rowState } = outcomeState

    return (
      <Row2Sell
        {...crimes[1]}
        typeID={typeID}
        jailed={jailed}
        action={this._attempt}
        copiedCDs={copiedCDs}
        mediaType={mediaType}
        userNerve={userNerve}
        itemId={attemptProgress.itemId}
        outcome={outcome}
        currentRowID={currentRowID}
        isCurrentRow={isCurrentRow}
        lastSubCrimeID={lastSubCrimeID}
        isRowActive={isRowActive}
        isRowNotInProgress={isRowNotInProgress}
        isRowHaveOutcome={isRowHaveOutcome}
        showOutcome={showOutcome}
        state={rowState}
        closeOutcome={closeOutcome}
      />
    )
  }

  _renderRow3SetupStore = (outcomeState, setupOnlineStoreCompleted) => {
    const {
      crimes,
      attemptProgress,
      mediaType,
      lastSubCrimeID,
      currentRowID,
      current,
      isCurrentRow,
      progress,
      maxProgress,
      jailed,
      typeID,
      closeOutcome
    } = this.props

    const { isRowNotInProgress, isRowActive, outcome, isRowHaveOutcome, showOutcome, rowState } = outcomeState

    return (
      <Row3SetupStore
        {...crimes[2]}
        typeID={typeID}
        jailed={jailed}
        action={this._attempt}
        mediaType={mediaType}
        current={current}
        progress={progress}
        itemId={attemptProgress.itemId}
        maxProgress={maxProgress}
        isShopComplete={setupOnlineStoreCompleted}
        outcome={outcome}
        currentRowID={currentRowID}
        isCurrentRow={isCurrentRow}
        lastSubCrimeID={lastSubCrimeID}
        isRowActive={isRowActive}
        isRowNotInProgress={isRowNotInProgress}
        isRowHaveOutcome={isRowHaveOutcome}
        showOutcome={showOutcome}
        state={rowState}
        closeOutcome={closeOutcome}
      />
    )
  }

  _renderRow4Collect = outcomeState => {
    const {
      crimes,
      attemptProgress,
      additionalInfo,
      startPooling,
      mediaType,
      lastSubCrimeID,
      currentRowID,
      isCurrentRow,
      jailed,
      typeID,
      closeOutcome
    } = this.props

    const { isRowNotInProgress, isRowActive, outcome, isRowHaveOutcome, showOutcome, rowState } = outcomeState

    return (
      <Row4Collect
        {...crimes[2]}
        typeID={typeID}
        jailed={jailed}
        additionalInfo={additionalInfo}
        action={this._attempt}
        startPooling={startPooling}
        mediaType={mediaType}
        itemId={attemptProgress.itemId}
        outcome={outcome}
        currentRowID={currentRowID}
        isCurrentRow={isCurrentRow}
        lastSubCrimeID={lastSubCrimeID}
        isRowActive={isRowActive}
        isRowNotInProgress={isRowNotInProgress}
        isRowHaveOutcome={isRowHaveOutcome}
        showOutcome={showOutcome}
        state={rowState}
        closeOutcome={closeOutcome}
      />
    )
  }

  _renderRow3Row4 = outcomeState => {
    const { activeFundsCrime } = this.state
    const { setupOnlineStoreCompleted, isSetupOnlineShop, setupStoreClass, collectFundsClass } = this.props

    if (!activeFundsCrime && isSetupOnlineShop) {
      return (
        <div className={setupStoreClass}>{this._renderRow3SetupStore(outcomeState, setupOnlineStoreCompleted)}</div>
      )
    }

    return <div className={collectFundsClass}>{this._renderRow4Collect(outcomeState)}</div>
  }

  render() {
    const { crimes } = this.props

    if (!crimes) return null

    const [row1Copy, row2Sell, row3SetupStore4Collect] = crimes

    return (
      <div className='main-container'>
        <div className='subcrimes-list'>
          {this._renderRow1Copy(row1Copy)}
          {this._renderRow2Sell(row2Sell)}
          {this._renderRow3Row4(row3SetupStore4Collect)}
        </div>
      </div>
    )
  }
}

export default CrimesList
