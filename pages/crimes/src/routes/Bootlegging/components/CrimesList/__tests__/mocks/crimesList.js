const initialState = {
  user: {
    nerve: 2
  },
  stat: {},
  minCrimeLVL: {
    available: true
  },
  setupOnlineStoreCompleted: false,
  isSetupOnlineShop: true,
  setupStoreClass: 'store--wrap',
  collectFundsClass: 'funds--wrap',
  copiedCDs: 12,
  onCurrentRow: false,
  findRowForOutcome: [
    {
      isRowHaveOutcome: false,
      isRowNotInProgress: true,
      rowState: 'locked',
      showOutcome: false
    },
    {
      isRowHaveOutcome: false,
      isRowNotInProgress: true,
      rowState: 'locked',
      showOutcome: false
    },
    {
      isRowHaveOutcome: false,
      isRowNotInProgress: true,
      rowState: 'locked',
      showOutcome: false
    }
  ],
  callback: () => {},
  startBreakAttempt: () => {},
  startPooling: () => {},
  additionalInfo: {
    CDs: {},
    maxProgress: 10,
    progress: 0,
    moneyToCollect: 1
  },
  blankSelectedCDs: false,
  crimes: [
    {
      additionalInfo: {
        CDsLeft: true,
        blankCDs: 0,
        currentQueue: null,
        timeForCD: 0,
        timeLeftForNextCD: 0
      },
      available: true,
      crimeID: 0,
      currentTemplate: 'BootleggingMakeCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: 61,
            available: false
          },
          {
            label: '',
            value: 61,
            available: false
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    },
    {
      additionalInfo: null,
      available: true,
      minCrimeLVL: {
        available: true
      },
      crimeID: 1,
      currentTemplate: 'BootleggingSellCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: '',
            available: true
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    },
    {
      additionalInfo: {
        lastMoneyCheckStamp: 0,
        moneyToCollect: 0
      },
      minCrimeLVL: {
        available: true
      },
      minCrimeLevel: {
        value: 12
      },
      available: true,
      crimeID: 2,
      currentTemplate: 'BootleggingSellCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        minCrimeLevel: {
          value: 50,
          available: false
        }
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    }
  ],
  current: 0,
  outcome: {},
  isCurrentRow: false,
  lastSubCrimeID: 0,
  currentRowID: 0,
  startAttempt: () => {},
  startToggleCDs: () => {},
  pushCurrentRow: () => {},
  attemptProgress: {},
  cdSelected: false,
  blankCDsCount: 0,
  blankCDs: 0,
  genres: {},
  progress: 0,
  maxProgress: 10,
  showCDsSelector: false,
  mediaType: 'desktop'
}

export const setupOnlineStoreCompleted = {
  ...initialState,
  isSetupOnlineShop: false,
  additionalInfo: {
    CDs: {},
    maxProgress: 10,
    progress: 10,
    moneyToCollect: 1
  },
  crimes: [
    {
      additionalInfo: {
        CDsLeft: true,
        blankCDs: 0,
        currentQueue: null,
        timeForCD: 0,
        timeLeftForNextCD: 0
      },
      available: true,
      crimeID: 0,
      currentTemplate: 'BootleggingMakeCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: 61,
            available: false
          },
          {
            label: '',
            value: 61,
            available: false
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    },
    {
      additionalInfo: null,
      available: true,
      crimeID: 1,
      currentTemplate: 'BootleggingSellCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: '',
            available: true
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    },
    {
      additionalInfo: {
        lastMoneyCheckStamp: 0,
        moneyToCollect: 0
      },
      available: true,
      crimeID: 2,
      currentTemplate: 'BootleggingCollectFunds',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: '',
            available: true
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    }
  ]
}

export default initialState
