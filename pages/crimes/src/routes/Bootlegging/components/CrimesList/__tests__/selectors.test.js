import initialState from './mocks/selectors'
import {
  setupOnlineStoreCompletedState,
  isSetupOnlineShopState,
  setupStoreClassState,
  copiedCDsState
} from '../selectors'

describe('CrimeList selectors', () => {
  it('should return true once SetupOnline Store is completed', () => {
    expect(setupOnlineStoreCompletedState(initialState)).toBeTruthy()
  })
  it('should return true in case if BootleggingSellCD is the 3td crimeRow in the crime', () => {
    expect(isSetupOnlineShopState(initialState)).toBeTruthy()
  })
  it('should return the store--wrap for className once the SetupStore will be completed', () => {
    expect(setupStoreClassState(initialState)).toBe('store--wrap')
  })
  it('should return 16 copied CDs in crimeRow', () => {
    expect(copiedCDsState(initialState)).toBe(16)
  })
})
