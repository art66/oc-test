const initialState = {
  jailed: false,
  user: {
    nerve: 2
  },
  blankSelectedCDs: false,
  currentLevel: 0,
  isCurrentRow: false,
  lastSubCrimeID: 0,
  skillLevelTotal: 0,
  currentRowID: 0,
  crimeId: 0,
  showPlaceholder: true,
  additionalInfo: {
    progress: 0,
    sold: 0,
    customers: 0,
    CDsLeft: false,
    blankCDs: 20,
    currentQueue: [],
    moneyToCollect: 0,
    maxProgress: 0,
    timeForCD: 0,
    timeLeftForNextCD: 0,
    CDs: {
      1: 5,
      2: 3,
      3: 2,
      4: 2,
      5: 0,
      6: 1,
      7: 0,
      8: 3
    }
  },
  requirements: {
    items: [
      {
        available: false,
        blankCDsCount: 0,
        count: 0,
        label: 'Pack of Blank CDs',
        value: 0
      },
      {
        available: false,
        count: 0,
        label: 'Personal Computer',
        value: 0
      }
    ]
  },
  attemptProgress: {
    itemId: 0,
    progress: 0,
    inProgress: false
  },
  outcome: {
    customers: 0,
    outcomeDesc: '',
    result: '',
    rewardsGive: {
      itemsRewards: null,
      userRewards: {
        money: 0
      }
    },
    sold: 0,
    story: []
  },
  crimesByType: [
    {
      additionalInfo: {
        CDsLeft: true,
        blankCDs: 0,
        currentQueue: null,
        timeForCD: 0,
        timeLeftForNextCD: 0
      },
      available: true,
      crimeID: 0,
      currentTemplate: 'BootleggingMakeCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: 61,
            available: false
          },
          {
            label: '',
            value: 61,
            available: false
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    },
    {
      additionalInfo: null,
      available: true,
      crimeID: 1,
      currentTemplate: 'BootleggingSellCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: '',
            available: true
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    },
    {
      additionalInfo: {
        lastMoneyCheckStamp: 0,
        moneyToCollect: 0
      },
      available: true,
      crimeID: 2,
      currentTemplate: 'BootleggingSellCD',
      iconClass: '',
      user: {
        nerve: 2
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        }
      ],
      requirements: {
        items: [
          {
            label: '',
            value: '',
            available: true
          }
        ]
      },
      result: '',
      skillLevel: 0,
      skillLevelTotal: 0,
      subID: 0,
      title: ''
    }
  ],
  currentType: {
    ID: 0,
    active: 0,
    crimeRoute: '',
    expForLevel100: 0,
    img: '',
    title: '',
    typeID: 0,
    _id: {
      $oid: '56f940e9c46988ee2944bcdc'
    }
  },
  currentUserStatistics: [
    {
      label: '',
      value: ''
    }
  ],
  currentUserStats: {
    CDType1Sold: 0,
    CDType2Sold: 0,
    CDType3Sold: 0,
    CDType4Sold: 0,
    CDType5Sold: 0,
    CDType6Sold: 0,
    CDType7Sold: 0,
    CDType8Sold: 0,
    CDsCopiedTotal: 0,
    CDsSoldTotal: 0,
    attemptsTotal: 0,
    crimesByIDAttempts: {
      8: 0,
      9: 0,
      10: 0,
      11: 0
    },
    failedTotal: 0,
    moneyMadeOnlineStore: 0,
    moneyMadeTotal: 0,
    onlineStoreCustomers: 0,
    onlineStoreVisits: 0,
    skill: 0,
    skillLevel: 0,
    statsByType: {
      successesTotal: 0
    },
    successesTotal: 0
  },
  genres: {
    1: 'Pop',
    2: 'Rock',
    3: 'Urban',
    4: 'Country',
    5: 'Classical',
    6: 'Dance',
    7: 'Rap',
    8: 'Jazz'
  }
}

export default initialState
