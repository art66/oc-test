/* eslint-disable max-len */
import React from 'react'
import { mount, shallow } from 'enzyme'
import initialState, { setupOnlineStoreCompleted } from './mocks/crimesList'
import CrimesList from '../CrimesList'

describe('<CrimesList />', () => {
  it('should render CrimesList container', () => {
    const Component = shallow(<CrimesList {...initialState} />)

    expect(Component.find('.main-container').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render only Copy, Sell and SetupStore subrimes', () => {
    const Component = shallow(<CrimesList {...initialState} />)
    const { crimes, additionalInfo } = Component.instance().props

    const activeFundsCrime = additionalInfo.progress === additionalInfo.maxProgress
    const isSetupOnlineShop = (crimes[2] && crimes[2].currentTemplate) === 'BootleggingSellCD'

    expect(!activeFundsCrime && isSetupOnlineShop).toBeTruthy()
    expect(Component.find('Row3SetupStore').length).toBe(1)
    expect(Component.find('Row4Collect').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render only Copy, Sell and Collect subrimes', () => {
    const Component = shallow(<CrimesList {...setupOnlineStoreCompleted} />)
    const { crimes, additionalInfo } = Component.instance().props

    const activeFundsCrime = additionalInfo.progress === additionalInfo.maxProgress
    const isCollectFunds = (crimes[2] && crimes[2].currentTemplate) === 'BootleggingCollectFunds'

    expect(activeFundsCrime && isCollectFunds).toBeTruthy()
    expect(Component.find('Row3SetupStore').length).toBe(0)
    expect(Component.find('Row4Collect').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should replace SetupStore subrime on Collect subrime once the first one will be completed in real-time play', () => {
    let Component = mount(<CrimesList {...initialState} />)
    const {
      props: {
        crimes: sellCDsCrime,
        additionalInfo: sellCDsCrimeGlobalInfo
      }
    } = Component.instance()
    let activeFundsCrime = sellCDsCrimeGlobalInfo.progress === sellCDsCrimeGlobalInfo.maxProgress
    let isSetupOnlineShop = (sellCDsCrime[2] && sellCDsCrime[2].currentTemplate) === 'BootleggingSellCD'

    expect(!activeFundsCrime && isSetupOnlineShop).toBeTruthy()
    expect(Component.find('Row3SetupStore').length).toBe(1)
    expect(Component.find('Row4Collect').length).toBe(0)

    // re-mount our component with new Redux state and checks
    // if BootleggingCollectFunds will been replaced with BootleggingSellCD
    Component = mount(<CrimesList {...setupOnlineStoreCompleted} />)
    const {
      props: {
        crimes: collectFundsCrime,
        additionalInfo: collectFundsCrimeGlobalInfo
      }
    } = Component.instance()

    activeFundsCrime = collectFundsCrimeGlobalInfo.progress === collectFundsCrimeGlobalInfo.maxProgress
    isSetupOnlineShop = (collectFundsCrime[2] && collectFundsCrime[2].currentTemplate) === 'BootleggingCollectFunds'

    expect(activeFundsCrime && isSetupOnlineShop).toBeTruthy()
    expect(Component.find('Row3SetupStore').length).toBe(0)
    expect(Component.find('Row4Collect').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
})
