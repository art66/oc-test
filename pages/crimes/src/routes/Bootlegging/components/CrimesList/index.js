import { connect } from 'react-redux'
import CrimesList from './CrimesList'
import { closeOutcome, attempt, pooling } from '../../../../modules/actions'
import { breakSelectedCDs, toggleCDsSelector } from '../../modules/actions'
import getGlobalSelector from '../../../../utils/globalListPropsSelector'
import {
  setupOnlineStoreCompletedState,
  isSetupOnlineShopState,
  setupStoreClassState,
  collectFundsClassState,
  copiedCDsState,
  additionalInfoState,
  blankSelectedCDsState,
  tempCountUsedState,
  // blankCDsCountState,
  blankCDsState,
  CDsLeftState,
  progressState,
  maxProgressState,
  genresState,
  showCDsSelectorState
} from './selectors'

const mapStateToProps = ({ bootlegging, browser, common }) => ({
  ...getGlobalSelector(bootlegging, browser, common),
  additionalInfo: additionalInfoState(bootlegging),
  blankSelectedCDs: blankSelectedCDsState(bootlegging),
  tempCountUsed: tempCountUsedState(bootlegging),
  // blankCDsCount: blankCDsCountState(bootlegging),
  blankCDs: blankCDsState(bootlegging),
  cdSelected: CDsLeftState(bootlegging),
  progress: progressState(bootlegging),
  maxProgress: maxProgressState(bootlegging),
  genres: genresState(bootlegging),
  showCDsSelector: showCDsSelectorState(bootlegging),
  copiedCDs: copiedCDsState(bootlegging),
  setupOnlineStoreCompleted: setupOnlineStoreCompletedState(bootlegging),
  isSetupOnlineShop: isSetupOnlineShopState(bootlegging),
  setupStoreClass: setupStoreClassState(bootlegging),
  collectFundsClass: collectFundsClassState(bootlegging)
})

const mapDispatchToProps = dispatch => ({
  closeOutcome: () => dispatch(closeOutcome()),
  startAttempt: (itemId, url, isCurrentRow) => dispatch(attempt('bootlegging', itemId, url, isCurrentRow)),
  startToggleCDs: value => dispatch(toggleCDsSelector(value)),
  startBreakSelectedCDs: () => dispatch(breakSelectedCDs()),
  startPooling: () => dispatch(pooling('bootlegging'))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrimesList)
