import { createSelector } from 'reselect'
import bootleggingStyles from '../../../../styles/bootlegging.cssmodule.scss'

const getProgress = state => state.additionalInfo.progress
const getMaxProgress = state => state.additionalInfo.maxProgress
const getStoreCrime = state => state.crimesByType[2]
const getStoreCrimeTitle = state => state.crimesByType[2].currentTemplate
const getAdditionalInfo = state => state.additionalInfo
const getBlankSelectedCDs = state => state.blankSelectedCDs
// const getBlankCDsCount = state => state.crimesByType[0].additionalInfo.blankCDs
const getTempCountUsed = state => state.crimesByType[0].additionalInfo.tempCDsUsed
const getBlankCDs = state => state.crimesByType[0].additionalInfo.blankCDs
const getCDsLeft = state => state.crimesByType[0].additionalInfo.CDsLeft
const getGenres = state => state.genres
const getShowCDsSelector = state => state.showCDsSelector
// TODO: shold not be here!
// Migrate getCopiedCDs selectorHelper calculation into correspoding reducer
const getCopiedCDs = state => {
  const { CDs } = state.additionalInfo
  const copyedCDs = CDs && Object.keys(CDs).reduce((sum, i) => sum + CDs[i], 0)

  return copyedCDs
}

// reselect functions
export const additionalInfoState = createSelector([getAdditionalInfo], additionalInfo => additionalInfo)
export const blankSelectedCDsState = createSelector([getBlankSelectedCDs], blankSelectedCDs => blankSelectedCDs)
// export const blankCDsCountState = createSelector([getBlankCDsCount], blankCDsCount => blankCDsCount)
export const tempCountUsedState = createSelector([getTempCountUsed], tempCountUsed => {
  const newTempCountUsedState = {
    ...tempCountUsed
  }

  Object.keys(newTempCountUsedState || {}).forEach(itemKey => {
    newTempCountUsedState[itemKey] = `${newTempCountUsedState[itemKey]}`
  })

  return newTempCountUsedState
})
export const blankCDsState = createSelector([getBlankCDs], blankCDs => blankCDs)
export const CDsLeftState = createSelector([getCDsLeft], CDsLeft => CDsLeft)
export const progressState = createSelector([getProgress], progress => progress)
export const maxProgressState = createSelector([getMaxProgress], maxProgress => maxProgress)
export const genresState = createSelector([getGenres], genres => genres)
export const showCDsSelectorState = createSelector([getShowCDsSelector], showCDsSelector => showCDsSelector)

export const setupOnlineStoreCompletedState = createSelector(
  [getProgress, getMaxProgress],
  (progress, maxProgress) => progress === maxProgress && maxProgress !== undefined
)

export const isSetupOnlineShopState = createSelector(
  [getStoreCrime, getStoreCrimeTitle],
  (storeCrime, storeCrimeTitle) => storeCrime && storeCrimeTitle === 'BootleggingSellCD'
)
export const setupStoreClassState = createSelector(
  [setupOnlineStoreCompletedState],
  isStoreCompleted => (isStoreCompleted ? bootleggingStyles['store--wrap'] : '')
)
export const collectFundsClassState = createSelector(
  [setupOnlineStoreCompletedState],
  isStoreCompleted => (isStoreCompleted ? bootleggingStyles['funds--wrap'] : '')
)

export const copiedCDsState = createSelector([getCopiedCDs], copiedCD => copiedCD || null)
