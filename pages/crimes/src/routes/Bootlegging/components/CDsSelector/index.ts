import { connect } from 'react-redux'
import CDsSelector from './CDsSelector'
import { toggleCDsSelector, selectCDs, blankSelectedCDs } from '../../modules/actions'

const mapStateToProps = state => ({
  stat: state.bootlegging,
  countCDs: state.bootlegging.crimesByType[0].requirements.items[0].count,
  blankCDs: state.bootlegging.crimesByType[0].additionalInfo.blankCDs,
  items: state.bootlegging.crimesByType[0].requirements.items,
  mediaType: state.browser.mediaType,
  showCDsSelector: state.bootlegging.showCDsSelector
})

const mapDispatchToProps = dispatch => ({
  toggleDispatch: value => dispatch(toggleCDsSelector(value)),
  blankDispatch: value => dispatch(blankSelectedCDs(value)),
  selectCDsDispatch: (packID, countCDs, value) => dispatch(selectCDs(packID, countCDs, value))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CDsSelector)
