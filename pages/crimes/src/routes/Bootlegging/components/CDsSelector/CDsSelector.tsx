import React, { Component } from 'react'
import classnames from 'classnames'
import { toMoney } from '@torn/shared/utils'

import { Popup } from '../../../../components'
import { SELECTOR_POS_LEFT_BOOTLEGGING } from '../../constants'

import localStyles from './index.cssmodule.scss'
import styles from '../../../../styles/bootlegging.cssmodule.scss'

export interface IProps {
  items: {
    available: boolean
  }[]
  countCDs: number
  blankCDs: number
  mediaType: string
  showCDsSelector: boolean
  toggleDispatch: (status: boolean) => void
  blankDispatch: (status: boolean) => void
  selectCDsDispatch: (packID: number, capacity: number, value: number) => void
}

export class CDsSelector extends Component<IProps> {
  static defaultProps: IProps = {
    items: [],
    countCDs: 0,
    blankCDs: 0,
    mediaType: '',
    showCDsSelector: false,
    toggleDispatch: () => {},
    blankDispatch: () => {},
    selectCDsDispatch: () => {}
  }

  _classNameHolder = () => {
    const { items } = this.props

    const classesFilled = classnames({
      [styles.imageWrap]: true,
      [styles.imageWrapForCDs]: true,
      [styles.filledCDs]: items[0].available
    })

    const classesNonFilled = classnames({
      [styles.imageWrap]: true,
      [styles.imageWrapForCDs]: true,
      [styles.responsive]: true,
      [styles.nonFilledCDs]: true
    })

    return {
      classesFilled,
      classesNonFilled
    }
  }

  _close = () => {
    const { toggleDispatch } = this.props

    toggleDispatch(false)
  }

  _handleClick = e => {
    const { blankDispatch, selectCDsDispatch } = this.props

    const { id: packID, available: isAvailable, count: countCDs, capacity, value } = e.target.dataset

    if (!isAvailable || !this._checkCountsAvailable(countCDs)) return

    e.preventDefault()
    e.stopPropagation()

    selectCDsDispatch(Number(packID), Number(capacity), value)
    blankDispatch(true)
  }

  _checkCountsAvailable = countCDs => {
    const isCountAvaillable = countCDs > 0

    return isCountAvaillable
  }

  _getBlankCDsCount = countCDs => {
    const isCDsAvailable = this._checkCountsAvailable(countCDs)

    if (!isCDsAvailable) {
      return null
    }

    return <span className={styles.amountOfCDs}>{toMoney(countCDs)}</span>
  }

  _itemsSorter = items => {
    const itemsToSort = [...items]

    itemsToSort.sort((a, b) => {
      const [aSortValue = 1000] = a.label.match(/\d+$/) || []
      const [bSortValue = 1000] = b.label.match(/\d+$/) || []

      return Number(aSortValue) > Number(bSortValue) ? 1 : -1
    })

    return itemsToSort
  }

  _renderCDsCells = () => {
    const { items } = this.props
    const { classesFilled } = this._classNameHolder()

    const sortedItems = this._itemsSorter(items)

    const itemsToRender = sortedItems.map((item, index) => {
      const { available, label, value, blankCDsCount, count } = item

      // the last one is computer object, ommit it
      if (index <= 2) {
        const itemImgPath = available && this._checkCountsAvailable(count)

        const itemImgType = itemImgPath ? 'large_dark' : 'crimes_blank'

        return (
          <button
            key={label}
            data-id={index}
            data-available={available}
            data-count={count}
            data-capacity={blankCDsCount}
            data-value={value}
            type='button'
            className={classesFilled}
            onMouseDown={this._handleClick}
          >
            <img alt={item.label} src={`/images/items/${value}/${itemImgType}.png`} />
            {this._getBlankCDsCount(count)}
          </button>
        )
      }

      return null
    })

    return itemsToRender
  }

  render() {
    const { mediaType, showCDsSelector } = this.props

    if (!showCDsSelector) return null

    const pos = SELECTOR_POS_LEFT_BOOTLEGGING[mediaType]

    return (
      <Popup closePopUp={this._close} pos={pos}>
        <div className={`${styles.cdsSelector} ui-tooltip ui-widget ui-corner-all ui-widget-content white-tooltip`}>
          <div className={`p10 text-a-center ${styles.centerFlex}`}>{this._renderCDsCells()}</div>
          <hr />
          <div className='p10 text-a-center'>Select a Pack of Blank CDs</div>
          <div className={`tooltip-arrow left bottom ${localStyles.localArrow}`} />
        </div>
      </Popup>
    )
  }
}

export default CDsSelector
