const initialState = {
  items: [
    {
      available: true,
      label: 'Pack of CDs: 50'
    }
  ],
  pos: {
    top: 250,
    left: 42
  },
  crimesByType: [
    {
      additionalInfo: {
        blankCDs: 90
      },
      requirements: {
        items: [
          {
            available: false,
            count: 0,
            label: 'Pack of CDs: 50'
          }
        ]
      }
    },
    {},
    {}
  ],
  toggleDispatch: () => {},
  countCDs: 0,
  blankCDs: 0,
  showCDsSelector: false,
  blankDispatch: () => {},
  selectCDsDispatch: () => {},
  selectCDs: () => {},
  toggleCDsSelector: () => {},
  blankSelectedCDs: () => {},
  mediaType: 'desktop'
}

export const showCDsSelector = {
  ...initialState,
  items: [
    {
      available: false,
      label: 'Pack of CDs: 50'
    }
  ],
  showCDsSelector: true
}

export const isAvailable = {
  ...initialState,
  blankCDs: 90,
  crimesByType: [
    {
      additionalInfo: {
        blankCDs: 90
      },
      requirements: {
        items: [
          {
            available: true,
            count: 0,
            label: 'Pack of CDs: 50'
          }
        ]
      }
    }
  ],
  showCDsSelector: true
}

export const firstAndLastCDsAvailable = {
  ...initialState,
  items: [
    {
      available: true,
      count: 5,
      value: '44',
      label: 'Pack of CDs: 50'
    },
    {
      available: false,
      count: 0,
      value: '956',
      label: 'Pack of CDs: 100'
    },
    {
      available: true,
      count: 9,
      value: '957',
      label: 'Pack of CDs: 250'
    }
  ],
  showCDsSelector: true,
  crimesByType: [
    {
      additionalInfo: {
        blankCDs: 90
      },
      requirements: {
        items: [
          {
            available: true,
            count: 5,
            label: 'Pack of CDs: 50'
          }
        ]
      }
    }
  ]
}

export default initialState
