import React from 'react'
import { mount } from 'enzyme'
import initialState, { showCDsSelector, isAvailable, firstAndLastCDsAvailable } from './mocks/cdSelector'
import CDsSelector from '../CDsSelector'

describe('<CDsSelector />', () => {
  it('should non-render Popup', () => {
    const Component = mount(<CDsSelector {...initialState} />)

    expect(Component.find('Popup').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render Popup', () => {
    const Component = mount(<CDsSelector {...showCDsSelector} />)

    expect(Component.find('Popup').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should prodive crimes_blank image in case of empty count of CDs Pack', () => {
    const Component = mount(<CDsSelector {...showCDsSelector} />)
    const { available } = Component.instance().props.items[0]

    expect(available).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('should prodive regular CDs Pack image in case of some count in CDs Pack', () => {
    const Component = mount(<CDsSelector {...isAvailable} />)
    const { available } = Component.instance().props.items[0]

    expect(available).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should prodive crimes_blank image in case of unavailable of CDs Pack', () => {
    const Component = mount(<CDsSelector {...showCDsSelector} />)
    const { available } = Component.instance().props.items[0]

    expect(available).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('should prodive regular CDs Pack image in case of its availabitity', () => {
    const Component = mount(<CDsSelector {...isAvailable} />)
    const { available } = Component.instance().props.items[0]

    expect(available).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should prodive us with CDs count in placeholder', () => {
    const Component = mount(<CDsSelector {...isAvailable} />)
    const { items, blankCDs } = Component.instance().props

    expect(blankCDs && items[0].available).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should allow us to make a click and set CDs in placeholder if we have some CDs', () => {
    const Component = mount(<CDsSelector {...isAvailable} />)
    const { items } = Component.instance().props

    expect(items[0].available).toBeTruthy()
    expect(Component.find('.imageWrap.imageWrapForCDs.filledCDs').length).toBe(1)
    expect(Component.find('.imageWrap.imageWrapForCDs.filledCDs').simulate('click')).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should render CDsSelector with first and last CDs Pack in it, with the exception of the middle one', () => {
    const Component = mount(<CDsSelector {...firstAndLastCDsAvailable} />)
    const { items } = Component.instance().props

    expect(items[0].available).toBeTruthy()
    expect(items[1].available).toBeFalsy()
    expect(items[2].available).toBeTruthy()

    expect(Component.find('.imageWrap.imageWrapForCDs.filledCDs').length).toBe(3)
    expect(
      Component.find('.imageWrap.imageWrapForCDs.filledCDs')
        .at(0)
        .childAt(0)
        .prop('src')
    ).toBe('/images/items/44/large_dark.png')
    expect(
      Component.find('.imageWrap.imageWrapForCDs.filledCDs')
        .at(1)
        .childAt(0)
        .prop('src')
    ).toBe('/images/items/956/crimes_blank.png')
    expect(
      Component.find('.imageWrap.imageWrapForCDs.filledCDs')
        .at(2)
        .childAt(0)
        .prop('src')
    ).toBe('/images/items/957/large_dark.png')

    expect(Component).toMatchSnapshot()
  })
})
