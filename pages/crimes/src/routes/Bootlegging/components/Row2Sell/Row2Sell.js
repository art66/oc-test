import PropTypes from 'prop-types'
import React, { Component } from 'react'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'
import { CrimeRow } from '../../../../components'
import ownCrimeRowProps from './selectors'
import { bootleggingRow2SellUpdate } from '../../utils/shouldRowsUpdate'
import styles from '../../../../styles/bootlegging.cssmodule.scss'

export class Row2Sell extends Component {
  static propTypes = {
    attempt: PropTypes.func,
    showOutcome: PropTypes.bool,
    itemId: PropTypes.number,
    available: PropTypes.bool,
    copiedCDs: PropTypes.number,
    crimeID: PropTypes.number,
    iconClass: PropTypes.string,
    mediaType: PropTypes.string,
    nerve: PropTypes.number,
    outcome: PropTypes.object,
    isCurrentRow: PropTypes.bool,
    title: PropTypes.string.isRequired,
    userNerve: PropTypes.number,
    closeOutcome: PropTypes.func,
    lastSubCrimeID: PropTypes.number,
    currentRowID: PropTypes.number,
    state: PropTypes.string
  }

  static defaultProps = {
    attempt: () => {},
    showOutcome: false,
    itemId: 0,
    available: false,
    copiedCDs: 0,
    crimeID: 0,
    iconClass: '',
    mediaType: '',
    nerve: 0,
    outcome: {},
    isCurrentRow: false,
    userNerve: 0,
    closeOutcome: () => {},
    lastSubCrimeID: 0,
    currentRowID: 0,
    state: ''
  }

  shouldComponentUpdate(nextProps, nextState) {
    const rowCheck = bootleggingRow2SellUpdate(this.props, nextProps)
    const globalCheck = globalCrimeRowCheck(this.props, nextProps)

    return globalCheck || rowCheck || false
  }

  _getDesktopLayout = () => {
    const { mediaType } = this.props

    if (mediaType !== 'desktop') return null

    return <div className={`${styles.inner_cell_1} ${styles.dlm} ${styles.nonBorder}`} />
  }

  render() {
    const { throwGlobalCrimeRowProps: props } = ownCrimeRowProps(this.props)

    return (
      <CrimeRow {...props} rowShouldUpdate={bootleggingRow2SellUpdate}>
        {this._getDesktopLayout()}
        <div className={`${styles.inner_cell_2} ${styles.dlm} ${styles.nonBorder}`} />
      </CrimeRow>
    )
  }
}

export default Row2Sell
