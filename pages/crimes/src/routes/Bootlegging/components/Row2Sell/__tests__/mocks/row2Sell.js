export default {
  attempt: () => {},
  isRowActive: false,
  jailed: false,
  showOutcome: true,
  itemId: 0,
  available: false,
  copiedCDs: 0,
  crimeID: 2,
  iconClass: '',
  mediaType: 'desktop',
  nerve: 0,
  outcome: {},
  isCurrentRow: false,
  title: '',
  user: {
    nerve: 0
  },
  closeOutcome: () => {},
  lastSubCrimeID: 0,
  currentRowID: 2,
  state: ''
}

export const attemptButtonActive = {
  user: {
    nerve: 1
  },
  copiedCDs: 2,
  available: true,
  isRowActive: true,
  showOutcome: true
}
