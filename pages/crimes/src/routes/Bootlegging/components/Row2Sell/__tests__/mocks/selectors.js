const initialState = {
  attempt: () => {},
  showOutcome: true,
  itemId: 0,
  available: true,
  copiedCDs: 0,
  crimeID: 2,
  iconClass: '',
  mediaType: 'desktop',
  nerve: 0,
  outcome: {},
  isCurrentRow: false,
  title: '',
  user: {
    nerve: 0
  },
  closeOutcome: () => {},
  lastSubCrimeID: 0,
  currentRowID: 2,
  state: ''
}

export const disabledAttemptButton = {
  ...initialState,
  available: false
}

export const jailed = {
  ...initialState,
  jailed: true
}

export default initialState
