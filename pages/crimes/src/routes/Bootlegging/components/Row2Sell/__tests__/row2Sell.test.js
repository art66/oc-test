import React from 'react'
import { shallow } from 'enzyme'
import initialState, { attemptButtonActive } from './mocks/row2Sell'
import Row2Sell from '../Row2Sell'

describe('<Row2Sell />', () => {
  it('should render the row', () => {
    const Component = shallow(<Row2Sell {...initialState} />)

    expect(Component.find('CrimeRow').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render the 1st cell in the desktop mode', () => {
    const Component = shallow(<Row2Sell {...initialState} />)

    expect(Component.find('.inner_cell_1.dlm').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('attempt button should be disabled', () => {
    const Component = shallow(<Row2Sell {...initialState} />)
    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('attempt button should be active', () => {
    const buttonIsActive = Object.assign(initialState, attemptButtonActive)
    const Component = shallow(<Row2Sell {...buttonIsActive} />)

    const buttonIsDisabled = Component.instance().render().props.button.disabled

    expect(buttonIsDisabled).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('row should have \'active\' className if clicked his attempt button', () => {
    const Component = shallow(<Row2Sell {...initialState} />)
    const buttonIsDisabled = Component.instance().render().props.className

    expect(buttonIsDisabled).toEqual('active')
    expect(Component).toMatchSnapshot()
  })
})
