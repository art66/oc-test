import initialState, { disabledAttemptButton, jailed } from './mocks/selectors'
import ownCrimeRowProps from '../selectors'

describe('Row2Sell selector', () => {
  it('should return whole crimeRow info in the global crimeRow with active button', () => {
    const { throwGlobalCrimeRowProps } = ownCrimeRowProps(initialState)

    expect(typeof throwGlobalCrimeRowProps === 'object').toBeTruthy()
  })
  it('should return locked button in case of jailed status', () => {
    const { throwGlobalCrimeRowProps } = ownCrimeRowProps(jailed)

    expect(throwGlobalCrimeRowProps.button.disabled).toBeTruthy()
  })
  it('should return whole crimeRow info in the global crimeRow with disabled button', () => {
    const { throwGlobalCrimeRowProps } = ownCrimeRowProps(disabledAttemptButton)

    expect(typeof throwGlobalCrimeRowProps === 'object').toBeTruthy()
  })
})
