import { createSelector } from 'reselect'
import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import { getIconClass, getUserNerve, getMediaType, getTitle } from '../../../../utils/globalSelectorsHelpers'

import styles from '../../../../styles/bootlegging.cssmodule.scss'

const DISABLE_BORDER = false
const BUTTON_LABEL = 'Initiate'

const getCopiedCDs = props => props.copiedCDs
const getNerve = props => props.nerve

// reselect functions
const rowIcon = createSelector([getIconClass], iconClass => styles[iconClass])
const disableButtonState = createSelector([getUserNerve, getNerve, getCopiedCDs], (...props) => {
  const [userNerve, nerve, copiedCDs] = props
  const isButtonDisabled = userNerve < nerve || copiedCDs < 1

  return isButtonDisabled
})
const rowTitle = createSelector([getMediaType, getTitle], (mediaType, title) => {
  if (mediaType === 'mobile') {
    return 'Sell CDs'
  }
  return title
})

const globalCrimeRowProps = createSelector(
  [globalRowPropsSelector, rowIcon, getCopiedCDs, disableButtonState, rowTitle],
  (...props) => {
    const [globalProps, iconClass, copiedCDs, isButtonDisabled, title] = props

    return {
      ...globalProps,
      button: {
        ...globalProps.button,
        label: BUTTON_LABEL,
        disabled: globalProps.button.disabled || isButtonDisabled
      },
      nonBorder: DISABLE_BORDER,
      copiedCDs,
      title,
      iconClass,
      styles
    }
  }
)

const ownCrimeRowProps = props => ({
  throwGlobalCrimeRowProps: globalCrimeRowProps(props)
})

export default ownCrimeRowProps
