import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import classnames from 'classnames'

import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

import styles from './index.cssmodule.scss'
import './styles.scss'
import './anim.scss'

export class Cd extends Component {
  static propTypes = {
    genre: PropTypes.string,
    queued: PropTypes.number,
    total: PropTypes.number,
    progress: PropTypes.number,
    timeForCD: PropTypes.number,
    timeLeftForNextCD: PropTypes.number
  }

  static defaultProps = {
    genre: '',
    queued: 0,
    total: 0,
    progress: 0,
    timeForCD: 0,
    timeLeftForNextCD: 0
  }

  constructor(props) {
    super(props)

    this.state = {
      totalCDs: 0,
      sellStatus: false,
      shouldDecrement: false
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // should update CD count once it change from Back-end
    if (nextProps.total >= prevState.totalCDs) {
      return { ...prevState, totalCDs: nextProps.total }
      // should decrement CD count if nextProps.total less then prevState.totalCDs
    } else if (nextProps.total < prevState.totalCDs) {
      return { ...prevState, totalCDs: nextProps.total, shouldDecrement: true }
    }

    return prevState
  }

  componentDidMount() {
    this._subscribeTooltip('subscribe')
  }

  shouldComponentUpdate(nextProps, prevState) {
    const { progress, queued } = this.props
    const { shouldDecrement, totalCDs } = this.state

    const quiueChanged = queued !== nextProps.queued
    const progressChanged = progress !== nextProps.progress
    const statusChanged = shouldDecrement !== prevState.shouldDecrement
    const totalAmountChanged = totalCDs !== prevState.totalCDs

    // if some amount of CD is changed
    if (quiueChanged || progressChanged || statusChanged || totalAmountChanged) {
      return true
    }

    return false
  }

  componentDidUpdate() {
    const { shouldDecrement } = this.state

    this._subscribeTooltip('update')

    if (shouldDecrement) {
      setTimeout(() => {
        this.setState(prevState => ({
          ...prevState,
          shouldDecrement: false
        }))
      }, 2000)
    }
  }

  _calculateAnimProgress = () => {
    const { timeForCD, timeLeftForNextCD } = this.props
    const { shouldDecrement } = this.state

    const animFrequency = Math.ceil(timeForCD - (timeForCD * 95) / 100)

    const copyDecrement = shouldDecrement
    const copyWaiting = timeLeftForNextCD - animFrequency >= 0
    // const copySuccess = timeLeftForNextCD <= animFrequency

    const copyingAfterStart = timeForCD - timeLeftForNextCD >= animFrequency
    const copyingBeforeFinish = timeLeftForNextCD > animFrequency
    const disableDiskBloom = copyingAfterStart && copyingBeforeFinish

    return {
      copyDecrement,
      copyWaiting,
      // copySuccess,
      disableDiskBloom
    }
  }

  _classNameHolder = () => {
    const { genre } = this.props

    const { copyDecrement, disableDiskBloom } = this._calculateAnimProgress()

    const progressClass = classnames({
      'disk-box--waiting': !this._checkQueueEmpty(),
      'disk-box--decremental': copyDecrement
      // 'disk-box--waiting': progress && copySuccess
    })

    const imageDiskClass = classnames({
      [genre.toLowerCase()]: true,
      'disk-image-large': true,
      'disk-disable-bloom': disableDiskBloom
    })

    const queueClass = classnames({
      [styles.isCopying]: !this._checkQueueEmpty()
    })

    return {
      progressClass,
      imageDiskClass,
      queueClass
    }
  }

  _checkQueueEmpty = () => {
    const { queued, progress } = this.props

    return queued === 0 || progress === null || false
  }

  _injectTooltip = (payload, isTooltipAlreadyiIn, stage) => {
    if (!isTooltipAlreadyiIn || stage === 'subscribe') {
      tooltipSubscriber.subscribe(payload)
    } else if (stage === 'update') {
      tooltipSubscriber.update(payload)
    }
  }

  _subscribeTooltip = (stage = 'subscribe') => {
    const { genre, timeLeftForNextCD } = this.props

    const tooltipFinded = tooltipSubscriber.getTargets().find(tooltip => tooltip.ID === genre)

    if (this._checkQueueEmpty()) {
      if (tooltipFinded) {
        tooltipSubscriber.unsubscribe(tooltipFinded)
      }

      return
    }

    const payload = {
      child: `${timeLeftForNextCD || 0} seconds left`,
      ID: genre
    }

    this._injectTooltip(payload, tooltipFinded, stage)
  }

  _renderQueueAnimation = (children, animName, classes) => {
    const { queueClass } = this._classNameHolder()
    const [wrapClass, childClass] = classes

    return (
      <TransitionGroup className={wrapClass}>
        <CSSTransition component='div' key={children} timeout={400} classNames={animName}>
          <span className={`${childClass} ${queueClass}`}>{children}</span>
        </CSSTransition>
      </TransitionGroup>
    )
  }

  _renderDisksLeft = () => {
    const { queued } = this.props
    const classes = [styles.queueCountWrap, styles.queueCount]

    return this._renderQueueAnimation(queued, 'slide-number--queue', classes)
  }

  _renderDisksText = () => {
    const currentStage = this._checkQueueEmpty() ? 'queued' : 'copying'
    const classes = [styles.queueTextWrap, styles.queueText]

    return this._renderQueueAnimation(currentStage, 'slide-number--text', classes)
  }

  _renderDisksTotal = () => {
    const { total } = this.props

    if (this._checkQueueEmpty() && total === 0) {
      return (
        <div className='slide-number-wrap'>
          <span className='total total--empty'>{0}</span>
        </div>
      )
    }

    return (
      <TransitionGroup className='slide-number-wrap'>
        <CSSTransition component='div' key={total} classNames='slide-number--total' timeout={400}>
          <span className={`total ${!total ? 'total--empty' : ''}`}>{total}</span>
        </CSSTransition>
      </TransitionGroup>
    )
  }

  _getBarLineStyles = () => {
    const { progress } = this.props

    const inlineStyles = {
      width: `${progress || 0}%`
    }

    if (progress < 5) {
      inlineStyles.transition = 'none'
    }

    return inlineStyles
  }

  render() {
    const { genre, total } = this.props

    const { progressClass, imageDiskClass } = this._classNameHolder()

    return (
      <li className={`disk-box ${progressClass}`}>
        <div className={styles.topSection}>
          <span className={`category ${total && styles.haveDisks}`}>{genre}</span>
          <i className={imageDiskClass} />
          <div className={styles.queueWrap}>
            {this._renderDisksLeft()}
            {this._renderDisksText()}
          </div>
        </div>
        <div className={styles.bottomSection}>
          <div id={genre} className='progress-bar'>
            <div className='bar-line' style={this._getBarLineStyles()} />
          </div>
          <div className='quantity'>{this._renderDisksTotal()}</div>
        </div>
      </li>
    )
  }
}

export default Cd
