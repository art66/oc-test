import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { removeCdFromQueue, setTimeForNextCd, cdQueueEmpty } from '../../modules/actions'
import { Cd } from './Cd'
import './styles.scss'

export class CopyProgress extends Component {
  componentDidMount() {
    const {
      copyProgress: { timeLeftForNextCD }
    } = this.props

    if (timeLeftForNextCD) {
      this._runTimer(timeLeftForNextCD)
    }
  }

  componentDidUpdate() {
    const {
      copyProgress: { timeLeftForNextCD }
    } = this.props

    if (timeLeftForNextCD) {
      this._clearTimer(this.timerId)
      this._runTimer(timeLeftForNextCD)
    }
  }

  componentWillUnmount() {
    this._clearTimer(this.timerId)
  }

  _reorderQueue = queue => {
    const queuedCDs = Array(8).fill(0)

    if (queue) {
      queue.map(genre => queuedCDs[genre - 1]++)
    }

    return queuedCDs
  }

  _updateCDsList() {
    const {
      startUpdateCDsList,
      copiedCDs,
      copyProgress: { currentQueue }
    } = this.props

    if (!currentQueue) return

    const genreToUpdate = parseInt(currentQueue[0], 10)

    const updatedCopiedCDs = {
      ...copiedCDs,
      [genreToUpdate]: (copiedCDs[genreToUpdate] || 0) + 1
    }

    startUpdateCDsList(updatedCopiedCDs)
  }

  _runTimer = timeLeftForNextCD => {
    const { startSetTimeForNextCd, copyProgress, makeCDsQueueEmpty } = this.props
    const { currentQueue, timeForCD } = copyProgress

    this.timerId = setInterval(() => {
      timeLeftForNextCD--
      if (timeLeftForNextCD <= 0) {
        if (!currentQueue || Object.keys(currentQueue).length <= 1) {
          makeCDsQueueEmpty()

          this._clearTimer(this.timerId)
        } else {
          timeLeftForNextCD = timeForCD
        }
        this._updateCDsList()
      }
      startSetTimeForNextCd(timeLeftForNextCD)
    }, 1000)
  }

  _clearTimer = timerId => {
    clearInterval(timerId)
  }

  _buildCdList = () => {
    const { copyProgress, genres, copiedCDs } = this.props
    const { timeForCD, currentQueue, timeLeftForNextCD } = copyProgress
    const queuedCDs = this._reorderQueue(currentQueue)
    const copiedCDsNull = 0
    const cdsList = []
    const genreCurrentlyInProgress = currentQueue ? currentQueue[0] : null
    const calcCDsTime = (1 - timeLeftForNextCD / timeForCD) * 100
    const cdProgress = currentQueue ? calcCDsTime : null

    Object.keys(genres).map(genre => {
      const totalCDs = copiedCDs === null ? copiedCDsNull : copiedCDs[genre]
      const progress = parseInt(genreCurrentlyInProgress, 10) === parseInt(genre, 10) ? cdProgress : null

      return cdsList.push(
        <Cd
          key={genre}
          genre={genres[genre]}
          queued={queuedCDs[genre - 1]}
          timeForCD={timeForCD}
          timeLeftForNextCD={timeLeftForNextCD}
          total={totalCDs}
          progress={progress}
        />
      )
    })

    return cdsList
  }

  render() {
    return <ul className='disks-panel clearfix'>{this._buildCdList()}</ul>
  }
}

CopyProgress.propTypes = {
  genres: PropTypes.object,
  copiedCDs: PropTypes.object,
  copyProgress: PropTypes.object,
  makeCDsQueueEmpty: PropTypes.func,
  startUpdateCDsList: PropTypes.func,
  startSetTimeForNextCd: PropTypes.func
}

CopyProgress.defaultProps = {
  genres: {},
  copiedCDs: {},
  copyProgress: {},
  makeCDsQueueEmpty: () => {},
  startUpdateCDsList: () => {},
  startSetTimeForNextCd: () => {}
}

const mapStateToProps = state => ({
  copyProgress: state.bootlegging.crimesByType[0].additionalInfo,
  copiedCDs: state.bootlegging.additionalInfo.CDs,
  genres: state.bootlegging.genres
})

const mapDispatchToProps = dispatch => ({
  startUpdateCDsList: copiedCDs => dispatch(removeCdFromQueue(copiedCDs)),
  startSetTimeForNextCd: timeLeftForNextCD => dispatch(setTimeForNextCd(timeLeftForNextCD)),
  makeCDsQueueEmpty: () => dispatch(cdQueueEmpty())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CopyProgress)
