const initialState = {
  copyProgress: {
    timeForCD: 0,
    currentQueue: [1, 1, 1, 1],
    timeLeftForNextCD: 20
  },
  copiedCDs: {
    1: 21,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0
  },
  additionalInfo: {
    CDs: {
      1: 0,
      2: 0,
      3: 0,
      4: 0,
      5: 0,
      6: 0,
      7: 0,
      8: 0
    }
  },
  genres: {
    1: 'Pop',
    2: 'Rock',
    3: 'Urban',
    4: 'Country',
    5: 'Classical',
    6: 'Dance',
    7: 'Rap',
    8: 'Jazz'
  },
  startSetTimeForNextCd: () => {}
}

export default initialState
