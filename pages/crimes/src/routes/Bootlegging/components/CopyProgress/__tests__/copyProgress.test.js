import React from 'react'
import { mount } from 'enzyme'
import initialState from './mocks/copyProgress'
import { CopyProgress } from '..'

describe('<CopyProgress />', () => {
  it('should render CopyProgress container', () => {
    const Component = mount(<CopyProgress {...initialState} />)

    expect(Component.find('.disks-panel.clearfix').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render 8 CDs genres', () => {
    const Component = mount(<CopyProgress {...initialState} />)
    const cdsLength = Component.instance()._buildCdList().length

    expect(cdsLength).toBe(8)
    expect(Component.find('.disks-panel.clearfix').length).toBe(1)
    expect(Component.find('.disk-box').length).toBe(8)
    expect(Component).toMatchSnapshot()
  })
  it('should return reordered CDs queue', () => {
    const Component = mount(<CopyProgress {...initialState} />)
    const reorderedQueue = Component.instance()._reorderQueue()

    expect(reorderedQueue.length).toBe(8)
    expect(Component).toMatchSnapshot()
  })
  it('should update CDs List', () => {
    let CDsStore = {}

    const startUpdateCDsList = jest.fn(res => {
      CDsStore = res
    })

    const Component = mount(<CopyProgress {...initialState} startUpdateCDsList={startUpdateCDsList} />)

    const copiedCDsList = Component.instance().props.copiedCDs

    CDsStore = copiedCDsList
    expect(CDsStore[1]).toBe(21)

    // must update current CDs list
    Component.instance()._updateCDsList()

    expect(CDsStore[1]).toBe(22)
    expect(Component).toMatchSnapshot()
  })
  it('should start countdown timer', () => {
    const Component = mount(<CopyProgress {...initialState} />)
    const timer = Component.instance()._runTimer

    jest.useFakeTimers()
    timer()

    expect(setInterval).toHaveBeenCalledTimes(1)
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 1000)

    expect(Component).toMatchSnapshot()
  })
  it('should clear countdown timer', () => {
    const Component = mount(<CopyProgress {...initialState} />)
    const clearTimer = Component.instance()._clearTimer

    jest.useFakeTimers()
    clearTimer()

    expect(clearInterval).toHaveBeenCalledTimes(1)
    expect(Component).toMatchSnapshot()
  })
})
