const initialState = {
  genre: 'Rock',
  queued: 0,
  total: 11,
  progress: 0
}

export const copyDecrement = {
  ...initialState,
  queued: 5,
  total: 10,
  progress: 0,
  timeForCD: 38,
  timeLeftForNextCD: 36
}

export const copyWaiting = {
  ...initialState,
  queued: 5,
  progress: 90,
  timeForCD: 38,
  timeLeftForNextCD: 30
}

export const copySuccess = {
  ...initialState,
  queued: 5,
  progress: 99,
  timeForCD: 38,
  timeLeftForNextCD: 1
}

export default initialState
