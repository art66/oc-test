import React from 'react'
import { mount } from 'enzyme'
import initialState, { copyWaiting, copyDecrement } from './mocks/cd'
import Cd from '../Cd'

describe('<CD />', () => {
  it('should render CD container without effects', () => {
    const Component = mount(<Cd {...initialState} />)

    expect(Component.find('.disk-box').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render CD copyWaiting effects', () => {
    const Component = mount(<Cd {...copyWaiting} />)

    expect(Component.find('.disk-box').length).toBe(1)
    expect(Component.find('.disk-box--success').length).toBe(0)
    expect(Component.find('.disk-box--decremental').length).toBe(0)
    expect(Component.find('.disk-box--waiting').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render CD copyDecrement effects while disk in progress', () => {
    const Component = mount(<Cd {...copyDecrement} />)

    Component.setProps({ total: 8 })

    expect(Component).toMatchSnapshot()

    expect(Component.find('.disk-box').length).toBe(1)
    expect(Component.find('.disk-box--success').length).toBe(0)
    expect(Component.find('.disk-box--waiting').length).toBe(1)
    expect(Component.find('.disk-box--decremental').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
})
