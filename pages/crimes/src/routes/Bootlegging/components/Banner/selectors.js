// ------------------------------
// @param currently deprecated
// This functional is not required at all. Maybe we'll need this in future.
// ------------------------------
import { createSelector } from 'reselect'
import { getDesktopManualLayout } from '../../../../utils/globalSelectorsHelpers'

const getMediaTypeDesktop = props => props.screen.is.desktop
const getMediaTypeTablet = props => props.screen.is.tablet
const getMediaTypeMobile = props => props.screen.is.mobile

const isMain = createSelector(
  [getMediaTypeDesktop, getDesktopManualLayout],
  (desktop, isDesktopLayoutSetted) => desktop || isDesktopLayoutSetted
)
const isTablet = createSelector(
  [getMediaTypeDesktop, getMediaTypeTablet, getMediaTypeMobile, getDesktopManualLayout],
  (desktop, tablet, mobile, isDesktopLayoutSetted) => !isDesktopLayoutSetted && !desktop && !mobile && tablet
)
const isMobile = createSelector(
  [getMediaTypeDesktop, getMediaTypeTablet, getMediaTypeMobile, getDesktopManualLayout],
  (desktop, tablet, mobile, isDesktopLayoutSetted) => !isDesktopLayoutSetted && !desktop && !tablet && mobile
)

const bannerSelectorsProps = props => ({
  isMain: isMain(props),
  isTablet: isTablet(props),
  isMobile: isMobile(props)
})

export default bannerSelectorsProps
