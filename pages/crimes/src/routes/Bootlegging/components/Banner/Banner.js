import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { showDropDownPanel, toggleArrows } from '../../../../modules/actions'
import BannerGlobal from '../../../../components/Banner'

import styles from './anim.cssmodule.scss'

const SPINNER_TIMING = '.103s linear 2s infinite'
const SPINNER_ANIMATION_START = '2s cubic-bezier(0,.01,.85,.04) 0s normal'
const SPINNER_ANIMATION_RUNNING = '.103s linear 0s infinite'
const SPINNER_ANIMTAION_FINISH = '2s cubic-bezier(0,.01,.85,.04) 0s reverse'

const TENDRIL_TIMING = '3s cubic-bezier(.1,.78,.77,.23) 1s infinite'
const TENDRIL_ANIMATION_START = '1s cubic-bezier(0,.01,.85,.04) 0s normal'
const TENDRIL_ANIMATION_RUNNING = '3s cubic-bezier(.1,.78,.77,.23) 0s infinite'
const TENDRIL_ANIMATION_FINISH = '1s cubic-bezier(0,.01,.85,.04) 0s normal'

export class Banner extends Component {
  static propTypes = {
    currentQueue: PropTypes.array,
    dropDownPanelToShow: PropTypes.func,
    toggleArrow: PropTypes.func,
    screen: PropTypes.object,
    isDesktopLayoutSetted: PropTypes.bool,
    СDsBootStage: PropTypes.string
  }

  static defaultProps = {
    currentQueue: null,
    isDesktopLayoutSetted: false,
    СDsBootStage: '',
    dropDownPanelToShow: () => {},
    toggleArrow: () => {},
    screen: {}
  }

  shouldComponentUpdate(nextProps) {
    const { screen, currentQueue, isDesktopLayoutSetted } = this.props
    const isQueueChanged = currentQueue !== nextProps.currentQueue
    const isScreenChanged = screen.mediaType !== nextProps.screen.mediaType
    const isManualScreenChanged = isDesktopLayoutSetted !== nextProps.isDesktopLayoutSetted

    if (isScreenChanged || isQueueChanged || isManualScreenChanged) {
      return true
    }

    return false
  }

  _animationsBreakPoints = () => {
    return {
      animationSpinnerStart: `${styles.startSpinnerRotator} ${SPINNER_ANIMATION_START},
       ${styles.runningRotator} ${SPINNER_TIMING}`,
      animationSpinnerInProgress: `${styles.runningRotator} ${SPINNER_ANIMATION_RUNNING}`,
      animationSpinnerFinish: `${styles.finishSpinnerRotator} ${SPINNER_ANIMTAION_FINISH}`,
      animationTendrilStart: `${styles.startTendril} ${TENDRIL_ANIMATION_START},
       ${styles.runningTendril} ${TENDRIL_TIMING}`,
      animationTendrilInProgress: `${styles.runningTendril} ${TENDRIL_ANIMATION_RUNNING}`,
      animationTendrilFinish: `${styles.finishTendril} ${TENDRIL_ANIMATION_FINISH}`
    }
  }

  _getAnimation = () => {
    const { СDsBootStage } = this.props

    const {
      animationSpinnerStart,
      animationSpinnerInProgress,
      animationSpinnerFinish,
      animationTendrilStart,
      animationTendrilInProgress,
      animationTendrilFinish
    } = this._animationsBreakPoints()

    const animPhases = {
      started: {
        spinnerAnimation: animationSpinnerStart,
        tendrilAnimation: animationTendrilStart
      },
      progress: {
        spinnerAnimation: animationSpinnerInProgress,
        tendrilAnimation: animationTendrilInProgress
      },
      finished: {
        spinnerAnimation: animationSpinnerFinish,
        tendrilAnimation: animationTendrilFinish
      }
    }

    return animPhases[СDsBootStage] || {}
  }

  _bannerMain = () => {
    const { spinnerAnimation = '', tendrilAnimation = '' } = this._getAnimation()

    return (
      <div className={styles.bannerContainerMain}>
        <div className={styles.bannerBackground} />
        <div className={styles.diskWrap}>
          <div className={styles.wholeDisk} />
          <div className={styles.roundCircle} style={{ animation: spinnerAnimation }} />
          <div className={styles.centerSpinner} style={{ animation: spinnerAnimation }} />
        </div>
        <div className={styles.tendrilWrap}>
          <div className={styles.tendrilDriver} style={{ animation: tendrilAnimation }} />
          <div className={styles.tendrilHolder} />
        </div>
        <div className={styles.diskHover} />
      </div>
    )
  }

  render() {
    const { dropDownPanelToShow, toggleArrow } = this.props

    return (
      <BannerGlobal
        customClass={styles.bannerContainer}
        dropDownPanelToShow={dropDownPanelToShow}
        toggleArrows={toggleArrow}
      >
        {this._bannerMain()}
      </BannerGlobal>
    )
  }
}

const mapStateToProps = state => ({
  isDesktopLayoutSetted: state.common.isDesktopLayoutSetted
})

const mapDispatchToProps = dispatch => ({
  dropDownPanelToShow: () => dispatch(showDropDownPanel()),
  toggleArrow: () => dispatch(toggleArrows())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Banner)
