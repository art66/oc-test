const initialState = {
  screen: {
    is: {
      desktop: true,
      tablet: false,
      mobile: false
    }
  }
}

export const animationStarts = {
  screen: {
    ...initialState.screen
  },
  СDsBootStage: 'started'
}

export const animationRunning = {
  screen: {
    ...initialState.screen
  },
  СDsBootStage: 'progress'
}

export const animationStops = {
  screen: {
    ...initialState.screen
  },
  СDsBootStage: 'finished'
}

export const noAnimation = {
  screen: {
    ...initialState.screen
  },
  СDsBootStage: 'hold'
}

export const desktopMode = {
  screen: {
    is: {
      desktop: true,
      tablet: false,
      mobile: false
    },
    breakpoints: {
      ...initialState.screen.breakpoints
    }
  }
}

export default initialState
