import React from 'react'
import { shallow, mount } from 'enzyme'
import configureStore from 'redux-mock-store'
import initialState, {
  animationStarts,
  animationRunning,
  animationStops,
  noAnimation,
  desktopMode
} from './mocks/banner'
import { Banner } from '../Banner'

describe('<BannersCSSAnim />', () => {
  const mockStore = configureStore()

  it('should render Banner', () => {
    const store = mockStore(initialState)
    const Component = mount(<Banner store={store} {...store.getState()} />)

    expect(Component.find('.bannerContainer').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('animation should start running', () => {
    const Component = shallow(<Banner {...animationStarts} />)
    const { spinnerAnimation, tendrilAnimation } = Component.instance()._getAnimation()

    expect(spinnerAnimation).toBeTruthy()
    expect(tendrilAnimation).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('animation should running', () => {
    const Component = shallow(<Banner {...animationRunning} />)
    const { spinnerAnimation, tendrilAnimation } = Component.instance()._getAnimation()

    expect(spinnerAnimation).toBeTruthy()
    expect(tendrilAnimation).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('animation should finish', () => {
    const Component = shallow(<Banner {...animationStops} />)
    const { spinnerAnimation, tendrilAnimation } = Component.instance()._getAnimation()

    expect(spinnerAnimation).toBeTruthy()
    expect(tendrilAnimation).toBeTruthy()
  })
  it('animation should not running', () => {
    const Component = shallow(<Banner {...noAnimation} />)
    const { spinnerAnimation, tendrilAnimation } = Component.instance()._getAnimation()

    expect(spinnerAnimation).toBeFalsy()
    expect(tendrilAnimation).toBeFalsy()
  })
  it('should display animation banner for desktops', () => {
    const Component = shallow(<Banner {...desktopMode} />)

    expect(Component.find('.bannerContainerMain').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })

  // ---------------------------
  // DEPRECATED
  // ---------------------------
  // it('should display banner placeholder for tablets', () => {
  //   const Component = shallow(<Banner {...tabletMode} />)
  //   const { is } = Component.instance().props.screen
  //   const isTablet = !is.mobile && is.tablet && !is.desktop

  //   expect(isTablet).toBeTruthy()
  //   expect(Component.find('.bannerContainerMain').length).toBe(0)
  //   expect(Component.find('.bannerContainerTablet').length).toBe(1)
  //   expect(Component).toMatchSnapshot()
  // })
  // it('should display banner placeholder for mobiles', () => {
  //   const Component = shallow(<Banner {...mobileMode} />)
  //   const { is } = Component.instance().props.screen

  //   expect(is.mobile).toBeTruthy()
  //   expect(is.tablet).toBeFalsy()
  //   expect(is.desktop).toBeFalsy()

  //   expect(Component.find('.bannerContainerMain').length).toBe(0)
  //   expect(Component.find('.bannerContainerMobile').length).toBe(1)
  //   expect(Component).toMatchSnapshot()
  // })
})
