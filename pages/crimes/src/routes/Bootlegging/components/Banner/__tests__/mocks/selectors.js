const initialState = {
  screen: {
    is: {
      desktop: true,
      tablet: false,
      mobile: false
    }
  }
}

export const tabletLayout = {
  screen: {
    is: {
      desktop: false,
      tablet: true,
      mobile: false
    }
  }
}

export const mobileLayout = {
  screen: {
    is: {
      desktop: false,
      tablet: false,
      mobile: true
    }
  }
}

export default initialState
