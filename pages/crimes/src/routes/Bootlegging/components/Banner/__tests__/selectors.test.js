import initialState, { tabletLayout, mobileLayout } from './mocks/selectors'
import bannerSelectorsProps from '../selectors'

describe('Banner selector', () => {
  it('should return Desktop version', () => {
    const { isMain, isTablet, isMobile } = bannerSelectorsProps(initialState)

    expect(isMain).toBeTruthy()
    expect(isTablet).toBeFalsy()
    expect(isMobile).toBeFalsy()
  })
  it('should return Tablet version', () => {
    const { isMain, isTablet, isMobile } = bannerSelectorsProps(tabletLayout)

    expect(isMain).toBeFalsy()
    expect(isTablet).toBeTruthy()
    expect(isMobile).toBeFalsy()
  })
  it('should return Mobile version', () => {
    const { isMain, isTablet, isMobile } = bannerSelectorsProps(mobileLayout)

    expect(isMain).toBeFalsy()
    expect(isTablet).toBeFalsy()
    expect(isMobile).toBeTruthy()
  })
})
