const ROW_COPY_ID = 8
const ROW_COLLECT_ID = 11

const bootleggingRow1CopyUpdate = (prevProps, nextProps) => {
  const { crimeID, isCDSelected, blankCDs, genre } = prevProps
  const isCDsChange = isCDSelected !== nextProps.isCDSelected
  const isGenreChange = genre !== nextProps.genre
  const isCdsCountChanged = blankCDs !== nextProps.blankCDs

  // update Row1Copy row once user put CDs pack in row or chage Music genre [BOOTLEGGING CRIME]
  if (crimeID === ROW_COPY_ID && (isCDsChange || isGenreChange || isCdsCountChanged)) {
    return true
  }
}

const bootleggingRow2SellUpdate = (prevProps, nextProps) => {
  const { copiedCDs } = nextProps

  if (prevProps.copiedCDs !== copiedCDs) return true
}

const bootleggingRow23SetupStore = (prevProps, nextProps) => {
  const { barProgress, available } = prevProps

  // check if row availability is changed
  if (available !== nextProps.available) {
    return true
  }

  // check if the bar progress is change
  if (barProgress !== nextProps.barProgress) {
    return true
  }
}

const bootleggingRow4CollectUpdate = (prevProps, nextProps) => {
  const { additionalInfo, crimeID } = prevProps

  // update row if money value is changed
  if (crimeID === ROW_COLLECT_ID && additionalInfo.moneyToCollect !== nextProps.additionalInfo.moneyToCollect) {
    return true
  }
}

export {
  bootleggingRow1CopyUpdate,
  bootleggingRow2SellUpdate,
  bootleggingRow23SetupStore,
  bootleggingRow4CollectUpdate
}
