import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import { fetchData } from '../../modules/actions'
import { typeID } from './constants'
import reducer from './modules/reducers'
import saga from './modules/saga'
import rootStore from '../../store/createStore'

import Placeholder from './components/Skeleton'

const Preloader = () => <Placeholder type='full' />

const BootleggingContainer = Loadable({
  loader: async () => {
    const Bootlegging = await import(/* webpackChunkName: "bootlegging" */ './containers/BootleggingContainer')

    injectReducer(rootStore, { key: 'bootlegging', reducer })
    injectSaga({ key: 'bootlegging', saga })
    rootStore.dispatch(fetchData('bootlegging', `&step=crimesList&typeID=${typeID}`))

    return Bootlegging
  },
  loading: Preloader
})

export default BootleggingContainer
