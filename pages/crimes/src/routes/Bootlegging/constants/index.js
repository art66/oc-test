/* eslint-disable max-len */
export const typeID = 2

export const CRIME_TITLE = 'bootlegging'

export const SELECTOR_POS_LEFT_BOOTLEGGING = {
  desktop: { left: 406, top: 114 },
  tablet: { left: 136, top: 118 },
  mobile: { left: 90, top: 118 }
}

// ------------------------------------
// Banner Images
// ------------------------------------
// export const MOBILE_PLACEHOLDER = '/images/v2/2sef32sdfr422svbfr2/crimes/bootlegging/320_banner.jpg' // currently deprecated
// export const TABLET_PLACEHOLDER = '/images/v2/2sef32sdfr422svbfr2/crimes/bootlegging/578_banner.jpg' // currently deprecated

// ------------------------------------
// Action Constants
// ------------------------------------
export const ATTEMPT = 'bootlegging/ATTEMPT'
export const DATA_FETCHED = 'bootlegging/DATA_FETCHED'
export const FETCH_DATA = 'bootlegging/FETCH_DATA'
export const ATTEMPT_START = 'bootlegging/ATTEMPT_START'
export const ATTEMPT_FETCHED = 'bootlegging/ATTEMPT_FETCHED'
export const POOLING = 'bootlegging/POOLING'
export const POOLING_FETCHED = 'bootlegging/POOLING_FETCHED'
export const LEVEL_UP = 'bootlegging/LEVEL_UP'
export const REMOVE_CD_FROM_QUEUE = 'bootlegging/REMOVE_CD_FROM_QUEUE'
export const SET_TIME_FOR_NEXT_CD = 'bootlegging/SET_TIME_FOR_NEXT_CD'
export const TOGGLE_CDS_SELECTOR = 'bootlegging/TOGGLE_CDS_SELECTOR'
export const SELECT_CDS = 'bootlegging/SELECT_CDS'
export const SET_CRIME_LEVEL = 'bootlegging/SET_CRIME_LEVEL'
export const BLANK_SELECTED_CDS = 'bootlegging/BLANK_SELECTED_CDS'
export const BREAK_BLANK_SELECTED_CDS = 'bootlegging/BREAK_ATTEMPT'
export const CURRENT_ROW = 'bootlegging/CURRENT_ROW'
export const CLEAR_OUTDATED_OUTCOME = 'bootlegging/CLEAR_OUTDATED_OUTCOME'
export const CD_QUEUE_EMPTY = 'CD_QUEUE_EMPTY'
export const RESET_STATS_DONE = 'bootlegging/RESET_STATS_DONE'
export const CLOSE_OUTCOME = 'bootlegging/CLOSE_OUTCOME'
