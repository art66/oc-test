import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { clearOutdatedOutcome } from '../modules/actions'
import { fetchData } from '../../../modules/actions'

import { typeID, CRIME_TITLE } from '../constants'

import SubCrimesLayout from '../../../components/SubCrimesLayout'
import { CrimesList, Banner, CopyProgress, CDsSelector, Skeleton } from '../components'

class BootleggingContainer extends PureComponent {
  static propTypes = {
    state: PropTypes.object,
    screen: PropTypes.object,
    showPlaceholder: PropTypes.bool,
    current: PropTypes.number,
    currentQueue: PropTypes.array,
    progress: PropTypes.number,
    stats: PropTypes.array,
    СDsBootStage: PropTypes.string,
    startClearOutdatedOutcome: PropTypes.func,
    fetchDataAction: PropTypes.func
  }

  static defaultProps = {
    state: {},
    startClearOutdatedOutcome: () => {},
    fetchDataAction: () => {},
    screen: {},
    showPlaceholder: true,
    СDsBootStage: '',
    current: 0,
    currentQueue: null,
    progress: 0,
    stats: []
  }

  componentDidMount() {
    window.addEventListener('focus', this._fetchData)
  }

  componentWillUnmount() {
    window.removeEventListener('focus', this._fetchData)

    const {
      startClearOutdatedOutcome,
      state: { bootlegging }
    } = this.props
    const { outcome: { story } = {} } = bootlegging || {}

    if (!story || (bootlegging && story.length !== 0)) {
      startClearOutdatedOutcome()
    }
  }

  // fetching server on updates once user left and came back to the Bootlegging Tab
  _fetchData = () => {
    const { fetchDataAction } = this.props

    fetchDataAction()
  }

  render() {
    const { stats, current, progress, currentQueue, showPlaceholder, screen, СDsBootStage } = this.props

    const BannerRender = <Banner currentQueue={currentQueue} screen={screen} СDsBootStage={СDsBootStage} />

    return (
      <SubCrimesLayout
        customWrapClass='bootlegging'
        showPlaceholder={showPlaceholder}
        title='Bootlegging'
        skeleton={Skeleton}
        banners={BannerRender}
        stats={stats}
        current={current}
        progress={progress}
      >
        <CrimesList />
        <CDsSelector />
        <CopyProgress />
      </SubCrimesLayout>
    )
  }
}

const mapStateToProps = state => ({
  screen: state.browser,
  current: state.bootlegging.currentLevel,
  showPlaceholder: state.bootlegging.showPlaceholder,
  currentQueue: state.bootlegging.crimesByType[0].additionalInfo.currentQueue,
  СDsBootStage: state.bootlegging.crimesByType[0].additionalInfo.СDsBootStage,
  progress: state.bootlegging.skillLevelTotal,
  stats: state.bootlegging.currentUserStatistics,
  freeze: state.common.freeze
})

const mapDispatchToProps = dispatch => ({
  startClearOutdatedOutcome: () => dispatch(clearOutdatedOutcome(CRIME_TITLE)),
  fetchDataAction: () => dispatch(fetchData(CRIME_TITLE, `&step=crimesList&typeID=${typeID}`))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BootleggingContainer)
