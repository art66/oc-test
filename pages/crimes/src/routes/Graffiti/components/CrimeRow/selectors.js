import { createSelector } from 'reselect'
import classnames from 'classnames'
import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import {
  getIconClass,
  getAdditionalInfo,
  getMediaType,
  getDesktopManualLayout
} from '../../../../utils/globalSelectorsHelpers'
import st from '../../../../styles/graffiti-stars-icons.cssmodule.scss'
// import pi from '../../../../styles/graffiti-progress-icons.cssmodule.scss'
import styles from '../../../../styles/graffiti.cssmodule.scss'

const STATS_COLORS = {
  gray: 'statsColor_gray',
  green: 'statsColor_green',
  red: 'statsColor_red'
}

const getAttemptsTotal = props => props.attemptsTotal
const getSprayColor = props => props.sprayColor
const getSpraySelected = props => props.spraySelected
const getAttemptCountFontColor = props => props.attemptCountFontColor
const getAttemptCount = props => props.attemptCount
const getColorsIDs = props => props.colorsIDs
const getCansLeft = props => props.cansLeft
const getOtcomeHeight = props => props.getCurrentOutcome
const getBlankSelectedSpray = props => props.blankSelectedSpray
const getRequestProgress = props => props.additionalInfo.sprayRequestProgress

// reselect functions
const checkColorsAvaible = createSelector([getAdditionalInfo], additionalInfo => {
  const { availableCans } = additionalInfo

  const isSomeColorAvailable = availableCans.some(availability => {
    if (availability.available) {
      return true
    }

    return false
  })

  return isSomeColorAvailable
})

const rowIcon = createSelector([getIconClass], iconClass => styles[iconClass])

const sprayStatuses = createSelector(
  [getSpraySelected, getCansLeft, getBlankSelectedSpray, checkColorsAvaible],
  (...props) => {
    const [spraySelected, cansLeft, blankSelectedSpray, isSomeSprayAvailable] = props
    const isSprayMustBeShowed = cansLeft && Object.keys(cansLeft).length !== 0
    const isImgToggleEnabled = isSomeSprayAvailable && (isSprayMustBeShowed || blankSelectedSpray)
    const isImgSpraySelected = spraySelected && isImgToggleEnabled

    return {
      isImgToggleEnabled,
      isImgSpraySelected,
      isSprayMustBeShowed
    }
  }
)

const getSprayCount = createSelector(
  [getCansLeft, getColorsIDs, getSprayColor, getSpraySelected],
  (...props) => {
    const [cansLeft = {}, colorsIDs = {}, sprayColor, spraySelected] = props
    const spray = sprayColor || spraySelected

    const isSpraysAvaible = cansLeft && colorsIDs && spray
    const cansCount = isSpraysAvaible ? cansLeft[colorsIDs[spray]] : 0

    return {
      spray,
      sprayCansCount: cansCount
    }
  }
)

const sprayShouldNotBeShown = createSelector(
  [sprayStatuses, getSprayCount],
  ({ isImgSpraySelected }, { sprayCansCount }) => !isImgSpraySelected || sprayCansCount === 0
)

const checkStarsProgress = createSelector([getAttemptCount], attemptCount => {
  const totalGraffitiCount = !attemptCount || Object.keys(attemptCount).length === 0 ? 0 : attemptCount
  const { new: newNumber } = totalGraffitiCount

  let starsStep = 0

  if (newNumber >= 500) {
    starsStep = 5
  } else if (newNumber >= 200) {
    starsStep = 4
  } else if (newNumber >= 100) {
    starsStep = 3
  } else if (newNumber >= 50) {
    starsStep = 2
  } else if (newNumber >= 10) {
    starsStep = 1
  }

  // const progressIconName = `${iconClass}_${starsStep}`

  return {
    totalGraffitiCount,
    // progressIconName,
    starsCount: starsStep
  }
})

const classNamesHolder = createSelector(
  [checkStarsProgress, checkColorsAvaible, getRequestProgress],
  (starsProgress, isSomeColorAvailable, spraySelectionProgress) => {
    const { starsIcons } = starsProgress

    const statusRow = classnames(styles.imageWrap, styles.selector, {
      [styles.locked]: !isSomeColorAvailable, [styles.spraySelectedInProgress]: spraySelectionProgress
    })
    const cansCountWrap = classnames(styles.cansCountWrapper, styles.cansFlexCenter)
    const starsIcon = classnames(st[starsIcons], styles.marginLeftAuto)
    const selectorWrap = classnames(styles.selectorWrap, styles.dlmAny, styles.center)
    const progressIcon = classnames(styles.progressIcon, styles.center)
    const titleContainer = classnames(styles.underTitle, styles.stats, styles.dlm, styles.center)

    return {
      statusRow,
      starsIcon,
      selectorWrap,
      progressIcon,
      cansCountWrap,
      titleContainer
    }
  }
)

const sprayColorState = createSelector(
  [getSprayColor, getSpraySelected],
  (sprayColor, spraySelected) => sprayColor || spraySelected
)

const isButtonGraffitiActive = createSelector(
  [sprayColorState, checkColorsAvaible, getSprayCount],
  (isSomeColorAvailable, sprayColor, { sprayCansCount }) => {
    const activeButton = isSomeColorAvailable && sprayColor && sprayCansCount

    return activeButton
  }
)

const checkDesktopLayout = createSelector(
  [getMediaType, getDesktopManualLayout],
  (mediaType, isDesktopLayoutSetted) => (isDesktopLayoutSetted || mediaType === 'desktop' ? 'desktop' : 'mobile')
)

const animationNamespaces = createSelector(
  [checkDesktopLayout, getAttemptCountFontColor],
  (currentLayout, attemptCountFontColor) => {
    const statsAnimationName = STATS_COLORS[attemptCountFontColor]

    return {
      statsStaticValueAnim: statsAnimationName,
      statsDiffAnim: `${statsAnimationName}_${currentLayout}_diff`
    }
  }
)

const globalCrimeRowProps = createSelector(
  [
    globalRowPropsSelector,
    getAttemptsTotal,
    rowIcon,
    sprayColorState,
    getSpraySelected,
    getOtcomeHeight,
    getSprayCount,
    isButtonGraffitiActive
  ],
  (...props) => {
    const [
      globalProps,
      attemptsTotal,
      iconClass,
      sprayColor,
      spraySelected,
      outcomeHeight,
      sprayCount,
      activeButton
    ] = props

    const { sprayCansCount } = sprayCount

    return {
      ...globalProps,
      button: {
        ...globalProps.button,
        disabled: globalProps.button.disabled || !activeButton,
        action: () => globalProps.button.action({ subURL: `&value1=${sprayColor}` })
      },
      attemptsTotal,
      dlmForTitle: false,
      iconClass,
      getCurrentOutcome: outcomeHeight,
      styles,
      spraySelected,
      sprayCansCount
    }
  }
)

const ownCrimeRowProps = props => ({
  checkColorsAvaible: checkColorsAvaible(props),
  sprayStatuses: sprayStatuses(props),
  getSprayCount: getSprayCount(props),
  checkStarsProgress: checkStarsProgress(props),
  classNamesHolder: classNamesHolder(props),
  throwGlobalCrimeRowProps: globalCrimeRowProps(props),
  sprayShouldNotBeShown: sprayShouldNotBeShown(props),
  animationNamespaces: animationNamespaces(props)
})

export default ownCrimeRowProps
