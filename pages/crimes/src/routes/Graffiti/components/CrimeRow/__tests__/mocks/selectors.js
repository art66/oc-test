const initialState = {
  outcome: {
    story: ['story']
  },
  state: '',
  startAttempt: () => {},
  lastSubCrimeID: 0,
  pushCurrentRow: () => {},
  attemptCount: {
    new: 102,
    old: 102,
    diff: 0
  },
  attemptProgress: {
    inProgress: false,
    itemId: 2
  },
  isCurrentRow: false,
  isDesktopLayoutSetted: false,
  itemId: 0,
  closeOutcome: () => {},
  checkStarsProgress: 53,
  currentRowID: 0,
  spraySelected: false,
  sprayColor: 'Black',
  isSomeSprayAvailable: true,
  blankSelectedSpray: false,
  cansLeft: {
    856: 24
  },
  colorsIDs: {
    Black: 856,
    Red: 857,
    Pink: 858,
    Purple: 859,
    Blue: 860,
    Green: 861,
    Yellow: 862,
    Orange: 863
  },
  action: () => {},
  additionalInfo: {
    title: 'Graffiti',
    progressIconState: null,
    barValue: 53,
    availableCans: [
      {
        available: true
      }
    ]
  },
  available: false,
  crimeID: 6,
  iconClass: 'graffiti-icon-class',
  mediaType: 'desktop',
  attemptCountFontColor: 'green',
  nerve: 0,
  requirements: {
    items: ['', '']
  },
  showOutcome: false,
  title: 'Crime',
  result: '',
  story: []
}

export default initialState
