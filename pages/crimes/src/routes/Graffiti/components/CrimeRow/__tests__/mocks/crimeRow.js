const initialState = {
  toggleCanSelector: () => {},
  visible: false,
  mediaType: 'desktop',
  colorsIDs: {
    Black: 856,
    Red: 857,
    Pink: 858,
    Purple: 859,
    Blue: 860,
    Green: 861,
    Yellow: 862,
    Orange: 863
  },
  attemptCountFontColor: 'green',
  attemptCount: {
    new: 512,
    old: 510,
    diff: 5
  },
  currentTemplate: 'Graffiti',
  cansLeft: {
    0: 0,
    1: 0
  },
  skillLevel: 0,
  skillLevelTotal: 0,
  title: '',
  nerve: 0,
  iconClass: '',
  crimeID: 0,
  subID: 0,
  requirements: {},
  available: true,
  additionalInfo: {
    total: null,
    spraySelected: '',
    blankSelectedSpray: false,
    availableCans: [
      {
        color: '',
        itemID: 0,
        available: true,
        value: 0
      },
      {
        color: '',
        itemID: 0,
        available: false,
        value: 0
      },
      {
        color: '0',
        itemID: 0,
        available: false,
        value: 0
      },
      {
        color: '',
        itemID: 0,
        available: false,
        value: 0
      },
      {
        color: '',
        itemID: 0,
        available: false,
        value: 0
      },
      {
        color: '',
        itemID: 0,
        available: false,
        value: 0
      },
      {
        color: '',
        itemID: 0,
        available: false,
        value: 0
      },
      {
        color: '',
        itemID: 0,
        available: false,
        value: 0
      }
    ]
  },
  panelItems: [
    {
      removeLink: true,
      label: '',
      value: ''
    },
    {
      removeLink: false,
      label: '',
      value: ''
    }
  ],
  result: ''
}

export const imgSpraySelected = {
  ...initialState,
  spraySelected: 'Black'
}

export const imgSprayNotSelected = {
  ...initialState,
  sprayShouldNotBeShown: false
}

export const fiveStarsIcon = {
  ...initialState,
  attemptCount: {
    new: 510,
    old: 510,
    diff: 0
  },
  iconClass: 'west_side'
}

export const attemptActive = {
  ...initialState,
  cansLeft: {
    ...initialState.cansLeft,
    856: 10
  },
  attemptCount: {
    new: 510,
    old: 510,
    diff: 0
  },
  sprayColor: 'Black',
  spraySelected: 'Black',
  iconClass: 'west_side'
}

export const attemptDisabled = {
  ...initialState,
  attemptCount: {
    new: 510,
    old: 510,
    diff: 0
  },
  sprayColor: '',
  spraySelected: '',
  iconClass: 'west_side'
}

export const statsIncrement = {
  ...initialState
}

export const statsDecrement = {
  ...initialState,
  attemptCount: {
    new: 508,
    old: 510,
    diff: 2
  }
}

export default initialState
