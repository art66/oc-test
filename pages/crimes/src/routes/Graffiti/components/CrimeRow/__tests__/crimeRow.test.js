import React from 'react'
import { mount, shallow, render } from 'enzyme'

import initialState, {
  imgSpraySelected,
  imgSprayNotSelected,
  fiveStarsIcon,
  attemptActive,
  attemptDisabled,
  statsIncrement,
  statsDecrement
} from './mocks/crimeRow'
import CrimeRow from '../CrimeRow'

describe('<CrimeRow />', () => {
  it('should render the crimeRow', () => {
    const Component = shallow(<CrimeRow {...initialState} />)

    expect(Component.find('.progressIcon.center').length).toBe(1)
    expect(Component.find('.stats').length).toBe(1)
    expect(Component.find('.selectorWrap').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should have the spray image inside row', () => {
    const Component = shallow(<CrimeRow {...imgSpraySelected} />)

    expect(Component.find('.progressIcon.center').length).toBe(1)
    expect(Component.find('img.graffitiImage').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should not have the spray image inside row if imgSprayNotSelected', () => {
    const Component = shallow(<CrimeRow {...imgSprayNotSelected} />)

    expect(Component.find('.progressIcon.center').length).toBe(1)
    expect(Component.find('img.graffitiImage').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should have the gold city icon with 5 stars', () => {
    const Component = shallow(<CrimeRow {...fiveStarsIcon} />)

    expect(Component.find('.progressIcon.center').length).toBe(1)
    expect(Component.find('CrimeRowIcon').prop('cityLevel')).toBe(5)
    expect(Component.find('Stars').length).toBe(5)
    expect(
      Component.find('Stars')
        .at(0)
        .prop('isGoldenIcon')
    ).toBe(true)
    expect(
      Component.find('Stars')
        .at(1)
        .prop('isGoldenIcon')
    ).toBe(true)
    expect(
      Component.find('Stars')
        .at(2)
        .prop('isGoldenIcon')
    ).toBe(true)
    expect(
      Component.find('Stars')
        .at(3)
        .prop('isGoldenIcon')
    ).toBe(true)
    expect(
      Component.find('Stars')
        .at(4)
        .prop('isGoldenIcon')
    ).toBe(true)
    expect(Component).toMatchSnapshot()
  })
  it('crime attempt button should be activated', () => {
    const Component = shallow(<CrimeRow {...attemptActive} />)
    const { disabled } = Component.instance().render().props.button

    expect(disabled).toBeFalsy()
    expect(Component).toMatchSnapshot()
  })
  it('crime attempt button should be deactivated', () => {
    const Component = shallow(<CrimeRow {...attemptDisabled} />)
    const { disabled } = Component.instance().render().props.button

    expect(disabled).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should set the flag to show Popup with sprays', () => {
    const Component = mount(<CrimeRow {...initialState} />)

    expect(Component.find('.imageWrap.selector').length).toBe(1)
    expect(
      Component.find('.imageWrap.selector').simulate('click', {
        nativeEvent: {
          stopImmediatePropagation: () => {}
        }
      })
    ).toBeTruthy()

    Component.setProps({ visible: true })
    expect(Component.prop('visible')).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should show statsValue animation Up', () => {
    const Component = render(<CrimeRow {...statsIncrement} />)

    expect(Component.find('.statsDiff').text()).toEqual('+5')
    expect(Component.find('.statsStaticValueWrap').text()).toEqual('512')

    expect(Component).toMatchSnapshot()
  })
  it('should show statsValue animation Down', () => {
    const Component = render(<CrimeRow {...statsDecrement} />)

    expect(Component.find('.statsDiff').text()).toEqual('-2')
    expect(Component.find('.statsStaticValueWrap').text()).toEqual('508')

    expect(Component).toMatchSnapshot()
  })
})
