/* eslint-disable max-len */
import initialState from './mocks/selectors'
import ownCrimeRowProps from '../selectors'

describe('Graffiti crimeRow selectors', () => {
  it('should return all selectors', () => {
    const {
      checkColorsAvaible,
      sprayStatuses,
      getSprayCount,
      checkStarsProgress,
      classNamesHolder,
      throwGlobalCrimeRowProps: props,
      sprayShouldNotBeShown
    } = ownCrimeRowProps(initialState)

    expect(checkColorsAvaible).toBeTruthy()
    expect(typeof sprayStatuses === 'object').toBeTruthy()
    expect(typeof getSprayCount === 'object').toBeTruthy()
    expect(typeof checkStarsProgress === 'object').toBeTruthy()
    expect(typeof classNamesHolder === 'object').toBeTruthy()
    expect(typeof props === 'object').toBeTruthy()
    expect(typeof sprayShouldNotBeShown === 'boolean').toBeTruthy()
    expect(props).toMatchSnapshot()
  })
  it('should return true for available color in whole rows array', () => {
    const { checkColorsAvaible } = ownCrimeRowProps(initialState)

    expect(checkColorsAvaible).toBeTruthy()
    expect(checkColorsAvaible).toMatchSnapshot()
  })
  it('should return sprayStatuses object with enabled isImgToggleEnabled, disabled isImgSpraySelected and enabled isSprayMustBeShowed', () => {
    const { sprayStatuses } = ownCrimeRowProps(initialState)
    const { isImgToggleEnabled, isImgSpraySelected, isSprayMustBeShowed } = sprayStatuses

    expect(typeof sprayStatuses === 'object').toBeTruthy()
    expect(isImgToggleEnabled).toBeTruthy()
    expect(isImgSpraySelected).toBeFalsy()
    expect(isSprayMustBeShowed).toBeTruthy()
    // expect(sprayStatuses).toMatchSnapshot()
  })
  it('should return getSprayCount object with Black Spray selected and its cansLoad on 24%', () => {
    const { getSprayCount } = ownCrimeRowProps(initialState)
    const { spray, sprayCansCount } = getSprayCount

    expect(typeof getSprayCount === 'object').toBeTruthy()
    expect(spray).toBe('Black')
    expect(sprayCansCount).toBe(24)
    expect(getSprayCount).toMatchSnapshot()
  })
  it('should return checkStarsProgress object with 3thd stage of 102 stars with classNames graffiti-icon-class_3 and starsIcons_3', () => {
    const { checkStarsProgress } = ownCrimeRowProps(initialState)
    const { totalGraffitiCount, progressIconName, starsIcons } = checkStarsProgress

    expect(typeof checkStarsProgress === 'object').toBeTruthy()
    expect(totalGraffitiCount).toEqual({ new: 102, old: 102, diff: 0 })
    expect(progressIconName).toBe(undefined) // removed after SVG icons migration
    expect(starsIcons).toBe(undefined) // removed after SVG icons migration
    expect(checkStarsProgress).toMatchSnapshot()
  })
  it('should not show spray in the row', () => {
    const { sprayShouldNotBeShown } = ownCrimeRowProps(initialState)

    expect(sprayShouldNotBeShown).toBeTruthy()
  })
  it('should return classNamesHolder object with all classes in CrimeRow', () => {
    const { classNamesHolder, animationNamespaces } = ownCrimeRowProps(initialState)
    const {
      iconName,
      statusRow,
      starsIcon,
      selectorWrap,
      progressIcon,
      cansCountWrap,
      titleContainer
    } = classNamesHolder

    expect(typeof classNamesHolder === 'object').toBeTruthy()
    expect(iconName).toEqual(undefined) // removed after SVG icons migration
    expect(statusRow).toBe('imageWrap selector')
    expect(starsIcon).toBe('undefined marginLeftAuto') // removed after SVG icons migration
    expect(selectorWrap).toBe('selectorWrap dlmAny center')
    expect(progressIcon).toBe('progressIcon center')
    expect(cansCountWrap).toBe('cansCountWrapper cansFlexCenter')
    expect(titleContainer).toBe('underTitle stats dlm center')
    expect(animationNamespaces).toEqual({
      statsDiffAnim: 'statsColor_green_desktop_diff',
      statsStaticValueAnim: 'statsColor_green'
    })
    expect(classNamesHolder).toMatchSnapshot()
  })
})
