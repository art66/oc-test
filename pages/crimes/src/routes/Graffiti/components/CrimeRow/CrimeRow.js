import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew/'
import { toMoney } from '@torn/shared/utils'

import { graffitiRowsUpdate } from '../../utils/shouldRowsUpdate'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'
import { CrimeRow as CrimeRowGlobal } from '../../../../components'
import CrimeRowIcon from '../CrimeRowIcon'
import Stars from '../Stars'

import ownCrimeRowProps from './selectors'
import styles from '../../../../styles/graffiti.cssmodule.scss'
import localStyles from './index.cssmodule.scss'
import './anim.scss'

const ARRAY_INDEX_FIXER = 1

export class CrimeRow extends Component {
  static propTypes = {
    startAttempt: PropTypes.func,
    crimeId: PropTypes.number,
    color: PropTypes.string,
    currentRowID: PropTypes.number,
    attemptCount: PropTypes.object,
    attemptCountFontColor: PropTypes.string,
    spraySelected: PropTypes.string,
    blankSelectedSpray: PropTypes.bool,
    cansLeft: PropTypes.object,
    colorsIDs: PropTypes.object,
    outcome: PropTypes.object,
    state: PropTypes.string,
    itemId: PropTypes.number,
    lastSubCrimeID: PropTypes.number,
    sprayColor: PropTypes.string,
    canSelector: PropTypes.object,
    isCurrentRow: PropTypes.bool,
    additionalInfo: PropTypes.object,
    attemptsTotal: PropTypes.number,
    available: PropTypes.bool,
    crimeID: PropTypes.number,
    iconClass: PropTypes.string,
    index: PropTypes.number,
    mediaType: PropTypes.string,
    nerve: PropTypes.number,
    showOutcome: PropTypes.bool,
    title: PropTypes.string.isRequired,
    toggleCanSelector: PropTypes.func,
    getCurrentOutcome: PropTypes.func
  }

  static defaultProps = {
    startAttempt: () => {},
    toggleCanSelector: () => {},
    getCurrentOutcome: () => {},
    crimeId: 0,
    color: '',
    currentRowID: 0,
    attemptCount: {},
    attemptCountFontColor: '',
    spraySelected: '',
    blankSelectedSpray: false,
    cansLeft: {},
    colorsIDs: {},
    outcome: {},
    state: '',
    itemId: 0,
    lastSubCrimeID: 0,
    sprayColor: '',
    canSelector: {},
    isCurrentRow: false,
    additionalInfo: {},
    attemptsTotal: 0,
    available: false,
    crimeID: 0,
    iconClass: '',
    index: 0,
    mediaType: '',
    nerve: 0,
    showOutcome: false
  }

  constructor(props) {
    super(props)

    this.state = {
      newStat: 0,
      isRowStatsDiff: false
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { checkStarsProgress } = ownCrimeRowProps(nextProps)
    const {
      totalGraffitiCount: { new: newNumber, diff = 0 }
    } = checkStarsProgress

    const isNewStats = newNumber !== prevState.newStat
    const isStatsDiff = diff !== 0

    return {
      newStat: newNumber,
      isRowStatsDiff: (isNewStats && isStatsDiff) || prevState.isRowStatsDiff
    }
  }

  componentDidMount() {
    this._mountTooltips()
  }

  shouldComponentUpdate(nextProps, prevState) {
    const { isRowStatsDiff } = this.state
    const {
      getSprayCount: { sprayCansCount }
    } = ownCrimeRowProps(this.props)
    const {
      getSprayCount: { sprayCansCount: nextSprayCount }
    } = ownCrimeRowProps(nextProps)

    this.props = {
      ...this.props,
      sprayCansCount
    }

    nextProps = {
      ...nextProps,
      nextSprayCount
    }

    // watching for the animation's changes
    const internalRowStateCheck = (() => prevState.isRowStatsDiff !== isRowStatsDiff)()
    const rowCheck = graffitiRowsUpdate(this.props, nextProps)
    const globalCheck = globalCrimeRowCheck(this.props, nextProps)

    return rowCheck || globalCheck || internalRowStateCheck || false
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _getTooltipID = () => {
    const { crimeID } = this.props

    return {
      area: `progressRow__area__Graffiti_${crimeID}`,
      graffitiCount: `progressRow__graffitiCount__Graffiti_${crimeID}`
    }
  }

  _getTooltipData = () => {
    return [
      {
        ID: this._getTooltipID().area,
        child: 'Area reputation'
      },
      {
        ID: this._getTooltipID().graffitiCount,
        child: 'Graffiti placed'
      }
    ]
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltipData(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltipData(), type: 'update' })

  _toggleSelector = event => {
    event.nativeEvent.stopImmediatePropagation()

    const {
      additionalInfo: { availableCans, sprayRequestProgress },
      index,
      crimeID,
      toggleCanSelector
    } = this.props

    if (sprayRequestProgress) return

    toggleCanSelector({
      ID: index,
      items: availableCans,
      visible: true,
      crimeID
    })
  }

  _getSprayButtonWrap = () => {
    const { colorsIDs } = this.props
    const {
      getSprayCount: { spray },
      sprayShouldNotBeShown
    } = ownCrimeRowProps(this.props)

    if (sprayShouldNotBeShown) return null

    return <img alt='spray' className={styles.graffitiImage} src={`/images/items/${colorsIDs[spray]}/medium.png`} />
  }

  _getCountWrap = () => {
    const {
      classNamesHolder: { cansCountWrap },
      getSprayCount: { sprayCansCount },
      sprayShouldNotBeShown
    } = ownCrimeRowProps(this.props)

    if (sprayShouldNotBeShown) return null

    return (
      <div className={cansCountWrap}>
        <span className={styles.cansCount}>{sprayCansCount}%</span>
      </div>
    )
  }

  _getStars = starsCount => {
    return Array.from(Array(5).keys()).map((star, index) => {
      const isGoldenIcon = starsCount >= index + ARRAY_INDEX_FIXER

      return <Stars key={star} isGoldenIcon={isGoldenIcon} />
    })
  }

  _getStats = () => {
    const { isRowStatsDiff } = this.state
    const {
      checkStarsProgress: { totalGraffitiCount },
      classNamesHolder: { titleContainer },
      animationNamespaces: { statsStaticValueAnim, statsDiffAnim }
    } = ownCrimeRowProps(this.props)

    const { new: newNumber = 0, old: oldNumber = 0, diff = 0 } = totalGraffitiCount
    const stats = toMoney(newNumber)

    const diffValue = `${oldNumber > newNumber ? '-' : '+'}${diff}`

    const renderStatsDiff = () => (
      <div className={localStyles.statsDiffWrap}>
        <CSSTransition
          appear
          in={isRowStatsDiff}
          timeout={{ appear: 500, enter: 500, exit: 2500 }}
          classNames={statsDiffAnim}
          unmountOnExit
          onEntered={() => this.setState({ isRowStatsDiff: false })}
        >
          <span className={localStyles.statsDiff}>{diff && diffValue}</span>
        </CSSTransition>
      </div>
    )

    const renderStatsStaticValue = () => {
      return (
        <TransitionGroup className={localStyles.statsStaticValueWrap} id={this._getTooltipID().graffitiCount}>
          <CSSTransition
            appear
            component='div'
            key={stats}
            timeout={{ appear: 5000, enter: 5000, exit: 0 }}
            className={localStyles.statCount}
            classNames={statsStaticValueAnim}
            unmountOnExit
          >
            <span>{stats}</span>
          </CSSTransition>
        </TransitionGroup>
      )
    }

    return (
      <div className={`${localStyles.statsWrap} ${titleContainer}`}>
        {renderStatsDiff()}
        {renderStatsStaticValue()}
      </div>
    )
  }

  render() {
    const { crimeID } = this.props

    const { checkStarsProgress, classNamesHolder, throwGlobalCrimeRowProps: props } = ownCrimeRowProps(this.props)
    const { statusRow, selectorWrap, progressIcon } = classNamesHolder

    const { starsCount } = checkStarsProgress

    return (
      <CrimeRowGlobal {...props} underTitle rowShouldUpdate={graffitiRowsUpdate}>
        <div className={localStyles.progressIconWrap}>
          <div className={progressIcon} id={this._getTooltipID().area}>
            <CrimeRowIcon crimeID={crimeID} cityLevel={starsCount} />
            <div className={localStyles.starsWrap}>{this._getStars(starsCount)}</div>
          </div>
        </div>
        {this._getStats()}
        <div className={selectorWrap}>
          <button className={statusRow} type='button' onClick={this._toggleSelector}>
            {this._getSprayButtonWrap()}
          </button>
          {this._getCountWrap()}
        </div>
      </CrimeRowGlobal>
    )
  }
}

export default CrimeRow
