import { createSelector } from 'reselect'
import isEmpty from '@torn/shared/utils/isEmpty'

import { RESOLUTIONS_REVERTS } from '../../constants'
import { getDesktopManualLayout } from '../../../../utils/globalSelectorsHelpers'

const MANUAL_DESKTOP_RESOLUTION = 976

const getBannerMainImages = props => props.bannerMainImages
const getMediaType = props => props.mediaType

const cashedImg = createSelector(
  [getBannerMainImages, getMediaType, getDesktopManualLayout],
  (bannerMainImages, mediaType, isDesktopLayoutSetted) => {
    if (isEmpty(bannerMainImages)) {
      return null
    }

    const isAllResolutionsLoaded = bannerMainImages.length === 3
    const isImgURLNotEmpty = bannerMainImages[2].imgUrl

    if (isAllResolutionsLoaded && isImgURLNotEmpty) {
      const img = bannerMainImages.filter(image => {
        if (isDesktopLayoutSetted) {
          return image.resolution === MANUAL_DESKTOP_RESOLUTION
        }

        return RESOLUTIONS_REVERTS[image.resolution] === mediaType
      })

      return img[0].imgUrl
    }

    return null
  }
)

const bannerSelectors = props => ({
  cashedImg: cashedImg(props)
})

export default bannerSelectors
