import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { showDropDownPanel, toggleArrows } from '../../../../modules/actions'
import { bannerFetchingFinish } from '../../modules/actions'

import BannerGlobal from '../../../../components/Banner'
import bannerSelectors from './selectors'
import styles from './banners.cssmodule.scss'

export class Banners extends Component {
  static propTypes = {
    bannerLoaded: PropTypes.func,
    dropDownPanelToShow: PropTypes.func,
    toggleArrow: PropTypes.func
  }

  static defaultProps = {
    bannerLoaded: () => {},
    dropDownPanelToShow: () => {},
    toggleArrow: () => {}
  }

  constructor(props) {
    super(props)

    this.state = {
      isImageCached: false
    }

    this.ref = React.createRef()
  }

  componentDidMount() {
    this._loadBannerImage()
  }

  shouldComponentUpdate(nextProps) {
    const { cashedImg } = bannerSelectors(this.props)
    const { cashedImg: cashedImgNext } = bannerSelectors(nextProps)

    if (cashedImg === cashedImgNext) {
      return false
    }

    return true
  }

  componentDidUpdate() {
    const { isImageCached } = this.state

    if (!isImageCached) {
      this._loadBannerImage()
    }
  }

  _loadBannerImage = () => {
    const { bannerLoaded } = this.props
    const { cashedImg } = bannerSelectors(this.props)

    if (!cashedImg) {
      return
    }

    const img = new Image()

    img.onload = () => {
      this.setState({
        isImageCached: true
      })

      this.ref.current.src = cashedImg
      bannerLoaded()
    }

    img.src = cashedImg
  }

  render() {
    const { dropDownPanelToShow, toggleArrow } = this.props

    return (
      <BannerGlobal
        customClass={styles.bannerWrap}
        dropDownPanelToShow={dropDownPanelToShow}
        toggleArrows={toggleArrow}
      >
        <div className={styles.imgPlaceholder} />
        <div className={styles.graffitiBanners}>
          <img
            ref={this.ref}
            src=''
            alt=''
            style={{ width: '100%', height: '100%' }}
          />
        </div>
      </BannerGlobal>
    )
  }
}

const mapStateToProps = state => ({
  isDesktopLayoutSetted: state.common.isDesktopLayoutSetted
})

const mapDispatchToProps = dispatch => ({
  bannerLoaded: () => dispatch(bannerFetchingFinish()),
  dropDownPanelToShow: () => dispatch(showDropDownPanel()),
  toggleArrow: () => dispatch(toggleArrows())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Banners)
