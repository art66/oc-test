const initialState = {
  mediaType: 'desktop',
  bannerMainImages: [
    {
      resolution: 976,
      imgUrl: 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=976'
    },
    {
      resolution: 578,
      imgUrl: 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=578'
    },
    {
      resolution: 320,
      imgUrl: 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=320'
    }
  ]
}

export default initialState
