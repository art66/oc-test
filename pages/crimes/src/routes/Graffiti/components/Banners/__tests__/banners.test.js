import React from 'react'
import { mount } from 'enzyme'
import configureStore from 'redux-mock-store'

import { Banners } from '..'
import initialState, { mobileLayout, tabletLayout, manualDesktopMode } from './mocks'

const DESKTOP_IMAGE = 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=976'
const TABLET_IMAGE = 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=578'
const MOBILE_IMAGE = 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=320'

describe('<Banners />', () => {
  const mockStore = configureStore()

  it('Banner image should be showed', () => {
    const store = mockStore(initialState)
    const Component = mount(<Banners store={store} {...store.getState()} />)

    expect(Component.find('img').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render Banner image of Desktop version', async () => {
    const store = mockStore(initialState)
    const Component = await mount(<Banners store={store} {...store.getState()} />)

    await Component.find('img').simulate('load')
    Component.find('img').props().src = DESKTOP_IMAGE

    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.imgPlaceholder').length).toBe(1)
    expect(Component.find('.graffitiBanners').length).toBe(1)
    expect(Component.find('img').length).toBe(1)
    expect(Component.find('img').prop('src')).toBe(DESKTOP_IMAGE)
    expect(Component).toMatchSnapshot()
  })
  it('should render Banner image of Tablet version', async () => {
    const store = mockStore(tabletLayout)
    const Component = mount(<Banners store={store} {...store.getState()} />)

    await Component.find('img').simulate('load')
    Component.find('img').props().src = TABLET_IMAGE

    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.imgPlaceholder').length).toBe(1)
    expect(Component.find('.graffitiBanners').length).toBe(1)
    expect(Component.find('img').length).toBe(1)
    expect(Component.find('img').prop('src')).toBe(TABLET_IMAGE)
    expect(Component).toMatchSnapshot()
  })
  it('should render Banner image of Mobile version', async () => {
    const store = mockStore(mobileLayout)
    const Component = mount(<Banners store={store} {...store.getState()} />)

    await Component.find('img').simulate('load')
    Component.find('img').props().src = MOBILE_IMAGE

    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.imgPlaceholder').length).toBe(1)
    expect(Component.find('.graffitiBanners').length).toBe(1)
    expect(Component.find('img').length).toBe(1)
    expect(Component.find('img').prop('src')).toBe(MOBILE_IMAGE)
    expect(Component).toMatchSnapshot()
  })
  it('should always render desktop Banner image if manualDesktopMode activated', async () => {
    const store = mockStore(manualDesktopMode)
    const Component = mount(<Banners store={store} {...store.getState()} />)

    await Component.find('img').simulate('load')
    Component.find('img').props().src = DESKTOP_IMAGE

    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.imgPlaceholder').length).toBe(1)
    expect(Component.find('.graffitiBanners').length).toBe(1)
    expect(Component.find('img').length).toBe(1)
    expect(Component.find('img').prop('src')).toBe(DESKTOP_IMAGE)
    expect(Component).toMatchSnapshot()
  })
})
