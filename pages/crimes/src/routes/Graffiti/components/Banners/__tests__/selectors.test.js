import initialState from './mocks'
import bannerSelectors from '../selectors'

const IMG_URL = 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=976'

describe('Graffiti banners selector', () => {
  it('should return banner image', () => {
    const { cashedImg } = bannerSelectors(initialState)

    expect(cashedImg).toBe(IMG_URL)
    expect(cashedImg).toMatchSnapshot()
  })
})
