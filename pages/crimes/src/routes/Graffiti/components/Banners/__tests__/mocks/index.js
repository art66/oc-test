const initialState = {
  isBannerImagesFetched: true,
  mediaType: 'desktop',
  bannerMainImages: [
    {
      resolution: 976,
      imgUrl: 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=976'
    },
    {
      resolution: 578,
      imgUrl: 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=578'
    },
    {
      resolution: 320,
      imgUrl: 'crimes_banners_demo.php?step=banners&playerName=svyat770&color=FF0000&resolution=320'
    }
  ]
}

export const tabletLayout = {
  ...initialState,
  mediaType: 'tablet'
}

export const mobileLayout = {
  ...initialState,
  mediaType: 'mobile'
}

export const disabledBanner = {
  ...initialState,
  isBannerImagesFetched: false
}

export const eventClicked = {
  ...initialState,
  clientX: 100,
  clientY: 200
}

export const mouseDownMock = {
  coordsStart: {
    X: 100,
    Y: 200
  }
}

export const mouseUpMock = {
  coordsStart: {
    X: 100,
    Y: 200
  }
}

export const mouseUpWrongMock = {
  coordsStart: {
    X: 200,
    Y: 200
  }
}

export const manualDesktopMode = {
  ...initialState,
  isDesktopLayoutSetted: true
}

export default initialState
