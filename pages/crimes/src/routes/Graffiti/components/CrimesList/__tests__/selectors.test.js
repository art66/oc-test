import initialState, { stateCrime2, stateCrime3 } from './mocks/selectors'
import getGlobalSelector from '../../../../../utils/globalListPropsSelector'
import {
  rowIDState,
  colorState,
  playerNameState,
  graffitiLevelState,
  graffitiColorState,
  fontSizeState,
  colorsIDsState,
  canSelectorState,
  attemptsTotalState
} from '../selectors'

describe('Graffiti crimeList selectors', () => {
  it('should update 1st crime of crimesList with outcome result obtained', () => {
    const [route, browser, common] = initialState
    const { crimes } = getGlobalSelector(route, browser, common)

    expect(crimes[0].isRowHaveOutcome).toBeTruthy()
    expect(crimes[0].rowState).toBe('outcome is there')
    expect(crimes).toMatchSnapshot()
  })
  it('should throw outcome to 1st crime and then, after state update, throw it to the 3thd', () => {
    const [route1, browser1, common1] = stateCrime2
    const { crimes: newCrimesState1 } = getGlobalSelector(route1, browser1, common1)

    expect(newCrimesState1[0].isRowHaveOutcome).toBeFalsy()
    expect(newCrimesState1[0].rowState).toBe('locked')

    expect(newCrimesState1[1].isRowHaveOutcome).toBeTruthy()
    expect(newCrimesState1[1].rowState).toBe('outcome is there')
    expect(newCrimesState1).toMatchSnapshot()

    const [route2, browser2, common2] = stateCrime3
    const { crimes: newCrimesState2 } = getGlobalSelector(route2, browser2, common2)

    expect(newCrimesState2[1].isRowHaveOutcome).toBeFalsy()
    expect(newCrimesState2[1].rowState).toBe('locked')
    expect(newCrimesState2[2].isRowHaveOutcome).toBeTruthy()
    expect(newCrimesState2[2].rowState).toBe('outcome is there')
    expect(newCrimesState2).toMatchSnapshot()
  })
  it('should run and return all selectors', () => {
    const [route] = initialState

    expect(typeof rowIDState(route) === 'number').toBeTruthy()
    expect(typeof colorState(route) === 'string').toBeTruthy()
    expect(typeof playerNameState(route) === 'string').toBeTruthy()
    expect(typeof graffitiLevelState(route) === 'number').toBeTruthy()
    expect(typeof graffitiColorState(route) === 'string').toBeTruthy()
    expect(typeof fontSizeState(route) === 'number').toBeTruthy()
    expect(typeof colorsIDsState(route) === 'object').toBeTruthy()
    expect(typeof canSelectorState(route) === 'object').toBeTruthy()
    expect(typeof attemptsTotalState(route) === 'number').toBeTruthy()
  })
})
