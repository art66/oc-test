const initialState = [
  {
    user: {
      nerve: 2
    },
    startAttempt: () => {},
    statusTimersAction: () => {},
    lastSelectedColor: '',
    playerName: '',
    graffitiLevel: 0,
    graffitiColor: '',
    fontSize: 0,
    colorsIDs: [],
    attemptProgress: {
      inProgress: false,
      itemId: 1
    },
    canSelector: {
      ID: 0,
      items: [
        {
          color: '',
          itemID: 0,
          available: false,
          value: 0
        }
      ],
      length: 0,
      pos: {
        left: 0,
        top: 0
      },
      selectedItems: [],
      visible: false
    },
    currentRowID: 1,
    isCurrentRow: false,
    showPlaceholder: true,
    outcome: {
      outcomeDesc: null,
      result: 'outcome is there',
      rewardsGive: {
        itemsRewards: null,
        userRewards: {
          money: 0
        }
      },
      story: []
    },
    result: 'result_there',
    story: ['some_story'],
    crimesByType: [
      {
        currentTemplate: 'graffiti',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        user: {
          nerve: 2
        },
        iconClass: 'graffiti',
        crimeID: 1,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'graffiti',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        user: {
          nerve: 2
        },
        iconClass: 'graffiti',
        crimeID: 2,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'graffiti',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        user: {
          nerve: 2
        },
        iconClass: 'graffiti',
        crimeID: 3,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'graffiti',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        user: {
          nerve: 2
        },
        iconClass: 'graffiti',
        crimeID: 4,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'graffiti',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        user: {
          nerve: 2
        },
        iconClass: 'graffiti',
        crimeID: 5,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      },
      {
        currentTemplate: 'graffiti',
        skillLevel: 0,
        skillLevelTotal: 0,
        title: 'Search the Trash',
        user: {
          nerve: 2
        },
        iconClass: 'graffiti',
        crimeID: 6,
        subID: 0,
        requirements: {
          items: {},
          minCrimeLevel: {}
        },
        available: true,
        additionalInfo: {
          notoriety: 100,
          statusEffects: [
            {
              disabled: false,
              reduceRFC: 0,
              timePercent: 0,
              timeTillNextCycle: 0,
              title: 'oneCamera'
            }
          ]
        },
        panelItems: [
          {
            removeLink: true,
            label: '',
            value: ''
          },
          {
            removeLink: false,
            label: '',
            value: ''
          }
        ],
        result: 'info'
      }
    ],
    additionalInfo: null,
    currentType: {
      _id: {
        $oid: ''
      },
      ID: 0,
      typeID: 0,
      title: 'graffiti',
      crimeRoute: 'graffiti',
      active: 0,
      img: '',
      expForLevel100: 0
    },
    skillLevelTotal: 0,
    currentLevel: 0,
    lastSubCrimeID: 6,
    currentUserStats: {
      failedTotal: 0,
      attemptsTotal: 1000,
      crimesByIDAttempts: {
        1: 0,
        2: 0,
        3: 0
      },
      successesTotal: 0,
      moneyFoundTotal: 0,
      jailedTotal: 0,
      itemsFound: {
        1: 0
      },
      critFailedTotal: 0,
      statsByType: [
        {
          successesTotal: 0
        }
      ],
      skill: 0,
      skillLevel: 0
    },
    currentUserStatistics: [
      {
        label: '',
        value: ''
      }
    ]
  },
  {
    mediaType: 'desktop'
  },
  {
    jailed: false,
    user: {
      nerve: 3
    }
  }
]

export const stateCrime2 = [
  {
    ...initialState[0],
    attemptProgress: {
      inProgress: false,
      itemId: 2
    }
  },
  {
    mediaType: 'desktop'
  },
  {
    jailed: false,
    user: {
      nerve: 3
    }
  }
]

export const stateCrime3 = [
  {
    ...initialState[0],
    attemptProgress: {
      inProgress: false,
      itemId: 3
    }
  },
  {
    mediaType: 'desktop'
  },
  {
    jailed: false,
    user: {
      nerve: 3
    }
  }
]

export default initialState
