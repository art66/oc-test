const initialState = {
  startAttempt: () => {},
  cansLeft: {
    857: 10,
    860: 9
  },
  additionalInfo: {
    availableCans: [
      { color: 'Black', itemID: 856, available: false, value: 100 },
      { color: 'Red', itemID: 857, available: true, value: 10 },
      { color: 'Pink', itemID: 858, available: false, value: 100 },
      { color: 'Purple', itemID: 859, available: false, value: 100 },
      { color: 'Blue', itemID: 860, available: true, value: 9 },
      { color: 'Green', itemID: 861, available: false, value: 100 },
      { color: 'Yellow', itemID: 862, available: false, value: 100 },
      { color: 'Orange', itemID: 863, available: false, value: 100 }
    ]
  },
  attemptProgress: {
    inProgress: false,
    itemId: 0
  },
  isBannerImagesFetched: false,
  showPlaceholder: true,
  currentRowID: 0,
  lastSelectedColor: 'Black',
  user: {
    playername: 'TornCity'
  },
  graffitiColor: '333333',
  graffitiLevel: 1,
  bannerSupportImages: [
    {
      resolution: '',
      imgUrl: ''
    }
  ],
  bannerMainImages: [
    {
      resolution: '',
      imgUrl: ''
    }
  ],
  canSelector: {
    ID: 0,
    items: [
      {
        color: '',
        itemID: 0,
        available: false,
        value: 0
      }
    ],
    length: 0,
    pos: {
      left: 0,
      top: 0
    },
    selectedItems: [],
    visible: false
  },
  isCurrentRow: false,
  colorsIDs: {
    Black: 856,
    Red: 857,
    Pink: 858,
    Purple: 859,
    Blue: 860,
    Green: 861,
    Yellow: 862,
    Orange: 863
  },
  colorsSpray: {
    Black: '333333',
    Red: 'FF0000',
    Pink: 'D24DFF',
    Purple: 'D4DFF',
    Blue: '26C9FF',
    Green: 'A3D3900',
    Yellow: 'FFBF00',
    Orange: 'FF8000'
  },
  crimes: [
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        cansLeft: {
          857: 10,
          860: 9
        },
        total: null,
        spraySelected: null,
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    },
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        cansLeft: {
          857: 10,
          860: 9
        },
        total: null,
        spraySelected: 'Red',
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    },
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        cansLeft: {
          857: 10,
          860: 9
        },
        total: null,
        spraySelected: null,
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    },
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        cansLeft: {
          857: 10,
          860: 9
        },
        total: null,
        spraySelected: 'Red',
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    },
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        cansLeft: {
          857: 10,
          860: 9
        },
        total: null,
        spraySelected: null,
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    },
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        cansLeft: {
          857: 10,
          860: 9
        },
        total: null,
        spraySelected: null,
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    },
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        cansLeft: {
          857: 10,
          860: 9
        },
        total: null,
        spraySelected: 'Red',
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    }
  ],
  outcome: {
    outcomeDesc: '',
    result: '',
    rewardsGive: {
      itemsRewards: null,
      userRewards: {}
    },
    story: []
  }
}

export const emptyCrimeList = {
  ...initialState,
  crimes: []
}

export const cansLeftChange = {
  ...initialState,
  cansLeft: {
    857: 7,
    860: 9
  }
}

export default initialState
