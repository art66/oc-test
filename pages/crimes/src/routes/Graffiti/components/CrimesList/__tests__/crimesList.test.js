import React from 'react'
import { mount } from 'enzyme'
import { CrimesList } from '../CrimesList'
import initialState, { emptyCrimeList, cansLeftChange } from './mocks/crimesList'

describe('<CrimesList />', () => {
  const {
    attemptProgress: { itemId, inProgress },
    outcome: { story }
  } = initialState

  it('should render whole crimes list', () => {
    const Component = mount(<CrimesList {...initialState} />)
    const crimes = Component.instance()._buildCrimesList()

    expect(crimes.length).toBe(7)
    expect(Component).toMatchSnapshot()
  })
  it('should not render crimes list', () => {
    const Component = mount(<CrimesList {...emptyCrimeList} />)
    const crimes = Component.instance()._buildCrimesList()

    expect(crimes.length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should throw outcome to the 2nd subcrime', () => {
    const Component = mount(<CrimesList {...initialState} />)
    const getCrimesList = Component.instance()._buildCrimesList()
    const otcomeToSubcrime = getCrimesList.filter(crime => itemId === crime.props.crimeID) || false
    const isOutomeMustBeShowed = story && otcomeToSubcrime && !inProgress

    expect(isOutomeMustBeShowed).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should change sprayCans value for all rows that uses the same spray', () => {
    const Component = mount(<CrimesList {...initialState} />)

    expect(Component.find('.graffitiImage').length).toBe(3)
    expect(Component.find('.cansCount').length).toBe(3)
    expect(Component.find('.cansCount').get(0).props.children).toEqual([10, '%'])
    expect(Component.find('.cansCount').get(1).props.children).toEqual([10, '%'])
    expect(Component.find('.cansCount').get(2).props.children).toEqual([10, '%'])

    expect(Component).toMatchSnapshot()
    Component.setProps(cansLeftChange)

    expect(Component.find('.cansCount').get(0).props.children).toEqual([7, '%'])
    expect(Component.find('.cansCount').get(1).props.children).toEqual([7, '%'])
    expect(Component.find('.cansCount').get(2).props.children).toEqual([7, '%'])

    expect(Component).toMatchSnapshot()
  })
})
