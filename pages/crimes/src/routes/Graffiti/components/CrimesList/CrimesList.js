import PropTypes from 'prop-types'
import React, { Component } from 'react'
import CrimeRow from '../CrimeRow/CrimeRow'
import '../../../../styles/subcrimes.scss'

export class CrimesList extends Component {
  static propTypes = {
    startAttempt: PropTypes.func.isRequired,
    rowID: PropTypes.number,
    color: PropTypes.string,
    playerName: PropTypes.string,
    currentLevel: PropTypes.number,
    graffitiLevel: PropTypes.number,
    graffitiColor: PropTypes.string,
    imgBannerLoaded: PropTypes.bool,
    colorsIDs: PropTypes.object,
    startToggleCDs: PropTypes.func,
    outcome: PropTypes.object,
    pushCurrentRow: PropTypes.func,
    attemptProgress: PropTypes.object,
    lastSubCrimeID: PropTypes.number,
    isCurrentRow: PropTypes.bool,
    canSelector: PropTypes.object,
    attemptsTotal: PropTypes.number,
    crimes: PropTypes.array,
    mediaType: PropTypes.string,
    getIncCount: PropTypes.func,
    getOutcome: PropTypes.func,
    currentRowID: PropTypes.number,
    isDesktopLayoutSetted: PropTypes.bool,
    cansLeft: PropTypes.object,
    typeID: PropTypes.number,
    jailed: PropTypes.bool,
    closeOutcome: PropTypes.bool
  }

  static defaultProps = {
    startToggleCDs: () => {},
    pushCurrentRow: () => {},
    getIncCount: () => {},
    getOutcome: () => {},
    rowID: 0,
    color: '',
    playerName: '',
    currentLevel: 0,
    graffitiLevel: 0,
    graffitiColor: '',
    imgBannerLoaded: false,
    colorsIDs: {},
    outcome: {},
    attemptProgress: {},
    lastSubCrimeID: 0,
    isCurrentRow: false,
    canSelector: {},
    attemptsTotal: 0,
    crimes: [],
    mediaType: '',
    currentRowID: 0,
    isDesktopLayoutSetted: false,
    cansLeft: {},
    typeID: null,
    jailed: false
  }

  _attempt = (url, itemId) => {
    const { startAttempt, currentRowID } = this.props
    const onCurrentRow = currentRowID === Number(itemId)

    startAttempt(url, itemId, onCurrentRow)
  }

  _buildCrimesList = () => {
    const renderedCrimeList = []
    const {
      attemptProgress,
      canSelector,
      attemptsTotal,
      crimes,
      mediaType,
      isCurrentRow,
      startToggleCDs,
      outcome,
      color,
      currentRowID,
      lastSubCrimeID,
      colorsIDs,
      getOutcome,
      cansLeft,
      typeID,
      jailed,
      isDesktopLayoutSetted,
      closeOutcome
    } = this.props

    crimes
      && crimes.map((row, i) => {
        const { additionalInfo, showOutcome, sprayColor, rowState, crimeID } = row
        const { total, fontColor, spraySelected, blankSelectedSpray } = additionalInfo

        return renderedCrimeList.push(
          <CrimeRow
            {...row}
            key={i}
            index={i}
            colorsIDs={colorsIDs}
            color={color}
            action={this._attempt}
            jailed={jailed}
            cansLeft={cansLeft}
            typeID={typeID}
            attemptCount={total}
            attemptCountFontColor={fontColor}
            additionalInfo={additionalInfo}
            state={rowState}
            crimeID={crimeID}
            attemptsTotal={attemptsTotal}
            canSelector={canSelector}
            mediaType={mediaType}
            isDesktopLayoutSetted={isDesktopLayoutSetted}
            itemId={attemptProgress.itemId}
            lastSubCrimeID={lastSubCrimeID}
            currentRowID={currentRowID}
            showOutcome={showOutcome}
            isCurrentRow={isCurrentRow}
            outcome={outcome}
            toggleCanSelector={startToggleCDs}
            sprayColor={sprayColor}
            spraySelected={spraySelected}
            blankSelectedSpray={blankSelectedSpray}
            getCurrentOutcome={getOutcome}
            closeOutcome={closeOutcome}
          />
        )
      })

    return renderedCrimeList
  }

  render() {
    return (
      <div className='main-container'>
        <div className='subcrimes-list'>{this._buildCrimesList()}</div>
      </div>
    )
  }
}

export default CrimesList
