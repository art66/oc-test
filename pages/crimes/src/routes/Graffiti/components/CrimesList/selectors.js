import { createSelector } from 'reselect'
import getRowSprays from '../../utils/getRowSpray'

const getRowID = state => state.attemptProgress.itemId
const getColor = state => state.lastSelectedColor
const getPlayerName = state => state.playerName
const getGaffitiLevel = state => state.graffitiLevel
const getGraffitiColor = state => state.graffitiColor
const getFontSize = state => state.fontSize
const getColorsIDs = state => state.colorsIDs
const getCanSelector = state => state.canSelector
const getAttemptsTotal = state => state.currentUserStats.attemptsTotal
const getCrimes = state => state.crimes
const getGlobalCansLeft = state => state.additionalInfo.cansLeft

// reselect functions
export const rowIDState = createSelector([getRowID], itemId => itemId)
export const colorState = createSelector([getColor], color => color)
export const playerNameState = createSelector([getPlayerName], playerName => playerName)
export const graffitiLevelState = createSelector([getGaffitiLevel], graffitiLevel => graffitiLevel)
export const graffitiColorState = createSelector([getGraffitiColor], graffitiColor => graffitiColor)
export const fontSizeState = createSelector([getFontSize], fontSize => fontSize)
export const colorsIDsState = createSelector([getColorsIDs], colorsIDs => colorsIDs)
export const canSelectorState = createSelector([getCanSelector], canSelector => canSelector)
export const attemptsTotalState = createSelector([getAttemptsTotal], attemptsTotal => attemptsTotal)
export const cansLeftState = createSelector([getGlobalCansLeft], canstLeft => canstLeft)

export const updateCrimesWithSprayState = createSelector([getCrimes, getCanSelector], (...props) => {
  const [crimes, canSelector] = props

  const updatedCrimes = crimes.map((row, i) => ({
    ...row,
    sprayColor: getRowSprays(canSelector, i)
  }))

  return updatedCrimes
})
