import { connect } from 'react-redux'
import CrimesList from './CrimesList'
import { toggleCanSelector } from '../../modules/actions'
import { closeOutcome, attempt, getCurrentOutcome } from '../../../../modules/actions'
import getGlobalSelector from '../../../../utils/globalListPropsSelector'

import {
  rowIDState,
  colorState,
  playerNameState,
  graffitiLevelState,
  graffitiColorState,
  fontSizeState,
  colorsIDsState,
  canSelectorState,
  attemptsTotalState,
  cansLeftState,
  updateCrimesWithSprayState
} from './selectors'

const mapStateToProps = ({ graffiti, browser, common }) => {
  const updatedGraffitiGlobalData = getGlobalSelector(graffiti, browser, common)

  const graffitiUpdatedState = {
    ...graffiti,
    ...updatedGraffitiGlobalData
  }

  return {
    ...updatedGraffitiGlobalData,
    crimes: updateCrimesWithSprayState(graffitiUpdatedState),
    rowID: rowIDState(graffiti),
    color: colorState(graffiti),
    playerName: playerNameState(graffiti),
    graffitiLevel: graffitiLevelState(graffiti),
    graffitiColor: graffitiColorState(graffiti),
    fontSize: fontSizeState(graffiti),
    colorsIDs: colorsIDsState(graffiti),
    canSelector: canSelectorState(graffiti),
    attemptsTotal: attemptsTotalState(graffiti),
    cansLeft: cansLeftState(graffiti)
  }
}

const mapDispatchToProps = dispatch => ({
  closeOutcome: () => dispatch(closeOutcome()),
  startAttempt: (url, itemId, isCurrentRow) => dispatch(attempt('graffiti', url, itemId, isCurrentRow)),
  startToggleCDs: value => dispatch(toggleCanSelector(value)),
  getOutcome: (outcomeHeight, outcomeOffestTop) => dispatch(getCurrentOutcome(outcomeHeight, outcomeOffestTop))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrimesList)
