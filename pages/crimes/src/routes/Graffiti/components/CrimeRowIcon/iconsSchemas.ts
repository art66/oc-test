const iconsSchema = {
  iconName: 'CrimeCity',
  12: {
    type: 'westSide',
    dimensions: {
      width: '35px',
      height: '23px',
      viewbox: '0 0 38 26'
    }
  },
  13: {
    type: 'northSide',
    dimensions: {
      width: '35px',
      height: '21px',
      viewbox: '0 0 37 23'
    }
  },
  14: {
    type: 'redLights',
    dimensions: {
      width: '35px',
      height: '14px',
      viewbox: '0 0 37 16'
    }
  },
  15: {
    type: 'residential',
    dimensions: {
      width: '35px',
      height: '23px',
      viewbox: '0 0 37 26'
    }
  },
  16: {
    type: 'default',
    dimensions: {
      width: '35px',
      height: '32px',
      viewbox: '0 0 37 37'
    }
  },
  17: {
    type: 'financial',
    dimensions: {
      width: '35px',
      height: '32px',
      viewbox: '0 0 37 37'
    }
  },
  18: {
    type: 'eastSide',
    dimensions: {
      width: '35px',
      height: '23px',
      viewbox: '0 0 37 26'
    }
  }
}

const gradientSchema = {
  transform: 'rotate(90)',
  layoutTypes: {
    5: {
      ID: 'crimes_5',
      scheme: [{ step: '0', color: '#ffdc73' }, { step: '1', color: '#dda80b' }]
    },
    4: {
      ID: 'crimes_4',
      scheme: [{ step: '0', color: '#EDD384' }, { step: '1', color: '#C29B25' }]
    },
    3: {
      ID: 'crimes_3',
      scheme: [{ step: '0', color: '#DDCC9A' }, { step: '1', color: '#AC9348' }]
    },
    2: {
      ID: 'crimes_2',
      scheme: [{ step: '0', color: '#D1C9B2' }, { step: '1', color: '#9E9370' }]
    },
    1: {
      ID: 'crimes_1',
      scheme: [{ step: '0', color: '#D6CFBA' }, { step: '1', color: '#A99F7F' }]
    },
    0: {
      ID: 'crimes_0',
      scheme: [{ step: '0', color: '#C9C9C7' }, { step: '1', color: '#959594' }]
    }
  }
}

export { iconsSchema, gradientSchema }
