import React from 'react'
import SVGIcon from '../../../../components/SVGIcon'

import { iconsSchema, gradientSchema } from './iconsSchemas'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

class CrimeRowIcon extends React.PureComponent<IProps> {
  render() {
    const { crimeID, cityLevel } = this.props
    const { ID, scheme } = gradientSchema.layoutTypes[cityLevel]

    const iconProps = {
      iconName: iconsSchema.iconName,
      ...iconsSchema[crimeID],
      fill: {
        color: `url(#crimes_${cityLevel})`
      },
      gradient: {
        active: true,
        ID,
        scheme,
        transform: gradientSchema.transform
      },
      preset: {
        type: 'crimes',
        subtype: 'GRAFFITI'
      }
    }

    return (
      <div className={styles.svgIconWrap}>
        <SVGIcon iconProps={iconProps} />
      </div>
    )
  }
}

export default CrimeRowIcon
