import { connect } from 'react-redux'
import CanSelector from './CanSelector'
import { selectCan, isCanSelectorVisible } from '../../modules/actions'

const mapStateToProps = state => ({
  id: state.graffiti.canSelector.ID,
  usedCrimeID: state.graffiti.canSelector.usedCrimeID,
  visible: state.graffiti.canSelector.visible,
  colorsIDs: state.graffiti.colorsIDs,
  items: state.graffiti.canSelector.items,
  mediaType: state.browser.mediaType,
  pos: state.graffiti.canSelector.pos,
  outcomeHeight: state.common.outcomeHeight,
  outcomeOffestTop: state.common.outcomeOffestTop,
  isDesktopLayoutSetted: state.common.isDesktopLayoutSetted
})

const mapDispatchToProps = dispatch => ({
  startSelectCan: (color, id) => dispatch(selectCan(color, id)),
  startIsCanSelectorVisible: value => dispatch(isCanSelectorVisible(value))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CanSelector)
