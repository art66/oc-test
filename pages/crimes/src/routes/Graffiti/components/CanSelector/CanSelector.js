import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { SELECTOR_POS_LEFT_GRAFFITI, ROW_HEIGHT } from '../../constants'
import { Popup } from '../../../../components'
import styles from '../../../../styles/graffiti.cssmodule.scss'

const CTA_TEXT = 'Select Spray Color'

export class CanSelector extends Component {
  static propTypes = {
    id: PropTypes.number,
    usedCrimeID: PropTypes.number,
    visible: PropTypes.bool,
    colorsIDs: PropTypes.object,
    items: PropTypes.array.isRequired,
    startSelectCan: PropTypes.func,
    mediaType: PropTypes.string,
    startIsCanSelectorVisible: PropTypes.func,
    outcomeHeight: PropTypes.number,
    outcomeOffestTop: PropTypes.number,
    isDesktopLayoutSetted: PropTypes.bool
  }

  static defaultProps = {
    id: null,
    usedCrimeID: null,
    visible: false,
    colorsIDs: {},
    mediaType: '',
    outcomeHeight: 0,
    outcomeOffestTop: 0,
    isDesktopLayoutSetted: false,
    startSelectCan: () => {},
    startIsCanSelectorVisible: () => {}
  }

  _handleSelect = ({ target: item }) => {
    const itemAvailable = item.getAttribute('data-available')

    if (itemAvailable === 'false') return

    const { startSelectCan, usedCrimeID } = this.props
    const itemColor = item.getAttribute('data-color')

    startSelectCan(itemColor, usedCrimeID)
  }

  _closePopup = () => {
    const { startIsCanSelectorVisible } = this.props

    startIsCanSelectorVisible(false)
  }

  _sprayItemsRender = () => {
    const { items } = this.props

    const itemsList = items.map((item, i) => {
      return (
        <button key={i} type='button' className={styles.sprayCan} onMouseDown={this._handleSelect}>
          {this._sprayItemImgRender(item)}
        </button>
      )
    })

    return itemsList
  }

  _sprayItemImgRender = item => {
    const { colorsIDs } = this.props
    const isImgToShow = !item.available ? 'blank' : 'medium'

    return (
      <img
        className={styles.sprayImg}
        src={`/images/items/${colorsIDs[item.color]}/${isImgToShow}.png`}
        data-color={item.color}
        data-available={item.available}
        alt='item'
      />
    )
  }

  render() {
    const { mediaType, id, outcomeHeight, outcomeOffestTop, visible, isDesktopLayoutSetted } = this.props

    const actualMediaType = isDesktopLayoutSetted ? 'desktop' : mediaType

    const pos = {
      left: SELECTOR_POS_LEFT_GRAFFITI[actualMediaType].left,
      top: SELECTOR_POS_LEFT_GRAFFITI[actualMediaType].top + id * ROW_HEIGHT
    }

    if (!visible) return null

    return (
      <Popup
        closePopUp={this._closePopup}
        pos={pos}
        styleRelative='true'
        outcomeHeight={outcomeHeight}
        outcomeOffestTop={outcomeOffestTop}
      >
        <div ref={this.node} className={styles.sprayCanWrap}>
          {this._sprayItemsRender()}
        </div>
        <div className='p10 text-a-center'>{CTA_TEXT}</div>
        <div className='tooltip-arrow left bottom' style={{ top }} />
      </Popup>
    )
  }
}

export default CanSelector
