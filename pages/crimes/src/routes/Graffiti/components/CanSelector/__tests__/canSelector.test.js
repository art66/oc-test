import React from 'react'
import { mount, shallow } from 'enzyme'
import CanSelector from '../CanSelector'
import initialState, { tabletlayout, mobilelayout, disabledlayout, manualDesktopMode } from './mocks/canSelector'

describe('<CanSelector />', () => {
  it('should not render graffiti Popup', () => {
    const Component = shallow(<CanSelector {...disabledlayout} />)

    expect(Component.find('.sprayCanWrap').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render graffiti Popup on Desktop', () => {
    const Component = shallow(<CanSelector {...initialState} />)
    const { left, top } = Component.props().pos

    expect(Component.find('.sprayCanWrap').length).toBe(1)
    expect(left).toBe(391)
    expect(top).toBe(105)
    expect(Component).toMatchSnapshot()
  })
  it('should render desktop graffiti Popup in case of manualDesktopMode activated', () => {
    const Component = shallow(<CanSelector {...manualDesktopMode} />)
    const { left, top } = Component.props().pos

    expect(Component.find('.sprayCanWrap').length).toBe(1)
    expect(left).toBe(391)
    expect(top).toBe(105)
    expect(Component).toMatchSnapshot()
  })
  it('should render graffiti Popup on Tablet', () => {
    const Component = shallow(<CanSelector {...tabletlayout} />)
    const { left, top } = Component.props().pos

    expect(Component.find('.sprayCanWrap').length).toBe(1)
    expect(left).toBe(146)
    expect(top).toBe(104)
    expect(Component).toMatchSnapshot()
  })
  it('should render graffiti Popup on Mobile', () => {
    const Component = shallow(<CanSelector {...mobilelayout} />)
    const { left, top } = Component.props().pos

    expect(Component.find('.sprayCanWrap').length).toBe(1)
    expect(left).toBe(80)
    expect(top).toBe(104)
    expect(Component).toMatchSnapshot()
  })
  it('should select spray by certain spray click', () => {
    const Component = mount(<CanSelector {...initialState} />)

    expect(Component.find('.sprayCanWrap').length).toBe(1)
    expect(Component.find({ src: '/images/items/857/medium.png' }).length).toBe(3)
    expect(
      Component.find('Popup')
        .first()
        .simulate('click')
    ).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('should close PopUp on any other click', () => {
    const Component = mount(<CanSelector {...initialState} />)

    expect(Component.find('.sprayCanWrap').length).toBe(1)
    expect(Component.find({ src: '/images/items/857/medium.png' }).length).toBe(3)
    expect(Component.find('Popup').simulate('click')).toBeTruthy()
    expect(Component).toMatchSnapshot()

    Component.setProps({ visible: false })
    expect(Component.find('.sprayCanWrap').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
