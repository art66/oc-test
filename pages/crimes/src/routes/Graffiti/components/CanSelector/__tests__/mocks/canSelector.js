const initialState = {
  startSelectCan: () => {},
  id: 0,
  items: [
    {
      available: true,
      color: 'Red'
    },
    {
      available: true,
      color: 'Black'
    },
    {
      available: true,
      color: 'Green'
    },
    {
      available: false,
      color: 'Green'
    }
  ],
  mediaType: 'desktop',
  colorsIDs: {
    Red: 857,
    Black: 857,
    Green: 857
  },
  outcomeHeight: 81,
  outcomeOffestTop: 0,
  visible: true
}

export const tabletlayout = {
  ...initialState,
  mediaType: 'tablet'
}

export const mobilelayout = {
  ...initialState,
  mediaType: 'mobile'
}

export const disabledlayout = {
  ...initialState,
  visible: false
}

export const manualDesktopMode = {
  ...initialState,
  isDesktopLayoutSetted: true
}

export default initialState
