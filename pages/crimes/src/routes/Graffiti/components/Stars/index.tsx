import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class Stars extends React.PureComponent<IProps> {
  render() {
    const { isGoldenIcon } = this.props

    const iconProps = {
      iconName: 'Star',
      fill: {
        color: isGoldenIcon ? '#DCA709' : '#CCCCCC',
        stroke: '0',
        strokeWidth: '0'
      },
      dimensions: {
        width: '5px',
        height: '5px',
        viewbox: '0 0 5 5'
      },
      filter: {
        active: false
      }
    }

    return (
      <div className={styles.svgIconStar}>
        <SVGIconGenerator {...iconProps} />
      </div>
    )
  }
}

export default Stars
