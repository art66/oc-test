import React from 'react'
import { shallow } from 'enzyme'
import Skeleton from '..'

describe('<CrimeRowPlaceholder />', () => {
  it('should render crimeRow', () => {
    const Component = shallow(<Skeleton />)

    expect(Component.find('.rowsWrap').length).toBe(1)
    expect(Component.find('.rowWrap').length).toBe(7)
    expect(Component).toMatchSnapshot()
  })
})
