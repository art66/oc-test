import React from 'react'

import Placeholder from '../../../../components/Skeleton'

import globalSkeleton from '../../../../styles/skeleton.cssmodule.scss'
import styles from './styles.cssmodule.scss'

const CrimeRowPlaceholder = ({ type }: { type: string }) => {
  const rowsList = () => Array.from(Array(7).keys()).map(key => (
    <div className={globalSkeleton.rowWrap} key={key}>
      <div className={globalSkeleton.titleImg} />
      <div className={`${globalSkeleton.titleText} ${globalSkeleton.titleTextUp} ${styles.title}`} />
      <div className={styles.cityContainer}>
        <div className={styles.areaIcon} />
        <div className={styles.starCounter} />
      </div>
      <div className={`${globalSkeleton.itemContainer} ${globalSkeleton.itemWithNose} ${styles.itemContainer}`}>
        <div className={globalSkeleton.addItemNose} />
      </div>
      <div className={`${globalSkeleton.nerveContainer} ${styles.nerve}`} />
      <div className={globalSkeleton.buttonAction} />
    </div>
  ))

  return (
    <Placeholder type={type}>
      <div className={globalSkeleton.rowsWrap}>
        {rowsList()}
      </div>
    </Placeholder>
  )
}

export default CrimeRowPlaceholder
