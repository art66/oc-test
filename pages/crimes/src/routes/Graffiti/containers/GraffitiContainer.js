import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { clearOutdatedOutcome } from '../../../modules/actions'

import { CrimesList, Banners, CanSelector, Skeleton } from '../components'
import SubCrimesLayout from '../../../components/SubCrimesLayout'

class GraffitiContainer extends PureComponent {
  static propTypes = {
    state: PropTypes.object,
    showPlaceholder: PropTypes.bool,
    isBannerFetched: PropTypes.bool,
    isBannerImagesFetched: PropTypes.bool,
    bannerMainImages: PropTypes.array,
    canSelector: PropTypes.object,
    current: PropTypes.number,
    progress: PropTypes.number,
    stats: PropTypes.array,
    mediaType: PropTypes.string,
    startClearOutdatedOutcome: PropTypes.func
  }

  static defaultProps = {
    state: {},
    startClearOutdatedOutcome: () => {},
    showPlaceholder: true,
    isBannerFetched: false,
    isBannerImagesFetched: false,
    bannerMainImages: [],
    canSelector: {},
    current: 0,
    progress: 0,
    stats: [],
    mediaType: ''
  }

  componentWillUnmount() {
    const {
      startClearOutdatedOutcome,
      state: { graffiti }
    } = this.props
    const { outcome: { story } = {} } = graffiti || {}

    if (!story || (graffiti && story.length !== 0)) {
      startClearOutdatedOutcome()
    }
  }

  render() {
    const {
      canSelector,
      current,
      progress,
      stats,
      showPlaceholder,
      isBannerFetched,
      isBannerImagesFetched,
      bannerMainImages,
      mediaType
    } = this.props

    const BannerRender = <Banners
      mediaType={mediaType}
      isBannerImagesFetched={isBannerImagesFetched}
      bannerMainImages={bannerMainImages}
    />

    return (
      <SubCrimesLayout
        customWrapClass='graffiti'
        showPlaceholder={showPlaceholder || !isBannerFetched}
        title='Graffiti'
        skeleton={Skeleton}
        banners={BannerRender}
        stats={stats}
        current={current}
        progress={progress}
      >
        {canSelector.visible && <CanSelector />}
        <CrimesList />
      </SubCrimesLayout>
    )
  }
}

const mapStateToProps = state => ({
  stat: state.graffiti,
  isBannerImagesFetched: state.graffiti.isBannerImagesFetched,
  bannerMainImages: state.graffiti.bannerMainImages,
  isBannerFetched: state.graffiti.isBannerFetched,
  showPlaceholder: state.graffiti.showPlaceholder,
  canSelector: state.graffiti.canSelector,
  current: state.graffiti.currentLevel,
  progress: state.graffiti.skillLevelTotal,
  stats: state.graffiti.currentUserStatistics,
  mediaType: state.browser.mediaType,
  freeze: state.common.freeze
})

const mapDispatchToProps = dispatch => ({
  startClearOutdatedOutcome: () => dispatch(clearOutdatedOutcome('graffiti'))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GraffitiContainer)
