const getRowSprays = (spraySelector, index) => {
  let sprayColor = ''
  const isItems = spraySelector.selectedItems.length >= 1
  const isSomeItemsSelected = typeof spraySelector.selectedItems !== 'undefined'
  const { selectedItems = [] } = spraySelector

  if (isSomeItemsSelected && isItems) {
    sprayColor = selectedItems.forEach(item => {
      if (item.ID !== index) return null

      return item.selectedItem
    })
  }

  return sprayColor
}

export default getRowSprays
