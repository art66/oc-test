const graffitiRowsUpdate = (prevProps, nextProps) => {
  const { spraySelected, sprayCansCount, additionalInfo: { sprayRequestProgress } } = prevProps

  // update row that chenged own spray color
  if (spraySelected !== nextProps.spraySelected) {
    return true
  }

  // update all rows if SprayColor is changed
  if (sprayCansCount !== nextProps.nextSprayCount) {
    return true
  }

  // update row if spray in request
  if (sprayRequestProgress !== nextProps.additionalInfo.sprayRequestProgress) {
    return true
  }
}

export { graffitiRowsUpdate }
