export const TYPE_ID = 3
export const BANNER_COLOR_URL = '&step=graffitiInfo'
export const IMG_ROOT_URL = 'loader2.php?sid=crimesData&step=graffitiBanner'
export const IMG_FONT_SIZE = '&numberFont='
export const IMG_COLOR = '&color='
export const IMG_RESOLUTION = '&resolution='
export const IMG_NUMBER = '&number='
export const IMG_FONT_NUMBER = '&numberFont='
export const SPRAY_SELECTION = 'prepare'

// ------------------------------------
// GRAFFITI ROW HEIGHT
// ------------------------------------
export const ROW_HEIGHT = 51

// ------------------------------------
// PopUp Coords Constants
// ------------------------------------
export const SELECTOR_POS_LEFT_GRAFFITI = {
  desktop: { left: 391, top: 105 },
  tablet: { left: 146, top: 104 },
  mobile: { left: 80, top: 104 }
}

// ------------------------------------
// Banner Dimensions Constants
// ------------------------------------
export const RESOLUTIONS = {
  desktop: 976,
  tablet: 578,
  mobile: 320
}

export const RESOLUTIONS_REVERTS = {
  976: 'desktop',
  578: 'tablet',
  320: 'mobile'
}

// ------------------------------------
// Banner Animation Constants
// ------------------------------------
export const BANNER_ANIMATION_DURATION = 1000

// ------------------------------------
// Banner LocalStorage Constants
// ------------------------------------
export const CRIME_IMAGE_DATA_FETCHED = 'CrimeImage_DataFetched'
export const CRIME_IMAGE_ATTEMPT = 'CrimeImage_Attempt'

// ------------------------------------
// Banner Count Constants
// ------------------------------------
export const BANNERS_MIN = 0
export const BANNERS_MAX = 19

// ------------------------------------
// Banner Colors IDs
// ------------------------------------
export const colorIDs = {
  856: 'Black',
  857: 'Red',
  858: 'Pink',
  859: 'Purple',
  860: 'Blue',
  861: 'Green',
  862: 'Yellow',
  863: 'Orange'
}

// ------------------------------------
// Spray Fonts
// ------------------------------------
export const SPRAY_RANDOM_HOLDER = {
  1: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
  2: [19, 20, 21, 22, 23, 24, 25, 26],
  3: [27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42],
  4: [43, 44, 45, 46, 47, 48, 49, 50, 51, 52],
  5: [53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63],
  6: [64, 65, 66, 67, 68]
}

// ------------------------------------
// Spray Fonts Separation by LVL Borders
// ------------------------------------
export const BEGINNER_LVL = 17
export const LOW_MIDDLE_LVL = 34
export const MIDDLE_LVL = 50
export const STRONG_MIDDLE_LVL = 67
export const SENIOR_LVL = 84
export const MASTER_LVL = 100

export const BEGINNER = 1
export const LOW_MIDDLE = 2
export const MIDDLE = 3
export const STRONG_MIDDLE = 4
export const SENIOR = 5
export const MASTER = 6

// ------------------------------------
// Action Constants
// ------------------------------------
export const ATTEMPT = 'graffiti/ATTEMPT'
export const FETCH_DATA = 'graffiti/FETCH_DATA'
export const DATA_FETCH_START = 'DATA_FETCH_START'
export const DATA_FETCHED = 'graffiti/DATA_FETCHED'
export const ATTEMPT_START = 'graffiti/ATTEMPT_START'
export const ATTEMPT_FETCHED = 'graffiti/ATTEMPT_FETCHED'
export const TOGGLE_CAN_SELECTOR = 'graffiti/TOGGLE_CAN_SELECTOR'
export const IS_CAN_SELECTOR_VISIBLE = 'graffiti/IS_CAN_SELECTOR_VISIBLE'
export const SELECT_CAN_START = 'graffiti/SELECT_CAN_START'
export const SELECT_CAN_FINISH = 'graffiti/SELECT_CAN_FINISH'
export const IMG_PLAYER_NAME = '&playerName='
export const SET_CRIME_LEVEL = 'graffiti/SET_CRIME_LEVEL'
export const GET_INC_COUNT = 'graffiti/GET_INC_COUNT'
export const CURRENT_ROW = 'graffiti/CURRENT_ROW'
export const FETCH_IMG = 'graffiti/FETCH_IMG'
export const FETCH_BANNER_COLOR = 'FETCH_BANNER_COLOR'
export const FETCH_BANNER_IMG_START = 'graffiti/FETCH_BANNER_IMG_START'
export const BANNER_IMG_LOADED = 'graffiti/BANNER_IMG_LOADED'
export const REPLACE_MAIN_BANNER_IMAGE = 'graffiti/REPLACE_MAIN_BANNER_IMAGE'
export const SET_MAIN_BANNER_IMGS = 'graffiti/SET_MAIN_BANNER_IMGS'
export const SET_SUPPORT_BANNER_IMGS = 'graffiti/SET_SUPPORT_BANNER_IMGS'
export const GET_OUTCOME_HIGHT = 'graffiti/GET_OUTCOME_HIGHT'
export const CLEAR_OUTDATED_OUTCOME = 'graffiti/CLEAR_OUTDATED_OUTCOME'
export const MOUNT = 'graffiti/MOUNT'
export const RESET_STATS_DONE = 'graffiti/RESET_STATS_DONE'
export const PRELOAD_NEXT_BANNERS = 'graffiti/PRELOAD_NEXT_BANNERS'
export const SET_BANNER_IMAGE_URLS = 'graffiti/SET_BANNER_IMAGE_URLS'
export const MAIN_IMAGES_PRELOAD = 'graffiti/MAIN_IMAGES_PRELOAD'
export const CLOSE_OUTCOME = 'graffiti/CLOSE_OUTCOME'
