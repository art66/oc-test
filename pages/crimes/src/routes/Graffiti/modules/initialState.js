export default {
  attemptProgress: {
    inProgress: false,
    itemId: 0
  },
  typeID: 3,
  isBannerImagesFetched: false,
  showPlaceholder: true,
  isBannerFetched: false,
  isMainBannerImagesFetched: false,
  currentRowID: null,
  lastSelectedColor: 'Black',
  user: {
    playername: 'TornCity'
  },
  graffitiColor: '333333',
  graffitiLevel: 1,
  bannerSupportImages: [
    // {
    //   resolution: '',
    //   imgUrl: ''
    // }
  ],
  bannerMainImages: [
    // {
    //   resolution: '',
    //   imgUrl: ''
    // },
    // {
    //   resolution: '',
    //   imgUrl: ''
    // },
    // {
    //   resolution: '',
    //   imgUrl: ''
    // }
  ],
  canSelector: {
    selectedItems: [],
    visible: false
  },
  isCurrentRow: false,
  colorsIDs: {
    Black: 856,
    Red: 857,
    Pink: 858,
    Purple: 859,
    Blue: 860,
    Green: 861,
    Yellow: 862,
    Orange: 863
  },
  colorsSpray: {
    Black: '333333',
    Red: 'FF0000',
    Pink: 'D24DFF',
    Purple: 'D4DFF',
    Blue: '26C9FF',
    Green: 'A3D3900',
    Yellow: 'FFBF00',
    Orange: 'FF8000'
  },
  crimesByType: [
    {
      currentTemplate: 'Graffiti',
      skillLevel: 0,
      skillLevelTotal: 0,
      title: '',
      nerve: 0,
      iconClass: '',
      crimeID: 0,
      subID: 0,
      requirements: {},
      available: true,
      additionalInfo: {
        sprayRequestProgress: true,
        cansLeft: null,
        total: null,
        spraySelected: '',
        blankSelectedSpray: false,
        availableCans: [
          {
            color: '',
            itemID: 0,
            available: true,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '0',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          },
          {
            color: '',
            itemID: 0,
            available: false,
            value: 0
          }
        ]
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: ''
    }
  ],
  additionalInfo: {
    cansLeft: {},
    lastUsedCan: ''
  },
  outcome: {
    outcomeDesc: '',
    result: '',
    rewardsGive: {
      itemsRewards: null,
      userRewards: {}
    },
    story: []
  },
  currentType: {
    _id: {
      $oid: '58983724c469886d558b4567'
    },
    ID: 3,
    typeID: 3,
    title: 'Graffiti',
    crimeRoute: 'graffiti',
    active: 1,
    img: '/images/v2/2sef32sdfr422svbfr2/crimes/placeholder/placeholder_1.jpg',
    expForLevel100: 0
  },
  skillLevelTotal: 0,
  currentLevel: null,
  lastSubCrimeID: 0,
  currentUserStats: {
    failedTotal: 0,
    attemptsTotal: 0,
    crimesByIDAttempts: {
      12: 0,
      13: 0,
      14: 0,
      15: 0,
      16: 0,
      17: 0,
      18: 0
    },
    jailedTotal: 0,
    successesTotal: 0,
    critFailedTotal: 0,
    statsByType: {
      3: {
        successesTotal: 0
      }
    },
    skill: 0,
    skillLevel: 0
  },
  currentUserStatistics: []
}
