// @param
// @depracated
// Currently deprecated due to static font size choosing.

import {
  SPRAY_RANDOM_HOLDER,
  BEGINNER_LVL,
  LOW_MIDDLE_LVL,
  MIDDLE_LVL,
  STRONG_MIDDLE_LVL,
  SENIOR_LVL,
  MASTER_LVL,
  BEGINNER,
  LOW_MIDDLE,
  MIDDLE,
  STRONG_MIDDLE,
  SENIOR,
  MASTER
} from '../constants'

const fontRandom = userLvlStage => {
  return Math.floor(Math.random() * SPRAY_RANDOM_HOLDER[userLvlStage].length)
}

const getFontRandomImg = currentUserLvl => {
  if (currentUserLvl <= BEGINNER_LVL) {
    return fontRandom(BEGINNER)
  } else if (currentUserLvl <= LOW_MIDDLE_LVL) {
    return fontRandom(LOW_MIDDLE)
  } else if (currentUserLvl <= MIDDLE_LVL) {
    return fontRandom(MIDDLE)
  } else if (currentUserLvl <= STRONG_MIDDLE_LVL) {
    return fontRandom(STRONG_MIDDLE)
  } else if (currentUserLvl <= SENIOR_LVL) {
    return fontRandom(SENIOR)
  } else if (currentUserLvl <= MASTER_LVL) {
    return fontRandom(MASTER)
  }

  return null
}

export default getFontRandomImg
