import reducer from '../reducers.js'
import { toggleCanSelector } from '../actions.js'
import mocks from './mocks'

describe('test TOGGLE_CAN_SELECTOR', () => {
  it('should update visible state', () => {
    const toggle = toggleCanSelector(mocks)
    const actionToReducer = reducer(
      {
        canSelector: {
          visible: false,
          ID: 0,
          selectedItems: []
        }
      },
      toggle
    )
    const newState = {
      canSelector: {
        visible: true,
        ID: 50,
        items: {},
        selectedItems: []
      }
    }

    expect(toggle).toEqual(mocks)
    expect(actionToReducer).toEqual(newState)
    expect(newState).toMatchSnapshot()
  })
})
