import { put } from 'redux-saga/effects'
import { fetchImgUrl } from '@torn/shared/utils'

import { showDebugInfo } from '../../../modules/actions'
import { preloadImagesNotify } from './actions'

function* fetchBannerImgs(imgURLsArray: { imgUrl: string }[], type: 'main' | 'support') {
  try {
    yield imgURLsArray.forEach(img => fetchImgUrl(img.imgUrl))

    yield put(preloadImagesNotify({ type: type.toUpperCase() }))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}


export default fetchBannerImgs
