import {
  FETCH_IMG,
  FETCH_BANNER_COLOR,
  FETCH_BANNER_IMG_START,
  BANNER_IMG_LOADED,
  TOGGLE_CAN_SELECTOR,
  IS_CAN_SELECTOR_VISIBLE,
  SELECT_CAN_START,
  SELECT_CAN_FINISH,
  CURRENT_ROW,
  REPLACE_MAIN_BANNER_IMAGE,
  SET_MAIN_BANNER_IMGS,
  SET_SUPPORT_BANNER_IMGS,
  GET_OUTCOME_HIGHT,
  CLEAR_OUTDATED_OUTCOME,
  PRELOAD_NEXT_BANNERS
} from '../constants'

// ------------------------------------
// Actions
// ------------------------------------
export const fetchBannerColor = bannerColor => ({
  type: FETCH_BANNER_COLOR,
  bannerColor
})

export const bannerFetchingStart = () => ({
  type: FETCH_BANNER_IMG_START,
  status: false
})

export const bannerFetchingFinish = () => ({
  type: BANNER_IMG_LOADED,
  status: true
})

export const fetchImage = () => ({
  type: FETCH_IMG
})

export const toggleCanSelector = ({ ID, crimeID, items, visible }) => ({
  type: TOGGLE_CAN_SELECTOR,
  ID,
  items,
  visible,
  crimeID
})

export const isCanSelectorVisible = visible => ({
  type: IS_CAN_SELECTOR_VISIBLE,
  visible
})

export const selectCan = (item, id) => ({
  type: SELECT_CAN_START,
  isSelectCanProgress: true,
  item,
  id
})

export const sprayAddingFinished = ({ payload, selectedSpray }) => ({
  type: SELECT_CAN_FINISH,
  isSelectCanProgress: false,
  payload,
  selectedSpray
})

export const getCurrentRow = (currentRowID, isCurrentRow) => ({
  type: CURRENT_ROW,
  currentRowID,
  isCurrentRow
})

export const replaceMainBannerImage = () => ({
  type: REPLACE_MAIN_BANNER_IMAGE
})

export const setMainBannerImages = imgSet => ({
  type: SET_MAIN_BANNER_IMGS,
  imgSet
})

export const setSupportBannerImages = imgSet => ({
  type: SET_SUPPORT_BANNER_IMGS,
  imgSet
})

export const getCurrentOutcome = (outcomeHeight, outcomeOffestTop) => ({
  type: GET_OUTCOME_HIGHT,
  outcomeHeight,
  outcomeOffestTop
})

export const clearOutdatedOutcome = () => ({
  type: CLEAR_OUTDATED_OUTCOME
})

export const preloadNextBanners = () => ({
  type: PRELOAD_NEXT_BANNERS
})

export const preloadImagesNotify = ({ type }) => ({
  type: `graffiti/${type}_IMAGES_PRELOAD`
})
