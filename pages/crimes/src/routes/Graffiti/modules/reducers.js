import initialState from './initialState'
import { LOCATION_CHANGE } from '../../../constants'
import {
  DATA_FETCH_START,
  DATA_FETCHED,
  ATTEMPT_START,
  ATTEMPT_FETCHED,
  TOGGLE_CAN_SELECTOR,
  IS_CAN_SELECTOR_VISIBLE,
  SELECT_CAN_START,
  SELECT_CAN_FINISH,
  SET_CRIME_LEVEL,
  REPLACE_MAIN_BANNER_IMAGE,
  SET_MAIN_BANNER_IMGS,
  SET_SUPPORT_BANNER_IMGS,
  CLEAR_OUTDATED_OUTCOME,
  RESET_STATS_DONE,
  FETCH_BANNER_COLOR,
  FETCH_BANNER_IMG_START,
  BANNER_IMG_LOADED,
  MAIN_IMAGES_PRELOAD,
  CLOSE_OUTCOME
} from '../constants'

const FULL_SPRAY_CAPACITY = 100

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [DATA_FETCH_START]: state => ({
    ...state
  }),
  [DATA_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    showPlaceholder: false,
    lastSelectedColor: action.json.DB?.additionalInfo?.lastUsedCan,
    graffitiColor: action.json.DB?.lastUsedCan,
    graffitiLevel: action.json.DB.currentLevel
  }),
  [ATTEMPT_START]: (state, action) => ({
    ...state,
    attemptProgress: {
      itemId: action.itemId,
      inProgress: true
    },
    currentRowID: action.itemId,
    isCurrentRow: action.isCurrentRow
  }),
  [ATTEMPT_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    additionalInfo: {
      ...state.additionalInfo,
      ...action.json.DB.additionalInfo,
      cansLeft: {
        ...state.additionalInfo.cansLeft,
        ...action.json.DB.additionalInfo.cansLeft
      }
    },
    graffitiLevel: action.json.DB.currentLevel,
    lastSelectedColor: action.json.DB.additionalInfo.spraySelected,
    bannerColor: action.json.DB.additionalInfo.spraySelected,
    attemptProgress: {
      itemId: state.attemptProgress.itemId,
      inProgress: false,
      imgBannerCached: true
    },
    crimesByType: state.crimesByType.map(crime => {
      return crime.crimeID === state.currentRowID ?
        {
          ...crime,
          additionalInfo: {
            ...crime.additionalInfo,
            ...action.json.DB.additionalInfo,
            blankSelectedSpray: false
          }
        } :
        {
          ...crime,
          additionalInfo: {
            ...crime.additionalInfo,
            spraySelected:
                action.json.DB.additionalInfo.cansLeft[state.colorsIDs[crime.additionalInfo.spraySelected]] > 0 ?
                  crime.additionalInfo.spraySelected :
                  ''
          }
        }
    })
  }),
  [TOGGLE_CAN_SELECTOR]: (state, action) => ({
    ...state,
    canSelector: {
      ...state.canSelector,
      items: action.items,
      selectedItems: state.canSelector.selectedItems,
      ID: action.ID,
      usedCrimeID: action.crimeID,
      visible: action.visible
    }
  }),
  [IS_CAN_SELECTOR_VISIBLE]: (state, action) => ({
    ...state,
    canSelector: {
      ...state.canSelector,
      visible: action.visible
    }
  }),
  [SELECT_CAN_START]: (state, action) => ({
    ...state,
    lastSelectedColor: action.item,
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === action.id ?
      {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          spraySelected: null,
          sprayRequestProgress: action.isSelectCanProgress
        }
      } :
      crime))
  }),
  [SELECT_CAN_FINISH]: (state, action) => ({
    ...state,
    additionalInfo: {
      ...state.additionalInfo,
      cansLeft: {
        ...state.additionalInfo.cansLeft,
        [state.colorsIDs[action.selectedSpray.item]]:
          state.additionalInfo.cansLeft[state.colorsIDs[action.selectedSpray.item]] || FULL_SPRAY_CAPACITY
      }
    },
    canSelector: {
      ...state.canSelector,
      visible: false,
      selectedItems: [
        ...state.canSelector.selectedItems,
        {
          selectedItem: action.selectedSpray.item,
          ID: action.selectedSpray.id
        }
      ]
    },
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === action.selectedSpray.id ?
      {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          spraySelected: action.selectedSpray.item,
          sprayRequestProgress: action.isSelectCanProgress,
          blankSelectedSpray: true
        }
      } :
      crime))
  }),
  [SET_CRIME_LEVEL]: (state, action) => ({
    ...state,
    ...action.result.DB
  }),
  [REPLACE_MAIN_BANNER_IMAGE]: state => ({
    ...state,
    bannerMainImages: state.bannerSupportImages
  }),
  [SET_MAIN_BANNER_IMGS]: (state, action) => ({
    ...state,
    isBannerImagesFetched: true,
    bannerMainImages: action.imgSet
  }),
  [SET_SUPPORT_BANNER_IMGS]: (state, action) => ({
    ...state,
    bannerSupportImages: action.imgSet
  }),
  [CLEAR_OUTDATED_OUTCOME]: state => ({
    ...state,
    outcome: {},
    isCurrentRow: false,
    currentRowID: -1,
    attemptProgress: {
      ...state.attemptProgress,
      itemId: -1
    }
  }),
  [RESET_STATS_DONE]: (state, action) => ({
    ...state,
    ...action.json.DB
  }),
  [FETCH_BANNER_IMG_START]: (state, action) => ({
    ...state,
    isBannerFetched: action.status
  }),
  [BANNER_IMG_LOADED]: (state, action) => ({
    ...state,
    isBannerFetched: action.status
  }),
  [FETCH_BANNER_COLOR]: (state, action) => ({
    ...state,
    bannerColor: action.bannerColor
  }),
  [LOCATION_CHANGE]: state => ({
    ...state,
    crimesByType: state.crimesByType.map(crime => (crime.crimeID === state.lastSubCrimeID ?
      {
        ...crime,
        additionalInfo: {
          ...crime.additionalInfo,
          total: {
            ...crime.additionalInfo.total,
            diff: 0
          }
        }
      } :
      crime))
  }),
  [MAIN_IMAGES_PRELOAD]: state => ({
    ...state,
    isMainBannerImagesFetched: true
  }),
  [CLOSE_OUTCOME]: state => ({
    ...state,
    attemptProgress: {
      itemId: 0
    }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
export default (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
