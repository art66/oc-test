import { put } from 'redux-saga/effects'

import getBannerRandomImg from './bannerRandomizer'
import stateHolder from './sagaStateHolder'
import { setMainBannerImages, setSupportBannerImages } from './actions'

import { IMG_ROOT_URL, IMG_RESOLUTION, IMG_COLOR, IMG_NUMBER, RESOLUTIONS } from '../constants'

function* generateBannerImgs({ type }: { type: 'main' | 'support' }) {
  const { bannerColor, colorsSpray } = yield stateHolder()

  const imgRandomNumber = getBannerRandomImg()
  const imgURLsArray = []

  Object.values(RESOLUTIONS).map(res => {
    const imgURL =
      IMG_ROOT_URL + IMG_COLOR + colorsSpray[bannerColor] + IMG_RESOLUTION + res + IMG_NUMBER + imgRandomNumber

    const imgDevice = {
      resolution: res,
      imgUrl: imgURL
    }

    return imgURLsArray.push(imgDevice)
  })

  if (type === 'main') {
    yield put(setMainBannerImages(imgURLsArray))
  } else if (type === 'support') {
    yield put(setSupportBannerImages(imgURLsArray))
  }

  return imgURLsArray
}

export default generateBannerImgs
