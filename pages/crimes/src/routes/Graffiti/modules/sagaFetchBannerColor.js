import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { showDebugInfo } from '../../../modules/actions'
import { fetchBannerColor } from './actions'
import stateHolder from './sagaStateHolder'

import { ROOT_URL } from '../../../constants'
import { BANNER_COLOR_URL } from '../constants'

export function* sagaFetchBannerColor() {
  const { bannerColor } = yield stateHolder()

  if (bannerColor) {
    return
  }

  try {
    const {
      DB: { lastUsedColor }
    } = yield fetchUrl(ROOT_URL + BANNER_COLOR_URL)

    yield put(fetchBannerColor(lastUsedColor))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default sagaFetchBannerColor
