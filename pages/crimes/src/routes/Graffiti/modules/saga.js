import { put, takeLatest } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import isEmpty from '@torn/shared/utils/isEmpty'

import generateBannerImgs from './sagaBannerGenerate'
import fetchBannerImgs from './sagaBannerFetch'
import stateHolder from './sagaStateHolder'
import sagaFetchBannerColor from './sagaFetchBannerColor'

import {
  dataFetched,
  attemptStart,
  attemptFetched,
  crimeReady,
  showDebugInfo,
  getCurrentPageData,
  userJailed
} from '../../../modules/actions'
import {
  replaceMainBannerImage,
  sprayAddingFinished
} from './actions'

import { TYPE_IDS, ROOT_URL } from '../../../constants'
import {
  SELECT_CAN_START,
  ATTEMPT,
  FETCH_DATA,
  TYPE_ID,
  SPRAY_SELECTION,
  BANNER_IMG_LOADED
} from '../constants'

export function* mainBannerFetch() {
  const { isMainBannerImagesFetched, bannerMainImages } = yield stateHolder()

  if (!isMainBannerImagesFetched) {
    yield fetchBannerImgs(bannerMainImages, 'main')
  }
}

export function* supportBannerFetch() {
  const { bannerSupportImages } = yield stateHolder()

  yield fetchBannerImgs(bannerSupportImages, 'support')
}

function* bannerReplace() {
  const { bannerMainImages, bannerSupportImages } = yield stateHolder()

  if (isEmpty(bannerMainImages) || isEmpty(bannerSupportImages)) {
    return
  }

  yield put(replaceMainBannerImage())
}

export function* bannerUpdater() {
  const { bannerMainImages } = yield stateHolder()

  yield sagaFetchBannerColor()
  yield bannerReplace()

  // we shall run it only once on the init mount, even we got a route switch after
  if (isEmpty(bannerMainImages)) {
    yield generateBannerImgs({ type: 'main' })
  }

  yield generateBannerImgs({ type: 'support' })
}

export function* bannerFetcher() {
  yield mainBannerFetch()
  yield supportBannerFetch()
}

export function* supportImagesUpdate() {
  const newImageSet = yield generateBannerImgs({ type: 'support' })

  yield fetchBannerImgs(newImageSet, 'support')
}

export function* fetchData({ url }) {
  yield bannerUpdater()

  try {
    const json = yield fetchUrl(ROOT_URL + url)

    if (json.DB.error) {
      throw new Error(json.DB.error)
    }

    yield put(dataFetched('graffiti', json))
    yield put(getCurrentPageData(json.DB.currentType.crimeRoute, TYPE_IDS[json.DB.currentType.crimeRoute]))
    yield put(crimeReady('/graffiti'))
    // yield staticFetcher()
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

function* attempt({ url, itemId, isCurrentRow }) {
  try {
    yield put(attemptStart('graffiti', itemId, isCurrentRow))
    const json = yield fetchUrl(ROOT_URL + url)

    const {
      DB: { error, outcome }
    } = json

    if (error) {
      throw new Error(error)
    }

    if (!['success', 'failed'].includes(outcome.result)) {
      yield put(userJailed(true))
    }

    yield put(attemptFetched('graffiti', json))

    yield supportImagesUpdate()
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

function* spraySelect({ id: ID, item: sprayColor }) {
  try {
    const URLPath = `${ROOT_URL}&step=${SPRAY_SELECTION}&typeID=${TYPE_ID}&crimeID=${ID}&value1=${sprayColor}`
    const json = yield fetchUrl(URLPath)

    if (json.DB.error) {
      throw new Error(json.DB.error)
    }

    yield put(sprayAddingFinished({ payload: json, selectedSpray: { id: ID, item: sprayColor } }))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default function* watchGraffiti() {
  yield takeLatest(FETCH_DATA, fetchData)
  yield takeLatest(ATTEMPT, attempt)
  yield takeLatest(SELECT_CAN_START, spraySelect)
  yield takeLatest(BANNER_IMG_LOADED, bannerFetcher)
}
