import { select } from 'redux-saga/effects'

function* stateHolder() {
  const getState = crimeState => crimeState
  const state = yield select(getState)

  const {
    graffitiLevel,
    lastSelectedColor,
    currentLevel,
    colorsSpray,
    isMainBannerImagesFetched,
    bannerSupportImages,
    bannerMainImages,
    bannerColor
  } = state.graffiti

  const { user } = state.common

  return {
    isMainBannerImagesFetched,
    bannerSupportImages,
    bannerMainImages,
    graffitiLevel,
    lastSelectedColor,
    currentLevel,
    colorsSpray,
    user,
    bannerColor
  }
}

export default stateHolder
