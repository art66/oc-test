import { BANNERS_MIN, BANNERS_MAX } from '../constants'

const getBannerRandomImg = () => {
  return Math.floor(Math.random() * (BANNERS_MAX - BANNERS_MIN)) + BANNERS_MIN
}

export default getBannerRandomImg
