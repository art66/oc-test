function staticFetcher() {
  const urls = [
    '/images/items/856/blank.png',
    '/images/items/862/medium.png',
    '/images/items/856/medium.png',
    '/images/items/857/medium.png',
    '/images/items/859/medium.png',
    '/images/items/863/medium.png',
    '/images/items/861/medium.png',
    '/images/items/860/medium.png',
    '/images/items/858/medium.png',
    '/images/v2/2sef32sdfr422svbfr2/crimes/graffiti/graffiti_subcrimes_v6.png'
  ]

  caches.keys().then(cacheStore => {
    if (cacheStore.length === 0) {
      caches.open('graffiti_sprays_v1').then(cache => {
        return cache.addAll(urls)
      })
    }
  })
}

export default staticFetcher
