import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import { fetchData } from '../../modules/actions'
import { TYPE_ID } from './constants'
import reducer from './modules/reducers'
import saga from './modules/saga'
import rootStore from '../../store/createStore'

import Placeholder from './components/Skeleton'

const Preloader = () => <Placeholder type='full' />

const GraffitiContainer = Loadable({
  loader: async () => {
    const Graffiti = await import(/* webpackChunkName: "graffiti" */ './containers/GraffitiContainer')

    injectReducer(rootStore, { key: 'graffiti', reducer })
    injectSaga({ key: 'graffiti', saga })
    rootStore.dispatch(fetchData('graffiti', `&step=crimesList&typeID=${TYPE_ID}`))

    return Graffiti
  },
  loading: Preloader
})

export default GraffitiContainer
