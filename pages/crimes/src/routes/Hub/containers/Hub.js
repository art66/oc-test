import { connect } from 'react-redux'
import { loadHubData } from '../../../modules/actions'
import Hub from '../components/Hub'

const mapDispatchToProps = {
  loadHubData
}

const mapStateToProps = state => ({
  db: state.common || {}
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hub)
