import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import StarLVL from '../../../components/StarLVL'

import styles from './stylesNew.cssmodule.scss'

const CONTAINER_DESKTOP_WIDTH = 782

const CARD_DIM = {
  lessOrEqualThree: {
    widthMin: 254,
    widthMax: 254,
    heightMin: 190,
    heightMax: 230
  },
  moreThanThree: {
    widthMin: 170,
    widthMax: 210,
    heightMin: 190,
    heightMax: 230
  }
}

export class Card extends Component {
  static propTypes = {
    disabled: PropTypes.bool,
    imgsrc: PropTypes.string.isRequired,
    type: PropTypes.string,
    desc: PropTypes.string,
    title: PropTypes.string,
    currentLevel: PropTypes.number.isRequired,
    crimeRoute: PropTypes.string.isRequired,
    ID: PropTypes.number,
    favoriteLength: PropTypes.number,
    isDesktop: PropTypes.bool,
    isShortList: PropTypes.bool
  }

  static defaultProps = {
    disabled: true,
    type: '',
    desc: '',
    title: '',
    ID: null,
    favoriteLength: null,
    isDesktop: null,
    isShortList: null
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      isHovered: false
    }

    this.ref = React.createRef()
  }

  _handleMagnify = () => {
    const { disabled } = this.props

    if (!disabled) {
      this.setState({ isHovered: true })
      this.ref.current.style.webkitFilter = 'saturate(100%)'
    }
  }

  _handleMinify = () => {
    const { disabled } = this.props

    if (!disabled) {
      this.setState({ isHovered: false })
      this.ref.current.style.webkitFilter = ''
    }
  }

  _getSVGIconStar = () => {
    const { currentLevel = 0 } = this.props

    if (!currentLevel) return null

    return <StarLVL lvl={currentLevel} />
  }

  _isFavorite = type => {
    return type === 'fav'
  }

  _getCardDim = () => {
    const { isShortList } = this.props

    const { lessOrEqualThree, moreThanThree } = CARD_DIM
    const cardDim = isShortList ? lessOrEqualThree : moreThanThree

    return cardDim
  }

  _calcCardLeftShift = (cardsLength, cardID) => {
    const cardDim = this._getCardDim()

    const containerCapacity = Math.floor(CONTAINER_DESKTOP_WIDTH / cardDim.widthMin)
    const basicShift = containerCapacity <= cardsLength ? cardDim.widthMin : CONTAINER_DESKTOP_WIDTH / cardsLength
    const cardShift = Math.ceil(CONTAINER_DESKTOP_WIDTH / cardDim.widthMin) * 2
    const gap = (CONTAINER_DESKTOP_WIDTH - (cardDim.widthMin * cardsLength)) / (cardsLength - 1)
    const shift = containerCapacity <= cardsLength ? basicShift : basicShift - cardShift

    return shift * cardID + gap * cardID
  }

  _getCustomParams = () => {
    const { isHovered } = this.state
    const { favoriteLength, ID, type, isDesktop, isShortList } = this.props

    const isFavoriteCard = this._isFavorite(type)

    if (!isFavoriteCard || !isDesktop) {
      return null
    }

    const cardDim = this._getCardDim()

    return {
      width: isHovered ? `${cardDim.widthMax}px` : `${cardDim.widthMin}px`,
      height: isHovered ? `${cardDim.heightMax}px` : `${cardDim.heightMin}px`,
      left: !isShortList ? this._calcCardLeftShift(favoriteLength, ID) : 'unset'
    }
  }

  render() {
    const { crimeRoute, imgsrc, desc, type, title } = this.props
    const { isHovered } = this.state
    const hover = isHovered ? ' hover' : ''

    if (!imgsrc) return null

    const isFavoriteCard = this._isFavorite(type)

    return (
      <li
        className={`${styles.cardContainer} ${type === 'fav' ? styles.favoriteCard : ''} completed ${hover}`}
        style={...this._getCustomParams()}
        onMouseEnter={isFavoriteCard ? this._handleMagnify : undefined}
        onMouseLeave={isFavoriteCard ? this._handleMinify : undefined}
      >
        <Link to={`/${crimeRoute}`}>
          <div className='t-overflow__icon title t-overflow'>
            <div className='iconContainer'>{this._getSVGIconStar()}</div>
            {title}
          </div>
          <div className='image-wrap'>
            <img alt='fadingImage' ref={this.ref} src={imgsrc} className='image-wrap__image favorites-image' />
          </div>
          <div className='footer' title={isFavoriteCard ? null : desc}>
            {desc || isFavoriteCard && 'N/A'}
          </div>
          <div className='clear' />
        </Link>
      </li>
    )
  }
}
const mapStateToProps = state => ({
  isDesktop: state.browser.is.desktop
})

export default connect(
  mapStateToProps,
  null
)(Card)
