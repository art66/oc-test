import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Redirect } from 'react-router'
import CardList from './CardList'
import './styles.scss'

class Hub extends Component {
  static propTypes = {
    db: PropTypes.object,
    loadHubData: PropTypes.func,
    hubRedirect: PropTypes.bool
  }

  static defaultProps = {
    db: {},
    loadHubData: () => {},
    hubRedirect: false
  }

  componentDidMount() {
    const { loadHubData } = this.props

    loadHubData()
  }

  _getFavorites = (crimesTypes = [], crimesFavorites = []) => {
    const noFavorites = !crimesFavorites || crimesFavorites.length === 0

    if (noFavorites) return null

    const favorites = crimesTypes.filter(crime => {
      return crimesFavorites.filter(favorite => {
        const isFavorite = crime.ID === favorite

        if (!isFavorite) return null

        return crime
      })
    })

    return favorites
  }

  _renderFavotitesList = favorites => {
    if (!favorites) return null

    return (
      <CardList className={`favorites-crimes crimes-list amount-${favorites.length}`} cards={favorites} type='fav' />
    )
  }

  _renderCrimesList = crimesTypes => {
    return <CardList className='crimes-boxes crimes-list' cards={crimesTypes} />
  }

  render() {
    const { hubRedirect } = this.props

    if (hubRedirect) {
      return <Redirect to='/searchforcash' />
    }

    const {
      db: { crimesTypes = [], crimesFavorites = [] }
    } = this.props

    const noCrimes = !crimesTypes || crimesTypes.length === 0

    if (noCrimes) return null

    const CrimesList = this._renderCrimesList(crimesTypes)
    const favorites = this._getFavorites(crimesTypes, crimesFavorites)
    const FavoritesList = this._renderFavotitesList(favorites)

    return (
      <div className='crimesHub'>
        {FavoritesList}
        <hr className='page-head-delimiter m-bottom10' />
        {CrimesList}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  hubRedirect: state.common.hubRedirect,
  isDesktop: state.browser.is.desktop
})

export default connect(
  mapStateToProps,
  null
)(Hub)
