const initialState = {
  disabled: false,
  imgsrc: '/images/v2/2sef32sdfr422svbfr2/crimes/placeholder/placeholder_2.jpg',
  desc: 'bootlegging',
  title: 'Bootlegging',
  crimeRoute: 'searchforcash',
  currentLevel: 11,
  type: ''
}

export const noImage = {
  ...initialState,
  imgsrc: ''
}

export default initialState
