const initialState = {
  db: {
    crimesFavorites: [1, 2],
    crimesTypes: [
      {
        ID: 1,
        active: 1,
        crimeRoute: 'searchforcash',
        disabled: false,
        enhancer: 0,
        expForLevel100: 0,
        img: '/images/v2/2sef32sdfr422svbfr2/crimes/placeholder/placeholder_1.jpg',
        title: 'Search for cash',
        typeID: 1
      },
      {
        ID: 2,
        active: 1,
        crimeRoute: 'searchforcash',
        disabled: false,
        enhancer: 0,
        expForLevel100: 0,
        img: '/images/v2/2sef32sdfr422svbfr2/crimes/placeholder/placeholder_2.jpg',
        title: 'Bootlegging',
        typeID: 2
      }
    ]
  },
  loadHubData: () => {}
}

export const withoutFavorites = {
  ...initialState,
  db: {
    ...initialState.db,
    crimesFavorites: []
  }
}

export const noCrimes = {
  ...initialState,
  db: {}
}

export const reduxStoreProps = {
  common: {
    hubRedirect: false
  }
}

export default initialState
