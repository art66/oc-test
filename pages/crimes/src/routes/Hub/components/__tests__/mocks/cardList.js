const initialState = {
  className: 'crimes-boxes crimes-list',
  cards: [
    {
      ID: 1,
      active: 1,
      crimeRoute: 'searchforcash',
      currentLevel: 10,
      disabled: false,
      enhancer: 0,
      expForLevel100: 0,
      img: '/images/v2/2sef32sdfr422svbfr2/crimes/placeholder/placeholder_1.jpg',
      title: 'Search for cash',
      typeID: 1
    },
    {
      ID: 2,
      active: 1,
      crimeRoute: 'searchforcash',
      currentLevel: 9,
      disabled: false,
      enhancer: 0,
      expForLevel100: 0,
      img: '/images/v2/2sef32sdfr422svbfr2/crimes/placeholder/placeholder_2.jpg',
      title: 'Bootlegging',
      typeID: 2
    }
  ]
}

export const favoriteCrimes = {
  ...initialState,
  className: 'favorites-crimes crimes-list amount-2'
}

export const basicCrimes = {
  ...initialState,
  className: 'crimes-boxes crimes-list'
}

export const noCrimes = {
  ...initialState,
  cards: []
}

export default initialState
