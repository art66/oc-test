import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter as Router } from 'react-router-dom'
import initialState, { favoriteCrimes, basicCrimes, noCrimes } from './mocks/cardList'
import CardList from '../CardList'

describe('<CardList />', () => {
  it('should render CardList with 2 crimes', () => {
    const Component = mount(
      <Router>
        <CardList {...initialState} />
      </Router>
    )

    expect(Component.find('CardList').length).toBe(1)
    expect(Component.find('ul').length).toBe(1)
    expect(Component.find('Card').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render Favorite CardList with 2 crimes', () => {
    const Component = mount(
      <Router>
        <CardList {...favoriteCrimes} />
      </Router>
    )

    expect(Component.find('CardList').length).toBe(1)
    expect(Component.find('CardList').prop('className')).toBe('favorites-crimes crimes-list amount-2')
    expect(Component.find('ul').length).toBe(1)
    expect(Component.find('Card').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render Basic CardList with 2 crimes', () => {
    const Component = mount(
      <Router>
        <CardList {...basicCrimes} />
      </Router>
    )

    expect(Component.find('CardList').length).toBe(1)
    expect(Component.find('CardList').prop('className')).toBe('crimes-boxes crimes-list')
    expect(Component.find('ul').length).toBe(1)
    expect(Component.find('Card').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should not render CardList in case of 0 crimes', () => {
    const Component = mount(
      <Router>
        <CardList {...noCrimes} />
      </Router>
    )

    expect(Component.find('CardList').length).toBe(1)
    expect(Component.find('ul').length).toBe(0)
    expect(Component.find('Card').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
