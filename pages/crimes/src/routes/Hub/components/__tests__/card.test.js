import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter as Router } from 'react-router-dom'
import initialState, { noImage } from './mocks/card'
import Card from '../Card'

describe('<Card />', () => {
  it('should render Card', () => {
    const Component = mount(
      <Router>
        <Card {...initialState} />
      </Router>
    )

    expect(Component.find('li').length).toBe(1)
    expect(Component.find('Link').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should change Card state of isHovered by mouse events enter/leave ', () => {
    const Component = mount(
      <Router>
        <Card {...initialState} />
      </Router>
    )

    expect(Component.find('li').length).toBe(1)
    expect(Component.find('Card').instance().state).toEqual({ isHovered: false })
    Component.find('li').simulate('mouseEnter')
    expect(Component.find('Card').instance().state).toEqual({ isHovered: true })
    Component.find('li').simulate('mouseLeave')
    expect(Component.find('Card').instance().state).toEqual({ isHovered: false })
    expect(Component.find('Link').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should not render Card if no image provided', () => {
    const Component = mount(
      <Router>
        <Card {...noImage} />
      </Router>
    )

    expect(Component.find('li').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
