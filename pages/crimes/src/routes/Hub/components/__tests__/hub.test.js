import React from 'react'
import { mount } from 'enzyme'
import configureStore from 'redux-mock-store'
import { BrowserRouter as Router } from 'react-router-dom'
import initialState, { reduxStoreProps, withoutFavorites, noCrimes } from './mocks/hub'
import Hub from '../Hub'

const mockStore = configureStore()
const store = mockStore(reduxStoreProps)

describe('<Hub />', () => {
  it('should render Hub with 2 crimes and 2 favorites crimes', () => {
    const Component = mount(
      <Router>
        <Hub store={store} {...initialState} />
      </Router>
    )

    expect(Component).toMatchSnapshot()

    expect(Component.find('.crimesHub').length).toBe(1)
    expect(Component.find('CardList').length).toBe(2)
    expect(Component.find('Card').length).toBe(4)
    expect(Component.find('Link').length).toBe(4)

    // find basic and favorite Card Components count
    expect(Component.find({ type: '' }).length).toBe(3)
    expect(Component.find({ type: 'fav' }).length).toBe(3)

    expect(Component).toMatchSnapshot()
  })
  it('should render Hub only with 2 crimes, without favorites crimes', () => {
    const Component = mount(
      <Router>
        <Hub store={store} {...withoutFavorites} />
      </Router>
    )

    expect(Component.find('.crimesHub').length).toBe(1)
    expect(Component.find('CardList').length).toBe(1)
    expect(Component.find('Card').length).toBe(2)
    expect(Component.find('Link').length).toBe(2)

    // find basic and favorite Card Components count
    expect(Component.find({ type: '' }).length).toBe(3)
    expect(Component.find({ type: 'fav' }).length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should not render Hub if no Crimes provided', () => {
    const Component = mount(
      <Router>
        <Hub store={store} {...noCrimes} />
      </Router>
    )

    expect(Component.find('.crimesHub').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
