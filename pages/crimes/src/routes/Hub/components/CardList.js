import PropTypes from 'prop-types'
import React from 'react'
import Card from './Card'

export const CardList = props => {
  const { className, cards } = props

  if (!cards || cards.length === 0) return null

  const modifiedCards = [...cards]
  const isShortList = modifiedCards.length <= 3

  return (
    <ul className={`${className} ${isShortList ? 'short-list' : ''}`}>
      {modifiedCards.map((card, i) => (
        <Card
          key={i}
          ID={i}
          isShortList={isShortList}
          favoriteLength={modifiedCards.length}
          imgsrc={card.img}
          disabled={card.disabled}
          title={card.title}
          crimeRoute={card.crimeRoute}
          currentLevel={card.currentLevel}
          type={props.type}
        />
      ))}
    </ul>
  )
}

CardList.propTypes = {
  cards: PropTypes.array.isRequired,
  className: PropTypes.string.isRequired,
  type: PropTypes.string
}

CardList.defaultProps = {
  type: ''
}

export default CardList
