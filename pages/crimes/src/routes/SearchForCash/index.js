import React from 'react'
import Loadable from 'react-loadable'
import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import { fetchData } from '../../modules/actions'
import { typeID } from './constants'
import reducer from './modules/reducers'
import saga from './modules/saga'
import rootStore from '../../store/createStore'

import Placeholder from './components/Skeleton'

const Preloader = () => <Placeholder type='full' />

const SearchForCashContainer = Loadable({
  loader: async () => {
    const SearchForCash = await import(/* webpackChunkName: "searchforcash" */ './containers/SearchForCashContainer')

    injectReducer(rootStore, { key: 'searchforcash', reducer })
    injectSaga({ key: 'searchforcash', saga })
    rootStore.dispatch(fetchData('searchforcash', `&step=crimesList&typeID=${typeID}`))

    return SearchForCash
  },
  loading: Preloader
})

export default SearchForCashContainer
