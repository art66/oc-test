export const typeID = 1

// ------------------------------------
// Action Constants
// ------------------------------------
export const ATTEMPT = 'searchforcash/ATTEMPT'
export const FETCH_DATA = 'searchforcash/FETCH_DATA'
export const DATA_FETCHED = 'searchforcash/DATA_FETCHED'
export const ATTEMPT_START = 'searchforcash/ATTEMPT_START'
export const ATTEMPT_FETCHED = 'searchforcash/ATTEMPT_FETCHED'
export const CRIME_IS_READY = 'searchforcash/CRIME_IS_READY'
export const LEVEL_UP = 'searchforcash/LEVEL_UP'
export const SET_CRIME_LEVEL = 'searchforcash/SET_CRIME_LEVEL'
export const CLEAR_OUTDATED_OUTCOME = 'searchforcash/CLEAR_OUTDATED_OUTCOME'
export const POOLING = 'searchforcash/POOLING'
export const POOLING_FETCHED = 'searchforcash/POOLING_FETCHED'
export const RESET_STATS_DONE = 'searchforcash/RESET_STATS_DONE'
export const CLOSE_OUTCOME = 'searchforcash/CLOSE_OUTCOME'
