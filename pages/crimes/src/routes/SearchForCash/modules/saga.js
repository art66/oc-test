import { put, takeLatest, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  attemptStart,
  attemptFetched,
  dataFetched,
  poolingFetched,
  crimeReady,
  showDebugInfo,
  // updateExp,
  getCurrentPageData,
  userJailed
} from '../../../modules/actions'
import { TYPE_IDS, ROOT_URL } from '../../../constants'
import { ATTEMPT, FETCH_DATA, POOLING } from '../constants'

export function* fetchData({ url }) {
  try {
    const json = yield fetchUrl(ROOT_URL + url)

    if (json.DB.error) {
      throw new Error(json.DB.error)
    }
    yield put(dataFetched('searchforcash', json))

    yield put(getCurrentPageData(json.DB.currentType.crimeRoute, TYPE_IDS[json.DB.currentType.crimeRoute]))
    yield put(crimeReady('/searchforcash'))
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export function* attempt({ url, itemId, isCurrentRow }) {
  try {
    yield put(attemptStart('searchforcash', itemId, isCurrentRow))
    const json = yield fetchUrl(ROOT_URL + url)

    const {
      DB: { error, outcome }
    } = json

    if (error) {
      throw new Error(error)
    }

    if (!['success', 'failed'].includes(outcome.result)) {
      yield put(userJailed(true))
    }

    yield put(attemptFetched('searchforcash', json))
    // yield put(updateExp()) // temporary removed for testing without attemp exp update
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export function* pooling() {
  try {
    const json = yield fetchUrl(`${ROOT_URL}&step=crimesList&typeID=1&type=poll`)

    const getState = yield select(state => state)

    if (json.DB?.error) {
      yield put(showDebugInfo({ msg: json.DB?.error }))

      return
      // throw new Error(json.DB.error)
    }

    if (getState.searchforcash.attemptProgress.inProgress === false) {
      yield put(poolingFetched('searchforcash', json))
    }
  } catch (error) {
    yield put(showDebugInfo({ msg: error.toString() }))
  }
}

export default function* watchSearchForCash() {
  yield takeLatest(FETCH_DATA, fetchData)
  yield takeLatest(ATTEMPT, attempt)
  yield takeLatest(POOLING, pooling)
}
