import initialState from './initialState'
import {
  DATA_FETCHED,
  ATTEMPT_START,
  ATTEMPT_FETCHED,
  LEVEL_UP,
  SET_CRIME_LEVEL,
  CLEAR_OUTDATED_OUTCOME,
  POOLING_FETCHED,
  RESET_STATS_DONE,
  CLOSE_OUTCOME
} from '../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [DATA_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    showPlaceholder: false
  }),
  [ATTEMPT_START]: (state, action) => ({
    ...state,
    bannerID: action.itemId,
    attemptProgress: {
      itemId: action.itemId,
      inProgress: true
    },
    currentRowID: action.itemId,
    isCurrentRow: action.isCurrentRow
  }),
  [ATTEMPT_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    attemptProgress: {
      itemId: state.attemptProgress.itemId,
      inProgress: false
    }
  }),
  [LEVEL_UP]: (state, action) => ({
    ...state,
    level: action.payload.level
  }),
  [SET_CRIME_LEVEL]: (state, action) => ({
    ...state,
    ...action.result.DB
  }),
  [CLEAR_OUTDATED_OUTCOME]: state => ({
    ...state,
    outcome: {},
    isCurrentRow: false,
    currentRowID: -1,
    attemptProgress: {
      ...state.attemptProgress,
      itemId: -1
    }
  }),
  [POOLING_FETCHED]: (state, action) => ({
    ...state,
    ...action.json.DB
  }),
  [RESET_STATS_DONE]: (state, action) => ({
    ...state,
    ...action.json.DB
  }),
  [CLOSE_OUTCOME]: state => ({
    ...state,
    attemptProgress: {
      itemId: 0
    }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
export default (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
