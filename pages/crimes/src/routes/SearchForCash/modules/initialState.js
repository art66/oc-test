export default {
  attemptProgress: {
    inProgress: false,
    itemId: 0
  },
  typeID: 1,
  buttonLabel: 'Attempt',
  isCurrentRow: false,
  showPlaceholder: true,
  bannerID: 1,
  crimesByType: [
    {
      currentTemplate: 'searchForCash',
      skillLevel: 0,
      skillLevelTotal: null,
      title: 'Search the Trash',
      nerve: 0,
      iconClass: 'search-cash-image-search-trash',
      crimeID: 0,
      subID: 0,
      requirements: {
        items: {},
        minCrimeLevel: {}
      },
      available: true,
      additionalInfo: {
        barValue: 0,
        title: '',
        progressIconState: 0,
        currentSearchers: 0
      },
      panelItems: [
        {
          removeLink: true,
          label: '',
          value: ''
        },
        {
          removeLink: false,
          label: '',
          value: ''
        }
      ],
      result: 'info'
    }
  ],
  additionalInfo: null,
  currentType: {
    _id: {
      $oid: ''
    },
    ID: 0,
    typeID: 0,
    title: 'Search for cash',
    crimeRoute: 'searchforcash',
    active: 0,
    img: '',
    expForLevel100: 0
  },
  skillLevelTotal: 0,
  currentLevel: null,
  lastSubCrimeID: 0,
  currentUserStats: {
    failedTotal: 0,
    attemptsTotal: 0,
    crimesByIDAttempts: {
      1: 0,
      2: 0,
      3: 0
    },
    successesTotal: 0,
    moneyFoundTotal: 0,
    jailedTotal: 0,
    itemsFound: {
      1: 0
    },
    critFailedTotal: 0,
    statsByType: [
      {
        successesTotal: 0
      }
    ],
    skill: 0,
    skillLevel: 0
  },
  currentUserStatistics: []
}
