import { connect } from 'react-redux'
import CrimesList from './CrimesList'
import { closeOutcome, attempt, pooling } from '../../../../modules/actions'
import getGlobalSelector from '../../../../utils/globalListPropsSelector'

const mapStateToProps = ({ searchforcash, browser, common }) => {
  return getGlobalSelector(searchforcash, browser, common)
}

const mapDispatchToProps = dispatch => ({
  closeOutcome: () => dispatch(closeOutcome()),
  startAttempt: (url, itemId, isCurrentRow) => dispatch(attempt('searchforcash', url, itemId, isCurrentRow)),
  startPooling: () => dispatch(pooling('searchforcash'))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrimesList)
