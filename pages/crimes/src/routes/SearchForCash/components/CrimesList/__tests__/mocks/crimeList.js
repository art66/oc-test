const initialState = {
  outcome: {
    story: ['story']
  },
  state: '',
  startAttempt: () => {},
  lastSubCrimeID: 0,
  pushCurrentRow: () => {},
  attemptProgress: {
    inProgress: false,
    itemId: 1
  },
  isCurrentRow: false,
  crimes: [
    {
      outcome: {},
      itemId: 0,
      closeOutcome: () => {},
      lastSubCrimeID: 0,
      currentRowID: 0,
      isCurrentRow: false,
      action: () => {},
      additionalInfo: {
        title: '',
        progressIconState: ''
      },
      attemptProgress: {},
      available: false,
      crimeID: 6,
      iconClass: '',
      mediaType: '',
      nerve: 0,
      requirements: {
        items: ['', '']
      },
      showOutcome: false,
      state: '',
      title: 'Crime'
    },
    {
      outcome: {},
      itemId: 0,
      closeOutcome: () => {},
      lastSubCrimeID: 0,
      currentRowID: 0,
      isCurrentRow: false,
      action: () => {},
      additionalInfo: {
        title: '',
        progressIconState: ''
      },
      attemptProgress: {},
      available: false,
      crimeID: 7,
      iconClass: '',
      mediaType: '',
      nerve: 0,
      requirements: {
        items: ['', '']
      },
      showOutcome: false,
      state: '',
      title: 'Crime'
    },
    {
      outcome: {},
      itemId: 0,
      closeOutcome: () => {},
      lastSubCrimeID: 0,
      currentRowID: 0,
      isCurrentRow: false,
      action: () => {},
      additionalInfo: {
        title: '',
        progressIconState: ''
      },
      attemptProgress: {},
      available: false,
      crimeID: 1,
      iconClass: '',
      mediaType: '',
      nerve: 0,
      requirements: {
        items: ['', '']
      },
      showOutcome: false,
      state: '',
      title: 'Crime'
    },
    {
      outcome: {},
      itemId: 0,
      closeOutcome: () => {},
      lastSubCrimeID: 0,
      currentRowID: 0,
      isCurrentRow: false,
      action: () => {},
      additionalInfo: {
        title: '',
        progressIconState: ''
      },
      attemptProgress: {},
      available: false,
      crimeID: 2,
      iconClass: '',
      mediaType: '',
      nerve: 0,
      requirements: {
        items: ['', '']
      },
      showOutcome: false,
      state: '',
      title: 'Crime'
    },
    {
      outcome: {},
      itemId: 0,
      closeOutcome: () => {},
      lastSubCrimeID: 0,
      currentRowID: 0,
      isCurrentRow: false,
      action: () => {},
      additionalInfo: {
        title: '',
        progressIconState: ''
      },
      attemptProgress: {},
      available: false,
      crimeID: 3,
      iconClass: '',
      mediaType: '',
      nerve: 0,
      requirements: {
        items: ['', '']
      },
      showOutcome: false,
      state: '',
      title: 'Crime'
    },
    {
      outcome: {},
      itemId: 0,
      closeOutcome: () => {},
      lastSubCrimeID: 0,
      currentRowID: 0,
      isCurrentRow: false,
      action: () => {},
      additionalInfo: {
        title: '',
        progressIconState: ''
      },
      attemptProgress: {},
      available: false,
      crimeID: 4,
      iconClass: '',
      mediaType: '',
      nerve: 0,
      requirements: {
        items: ['', '']
      },
      showOutcome: false,
      state: '',
      title: 'Crime'
    },
    {
      outcome: {},
      itemId: 0,
      closeOutcome: () => {},
      lastSubCrimeID: 0,
      currentRowID: 0,
      isCurrentRow: false,
      action: () => {},
      additionalInfo: {
        title: '',
        progressIconState: ''
      },
      attemptProgress: {},
      available: false,
      crimeID: 5,
      iconClass: '',
      mediaType: '',
      nerve: 0,
      requirements: {
        items: ['', '']
      },
      showOutcome: false,
      state: '',
      title: 'Crime'
    }
  ],
  mediaType: '',
  result: '',
  story: [],
  currentRowID: 0
}

export const stateCrime2 = {
  ...initialState,
  attemptProgress: {
    ...initialState.attemptProgress,
    itemId: 2
  }
}

export const stateCrime3 = {
  ...initialState,
  attemptProgress: {
    ...initialState.attemptProgress,
    itemId: 3
  }
}

export default initialState
