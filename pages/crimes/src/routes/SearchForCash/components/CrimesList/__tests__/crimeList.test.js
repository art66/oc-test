import React from 'react'
import { mount } from 'enzyme'
import initialState from './mocks/crimeList'
import { CrimesList } from '../CrimesList'

describe('<crimeList />', () => {
  const {
    attemptProgress: { itemId, inProgress },
    outcome: { story }
  } = initialState

  it('should render crimeList of 7 subcrimes', () => {
    const Component = mount(<CrimesList {...initialState} />)
    const crimes = Component.instance()._buildCrimesList()

    expect(crimes.length).toBe(7)
    expect(Component).toMatchSnapshot()
  })
  it('should throw outcome to the 2nd subcrime', () => {
    const Component = mount(<CrimesList {...initialState} />)
    const getCrimesList = Component.instance()._buildCrimesList()
    const otcomeToSubcrime = getCrimesList.filter(crime => itemId === crime.props.crimeID) || false
    const isOutomeMustBeShowed = story && otcomeToSubcrime && !inProgress

    expect(isOutomeMustBeShowed).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
})
