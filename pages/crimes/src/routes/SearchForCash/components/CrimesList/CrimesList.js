import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { CrimeRow } from '../CrimeRow/CrimeRow'
import '../../../../styles/subcrimes.scss'

export class CrimesList extends PureComponent {
  static propTypes = {
    outcome: PropTypes.object,
    startAttempt: PropTypes.func.isRequired,
    lastSubCrimeID: PropTypes.number,
    isCurrentRow: PropTypes.bool,
    crimes: PropTypes.array,
    mediaType: PropTypes.string,
    result: PropTypes.string,
    story: PropTypes.array,
    currentRowID: PropTypes.number,
    startPooling: PropTypes.func,
    attemptProgress: PropTypes.bool,
    buttonLabel: PropTypes.string,
    typeID: PropTypes.number,
    jailed: PropTypes.bool,
    closeOutcome: PropTypes.bool
  }

  static defaultProps = {
    outcome: {},
    lastSubCrimeID: 0,
    isCurrentRow: false,
    crimes: [],
    mediaType: 'desktop',
    result: '',
    story: [],
    currentRowID: 0,
    startPooling: null
  }

  componentDidMount() {
    this.poollingTimerId = setInterval(this._startPoolingTimer, 20000)
  }

  componentWillUnmount() {
    this._clearPoolingTimer(this.poollingTimerId)
  }

  _startPoolingTimer = () => {
    const { startPooling } = this.props

    startPooling()
  }

  _clearPoolingTimer = timer => {
    clearInterval(timer)
  }

  _attempt = (url, itemId) => {
    const { startAttempt, currentRowID } = this.props
    const onCurrentRow = currentRowID === Number(itemId)

    startAttempt(url, itemId, onCurrentRow)
  }

  _buildCrimesList = () => {
    const lis = []
    const {
      attemptProgress,
      crimes,
      mediaType,
      lastSubCrimeID,
      currentRowID,
      isCurrentRow,
      buttonLabel,
      typeID,
      jailed,
      closeOutcome
    } = this.props

    crimes
      && crimes.map((row, i) => {
        return lis.push(
          <CrimeRow
            {...row}
            key={i}
            isRowActive={row.isRowActive}
            action={this._attempt}
            lastSubCrimeID={lastSubCrimeID}
            currentRowID={currentRowID}
            isCurrentRow={isCurrentRow}
            itemId={attemptProgress.itemId}
            buttonLabel={buttonLabel}
            crimeID={row.crimeID}
            typeID={typeID}
            jailed={jailed}
            mediaType={mediaType}
            state={row.rowState}
            story={row.rowStory}
            closeOutcome={closeOutcome}
          />
        )
      })

    return lis
  }

  render() {
    return (
      <div className='main-container'>
        <div className='subcrimes-list'>
          <div>{this._buildCrimesList()}</div>
        </div>
      </div>
    )
  }
}

export default CrimesList
