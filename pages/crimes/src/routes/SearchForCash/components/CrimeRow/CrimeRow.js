import PropTypes from 'prop-types'
import React, { Component } from 'react'
import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

import { searchforcashRowsUpdate } from '../../utils/shouldRowsUpdate'
import globalCrimeRowCheck from '../../../../utils/shouldRowsUpdate'

import ownCrimeRowProps from './selectors'

import CrimeRowIcon from '../CrimeRowIcon'
import { CrimeRow as CrimeRowGlobal } from '../../../../components'

import StarLVL from '../../../../components/StarLVL'

import styles from '../../../../styles/searchforcash.cssmodule.scss'
import localStyles from './index.cssmodule.scss'

const GOLD_STAR_ID = 75

export class CrimeRow extends Component {
  static propTypes = {
    outcome: PropTypes.object,
    itemId: PropTypes.number,
    closeOutcome: PropTypes.func,
    lastSubCrimeID: PropTypes.number,
    currentRowID: PropTypes.number,
    isCurrentRow: PropTypes.bool,
    action: PropTypes.func,
    additionalInfo: PropTypes.object,
    attemptProgress: PropTypes.object,
    available: PropTypes.bool,
    crimeID: PropTypes.number,
    iconClass: PropTypes.string,
    mediaType: PropTypes.string,
    nerve: PropTypes.number,
    requirements: PropTypes.object,
    showOutcome: PropTypes.bool,
    state: PropTypes.string,
    title: PropTypes.string.isRequired
  }

  static defaultProps = {
    outcome: {},
    itemId: 0,
    closeOutcome: () => {},
    lastSubCrimeID: 0,
    currentRowID: 0,
    isCurrentRow: false,
    action: () => {},
    additionalInfo: {},
    attemptProgress: {},
    available: false,
    crimeID: 0,
    iconClass: '',
    mediaType: '',
    nerve: 0,
    requirements: {},
    showOutcome: false,
    state: ''
  }

  componentDidMount() {
    this._initializeTooltips()
  }

  shouldComponentUpdate(nextProps) {
    const rowUpdate = searchforcashRowsUpdate(this.props, nextProps)
    const globalCheck = globalCrimeRowCheck(this.props, nextProps)

    return rowUpdate || globalCheck || false
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  getCurrentImage = () => {
    const { requirements } = this.props
    const { minCrimeLevel } = requirements
    const image = requirements.items && requirements.items[0]

    if (image) {
      return this.imageTitle(image)
    }

    if (!image && minCrimeLevel) {
      return this.imageMinCrimeLVL()
    }

    return this.imageLocked()
  }

  _initializeTooltips = () => {
    const { crimeID, requirements: { items } = {} } = this.props
    const { progressTitle } = ownCrimeRowProps(this.props)
    const image = items && items[0]

    const itemsToTooltips = [progressTitle]

    if (image) {
      itemsToTooltips.push(image)
    }

    itemsToTooltips.forEach(itemToTooltip => {
      const isTootipObject = typeof itemToTooltip === 'object'

      const tooltipProps = {
        child: isTootipObject ? itemToTooltip.label : progressTitle,
        ID: isTootipObject ? itemToTooltip.value : crimeID,
        withHTML: true
      }

      this._renderTooltip(tooltipProps)
    })
  }

  _updateTooltips = () => {
    const { crimeID } = this.props
    const { progressTitle } = ownCrimeRowProps(this.props)

    const tooltipProps = {
      child: progressTitle,
      ID: `progressRow${crimeID}`,
      withHTML: true,
      update: true
    }

    this._renderTooltip(tooltipProps)
  }

  _renderTooltip = ({ child, ID, withHTML = false, update = false }) => {
    const setChild = withHTML ? <span dangerouslySetInnerHTML={{ __html: child }} /> : child

    if (update) {
      tooltipSubscriber.update({ child: setChild, ID: `progressRow${ID}` })

      return
    }

    tooltipSubscriber.subscribe({ child: setChild, ID: `progressRow${ID}` })
  }

  imageTitle = (image = {}) => {
    const { available, value } = image
    const imageType = available ? 'medium' : 'blank'

    return (
      <div id={`progressRow${value}`} className={styles.imageWrap}>
        <img className={styles.image} src={`/images/items/${value}/${imageType}.png`} alt={image.label} />
      </div>
    )
  }

  imageMinCrimeLVL = () => {
    const { requirements } = this.props
    const { minCrimeLevel } = requirements

    return (
      <div className={localStyles.minCrimeLVL}>
        <span className={`${localStyles.text} t-gray-9`}>Level</span>
        <div className={localStyles.iconContainer}>
          <StarLVL
            lvl={GOLD_STAR_ID}
            customStyles={{ text: localStyles.customTextStyle }}
            customTitle={minCrimeLevel.value}
          />
        </div>
      </div>
    )
  }

  imageLocked = () => <div className={`${styles.imageWrap} ${styles['locked']}`} />

  _action = () => {
    const { action, crimeID } = this.props

    action(`&step=attempt&typeID=1&crimeID=${crimeID}`, crimeID)
  }

  render() {
    const { crimeID } = this.props
    const { styleMediaType, barValue, throwGlobalCrimeRowProps: props } = ownCrimeRowProps(this.props)

    return (
      <CrimeRowGlobal {...props} rowShouldUpdate={searchforcashRowsUpdate}>
        <div id={`progressRow${crimeID}`} className={`${styles['pb-wrap']} ${styles.dlm} ${styles.center}`}>
          <CrimeRowIcon crimeID={crimeID} />
          <div className={`${styles['pb']} ${styleMediaType}`}>
            <div className={styles['bar-line']} style={{ width: `${barValue}%` }}>
              <div className={styles['highlight']} />
            </div>
          </div>
        </div>
        <div className={`${styles['required-item']} ${styles.dlm} ${styles.center}`}>{this.getCurrentImage()}</div>
      </CrimeRowGlobal>
    )
  }
}

export default CrimeRow
