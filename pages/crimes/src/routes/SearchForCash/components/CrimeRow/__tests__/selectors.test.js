import initialState from './mocks/selectors'
import ownCrimeRowProps from '../selectors'

describe('selectors', () => {
  it('should return all selectors', () => {
    const {
      styleRowClass,
      styleMediaType,
      progressTitle,
      barValue,
      throwGlobalCrimeRowProps: props
    } = ownCrimeRowProps(initialState)

    expect(styleRowClass).toBe('pb-icon--6-1')
    expect(styleMediaType).toBeFalsy()
    expect(progressTitle).toBe('SearchForCash')
    expect(barValue).toBe(53)
    expect(props).toBeTruthy()
    expect(props).toMatchSnapshot()
  })
  it('should return 1st row class', () => {
    const { styleRowClass } = ownCrimeRowProps(initialState)

    expect(styleRowClass).toBe('pb-icon--6-1')
    expect(styleRowClass).toMatchSnapshot()
  })
  it('should return progress barValue to 53%', () => {
    const { barValue } = ownCrimeRowProps(initialState)

    expect(barValue).toBe(53)
    expect(barValue).toMatchSnapshot()
  })
  it('should return desktop styleMediaType', () => {
    const { styleMediaType } = ownCrimeRowProps(initialState)

    expect(styleMediaType).toBeFalsy()
    expect(styleMediaType).toMatchSnapshot()
  })
  it('should return correct progressTitle', () => {
    const { progressTitle } = ownCrimeRowProps(initialState)

    expect(progressTitle).toBe('SearchForCash')
    expect(progressTitle).toMatchSnapshot()
  })
})
