import React from 'react'
import { shallow } from 'enzyme'
import mocks, { minLvlrequired, crimeIsLocked, userJailed } from './mocks/crimesRow'
import CrimeRow from '../CrimeRow'

describe('<CrimeRow />', () => {
  it('should render crimeRow', () => {
    const Component = shallow(<CrimeRow {...mocks} />)

    expect(Component.find('.pb-wrap.dlm.center').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should the image to be inside crimeRow', () => {
    const Component = shallow(<CrimeRow {...mocks} />)

    expect(Component.find('.imageWrap').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('user does not have min 50lvl', () => {
    const newMocks = Object.assign(mocks, minLvlrequired)
    const Component = shallow(<CrimeRow {...newMocks} />)

    expect(Component.find('StarLVL').prop('lvl')).toBe(75)
    expect(Component).toMatchSnapshot()
  })
  it('crime is locked', () => {
    const newMocks = Object.assign(mocks, crimeIsLocked)
    const Component = shallow(<CrimeRow {...newMocks} />)

    expect(Component.find('.imageWrap.locked').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('crime attempt button is deactivated in case of disabled', () => {
    const Component = shallow(<CrimeRow {...mocks} />)
    const button = Component.instance().render().props.button.disabled

    expect(button).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
  it('crime attempt button is deactivated in case of Jailed uesr status', () => {
    const Component = shallow(<CrimeRow {...userJailed} />)
    const button = Component.instance().render().props.button.disabled

    expect(button).toBeTruthy()
    expect(Component).toMatchSnapshot()
  })
})
