const mocks = {
  outcome: {},
  itemId: 0,
  closeOutcome: () => {},
  lastSubCrimeID: 0,
  currentRowID: 0,
  isCurrentRow: false,
  action: () => {},
  additionalInfo: {
    title: '',
    progressIconState: ''
  },
  attemptProgress: {},
  available: false,
  crimeID: 0,
  iconClass: '',
  mediaType: '',
  nerve: 0,
  requirements: {
    items: ['', '']
  },
  showOutcome: false,
  state: '',
  title: 'Crime'
}

export const minLvlrequired = {
  ...mocks,
  requirements: {
    minCrimeLevel: {
      value: 49
    }
  }
}

export const crimeIsLocked = {
  ...mocks,
  requirements: {}
}

export const userJailed = {
  ...mocks,
  jailed: true
}

export default mocks
