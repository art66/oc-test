const initialState = {
  outcome: {
    story: ['story']
  },
  state: '',
  isRowActive: false,
  startAttempt: () => {},
  lastSubCrimeID: 0,
  pushCurrentRow: () => {},
  attemptProgress: {
    inProgress: false,
    itemId: 2
  },
  isCurrentRow: false,
  itemId: 0,
  closeOutcome: () => {},
  currentRowID: 0,
  action: () => {},
  jailed: false,
  additionalInfo: {
    title: 'SearchForCash',
    progressIconState: null,
    barValue: 53
  },
  available: false,
  crimeID: 6,
  iconClass: 'search-cash-image-search-trash',
  mediaType: 'desktop',
  nerve: 0,
  requirements: {
    items: ['', '']
  },
  showOutcome: false,
  title: 'Crime',
  result: '',
  story: []
}

export default initialState
