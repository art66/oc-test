import { createSelector } from 'reselect'
import globalRowPropsSelector from '../../../../utils/globalRowPropsSelector'
import {
  getCrimeId,
  getIconClass,
  getAdditionalInfo,
  getMediaType,
  getDesktopManualLayout
} from '../../../../utils/globalSelectorsHelpers'
import styles from '../../../../styles/searchforcash.cssmodule.scss'

// reselect functions
const styleMediaType = createSelector(
  [getDesktopManualLayout, getMediaType],
  (isDesktopLayoutSetted, mediaType) => (!isDesktopLayoutSetted && mediaType !== 'desktop' ? styles['pb--short'] : '')
)
const barValue = createSelector(
  [getAdditionalInfo],
  additionalInfo => (additionalInfo && additionalInfo.barValue) || 0
)
const rowIcon = createSelector(
  [getIconClass],
  iconClass => styles[iconClass]
)

const progressIconState = createSelector(
  [getAdditionalInfo],
  additionalInfo => (additionalInfo && additionalInfo.progressIconState) || 1
)

const styleRowClass = createSelector(
  [getCrimeId, progressIconState],
  (crimeID, iconState) => {
    return styles[`pb-icon--${crimeID}-${iconState}`]
  }
)

const progressTitle = createSelector(
  [getAdditionalInfo],
  additionalInfo => (additionalInfo && additionalInfo.title) || ''
)

const globalCrimeRowProps = createSelector(
  [rowIcon, globalRowPropsSelector],
  (iconClass, globalProps) => {
    return {
      ...globalProps,
      styles,
      iconClass
    }
  }
)

const ownCrimeRowProps = props => ({
  barValue: barValue(props),
  progressTitle: progressTitle(props),
  styleRowClass: styleRowClass(props),
  styleMediaType: styleMediaType(props),
  progressIconState: progressIconState(props),
  throwGlobalCrimeRowProps: globalCrimeRowProps(props)
})

export default ownCrimeRowProps
