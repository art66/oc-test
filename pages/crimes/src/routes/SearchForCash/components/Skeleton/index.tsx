import React from 'react'

import Placeholder from '../../../../components/Skeleton'

import globalSkeleton from '../../../../styles/skeleton.cssmodule.scss'
import styles from './styles.cssmodule.scss'

const Skeleton = ({ type }: { type: string }) => {
  const rowSkeleton = (key: number) => {
    return (
      <div className={globalSkeleton.rowWrap} key={key}>
        <div className={globalSkeleton.titleImg} />
        <div className={globalSkeleton.titleText} />
        <div className={styles.statusContainer}>
          <div className={styles.icon} />
          <div className={globalSkeleton.progress} />
        </div>
        <div className={globalSkeleton.itemContainer} />
        <div className={globalSkeleton.nerveContainer} />
        <div className={globalSkeleton.buttonAction} />
      </div>
    )
  }

  return (
    <Placeholder type={type}>
      <div className={globalSkeleton.rowsWrap}>
        {Array.from(Array(6).keys()).map((key: number) => rowSkeleton(key))}
      </div>
    </Placeholder>
  )
}

export default Skeleton
