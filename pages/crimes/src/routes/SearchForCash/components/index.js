import Banners from './Banners'
import CrimesList from './CrimesList'
import Skeleton from './Skeleton'

export { Banners, CrimesList, Skeleton }
