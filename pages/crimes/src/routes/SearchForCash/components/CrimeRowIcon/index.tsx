import React from 'react'
import SVGIcon from '../../../../components/SVGIcon'

import iconsSchema from './iconsSchemas'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

class CrimeRowIcon extends React.PureComponent<IProps> {
  render() {
    const { crimeID } = this.props

    const iconProps = {
      ...iconsSchema[crimeID],
      preset: { type: 'crimes', subtype: 'SEARCHFORCASH' }
    }

    return (
      <div className={styles.svgIconWrap}>
        <SVGIcon iconProps={iconProps} />
      </div>
    )
  }
}

export default CrimeRowIcon
