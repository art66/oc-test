const iconsSchema = {
  1: {
    iconName: 'Trash',
    dimensions: {
      width: '13px',
      height: '17px',
      viewbox: '0 0 20 24'
    }
  },
  2: {
    iconName: 'Person',
    dimensions: {
      width: '7px',
      height: '17px',
      viewbox: '0 0 7 17'
    }
  },
  3: {
    iconName: 'ScrapYard',
    dimensions: {
      width: '19px',
      height: '20px',
      viewbox: '0 0 20 20'
    }
  },
  4: {
    iconName: 'Tide',
    type: 'tideThree',
    dimensions: {
      width: '16px',
      height: '12px',
      viewbox: '0 0 16 12'
    }
  },
  5: {
    iconName: 'Tombstone',
    type: 'withBroom',
    dimensions: {
      width: '17px',
      height: '17px',
      viewbox: '0 0 18 17'
    }
  },
  6: {
    iconName: 'Fountain',
    type: 'withWater',
    dimensions: {
      width: '17px',
      height: '17px',
      viewbox: '0 0 18 17'
    }
  }
}

export default iconsSchema
