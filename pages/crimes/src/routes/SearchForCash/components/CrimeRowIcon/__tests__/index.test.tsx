import React from 'react'
import { mount } from 'enzyme'
import CrimeRowIcon from '..'

describe('<CrimeRowIcon SFC />', () => {
  it('should return CrimeRowIcon Component', async done => {
    const Component = mount(<CrimeRowIcon crimeID={4} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Tide')
    expect(Component.find('svg').length).toBe(1)
    expect(Component).toMatchSnapshot()

    done()
  })
})
