import React, { PureComponent, Fragment } from 'react'
import getRandomGhostID from '../../utils/bannerGYRandomizer'

const trash = () => (
  <div className='crimes-search-for-cash trash'>
    <div className='money animation' />
    <div className='trash-bags animation'>
      <i className='trash-1' />
      <i className='trash-2' />
    </div>
    <div className='shadow animation' />
    <div className='shadow shadow-2 animation' />
  </div>
)

const trainstation = () => (
  <div className='crimes-search-for-cash train-station'>
    <div className='white-light animation' />
    <div className='green-light animation' />
    <div className='darkens animation' />
    <div className='money animation' />
  </div>
)

const beach = () => (
  <div className='crimes-search-for-cash beach'>
    <ul className='rain animation'>
      <li className='rd-1' />
      <li className='rd-2' />
      <li className='rd-3' />
      <li className='rd-4' />
      <li className='rd-5' />
      <li className='rd-6' />
      <li className='rd-7' />
      <li className='rd-8' />
      <li className='rd-9' />
      <li className='rd-10' />
      <li className='rd-11' />
      <li className='rd-12' />
      <li className='rd-13' />
      <li className='rd-14' />
    </ul>
    <div className='money animation' />
  </div>
)

export class Graveyard extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      randomGhostID: null
    }
  }

  componentDidMount() {
    this.ghostPromise = getRandomGhostID()

    this.ghostPromise.promise
      .then(res => {
        this.setState({
          randomGhostID: res
        })
      })
      .catch((e) => console.error('Promise is cancaled', e))
  }

  componentWillUnmount() {
    this.ghostPromise.cancel()
  }

  ghostAnimation = () => {
    const { randomGhostID } = this.state

    if (randomGhostID === 1) {
      return (
        <Fragment>
          <div className='ghost-1 animation' />
          <div className='trees' />
        </Fragment>
      )
    }

    if (randomGhostID === 2 || randomGhostID === 3) {
      return (
        <Fragment>
          <div className={`ghost-${randomGhostID} body animation`} />
          <div className={`ghost-${randomGhostID} silhouette animation`} />
        </Fragment>
      )
    }

    return null
  }

  render() {
    return (
      <div className='crimes-search-for-cash graveyard'>
        <div className='mists animation' />
        {this.ghostAnimation()}
      </div>
    )
  }
}

const scrapyard = () => (
  <div className='crimes-search-for-cash scrapyard'>
    <ul className='rain animation'>
      <li className='rd-1' />
      <li className='rd-2' />
      <li className='rd-3' />
      <li className='rd-4' />
      <li className='rd-5' />
      <li className='rd-6' />
      <li className='rd-7' />
      <li className='rd-8' />
      <li className='rd-9' />
      <li className='rd-10' />
      <li className='rd-11' />
      <li className='rd-12' />
      <li className='rd-13' />
      <li className='rd-14' />
    </ul>
    <div className='darkens animation' />
  </div>
)

const fountain = () => (
  <div className='crimes-search-for-cash fountain'>
    <ul className='water-elements animation'>
      <li className='jet-1' />
      <li className='jet-2' />
      <li className='jet-3' />
      <li className='jet-4' />
      <li className='jet-5' />
      <li className='jet-6' />
      <li className='jet-7' />
      <li className='jet-8' />
      <li className='jet-9' />
      <li className='jet-10' />
      <li className='jet-11' />
      <li className='jet-12' />
      <li className='jet-13' />
      <li className='jet-14' />
      <li className='jet-15' />
      <li className='jet-16' />
      <li className='jet-17' />
    </ul>
    <ul className='lights animation'>
      <li className='lt-1' />
      <li className='lt-2' />
      <li className='lt-3' />
      <li className='lt-4' />
      <li className='lt-5' />
      <li className='lt-6' />
      <li className='lt-7' />
    </ul>
  </div>
)

export default {
  1: trash,
  2: trainstation,
  3: scrapyard,
  4: beach,
  5: Graveyard,
  6: fountain
}
