const initialState = {
  searchforcash: {
    bannerID: 1
  }
}

export const zeroBanners = {
  searchforcash: {
    bannerID: 12
  }
}

export const secondBanners = {
  searchforcash: {
    bannerID: 2
  }
}

export const fifthBanner = {
  searchforcash: {
    bannerID: 5
  }
}

export default initialState
