import React from 'react'
import { mount } from 'enzyme'
import configureStore from 'redux-mock-store'
import initialState, { zeroBanners, secondBanners, fifthBanner } from './mocks'
import Banners from '..'
import { Graveyard } from '../BannersHolder'

describe('<Banners />', () => {
  const mockStore = configureStore()

  it('should render 1st banner', () => {
    const store = mockStore(initialState)
    const Component = mount(<Banners store={store} />)

    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.crimes-search-for-cash.trash').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render 1st banner in case of zero banners', () => {
    const state = Object.assign(initialState, zeroBanners)
    const store = mockStore(state)
    const Component = mount(<Banners store={store} />)

    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.crimes-search-for-cash.trash').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should replace 1st banner on 2nd one', () => {
    let store = mockStore(initialState)

    let Component = mount(<Banners store={store} />)

    expect(Component).toMatchSnapshot()
    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.crimes-search-for-cash.trash').length).toBe(1)

    store = mockStore(Object.assign(store, secondBanners))
    Component = mount(<Banners store={store} />)

    expect(Component).toMatchSnapshot()
    expect(Component.find('.bannerWrap').length).toBe(1)
    expect(Component.find('.crimes-search-for-cash.train-station').length).toBe(1)
  })
  it('should render Graveyard banner without ghosts', () => {
    const store = mockStore(fifthBanner)
    const Component = mount(<Graveyard store={store} />)

    Component.setState({ randomGhostID: null })

    expect(Component.find('.crimes-search-for-cash.graveyard').length).toBe(1)
    expect(Component.find('.mists.animation').length).toBe(1)
    expect(Component.find('.trees').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render Graveyard banner with 1st ghost', () => {
    const store = mockStore(fifthBanner)
    const Component = mount(<Graveyard store={store} />)

    Component.setState({ randomGhostID: 1 })

    expect(Component.find('.crimes-search-for-cash.graveyard').length).toBe(1)
    expect(Component.find('.ghost-1.animation').length).toBe(1)
    expect(Component.find('.mists.animation').length).toBe(1)
    expect(Component.find('.trees').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render Graveyard banner with 2nd ghost', () => {
    const store = mockStore(fifthBanner)
    const Component = mount(<Graveyard store={store} />)

    Component.setState({ randomGhostID: 2 })

    expect(Component.find('.crimes-search-for-cash.graveyard').length).toBe(1)
    expect(Component.find('.ghost-2.body.animation').length).toBe(1)
    expect(Component.find('.ghost-2.silhouette.animation').length).toBe(1)
    expect(Component.find('.mists.animation').length).toBe(1)
    expect(Component.find('.trees').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render Graveyard banner 3thd ghost', () => {
    const store = mockStore(fifthBanner)
    const Component = mount(<Graveyard store={store} />)

    Component.setState({ randomGhostID: 3 })

    expect(Component.find('.crimes-search-for-cash.graveyard').length).toBe(1)
    expect(Component.find('.ghost-3.body.animation').length).toBe(1)
    expect(Component.find('.ghost-3.silhouette.animation').length).toBe(1)
    expect(Component.find('.mists.animation').length).toBe(1)
    expect(Component.find('.trees').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
