import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { showDropDownPanel, toggleArrows } from '../../../../modules/actions'
import BMAP from './BannersHolder'
import BannerGlobal from '../../../../components/Banner'
import '../../../../styles/searchforcash-banners.scss'
import '../../../../styles/animations.scss'

const ANIMATION_DURATION = 1000

export class Banners extends Component {
  static propTypes = {
    bannerID: PropTypes.number,
    dropDownPanelToShow: PropTypes.func,
    toggleArrow: PropTypes.func
  }

  static defaultProps = {
    bannerID: 1,
    dropDownPanelToShow: () => {},
    toggleArrow: () => {}
  }

  shouldComponentUpdate(nextProps) {
    const { bannerID } = this.props

    if (bannerID === nextProps.bannerID) {
      return false
    }

    return true
  }

  render() {
    const { bannerID, dropDownPanelToShow, toggleArrow } = this.props

    const Banner = !BMAP[bannerID] ? BMAP[1] : BMAP[bannerID]

    return (
      <BannerGlobal dropDownPanelToShow={dropDownPanelToShow} toggleArrows={toggleArrow}>
        <TransitionGroup>
          <CSSTransition
            classNames='fade'
            key={bannerID}
            timeout={{ enter: ANIMATION_DURATION, exit: ANIMATION_DURATION }}
          >
            <Banner />
          </CSSTransition>
        </TransitionGroup>
      </BannerGlobal>
    )
  }
}

const mapStateToProps = state => ({
  bannerID: state.searchforcash.bannerID
})

const mapDispatchToProps = dispatch => ({
  dropDownPanelToShow: () => dispatch(showDropDownPanel()),
  toggleArrow: () => dispatch(toggleArrows())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Banners)
