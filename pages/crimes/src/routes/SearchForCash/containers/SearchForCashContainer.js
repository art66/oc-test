import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { clearOutdatedOutcome } from '../../../modules/actions'

import { CrimesList, Skeleton, Banners } from '../components'
import SubCrimesLayout from '../../../components/SubCrimesLayout'

class SearchForCashContainer extends PureComponent {
  static propTypes = {
    state: PropTypes.object,
    showPlaceholder: PropTypes.bool,
    current: PropTypes.number,
    progress: PropTypes.number,
    stats: PropTypes.array,
    startClearOutdatedOutcome: PropTypes.func
  }

  static defaultProps = {
    state: {},
    startClearOutdatedOutcome: () => {},
    showPlaceholder: true,
    current: null,
    progress: null,
    stats: []
  }

  componentWillUnmount() {
    const {
      startClearOutdatedOutcome,
      state: { searchforcash }
    } = this.props
    const { outcome: { story } = {} } = searchforcash || {}

    if (!story || (searchforcash && story.length !== 0)) {
      startClearOutdatedOutcome()
    }
  }

  render() {
    const { stats, current, progress, showPlaceholder } = this.props

    return (
      <SubCrimesLayout
        customWrapClass='search-for-cash'
        showPlaceholder={showPlaceholder}
        title='Search for cash'
        skeleton={Skeleton}
        banners={<Banners />}
        stats={stats}
        current={current}
        progress={progress}
      >
        <CrimesList />
      </SubCrimesLayout>
    )
  }
}

const mapStateToProps = state => ({
  stat: state.searchforcash,
  current: state.searchforcash.currentLevel,
  progress: state.searchforcash.skillLevelTotal,
  stats: state.searchforcash.currentUserStatistics,
  showPlaceholder: state.searchforcash.showPlaceholder
})

const mapDispatchToProps = dispatch => ({
  startClearOutdatedOutcome: () => dispatch(clearOutdatedOutcome('searchforcash'))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchForCashContainer)
