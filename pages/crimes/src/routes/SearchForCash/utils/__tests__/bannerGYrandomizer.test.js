import { computeResult, getRandomDelay, setGhostDelay, getRandomGhostID } from '../bannerGYRandomizer'

describe('Ghost Banner Randomizer', () => {
  it('should return one of three random ghost animations once per 1000 iterations', () => {
    const ghostBannerID = Array.from(new Array(1000).keys()).map(() => {
      const ID = computeResult()

      return ID && ID
    })

    typeof ghostBannerID === 'number' && expect([1, 2, 3].includes(ghostBannerID)).toBeTruthy()
  })
  it('should return random delay (10 - 20 sec) for Promise in future', () => {
    const randomDealy = getRandomDelay()
    const delayRange = Array.from(new Array(20001).keys()).slice(10000)
    const isDelayInRange = delayRange.includes(randomDealy)

    expect(isDelayInRange).toBeTruthy()
    expect(isDelayInRange).toMatchSnapshot()
  })
  it(
    'should return main Promise with setTimeout delay',
    async () => {
      const randomDealy = getRandomDelay()
      const result = await setGhostDelay(randomDealy).then(() => 'Promise is fired!')
      const delayRange = Array.from(new Array(20001).keys()).slice(10000)

      expect(delayRange.includes(randomDealy)).toBeTruthy()
      expect(result).toBe('Promise is fired!')
      expect(result).toMatchSnapshot()
    },
    20001
  )
  test(
    'should return cancalable getRandomGhostID Promise with ghostID or null after main Promise with setTimeout delay',
    async () => {
      const ghostID = await getRandomGhostID().promise
      const result = ['object', 'number'].includes(typeof ghostID)

      expect(result).toBeTruthy()
      expect(result).toMatchSnapshot()
    },
    20000
  )
  test(
    'should cancel getRandomGhostID Promise once it started when crime is change',
    async () => {
      const ghostID = getRandomGhostID()

      expect(['object', 'number'].includes(typeof (await ghostID.promise))).toBeTruthy()
      expect(ghostID.cancel()).toBeTruthy()
    },
    20000
  )
})
