const OPTIMIZER_COUNT = 1
const GHOSTS_COUNT_MIN = 1
const GHOSTS_COUNT_MAX = 3
const GHOSTS_RANDOM_MIN = 1
const GHOSTS_RANDOM_MAX = 1000
const GHOSTS_TIMER_MIN = 10000
const GHOSTS_TIMER_MAX = 20000

let HAS_CANCELED = false

export const computeResult = () => {
  const randomGhostChance = Math.floor(
    GHOSTS_COUNT_MIN + Math.random() * (GHOSTS_RANDOM_MAX + OPTIMIZER_COUNT - GHOSTS_RANDOM_MIN)
  )

  if (randomGhostChance === 1) {
    const randomGhostID = Math.floor(
      GHOSTS_COUNT_MIN + Math.random() * (GHOSTS_COUNT_MAX + OPTIMIZER_COUNT - GHOSTS_COUNT_MIN)
    )

    return randomGhostID
  }

  return null
}

export const getRandomDelay = () => {
  const randomDelay = Math.floor(
    GHOSTS_TIMER_MIN + Math.random() * (GHOSTS_TIMER_MAX + OPTIMIZER_COUNT - GHOSTS_TIMER_MIN)
  )

  return randomDelay
}

export const setGhostDelay = delay => {
  return new Promise(resolve => setTimeout(resolve, delay))
}

export const makePromiseCancelable = promise => {
  const getGhostID = computeResult()

  const wrappedPromise = new Promise((resolve, reject) => {
    promise
      .then(() => (HAS_CANCELED ? reject({ isCanceled: true }) : resolve(getGhostID)))
      .catch(e => (HAS_CANCELED ? reject({ isCanceled: true }) : reject(console.error(e))))
  })

  return {
    promise: wrappedPromise,
    cancel() {
      HAS_CANCELED = true

      return HAS_CANCELED
    }
  }
}

export const getRandomGhostID = () => {
  const delay = getRandomDelay()
  const getGhostDealy = setGhostDelay(delay)
  const ghostID = makePromiseCancelable(getGhostDealy)

  return ghostID
}

export default getRandomGhostID
