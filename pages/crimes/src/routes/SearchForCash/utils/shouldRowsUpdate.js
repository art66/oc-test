const searchforcashRowsUpdate = (prevProps, nextProps) => {
  const isAddInfoNotChanged = JSON.stringify(prevProps.additionalInfo) !== JSON.stringify(nextProps.additionalInfo)

  // update only rows that received updated statistics data
  if (isAddInfoNotChanged) {
    return true
  }
}

export { searchforcashRowsUpdate }
