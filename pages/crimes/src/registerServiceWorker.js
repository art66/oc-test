export default function () {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      && navigator.serviceWorker.register('./crimes/serviceWorkers.js').then((registration) => {
        console.log('Excellent, registered with scope: ', registration.scope)
      })
  } else {
    console.log('Error in service worker')
  }
}
