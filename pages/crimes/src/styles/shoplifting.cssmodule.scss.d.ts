// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'action': string;
  'active': string;
  'bar-line': string;
  'big_als_gun_shop': string;
  'bits_n_bobs': string;
  'brn-gold': string;
  'cameraStatus': string;
  'center': string;
  'crimeRow': string;
  'disks-panel': string;
  'dlm': string;
  'dlmAny': string;
  'globalSvgShadow': string;
  'guardsStatus': string;
  'highlight': string;
  'image': string;
  'imageWrap': string;
  'jewelry_shop': string;
  'loadWrap': string;
  'loadingTest': string;
  'locked': string;
  'misc': string;
  'nerve': string;
  'next': string;
  'nonBorder': string;
  'pb': string;
  'pb--short': string;
  'pb-wrap': string;
  'quantity': string;
  'red': string;
  'responsive': string;
  'roundCircle': string;
  'sallys_sweet_shop': string;
  'selected': string;
  'selector': string;
  'statusEffects': string;
  'subcrimeImgSprite': string;
  'superstore': string;
  'tc_clothing': string;
  'title': string;
  'underTitle': string;
  'withUnderneath': string;
}
export const cssExports: CssExports;
export default cssExports;
