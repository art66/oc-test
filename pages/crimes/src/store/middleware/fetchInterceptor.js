/* eslint-disable complexity */
import { showDebugInfo, showInfoBox } from '../../modules/actions'

export const fetchInterceptor = store => next => action => {
  // TODO: remove line below once the HUB page will be done!
  if (action.json && action.json.DB && action.json.DB.hubRedirect && !location.href.match(/searchforcash/gi)) {
    location.href = `${location.protocol}//${location.hostname}/loader.php?sid=crimes#/searchforcash`
  }

  if (action.meta === 'ajax' && action.json) {
    if (action.json.DB && action.json.DB.debugInfo) {
      const stringInfo = JSON.stringify(action.json.DB.debugInfo, null, '\t')

      store.dispatch(showDebugInfo(`DebugInfo:\n\n${stringInfo}`))
    } else if (action.json.DB && action.json.DB.redirect !== undefined) {
      if (action.json.DB.redirect === true) {
        location.href = `${location.protocol}//${location.hostname}/${action.json.url}`
      } else if (action.json.DB && action.json.DB.redirect === false) {
        store.dispatch(showInfoBox({ msg: action.json.content, color: 'red' }))
      }
    } else if (action.json && action.json.content) {
      store.dispatch(showInfoBox({ msg: action.json.content, color: 'red' }))
      if (action.json.redirect) {
        location.href = `${location.protocol}//${location.hostname}/${action.json.url}`
      }
    }
  }

  return next(action)
}
