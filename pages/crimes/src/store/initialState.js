import { TYPE_IDS } from '../constants'
import getCurrentRootName from '../utils/getCurrentRootName'

const initialState = {
  common: {
    appID: 'Crimes',
    pageID: TYPE_IDS[getCurrentRootName()],
    nextPathname: '',
    loadedBundles: [],
    crimesTypes: [],
    showArrows: true,
    crimeName: getCurrentRootName(),
    currentArrow: 'next',
    freeze: true,
    showDropDown: false,
    editorSwitch: true,
    jailed: false,
    tutorial: {},
    hideTestPanel: true,
    isDesktopLayoutSetted: false
  }
}

export default initialState
