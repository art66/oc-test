import React from 'react'
import ReactDOM from 'react-dom'
import { syncHistoryWithStore } from 'react-router-redux'
import { createBrowserHistory } from 'history'
import RedBox from 'redbox-react'
import rootStore from './store/createStore'
import AppContainer from './containers/AppContainer'

const MOUNT_NODE = document.getElementById('react-root')

// TODO: Enable ServiceWorkers once it'll be accepted by the team
// import AppContainer from './containers/AppContainer'
// import registerServiceWorker from './registerServiceWorker'

// ========================================================
// Render Setup
// ========================================================
const history = syncHistoryWithStore(createBrowserHistory(), rootStore)

const render = () => {
  ReactDOM.render(<AppContainer store={rootStore} history={history} />, MOUNT_NODE)
}

const renderError = error => {
  ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
}

// This code is excluded from production bundle
if (__DEV__) {
  // importing real sidebar app bundle only for developing stage
  // import('../../sidebar/src/main')
  // adding Redux store for live accessing in the browser console
  window.store = rootStore
  // ========================================================
  // DEVELOPMENT STAGE! HOT MODULE REPLACE ACTIVATION!
  // ========================================================
  const devRender = () => {
    if (module.hot) {
      module.hot.accept('./containers/AppContainer', () => render())
    }

    render()
  }

  // Wrap render in try/catch
  try {
    devRender()
  } catch (error) {
    console.error(error)
    renderError(error)
  }
} else {
  // ========================================================
  // PRODUCTION GO!
  // ========================================================
  render()
}
