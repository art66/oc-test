# Crimes - Torn


# 5.0.1
 * Addded global Close Outcom logic.

# 5.0.0
 * Improved crimes-wide Skeleton components logic.
 * Improved Banner DropDown layout.
 * Improved Banner render speed.

# 4.69.0
 * Improved Placeholders in Pickpocket.
 * Added onLoad placeholder for dynamic bundles import.
 * Fixed broken NO_CRIMES placeholder in Pickpocket.
 * Fixed rows update logic in Pickpocket.
 * Fixed rows update logic in Pickpocket.

# 4.68.2
 * Fixed broken Outcome animation appear in a whole Crime app.

# 4.68.1
 * Fixed broken timer updates in Shoplifting.

# 4.68.0
 * Bumped up react-redux package.

# 4.67.3
 * Added tooltips over the Area in Graffiti.

# 4.66.3
 * Fixed crimes ordering by crimeID in SearchForCash.

# 4.66.3
 * Fixed crimes ordering by crimeID in Shoplifting.

# 4.66.2
 * Fixed crimes ordering by crimeID in Bootlegging, instead of explicitly array index using.
 * Fixed pooling running in case of reverted lvl and working again with setup online store, instead of collect funds crime.
 * Updated test suits.

# 4.66.1
 * Add tooltips over the graffiti counter in the row.
 * Fixed weird Graffiti rows commits.
 * Updated tests suits on Graffiti.

# 4.65.5
 * Fixed bootlegging styles imports in CD sections.

# 4.65.4
 * Removed server configuration in a whole.

# 4.64.4
 * Added particular gradients to the start icons in the ExperienceBar component.

# 4.64.3
 * Fixed shadow in start containers in ExperienceBar Component.

# 4.64.2
 * Fixed shadow in start containers in ExperienceBar Component.

# 4.64.1
 * Updated shoplifting toolips logic.
 * Improved stability in case of missing row icons.
 * Removed legacy server from Crimes.
 * Updated test suyarn jest .

# 4.63.4
 * Updated tests suits.

# 4.62.4
 * Fixed Editor inputs update block after react v.16.8 update.

# 4.62.3
 * Fixed index.html container due to the svgiconsgenerator updates.

# 4.62.2
 * Fixed outcomeHeight calculation and erashing.
 * Fixed Graffiti banners extra refetch.
 * Fixed Graffiti crimeRow stats animation while animation is started and row is changed out/in.

# 4.62.1
 * Integrated new Saga Fetch request for blizing fast banner color receiving.
 * Fixed banner color switch once commit done and resolution is change.

# 4.61.0
 * Added new placeholder for Graffiti Crime.
 * Update Graffiti Banner unit tests.

# 4.60.0
 * Update Graffiti crimeRow unit tests.

# 4.59.0
 * Significantly improved current Graffiti banners system (speed and quality).
 * Huge part of bussiness logic of the Graffiti banners generation moved to back-end.

# 4.58.3
 * Added extra shouldComponentUpdate method for handling rows spray set.

# 4.58.2
 * Added new Selection API system for independent fetch current set spray in the row..

# 4.57.2
 * Graffiti banners system has been updated.

# 4.56.2
 * Added animation for Spray selection on back-end request.

# 4.55.2
 * Fixed CrimeRow SelectCan action payload.

# 4.55.1
 * Fixed ExperienceBar SVG icons layout.

# 4.55.0
 * Graffiti banners IMGs system has been upgraded (All logic has been moved to back-end side).

# 4.54.1
 * Fixed createBrowserHistory import in main.js file.

# 4.54.0
 * Added animation for Graffiti subscrimes stats counter.
 * Updated test suits.

# 4.53.0
 * Update row Selector for the new animation layout.
 * Updates they tests suits.

# 4.52.0
 * Updated global and row Selectors.

# 4.51.0
 * Added animation for Graffiti banner.

# 4.50.5
 * Fixed Graffiti commiting system.
 * Tests suits were updated.

# 4.50.4
 * Fixed wrong mediaType changing for DropDown slider by hack in DOM Nodes directly.

# 4.50.3
 * Fixed wrong mediaType chenging for DropDown slider.
 * Fix an exata updates while commiting in Graffiti.

# 4.50.2
 * Fixed Tooltips initialization bug in render method of Bootlegging.

# 4.50.1
 * Fixed Tooltips initialization bug in render method of SFC.

# 4.50.0
 * Replaced png's icons and assets on SVG-based ones inside ExperienceBar Component.
 * Rewritted stylesheets on a better fashion and productivity work.
 * Update tests suits.

# 4.49.7
 * Replaced legacy experienceBar layuot from images to css based elements.

# 4.49.6
 * Fixed Graffiti skeleton in tablet/mobile modes.

# 4.49.5
 * Fixed Graffiti crashing once additionalInfo is not received.
 * Fixed cansLeft appear on tablet/mobile devices.

# 4.49.4
 * Fixed Graffiti skeletons accordingly to its real content.

# 4.49.3
 * Fixed Outcome main text arrow in case of no outcomeDesc provided.

# 4.49.2
 * Fix for correct layout in manualDesktopModes in Banner and CanSelector Components in Graffiti subcrime.
 * Update tests for above one.

# 4.49.1
 * Fix for Graffiti layout in manualDesktopMode.

# 4.49.0
 * Update Banner component in Bootlegging for global selector helper usage in manualDesktopMode case.

# 4.48.0
 * Add new global selector helper getDesktopManualLayout.
 * Written tests for above one.

# 4.47.4
 * Fixed Tooltips render method in PanelTitle Component.
 * Fixed ExperienceBar keys issue while iterating.
 * Fixed alt tag for banner img component.
 * Update test suits.

# 4.47.3
 * Fixed ToolTips padding.

# 4.47.2
 * Fixed flaxing light shift in CrimeSlider Arrows once they is hovered.

# 4.47.1
 * Fixed bug in Stats slider navigation during meria screen change.

# 4.47.0
 * Removed legacy .png exp bar, replaced by pure css one instead.

# 4.46.1
 * Added smooth css arrows inside SlickSlider component.
 * Improved margins in StarLVL component.

# 4.45.0
 * Refactored Tooltip Component.

# 4.44.1
 * Fixed Tooltip appear and background flashing in CDs Component.
 * Updated tests.

# 4.44.0
 * Added brand new Tooltips inside Row1Copy, Row3SetupStore, and SFC CrimeRows.
 * Updated test snapshots.

# 4.43.0
 * Replaced legacy Tooltips in CrimeRow, Editor, ExperienceBar and PanelTitle Components on brand new react-tooltip.

# 4.42.1
 * Added brand new Tooltips Component logic into the CDs container.
 * Added new count/total/text animation logic into the CDs container.
 * Added smooth transition for CDs progress bar.
 * Fixed CDs bar line transiotion once it come to 100% and then instantly returns to 0%.
 * Fixed CDs count change switching.
 * Updated CDs decremental animation logic.
 * Updated CDs tests snapshots.

# 4.41.5
 * Updated PanelTitle Tests environment.

# 4.41.4
 * Fixed StarLVL icon positioning.

# 4.41.3
 * Fixed SlickSlider arows & dots positioning.

# 4.41.2
 * Fixed styles for Bootlegging and SFC placeholders.

# 4.41.1
 * Fixed CDs count Wrap layout in Bootleging.

# 4.41.0
 * Added brand new Placeholder component.
 * Updated placeholders in SFC and Bootlegging for better handling while devices connection is slow.

## 4.40.1
 * Added brand new manual desktop mode checker logic.
 * Fixed Global wrapper padding while freeze mode.
 * Fixed SFC banner dollar animations in mobile/tablet mode.

## 4.39.0
 * Added star placeholder for 3thd row in Bootlegging.
 * Written tests.

## 4.38.0
 * Added SVG star in 3rd Bootleging row instead of using png one.

## 4.37.0
 * Removed success ending phase in CDs copying process.

## 4.36.0
 * Added new SVG star to SFC last crime row.
 * Updated Tests.

## 4.35.0
 * Card Component in HUB switched to global Star component usage.

## 4.34.0
 * Added SVG Stars to Experience bar component.

## 4.34.0
 * Added Star icon Component.

## 4.33.2
 * Fixed type in initialState files of crimes.

## 4.33.1
 * Fixed SVG Stars icons layout.

## 4.33.0
 * Added SVG Stars icons for HUB Component.

## 4.32.0
 * Added banner animations for tablet/mobile layouts.

## 4.31.0
 * Added brand new CDs layout/animation in Bootlegging.

## 4.30.3
 * Fixed Banner animation finishing phase in Bootlegging.
 * Updated Banner tests.

## 4.30.2
 * Fixed total CDs amount layout once appered with queue.

## 4.30.1
 * Fixed money to collect amount layout transition.

## 4.30.0
 * Added special text layout for mobile orientation in Copy CDs Outcome.

## 4.29.2
 * Updated dependency for react-transition-group.
 * Partiarly refactored CD Component.
 * Fixed extra rerenders in Sell CDs Crime row.
 * Fixed extra rerenders in HorizontalSelect Component.

## 4.28.1
 * Added special font for special Outcomes in whole crimes.
 * fixed padding in 4th Crime row of Bootlegging.

## 4.27.0
 * Added data fetcher on each tab user's entering.
 * Fixed CDs timer overloop when the queue is empty.

## 4.26.2
 * Partiarly refactored Outcome Container.
 * Updated Tests.

## 4.26.1
 * Fixed StarButton layout in tablet/mobile modes.

## 4.26.0
 * Added tests for Global Outcome Component.

## 4.25.0
 * Fixed Bootlegging Animation banner running phase.

## 4.24.4
 * Fixed Outcomes Row1Copy Hook.

## 4.24.3
 * Fixed Row1Copy Outcome desc in case of Failed and Jailed.

## 4.24.2
 * Fixed Outcome height recalculation once screen is resized.

## 4.24.1
 * Fixed core-layout max-width.

## 4.24.0
 * Removed local DebugBox component in way of using the global one.

## 4.23.1
 * Removed legacy title creator function from CoreLayout.
 * Fixed cursor pointer in DropDown panel.

## 4.22.0
 * Fixed Outcome number font size.
 * Fixed the “+” symbol appear on the packs of CDs.
 * The progress bar used on “Setup online store” switched to green.
 * Expanded HorizontalSelect Slider show width.
 * Fixed font issue in Bootlegging Outcome result section.
 * Word can be aligned center between the two arrows in HorizontalSelect Slider.

## 4.21.1
 * Fixed progressBar layout in SFC crimeRows.

## 4.21.0
 * Removed unused code from Crime App.

## 4.20.1
 * Added feature to switching CrimeRow title based on mediaType rule.
 * Fixed Bootlegging required items in rows layout.
 * Fixed Bootlegging CDsSelector position coords once opened.

## 4.19.0
 * Renamed type of bootlegging.cssmodule to bootlegging.cssmodule.scss for better processes handling.

## 4.18.0
 * Fixed layout for placeholders in Bootlegging.

## 4.17.0
 * Added getTitle selector helper.

## 4.16.2
 * Fixed active button state switching once user is jailed.

## 4.16.1
 * Added green progress bar full width under subcrime name.
 * Fixed new placeholder (simple rectangle) in mobile mode.

## 4.15.0
 * Added new placeholder (simple rectangle).

## 4.14.1
 * Updated common Sagas in Crimes for better stats update handling.
 * Added available prop to global crimeRows selector. Updated Selector tests.
 * Added row3SetupStore shouldComponentUpdate func.
 * Removed legacy from CopyProgress index component.
 * Added ability to switch 3rd and 4th crime rows during live under debug mode.

## 4.13.1
 * Legacy .png Banner Arrows Buttons switched on .svg ones.

## 4.12.1
 * Fix for Outcome layout in case of fast clicking next/prev CrimeRows Attempt Buttons.

## 4.12.0
 * Added tests for new layout in CrimeRowButton.

## 4.11.0
 * CooldownStar Component has been integrated inside Crimes Row Button.

## 4.10.0
 * Fixed Bootlegging placeholder bottom margins.
 * Fixed reducers Attempt_fetch case.

## 4.9.1
 * Improved Banner animation rotation in case of already setted CDs queue.
 * Improved disk animation layout in CDs block on levels over 90.
 * Updated Tests suits.

## 4.8.1
 * Fixed disk animation appear in CDs block.
 * Updated Tests suits.

## 4.7.1
 * Fixed integration for 3 separated CDs Packs updating.
 * Fixed animation appear for ExperienceBar progress motion.
 * Updated tests suits.

## 4.7.0
 * Added Back-end integration for 3 separated CDs Packs updating.

## 4.6.1
 * Added 3 separated CDs Packs layout in PopUp.
 * Updated Action, Reducer and Row1Copy Logic to enable the output of three different types of disks.
 * Tests Updated.
 * Minor fixes.

## 4.5.1
 * Added icons for CDs Outcome Copying process layout.
 * Updated Pck of Blank CDs icon in PopUp.
 * Fixed ToolTip PopUp position in tablet and mobile mode.
 * Fixed Placeholder Component layout in tablet and mobile mode.
 * Fixed CrimeRowButton layout in tablet/mobile mode.

## 4.4.1
 * Added polling for Pickpocketing crime.
 * Added Row timer update logic for Pickpocketing.
 * Updated preset in SVG crimes icons.
 * Minor fixes.

## 4.3.1
 * Removed legacy png icons from Shoplifting crime, replaced on SVG ones.
 * Minor improvements and fixes.

## 4.2.0
 * Removed legacy png icons from Graffiti crime, replaced on SVG ones.
 * Minor improvements.

## 4.1.0
 * Added Global and local SVGIcons Components in SFC.
 * Tests written for new Components.

## 4.0.0
 * Tests updated for several Components.
 * Fixed all bugs from Joe report since January 25.
 * Improived Stability.

## 3.9.0
 * Added tests for Tooltip Component.

## 3.8.1
 * Legacy Tooltip replaced on React-based one.
 * Tests suits updated for a long time.
 * Minor code/styles fixes.

## 3.7.1
 * Improved stability in InfoPanel component.
 * Fixes layout in manual desktop mode.

## 3.6.0
 * Added Global CooldownStar Animation Component.

## 3.5.0
 * Created Pickpocketing Placeholder in desktop/tablet/mobile layouts.
 * Created Pickpocketing Banners layout.
 * Cutted Pickpocketing Banners styles from global scope, create local one instead.

## 3.4.0
 * Basic Pickpocketing App structure created.

## 3.3.4
 * Fixed DropDown data update during mediaType(orientation) change.

## 3.3.3
 * Fixed DropDown data update during props change.

## 3.3.2
 * Fixed CrimesSlider Component animation logic. Prevented phantom animation changing.

## 3.3.1
 * Added crime main load action on each Location Change for updating crime data on re-layout.
 * Fixed Mobile/Tablet layouts for Crimes Plaseholders.
 * Fixed styles for experienceBarWrap.
 * Fixed incorrect animation slide effect on crimes change.

## 3.2.0
 * Refactored crimes particular sagas structure.
 * Other minor improvements.

## 3.1.0
 * Refactored common sagas structure of the app.

## 3.0.2
 * Minor style-fixes for icons in Bootlegging and Hub Components.

## 3.0.1
 * Crimes List/Row bussiness logic has been migrated to one-manipulating global place, instead of copy-past using from each Crime.
 * Written tests for new state holders.
 * Minor Bootlegging bug fixes(CDs count animation fix, SetupOnlineStore icons layout fix, Sell CDs subcrime button fix).
 * Crime Rows shouldComponentUpdate function are now transmitted through the HOC prop to Global Crime Row, instead of expliciting including them in it.

## 2.11.1
 * Written tests for Popup and SlickSlider Components.
 * Improved CrimesSlider logic, improved speed.
 * Removed legacy event handler from Popup Component.
 * Fixed some minor errors.

## 2.10.1
 * Added tests for HorizontalSelect, InfoPanel, PanelTitle.
 * Improved component logic for components above.

## 2.9.1
 * Added tests for Editor, Banner and ExperienceBar Global Components.
 * Refactored ExperienceBar Global Components.

## 2.9.1
 * Replaced blurred effect for CanSelector img items on siluetely icons.
 * Fixed shouldComponentUpdate in CrimeRow to allow update rows with changed sprayCans value.
 * Written extra test for CrimeList to check if it can update rows with diff spayCans value once the last is changed.

## 2.8.1
 * Fixed Outcome container height for devices resizing/oriantation change.

## 2.8.0
 * Added extra data replacement for live-mode in crime play.

## 2.7.1
 * Improved shouldComponentUpdate logic for all components, made it more predictable and single placed.
 * Fixed extra rerenders in Bootlegging crimes.
 * Fixed reset Stats sagas function while changing crimes.
 * Rewritten some tests.

## 2.6.0
 * Added reset Stats sagas function for revert user LVL in particular crime.

## 2.5.1
 * Replaced Banners with unique global Banner Component for all Crimes.
 * Rewritten some test for them.
 * Implemented RESET button for Crimes Editor Component.
 * Fixed some legacy stylesheet.

## 2.4.0
 * Added Outcome Counter Layout.
 * Created Banner Global Component for future migration from copy-past banner wrapper in each Crime with extra event listeners.

## 2.3.0
 * Added dynamic Title-Tutorials for crimes.

## 2.2.0
 * Refactored Hub Component.
 * Written tests for Hub route.
 * Made Hub Component more security-safe.

## 2.1.2
 * Fixed viewport dynamic changing that previously does not affect Crimes layout to change.

## 2.1.1
 * Refactored Outcome Component and animation Trancitions of it.
 * Improved speed, reduced unnesessary rerenders.
 * Fixed outcome speed layout.

## 2.0.1
 * Major Crimes Update!
 * Magnificaly improved speed of all actions in crimes.
 * Added shouldComponentUpdate method in many components (CrimeRow, Banner, etc.) to prevent extra rerenders.
 * Repaced Component on PureComponent React prototypes in cases where it possible.
 * Fixed a lot of bugs and legacy in many Components (ExpirienceBar, InfoPanel, PanelTitle).
 * Improved styles in CopyProgress Bootlegging Component.
 * Updated tests.

## 1.5.4
 * Integrated Loadabled library for async bundles load.
 * Update routing based on Loadable library structure.
 * Improved speed of components initial load.
 * Fixed extra rerenders in core crimes Components.

## 1.5.4
 * Added Dynamic Crimes AppHeader(with Title & Tutrial).
 * Fixed Editor value error in input field of Bootlegging and Searchforcash crimes.
 * Fixed gerde selector height cutting in Bootlegging of 1st crimesrow.
 * Fixed Hot Module Replacement in App CORE.
 * Other minor fixes.

## 1.4.3
 * Fixed Graffiti Banner layout with proper user name on start loading.
 * Fixed correct sprays decreasing for same spray in the several rows.

## 1.4.2
 * Written tests for DropDown Global Component.
 * Rewritten selectors in Banner of Bootlegging component.
 * Added tests for DebugInfo and DroDown Components.
 * Added fixes for better CrimesSlider animation.
 * Updated gsap module from 1.20.2 to 2.0.2.
 * Updated redux module from 3.6.0 to 4.0.0.
 * Updated react-redux module rom 4.4.5 to 5.0.7.
 * Updated redux-responsive from 3.0.2 to 4.3.8.
 * Fixed bootlegging Banner Component animation on end stage.

## 1.4.1
 * Added tests for DebugInfo and DroDown Components.

## 1.3.1
 * Fixed Error with animation in CrimesSlider when Crimes is slides.
 * Fixed problem with unhidden arrows on unmount crime stage.
 * Refactored Root router for better perfomance.
 * Other minor improvements.

## 1.2.2
 * Fixed Touch events with CrimesSlider navigation on mobile devices.

## 1.2.1
 * Added Outcome emptier on each Crimes state `componentWillUnmout` to prevent unnessary rerenders.
 * Fixed incorrect multy-rerenders of the Banners components of SearchForCash and Shoplifting Crimes.
 * Fixed Attempt button freeze in CrimeRow after immediate click on other CrimeRow Attempt button.
 * Improved code in Outcome and Redux Saga of all Crimes.
 * Fixed animation in Row4Collect component.
 * Added withRouter wrapper for missing Route components.

## 1.1.1
 * Major Crime Apps update!
 * Migrated from react-router .v2 to react-router-dom .v4
 * Migrated from react-transition-group .v1 to react-transition-group .v2
 * Rewrited async chunk (routes) bundles loading from old to modern dinamyc logic.
 * Rewrited CSSTransition animations from old style to modern one.
 * Rewritted React-Routing from old style to modern one.
 * Removed legacy and unstable React code from CrilesSlider.
 * Removed legacy and unstable React code from Routes index files.
 * Removed legacy and unstable React code from AppContainer.
 * Removed legacy and unstable React code from Editor component.
 * Removed legacy and unstable React code from ExperienceBar Component.
 * Removed legacy and unstable React code from Card.
 * Fixed multy-times chunks loading and actions invoke on every enter on the new Crime.
 * Fixed multy-times fetch requests firing on Crimes loading stage.
 * Fixed incorrect animation stoping once currentQueue in Bootlegging is being empty.
 * Improved speed of all CSSTransition animations while them in work.
 * Improved speed of Crimes load in CrimesSlider component.
 * Added more predictable state managing in Outcome component while its state updates.
 * Added few common actions/reducers to move React component-based state logic in Redux single-truth state.
 * Written tests for CrimesSlider component.

## 0.20.1
 * Rewrited and optimized Global CrimeRow, CrimeRowButton and Outcome components.
 * Written tests for Global CrimeRow, CrimeRowButton components.
 * Refactored OutcomeContainer Global component.
 * Updated Jest tests shanshots.
 * Fixed minor bugs and issues.

## 0.19.0
 * ProgressCircle and suits files in Shoplifting has been rewritten on the TypeScript.

## 0.18.1
 * OutcomeHeight calculation fix in Graffiti.
 * Written an extra action and reducer for Redux state.

## 0.18.0
 * Added selectors for ProgressCircle component in Shoplifting.
 * Written tests for it.

## 0.17.1
 * Fixed Outcome animation in Bootlegging.
 * Fixed Banner Component layout in Graffiti.

## 0.17.0
 * Major refactoring of Bootlegging Crime Components. Written updated tests.

## 0.16.0
 * Added selectors for CrimeRows and CrilesList in Bootlegging crime.

## 0.15.2
 * Refactored Row1Copy and CrimeList and CanSelectors component in Bootlegging to fix problem with blankCDs counter.
 * Fixed and updated reducers in Bootlegging.

## 0.15.1
 * Added selectors for CrimeRow and CrilesList in Graffiti crime. Written tets for them.
 * Minor fixes and refactoring.

## 0.14.0
 * Added selectors for CrimeRow and CrilesList in SearchForCash crime. Written tets for them

## 0.13.0
 * Added random ghost animation appear for Banners SearchForCash Component.
 * Written tests for GYBannerRandomizer function.
 * Rewrited tests for Banners components fue to the reason of implemention randomizer for Graveyard banner in Shoplifting.

## 0.12.1
 * Refactored CrimeList and ProgressCDs components in Bootlegging due for the reason of a bug.

## 0.11.1
 * Upgrated tests for Banner and selectors of its component.

## 0.10.1
 * Added banner animation hide/show feature, based on cameraStatus key.
 * added selector for cameraStatus in Banners.
 * Fixed tests in Sholifting Banner folder for banner and selectors test suits.

## 0.9.2
 * Minor fixes in shoplifting core.
 * Load button animation fix.
 * Disabled reduxLogger and reduxDevtools for production build.

## 0.10.1
 * Implemented css loader animation in attempt button, insted of .gif animation.
 * Fixed ESlint TSlint and STYLElinst rules.
 * Other minor fixes in crimes core files.

## 0.9.1
 * Minor fixes added in Shoplifting and Graffiti crimes.

## 0.9.0
 * Writed Unit tests for Graffiti crimes.

## 0.8.0
 * Writed Unit tests for Shoplifting crimes.

## 0.7.0
 * Added StyleLint for all crimes and Husky for Crimes.

## 0.7.0
 * Cuted Flow type checking and replaced with TypeScript checking.

## 0.6.0
 * Implemented Flow type checking inside the Shoplifting Crime.

## 0.5.0
 * Implemented Reselect inside the Shoplifting Crime.

## 0.4.1
 * Updated Graffiti Banner rotation logic. Refactored CrimeRow and Banner Components

## 0.4.0
 * Added Shoplifting Crime.

## 0.3.8
 * Refactored and separated Global CrimewRow and Outcome Components.

## 0.3.7
 * Improved current Crimes state.

## 0.3.6
 * Refactored Banner logic in Graffiti crime.

## 0.3.5
 * Added tiny fixes for Bootlegging crime.

## 0.3.4
 * Added tiny fixes for Bootlegging crime.

## 0.3.3
 * Added Graffiti Crime. Improved state stability
