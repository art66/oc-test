export interface IProps {
  store?: object
  common?: {
    debugBox: object
    infoBox: {
      msg: any
      color: any
    }
  }
  closeDebugAction?: () => void
  injectSagaAction?: () => void
  popupHideAction?: () => void
}
