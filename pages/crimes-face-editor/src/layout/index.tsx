import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import InfoBox from '@torn/shared/components/InfoBox'
import DebugInfo from '@torn/shared/components/DebugBox'
import AppHeader from '@torn/shared/components/AppHeader'

import { Header } from '../components'
import { Manage, Users, Pagination } from '../containers'

import { hideDebugInfo, popupHide } from '../modules/actions'
import { LOAD_ROOT_DATA } from '../constants'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

const APP_TITLE = 'Crimes Face Editor'
const ESCAPE_KEY_ID = 27

class AppLayout extends PureComponent<IProps> {
  componentDidMount() {
    const { injectSagaAction } = this.props

    injectSagaAction()
  }

  _handlerClick = () => {
    const { closeDebugAction } = this.props

    return closeDebugAction()
  }

  _handlerKeyDown = e => {
    const { popupHideAction } = this.props

    const keyCode = e.which

    if (keyCode === ESCAPE_KEY_ID) {
      return popupHideAction()
    }
  }

  _renderSystemMessages = () => {
    const {
      common: { debugBox = null, infoBox = null }
    } = this.props

    if (!debugBox && !infoBox) {
      return null
    }

    if (debugBox) {
      return <DebugInfo debugMessage={debugBox} close={this._handlerClick} />
    }

    return <InfoBox {...infoBox} />
  }

  render() {
    const titles = {
      default: {
        title: APP_TITLE,
        ID: 0
      }
    }

    return (
      <div className={styles.appWrapper} onKeyDown={this._handlerKeyDown}>
        {this._renderSystemMessages()}
        <AppHeader clientProps={{ titles }} appID={'null'} />
        <Header />
        <div className={styles.body}>
          <Manage />
          <Users />
          <Pagination />
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ app }) => ({
  common: app.common,
  hidePopup: app.hidePopup
})

const mapDispatchToProps = dispatch => ({
  injectSagaAction: () => dispatch({ type: LOAD_ROOT_DATA }),
  closeDebugAction: () => dispatch(hideDebugInfo()),
  popupHideAction: () => dispatch(popupHide())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppLayout)
