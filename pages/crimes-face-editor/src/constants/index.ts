// -------------------------------------------
// ACTION-REDUCERS
// -------------------------------------------
export const LOAD_HUB_DATA = 'LOAD_HUB_DATA'
export const SHOW_DEBUG_INFO = 'SHOW_DEBUG_INFO'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_DEBUG_INFO = 'HIDE_DEBUG_INFO'
export const CREATE_NEW_TAG = 'CREATE_NEW_TAG'
export const NEW_TAG_CREATED = 'NEW_TAG_CREATED'
export const TAG_DELETE = 'TAG_DELETE'
export const TAG_DELETE_DONE = 'TAG_DELETE_DONE'
export const TAG_DELETED = 'TAG_DELETED'
export const SEARCH_TAG = 'SEARCH_TAG'
export const SEARCH_TAG_DELETE = 'SEARCH_TAG_DELETE'
export const SEARCH_TAG_CREATE = 'SEARCH_TAG_CREATE'
export const SEARCH_TAG_POPUP = 'SEARCH_TAG_POPUP'
export const DROP_DOWN = 'DROP_DOWN'
export const DROP_DOWN_SHOW = 'DROP_DOWN_SHOW'
export const DROP_DOWN_HIDE = 'DROP_DOWN_HIDE'
export const POPUP = 'POPUP'
export const POPUP_SHOW = 'POPUP_SHOW'
export const POPUP_HIDE = 'POPUP_HIDE'
export const ADD_TAG = 'ADD_TAG'
export const TAG_ADDED = 'TAG_ADDED'
export const USER_TAG_ADDED = 'USER_TAG_ADDED'
export const TAG_ALREADY_EXIST = 'TAG_ALREADY_EXIST'
export const EMPTY_TOOLTIP = 'EMPTY_TOOLTIP'
export const TAG_DELETE_CANCEL = 'TAG_DELETE_CANCEL'
export const SET_PAGE_ID = 'SET_PAGE_ID'
export const CHANGE_PAGE_NUMBER = 'CHANGE_PAGE_NUMBER'
export const ROOT_DATA_LOADED = 'ROOT_DATA_LOADED'
export const LOAD_ROOT_DATA = 'LOAD_ROOT_DATA'
export const SET_USER_STAT = 'SET_USER_STAT'
export const USER_STAT_SETTED = 'USER_STAT_SETTED'
export const FILTER_FACES = 'FILTER_FACES'

// -------------------------------------------
// FETCH URLS
// -------------------------------------------
export const ROOT_URL = 'loader.php?sid=manageCrimesTargets&step='

// -------------------------------------------
// COMMON URLS
// -------------------------------------------
export const TAG_HAS_BEEN_CREATED = 'New tag created!'
export const TAG_HAS_BEEN_DELETED = 'Tag has been deleted'
export const TAG_ALREADY_EXISTS = 'Tag already exist'

// -------------------------------------------
// USERS CONST
// -------------------------------------------
export const USERS_PER_PAGE = 120
export const PAGE_ID_FIXER = 1
