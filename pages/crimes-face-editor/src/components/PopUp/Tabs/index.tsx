import React, { Component } from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const LAYOUT_SEARCH = 'search'
const LAYOUT_TABS = 'tabs'
const TABS = ['Gender', 'Age', 'Height', 'Muscular']

class Tabs extends Component<IProps> {
  _checkLayout = () => {
    const { layout } = this.props
    const isTabLayout = layout === LAYOUT_TABS

    return isTabLayout
  }

  _handlerClick = e => {
    e.preventDefault()
    e.stopPropagation()

    const { setLayout } = this.props

    const isTabLayout = this._checkLayout()
    const currentLayout = isTabLayout ? LAYOUT_SEARCH : LAYOUT_TABS

    setLayout(currentLayout)
  }

  _getTab = tabLabel => {
    const { user } = this.props

    const selectedValue = user.stats && user.stats[tabLabel.toLowerCase()]
    const isValueSelected = selectedValue ? styles.itemSelected : ''

    return (
      <div key={tabLabel} className={`${styles.tab} ${isValueSelected}`}>
        <span className={styles.title}>{selectedValue || tabLabel}</span>
      </div>
    )
  }

  _renderTabs = () => {
    return TABS.map(tab => {
      return this._getTab(tab)
    })
  }

  render() {
    const isTabLayout = this._checkLayout()
    const tabClass = isTabLayout ? styles.tabsLayout : ''

    return (
      <div className={`${styles.tabsContainer} ${tabClass}`} onMouseDown={this._handlerClick} data-type='popup'>
        {this._renderTabs()}
      </div>
    )
  }
}

export default Tabs
