import { IState as IPopUpState, IUser } from '../../types'

export interface IProps extends IUser, IPopUpState {
  setLayout: (label: string) => void
}
