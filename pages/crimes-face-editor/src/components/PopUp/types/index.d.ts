import { ICharOptions } from '../Columns/types'

export interface IUser {
  user?: {
    stats?: object
  }
}

export interface IProps extends ICharOptions {
  targets: {
    stats?: object
    _id: {
      $oid: string
    }
  }[]
  activeUserID: string
  setStatAction: (key: string, value: string) => void
}

export interface IState {
  layout?: string
}
