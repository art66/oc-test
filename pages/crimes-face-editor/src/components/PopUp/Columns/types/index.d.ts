import { IState as IPopUpState, IUser } from '../../types'

export interface ICharOptions extends IUser {
  characterOptions?: {
    type: string
    list: object[]
  }[]
}
export interface IProps extends IPopUpState, ICharOptions {
  setColumnValue: (key: string, value: string) => void
}
