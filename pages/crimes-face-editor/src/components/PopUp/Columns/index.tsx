import React, { Component } from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const NO_ITEM_SELECTED = 'none'
const ITEM_NOT_FOUND = ''
const DATA_TYPE = 'popup'

class Columns extends Component<IProps> {
  _handlerClick = e => {
    e.preventDefault()

    const { setColumnValue } = this.props
    const {
      dataset: { column, label }
    } = e.target

    setColumnValue(column, label)
  }

  _getColumn = column => {
    const { user } = this.props

    return column.list.map(label => {
      const selectedItemLabel = (user.stats && user.stats[column.type]) || NO_ITEM_SELECTED
      const isItemSelected = selectedItemLabel === label ? styles.itemSelected : ITEM_NOT_FOUND

      return (
        <div key={label} className={styles.item}>
          <button
            type='button'
            data-column={column.type}
            data-label={label}
            data-type={DATA_TYPE}
            className={`${styles.buttonColumnItem} ${isItemSelected}`}
            onMouseDown={this._handlerClick}
          >
            <span className={styles.title}>{label}</span>
          </button>
        </div>
      )
    })
  }

  _renderColumns = () => {
    const { characterOptions } = this.props

    if (!characterOptions) {
      return null
    }

    return characterOptions.map((column, index) => {
      return (
        <div key={index} className={styles.columnWrap}>
          {this._getColumn(column)}
        </div>
      )
    })
  }

  render() {
    return <div className={styles.columnsContainer}>{this._renderColumns()}</div>
  }
}

export default Columns
