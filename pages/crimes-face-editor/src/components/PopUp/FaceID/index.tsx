import React, { PureComponent } from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class FaceID extends PureComponent<IProps> {
  _handlerClick = e => {
    e.preventDefault()
  }

  render() {
    const { faceID } = this.props

    return (
      <div className={styles.faceWrap} data-type='popup' onMouseDown={this._handlerClick}>
        <span className={styles.face}>FaceID: #{faceID}</span>
      </div>
    )
  }
}

export default FaceID
