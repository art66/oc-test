import React, { Component } from 'react'
import FaceID from './FaceID'
import Tabs from './Tabs'
import Columns from './Columns'
import { Search } from '../../containers'
import getFilteredUser from '../../utils/getFilteredUser'

import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'

const SEARCH_LAYOUT = 'search'
const SEARCH_PLACEHOLDER = 'Search tag to add...'
const ACTION_TYPE = 'popup'

class PopUp extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      layout: SEARCH_LAYOUT
    }
  }

  _handlerSetValue = (key: string, value: string) => {
    const { setStatAction } = this.props

    setStatAction(key, value)
  }

  _handlerSetLayout = layout => {
    this.setState(prevState => ({
      ...prevState,
      layout
    }))
  }

  _getClickedUser = () => {
    const { targets, activeUserID } = this.props

    const clickedUserProps = getFilteredUser(targets, activeUserID)

    return clickedUserProps
  }

  _renderLayout = () => {
    const { layout } = this.state
    const { characterOptions } = this.props

    const clickedUserProps = this._getClickedUser()

    if (layout === SEARCH_LAYOUT) {
      return <Search placeholder={SEARCH_PLACEHOLDER} activeSearch={true} actionType={ACTION_TYPE} />
    }

    return (
      <Columns user={clickedUserProps} characterOptions={characterOptions} setColumnValue={this._handlerSetValue} />
    )
  }

  render() {
    const { layout } = this.state
    const { activeUserID } = this.props

    const clickedUserProps = this._getClickedUser()
    const searchLayout = layout ? styles.searchLayout : ''

    return (
      <div className={`${styles.popUpContainer} ${searchLayout}`}>
        <FaceID faceID={activeUserID} />
        <Tabs user={clickedUserProps} setLayout={this._handlerSetLayout} layout={layout} />
        {this._renderLayout()}
      </div>
    )
  }
}

export default PopUp
