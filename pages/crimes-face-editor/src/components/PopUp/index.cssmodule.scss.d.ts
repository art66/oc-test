// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'columns': string;
  'columnsFlex': string;
  'globalSvgShadow': string;
  'popUpContainer': string;
  'searchLayout': string;
}
export const cssExports: CssExports;
export default cssExports;
