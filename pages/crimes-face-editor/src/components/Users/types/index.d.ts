export interface IProps {
  currentPageID?: number
  activeUserID: number
  tagNameFilter: string
  popupShowAction: (ID: string) => void
  popupHideAction: () => void
  targets: {
    image: string
    title: string
    stats: object
    tags: string[]
    _id: {
      $oid: string
    }
  }[]
}

export interface IState {
  currentUserID: number
}
