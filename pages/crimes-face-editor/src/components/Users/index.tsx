import React, { PureComponent } from 'react'

import User from './User'

import getFilteredUsersByTag from '../../utils/getFilteredUsersByTag'
import getFilterUsersByPage from '../../utils/getFilterUsersByPage'

import { USERS_PER_PAGE } from '../../constants'

import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'

const NO_TARGETS = 0

class Users extends PureComponent<IProps, IState> {
  _showPopUp = clickedUserID => {
    const { popupShowAction, activeUserID } = this.props

    if (activeUserID) return

    popupShowAction(clickedUserID)
  }

  _hidePopUp = () => {
    const { popupHideAction } = this.props

    popupHideAction()
  }

  _checkActiveUser = index => {
    const { activeUserID } = this.props

    const userActive = index === activeUserID && activeUserID !== null

    return userActive
  }

  _getCurrentPageUsers = () => {
    const { targets, currentPageID, tagNameFilter } = this.props

    if (!targets || targets.length === NO_TARGETS) {
      return []
    }

    if (tagNameFilter) {
      return targets
        .filter(user => getFilteredUsersByTag(user, tagNameFilter))
        // eslint-disable-next-line no-unused-vars
        .filter((_user, index) => getFilterUsersByPage(index, currentPageID))
    }

    // eslint-disable-next-line no-unused-vars
    return targets.filter((_user, index) => getFilterUsersByPage(index, currentPageID))
  }

  _getImgURL = image => {
    return `/images/v2/2sef32sdfr422svbfr2/crimes/targets_faces/${image}.png`
  }

  _renderUsers = () => {
    const filteredUsersForPage = this._getCurrentPageUsers()
    const usersPerPageArray = Array.from(Array(USERS_PER_PAGE).keys())

    return usersPerPageArray.map(index => {
      const user = filteredUsersForPage && filteredUsersForPage[index]
      const { _id: { $oid = '' } = {}, image = 'none', stats = {} } = user || {}

      const userActive = this._checkActiveUser($oid)

      return (
        <User
          key={index}
          ID={$oid}
          userActive={userActive}
          stats={stats}
          faceImgURL={this._getImgURL(image)}
          showPopUp={this._showPopUp}
          hidePopUp={this._hidePopUp}
        />
      )
    })
  }

  render() {
    return <div className={styles.usersContainer}>{this._renderUsers()}</div>
  }
}

export default Users
