import React, { PureComponent } from 'react'
import { PopUp } from '../../../containers'

import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'

class User extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      imgLoadStatus: '',
      faceImgURL: ''
    }
  }

  static getDerivedStateFromProps(nextProps: IProps, nextState: IState) {
    // @ts-ignore: Unreachable code error
    if (nextState.faceImgURL !== nextProps.faceImgURL) {
      return {
        imgLoadStatus: '',
        faceImgURL: ''
      }
    }

    return nextState
  }

  _handlerClick = ({ target }) => {
    const {
      dataset: { id: ID }
    } = target

    const { showPopUp } = this.props

    showPopUp(ID)
  }

  _handlerBlur = () => {
    const { hidePopUp } = this.props

    // permit search input fire while popup active
    const getCurrentClick = e => {
      if (e.target.dataset.type === 'popup') {
        return
      }

      document.body.removeEventListener('click', getCurrentClick)
      hidePopUp()
    }

    document.body.addEventListener('click', getCurrentClick)
  }

  _renderPopUpBlock = () => {
    const { userActive } = this.props

    if (!userActive) {
      return null
    }

    return <PopUp />
  }

  _imgFetcher = () => {
    const { faceImgURL } = this.props

    const img = new Image()

    img.onload = () => {
      this.setState({
        imgLoadStatus: 'loaded',
        faceImgURL
      })
    }

    img.onerror = () => {
      this.setState({
        imgLoadStatus: 'error',
        faceImgURL
      })
    }

    img.src = faceImgURL
  }

  _getImg = imgSrc => {
    this._imgFetcher()

    const { imgLoadStatus } = this.state

    if (!imgLoadStatus) {
      return null
    }

    if (imgLoadStatus === 'error') {
      return 'No Image'
    }

    return <img src={imgSrc} className={styles.userImage} />
  }

  _getUserImg = () => {
    const { ID, userActive, faceImgURL, stats } = this.props

    if (!faceImgURL || faceImgURL.match(/none/gi)) {
      return <span className={styles.imgPlaceholder} />
    }

    const clickedClass = userActive ? styles.imageClicked : ''

    const fulfilledUserClass = Object.values(stats).some(value => value === '') ? '' : styles.fulfilledUser

    return (
      <button
        type='button'
        data-id={ID}
        className={`${styles.userButton} ${clickedClass} ${fulfilledUserClass}`}
        onClick={this._handlerClick}
        onBlur={this._handlerBlur}
      >
        {this._getImg(faceImgURL)}
      </button>
    )
  }

  render() {
    return (
      <div className={styles.userContainer}>
        {this._getUserImg()}
        {this._renderPopUpBlock()}
      </div>
    )
  }
}

export default User
