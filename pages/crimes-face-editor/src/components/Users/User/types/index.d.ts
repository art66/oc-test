export interface IProps {
  stats: object
  ID: string | number
  key: string | number
  faceImgURL: string
  showPopUp: (ID: number) => void
  hidePopUp: () => void
  userActive: boolean
  setLoadStatus?: (status: boolean) => void
}

export interface IState {
  imgLoadStatus: string
  faceImgURL: string
}
