import { ITags } from '../DropDownList/types'

export interface IProps extends ITags {
  hideDropDown?: boolean
  tagnamedelete?: string
  tagnamecreate?: string
  tagnamepopup?: string
  tagnamefilter?: string
  activeSearch?: boolean
  activeUserID?: string
  setValue?: string
  type?: string
  placeholder: string
  actionType: string
  characterOptions: {
    type: string
    list: string[]
  }[]
  tagNameSearchAction: (tagName: string, type: string) => void
  dropDownAction: (status: boolean, type: string) => void
  filterFacesByTag: (tagName: string) => void
}

export interface IStore {
  inputValue: string
}
