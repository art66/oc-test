import React, { Component } from 'react'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import getFilteredUser from '../../utils/getFilteredUser'
import getDeleteTag from '../../utils/getDeleteTag'
import getNormalizedCharacteristics from '../../utils/getNormalizedCharacteristics'
import DropDownList from './DropDownList'

import { IProps, IStore } from './interfaces'
import styles from './index.cssmodule.scss'
import InputStringChecker from '@torn/shared/utils/stringChecker'
// import globalIcons from '@torn/shared/SVG/helpers/iconsHolder/global'

const SHOW_ALL = 'All'
const ACTION_TYPE_DELETE = 'delete'
const ACTION_TYPE_POPUP = 'popup'
const ACTION_TYPE_CREATE = 'create'
const SHOW_DROPDOWN = 'SHOW'
const LOWEST_LETTER_BORDER_ID = 60
const BIGGEST_LETTER_BORDER_ID = 95
const ENTER_BUTTON_ID = 27
const BACKSPACE_BUTTON_ID = 8

const SVG_PROPS = {
  iconsHolder: import(/* webpackChunkName: "globalSVGIcons" */ '@torn/shared/SVG/helpers/iconsHolder/global'),
  iconName: 'Search',
  color: '#bfbfb0',
  strokeWidth: 0,
  viewbox: '0 0 15 15',
  onHover: {
    active: true,
    fill: {
      color: '#6a6b59'
    }
  }
}

class Search extends Component<IProps, IStore> {
  private _stringChecker: any

  componentDidMount() {
    this._initializeStringChecker()
  }

  shouldComponentUpdate(nextProps: IProps) {
    return this._flagsChanged(nextProps) || this._mainDataChanged(nextProps) || this._listsChanged(nextProps)
  }

  _flagsChanged = nextProps => {
    const {
      tagnamecreate,
      tagnamedelete,
      tagnamefilter,
      tagnamepopup,
      hideDropDown,
      activeSearch,
      actionType,
      filter
    } = this.props

    const dropDownChanged = hideDropDown !== nextProps.hideDropDown
    const createSearchChange = actionType === 'create' && tagnamecreate !== nextProps.tagnamecreate
    const deleteSearchChange = actionType === 'delete' && tagnamedelete !== nextProps.tagnamedelete
    const filterSearchChange = tagnamefilter !== nextProps.tagnamefilter
    const popupChanged = tagnamepopup !== nextProps.tagnamepopup
    const activeSearchChanged = activeSearch !== nextProps.activeSearch
    const filterChanged = filter !== nextProps.filter

    return filterSearchChange
    || createSearchChange
    || deleteSearchChange
    || dropDownChanged
    || popupChanged
    || activeSearchChanged
    || filterChanged
  }

  _mainDataChanged = nextProps => {
    const { activeUserID, setValue, type, placeholder, actionType } = this.props

    const activeUserIDChanged = activeUserID !== nextProps.activeUserID
    const setValueChanged = setValue !== nextProps.setValue
    const typeChanged = type !== nextProps.type
    const placeholderChanged = placeholder !== nextProps.placeholder
    const actionTypeChanged = actionType !== nextProps.actionType

    return activeUserIDChanged
    || setValueChanged
    || typeChanged
    || placeholderChanged
    || actionTypeChanged
  }

  _listsChanged = nextProps => {
    const { tags, targets } = this.props

    const tagsChanged = JSON.stringify(tags) !== JSON.stringify(nextProps.tags)
    const targetsChanged = JSON.stringify(targets) !== JSON.stringify(nextProps.targets)

    return tagsChanged || targetsChanged
  }

  _initializeStringChecker = () => {
    this._stringChecker = new InputStringChecker({ maxLength: 50, restrictedSymbols: '[^0-9|a-z|A-Z|_|-]' })
  }

  _classNameWrapHolder = () => {
    const { actionType } = this.props

    const layoutClass = classnames({
      [styles.searchContainer]: true,
      [styles[`${actionType}Layout`]]: true
    })

    return layoutClass
  }

  _classNameInputHolder = dropDown => {
    const inputClass = classnames({
      [styles.search]: true,
      [styles.removeBorderRadius]: dropDown
    })

    return inputClass
  }

  _handlerClick = () => {
    const { dropDownAction, actionType, hideDropDown } = this.props

    if (actionType === ACTION_TYPE_POPUP || actionType === ACTION_TYPE_CREATE || !hideDropDown) return

    dropDownAction(false, SHOW_DROPDOWN.toUpperCase())
  }

  _handlerBlur = () => {
    const { dropDownAction, type, hideDropDown } = this.props

    if (type === ACTION_TYPE_POPUP || type === ACTION_TYPE_CREATE || hideDropDown) return

    dropDownAction(true, SHOW_DROPDOWN)
  }

  _handlerKeyDown = e => {
    const key = e.which

    if (key < LOWEST_LETTER_BORDER_ID || key > BIGGEST_LETTER_BORDER_ID) {
      if (key !== BACKSPACE_BUTTON_ID || key !== ENTER_BUTTON_ID) return

      e.preventDefault()
    }
  }

  _handlerChange = e => {
    e.preventDefault()

    const { actionType, tagNameSearchAction } = this.props

    const {
      isRestrictedSymbolFounded,
      value: valueNormalized
    } = this._stringChecker.checkString({ text: e.target.value })

    if (isRestrictedSymbolFounded) {
      return
    }

    tagNameSearchAction(valueNormalized, actionType.toUpperCase())
  }

  _handleClickFilter = () => {
    const { filterFacesByTag, tagnamedelete = '', tagnamefilter = '' } = this.props

    if (!tagnamefilter && (!tagnamedelete || !getDeleteTag(this._getTags(), tagnamedelete))) {
      return
    }

    filterFacesByTag(tagnamefilter !== tagnamedelete ? tagnamedelete : '')
  }

  _handleClearFilters = () => {
    const { actionType, tagNameSearchAction, filterFacesByTag } = this.props

    tagNameSearchAction('', actionType.toUpperCase())
    filterFacesByTag('')
  }

  _setInputValue = value => {
    const { actionType, tagNameSearchAction } = this.props

    tagNameSearchAction(value, actionType.toUpperCase())
  }

  _getTags = () => {
    const { actionType, tags, characterOptions } = this.props
    const charList = getNormalizedCharacteristics(characterOptions)

    if (actionType === ACTION_TYPE_POPUP) {
      return tags
    }

    return [...tags, ...charList]
  }

  _getClickedUser = () => {
    const { targets, activeUserID } = this.props

    const clickedUserProps = getFilteredUser(targets, activeUserID)

    return clickedUserProps
  }

  _getClearButton = () => {
    const { tagnamedelete, actionType } = this.props

    if (!tagnamedelete || actionType === ACTION_TYPE_POPUP) {
      return null
    }

    return (
      <button type='button' className={styles.clearButton} onClick={this._handleClearFilters}>
        x
      </button>
    )
  }

  _getSearchButton = () => {
    const { tagnamedelete, tagnamefilter, actionType } = this.props

    if (actionType === ACTION_TYPE_POPUP) {
      return null
    }

    const { iconsHolder, iconName, color, strokeWidth, viewbox, onHover } = SVG_PROPS
    const isFilterAllowed = !!getDeleteTag(this._getTags(), tagnamedelete)

    const classes = classnames({
      [styles.iconWrapperButton]: true,
      [styles.iconWrapperButtonActive]: isFilterAllowed,
      [styles.iconWrapperButtonClicked]: tagnamefilter,
      [styles.iconWrapperButtonActiveDropDown]: this._getDropDownList()
    })

    const getIconColor = () => {
      if (tagnamefilter) {
        return onHover.fill.color
      }

      if (isFilterAllowed) {
        return '#8a8a79'
      }

      return color
    }

    return (
      <button type='button' className={classes} onClick={this._handleClickFilter}>
        <SVGIconGenerator
          iconsHolder={iconsHolder}
          iconName={iconName}
          fill={{ color: getIconColor(), strokeWidth }}
          dimensions={{ viewbox }}
          onHover={{ ...onHover, active: isFilterAllowed }}
        />
      </button>
    )
  }

  _getNormalizedCharacteristics = () => {
    const { characterOptions } = this.props

    const recurringList = []

    characterOptions.forEach(({ type, list }) => {
      list.forEach(character => {
        recurringList.push({
          _id: {
            $oid: `${type}_${character}`
          },
          tag: character,
          type
        })
      })
    })

    return recurringList
  }

  _getDropDownList = () => {
    const { actionType, hideDropDown, tagAddedAction, ...inputValue } = this.props
    const filter = inputValue[`tagname${actionType}`] || SHOW_ALL

    if (hideDropDown && actionType === ACTION_TYPE_DELETE) {
      return null
    }

    const clickedUserProps = this._getClickedUser()


    return (
      <DropDownList
        setInputValue={this._setInputValue}
        user={clickedUserProps}
        tags={this._getTags()}
        type={actionType}
        filter={filter}
        tagAddedAction={tagAddedAction}
      />
    )
  }

  _renderSearchSection = () => {
    const { activeSearch } = this.props

    if (!activeSearch) {
      return null
    }

    return {
      clearButton: this._getClearButton(),
      searchButton: this._getSearchButton(),
      dropDown: this._getDropDownList()
    }
  }

  render() {
    const { placeholder, actionType, hideDropDown, ...inputValue } = this.props
    const { clearButton = null, searchButton = null, dropDown = null } = this._renderSearchSection() || {}
    const layoutClass = this._classNameWrapHolder()
    const inputClass = this._classNameInputHolder(dropDown)

    const inputValueFound = inputValue[`tagname${actionType}`]
    const actionDeleteFired = actionType === ACTION_TYPE_DELETE
    const noDropDown = hideDropDown && SHOW_ALL

    const value = inputValueFound || (actionDeleteFired && noDropDown) || ''

    return (
      <div className={layoutClass}>
        <div className={styles.searchInputWrap}>
          <input
            data-type={actionType}
            type='text'
            className={inputClass}
            onChange={this._handlerChange}
            onKeyDown={this._handlerKeyDown}
            onClick={this._handlerClick}
            onBlur={this._handlerBlur}
            value={value}
            placeholder={placeholder}
          />
          {clearButton}
          {searchButton}
        </div>
        {dropDown}
      </div>
    )
  }
}

export default Search
