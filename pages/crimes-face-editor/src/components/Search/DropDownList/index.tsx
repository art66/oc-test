import React, { PureComponent } from 'react'
import classnames from 'classnames'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const MINIMAL_CONTAINER_USERS_CAPACITY = 10
const ACTION_TYPE_POPUP = 'popup'
const SHOW_ALL = 'All'

class DropDownList extends PureComponent<IProps, any> {
  _dropDownClassNameHolder = () => {
    const dataList = this._filterDropDownData()

    const dropDownClass = classnames({
      [styles.dropDownContainer]: true,
      [styles.noScroll]: dataList && dataList.length <= MINIMAL_CONTAINER_USERS_CAPACITY
    })

    return dropDownClass
  }

  _handlerClick = e => {
    const { setInputValue, type, tagAddedAction } = this.props

    if (!setInputValue) return

    const {
      dataset: { tagname, id: ID }
    } = e.target

    if (type === ACTION_TYPE_POPUP) {
      tagAddedAction(ID)
      e.preventDefault()

      return
    }

    setInputValue(tagname)
  }

  _filterDropDownData = () => {
    const { tags, filter } = this.props

    const regFilter = new RegExp(filter, 'gi')
    const dataList = filter !== SHOW_ALL ? tags.filter(item => item.tag.match(regFilter)) : tags

    return dataList
  }

  _renderDropDownData = () => {
    const { type, user } = this.props
    const dataList = this._filterDropDownData()

    if (!dataList || dataList.length === 0) {
      return <span className={styles.dropdownPlaceholder}>No Tags Were Found.</span>
    }

    return dataList.map(item => {
      const isTagClicked =
        type === ACTION_TYPE_POPUP && user.tags.some(tag => tag === item.tag) ? styles.tagClicked : ''

      return (
        <button
          key={item.tag}
          type='button'
          data-type={type}
          data-id={item._id.$oid}
          data-tagname={item.tag}
          className={`${styles.buttonTag} ${isTagClicked}`}
          onMouseDown={this._handlerClick}
        >
          <span className={styles.btnTitle}>{item.tag}</span>
          {item.type && <span className={styles.btnType}>{item.type}</span>}
        </button>
      )
    })
  }

  render() {
    const dropDownClass = this._dropDownClassNameHolder()

    return <div className={dropDownClass}>{this._renderDropDownData()}</div>
  }
}

export default DropDownList
