export interface IUser {
  user?: {
    tags?: string[]
  }
}

export interface ITags extends IUser {
  targets?: {
    tags?: string[]
    _id: {
      $oid: string
    }
  }[]
  tags?: {
    type?: string
    tag: string
    clicked: boolean
    type?: string
    ID: number
    _id: {
      $oid: string
    }
  }[]
  type?: string
  filter?: string
  tagAddedAction: (ID: number) => void
}

export interface IProps extends ITags {
  setInputValue: (value: string) => void
}
