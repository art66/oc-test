import React, { PureComponent } from 'react'
import { CSSTransition } from 'react-transition-group'

import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'
import './anim.scss'

const SUCCESS = 'success'
const ANIM_PROPS = {
  className: 'tooltip',
  timeIn: 1000,
  timeExit: 300
}

class ToolTip extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      showTooltip: false,
      updateType: ''
    }
  }

  static getDerivedStateFromProps(nextProps: IProps, prevState: IState) {
    if (prevState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    return {
      showTooltip: nextProps.showTooltip
    }
  }

  _animStop = () => {
    this.setState({
      showTooltip: false,
      updateType: 'state'
    })
  }

  _animFinished = () => {
    const { emptyToolTipAction } = this.props

    emptyToolTipAction()
  }

  _renderToolTip() {
    const { title = 'ToolTip', type } = this.props
    const layoutType = type === SUCCESS ? styles.containerSuccess : styles.containerFailure

    return (
      <div className={`${styles.tooltipContainer} ${layoutType}`}>
        <span className={styles.title}>{title}</span>
      </div>
    )
  }

  render() {
    const { showTooltip } = this.state
    const { className, timeExit, timeIn } = ANIM_PROPS

    return (
      <CSSTransition
        in={showTooltip}
        classNames={className}
        timeout={{ enter: timeIn, exit: timeExit }}
        onEntered={() => this._animStop()}
        onExited={() => this._animFinished()}
        unmountOnExit={true}
      >
        {this._renderToolTip()}
      </CSSTransition>
    )
  }
}

export default ToolTip
