export interface IProps {
  title: string
  type?: string
  showTooltip: boolean
  emptyToolTipAction?: () => void
}

export interface IState {
  showTooltip: boolean,
  updateType: string
}
