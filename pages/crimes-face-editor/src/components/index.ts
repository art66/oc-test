import Header from './Header'
import Manage from './Manage'
import Users from './Users'
import Search from './Search'
import PopUp from './PopUp'
import Outcome from './Outcome'
import Pagination from './Pagination'

export { Header, Manage, Users, Search, PopUp, Outcome, Pagination }
