import React, { PureComponent } from 'react'
import PaginationGlobal from '@torn/shared/components/Pagination'

import { IProps } from './types'

class Pagination extends PureComponent<IProps> {
  render() {
    return <PaginationGlobal {...this.props} />
  }
}

export default Pagination
