export interface IProps {
  pageCount?: number
  currentPage?: number
  setPageNumberAction?: (ID: number) => void
  changePageNumberAction?: (type: number) => void
}
