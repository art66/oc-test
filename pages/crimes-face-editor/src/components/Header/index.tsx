import React from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const TITLE = 'Faces Editor'

class Header extends React.Component<IProps> {
  render() {
    return (
      <div className={styles.headerContainer}>
        <span className={styles.title}>{TITLE}</span>
      </div>
    )
  }
}

export default Header
