export interface IAction {
  createNewTag: (tagName: string) => void
  tagAlreadyExistAction: (status: string) => void
}

export interface ITags {
  newTagInProgress: boolean
  tags: {
    tag: string
  }[]
}

export interface IProps extends IAction, ITags {
  newTagName: string
}
