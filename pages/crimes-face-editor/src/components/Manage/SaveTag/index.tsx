import React, { Component } from 'react'

import ActionButton from '../ActionButton'
import { Search } from '../../../containers'
import { TAG_ALREADY_EXISTS } from '../../../constants'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const SEARCH_PLACEHOLDER = 'Create new tag'
const ACTION_TYPE = 'create'
const TAG_LABEL = 'Create Tag'

class SaveTag extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps) {
    const { newTagInProgress, newTagName } = this.props

    const currentNoData = newTagName.length === 0
    const nextNoData = nextProps.newTagName.length === 0
    const dataNotEqual = newTagName !== nextProps.newTagName

    const userAddSearchData = dataNotEqual && currentNoData
    const userDeleteSearchData = dataNotEqual && nextNoData

    const buttonNotInProgress = newTagInProgress !== nextProps.newTagInProgress

    if (userAddSearchData || userDeleteSearchData || buttonNotInProgress) {
      return true
    }

    return false
  }

  _isTagAlreadyExist = newTagName => {
    const { tags } = this.props

    const isSomeTagNameExist = tags.some(item => item.tag.toLowerCase() === newTagName.toLowerCase())

    return isSomeTagNameExist
  }

  _handlerClick = () => {
    const { createNewTag, newTagName, tagAlreadyExistAction } = this.props

    if (this._isTagAlreadyExist(newTagName)) {
      tagAlreadyExistAction(TAG_ALREADY_EXISTS)

      return
    }

    createNewTag(newTagName)
  }

  render() {
    const { newTagName, newTagInProgress } = this.props

    return (
      <div className={styles.saveTagContainer}>
        <Search placeholder={SEARCH_PLACEHOLDER} actionType={ACTION_TYPE} type={ACTION_TYPE} />
        <ActionButton
          tagLabel={TAG_LABEL}
          actionClick={this._handlerClick}
          tagValue={newTagName}
          inProgress={newTagInProgress}
        />
      </div>
    )
  }
}

export default SaveTag
