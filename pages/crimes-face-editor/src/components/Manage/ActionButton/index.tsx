import React, { PureComponent } from 'react'
import classnames from 'classnames'

import AnimationLoad from '@torn/shared/components/AnimationLoad'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const DOTS_COUNT = 7

class ActionButton extends PureComponent<IProps> {
  _classNameHolder = () => {
    const { tagValue, inProgress, buttonInProgress } = this.props

    const buttonClass = classnames({
      [styles.buttonAction]: true,
      [styles.buttonInactive]: !tagValue,
      [styles.buttonDisabled]: inProgress,
      [styles.deleteButtonInProgress]: buttonInProgress
    })

    return buttonClass
  }

  _handlerClick = () => {
    const { actionClick, buttonInProgress, tagValue } = this.props

    if (!tagValue || buttonInProgress) return

    actionClick()
  }

  _renderButton = () => {
    const { tagLabel, inProgress } = this.props
    const buttonClass = this._classNameHolder()

    if (inProgress) {
      return (
        <div className={buttonClass}>
          <AnimationLoad dotsCount={DOTS_COUNT} isAdaptive={true} />
        </div>
      )
    }

    return (
      <button type='button' className={buttonClass} onClick={this._handlerClick}>
        <span className={styles.text}>{tagLabel}</span>
      </button>
    )
  }

  render() {
    return <div className={styles.actionButtonContainer}>{this._renderButton()}</div>
  }
}

export default ActionButton
