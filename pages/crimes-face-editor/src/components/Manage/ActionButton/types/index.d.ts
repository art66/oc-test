export interface IProps {
  buttonInProgress?: boolean
  inProgress?: boolean
  tagLabel: string
  tagValue: string
  actionClick: () => void
}
