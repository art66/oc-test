// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'actionButtonContainer': string;
  'buttonAction': string;
  'buttonDefauls': string;
  'buttonDisabled': string;
  'buttonInactive': string;
  'deleteButtonInProgress': string;
  'globalSvgShadow': string;
  'text': string;
}
export const cssExports: CssExports;
export default cssExports;
