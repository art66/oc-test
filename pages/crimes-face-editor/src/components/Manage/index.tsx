import React, { PureComponent } from 'react'
import SaveTag from './SaveTag'
import DeleteTag from './DeleteTag'
import { Outcome } from '../../containers'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class Manage extends PureComponent<IProps> {
  _renderTopSection = () => {
    const {
      deleteTag,
      createNewTagAction,
      deleteTagAction,
      tagAlreadyExistAction,
      tagNameCreate,
      tagNameDelete,
      tooltipStatus,
      newTagInProgress,
      tags
    } = this.props

    return (
      <div className={styles.topSection}>
        <DeleteTag
          tooltipStatus={tooltipStatus}
          deleteTag={deleteTagAction}
          tagNameDelete={tagNameDelete}
          deleteTagInProgress={deleteTag}
          tags={tags}
        />
        <hr className={styles.divider} />
        <SaveTag
          newTagInProgress={newTagInProgress}
          createNewTag={createNewTagAction}
          tagAlreadyExistAction={tagAlreadyExistAction}
          newTagName={tagNameCreate}
          tags={tags}
        />
      </div>
    )
  }

  _renderBottomSection = () => {
    const { deleteTag = false } = this.props

    if (!deleteTag) {
      return null
    }

    return (
      <div className={styles.bottomSection}>
        <Outcome />
      </div>
    )
  }

  render() {
    return (
      <div className={styles.manageContainer}>
        {this._renderTopSection()}
        {this._renderBottomSection()}
      </div>
    )
  }
}

export default Manage
