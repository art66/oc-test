export interface IDeleteTag {
  tags: {
    tag: string
  }[]
  deleteTag: () => void
  deleteTagInProgress: boolean
}

export interface IProps extends IDeleteTag {
  tooltipStatus: string
  tagNameDelete: string
}
