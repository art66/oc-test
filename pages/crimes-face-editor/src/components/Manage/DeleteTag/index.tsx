import React, { PureComponent } from 'react'
import ActionButton from '../ActionButton'
import { TAG_HAS_BEEN_CREATED, TAG_HAS_BEEN_DELETED, TAG_ALREADY_EXISTS } from '../../../constants'
import { Search, ToolTip } from '../../../containers'

import getDeleteTag from '../../../utils/getDeleteTag'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const SHOW_ALL = 'All'
const SEARCH_PLACEHOLDER = 'Enter tag or trait...'
const ACTION_TYPE = 'delete'
const TAG_LABEL = 'Delete Tag'
const SUCCESS = 'success'
const FAIL = 'fail'

class DeleteTag extends PureComponent<IProps> {
  _handlerClick = () => {
    const { deleteTag } = this.props

    deleteTag()
  }

  _renderToolTip = () => {
    const { tooltipStatus } = this.props

    const isTagCreated = tooltipStatus === TAG_HAS_BEEN_CREATED
    const isTagDeleted = tooltipStatus === TAG_HAS_BEEN_DELETED
    const isTagPresent = tooltipStatus === TAG_ALREADY_EXISTS

    const showTooltip = isTagCreated || isTagDeleted || isTagPresent
    const tooltipColor = tooltipStatus === TAG_HAS_BEEN_CREATED ? SUCCESS : FAIL

    return <ToolTip title={tooltipStatus} type={tooltipColor} showTooltip={showTooltip} />
  }

  _getTagToDelete = () => {
    const { tagNameDelete, tags } = this.props

    return getDeleteTag(tags, tagNameDelete)
  }

  render() {
    const { deleteTagInProgress } = this.props
    const deleteValue = this._getTagToDelete()

    return (
      <div className={styles.deleteTagContainer}>
        <Search activeSearch={true} setValue={SHOW_ALL} placeholder={SEARCH_PLACEHOLDER} actionType={ACTION_TYPE} />
        <ActionButton
          tagLabel={TAG_LABEL}
          actionClick={this._handlerClick}
          tagValue={deleteValue}
          buttonInProgress={deleteTagInProgress}
        />
        {this._renderToolTip()}
      </div>
    )
  }
}

export default DeleteTag
