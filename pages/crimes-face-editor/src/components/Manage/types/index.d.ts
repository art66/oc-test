import { ITags } from '../SaveTag/types'

export interface IProps extends ITags {
  deleteTag?: boolean
  tooltipStatus: string
  tagNameCreate: string
  tagNameDelete: string
  createNewTagAction: (name: string) => void
  deleteTagAction: () => void
  tagAlreadyExistAction: (name: string) => void
}
