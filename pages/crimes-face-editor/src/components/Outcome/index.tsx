import React, { PureComponent, Fragment } from 'react'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const DOTS_COUNT = 7

const CONFIRM_MESSAGE = 'Are you shure you would like to remove this tag? Deleting a tag removes it from all faces.'

class Outcome extends PureComponent<IProps> {
  _handleDelete = () => {
    const { tagNameDelete, deleteTagDoneAction } = this.props

    deleteTagDoneAction(tagNameDelete)
  }

  _handleCancel = () => {
    const { deleteTagCancelAction } = this.props

    deleteTagCancelAction()
  }

  _renderButtons = () => {
    const { deleteTagInProgress } = this.props

    if (deleteTagInProgress) {
      return <AnimationLoad dotsCount={DOTS_COUNT} isAdaptive={true} />
    }

    return (
      <Fragment>
        <button className={styles.buttonDelete} onClick={this._handleDelete}>
          Delete
        </button>
        <button className={styles.buttonCancel} onClick={this._handleCancel}>
          Cancel
        </button>
      </Fragment>
    )
  }

  render() {
    return (
      <div className={styles.outcomeContainer}>
        <div className={styles.textWrap}>
          <span className={styles.text}>{CONFIRM_MESSAGE}</span>
        </div>
        <div className={styles.buttonsWrap}>{this._renderButtons()}</div>
      </div>
    )
  }
}

export default Outcome
