export interface IProps {
  deleteTagInProgress: boolean
  tagNameDelete: string
  deleteTagDoneAction?: (name: string) => void
  deleteTagCancelAction?: () => {}
}
