import { LOCATION_CHANGE } from 'react-router-redux'
import initialState from './initialState'
import onPageID from '../utils/getPageLocation'
import getPageCount from '../utils/getPageCount'
import getFilteredUsersByTag from '../utils/getFilteredUsersByTag'

import {
  ROOT_DATA_LOADED,
  SHOW_DEBUG_INFO,
  HIDE_DEBUG_INFO,
  SHOW_INFO_BOX,
  CREATE_NEW_TAG,
  NEW_TAG_CREATED,
  TAG_DELETE,
  TAG_DELETE_DONE,
  TAG_DELETE_CANCEL,
  TAG_DELETED,
  TAG_ADDED,
  USER_TAG_ADDED,
  SEARCH_TAG_CREATE,
  SEARCH_TAG_DELETE,
  SEARCH_TAG_POPUP,
  DROP_DOWN_SHOW,
  DROP_DOWN_HIDE,
  POPUP_SHOW,
  POPUP_HIDE,
  SET_USER_STAT,
  USER_STAT_SETTED,
  TAG_ALREADY_EXIST,
  EMPTY_TOOLTIP,
  SET_PAGE_ID,
  CHANGE_PAGE_NUMBER,
  TAG_HAS_BEEN_CREATED,
  TAG_HAS_BEEN_DELETED,
  FILTER_FACES
} from '../constants'

const MAX_USERS_PER_PAGE_COUNT = 120
const FIRST_PAGE = 1

const ACTION_HOLDER = {
  [ROOT_DATA_LOADED]: (state, action) => ({
    ...state,
    ...action.data,
    pageCount: getPageCount(action.data.targets.length, MAX_USERS_PER_PAGE_COUNT),
    currentPageID:
      onPageID() > getPageCount(action.data.targets.length, MAX_USERS_PER_PAGE_COUNT) ? FIRST_PAGE : onPageID()
  }),
  [SHOW_DEBUG_INFO]: (state, action) => ({
    ...state,
    common: {
      ...state.common,
      debugBox: action.debugInfo
    }
  }),
  [HIDE_DEBUG_INFO]: state => ({
    ...state,
    common: {
      ...state.common,
      debugBox: null
    }
  }),
  [SHOW_INFO_BOX]: (state, action) => ({
    ...state,
    common: {
      ...state.common,
      infoBox: action.payload
    }
  }),
  [CREATE_NEW_TAG]: state => ({
    ...state,
    createNewTagInProgress: true
  }),
  [NEW_TAG_CREATED]: (state, action) => ({
    ...state,
    tags: action.payload.tags,
    createNewTagInProgress: false,
    tooltipStatus: TAG_HAS_BEEN_CREATED,
    tagNameCreate: ''
  }),
  [TAG_DELETE]: state => ({
    ...state,
    deleteTag: true
  }),
  [TAG_DELETE_CANCEL]: state => ({
    ...state,
    deleteTag: false,
    // tagNameDelete: '',
    // tagNameFilter: '',
    // pageCount: getPageCount(state.targets.length, MAX_USERS_PER_PAGE_COUNT)
  }),
  [TAG_DELETE_DONE]: state => ({
    ...state,
    deleteTagInProgress: true
  }),
  [TAG_DELETED]: (state, action) => ({
    ...state,
    tooltipStatus: TAG_HAS_BEEN_DELETED,
    deleteTag: false,
    tagNameDelete: '',
    tagNameFilter: '',
    tags: action.payload.tags,
    deleteTagInProgress: false
  }),
  [TAG_ADDED]: (state, action) => {
    const actionTagLabel = Object.values(state.tags.find(tag => tag._id.$oid === action.ID))[1]

    const updateTags = tags => {
      if (tags.includes(actionTagLabel)) {
        return tags.filter(tag => tag !== actionTagLabel)
      }

      return [...tags, actionTagLabel]
    }

    const nextState = {
      ...state,
      targets: state.targets.map(user => (
        user._id.$oid === state.activeUserID ?
          {
            ...user,
            tags: updateTags(user.tags)
          } : user
      ))
    }

    return nextState
  },
  [USER_TAG_ADDED]: (state, action) => ({
    ...state,
    targets: action.payload.targets
  }),
  [SEARCH_TAG_CREATE]: (state, action) => ({
    ...state,
    tagNameCreate: action.tagName
  }),
  [SEARCH_TAG_DELETE]: (state, action) => ({
    ...state,
    tagNameDelete: action.tagName
  }),
  [SEARCH_TAG_POPUP]: (state, action) => ({
    ...state,
    tagNamePopup: action.tagName
  }),
  [DROP_DOWN_SHOW]: (state, action) => ({
    ...state,
    hideDropDown: action.status
  }),
  [DROP_DOWN_HIDE]: (state, action) => ({
    ...state,
    hideDropDown: action.status
  }),
  [POPUP_SHOW]: (state, action) => ({
    ...state,
    activeUserID: action.ID,
    hidePopup: false,
    hideDropDown: true
  }),
  [POPUP_HIDE]: state => ({
    ...state,
    activeUserID: null,
    hidePopup: true,
    hideDropDown: true
  }),
  [SET_USER_STAT]: (state, action) => ({
    ...state,
    targets: state.targets.map(
      user =>
        user._id.$oid === state.activeUserID
          ? {
              ...user,
              stats: {
                ...user.stats,
                [action.key]: action.value
              }
            }
          : user
    )
  }),
  [USER_STAT_SETTED]: (state, action) => ({
    ...state,
    targets: action.payload.targets
  }),
  [TAG_ALREADY_EXIST]: (state, action) => ({
    ...state,
    tooltipStatus: action.status
  }),
  [EMPTY_TOOLTIP]: state => ({
    ...state,
    tooltipStatus: ''
  }),
  [SET_PAGE_ID]: (state, action) => ({
    ...state,
    currentPageID: action.pageID
  }),
  [CHANGE_PAGE_NUMBER]: (state, action) => ({
    ...state,
    currentPageID: action.changedPageID
  }),
  [LOCATION_CHANGE]: state => ({
    ...state,
    currentPageID: onPageID() > state.pageCount && state.pageCount ? FIRST_PAGE : onPageID()
  }),
  [FILTER_FACES]: (state, action) => {
    const filteredUserList = state.targets.filter(target => getFilteredUsersByTag(target, action.tagName))

    return {
      ...state,
      tagNameFilter: action.tagName,
      pageCount: getPageCount(filteredUserList.length, MAX_USERS_PER_PAGE_COUNT)
    }
  }
}

const actionHandler = (state = initialState, action) => {
  const handler = ACTION_HOLDER[action.type]

  return handler ? handler(state, action) : state
}

export default actionHandler
