import {
  SHOW_DEBUG_INFO,
  HIDE_DEBUG_INFO,
  SHOW_INFO_BOX,
  CREATE_NEW_TAG,
  NEW_TAG_CREATED,
  TAG_DELETE,
  TAG_DELETE_DONE,
  TAG_DELETE_CANCEL,
  TAG_DELETED,
  ADD_TAG,
  TAG_ADDED,
  SEARCH_TAG,
  DROP_DOWN,
  POPUP_HIDE,
  POPUP_SHOW,
  SET_USER_STAT,
  TAG_ALREADY_EXIST,
  EMPTY_TOOLTIP,
  SET_PAGE_ID,
  CHANGE_PAGE_NUMBER,
  ROOT_DATA_LOADED,
  USER_STAT_SETTED,
  USER_TAG_ADDED,
  FILTER_FACES
} from '../constants'

export const rootDataLoaded = data => ({
  type: ROOT_DATA_LOADED,
  data
})

export const showInfoBox = payload => ({
  payload,
  type: SHOW_INFO_BOX
})

export const showDebugInfo = debugInfo => ({
  type: SHOW_DEBUG_INFO,
  debugInfo
})

export const hideDebugInfo = () => ({
  type: HIDE_DEBUG_INFO
})

export const createNewTag = tagName => ({
  type: CREATE_NEW_TAG,
  tagName
})

export const newTagCreted = payload => ({
  type: NEW_TAG_CREATED,
  payload
})

export const deleteTag = () => ({
  type: TAG_DELETE
})

export const deleteTagCancel = () => ({
  type: TAG_DELETE_CANCEL
})

export const deleteTagDone = tagName => ({
  type: TAG_DELETE_DONE,
  tagName
})

export const tagDeleted = payload => ({
  type: TAG_DELETED,
  payload
})

export const addTag = ID => ({
  type: ADD_TAG,
  ID
})

export const tadAdded = ID => ({
  type: TAG_ADDED,
  ID
})

export const tagNameSearch = (tagName, type) => ({
  type: `${SEARCH_TAG}_${type}`,
  tagName
})

export const dropDown = (status, type) => ({
  type: `${DROP_DOWN}_${type}`,
  status
})

export const popupShow = ID => ({
  type: POPUP_SHOW,
  ID
})

export const popupHide = () => ({
  type: POPUP_HIDE
})

export const setStat = (key, value) => ({
  type: SET_USER_STAT,
  key,
  value
})

export const tagAlreadyExist = status => ({
  type: TAG_ALREADY_EXIST,
  status
})

export const emptyToolTip = () => ({
  type: EMPTY_TOOLTIP
})

export const setPageNumber = pageID => ({
  type: SET_PAGE_ID,
  pageID
})

export const changePageNumber = changedPageID => ({
  type: CHANGE_PAGE_NUMBER,
  changedPageID
})

export const userStatSetted = payload => ({
  type: USER_STAT_SETTED,
  payload
})

export const userTagAdded = payload => ({
  type: USER_TAG_ADDED,
  payload
})

export const filterFaces = tagName => ({
  type: FILTER_FACES,
  tagName
})
