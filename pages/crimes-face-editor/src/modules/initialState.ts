import onPageID from '../utils/getPageLocation'

export default {
  currentPageID: onPageID(),
  createNewTagInProgress: false,
  deleteTagInProgress: false,
  tooltipStatus: '',
  tagNameDelete: '',
  tagNameCreate: '',
  tagNamePopup: '',
  tagNameFilter: '',
  pageCount: 0,
  deleteTag: false,
  hideDropDown: true,
  hidePopup: true,
  activeUserID: null,
  common: {
    debugBox: null,
    infoBox: null
  },
  users: [],
  tags: [],
  characterOptions: [
    {
      type: 'gender',
      list: ['Male', 'Female']
    },
    {
      type: 'age',
      list: ['16 - 18', '19 - 24', '25 - 34', '35 - 44', '45 - 54', '55 - 64', '65 - 74', '75+']
    },
    {
      type: 'height',
      list: ['Short', 'Medium', 'Tall']
    },
    {
      type: 'muscular',
      list: ['Skinny', 'Average', 'Athletic', 'Muscular', 'Heavyset']
    }
  ],
  settedProps: {
    gender: '',
    age: '',
    height: '',
    muscular: ''
  }
}
