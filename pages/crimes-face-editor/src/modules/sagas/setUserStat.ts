import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { userStatSetted, showDebugInfo } from '../actions'
import { ROOT_URL } from '../../constants'
import getFilteredUser from '../../utils/getFilteredUser'
import throwResponseError from '../../utils/throwResponseError'

const STEP = 'addStatsToTarget'

function* setUserStat({ key, value }: any) {
  try {
    const getState = state => state
    const {
      app: { targets, activeUserID }
    } = yield select(getState)

    const userToSet = getFilteredUser(targets, activeUserID)
    const {
      _id: { $oid: userID }
    } = userToSet

    const json = yield fetchUrl(`${ROOT_URL}${STEP}&${key}=${encodeURIComponent(value)}&targetID=${userID}`, null, false)

    throwResponseError(json)

    yield put(userStatSetted(json))
  } catch (error) {
    yield put(
      showDebugInfo({ msg: `${error.toString()}. Oops! Target's stat was not setted! Please, reload the page!` })
    )
  }
}

export default setUserStat
