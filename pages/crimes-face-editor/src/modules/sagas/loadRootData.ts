import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { rootDataLoaded, showDebugInfo } from '../actions'
import { ROOT_URL } from '../../constants'
import throwResponseError from '../../utils/throwResponseError'

const STEP = 'data'

function* loadRootData() {
  try {
    const json = yield fetchUrl(`${ROOT_URL}${STEP}`, null, false)

    throwResponseError(json)

    yield put(rootDataLoaded(json))
  } catch (error) {
    yield put(showDebugInfo({ msg: `${error.toString()} Oops! InitialData was not loaded. Please, restart the page!` }))
  }
}

export default loadRootData
