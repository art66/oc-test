import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { tadAdded, userTagAdded, showDebugInfo } from '../actions'
import { ROOT_URL } from '../../constants'
import getFilteredUser from '../../utils/getFilteredUser'
import throwResponseError from '../../utils/throwResponseError'

const STEP_ADD = 'addTagToTarget'
const STEP_REMOVE = 'removeTagFromTarget'

function* manageUserTag({ ID }: any) {
  try {
    const getState = state => state
    const {
      app: { targets, tags, activeUserID }
    } = yield select(getState)

    const userToSet = getFilteredUser(targets, activeUserID)
    const tagToSet = tags.find(tag => tag._id.$oid === ID)

    // console.log(tagToSet, 'tagToSet')
    const {
      tags: userTags,
      _id: { $oid: userID }
    } = userToSet

    const {
      tag: tagToSetLabel,
      _id: { $oid: tagID }
    } = tagToSet

    yield put(tadAdded(ID))

    const removeTagURL = `${ROOT_URL}${STEP_REMOVE}&tagID=${tagID}&targetID=${userID}`
    const addTagURL = `${ROOT_URL}${STEP_ADD}&tagID=${tagID}&targetID=${userID}`
    const urlToFetch = userTags.includes(tagToSetLabel) ? removeTagURL : addTagURL

    const json = yield fetchUrl(urlToFetch, null, false)

    throwResponseError(json)

    yield put(userTagAdded(json))
  } catch (error) {
    yield put(showDebugInfo({ msg: `${error.toString()}. Oops! Tag was not processed. Please, restart the page!` }))
  }
}

export default manageUserTag
