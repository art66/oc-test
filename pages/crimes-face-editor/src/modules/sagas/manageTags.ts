import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { newTagCreted, tagDeleted, showDebugInfo } from '../actions'
import { ROOT_URL, CREATE_NEW_TAG, TAG_DELETE_DONE } from '../../constants'
import throwResponseError from '../../utils/throwResponseError'

const SAGA_ACTION_HOLDER = {
  [CREATE_NEW_TAG]: {
    fetchLabel: 'addTag',
    step: 'tag',
    tagToManage: tagName => tagName,
    sagaAction: json => newTagCreted(json)
  },
  [TAG_DELETE_DONE]: {
    fetchLabel: 'removeTag',
    step: 'tagID',
    tagToManage: deleteTagID => deleteTagID._id.$oid,
    sagaAction: json => tagDeleted(json)
  }
}

function* manageTags({ type, tagName }: any) {
  try {
    const { fetchLabel, tagToManage, step, sagaAction } = SAGA_ACTION_HOLDER[type]

    const getState = state => state
    const {
      app: { tags }
    } = yield select(getState)

    const deleteTagID = tags.find(tag => tag.tag === tagName)
    const tagStage = fetchLabel === 'removeTag' ? tagToManage(deleteTagID) : tagToManage(tagName)

    const json = yield fetchUrl(`${ROOT_URL}${fetchLabel}&${step}=${tagStage}`, null, false)

    throwResponseError(json)

    yield put(sagaAction(json))
  } catch (error) {
    yield put(showDebugInfo({ msg: `${error.toString()} Oops! New Tag was not Created. Please, restart the page!` }))
  }
}

export default manageTags
