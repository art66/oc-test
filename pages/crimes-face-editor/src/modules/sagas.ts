import { takeLatest } from 'redux-saga/effects'
import loadRootData from './sagas/loadRootData'
import setUserStat from './sagas/setUserStat'
import manageUserTag from './sagas/manageUserTag'
import manageTags from './sagas/manageTags'

import { LOAD_ROOT_DATA, SET_USER_STAT, ADD_TAG, CREATE_NEW_TAG, TAG_DELETE_DONE } from '../constants'

export default function* watchData() {
  yield takeLatest(LOAD_ROOT_DATA, loadRootData)
  yield takeLatest(SET_USER_STAT, setUserStat)
  yield takeLatest(ADD_TAG, manageUserTag)
  yield takeLatest(TAG_DELETE_DONE, manageTags)
  yield takeLatest(CREATE_NEW_TAG, manageTags)
}
