const throwResponseError = json => {
  if (json && json.error) {
    throw new Error(json.DB.error)
  }
}

export default throwResponseError
