import { USERS_PER_PAGE, PAGE_ID_FIXER } from '../constants'

const getFilterUsersByPage = (targetIndex, pageIndex) => {
  const currentPage = pageIndex - PAGE_ID_FIXER
  const bottomFilterBorder = USERS_PER_PAGE * currentPage
  const topFilterBorder = USERS_PER_PAGE * currentPage + USERS_PER_PAGE

  const getUserByPage = targetIndex >= bottomFilterBorder && targetIndex < topFilterBorder

  return getUserByPage
}

export default getFilterUsersByPage
