
const getFilteredUserByTag = (user, tagName) => {
  if (!user.tags || !tagName) {
    return true
  }

  const tagsMatch = () => {
    return user.tags.some((tag: string) => tag.toLowerCase() === tagName.toLowerCase())
  }

  const charactersMatch = () => {
    return Object.values(user.stats).some((value: string) => value.toLowerCase() === tagName.toLowerCase())
  }

  return tagsMatch() || charactersMatch()
}

export default getFilteredUserByTag
