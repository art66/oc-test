interface IUser {
  stats?: object
  tags?: string[]
  _id: {
    $oid: string
  }
}

const getFilteredUser = (targets: IUser[], activeUserID: string) => {
  const user = targets.find(user => user._id.$oid === activeUserID)

  return user
}

export default getFilteredUser
