const onPageID = () => Number(window.location.hash.match(/\d*$/)[0]) || 1

export default onPageID
