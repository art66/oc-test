const getDeleteTag = (tags, tagName) => {
  if (tags && tagName && tags.some(tag => tag.tag === tagName)) {
    return tagName
  }

  return ''
}

export default getDeleteTag
