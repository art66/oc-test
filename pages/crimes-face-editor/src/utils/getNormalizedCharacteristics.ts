const getNormalizedCharacteristics = charactersList => {
  const recurringList = []

  charactersList.forEach(({ type, list }) => {
    list.forEach(character => {
      recurringList.push({
        _id: {
          $oid: `${type}_${character}`
        },
        tag: character,
        type
      })
    })
  })

  return recurringList
}

export default getNormalizedCharacteristics
