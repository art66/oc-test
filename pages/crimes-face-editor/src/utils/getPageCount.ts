const getPageCount = (targetsLength: number, usersCount: number) =>
  Number(Math.ceil(targetsLength / usersCount).toFixed(0))

export default getPageCount
