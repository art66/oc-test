const TARGETS_LIST = Array.from(Array(1478).keys())

const deleteOldCache = (caches, cache) => Promise.all(cache.map(cacheName => caches.delete(cacheName)))
const imagesListToLoad = () => TARGETS_LIST.map(imageID => (
  `/images/v2/2sef32sdfr422svbfr2/crimes/targets_faces/${imageID + 1}.png`
))

self.addEventListener('install', event => {
  self.skipWaiting()

  event.waitUntil(caches.keys().then(cacheNames => deleteOldCache(caches, cacheNames)))
})

self.addEventListener('activate', event => {
  event.waitUntil(caches.open('crime-faces-images').then(cache => cache.addAll(imagesListToLoad())))
})

self.addEventListener('fetch', event => {
  const { request = {} } = event

  // some strange exceptions...
  const googleAllow = /(analytics)/.test(event.request.url)
  const bingAllow = /(bing)/.test(event.request.url)
  const doubleclickAllow = /(doubleclick)/.test(event.request.url)

  const isNotAnAmmoImage = request.destination !== 'image' || !/(images\/v2\/crimes\/targets_faces)/i.test(request.url)

  if (isNotAnAmmoImage || googleAllow || bingAllow || doubleclickAllow) {
    return
  }

  // load real data if cache is missed
  const loadRealImagesData = () => {
    return fetch(event.request).then(response => {
      return caches.open('crime-faces-images').then(cache => {
        cache.put(event.request, response.clone())

        return response
      })
    })
  }

  event.respondWith(
    caches.match(event.request).then(resp => {
      return resp || loadRealImagesData()
    })
  )
})
