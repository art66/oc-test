import { connect } from 'react-redux'
import { Manage } from '../../components'
import { createNewTag, deleteTag, tagAlreadyExist } from '../../modules/actions'

const mapStateToProps = ({ app }) => ({
  newTagInProgress: app.createNewTagInProgress,
  deleteTag: app.deleteTag,
  tooltipStatus: app.tooltipStatus,
  tagNameCreate: app.tagNameCreate,
  tagNameDelete: app.tagNameDelete,
  tags: app.tags
})

const mapDispatchToProps = dispatch => ({
  createNewTagAction: name => dispatch(createNewTag(name)),
  deleteTagAction: () => dispatch(deleteTag()),
  tagAlreadyExistAction: status => dispatch(tagAlreadyExist(status))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Manage)
