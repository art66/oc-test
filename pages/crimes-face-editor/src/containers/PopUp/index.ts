import { connect } from 'react-redux'
import { PopUp } from '../../components'
import { setStat } from '../../modules/actions'

const mapStateToProps = ({ app }) => ({
  characterOptions: app.characterOptions,
  activeUserID: app.activeUserID,
  targets: app.targets
})

const mapDispatchToProps = dispatch => ({
  setStatAction: (option, ID) => dispatch(setStat(option, ID))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PopUp)
