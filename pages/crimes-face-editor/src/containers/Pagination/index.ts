import { connect } from 'react-redux'
import { Pagination } from '../../components'
import { setPageNumber, changePageNumber } from '../../modules/actions'

const mapStateToprops = ({ app }) => ({
  pageCount: app.pageCount,
  currentPage: app.currentPageID
})

const mapDispatchToProps = dispatch => ({
  setPageNumberAction: ID => dispatch(setPageNumber(ID)),
  changePageNumberAction: nextPageID => dispatch(changePageNumber(nextPageID))
})

export default connect(
  mapStateToprops,
  mapDispatchToProps
)(Pagination)
