import React, { PureComponent } from 'react'
import { Provider } from 'react-redux'
import { HashRouter, Route } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'

import AppLayout from '../../layout'
import { IProps } from './types'

class AppContainer extends PureComponent<IProps> {
  render() {
    const { store, history } = this.props

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <HashRouter>
            <Route exact={true} path='/' component={AppLayout} />
          </HashRouter>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default AppContainer
