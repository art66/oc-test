import { connect } from 'react-redux'
import { Outcome } from '../../components'
import { deleteTagCancel, deleteTagDone } from '../../modules/actions'

const mapStateToprops = ({ app }) => ({
  deleteTagInProgress: app.deleteTagInProgress,
  tagNameDelete: app.tagNameDelete
})

const mapDispatchToProps = dispatch => ({
  deleteTagDoneAction: name => dispatch(deleteTagDone(name)),
  deleteTagCancelAction: () => dispatch(deleteTagCancel())
})

export default connect(
  mapStateToprops,
  mapDispatchToProps
)(Outcome)
