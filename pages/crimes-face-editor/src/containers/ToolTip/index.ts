import { connect } from 'react-redux'
import ToolTip from '../../components/Tooltip'
import { emptyToolTip } from '../../modules/actions'

const mapDispatchToProps = dispatch => ({
  emptyToolTipAction: () => dispatch(emptyToolTip())
})

export default connect(
  null,
  mapDispatchToProps
)(ToolTip)
