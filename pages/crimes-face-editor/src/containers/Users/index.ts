import { connect } from 'react-redux'
import { Users } from '../../components'
import { popupShow, popupHide } from '../../modules/actions'

const mapStateToProps = ({ app }) => ({
  hidePopup: app.hidePopup,
  activeUserID: app.activeUserID,
  tagNameFilter: app.tagNameFilter,
  // users: app.users,
  targets: app.targets,
  currentPageID: app.currentPageID
})

const mapDispatchToState = dispatch => ({
  popupShowAction: ID => dispatch(popupShow(ID)),
  popupHideAction: () => dispatch(popupHide())
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(Users)
