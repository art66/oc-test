import { connect } from 'react-redux'
import { Search } from '../../components'
import { tagNameSearch, filterFaces, dropDown, addTag } from '../../modules/actions'

const mapStateToProps = ({ app }) => ({
  targets: app.targets,
  tags: app.tags,
  activeUserID: app.activeUserID,
  hideDropDown: app.hideDropDown,
  tagnamedelete: app.tagNameDelete,
  tagnamecreate: app.tagNameCreate,
  tagnamepopup: app.tagNamePopup,
  tagnamefilter: app.tagNameFilter,
  characterOptions: app.characterOptions
})

const mapDispatchToProps = dispatch => ({
  tagNameSearchAction: (tagName, type) => dispatch(tagNameSearch(tagName, type)),
  dropDownAction: (status, type) => dispatch(dropDown(status, type)),
  filterFacesByTag: tagName => dispatch(filterFaces(tagName)),
  tagAddedAction: ID => dispatch(addTag(ID))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)
