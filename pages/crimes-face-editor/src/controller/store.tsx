import { createStore, applyMiddleware, compose, Store } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import { routerMiddleware } from 'connected-react-router'

import saga, { rootSaga, runSaga } from './middleware/saga'
import reduxLogger from './middleware/reduxLogger'
import makeRootReducer from './middleware/rootReducer'
import activateStoreHMR from './middleware/storeHMR'
import fetchInterceptor from './middleware/fetchInterceptor'
import history from './history'

interface IStore extends Store<any> {
  runSaga?: any
  asyncReducers?: any
}

const rootStore = () => {
  // ======================================================
  // Middleware & Enhancers Configuration
  // ======================================================
  const middleware = [routerMiddleware(history), saga, fetchInterceptor]
  const enhancers = []

  if (__DEV__) {
    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
    }

    middleware.push(reduxLogger)
  }

  // ======================================================
  // Store Instantiation
  // ======================================================
  const store: IStore = createStore(
    makeRootReducer({}),
    compose(
      responsiveStoreEnhancer,
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  // ======================================================
  // Sagas and Reducer Replacing Store Injection
  // ======================================================
  store.runSaga = runSaga
  runSaga(rootSaga)

  activateStoreHMR(store)

  return store
}

export default rootStore()
