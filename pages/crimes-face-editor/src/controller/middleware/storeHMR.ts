import makeRootReducer from './rootReducer'
import { Store } from 'redux'

const activateStoreHMR = (store: Store<any> & any) => {
  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }
}

export default activateStoreHMR
