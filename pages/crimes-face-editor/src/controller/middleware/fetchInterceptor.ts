import { showDebugInfo, showInfoBox } from '../../modules/actions'

const fetchInterceptor = store => next => action => {
  // if (!window.location.href.match(/page/gi)) {
  //   location.href = `${location.protocol}//${location.hostname}/?page=1`
  // }
  if (action.meta === 'ajax' && action.json) {
    if (action.json.DB.debugInfo) {
      const stringInfo = JSON.stringify(action.json.DB.debugInfo, null, '\t')

      store.dispatch(showDebugInfo(`DebugInfo:\n\n${stringInfo}`))
    } else if (action.json.DB.redirect !== undefined) {
      if (action.json.DB.redirect === true) {
        location.href = `${location.protocol}//${location.hostname}/${action.json.url}`
      } else if (action.json.DB.redirect === false) {
        store.dispatch(showInfoBox({ msg: action.json.content, color: 'red' }))
      }
    }
  }

  return next(action)
}

export default fetchInterceptor
