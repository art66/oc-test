import { Store, combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import { connectRouter } from 'connected-react-router'
import app from '../../modules/reducers'
import history from '../history'

interface IStore {
  asyncReducers?: any
}

interface IProps {
  key: string
  reducer: any
}

const makeRootReducer = (asyncReducers?: object) => combineReducers({
  ...asyncReducers,
  app,
  router: connectRouter(history),
  browser: createResponsiveStateReducer({
    mobile: 600,
    tablet: 1000,
    desktop: 5000
  })
})

export const injectReducer = (store: IStore & Store, { key, reducer }: IProps) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return
  store.asyncReducers[key] = reducer

  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
