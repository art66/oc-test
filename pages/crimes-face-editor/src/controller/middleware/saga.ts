import createSagaMiddleware from 'redux-saga'
import { all } from 'redux-saga/effects'
import createSagaMiddlewareHelpers from 'redux-saga-watch-actions/lib/middleware'
import watchData from '../../modules/sagas'

const sagaMiddleware = createSagaMiddleware()
const runSaga = saga => sagaMiddleware.run(saga)

const { injectSaga, cancelTask } = createSagaMiddlewareHelpers(sagaMiddleware) // <-- bind to sagaMiddleware.run

export function* rootSaga() {
  yield all([watchData()])
}

export { cancelTask, injectSaga, runSaga }
export default sagaMiddleware
