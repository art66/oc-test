# Crimes Face Editor - Torn


## 3.2.1
 * Fixed traits and tags search.

## 3.2.0
 * Added filtration logic by tags and characters.

## 3.1.0
 * Added input tag string checker regexp.

## 3.0.0
 * Added users filtering functionality.
 * Improved several utils for reusable usage.

## 2.9.1
 * Fixed state update after react v16.8.6 upgrade.

## 2.9.0
 * Updated all modules up date.
 * Temporary disable app SW.

## 2.8.0
 * Added PopUp hidding by Esc button click.
 * PopUp layout moved from right to left position around the user icon.

## 2.7.0
 * Fix for FaceID layout.
 * Improved User border highlighting.

## 2.6.0
 * Added FaceID layout for PopUp Component.

## 2.5.1
 * SaveTag Tooltip layout has benn moved to the DeleteTag Component.
 * Fixed Scroll Bar layout in DropDown Component.

## 2.3.1
 * Fixed Users Component rerenders.

## 2.3.0
 * Added ServiceWorker inside App for image async fetching.

 ## 2.2.1
 * Removed magic numbers and magic strings.
 * Minor improvements.
 * Fixed bugs with tag adding in search mode of PopUp Component.

## 2.1.1
 * Added DeleteTag Global Action.
 * Improved Sagas structure.

## 2.0.0
 * Added all Sagas actions for async requests on back-end (delete, create, add, remove).
 * Added animation for fetch request once its button is clicked.

## 1.1.0
 * Added Sagas Async request for initial data load.
 * Replaced mock data with real back-end one.

## 1.0.1
 * First stabel client-based App version.
 * Added full Pagination functional.
 * Added independent users tags/currenPtops props storage.
 * Added sixes scroll bar with layout logic.
 * Improved stability.
 * Fixed some legacy bugs.

## 0.13.0
 * Added navigation functional to Pagination.

## 0.12.0
 * Added Pagination Component.

## 0.11.0
 * Added Outcome Component. Connected to the Redux store.

## 0.10.0
 * ToolTip is connected to the Redux store.

## 0.9.0
 * Added ToolTip Component.

## 0.8.1
 * Added Select User Options Environment via Redux handler.
 * Minor bug fixes.

## 0.7.1
 * Added Create, Delete Tags environment.
 * Added disabled layout state for Action buttons.
 * Minor bug fixes.

## 0.6.0
 * Added bussiness logic for manipulationg Search inputs and PopUp Components layout.

## 0.5.0
 * Components has been connected to th Redux store.

## 0.4.0
 * Created Users, Columns, Tabs and Popup Components.

## 0.3.0
 * Created Search, Manage and Button Components.

## 0.2.0
 * Added Layout and Pagination in App Structure.

## 0.1.0
 * Created App Controller Configuration.
 * Created Basic App Structure.

## 0.0.1
 * Folder has been created.
