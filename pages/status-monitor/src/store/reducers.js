import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import statusMonitor from '../modules'

const browser = createResponsiveStateReducer({
  mobile: 600,
  tablet: 1000,
  desktop: 5000
})

export const makeRootReducer = asyncReducers => {
  return combineReducers({
    browser,
    statusMonitor,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
