import { connect } from 'react-redux'
import StatusMonitor from '../components/StatusMonitor'
import { loadAppData, clockTick } from '../modules'

const mapStateToProps = state => ({
  browser: state.browser,
  statusMonitor: state.statusMonitor
})

const mapDispatchToProps = { loadAppData, clockTick }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusMonitor)
