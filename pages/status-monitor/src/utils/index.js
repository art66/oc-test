export const formatTimeNumber = val => (val.toString().length < 2 ? '0' + val : val)
