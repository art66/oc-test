import React, { Component } from 'react'
import cn from 'classnames'
import Time from './Time'
import UsersOnline from './UsersOnline'
import Stats from './Stats'
import UpdateFailedPopup from './UpdateFailedPopup'
import Chart from './Chart'
import Backups from './Backups'
import s from '../styles/StatusMonitor.cssmodule.scss'

const UPDATE_INTERVAL = 15000

interface IProps {
  statusMonitor: {
    updatedSecondsAgo: number
    dataFetched: boolean
    updateFailed: number
    backups: {
      name: string
      date: string
      alert: number
    }[]
    graphs: {
      loadtime: {
        alert: string
        php1: {}
        php2: {}
        php3: {}
        pod1: {}
      }
      slowqueries: {
        alert: string
        db1: {}
        db2: {}
        db3: {}
        db4: {}
      }
    }
    statistics: {
      name: string
      minute: number
      hour: string
      day: string
      alert: string
    }[]
    time: string
    usersonline: {
      day: {
        value: number
        alert: string
      }
      hour: {
        value: number
        alert: string
      }
      now: {
        value: number
        alert: string
      }
    }
  }
  loadAppData: () => {}
  clockTick: () => {}
}

class StatusMonitor extends Component<IProps> {
  pollInterval: any

  clockInterval: any

  alert = false

  alertAudio = null

  componentDidMount() {
    const { loadAppData } = this.props

    loadAppData()
    this.startPollingData()
    this.startClock()
  }

  componentDidUpdate() {
    this.checkSoundAlert()
  }

  componentWillUnmount() {
    clearInterval(this.pollInterval)
    clearInterval(this.clockInterval)
  }

  startPollingData() {
    const { loadAppData } = this.props

    this.pollInterval = setInterval(() => {
      loadAppData()
    }, UPDATE_INTERVAL)
  }

  startClock() {
    const { clockTick } = this.props

    this.clockInterval = setInterval(() => {
      clockTick()
    }, 1000)
  }

  checkUsersOnlineAlert = period => {
    const { statusMonitor } = this.props

    return parseInt(statusMonitor.usersonline[period].alert, 10) === 1
  }

  checkSoundAlert = () => {
    const { statusMonitor } = this.props
    const { usersonline, graphs, statistics } = statusMonitor

    if (statusMonitor.updatedSecondsAgo !== 0) {
      return
    }

    let alert = false

    if (
      parseInt(usersonline.now.alert, 10)
      || parseInt(usersonline.hour.alert, 10)
      || parseInt(usersonline.day.alert, 10)
      || statistics.some(item => parseInt(item.alert, 10))
      || parseInt(graphs.loadtime.alert, 10)
      || parseInt(graphs.slowqueries.alert, 10)
    ) {
      alert = true
    }

    if (alert) {
      if (!this.alert) {
        this.alert = true

        try {
          const playPromise = this.alertAudio.play()

          if (playPromise !== undefined) {
            playPromise
              .then(() => {})
              .catch(error => {
                console.error(error)
              })
          }
        } catch (e) {
          console.error(e)
        }
      }
    } else {
      this.alert = false
    }
  }

  render() {
    const { statusMonitor } = this.props
    const { usersonline, statistics, graphs, time, updatedSecondsAgo, updateFailed, backups } = statusMonitor

    if (!statusMonitor.dataFetched) {
      return <div>Loading...</div>
    }

    const usersOnlineNowAlert = this.checkUsersOnlineAlert('now')
    const usersOnlineHourAlert = this.checkUsersOnlineAlert('hour')
    const usersOnlineDayAlert = this.checkUsersOnlineAlert('day')

    return (
      <div className={s.statusMonitor}>
        <div className={`${s.statusMonitorWrapper}`}>
          <div className={s.usersOnlineWrapper}>
            <div className={cn(s.left, s.now, { [s.alertBlock]: usersOnlineNowAlert })} />
            <div className={cn(s.left, s.hour, { [s.alertBlock]: usersOnlineHourAlert })} />
            <div className={cn(s.left, s.day, { [s.alertBlock]: usersOnlineDayAlert })} />
            <div className={s.usersOnline}>
              <Time time={time} updatedSecondsAgo={updatedSecondsAgo} />
              <UsersOnline usersonline={usersonline} updatedSecondsAgo={updatedSecondsAgo} />
            </div>
            <div className={cn(s.right, s.now, { [s.alertBlock]: usersOnlineNowAlert })} />
            <div className={cn(s.right, s.hour, { [s.alertBlock]: usersOnlineHourAlert })} />
            <div className={cn(s.right, s.day, { [s.alertBlock]: usersOnlineDayAlert })} />
          </div>
          <Stats statistics={statistics} updatedSecondsAgo={updatedSecondsAgo} />
          <Chart name='Page Load Time' servers={graphs.loadtime} maxValue={2} updatedSecondsAgo={updatedSecondsAgo} />
          <Chart
            name='Slow Queries'
            servers={graphs.slowqueries}
            maxValue={100}
            updatedSecondsAgo={updatedSecondsAgo}
          />
          {backups.length !== 0 && <Backups backups={backups} updatedSecondsAgo={updatedSecondsAgo} />}
        </div>
        {updateFailed && <UpdateFailedPopup />}
        <audio ref={el => (this.alertAudio = el)} src='/status_monitor/audio/status_monitor_alert.mp3' />
      </div>
    )
  }
}

export default StatusMonitor
