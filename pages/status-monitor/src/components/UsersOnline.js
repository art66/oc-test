import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import s from '../styles/UsersOnline.cssmodule.scss'

class Stats extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.updatedSecondsAgo === 0
  }

  render() {
    const { usersonline } = this.props

    return (
      <div className={s.usersStatisticWrapper}>
        <h2 className={s.title}>
          <div className={s.arrows} />
          Users online
        </h2>
        <ul className={s.usersStatistic}>
          <li className={cn({ [s.alert]: parseInt(usersonline.now.alert) })}>
            <div className={s.amount}>{usersonline.now.value}</div>
            <div className={s.interval}>NOW</div>
          </li>
          <li className={cn({ [s.alert]: parseInt(usersonline.hour.alert) })}>
            <div className={s.amount}>{usersonline.hour.value}</div>
            <div className={s.interval}>HOUR</div>
          </li>
          <li className={cn({ [s.alert]: parseInt(usersonline.day.alert) })}>
            <div className={s.amount}>{usersonline.day.value}</div>
            <div className={s.interval}>DAY</div>
          </li>
        </ul>
      </div>
    )
  }
}

Stats.propTypes = {
  usersonline: PropTypes.object,
  updatedSecondsAgo: PropTypes.number
}

export default Stats
