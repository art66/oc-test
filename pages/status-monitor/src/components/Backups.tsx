import React, { Component } from 'react'
import cn from 'classnames'
import colors from '../constants'
import s from '../styles/Backups.cssmodule.scss'

interface IProps {
  backups: {
    name: string,
    date: string,
    alert: number
  }[],
  updatedSecondsAgo: number
}

class Backups extends Component<IProps> {
  shouldComponentUpdate(nextProps) {
    return nextProps.updatedSecondsAgo === 0
  }

  getList = () => {
    const { backups } = this.props

    return backups.map(item => (
      <li key={item.name} className={item.alert ? s.itemAlert : ''}>
        <div className={s.square} style={{ background: colors[item.name] }} />
        <div>{item.name}</div>
        <div>{item.date}</div>
      </li>
    ))
  }

  checkAlert = () => {
    const { backups } = this.props

    return backups.some(item => item.alert)
  }

  render() {
    const list = this.getList()
    const alert = this.checkAlert()

    return (
      <div className={`${s.blockWrapper} ${s.backupsBlock}`}>
        <div className={cn(s.left, { [s.alertBlock]: alert })} />
        <div className={s.backupsWrapper}>
          <h2 className={s.title}>
            <div className={s.arrows} />
            Backups
          </h2>
          <div className={cn(s.backups, { [s.alert]: alert })}>
            <div className={`${s.square} ${s.left} ${s.top}`} />
            <div className={`${s.square} ${s.right} ${s.top}`} />
            <div className={`${s.square} ${s.right} ${s.bottom}`} />
            <div className={`${s.square} ${s.left} ${s.bottom}`} />
            <div className={s.backupsListWrapper}>
              <ul className={s.backupsList}>
                { list }
              </ul>
            </div>
          </div>
        </div>
        <div className={cn(s.right, { [s.alertBlock]: alert })} />
      </div>
    )
  }
}

export default Backups
