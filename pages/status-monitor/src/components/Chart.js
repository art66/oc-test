import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { GoogleCharts } from 'google-charts'
import cn from 'classnames'
import s from '../styles/Chart.cssmodule.scss'

class Chart extends Component {
  static defaultProps = {
    chartOptions: {},
    maxValue: 2
  }

  constructor(props) {
    super(props)
    const { maxValue } = this.props

    this.chartPlaceholder = React.createRef()
    this.chartInstance = null
    this.chartLoaded = false
    this.defaultChartOptions = {
      hAxis: {
        textStyle: { color: '#aaa' },
        format: 'HH:mm'
      },
      vAxis: {
        textStyle: { color: '#aaa' },
        viewWindow: {
          max: maxValue,
          min: 0
        }
      },
      colors: ['#c9d5d7', '#a52714', '#11b0e2', '#fb7f72', '#45d0cb', '#7d847b'],
      style: {
        color: '#000000'
      },
      crosshair: {
        color: '#000',
        trigger: 'selection'
      },
      backgroundColor: 'transparent',
      chartArea: {
        left: 58,
        top: 30,
        width: '88%',
        height: 133
      },
      legend: {
        position: 'bottom',
        textStyle: {
          color: '#aaa'
        }
      }
    }
  }

  componentDidMount() {
    this.loadChartAndDraw()
    window.addEventListener('resize', this.drawChart)
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.updatedSecondsAgo === 0
  }

  componentDidUpdate() {
    const { updatedSecondsAgo } = this.props

    if (updatedSecondsAgo === 0) {
      this.loadChartAndDraw()
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.drawChart)
  }

  loadChartAndDraw = () => {
    try {
      if (this.chartLoaded) {
        this.drawChart()
      } else {
        this.chartLoaded = true
        GoogleCharts.load(this.drawChart)
      }
    } catch (e) {
      console.error(e)
    }
  }

  // eslint-disable-next-line max-statements
  drawChart = () => {
    const { servers } = this.props
    const data = new GoogleCharts.api.visualization.DataTable()
    const requiredValuesInRow = Object.keys(servers).length

    let rows = []

    data.addColumn('datetime', 'time')

    Object.keys(servers).forEach((serverName, serverIndex) => {
      if (serverName === 'alert') {
        return
      }

      const server = servers[serverName]

      data.addColumn('number', serverName)
      Object.keys(server)
        .reverse()
        .forEach((recordTimestamp, recordIndex) => {
          const value = parseFloat(server[recordTimestamp])

          if (!rows[recordIndex]) {
            rows[recordIndex] = []
          }
          if (serverIndex === 0) {
            rows[recordIndex][0] = new Date(recordTimestamp * 1000)
          }
          rows[recordIndex][serverIndex + 1] = value
        })
    })

    rows = rows.reverse()

    if (rows.some(row => row.length < requiredValuesInRow)) {
      rows.forEach((row, index) => {
        if (row.length < requiredValuesInRow) {
          const lack = requiredValuesInRow - row.length

          rows[index] = rows[index].concat(new Array(lack).fill(0))
        }
      })
    }

    data.addRows(rows)

    const { defaultChartOptions } = this
    const { chartOptions } = this.props
    const options = {
      ...defaultChartOptions,
      ...chartOptions
    }

    const formatter = new GoogleCharts.api.visualization.DateFormat({ pattern: 'HH:mm', timeZone: 0 })

    formatter.format(data, 0)

    const xTicks = []

    for (let i = 0; i < data.getNumberOfRows(); i++) {
      xTicks.push({
        v: data.getValue(i, 0),
        f: data.getFormattedValue(i, 0)
      })
    }

    if (this.chartInstance) {
      this.chartInstance.clearChart()
    } else {
      this.chartInstance = new GoogleCharts.api.visualization.LineChart(this.chartPlaceholder.current)
    }
    this.chartInstance.draw(data, {
      ...options,
      hAxis: {
        ...this.defaultChartOptions.hAxis,
        ticks: xTicks.filter(({ f }) => f.includes(':00')).map((item) => ({ ...item, f: item.f.replace(':00', '')}))
      }
    })
  }

  checkAlert = () => {
    const { servers } = this.props

    return parseInt(servers['alert'], 10) === 1
  }

  render() {
    const { name } = this.props
    const alert = this.checkAlert()

    return (
      <div className={s.blockWrapper}>
        <div className={cn(s.left, { [s.alertBlock]: alert })} />
        <div className={s.graphStatsWrapper}>
          <h2 className={s.title}>
            <div className={s.arrows} />
            {name}
          </h2>
          <div className={cn(s.graphStats, { [s.alert]: alert })}>
            <div className={s.chart} ref={this.chartPlaceholder} />
            <div className={`${s.square} ${s.left} ${s.top}`} />
            <div className={`${s.square} ${s.right} ${s.top}`} />
            <div className={`${s.square} ${s.right} ${s.bottom}`} />
            <div className={`${s.square} ${s.left} ${s.bottom}`} />
            <ul className={s.graphicWrapper}>
              <li />
              <li />
              <li />
              <li />
            </ul>
          </div>
          <ul className={s.rightRectangles}>
            <li />
            <li />
            <li />
          </ul>
        </div>
        <div className={cn(s.right, { [s.alertBlock]: alert })} />
      </div>
    )
  }
}

Chart.propTypes = {
  name: PropTypes.string,
  servers: PropTypes.object,
  chartOptions: PropTypes.object,
  updatedSecondsAgo: PropTypes.number,
  maxValue: PropTypes.number
}

export default Chart
