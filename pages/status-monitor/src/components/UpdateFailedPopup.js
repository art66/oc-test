import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/UpdateFailedPopup.cssmodule.scss'

class UpdateFailedPopup extends Component {
  render() {
    return (
      <div className={s.updateFailed}>
        <div className={s.overlay} />
        <div className={s.popup}>
          <div className={`${s.corner} ${s.left} ${s.top}`} />
          <div className={`${s.corner} ${s.right} ${s.top}`} />
          <div className={`${s.corner} ${s.right} ${s.bottom}`} />
          <div className={`${s.corner} ${s.left} ${s.bottom}`} />
          <div className={s.icon} />
          <h1 className={s.message}>UPDATE FAILED!</h1>
        </div>
      </div>
    )
  }
}

UpdateFailedPopup.propTypes = {
  updateFailed: PropTypes.bool
}

export default UpdateFailedPopup
