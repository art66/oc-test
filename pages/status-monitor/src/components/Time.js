import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { formatTimeNumber } from '../utils'
import s from '../styles/Time.cssmodule.scss'

class Stats extends Component {
  render() {
    const { time, updatedSecondsAgo } = this.props
    const date = new Date(time * 1000)
    const seconds = formatTimeNumber(date.getUTCSeconds())
    const minutes = formatTimeNumber(date.getUTCMinutes())
    const hours = formatTimeNumber(date.getUTCHours())
    const day = formatTimeNumber(date.getUTCDate())
    const month = formatTimeNumber(date.getUTCMonth() + 1)
    const year = formatTimeNumber(date.getUTCFullYear())

    return (
      <div className={s.dateTimeWrapper}>
        <div className={s.lastUpdate}>
          {updatedSecondsAgo} s<span className={s.hideOnMobile}>econds</span> ago
        </div>
        <div className={s.date}>
          {day}/{month}/{year}
        </div>
        <div className={s.time}>
          {hours}:{minutes}:{seconds}
        </div>
      </div>
    )
  }
}

Stats.propTypes = {
  time: PropTypes.number,
  updatedSecondsAgo: PropTypes.number
}

export default Stats
