import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import s from '../styles/Stats.cssmodule.scss'
import { MULTIPLIED_STATS, MULTIPLIER } from '../constants'

class Stats extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.updatedSecondsAgo === 0
  }

  getStatsValue = (name, value) => {
    const needMultiply = MULTIPLIED_STATS.some(item => name === item)

    return needMultiply ? +value * MULTIPLIER : value
  }

  checkAlert = () => {
    const { statistics } = this.props

    statistics.some(item => item.alert)
  }

  renderList = () => {
    const { statistics } = this.props

    return statistics.map(item => {
      return (
        <li key={item.name} className={cn({ [s.alert]: item.alert })}>
          <div className={s.itemTitle}>{item.name}</div>
          <div className={cn({ [s.zero]: item.minute === 0 })}>{this.getStatsValue(item.name, item.minute)}</div>
          <div>{this.getStatsValue(item.name, item.hour)}</div>
          <div>{this.getStatsValue(item.name, item.day)}</div>
        </li>
      )
    })
  }

  render() {
    const list = this.renderList()
    const alert = this.checkAlert()

    return (
      <div className={`${s.blockWrapper} ${s.statsBlock}`}>
        <div className={cn(s.left, { [s.alertBlock]: alert })} />
        <div className={s.statsWrapper}>
          <h2 className={s.title}>
            <div className={s.arrows} />
            Stats
          </h2>
          <div className={cn(s.stats, { [s.alert]: alert })}>
            <div className={`${s.square} ${s.left} ${s.top}`} />
            <div className={`${s.square} ${s.right} ${s.top}`} />
            <div className={`${s.square} ${s.right} ${s.bottom}`} />
            <div className={`${s.square} ${s.left} ${s.bottom}`} />
            <div className={s.statsTableWrapper}>
              <div className={`${s.corner} ${s.left} ${s.top}`} />
              <div className={`${s.corner} ${s.right} ${s.top}`} />
              <div className={`${s.corner} ${s.right} ${s.bottom}`} />
              <div className={`${s.corner} ${s.left} ${s.bottom}`} />
              <ul className={s.statsTable}>
                <li className={s.tableHeader}>
                  <div />
                  <div className={s.minute}>
                    <div>MINUTE</div>
                  </div>
                  <div className={s.hour}>
                    <div>HOUR</div>
                  </div>
                  <div className={s.day}>
                    <div>DAY</div>
                  </div>
                </li>
                {list}
              </ul>
            </div>
          </div>
          <ul className={s.rightRectangles}>
            <li />
            <li />
            <li />
          </ul>
        </div>
        <div className={cn(s.right, { [s.alertBlock]: alert })} />
      </div>
    )
  }
}

Stats.propTypes = {
  statistics: PropTypes.array,
  updatedSecondsAgo: PropTypes.number
}

export default Stats
