import { createAction, handleActions } from 'redux-actions'

const fetchUrl = (url, data) => {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      try {
        var json = JSON.parse(text)
        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = 'Server responded with: ' + text
      }

      if (error) {
        throw new Error(error.message)
      } else {
        return json
      }
    })
  })
}

const initialState = {
  dataFetched: false,
  updatedSecondsAgo: 1,
  time: parseInt(new Date().getTime() / 1000),
  usersonline: {
    now: {
      value: null,
      alert: null
    },
    hour: {
      value: null,
      alert: null
    },
    day: {
      value: null,
      alert: null
    }
  },
  statistics: [],
  graphs: {
    loadtime: {},
    slowqueries: {}
  }
}

export const setAppState = createAction('set app state', data => ({ data }))
export const dataLoaded = createAction('data loaded', data => ({ data }))
export const clockTick = createAction('clock tick')
export const updateFailed = createAction('update failed', failed => ({ failed }))

export const loadAppData = () => dispatch => {
  let url = new URL(location.href)
  let password = url.searchParams.get('password')

  return fetchUrl('/status_monitor.php?step=getStatusMonitorData&password=' + password)
    .then(data => {
      dispatch(dataLoaded(data))
    })
    .catch(error => {
      console.error(error)
      dispatch(updateFailed(true))
    })
}

export default handleActions(
  {
    'set app state'(state, action) {
      return {
        ...state,
        ...action.payload.data
      }
    },
    'data loaded'(state, action) {
      return {
        ...state,
        ...action.payload.data,
        dataFetched: true,
        updatedSecondsAgo: 0,
        updateFailed: false
      }
    },
    'clock tick'(state) {
      return {
        ...state,
        updatedSecondsAgo: state.updatedSecondsAgo + 1,
        time: parseInt(state.time) + 1
      }
    },
    'update failed'(state, action) {
      return {
        ...state,
        updateFailed: action.payload.failed
      }
    }
  },
  initialState
)
