const colors = {
  db1: '#c9d4d6',
  db2: '#9f2919',
  db3: '#38afdf',
  db4: '#f57d71',
  mDB: '#36a959'
}

export const MULTIPLIED_STATS = ['Activity logs', 'API status']
export const MULTIPLIER = 100

export default colors
