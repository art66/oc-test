// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'alert': string;
  'alertBlock': string;
  'arrows': string;
  'blockWrapper': string;
  'bottom': string;
  'chart': string;
  'day': string;
  'globalSvgShadow': string;
  'graphStats': string;
  'graphStatsWrapper': string;
  'graphicWrapper': string;
  'hideOnMobile': string;
  'hour': string;
  'left': string;
  'now': string;
  'pulse': string;
  'right': string;
  'rightRectangles': string;
  'slowQueries': string;
  'square': string;
  'stats': string;
  'statsBlock': string;
  'title': string;
  'top': string;
}
export const cssExports: CssExports;
export default cssExports;
