import React, { useCallback, useState } from 'react'
import { UseKeyEnter } from '../../hooks'
import DetailsInfo from '../DetailsInfo'
import { CONFIG, generateDetailsView } from './constants'
import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

export function UserStatsTableDetailsSubTable(props: IProps) {
  const { byProvider, bySport, overall } = props

  const [isByProviderShowing, setIsByProviderShowing] = useState(false)
  const [isBySportShowing, setIsBySportShowing] = useState(false)

  const onToggleShowResult = useCallback(() => {
    setIsByProviderShowing(!isByProviderShowing)
  }, [isByProviderShowing])
  const onToggleShowBySport = useCallback(() => {
    setIsBySportShowing(!isBySportShowing)
  }, [isBySportShowing])

  return (
    <table className={styles.detailsTable}>
      <thead>
        <tr>
          <th aria-label='Blank' />
          <th colSpan={5}>Bets</th>
          <th colSpan={4}>Money</th>
        </tr>
        <tr>
          {CONFIG.map(cell => {
            return (
              <th key={cell.dataKey} aria-label={cell.title || 'Blank'}>
                {cell.title}
              </th>
            )
          })}
        </tr>
      </thead>
      <tbody>
        <tr>
          {CONFIG.map(cell => {
            return (
              <td key={cell.dataKey} aria-label={cell.title || 'Blank'}>
                {overall[cell.dataKey]}
              </td>
            )
          })}
        </tr>
        <tr className={styles.subHeader}>
          <td onClick={onToggleShowResult} onKeyDown={UseKeyEnter(onToggleShowResult)}>
            {isByProviderShowing ? '-' : '+'} By provider
          </td>
          <td colSpan={9} />
        </tr>
        {isByProviderShowing
          && byProvider.map(provider => {
            return (
              <tr key={provider.name}>
                {CONFIG.map(cell => {
                  return (
                    <td key={cell.dataKey} aria-label={cell.title || 'Blank'}>
                      {provider[cell.dataKey]}
                    </td>
                  )
                })}
              </tr>
            )
          })}
        <tr className={styles.subHeader}>
          <td onClick={onToggleShowBySport} onKeyDown={UseKeyEnter(onToggleShowBySport)}>
            {isBySportShowing ? '-' : '+'} By sport
          </td>
          <td colSpan={9} />
        </tr>
        {isBySportShowing
          && bySport.map(sport => {
            return (
              <tr key={sport.name}>
                {CONFIG.map(cell => {
                  return (
                    <td key={cell.dataKey} aria-label={cell.title || 'Blank'}>
                      {sport[cell.dataKey]}
                    </td>
                  )
                })}
              </tr>
            )
          })}
      </tbody>
    </table>
  )
}

export function UserStatsTableDetails(props: IProps) {
  return (
    <div className={styles.details}>
      <DetailsInfo list={generateDetailsView(props)} />
      <div>
        <UserStatsTableDetailsSubTable {...props} />
      </div>
    </div>
  )
}
