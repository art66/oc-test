export const CONFIG = [
  {
    title: '',
    dataKey: 'name'
  },
  {
    title: 'Total',
    dataKey: 'bets'
  },
  {
    title: 'Won',
    dataKey: 'betsWon'
  },
  {
    title: 'Lost',
    dataKey: 'betsLost'
  },
  {
    title: 'Pending',
    dataKey: 'betsPending'
  },
  {
    title: 'Refunded',
    dataKey: 'betsRefunded'
  },
  {
    title: 'Bet',
    dataKey: 'moneyBet'
  },
  {
    title: 'Won',
    dataKey: 'moneyWon'
  },
  {
    title: 'Lost',
    dataKey: 'moneyLost'
  },
  {
    title: 'Refunded',
    dataKey: 'moneyRefunded'
  }
]

export function generateDetailsView({
  playername,
  id,
  totalBets,
  netProfit,
  moneyWonLostRatio,
  biggestMoneyWon,
  biggestMoneyLost,
  bestStrike,
  currentStrike,
  suspicions
}) {
  return {
    Player: `${playername} (${id})`,
    Bets: totalBets,
    'Net profit': netProfit,
    'Won/Lost ratio': moneyWonLostRatio,
    'Biggest Won': biggestMoneyWon,
    'Biggest Lost': biggestMoneyLost,
    'Best strike': bestStrike,
    'Current strike': currentStrike,
    Suspicions: suspicions
  }
}
