import React from 'react'
import DetailsEvent from '../DetailsEvent'
import DetailsInfo from '../DetailsInfo'
import DetailsSimpleSubtable from '../DetailsSimpleSubtable'
import ResultDetails from '../ResultDetails'
import { generateDetailsInfo, generateDetailsSimpleSubtable } from './constants'
import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

export default function EventDetails(props: IProps) {
  const { name, id, icon, competition, results } = props

  return (
    <div className={styles.details}>
      <DetailsInfo list={generateDetailsInfo(props)} />
      <DetailsEvent icon={icon} name={name} id={id} competition={competition} />
      <DetailsSimpleSubtable list={generateDetailsSimpleSubtable(props)} />
      <ResultDetails results={results} />
    </div>
  )
}
