import { ReactElement } from 'react'
import { TEventDetails } from '../../modules/interfaces'
import { TIsLoading } from '../../modules/interfaces'

export interface IProps extends TEventDetails {}

export type TCellConfig = {
  id: string
  name: string
  width?: number
  contentAsHtml?: boolean
  rotated?: boolean
}

export type TCellsConfig = {
  [key: string]: TCellConfig
}

export type TItem = { id: string; [key: string]: any }

export type TList = TItem[]

export interface TProps {
  cellsConfig: TCellsConfig
  list: TList
  excludeCells?: string[]
  headerHeight?: number
  isLoading?: TIsLoading
  onLoad: () => any
  onLoadMore: () => any
  onOrder: (id: string, order: boolean) => any
  searchContent?: ReactElement
}
