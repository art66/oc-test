export function generateDetailsInfo({ bets, amount, possibleGain }) {
  return {
    Bets: bets,
    Amount: amount,
    'Possible gain': possibleGain
  }
}

export function generateDetailsSimpleSubtable({ startdate, status, statusDesc, dateStarted, dateEnded }) {
  return [
    {
      title: 'Start date',
      value: startdate
    },
    {
      title: 'Status',
      value: status
    },
    {
      title: 'Status desc',
      value: statusDesc
    },
    {
      title: 'Date started',
      value: dateStarted
    },
    {
      title: 'Date ended',
      value: dateEnded
    }
  ]
}
