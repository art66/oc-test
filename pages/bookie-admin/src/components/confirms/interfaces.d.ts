import { ReactElement } from 'react'

export type TTitle = string | ReactElement | ReactElement[]

export interface IProps {
  title: TTitle
  description: string
}
