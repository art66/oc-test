import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { TGetView, IViews, IView } from '../Confirm/interfaces'
import {
  allowBetRequestActions,
  allowEventRequestActions,
  refundBetRequestActions,
  refundEventRequestActions,
  suspendBetRequestActions,
  suspendEventRequestActions
} from '../../modules/actions'
import {
  ALLOW_BET_TYPE,
  ALLOW_EVENT_TYPE,
  REFUND_BET_TYPE,
  REFUND_EVENT_TYPE,
  SUSPEND_BET_TYPE,
  SUSPEND_EVENT_TYPE
} from './constants'
import ConfirmTemplate from './ConfirmTemplate'
import { TTitle } from './interfaces'

function UseConfirmConfig(getProps: (props: any) => { title: TTitle; onYes: (arg: any) => void }) {
  return useCallback((payload): IView => {
    const { title, onYes } = getProps(payload)
    const { description } = payload

    return {
      content: <ConfirmTemplate title={title} description={description} />,
      yesButton: true,
      noButton: true,
      onYes
    }
  }, [])
}

function UseSuspendBetConfirmSettings(): TGetView {
  const dispatch = useDispatch()

  return UseConfirmConfig(payload => {
    const { id, playername } = payload

    return {
      title: (
        <>
          Suspend <b>{playername}</b> bet <b>{id}</b>:
        </>
      ),
      onYes() {
        dispatch(suspendBetRequestActions.initialAction(id))
      }
    }
  })
}

function UseAllowBetConfirmSettings(): TGetView {
  const dispatch = useDispatch()

  return UseConfirmConfig(payload => {
    const { id, playername } = payload

    return {
      title: (
        <>
          Allow <b>{playername}</b> bet <b>{id}</b>:
        </>
      ),
      onYes() {
        dispatch(allowBetRequestActions.initialAction(id))
      }
    }
  })
}

function UseRefundBetConfirmSettings(): TGetView {
  const dispatch = useDispatch()

  return UseConfirmConfig(payload => {
    const { id, playername } = payload

    return {
      title: (
        <>
          Refund <b>{playername}</b> bet <b>{id}</b>:
        </>
      ),
      onYes() {
        dispatch(refundBetRequestActions.initialAction(id))
      }
    }
  })
}

function UseSuspendEventConfirmSettings(): TGetView {
  const dispatch = useDispatch()

  return UseConfirmConfig(payload => {
    const { id, sport, name } = payload

    return {
      title: (
        <>
          Suspend <b>{id}</b> event{' '}
          <b>
            {sport} - {name}
          </b>
        </>
      ),
      onYes() {
        dispatch(suspendEventRequestActions.initialAction(id))
      }
    }
  })
}

function UseAllowEventConfirmSettings(): TGetView {
  const dispatch = useDispatch()

  return UseConfirmConfig(payload => {
    const { id, sport, name } = payload

    return {
      title: (
        <>
          Allow <b>{id}</b> event{' '}
          <b>
            {sport} - {name}
          </b>
        </>
      ),
      onYes() {
        dispatch(allowEventRequestActions.initialAction(id))
      }
    }
  })
}

function UseRefundEventConfirmSettings(): TGetView {
  const dispatch = useDispatch()

  return UseConfirmConfig(payload => {
    const { id, sport, name } = payload

    return {
      title: (
        <>
          Refund <b>{id}</b> event{' '}
          <b>
            {sport} - {name}
          </b>
        </>
      ),
      onYes() {
        dispatch(refundEventRequestActions.initialAction(id))
      }
    }
  })
}

export default function UseConfirmViews(): IViews {
  return {
    [SUSPEND_BET_TYPE]: UseSuspendBetConfirmSettings(),
    [ALLOW_BET_TYPE]: UseAllowBetConfirmSettings(),
    [REFUND_BET_TYPE]: UseRefundBetConfirmSettings(),
    [SUSPEND_EVENT_TYPE]: UseSuspendEventConfirmSettings(),
    [ALLOW_EVENT_TYPE]: UseAllowEventConfirmSettings(),
    [REFUND_EVENT_TYPE]: UseRefundEventConfirmSettings()
  }
}
