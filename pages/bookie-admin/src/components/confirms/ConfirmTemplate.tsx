import React, { memo } from 'react'
import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

export default memo((props: IProps) => {
  const { title, description } = props

  return (
    <>
      {title}
      <div className={styles.details} dangerouslySetInnerHTML={{ __html: description }} />
    </>
  )
})
