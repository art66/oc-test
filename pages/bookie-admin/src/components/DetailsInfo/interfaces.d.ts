export type TList = {
  [key: string]: string | number
}

export interface IProps {
  list: TList
}
