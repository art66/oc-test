import React from 'react'
import { IProps } from './interfaces'

export default function DetailsInfo(props: IProps) {
  const { list } = props

  return (
    <ul>
      {Object.keys(list).map(key => {
        const value = list[key]

        return (
          <li key={key}>
            <b>{key}:</b> {value}
          </li>
        )
      })}
    </ul>
  )
}
