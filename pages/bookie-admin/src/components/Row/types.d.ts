import { ReactElement } from 'react'
import { TCellsConfig, TExcludeCells } from '../StatsTable/types'

export interface IProps {
  cellsConfig: TCellsConfig
  excludeCells?: TExcludeCells
  data: {
    [key: string]: any
  }
  detailsView?: ReactElement
  children?: ReactElement | ReactElement[]
  onClick?: (id: string) => any
}
