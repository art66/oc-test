import React, { Fragment, memo } from 'react'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

export default memo(function Row(props: IProps) {
  const { data, cellsConfig, excludeCells, detailsView, children, onClick: onClickProp } = props

  function onClick() {
    if (onClickProp) {
      onClickProp(data.id)
    }
  }

  return (
    <Fragment key={data.id}>
      <tr onClick={onClick}>
        {Object.keys(data).map(key => {
          const value = data[key]
          const cellConfig = cellsConfig[key]

          if (excludeCells?.includes(key)) {
            return null
          }

          if (cellConfig?.contentAsHtml) {
            return <td key={key} dangerouslySetInnerHTML={{ __html: value }} />
          }

          return <td key={key}>{`${value}`}</td>
        })}
        <td className={styles.buttons}>{children}</td>
      </tr>
      {detailsView && (
        <tr key={`${data.id}-details`}>
          <td style={{ padding: 0 }} colSpan={Object.keys(data).length}>
            {detailsView}
          </td>
        </tr>
      )}
    </Fragment>
  )
})
