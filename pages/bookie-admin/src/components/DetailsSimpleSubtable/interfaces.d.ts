export interface IProps {
  list: {
    title: string
    value: string | number
  }[]
}
