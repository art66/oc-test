import React from 'react'
import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

export default function DetailsSimpleSubtable(props: IProps) {
  const { list } = props

  const headerCells = list.map(item => {
    return <th key={item.title}>{item.title}</th>
  })

  const bodyCells = list.map(item => {
    return <td key={item.title}>{item.value}</td>
  })

  return (
    <table className={styles.detailsTable}>
      <thead>
        <tr>{headerCells}</tr>
      </thead>
      <tbody>
        <tr>{bodyCells}</tr>
      </tbody>
    </table>
  )
}
