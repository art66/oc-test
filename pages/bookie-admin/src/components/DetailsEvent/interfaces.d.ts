export interface IProps {
  id: string
  icon: string
  name: string
  competition: string
}
