import React from 'react'
import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

export default function DetailsEvent(props: IProps) {
  const { icon, name, id, competition } = props

  return (
    <div className={styles.event}>
      <div>
        <i className={`gm-${icon}-icon`} />
      </div>
      <div>
        <div>
          {name} ({id})
        </div>
        <div>{competition}</div>
      </div>
    </div>
  )
}
