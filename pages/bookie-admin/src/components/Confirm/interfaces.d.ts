import { ReactElement } from 'react'

export interface IView {
  content?: string | ReactElement | ReactElement[]
  yesButton?: boolean
  noButton?: boolean
  onYes?: (...payload: any[]) => void
  onNo?: (...payload: any[]) => void
}

export type TGetView = (...payload: any[]) => IView

export interface IViews {
  [viewKey: string]: IView | TGetView
}

export interface IProps {
  views?: IViews
}

export interface IState {
  isShowing: boolean
  activeViewKey: string
  payload?: any
}
