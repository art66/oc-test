import { createAction } from '@reduxjs/toolkit'
import { IState } from './interfaces'

export const initialState: IState = {
  isShowing: false,
  activeViewKey: null,
  payload: null
}

export const showAction = createAction<boolean>('CONFIRM_SHOW')
export const showViewAction = createAction<{ viewKey: string; payload?: any }>('CONFIRM_SHOW_VIEW')

export function reducer(state: IState, action) {
  switch (action.type) {
    case showAction.type: {
      return { ...state, isShowing: action.payload }
    }
    case showViewAction.type: {
      return { ...state, activeViewKey: action.payload.viewKey, payload: action.payload.payload, isShowing: true }
    }
    default: {
      throw new Error()
    }
  }
}
