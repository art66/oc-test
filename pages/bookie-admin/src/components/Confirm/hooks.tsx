import React, { createContext, useContext, useReducer, useCallback, useMemo } from 'react'
import { initialState, reducer, showAction, showViewAction } from './store'
import { IState, IView, TGetView } from './interfaces'

const store = createContext(null)

const { Provider } = store

export function ConfirmProvider(props: { children: JSX.Element }) {
  const { children } = props
  const [state, dispatch]: [IState, any] = useReducer(reducer, initialState)

  return <Provider value={{ state, dispatch }}>{children}</Provider>
}

export function UseConfirm() {
  const globalState = useContext(store)
  const { state, dispatch } = globalState
  const { isShowing, activeViewKey, payload } = state

  const show = useCallback(isShowingValue => dispatch(showAction(isShowingValue)), [])
  const showView = useCallback(showViewValue => dispatch(showViewAction(showViewValue)), [])

  return {
    show,
    showView,
    isShowing,
    activeViewKey,
    payload
  }
}

export function UseResolveView(view: IView | TGetView, payload) {
  return useMemo((): IView => {
    if (typeof view === 'function') {
      return view(payload)
    }

    return view
  }, [view, payload])
}
