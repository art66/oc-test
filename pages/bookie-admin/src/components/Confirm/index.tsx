import React, { memo, useCallback } from 'react'
import Button from '@torn/shared/components/Button'
import { UseConfirm, UseResolveView } from './hooks'
import { IProps } from './interfaces'
import styles from './styles.cssmodule.scss'

export default memo((props: IProps) => {
  const { views } = props

  const { show, isShowing, activeViewKey, payload } = UseConfirm()

  const view = UseResolveView(views[activeViewKey], payload)

  const onClickNo = useCallback(() => {
    show(false)

    view?.onNo()
  }, [show, view?.onNo])
  const onClickYes = useCallback(() => {
    show(false)

    view?.onYes()
  }, [show, view?.onYes])

  if (!isShowing) return

  return (
    <div className={styles.background}>
      <div className={styles.confirm}>
        <div className={styles.content}>{view?.content}</div>
        <div className={styles.buttons}>
          {view?.yesButton && <Button className={styles.button} value='Yes' onClick={onClickYes} />}
          {view?.noButton && <Button className={styles.button} value='No' onClick={onClickNo} />}
        </div>
      </div>
    </div>
  )
})

export { UseConfirm }
export { ConfirmProvider } from './hooks'
