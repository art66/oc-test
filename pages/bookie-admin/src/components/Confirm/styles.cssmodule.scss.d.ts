// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'background': string;
  'button': string;
  'buttons': string;
  'confirm': string;
  'content': string;
  'globalSvgShadow': string;
}
export const cssExports: CssExports;
export default cssExports;
