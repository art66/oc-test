import { THeaderCellConfig } from '../../containers/AbuseAlerts/types'
import { TOrder } from '../../modules/interfaces'
import { TCellConfig } from '../StatsTable/types'

export interface IProps extends TCellConfig {
  order?: TOrder
  setOrder: (id: string, order: TOrder) => any
  rotated?: THeaderCellConfig['rotated']
}
