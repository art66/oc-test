import React, { useMemo, useCallback } from 'react'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import { ORDER_ASC, ORDER_DESC } from '../../modules/constants'
import { UseKeyEnter } from '../../hooks'
import { DEFAULT_CELL_WIDTH } from './constants'

export function HeaderCell(props: IProps) {
  const { id, name, width, order, rotated, setOrder } = props

  const onNameClick = useCallback(() => {
    if (!order) {
      setOrder(id, ORDER_ASC)
    } else if (order === ORDER_ASC) {
      setOrder(id, ORDER_DESC)
    } else if (order) {
      setOrder(null, null)
    }
  }, [setOrder, order])

  const orderIconView = useMemo(() => {
    if (order === ORDER_DESC) {
      return <>&#9650;</>
    } else if (order === ORDER_ASC) {
      return <>&#9660;</>
    }

    return null
  }, [order])
  const orderView = useMemo(() => orderIconView && <i className={styles.sortIcon}>{orderIconView}</i>, [orderIconView])

  return (
    <div className={styles.container} style={{ width: width || DEFAULT_CELL_WIDTH }}>
      <p className={styles.nameContainer} onClick={onNameClick} onKeyDown={UseKeyEnter(onNameClick)}>
        <span className={cn(styles.name, rotated && styles.rotatedName)}>{name}</span>
        {orderView}
      </p>
    </div>
  )
}
