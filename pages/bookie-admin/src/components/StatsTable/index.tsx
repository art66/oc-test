import React, { useEffect, useMemo, useState, useCallback } from 'react'
import { TProps } from './types'
import styles from './index.cssmodule.scss'
import { HeaderCell } from '../HeaderCell'
import { LoadMore } from '../LoadMore'

export default function StatsTable(props: TProps) {
  const {
    cellsConfig,
    excludeCells,
    list,
    isLoading,
    headerHeight,
    onLoad,
    onLoadMore,
    onOrder,
    searchContent,
    children
  } = props

  useEffect(() => {
    onLoad()
  }, [])

  const [orderBy, setOrderBy] = useState(null)
  const [order, setReverse] = useState(null)

  const onSetOrder = useCallback(
    (id, nextOrder) => {
      setOrderBy(id)
      setReverse(nextOrder)

      onOrder(id, nextOrder)
    },
    [setOrderBy]
  )
  const getNormalizedOrder = useCallback(id => (orderBy === id ? order : null), [orderBy, order])

  const headerCellsView = useMemo(() => {
    return (
      list.length > 0 &&
      Object.keys(list[0]).map(key => {
        const cellConfig = cellsConfig[key]

        if (excludeCells?.includes(key)) {
          return null
        }

        const normalizedOrder = getNormalizedOrder(cellConfig?.id || key)

        if (cellConfig) {
          return (
            <th className={styles.rotated} key={cellConfig.id}>
              <HeaderCell {...cellConfig} order={normalizedOrder} setOrder={onSetOrder} rotated={cellConfig.rotated} />
            </th>
          )
        }

        return (
          <th key={key} className={styles.rotated}>
            <HeaderCell id={key} name={key} order={normalizedOrder} setOrder={onSetOrder} rotated />
          </th>
        )
      })
    )
  }, [list, order, orderBy, onSetOrder])

  return (
    <>
      <header>{searchContent}</header>
      <div className={styles.container}>
        {list.length > 0 ? (
          <table className={styles.table}>
            <thead>
              <tr style={{ height: headerHeight }}>
                {headerCellsView}
                <th aria-label='Blank' />
              </tr>
            </thead>
            <tbody>{children}</tbody>
          </table>
        ) : (
          !isLoading && <div className={styles.noResults}>No results</div>
        )}
      </div>
      <LoadMore onLoading={onLoadMore} isLoading={isLoading} />
    </>
  )
}
