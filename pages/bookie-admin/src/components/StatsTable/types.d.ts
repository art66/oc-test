import { ReactElement } from 'react'
import { TIsLoading } from '../../modules/interfaces'

export type TCellConfig = {
  id: string
  name: string
  width?: number
  contentAsHtml?: boolean
  rotated?: boolean
}

export type TCellsConfig = {
  [key: string]: TCellConfig
}

export type TItem = { id: string; [key: string]: any }

export type TList = TItem[]

export type TExcludeCells = string[]

export interface TProps {
  cellsConfig: TCellsConfig
  list: TList
  excludeCells?: TExcludeCells
  headerHeight?: number
  isLoading?: TIsLoading
  searchContent?: ReactElement
  detailsRowIndex?: number
  onLoad: () => any
  onLoadMore: () => any
  onOrder: (id: string, order: boolean) => any
  children: ReactElement[]
}
