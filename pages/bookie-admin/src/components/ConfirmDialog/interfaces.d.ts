export interface IProps {
  text: string
  onYes: () => void
  onNo: () => void
}
