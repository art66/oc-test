import React from 'react'
import { IProps } from './interfaces'
import styles from './index.cssmodule.scss'

export default function ConfirmDialog(props: IProps) {
  const { text, onYes, onNo } = props

  return (
    <div className={styles.confirmDialogWrap}>
      <div className={styles.confirmDialog}>
        <div>{text}</div>
        <div className={styles.buttons}>
          <button onClick={onYes}>Yes</button>
          <button onClick={onNo}>No</button>
        </div>
      </div>
    </div>
  )
}
