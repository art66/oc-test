import React, { useCallback, useState, useMemo } from 'react'
import styles from './index.cssmodule.scss'
import { TProps } from './types'
import { slower } from '../../utils'

export function Search(props: TProps) {
  const { onChange, children, width, defaultValue } = props

  const [search, setSearch] = useState(defaultValue || '')

  const onSetUsersStatsSearch = useCallback(searchValue => onChange(searchValue), [onChange])
  const slow = useMemo(() => slower(onSetUsersStatsSearch, 750), [onSetUsersStatsSearch])

  const onInput = useCallback(
    ({ target: { value } }) => {
      setSearch(value)

      slow(value)
    },
    [onSetUsersStatsSearch]
  )

  return (
    <label className={styles.label}>
      <p className={styles.description}>{children}</p>
      <input className={styles.input} style={{ width }} type='text' onChange={onInput} value={search} />
    </label>
  )
}
