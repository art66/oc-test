import { ReactElement } from 'react'

export type TProps = {
  defaultValue?: string
  onChange: (search: string) => any
  children: string | HTMLElement | ReactElement
  width?: number
}
