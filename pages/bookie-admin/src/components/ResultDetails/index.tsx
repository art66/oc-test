import React, { Fragment, useCallback, useState } from 'react'
import { UseKeyEnter } from '../../hooks'
import styles from './index.cssmodule.scss'
import { TProps } from './interfaces'

export default function ResultDetails(props: TProps) {
  const { results } = props

  const [isResultShowing, setIsResultShowing] = useState(false)

  const onToggleShowBySport = useCallback(() => {
    setIsResultShowing(!isResultShowing)
  }, [isResultShowing])

  return (
    <div className={styles.resultContainer}>
      <div
        className={styles.resultActivator}
        onClick={onToggleShowBySport}
        onKeyDown={UseKeyEnter(onToggleShowBySport)}
      >
        {isResultShowing ? '-' : '+'} Results
      </div>
      {isResultShowing && (
        <table className={styles.detailsResultsTable}>
          <thead>
            <tr>
              <th>Participant</th>
              <th>Value</th>
              <th>Update time</th>
            </tr>
          </thead>
          <tbody>
            {results.map(result => {
              return (
                <Fragment key={result.name}>
                  <tr className={styles.subheader}>
                    <td colSpan={3}>{result.name}</td>
                  </tr>
                  {result.values.map(value => {
                    return (
                      <tr key={value.name} className={styles.rowsContent}>
                        <td>{value.name}</td>
                        <td>{value.value}</td>
                        <td>{value.ut}</td>
                      </tr>
                    )
                  })}
                </Fragment>
              )
            })}
          </tbody>
        </table>
      )}
    </div>
  )
}
