export interface TProps {
  results: {
    name: string
    values: {
      name: string
      value: string
      ut: string
    }[]
  }[]
}
