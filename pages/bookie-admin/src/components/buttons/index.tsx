import React, { memo } from 'react'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import ActionButton from '../ActionButton'

export const SuspendButton = memo((props: IProps) => {
  const { onClick } = props

  return (
    <ActionButton className={styles.suspend} onClick={onClick}>
      Suspend
    </ActionButton>
  )
})

export const AllowButton = memo((props: IProps) => {
  const { onClick } = props

  return (
    <ActionButton className={styles.allow} onClick={onClick}>
      Allow
    </ActionButton>
  )
})

export const RefundButton = memo((props: IProps) => {
  const { onClick } = props

  return (
    <ActionButton className={styles.refund} onClick={onClick}>
      Refund
    </ActionButton>
  )
})

export const RefundedButton = memo(() => {
  return (
    <ActionButton className={styles.refunded} disabled={true}>
      Refunded
    </ActionButton>
  )
})
