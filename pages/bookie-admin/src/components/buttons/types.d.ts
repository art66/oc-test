import { IProps as IButtonProps } from '../ActionButton/types'

export interface IProps {
  onClick: IButtonProps['onClick']
}
