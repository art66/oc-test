export interface IProps {
  className: string
  children: string
  onClick?: () => void
  disabled?: boolean
}
