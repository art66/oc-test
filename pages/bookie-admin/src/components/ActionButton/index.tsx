import React, { memo } from 'react'
import cn from 'classnames'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

export default memo((props: IProps) => {
  const { className, children, onClick } = props

  return (
    <button type='button' className={cn(styles.button, className)} onClick={onClick}>
      {children}
    </button>
  )
})
