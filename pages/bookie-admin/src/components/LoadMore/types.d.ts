import { TIsLoading } from '../../modules/interfaces'

export type TProps = {
  isLoading: TIsLoading
  onLoading: () => any
}
