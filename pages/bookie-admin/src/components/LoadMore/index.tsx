import React, { useCallback, useEffect, useState, useMemo, useRef } from 'react'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import { TProps } from './types'

const LOADING_PRESTART_RANGE = 100
const LOADING_CONFIRM_RANGE = 50

export function LoadMore(props: TProps) {
  const { onLoading, isLoading } = props

  const ref = useRef(null)
  const element = ref.current

  const [[prestartProportion, loadingConfirmProportion], setProportion] = useState([0, 0])

  const onWindowScroll = useCallback(() => {
    if (isLoading || !element) return

    const scrollY = document.documentElement.scrollTop
    const extraScrollY = scrollY + window.innerHeight - (element.offsetTop + element.clientHeight)
    // @ts-ignore
    const isForward = scrollY > onWindowScroll.prevScrollY
    const currentPrestartProportion = extraScrollY / LOADING_PRESTART_RANGE

    if (currentPrestartProportion < 1) {
      setProportion([currentPrestartProportion, 0])
    } else {
      const currentLoadingConfirmProportion = (extraScrollY - LOADING_PRESTART_RANGE) / LOADING_CONFIRM_RANGE

      if (currentLoadingConfirmProportion < 1) {
        setProportion([1, currentLoadingConfirmProportion])
      } else {
        setProportion([1, 1])

        if (isForward) {
          onLoading()
        }
      }
    }

    // @ts-ignore
    onWindowScroll.prevScrollY = scrollY
  }, [element, isLoading])

  useEffect(() => {
    window.addEventListener('scroll', onWindowScroll)

    return () => window.removeEventListener('scroll', onWindowScroll)
  }, [onWindowScroll])

  const style = useMemo(() => {
    if (prestartProportion < 1) {
      return {
        opacity: prestartProportion,
        fontSize: 30 * prestartProportion,
        height: 50 * prestartProportion
      }
    } else if (loadingConfirmProportion > 0 && loadingConfirmProportion < 1) {
      return {
        opacity: 1 - loadingConfirmProportion,
        fontSize: 30,
        transform: `translateY(${20 * loadingConfirmProportion}px)`,
        height: 50
      }
    }
  }, [prestartProportion, loadingConfirmProportion])

  return (
    <aside className={styles.container} ref={ref}>
      <i className={styles.arrowIcon} style={style}>
        &#8595;
      </i>
      <i className={cn(styles.moreIcon, isLoading && styles.loading)}>&#8230;</i>
    </aside>
  )
}
