import { TBetDetails } from '../../modules/interfaces'

export interface IProps extends Pick<TBetDetails, 'id' | 'result'> {
  isLoading: TBetDetails['isResultLoading']
}
