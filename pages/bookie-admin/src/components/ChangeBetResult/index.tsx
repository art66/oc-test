import React, { useCallback, useState } from 'react'
import { useDispatch } from 'react-redux'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'
import { CHANGE_RESULTS_OPTIONS } from './constants'
import { fixBetResultRequestActions } from '../../modules/actions'
import ConfirmDialog from '../ConfirmDialog'

export default function ChangeBetResult(props: IProps) {
  const { id, result } = props

  const dispatch = useDispatch()

  const [resultState, setResultState] = useState(result)
  const [isChangingResult, setIsChangingResult] = useState(false)
  const [isAlertShowing, setIsAlertShowing] = useState(false)

  const onChangeButtonClick = useCallback(() => setIsChangingResult(true), [])
  const onChangeSelect = useCallback(event => {
    const nextResult = event.target.value

    setResultState(nextResult)
  }, [])
  const onChangeCancel = useCallback(() => {
    setIsChangingResult(false)
    setResultState(result)
  }, [result])
  const onSaveButton = useCallback(() => setIsAlertShowing(true), [])
  const onSaveAlertButton = useCallback(() => {
    setIsAlertShowing(false)

    dispatch(fixBetResultRequestActions.initialAction(id, resultState))
  }, [id, resultState])
  const onCloseAlertButton = useCallback(() => setIsAlertShowing(false), [])

  return !isChangingResult ? (
    <button className={styles.change} onClick={onChangeButtonClick}>
      [Change]
    </button>
  ) : (
    <>
      <select className={styles.resultSelector} value={resultState} onChange={onChangeSelect}>
        {CHANGE_RESULTS_OPTIONS.map(option => {
          return (
            <option className={cn(option.id === result && styles.active)} key={option.id} value={option.id}>
              {option.title}
            </option>
          )
        })}
      </select>
      <button className={cn(styles.change, styles.subButton)} onClick={onSaveButton} disabled={resultState === result}>
        Save
      </button>
      <button
        className={cn(styles.change, styles.subButton)}
        onClick={onChangeCancel}
        disabled={resultState === result}
      >
        Cancel
      </button>
      {isAlertShowing && (
        <ConfirmDialog
          text={`Do you want to change the result from ${result} to ${resultState}?`}
          onYes={onSaveAlertButton}
          onNo={onCloseAlertButton}
        />
      )}
    </>
  )
}
