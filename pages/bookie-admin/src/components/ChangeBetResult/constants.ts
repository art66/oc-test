export const CHANGE_RESULTS_OPTIONS = [
  {
    id: 'won',
    title: 'Won'
  },
  {
    id: 'lost',
    title: 'Lost'
  },
  {
    id: 'refunded',
    title: 'Refunded'
  }
]
