export function generateTableOne({ id, userID, playername, outcomeId, bettingofferId }) {
  return [
    {
      title: 'Id',
      value: id
    },
    {
      title: 'User id',
      value: userID
    },
    {
      title: 'Player name',
      value: playername
    },
    {
      title: 'Outcome id',
      value: outcomeId
    },
    {
      title: 'Bettingoffer id',
      value: bettingofferId
    }
  ]
}

export function generateTableTwo({ name, timestamp, amount, odds, handicap, odds_provider, result, withdrawn }) {
  return [
    {
      title: 'Bettingoffer',
      value: name
    },
    {
      title: 'Date placed',
      value: timestamp
    },
    {
      title: 'Amount',
      value: amount
    },
    {
      title: 'Odds',
      value: odds
    },
    {
      title: 'Handicap',
      value: handicap
    },
    {
      title: 'Provider',
      value: odds_provider
    },
    {
      title: 'Result',
      value: result
    },
    {
      title: 'Withdrawn',
      value: `${withdrawn}`
    }
  ]
}

export function generateTableThree({ startdate, status, statusDesc, dateStarted, dateEnded }) {
  return [
    {
      title: 'Start date',
      value: startdate
    },
    {
      title: 'Status',
      value: status
    },
    {
      title: 'Status desc',
      value: statusDesc
    },
    {
      title: 'Date started',
      value: dateStarted
    },
    {
      title: 'Date ended',
      value: dateEnded
    }
  ]
}

export const RESULT_PENDING_STATE = 'pending'
