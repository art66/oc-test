import React, { useCallback, useState } from 'react'
import styles from './index.cssmodule.scss'
import DetailsEvent from '../DetailsEvent'
import DetailsSimpleSubtable from '../DetailsSimpleSubtable'
import ResultDetails from '../ResultDetails'
import { IProps } from './interfaces'
import { UseKeyEnter } from '../../hooks'
import { generateTableOne, generateTableThree, generateTableTwo, RESULT_PENDING_STATE } from './constants'
import ChangeBetResult from '../ChangeBetResult'

export default function BetsDetails(props: IProps) {
  const {
    id,
    eventName,
    eventId,
    description,
    event,
    results,
    log,
    onShowLog,
    odds_provider,
    result,
    isResultLoading,
    withdrawn
  } = props

  const [isBetLogShowing, setIsBetLogShowing] = useState(false)

  const toggleBetLog = useCallback(() => {
    setIsBetLogShowing(!isBetLogShowing)

    if (!isBetLogShowing && onShowLog) {
      onShowLog()
    }
  }, [isBetLogShowing, onShowLog])

  return (
    <div className={styles.details}>
      <DetailsSimpleSubtable list={generateTableOne(props)} />
      <DetailsSimpleSubtable list={generateTableTwo(props)} />
      <DetailsEvent icon={event.icon} name={eventName} id={eventId} competition={event.competition} />
      <DetailsSimpleSubtable list={generateTableThree(event)} />
      <ResultDetails results={results} />
      <div className={styles.betLog}>
        <div>
          <div className={styles.betLogActivator} onClick={toggleBetLog} onKeyDown={UseKeyEnter(toggleBetLog)}>
            {isBetLogShowing ? 'Hide' : 'View'} bet log
          </div>
          {isBetLogShowing && (
            <ul className={styles.betLogList}>
              {log?.isLoading ?
                'Loading...' :
                log?.logData?.map(logItem => {
                  return (
                    <li key={logItem.message}>
                      {logItem.date} {logItem.message}
                    </li>
                  )
                })}
            </ul>
          )}
        </div>
      </div>
      <div className={styles.description}>
        <div className={styles.descriptionTitle}>Description:</div>
        {description ? <p dangerouslySetInnerHTML={{ __html: description }} /> : <p>None</p>}
      </div>
      <div className={styles.betResult}>
        <table>
          <thead>
            <tr>
              <th>Provider</th>
              <th>Result</th>
              <th>Withdrawn</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{odds_provider}</td>
              <td>
                <div>{result}</div>
                {result !== RESULT_PENDING_STATE && (
                  <div>
                    <ChangeBetResult id={id} result={result} isLoading={isResultLoading} />
                  </div>
                )}
              </td>
              <td>{withdrawn}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}
