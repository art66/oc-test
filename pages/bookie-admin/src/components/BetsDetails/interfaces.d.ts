import { TBetDetails } from '../../modules/interfaces'

export interface IProps extends TBetDetails {
  onShowLog: () => void
}
