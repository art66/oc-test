export const RESULT_PENDING_TYPE = 'pending'
export const RESULT_NOTSTARTED_TYPE = 'notstarted'
export const RESULT_INPROGRESS_TYPE = 'inprogress'
export const RESULT_WON_TYPE = 'won'
export const RESULT_LOST_TYPE = 'lost'
export const RESULT_REFUNDED_TYPE = 'refunded'

export const EVENTS_TABLE_KEY = 'events'
export const ABUSE_ALERTS_TABLE_KEY = 'abuse_alerts'
export const BETS_TABLE_KEY = 'bets'
export const USER_STATS_TABLE_KEY = 'user_stats'

export const DEFAULT_WIDTH = 30

export const USER_SEARCH_QUERY_KEYS_LIST = ['userIdOrPlayerName']
export const BETS_SEARCH_QUERY_KEYS_LIST = ['playername', 'userId', 'betId', 'eventId']
export const ABUSE_ALERTS_SEARCH_QUERY_KEYS_LIST = ['userIdOrPlayerName']
export const EVENTS_SEARCH_QUERY_KEYS_LIST = ['eventId', 'nameOrCompetition', 'userId']
