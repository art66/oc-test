import { TCommonState } from '../modules/interfaces'

export interface IState {
  common: TCommonState
}
