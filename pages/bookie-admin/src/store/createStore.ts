import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'
import initialState from './initialState'
import saga, { rootSaga, runSaga } from './middleware/saga'
import logger from './middleware/reduxLogger'
import makeRootReducer from './rootReducer'

const rootStore = () => {
  // ========================================================
  // Store and History Instantiation
  // ========================================================

  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [saga, thunk]

  // @ts-ignore
  if (__DEV__) {
    middleware.push(logger)
  }

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  // @ts-ignore
  if (__DEV__ && window.__REDUX_DEVTOOLS_EXTENSION__) {
    // @ts-ignore
    enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    // @ts-ignore
    makeRootReducer(),
    initialState as any,
    compose(responsiveStoreEnhancer, applyMiddleware(...middleware), ...enhancers)
  )

  // @ts-ignore
  store.runSaga = runSaga
  runSaga(rootSaga)
  // @ts-ignore
  store.asyncReducers = {}

  // @ts-ignore
  if (module.hot) {
    // @ts-ignore
    module.hot.accept('./rootReducer', () => {
      // @ts-ignore
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }

  return store
}

export default rootStore()
