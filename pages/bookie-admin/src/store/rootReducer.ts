import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import { routerReducer as routing } from 'react-router-redux'
import common from '../modules/reducers'

const makeRootReducer = asyncReducers => {
  return combineReducers({
    ...asyncReducers,
    common,
    routing,
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    })
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return
  store.asyncReducers[key] = reducer
  console.log('Add new route: ', store.asyncReducers)

  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
