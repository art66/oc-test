import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getOverallRequestActions } from '../actions'
import { GET_OVERALL_URL } from './constants'

export default function* getOverallRequestSaga() {
  try {
    const response = yield fetchUrl(GET_OVERALL_URL, null, null)

    if (response.error) {
      throw response.error
    }

    yield put(getOverallRequestActions.successAction(response.stats))
  } catch (error) {
    console.error(error)

    yield put(getOverallRequestActions.failureAction())
  }
}
