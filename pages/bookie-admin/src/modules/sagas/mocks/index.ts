export const eventsResponse = {
  events: [
    {
      id: '3522332',
      sportID: 24,
      sport: 'Am. Football',
      name: 'Tampa Bay Buccaneers-Kansas City Chiefs',
      stage: 'Superbowl',
      template: 'USA 1',
      tournament: '2020/2021',
      competition: 'Superbowl 2020/2021(USA 1)',
      countryCode: 'USA',
      gender: 'male',
      status: 'notstarted',
      startdate: '2021-02-08 00:30:00',
      alias: 'american-football',
      amount: 210,
      possibleGain: 208,
      bets: 2,
      suspended: false,
      refunded: false
    }
  ]
}
