import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { FIX_BET_RESULT_URL } from './constants'
import { fixBetResultRequestActions } from '../actions'

export default function* changeBetResultSaga(action: { payload: [string, string] }) {
  const {
    payload: [betId, result]
  } = action

  try {
    const response = yield fetchUrl(FIX_BET_RESULT_URL({ betId, result }), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(fixBetResultRequestActions.successAction(betId, result))
  } catch (error) {
    yield put(fixBetResultRequestActions.failureAction(betId, error))
  }
}
