import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  getUsersStatsRequestActions,
  setUsersStatsOrderRequestActions,
  usersStatsLoadMoreRequestActions
} from '../actions'
import { GET_USER_STATS_URL } from './constants'
import { getUsersStatsOrderBy, getUsersStatsReverse, getUsersStatsNextPage } from '../selectors'
import { getSearchQuery } from './utils'
import { USER_SEARCH_QUERY_KEYS_LIST } from '../../constants'

export default function* getUserStatsRequestHandlerSaga() {
  try {
    const search = getSearchQuery(USER_SEARCH_QUERY_KEYS_LIST)
    const orderBy = yield select(getUsersStatsOrderBy)
    const order = yield select(getUsersStatsReverse)
    const page = yield select(getUsersStatsNextPage)

    const response = yield fetchUrl(GET_USER_STATS_URL({ search, orderBy, order, page }), null, null)

    if (response.error) {
      throw response.error
    }

    return response.betStats
  } catch (error) {
    console.error(error)

    throw error
  }
}

export function* getUserStatsRequestSaga() {
  try {
    const userStats = yield getUserStatsRequestHandlerSaga()

    yield put(getUsersStatsRequestActions.successAction(userStats))
  } catch (error) {
    yield put(getUsersStatsRequestActions.failureAction())
  }
}

export function* setUsersStatsOrderRequestSaga() {
  try {
    const userStats = yield getUserStatsRequestHandlerSaga()

    yield put(setUsersStatsOrderRequestActions.successAction(userStats))
  } catch (error) {
    yield put(setUsersStatsOrderRequestActions.failureAction())
  }
}

export function* usersStatsLoadMoreRequestSaga() {
  try {
    const userStats = yield getUserStatsRequestHandlerSaga()

    yield put(usersStatsLoadMoreRequestActions.successAction(userStats))
  } catch (error) {
    yield put(usersStatsLoadMoreRequestActions.failureAction())
  }
}
