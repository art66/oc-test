import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getBookmakersRequestActions } from '../actions'
import { GET_BOOKMAKERS_URL } from './constants'

export default function* getBookmakersRequestSaga() {
  try {
    const response = yield fetchUrl(GET_BOOKMAKERS_URL, null, null)

    if (response.error) {
      throw response.error
    }

    yield put(getBookmakersRequestActions.successAction(response.stats))
  } catch (error) {
    console.error(error)

    yield put(getBookmakersRequestActions.failureAction())
  }
}
