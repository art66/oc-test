import {
  normalizeBetsSearch,
  normalizeEventsSearch,
  normalizeSuspiciousActions,
  normalizeUsersSearch
} from './normalizers'

export const GET_OVERALL_URL = '/page.php?sid=bookieApiSecured&action=getOverall'

export const GET_BOOKMAKERS_URL = '/page.php?sid=bookieApiSecured&action=getStatsByProvider'

export const GET_USER_STATS_URL = ({ search, orderBy, order, page }) =>
  `/page.php?sid=bookieApiSecured&action=getBetStats${search ? normalizeUsersSearch(search) : ''}${
    orderBy ? `&orderBy=${orderBy}` : ''
  }${order ? `&order=${order}` : ''}&page=${page}`

export const GET_BETS_STATS_URL = ({ search, orderBy, order, page }) =>
  `/page.php?sid=bookieApiSecured&action=getBets${search ? normalizeBetsSearch(search) : ''}${
    orderBy ? `&orderBy=${orderBy}` : ''
  }${order ? `&order=${order}` : ''}&page=${page}`

export const GET_SUSPICIOUS_ACTIONS_URL = ({ search, orderBy, order, page }) =>
  `/page.php?sid=bookieApiSecured&action=getSuspicions${search ? normalizeSuspiciousActions(search) : ''}${
    orderBy ? `&orderBy=${orderBy}` : ''
  }${order ? `&order=${order}` : ''}&page=${page}`

export const GET_EVENTS_URL = ({ search, orderBy, order, page }) =>
  `/page.php?sid=bookieApiSecured&action=getEvents${search ? normalizeEventsSearch(search) : ''}${
    orderBy ? `&orderBy=${orderBy}` : ''
  }${order ? `&order=${order}` : ''}&page=${page}`

export const GET_BET_LOG_URL = betId => `/page.php?sid=bookieApiSecured&action=getBetLog&betId=${betId}`
export const SUSPEND_BET_URL = id => `/page.php?sid=bookieApiSecured&action=suspendBet&betId=${id}`
export const ALLOW_BET_URL = id => `/page.php?sid=bookieApiSecured&action=allowBet&betId=${id}`
export const REFUND_BET_URL = id => `/page.php?sid=bookieApiSecured&action=refundBet&betId=${id}`

export const SUSPEND_EVENT_URL = id => `/page.php?sid=bookieApiSecured&action=suspendEvent&eventId=${id}`
export const ALLOW_EVENT_URL = id => `/page.php?sid=bookieApiSecured&action=allowEvent&eventId=${id}`
export const REFUND_EVENT_URL = id => `/page.php?sid=bookieApiSecured&action=refundEvent&eventId=${id}`

export const FIX_BET_RESULT_URL = ({ betId, result }) =>
  `/page.php?sid=bookieApiSecured&action=fixBet&betId=${betId}&result=${result}`
