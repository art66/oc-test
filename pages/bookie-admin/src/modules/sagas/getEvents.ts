import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getEventsRequestActions, setEventsOrderRequestActions, eventsLoadMoreRequestActions } from '../actions'
import { GET_EVENTS_URL } from './constants'
import { getEventsOrderBy, getEventsReverse, getEventsNextPage } from '../selectors'
import { EVENTS_SEARCH_QUERY_KEYS_LIST } from '../../constants'
import { getSearchQuery } from './utils'
// import { MOCK_EVETS_RESPONSE } from './mocks/eventsResponse'

export default function* getEventsRequestHandlerSaga() {
  try {
    const search = getSearchQuery(EVENTS_SEARCH_QUERY_KEYS_LIST)
    const orderBy = yield select(getEventsOrderBy)
    const order = yield select(getEventsReverse)
    const page = yield select(getEventsNextPage)

    const response = yield fetchUrl(GET_EVENTS_URL({ search, orderBy, order, page }), null, null)

    if (response.error) {
      throw response.error
    }

    return response.events
    // return MOCK_EVETS_RESPONSE.events
  } catch (error) {
    console.error(error)

    throw error
  }
}

export function* getEventsRequestSaga() {
  try {
    const events = yield getEventsRequestHandlerSaga()

    yield put(getEventsRequestActions.successAction(events))
  } catch (error) {
    yield put(getEventsRequestActions.failureAction())
  }
}

export function* setEventsOrderRequestSaga() {
  try {
    const events = yield getEventsRequestHandlerSaga()

    yield put(setEventsOrderRequestActions.successAction(events))
  } catch (error) {
    yield put(setEventsOrderRequestActions.failureAction())
  }
}

export function* eventsLoadMoreRequestSaga() {
  try {
    const events = yield getEventsRequestHandlerSaga()

    yield put(eventsLoadMoreRequestActions.successAction(events))
  } catch (error) {
    yield put(eventsLoadMoreRequestActions.failureAction())
  }
}
