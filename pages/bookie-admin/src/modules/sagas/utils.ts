import { getQuery } from '../../utils'

export function getSearchQuery(paramsList: string[]) {
  const queryObject = getQuery()

  return paramsList.reduce((searchData, paramKey) => {
    if (queryObject.has(paramKey)) {
      searchData[paramKey] = queryObject.get(paramKey)
    }

    return searchData
  }, {}) as { [key: string]: string }
}
