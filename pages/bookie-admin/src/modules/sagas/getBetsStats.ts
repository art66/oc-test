import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getBetsRequestActions, setBetsOrderRequestActions, betsLoadMoreRequestActions } from '../actions'
import { GET_BETS_STATS_URL } from './constants'
import { getBetsOrderBy, getBetsReverse, getBetsNextPage } from '../selectors'
import { getSearchQuery } from './utils'
import { BETS_SEARCH_QUERY_KEYS_LIST } from '../../constants'
// import { MOCK_BETS_RESPONSE } from './mocks/betsResponse'

export default function* getBetsRequestHandlerSaga() {
  try {
    const search = getSearchQuery(BETS_SEARCH_QUERY_KEYS_LIST)
    const orderBy = yield select(getBetsOrderBy)
    const order = yield select(getBetsReverse)
    const page = yield select(getBetsNextPage)

    const response = yield fetchUrl(GET_BETS_STATS_URL({ search, orderBy, order, page }), null, null)

    if (response.error) {
      throw response.error
    }

    return response.bets
  } catch (error) {
    console.error(error)

    throw error
  }
}

export function* getBetsRequestSaga() {
  try {
    const bets = yield getBetsRequestHandlerSaga()

    yield put(getBetsRequestActions.successAction(bets))
  } catch (error) {
    yield put(getBetsRequestActions.failureAction())
  }
}

export function* setBetsOrderRequestSaga() {
  try {
    const bets = yield getBetsRequestHandlerSaga()

    yield put(setBetsOrderRequestActions.successAction(bets))
  } catch (error) {
    yield put(setBetsOrderRequestActions.failureAction())
  }
}

export function* betsLoadMoreRequestSaga() {
  try {
    const bets = yield getBetsRequestHandlerSaga()

    yield put(betsLoadMoreRequestActions.successAction(bets))
  } catch (error) {
    yield put(betsLoadMoreRequestActions.failureAction())
  }
}
