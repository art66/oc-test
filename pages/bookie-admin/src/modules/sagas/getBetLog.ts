import { fetchUrl } from '@torn/shared/utils'
import { put } from 'redux-saga/effects'
import { GET_BET_LOG_URL } from './constants'
import { getBetsLogRequestActions } from '../actions'

export default function* getBetsLogRequestSaga(action: { payload: [string] }) {
  const {
    payload: [betId]
  } = action

  try {
    const response = yield fetchUrl(GET_BET_LOG_URL(betId), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(getBetsLogRequestActions.successAction(betId, response.result))
  } catch (error) {
    yield put(getBetsLogRequestActions.failureAction(betId))
  }
}
