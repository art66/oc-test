import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { refundBetRequestActions, allowBetRequestActions, suspendBetRequestActions, updateBetAction } from '../actions'
import { REFUND_BET_URL, ALLOW_BET_URL, SUSPEND_BET_URL } from './constants'

export function* refundBetRequestSaga(action: { payload: [string] }) {
  try {
    const betId = action.payload[0]

    const response = yield fetchUrl(REFUND_BET_URL(betId), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(updateBetAction(response.bet))

    yield put(refundBetRequestActions.successAction())
  } catch (error) {
    console.error(error)

    yield put(refundBetRequestActions.failureAction())
  }
}

export function* allowBetRequestSaga(action: { payload: [string] }) {
  try {
    const betId = action.payload[0]

    const response = yield fetchUrl(ALLOW_BET_URL(betId), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(updateBetAction(response.bet))

    yield put(allowBetRequestActions.successAction())
  } catch (error) {
    console.error(error)

    yield put(allowBetRequestActions.failureAction())
  }
}

export function* suspendBetRequestSaga(action: { payload: [string] }) {
  try {
    const betId = action.payload[0]

    const response = yield fetchUrl(SUSPEND_BET_URL(betId), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(updateBetAction(response.bet))

    yield put(suspendBetRequestActions.successAction())
  } catch (error) {
    console.error(error)

    yield put(suspendBetRequestActions.failureAction())
  }
}
