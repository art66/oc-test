export function normalizeUsersSearch(props) {
  if (!props) return ''

  const { userIdOrPlayerName, ...otherProps } = props

  const queryParamsString = userIdOrPlayerName ? `&search=${userIdOrPlayerName}` : ''
  const otherPropsParamsString = `&${new URLSearchParams(otherProps).toString()}`

  return `${queryParamsString}${otherPropsParamsString}`
}

export function normalizeBetsSearch(props) {
  if (!props) return ''

  const { playername, userId, ...otherProps } = props

  const queryParamsString = playername ? `&search=${playername}` : ''
  const userIdQueryParamsString = userId ? `&sUserId=${userId}` : ''
  const otherPropsParamsString = `&${new URLSearchParams(otherProps).toString()}`

  return `${userIdQueryParamsString}${queryParamsString}${otherPropsParamsString}`
}

export function normalizeSuspiciousActions(props) {
  if (!props) return ''

  const { userIdOrPlayerName, ...otherProps } = props

  const queryParamsString = userIdOrPlayerName ? `&search=${userIdOrPlayerName}` : ''
  const otherPropsParamsString = `&${new URLSearchParams(otherProps).toString()}`

  return `${queryParamsString}${otherPropsParamsString}`
}

export function normalizeEventsSearch(props) {
  if (!props) return ''

  const { nameOrCompetition, userId, ...otherProps } = props

  const userIdQueryParamsString = userId ? `&sUserId=${userId}` : ''
  const nameOrCompetitionQueryParamsString = nameOrCompetition ? `&search=${nameOrCompetition}` : ''
  const otherPropsParamsString = `&${new URLSearchParams(otherProps).toString()}`

  return `${userIdQueryParamsString}${nameOrCompetitionQueryParamsString}${otherPropsParamsString}`
}
