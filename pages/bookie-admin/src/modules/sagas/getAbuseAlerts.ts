import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  getAbuseAlertsRequestActions,
  setAbuseAlertsOrderRequestActions,
  abuseAlertsLoadMoreRequestActions
} from '../actions'
import { GET_SUSPICIOUS_ACTIONS_URL } from './constants'
import { getAbuseAlertsOrderBy, getAbuseAlertsReverse, getAbuseAlertsNextPage } from '../selectors'
import { getSearchQuery } from './utils'
import { ABUSE_ALERTS_SEARCH_QUERY_KEYS_LIST } from '../../constants'

export default function* getAbuseAlertsRequestHandlerSaga() {
  try {
    const search = getSearchQuery(ABUSE_ALERTS_SEARCH_QUERY_KEYS_LIST)
    const orderBy = yield select(getAbuseAlertsOrderBy)
    const order = yield select(getAbuseAlertsReverse)
    const page = yield select(getAbuseAlertsNextPage)

    const response = yield fetchUrl(GET_SUSPICIOUS_ACTIONS_URL({ search, orderBy, order, page }), null, null)

    if (response.error) {
      throw response.error
    }

    return response.suspicions
  } catch (error) {
    console.error(error)

    throw error
  }
}

export function* getAbuseAlertsRequestSaga() {
  try {
    const abuseAlerts = yield getAbuseAlertsRequestHandlerSaga()

    yield put(getAbuseAlertsRequestActions.successAction(abuseAlerts))
  } catch (error) {
    yield put(getAbuseAlertsRequestActions.failureAction())
  }
}

export function* setAbuseAlertsOrderRequestSaga() {
  try {
    const abuseAlerts = yield getAbuseAlertsRequestHandlerSaga()

    yield put(setAbuseAlertsOrderRequestActions.successAction(abuseAlerts))
  } catch (error) {
    yield put(setAbuseAlertsOrderRequestActions.failureAction())
  }
}

export function* abuseAlertsLoadMoreRequestSaga() {
  try {
    const abuseAlerts = yield getAbuseAlertsRequestHandlerSaga()

    yield put(abuseAlertsLoadMoreRequestActions.successAction(abuseAlerts))
  } catch (error) {
    yield put(abuseAlertsLoadMoreRequestActions.failureAction())
  }
}
