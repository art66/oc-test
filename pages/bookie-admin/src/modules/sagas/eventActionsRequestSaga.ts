import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  refundEventRequestActions,
  allowEventRequestActions,
  suspendEventRequestActions,
  updateEventAction
} from '../actions'
import { REFUND_EVENT_URL, ALLOW_EVENT_URL, SUSPEND_EVENT_URL } from './constants'

export function* refundEventRequestSaga(action: { payload: [string] }) {
  try {
    const eventId = action.payload[0]

    const response = yield fetchUrl(REFUND_EVENT_URL(eventId), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(updateEventAction(response.event))

    yield put(refundEventRequestActions.successAction())
  } catch (error) {
    console.error(error)

    yield put(refundEventRequestActions.failureAction())
  }
}

export function* allowEventRequestSaga(action: { payload: [string] }) {
  try {
    const eventId = action.payload[0]

    const response = yield fetchUrl(ALLOW_EVENT_URL(eventId), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(updateEventAction(response.event))

    yield put(allowEventRequestActions.successAction())
  } catch (error) {
    console.error(error)

    yield put(allowEventRequestActions.failureAction())
  }
}

export function* suspendEventRequestSaga(action: { payload: [string] }) {
  try {
    const eventId = action.payload[0]

    const response = yield fetchUrl(SUSPEND_EVENT_URL(eventId), null, null)

    if (response.error) {
      throw response.error
    }

    yield put(updateEventAction(response.event))

    yield put(suspendEventRequestActions.successAction())
  } catch (error) {
    console.error(error)

    yield put(suspendEventRequestActions.failureAction())
  }
}
