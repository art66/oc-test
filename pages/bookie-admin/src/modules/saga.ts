import { takeLatest, debounce } from 'redux-saga/effects'
import {
  getOverallRequestActions,
  getBookmakersRequestActions,
  getUsersStatsRequestActions,
  setUsersStatsOrderRequestActions,
  usersStatsLoadMoreRequestActions,
  getBetsRequestActions,
  setBetsOrderRequestActions,
  betsLoadMoreRequestActions,
  getAbuseAlertsRequestActions,
  setAbuseAlertsOrderRequestActions,
  abuseAlertsLoadMoreRequestActions,
  getEventsRequestActions,
  setEventsOrderRequestActions,
  eventsLoadMoreRequestActions,
  getBetsLogRequestActions,
  suspendBetRequestActions,
  refundBetRequestActions,
  allowBetRequestActions,
  suspendEventRequestActions,
  allowEventRequestActions,
  refundEventRequestActions,
  fixBetResultRequestActions
} from './actions'
import getOverallRequestSaga from './sagas/getOverallRequestSaga'
import getBookmakersRequestSaga from './sagas/getBookmakersRequestSaga'
import {
  getUserStatsRequestSaga,
  setUsersStatsOrderRequestSaga,
  usersStatsLoadMoreRequestSaga
} from './sagas/getUsersStats'
import { getBetsRequestSaga, setBetsOrderRequestSaga, betsLoadMoreRequestSaga } from './sagas/getBetsStats'
import {
  getAbuseAlertsRequestSaga,
  setAbuseAlertsOrderRequestSaga,
  abuseAlertsLoadMoreRequestSaga
} from './sagas/getAbuseAlerts'
import { eventsLoadMoreRequestSaga, getEventsRequestSaga, setEventsOrderRequestSaga } from './sagas/getEvents'
import getBetsLogRequestSaga from './sagas/getBetLog'
import { allowBetRequestSaga, refundBetRequestSaga, suspendBetRequestSaga } from './sagas/betActionsRequestSaga'
import { allowEventRequestSaga, refundEventRequestSaga, suspendEventRequestSaga } from './sagas/eventActionsRequestSaga'
import changeBetResultSaga from './sagas/changeBetResultSaga'

export default function* rootSaga() {
  yield takeLatest(getOverallRequestActions.INITIAL_TYPE, getOverallRequestSaga)
  yield takeLatest(getBookmakersRequestActions.INITIAL_TYPE, getBookmakersRequestSaga)

  yield takeLatest(getUsersStatsRequestActions.INITIAL_TYPE, getUserStatsRequestSaga)
  yield takeLatest(setUsersStatsOrderRequestActions.INITIAL_TYPE, setUsersStatsOrderRequestSaga)
  yield debounce(1000, usersStatsLoadMoreRequestActions.INITIAL_TYPE, usersStatsLoadMoreRequestSaga)

  yield takeLatest(getBetsRequestActions.INITIAL_TYPE, getBetsRequestSaga)
  yield takeLatest(setBetsOrderRequestActions.INITIAL_TYPE, setBetsOrderRequestSaga)
  yield debounce(1000, betsLoadMoreRequestActions.INITIAL_TYPE, betsLoadMoreRequestSaga)

  yield takeLatest(getAbuseAlertsRequestActions.INITIAL_TYPE, getAbuseAlertsRequestSaga)
  yield takeLatest(setAbuseAlertsOrderRequestActions.INITIAL_TYPE, setAbuseAlertsOrderRequestSaga)
  yield debounce(1000, abuseAlertsLoadMoreRequestActions.INITIAL_TYPE, abuseAlertsLoadMoreRequestSaga)

  yield takeLatest(getEventsRequestActions.INITIAL_TYPE, getEventsRequestSaga)
  yield takeLatest(setEventsOrderRequestActions.INITIAL_TYPE, setEventsOrderRequestSaga)
  yield debounce(1000, eventsLoadMoreRequestActions.INITIAL_TYPE, eventsLoadMoreRequestSaga)

  // @ts-ignore
  yield takeLatest(getBetsLogRequestActions.INITIAL_TYPE, getBetsLogRequestSaga)
  // @ts-ignore
  yield takeLatest(suspendBetRequestActions.INITIAL_TYPE, suspendBetRequestSaga)
  // @ts-ignore
  yield takeLatest(allowBetRequestActions.INITIAL_TYPE, allowBetRequestSaga)
  // @ts-ignore
  yield takeLatest(refundBetRequestActions.INITIAL_TYPE, refundBetRequestSaga)
  // @ts-ignore
  yield takeLatest(suspendEventRequestActions.INITIAL_TYPE, suspendEventRequestSaga)
  // @ts-ignore
  yield takeLatest(allowEventRequestActions.INITIAL_TYPE, allowEventRequestSaga)
  // @ts-ignore
  yield takeLatest(refundEventRequestActions.INITIAL_TYPE, refundEventRequestSaga)

  // @ts-ignore
  yield takeLatest(fixBetResultRequestActions.INITIAL_TYPE, changeBetResultSaga)
}
