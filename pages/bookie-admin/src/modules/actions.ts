import { createAction, createRequestActions } from './utils'

export const getOverallRequestActions = createRequestActions('GET_OVERALL_REQUEST')

export const getBookmakersRequestActions = createRequestActions('GET_BOOKMAKERS_REQUEST')

export const getUsersStatsRequestActions = createRequestActions('GET_USERS_STATS_REQUEST')
export const setUsersStatsOrderRequestActions = createRequestActions('SET_USERS_STATS_ORDER_REQUEST')
export const usersStatsLoadMoreRequestActions = createRequestActions('USERS_STATS_LOADING_MORE_REQUEST')

export const getBetsRequestActions = createRequestActions('GET_BETS_REQUEST')
export const setBetsOrderRequestActions = createRequestActions('SET_BETS_ORDER_REQUEST')
export const betsLoadMoreRequestActions = createRequestActions('BETS_LOADING_MORE_REQUEST')
export const updateBetAction = createAction('UPDATE_BET')
export const suspendBetRequestActions = createRequestActions('SUSPEND_BET_REQUEST')
export const allowBetRequestActions = createRequestActions('ALLOW_BET_REQUEST')
export const refundBetRequestActions = createRequestActions('REFUND_BET_REQUEST')

export const getAbuseAlertsRequestActions = createRequestActions('GET_INAPPROPRIATE_ACTIONS_REQUEST')
export const setAbuseAlertsOrderRequestActions = createRequestActions('SET_INAPPROPRIATE_ACTIONS_ORDER_REQUEST')
export const abuseAlertsLoadMoreRequestActions = createRequestActions('INAPPROPRIATE_ACTIONS_LOADING_MORE_REQUEST')

export const getEventsRequestActions = createRequestActions('GET_EVENTS_REQUEST')
export const setEventsOrderRequestActions = createRequestActions('SET_EVENTS_ORDER_REQUEST')
export const eventsLoadMoreRequestActions = createRequestActions('EVENTS_LOADING_MORE_REQUEST')

export const getBetsLogRequestActions = createRequestActions('GET_BETS_LOG_REQUEST')
export const updateEventAction = createAction('UPDATE_EVENT')
export const suspendEventRequestActions = createRequestActions('EVENT_SUSPEND_REQUEST')
export const allowEventRequestActions = createRequestActions('EVENT_ALLOW_REQUEST')
export const refundEventRequestActions = createRequestActions('EVENT_REFUND_REQUEST')

export const fixBetResultRequestActions = createRequestActions('FIX_BET_RESULT_REQUEST')
