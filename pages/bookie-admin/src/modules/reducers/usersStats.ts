import { PayloadAction } from '@reduxjs/toolkit'
import { TUsersStats, TUsersStatsList, TReverse } from '../interfaces'
import {
  getUsersStatsRequestActions,
  setUsersStatsOrderRequestActions,
  usersStatsLoadMoreRequestActions
} from '../actions'
import { createReducer } from '../utils'

export const initialState: TUsersStats = {
  data: [],
  page: 0,
  orderBy: null,
  order: false,
  isLoading: false,
  error: false,
  isSearchLoading: false,
  searchError: false,
  isOrderLoading: false,
  orderError: false,
  isMoreLoading: false,
  moreError: false
}

const ACTION_HANDLERS = {
  [getUsersStatsRequestActions.INITIAL_TYPE]: (state: TUsersStats) => {
    state.page = 0
    state.isLoading = true
    state.error = false
  },
  [getUsersStatsRequestActions.SUCCESS_TYPE]: (state: TUsersStats, action: PayloadAction<[TUsersStatsList]>) => {
    const [list] = action.payload

    state.data = list
    state.isLoading = false
  },
  [getUsersStatsRequestActions.FAILURE_TYPE]: (state: TUsersStats) => {
    state.isLoading = false
    state.error = true
  },
  [setUsersStatsOrderRequestActions.INITIAL_TYPE]: (state: TUsersStats, action: PayloadAction<[string, TReverse]>) => {
    const [orderBy, order] = action.payload

    state.orderBy = orderBy
    state.order = order
    state.page = 0
    state.isOrderLoading = true
    state.orderError = false
  },
  [setUsersStatsOrderRequestActions.SUCCESS_TYPE]: (state: TUsersStats, action: PayloadAction<[TUsersStatsList]>) => {
    const [list] = action.payload

    state.data = list
    state.isOrderLoading = false
  },
  [setUsersStatsOrderRequestActions.FAILURE_TYPE]: (state: TUsersStats) => {
    state.isOrderLoading = false
    state.orderError = true
  },
  [usersStatsLoadMoreRequestActions.INITIAL_TYPE]: (state: TUsersStats) => {
    state.page += 1
    state.isMoreLoading = true
    state.moreError = false
  },
  [usersStatsLoadMoreRequestActions.SUCCESS_TYPE]: (state: TUsersStats, action: PayloadAction<[TUsersStatsList]>) => {
    const [list] = action.payload

    state.data = state.data.concat(list)
    state.page += list.length ? 0 : -1
    state.isMoreLoading = false
  },
  [usersStatsLoadMoreRequestActions.FAILURE_TYPE]: (state: TUsersStats) => {
    state.isMoreLoading = false
    state.moreError = true
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
