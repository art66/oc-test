import { combineReducers } from '@reduxjs/toolkit'
import overall, { initialState as overallInitialState } from './overall'
import bookmakers, { initialState as bookmakersInitialState } from './bookmakers'
import usersStats, { initialState as usersStatsInitialState } from './usersStats'
import bets, { initialState as betsInitialState } from './bets'
import abuseAlerts, { initialState as abuseAlertsInitialState } from './abuseAlerts'
import events, { initialState as eventsInitialState } from './events'
import { TCommonState } from '../interfaces'

export const commonInitialState: TCommonState = {
  overall: overallInitialState,
  bookmakers: bookmakersInitialState,
  usersStats: usersStatsInitialState,
  bets: betsInitialState,
  abuseAlerts: abuseAlertsInitialState,
  events: eventsInitialState
}

export default combineReducers({ overall, bookmakers, usersStats, bets, abuseAlerts, events })
