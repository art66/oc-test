import { PayloadAction } from '@reduxjs/toolkit'
import { TAbuseAlerts, TAbuseAlertsList, TReverse } from '../interfaces'
import {
  getAbuseAlertsRequestActions,
  setAbuseAlertsOrderRequestActions,
  abuseAlertsLoadMoreRequestActions
} from '../actions'
import { createReducer } from '../utils'

export const initialState: TAbuseAlerts = {
  data: [],
  page: 0,
  orderBy: null,
  order: false,
  isLoading: false,
  error: false,
  isSearchLoading: false,
  searchError: false,
  isOrderLoading: false,
  orderError: false,
  isMoreLoading: false,
  moreError: false
}

const ACTION_HANDLERS = {
  [getAbuseAlertsRequestActions.INITIAL_TYPE]: (state: TAbuseAlerts) => {
    state.page = 0
    state.isLoading = true
    state.error = false
  },
  [getAbuseAlertsRequestActions.SUCCESS_TYPE]: (state: TAbuseAlerts, action: PayloadAction<[TAbuseAlertsList]>) => {
    const [list] = action.payload

    state.data = list
    state.isLoading = false
  },
  [getAbuseAlertsRequestActions.FAILURE_TYPE]: (state: TAbuseAlerts) => {
    state.isLoading = false
    state.error = true
  },
  [setAbuseAlertsOrderRequestActions.INITIAL_TYPE]: (
    state: TAbuseAlerts,
    action: PayloadAction<[string, TReverse]>
  ) => {
    const [orderBy, order] = action.payload

    state.orderBy = orderBy
    state.order = order
    state.page = 0
    state.isOrderLoading = true
    state.orderError = false
  },
  [setAbuseAlertsOrderRequestActions.SUCCESS_TYPE]: (
    state: TAbuseAlerts,
    action: PayloadAction<[TAbuseAlertsList]>
  ) => {
    const [list] = action.payload

    state.data = list
    state.isOrderLoading = false
  },
  [setAbuseAlertsOrderRequestActions.FAILURE_TYPE]: (state: TAbuseAlerts) => {
    state.isOrderLoading = false
    state.orderError = true
  },
  [abuseAlertsLoadMoreRequestActions.INITIAL_TYPE]: (state: TAbuseAlerts) => {
    state.page += 1
    state.isMoreLoading = true
    state.moreError = false
  },
  [abuseAlertsLoadMoreRequestActions.SUCCESS_TYPE]: (
    state: TAbuseAlerts,
    action: PayloadAction<[TAbuseAlertsList]>
  ) => {
    const [list] = action.payload

    state.data = state.data.concat(list)
    state.page += list.length ? 0 : -1
    state.isMoreLoading = false
  },
  [abuseAlertsLoadMoreRequestActions.FAILURE_TYPE]: (state: TAbuseAlerts) => {
    state.isMoreLoading = false
    state.moreError = true
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
