import { PayloadAction } from '@reduxjs/toolkit'
import { TEvents, TEvent, TEventsList, TReverse } from '../interfaces'
import {
  getEventsRequestActions,
  setEventsOrderRequestActions,
  eventsLoadMoreRequestActions,
  updateEventAction
} from '../actions'
import { createReducer } from '../utils'

export const initialState: TEvents = {
  data: [],
  page: 0,
  orderBy: null,
  order: false,
  isLoading: false,
  error: false,
  isSearchLoading: false,
  searchError: false,
  isOrderLoading: false,
  orderError: false,
  isMoreLoading: false,
  moreError: false
}

const ACTION_HANDLERS = {
  [getEventsRequestActions.INITIAL_TYPE]: (state: TEvents) => {
    state.page = 0
    state.isLoading = true
    state.error = false
  },
  [getEventsRequestActions.SUCCESS_TYPE]: (state: TEvents, action: PayloadAction<[TEventsList]>) => {
    const [list] = action.payload

    state.data = list
    state.isLoading = false
  },
  [getEventsRequestActions.FAILURE_TYPE]: (state: TEvents) => {
    state.isLoading = false
    state.error = true
  },
  [updateEventAction.type]: (state: TEvents, action: PayloadAction<[TEvent]>) => {
    const [nextEvent] = action.payload

    const eventIndex = state.data.findIndex(event => nextEvent.id === event.id)

    state.data.splice(eventIndex, 1, nextEvent)
  },
  [setEventsOrderRequestActions.INITIAL_TYPE]: (state: TEvents, action: PayloadAction<[string, TReverse]>) => {
    const [orderBy, order] = action.payload

    state.orderBy = orderBy
    state.order = order
    state.page = 0
    state.isOrderLoading = true
    state.orderError = false
  },
  [setEventsOrderRequestActions.SUCCESS_TYPE]: (state: TEvents, action: PayloadAction<[TEventsList]>) => {
    const [list] = action.payload

    state.data = list
    state.isOrderLoading = false
  },
  [setEventsOrderRequestActions.FAILURE_TYPE]: (state: TEvents) => {
    state.isOrderLoading = false
    state.orderError = true
  },
  [eventsLoadMoreRequestActions.INITIAL_TYPE]: (state: TEvents) => {
    state.page += 1
    state.isMoreLoading = true
    state.moreError = false
  },
  [eventsLoadMoreRequestActions.SUCCESS_TYPE]: (state: TEvents, action: PayloadAction<[TEventsList]>) => {
    const [list] = action.payload

    state.data = state.data.concat(list)
    state.page += list.length ? 0 : -1
    state.isMoreLoading = false
  },
  [eventsLoadMoreRequestActions.FAILURE_TYPE]: (state: TEvents) => {
    state.isMoreLoading = false
    state.moreError = true
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
