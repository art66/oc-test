import { PayloadAction } from '@reduxjs/toolkit'
import { TBookmakers, TBookmakersData } from '../interfaces'
import { getBookmakersRequestActions } from '../actions'
import { createReducer } from '../utils'

export const initialState: TBookmakers = {
  data: [],
  isLoading: true,
  error: false
}

const ACTION_HANDLERS = {
  [getBookmakersRequestActions.INITIAL_TYPE]: (state: TBookmakers) => {
    state.isLoading = true
    state.error = false
  },
  [getBookmakersRequestActions.SUCCESS_TYPE]: (state: TBookmakers, action: PayloadAction<[TBookmakersData]>) => {
    const [data] = action.payload

    state.data = data
    state.isLoading = false
  },
  [getBookmakersRequestActions.FAILURE_TYPE]: (state: TBookmakers) => {
    state.isLoading = false
    state.error = true
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
