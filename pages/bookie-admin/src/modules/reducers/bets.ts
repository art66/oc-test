import { PayloadAction } from '@reduxjs/toolkit'
import { TBet, TBets, TBetsList, TReverse } from '../interfaces'
import {
  getBetsRequestActions,
  setBetsOrderRequestActions,
  betsLoadMoreRequestActions,
  getBetsLogRequestActions,
  updateBetAction,
  fixBetResultRequestActions
} from '../actions'
import { createReducer } from '../utils'

export const initialState: TBets = {
  data: [],
  page: 0,
  orderBy: null,
  order: false,
  isLoading: false,
  error: false,
  isSearchLoading: false,
  searchError: false,
  isOrderLoading: false,
  orderError: false,
  isMoreLoading: false,
  moreError: false
}

const ACTION_HANDLERS = {
  [getBetsRequestActions.INITIAL_TYPE]: (state: TBets) => {
    state.page = 0
    state.isLoading = true
    state.error = false
  },
  [getBetsRequestActions.SUCCESS_TYPE]: (state: TBets, action: PayloadAction<[TBetsList]>) => {
    const [list] = action.payload

    state.data = list
    state.isLoading = false
  },
  [getBetsRequestActions.FAILURE_TYPE]: (state: TBets) => {
    state.isLoading = false
    state.error = true
  },
  [updateBetAction.type]: (state: TBets, action: PayloadAction<[TBet]>) => {
    const [nextBet] = action.payload

    const betIndex = state.data.findIndex(bet => nextBet.id === bet.id)

    state.data.splice(betIndex, 1, nextBet)
  },
  [setBetsOrderRequestActions.INITIAL_TYPE]: (state: TBets, action: PayloadAction<[string, TReverse]>) => {
    const [orderBy, order] = action.payload

    state.orderBy = orderBy
    state.order = order
    state.page = 0
    state.isOrderLoading = true
    state.orderError = false
  },
  [setBetsOrderRequestActions.SUCCESS_TYPE]: (state: TBets, action: PayloadAction<[TBetsList]>) => {
    const [list] = action.payload

    state.data = list
    state.isOrderLoading = false
  },
  [setBetsOrderRequestActions.FAILURE_TYPE]: (state: TBets) => {
    state.isOrderLoading = false
    state.orderError = true
  },
  [getBetsLogRequestActions.INITIAL_TYPE]: (state: TBets, action: PayloadAction<[string]>) => {
    const [betId] = action.payload

    state.data = betLogRequestHelper(state.data, betId, {
      isLoading: true,
      error: null
    })
  },
  [getBetsLogRequestActions.SUCCESS_TYPE]: (state: TBets, action: PayloadAction<[string, string[]]>) => {
    const [betId, logData] = action.payload

    state.data = betLogRequestHelper(state.data, betId, {
      logData,
      isLoading: false,
      error: null
    })
  },
  [getBetsLogRequestActions.FAILURE_TYPE]: (state: TBets, action: PayloadAction<{ betId: string; error: string }>) => {
    const { betId, error } = action.payload

    state.data = betLogRequestHelper(state.data, betId, {
      isLoading: false,
      error
    })
  },
  [betsLoadMoreRequestActions.INITIAL_TYPE]: (state: TBets) => {
    state.page += 1
    state.isMoreLoading = true
    state.moreError = false
  },
  [betsLoadMoreRequestActions.SUCCESS_TYPE]: (state: TBets, action: PayloadAction<[TBetsList]>) => {
    const [list] = action.payload

    state.data = state.data.concat(list)
    state.page += list.length ? 0 : -1
    state.isMoreLoading = false
  },
  [betsLoadMoreRequestActions.FAILURE_TYPE]: (state: TBets) => {
    state.isMoreLoading = false
    state.moreError = true
  },
  [fixBetResultRequestActions.INITIAL_TYPE]: (state: TBets, action: PayloadAction<[string, string]>) => {
    const [betId] = action.payload

    state.data = updateBetHelper(state.data, betId, {
      isResultLoading: true,
      resultError: null
    })
  },
  [fixBetResultRequestActions.SUCCESS_TYPE]: (state: TBets, action: PayloadAction<[string, string]>) => {
    const [betId, result] = action.payload

    state.data = updateBetHelper(state.data, betId, {
      isResultLoading: false,
      result
    })
  },
  [fixBetResultRequestActions.FAILURE_TYPE]: (state: TBets, action: PayloadAction<[string, string]>) => {
    const [betId, error] = action.payload

    state.data = updateBetHelper(state.data, betId, {
      isResultLoading: false,
      resultError: error
    })
  }
}

export default createReducer(initialState, ACTION_HANDLERS)

function betLogRequestHelper(list: TBetsList, betId: string, data) {
  return list.map(bet => {
    if (bet.id === betId) {
      return {
        ...bet,
        details: {
          ...bet.details,
          log: {
            ...bet.details.log,
            ...data
          }
        }
      }
    }

    return bet
  })
}

function updateBetHelper(list, betId, props) {
  return list.map(bet => {
    if (bet.id === betId) {
      return {
        ...bet,
        details: {
          ...bet.details,
          ...props
        }
      }
    }

    return bet
  })
}
