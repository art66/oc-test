import { PayloadAction } from '@reduxjs/toolkit'
import { TOverallData, TOverall } from '../interfaces'
import { getOverallRequestActions } from '../actions'
import { createReducer } from '../utils'

export const initialState: TOverall = {
  data: {
    bets: null,
    betsLost: null,
    betsRefunded: null,
    betsWon: null,
    moneyBet: null,
    moneyLost: null,
    moneyRefunded: null,
    moneyWon: null,
    onhold: null,
    balance: null,
    ratio: null
  },
  isLoading: true,
  error: false
}

const ACTION_HANDLERS = {
  [getOverallRequestActions.INITIAL_TYPE]: (state: TOverall) => {
    state.isLoading = true
    state.error = false
  },
  [getOverallRequestActions.SUCCESS_TYPE]: (state: TOverall, action: PayloadAction<[TOverallData]>) => {
    const [data] = action.payload

    state.data = data
    state.isLoading = false
  },
  [getOverallRequestActions.FAILURE_TYPE]: (state: TOverall) => {
    state.isLoading = false
    state.error = true
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
