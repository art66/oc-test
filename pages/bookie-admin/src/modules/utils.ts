import * as reduxToolkit from '@reduxjs/toolkit'

export const createAction = (type: string, resolve?: reduxToolkit.PrepareAction<any>) => {
  const action = reduxToolkit.createAction(
    type,
    resolve
      || ((...payload) => {
        return {
          payload
        }
      })
  )

  return action
}

export const createReducer = (initialState, handlers) => reduxToolkit.createReducer(initialState, handlers)

export const createRequestActions = type => {
  const initialAction = createAction(type)
  const successAction = createAction(`${type}_SUCCESS`)
  const failureAction = createAction(`${type}_FAILURE`)

  return {
    INITIAL_TYPE: initialAction.type,
    SUCCESS_TYPE: successAction.type,
    FAILURE_TYPE: failureAction.type,
    initialAction,
    successAction,
    failureAction
  }
}
