import { createSelector } from 'reselect'
import { IState } from '../store/types'
import {
  TOverallData,
  TIsLoading,
  TBookmakersData,
  TUsersStatsList,
  TReverse,
  TUsersStats,
  TBets,
  TBetsList,
  TAbuseAlerts,
  TAbuseAlertsList,
  TEvents,
  TEventsList
} from './interfaces'

export const getOverall = (state: IState): TOverallData => state.common.overall.data
export const getOverallIsLoading = (state: IState): TIsLoading => state.common.overall.isLoading

export const getBookmakers = (state: IState): TBookmakersData => state.common.bookmakers.data
export const getBookmakersIsLoading = (state: IState): TIsLoading => state.common.bookmakers.isLoading

export const getUsersStats = (state: IState): TUsersStats => state.common.usersStats
export const getUsersStatsList = (state: IState): TUsersStatsList => state.common.usersStats.data
export const getUsersStatsIsLoading = (state: IState): TIsLoading => state.common.usersStats.isLoading
export const getUsersStatsOrderBy = (state: IState): string => state.common.usersStats.orderBy
export const getUsersStatsReverse = (state: IState): TReverse => state.common.usersStats.order
export const getUsersStatsIsMoreLoading = (state: IState): TIsLoading => state.common.usersStats.isMoreLoading
export const getUsersStatsNextPage = (state: IState): number => state.common.usersStats.page

export const getUsersStatsIsLoadingAtAll = createSelector(
  [getUsersStats],
  ({ isLoading, isSearchLoading, isOrderLoading, isMoreLoading }): TIsLoading => {
    return isLoading || isSearchLoading || isOrderLoading || isMoreLoading
  }
)

export const getBets = (state: IState): TBets => state.common.bets
export const getBetsList = (state: IState): TBetsList => state.common.bets.data
export const getBetsIsLoading = (state: IState): TIsLoading => state.common.bets.isLoading
export const getBetsOrderBy = (state: IState): string => state.common.bets.orderBy
export const getBetsReverse = (state: IState): TReverse => state.common.bets.order
export const getBetsIsMoreLoading = (state: IState): TIsLoading => state.common.bets.isMoreLoading
export const getBetsNextPage = (state: IState): number => state.common.bets.page

export const getBetsIsLoadingAtAll = createSelector(
  [getBets],
  ({ isLoading, isSearchLoading, isOrderLoading, isMoreLoading }): TIsLoading => {
    return isLoading || isSearchLoading || isOrderLoading || isMoreLoading
  }
)

export const getAbuseAlerts = (state: IState): TAbuseAlerts => state.common.abuseAlerts
export const getAbuseAlertsList = (state: IState): TAbuseAlertsList => state.common.abuseAlerts.data
export const getAbuseAlertsIsLoading = (state: IState): TIsLoading => state.common.abuseAlerts.isLoading
export const getAbuseAlertsOrderBy = (state: IState): string => state.common.abuseAlerts.orderBy
export const getAbuseAlertsReverse = (state: IState): TReverse => state.common.abuseAlerts.order
export const getAbuseAlertsIsMoreLoading = (state: IState): TIsLoading => state.common.abuseAlerts.isMoreLoading
export const getAbuseAlertsNextPage = (state: IState): number => state.common.abuseAlerts.page

export const getAbuseAlertsIsLoadingAtAll = createSelector(
  [getAbuseAlerts],
  ({ isLoading, isSearchLoading, isOrderLoading, isMoreLoading }): TIsLoading => {
    return isLoading || isSearchLoading || isOrderLoading || isMoreLoading
  }
)

export const getEvents = (state: IState): TEvents => state.common.events
export const getEventsList = (state: IState): TEventsList => state.common.events.data
export const getEventsIsLoading = (state: IState): TIsLoading => state.common.events.isLoading
export const getEventsOrderBy = (state: IState): string => state.common.events.orderBy
export const getEventsReverse = (state: IState): TReverse => state.common.events.order
export const getEventsIsMoreLoading = (state: IState): TIsLoading => state.common.events.isMoreLoading
export const getEventsNextPage = (state: IState): number => state.common.events.page

export const getEventsIsLoadingAtAll = createSelector(
  [getEvents],
  ({ isLoading, isSearchLoading, isOrderLoading, isMoreLoading }): TIsLoading => {
    return isLoading || isSearchLoading || isOrderLoading || isMoreLoading
  }
)
