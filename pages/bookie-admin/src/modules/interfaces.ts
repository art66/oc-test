import {
  RESULT_INPROGRESS_TYPE,
  RESULT_NOTSTARTED_TYPE,
  RESULT_PENDING_TYPE,
  RESULT_WON_TYPE,
  RESULT_LOST_TYPE,
  RESULT_REFUNDED_TYPE
} from '../constants'
import { ORDER_ASC, ORDER_DESC } from './constants'

export type TIsLoading = boolean
export type TLoaded = boolean
export type TError = boolean

export type TId = string
export type TName = string

export type TOrder = typeof ORDER_ASC | typeof ORDER_DESC

export type TReverse = boolean

export type TOverallData = {
  balance: number
  bets: number
  betsLost: number
  betsRefunded: number
  betsWon: number
  moneyBet: number
  moneyLost: number
  moneyRefunded: number
  moneyWon: number
  onhold: boolean
  ratio: number
}

export type TOverall = {
  data: TOverallData
  isLoading: TIsLoading
  error: TLoaded
}

export type TBookmaker = {
  name: TName
  bets: number
  betsPending: number
  betsLost: number
  betsWon: number
  betsRefunded: number
  moneyBet: number
  moneyLost: number
  moneyWon: number
  moneyRefunded: number
}

export type TBookmakersData = TBookmaker[]

export type TBookmakers = {
  data: TBookmakersData
  isLoading: TIsLoading
  error: TLoaded
}

export interface IUserStatsDetails {
  id: TId
  playername: TName
  totalBets: number
  netProfit: number
  moneyWonLostRatio: number
  biggestMoneyWon: number
  biggestMoneyLost: number
  bestStrike: number
  currentStrike: number
  suspicions: string
  overall: {
    bets: string
    betsWon: string
    betsLost: string
    betsPending: string
    betsRefunded: string
    moneyBet: string
    moneyWon: string
    moneyLost: string
    moneyRefunded: string
  }
  byProvider: {
    name: string
    bets: string
    betsWon: string
    betsLost: string
    betsPending: string
    betsRefunded: string
    moneyBet: string
    moneyWon: string
    moneyLost: string
    moneyRefunded: string
  }[]
  bySport: {
    name: string
    bets: string
    betsWon: string
    betsLost: string
    betsPending: string
    betsRefunded: string
    moneyBet: string
    moneyWon: string
    moneyLost: string
    moneyRefunded: string
  }[]
}

export type TUserStats = {
  id: TId
  playername: TName
  totalBets: number
  totalBetsWon: number
  totalBetsLost: number
  totalBetsRefunded: number
  totalMoneyBet: number
  totalMoneyWon: number
  totalMoneyLost: number
  totalMoneyRefunded: number
  biggestMoneyWon: number
  biggestMoneyLost: number
  bestStrike: number
  currentStrike: number
  netProfit: number
  moneyWonLostRatio: number
  details: IUserStatsDetails
}

export type TUsersStatsList = TUserStats[]

export type TUsersStats = {
  data: TUsersStatsList
  page: number
  orderBy: string
  order: TReverse
  isLoading: TIsLoading
  error: TError
  isSearchLoading: TIsLoading
  searchError: TError
  isOrderLoading: TIsLoading
  orderError: TError
  isMoreLoading: TIsLoading
  moreError: TError
}

export type TBetDetails = {
  id: TId
  userID: string
  playername: string
  outcomeId: string
  bettingofferId: string
  name: string
  amount: number
  odds: number
  odds_provider: string
  result: TEventResult
  withdrawn: boolean
  eventName: string
  eventId: string
  timestamp: string
  handicap: number
  description: string
  event: {
    competition: string
    icon: string
    startdate: string
    status: string
    statusDesc: string
    dateStarted: string
    dateEnded: string
  }
  results: []
  isResultLoading: boolean
  log: {
    logData: { date: string; message: string }[]
    isLoading: boolean
    error?: string
  }
}

export type TBet = {
  id: TId
  userID: string
  playername: string
  name: string
  eventName: string
  eventId: string
  outcomeId: string
  bettingofferId: string
  odds_provider: string
  handicap: number
  odds: number
  amount: number
  status_type: string
  timestamp: string
  withdrawn: boolean
  description: string
  details: TBetDetails
  result: TEventResult
  control: {
    suspended: boolean
    refunded: boolean
  }
}

export type TBetsList = TBet[]

export type TBets = {
  data: TBetsList
  page: number
  orderBy: string
  order: TReverse
  isLoading: TIsLoading
  error: TError
  isSearchLoading: TIsLoading
  searchError: TError
  isOrderLoading: TIsLoading
  orderError: TError
  isMoreLoading: TIsLoading
  moreError: TError
}

export type TInappropriateAction = {
  id: TId
  userID: string
  playername: string
  eventId: string
  bettingofferId: string
  text: string
  reason: string
  timestamp: string
}

export type TAbuseAlertsList = TInappropriateAction[]

export type TAbuseAlerts = {
  data: TAbuseAlertsList
  page: number
  orderBy: string
  order: TReverse
  isLoading: TIsLoading
  error: TError
  isSearchLoading: TIsLoading
  searchError: TError
  isOrderLoading: TIsLoading
  orderError: TError
  isMoreLoading: TIsLoading
  moreError: TError
}

export type TEventResult =
  | typeof RESULT_PENDING_TYPE
  | typeof RESULT_NOTSTARTED_TYPE
  | typeof RESULT_INPROGRESS_TYPE
  | typeof RESULT_WON_TYPE
  | typeof RESULT_LOST_TYPE
  | typeof RESULT_REFUNDED_TYPE

export interface TEventDetails {
  bets: number
  amount: number
  possibleGain: number
  name: string
  id: TId
  icon: string
  competition: string
  startdate: string
  status: TEventResult
  statusDesc: string
  dateStarted: string
  dateEnded: string
  results: any
}

export type TEvent = {
  id: TId
  sportID: number
  sport: string
  name: string
  stage: string
  template: string
  tournament: string
  competition: string
  countryCode: string
  gender: string
  startdate: string
  alias: string
  amount: number
  possibleGain: number
  bets: number
  status: TEventResult
  details?: any
  control: {
    suspended: boolean
    refunded: boolean
  }
}

export type TEventsList = TEvent[]

export type TCellState = {
  canSuspend: boolean
  canAllow: boolean
  canRefund: boolean
  canRefunded: boolean
}

export type TEvents = {
  data: TEventsList
  page: number
  orderBy: string
  order: TReverse
  isLoading: TIsLoading
  error: TError
  isSearchLoading: TIsLoading
  searchError: TError
  isOrderLoading: TIsLoading
  orderError: TError
  isMoreLoading: TIsLoading
  moreError: TError
}

export type TCommonState = {
  overall: TOverall
  bookmakers: TBookmakers
  usersStats: TUsersStats
  bets: TBets
  abuseAlerts: TAbuseAlerts
  events: TEvents
}
