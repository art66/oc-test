import { useCallback } from 'react'
import { useLocation, useHistory } from 'react-router'
import { createPath } from './utils'

export function UseQuery() {
  return new URLSearchParams(useLocation().search)
}

export function UrlHook(paramsKeysList: string[]) {
  const history = useHistory()
  const query = UseQuery()

  const search = paramsKeysList.reduce((searchData, paramKey) => {
    if (query.has(paramKey)) {
      searchData[paramKey] = query.get(paramKey)
    }

    return searchData
  }, {}) as { [key: string]: string }
  const queryDetailsRowIdValue = query.get('detailsRowId')
  const params = {
    ...search,
    detailsRowId: queryDetailsRowIdValue
  }

  const setParam = useCallback(
    (paramName, value) => {
      history.push(createPath(query, { ...params, [paramName]: value }))
    },
    [query, params]
  )

  return {
    search,
    params,
    setParam
  }
}

export function UseKeyEnter(callback) {
  return useCallback(
    event => {
      if (event.key === 'Enter') {
        callback()
      }
    },
    [callback]
  )
}
