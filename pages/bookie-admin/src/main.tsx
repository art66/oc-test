import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import { Provider } from 'react-redux'
import { HashRouter as Router } from 'react-router-dom'
import AppContainer from './containers/AppContainer'
import rootStore from './store/createStore'
import { ConfirmProvider } from './components/Confirm'

const MOUNT_NODE = document.getElementById('react-root')

// ========================================================
// Render Setup
// ========================================================

const Root = props => (
  <Router>
    <Provider store={rootStore}>
      <ConfirmProvider>
        <AppContainer {...props} />
      </ConfirmProvider>
    </Provider>
  </Router>
)

const render = () => {
  ReactDOM.render(<Root store={rootStore} history={history} />, MOUNT_NODE)
}

const renderError = error => {
  ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
}

// This code is excluded from production bundle
// @ts-ignore
if (__DEV__) {
  // importing real sidebar app bundle only for developing stage
  // import('../../sidebar/src/main')
  // adding Redux store for live accessing in the browser console
  // @ts-ignore
  window.store = rootStore
  // ========================================================
  // DEVELOPMENT STAGE! HOT MODULE REPLACE ACTIVATION!
  // ========================================================
  const devRender = () => {
    if (module.hot) {
      module.hot.accept('./containers/AppContainer', () => render())
    }

    render()
  }

  // Wrap render in try/catch
  try {
    devRender()
  } catch (error) {
    console.error(error)
    renderError(error)
  }
} else {
  // ========================================================
  // PRODUCTION GO!
  // ========================================================
  render()
}
