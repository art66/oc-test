import { createSelector } from 'reselect'
import { RESULT_PENDING_TYPE } from '../../constants'
import { getBetsList } from '../../modules/selectors'
import { TBet, TBetsList } from '../../modules/interfaces'
import { IState } from '../../store/types'
import { TNormalizedBetsList } from './types'

export const getBetState = createSelector<
TBet,
TBet,
{
  canSuspend: boolean
  canAllow: boolean
  canRefund: boolean
  canRefunded: boolean
}
>([bet => bet], bet => {
  if (!bet.control.suspended && bet.result !== RESULT_PENDING_TYPE) {
    return {
      canSuspend: false,
      canAllow: false,
      canRefund: false,
      canRefunded: false
    }
  }

  const canSuspend = bet.result === RESULT_PENDING_TYPE && !bet.control.suspended && !bet.control.refunded
  const canAllow = bet.control.suspended
  const canRefund = bet.control.suspended
  const canRefunded = bet.control.refunded

  return {
    canSuspend,
    canAllow,
    canRefund,
    canRefunded
  }
})

export const getNormalizedBets = createSelector<IState, TBetsList, TNormalizedBetsList>([getBetsList], betsList => {
  const normalizedBetsList = betsList.map(bet => {
    const betState = getBetState(bet)

    return {
      ...bet,
      ...betState
    }
  })

  return normalizedBetsList
})
