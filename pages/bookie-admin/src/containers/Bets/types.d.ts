import { TBet, TCellState } from '../../modules/interfaces'

export type TProps = {
  className?: string
}

export type THeaderCellConfig = {
  id: string
  name: string
  search?: boolean
  width?: number
  contentAsHtml?: boolean
}

export type THeaderCellsConfig = THeaderCellConfig[]

export interface TNormalizedBet extends TBet, TCellState {}

export type TNormalizedBetsList = TNormalizedBet[]
