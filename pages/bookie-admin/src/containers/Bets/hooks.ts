import { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { getBetsRequestActions, setBetsOrderRequestActions } from '../../modules/actions'
import { UseConfirm } from '../../components/Confirm'
import { ALLOW_BET_TYPE, REFUND_BET_TYPE, SUSPEND_BET_TYPE } from '../../components/confirms/constants'

export function SearchHook() {
  const dispatch = useDispatch()

  const onSearch = useCallback(() => dispatch(getBetsRequestActions.initialAction()), [])
  const onSearchUserId = useCallback(() => dispatch(getBetsRequestActions.initialAction()), [])
  const onSearchBetId = useCallback(() => dispatch(getBetsRequestActions.initialAction()), [])
  const onSearchEventId = useCallback(() => dispatch(getBetsRequestActions.initialAction()), [])
  const onSetOrder = useCallback((id, order) => dispatch(setBetsOrderRequestActions.initialAction(id, order)), [])

  return {
    onSearch,
    onSearchUserId,
    onSearchBetId,
    onSearchEventId,
    onSetOrder
  }
}

export function UseButtonsHandlers() {
  const confirm = UseConfirm()

  const suspend = useCallback(
    (payload: { id: string; playername: string; description: string }) => () => {
      confirm.showView({ viewKey: SUSPEND_BET_TYPE, payload })
    },
    []
  )
  const allow = useCallback(
    (payload: { id: string; playername: string; description: string }) => () => {
      confirm.showView({ viewKey: ALLOW_BET_TYPE, payload })
    },
    []
  )
  const refund = useCallback(
    (payload: { id: string; playername: string; description: string }) => () => {
      confirm.showView({ viewKey: REFUND_BET_TYPE, payload })
    },
    []
  )

  return {
    suspend,
    allow,
    refund
  }
}
