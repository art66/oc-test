import { TCellsConfig } from '../../components/StatsTable/types'

export const DEFAULT_WIDTH = 30

export const EXCLUDE_CELLS = ['details', 'control']

export const HEADER_CELLS_CONFIG: TCellsConfig = {
  id: {
    id: 'betId',
    name: 'ID',
    width: DEFAULT_WIDTH
  },
  userID: {
    id: 'userID',
    name: 'User ID',
    width: 60
  },
  playername: {
    id: 'playername',
    name: 'Player name',
    width: 90
  },
  eventId: {
    id: 'eventId',
    name: 'Event id',
    width: 80
  },
  outcomeId: {
    id: 'outcomeId',
    name: 'Outcome id',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  bettingofferId: {
    id: 'bettingofferId',
    name: 'Bettingoffer id',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  odds_provider: {
    id: 'odds_provider',
    name: 'Odds provider',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  handicap: {
    id: 'handicap',
    name: 'Handicap',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  odds: {
    id: 'odds',
    name: 'Odds',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  amount: {
    id: 'amount',
    name: 'Amount',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  status_type: {
    id: 'status_type',
    name: 'Status type',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  result: {
    id: 'result',
    name: 'Result',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  withdrawn: {
    id: 'withdrawn',
    name: 'Withdrawn',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  timestamp: {
    id: 'timestamp',
    name: 'Timestamp',
    width: 130,
    rotated: true
  },
  description: {
    id: 'description',
    name: 'Description',
    width: 300,
    contentAsHtml: true
  }
}
