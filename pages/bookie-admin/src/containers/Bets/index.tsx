import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  setBetsOrderRequestActions,
  betsLoadMoreRequestActions,
  getBetsRequestActions,
  getBetsLogRequestActions
} from '../../modules/actions'
import { getBetsIsLoadingAtAll } from '../../modules/selectors'
import { EXCLUDE_CELLS, HEADER_CELLS_CONFIG } from './constants'
import StatsTable from '../../components/StatsTable'
import { Search } from '../../components/Search'
import BetsDetails from '../../components/BetsDetails'
import { UrlHook } from '../../hooks'
import { getNormalizedBets } from './selectors'
import Row from '../../components/Row'
import { AllowButton, RefundButton, RefundedButton, SuspendButton } from '../../components/buttons'
import { UseButtonsHandlers } from './hooks'
import { BETS_SEARCH_QUERY_KEYS_LIST } from '../../constants'

export function Bets() {
  const dispatch = useDispatch()

  const normalizedBets = useSelector(getNormalizedBets)
  const isLoading = useSelector(getBetsIsLoadingAtAll)

  const url = UrlHook(BETS_SEARCH_QUERY_KEYS_LIST)

  const load = useCallback(() => {
    dispatch(getBetsRequestActions.initialAction())
  }, [])
  const loadMore = useCallback(() => dispatch(betsLoadMoreRequestActions.initialAction()), [])
  const onSearch = useCallback(
    key => search => {
      url.setParam(key, search)

      dispatch(getBetsRequestActions.initialAction())
    },
    [url.setParam]
  )
  const onSetOrder = useCallback((id, order) => dispatch(setBetsOrderRequestActions.initialAction(id, order)), [])
  const onClick = useCallback(
    nextDetailsRowId => {
      url.setParam('detailsRowId', url.params.detailsRowId !== nextDetailsRowId ? nextDetailsRowId : '')
    },
    [url.setParam, url.params.detailsRowId]
  )

  const detailsRowIndex = normalizedBets.findIndex(item => item.id === url.params.detailsRowId)

  const buttonsHandlers = UseButtonsHandlers()

  const searchView = (
    <>
      <Search onChange={onSearch('playername')} defaultValue={url.search.playername}>
        <span>
          Search by <b>playername</b>
        </span>
      </Search>
      <Search width={80} onChange={onSearch('userId')} defaultValue={url.search.userId}>
        <b>userID</b>
      </Search>
      <Search width={80} onChange={onSearch('betId')} defaultValue={url.search.betId}>
        <b>betId</b>
      </Search>
      <Search width={80} onChange={onSearch('eventId')} defaultValue={url.search.eventId}>
        <b>eventId</b>
      </Search>
    </>
  )

  return (
    <StatsTable
      cellsConfig={HEADER_CELLS_CONFIG}
      excludeCells={EXCLUDE_CELLS}
      list={normalizedBets}
      isLoading={isLoading}
      headerHeight={85}
      onLoad={load}
      onLoadMore={loadMore}
      onOrder={onSetOrder}
      searchContent={searchView}
      detailsRowIndex={detailsRowIndex}
    >
      {normalizedBets.map(data => {
        const { id, playername, description } = data
        const payload = { id, playername, description }
        const detailsView = url.params?.detailsRowId === data.id && (
          <BetsDetails
            {...data.details}
            onShowLog={() => {
              dispatch(getBetsLogRequestActions.initialAction(data.id))
            }}
          />
        )

        return (
          <Row
            key={id}
            cellsConfig={HEADER_CELLS_CONFIG}
            excludeCells={EXCLUDE_CELLS}
            data={data}
            onClick={onClick}
            detailsView={detailsView}
          >
            {data.canSuspend && <SuspendButton onClick={buttonsHandlers.suspend(payload)} />}
            {data.canAllow && <AllowButton onClick={buttonsHandlers.allow(payload)} />}
            {data.canRefund && <RefundButton onClick={buttonsHandlers.refund(payload)} />}
            {data.canRefunded && <RefundedButton />}
          </Row>
        )
      })}
    </StatsTable>
  )
}

export default Bets
