import React, { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { TProps } from './types'
import { getBookmakersRequestActions } from '../../modules/actions'
import { getBookmakers } from '../../modules/selectors'
import styles from './index.cssmodule.scss'

export function Bookmakers(props: TProps) {
  const { className } = props

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getBookmakersRequestActions.initialAction())
  }, [])

  const bookmakers = useSelector(getBookmakers)

  const colSpan = bookmakers.length + 1

  const boomakersBodyCellsView = useMemo(() => {
    if (!(bookmakers && bookmakers[0])) {
      return null
    }

    return bookmakers.map(bookmaker => {
      return (
        <tr key={bookmaker.name}>
          <td>{bookmaker.name}</td>
          <td>{bookmaker.bets}</td>
          <td>{bookmaker.betsWon}</td>
          <td>{bookmaker.betsLost}</td>
          <td>{bookmaker.betsPending}</td>
          <td>{bookmaker.betsRefunded}</td>
          <td>{bookmaker.moneyBet}</td>
          <td>{bookmaker.moneyWon}</td>
          <td>{bookmaker.moneyLost}</td>
          <td>{bookmaker.moneyRefunded}</td>
        </tr>
      )
    })
  }, [bookmakers])

  return (
    <table className={cn(styles.table, className)}>
      <thead>
        <tr>
          <th colSpan={colSpan}>Bookmakers</th>
          <th colSpan={colSpan} aria-label='Blank' />
        </tr>
        <tr>
          <th aria-label='Blank' />
          <th colSpan={5} style={{ textAlign: 'center' }}>
            Bets
          </th>
          <th colSpan={4} style={{ textAlign: 'center' }}>
            Money
          </th>
        </tr>
        <tr>
          <th>Provider</th>
          <th>Total</th>
          <th>Won</th>
          <th>Lost</th>
          <th>Pending</th>
          <th>Refunded</th>
          <th>Bet</th>
          <th>Won</th>
          <th>Lost</th>
          <th>Refunded</th>
        </tr>
      </thead>
      <tbody>{boomakersBodyCellsView}</tbody>
    </table>
  )
}

export default Bookmakers
