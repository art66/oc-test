export const PROPS_ALLIAS = {
  bets: 'Bets',
  betsPending: 'Bets Pending',
  betsLost: 'Bets Lost',
  betsWon: 'Bets Won',
  betsRefunded: 'Bets Refunded',
  moneyBet: 'Money Bet',
  moneyLost: 'Money Lost',
  moneyWon: 'Money Won',
  moneyRefunded: 'Money Refunded'
}
