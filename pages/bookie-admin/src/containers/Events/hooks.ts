import { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { UseConfirm } from '../../components/Confirm'
import { SUSPEND_EVENT_TYPE, ALLOW_EVENT_TYPE, REFUND_EVENT_TYPE } from '../../components/confirms/constants'

export function UseButtonsHandlers() {
  const dispatch = useDispatch()

  const confirm = UseConfirm()

  const suspend = useCallback(
    (payload: { id: string; sport: string; name: string }) => () => {
      confirm.showView({ viewKey: SUSPEND_EVENT_TYPE, payload })
    },
    [dispatch]
  )
  const allow = useCallback(
    (payload: { id: string; sport: string; name: string }) => () => {
      confirm.showView({ viewKey: ALLOW_EVENT_TYPE, payload })
    },
    [dispatch]
  )
  const refund = useCallback(
    (payload: { id: string; sport: string; name: string }) => () => {
      confirm.showView({ viewKey: REFUND_EVENT_TYPE, payload })
    },
    [dispatch]
  )

  return {
    suspend,
    allow,
    refund
  }
}
