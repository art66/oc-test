import { createSelector } from 'reselect'
import { RESULT_INPROGRESS_TYPE, RESULT_NOTSTARTED_TYPE } from '../../constants'
import { getEventsList } from '../../modules/selectors'
import { TEvent, TEventsList } from '../../modules/interfaces'
import { IState } from '../../store/types'
import { TNormalizedEventsList } from './types'

export const getEventState = createSelector<
TEvent,
TEvent,
{
  canSuspend: boolean
  canAllow: boolean
  canRefund: boolean
  canRefunded: boolean
}
>([event => event], event => {
  if (!event.control.suspended && event.status !== RESULT_NOTSTARTED_TYPE && event.status !== RESULT_INPROGRESS_TYPE) {
    return {
      canSuspend: false,
      canAllow: false,
      canRefund: false,
      canRefunded: false
    }
  }

  const canSuspend =
    (event.status === RESULT_NOTSTARTED_TYPE || event.status === RESULT_INPROGRESS_TYPE)
    && !event.control.suspended
    && !event.control.refunded
  const canAllow = event.control.suspended
  const canRefund = event.control.suspended
  const canRefunded = event.control.refunded

  return {
    canSuspend,
    canAllow,
    canRefund,
    canRefunded
  }
})

export const getNormalizedEvents = createSelector<IState, TEventsList, TNormalizedEventsList>(
  [getEventsList],
  eventsList => {
    const normalizedEventsList = eventsList.map(event => {
      const eventState = getEventState(event)

      return {
        ...event,
        ...eventState
      }
    })

    return normalizedEventsList
  }
)
