import React, { useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  setEventsOrderRequestActions,
  eventsLoadMoreRequestActions,
  getEventsRequestActions
} from '../../modules/actions'
import { getEventsIsLoadingAtAll } from '../../modules/selectors'
import { EXCLUDE_CELLS, HEADER_CELLS_CONFIG } from './constants'
import StatsTable from '../../components/StatsTable'
import { Search } from '../../components/Search'
import EventDetails from '../../components/EventDetails'
import { UrlHook } from '../../hooks'
import { getNormalizedEvents } from './selectors'
import Row from '../../components/Row'
import { AllowButton, RefundButton, RefundedButton, SuspendButton } from '../../components/buttons'
import { UseButtonsHandlers } from './hooks'
import { EVENTS_SEARCH_QUERY_KEYS_LIST } from '../../constants'

export function Events() {
  const dispatch = useDispatch()

  const url = UrlHook(EVENTS_SEARCH_QUERY_KEYS_LIST)

  const normalizedEvents = useSelector(getNormalizedEvents)
  const isLoading = useSelector(getEventsIsLoadingAtAll)

  const load = useCallback(() => dispatch(getEventsRequestActions.initialAction()), [])
  const loadMore = useCallback(() => dispatch(eventsLoadMoreRequestActions.initialAction()), [])
  const onSearch = useCallback(
    key => search => {
      url.setParam(key, search)

      dispatch(getEventsRequestActions.initialAction({ [key]: search }))
    },
    [url.setParam]
  )
  const onSetOrder = useCallback((id, order) => dispatch(setEventsOrderRequestActions.initialAction(id, order)), [])
  const onClick = useCallback(
    nextDetailsRowId => {
      url.setParam('detailsRowId', url.params.detailsRowId !== nextDetailsRowId ? nextDetailsRowId : '')
    },
    [url.setParam, url.params.detailsRowId]
  )

  const buttonsHandlers = UseButtonsHandlers()

  const bodyCellsView = useMemo(() => {
    return normalizedEvents.map(data => {
      const { id, name, sport } = data
      const payload = { id, name, sport }
      const detailsView = url.params?.detailsRowId === data.id && <EventDetails key={data.id} {...data.details} />

      return (
        <Row
          key={data.id}
          cellsConfig={HEADER_CELLS_CONFIG}
          excludeCells={EXCLUDE_CELLS}
          data={data}
          detailsView={detailsView}
          onClick={onClick}
        >
          {data.canSuspend && <SuspendButton onClick={buttonsHandlers.suspend(payload)} />}
          {data.canAllow && <AllowButton onClick={buttonsHandlers.allow(payload)} />}
          {data.canRefund && <RefundButton onClick={buttonsHandlers.refund(payload)} />}
          {data.canRefunded && <RefundedButton />}
        </Row>
      )
    })
  }, [normalizedEvents, url.params?.detailsRowId, onClick])

  return (
    <StatsTable
      cellsConfig={HEADER_CELLS_CONFIG}
      excludeCells={EXCLUDE_CELLS}
      list={normalizedEvents}
      isLoading={isLoading}
      headerHeight={80}
      onLoad={load}
      onLoadMore={loadMore}
      onOrder={onSetOrder}
      searchContent={
        <>
          <Search defaultValue={url.search.eventId} onChange={onSearch('eventId')}>
            <span>
              Search by <b>eventID</b>
            </span>
          </Search>
          <Search defaultValue={url.search.nameOrCompetition} onChange={onSearch('nameOrCompetition')}>
            <span>
              Search by <b>name</b> or <b>competition</b>
            </span>
          </Search>
          <Search defaultValue={url.search.userId} onChange={onSearch('userId')}>
            <span>
              Search by <b>userID</b>
            </span>
          </Search>
        </>
      }
    >
      {bodyCellsView}
    </StatsTable>
  )
}

export default Events
