import { TCellsConfig } from '../../components/StatsTable/types'

export const DEFAULT_WIDTH = 30

export const EXCLUDE_CELLS = ['details', 'control', 'icon']

export const HEADER_CELLS_CONFIG: TCellsConfig = {
  eventId: {
    id: 'eventId',
    name: 'ID',
    width: DEFAULT_WIDTH
  },
  sportID: {
    id: 'sportID',
    name: 'Sport ID',
    rotated: true
  },
  sport: {
    id: 'sport',
    name: 'Sport',
    rotated: true
  },
  name: {
    id: 'name',
    name: 'Name',
    rotated: true
  },
  stage: {
    id: 'stage',
    name: 'Stage',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  template: {
    id: 'template',
    name: 'Template',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  tournament: {
    id: 'tournament',
    name: 'Tournament',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  competition: {
    id: 'competition',
    name: 'Competition',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  countryCode: {
    id: 'countryCode',
    name: 'Country code',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  gender: {
    id: 'gender',
    name: 'Gender',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  status: {
    id: 'status',
    name: 'Status',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  startdate: {
    id: 'startdate',
    name: 'Startdate',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  alias: {
    id: 'alias',
    name: 'Alias',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  amount: {
    id: 'amount',
    name: 'Amount',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  possibleGain: {
    id: 'possibleGain',
    name: 'Possible gain',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  bets: {
    id: 'bets',
    name: 'Bets',
    width: DEFAULT_WIDTH,
    rotated: true
  }
}
