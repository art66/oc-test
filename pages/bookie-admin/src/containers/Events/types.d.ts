import { TCellState, TEvent } from '../../modules/interfaces'

export type THeaderCellConfig = {
  id: string
  name: string
  search?: boolean
  width?: number
}

export type THeaderCellsConfig = THeaderCellConfig[]

export type TBodyCellConfig = {
  html?: boolean
}

export type TBodyCellsConfig = {
  [key: string]: TBodyCellConfig
}

export interface TNormalizedEvent extends TEvent, TCellState {}

export type TNormalizedEventsList = TNormalizedEvent[]
