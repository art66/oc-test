import { TCellsConfig } from '../../components/StatsTable/types'

export const DEFAULT_WIDTH = 30

export const HEADER_CELLS_CONFIG: TCellsConfig = {
  suspicionId: {
    id: 'suspicionId',
    name: 'ID',
    width: DEFAULT_WIDTH
  },
  userID: {
    id: 'userID',
    name: 'User ID',
    width: 90
  },
  playername: {
    id: 'playername',
    name: 'Player name',
    width: 110
  },
  eventId: {
    id: 'eventId',
    name: 'Event id',
    width: 80
  },
  bettingofferId: {
    id: 'bettingofferId',
    name: 'Bettingoffer id',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  text: {
    id: 'text',
    name: 'Text message',
    width: 150,
    contentAsHtml: true
  },
  reason: {
    id: 'reason',
    name: 'Reason',
    width: DEFAULT_WIDTH
  },
  timestamp: {
    id: 'timestamp',
    name: 'Timestamp',
    width: DEFAULT_WIDTH
  }
}
