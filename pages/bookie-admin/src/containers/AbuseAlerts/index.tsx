import React, { useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  setAbuseAlertsOrderRequestActions,
  abuseAlertsLoadMoreRequestActions,
  getAbuseAlertsRequestActions
} from '../../modules/actions'
import { getAbuseAlertsList, getAbuseAlertsIsLoadingAtAll } from '../../modules/selectors'
import { HEADER_CELLS_CONFIG } from './constants'
import StatsTable from '../../components/StatsTable'
import { Search } from '../../components/Search'
import { UrlHook } from '../../hooks'
import Row from '../../components/Row'
import { ABUSE_ALERTS_SEARCH_QUERY_KEYS_LIST } from '../../constants'

export function AbuseAlerts() {
  const dispatch = useDispatch()

  const abuseAlertsList = useSelector(getAbuseAlertsList)
  const isLoading = useSelector(getAbuseAlertsIsLoadingAtAll)

  const url = UrlHook(ABUSE_ALERTS_SEARCH_QUERY_KEYS_LIST)

  const load = useCallback(() => dispatch(getAbuseAlertsRequestActions.initialAction()), [])
  const loadMore = useCallback(() => dispatch(abuseAlertsLoadMoreRequestActions.initialAction()), [])
  const onSearch = useCallback(
    key => search => {
      url.setParam(key, search)

      dispatch(getAbuseAlertsRequestActions.initialAction({ [key]: search }))
    },
    [url.setParam]
  )
  const onSetOrder = useCallback(
    (id, order) => dispatch(setAbuseAlertsOrderRequestActions.initialAction(id, order)),
    []
  )

  const detailsRowIndex = abuseAlertsList.findIndex(item => item.id === url.params.detailsRowId)
  const bodyCellsView = useMemo(() => {
    return abuseAlertsList.map(data => {
      return <Row key={data.id} cellsConfig={HEADER_CELLS_CONFIG} data={data} />
    })
  }, [abuseAlertsList])

  return (
    <StatsTable
      cellsConfig={HEADER_CELLS_CONFIG}
      list={abuseAlertsList}
      isLoading={isLoading}
      headerHeight={80}
      onLoad={load}
      onLoadMore={loadMore}
      onOrder={onSetOrder}
      searchContent={
        <Search onChange={onSearch('userIdOrPlayerName')}>
          <span>
            Search by <b>userID</b> or <b>playername</b>
          </span>
        </Search>
      }
      detailsRowIndex={detailsRowIndex}
    >
      {bodyCellsView}
    </StatsTable>
  )
}

export default AbuseAlerts
