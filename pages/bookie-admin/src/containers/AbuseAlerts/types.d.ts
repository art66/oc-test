export type THeaderCellConfig = {
  id: string
  name: string
  search?: boolean
  width?: number
  rotated?: boolean
}

export type THeaderCellsConfig = THeaderCellConfig[]

export type TBodyCellConfig = {
  html?: boolean
}

export type TBodyCellsConfig = {
  [key: string]: TBodyCellConfig
}
