import React, { useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  setUsersStatsOrderRequestActions,
  usersStatsLoadMoreRequestActions,
  getUsersStatsRequestActions
} from '../../modules/actions'
import { getUsersStatsList, getUsersStatsIsLoadingAtAll } from '../../modules/selectors'
import { EXCLUDE_CELLS, HEADER_CELLS_CONFIG } from './constants'
import StatsTable from '../../components/StatsTable'
import { Search } from '../../components/Search'
import { UserStatsTableDetails } from '../../components/UserStatsTableDetails'
import { UrlHook } from '../../hooks'
import Row from '../../components/Row'
import { USER_SEARCH_QUERY_KEYS_LIST } from '../../constants'

export default function UsersStats() {
  const dispatch = useDispatch()

  const usersStatsList = useSelector(getUsersStatsList)
  const isLoading = useSelector(getUsersStatsIsLoadingAtAll)

  const url = UrlHook(USER_SEARCH_QUERY_KEYS_LIST)

  const onSearch = useCallback(
    key => search => {
      url.setParam(key, search)

      dispatch(getUsersStatsRequestActions.initialAction())
    },
    [url.setParam]
  )
  const load = useCallback(() => dispatch(getUsersStatsRequestActions.initialAction()), [])
  const loadMore = useCallback(() => dispatch(usersStatsLoadMoreRequestActions.initialAction()), [])
  const onSetOrder = useCallback((id, order) => dispatch(setUsersStatsOrderRequestActions.initialAction(id, order)), [])
  const onClick = useCallback(
    nextDetailsRowId => {
      url.setParam('detailsRowId', url.params.detailsRowId !== nextDetailsRowId ? nextDetailsRowId : '')
    },
    [url.setParam, url.params.detailsRowId]
  )

  const bodyCellsView = useMemo(() => {
    return usersStatsList.map(data => {
      const detailsView = url.params?.detailsRowId === data.id && (
        <UserStatsTableDetails key={data.id} {...data.details} />
      )

      return (
        <Row
          key={data.id}
          cellsConfig={HEADER_CELLS_CONFIG}
          excludeCells={EXCLUDE_CELLS}
          data={data}
          onClick={onClick}
          detailsView={detailsView}
        />
      )
    })
  }, [usersStatsList, onClick, url.params?.detailsRowId])

  return (
    <StatsTable
      cellsConfig={HEADER_CELLS_CONFIG}
      excludeCells={EXCLUDE_CELLS}
      list={usersStatsList}
      isLoading={isLoading}
      onLoad={load}
      headerHeight={105}
      onLoadMore={loadMore}
      onOrder={onSetOrder}
      searchContent={
        <Search onChange={onSearch('userIdOrPlayerName')} defaultValue={url.search.userIdOrPlayerName}>
          <span>
            Search by <b>userID</b> or <b>playername</b>
          </span>
        </Search>
      }
    >
      {bodyCellsView}
    </StatsTable>
  )
}
