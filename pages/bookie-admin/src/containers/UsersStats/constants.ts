import { TCellsConfig } from '../../components/StatsTable/types'
import { DEFAULT_WIDTH } from '../../constants'

export const EXCLUDE_CELLS = ['details']

export const HEADER_CELLS_CONFIG: TCellsConfig = {
  id: {
    id: 'userID',
    name: 'User id',
    width: DEFAULT_WIDTH
  },
  playername: {
    id: 'playername',
    name: 'Player name',
    width: 70
  },
  totalBets: {
    id: 'totalBets',
    name: 'Total bets',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  totalBetsWon: {
    id: 'totalBetsWon',
    name: 'Total bets won',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  totalBetsLost: {
    id: 'totalBetsLost',
    name: 'Total bets lost',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  totalMoneyBet: {
    id: 'totalMoneyBet',
    name: 'Total money bet',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  totalMoneyWon: {
    id: 'totalMoneyWon',
    name: 'Total money won',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  totalMoneyLost: {
    id: 'totalMoneyLost',
    name: 'Total money lost',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  biggestMoneyWon: {
    id: 'biggestMoneyWon',
    name: 'Biggest money won',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  biggestMoneyLost: {
    id: 'biggestMoneyLost',
    name: 'Biggest money lost',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  bestStrike: {
    id: 'bestStrike',
    name: 'Best strike',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  currentStrike: {
    id: 'currentStrike',
    name: 'Current strike',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  netProfit: {
    id: 'netProfit',
    name: 'Net profit',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  moneyWonLostRatio: {
    id: 'moneyWonLostRatio',
    name: 'Money won/lost ratio',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  totalMoneyRefunded: {
    id: 'totalMoneyRefunded',
    name: 'Total money refunded',
    width: DEFAULT_WIDTH,
    rotated: true
  },
  totalBetsRefunded: {
    id: 'totalBetsRefunded',
    name: 'Total bets refunded',
    width: DEFAULT_WIDTH,
    rotated: true
  }
}
