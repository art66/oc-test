export const CONFIG = {
  balance: {
    title: 'Balance'
  },
  bets: {
    title: 'Bets'
  },
  betsLost: {
    title: 'Bets lost'
  },
  betsRefunded: {
    title: 'Bets refunded'
  },
  betsWon: {
    title: 'Bets won'
  },
  moneyBet: {
    title: 'Money bet'
  },
  moneyLost: {
    title: 'Money lost'
  },
  moneyRefunded: {
    title: 'Money refunded'
  },
  moneyWon: {
    title: 'Money won'
  },
  ratio: {
    title: 'Ratio'
  },
  onhold: {
    title: 'Bets on hold',
    toString: true
  },
  eventWinThreshold: {
    title: 'Event win threshold'
  }
}
