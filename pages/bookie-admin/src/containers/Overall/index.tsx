import React, { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getOverallRequestActions } from '../../modules/actions'
import { getOverall, getOverallIsLoading } from '../../modules/selectors'
import styles from './index.cssmodule.scss'
import { CONFIG } from './constants'

export function Overall() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getOverallRequestActions.initialAction())
  }, [])

  const overall = useSelector(getOverall)
  const isLoading = useSelector(getOverallIsLoading)

  const listView = useMemo(() => {
    return Object.keys(overall).map(key => {
      const value = overall[key]
      const normalizedTitle = CONFIG[key]?.title || key
      const normalizedValue = CONFIG[key]?.toString ? `${value}` : value

      return (
        <tr key={key}>
          <td>{normalizedTitle}</td>
          <td className={styles.value}>{normalizedValue}</td>
        </tr>
      )
    })
  }, [overall])

  if (isLoading) return null

  return (
    <table className={styles.table}>
      <thead>
        <tr>
          <th colSpan={2}>Overall</th>
        </tr>
        <tr>
          <th style={{ width: 200 }}>Stats</th>
          <th className={styles.value} style={{ minWidth: 200 }}>
            Values
          </th>
        </tr>
      </thead>
      <tbody>{listView}</tbody>
    </table>
  )
}

export default Overall
