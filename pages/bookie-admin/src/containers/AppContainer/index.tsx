import React from 'react'
import { Route, NavLink } from 'react-router-dom'
import Overall from '../Overall'
import Bookmakers from '../Bookmakers'
import UsersStats from '../UsersStats'
import Bets from '../Bets'
import AbuseAlerts from '../AbuseAlerts'
import Events from '../Events'
import styles from './index.cssmodule.scss'
import Confirm from '../../components/Confirm'
import UseConfirmViews from '../../components/confirms/hooks'

export function AppContainer() {
  const confirmViews = UseConfirmViews()

  return (
    <>
      <Overall />
      <Bookmakers className={styles.bookmakers} />
      <nav className={styles.navigation}>
        <NavLink className={styles.link} to='/' activeClassName={styles.activeLink} exact>
          Users
        </NavLink>
        <NavLink className={styles.link} to='/bets' activeClassName={styles.activeLink}>
          Bets
        </NavLink>
        <NavLink className={styles.link} to='/abuse-alerts' activeClassName={styles.activeLink}>
          Abuse alerts
        </NavLink>
        <NavLink className={styles.link} to='/events' activeClassName={styles.activeLink}>
          Events
        </NavLink>
      </nav>
      <div className={styles.container}>
        <Route path='/' exact>
          <UsersStats />
        </Route>
        <Route path='/bets'>
          <Bets />
        </Route>
        <Route path='/abuse-alerts'>
          <AbuseAlerts />
        </Route>
        <Route path='/events'>
          <Events />
        </Route>
        {/* <Redirect from='/' to='/users' /> */}
      </div>
      <Confirm views={confirmViews} />
    </>
  )
}

export default AppContainer
