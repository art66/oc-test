export const slower = (handler: Function, ms = 750) => {
  let timeoutId = null

  return (...args) => {
    try {
      if (typeof timeoutId === 'number') {
        clearTimeout(timeoutId)
      }

      timeoutId = setTimeout(() => {
        handler(...args)
      }, ms)
    } catch (error) {
      console.error(error)
    }
  }
}

export function createPath(query, props?: { [key: string]: string }) {
  return Object.keys(props).reduce((queryString, key) => {
    const value = props[key]
    const valueNormalized = typeof value === 'string' ? value || null : query.get(key)
    const paramString = valueNormalized ? `&${key}=${valueNormalized}` : ''

    return queryString + paramString
  }, '?')
}

export function getQuery() {
  return new URLSearchParams(window.location.hash)
}
