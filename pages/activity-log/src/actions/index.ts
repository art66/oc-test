import { createAction } from 'redux-actions'
import IGroup from '@torn/header/src/interfaces/recentHistory/IGroup'
import IDate from '../interfaces/IDate'
import { TInitPayload, TUpdateDayPayload, TUpdateFilterPayload, TUpdateFormPayload } from './types'
import * as a from './actionTypes'
import IDropdownItem from '../interfaces/IDropdownItem'

export const fetchInitialActivityLogData = createAction(a.FETCH_INITIAL_ACTIVITY_LOG_DATA)
export const setInitialActivityLogData = createAction<TInitPayload, TInitPayload>(
  a.SET_INITIAL_ACTIVITY_LOG_DATA,
  ({ log, startFrom, isStaff, types, categories }) => ({ log, startFrom, isStaff, types, categories })
)
export const loadNextChunk = createAction(a.LOAD_NEXT_CHUNK)
export const changeDMState = createAction<{ enabled: boolean }, boolean>(a.CHANGE_DM_STATE, enabled => ({ enabled }))
export const updateFilter = createAction<TUpdateFilterPayload, string, any>(a.UPDATE_FILTER, (key, value) => ({
  key,
  value
}))
export const setHoveredDay = createAction<{ date: IDate }, IDate>(a.SET_HOVERED_DAY, date => ({ date }))
export const setSelectedDay = createAction<TUpdateDayPayload, string, IDate>(a.SET_SELECTED_DAY, (label, date) => ({
  label,
  date
}))
export const updateSelectedDay = createAction<TUpdateDayPayload, string, IDate>(
  a.UPDATE_SELECTED_DAY,
  (label, date) => ({ label, date })
)
export const updateFormData = createAction<TUpdateFormPayload, string, any>(a.UPDATE_FORM_DATA, (label, data) => ({
  label,
  data
}))
export const updateRequestUrl = createAction(a.UPDATE_REQUEST_URL)
export const setRequestUrl = createAction<{ url: string }, string>(a.SET_REQUEST_URL, url => ({ url }))
export const fetchLogs = createAction(a.FETCH_LOGS)
export const setLogs = createAction<{ log: IGroup[] }, IGroup[]>(a.SET_LOGS, log => ({ log }))
export const setStopFetching = createAction<{ stop: boolean }, boolean>(a.SET_STOP_FETCHING, stop => ({ stop }))
export const toggleView = createAction<{ view: string }, string>(a.TOGGLE_VIEW, view => ({ view }))
export const toggleDatePicker = createAction<{ open: boolean }, boolean>(a.TOGGLE_DATE_PICKER, open => ({ open }))
export const resetState = createAction(a.RESET_STATE)
export const selectDay = createAction<{ day: IDate }, IDate>(a.SELECT_DAY, day => ({ day }))
export const resetSelectedDate = createAction(a.RESET_SELECTED_DATE)
export const selectCategory = createAction<{ value: IDropdownItem }, IDropdownItem>(a.SELECT_CATEGORY, value => ({
  value
}))
export const selectType = createAction<{ value: IDropdownItem[] }, IDropdownItem[]>(a.SELECT_TYPE, value => ({
  value
}))
