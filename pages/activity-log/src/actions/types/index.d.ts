import IGroup from '@torn/header/src/interfaces/recentHistory/IGroup'
import IDropdownItem from '../../interfaces/IDropdownItem'
import IDate from '../../interfaces/IDate'

export type TInitPayload = {
  log: IGroup[]
  startFrom: string
  isStaff: boolean
  types: IDropdownItem[]
  categories: IDropdownItem[]
}

export type TUpdateDayPayload = {
  label: string
  date: IDate
}

export type TUpdateFilterPayload = {
  key: string
  value: any
}

export type TUpdateFormPayload = {
  label: string
  data: any
}
