import { isDMEnabled } from '../utils'
import { DEFAULT_CATEGORY_DROPDOWN_VALUE, SENTENCE_VIEW } from './index'

export const initialState = {
  darkModeEnabled: isDMEnabled(),
  filters: {
    category: DEFAULT_CATEGORY_DROPDOWN_VALUE,
    userID: '',
    searchText: '',
    amountRows: '',
    datePicker: {
      timeFrom: null,
      timeTo: null,
      hovered: null,
      open: false
    }
  },
  formData: {
    userID: '',
    amount: '',
    searchIn: ''
  },
  url: '',
  stopFetching: false,
  view: SENTENCE_VIEW
}

export default initialState
