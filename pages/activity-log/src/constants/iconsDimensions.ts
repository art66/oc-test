export const TITLE_ICONS = {
  width: 15,
  height: 15,
  viewbox: '0 0 15 15'
}

export const ARROW_ICONS = {
  width: 10,
  height: 16,
  viewbox: '0 0 10 16'
}

export const RESET_ICON = {
  width: 16,
  height: 12,
  viewbox: '3 3 18 18'
}

export const CALENDAR_ICON = {
  width: 16,
  height: 18,
  viewbox: '0 0 16 18'
}
