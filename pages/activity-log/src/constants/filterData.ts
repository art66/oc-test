export const CATEGORY_FILTER_DATA = 'category'
export const TYPE_FILTER_DATA = 'type'
export const TIME_TO_FILTER_DATA = 'timeTo'
export const TIME_FROM_FILTER_DATA = 'timeFrom'
export const USER_FILTER_DATA = 'userID'
export const TEXT_FILTER_DATA = 'searchText'
export const ROWS_FILTER_DATA = 'amountRows'
