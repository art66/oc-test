export const CATEGORY_FORM_DATA = 'cat'
export const TYPE_FORM_DATA = 'log'
export const TIME_TO_FORM_DATA = 'to'
export const TIME_FROM_FORM_DATA = 'from'
export const USER_FORM_DATA = 'userID'
export const TEXT_FORM_DATA = 'searchIn'
export const ROWS_FORM_DATA = 'amount'
export const STAFF_FORM_DATA = 'staff'

export const FORM_DATA_ARRAY = [
  STAFF_FORM_DATA,
  CATEGORY_FORM_DATA,
  TYPE_FORM_DATA,
  TIME_TO_FORM_DATA,
  TIME_FROM_FORM_DATA,
  USER_FORM_DATA,
  TEXT_FORM_DATA,
  ROWS_FORM_DATA
]
