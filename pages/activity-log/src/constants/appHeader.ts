export const CLIENT_PROPS = {
  titles: {
    default: {
      ID: 0,
      title: 'Log'
    }
  },
  links: {
    default: {
      ID: 0,
      items: [
        {
          href: 'personalstats.php',
          icon: 'ViewShow',
          label: 'personal-stats',
          title: 'Personal stats'
        },
        {
          href: 'events.php',
          icon: 'Events',
          label: 'events',
          title: 'Events'
        },
        {
          href: 'index.php',
          icon: 'Home',
          label: 'home',
          title: 'Home'
        }
      ]
    }
  }
}

export default CLIENT_PROPS
