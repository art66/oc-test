export const PRESET_TYPE = 'activityLog'
export const DEFAULT_TITLE_BUTTON_PRESET = 'DEFAULT'
export const DISABLED_TITLE_BUTTON_PRESET = 'DISABLED'
export const HOVERED_TITLE_BUTTON_PRESET = 'HOVERED'
export const DEFAULT_CALENDAR_ARROW_PRESET = 'DEFAULT_ARROW'
export const HOVERED_CALENDAR_ARROW_PRESET = 'HOVERED_ARROW'
export const DISABLED_CALENDAR_ARROW_PRESET = 'DISABLED_ARROW'
export const RESET_ICON_PRESET = 'RESET_ICON'
export const CALENDAR_ICON_PRESET = 'CALENDAR_ICON'

export const AMOUNT_DAYS_IN_WEEK = 7
export const AMOUNT_MONTHS_IN_YEAR = 12
export const AMOUNT_ITEMS_IN_MONTH_GRID = 42

export const SENTENCE_VIEW = 'row'
export const TABLE_VIEW = 'table'

export const DEFAULT_TYPE_DROPDOWN_VALUE = {
  ID: -1,
  name: 'All Logs'
}
export const DEFAULT_CATEGORY_DROPDOWN_VALUE = {
  ID: -1,
  name: 'All Categories'
}

export const TABLE_VIEW_BTN_ID = 'table-view-button'

export const TYPES_LIMIT = 10

export const BOTTOM_DATE_PICKER_RESTRICTION = {
  month: 1,
  year: 2017
}
