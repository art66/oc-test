import { takeEvery } from 'redux-saga/effects'
import * as a from '../actions/actionTypes'
import selectDay from './selectDay'
import resetSelectedDate from './resetSelectedDate'
import selectCategory from './selectCategory'
import selectType from './selectType'
import updateSelectedDay from './updateSelectedDay'
import loadNextChunk from './loadNextChunk'
import updateRequestURL from './updateRequestURL'
import fetchInitialActivityLogData from './fetchInitialActivityLogData'
import fetchLogs from './fetchLogs'

export default function* activityLog() {
  yield takeEvery(a.FETCH_INITIAL_ACTIVITY_LOG_DATA, fetchInitialActivityLogData)
  yield takeEvery(a.LOAD_NEXT_CHUNK, loadNextChunk)
  yield takeEvery(a.UPDATE_SELECTED_DAY, updateSelectedDay)
  yield takeEvery(a.UPDATE_REQUEST_URL, updateRequestURL)
  yield takeEvery(a.FETCH_LOGS, fetchLogs)
  yield takeEvery(a.SELECT_DAY, selectDay)
  yield takeEvery(a.RESET_SELECTED_DATE, resetSelectedDate)
  yield takeEvery(a.SELECT_CATEGORY, selectCategory)
  yield takeEvery(a.SELECT_TYPE, selectType)
}
