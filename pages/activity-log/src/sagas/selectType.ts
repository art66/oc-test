import { Action } from 'redux-actions'
import { put, select } from 'redux-saga/effects'
import { fetchLogs, updateFilter, updateFormData } from '../actions'
import { CATEGORY_FILTER_DATA, TYPE_FILTER_DATA } from '../constants/filterData'
import { DEFAULT_CATEGORY_DROPDOWN_VALUE } from '../constants'
import { CATEGORY_FORM_DATA, TYPE_FORM_DATA } from '../constants/formData'
import { getTypesData } from '../utils'
import IDropdownItem from '../interfaces/IDropdownItem'
import { getSelectedCategory } from '../selectors'

export default function* selectType(action: Action<{ value: IDropdownItem[] }>) {
  const { value } = action.payload
  const selectedCategory = yield select(getSelectedCategory)

  yield put(updateFilter(TYPE_FILTER_DATA, value))

  if (selectedCategory?.ID !== -1) {
    yield put(updateFilter(CATEGORY_FILTER_DATA, DEFAULT_CATEGORY_DROPDOWN_VALUE))
    yield put(updateFormData(CATEGORY_FORM_DATA, null))
  }

  const typesData = getTypesData(value)

  yield put(updateFormData(TYPE_FORM_DATA, typesData))
  yield put(fetchLogs())
}
