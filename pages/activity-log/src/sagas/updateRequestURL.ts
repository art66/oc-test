import { put, select } from 'redux-saga/effects'
import { getFormData } from '../selectors'
import { setRequestUrl } from '../actions'

export default function* updateRequestURL(disableReplaceURL?: boolean) {
  const formData = yield select(getFormData)
  const url = Object.entries(formData)
    .map(([key, val]) => (val ? `${key}=${val}` : null))
    .filter(item => item)
    .join('&')
  const urlToSave = url.length ? `&${url}` : url

  yield put(setRequestUrl(urlToSave))

  if (!disableReplaceURL) {
    const newUrl = `?sid=log${urlToSave}`

    if (window.location.search !== newUrl) {
      window.history.pushState(null, null, newUrl)
    }
  }
}
