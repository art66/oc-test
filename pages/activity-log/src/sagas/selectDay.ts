import { Action } from 'redux-actions'
import { put, select } from 'redux-saga/effects'
import IDate from '../interfaces/IDate'
import { getTimeFrom, getTimeFromFormData, getTimeTo, getTimeToFormData } from '../selectors'
import {
  fetchLogs,
  setSelectedDay,
  toggleDatePicker,
  updateFormData,
  updateSelectedDay as updateDatePickerDay
} from '../actions'
import { swapTimeValues } from '../utils'
import { TIME_FROM_FILTER_DATA, TIME_TO_FILTER_DATA } from '../constants/filterData'
import { TIME_FROM_FORM_DATA, TIME_TO_FORM_DATA } from '../constants/formData'

export default function* selectDay(action: Action<{ day: IDate }>) {
  const { day, month, year } = action.payload.day
  const timeFrom = yield select(getTimeFrom)
  const timeTo = yield select(getTimeTo)
  const formDataTimeFrom = yield select(getTimeFromFormData)
  const formDataTimeTo = yield select(getTimeToFormData)

  if (!timeFrom) {
    yield put(updateDatePickerDay(TIME_FROM_FILTER_DATA, { day, month, year }))
  } else if (timeFrom && !timeTo) {
    yield put(updateDatePickerDay(TIME_TO_FILTER_DATA, { day, month, year }))

    const [timeFromTimestamp, timeToTimestamp] = swapTimeValues(timeFrom, { year, month, day })

    if (formDataTimeFrom !== timeFromTimestamp || formDataTimeTo !== timeToTimestamp) {
      yield put(updateFormData(TIME_TO_FORM_DATA, timeToTimestamp))
      yield put(updateFormData(TIME_FROM_FORM_DATA, timeFromTimestamp))
      yield put(fetchLogs())
    }

    yield put(toggleDatePicker(false))
  } else if (timeFrom && timeTo) {
    yield put(updateDatePickerDay(TIME_FROM_FILTER_DATA, { day, month, year }))
    yield put(setSelectedDay(TIME_TO_FILTER_DATA, null))
  }
}
