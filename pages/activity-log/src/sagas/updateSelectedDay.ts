import { Action } from 'redux-actions'
import { put, select } from 'redux-saga/effects'
import { TUpdateDayPayload } from '../actions/types'
import { getTimeFrom } from '../selectors'
import { setSelectedDay } from '../actions'

export default function* updateSelectedDay(action: Action<TUpdateDayPayload>) {
  if (action.payload.date) {
    const {
      date: { year, month, day },
      label
    } = action.payload
    const timeFrom = yield select(getTimeFrom)

    if (label === 'timeTo' && Date.UTC(timeFrom.year, timeFrom.month, timeFrom.day) > Date.UTC(year, month, day)) {
      yield put(setSelectedDay('timeTo', timeFrom))
      yield put(setSelectedDay('timeFrom', { year, month, day }))
    } else {
      yield put(setSelectedDay(label, { year, month, day }))
    }
  }
}
