import { Action } from 'redux-actions'
import { put, select } from 'redux-saga/effects'
import { fetchLogs, updateFilter, updateFormData } from '../actions'
import { CATEGORY_FILTER_DATA, TYPE_FILTER_DATA } from '../constants/filterData'
import { DEFAULT_TYPE_DROPDOWN_VALUE } from '../constants'
import { CATEGORY_FORM_DATA, TYPE_FORM_DATA } from '../constants/formData'
import IDropdownItem from '../interfaces/IDropdownItem'
import { getSelectedType } from '../selectors'

export default function* selectCategory(action: Action<{ value: IDropdownItem }>) {
  const { value } = action.payload
  const selectedType = yield select(getSelectedType)

  yield put(updateFilter(CATEGORY_FILTER_DATA, value))

  if (!(selectedType?.length === 1 && selectedType[0].ID === -1)) {
    yield put(updateFilter(TYPE_FILTER_DATA, [DEFAULT_TYPE_DROPDOWN_VALUE]))
    yield put(updateFormData(TYPE_FORM_DATA, null))
    yield put(updateFormData(CATEGORY_FORM_DATA, null))
  }

  if (value.ID !== -1) {
    yield put(updateFormData(CATEGORY_FORM_DATA, value.ID))
  } else {
    yield put(updateFormData(CATEGORY_FORM_DATA, null))
  }

  yield put(fetchLogs())
}
