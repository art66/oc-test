import LogGroupsGenerator from '@torn/header/src/utils/logGroupsGenerator'
import { fetchUrl } from '@torn/shared/utils'
import { call, put, select } from 'redux-saga/effects'
import { setFetchState, setStartFrom } from '@torn/header/src/actions/recentHistory'
import { setLogs, setStopFetching } from '../actions'
import updateRequestURL from './updateRequestURL'
import { getRequestURL } from '../selectors'

export function* handleSetLogData(data) {
  if (data.log) {
    const logHelper = new LogGroupsGenerator(data.log)
    const log = logHelper.getGroupedLogs()

    yield put(setLogs(log))
  }
}

export default function* fetchLogs() {
  try {
    yield call(updateRequestURL)
    yield put(setFetchState(true))
    yield put(setLogs([]))

    const url = yield select(getRequestURL)
    const data = yield fetchUrl(`/page.php?sid=activityLogUserData${url}`)

    yield call(handleSetLogData, data)

    if (+data.startFrom && !data.log) {
      yield put(setStopFetching(true))
    }

    yield put(setStartFrom(data.startFrom))
  } catch (e) {
    console.error(e)
    yield put(setStopFetching(true))
  }

  yield put(setFetchState(false))
}
