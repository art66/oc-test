import { put, select } from 'redux-saga/effects'
import { fetchLogs, setSelectedDay, toggleDatePicker, updateFormData } from '../actions'
import { TIME_FROM_FILTER_DATA, TIME_TO_FILTER_DATA } from '../constants/filterData'
import { TIME_FROM_FORM_DATA, TIME_TO_FORM_DATA } from '../constants/formData'
import { getTimeFrom, getTimeTo } from '../selectors'

export default function* resetSelectedDate() {
  const timeFrom = yield select(getTimeFrom)
  const timeTo = yield select(getTimeTo)

  if (timeFrom || timeTo) {
    yield put(setSelectedDay(TIME_FROM_FILTER_DATA, null))
    yield put(updateFormData(TIME_FROM_FORM_DATA, null))
    yield put(setSelectedDay(TIME_TO_FILTER_DATA, null))
    yield put(updateFormData(TIME_TO_FORM_DATA, null))
    yield put(fetchLogs())
    yield put(toggleDatePicker(false))
  }
}
