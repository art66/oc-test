import { fetchUrl } from '@torn/shared/utils'
import { call, put, select } from 'redux-saga/effects'
import { getLogsFromLastGroup } from '@torn/header/src/utils/historyHelpers'
import LogGroupsGenerator from '@torn/header/src/utils/logGroupsGenerator'
import { setFetchState, setNextChunk, setStartFrom } from '@torn/header/src/actions/recentHistory'
import IGroup from '@torn/header/src/interfaces/recentHistory/IGroup'
import { setLogs, setStopFetching } from '../actions'
import { getLogsList, getRequestURL, getStartFrom } from '../selectors'

export function* updateNextChunk(history: IGroup[], data) {
  if (history.length) {
    const recordsFromLastGroup = yield call(getLogsFromLastGroup, history)
    const logHelper = new LogGroupsGenerator([...recordsFromLastGroup, ...data.log])
    const log = logHelper.getGroupedLogs()

    yield put(setNextChunk(log, data.startFrom))
  } else {
    const logHelper = new LogGroupsGenerator(data.log)
    const log = logHelper.getGroupedLogs()

    yield put(setLogs(log))
    yield put(setStartFrom(data.startFrom))
  }
}

export default function* loadNextChunk() {
  try {
    const startFrom = yield select(getStartFrom)
    const url = yield select(getRequestURL)

    yield put(setFetchState(true))
    const data = yield fetchUrl(`page.php?sid=activityLogUserData&startFrom=${startFrom}${url}`)
    const history = yield select(getLogsList)

    if (+data.startFrom && data.log && history) {
      yield call(updateNextChunk, history, data)
    }

    if (+data.startFrom && !data.log) {
      yield put(setStopFetching(true))
    }

    yield put(setStartFrom(data.startFrom))
  } catch (e) {
    console.error(e)
    yield put(setStopFetching(true))
  }

  yield put(setFetchState(false))
}
