import { call, put } from 'redux-saga/effects'
import { BOTTOM_DATE_PICKER_RESTRICTION } from '../constants'
import { updateFilter, updateFormData, updateSelectedDay as updateDatePickerDay } from '../actions'
import { convertDateToObject, getNextMonthNum, getTimestamp, isValidDate } from '../utils'
import IWindow from '../interfaces/IWindow'
import * as formDataConst from '../constants/formData'
import * as filterDataConst from '../constants/filterData'

declare const window: IWindow

export function* handleUpdateCategoryFormData(urlParams: URLSearchParams) {
  const category = urlParams.get(formDataConst.CATEGORY_FORM_DATA)

  yield put(updateFormData(formDataConst.CATEGORY_FORM_DATA, category || null))

  if (category) {
    yield put(updateFilter(filterDataConst.TYPE_FILTER_DATA, null))
    yield put(updateFilter(formDataConst.TYPE_FORM_DATA, null))
  }
}

export function* handleUpdateTypeFormData(urlParams: URLSearchParams) {
  if (!urlParams.get(formDataConst.CATEGORY_FORM_DATA)) {
    const types = urlParams.get(formDataConst.TYPE_FORM_DATA)
    const typesArr = types ? types.replace(/[[\]]/g, '').split(',') : null

    yield put(updateFormData(formDataConst.TYPE_FORM_DATA, typesArr))
  }
}

export function* handleUpdateDateData(urlParams: URLSearchParams) {
  const [timeFrom, timeTo] = [
    urlParams.get(formDataConst.TIME_FROM_FORM_DATA),
    urlParams.get(formDataConst.TIME_TO_FORM_DATA)
  ]
  const [timeFromDate, timeToDate] = [new Date(+timeFrom), new Date(+timeTo)]
  const validValues = timeFrom && timeTo && isValidDate(timeFromDate) && isValidDate(timeToDate)
  const { year, month } = convertDateToObject(new Date(window.getCurrentTimestamp()))
  const nextMonthNum = getNextMonthNum(month)
  const cornerTimestamp = getTimestamp(year, nextMonthNum - 1, 1)
  const bottomCornerTimestamp = getTimestamp(
    BOTTOM_DATE_PICKER_RESTRICTION.year,
    BOTTOM_DATE_PICKER_RESTRICTION.month - 1,
    1
  )

  if (validValues && +timeFrom < +timeTo && +timeTo < cornerTimestamp && +timeFrom >= bottomCornerTimestamp) {
    yield put(updateFormData(formDataConst.TIME_FROM_FORM_DATA, timeFrom))
    yield put(
      updateDatePickerDay(filterDataConst.TIME_FROM_FILTER_DATA, convertDateToObject(new Date(+timeFrom * 1000)))
    )
    yield put(updateFormData(formDataConst.TIME_TO_FORM_DATA, timeTo))
    yield put(
      updateDatePickerDay(filterDataConst.TIME_TO_FILTER_DATA, convertDateToObject(new Date(+timeTo * 1000 - 1000)))
    )
  } else {
    yield put(updateFormData(formDataConst.TIME_FROM_FORM_DATA, null))
    yield put(updateFormData(formDataConst.TIME_TO_FORM_DATA, null))
  }
}

export function* handleUpdateUserData(urlParams: URLSearchParams) {
  const userID = urlParams.get(formDataConst.USER_FORM_DATA)

  if (userID && !isNaN(+userID)) {
    yield put(updateFormData(formDataConst.USER_FORM_DATA, userID))
    yield put(updateFilter(filterDataConst.USER_FILTER_DATA, userID))
  }
}

export function* handleUpdateSearchTextData(urlParams: URLSearchParams) {
  const text = urlParams.get(formDataConst.TEXT_FORM_DATA)

  if (text) {
    yield put(updateFormData(formDataConst.TEXT_FORM_DATA, text))
    yield put(updateFilter(filterDataConst.TEXT_FILTER_DATA, text))
  }
}

export function* handleUpdateRowsData(urlParams: URLSearchParams) {
  const rows = urlParams.get(formDataConst.ROWS_FORM_DATA)

  if (rows && !isNaN(+rows)) {
    yield put(updateFormData(formDataConst.ROWS_FORM_DATA, rows))
    yield put(updateFilter(filterDataConst.ROWS_FILTER_DATA, rows))
  }
}

export function* updateStaffParam(urlParams: URLSearchParams) {
  const staffParam = urlParams.get(formDataConst.STAFF_FORM_DATA)

  yield put(updateFormData(formDataConst.STAFF_FORM_DATA, staffParam))
}

export default function* handleURLParams(key: string, urlParams: URLSearchParams) {
  switch (key) {
    case formDataConst.STAFF_FORM_DATA:
      yield call(updateStaffParam, urlParams)
      break
    case formDataConst.CATEGORY_FORM_DATA:
      yield call(handleUpdateCategoryFormData, urlParams)
      break
    case formDataConst.TYPE_FORM_DATA:
      yield call(handleUpdateTypeFormData, urlParams)
      break
    case formDataConst.TIME_TO_FORM_DATA:
      yield call(handleUpdateDateData, urlParams)
      break
    case formDataConst.USER_FORM_DATA:
      yield call(handleUpdateUserData, urlParams)
      break
    case formDataConst.TEXT_FORM_DATA:
      yield call(handleUpdateSearchTextData, urlParams)
      break
    case formDataConst.ROWS_FORM_DATA:
      yield call(handleUpdateRowsData, urlParams)
      break
  }
}
