import { call, put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import LogGroupsGenerator from '@torn/header/src/utils/logGroupsGenerator'
import { resetState, setInitialActivityLogData } from '../actions'
import updateFiltersAccordingToURL from './updateFiltersAccordingToURL'
import { getRequestURL } from '../selectors'
import updateFiltersAfterFetchingInitialData from './updateFiltersAfterFetchingInitialData'

export default function* fetchInitialActivityLogData() {
  try {
    yield put(resetState())
    yield call(updateFiltersAccordingToURL, true)

    const url = yield select(getRequestURL)
    const data = yield fetchUrl(`/page.php?sid=activityLogUserData${url}`)

    if (data.log) {
      const logHelper = new LogGroupsGenerator(data.log)
      const log = logHelper.getGroupedLogs()

      yield put(setInitialActivityLogData({ ...data, log }))
    } else {
      yield put(setInitialActivityLogData({ ...data, log: [] }))
    }
    yield call(updateFiltersAfterFetchingInitialData)
  } catch (e) {
    console.error(e)
  }
}
