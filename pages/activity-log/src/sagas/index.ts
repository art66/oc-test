import { all } from 'redux-saga/effects'
import activityLog from './activityLog'

export default function* rootSaga() {
  yield all([activityLog()])
}
