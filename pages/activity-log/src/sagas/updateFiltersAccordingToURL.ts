import { call } from 'redux-saga/effects'
import { FORM_DATA_ARRAY } from '../constants/formData'
import handleURLParams from './handleUrlParams'
import updateRequestURL from './updateRequestURL'

export default function* updateFiltersAccordingToURL(disableReplaceURL: boolean) {
  const queryStr = window.location.search

  const urlParams = new URLSearchParams(queryStr.replace('?', ''))

  for (const key of FORM_DATA_ARRAY) {
    if (urlParams.get(key)) {
      yield call(handleURLParams, key, urlParams)
    }
  }

  yield call(updateRequestURL, disableReplaceURL)
}
