import { call, put, select } from 'redux-saga/effects'
import { getCategories, getFormDataSelectedCategory, getFormDataSelectedType, getTypes } from '../selectors'
import { updateFilter } from '../actions'
import { CATEGORY_FILTER_DATA, TYPE_FILTER_DATA } from '../constants/filterData'
import { DEFAULT_CATEGORY_DROPDOWN_VALUE, DEFAULT_TYPE_DROPDOWN_VALUE } from '../constants'

export function* handleUpdateCategoryFilterData() {
  const categories = yield select(getCategories)
  const catID = yield select(getFormDataSelectedCategory)

  const category = categories.find(cat => `${cat.ID}` === `${catID}`)

  yield put(updateFilter(CATEGORY_FILTER_DATA, category || DEFAULT_CATEGORY_DROPDOWN_VALUE))

  if (category) {
    yield put(updateFilter(TYPE_FILTER_DATA, null))
  }
}

export function* handleUpdateTypeFilterData() {
  const catID = yield select(getFormDataSelectedCategory)

  if (!catID || catID === -1) {
    const types = yield select(getTypes)
    const typeIDs = yield select(getFormDataSelectedType)
    const selectedTypes = typeIDs ?
      types.filter(type => typeIDs.some(ID => `${ID}` === `${type.ID}`)) :
      [DEFAULT_TYPE_DROPDOWN_VALUE]

    yield put(updateFilter(TYPE_FILTER_DATA, selectedTypes))
  }
}

export default function* updateFiltersAfterFetchingInitialData() {
  yield call(handleUpdateCategoryFilterData)
  yield call(handleUpdateTypeFilterData)
}
