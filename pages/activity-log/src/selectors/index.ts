import { createSelector } from 'reselect'
import * as formDataConst from '../constants/formData'
import IStore from '../interfaces/IStore'

export const getLogsList = (state: IStore) => state.activityLog.history
export const getIsFetchingLogs = (state: IStore) => state.activityLog.isFetching
export const getStartFrom = (state: IStore) => state.activityLog.startFrom
export const getLogsEndReached = (state: IStore) => !+getStartFrom(state)
export const getDarkModeEnabled = (state: IStore) => state.activityLog.darkModeEnabled
export const getCategories = (state: IStore) => state.activityLog.categories
export const getTypes = (state: IStore) => state.activityLog.types
export const getFilters = (state: IStore) => state.activityLog.filters
export const getIsStaff = (state: IStore) => state.activityLog.isStaff
export const getFormData = (state: IStore) => state.activityLog.formData
export const getRequestURL = (state: IStore) => state.activityLog.url
export const getStopFetching = (state: IStore) => state.activityLog.stopFetching
export const getLogView = (state: IStore) => state.activityLog.view

export const getMediaType = (state: IStore) => state.browser.mediaType

export const getSelectedCategory = createSelector([getFilters], filters => filters.category)
export const getSelectedType = createSelector([getFilters], filters => filters.type)
export const getUserID = createSelector([getFilters], filters => filters.userID)
export const getSearchText = createSelector([getFilters], filters => filters.searchText)
export const getAmountRows = createSelector([getFilters], filters => filters.amountRows)
export const getDatePicker = createSelector([getFilters], filters => filters.datePicker)

export const getHoveredDate = createSelector([getDatePicker], datePicker => datePicker.hovered)
export const getTimeFrom = createSelector([getDatePicker], datePicker => datePicker.timeFrom)
export const getTimeTo = createSelector([getDatePicker], datePicker => datePicker.timeTo)
export const getDatePickerState = createSelector([getDatePicker], datePicker => datePicker.open)

export const getFormDataUserID = createSelector([getFormData], formData => formData[formDataConst.USER_FORM_DATA])
export const getFormDataSearchText = createSelector([getFormData], formData => formData[formDataConst.TEXT_FORM_DATA])
export const getFormDataAmountRows = createSelector([getFormData], formData => formData[formDataConst.ROWS_FORM_DATA])
export const getFormDataSelectedType = createSelector([getFormData], formData => formData[formDataConst.TYPE_FORM_DATA])
export const getFormDataSelectedCategory = createSelector(
  [getFormData],
  formData => formData[formDataConst.CATEGORY_FORM_DATA]
)
export const getTimeFromFormData = createSelector(
  [getFormData],
  formData => formData[formDataConst.TIME_FROM_FORM_DATA]
)
export const getTimeToFormData = createSelector([getFormData], formData => formData[formDataConst.TIME_TO_FORM_DATA])
