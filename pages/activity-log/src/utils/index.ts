import { getCookie } from '@torn/shared/utils'
import { RefObject } from 'react'
import {
  AMOUNT_DAYS_IN_WEEK,
  AMOUNT_MONTHS_IN_YEAR,
  DEFAULT_CALENDAR_ARROW_PRESET,
  DISABLED_CALENDAR_ARROW_PRESET
} from '../constants'
import IWindow from '../interfaces/IWindow'
import IDate from '../interfaces/IDate'
import IDropdownItem from '../interfaces/IDropdownItem'

declare const window: IWindow

export const isDMEnabled = (): boolean => getCookie('darkModeEnabled') === 'true'

export const getWeekday = (date: Date): number => (date.getUTCDay() === 0 ? AMOUNT_DAYS_IN_WEEK : date.getUTCDay())

export const getAmountDaysInMonth = (year: number, monthNum: number): number =>
  new Date(Date.UTC(year, monthNum, 0)).getUTCDate()

export const convertDateToObject = (date: Date): IDate => {
  return {
    day: date.getUTCDate(),
    month: date.getUTCMonth() + 1,
    year: date.getUTCFullYear(),
    weekday: getWeekday(date)
  }
}

export const getTimestamp = (year: number, month: number, day: number): number => {
  return Date.UTC(year, month, day) / 1000
}

export const getLastMonthDay = (year: number, month: number): number => {
  return new Date(Date.UTC(year, month, 0)).getUTCDate()
}

export const getMonthName = (month: number): string => {
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]

  return months[month - 1]
}

export const getPrevMonthNum = (monthNum: number): number => (monthNum === 1 ? AMOUNT_MONTHS_IN_YEAR : monthNum - 1)

export const getNextMonthNum = (monthNum: number): number => (monthNum === AMOUNT_MONTHS_IN_YEAR ? 1 : monthNum + 1)

export const getArrowPreset = (enabled: boolean): string => {
  return enabled ? DEFAULT_CALENDAR_ARROW_PRESET : DISABLED_CALENDAR_ARROW_PRESET
}

export const checkEqualityOfDays = (firstDate: IDate, secondDate: IDate): boolean => {
  const { day, month, year } = firstDate

  return day === secondDate?.day && month === secondDate?.month && year === secondDate?.year
}

export const checkIfDateInRange = (dateFrom: IDate, dateTo: IDate, date: IDate): boolean => {
  const dateUTC = Date.UTC(date.year, date.month - 1, date.day)

  return (
    Date.UTC(dateFrom?.year, dateFrom?.month - 1, dateFrom?.day) < dateUTC
    && dateUTC < Date.UTC(dateTo?.year, dateTo?.month - 1, dateTo?.day)
  )
}

export const addLeadZero = (num: number): string => {
  return num < 10 ? `0${num}` : `${num}`
}

const getActionTimeInObject = (
  time: number
): {
  day: string
  month: string
  year: string
  hours: string
  minutes: string
  seconds: string
} => {
  const date = new Date(time * 1000)
  const day = addLeadZero(date.getUTCDate())
  const month = addLeadZero(date.getUTCMonth() + 1)
  const year = addLeadZero(date.getUTCFullYear())
  const hours = addLeadZero(date.getUTCHours())
  const minutes = addLeadZero(date.getUTCMinutes())
  const seconds = addLeadZero(date.getUTCSeconds())

  return {
    seconds,
    minutes,
    hours,
    year,
    month,
    day
  }
}

export const getActionTime = (time: number): string => {
  const { seconds, minutes, hours, year, month, day } = getActionTimeInObject(time)

  return `${hours}:${minutes}:${seconds} ${day}/${month}/${+year % 100}`
}

export const isOutsideElement = (ref: RefObject<HTMLElement>, target: HTMLElement): boolean => {
  if (!document.body.contains(target)) return false

  return !ref?.current?.contains(target)
}

export const getTypesData = (value: IDropdownItem[]): number[] | null => {
  if (value.length || value.length === 0) {
    const types = value.map(item => item.ID).filter(id => id !== -1)

    return types.length ? types : null
  }

  return null
}

export const swapTimeValues = (timeFrom: IDate, currDay: IDate): [number, number] => {
  const timeToTimestamp = getTimestamp(currDay.year, currDay.month - 1, currDay.day)
  const timeFromTimestamp = getTimestamp(timeFrom.year, timeFrom.month - 1, timeFrom.day)

  if (timeToTimestamp < timeFromTimestamp) {
    return [
      getTimestamp(currDay.year, currDay.month - 1, currDay.day),
      getTimestamp(timeFrom.year, timeFrom.month - 1, timeFrom.day + 1)
    ]
  }

  return [timeFromTimestamp, getTimestamp(currDay.year, currDay.month - 1, currDay.day + 1)]
}

export const getSelectedMonth = (timeFrom: IDate): number => {
  return timeFrom ? timeFrom.month : convertDateToObject(new Date(window.getCurrentTimestamp())).month
}

export const getSelectedYear = (timeFrom: IDate): number => {
  return timeFrom ? timeFrom.year : convertDateToObject(new Date(window.getCurrentTimestamp())).year
}

export const isValidDate = (date: Date): boolean => {
  return date instanceof Date && !isNaN(date as any)
}
