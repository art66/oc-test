import { IBrowser } from 'redux-responsive'
import IActivityLog from './IActivityLog'

export default interface IStore {
  browser: IBrowser
  activityLog: IActivityLog
}
