import IDate from './IDate'
import IDropdownItem from './IDropdownItem'

export default interface IFilters {
  category: IDropdownItem
  userID: string
  searchText: string
  amountRows: string
  datePicker: {
    timeFrom: IDate | null
    timeTo: IDate | null
    hovered: IDate | null
    open: boolean
  }
  type?: IDropdownItem[]
}
