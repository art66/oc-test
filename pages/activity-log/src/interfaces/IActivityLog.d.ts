import IGroup from '@torn/header/src/interfaces/recentHistory/IGroup'
import IFilters from './IFilters'
import IDropdownItem from './IDropdownItem'

export default interface IActivityLog {
  history?: IGroup[]
  darkModeEnabled: boolean
  filters: IFilters
  formData: {
    [key: string]: any
  }
  url: string
  stopFetching: boolean
  view: string
  startFrom?: string
  categories?: IDropdownItem[]
  types?: IDropdownItem[]
  isStaff?: boolean
  isFetching?: boolean
}
