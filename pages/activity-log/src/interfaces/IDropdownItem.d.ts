export default interface IDropdownItem {
  ID: number
  name: string
}
