export default interface IWindow extends Window {
  getCurrentTimestamp?: () => number
}
