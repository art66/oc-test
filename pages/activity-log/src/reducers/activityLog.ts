import { RECENT_HISTORY_ACTION_HANDLERS } from '@torn/header/src/reducers/recentHistory'
import { Action } from 'redux-actions'
import IGroup from '@torn/header/src/interfaces/recentHistory/IGroup'
import initialState from '../constants/initialState'
import * as a from '../actions/actionTypes'
import IActivityLog from '../interfaces/IActivityLog'
import IDate from '../interfaces/IDate'
import { TInitPayload, TUpdateDayPayload, TUpdateFilterPayload, TUpdateFormPayload } from '../actions/types'
import { DEFAULT_CATEGORY_DROPDOWN_VALUE, DEFAULT_TYPE_DROPDOWN_VALUE } from '../constants'

const ACTION_HANDLERS = {
  ...RECENT_HISTORY_ACTION_HANDLERS,
  [a.SET_INITIAL_ACTIVITY_LOG_DATA]: (state: IActivityLog, action: Action<TInitPayload>) => {
    const { log, startFrom, categories, types, isStaff } = action.payload

    return {
      ...state,
      history: log,
      startFrom,
      categories: [DEFAULT_CATEGORY_DROPDOWN_VALUE, ...categories],
      types: [DEFAULT_TYPE_DROPDOWN_VALUE, ...types],
      isStaff,
      isFetching: false
    }
  },
  [a.CHANGE_DM_STATE]: (state: IActivityLog, action: Action<{ enabled: boolean }>) => {
    return {
      ...state,
      darkModeEnabled: action.payload.enabled
    }
  },
  [a.UPDATE_FILTER]: (state: IActivityLog, action: Action<TUpdateFilterPayload>) => {
    const { key, value } = action.payload

    return {
      ...state,
      filters: {
        ...state.filters,
        [key]: value
      }
    }
  },
  [a.SET_HOVERED_DAY]: (state: IActivityLog, action: Action<{ date: IDate }>) => {
    return {
      ...state,
      filters: {
        ...state.filters,
        datePicker: {
          ...state.filters.datePicker,
          hovered: action.payload.date
        }
      }
    }
  },
  [a.SET_SELECTED_DAY]: (state: IActivityLog, action: Action<TUpdateDayPayload>) => {
    return {
      ...state,
      filters: {
        ...state.filters,
        datePicker: {
          ...state.filters.datePicker,
          [action.payload.label]: action.payload.date
        }
      }
    }
  },
  [a.UPDATE_FORM_DATA]: (state: IActivityLog, action: Action<TUpdateFormPayload>) => {
    return {
      ...state,
      formData: {
        ...state.formData,
        [action.payload.label]: action.payload.data
      }
    }
  },
  [a.SET_REQUEST_URL]: (state: IActivityLog, action: Action<{ url: string }>) => {
    return {
      ...state,
      url: action.payload.url
    }
  },
  [a.SET_LOGS]: (state: IActivityLog, action: Action<{ log: IGroup[] }>) => {
    return {
      ...state,
      history: action.payload.log
    }
  },
  [a.SET_STOP_FETCHING]: (state: IActivityLog, action: Action<{ stop: boolean }>) => {
    return {
      ...state,
      stopFetching: action.payload.stop
    }
  },
  [a.TOGGLE_VIEW]: (state: IActivityLog, action: Action<{ view: string }>) => {
    return {
      ...state,
      view: action.payload.view
    }
  },
  [a.TOGGLE_DATE_PICKER]: (state: IActivityLog, action: Action<{ open: boolean }>) => {
    return {
      ...state,
      filters: {
        ...state.filters,
        datePicker: {
          ...state.filters.datePicker,
          open: action.payload.open
        }
      }
    }
  },
  [a.RESET_STATE]: () => {
    return initialState
  }
}

export default function activityLogReducer(state: IActivityLog = initialState, action: Action<any>) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
