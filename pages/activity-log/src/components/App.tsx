import React, { useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AppHeader from '@torn/shared/components/AppHeader'
import { TOGGLE_DARK_MODE_EVENT } from '@torn/header/src/constants'
import Preloader from '@torn/shared/components/Preloader'
import { fetchInitialActivityLogData, changeDMState } from '../actions'
import Log from './Log'
import { getLogsList } from '../selectors'
import { isDMEnabled } from '../utils'
import FiltersPanel from './FiltersPanel'
import CLIENT_PROPS from '../constants/appHeader'
import '../styles/common.scss'

export const App = () => {
  const dispatch = useDispatch()
  const history = useSelector(getLogsList)

  const handleNavigateByArrowsInTheBrowser = useCallback(() => {
    dispatch(fetchInitialActivityLogData())
  }, [dispatch])

  useEffect(() => {
    const handleChangeDMState = () => {
      dispatch(changeDMState(isDMEnabled()))
    }

    dispatch(fetchInitialActivityLogData())
    window.addEventListener(TOGGLE_DARK_MODE_EVENT, handleChangeDMState)

    return () => {
      window.removeEventListener(TOGGLE_DARK_MODE_EVENT, handleChangeDMState)
    }
  }, [dispatch])

  useEffect(() => {
    window.addEventListener('popstate', handleNavigateByArrowsInTheBrowser)

    return () => {
      window.removeEventListener('popstate', handleNavigateByArrowsInTheBrowser)
    }
  }, [handleNavigateByArrowsInTheBrowser])

  return (
    <div className='activity-log'>
      <AppHeader appID='activity-log' clientProps={CLIENT_PROPS} />
      {history ? (
        <>
          <FiltersPanel />
          <Log />
        </>
      ) : (
        <div className='log-preloader-wrapper'>
          <Preloader />
        </div>
      )}
    </div>
  )
}

export default React.memo(App)
