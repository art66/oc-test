import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import ILog from '@torn/header/src/interfaces/recentHistory/IAction'
import { getIsFetchingLogs, getLogsList } from '../../selectors'
import { getActionTime } from '../../utils'
import TableRows from './TableRows'
import mobileStyles from './styles/TableMobile.cssmodule.scss'
import s from './styles/Table.cssmodule.scss'

export const TableMobile = () => {
  const history = useSelector(getLogsList)
  const isFetching = useSelector(getIsFetchingLogs)

  const renderDateCol = () => {
    const dates = history.length ?
      [
          <span className={cn(s.row, s.title, mobileStyles.row)} key='date'>
            Date
          </span>
      ] :
      []

    history.forEach(group => {
      group.log.forEach(log => {
        dates.push(
          <div className={cn(s.row, mobileStyles.row)} key={log.ID}>
            {getActionTime(log.time)}
          </div>
        )

        if (log.similar) {
          log.similar.log.forEach(similarLog => {
            dates.push(
              <div className={cn(s.row, mobileStyles.row)} key={similarLog.ID}>
                {getActionTime(similarLog.time)}
              </div>
            )
          })
        }
      })
    })

    return dates
  }

  const renderTableTitle = () => {
    return (
      history[0]
      && history[0].log[0].table.map(col => {
        return (
          <span className={cn(s.col, mobileStyles.col)} key={col.name || col.value}>
            {col.name}
          </span>
        )
      })
    )
  }

  const getRow = useCallback((log: ILog): JSX.Element[] => {
    const cols = []

    log.table.forEach(col => {
      cols.push(
        <span className={cn(s.col, mobileStyles.col)} key={col.name} dangerouslySetInnerHTML={{ __html: col.value }} />
      )
    })

    return cols
  }, [])

  return (
    <div className={cn(s.table, mobileStyles.table, { [s.fetched]: !isFetching })}>
      <div className={cn(s.dateCol, mobileStyles.dateCol)}>{renderDateCol()}</div>

      <div className={mobileStyles.scrollArea}>
        <div className={mobileStyles.dataWrapper}>
          <div className={cn(s.data, mobileStyles.data)}>
            <div className={cn(s.row, mobileStyles.row, s.title)}>{renderTableTitle()}</div>
            <TableRows getRow={getRow} rowClassName={cn(s.row, mobileStyles.row)} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default TableMobile
