import React from 'react'
import { useSelector } from 'react-redux'
import ILog from '@torn/header/src/interfaces/recentHistory/IAction'
import IGroup from '@torn/header/src/interfaces/recentHistory/IGroup'
import { getLogsList } from '../../selectors'

interface IProps {
  rowClassName: string
  getRow: (log: ILog) => JSX.Element[]
}

export const TableRows = React.memo((props: IProps) => {
  const history = useSelector(getLogsList)
  const logs = []

  const addRow = (log: ILog) => {
    logs.push(
      <div className={props.rowClassName} key={log.ID}>
        {props.getRow(log)}
      </div>
    )
  }

  history.forEach((group: IGroup) => {
    group.log.forEach(log => {
      addRow(log)

      if (log.similar) {
        log.similar.log.forEach(similarLog => {
          addRow(similarLog)
        })
      }
    })
  })

  return <>{logs}</>
})

export default TableRows
