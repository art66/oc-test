import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import ILog from '@torn/header/src/interfaces/recentHistory/IAction'
import IGroup from '@torn/header/src/interfaces/recentHistory/IGroup'
import { getIsFetchingLogs, getLogsList } from '../../selectors'
import { getActionTime } from '../../utils'
import desktopStyles from './styles/TableDesktop.cssmodule.scss'
import s from './styles/Table.cssmodule.scss'

export const TableDesktop = () => {
  const history = useSelector(getLogsList)
  const isFetching = useSelector(getIsFetchingLogs)

  const renderTableTitle = () => {
    return (
      history[0]
      && [{ name: 'Date' }, ...history[0].log[0].table].map(col => {
        return (
          <th
            className={cn(s.col, desktopStyles.col, { [desktopStyles.date]: col.name === 'Date' })}
            key={col.name || col.value}
          >
            {col.name}
          </th>
        )
      })
    )
  }

  const getRow = useCallback((log: ILog) => {
    return [{ name: 'Date', value: getActionTime(log.time) }, ...log.table].map(col => {
      return (
        <td
          className={cn(s.col, desktopStyles.col, desktopStyles.value, { [desktopStyles.date]: col.name === 'Date' })}
          key={col.name}
          dangerouslySetInnerHTML={{ __html: col.value }}
        />
      )
    })
  }, [])

  const renderRows = () => {
    const logs = []

    const addRow = (log: ILog) => {
      logs.push(
        <tr className={cn(s.row, desktopStyles.row)} key={log.ID}>
          {getRow(log)}
        </tr>
      )
    }

    history.forEach((group: IGroup) => {
      group.log.forEach(log => {
        addRow(log)

        if (log.similar) {
          log.similar.log.forEach(similarLog => {
            addRow(similarLog)
          })
        }
      })
    })

    return <>{logs}</>
  }

  return (
    <table className={cn(s.table, desktopStyles.table, { [s.fetched]: !isFetching })}>
      <thead className={desktopStyles.tableHeader}>
        <tr className={cn(s.row, s.title, desktopStyles.row)}>{renderTableTitle()}</tr>
      </thead>
      <tbody>{renderRows()}</tbody>
    </table>
  )
}

export default TableDesktop
