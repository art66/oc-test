import React, { useCallback, useEffect } from 'react'
import cn from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import {
  PRESET_TYPE,
  DEFAULT_TITLE_BUTTON_PRESET,
  SENTENCE_VIEW,
  TABLE_VIEW,
  TABLE_VIEW_BTN_ID,
  DISABLED_TITLE_BUTTON_PRESET,
  HOVERED_TITLE_BUTTON_PRESET
} from '../../constants'
import { TITLE_ICONS } from '../../constants/iconsDimensions'
import { toggleView } from '../../actions'
import { getLogView, getSelectedType } from '../../selectors'
import s from './styles/Title.cssmodule.scss'
import tooltipStyles from './styles/Tooltip.cssmodule.scss'

const Title = () => {
  const dispatch = useDispatch()
  const view = useSelector(getLogView)
  const selectedType = useSelector(getSelectedType)

  const handleClickOnListViewButton = () => {
    if (view !== SENTENCE_VIEW) {
      dispatch(toggleView(SENTENCE_VIEW))
    }
  }

  const isTableViewBtnEnabled = useCallback(() => {
    return selectedType?.length === 1 && selectedType[0].ID !== -1
  }, [selectedType])

  const handleClickOnTableViewButton = () => {
    if (view !== TABLE_VIEW && isTableViewBtnEnabled()) {
      dispatch(toggleView(TABLE_VIEW))
    }
  }

  useEffect(() => {
    if (!isTableViewBtnEnabled()) {
      tooltipsSubscriber.subscribe({
        ID: TABLE_VIEW_BTN_ID,
        child: <span>No types selected</span>,
        customConfig: {
          position: { x: 'center', y: 'top' },
          overrideStyles: {
            container: tooltipStyles.container,
            title: tooltipStyles.title,
            tooltipArrow: tooltipStyles.tipArrow
          }
        }
      })
    }

    return () => {
      tooltipsSubscriber.unsubscribe({
        ID: TABLE_VIEW_BTN_ID,
        child: null
      })
    }
  }, [isTableViewBtnEnabled])

  const getDefaultTableViewPreset = () => {
    return view === TABLE_VIEW ? HOVERED_TITLE_BUTTON_PRESET : DEFAULT_TITLE_BUTTON_PRESET
  }

  return (
    <div className={s.title}>
      <span className={s.name}>Activity Log</span>
      <div className={s.buttonsWrapper}>
        <button
          type='button'
          className={cn(s.button, { [s.active]: view === SENTENCE_VIEW })}
          onClick={handleClickOnListViewButton}
          aria-label='Sentence view'
        >
          <SVGIconGenerator
            iconName='ListView'
            customClass={s.icon}
            preset={{
              type: PRESET_TYPE,
              subtype: view === SENTENCE_VIEW ? HOVERED_TITLE_BUTTON_PRESET : DEFAULT_TITLE_BUTTON_PRESET
            }}
            dimensions={TITLE_ICONS}
            onHover={{ active: true, preset: { type: PRESET_TYPE, subtype: HOVERED_TITLE_BUTTON_PRESET } }}
          />
        </button>
        <button
          type='button'
          className={cn(s.button, { [s.active]: view === TABLE_VIEW, [s.disabled]: !isTableViewBtnEnabled() })}
          onClick={handleClickOnTableViewButton}
          id={TABLE_VIEW_BTN_ID}
          aria-label='Table view'
        >
          <SVGIconGenerator
            iconName='TableView'
            customClass={s.icon}
            preset={{
              type: PRESET_TYPE,
              subtype: isTableViewBtnEnabled() ? getDefaultTableViewPreset() : DISABLED_TITLE_BUTTON_PRESET
            }}
            dimensions={TITLE_ICONS}
            onHover={{
              active: isTableViewBtnEnabled(),
              preset: { type: PRESET_TYPE, subtype: HOVERED_TITLE_BUTTON_PRESET }
            }}
          />
        </button>
      </div>
    </div>
  )
}

export default React.memo(Title)
