import React from 'react'
import { useSelector } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'
import { getIsFetchingLogs, getMediaType } from '../../selectors'
import TableDesktop from './TableDesktop'
import TableMobile from './TableMobile'
import s from './styles/Table.cssmodule.scss'

export const Table = () => {
  const mediaType = useSelector(getMediaType)
  const isFetching = useSelector(getIsFetchingLogs)
  const isManualDesktopMode = document.body.classList.contains('d') && !document.body.classList.contains('r')

  const renderPreloader = () => {
    return isFetching ? (
      <div className={s.preloaderWrapper}>
        <Preloader />
      </div>
    ) : null
  }

  return (
    <>
      {mediaType === 'desktop' || isManualDesktopMode ? <TableDesktop /> : <TableMobile />}
      {renderPreloader()}
    </>
  )
}

export default Table
