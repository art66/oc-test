import React, { useCallback, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
// eslint-disable-next-line max-len
import LogList from '@torn/header/src/components/HeaderWrapperTop/NavigationToolbar/RecentHistory/RecentHistoryApp/LogList/LogList'
import {
  getDarkModeEnabled,
  getIsFetchingLogs,
  getIsStaff,
  getLogsEndReached,
  getLogsList,
  getLogView,
  getStopFetching
} from '../../selectors'
import { loadNextChunk } from '../../actions'
import Title from './Title'
import Table from './Table'
import { TABLE_VIEW } from '../../constants'
import s from './styles/index.cssmodule.scss'

const getHeightWithTopOffset = elRect => elRect.height + (elRect.top + window.pageYOffset)
const getWinHeightWithTopOffset = () => window.innerHeight + window.pageYOffset

// eslint-disable-next-line max-statements
export const Log = () => {
  const dispatch = useDispatch()
  const logPanel = useRef<HTMLDivElement>(null)
  const timer = useRef(null)
  const topOffset = useRef(0)
  const winHeightWithTopOffset = useRef(getWinHeightWithTopOffset())
  const isFetching = useSelector(getIsFetchingLogs)
  const history = useSelector(getLogsList)
  const logsEndReached = useSelector(getLogsEndReached)
  const darkModeEnabled = useSelector(getDarkModeEnabled)
  const stopFetching = useSelector(getStopFetching)
  const view = useSelector(getLogView)
  const isStaff = useSelector(getIsStaff)

  const autoLoading = useCallback(() => {
    if (!isFetching) {
      topOffset.current = getHeightWithTopOffset(logPanel.current.getBoundingClientRect())

      if (topOffset.current < winHeightWithTopOffset.current && !logsEndReached && !stopFetching) {
        dispatch(loadNextChunk())
      }
    }

    if (logsEndReached || topOffset.current >= winHeightWithTopOffset.current || stopFetching) {
      clearInterval(timer.current)
    }
  }, [isFetching, logsEndReached, stopFetching, dispatch])

  const handleScroll = useCallback(() => {
    topOffset.current = getHeightWithTopOffset(logPanel.current.getBoundingClientRect())
    winHeightWithTopOffset.current = getWinHeightWithTopOffset()
    const diff = topOffset.current - winHeightWithTopOffset.current

    if (diff <= 0 && !isFetching && !logsEndReached) {
      dispatch(loadNextChunk())
    }
  }, [isFetching, logsEndReached, dispatch])

  const renderLog = () => {
    if (view === TABLE_VIEW) {
      return <Table />
    }

    return (
      <LogList
        darkModeEnabled={darkModeEnabled}
        isFetching={isFetching}
        history={history}
        mode='light'
        isStaffVersion={isStaff}
      />
    )
  }

  useEffect(() => {
    topOffset.current = getHeightWithTopOffset(logPanel.current.getBoundingClientRect())
  }, [])

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    timer.current = setInterval(autoLoading, 200)

    return () => {
      window.removeEventListener('scroll', handleScroll)
      clearInterval(timer.current)
    }
  }, [handleScroll, autoLoading])

  return (
    <div className='panel' ref={logPanel}>
      <Title />
      <div className={cn(s.logWrapper, { [s.tableView]: view === TABLE_VIEW })}>
        {!history?.length && !isFetching ? <div className={s.notFoundMsg}>No records found</div> : renderLog()}
      </div>
    </div>
  )
}

export default React.memo(Log)
