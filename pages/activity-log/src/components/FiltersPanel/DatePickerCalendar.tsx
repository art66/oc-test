import React, { useCallback, useEffect, useRef, useState } from 'react'
import cn from 'classnames'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import { useDispatch, useSelector } from 'react-redux'
import {
  PRESET_TYPE,
  DEFAULT_CALENDAR_ARROW_PRESET,
  HOVERED_CALENDAR_ARROW_PRESET,
  AMOUNT_MONTHS_IN_YEAR,
  AMOUNT_ITEMS_IN_MONTH_GRID,
  RESET_ICON_PRESET,
  BOTTOM_DATE_PICKER_RESTRICTION
} from '../../constants'
import { TIME_FROM_FORM_DATA } from '../../constants/formData'
import { TIME_FROM_FILTER_DATA } from '../../constants/filterData'
import { ARROW_ICONS, RESET_ICON } from '../../constants/iconsDimensions'
import {
  convertDateToObject,
  getAmountDaysInMonth,
  getArrowPreset,
  getMonthName,
  getNextMonthNum,
  getPrevMonthNum,
  getSelectedMonth,
  getSelectedYear,
  getWeekday
} from '../../utils'
import IWindow from '../../interfaces/IWindow'
import DatePickerDay from './DatePickerDay'
import { resetSelectedDate, setSelectedDay, toggleDatePicker, updateFormData } from '../../actions'
import { getDatePickerState, getTimeFrom, getTimeTo } from '../../selectors'
import { isOutsideElement } from '../../../../../modules/header/src/utils'
import s from './styles/DatePickerCalendar.cssmodule.scss'

declare const window: IWindow

const DatePickerCalendar = () => {
  const dispatch = useDispatch()
  const timeFrom = useSelector(getTimeFrom)
  const timeTo = useSelector(getTimeTo)
  const [monthNum, setActiveMonth] = useState(() => getSelectedMonth(timeFrom))
  const [year, setActiveYear] = useState(() => getSelectedYear(timeFrom))
  const [canGoToNextMonth, setCanGoToNextMonth] = useState(false)
  const [canGoToPrevMonth, setCanGoToPrevMonth] = useState(false)
  const open = useSelector(getDatePickerState)
  const calendarRef = useRef<HTMLDivElement>()

  const handleClickOutside = useCallback(
    (e: MouseEvent) => {
      const isOutside = isOutsideElement(calendarRef, e.target)

      if (open && isOutside) {
        dispatch(toggleDatePicker(false))

        if (timeFrom && !timeTo) {
          dispatch(setSelectedDay(TIME_FROM_FILTER_DATA, null))
          dispatch(updateFormData(TIME_FROM_FORM_DATA, null))
        }
      }
    },
    [dispatch, open, timeFrom, timeTo]
  )

  const setNextMonth = () => {
    if (!canGoToNextMonth) return false

    setActiveMonth(getNextMonthNum(monthNum))

    if (monthNum === AMOUNT_MONTHS_IN_YEAR) {
      setActiveYear(year + 1)
    }
  }

  const setPrevMonth = () => {
    setActiveMonth(getPrevMonthNum(monthNum))

    if (monthNum === 1) {
      setActiveYear(year - 1)
    }
  }

  const renderDays = () => {
    const days: JSX.Element[] = []
    const weekdayOfFirstDay = getWeekday(new Date(Date.UTC(year, monthNum - 1, 1)))
    const amountDaysInMonth = getAmountDaysInMonth(year, monthNum)
    const amountDaysInPrevMonth = getAmountDaysInMonth(year, monthNum ? monthNum - 1 : 12)
    const startGridDay = amountDaysInPrevMonth - weekdayOfFirstDay + 2

    for (let i = startGridDay; i <= amountDaysInPrevMonth; i++) {
      days.push(<DatePickerDay key={`${i}${monthNum - 1}`} enabled={false} day={i} />)
    }

    for (let i = 1; i <= amountDaysInMonth; i++) {
      days.push(<DatePickerDay key={`${i}${monthNum}`} enabled={true} day={i} month={monthNum} year={year} />)
    }

    for (let i = days.length, j = 1; i < AMOUNT_ITEMS_IN_MONTH_GRID; i++, j++) {
      days.push(<DatePickerDay key={`${j}${monthNum + 1}`} enabled={false} day={j} />)
    }

    return days
  }

  const resetToAllTime = () => dispatch(resetSelectedDate())

  useEffect(() => {
    const { month: currMonth, year: currYear } = convertDateToObject(new Date(window.getCurrentTimestamp()))

    setCanGoToNextMonth(currMonth !== monthNum || currYear !== year)
  }, [monthNum, year])

  useEffect(() => {
    const canGo =
      year > BOTTOM_DATE_PICKER_RESTRICTION.year
      || (monthNum > BOTTOM_DATE_PICKER_RESTRICTION.month && year === BOTTOM_DATE_PICKER_RESTRICTION.year)

    setCanGoToPrevMonth(canGo)
  }, [monthNum, year])

  useEffect(() => {
    document.addEventListener('click', handleClickOutside)

    return () => {
      document.removeEventListener('click', handleClickOutside)
    }
  }, [handleClickOutside])

  return (
    <div className={s.calendar} ref={calendarRef} role='dialog'>
      <div className={s.monthToggler}>
        <button
          type='button'
          className={cn(s.arrowBtn, s.left)}
          onClick={setPrevMonth}
          aria-label='Previous month'
          disabled={!canGoToPrevMonth}
        >
          <SVGIconGenerator
            iconName='ArrowLeftThin'
            preset={{ type: PRESET_TYPE, subtype: DEFAULT_CALENDAR_ARROW_PRESET }}
            dimensions={ARROW_ICONS}
            customClass={s.arrowIcon}
            onHover={{ active: true, preset: { type: PRESET_TYPE, subtype: HOVERED_CALENDAR_ARROW_PRESET } }}
          />
        </button>
        <span className={s.title}>
          {getMonthName(monthNum)} {year}
        </span>
        <button
          type='button'
          className={cn(s.arrowBtn, s.right)}
          onClick={setNextMonth}
          disabled={!canGoToNextMonth}
          aria-label='Next month'
        >
          <SVGIconGenerator
            iconName='ArrowRightThin'
            preset={{ type: PRESET_TYPE, subtype: getArrowPreset(canGoToNextMonth) }}
            dimensions={ARROW_ICONS}
            customClass={s.arrowIcon}
            onHover={{
              active: canGoToNextMonth,
              preset: { type: PRESET_TYPE, subtype: HOVERED_CALENDAR_ARROW_PRESET }
            }}
          />
        </button>
      </div>
      <div className={s.month}>{renderDays()}</div>
      <div className={s.resetBlock}>
        <button type='button' className={s.resetBtn} onClick={resetToAllTime}>
          <SVGIconGenerator
            iconName='RefreshCircular'
            customClass={s.icon}
            iconsHolder={iconsHolder}
            preset={{ type: PRESET_TYPE, subtype: RESET_ICON_PRESET }}
            dimensions={RESET_ICON}
          />
          <span className={s.text}>Reset to all time</span>
        </button>
      </div>
    </div>
  )
}

export default React.memo(DatePickerCalendar)
