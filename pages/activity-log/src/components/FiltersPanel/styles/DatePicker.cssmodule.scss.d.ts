// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'calendarIcon': string;
  'datePicker': string;
  'globalSvgShadow': string;
  'open': string;
  'selectedDate': string;
  'selectedDateWrapper': string;
  'toggler': string;
}
export const cssExports: CssExports;
export default cssExports;
