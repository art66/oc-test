import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { setHoveredDay, selectDay } from '../../actions'
import { checkEqualityOfDays, checkIfDateInRange, getAmountDaysInMonth } from '../../utils'
import { getHoveredDate, getTimeFrom, getTimeTo } from '../../selectors'
import s from './styles/DatePickerCalendar.cssmodule.scss'

interface IProps {
  enabled: boolean
  day: number
  month?: number
  year?: number
}

const DatePickerDay = (props: IProps) => {
  const { day, month, year, enabled } = props
  const dispatch = useDispatch()
  const timeFrom = useSelector(getTimeFrom)
  const timeTo = useSelector(getTimeTo)
  const hovered = useSelector(getHoveredDate)
  const [isSelectedDay, updateIsSelectedDay] = useState(false)
  const [dateInRange, setInRange] = useState(false)

  const handleHover = () => {
    if (enabled) {
      dispatch(setHoveredDay({ day, month, year }))
    }
  }

  const handleMouseLeave = () => {
    if (enabled) {
      dispatch(setHoveredDay(null))
    }
  }

  const handleClick = () => {
    if (enabled) {
      updateIsSelectedDay(true)

      dispatch(selectDay({ day, month, year }))
    }
  }

  const checkHoveredLessThanTimeFrom = () => {
    return (
      Date.UTC(timeFrom?.year, timeFrom?.month - 1, timeFrom?.day)
      > Date.UTC(hovered?.year, hovered?.month - 1, hovered?.day)
    )
  }

  const checkHoveredMoreThanTimeFrom = () => {
    return (
      Date.UTC(timeFrom?.year, timeFrom?.month - 1, timeFrom?.day)
      < Date.UTC(hovered?.year, hovered?.month - 1, hovered?.day)
    )
  }

  const timeFromClassActive = (isHovered: boolean, isTimeFrom: boolean): boolean => {
    const isSelectedTimeFrom = timeTo && isTimeFrom
    const isHoveredLessForTimeFrom = hovered && !timeTo && isHovered && checkHoveredLessThanTimeFrom()
    const isHoveredMoreForTimeFrom = isTimeFrom && checkHoveredMoreThanTimeFrom()

    return timeFrom && (isSelectedTimeFrom || isHoveredLessForTimeFrom || isHoveredMoreForTimeFrom)
  }

  const timeToClassActive = (isHovered: boolean, isTimeTo: boolean, isTimeFrom: boolean): boolean => {
    const isSelectedTimeTo = timeTo && isTimeTo
    const isHoveredMoreForTimeTo = !timeTo && timeFrom && hovered && isHovered && checkHoveredMoreThanTimeFrom()
    const isHoveredLessForTimeTo = !timeTo && isTimeFrom && checkHoveredLessThanTimeFrom()

    return isSelectedTimeTo || isHoveredMoreForTimeTo || isHoveredLessForTimeTo
  }

  const getClassNames = () => {
    const isHovered = checkEqualityOfDays({ day, month, year }, hovered)
    const isTimeTo = checkEqualityOfDays({ day, month, year }, timeTo)
    const isTimeFrom = checkEqualityOfDays({ day, month, year }, timeFrom)

    return cn(s.day, {
      [s.notActive]: !enabled,
      [s.selected]: isSelectedDay || isHovered,
      [s.inRange]: dateInRange,
      [s.timeFrom]: timeFromClassActive(isHovered, isTimeFrom),
      [s.timeTo]: timeToClassActive(isHovered, isTimeTo, isTimeFrom),
      [s.firstDay]: enabled && day === 1,
      [s.lastDay]: enabled && day === getAmountDaysInMonth(year, month)
    })
  }

  const handleKeyPress = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      handleClick()
    }
  }

  useEffect(() => {
    const isSelected =
      checkEqualityOfDays({ day, month, year }, timeFrom) || checkEqualityOfDays({ day, month, year }, timeTo)
    const inRange =
      checkIfDateInRange(timeFrom, timeTo, { day, month, year })
      || (!timeTo
        && timeFrom
        && (checkIfDateInRange(timeFrom, hovered, { day, month, year })
          || checkIfDateInRange(hovered, timeFrom, { day, month, year })))

    updateIsSelectedDay(isSelected)
    setInRange(inRange)
  }, [timeFrom, timeTo, isSelectedDay, day, month, year, hovered])

  return (
    <div
      role='button'
      tabIndex={props.enabled ? 0 : -1}
      className={getClassNames()}
      onMouseEnter={handleHover}
      onClick={handleClick}
      onMouseLeave={handleMouseLeave}
      onKeyPress={handleKeyPress}
    >
      <div className={s.numWrap}>
        <div className={s.num}>{props.day}</div>
      </div>
    </div>
  )
}

export default DatePickerDay
