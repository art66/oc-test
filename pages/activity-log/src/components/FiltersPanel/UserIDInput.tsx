import React from 'react'
import cn from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import UsersAutocomplete from '@torn/shared/components/UsersAutocomplete'
import { getFormDataUserID } from '../../selectors'
import { fetchLogs, updateFilter, updateFormData } from '../../actions'
import { USER_FORM_DATA } from '../../constants/formData'
import { USER_FILTER_DATA } from '../../constants/filterData'
import s from './styles/ExtendedFilters.cssmodule.scss'

const UserIDInput = () => {
  const dispatch = useDispatch()
  const formDataUserID = useSelector(getFormDataUserID)

  const handleSelect = selected => {
    if (selected.id !== formDataUserID) {
      dispatch(updateFilter(USER_FILTER_DATA, selected.id))
      dispatch(updateFormData(USER_FORM_DATA, selected.id))
      dispatch(fetchLogs())
    }
  }

  const handleUserSelectClear = () => {
    if (formDataUserID) {
      dispatch(updateFilter('userID', ''))
      dispatch(updateFormData(USER_FORM_DATA, ''))
      dispatch(fetchLogs())
    }
  }

  return (
    <UsersAutocomplete
      withCategories={true}
      placeholder='User ID'
      onSelect={handleSelect}
      inputClassName={cn(s.inputField, s.autocomplete, 'activity-log-user-autocomplete-input')}
      onClear={handleUserSelectClear}
      initSearchValue={formDataUserID}
    />
  )
}

export default UserIDInput
