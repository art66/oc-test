import React from 'react'
import cn from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/topPageLinks'
import DatePickerCalendar from './DatePickerCalendar'
import { getDatePickerState, getTimeFrom, getTimeTo } from '../../selectors'
import { addLeadZero, checkEqualityOfDays } from '../../utils'
import { setSelectedDay, toggleDatePicker, updateFormData } from '../../actions'
import { CALENDAR_ICON_PRESET, PRESET_TYPE } from '../../constants'
import { TIME_FROM_FORM_DATA } from '../../constants/formData'
import { TIME_FROM_FILTER_DATA } from '../../constants/filterData'
import { CALENDAR_ICON } from '../../constants/iconsDimensions'
import s from './styles/DatePicker.cssmodule.scss'

const DatePicker = () => {
  const dispatch = useDispatch()
  const timeFrom = useSelector(getTimeFrom)
  const timeTo = useSelector(getTimeTo)
  const open = useSelector(getDatePickerState)

  const handleToggle = () => {
    if (open && timeFrom && !timeTo) {
      dispatch(setSelectedDay(TIME_FROM_FILTER_DATA, null))
      dispatch(updateFormData(TIME_FROM_FORM_DATA, null))
    }

    dispatch(toggleDatePicker(!open))
  }

  const renderLabel = () => {
    if (timeTo) {
      const dateFrom = `${addLeadZero(timeFrom.day)}/${addLeadZero(timeFrom.month)}/${timeFrom.year}`

      if (checkEqualityOfDays(timeFrom, timeTo)) {
        return (
          <span className={s.selectedDateWrapper}>
            <SVGIconGenerator
              iconName='Calendar'
              iconsHolder={iconsHolder}
              customClass={s.calendarIcon}
              preset={{ type: PRESET_TYPE, subtype: CALENDAR_ICON_PRESET }}
              dimensions={CALENDAR_ICON}
            />
            <span className={s.selectedDate}>{dateFrom}</span>
          </span>
        )
      }

      return (
        <span className={s.selectedDateWrapper}>
          <SVGIconGenerator
            iconName='Calendar'
            iconsHolder={iconsHolder}
            customClass={s.calendarIcon}
            preset={{ type: PRESET_TYPE, subtype: CALENDAR_ICON_PRESET }}
            dimensions={CALENDAR_ICON}
          />
          <span className={s.selectedDate}>
            {`${dateFrom} - ${addLeadZero(timeTo.day)}/${addLeadZero(timeTo.month)}/${timeTo.year}`}
          </span>
        </span>
      )
    }

    return <span>All Time</span>
  }

  return (
    <div className={cn(s.datePicker, { [s.open]: open })}>
      <button type='button' className={s.toggler} onClick={handleToggle} aria-expanded={open} aria-haspopup='dialog'>
        {renderLabel()}
      </button>
      {open && <DatePickerCalendar />}
    </div>
  )
}

export default React.memo(DatePicker)
