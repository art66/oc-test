import React from 'react'
import cn from 'classnames'
import { useSelector } from 'react-redux'
import DefaultFilters from './DefaultFilters'
import ExtendedFilters from './ExtendedFilters'
import { getIsStaff } from '../../selectors'
import s from './styles/index.cssmodule.scss'

const FiltersPanel = () => {
  const isStaff = useSelector(getIsStaff)

  return (
    <div className={cn(s.filtersPanel, { staff: isStaff })}>
      <DefaultFilters />
      {isStaff && <ExtendedFilters />}
    </div>
  )
}

export default React.memo(FiltersPanel)
