import React from 'react'
import CategoriesDropdown from './CategoriesDropdown'
import TypesDropdown from './TypesDropdown'
import DatePicker from './DatePicker'
import s from './styles/DefaultFilters.cssmodule.scss'

const DefaultFilters = () => {
  return (
    <div className={s.defaultFilters}>
      <CategoriesDropdown />
      <TypesDropdown />
      <DatePicker />
    </div>
  )
}

export default DefaultFilters
