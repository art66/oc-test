import React, { useCallback } from 'react'
import Dropdown from '@torn/shared/components/Dropdown'
import { useDispatch, useSelector } from 'react-redux'
import { getCategories, getSelectedCategory } from '../../selectors'
import { selectCategory } from '../../actions'
import IDropdownItem from '../../interfaces/IDropdownItem'

const CategoriesDropdown = () => {
  const dispatch = useDispatch()
  const categories = useSelector(getCategories)
  const selectedCategory = useSelector(getSelectedCategory)

  const handleSelect = useCallback(
    (value: IDropdownItem) => {
      if (value.ID === selectedCategory.ID) return false

      dispatch(selectCategory(value))
    },
    [dispatch, selectedCategory]
  )

  return <Dropdown list={categories} search={true} selected={selectedCategory} onChange={handleSelect} />
}

export default CategoriesDropdown
