import React from 'react'
import UserIDInput from './UserIDInput'
import SearchTextInput from './SearchTextInput'
import RowsInputField from './RowsInputField'
import s from './styles/ExtendedFilters.cssmodule.scss'

const ExtendedFilters = () => {
  return (
    <div className={s.extendedFilters}>
      <UserIDInput />
      <SearchTextInput />
      <RowsInputField />
    </div>
  )
}

export default ExtendedFilters
