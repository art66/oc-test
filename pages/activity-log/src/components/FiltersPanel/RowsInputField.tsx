import React, { ChangeEvent } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getAmountRows, getFormDataAmountRows } from '../../selectors'
import { fetchLogs, updateFilter, updateFormData } from '../../actions'
import { ROWS_FORM_DATA } from '../../constants/formData'
import { ROWS_FILTER_DATA } from '../../constants/filterData'
import s from './styles/ExtendedFilters.cssmodule.scss'

const RowsInputField = () => {
  const dispatch = useDispatch()
  const amountRows = useSelector(getAmountRows)
  const formDataAmountRows = useSelector(getFormDataAmountRows)

  const handleInput = (e: ChangeEvent<HTMLInputElement>) => {
    const amount = e.target.value.replace(/(?![0-9])./g, '')

    dispatch(updateFilter(ROWS_FILTER_DATA, amount))
  }

  const handleBlur = () => {
    if (formDataAmountRows !== amountRows) {
      dispatch(updateFormData(ROWS_FORM_DATA, amountRows))
      dispatch(fetchLogs())
    }
  }

  return (
    <input
      className={s.inputField}
      placeholder='Amount of rows'
      type='text'
      value={amountRows}
      onChange={handleInput}
      onBlur={handleBlur}
      data-lpignore={true}
    />
  )
}

export default RowsInputField
