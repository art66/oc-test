import React, { ChangeEvent } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getFormDataSearchText, getSearchText } from '../../selectors'
import { fetchLogs, updateFilter, updateFormData } from '../../actions'
import { TEXT_FORM_DATA } from '../../constants/formData'
import { TEXT_FILTER_DATA } from '../../constants/filterData'
import s from './styles/ExtendedFilters.cssmodule.scss'

const SearchTextInput = () => {
  const dispatch = useDispatch()
  const searchText = useSelector(getSearchText)
  const formDataSearchText = useSelector(getFormDataSearchText)

  const handleInput = (e: ChangeEvent<HTMLInputElement>) => {
    dispatch(updateFilter(TEXT_FILTER_DATA, e.target.value))
  }

  const handleBlur = () => {
    if (searchText !== formDataSearchText) {
      dispatch(updateFormData(TEXT_FORM_DATA, searchText))
      dispatch(fetchLogs())
    }
  }

  return (
    <input
      className={s.inputField}
      type='text'
      placeholder='Search text'
      value={searchText}
      onChange={handleInput}
      onBlur={handleBlur}
    />
  )
}

export default SearchTextInput
