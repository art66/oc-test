import React, { useCallback, useEffect } from 'react'
import Dropdown from '@torn/shared/components/Dropdown'
import { useDispatch, useSelector } from 'react-redux'
import { getFormDataSelectedType, getSelectedType, getTypes } from '../../selectors'
import { selectType, toggleView, updateFilter, updateFormData } from '../../actions'
import { DEFAULT_TYPE_DROPDOWN_VALUE, SENTENCE_VIEW, TYPES_LIMIT } from '../../constants'
import { TYPE_FORM_DATA } from '../../constants/formData'
import { TYPE_FILTER_DATA } from '../../constants/filterData'
import { getTypesData } from '../../utils'
import IDropdownItem from '../../interfaces/IDropdownItem'

const TypesDropdown = () => {
  const dispatch = useDispatch()
  const types = useSelector(getTypes)
  const selectedType = useSelector(getSelectedType)
  const formDataSelectedType = useSelector(getFormDataSelectedType)

  const getSelectedItem = () => {
    if (!selectedType) {
      return [DEFAULT_TYPE_DROPDOWN_VALUE]
    }

    return selectedType
  }

  const handleSelect = useCallback(
    (value: IDropdownItem[]) => {
      if (JSON.stringify(formDataSelectedType) === JSON.stringify(getTypesData(value))) return false

      dispatch(selectType(value))
    },
    [dispatch, formDataSelectedType]
  )

  useEffect(() => {
    if (!formDataSelectedType || formDataSelectedType.length > 1) {
      dispatch(toggleView(SENTENCE_VIEW))
    }
  }, [dispatch, formDataSelectedType])

  useEffect(() => {
    if (selectedType?.length === 0) {
      dispatch(updateFilter(TYPE_FILTER_DATA, [DEFAULT_TYPE_DROPDOWN_VALUE]))
      dispatch(updateFormData(TYPE_FORM_DATA, null))
    }
  }, [dispatch, selectedType])

  return (
    <Dropdown
      id='types-multiselect'
      list={types}
      search={true}
      limit={TYPES_LIMIT}
      selected={getSelectedItem()}
      onChange={handleSelect}
      multiselect={true}
    />
  )
}

export default TypesDropdown
