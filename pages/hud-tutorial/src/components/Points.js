import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Points.cssmodule.scss'
import Point from './Point'
import Tooltip from './Tooltip'
import { connect } from 'react-redux'
import classnames from 'classnames'

class Points extends Component {
  render() {
    const { money, level } = this.props.point
    const { currStep, name } = this.props
    const c = classnames({
      [s['points-mobile']]: true,
      [s['active']]: currStep === 6,
      [s['checked']]: currStep > 7
    })

    if (this.props.isMobileMode) {
      return (
        <div className={c} id={'pointsMoney'}>
          <Point data={money} type={'money'} />
          {currStep === 6 || (this.props.isMobileMode && currStep > 7) ? (
            <Tooltip position="top" arrow="center" parent="pointsMoney">
              <span>You have $250</span>
            </Tooltip>
          ) : (
            ''
          )}
        </div>
      )
    }

    return (
      <div className={s['points']}>
        <Point data={{ key: 'Name', value: name }} type={'username'} notChecked={currStep < 2} />
        <Point data={money} type={'money'} isActive={currStep === 2} notChecked={currStep < 2} />
        <Point data={level} type={'level'} isActive={currStep === 3} notChecked={currStep < 3} />
      </div>
    )
  }
}

Points.propTypes = {
  point: PropTypes.object,
  isMobileMode: PropTypes.bool,
  currStep: PropTypes.number,
  name: PropTypes.string
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep,
  isMobileMode: state.browser.lessThan.tablet
})

export default connect(mapStateToProps)(Points)
