import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Information.cssmodule.scss'
import classnames from 'classnames'
import { connect } from 'react-redux'

class Information extends Component {
  render() {
    const { children, isMobileMode, currStep } = this.props
    const c = classnames({
      [s['user-information']]: !isMobileMode,
      [s['user-information-mobile']]: isMobileMode,
      [s['not-checked']]: isMobileMode && currStep < 6
    })
    return (
      <div className={c}>
        <h2 className={s['header']}>Information</h2>
        <div className={s['toggle-content']}>
          <div className={s['content']}>{children}</div>
        </div>
      </div>
    )
  }
}

Information.propTypes = {
  children: PropTypes.node,
  isMobileMode: PropTypes.bool,
  currStep: PropTypes.number
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep,
  isMobileMode: state.browser.lessThan.tablet
})

export default connect(mapStateToProps)(Information)
