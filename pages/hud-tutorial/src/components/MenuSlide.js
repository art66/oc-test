import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/MenuSlider.cssmodule.scss'

class MenuSlide extends Component {
  render() {
    const { slide } = this.props
    return (
      <div className={s['slide-mobile']}>
        <a className={s['slide-row']} href={'#'}>
          <i className={s[slide.icon.toLowerCase() + '-icon'] + ' ' + s['icon']} />
          <span>
            {slide.hasOwnProperty('shortName') && slide.shortName ? slide.shortName : slide.name.toLowerCase()}
          </span>
        </a>
      </div>
    )
  }
}

MenuSlide.propTypes = {
  slide: PropTypes.object
}

export default MenuSlide
