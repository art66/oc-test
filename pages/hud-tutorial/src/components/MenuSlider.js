import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import s from '../styles/MenuSlider.cssmodule.scss'
import { Swiper, Slide } from 'react-dynamic-swiper'
import MenuSlide from './MenuSlide'
import { connect } from 'react-redux'
import { toggleAnimation, resetSteps } from '../modules'
let timeout1, timeout2, timeout3, timeout4

class MenuSlider extends Component {
  componentDidMount() {
    this.props.resetSteps()
    let width = this.props.width
    this.startAnimation(width)
  }

  animateSlider = transform => {
    clearTimeout(timeout1)
    clearTimeout(timeout2)
    clearTimeout(timeout3)
    clearTimeout(timeout4)
    let props = this.props
    this.props.toggleAnimation(true)
    let slider = document.getElementsByClassName('swiper-wrapper')[0]
    timeout1 = setTimeout(function() {
      slider.style.transform = `translate3d(0, 0, 0)`
    }, 0)
    timeout2 = setTimeout(function() {
      slider.style.transform = `translate3d(-${transform}px, 0, 0)`
    }, 10)
    timeout3 = setTimeout(function() {
      slider.style.transform = 'translate3d(0px, 0, 0)'
    }, 4000)
    timeout4 = setTimeout(function() {
      props.toggleAnimation(false)
    }, 8000)
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillUpdate(nextProps) {
  //   if (
  //     (this.props.tutorial.currStep !== 1 && nextProps.tutorial.currStep === 1) ||
  //     (this.props.width !== nextProps.width && this.props.tutorial.currStep === 1)
  //   ) {
  //     let width = this.props.width
  //     this.startAnimation(width)
  //   }
  // }

  componentDidUpdate(prevProps) {
    if (
      (prevProps.tutorial.currStep !== 1 && this.props.tutorial.currStep === 1) ||
      (prevProps.width !== this.props.width && prevProps.tutorial.currStep === 1)
    ) {
      let width = prevProps.width
      this.startAnimation(width)
    }
  }

  startAnimation = width => {
    if (width >= 550) {
      this.animateSlider(width - 212)
    } else if (width >= 450) {
      this.animateSlider(width - 31)
    } else if (width >= 400) {
      this.animateSlider(width + 143)
    } else if (width >= 350) {
      this.animateSlider(width + 230)
    } else {
      this.animateSlider(width * 2 + 10)
    }
  }

  getAreasAmount(width) {
    let areasAmount

    if (width >= 550) {
      areasAmount = 13
    } else if (width >= 450) {
      areasAmount = 11
    } else if (width >= 400) {
      areasAmount = 9
    } else if (width >= 350) {
      areasAmount = 8
    } else {
      areasAmount = 7
    }

    return areasAmount
  }

  render() {
    const { slides, width, tutorial } = this.props

    const options = {
      slidesPerView: this.getAreasAmount(width),
      slidesPerGroup: this.getAreasAmount(width)
    }

    const cDisableSlider = classnames({
      [s['disable-slider']]: tutorial.sliderAnimationOn
    })

    let keySorted = Object.keys(slides).sort(function(a, b) {
      return slides[a].linkOrder - slides[b].linkOrder
    })

    let sortSlides = {}

    keySorted.map(function(item) {
      sortSlides[item] = slides[item]
    })

    return (
      <div className={s['menu-mobile']}>
        <div className={cDisableSlider} />
        <Swiper
          pagination={false}
          swiperOptions={options}
          prevButton={<div className={`${s['swiper-button']} ${s['button-prev']}`} />}
          nextButton={<div className={`${s['swiper-button']} ${s['button-next']}`} />}
        >
          {Object.keys(sortSlides).map((slide, i) => {
            return (
              <Slide key={i}>
                <MenuSlide slide={sortSlides[slide]} />
              </Slide>
            )
          })}
        </Swiper>
      </div>
    )
  }
}

MenuSlider.propTypes = {
  slides: PropTypes.object,
  width: PropTypes.number,
  toggleAnimation: PropTypes.func,
  tutorial: PropTypes.object,
  isMobileMode: PropTypes.bool,
  resetSteps: PropTypes.func
}

const mapStateToProps = state => ({
  width: state.browser.width,
  tutorial: state.tutorial,
  isMobileMode: state.browser.lessThan.tablet
})

const mapDispatchToProps = {
  toggleAnimation,
  resetSteps
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuSlider)
