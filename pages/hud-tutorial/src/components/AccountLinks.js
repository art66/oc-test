import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/AccountLinks.cssmodule.scss'
import classnames from 'classnames'
import { connect } from 'react-redux'

class AccountLinks extends Component {
  render() {
    const { account, currStep } = this.props
    const c = classnames({
      [s['account-links']]: true,
      [s['active']]: currStep === 8,
      [s['not-checked']]: currStep < 8
    })
    return (
      <div className={s['account-links-wrap']}>
        <div className={c}>
          {Object.keys(account).map((item, i) => {
            return (
              <div key={i} className={s['wrap']}>
                <a className={s[account[item].name]} aria-label={account[item].name} />
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}

AccountLinks.propTypes = {
  account: PropTypes.object,
  currStep: PropTypes.number
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep
})

export default connect(mapStateToProps)(AccountLinks)
