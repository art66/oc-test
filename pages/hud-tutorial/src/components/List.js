import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/List.cssmodule.scss'

class List extends Component {
  render() {
    const { list } = this.props
    return (
      <div className={s['list-desktop']}>
        <div className={s['list-row']}>
          <a href={'#'}>
            <i className={s[list.icon + '-icon'] + ' ' + s['icon']} />
            <span>{list.name}</span>
          </a>
          <div className={s['info']}>
            <span className={s['amount']}>0</span>
            <span className={s['arrow'] + ' ' + s['disabled']} />
          </div>
        </div>
      </div>
    )
  }
}

List.propTypes = {
  list: PropTypes.object
}

export default List
