import React, { Component } from 'react'
import s from '../styles/StatusIcons.cssmodule.scss'
import StatusIcon from './StatusIcon'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { connect } from 'react-redux'

class StatusIcons extends Component {
  render() {
    const { icons, isMobileMode, currStep, gender } = this.props

    const c = classnames({
      [s['status-icons']]: true,
      [s['big']]: true,
      [s['mobile']]: isMobileMode,
      [s['active']]: currStep === 1,
      [s['not-checked']]: isMobileMode && currStep < 7
    })

    return (
      <ul className={c}>
        <StatusIcon icon={gender === 'Male' ? icons.maleGender : icons.femaleGender} />
      </ul>
    )
  }
}

StatusIcons.propTypes = {
  icons: PropTypes.object,
  isMobileMode: PropTypes.bool,
  currStep: PropTypes.number,
  gender: PropTypes.string
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep,
  isMobileMode: state.browser.lessThan.tablet
})

export default connect(mapStateToProps)(StatusIcons)
