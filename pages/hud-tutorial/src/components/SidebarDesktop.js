import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/SidebarDesktop.cssmodule.scss'

class SidebarDesktop extends Component {
  render() {
    const { children } = this.props
    return <div className={s['sidebar']}>{children}</div>
  }
}

SidebarDesktop.propTypes = {
  children: PropTypes.node
}

export default SidebarDesktop
