import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Areas.cssmodule.scss'

class Area extends Component {
  render() {
    const { area } = this.props
    return (
      <div className={s['area-desktop']}>
        <div className={s['area-row']}>
          <a href={'#'}>
            <i className={s[area.icon + '-icon'] + ' ' + s['icon']} />
            <span>{area.name}</span>
          </a>
        </div>
      </div>
    )
  }
}

Area.propTypes = {
  area: PropTypes.object
}

export default Area
