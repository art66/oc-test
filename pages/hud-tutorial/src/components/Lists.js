import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/List.cssmodule.scss'
import List from './List'
import classnames from 'classnames'
import { connect } from 'react-redux'

class Lists extends Component {
  render() {
    const { lists, currStep } = this.props
    const c = classnames({
      [s['lists']]: true,
      [s['active']]: currStep === 9,
      [s['not-checked']]: currStep < 9
    })
    return (
      <div className={c}>
        <div className={s['toggle-block']}>
          <h2 className={s['header'] + ' ' + s['desktop']}>
            <span>Lists</span>
            <i className={s['header-arrow']} />
          </h2>
          {Object.keys(lists).map((list, i) => {
            return <List list={lists[list]} listName={list} key={i} field={list} />
          })}
        </div>
      </div>
    )
  }
}

Lists.propTypes = {
  lists: PropTypes.object,
  currStep: PropTypes.number
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep
})

export default connect(mapStateToProps)(Lists)
