import React, { Component } from 'react'
import PropTypes from 'prop-types'
import '../styles/SidebarMobile.cssmodule.scss'

class SidebarMobile extends Component {
  render() {
    const { children } = this.props
    return <div>{children}</div>
  }
}

SidebarMobile.propTypes = {
  children: PropTypes.node
}

export default SidebarMobile
