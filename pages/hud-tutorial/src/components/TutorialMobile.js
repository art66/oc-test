import React, { Component } from 'react'
import PropTypes from 'prop-types'

class TutorialMobile extends Component {
  render() {
    const { children } = this.props
    return <div>{children}</div>
  }
}

TutorialMobile.propTypes = {
  children: PropTypes.node
}

export default TutorialMobile
