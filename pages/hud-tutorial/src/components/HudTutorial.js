import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/SidebarDesktop.cssmodule.scss'
import '../styles/HudTutorial.cssmodule.scss'
import SidebarDesktop from './SidebarDesktop'
import SidebarMobile from './SidebarMobile'
import Information from './Information'
import StatusIcons from './StatusIcons'
import Points from './Points'
import ProgressBars from './ProgressBars'
import AccountLinks from './AccountLinks'
import MenuSlider from './MenuSlider'
import TutorialMobile from './TutorialMobile'
import TutorialDesktop from './TutorialDesktop'
import Chats from './Chats'
import TutorialTooltip from './TutorialTooltip'
import Areas from './Areas'
import Lists from './Lists'
import BlocksFrame from './BlocksFrame'
import classnames from 'classnames'
import userData from '../user-data'
import tooltipData from '../tooltip-data'

const STEPS_AMOUNT_MOBILE = 8
const STEPS_AMOUNT = 10

class HudTutorial extends Component {
  constructor(props) {
    super(props)
    this.decreaseStep = this.props.decreaseStep
  }

  shouldComponentUpdate(nextProps) {
    if (!this.props.browser.lessThan.tablet && nextProps.browser.lessThan.tablet) {
      let currStep = this.props.tutorial.currStep
      if (currStep > STEPS_AMOUNT_MOBILE) {
        this.decreaseStep(currStep - STEPS_AMOUNT_MOBILE)
        return false
      }
    }
    return true
  }

  findGetParameter = parameterName => {
    let result = null
    let tmp = []
    let items = location.search.substr(1).split('&')
    for (let index = 0; index < items.length; index++) {
      tmp = items[index].split('=')
      if (tmp[0] === parameterName) {
        result = decodeURIComponent(tmp[1])
      }
    }
    return result
  }

  getName = () => {
    let name = this.findGetParameter('name')
    return !name ? 'TestPlayer' : name
  }

  getGender = () => {
    let gender = this.findGetParameter('gender')
    return !gender && (gender !== 'Male' || gender !== 'Female') ? 'Female' : gender
  }

  render() {
    const { lessThan } = this.props.browser
    const { currStep } = this.props.tutorial
    const c = classnames({
      [s['delimiter']]: true,
      [s['hidden']]: currStep < 4
    })

    let name = this.getName()
    let gender = this.getGender()

    let isMobileMode = lessThan.tablet
    return (
      <div>
        {isMobileMode ? (
          <TutorialMobile>
            <SidebarMobile>
              <MenuSlider slides={{ ...userData.areas, ...userData.lists, ...userData.account }} />
              <ProgressBars bars={userData.bars} />
              <Information>
                <Points point={userData.user} name={name} />
                <StatusIcons icons={userData.statusIcons.icons} gender={gender} />
              </Information>
            </SidebarMobile>
            {currStep === STEPS_AMOUNT_MOBILE ? <Chats data={tooltipData.mobile} /> : ''}
            {currStep < STEPS_AMOUNT_MOBILE ? <TutorialTooltip data={tooltipData.mobile} /> : ''}
          </TutorialMobile>
        ) : (
          <TutorialDesktop>
            <SidebarDesktop>
              <Information>
                <StatusIcons icons={userData.statusIcons.icons} gender={gender} />
                <hr className={s['delimiter']} />
                <Points point={userData.user} name={name} />
                <hr className={c} />
                <ProgressBars bars={userData.bars} />
              </Information>
              <AccountLinks account={userData.account} />
              <Areas areas={userData.areas} />
              <Lists lists={userData.lists} />
            </SidebarDesktop>
            {currStep === STEPS_AMOUNT ? <Chats data={tooltipData.desktop} /> : ''}
            {currStep < STEPS_AMOUNT - 1 ? <BlocksFrame /> : ''}
            {currStep < STEPS_AMOUNT ? <TutorialTooltip data={tooltipData.desktop} /> : ''}
          </TutorialDesktop>
        )}
      </div>
    )
  }
}

HudTutorial.propTypes = {
  browser: PropTypes.object,
  tutorial: PropTypes.object,
  lessThan: PropTypes.object,
  width: PropTypes.number,
  currStep: PropTypes.number,
  decreaseStep: PropTypes.func
}

export default HudTutorial
