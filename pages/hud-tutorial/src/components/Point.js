import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Points.cssmodule.scss'
import classnames from 'classnames'

class Point extends Component {
  render() {
    const { data, type, isActive, notChecked } = this.props

    const c = classnames({
      [s['point-block']]: true,
      [s['active']]: isActive,
      [s['not-checked']]: notChecked
    })

    return (
      <p className={c}>
        <span className={s['name']}>{data.key}:</span>
        <span className={s['value'] + ' ' + s[type]}>
          {type === 'money' ? '$' : ''}
          {data.value}
        </span>
      </p>
    )
  }
}

Point.propTypes = {
  data: PropTypes.object,
  type: PropTypes.string,
  isActive: PropTypes.bool,
  notChecked: PropTypes.bool
}

export default Point
