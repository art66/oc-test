import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Frame.cssmodule.scss'
import { connect } from 'react-redux'

class BlocksFrame extends Component {
  render() {
    const { currStep } = this.props
    return <div className={s['frame'] + ' ' + s[`step-${currStep}`]} />
  }
}

BlocksFrame.propTypes = {
  currStep: PropTypes.number
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep
})

export default connect(mapStateToProps)(BlocksFrame)
