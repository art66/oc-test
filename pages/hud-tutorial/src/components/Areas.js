import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Areas.cssmodule.scss'
import Area from './Area'
import classnames from 'classnames'
import { connect } from 'react-redux'

class Areas extends Component {
  render() {
    const { areas, currStep } = this.props
    const c = classnames({
      [s['areas']]: true,
      [s['active']]: currStep === 9,
      [s['not-checked']]: currStep < 9
    })
    return (
      <div className={c}>
        <h2 className={s['header']}>
          <span>Areas</span>
          <i className={s['header-arrow']} />
        </h2>
        <div className={s['toggle-content']}>
          {Object.keys(areas).map((area, i) => {
            return <Area area={areas[area]} key={i} />
          })}
        </div>
      </div>
    )
  }
}

Areas.propTypes = {
  areas: PropTypes.object,
  currStep: PropTypes.number
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep
})

export default connect(mapStateToProps)(Areas)
