import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { connect } from 'react-redux'
import s from '../styles/TutorialTooltip.cssmodule.scss'
import { decreaseStep, increaseStep } from '../modules'

const LAST_STEP_ON_THE_MOBILE = 8
const LAST_STEP_ON_THE_DESKTOP = 10
let moveBack = false

class TutorialTooltip extends Component {
  checkLastStep = () => {
    return (
      (this.props.isMobileMode && this.props.currStep === LAST_STEP_ON_THE_MOBILE) ||
      (!this.props.isMobileMode && this.props.currStep === LAST_STEP_ON_THE_DESKTOP)
    )
  }

  getData = (data, id) => {
    let tooltipData = {}
    Object.keys(data).map(function(key, i) {
      if (data[key].id === id) {
        tooltipData = data[key]
      }
    })

    return tooltipData
  }

  componentDidUpdate(prevProps) {
    const { currStep } = prevProps
    const { isMobileMode, currStep: nextStep } = this.props

    if (isMobileMode && nextStep === 4 && currStep === 5) {
      moveBack = true
    } else {
      moveBack = false
    }
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.isMobileMode && nextProps.currStep === 4 && this.props.currStep === 5) {
  //     moveBack = true
  //   } else {
  //     moveBack = false
  //   }
  // }

  backStep = () => {
    this.props.decreaseStep(1)
    let slider = document.getElementsByClassName('swiper-wrapper')[0]
    if (this.props.isMobileMode && this.props.currStep === 2) {
      slider.style.transform = 'translate3d(0px, 0, 0)'
      slider.style.transition = '0ms'
    }
  }

  render() {
    const { isMobileMode, currStep, data, increaseStep, sliderAnimationOn } = this.props

    const c = classnames({
      [s['tutorial-tooltip']]: true,
      [s['mobile']]: isMobileMode,
      [s[`step-${currStep}`]]: true,
      [s['back']]: moveBack
    })

    const cContinueBtn = classnames({
      [s['button']]: true,
      [s['continue']]: true,
      [s['not-active']]: isMobileMode && sliderAnimationOn
    })
    let tooltipData = this.getData(data, currStep)
    let isLastStep = this.checkLastStep()

    return (
      <div className={c}>
        <h3>{tooltipData.title}</h3>
        {tooltipData.text.map(function(item, i) {
          return <p key={i} dangerouslySetInnerHTML={{ __html: item }} />
        })}
        <div className={s['button-wrapper']}>
          {isLastStep || currStep !== 1 ? (
            <a
              href="#"
              className={s['button'] + ' ' + s['back']}
              onClick={this.backStep}
              aria-label="Go to the previous step"
            >
              Back
            </a>
          ) : (
            ''
          )}
          <a
            className={cContinueBtn}
            onClick={increaseStep}
            href={isLastStep ? '/loader.php?sid=missions' : '#'}
            aria-label="Go to the next step"
          >
            Continue
          </a>
        </div>
      </div>
    )
  }
}

TutorialTooltip.propTypes = {
  isMobileMode: PropTypes.bool,
  increaseStep: PropTypes.func,
  decreaseStep: PropTypes.func,
  currStep: PropTypes.number,
  data: PropTypes.object,
  sliderAnimationOn: PropTypes.bool
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep,
  sliderAnimationOn: state.tutorial.sliderAnimationOn,
  isMobileMode: state.browser.lessThan.tablet
})

const mapDispatchToProps = {
  decreaseStep,
  increaseStep
}

export default connect(mapStateToProps, mapDispatchToProps)(TutorialTooltip)
