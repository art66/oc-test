import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Chats.cssmodule.scss'
import TutorialTooltip from './TutorialTooltip'
import { connect } from 'react-redux'

const STEPS_AMOUNT = 10
const STEPS_AMOUNT_MOBILE = 8

class Chats extends Component {
  render() {
    const { currStep, isMobileMode, data } = this.props
    return (
      <div className={s['chat-box-wrap']}>
        <div className={s['chat-box-settings']}>
          <a className={s['chat-box-head']} href="#">
            <div className={s['chat-box-title']}>
              <i className={s['icon']} />
            </div>
            <div className={s['left']} />
            <div className={s['right']} />
          </a>
        </div>
        <div className={s['chat-box-people']}>
          <a className={s['chat-box-head']} href="#">
            <div className={s['chat-box-title']}>
              <i className={s['icon']} />
            </div>
            <div className={s['left']} />
            <div className={s['right']} />
          </a>
        </div>
        <div className={s['chat-box-new-players']}>
          <a className={s['chat-box-head']} href="#">
            <div className={s['chat-box-title']}>
              <i className={s['icon']} />
              <span className={s['name']}>New players</span>
            </div>
            <div className={s['left']} />
            <div className={s['right']} />
          </a>
        </div>
        <div className={s['chat-box-trade']}>
          <a className={s['chat-box-head']} href="#">
            <div className={s['chat-box-title']}>
              <i className={s['icon']} />
              <span className={s['name']}>Trade</span>
            </div>
            <div className={s['left']} />
            <div className={s['right']} />
          </a>
        </div>
        {currStep === STEPS_AMOUNT || (currStep === STEPS_AMOUNT_MOBILE && isMobileMode) ? (
          <TutorialTooltip data={data} />
        ) : (
          ''
        )}
      </div>
    )
  }
}

Chats.propTypes = {
  currStep: PropTypes.number,
  isMobileMode: PropTypes.bool,
  data: PropTypes.object
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep,
  isMobileMode: state.browser.lessThan.tablet
})

export default connect(mapStateToProps)(Chats)
