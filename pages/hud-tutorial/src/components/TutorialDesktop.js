import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/TutorialDesktop.cssmodule.scss'
import { resetSteps } from '../modules'
import { connect } from 'react-redux'

class TutorialDesktop extends Component {
  componentDidMount() {
    this.props.resetSteps()
  }

  componentWillUnmout() {}

  render() {
    const { children } = this.props
    return (
      <div className={s['tutorial-desktop']}>
        {children}
        <div className={s['content-wrapper']}>
          <hr className={s['page-head-delimiter']} />
        </div>
      </div>
    )
  }
}

TutorialDesktop.propTypes = {
  children: PropTypes.node,
  resetSteps: PropTypes.func
}

const mapStateToProps = state => ({})

const mapDispatchToProps = {
  resetSteps
}

export default connect(mapStateToProps, mapDispatchToProps)(TutorialDesktop)
