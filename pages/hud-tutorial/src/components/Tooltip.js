import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Tooltip.cssmodule.scss'
import ToolTip from 'react-portal-tooltip'

class Tooltip extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isTooltipActive: false
    }
  }

  componentDidMount() {
    this.parent = document.getElementById(this.props.parent)
    if (!this.parent) {
      return
    }

    window.addEventListener('click', this.hideContextMenuOnContentClick)
    this.parent.addEventListener('blur', this.hideTooltip)
    this.parent.addEventListener('focus', this.showTooltip)
    this.parent.addEventListener('touchstart', this.showTooltip)
    this.parent.addEventListener('mouseleave', this.hideTooltip)
    this.parent.addEventListener('mouseenter', this.showTooltip)
    this.parent.addEventListener('touchstart', this.showTooltip)
  }

  componentWillUnmount() {
    if (!this.parent) {
      return
    }

    window.removeEventListener('click', this.hideContextMenuOnContentClick)
    this.parent.removeEventListener('blur', this.hideTooltip)
    this.parent.removeEventListener('focus', this.showTooltip)
    this.parent.removeEventListener('touchstart', this.showTooltip)
    this.parent.removeEventListener('mouseleave', this.hideTooltip)
    this.parent.removeEventListener('mouseenter', this.showTooltip)
    this.parent.removeEventListener('touchstart', this.toggleTooltip)
  }

  hideContextMenuOnContentClick = e => {
    if (e && e.target && e.target.closest('#tutorialroot') && e.target.closest('#tutorialroot').length === 0) {
      this.hideTooltip()
    }
  }

  showTooltip = () => {
    this.setState({ isTooltipActive: true })
  }

  hideTooltip = () => {
    this.setState({ isTooltipActive: false })
  }

  toggleTooltip = () => {
    this.setState({ isTooltipActive: !this.state.isTooltipActive })
  }

  render() {
    let style = {
      style: {
        background: '#f2f2f2',
        padding: '5px 8px'
      },
      arrowStyle: {
        color: '#f2f2f2',
        borderColor: '#dadada',
        transition: 'none'
      }
    }

    const { children, position, arrow, parent, isMobileMode } = this.props
    const speed = isMobileMode ? 0 : 500
    return (
      <ToolTip
        active={this.state.isTooltipActive}
        position={position}
        arrow={arrow}
        parent={'#' + parent}
        style={style}
        tooltipTimeout={speed}
      >
        <div className={s['tooltip']}>{children}</div>
      </ToolTip>
    )
  }
}

Tooltip.propTypes = {
  children: PropTypes.node,
  position: PropTypes.string,
  arrow: PropTypes.string,
  parent: PropTypes.string,
  windowSize: PropTypes.string,
  mediaType: PropTypes.string,
  isMobileMode: PropTypes.bool,
  iconTip: PropTypes.string
}

export default Tooltip
