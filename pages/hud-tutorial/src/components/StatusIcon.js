import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/StatusIcons.cssmodule.scss'
import Tooltip from './Tooltip'
import { connect } from 'react-redux'

class StatusIcon extends Component {
  render() {
    const { icon, currStep, isMobileMode } = this.props
    const blockId = icon.iconID
    return (
      <li className={s[icon.iconID]}>
        <a id={blockId} href={'#'} aria-label="Female" />
        {(currStep > 1 && !isMobileMode) || (isMobileMode && currStep < 7) ? (
          ''
        ) : (
          <Tooltip iconTip={'status-icon'} position="top" arrow={isMobileMode ? 'right' : 'center'} parent={blockId}>
            <span>{icon.title}</span>
          </Tooltip>
        )}
      </li>
    )
  }
}

StatusIcon.propTypes = {
  icon: PropTypes.object,
  currStep: PropTypes.number,
  isMobileMode: PropTypes.bool
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep,
  isMobileMode: state.browser.lessThan.tablet
})

export default connect(mapStateToProps)(StatusIcon)
