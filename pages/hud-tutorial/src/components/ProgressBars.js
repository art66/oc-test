import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/ProgressBar.cssmodule.scss'
import ProgressBar from './ProgressBar'
import classnames from 'classnames'
import { connect } from 'react-redux'

class ProgressBars extends Component {
  render() {
    const { bars, isMobileMode, currStep } = this.props
    const c = classnames({
      [s['bars']]: true,
      [s['bars-mobile']]: isMobileMode,
      [s['bars-desktop']]: !isMobileMode,
      [s['not-checked']]: isMobileMode && currStep < 2,
      [s['checked']]: isMobileMode && currStep > 5
    })
    let diff = isMobileMode ? 2 : 4
    return (
      <div className={c}>
        {Object.keys(bars).map((bar, i) => {
          return <ProgressBar key={i} step={i + diff} bar={bars[bar]} isMobileMode={isMobileMode} />
        })}
      </div>
    )
  }
}

ProgressBars.propTypes = {
  bars: PropTypes.object,
  isMobileMode: PropTypes.bool,
  currStep: PropTypes.number
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep,
  isMobileMode: state.browser.lessThan.tablet
})

export default connect(mapStateToProps)(ProgressBars)
