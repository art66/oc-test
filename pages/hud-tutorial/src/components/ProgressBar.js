import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/ProgressBar.cssmodule.scss'
import Tooltip from './Tooltip'
import classnames from 'classnames'
import { connect } from 'react-redux'

class ProgressBar extends Component {
  render() {
    const { bar, step, currStep, isMobileMode } = this.props
    const blockId = 'bar' + bar.name
    const c = classnames({
      [s['bar']]: true,
      [s[bar.name.toLowerCase()]]: true,
      [s['bar-mobile']]: isMobileMode,
      [s['bar-desktop']]: !isMobileMode,
      [s['active']]: step === currStep,
      [s['not-checked']]: currStep < step
    })
    return (
      <a href={'#'} className={c} id={blockId} aria-label={`${bar.name} level ${bar.amount} of ${bar.max} points`}>
        <div className={s['bar-stats']}>
          <p className={s['bar-name']}>{bar.name}:</p>
          <p className={s['bar-value']}>
            {bar.amount} / {bar.max}
          </p>
          <p className={s['bar-descr']}>FULL</p>
        </div>
        <div className={s['progress'] + ' ' + s['full']}>
          <div className={s['progress-line-timer']} />
          <div className={s['progress-line']} />
        </div>
        <div>
          <ul className={s['tick-list']}>
            {[...Array(bar.tickDividers + 1)].map((_, i) => {
              return <li key={i} style={{ width: 100 / (bar.tickDividers + 1) + '%' }} />
            })}
          </ul>
        </div>
        {step === currStep || (isMobileMode && currStep > 5) ? (
          <Tooltip
            position={isMobileMode ? 'top' : 'bottom'}
            arrow="center"
            parent={blockId}
            isMobileMode={isMobileMode}
          >
            <div className={s['progress-bar-tip-content']}>
              {isMobileMode ? (
                <p>
                  {bar.name}: {bar.amount} / {bar.max}
                </p>
              ) : (
                ''
              )}
              <span>
                <b>
                  {' '}
                  {bar.name} increased by {bar.step} every {bar.interval} minutes{' '}
                </b>
              </span>
              <p>You have full {bar.name.toLowerCase()}</p>
            </div>
          </Tooltip>
        ) : (
          ''
        )}
      </a>
    )
  }
}

ProgressBar.propTypes = {
  bar: PropTypes.object,
  step: PropTypes.number,
  currStep: PropTypes.number,
  isMobileMode: PropTypes.bool
}

const mapStateToProps = state => ({
  currStep: state.tutorial.currStep
})

export default connect(mapStateToProps)(ProgressBar)
