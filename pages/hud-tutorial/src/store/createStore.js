import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'
import makeRootReducer from './reducers'

export default (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================

  // const updateSidebarMiddleWare = store => next => action => {
  //   const sidebarLS = localStorage.getItem('sidebarData')
  //   if (sidebarLS && sidebarLS.length) {
  //     console.log('midl ', store.getState().sidebar);
  //     // localStorage.setItem('sidebarData', JSON.stringify(store.getState().sidebar))
  //   }
  //   next(action)
  // }

  const middleware = [thunk]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []
  if (__DEV__) {
    const devToolsExtension = window.devToolsExtension
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer, applyMiddleware(...middleware), ...enhancers)
  )
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
