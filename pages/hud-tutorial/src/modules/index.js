// import initialState from './initialState'

// ------------------------------------
// Constants
// ------------------------------------
export const SET_SIDEBAR_MODE = 'SET_SIDEBAR_MODE'
export const INCREASE_STEP = 'INCREASE_STEP'
export const DECREASE_STEP = 'DECREASE_STEP'
export const TOGGLE_ANIMATION = 'TOGGLE_ANIMATION'
export const RESET_STEPS = 'RESET_STEPS'
// ------------------------------------
// Middlewares
// ------------------------------------
export const increaseStep = () => {
  return {
    type: INCREASE_STEP
  }
}

export const decreaseStep = value => {
  return {
    type: DECREASE_STEP,
    value
  }
}

const saveSidebarMode = isMobileMode => {
  return {
    type: SET_SIDEBAR_MODE,
    isMobileMode
  }
}

export const setSidebarMode = () => (dispatch, getState) => {
  let isMobileMode = !getState().browser.greaterThan.mobile
  dispatch(saveSidebarMode(isMobileMode))
}

export const toggleAnimation = sliderAnimationOn => {
  return {
    type: TOGGLE_ANIMATION,
    sliderAnimationOn
  }
}

export const resetSteps = () => {
  return {
    type: RESET_STEPS
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_SIDEBAR_MODE]: (state, action) => {
    return {
      ...state,
      isMobileMode: action.isMobileMode
    }
  },
  [INCREASE_STEP]: state => {
    return {
      ...state,
      currStep: state.currStep + 1
    }
  },
  [DECREASE_STEP]: (state, action) => {
    return {
      ...state,
      currStep: state.currStep - action.value
    }
  },
  [TOGGLE_ANIMATION]: (state, action) => {
    return {
      ...state,
      sliderAnimationOn: action.sliderAnimationOn
    }
  },
  [RESET_STEPS]: state => {
    return {
      ...state,
      currStep: 1
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  currStep: 1
}

export default function sidebarReducer(state = initialState, action) {
  let handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
