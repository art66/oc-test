import { connect } from 'react-redux'
import { setSidebarMode, increaseStep, decreaseStep, toggleAnimation, resetSteps } from '../modules'

import HudTutorial from '../components/HudTutorial'

const mapStateToProps = state => ({
  tutorial: state.tutorial,
  browser: state.browser
})

const mapDispatchToProps = {
  setSidebarMode,
  increaseStep,
  decreaseStep,
  toggleAnimation,
  resetSteps
}

export default connect(mapStateToProps, mapDispatchToProps)(HudTutorial)
