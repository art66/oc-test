export type TDebugMessage = string | object

export interface IType {
  type: string
}

export interface IDebugShow extends IType {
  msg: TDebugMessage
}

export interface IInfoShow extends IType {
  msg: TDebugMessage
}

export interface ILocationChange extends IType {
  payload: {
    location: {
      hash: string
    }
  }
}
