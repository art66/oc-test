export interface IConf {
  start: number
  sector: object
  size: object
  density: object
}

export interface IBlock {
  ID: string
  name: string
  totalBlocks?: number
  totalDailyRespect?: number
  sector?: string
  size?: string
  density?: string
}

export interface IRacket {
  name: string
  territoryID: number
  description: string
}

export interface IMapInfo {
  rackets: IRacket[]
}

export interface IInitialState {
  isDesktopManualLayout?: any
  locationCurrent?: string
  appID?: string
  pageID?: number
  info?: string
  debug?: {
    msg: string | object
  }
  debugCloseAction?: () => void
  dataLoaded?: boolean
  loading?: boolean
  mapInfo?: IMapInfo
  blocks?: IBlock[]
  activeBlock?: IBlock
  activeRacket?: IRacket
  conf?: object
  blockIDScroll?: string
}
