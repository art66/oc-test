import React from 'react'
import { connect } from 'react-redux'

import DebugBox from '@torn/shared/components/DebugBox'
import InfoBox from '@torn/shared/components/InfoBox'

import TerritoryControl from '../../components/TerritoryControl'

import { infoShow, debugHide } from '../../controller/actions/common'
import { IProps } from './interfaces'
import { IReduxStore } from '../../interfaces/IReduxStore'

import styles from './index.cssmodule.scss'
import '../../styles/global.cssmodule.scss'

class AppLayout extends React.PureComponent<IProps> {
  _renderInfoDebugAreas = () => {
    const { debug, debugCloseAction, info } = this.props

    return (
      <React.Fragment>
        {debug && <DebugBox
          debugMessage={debug} close={debugCloseAction}
          isBeatifyError={true}
        />}
        {info && <InfoBox msg={info} />}
      </React.Fragment>
    )
  }

  _renderBody = () => {
    const { loading } = this.props

    if (loading) {
      return this._renderLoading()
    }
    return (
      <div className={styles.bodyWrap}>
        <TerritoryControl />
      </div>
    )
  }

  _renderLoading = () => {
    return <img src='/images/v2/main/ajax-loader.gif' className='ajax-placeholder left m-top10 m-bottom10' />
  }

  render() {
    return (
      <div className={styles.appWrap}>
        {this._renderInfoDebugAreas()}
        {this._renderBody()}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  appID: state.common.appID,
  pageID: state.common.pageID,
  debug: state.common.debug,
  info: state.common.info,
  loading: state.common.loading
})

const mapDispatchToState = dispatch => ({
  debugCloseAction: () => dispatch(debugHide()),
  infoBoxShow: (msg: string) => dispatch(infoShow(msg))
})

export default connect(mapStateToProps, mapDispatchToState)(AppLayout)
