import { IInitialState } from '../../../interfaces/IStore'

export interface IProps extends IInitialState {
  infoBoxShow: (msg: string) => void
}
