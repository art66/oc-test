const { torn } = window as any

export function initMap(
  element: HTMLElement,
  mapInfo: object,
  callback: (blockID: string) => void,
  selectAll: () => void
) {
  torn.initFactionControlMap(
    element,
    {
      isFitBlock: false,
      callback
    },
    mapInfo
  )

  torn.on('onfactionmarkerclick', selectAll)
}

export function resetMapBlock() {
  torn.callMap('resetBlock')
}

export function setMapBlock(blackID: string) {
  torn.callMap('setBlock', blackID)
}
