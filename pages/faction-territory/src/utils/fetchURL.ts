import { fetchUrl as fetchURL } from '@torn/shared/utils'

const MAIN_URL = '/war.php?'
const fetchUrl = (url: string) => {
  return fetchURL(MAIN_URL + url)
}

export default fetchUrl
