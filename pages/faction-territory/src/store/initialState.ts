import { IInitialState } from '../interfaces/IStore'

const initialState: IInitialState = {
  isDesktopManualLayout: null,
  appID: 'FACTION_CONTROLS',
  pageID: null,
  debug: null,
  info: null,
  dataLoaded: false,
  loading: true
}

export default initialState
