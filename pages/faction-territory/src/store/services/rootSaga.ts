import { all } from 'redux-saga/effects'
import createSagaMiddleware from 'redux-saga'
import createSagaMiddlewareHelpers from 'redux-saga-watch-actions/lib/middleware'
import FactionNewsSaga from '@torn/factionNews/src/sagas/factionNews'
import appSaga from '../../controller/sagas'

const sagaMiddleware = createSagaMiddleware()
const { injectSaga } = createSagaMiddlewareHelpers(sagaMiddleware)

export { injectSaga, sagaMiddleware }

export default function* rootSaga() {
  yield all([appSaga(), FactionNewsSaga()])
}
