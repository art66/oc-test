import { Store, combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'

import common from '../../controller/reducers/common'
import factionNewsReducer from '../../../../faction-news/src/reducers/factionNews'
import { IStore, IProps } from './interfaces'

export const makeRootReducer = (asyncReducers?: object) => {
  return combineReducers({
    ...asyncReducers,
    common,
    factionNews: factionNewsReducer,
    browser: createResponsiveStateReducer({
      // helps handle media screen changes
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    })
  })
}

export const injectReducer = (store: IStore & Store<any>, { key, reducer }: IProps) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return
  store.asyncReducers[key] = reducer

  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
