import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import makeRootReducer from './services/rootReducer'
import rootSaga, { sagaMiddleware } from './services/rootSaga'
import activateStoreHMR from './services/storeHMR'

import { IAsyncReducersStore } from './interfaces'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    getCookie: any
    devToolsExtension: any
  }
}

// creating saga middleware for observation
// const sagaMiddleware = createSagaMiddleware()

const rootStore = (initialState: object = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [sagaMiddleware]
  const enhancers = []

  if (__DEV__) {
    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store: any & IAsyncReducersStore = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer as any, applyMiddleware(...middleware), ...enhancers)
  )

  sagaMiddleware.run(rootSaga) // starting middleware run
  store.asyncReducers = {} // activating async reducers replacement

  activateStoreHMR(store)

  return store
}

export default rootStore()
