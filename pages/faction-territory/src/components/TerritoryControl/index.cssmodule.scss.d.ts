// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'dropdown': string;
  'globalSvgShadow': string;
  'map': string;
  'mapTabs': string;
}
export const cssExports: CssExports;
export default cssExports;
