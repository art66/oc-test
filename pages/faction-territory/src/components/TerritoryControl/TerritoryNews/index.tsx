import React, { Component } from 'react'
import FactionNews from '@torn/factionNews/src/components/App'
import IInitParams from '@torn/factionNews/src/interfaces/IInitParams'

interface IProps {
  initParams: IInitParams
}

class TerritoryNews extends Component<IProps> {
  render() {
    const { initParams } = this.props

    return <FactionNews {...initParams} />
  }
}

export default TerritoryNews
