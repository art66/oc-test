import React, { PureComponent } from 'react'
import cn from 'classnames'
import { IBlock } from '../../../../interfaces/IStore'

interface IProps {
  block: IBlock
  activeBlock: IBlock
  onClick: (data: object) => void
}

class Block extends PureComponent<IProps> {
  _handleBlockClick = () => {
    const { onClick, block } = this.props

    onClick({ blockID: block.ID })
  }

  _handleKeyDown = event => {
    if (event.keyCode === 13) {
      this._handleBlockClick()
    }
  }

  render() {
    const { block, activeBlock } = this.props

    return (
      <div key={block.ID} onClick={this._handleBlockClick} onKeyDown={this._handleKeyDown}>
        <div className='c-pointer'>
          <span className={cn({ 't-gray-3': activeBlock.ID === block.ID })}>{block.name}</span>
        </div>
      </div>
    )
  }
}

export default Block
