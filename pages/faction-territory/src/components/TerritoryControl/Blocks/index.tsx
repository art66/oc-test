import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Dropdown from '@torn/shared/components/Dropdown'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'
import { IReduxStore } from '../../../interfaces/IReduxStore'
import { IBlock } from '../../../interfaces/IStore'
import Block from './Block'
import { selectBlock as selectBlockAction, updateManualDesktopMode } from '../../../controller/actions'
import styles from '../index.cssmodule.scss'

interface IProps {
  mediaType: string
  blocks: Array<IBlock>
  activeBlock: IBlock
  selectBlock: (data: object) => void
  updateManualDesktopMode: (isDesktopMode: boolean) => void
  blockIDScroll?: string
}

class Blocks extends React.Component<IProps> {
  private _blockRefs: object = {}
  private _parentRef = React.createRef<HTMLDivElement>()

  componentDidMount() {
    subscribeOnDesktopLayout((payload: boolean) => {
      this.props.updateManualDesktopMode(payload)
    })
  }

  componentDidUpdate() {
    const { blockIDScroll, mediaType } = this.props

    if (blockIDScroll !== undefined && mediaType === 'desktop') {
      this._parentRef.current.scrollTo({
        left: 0,
        top: this._blockRefs[blockIDScroll].offsetTop,
        behavior: 'smooth'
      })
    }
  }

  componeneWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  _handleBlockChange = (block): void => {
    const { selectBlock } = this.props

    selectBlock({ blockID: block.id })
  }

  _renderBlock(block, index) {
    const { blocks, activeBlock, selectBlock } = this.props

    return (
      <li
        key={block.ID}
        className={cn({ last: index === blocks.length - 1 })}
        ref={blockRef => (this._blockRefs[block.ID] = blockRef)}
      >
        <Block block={block} activeBlock={activeBlock} onClick={selectBlock} />
      </li>
    )
  }

  _renderScrollList() {
    const { blocks } = this.props

    return (
      <div className='map-scroll'>
        <div className='viewport scrollbar-bright right' ref={this._parentRef}>
          <div className='overview'>
            <ul className='block-tabs'>
              {blocks.map((block: IBlock, index: number) => this._renderBlock(block, index))}
            </ul>
          </div>
        </div>
      </div>
    )
  }

  _renderDropdownList() {
    const { blocks, activeBlock } = this.props

    return (
      <div className='territory-drop-list small-select-menu-wrap'>
        <div className='select-wrap'>
          <Dropdown
            className={styles.dropdown}
            list={blocks.map(item => ({ id: item.ID, name: item.name }))}
            selected={{ id: activeBlock.ID, name: activeBlock.name }}
            onChange={this._handleBlockChange}
          />
        </div>
      </div>
    )
  }

  render() {
    const { mediaType } = this.props

    if (mediaType === 'desktop') {
      return this._renderScrollList()
    }
    return this._renderDropdownList()
  }
}

const mapStateToProps = (state: IReduxStore) => {
  return {
    mediaType: state.common.isDesktopManualLayout ? 'desktop' : state.browser.mediaType,
    blocks: state.common.blocks,
    activeBlock: state.common.activeBlock,
    blockIDScroll: state.common.blockIDScroll
  }
}

const mapActionsToProps = {
  selectBlock: selectBlockAction,
  updateManualDesktopMode
}

export default connect(mapStateToProps, mapActionsToProps)(Blocks)
