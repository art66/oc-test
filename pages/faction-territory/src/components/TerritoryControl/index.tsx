import React from 'react'
import cn from 'classnames'
import Blocks from './Blocks'
import Map from './Map'
import TerritoryNews from './TerritoryNews'
import styles from './index.cssmodule.scss'

class TerritoryControl extends React.Component {
  _renderTerritoryControl = () => {
    return (
      <div className='territory-control-wrp'>
        <div className='title-black m-top10 top-round'>Your Territory</div>
        <div className='cont-gray map-wrap bottom-round'>
          <div className={cn(styles.mapTabs)}>
            <Blocks />
            <Map />
            <div className='clear' />
          </div>
        </div>
        <div className='m-top15'>
          <TerritoryNews
            initParams={{
              url: '/page.php?sid=factionsNews&step=list',
              tabs: [{ name: 'Territory News', type: 5, id: 'territory_news' }]
            }}
          />
        </div>
      </div>
    )
  }

  render() {
    return this._renderTerritoryControl()
  }
}

export default TerritoryControl
