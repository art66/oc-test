import React, { PureComponent } from 'react'
import { IConf, IBlock, IRacket } from '../../../interfaces/IStore'
import nf from '../../../utils/numberFormat'
import { ALL_BLOCKS_ID } from '../../../constants'

export interface IProps {
  conf: IConf
  activeBlock: IBlock
  racket?: IRacket
}

class Details extends PureComponent<IProps> {
  _renderAllBlocksDetails() {
    const { activeBlock } = this.props

    return (
      <div className='details left'>
        <div className='l-cont'>
          <div className='first map-inf'>
            <span className='bold'>Total blocks: </span>
            <span className='value'>{nf(activeBlock.totalBlocks)}</span>
          </div>
          <div className='map-inf'>
            <span className='bold'>Total daily respect: </span>
            <span className='value'>{nf(activeBlock.totalDailyRespect)}</span>
          </div>
          <div className='map-inf' />
        </div>
        <div className='r-cont'>
          <div className='map-inf' />
          <div className='map-inf' />
          <div className='map-inf last' />
        </div>
        <div className='clear' />
      </div>
    )
  }

  _countRespect(activeBlock, conf) {
    return nf(
      Math.round(
        conf.start *
          conf.sector[activeBlock.sector].value *
          conf.size[activeBlock.size].value *
          conf.density[activeBlock.density].value
      )
    )
  }

  _renderBonus() {
    const { racket } = this.props

    if (racket !== undefined) {
      return (
        <span className='value' title={racket.description}>
          {racket.name}
        </span>
      )
    }
    return <span className='value'>N/A</span>
  }

  _renderBlockDetails() {
    const { activeBlock, conf } = this.props

    return (
      <div className='details left'>
        <div className='l-cont'>
          <div className='first map-inf'>
            <span className='bold'>Name: </span>
            <span className='value'>{activeBlock.name}</span>
          </div>
          <div className='map-inf'>
            <span className='bold'>Daily respect: </span>
            <span className='value'>{this._countRespect(activeBlock, conf)}</span>
          </div>
          <div className='map-inf'>
            <span className='bold'>Sector: </span>
            <span className='value'>{conf.sector[activeBlock.sector].text}</span>
          </div>
        </div>
        <div className='r-cont'>
          <div className='map-inf'>
            <span className='bold'>Size: </span>
            <span className='value'>{conf.size[activeBlock.size].text}</span>
          </div>
          <div className='map-inf'>
            <span className='bold'>Density: </span>
            <span className='value'>{conf.density[activeBlock.density].text}</span>
          </div>
          <div className='map-inf last'>
            <span className='bold'>Bonus: </span>
            {this._renderBonus()}
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { activeBlock } = this.props

    if (activeBlock.ID === ALL_BLOCKS_ID) {
      return this._renderAllBlocksDetails()
    }
    return this._renderBlockDetails()
  }
}

export default Details
