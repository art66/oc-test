import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { IReduxStore } from '../../../interfaces/IReduxStore'
import { IConf, IBlock, IRacket, IMapInfo } from '../../../interfaces/IStore'
import Details from '../Details'
import { selectBlock as selectBlockAction } from '../../../controller/actions'
import { initMap, setMapBlock, resetMapBlock } from '../../../utils/tornMap'
import styles from '../index.cssmodule.scss'
import { ALL_BLOCKS_ID } from '../../../constants'

interface IProps {
  blocks: IBlock[]
  mapInfo: IMapInfo
  conf: IConf
  activeBlock: IBlock
  activeRacket: IRacket
  selectBlock: (data: object) => void
}

class Map extends React.Component<IProps> {
  private _mapRef = React.createRef<HTMLDivElement>()

  componentDidMount() {
    const { mapInfo, blocks, selectBlock } = this.props
    const allID = blocks[0].ID

    initMap(
      this._mapRef.current,
      mapInfo,
      blockID => {
        selectBlock({
          blockID,
          blockIDScroll: blockID
        })
      },
      () => {
        selectBlock({
          blockID: allID,
          blockIDScroll: allID
        })
      }
    )
  }

  componentDidUpdate() {
    const { activeBlock } = this.props

    if (activeBlock.ID === ALL_BLOCKS_ID) {
      resetMapBlock()
    } else {
      setMapBlock(activeBlock.ID)
    }
  }

  render() {
    const { activeBlock, activeRacket, conf } = this.props

    return (
      <div className='left tabs-panel-wrp'>
        <div className='tab-menu-cont'>
          <div className={cn('map left', styles.map)} ref={this._mapRef} />
          <Details activeBlock={activeBlock} conf={conf} racket={activeRacket} />
          <div className='clear' />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxStore) => {
  return {
    mapInfo: state.common.mapInfo,
    blocks: state.common.blocks,
    activeBlock: state.common.activeBlock,
    activeRacket: state.common.activeRacket,
    conf: state.common.conf
  }
}

const mapActionsToProps = {
  selectBlock: selectBlockAction
}

export default connect(mapStateToProps, mapActionsToProps)(Map)
