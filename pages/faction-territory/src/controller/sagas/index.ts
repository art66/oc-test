import { put, takeEvery } from 'redux-saga/effects'
import * as a from '../../constants'
import fetchUrl from '../../utils/fetchURL'
import { fetchTerritoriesSuccess } from '../actions'

function* fetchData() {
  try {
    const response = yield fetchUrl('step=getfactionterritories')

    yield put(fetchTerritoriesSuccess(response))
  } catch (error) {
    console.log(error)
  }
}

export default function* appSaga() {
  yield takeEvery(a.FETCH_TERRITORIES, fetchData)
}
