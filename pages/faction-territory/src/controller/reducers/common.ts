import initialState from '../../store/initialState'
import locationPath from '../helpers/locationPath'

import { IDebugShow, IInfoShow, IType, ILocationChange } from '../../interfaces/IController'
import { IInitialState } from '../../interfaces/IStore'

import * as a from '../../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [a.SHOW_DEBUG_BOX]: (state: IInitialState, action: IDebugShow) => ({
    ...state,
    debug: action.msg
  }),
  [a.HIDE_DEBUG_BOX]: (state: IInitialState) => ({
    ...state,
    debug: null
  }),
  [a.SHOW_INFO_BOX]: (state: IInitialState, action: IInfoShow) => ({
    ...state,
    info: action.msg
  }),
  [a.HIDE_INFO_BOX]: (state: IInitialState) => ({
    ...state,
    info: null
  }),
  [a.LOCATION_CHANGE]: (state: IInitialState, action: ILocationChange) => {
    const currentPath = locationPath(action.payload.location.hash)

    return {
      ...state,
      debug: null,
      info: null,
      locationCurrent: currentPath,
      pageID: a.PATHS_ID[currentPath]
    }
  },
  [a.FETCH_TERRITORIES_SUCCESS]: (state: IInitialState, { payload }) => ({
    ...state,
    ...payload,
    activeBlock: payload.blocks[0],
    loading: false,
    dataLoaded: true
  }),
  [a.SELECT_BLOCK]: (state: IInitialState, { payload }) => {
    const activeBlock = state.blocks.filter(block => block.ID === payload.blockID)[0]
    const activeRackets = state.mapInfo.rackets.filter(racket => racket.territoryID === parseInt(activeBlock.ID, 10))

    return {
      ...state,
      activeBlock,
      activeRacket: activeRackets ? activeRackets[0] : undefined,
      blockIDScroll: payload.blockIDScroll
    }
  },
  [a.UPDATE_MANUAL_DESKTOP_MODE]: (state: IInitialState, { payload }) => ({
    ...state,
    isDesktopManualLayout: payload
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
