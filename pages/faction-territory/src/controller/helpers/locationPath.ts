const locationPath = (url: string) => url.substr(2, url.length) || ''

export default locationPath
