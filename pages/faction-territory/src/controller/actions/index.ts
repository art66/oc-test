import { createAction } from 'redux-actions'
import * as a from '../../constants'

export const fetchTerritories = createAction(a.FETCH_TERRITORIES)
export const fetchTerritoriesSuccess = createAction(a.FETCH_TERRITORIES_SUCCESS)
export const selectBlock = createAction(a.SELECT_BLOCK, data => data)
export const updateManualDesktopMode = createAction(a.UPDATE_MANUAL_DESKTOP_MODE, isDesktopManual => isDesktopManual)
