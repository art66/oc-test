// --------------------------
// REDUX NAMESPACES
// --------------------------
export const SHOW_DEBUG_BOX = 'SHOW_DEBUG_BOX'
export const HIDE_DEBUG_BOX = 'HIDE_DEBUG_BOX'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_INFO_BOX = 'HIDE_INFO_BOX'
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE'
export const MEDIA_SCREEN_CHANGED = 'redux-responsive/CALCULATE_RESPONSIVE_STATE'
export const ROUTE_HEADER_SET = 'ROUTE_HEADER_SET'

export const FETCH_TERRITORIES = 'FETCH_TERRITORIES'
export const FETCH_TERRITORIES_SUCCESS = 'FETCH_TERRITORIES_SUCCESS'
export const SELECT_BLOCK = 'SELECT_BLOCK'
export const UPDATE_MANUAL_DESKTOP_MODE = 'UPDATE_MANUAL_DESKTOP_MODE'

// --------------------------
// APP HEADER PATHS IDs
// --------------------------
export const PATHS_ID = {
  '': 1,
  someSubRoute: 2
}

// ID for the first block
export const ALL_BLOCKS_ID = 'all'
