import { Store } from 'redux'

export interface IProps {
  store: Store,
  fetchTerritories: () => void
}
