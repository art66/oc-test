import React, { Component } from 'react'
import { connect, Provider } from 'react-redux'
import { fetchTerritories as fetchTerritoriesAction } from '../controller/actions'

import AppLayout from '../layout/AppLayout'

import { IProps } from './interfaces'

class AppContainer extends Component<IProps> {
  componentDidMount() {
    const { fetchTerritories } = this.props

    fetchTerritories()
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <AppLayout />
      </Provider>
    )
  }
}

const mapActionToProps = {
  fetchTerritories: fetchTerritoriesAction
}

export default connect(null, mapActionToProps)(AppContainer)
