# APP_NAME - Torn


## 1.2.2
 * Improved Sidebar import.

## 1.2.1
 * Added typings for controller.
 * Fixed missing roots.

## 1.1.0
 * Minor structure improvements.

## 1.0.1
 * First good structure created.

## 1.0.0
 * Initial template release.
