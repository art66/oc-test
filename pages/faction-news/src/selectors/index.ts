export const getStartFrom = state => state.factionNews.startFrom
export const getInitParams = state => state.factionNews.initParams
export const getNews = state => state.factionNews.newsList
export const getActiveTab = state => state.factionNews.activeTab
export const getDefaultTab = state => state.factionNews.initParams.tabs[0]
export const getTab = (state, type) => state.factionNews.initParams.tabs.filter(tab => tab.type === type)[0]
export const getTabs = state => state.factionNews.initParams.tabs
export const getSearchQuery = state => state.factionNews.searchQuery
export const getIsUserUpdate = state => state.factionNews.isUserUpdate
