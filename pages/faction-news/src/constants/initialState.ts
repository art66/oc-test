export const initialState = {
  searchQuery: '',
  activeTab: undefined,
  isUserUpdate: false,
  fetching: true,
  startFrom: '1'
}

export default initialState
