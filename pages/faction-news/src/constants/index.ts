export const FACTION_NEWS_TAPES = {
  tabs: [
    { name: 'Main News', type: 1, id: 'main_news' },
    { name: 'Attacking', type: 2, id: 'attacking' },
    { name: 'Funds', type: 3, id: 'funds' },
    { name: 'Armory', type: 4, id: 'armoury' },
    { name: 'Crimes', type: 7, id: 'crimes' },
    { name: 'Membership', type: 8, id: 'membership' }
  ]
}

export const SEARCH_ALL_TYPE = {
  name: 'Sort by',
  type: 'all',
  id: 'all'
}

export const MIN_SEARCH_LENGTH = 1
export const MAX_SEARCH_LENGTH = 100

export const SEARCH_ICON_DIMENSIONS = {
  width: 21,
  height: 21,
  viewbox: '-4 -2 24 24'
}

export const DEFAULT_PRESET = {
  type: 'header',
  subtype: 'RESET_DEFAULTS'
}

export const TAB_QUERY_KEY = 'type'
export const SEARCH_QUERY_KEY = 'search'
