import { createAction } from 'redux-actions'
import * as actionTypes from './actionTypes'

export const setNewsList = createAction(actionTypes.SET_NEWS_LIST, (list, error) => ({ list, error }))
export const setStartFrom = createAction(actionTypes.SET_START_FROM, startFrom => ({ startFrom }))
export const setIsFetching = createAction(actionTypes.SET_IS_FETCHING, fetching => ({ fetching }))
export const loadNextChunk = createAction(actionTypes.LOAD_NEXT_CHUNK)
export const setInitParams = createAction(actionTypes.SET_INIT_PARAMS, params => ({ params }))
export const setActiveTab = createAction(actionTypes.SET_ACTIVE_TAB, (tab, isUserUpdate = false) => ({ tab, isUserUpdate }))
export const setInitTab = createAction(actionTypes.SET_INIT_TAB)
export const setSearchQuery = createAction(actionTypes.SET_SEARCH_QUERY, value => ({ value }))
export const activateSearch = createAction(actionTypes.ACTIVATE_SEARCH)
export const setIsSearching = createAction(actionTypes.SET_IS_SEARCHING, searching => ({ searching }))
export const checkManualDesktopMode = createAction(actionTypes.CHECK_DESKTOP_MODE, isManualDesktopMode => ({
  isManualDesktopMode
}))
