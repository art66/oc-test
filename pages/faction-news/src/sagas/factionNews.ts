import { takeEvery, put, select, takeLatest } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { updateRequestURL } from '../utils'
import * as actionTypes from '../actions/actionTypes'
import {
  setNewsList,
  setStartFrom,
  setIsFetching,
  setSearchQuery,
  setIsSearching,
  setActiveTab,
  activateSearch
} from '../actions'
import {
  getStartFrom,
  getInitParams,
  getNews,
  getActiveTab,
  getDefaultTab,
  getTab,
  getTabs,
  getSearchQuery,
  getIsUserUpdate
} from '../selectors'
import { SEARCH_ALL_TYPE, TAB_QUERY_KEY, SEARCH_QUERY_KEY } from '../constants'

export function* loadNextChunk() {
  try {
    const startFrom = yield select(getStartFrom)
    const initParams = yield select(getInitParams)
    const activeTab = yield select(getActiveTab)
    const searchQuery = yield select(getSearchQuery)

    yield put(setIsFetching(true))
    const reqParams =
      activeTab.id === SEARCH_ALL_TYPE.id ? { startFrom, q: searchQuery } : { startFrom, type: activeTab.type }

    const data = yield fetchUrl(initParams.url, reqParams)
    const newsList = yield select(getNews)

    if (+data.startFrom && data.list && newsList) {
      yield put(setNewsList([...newsList, ...data.list], data.error))
    }

    yield put(setStartFrom(data.startFrom))
    yield put(setIsFetching(false))
  } catch (e) {
    console.error(e)
  }
}

export function* activateTab() {
  try {
    const activeTab = yield select(getActiveTab)

    if (activeTab.id === SEARCH_ALL_TYPE.id) {
      return
    }

    const defaultTab = yield select(getDefaultTab)
    const isUserUpdate = yield select(getIsUserUpdate)

    if(defaultTab.id === activeTab.id) {
      updateRequestURL(isUserUpdate)
    } else {
      updateRequestURL(isUserUpdate, TAB_QUERY_KEY, activeTab.type)
    }

    const initParams = yield select(getInitParams)

    yield put(setNewsList([], null))
    yield put(setSearchQuery(''))
    yield put(setIsFetching(true))

    const data = yield fetchUrl(initParams.url, { type: activeTab.type })

    yield put(setNewsList(data.list, data.error))
    yield put(setStartFrom(data.startFrom))
    yield put(setIsFetching(false))
  } catch (e) {
    console.error(e)
  }
}

export function* initTab() {
  try {
    const urlParams = new URLSearchParams(window.location.search.replace('?', ''))
    const type = urlParams.get(TAB_QUERY_KEY)
    const search = urlParams.get(SEARCH_QUERY_KEY)
    const defaultTab = yield select(getDefaultTab)
    const activeTab = yield select(getActiveTab)
    const tabs = yield select(getTabs)

    if(tabs.length === 1) {
      yield put(setActiveTab(defaultTab))
      return
    }

    if (search) {
      yield put(setActiveTab(SEARCH_ALL_TYPE))
      yield put(setSearchQuery(search))
      yield put(activateSearch())
      return
    }

    let initialTab

    if (type) {
      initialTab = yield select(getTab, parseInt(type, 10))
    } else {
      initialTab = defaultTab
    }

    if(!activeTab || initialTab.id !== activeTab.id) {
      yield put(setActiveTab(initialTab))
    }

  } catch (e) {
    console.log(e)
  }
}

export function* loadSearchQuery() {
  try {
    const searchQuery = yield select(getSearchQuery)

    if (searchQuery === '') {
      return
    }

    const isUserUpdate = yield select(getIsUserUpdate)

    updateRequestURL(isUserUpdate, SEARCH_QUERY_KEY, searchQuery)

    const initParams = yield select(getInitParams)

    yield put(setNewsList([], null))
    yield put(setIsFetching(true))
    yield put(setIsSearching(true))

    const data = yield fetchUrl(initParams.url, { q: searchQuery })

    yield put(setNewsList(data.list, data.error))
    yield put(setStartFrom(data.startFrom))
    yield put(setIsFetching(false))
    yield put(setIsSearching(false))
  } catch (e) {
    console.error(e)
  }
}

export default function* factionNews() {
  yield takeEvery(actionTypes.LOAD_NEXT_CHUNK, loadNextChunk)
  yield takeLatest(actionTypes.SET_ACTIVE_TAB, activateTab)
  yield takeEvery(actionTypes.SET_INIT_TAB, initTab)
  yield takeEvery(actionTypes.ACTIVATE_SEARCH, loadSearchQuery)
}
