import { all } from 'redux-saga/effects'
import factionNews from './factionNews'

export default function* rootSaga() {
  yield all([factionNews()])
}
