export const getHeightWithTopOffset = elRect => elRect.height + (elRect.top + window.pageYOffset)

export const getWinHeightWithTopOffset = () => window.innerHeight + window.pageYOffset

export const isManualDesktopMode = () => document.body.classList.contains('d') && !document.body.classList.contains('r')

export const updateRequestURL = (isUserUpdate, key = '', value = '') => {
  if (!isUserUpdate) {
    return
  }
  const hash = window.location.hash
  const oldUrl = window.location.search + hash
  const newUrl = '?step=your' + (key === '' && value === '' ? '' : `&${key}=${value}${hash}`)

  if (oldUrl !== newUrl) {
    window.history.pushState(null, null, newUrl)
  }
}
