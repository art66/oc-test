import React from 'react'
import Loadable from 'react-loadable'
import { Provider } from 'react-redux'
import reducer from '../reducers/factionNews'
import rootSaga from '../sagas'

const FactionNewsBundle = Loadable({
  loader: async () => {
    const factionNews = await import(/* webpackChunkName: "factionNews" */ '../components/App')

    return factionNews
  },
  render(asyncComponent: any, { rootStore, injector, initParams, injectSaga }: any) {
    const { default: FactionNews } = asyncComponent

    injector(rootStore, { reducer, key: 'factionNews' })

    injectSaga({ saga: rootSaga, key: 'factionNews' })

    return (
      <Provider store={rootStore}>
        <FactionNews {...initParams} />
      </Provider>
    )
  },
  loading: () => <div />
})

export default FactionNewsBundle
