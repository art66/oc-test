import { IBrowser } from 'redux-responsive'
import IFactionNews from './IFactionNews'

export default interface IStore {
  factionNews: IFactionNews
  browser: IBrowser
}
