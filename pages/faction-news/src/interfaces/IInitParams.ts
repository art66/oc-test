import ITab from './ITab'

export default interface IInitParams {
  url: string
  tabs: ITab[]
  search?: boolean
}
