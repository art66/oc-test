import IListItem from './IListItem'
import IInitParams from './IInitParams'
import ITab from './ITab'

export default interface IFactionNews {
  searchQuery: string
  activeTab: ITab | undefined
  fetching: boolean
  initParams: IInitParams
  newsList: IListItem[]
  startFrom: string
  isManualDesktopMode: boolean
  searching?: boolean
  error: string
}
