export default interface ITab {
  id: string
  name: string
  type: number | string
}
