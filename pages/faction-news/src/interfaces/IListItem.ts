export default interface IListItem {
  ID: string
  date: string
  time: string
  message: string
}
