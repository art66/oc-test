import React, { Component } from 'react'
import { connect } from 'react-redux'
import NewsList from './NewsList'
import NewsHeader from './NewsHeader'
import { setInitParams, setInitTab } from '../actions'
import IInitParams from '../interfaces/IInitParams'
import ITab from '../interfaces/ITab'

interface IProps {
  url: string
  tabs: ITab[]
  setInitParams: (initParams: IInitParams) => void
  setInitTab: () => void
}

class App extends Component<IProps> {
  constructor(props: IProps) {
    super(props)

    this.handleBrowserNavigate = this.handleBrowserNavigate.bind(this)
  }

  componentDidMount() {
    const { url, tabs } = this.props

    this.props.setInitParams({ url, tabs })
    this.props.setInitTab()
    window.addEventListener('popstate', this.handleBrowserNavigate)
  }

  componentDidUnMount() {
    window.removeEventListener('popstate', this.handleBrowserNavigate)
  }

  handleBrowserNavigate() {
    this.props.setInitTab()
  }

  render() {
    const { url, tabs } = this.props

    return (
      <div>
        <NewsHeader tabs={tabs} />
        <NewsList url={url} />
      </div>
    )
  }
}

export default connect(null, {
  setInitParams,
  setInitTab
})(App)
