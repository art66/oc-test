import React from 'react'
import cn from 'classnames'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import globalIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import ITab from '../../interfaces/ITab'
import styles from './index.cssmodule.scss'
import {
  SEARCH_ALL_TYPE,
  SEARCH_ICON_DIMENSIONS,
  DEFAULT_PRESET,
  MIN_SEARCH_LENGTH,
  MAX_SEARCH_LENGTH
} from '../../constants'

interface IProps {
  searchQuery: string
  activeTab: ITab
  fetching: boolean
  setActiveTabProp: (tab: ITab, isUserUpdate: boolean) => void
  setSearchQueryProp: (value: string) => void
  activateSearchProp: () => void
}

interface IState {
  disabled: boolean
}

export class Search extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this._onFocus = this._onFocus.bind(this)
    this._onChange = this._onChange.bind(this)
    this._onKeyPress = this._onKeyPress.bind(this)
    this._onClick = this._onClick.bind(this)

    this.state = {
      disabled: props.searchQuery === ''
    }
  }

  _onFocus() {
    this.props.setActiveTabProp(SEARCH_ALL_TYPE, true)
  }

  _onChange(event) {
    let searchVal = event.target.value
    const valueLen = searchVal.length
    const disabled = !(valueLen >= MIN_SEARCH_LENGTH && valueLen <= MAX_SEARCH_LENGTH)

    searchVal = searchVal.split(' ').join('')
    this.setState({ disabled })
    this.props.setSearchQueryProp(searchVal)
  }

  _onKeyPress(event) {
    if (event.key === 'Enter') {
      this._onClick()
    }
  }

  _onClick() {
    const { disabled } = this.state

    if (disabled) {
      return false
    }
    // this._onFocus()
    // this.props.setSearchQueryProp(searchVal)
    this.props.activateSearchProp()
  }

  render() {
    const { fetching, searchQuery } = this.props
    const { disabled } = this.state
    const isDisabled = fetching || disabled

    return (
      <div className={cn(styles.search)}>
        <div className={cn(styles.inputWrapper)}>
          <input
            className={cn(styles.input)}
            value={searchQuery}
            placeholder='search...'
            onFocus={this._onFocus}
            onChange={this._onChange}
            onKeyPress={this._onKeyPress}
          />
        </div>
        <button
          className={cn(styles.searchButton, cn({ [styles.disabled]: isDisabled }))}
          type='button'
          onClick={this._onClick}
        >
          <SVGIconGenerator
            iconsHolder={globalIconsHolder}
            iconName='Search'
            preset={DEFAULT_PRESET}
            dimensions={SEARCH_ICON_DIMENSIONS}
          />
        </button>
      </div>
    )
  }
}

export default Search
