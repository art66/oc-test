import React from 'react'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import ITab from '../../interfaces/ITab'

interface IProps {
  active: boolean
  isSingleTab?: boolean
  tab?: ITab
  setActiveTabProp?: (tab: ITab, isUserUpdate: boolean) => void
  mediaType: string
}

export class Tab extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props)

    this._onClick = this._onClick.bind(this)
  }

  _onClick() {
    const { setActiveTabProp, tab, mediaType, isSingleTab } = this.props

    if (mediaType === 'desktop' && !isSingleTab) {
      setActiveTabProp(tab, true)
    }
  }

  render() {
    const {
      tab: { name },
      active,
      isSingleTab
    } = this.props

    if (isSingleTab) {
      return (
        <span
          className={cn(styles.tab, {
            [styles.insensitive]: isSingleTab,
            [styles.single]: isSingleTab
          })}
        >
          {name}
        </span>
      )
    }

    return (
      <button
        className={cn(styles.tab, {
          [styles.active]: active
        })}
        type='button'
        onClick={this._onClick}
      >
        {name}
      </button>
    )
  }
}

export default Tab
