import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'

import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'
import Dropdown from '@torn/shared/components/Dropdown'
import Tab from '../Tab'
import Search from '../Search'
import styles from './index.cssmodule.scss'
import tabStyles from '../Tab/index.cssmodule.scss'

import ITab from '../../interfaces/ITab'
import IStore from '../../interfaces/IStore'

import { setActiveTab, setSearchQuery, activateSearch, checkManualDesktopMode } from '../../actions'
import { isManualDesktopMode } from '../../utils'

interface IProps {
  activeTab: ITab | undefined
  tabs: ITab[]
  setActiveTabProp: (tab: ITab, isUserUpdate: boolean) => void
  setSearchQueryProp: (value: string) => void
  activateSearchProp: () => void
  checkManualDesktopModeProp: (manualDesktopMode: boolean) => void
  searchQuery: string
  fetching: boolean
  mediaType: string
}

class NewsHeader extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)

    this._onDropdownChange = this._onDropdownChange.bind(this)
  }

  componentDidMount() {
    const { checkManualDesktopModeProp } = this.props

    subscribeOnDesktopLayout((payload: boolean) => checkManualDesktopModeProp(payload))
    checkManualDesktopModeProp(isManualDesktopMode())
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  _getTabById(id) {
    return this.props.tabs.filter(tab => tab.id === id)[0]
  }

  _onDropdownChange(tab) {
    this.props.setActiveTabProp(
      this._getTabById(tab.id),
      true
    )
  }

  _renderTabs() {
    const { mediaType, tabs, activeTab, setActiveTabProp } = this.props
    const isMultitab = tabs.length > 1

    if (mediaType === 'desktop' || !isMultitab) {
      return tabs.map(tab => (
        <Tab
          key={tab.id}
          tab={tab}
          active={isMultitab && activeTab !== undefined && tab.id === activeTab.id}
          isSingleTab={isMultitab === false}
          setActiveTabProp={setActiveTabProp}
          mediaType={mediaType}
        />
      ))
    }
    const selectedTab = activeTab || tabs[0]

    return (
      <div className={cn(tabStyles.tab, tabStyles.insensitive)}>
        <Dropdown
          list={tabs.map(tab => ({ id: tab.id, name: tab.name }))}
          selected={{ id: selectedTab.id, name: selectedTab.name }}
          onChange={this._onDropdownChange}
        />
      </div>
    )
  }

  _renderSearch() {
    const {
      tabs,
      searchQuery,
      activeTab,
      fetching,
      setActiveTabProp,
      activateSearchProp,
      setSearchQueryProp
    } = this.props

    if (tabs.length > 1) {
      return (
        <div className={cn(tabStyles.tab, tabStyles.insensitive)}>
          <Search
            searchQuery={searchQuery}
            activeTab={activeTab}
            fetching={fetching}
            setActiveTabProp={setActiveTabProp}
            setSearchQueryProp={setSearchQueryProp}
            activateSearchProp={activateSearchProp}
          />
        </div>
      )
    }

    return null
  }

  render() {
    const { tabs } = this.props
    const classNames = cn(styles.newsHeader, {
      [styles.single]: tabs.length <= 1
    })

    return (
      <div className={classNames}>
        {this._renderTabs()}
        {this._renderSearch()}
      </div>
    )
  }
}

export default connect(
  (state: IStore) => ({
    activeTab: state.factionNews.activeTab,
    searchQuery: state.factionNews.searchQuery,
    fetching: state.factionNews.fetching,
    mediaType: state.factionNews.isManualDesktopMode ? 'desktop' : state.browser.mediaType
  }),
  {
    setActiveTabProp: setActiveTab,
    setSearchQueryProp: setSearchQuery,
    activateSearchProp: activateSearch,
    checkManualDesktopModeProp: checkManualDesktopMode
  }
)(NewsHeader)
