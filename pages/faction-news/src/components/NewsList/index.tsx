import React, { Component, RefObject } from 'react'
import cn from 'classnames'
import { connect } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'
import { getWinHeightWithTopOffset, getHeightWithTopOffset } from '../../utils'
import { loadNextChunk } from '../../actions'
import IListItem from '../../interfaces/IListItem'
import IStore from '../../interfaces/IStore'
import ITab from '../../interfaces/ITab'
import s from './index.cssmodule.scss'

interface IProps {
  url: string
  fetching: boolean
  newsList: IListItem[]
  error: string
  newsListEndReached: boolean
  loadNextChunk: () => void
  activeTab: ITab
  searching: boolean
}

class NewsList extends Component<IProps> {
  newsListRef: RefObject<HTMLUListElement>
  timer: any
  topOffset: number
  winHeightWithTopOffset: number
  constructor(props: IProps) {
    super(props)
    this.newsListRef = React.createRef()
  }

  componentDidMount() {
    this.winHeightWithTopOffset = getWinHeightWithTopOffset()
    window.addEventListener('scroll', this._handleScroll)
    this.timer = setInterval(this.loadChunks, 200)
  }

  componentDidUpdate(prevProps) {
    const { activeTab, searching } = this.props

    if (
      this.newsListRef?.current
      && (activeTab?.type !== prevProps.activeTab?.type || searching !== prevProps.searching)
    ) {
      this.topOffset = getHeightWithTopOffset(this.newsListRef.current.getBoundingClientRect())
      clearInterval(this.timer)
      this.timer = setInterval(this.loadChunks, 200)
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this._handleScroll)
    clearInterval(this.timer)
  }

  loadChunks = () => {
    const { fetching, newsListEndReached, newsList } = this.props

    if (!fetching) {
      this.topOffset = getHeightWithTopOffset(this.newsListRef.current.getBoundingClientRect())

      if (this.topOffset < this.winHeightWithTopOffset && !newsListEndReached) {
        this.props.loadNextChunk()
      }

      if ((newsList && newsListEndReached) || this.topOffset >= this.winHeightWithTopOffset) {
        clearInterval(this.timer)
      }
    }
  }

  _handleScroll = () => {
    this.topOffset = getHeightWithTopOffset(this.newsListRef.current.getBoundingClientRect())
    this.winHeightWithTopOffset = getWinHeightWithTopOffset()
    const diff = this.topOffset - this.winHeightWithTopOffset
    const { fetching, newsListEndReached } = this.props

    if (diff <= 0 && !fetching && !newsListEndReached) {
      this.props.loadNextChunk()
    }
  }

  renderList = () => {
    const { newsList } = this.props

    return newsList.map(item => {
      return (
        <li key={item.ID}>
          <span className='date'>
            <span>{item.time}</span>
            <br />
            <span>{item.date}</span>
          </span>
          <span className='info' dangerouslySetInnerHTML={{ __html: item.message }} />
        </li>
      )
    })
  }

  render() {
    const { newsList, fetching, error } = this.props

    if (!newsList) {
      return <Preloader />
    }

    if (!fetching && error) {
      return (
        <ul ref={this.newsListRef}>
          <div className={cn('cont-gray', 'bottom-round', s.notFound)}>{error}</div>
        </ul>
      )
    }

    if (!fetching && !newsList.length) {
      return (
        <ul ref={this.newsListRef}>
          <div className={cn('cont-gray', 'bottom-round', s.notFound)}>No news found</div>
        </ul>
      )
    }

    return (
      <ul className='news-list fm-list t-blue-cont h cont-gray bottom-round' ref={this.newsListRef}>
        {this.renderList()}
        {fetching && (
          <div className={s.preloaderWrapper}>
            <Preloader />
          </div>
        )}
      </ul>
    )
  }
}

export default connect(
  (state: IStore) => ({
    fetching: state.factionNews.fetching,
    newsList: state.factionNews.newsList,
    error: state.factionNews.error,
    newsListEndReached: !+state.factionNews.startFrom,
    activeTab: state.factionNews.activeTab,
    searching: state.factionNews.searching
  }),
  {
    loadNextChunk
  }
)(NewsList)
