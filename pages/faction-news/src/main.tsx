import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import rootStore from './store/createStore'
import App from './components/App'
import IInitParams from './interfaces/IInitParams'
import { FACTION_NEWS_TAPES } from './constants'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    renderFactionNews: (node: HTMLElement, initParams: IInitParams) => void
  }
}

const MOUNT_NODE = document.getElementById('faction-news-root')
const TEST_PROPS = {
  url: '/page.php?sid=factionsNews&step=list'
}

const render = (node, initParams) => {
  ReactDOM.render(
    <Provider store={rootStore}>
      <App {...initParams} />
    </Provider>,
    node
  )
}

// This code is excluded from production bundle
// @ts-ignore
if (__DEV__) {
  if (module.hot) {
    render(MOUNT_NODE, { ...TEST_PROPS, ...FACTION_NEWS_TAPES })
    module.hot.accept()
  }
} else {
  render(MOUNT_NODE, { ...TEST_PROPS, ...FACTION_NEWS_TAPES })
}

// temporary fiz to wait cache update
window.renderFactionNews = () => {}
