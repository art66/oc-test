import initialState from '../constants/initialState'
import * as actionTypes from '../actions/actionTypes'

const ACTION_HANDLERS = {
  [actionTypes.SET_NEWS_LIST]: (state, action) => {
    return {
      ...state,
      newsList: action.payload.list || [],
      error: action.payload.error
    }
  },
  [actionTypes.SET_START_FROM]: (state, action) => {
    return {
      ...state,
      startFrom: action.payload.startFrom
    }
  },
  [actionTypes.SET_IS_FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.payload.fetching
    }
  },
  [actionTypes.SET_INIT_PARAMS]: (state, action) => {
    return {
      ...state,
      initParams: action.payload.params
    }
  },
  [actionTypes.SET_ACTIVE_TAB]: (state, action) => {
    return {
      ...state,
      activeTab: action.payload.tab,
      isUserUpdate: action.payload.isUserUpdate
    }
  },
  [actionTypes.SET_SEARCH_QUERY]: (state, action) => {
    return {
      ...state,
      searchQuery: action.payload.value
    }
  },
  [actionTypes.SET_IS_SEARCHING]: (state, action) => {
    return {
      ...state,
      searching: action.payload.searching
    }
  },
  [actionTypes.CHECK_DESKTOP_MODE]: (state, action) => {
    return {
      ...state,
      isManualDesktopMode: action.payload.isManualDesktopMode
    }
  }
}

export default function factionNewsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
