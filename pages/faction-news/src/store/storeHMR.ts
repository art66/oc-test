import makeRootReducer from '../reducers'

const activateStoreHMR = store => {
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      store.replaceReducer(makeRootReducer())
    })
  }
}

export default activateStoreHMR
