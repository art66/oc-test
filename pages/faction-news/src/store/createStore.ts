import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk'

import makeRootReducer from '../reducers'
import rootSaga from '../sagas'
import activateStoreHMR from './storeHMR'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    getCookie: any
    devToolsExtension: any
    __REDUX_DEVTOOLS_EXTENSION__: any
  }
}

const sagaMiddleware = createSagaMiddleware()
const rootStore = (initialState: object = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [sagaMiddleware, thunk]
  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  // @ts-ignore
  if (__DEV__) {
    const { __REDUX_DEVTOOLS_EXTENSION__ } = window

    if (__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store: any = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer as any, applyMiddleware(...middleware), ...enhancers)
  )


  sagaMiddleware.run(rootSaga)
  store.asyncReducers = {}

  activateStoreHMR(store)

  return store
}

export default rootStore()
