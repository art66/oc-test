import {
  SET_PAGE_ID,
  SHOW_DEBUG_INFO,
  HIDE_DEBUG_INFO,
  METADATA_REQUEST,
  METADATA_RESPONSE_SUCCESS,
  METADATA_RESPONSE_FAILURE,
  OWNER_REQUEST,
  OWNER_RESPONSE_SUCCESS,
  OWNER_RESPONSE_FAILURE,
  BAZAAR_DATA_REQUEST,
  BAZAAR_DATA_RESPONSE_SUCCESS,
  BAZAAR_DATA_RESPONSE_FAILURE,
  SET_BAZAAR_DATA,
  LINK_CHANGE
} from './constants'
import {
  SET_ACTIVE_VIEW_HEIGHT,
  BAZAAR_ITEM_INFO_REQUEST,
  BAZAAR_ITEM_INFO_SUCCESS,
  BAZAAR_ITEM_INFO_FAILURE,
  SHOW_INFOBOX,
  SET_REQUEST_INFOBOX,
  SET_LEAVE_REQUEST_INFO_BOX
} from './constants/actions'

import {
  ISetPageId,
  IShowDebugInfo,
  IHideDebugInfo,
  IMetadataRequest,
  IMetadataResponseSuccess,
  IMetadataResponseFailure,
  IOwnerRequest,
  IOwnerResponseSuccess,
  IOwnerResponseFailure,
  IBazaarDataRequest,
  IBazaarDataResponseSuccess,
  IBazaarDataResponseFailure,
  ISetBazaarData,
  TBazaarItemInfoRequestAction,
  TBazaarItemInfoSuccessAction,
  TBazaarItemInfoFailureAction,
  TSetActiveViewHeightAction,
  ILinkChage,
  TShowInfoBoxPayload,
  TSetRequestInfoBoxPayload,
  TLeaveRequestInfoBoxPayload
} from './types/actions'

// ------------------------------------
// Actions
// ------------------------------------

export const setRequestInfoBox = (infoBox: TSetRequestInfoBoxPayload['infoBox']): TSetRequestInfoBoxPayload => {
  return {
    type: SET_REQUEST_INFOBOX,
    infoBox
  }
}

export const showInfoBox = (infoBox: TShowInfoBoxPayload['infoBox']): TShowInfoBoxPayload => {
  return {
    type: SHOW_INFOBOX,
    infoBox
  }
}

export const leaveRequestInfoBox = (leave: TLeaveRequestInfoBoxPayload['leave']): TLeaveRequestInfoBoxPayload => {
  return {
    type: SET_LEAVE_REQUEST_INFO_BOX,
    leave
  }
}

export const setPageId = (pageID): ISetPageId => {
  return {
    type: SET_PAGE_ID,
    pageID
  }
}

export const showDebugInfo = (msg): IShowDebugInfo => {
  return {
    type: SHOW_DEBUG_INFO,
    msg
  }
}

export const hideDebugInfo = (): IHideDebugInfo => ({
  type: HIDE_DEBUG_INFO
})

export const metadataRequest = (): IMetadataRequest => {
  return {
    type: METADATA_REQUEST
  }
}

export const metadataResponseSuccess = (metadata): IMetadataResponseSuccess => {
  return {
    type: METADATA_RESPONSE_SUCCESS,
    metadata
  }
}

export const metadataResponseFailure = (): IMetadataResponseFailure => {
  return {
    type: METADATA_RESPONSE_FAILURE
  }
}

export const ownerRequest = (): IOwnerRequest => {
  return {
    type: OWNER_REQUEST
  }
}

export const ownerResponseSuccess = (owner): IOwnerResponseSuccess => {
  return {
    type: OWNER_RESPONSE_SUCCESS,
    owner
  }
}

export const ownerResponseFailure = (): IOwnerResponseFailure => {
  return {
    type: OWNER_RESPONSE_FAILURE
  }
}

export const bazaarDataRequest = (): IBazaarDataRequest => {
  return {
    type: BAZAAR_DATA_REQUEST
  }
}

export const bazaarDataResponseSuccess = (bazaarData): IBazaarDataResponseSuccess => {
  return {
    type: BAZAAR_DATA_RESPONSE_SUCCESS,
    bazaarData
  }
}

export const bazaarDataResponseFailure = (): IBazaarDataResponseFailure => {
  return {
    type: BAZAAR_DATA_RESPONSE_FAILURE
  }
}

export const setBazaarData = (bazaarData): ISetBazaarData => {
  return {
    type: SET_BAZAAR_DATA,
    bazaarData
  }
}

export const linkChage = (linkId): ILinkChage => {
  return {
    type: LINK_CHANGE,
    linkId
  }
}

export const setActiveViewHeight: TSetActiveViewHeightAction = (uniqueId, height) => {
  return {
    type: SET_ACTIVE_VIEW_HEIGHT,
    uniqueId,
    height
  }
}

export const bazaarItemInfoRequest: TBazaarItemInfoRequestAction = uniqueId => {
  return {
    type: BAZAAR_ITEM_INFO_REQUEST,
    uniqueId
  }
}

export const bazaarItemInfoSuccess: TBazaarItemInfoSuccessAction = (uniqueId, view) => {
  return {
    type: BAZAAR_ITEM_INFO_SUCCESS,
    uniqueId,
    view
  }
}

export const bazaarItemInfoFailure: TBazaarItemInfoFailureAction = uniqueId => {
  return {
    type: BAZAAR_ITEM_INFO_FAILURE,
    uniqueId
  }
}

export default {
  setPageId,
  showDebugInfo,
  hideDebugInfo,
  metadataRequest,
  metadataResponseSuccess,
  metadataResponseFailure,
  ownerRequest,
  ownerResponseSuccess,
  ownerResponseFailure,
  bazaarDataRequest,
  bazaarDataResponseSuccess,
  bazaarDataResponseFailure,
  setBazaarData,
  linkChage
}
