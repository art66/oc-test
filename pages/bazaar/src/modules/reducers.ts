import { createReducer } from './utils'
import {
  SET_PAGE_ID,
  SHOW_DEBUG_INFO,
  HIDE_DEBUG_INFO,
  METADATA_REQUEST,
  METADATA_RESPONSE_SUCCESS,
  METADATA_RESPONSE_FAILURE,
  OWNER_REQUEST,
  OWNER_RESPONSE_SUCCESS,
  OWNER_RESPONSE_FAILURE,
  BAZAAR_DATA_REQUEST,
  BAZAAR_DATA_RESPONSE_SUCCESS,
  BAZAAR_DATA_RESPONSE_FAILURE,
  SET_BAZAAR_DATA
} from './constants'
import {
  SET_ACTIVE_VIEW_HEIGHT,
  BAZAAR_ITEM_INFO_REQUEST,
  BAZAAR_ITEM_INFO_SUCCESS,
  BAZAAR_ITEM_INFO_FAILURE,
  SHOW_INFOBOX,
  SET_REQUEST_INFOBOX,
  SET_LEAVE_REQUEST_INFO_BOX
} from './constants/actions'
import {
  ISetPageId,
  ICommonState,
  IShowDebugInfo,
  IMetadataResponseSuccess,
  IOwnerResponseSuccess,
  IBazaarDataResponseSuccess,
  ISetBazaarData
} from './types'
import {
  TSetActiveViewHeightPayload,
  IBazaarItemInfoFailurePayload,
  IBazaarItemInfoSuccessPayload,
  IBazaarItemInfoRequestPayload,
  TShowInfoBoxPayload,
  TLeaveRequestInfoBoxPayload
} from './types/actions'
import { OPEN_BAZAAR_SUCCESS, CLOSE_BAZAAR_SUCCESS } from '../routes/Bazaar/modules/constants'

// ------------------------------------
// Initial State
// ------------------------------------

export const commonInitialState: ICommonState = {
  appID: 'Bazaar',
  pageID: null,
  dbg: null,
  requestInfoBox: {
    loading: false,
    msg: null,
    color: null,
    log: null
  },
  leaveRequestInfoBox: false,
  infoBox: {
    loading: false,
    msg: null,
    color: null,
    log: null
  },
  metadata: {
    isLoading: null,
    data: null,
    error: null
  },
  owner: {
    isLoading: false,
    data: { userId: '', playername: '' },
    error: null
  },
  bazaarData: {
    isLoading: false,
    data: {
      name: '',
      description: ''
    },
    error: null
  },
  isDesktopLayoutSetted: null,
  views: {},
  viewsSettings: {}
}

// ------------------------------------
// Action Handlers
// ------------------------------------

const ACTION_HANDLERS = {
  [SET_REQUEST_INFOBOX]: (state: ICommonState, action: TShowInfoBoxPayload): ICommonState => {
    return {
      ...state,
      requestInfoBox: action.infoBox
    }
  },
  [SET_LEAVE_REQUEST_INFO_BOX]: (state: ICommonState, action: TLeaveRequestInfoBoxPayload): ICommonState => {
    return {
      ...state,
      leaveRequestInfoBox: action.leave
    }
  },
  [SHOW_INFOBOX]: (state: ICommonState, action: TShowInfoBoxPayload): ICommonState => {
    return {
      ...state,
      infoBox: action.infoBox
    }
  },
  [SET_PAGE_ID]: (state: ICommonState, action: ISetPageId): ICommonState => {
    return {
      ...state,
      pageID: action.pageID
    }
  },
  [SHOW_DEBUG_INFO]: (state: ICommonState, action: IShowDebugInfo): ICommonState => {
    return {
      ...state,
      dbg: action.msg
    }
  },
  [HIDE_DEBUG_INFO]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      dbg: null
    }
  },
  [METADATA_REQUEST]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      metadata: {
        ...state.metadata,
        isLoading: true,
        error: null
      }
    }
  },
  [METADATA_RESPONSE_SUCCESS]: (state: ICommonState, action: IMetadataResponseSuccess): ICommonState => {
    return {
      ...state,
      metadata: {
        ...state.metadata,
        isLoading: false,
        data: action.metadata
      }
    }
  },
  [METADATA_RESPONSE_FAILURE]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      metadata: {
        ...state.metadata,
        isLoading: false,
        error: true
      }
    }
  },
  [OWNER_REQUEST]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      owner: {
        ...state.owner,
        isLoading: true,
        error: null
      }
    }
  },
  [OWNER_RESPONSE_SUCCESS]: (state: ICommonState, action: IOwnerResponseSuccess): ICommonState => {
    return {
      ...state,
      owner: {
        ...state.owner,
        isLoading: false,
        data: action.owner
      }
    }
  },
  [OWNER_RESPONSE_FAILURE]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      owner: {
        ...state.owner,
        isLoading: false,
        error: true
      }
    }
  },
  [BAZAAR_DATA_REQUEST]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      bazaarData: {
        ...state.bazaarData,
        isLoading: true,
        error: null
      }
    }
  },
  [BAZAAR_DATA_RESPONSE_SUCCESS]: (state: ICommonState, action: IBazaarDataResponseSuccess): ICommonState => {
    return {
      ...state,
      bazaarData: {
        ...state.bazaarData,
        isLoading: false,
        data: action.bazaarData
      }
    }
  },
  [BAZAAR_DATA_RESPONSE_FAILURE]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      bazaarData: {
        ...state.bazaarData,
        isLoading: false,
        error: true
      }
    }
  },
  [SET_BAZAAR_DATA]: (state: ICommonState, action: ISetBazaarData): ICommonState => {
    return {
      ...state,
      bazaarData: {
        ...state.bazaarData,
        data: action.bazaarData
      }
    }
  },
  [SET_BAZAAR_DATA]: (state: ICommonState, action: ISetBazaarData): ICommonState => {
    return {
      ...state,
      bazaarData: {
        ...state.bazaarData,
        data: action.bazaarData
      }
    }
  },
  [SET_ACTIVE_VIEW_HEIGHT]: (state: ICommonState, action: TSetActiveViewHeightPayload): ICommonState => {
    return {
      ...state,
      viewsSettings: {
        ...state.viewsSettings,
        [action.uniqueId]: {
          ...state.viewsSettings[action.uniqueId],
          height: action.height
        }
      }
    }
  },
  [BAZAAR_ITEM_INFO_REQUEST]: (state: ICommonState, action: IBazaarItemInfoRequestPayload): ICommonState => {
    return {
      ...state,
      viewsSettings: {
        ...state.viewsSettings,
        [action.uniqueId]: {
          ...state.viewsSettings[action.uniqueId],
          isLoading: true
        }
      }
    }
  },
  [BAZAAR_ITEM_INFO_SUCCESS]: (state: ICommonState, action: IBazaarItemInfoSuccessPayload): ICommonState => {
    return {
      ...state,
      views: {
        ...state.views,
        [action.uniqueId]: {
          ...state.views[action.uniqueId],
          ...action.view
        }
      },
      viewsSettings: {
        ...state.viewsSettings,
        [action.uniqueId]: {
          ...state.viewsSettings[action.uniqueId],
          isLoading: false
        }
      }
    }
  },
  [BAZAAR_ITEM_INFO_FAILURE]: (state: ICommonState, action: IBazaarItemInfoFailurePayload): ICommonState => {
    return {
      ...state,
      viewsSettings: {
        ...state.viewsSettings,
        [action.uniqueId]: {
          ...state.viewsSettings[action.uniqueId],
          isLoading: false
        }
      }
    }
  },
  [OPEN_BAZAAR_SUCCESS]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      metadata: {
        ...state.metadata,
        data: {
          ...state.metadata.data,
          isClosed: false
        }
      }
    }
  },
  [CLOSE_BAZAAR_SUCCESS]: (state: ICommonState): ICommonState => {
    return {
      ...state,
      metadata: {
        ...state.metadata,
        data: {
          ...state.metadata.data,
          isClosed: true
        }
      }
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------

export default createReducer(commonInitialState, ACTION_HANDLERS)
