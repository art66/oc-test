import { TRarity } from '@torn/shared/utils/getItemGlowClassName/interfaces'
import { TItem } from './state'

export type TItemData = {
  itemID: string
  amount: string
  price: string
  name: string
  armoryID: string
  type: string
  type2: string
  bazaarID: string
  averageprice: string
  dmg: string
  accuracy: string
  arm: string
  glow?: {
    color: string
    opacity: number
  }
  rarity?: TRarity
  bonuses: {
    class: string
    title: string
  }[]
  isBlockedForBuying: boolean
}

export type TNormalizedItems = {
  list: TItem[]
  amount: number
  total: number
  uniqueKey: string
}

export type TPureItems = { list: TItemData[]; total: number; uniqueKey: string }

export type TItemsNormalizer = (items: TPureItems) => TNormalizedItems
