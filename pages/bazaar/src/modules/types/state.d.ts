import { BAZAAR_PAGE_ID, ADD_PAGE_ID, MANAGE_PAGE_ID, PERSONALIZE_PAGE_ID } from '../../constants'
import { TItemData } from '../normalizers'

export enum EID {
  Items = BAZAAR_PAGE_ID,
  AddItems = ADD_PAGE_ID,
  Personalize = MANAGE_PAGE_ID,
  ManageItems = PERSONALIZE_PAGE_ID
}

export type TColor = 'red' | 'green'

export type TBazaarName = string

export type TMsg = string

export type TInfoBox = {
  loading?: boolean
  msg?: string
  color?: TColor
  log?: string[]
}

export type TPageID = EID

export type TBazaarDataError = boolean | null

export interface IMetadata {
  isBazaarExists: boolean
  isClosed: boolean
  isYours: boolean
}

export interface IOwner {
  playername: string
  userId: string
}

export interface IBazaarData {
  name: string
  description: string
}

interface IExtras {
  cl: string
  icon: string
  title: string
  type: string
  value: string
  descColor?: string
  descTitle?: string
}

export interface IBazaarItemInfo {
  armoryID?: number
  uniqueId?: string
  id?: string
  itemCirculation?: string
  itemCost?: string
  itemID?: string
  itemInfo?: string
  itemInfoContent?: string
  itemName?: string
  itemRareIcon?: string
  itemRareTitle?: string
  itemSell?: string
  itemType?: string
  itemValue?: string
  test?: string
  extras?: IExtras[]
  glow?: { color: string; opacity: number; overlay: string }
}

export interface IViews {
  [id: string]: IBazaarItemInfo
}

export interface IBazaarItemInfoSettings {
  height: number
  isLoading: boolean
}

export interface IViewsSettings {
  [id: string]: IBazaarItemInfoSettings
}

export type TOwner = {
  isLoading: boolean
  data: IOwner
  error?: boolean
}

export interface ICommonState {
  appID: string
  pageID?: TPageID
  isDesktopLayoutSetted?: boolean
  dbg?: TMsg
  requestInfoBox: TInfoBox
  leaveRequestInfoBox: boolean
  infoBox: TInfoBox
  metadata?: {
    isLoading: boolean
    data: IMetadata
    error?: boolean
  }
  owner: TOwner
  bazaarData: {
    isLoading: boolean
    data: IBazaarData
    error: TBazaarDataError
  }
  views: IViews
  viewsSettings: IViewsSettings
}

export type TBonus = {
  className: string
  title?: string
}

export type TItem = {
  uniqueId: string
  itemId: string
  bazaarId: string
  name: string
  type: string
  price: number
  amount: number
  imgMediumUrl: string
  imgLargeUrl: string
  imgBlankUrl: string
  armoryId: string
  categoryName: string
  averagePrice: number
  damageBonus: string
  armorBonus: string
  accuracyBonus: String
  bonuses: TBonus[]
  minPrice: number
  isBlockedForBuying: boolean
  rarity?: TItemData['rarity']
  glow: TItemData['glow']
}
