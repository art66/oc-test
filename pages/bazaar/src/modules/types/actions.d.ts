import {
  SET_PAGE_ID,
  SHOW_DEBUG_INFO,
  HIDE_DEBUG_INFO,
  METADATA_REQUEST,
  METADATA_RESPONSE_SUCCESS,
  METADATA_RESPONSE_FAILURE,
  OWNER_REQUEST,
  OWNER_RESPONSE_SUCCESS,
  OWNER_RESPONSE_FAILURE,
  BAZAAR_DATA_REQUEST,
  BAZAAR_DATA_RESPONSE_SUCCESS,
  BAZAAR_DATA_RESPONSE_FAILURE,
  SET_BAZAAR_DATA,
  LINK_CHANGE
} from '../constants'
import {
  SET_ACTIVE_VIEW_HEIGHT,
  BAZAAR_ITEM_INFO_REQUEST,
  BAZAAR_ITEM_INFO_SUCCESS,
  BAZAAR_ITEM_INFO_FAILURE,
  SHOW_INFOBOX,
  SET_REQUEST_INFOBOX,
  SET_LEAVE_REQUEST_INFO_BOX
} from '../constants/actions'
import { IBazaarItemInfo, TMsg, TPageID, IMetadata, IOwner, IBazaarData, TInfoBox } from './state'

export type TSetRequestInfoBoxPayload = {
  type: typeof SET_REQUEST_INFOBOX
  infoBox: TInfoBox
}

export type TLeaveRequestInfoBoxPayload = {
  type: typeof SET_LEAVE_REQUEST_INFO_BOX
  leave: boolean
}

export type TShowInfoBoxPayload = {
  type: typeof SHOW_INFOBOX
  infoBox: TInfoBox
}

export interface ISetPageId {
  type: typeof SET_PAGE_ID
  pageID: TPageID
}

export interface IShowDebugInfo {
  type: typeof SHOW_DEBUG_INFO
  msg: TMsg
}

export interface IHideDebugInfo {
  type: typeof HIDE_DEBUG_INFO
}

export interface IMetadataRequest {
  type: typeof METADATA_REQUEST
}

export interface IMetadataResponseSuccess {
  type: typeof METADATA_RESPONSE_SUCCESS
  metadata: IMetadata
}

export interface IMetadataResponseFailure {
  type: typeof METADATA_RESPONSE_FAILURE
}

export interface IOwnerRequest {
  type: typeof OWNER_REQUEST
}

export interface IOwnerResponseSuccess {
  type: typeof OWNER_RESPONSE_SUCCESS
  owner: IOwner
}

export interface IOwnerResponseFailure {
  type: typeof OWNER_RESPONSE_FAILURE
}

export interface IBazaarDataRequest {
  type: typeof BAZAAR_DATA_REQUEST
}

export interface IBazaarDataResponseSuccess {
  type: typeof BAZAAR_DATA_RESPONSE_SUCCESS
  bazaarData: IBazaarData
}

export interface IBazaarDataResponseFailure {
  type: typeof BAZAAR_DATA_RESPONSE_FAILURE
}

export interface ISetBazaarData {
  type: typeof SET_BAZAAR_DATA
  bazaarData: IBazaarData
}

export interface ILinkChage {
  type: typeof LINK_CHANGE
  linkId: string
}

export type TSetActiveViewHeightPayload = {
  type: typeof SET_ACTIVE_VIEW_HEIGHT
  uniqueId: string
  height: number
}

export type TSetActiveViewHeightAction = (uniqueId: string, height: number) => TSetActiveViewHeightPayload

export interface IBazaarItemInfoRequestPayload {
  type: typeof BAZAAR_ITEM_INFO_REQUEST
  uniqueId: string
}

export type TBazaarItemInfoRequestAction = (uniqueId: string) => IBazaarItemInfoRequestPayload

export interface IBazaarItemInfoSuccessPayload {
  type: typeof BAZAAR_ITEM_INFO_SUCCESS
  uniqueId: string
  view: IBazaarItemInfo
}

export type TBazaarItemInfoSuccessAction = (uniqueId: string, view: IBazaarItemInfo) => IBazaarItemInfoSuccessPayload

export interface IBazaarItemInfoFailurePayload {
  type: typeof BAZAAR_ITEM_INFO_FAILURE
  uniqueId: string
}

export type TBazaarItemInfoFailureAction = (uniqueId: string) => IBazaarItemInfoFailurePayload
