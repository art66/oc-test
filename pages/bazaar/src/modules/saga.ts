import { takeLatest } from 'redux-saga/effects'

import { handleOnLinkClick } from './sagas'
import { LINK_CHANGE } from './constants'

export default function* rootSaga() {
  yield takeLatest(LINK_CHANGE, handleOnLinkClick)
}
