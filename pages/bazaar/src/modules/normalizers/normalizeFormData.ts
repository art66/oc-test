import lodashUnescape from 'lodash.unescape'

export default ({ name, announcement, state: { isClosed } }) => ({
  name,
  description: lodashUnescape(announcement),
  action: isClosed
})
