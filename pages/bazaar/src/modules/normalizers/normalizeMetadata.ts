export default ({ state: { isYours, isClosed, isBazaarExists } }) => ({
  isYours,
  isClosed,
  isBazaarExists
})
