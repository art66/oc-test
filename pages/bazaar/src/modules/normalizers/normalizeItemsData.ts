import { TItemsNormalizer } from '../types/normalizers'

const normalizeValue = value => (!isNaN(value) ? value : 0)

const normalizeBonus = value => (!isNaN(value) ? parseFloat(value).toFixed(2) : '0.00')

const normalizeItemsData: TItemsNormalizer = itemsData => {
  const { list, total, uniqueKey } = itemsData

  const normalizedList = list.map(
    ({
      itemID,
      amount,
      price,
      name,
      armoryID,
      type,
      type2,
      bazaarID,
      averageprice,
      glow,
      dmg,
      arm,
      accuracy,
      bonuses,
      isBlockedForBuying,
      rarity
    }) => {
      const normalizedAmount = parseInt(amount, 10)
      const normalizedPrice = parseInt(price, 10)
      const normalizedAveragePrice = parseInt(averageprice, 10)
      const normalizedDmg = parseFloat(dmg)
      const normalizedAccuracyBonus = parseFloat(accuracy)
      const normalizedArmorBonus = parseFloat(arm)

      return {
        uniqueId: `${itemID}-${armoryID}`,
        itemId: itemID,
        amount: normalizeValue(normalizedAmount),
        name,
        price: normalizeValue(normalizedPrice),
        imgMediumUrl: `/images/items/${itemID}/medium.png`,
        imgLargeUrl: `/images/items/${itemID}/large.png?v=1528808940574`,
        imgBlankUrl: `/images/items/${itemID}/blank.png`,
        armoryId: armoryID,
        type,
        categoryName: type2,
        bazaarId: bazaarID,
        averagePrice: normalizeValue(normalizedAveragePrice),
        glow,
        damageBonus: normalizeBonus(normalizedDmg),
        accuracyBonus: normalizeBonus(normalizedAccuracyBonus),
        armorBonus: normalizeBonus(normalizedArmorBonus),
        bonuses: bonuses.map(bonus => ({ className: bonus.class, title: bonus.title || null })),
        minPrice: 1 /* Math.ceil(normalizedAveragePrice * 0.25) */, // FIXME: Update tests for it
        // FIXME: Limits may be
        // minPrice: Math.ceil(normalizedAveragePrice * 0.25) * normalizedAmount
        isBlockedForBuying,
        rarity
      }
    }
  )

  return {
    list: normalizedList,
    amount: normalizedList.length,
    total,
    uniqueKey
  }
}

export default normalizeItemsData
