export default ({ owner: { userId, playername } }) => ({
  userId,
  playername
})
