import { IBazaarItemInfo } from '../types/state'

export const normalizeItemInfo = (info): IBazaarItemInfo => {
  return {
    ...info,
    id: info.itemID,
    uniqueId: `${info.itemID}-${info.armoryID}`
  }
}
