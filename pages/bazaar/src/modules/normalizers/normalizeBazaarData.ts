import lodashUnescape from 'lodash.unescape'

export default ({ name, description }) => ({
  name,
  description: lodashUnescape(description)
})
