import { createSelector } from 'reselect'
import {
  BAZAAR_PAGE_ID,
  PERSONALIZE_PAGE_ID,
  TABLET_WIDTH,
  MOBILE_WIDTH,
  DESKTOP_WIDTH,
  TABLET_TYPE,
  DESKTOP_TYPE,
  MOBILE_TYPE
} from '../../constants'
import {
  ADD_TITLES,
  MANAGE_TITLES,
  ITEM_MARKET_LINK,
  YOUR_ITEMS_LINK,
  ADD_ITEMS_LINK,
  MANAGE_ITEMS_LINK,
  PERSONALIZE_LINK,
  ADD_ITEMS_LINKS,
  PERSONALIZE_LINKS,
  MANAGE_ITEMS_LINKS,
  DEFAULT_TITLE
} from '../constants'
import { IState } from '../../store/types'
import { OPEN_LINK, CLOSE_LINK } from '../constants/links'

export const getMediaType = (state: IState) =>
  state.common.isDesktopLayoutSetted ? DESKTOP_TYPE : state.browser.mediaType
export const getMetadata = (state: IState) => state.common.metadata.data
export const metadataLoadedSelector = (state: IState): boolean => !!state.common.metadata.data
export const getOwner = (state: IState) => {
  return state.common.owner.data
}
export const getBazaarName = (state: IState) => state.common.bazaarData.data.name
export const getView = (id) => (state: IState) => state.common.views[id]

export const getScreenWidth = createSelector([getMediaType], (mediaType): number => {
  switch (mediaType) {
    case TABLET_TYPE: {
      return TABLET_WIDTH
    }
    case MOBILE_TYPE: {
      return MOBILE_WIDTH
    }
    default: {
      return DESKTOP_WIDTH
    }
  }
})

export const getClosedLink = () => ({
  ...CLOSE_LINK,
  icon: 'BazaarClosed'
})

export const getOpenLink = () => ({
  ...OPEN_LINK,
  icon: 'BazaarOpen'
})

export const getYourProfileLink = createSelector([(data) => data], ({ userId }) => ({
  href: `profiles.php?XID=${userId}`,
  label: 'back',
  icon: 'Back',
  title: 'Your Profile'
}))

export const getOwnerProfileLink = createSelector([(data) => data], ({ userId, playername }) => {
  return {
    href: `profiles.php?XID=${userId}`,
    label: 'back',
    icon: 'Back',
    title: `${playername}'s Profile`
  }
})

export const getItems = createSelector(
  [getMetadata, getOwner, metadataLoadedSelector],
  (metadata, { userId, playername }, metadataLoaded) => {
    const items = []

    if (metadataLoaded) {
      items.push(YOUR_ITEMS_LINK, ITEM_MARKET_LINK)

      if (metadata?.isYours) {
        if (metadata?.isBazaarExists) {
          items.push(getClosedLink(), getOpenLink(), ADD_ITEMS_LINK, MANAGE_ITEMS_LINK, PERSONALIZE_LINK)
        }
        items.push(getYourProfileLink({ userId }))
      } else {
        items.push(getOwnerProfileLink({ userId, playername }))
      }
    }

    return items
  }
)

export const getItemsLinks = createSelector([getItems], (items) => {
  const isDropDownLayout = items.length >= 4

  return {
    ID: BAZAAR_PAGE_ID,
    isDropDownLayout,
    items
  }
})

export const getLinks = createSelector([getItemsLinks], (itemsLinks) => {
  return {
    default: itemsLinks,
    list: [itemsLinks, ADD_ITEMS_LINKS, PERSONALIZE_LINKS, MANAGE_ITEMS_LINKS]
  }
})

export const getTitles = createSelector([getBazaarName, metadataLoadedSelector], (bazaarName) => {
  const title = bazaarName || DEFAULT_TITLE

  const itemsTitle = {
    ID: BAZAAR_PAGE_ID,
    title
  }

  return {
    default: itemsTitle,
    list: [
      itemsTitle,
      ADD_TITLES,
      MANAGE_TITLES,
      {
        ID: PERSONALIZE_PAGE_ID,
        title
      }
    ]
  }
})

export const getHeadersData = createSelector([getTitles, getLinks], (titles, links) => {
  return {
    titles,
    links
  }
})
