import { ADD_PAGE_ID, MANAGE_PAGE_ID, PERSONALIZE_PAGE_ID } from '../../constants'

export const ITEM_MARKET_LINK = {
  href: 'imarket.php',
  label: 'item-market',
  icon: 'ItemMarket',
  title: 'Item Market'
}

export const YOUR_ITEMS_LINK = {
  href: 'item.php',
  label: 'your-items',
  icon: 'Items',
  title: 'Your Items'
}

export const BACK_LINK = {
  href: '#/',
  label: 'back-to',
  icon: 'Back',
  title: 'Back'
}

export const PERSONALIZE_LINK = {
  href: '#/personalize',
  label: 'personalize',
  icon: 'Personalize',
  title: 'Personalize'
}

export const ADD_ITEMS_LINK = {
  href: '#/add',
  label: 'add-items',
  icon: 'ItemsAdd',
  title: 'Add items'
}

export const MANAGE_ITEMS_LINK = {
  href: '#/manage',
  label: 'manage-items',
  icon: 'ManageItems',
  title: 'Manage items'
}

export const OPEN_LINK = {
  id: 'OPEN_BAZAAR',
  href: '#',
  label: 'bazaar-state',
  icon: 'BazaarStateOpen',
  title: 'Open'
}

export const CLOSE_LINK = {
  id: 'CLOSE_BAZAAR',
  href: '#',
  label: 'bazaar-state',
  icon: 'BazaarStateClosed',
  title: 'Closed'
}

export const ADD_ITEMS_LINKS = {
  ID: ADD_PAGE_ID,
  items: [MANAGE_ITEMS_LINK, PERSONALIZE_LINK, BACK_LINK]
}

export const PERSONALIZE_LINKS = {
  ID: PERSONALIZE_PAGE_ID,
  items: [MANAGE_ITEMS_LINK, ADD_ITEMS_LINK, BACK_LINK]
}

export const MANAGE_ITEMS_LINKS = {
  ID: MANAGE_PAGE_ID,
  items: [ADD_ITEMS_LINK, PERSONALIZE_LINK, BACK_LINK]
}
