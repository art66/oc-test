import { BAZAAR_PAGE_ID, ADD_PAGE_ID, MANAGE_PAGE_ID } from '../../constants'

export const DEFAULT_TITLE = 'Bazaar'

export const ITEMS_TITLES = {
  ID: BAZAAR_PAGE_ID,
  title: DEFAULT_TITLE
}

export const ADD_TITLES = {
  ID: ADD_PAGE_ID,
  title: DEFAULT_TITLE
}

export const MANAGE_TITLES = {
  ID: MANAGE_PAGE_ID,
  title: DEFAULT_TITLE
}
