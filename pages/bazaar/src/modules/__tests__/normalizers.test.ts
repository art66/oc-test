import { normalizeItemInfo } from '../normalizers/normalizeItemInfo'
import { viewInfo, viewInfoData } from './mocks/viewInfo'

describe('Bazaar items normalizers', () => {
  it('should handle normalizeItemInfo works correctly', () => {
    expect(normalizeItemInfo(viewInfoData)).toEqual(viewInfo)
  })
})
