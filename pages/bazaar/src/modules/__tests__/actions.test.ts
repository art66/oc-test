import {
  showDebugInfo,
  hideDebugInfo,
  setPageId,
  metadataRequest,
  metadataResponseSuccess,
  ownerRequest,
  ownerResponseSuccess,
  bazaarDataRequest,
  bazaarDataResponseSuccess,
  bazaarDataResponseFailure,
  setBazaarData,
  linkChage,
  setActiveViewHeight,
  bazaarItemInfoRequest,
  bazaarItemInfoSuccess,
  bazaarItemInfoFailure,
  showInfoBox,
  leaveRequestInfoBox
} from '../actions'
import { metadata, owner, bazaarData } from './mocks'
import { viewInfoWithExtras } from './mocks/viewInfo'
import { TColor } from '../types/state'

describe('bazaar module actions', () => {
  it('should handle showInfoBox action', () => {
    const infoBox = {
      msg: 'test infobox message',
      color: 'red' as TColor,
      log: ['Log message 1', 'Log message 2', 'Log message 3', 'Log message 4', 'Log message 5']
    }
    const expectedAction = {
      type: 'SHOW_INFOBOX',
      infoBox
    }

    expect(showInfoBox(infoBox)).toEqual(expectedAction)
  })

  it('should handle leaveRequestInfoBox action', () => {
    const expectedAction = {
      type: 'SET_LEAVE_REQUEST_INFO_BOX',
      leave: true
    }

    expect(leaveRequestInfoBox(true)).toEqual(expectedAction)
  })

  it('should handle leaveRequestInfoBox action with false value', () => {
    const expectedAction = {
      type: 'SET_LEAVE_REQUEST_INFO_BOX',
      leave: false
    }

    expect(leaveRequestInfoBox(false)).toEqual(expectedAction)
  })

  it('should handle showDebugInfo action', () => {
    const msg = 'test message'
    const expectedAction = {
      type: 'SHOW_DEBUG_INFO',
      msg
    }

    expect(showDebugInfo(msg)).toEqual(expectedAction)
  })

  it('should handle hideDebugInfo action', () => {
    const expectedAction = {
      type: 'HIDE_DEBUG_INFO'
    }

    expect(hideDebugInfo()).toEqual(expectedAction)
  })

  it('should handle setPageId action', () => {
    const pageID = '0'
    const expectedAction = {
      type: 'SET_PAGE_ID',
      pageID
    }

    expect(setPageId(pageID)).toEqual(expectedAction)
  })

  it('should handle metadataRequest action', () => {
    const expectedAction = {
      type: 'METADATA_REQUEST'
    }

    expect(metadataRequest()).toEqual(expectedAction)
  })

  it('should handle metadataResponseSuccess action', () => {
    const expectedAction = {
      type: 'METADATA_RESPONSE_SUCCESS',
      metadata
    }

    expect(metadataResponseSuccess(metadata)).toEqual(expectedAction)
  })

  it('should handle ownerRequest action', () => {
    const expectedAction = {
      type: 'OWNER_REQUEST'
    }

    expect(ownerRequest()).toEqual(expectedAction)
  })

  it('should handle ownerResponseSuccess action', () => {
    const expectedAction = {
      type: 'OWNER_RESPONSE_SUCCESS',
      owner
    }

    expect(ownerResponseSuccess(owner)).toEqual(expectedAction)
  })

  it('should handle bazaarDataRequest action', () => {
    const expectedAction = {
      type: 'BAZAAR_DATA_REQUEST'
    }

    expect(bazaarDataRequest()).toEqual(expectedAction)
  })

  it('should handle bazaarDataResponseSuccess action', () => {
    const expectedAction = {
      type: 'BAZAAR_DATA_RESPONSE_SUCCESS',
      bazaarData
    }

    expect(bazaarDataResponseSuccess(bazaarData)).toEqual(expectedAction)
  })

  it('should handle bazaarDataResponseFailure action', () => {
    const expectedAction = {
      type: 'BAZAAR_DATA_RESPONSE_FAILURE'
    }

    expect(bazaarDataResponseFailure()).toEqual(expectedAction)
  })

  it('should handle setBazaarData action', () => {
    const expectedAction = {
      type: 'SET_BAZAAR_DATA',
      bazaarData
    }

    expect(setBazaarData(bazaarData)).toEqual(expectedAction)
  })

  it('should handle linkChage action', () => {
    const expectedAction = {
      type: 'LINK_CHANGE',
      linkId: 1
    }

    expect(linkChage(1)).toEqual(expectedAction)
  })

  it('should handle setActiveViewHeight action', () => {
    const uniqueId = '4635556'
    const height = 256
    const expectedAction = {
      type: 'SET_ACTIVE_VIEW_HEIGHT',
      uniqueId,
      height
    }

    expect(setActiveViewHeight(uniqueId, height)).toEqual(expectedAction)
  })

  it('should handle bazaarItemInfoRequest action', () => {
    const uniqueId = '4635556'
    const expectedAction = {
      type: 'BAZAAR_ITEM_INFO_REQUEST',
      uniqueId
    }

    expect(bazaarItemInfoRequest(uniqueId)).toEqual(expectedAction)
  })

  it('should handle bazaarItemInfoSuccess action', () => {
    const uniqueId = '4635556'
    const expectedAction = {
      type: 'BAZAAR_ITEM_INFO_SUCCESS',
      uniqueId,
      view: viewInfoWithExtras
    }

    expect(bazaarItemInfoSuccess(uniqueId, viewInfoWithExtras)).toEqual(expectedAction)
  })

  it('should handle bazaarItemInfoFailure action', () => {
    const uniqueId = '4635556'
    const expectedAction = {
      type: 'BAZAAR_ITEM_INFO_FAILURE',
      uniqueId
    }

    expect(bazaarItemInfoFailure(uniqueId)).toEqual(expectedAction)
  })
})
