import { IMetadata, IOwner, IBazaarData } from '../../types/state'

export const metadata: IMetadata = {
  isBazaarExists: true,
  isYours: false,
  isClosed: true
}

export const owner: IOwner = { userId: '343253', playername: 'test_username' }

export const bazaarData: IBazaarData = {
  name: 'test bazaar name',
  description: '<p>test bazaar description</p>'
}
