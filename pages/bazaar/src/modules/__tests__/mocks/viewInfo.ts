import { IBazaarItemInfo, IBazaarItemInfoSettings } from '../../types/state'

export const viewInfoData = {
  itemName: 'Small First Aid Kit',
  itemType: 'Item',
  itemID: '68',
  armoryID: 0,
  itemInfo: 'This kit will stop you bleeding on your clothes and will shorten your hospital stay by a small amount.',
  itemCost: '$20,000',
  itemSell: 'N/A',
  itemValue: '$20,518',
  itemCirculation: '12,991,387',
  itemRareIcon: 'very-common-rarity',
  itemRareTitle: '<p>Very Common</p> 12,991,387 total in circulation',
  itemInfoContent:
    `↵            <div class='m-bottom10'>↵                <span class="bold">The Small First Aid Kit` +
    `</span> is a  Medical Item.↵            </div>↵            This kit will stop you bleeding on ` +
    `your clothes and will shorten your hospital stay by a small amount.↵            <div class="t ` +
    `- green bold item- effect m - top1,0">Effect: Reduces hospital time by 20 minutes and replenis` +
    `hes life by 3.25%.</div>`,
  extras: [],
  test: null
}

export const viewInfoInitialState: IBazaarItemInfo = {
  uniqueId: 'juin78ubj',
  id: null,
  itemCirculation: null,
  itemCost: null,
  itemID: null,
  itemInfo: null,
  itemInfoContent: null,
  itemName: null,
  itemRareIcon: null,
  itemRareTitle: null,
  itemSell: null,
  itemType: null,
  itemValue: null,
  test: null,
  extras: []
}

export const viewInfo: IBazaarItemInfo = {
  armoryID: 0,
  extras: [],
  id: '68',
  itemCirculation: '12,991,387',
  itemCost: '$20,000',
  itemID: '68',
  itemInfo: 'This kit will stop you bleeding on your clothes and will shorten your hospital stay by a small amount.',
  itemInfoContent:
    `↵            <div class='m-bottom10'>↵                <span class="bold">The Small First Aid Kit` +
    `</span> is a  Medical Item.↵            </div>↵            This kit will stop you bleeding on ` +
    `your clothes and will shorten your hospital stay by a small amount.↵            <div class="t ` +
    `- green bold item- effect m - top1,0">Effect: Reduces hospital time by 20 minutes and replenis` +
    `hes life by 3.25%.</div>`,
  itemName: 'Small First Aid Kit',
  itemRareIcon: 'very-common-rarity',
  itemRareTitle: '<p>Very Common</p> 12,991,387 total in circulation',
  itemSell: 'N/A',
  itemType: 'Item',
  itemValue: '$20,518',
  test: null,
  uniqueId: '68-0'
}

export const viewInfoWithExtras: IBazaarItemInfo = {
  ...viewInfo,
  extras: [
    {
      cl: 't-left',
      descColor: 'bonus-increase',
      descTitle: '+81.45',
      icon: 'bonus-attachment-item-damage-bonus',
      title: 'Damage',
      type: 'text',
      value: '81.45'
    },
    {
      cl: 't-right',
      descColor: 'bonus-increase',
      descTitle: '+45.26',
      icon: 'bonus-attachment-item-accuracy-bonus',
      title: 'Accuracy',
      type: 'text',
      value: '45.26'
    },
    {
      cl: 't-left',
      icon: 'bonus-attachment-item-rof-bonus',
      title: 'Rate of Fire',
      type: 'text',
      value: '10-15'
    },
    {
      cl: 't-right',
      descColor: 'bonus-increase',
      descTitle: '+3.10',
      icon: 'bonus-attachment-item-stealth-bonus',
      title: 'Stealth',
      type: 'text',
      value: '3.1'
    },
    {
      cl: 't-left',
      icon: 'bonus-attachment-item-ammo-bonus',
      title: 'Ammo',
      type: 'text',
      value: '9mm Parabellum Round'
    },
    {
      cl: 't-right',
      icon: 'bonus-attachment-item-clip-bonus',
      title: 'Clip',
      type: 'text',
      value: ` <span class="t- gray - 6">3 x 120 (0)</span>`
    },
    {
      cl: 't-left',
      descTitle: '<b>Emasculate</b><br/>Happiness increased by 15% of maximum upon final hit',
      icon: 'bonus-attachment-emasculated',
      title: 'Bonus',
      type: 'text',
      value: 'Emasculate'
    }
  ]
}

export const viewInfoDataWithError = {
  ...viewInfoData,
  error: 'Test error message'
}

export const viewInfoWithSettings: IBazaarItemInfoSettings = {
  isLoading: false,
  height: 126
}

export const viewInfoSettingsInitialState: IBazaarItemInfoSettings = {
  isLoading: true,
  height: 0
}

export default viewInfo
