import { IState } from '../../../store/types'

export const state: IState = {
  browser: {
    mediaType: 'desktop'
  },
  bazaar: {
    searchBar: { sortBy: 'default', orderBy: 'asc', search: '' },
    // @ts-ignore
    bazaarItems: {
      list: [],
      total: 0
    },
    activeView: {
      uniqueId: null,
      itemId: null,
      armoryId: null
    }
  },
  // @ts-ignore
  common: {
    views: {},
    viewsSettings: {}
  }
}

export const tabletState: IState = {
  ...state,
  browser: {
    mediaType: 'tablet'
  }
}

export const mobileState: IState = {
  ...state,
  browser: {
    mediaType: 'mobile'
  }
}

export const desktopForceDesktopState: IState = {
  ...state,
  browser: {
    mediaType: 'desktop'
  },
  common: {
    ...state.common,
    isDesktopLayoutSetted: true
  }
}

export const tabletForceDesktopState: IState = {
  ...state,
  browser: {
    mediaType: 'tablet'
  },
  common: {
    ...state.common,
    isDesktopLayoutSetted: true
  }
}

export const mobileForceDesktopState: IState = {
  ...state,
  browser: {
    mediaType: 'mobile'
  },
  common: {
    ...state.common,
    isDesktopLayoutSetted: true
  }
}

export const globalInitialState: IState = {
  common: {
    appID: 'Bazaar',
    pageID: null,
    infoBox: null,
    requestInfoBox: {
      loading: false,
      msg: null,
      color: null,
      log: null
    },
    leaveRequestInfoBox: false,
    dbg: null,
    metadata: {
      isLoading: false,
      data: { isBazaarExists: false, isYours: false, isClosed: false },
      error: null
    },
    owner: {
      isLoading: false,
      data: {
        userId: '2265266',
        playername: 'love__intent'
      },
      error: null
    },
    bazaarData: {
      isLoading: false,
      data: {
        name: '',
        description: ''
      },
      error: null
    },
    isDesktopLayoutSetted: null,
    views: {},
    viewsSettings: {}
  }
}
