import { getScreenWidth, getMediaType } from '../selectors'
import {
  state,
  tabletState,
  mobileState,
  mobileForceDesktopState,
  tabletForceDesktopState,
  desktopForceDesktopState
} from './mocks/state'

describe('Bazaar selectors', () => {
  it('getMediaType on desktop', () => {
    expect(getMediaType(state)).toBe('desktop')
  })

  it('getMediaType on tablet', () => {
    expect(getMediaType(tabletState)).toBe('tablet')
  })

  it('getMediaType on mobile', () => {
    expect(getMediaType(mobileState)).toBe('mobile')
  })

  it('getMediaType on desktop force desktop', () => {
    expect(getMediaType(desktopForceDesktopState)).toBe('desktop')
  })

  it('getMediaType on tablet force desktop', () => {
    expect(getMediaType(tabletForceDesktopState)).toBe('desktop')
  })

  it('getMediaType on mobile force desktop', () => {
    expect(getMediaType(mobileForceDesktopState)).toBe('desktop')
  })

  it('getScreenWidth on desktop', () => {
    expect(getScreenWidth(state)).toBe(784)
  })

  it('getScreenWidth on tablet', () => {
    expect(getScreenWidth(tabletState)).toBe(386)
  })

  it('getScreenWidth on mobile', () => {
    expect(getScreenWidth(mobileState)).toBe(320)
  })
})
