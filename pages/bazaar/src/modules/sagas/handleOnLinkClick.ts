import { select, put } from 'redux-saga/effects'

import { BAZAAR_PAGE_ID } from '../../constants'
import { openBazaar, closeBazaar } from '../../routes/Bazaar/modules/actions'

import { ILinkChage } from '../types/actions'
import { IState } from '../../store/types'
import { OPEN_LINK, CLOSE_LINK } from '../constants/links'

export default function* handleOnLinkClick({ linkId }: ILinkChage) {
  const {
    common: { pageID }
  }: IState = yield select()

  if (pageID === BAZAAR_PAGE_ID) {
    if (linkId === CLOSE_LINK.id) {
      yield put(closeBazaar())
    } else if (linkId === OPEN_LINK.id) {
      yield put(openBazaar())
    }
  }
}
