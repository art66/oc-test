import handleOnLinkClick from './handleOnLinkClick'
import hideInfobox from './hideInfobox'

export { handleOnLinkClick, hideInfobox }
