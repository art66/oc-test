import { put } from 'redux-saga/effects'
import { setPageId } from '../actions'

export default function* setPageIdSaga(pageId: number) {
  yield put(setPageId(pageId))
}
