import { runSaga } from 'redux-saga'
import fetchMock from 'fetch-mock'
import { bazaarItemInfoRequest, bazaarItemInfoSuccess, bazaarItemInfoFailure } from '../../actions'
import viewInfo, { viewInfoData, viewInfoDataWithError } from '../../__tests__/mocks/viewInfo'
import bazaarItemInfoRequestSaga, { createBazaarItemInfoUrl } from '../bazaarItemInfoRequest'
import { SET_ACTIVE_VIEW_ID } from '../../../routes/Bazaar/modules/constants'
import { setActiveViewUniqueId } from '../../../routes/Bazaar/modules/actions'

describe('Bazaar modules bazaarItemInfoRequestSaga sagas', () => {
  afterEach(fetchMock.reset)

  it('Should handle successfully', async () => {
    const dispatched = []

    fetchMock.get(createBazaarItemInfoUrl({ itemId: '68', armoryId: '0' }), viewInfoData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({
          common: {
            views: {},
            viewSettings: {}
          }
        })
      },
      bazaarItemInfoRequestSaga,
      { type: SET_ACTIVE_VIEW_ID, uniqueId: '68-0' }
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemInfoRequest('68-0'), bazaarItemInfoSuccess('68-0', viewInfo)])
  })

  it('Should handle failure with registered error', async () => {
    const dispatched = []

    fetchMock.get(createBazaarItemInfoUrl({ itemId: '68', armoryId: '0' }), viewInfoDataWithError, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({
          common: {
            views: {},
            viewSettings: {}
          }
        })
      },
      bazaarItemInfoRequestSaga,
      { type: SET_ACTIVE_VIEW_ID, uniqueId: '68-0' }
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemInfoRequest('68-0'), bazaarItemInfoFailure('68-0')])
  })

  it('Should handle failure', async () => {
    const dispatched = []

    fetchMock.get(
      createBazaarItemInfoUrl({ itemId: '68', armoryId: '0' }),
      () => {
        throw 'Error message'
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({
          common: {
            views: {},
            viewSettings: {}
          }
        })
      },
      bazaarItemInfoRequestSaga,
      { type: SET_ACTIVE_VIEW_ID, uniqueId: '68-0' }
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemInfoRequest('68-0'), bazaarItemInfoFailure('68-0')])
  })

  it('Should omit handle if view in cache', () => {
    const generator = bazaarItemInfoRequestSaga(setActiveViewUniqueId('68-0'))

    generator.next()
    /*
     * FIXME:
     *   Should check by:
     *     expect(generator.next().value).toMatchObject(select(getView('68-0')))
     *   But receive "serializes to the same string" error.
     */

    expect(generator.next(true).done).toBe(true)
  })
})
