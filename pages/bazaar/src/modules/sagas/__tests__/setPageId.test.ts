import { setPageId } from '../../actions'
import { runSaga } from 'redux-saga'
import setPageIdSaga from '../setPageId'

describe('Bazaar module setPageId saga', () => {
  it('Should handle successfully', async () => {
    const dispatched = []

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({})
      },
      setPageIdSaga,
      6
    ).toPromise()

    expect(dispatched).toEqual([setPageId(6)])
  })
})
