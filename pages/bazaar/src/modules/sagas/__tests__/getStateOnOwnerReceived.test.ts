import { select, take } from 'redux-saga/effects'
import getStateOnOwnerReceived from '../getStateOnOwnerReceived'
import { OWNER_RESPONSE_SUCCESS, OWNER_RESPONSE_FAILURE } from '../../constants'
import { globalInitialState, globalWithoutOwnerState } from '../../../routes/Bazaar/modules/__tests__/mocks/state'

describe('Bazaar module getStateOnOwnerReceived saga', () => {
  it('Should handle saga correctly when owner data has loaded', () => {
    const generator = getStateOnOwnerReceived()

    expect(generator.next().value).toEqual(select())

    const result = generator.next(globalInitialState)

    expect(result.value).toEqual(globalInitialState)

    expect(result.done).toBe(true)
  })

  it('Should handle saga correctly when owner data hasn\'t loaded', () => {
    const generator = getStateOnOwnerReceived()

    expect(generator.next().value).toEqual(select())

    expect(generator.next(globalWithoutOwnerState).value).toEqual(
      take([OWNER_RESPONSE_SUCCESS, OWNER_RESPONSE_FAILURE])
    )

    expect(generator.next().value).toEqual(select())

    const result = generator.next(globalInitialState)

    expect(result.value).toEqual(globalInitialState)

    expect(result.done).toBe(true)
  })
})
