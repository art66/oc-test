import { put, select } from 'redux-saga/effects'
import qs from 'qs'

import { fetchUrl } from '@torn/shared/utils'
import { TSetActiveViewPayload } from '../../routes/Bazaar/modules/types/actions'
import { getView } from '../selectors'
import { bazaarItemInfoRequest, bazaarItemInfoSuccess, bazaarItemInfoFailure } from '../actions'
import { normalizeItemInfo } from '../normalizers/normalizeItemInfo'
import { BAZAAR_ITEM_INFO_URL } from '../constants/urls'

export const createBazaarItemInfoUrl = ({ itemId, armoryId }) =>
  `${BAZAAR_ITEM_INFO_URL}&${qs.stringify({
    itemID: itemId,
    armouryID: armoryId,
    asObject: true
  })}`

export default function* bazaarItemInfoRequestSaga({ uniqueId }: TSetActiveViewPayload) {
  const view = yield select(getView(uniqueId))
  if (view) {
    // If view info alreandy has loaded
    return
  }

  try {
    yield put(bazaarItemInfoRequest(uniqueId))

    const separatorIndex = uniqueId.indexOf('-')
    const itemId = uniqueId.slice(0, separatorIndex)
    const armoryId = uniqueId.slice(separatorIndex + 1)
    // FIXME: Need to do it better

    const itemInfo = yield fetchUrl(createBazaarItemInfoUrl({ itemId, armoryId }), null, null)

    if (itemInfo.error) {
      throw itemInfo.error
    }

    const itemInfoNormalized = normalizeItemInfo(itemInfo)

    yield put(bazaarItemInfoSuccess(uniqueId, itemInfoNormalized))
  } catch (error) {
    yield put(bazaarItemInfoFailure(uniqueId))
  }
}
