import { select, take } from 'redux-saga/effects'

import { OWNER_RESPONSE_SUCCESS, OWNER_RESPONSE_FAILURE } from '../constants'

import { IState } from '../../store/types'

export default function* getStateOnOwnerReceivedSaga() {
  const state: IState = yield select()

  const { userId } = state.common.owner.data

  if (!userId) {
    yield take([OWNER_RESPONSE_SUCCESS, OWNER_RESPONSE_FAILURE])

    return yield select()
  }

  return state
}
