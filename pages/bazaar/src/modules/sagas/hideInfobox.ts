import { select, put } from 'redux-saga/effects'
import { IState } from '../../store/types'
import { showInfoBox } from '../actions'

export default function* hideInfobox() {
  const {
    common: { infoBox }
  }: IState = yield select()

  if (infoBox) {
    yield put(showInfoBox(null))
  }
}

// FIXME: Should write tests
