import subscribeOnDesktopLayout from '@torn/shared/utils/desktopLayoutChecker'
import { commonInitialState } from '../modules/reducers'
import { IState } from './types'

const initialState: IState = {
  common: commonInitialState
}

subscribeOnDesktopLayout(payload => (initialState.common.isDesktopLayoutSetted = payload))

export default initialState
