import { ICommonState } from '../modules/types'
import { IBazaarState } from '../routes/Bazaar/modules/types/state'
import { MOBILE_TYPE, TABLET_TYPE, DESKTOP_TYPE } from '../constants'
import { TManageState } from '../routes/Manage/modules/types'
import { IPersonalizeState } from '../routes/Personalize/modules/types/state'

export interface IState {
  common: ICommonState
  browser?: {
    mediaType: typeof MOBILE_TYPE | typeof TABLET_TYPE | typeof DESKTOP_TYPE
  }
  bazaar?: IBazaarState
  manage?: TManageState
  personalize?: IPersonalizeState
}
