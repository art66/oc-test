import { showDebugInfo } from '../../modules/actions'

export const fetchInterceptor = store => next => action => {
  if (action.meta === 'ajax' && action.json) {
    if (action.json.DB.debugInfo) {
      const stringInfo = JSON.stringify(action.json.DB.debugInfo, null, '\t')

      store.dispatch(showDebugInfo(`DebugInfo:\n\n${stringInfo}`))
    } else if (action.json.DB.redirect !== undefined) {
      if (action.json.DB.redirect === true) {
        location.href = location.protocol + '//' + location.hostname + '/' + action.json.url
      }
    }
  }

  return next(action)
}
