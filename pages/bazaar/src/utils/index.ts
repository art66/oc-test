export const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

export const capitalizeFirstLetter = text => text.charAt(0).toUpperCase() + text.slice(1)

export const numberToCurrency = value => value.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ',')

export const stringToCurrency = value => {
  if (value.includes('.')) {
    const [int, float] = value.split('.')

    return `${int.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}.${float}`
  }

  return value.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

// export const stringToCurrency = (amount: string, decimal = '.', thousands = ',') => {
//   try {
//     let i = Math.abs(Number(amount) || 0).toString()
//     let j = i.length > 3 ? i.length % 3 : 0

//     return (
//       (j ? i.substr(0, j) + thousands : '') +
//       i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
//       decimal +
//       Math.abs(parseFloat(amount) - parseFloat(i))
//     )
//   } catch (e) {
//     console.log(e)
//   }
// }
