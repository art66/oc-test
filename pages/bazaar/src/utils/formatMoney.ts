export const getInputMatch = (value: string, re: RegExp) => re.exec(value)?.map(match => parseFloat(match))

function digit(value, options): number {
  const inputMatch = getInputMatch(value, new RegExp('^(' + options.reNumsSign + '[1-9]\\d*)$', 'i'))

  return inputMatch ? inputMatch[1] : null
}

function float(value, options): number {
  const inputMatch = getInputMatch(
    value,
    new RegExp('^(' + options.reNumsSign + '[1-9]\\d*(?:[,]\\d{3})*)(?:[.]\\d{10})?$', 'i')
  )

  return inputMatch ? inputMatch[1] : null
}

function all(value, options): number {
  const inputMatch = getInputMatch(value, /^(all|max){1}$/i)

  return inputMatch && options.limit ? options.limit : null
}

function thousand(value, options): number {
  const inputMatch = getInputMatch(value, new RegExp('^(' + options.reNumsSign + '\\d+[.]?(\\d{1,3})?)k$', 'i'))

  return inputMatch ? Math.round(inputMatch[1] * 1000) : null
}

function million(value, options): number {
  const inputMatch = getInputMatch(value, new RegExp('^(' + options.reNumsSign + '\\d+[.]?(\\d{1,6})?)m$', 'i'))

  return inputMatch ? Math.round(inputMatch[1] * 1000000) : null
}

function billion(value, options): number {
  const inputMatch = getInputMatch(value, new RegExp('^(' + options.reNumsSign + '\\d+[.]?(\\d{1,9})?)b$', 'i'))

  return inputMatch ? Math.round(inputMatch[1] * 1000000000) : null
}

function quarter(value, options): number {
  const inputMatch = getInputMatch(value, /^(1\/4|quarter){1}$/i)

  return inputMatch && options.limit ? Math.round(parseInt(options.limit) / 4) : null
}

function third(value, options): number {
  const inputMatch = getInputMatch(value, /^(1\/3){1}$/i)

  return inputMatch && options.limit ? Math.round(parseInt(options.limit) / 3) : null
}

function half(value, options): number {
  const inputMatch = getInputMatch(value, /^(1\/2|half){1}$/i)

  return inputMatch && options.limit ? Math.round(parseInt(options.limit) / 2) : null
}

function percent(value, options): number {
  const inputMatch = getInputMatch(value, /^([1-9][0-9]?|100)%$/i)

  return inputMatch && options.limit ? Math.round((parseInt(options.limit) * inputMatch[1]) / 100) : null
}

function firstZero(value, options): number {
  const inputMatch = getInputMatch(value, /^([0])/i)
  const limitValue = options.limit ? parseInt(options.limit.replace(/,/g, '')) : null

  return inputMatch && limitValue == 0 ? inputMatch[1] : null
}

function zero(value, options): number {
  const inputMatch = getInputMatch(value, /^([0])$/i)

  return inputMatch && !options.strictMode ? inputMatch[1] : null
}

function fraction(value, options): number {
  const inputMatch = getInputMatch(value, /^(([1-9])\/([2-9]|10))$/i)

  return inputMatch && options.limit && inputMatch[2] < inputMatch[3]
    ? Math.round((parseInt(options.limit) * inputMatch[2]) / inputMatch[3])
    : null
}

const formatters = {
  digit,
  float,
  all,
  thousand,
  million,
  billion,
  quarter,
  third,
  half,
  percent,
  firstZero,
  zero,
  fraction
}

export type TFormatterOptions = {
  limit?: string
  allowNegative?: boolean
}

export default function createMoneyFormatter(options?: TFormatterOptions) {
  const limit = options?.limit
  const reNumsSign = !options?.allowNegative ? '' : '[-]?'

  const formatterOptions = { reNumsSign, limit }

  const limitValue = limit ? parseInt(limit.replace(/,/g, '')) : null

  return function formatMoney(value: string) {
    let inputValue = value.replace(/,/g, '')
    let intValue: number

    for (const formatterName in formatters) {
      const formatter = formatters[formatterName]

      inputValue = inputValue.trim()

      intValue = formatter(inputValue, formatterOptions)

      if (intValue || intValue == 0) {
        intValue = parseInt(intValue.toString().replace(/,/g, ''))

        if ((limitValue || limitValue === 0) && intValue >= limitValue) {
          intValue = limitValue
        }

        inputValue = intValue.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,')

        break
      }
    }

    return [intValue, inputValue] as [number, string]
  }
}
