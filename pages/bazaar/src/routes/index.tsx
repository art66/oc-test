import React, { PureComponent } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Bazaar from './Bazaar'
import Add from './Add'
import Manage from './Manage'
import Personalize from './Personalize'
import { getURLParameterByName } from '@torn/shared/utils'
import { connect } from 'react-redux'
import { IProps } from './types'

export class BazaarRoutesContainer extends PureComponent<IProps> {
  _renderMinorRoutes = () => {
    const { isBazaarExists } = this.props

    if (isBazaarExists === false) {
      return <Redirect to={{ pathname: '/' }} />
    }

    return (
      <>
        <Route path="/add" component={Add} />
        <Route path="/manage" component={Manage} />
        <Route path="/personalize" component={Personalize} />
      </>
    )
  }

  render() {
    return (
      <Switch>
        <Route exact={true} path="/" component={Bazaar} />
        {this._renderMinorRoutes()}
        <Redirect from="/p=bazaar" to={{ pathname: '/' }} />
        <Redirect from="/p=manage" to={{ pathname: '/manage' }} />
        <Redirect from="/p=add" to={{ pathname: '/add' }} />
        <Redirect from="/p=personalize" to={{ pathname: '/personalize' }} />
        <Redirect
          from="/p=bazaar&userID=:id"
          to={{ pathname: `/`, search: `?userId=${getURLParameterByName('userID')}` }}
        />
      </Switch>
    )
  }
}

const mapStateToProps = state => {
  return {
    // isLoading: state.common.metadata.isLoading,
    isBazaarExists: state.common.metadata.data?.isBazaarExists
  }
}

export default connect(mapStateToProps)(BazaarRoutesContainer)
