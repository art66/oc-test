import {
  GRADIENT,
  BAZAAR_CROSS_ICON_PRESET,
  BAZAAR_BUY_ICON_PRESET,
  BAZAAR_VIEW_ICON_PRESET
} from '@torn/shared/SVG/presets/icons/bazaar'

export const SORT_BY_DEFAULT_VALUE = 'default'
export const SORT_BY_NAME_VALUE = 'name'
export const SORT_BY_CATEGORY_VALUE = 'category'
export const SORT_BY_COST_VALUE = 'cost'
export const SORT_BY_VALUE_VALUE = 'value'
export const SORT_BY_RARITY_VALUE = 'rarity'

export const ASC_ORDER = 'asc'
export const DESC_ORDER = 'desc'

export const ORDER_BY_OPTIONS = [ASC_ORDER, DESC_ORDER]

export const SORT_BY_OPTIONS = [
  { name: 'Default', id: SORT_BY_DEFAULT_VALUE },
  { name: 'Name', id: SORT_BY_NAME_VALUE },
  { name: 'Category', id: SORT_BY_CATEGORY_VALUE },
  { name: 'Cost', id: SORT_BY_COST_VALUE },
  { name: 'Value', id: SORT_BY_VALUE_VALUE },
  { name: 'Rarity', id: SORT_BY_RARITY_VALUE }
]

export const CROSS_ICON_CONFIG = {
  ...BAZAAR_CROSS_ICON_PRESET,
  gradient: {
    ...BAZAAR_CROSS_ICON_PRESET.gradient,
    ID: 'bazaar_cross_gradient'
  },
  fill: {
    ...BAZAAR_CROSS_ICON_PRESET.fill,
    color: `url(#bazaar_cross_gradient)`
  },
  onHover: {
    ...BAZAAR_CROSS_ICON_PRESET.onHover,
    gradient: {
      ...BAZAAR_CROSS_ICON_PRESET.onHover.gradient,
      ID: 'bazaar_cross_hover_gradient'
    },
    fill: {
      ...BAZAAR_CROSS_ICON_PRESET.onHover.fill,
      color: `url(#bazaar_cross_hover_gradient)`
    }
  }
}

export const BUY_ICON_CONFIG = {
  ...BAZAAR_BUY_ICON_PRESET,
  gradient: {
    ...BAZAAR_BUY_ICON_PRESET.gradient,
    scheme: GRADIENT,
    ID: 'bazaar_buy_gradient'
  },
  fill: {
    ...BAZAAR_BUY_ICON_PRESET.fill,
    color: 'url(#bazaar_buy_gradient)'
  },
  onHover: {
    ...BAZAAR_BUY_ICON_PRESET.onHover,
    gradient: {
      ...BAZAAR_BUY_ICON_PRESET.onHover.gradient,
      ID: 'bazaar_buy_hover_gradient'
    },
    fill: {
      ...BAZAAR_BUY_ICON_PRESET.onHover.fill,
      color: 'url(#bazaar_buy_hover_gradient)'
    }
  }
}

export const VIEW_ICON_CONFIG = {
  ...BAZAAR_VIEW_ICON_PRESET,
  gradient: {
    ...BAZAAR_VIEW_ICON_PRESET.gradient,
    scheme: GRADIENT,
    ID: 'bazaar_view_gradient'
  },
  fill: {
    ...BAZAAR_VIEW_ICON_PRESET.fill,
    color: 'url(#bazaar_view_gradient)'
  },
  onHover: {
    ...BAZAAR_VIEW_ICON_PRESET.onHover,
    gradient: {
      ...BAZAAR_VIEW_ICON_PRESET.onHover.gradient,
      ID: 'bazaar_view_hover_gradient'
    },
    fill: {
      ...BAZAAR_VIEW_ICON_PRESET.onHover.fill,
      color: 'url(#bazaar_view_hover_gradient)'
    }
  }
}
