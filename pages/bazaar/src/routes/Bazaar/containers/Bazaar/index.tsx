import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import SearchBar from '@torn/shared/components/SearchBar'
import { withTheme } from '@torn/shared/hoc/withTheme/withTheme'
import { Devider } from '../../../../components'
import { Description, Segment, Items } from '../../components'
import { SORT_BY_OPTIONS, ORDER_BY_OPTIONS } from '../../constants'
import { sortByChange, searchChange, orderByChange, initBazaar as initBazaarAction } from '../../modules/actions'
import styles from './index.cssmodule.scss'
import { IProps, IStateProps, IStateActions } from './types'
import { IState } from '../../../../store/types'
import { getMediaType } from '../../../../modules/selectors'

export class Bazaar extends PureComponent<IProps> {
  componentDidMount() {
    const { initBazaar } = this.props

    initBazaar()
  }

  _getSearchBarConfig() {
    const { mediaType, sortBy, orderBy, search } = this.props

    return {
      mediaType,
      sortBy: {
        value: sortBy,
        list: SORT_BY_OPTIONS,
        onChange: this._onSortByChange
      },
      orderBy: {
        value: orderBy,
        list: ORDER_BY_OPTIONS,
        onChange: this._onOrderByChange
      },
      search: {
        value: search,
        list: [],
        onChange: this._onSearchChange,
        hideDropdown: true
      }
    }
  }

  _onSortByChange = value => {
    const { sortByChangeAction } = this.props

    sortByChangeAction(value)
  }

  _onOrderByChange = value => {
    const { orderByChangeAction } = this.props

    orderByChangeAction(value)
  }

  _onSearchChange = value => {
    const { searchChangeAction } = this.props

    searchChangeAction(value)
  }

  _renderDescription() {
    const { description, isClosed, isYours, owner } = this.props

    if (owner.isLoading || !owner.data.userId || (isClosed && !isYours) || !description) {
      return null
    }

    return (
      <>
        <Segment padding='none'>
          <Description>{description}</Description>
        </Segment>
        <Devider />
      </>
    )
  }

  _renderItems() {
    const { isClosed, isYours, owner, isLoading, error, theme } = this.props

    if (!owner.data.userId || owner.isLoading || isLoading || error || (isClosed && !isYours)) {
      return null
    }

    const isDarkMode = theme === 'dark'

    return (
      <>
        <SearchBar {...this._getSearchBarConfig()} />
        <Segment className={styles.itemsContainner} padding='none'>
          <Items isDarkMode={isDarkMode} />
        </Segment>
      </>
    )
  }

  render() {
    const { isBazaarExists } = this.props

    if (!isBazaarExists) {
      return null
    }

    return (
      <>
        {this._renderDescription()}
        {this._renderItems()}
      </>
    )
  }
}

const mapStateToProps = (state: IState): IStateProps => {
  const nextProps: IStateProps = {
    owner: state.common.owner,
    mediaType: getMediaType(state),
    description: state.common.bazaarData.data.description,
    sortBy: state.bazaar.searchBar.sortBy,
    orderBy: state.bazaar.searchBar.orderBy,
    search: state.bazaar.searchBar.search,
    isClosed: state.common.metadata.data?.isClosed,
    isYours: state.common.metadata.data?.isYours,
    isLoading: state.bazaar.isLoading,
    error: state.bazaar.bazaarItems.error,
    isBazaarExists: state.common.metadata.data?.isBazaarExists
  }

  return nextProps
}

const mapDispatchToProps: IStateActions = {
  sortByChangeAction: sortByChange,
  searchChangeAction: searchChange,
  orderByChangeAction: orderByChange,
  initBazaar: initBazaarAction
}

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Bazaar))
