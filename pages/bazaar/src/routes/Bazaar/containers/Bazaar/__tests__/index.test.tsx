import React from 'react'
import { shallow } from 'enzyme'
import {
  initialState,
  mobileState,
  tabletState,
  isClosedState,
  isYoursState,
  isYoursAndClosedState,
  withoutDescriptionState,
  ownerIsLoadingState,
  isNoOwnerState,
  withErrorState,
  doesntExistsState
} from './mocks'
import { Bazaar } from '../'
import { Segment, Description, Items } from '../../../components'
import { Devider } from '../../../../../components'
import SearchBar from '@torn/shared/components/SearchBar'
import { SORT_BY_OPTIONS } from '../../../constants'

describe('<Bazaar /> container', () => {
  it('should execute handlers on componentDidMount', () => {
    const initBazaar = jest.fn()
    const state = { ...initialState, initBazaar }
    const Component = shallow<Bazaar>(<Bazaar {...state} />)

    Component.unmount()

    expect(initBazaar).toHaveBeenCalledTimes(1)

    expect(initBazaar).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should render', () => {
    const Component = shallow<Bazaar>(<Bazaar {...initialState} />)
    const component = Component.instance()

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(4)
    const SegmentComponent = ComponentChildren.at(0)
    expect(SegmentComponent.is(Segment)).toBeTruthy()
    const SegmentChildren = SegmentComponent.children()
    expect(SegmentChildren.length).toBe(1)
    const DescriptionComponent = SegmentChildren.at(0)
    expect(DescriptionComponent.is(Description)).toBeTruthy()
    expect(DescriptionComponent.prop('children')).toBe('Best items for the beast')
    const DeviderComponent = ComponentChildren.at(1)
    expect(DeviderComponent.is(Devider)).toBeTruthy()
    const SearchBarComponent = ComponentChildren.at(2)
    expect(SearchBarComponent.is(SearchBar)).toBeTruthy()
    expect(SearchBarComponent.prop('attached')).toBeNull()
    expect(SearchBarComponent.prop('className')).toBe('')
    expect(SearchBarComponent.prop('mediaType')).toBe('desktop')
    expect(SearchBarComponent.prop('sortBy').value).toBe('default')
    expect(SearchBarComponent.prop('sortBy').list).toBe(SORT_BY_OPTIONS)
    expect(SearchBarComponent.prop('sortBy').onChange).toBe(component._onSortByChange)
    expect(SearchBarComponent.prop('search').value).toBe('')
    expect(SearchBarComponent.prop('search').list).toEqual([])
    expect(SearchBarComponent.prop('search').hideDropdown).toBeTruthy()
    expect(SearchBarComponent.prop('search').onChange).toBe(component._onSearchChange)
    const ItemsContainerComponent = ComponentChildren.at(3)
    expect(ItemsContainerComponent.is(Segment)).toBeTruthy()
    expect(ItemsContainerComponent.prop('className')).toBe('itemsContainner')
    expect(ItemsContainerComponent.prop('padding')).toBe('none')
    const ItemsContainerChildren = ItemsContainerComponent.children()
    expect(ItemsContainerChildren.length).toBe(1)
    const ItemsComponent = ItemsContainerChildren.at(0)
    expect(ItemsComponent.is(Items)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render on owner loading', () => {
    const Component = shallow<Bazaar>(<Bazaar {...ownerIsLoadingState} />)

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(0)

    expect(Component).toMatchSnapshot()
  })

  it('should render on no owner', () => {
    const Component = shallow<Bazaar>(<Bazaar {...isNoOwnerState} />)

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(0)

    expect(Component).toMatchSnapshot()
  })

  it('should render on tablet state', () => {
    const Component = shallow<Bazaar>(<Bazaar {...tabletState} />)
    const component = Component.instance()

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(4)
    const SegmentComponent = ComponentChildren.at(0)
    expect(SegmentComponent.is(Segment)).toBeTruthy()
    const SegmentChildren = SegmentComponent.children()
    expect(SegmentChildren.length).toBe(1)
    const DescriptionComponent = SegmentChildren.at(0)
    expect(DescriptionComponent.is(Description)).toBeTruthy()
    expect(DescriptionComponent.prop('children')).toBe('Best items for the beast')
    const DeviderComponent = ComponentChildren.at(1)
    expect(DeviderComponent.is(Devider)).toBeTruthy()
    const SearchBarComponent = ComponentChildren.at(2)
    expect(SearchBarComponent.is(SearchBar)).toBeTruthy()
    expect(SearchBarComponent.prop('attached')).toBeNull()
    expect(SearchBarComponent.prop('className')).toBe('')
    expect(SearchBarComponent.prop('mediaType')).toBe('tablet')
    expect(SearchBarComponent.prop('sortBy').value).toBe('default')
    expect(SearchBarComponent.prop('sortBy').list).toBe(SORT_BY_OPTIONS)
    expect(SearchBarComponent.prop('sortBy').onChange).toBe(component._onSortByChange)
    expect(SearchBarComponent.prop('search').value).toBe('')
    expect(SearchBarComponent.prop('search').list).toEqual([])
    expect(SearchBarComponent.prop('search').hideDropdown).toBeTruthy()
    expect(SearchBarComponent.prop('search').onChange).toBe(component._onSearchChange)
    const ItemsContainerComponent = ComponentChildren.at(3)
    expect(ItemsContainerComponent.is(Segment)).toBeTruthy()
    expect(ItemsContainerComponent.prop('className')).toBe('itemsContainner')
    expect(ItemsContainerComponent.prop('padding')).toBe('none')
    const ItemsContainerChildren = ItemsContainerComponent.children()
    expect(ItemsContainerChildren.length).toBe(1)
    const ItemsComponent = ItemsContainerChildren.at(0)
    expect(ItemsComponent.is(Items)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render on mobile state', () => {
    const Component = shallow<Bazaar>(<Bazaar {...mobileState} />)
    const component = Component.instance()

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(4)
    const SegmentComponent = ComponentChildren.at(0)
    expect(SegmentComponent.is(Segment)).toBeTruthy()
    const SegmentChildren = SegmentComponent.children()
    expect(SegmentChildren.length).toBe(1)
    const DescriptionComponent = SegmentChildren.at(0)
    expect(DescriptionComponent.is(Description)).toBeTruthy()
    expect(DescriptionComponent.prop('children')).toBe('Best items for the beast')
    const DeviderComponent = ComponentChildren.at(1)
    expect(DeviderComponent.is(Devider)).toBeTruthy()
    const SearchBarComponent = ComponentChildren.at(2)
    expect(SearchBarComponent.is(SearchBar)).toBeTruthy()
    expect(SearchBarComponent.prop('attached')).toBeNull()
    expect(SearchBarComponent.prop('className')).toBe('')
    expect(SearchBarComponent.prop('mediaType')).toBe('mobile')
    expect(SearchBarComponent.prop('sortBy').value).toBe('default')
    expect(SearchBarComponent.prop('sortBy').list).toBe(SORT_BY_OPTIONS)
    expect(SearchBarComponent.prop('sortBy').onChange).toBe(component._onSortByChange)
    expect(SearchBarComponent.prop('search').value).toBe('')
    expect(SearchBarComponent.prop('search').list).toEqual([])
    expect(SearchBarComponent.prop('search').hideDropdown).toBeTruthy()
    expect(SearchBarComponent.prop('search').onChange).toBe(component._onSearchChange)
    const ItemsContainerComponent = ComponentChildren.at(3)
    expect(ItemsContainerComponent.is(Segment)).toBeTruthy()
    expect(ItemsContainerComponent.prop('className')).toBe('itemsContainner')
    expect(ItemsContainerComponent.prop('padding')).toBe('none')
    const ItemsContainerChildren = ItemsContainerComponent.children()
    expect(ItemsContainerChildren.length).toBe(1)
    const ItemsComponent = ItemsContainerChildren.at(0)
    expect(ItemsComponent.is(Items)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render when is closed', () => {
    const Component = shallow<Bazaar>(<Bazaar {...isClosedState} />)

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(0)

    expect(Component).toMatchSnapshot()
  })

  it("should render when doesn't exists", () => {
    const Component = shallow<Bazaar>(<Bazaar {...doesntExistsState} />)

    expect(Component.html()).toBe(null)

    expect(Component).toMatchSnapshot()
  })

  it('should render when is yours', () => {
    const Component = shallow<Bazaar>(<Bazaar {...isYoursState} />)
    const component = Component.instance()

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(4)
    const SegmentComponent = ComponentChildren.at(0)
    expect(SegmentComponent.is(Segment)).toBeTruthy()
    const SegmentChildren = SegmentComponent.children()
    expect(SegmentChildren.length).toBe(1)
    const DescriptionComponent = SegmentChildren.at(0)
    expect(DescriptionComponent.is(Description)).toBeTruthy()
    expect(DescriptionComponent.prop('children')).toBe('Best items for the beast')
    const DeviderComponent = ComponentChildren.at(1)
    expect(DeviderComponent.is(Devider)).toBeTruthy()
    const SearchBarComponent = ComponentChildren.at(2)
    expect(SearchBarComponent.is(SearchBar)).toBeTruthy()
    expect(SearchBarComponent.prop('attached')).toBeNull()
    expect(SearchBarComponent.prop('className')).toBe('')
    expect(SearchBarComponent.prop('mediaType')).toBe('desktop')
    expect(SearchBarComponent.prop('sortBy').value).toBe('default')
    expect(SearchBarComponent.prop('sortBy').list).toBe(SORT_BY_OPTIONS)
    expect(SearchBarComponent.prop('sortBy').onChange).toBe(component._onSortByChange)
    expect(SearchBarComponent.prop('search').value).toBe('')
    expect(SearchBarComponent.prop('search').list).toEqual([])
    expect(SearchBarComponent.prop('search').hideDropdown).toBeTruthy()
    expect(SearchBarComponent.prop('search').onChange).toBe(component._onSearchChange)
    const ItemsContainerComponent = ComponentChildren.at(3)
    expect(ItemsContainerComponent.is(Segment)).toBeTruthy()
    expect(ItemsContainerComponent.prop('className')).toBe('itemsContainner')
    expect(ItemsContainerComponent.prop('padding')).toBe('none')
    const ItemsContainerChildren = ItemsContainerComponent.children()
    expect(ItemsContainerChildren.length).toBe(1)
    const ItemsComponent = ItemsContainerChildren.at(0)
    expect(ItemsComponent.is(Items)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render when is yours and closed', () => {
    const Component = shallow<Bazaar>(<Bazaar {...isYoursAndClosedState} />)
    const component = Component.instance()

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(4)
    const SegmentComponent = ComponentChildren.at(0)
    expect(SegmentComponent.is(Segment)).toBeTruthy()
    const SegmentChildren = SegmentComponent.children()
    expect(SegmentChildren.length).toBe(1)
    const DescriptionComponent = SegmentChildren.at(0)
    expect(DescriptionComponent.is(Description)).toBeTruthy()
    expect(DescriptionComponent.prop('children')).toBe('Best items for the beast')
    const DeviderComponent = ComponentChildren.at(1)
    expect(DeviderComponent.is(Devider)).toBeTruthy()
    const SearchBarComponent = ComponentChildren.at(2)
    expect(SearchBarComponent.is(SearchBar)).toBeTruthy()
    expect(SearchBarComponent.prop('attached')).toBeNull()
    expect(SearchBarComponent.prop('className')).toBe('')
    expect(SearchBarComponent.prop('mediaType')).toBe('desktop')
    expect(SearchBarComponent.prop('sortBy').value).toBe('default')
    expect(SearchBarComponent.prop('sortBy').list).toBe(SORT_BY_OPTIONS)
    expect(SearchBarComponent.prop('sortBy').onChange).toBe(component._onSortByChange)
    expect(SearchBarComponent.prop('search').value).toBe('')
    expect(SearchBarComponent.prop('search').list).toEqual([])
    expect(SearchBarComponent.prop('search').hideDropdown).toBeTruthy()
    expect(SearchBarComponent.prop('search').onChange).toBe(component._onSearchChange)
    const ItemsContainerComponent = ComponentChildren.at(3)
    expect(ItemsContainerComponent.is(Segment)).toBeTruthy()
    expect(ItemsContainerComponent.prop('className')).toBe('itemsContainner')
    expect(ItemsContainerComponent.prop('padding')).toBe('none')
    const ItemsContainerChildren = ItemsContainerComponent.children()
    expect(ItemsContainerChildren.length).toBe(1)
    const ItemsComponent = ItemsContainerChildren.at(0)
    expect(ItemsComponent.is(Items)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render without description', () => {
    const Component = shallow<Bazaar>(<Bazaar {...withoutDescriptionState} />)
    const component = Component.instance()

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const SearchBarComponent = ComponentChildren.at(0)
    expect(SearchBarComponent.is(SearchBar)).toBeTruthy()
    expect(SearchBarComponent.prop('attached')).toBeNull()
    expect(SearchBarComponent.prop('className')).toBe('')
    expect(SearchBarComponent.prop('mediaType')).toBe('desktop')
    expect(SearchBarComponent.prop('sortBy').value).toBe('default')
    expect(SearchBarComponent.prop('sortBy').list).toBe(SORT_BY_OPTIONS)
    expect(SearchBarComponent.prop('sortBy').onChange).toBe(component._onSortByChange)
    expect(SearchBarComponent.prop('search').value).toBe('')
    expect(SearchBarComponent.prop('search').list).toEqual([])
    expect(SearchBarComponent.prop('search').hideDropdown).toBeTruthy()
    expect(SearchBarComponent.prop('search').onChange).toBe(component._onSearchChange)
    const ItemsContainerComponent = ComponentChildren.at(1)
    expect(ItemsContainerComponent.is(Segment)).toBeTruthy()
    expect(ItemsContainerComponent.prop('className')).toBe('itemsContainner')
    expect(ItemsContainerComponent.prop('padding')).toBe('none')
    const ItemsContainerChildren = ItemsContainerComponent.children()
    expect(ItemsContainerChildren.length).toBe(1)
    const ItemsComponent = ItemsContainerChildren.at(0)
    expect(ItemsComponent.is(Items)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render with error', () => {
    const Component = shallow<Bazaar>(<Bazaar {...withErrorState} />)

    expect(Component.is('Fragment')).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const SegmentComponent = ComponentChildren.at(0)
    expect(SegmentComponent.is(Segment)).toBeTruthy()
    const SegmentChildren = SegmentComponent.children()
    expect(SegmentChildren.length).toBe(1)
    const DescriptionComponent = SegmentChildren.at(0)
    expect(DescriptionComponent.is(Description)).toBeTruthy()
    expect(DescriptionComponent.prop('children')).toBe('Best items for the beast')
    const DeviderComponent = ComponentChildren.at(1)
    expect(DeviderComponent.is(Devider)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should _onSearchChange method works correctly', () => {
    const searchChangeAction = jest.fn()
    const Component = shallow<Bazaar>(<Bazaar {...{ ...initialState, searchChangeAction }} />)
    const component = Component.instance()

    component._onSearchChange('test value')

    expect(searchChangeAction).toHaveBeenCalledTimes(1)
    expect(searchChangeAction.mock.calls[0]).toEqual(['test value'])

    expect(searchChangeAction).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  // it('should _onSortByChange method works correctly', () => {
  // FIXME: Should write test
  // })
})
