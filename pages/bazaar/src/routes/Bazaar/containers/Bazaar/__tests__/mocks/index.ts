import { IProps } from '../../types'
import { SORT_BY_DEFAULT_VALUE, ASC_ORDER } from '../../../../constants'

export const initialState: IProps = {
  owner: {
    isLoading: false,
    data: {
      userId: '785457',
      playername: 'love__intent'
    }
  },
  mediaType: 'desktop',
  isClosed: false,
  isYours: false,
  description: 'Best items for the beast',
  sortBy: SORT_BY_DEFAULT_VALUE,
  search: '',
  orderBy: ASC_ORDER,
  isLoading: false,
  error: false,
  isBazaarExists: true,
  theme: 'light',
  sortByChangeAction: () => {},
  searchChangeAction: () => {},
  orderByChangeAction: () => {},
  initBazaar: () => {}
}

export const tabletState: IProps = {
  ...initialState,
  mediaType: 'tablet'
}

export const mobileState: IProps = {
  ...initialState,
  mediaType: 'mobile'
}

export const ownerIsLoadingState: IProps = {
  ...initialState,
  owner: {
    ...initialState.owner,
    isLoading: true
  }
}

export const isNoOwnerState: IProps = {
  ...initialState,
  owner: {
    ...initialState.owner,
    data: {
      userId: '',
      playername: ''
    }
  }
}

export const isClosedState: IProps = {
  ...initialState,
  isClosed: true
}

export const isYoursState: IProps = {
  ...initialState,
  isYours: true
}

export const isYoursAndClosedState: IProps = {
  ...isYoursState,
  isClosed: true
}

export const withoutDescriptionState: IProps = {
  ...initialState,
  description: null
}

export const withErrorState: IProps = {
  ...initialState,
  error: true
}

export const doesntExistsState: IProps = {
  ...initialState,
  isBazaarExists: false
}
