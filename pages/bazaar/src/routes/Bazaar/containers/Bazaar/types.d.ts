import { ISearchBar } from '../../modules/types/state'
import { TOwner } from '../../../../modules/types/state'

export interface IProps extends IStateProps, IStateActions {
  theme: 'light' | 'dark'
}

export interface IStateProps {
  owner: TOwner
  userId?: string
  mediaType: 'desktop' | 'tablet' | 'mobile'
  isClosed: boolean
  isYours: boolean
  description?: string
  sortBy: ISearchBar['sortBy']
  search?: string
  orderBy: ISearchBar['orderBy']
  isLoading: boolean
  error: boolean
  isBazaarExists: boolean
}

export interface IStateActions {
  sortByChangeAction: (sortBy: ISearchBar['sortBy']) => void
  searchChangeAction: (search: string) => void
  orderByChangeAction: (orderBy: ISearchBar['orderBy']) => void
  initBazaar: () => any
}
