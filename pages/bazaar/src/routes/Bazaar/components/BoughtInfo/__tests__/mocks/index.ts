import { IProps } from '../../types'

export const initialState: IProps = {
  uniqueId: '7978-798',
  ownerName: 'love_intent',
  name: 'Test name',
  boughtAmount: 1324,
  price: 1573460,
  onClose: () => {}
}

export const classNameState: IProps = {
  ...initialState,
  className: 'testClassName'
}
