import React from 'react'
import { shallow } from 'enzyme'

import BoughtInfo from '../'
import { initialState, classNameState } from './mocks'

describe('<BoughtInfo />', () => {
  it('should render', () => {
    const Component = shallow<BoughtInfo>(<BoughtInfo {...initialState} />, { disableLifecycleMethods: true })

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe(/* 'boughtInfo' */ '')
    const BoughtInfoChildren = Component.children()
    expect(BoughtInfoChildren.at(0).prop('className')).toBe('description')
    expect(BoughtInfoChildren.at(0).text()).toBe(
      "You bought 1324 x\u00a0Test name\u00a0from\u00a0love_intent's\u00a0bazaar\u00a0for a total of\u00a0$2,083,261,040"
    )
    expect(BoughtInfoChildren.at(1).prop('className')).toBe('close')

    expect(Component).toMatchSnapshot()
  })

  it('should render with extra class name', () => {
    const Component = shallow<BoughtInfo>(<BoughtInfo {...classNameState} />, { disableLifecycleMethods: true })

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe(/* 'boughtInfo  */ 'testClassName')
    const BoughtInfoChildren = Component.children()
    expect(BoughtInfoChildren.at(0).prop('className')).toBe('description')
    expect(BoughtInfoChildren.at(0).text()).toBe(
      "You bought 1324 x\u00a0Test name\u00a0from\u00a0love_intent's\u00a0bazaar\u00a0for a total of\u00a0$2,083,261,040"
    )
    expect(BoughtInfoChildren.at(1).prop('className')).toBe('close')

    expect(Component).toMatchSnapshot()
  })

  it('should focus on render', () => {
    const focus = jest.fn()
    const Component = shallow<BoughtInfo>(<BoughtInfo {...initialState} />, { disableLifecycleMethods: true })
    const instance = Component.instance()

    // @ts-ignore
    instance._messageRef = { current: { focus } }

    instance.componentDidMount()

    expect(focus).toHaveBeenCalledTimes(1)

    expect(focus).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle click on close button', () => {
    const onClose = jest.fn()
    const event = {}
    const state = { ...initialState, onClose }
    const Component = shallow<BoughtInfo>(<BoughtInfo {...state} />, { disableLifecycleMethods: true })

    Component.find('.close').simulate('click', event)

    expect(onClose).toHaveBeenCalledTimes(1)

    expect(onClose.mock.calls[0][0]).toBe(event)

    expect(onClose).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
