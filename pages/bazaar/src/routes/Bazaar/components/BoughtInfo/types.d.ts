export interface IProps {
  className?: string
  uniqueId: string
  ownerName: string
  name: string
  boughtAmount: number
  price: number
  onClose: () => void
}
