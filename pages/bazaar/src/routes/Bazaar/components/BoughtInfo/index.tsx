import React, { PureComponent } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { CROSS_ICON_CONFIG } from '../../constants'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import { numberToCurrency } from '../../../../utils'

export class BoughtInfo extends PureComponent<IProps> {
  _messageRef: React.RefObject<HTMLParagraphElement>

  constructor(props) {
    super(props)

    this._messageRef = React.createRef()
  }

  componentDidMount() {
    this._messageRef.current.focus()
  }

  _getTotal = () => {
    const { boughtAmount, price } = this.props

    return numberToCurrency(boughtAmount * price)
  }

  render() {
    const { className, uniqueId, ownerName, name, boughtAmount, onClose } = this.props

    const buyConfirmationMsgId = `bought-msg-${uniqueId}`

    return (
      <div
        className={cn(/* styles.boughtInfo, */ className)}
        ref={this._messageRef}
        tabIndex={0}
        aria-label='Success'
        aria-labelledby={buyConfirmationMsgId}
        onBlur={onClose}
      >
        <p id={buyConfirmationMsgId} className={styles.description} tabIndex={0}>
          <span className={styles.line}>
            You bought {boughtAmount} x&nbsp;
            <span className={styles.nowrap}>{name}</span>
          </span>
          <span className={styles.line}>
            &nbsp;from&nbsp;<span className={styles.nowrap}>{ownerName}&apos;s</span>&nbsp;bazaar
          </span>
          <span className={styles.line}>
            &nbsp;for a total of&nbsp;
            <span className={styles.nowrap}>${this._getTotal()}</span>
          </span>
        </p>
        <button className={styles.close} onClick={onClose} type='button' aria-label='Close'>
          <SVGIconGenerator {...CROSS_ICON_CONFIG} />
        </button>
      </div>
    )
  }
}

export default BoughtInfo
