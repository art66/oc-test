import React, { PureComponent } from 'react'
import cn from 'classnames'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import ItemDescription from '../ItemDescription'
import BuyMenu from '../BuyMenu'
import BuyConfirmation from '../BuyConfirmation'
import BoughtInfo from '../BoughtInfo'
import BuyError from '../BuyError'
import { DESKTOP_TYPE } from '../../../../constants'
import { buyItemRequest } from '../../modules/api'
import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'
import {
  BUY_AMOUNT_DEFAULT,
  BUY_MENU_VIEW_TYPE,
  BUY_CONFIRMATION_VIEW_TYPE,
  BUY_BOUGHT_INFO_VIEW_TYPE,
  BUY_DENIED_VIEW_TYPE
} from '../../modules/constants'

export class Item extends PureComponent<IProps> {
  static defaultProps = {
    buyAmount: BUY_AMOUNT_DEFAULT,
    boughtAmount: 0,
    activeBuyType: null,
    isBuying: false,
    buyErrorMessage: null
  }

  _onViewActivate = () => {
    const { onViewActivate, uniqueId } = this.props

    onViewActivate(uniqueId)
  }

  _isMobile = () => {
    const { mediaType } = this.props

    return mediaType !== DESKTOP_TYPE
  }

  _onBuyActivate = () => {
    const { uniqueId, changeActiveBuyType } = this.props

    changeActiveBuyType(uniqueId, BUY_MENU_VIEW_TYPE)
  }

  _onBuyAmountChange = buyAmount => {
    const { uniqueId, updateFormData } = this.props

    updateFormData(uniqueId, { buyAmount })
  }

  _onBuy = () => {
    const { uniqueId, changeActiveBuyType } = this.props

    changeActiveBuyType(uniqueId, BUY_CONFIRMATION_VIEW_TYPE)
  }

  _onCloseBuy = () => {
    const { uniqueId, changeActiveBuyType } = this.props

    changeActiveBuyType(uniqueId, null)
  }

  _onBuyConfirmationYes = async () => {
    const { uniqueId, updateFormData, changeActiveBuyType } = this.props

    try {
      const { userId, bazaarId, itemId, price, buyAmount } = this.props

      updateFormData(uniqueId, { isBuying: true })

      const responseData = await buyItemRequest({
        userId,
        bazaarId,
        itemId,
        buyAmount,
        price,
        beforeval: price * buyAmount
      })

      if (!responseData.success) {
        throw responseData.text
      }

      updateFormData(uniqueId, {
        isBuying: false,
        boughtAmount: buyAmount,
        buyAmount: BUY_AMOUNT_DEFAULT
      })
      changeActiveBuyType(uniqueId, BUY_BOUGHT_INFO_VIEW_TYPE)
    } catch (error) {
      const message = error.toString()

      updateFormData(uniqueId, { isBuying: false, buyErrorMessage: message })
      changeActiveBuyType(uniqueId, BUY_DENIED_VIEW_TYPE)
    }
  }

  _onBuyConfirationNo = () => {
    const { uniqueId, changeActiveBuyType } = this.props

    changeActiveBuyType(uniqueId, null)
  }

  _onBoughtInfoClose = () => {
    const {
      uniqueId,
      removeItem,
      amount,
      boughtAmount,
      updateItemAmount,
      updateFormData,
      changeActiveBuyType
    } = this.props

    if (boughtAmount >= amount) {
      removeItem(uniqueId)
    } else {
      updateItemAmount(uniqueId, boughtAmount)
    }

    updateFormData(uniqueId, { boughtAmount: 0 })
    changeActiveBuyType(uniqueId, null)
  }

  _onBuyErrorClose = () => {
    const { uniqueId, updateFormData, changeActiveBuyType } = this.props

    updateFormData(uniqueId, { buyErrorMessage: null })
    changeActiveBuyType(uniqueId, null)
  }

  _onImgBarFocus = () => {
    const { uniqueId, onImgBarFocus } = this.props

    onImgBarFocus(uniqueId)
  }

  _renderBuyMenu = () => {
    const { name, amount, buyAmount, price } = this.props

    return (
      <BuyMenu
        className={styles.overPanelContainer}
        name={name}
        amount={amount}
        price={price}
        buyAmount={buyAmount}
        onBuyAmountChange={this._onBuyAmountChange}
        onBuy={this._onBuy}
        onClose={this._onCloseBuy}
      />
    )
  }

  _renderBuyConfirmation = () => {
    const { uniqueId, name, price, averagePrice, buyAmount } = this.props

    return (
      <BuyConfirmation
        className={styles.overPanelContainer}
        uniqueId={uniqueId}
        name={name}
        buyAmount={buyAmount}
        price={price}
        averagePrice={averagePrice}
        onYes={this._onBuyConfirmationYes}
        onNo={this._onBuyConfirationNo}
      />
    )
  }

  _renderBoughtInfo = () => {
    const { uniqueId, name, ownerName, price, boughtAmount } = this.props

    return (
      <BoughtInfo
        className={styles.overPanelContainer}
        uniqueId={uniqueId}
        name={name}
        boughtAmount={boughtAmount}
        price={price}
        ownerName={ownerName}
        onClose={this._onBoughtInfoClose}
      />
    )
  }

  _renderBuyError = () => {
    const { buyErrorMessage, uniqueId } = this.props

    return <BuyError uniqueId={uniqueId} message={buyErrorMessage} onClose={this._onBuyErrorClose} />
  }

  _renderBuy = () => {
    const { activeBuyType, isBuying } = this.props

    if (isBuying) {
      return <LoadingIndicator />
      // TODO: Should use the correct loader indicator(with 10 dots)
    }

    switch (activeBuyType) {
      case BUY_CONFIRMATION_VIEW_TYPE: {
        return this._renderBuyConfirmation()
      }
      case BUY_BOUGHT_INFO_VIEW_TYPE: {
        return this._renderBoughtInfo()
      }
      case BUY_DENIED_VIEW_TYPE: {
        return this._renderBuyError()
      }
      default: {
        return this._renderBuyMenu()
      }
    }
  }

  _renderItemContent = () => {
    const {
      name,
      price,
      amount,
      imgLargeUrl,
      imgBlankUrl,
      rarity,
      buyAmount,
      activeBuyType,
      imgBarFocused,
      isBlockedForBuying,
      isDarkMode
    } = this.props

    if (activeBuyType) {
      return this._renderBuy()
    }

    return (
      <ItemDescription
        name={name}
        price={price}
        amount={amount}
        imgLargeUrl={imgLargeUrl}
        imgBlankUrl={imgBlankUrl}
        rarity={rarity}
        isMobile={this._isMobile()}
        buyAmount={buyAmount}
        imgBarFocused={imgBarFocused}
        isBlockedForBuying={isBlockedForBuying}
        isDarkMode={isDarkMode}
        onViewActivate={this._onViewActivate}
        onBuyActivate={this._onBuyActivate}
        onBuyAmountChange={this._onBuyAmountChange}
        onBuy={this._onBuy}
        onImgBarFocus={this._onImgBarFocus}
      />
    )
  }

  render() {
    const { className } = this.props

    return <div className={cn(styles.item, className)}>{this._renderItemContent()}</div>
  }
}

export default Item
