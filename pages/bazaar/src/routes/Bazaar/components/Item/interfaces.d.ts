import { TItem } from '../../../../modules/types/state'
import { TBuyMenuType, TFormDataItem, TActiveBuyType } from '../../modules/types/state'

export interface IBuyState {
  isBuying: boolean
}

export interface IProps extends TItem {
  className?: string
  uniqueId: string
  userId: string
  ownerName: string
  mediaType: 'desktop' | 'tablet' | 'mobile'
  buyAmount: number
  boughtAmount: number
  activeBuyType?: TBuyMenuType
  isBuying: boolean
  isDarkMode: boolean
  buyErrorMessage?: string
  imgBarFocused?: boolean
  onViewActivate: (id: string) => void
  onViewDeactivate: () => void
  removeItem: (uniqueId: string) => void
  updateItemAmount: (uniqueId: string, boughtAmount: number) => void
  updateFormData: (uniqueId: string, formData: TFormDataItem) => void
  onImgBarFocus: (uniqueId: string) => void
  changeActiveBuyType: (uniqueId: string, activeBuyType?: TActiveBuyType) => void
}
