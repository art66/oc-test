import { IProps } from '../../interfaces'
import {
  BUY_BOUGHT_INFO_VIEW_TYPE,
  BUY_DENIED_VIEW_TYPE,
  BUY_CONFIRMATION_VIEW_TYPE,
  BUY_MENU_VIEW_TYPE
} from '../../../../modules/constants'

export const initialState: IProps = {
  userId: '780986',
  ownerName: 'love__intent',
  uniqueId: '373-1',
  itemId: '1',
  bazaarId: '7853458',
  armoryId: '46785',
  name: 'Lead Pipe',
  price: 20000000000,
  amount: 8000,
  imgLargeUrl: '/images/items/68/large.png?v=1528808940574',
  imgMediumUrl: '/images/items/68/medium.png',
  imgBlankUrl: '/images/items/68/blank.png',
  rarity: 0,
  isDarkMode: false,
  isBlockedForBuying: false,
  glow: null,
  categoryName: 'weapons',
  mediaType: 'desktop',
  armorBonus: '0',
  bonuses: [
    { className: 'bonus-attachment-blank-bonus-25', title: null },
    { className: 'bonus-attachment-blank-bonus-25', title: null }
  ],
  averagePrice: 54567687,
  damageBonus: '65657',
  accuracyBonus: '654',
  type: 'Other',
  minPrice: 1,
  buyAmount: 1,
  boughtAmount: 0,
  activeBuyType: null,
  isBuying: false,
  buyErrorMessage: null,
  onViewActivate: () => {},
  onViewDeactivate: () => {},
  removeItem: () => {},
  updateItemAmount: () => {},
  updateFormData: () => {},
  onImgBarFocus: () => {},
  changeActiveBuyType: () => {}
}

export const classNameExtendedState: IProps = {
  ...initialState,
  className: 'extraClassName'
}

export const tabletState: IProps = {
  ...initialState,
  mediaType: 'tablet'
}

export const mobileState: IProps = {
  ...initialState,
  mediaType: 'mobile'
}

export const boughtSameThanAmountState: IProps = {
  ...initialState,
  activeBuyType: BUY_BOUGHT_INFO_VIEW_TYPE,
  boughtAmount: 8000
}

export const boughtLessThanAmountState: IProps = {
  ...initialState,
  activeBuyType: BUY_BOUGHT_INFO_VIEW_TYPE,
  boughtAmount: 5000
}

export const buyDeniedState: IProps = {
  ...initialState,
  activeBuyType: BUY_DENIED_VIEW_TYPE,
  buyErrorMessage: 'Error message content'
}

export const boughtInfoState: IProps = {
  ...initialState,
  activeBuyType: BUY_BOUGHT_INFO_VIEW_TYPE
}

export const buyConfirmationState: IProps = {
  ...initialState,
  activeBuyType: BUY_CONFIRMATION_VIEW_TYPE
}

export const buyMenuState: IProps = {
  ...initialState,
  activeBuyType: BUY_MENU_VIEW_TYPE
}
