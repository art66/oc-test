import React from 'react'
import { shallow } from 'enzyme'
import Item from '../'
import { ItemDescription, BuyMenu, BuyConfirmation, BoughtInfo } from '../../'
import {
  initialState,
  classNameExtendedState,
  tabletState,
  mobileState,
  boughtSameThanAmountState,
  boughtLessThanAmountState,
  buyDeniedState,
  boughtInfoState,
  buyConfirmationState,
  buyMenuState
} from './mocks'
import { IProps } from '../interfaces'
import BuyError from '../../BuyError'

describe('<Item />', () => {
  it('should render', () => {
    const Component = shallow<Item>(<Item {...initialState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const ItemDescriptionComponent = ChildrenComponent.at(0)
    expect(ItemDescriptionComponent.is(ItemDescription)).toBeTruthy()
    expect(ItemDescriptionComponent.prop('name')).toBe('Lead Pipe')
    expect(ItemDescriptionComponent.prop('price')).toBe(20000000000)
    expect(ItemDescriptionComponent.prop('amount')).toBe(8000)
    expect(ItemDescriptionComponent.prop('imgLargeUrl')).toBe('/images/items/68/large.png?v=1528808940574')
    expect(ItemDescriptionComponent.prop('buyAmount')).toBe(1)
    expect(ItemDescriptionComponent.prop('isMobile')).toBe(false)
    expect(typeof ItemDescriptionComponent.prop('onViewActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onViewActivate')).toBe(component._onViewActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyActivate')).toBe(component._onBuyActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyAmountChange')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyAmountChange')).toBe(component._onBuyAmountChange)
    expect(typeof ItemDescriptionComponent.prop('onBuy')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuy')).toBe(component._onBuy)

    expect(Component).toMatchSnapshot()
  })

  it('should render with class name extended', () => {
    const Component = shallow<Item>(<Item {...classNameExtendedState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item extraClassName')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const ItemDescriptionComponent = ChildrenComponent.at(0)
    expect(ItemDescriptionComponent.is(ItemDescription)).toBeTruthy()
    expect(ItemDescriptionComponent.prop('name')).toBe('Lead Pipe')
    expect(ItemDescriptionComponent.prop('price')).toBe(20000000000)
    expect(ItemDescriptionComponent.prop('amount')).toBe(8000)
    expect(ItemDescriptionComponent.prop('imgLargeUrl')).toBe('/images/items/68/large.png?v=1528808940574')
    expect(ItemDescriptionComponent.prop('buyAmount')).toBe(1)
    expect(ItemDescriptionComponent.prop('isMobile')).toBe(false)
    expect(typeof ItemDescriptionComponent.prop('onViewActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onViewActivate')).toBe(component._onViewActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyActivate')).toBe(component._onBuyActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyAmountChange')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyAmountChange')).toBe(component._onBuyAmountChange)
    expect(typeof ItemDescriptionComponent.prop('onBuy')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuy')).toBe(component._onBuy)

    expect(Component).toMatchSnapshot()
  })

  it('should render on tablet', () => {
    const Component = shallow<Item>(<Item {...tabletState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const ItemDescriptionComponent = ChildrenComponent.at(0)
    expect(ItemDescriptionComponent.is(ItemDescription)).toBeTruthy()
    expect(ItemDescriptionComponent.prop('name')).toBe('Lead Pipe')
    expect(ItemDescriptionComponent.prop('price')).toBe(20000000000)
    expect(ItemDescriptionComponent.prop('amount')).toBe(8000)
    expect(ItemDescriptionComponent.prop('imgLargeUrl')).toBe('/images/items/68/large.png?v=1528808940574')
    expect(ItemDescriptionComponent.prop('buyAmount')).toBe(1)
    expect(ItemDescriptionComponent.prop('isMobile')).toBe(true)
    expect(typeof ItemDescriptionComponent.prop('onViewActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onViewActivate')).toBe(component._onViewActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyActivate')).toBe(component._onBuyActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyAmountChange')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyAmountChange')).toBe(component._onBuyAmountChange)
    expect(typeof ItemDescriptionComponent.prop('onBuy')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuy')).toBe(component._onBuy)

    expect(Component).toMatchSnapshot()
  })

  it('should render on mobile', () => {
    const Component = shallow<Item>(<Item {...mobileState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const ItemDescriptionComponent = ChildrenComponent.at(0)
    expect(ItemDescriptionComponent.is(ItemDescription)).toBeTruthy()
    expect(ItemDescriptionComponent.prop('name')).toBe('Lead Pipe')
    expect(ItemDescriptionComponent.prop('price')).toBe(20000000000)
    expect(ItemDescriptionComponent.prop('amount')).toBe(8000)
    expect(ItemDescriptionComponent.prop('imgLargeUrl')).toBe('/images/items/68/large.png?v=1528808940574')
    expect(ItemDescriptionComponent.prop('buyAmount')).toBe(1)
    expect(ItemDescriptionComponent.prop('isMobile')).toBe(true)
    expect(typeof ItemDescriptionComponent.prop('onViewActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onViewActivate')).toBe(component._onViewActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyActivate')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyActivate')).toBe(component._onBuyActivate)
    expect(typeof ItemDescriptionComponent.prop('onBuyAmountChange')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuyAmountChange')).toBe(component._onBuyAmountChange)
    expect(typeof ItemDescriptionComponent.prop('onBuy')).toBe('function')
    expect(ItemDescriptionComponent.prop('onBuy')).toBe(component._onBuy)

    expect(Component).toMatchSnapshot()
  })

  it('should render on main buy menu', () => {
    const Component = shallow<Item>(<Item {...buyMenuState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const ItemBuyMenuComponent = ChildrenComponent.at(0)
    expect(ItemBuyMenuComponent.is(BuyMenu)).toBeTruthy()
    expect(ItemBuyMenuComponent.prop('className')).toBe('overPanelContainer')
    expect(ItemBuyMenuComponent.prop('name')).toBe('Lead Pipe')
    expect(ItemBuyMenuComponent.prop('amount')).toBe(8000)
    expect(ItemBuyMenuComponent.prop('price')).toBe(20000000000)
    expect(ItemBuyMenuComponent.prop('buyAmount')).toBe(1)
    expect(typeof ItemBuyMenuComponent.prop('onBuyAmountChange')).toBe('function')
    expect(ItemBuyMenuComponent.prop('onBuyAmountChange')).toBe(component._onBuyAmountChange)
    expect(typeof ItemBuyMenuComponent.prop('onBuy')).toBe('function')
    expect(ItemBuyMenuComponent.prop('onBuy')).toBe(component._onBuy)
    expect(typeof ItemBuyMenuComponent.prop('onClose')).toBe('function')
    expect(ItemBuyMenuComponent.prop('onClose')).toBe(component._onCloseBuy)

    expect(Component).toMatchSnapshot()
  })

  it('should render on buy confirmation', () => {
    const Component = shallow<Item>(<Item {...buyConfirmationState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const BuyConfirmationComponent = ChildrenComponent.at(0)
    expect(BuyConfirmationComponent.is(BuyConfirmation)).toBeTruthy()
    expect(BuyConfirmationComponent.prop('className')).toBe('overPanelContainer')
    expect(BuyConfirmationComponent.prop('name')).toBe('Lead Pipe')
    expect(BuyConfirmationComponent.prop('buyAmount')).toBe(1)
    expect(BuyConfirmationComponent.prop('price')).toBe(20000000000)
    expect(typeof BuyConfirmationComponent.prop('onYes')).toBe('function')
    expect(BuyConfirmationComponent.prop('onYes')).toBe(component._onBuyConfirmationYes)
    expect(typeof BuyConfirmationComponent.prop('onNo')).toBe('function')
    expect(BuyConfirmationComponent.prop('onNo')).toBe(component._onBuyConfirationNo)

    expect(Component).toMatchSnapshot()
  })

  it('should render bought info', () => {
    const Component = shallow<Item>(<Item {...boughtInfoState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const BoughtInfoComponent = ChildrenComponent.at(0)
    expect(BoughtInfoComponent.is(BoughtInfo)).toBeTruthy()
    expect(BoughtInfoComponent.prop('className')).toBe('overPanelContainer')
    expect(BoughtInfoComponent.prop('name')).toBe('Lead Pipe')
    expect(BoughtInfoComponent.prop('boughtAmount')).toBe(0)
    expect(BoughtInfoComponent.prop('price')).toBe(20000000000)
    expect(BoughtInfoComponent.prop('ownerName')).toBe('love__intent')
    expect(typeof BoughtInfoComponent.prop('onClose')).toBe('function')
    expect(BoughtInfoComponent.prop('onClose')).toBe(component._onBoughtInfoClose)

    expect(Component).toMatchSnapshot()
  })

  it('should render buy denied', () => {
    const Component = shallow<Item>(<Item {...buyDeniedState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('item')
    const ChildrenComponent = Component.children()
    expect(ChildrenComponent.length).toBe(1)
    const BuyDeniedComponent = ChildrenComponent.at(0)
    expect(BuyDeniedComponent.is(BuyError)).toBeTruthy()
    expect(BuyDeniedComponent.prop('uniqueId')).toBe('373-1')
    expect(BuyDeniedComponent.prop('message')).toBe('Error message content')
    expect(typeof BuyDeniedComponent.prop('onClose')).toBe('function')
    expect(BuyDeniedComponent.prop('onClose')).toBe(component._onBuyErrorClose)

    expect(Component).toMatchSnapshot()
  })

  it('should handle view activation', () => {
    const onViewActivate = jest.fn()
    const state: IProps = { ...initialState, onViewActivate }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onViewActivate()

    expect(onViewActivate).toHaveBeenCalledTimes(1)
    expect(onViewActivate.mock.calls[0][0]).toBe('373-1')

    expect(onViewActivate).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle buy menu activation', () => {
    const changeActiveBuyType = jest.fn()
    const state: IProps = { ...initialState, changeActiveBuyType }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onBuyActivate()

    expect(changeActiveBuyType).toHaveBeenCalledTimes(1)
    expect(changeActiveBuyType.mock.calls[0]).toEqual(['373-1', 'BUY_MENU_VIEW_TYPE'])

    expect(changeActiveBuyType).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should change buy amount', () => {
    const updateFormData = jest.fn()
    const state: IProps = { ...initialState, updateFormData }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onBuyAmountChange(5)

    expect(updateFormData).toHaveBeenCalledTimes(1)
    expect(updateFormData.mock.calls[0]).toEqual(['373-1', { buyAmount: 5 }])

    expect(updateFormData).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle on buy', () => {
    const changeActiveBuyType = jest.fn()
    const state: IProps = { ...initialState, changeActiveBuyType }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onBuy()

    expect(changeActiveBuyType).toHaveBeenCalledTimes(1)
    expect(changeActiveBuyType.mock.calls[0]).toEqual(['373-1', 'BUY_CONFIRMATION_VIEW_TYPE'])

    expect(changeActiveBuyType).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle on buy menu close', () => {
    const changeActiveBuyType = jest.fn()
    const state: IProps = { ...initialState, changeActiveBuyType }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onCloseBuy()

    expect(changeActiveBuyType).toHaveBeenCalledTimes(1)
    expect(changeActiveBuyType.mock.calls[0]).toEqual(['373-1', null])

    expect(changeActiveBuyType).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  // it('should handle on buy confirmation yes', async () => {
  //   const Component = shallow<Item>(<Item {...initialState} />)
  //   const component = Component.instance()

  //   Component.setState({
  //     buyAmount: 5,
  //     activeBuyType: BUY_CONFIRMATION_VIEW_TYPE
  //   })

  //   await component._onBuyConfirmationYes()

  //   expect(Component.state()).toEqual({
  //     buyAmount: 5,
  //     activeBuyType: BUY_BOUGHT_INFO_VIEW_TYPE,
  //     buyErrorMessage: null,
  //     isBuying: true
  //   })

  //   expect(Component).toMatchSnapshot()
  //
  // })
  // FIXME: Should handle request updates

  it('should handle on buy confirmation no', () => {
    const changeActiveBuyType = jest.fn()
    const state: IProps = { ...initialState, changeActiveBuyType }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onBuyConfirationNo()

    expect(changeActiveBuyType).toHaveBeenCalledTimes(1)
    expect(changeActiveBuyType.mock.calls[0]).toEqual(['373-1', null])

    expect(changeActiveBuyType).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle updateItemAmount on bought info close', () => {
    const updateItemAmount = jest.fn()
    const updateFormData = jest.fn()
    const changeActiveBuyType = jest.fn()
    const state: IProps = { ...boughtLessThanAmountState, updateFormData, updateItemAmount, changeActiveBuyType }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onBoughtInfoClose()

    expect(updateItemAmount).toHaveBeenCalledTimes(1)
    expect(updateItemAmount.mock.calls[0]).toEqual(['373-1', 5000])

    expect(updateFormData).toHaveBeenCalledTimes(1)
    expect(updateFormData.mock.calls[0]).toEqual(['373-1', { boughtAmount: 0 }])

    expect(changeActiveBuyType).toHaveBeenCalledTimes(1)
    expect(changeActiveBuyType.mock.calls[0]).toEqual(['373-1', null])

    expect(updateFormData).toMatchSnapshot()
    expect(updateItemAmount).toMatchSnapshot()
    expect(changeActiveBuyType).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle removeItem on bought info close', () => {
    const removeItem = jest.fn()
    const updateFormData = jest.fn()
    const changeActiveBuyType = jest.fn()
    const state: IProps = { ...boughtSameThanAmountState, removeItem, updateFormData, changeActiveBuyType }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onBoughtInfoClose()

    expect(removeItem).toHaveBeenCalledTimes(1)
    expect(removeItem.mock.calls[0]).toEqual(['373-1'])

    expect(updateFormData).toHaveBeenCalledTimes(1)
    expect(updateFormData.mock.calls[0]).toEqual(['373-1', { boughtAmount: 0 }])

    expect(changeActiveBuyType).toHaveBeenCalledTimes(1)
    expect(changeActiveBuyType.mock.calls[0]).toEqual(['373-1', null])

    expect(removeItem).toMatchSnapshot()
    expect(updateFormData).toMatchSnapshot()
    expect(changeActiveBuyType).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle on buy error close', () => {
    const updateFormData = jest.fn()
    const changeActiveBuyType = jest.fn()
    const state: IProps = { ...initialState, updateFormData, changeActiveBuyType }
    const Component = shallow<Item>(<Item {...state} />)
    const component = Component.instance()

    component._onBuyErrorClose()

    expect(updateFormData).toHaveBeenCalledTimes(1)
    expect(updateFormData.mock.calls[0]).toEqual(['373-1', { buyErrorMessage: null }])

    expect(changeActiveBuyType).toHaveBeenCalledTimes(1)
    expect(changeActiveBuyType.mock.calls[0]).toEqual(['373-1', null])

    expect(updateFormData).toMatchSnapshot()
    expect(changeActiveBuyType).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
