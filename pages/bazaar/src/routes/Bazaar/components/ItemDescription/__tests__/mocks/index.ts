import { IProps } from '../../interfaces'

export const initialState: IProps = {
  name: 'Lead Pipe',
  price: 20000000000,
  amount: 8000,
  imgLargeUrl: '/images/items/68/large.png?v=1528808940574',
  imgBlankUrl: '/images/items/68/blank.png?v=1528808940574',
  rarity: 0,
  isBlockedForBuying: false,
  isMobile: false,
  buyAmount: 50,
  isDarkMode: false,
  onBuyActivate: () => {},
  onViewActivate: () => {},
  onBuyAmountChange: () => {},
  onBuy: () => {},
  onImgBarFocus: () => {}
}

export const glowState: IProps = {
  ...initialState
}

export const imgFocusedState: IProps = {
  ...initialState,
  imgBarFocused: true
}

export const tabletState: IProps = {
  ...initialState,
  isMobile: true
}

export const tabletImgFocusedState: IProps = {
  ...tabletState,
  imgBarFocused: true
}

export const mobileState: IProps = {
  ...initialState,
  isMobile: true
}

export const mobileImgFocuseState: IProps = {
  ...mobileState,
  imgBarFocused: true
}
