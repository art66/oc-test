import React, { PureComponent } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import getItemGlowClassName from '@torn/shared/utils/getItemGlowClassName'
import { VIEW_ICON_CONFIG, BUY_ICON_CONFIG } from '../../constants'
import BuyForm from '../BuyForm'
import { IProps } from './interfaces'
import styles from './index.cssmodule.scss'
import { numberToCurrency } from '../../../../utils'
import { CANT_BUY_TOOLTIP_TEXT } from './constants'
import tooltipStyles from './tooltip.cssmodule.scss'

export class ItemDescription extends PureComponent<IProps> {
  componentDidMount() {
    const { isBlockedForBuying } = this.props

    if (isBlockedForBuying) {
      this.addBlockedForBuyingTooltip()
    }
  }

  componentWillUnmount() {
    const { isBlockedForBuying } = this.props

    if (isBlockedForBuying) {
      this.removeBlockedForBuyingTooltip()
    }
  }

  addBlockedForBuyingTooltip = () => {
    tooltipsSubscriber.subscribe({
      ID: 'isBlockedForBuyingTooltip',
      customConfig: {
        position: { x: 'center', y: 'top' },
        manualCoodsFix: { fixX: -8, fixY: 0 },
        overrideStyles: { ...tooltipStyles }
      },
      child: CANT_BUY_TOOLTIP_TEXT
    })
  }

  removeBlockedForBuyingTooltip = () => {
    tooltipsSubscriber.unsubscribe({
      ID: 'isBlockedForBuyingTooltip',
      child: CANT_BUY_TOOLTIP_TEXT
    })
  }

  _getPrice = () => {
    const { price } = this.props

    return numberToCurrency(price)
  }

  _renderControlPanel = () => {
    const { isMobile, name, onViewActivate, onBuyActivate, imgBarFocused } = this.props

    if (isMobile) {
      if (imgBarFocused) {
        return (
          <button className={styles.viewIcon} aria-label={`Show info: ${name}`} onClick={onViewActivate} type='button'>
            <SVGIconGenerator {...VIEW_ICON_CONFIG} />
            <p className={styles.viewDetailsTitle}>
              View
              <br />
              Details
            </p>
          </button>
        )
      }

      return null
    }

    return (
      <div className={cn(styles.controlPanel, { [styles.focused]: imgBarFocused })}>
        <button
          className={styles.controlPanelButton}
          aria-label={`Show info: ${name}`}
          onClick={onViewActivate}
          type='button'
        >
          <SVGIconGenerator {...VIEW_ICON_CONFIG} />
        </button>
        <button className={styles.controlPanelButton} aria-label={`Buy: ${name}`} onClick={onBuyActivate} type='button'>
          <SVGIconGenerator {...BUY_ICON_CONFIG} />
        </button>
      </div>
    )
  }

  _renderBuyForm = () => {
    const { amount, buyAmount, name, isMobile, isBlockedForBuying, onBuyAmountChange, onBuy } = this.props

    if (!isMobile) {
      return null
    }

    return (
      <BuyForm
        className={styles.buyForm}
        buyAmount={buyAmount}
        amount={amount}
        name={name}
        disabled={isBlockedForBuying}
        onAmountChange={onBuyAmountChange}
        onBuy={onBuy}
      />
    )
  }

  _renderImg = () => {
    const { name, rarity, isBlockedForBuying, imgLargeUrl, imgBlankUrl, isDarkMode } = this.props

    const imageSrc = !isBlockedForBuying ? imgLargeUrl : imgBlankUrl

    const imgView = (
      <img
        className={cn(styles.img, getItemGlowClassName(rarity), isBlockedForBuying && styles.isBlockedForBuyingGlow)}
        src={imageSrc}
        alt={name}
      />
    )

    if (isBlockedForBuying && isDarkMode) {
      return (
        <div className={styles.dmContainer}>
          {imgView}
          <img className={styles.dmImg} src={imageSrc} />
        </div>
      )
    }

    return imgView
  }

  renderLock = () => {
    return (
      <div id='isBlockedForBuyingTooltip' className={styles.lockContainer}>
        <SVGIconGenerator
          iconName='Lock'
          preset={{
            type: 'bazaar',
            subtype: 'LOCK_ICON_PRESET'
          }}
        />
      </div>
    )
  }

  render() {
    const { amount, name, isMobile, isBlockedForBuying, onViewActivate, onImgBarFocus } = this.props

    const onImgBarClick = isMobile ? onViewActivate : null

    return (
      <div className={styles.itemDescription}>
        <div className={styles.imgBar} tabIndex={0} aria-label={name} onFocus={onImgBarFocus}>
          <canvas
            className={cn(styles.convertedImage, { [styles.isBlockedForBuying]: isBlockedForBuying })}
            width='100'
            height='50'
          ></canvas>
          <div className={cn(styles.imgContainer)} onClick={onImgBarClick}>
            {this._renderImg()}
          </div>
          {!isBlockedForBuying ? this._renderControlPanel() : this.renderLock()}
        </div>
        <div className={styles.description}>
          <p className={styles.name}>{name}</p>
          <p className={styles.price}>${this._getPrice()}</p>
          <p className={styles.amount}>
            (<span className={styles.amountValue}>{amount}</span>&nbsp;in stock)
          </p>
        </div>
        {this._renderBuyForm()}
      </div>
    )
  }
}

export default ItemDescription
