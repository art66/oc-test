import { TItem } from '../../../../modules/types/state'

export interface IProps
  extends Pick<TItem, 'name' | 'price' | 'amount' | 'isBlockedForBuying' | 'imgLargeUrl' | 'imgBlankUrl' | 'rarity'> {
  isMobile: boolean
  buyAmount: number
  isDarkMode: boolean
  imgBarFocused?: boolean
  onBuyActivate: () => void
  onViewActivate: () => void
  onBuyAmountChange: (buyAmount: number) => void
  onBuy: (event: React.MouseEvent<HTMLElement>) => void
  onImgBarFocus: () => void
}
