export const CANT_BUY_TOOLTIP_TEXT = `This item is only purchasable by
  a small random selection of people, and
  you're unfortunately not one of them.`

export const CANT_BUY_IMAGE_GLOW_SETTINGS = {
  color: '#FF3500',
  opacity: 1,
  shadowBlur: 2
}
