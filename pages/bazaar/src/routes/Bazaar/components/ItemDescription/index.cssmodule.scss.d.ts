// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'amount': string;
  'amountValue': string;
  'buyForm': string;
  'controlPanel': string;
  'controlPanelButton': string;
  'convertedImage': string;
  'description': string;
  'dmContainer': string;
  'dmImg': string;
  'focused': string;
  'globalSvgShadow': string;
  'img': string;
  'imgBar': string;
  'imgContainer': string;
  'isBlockedForBuying': string;
  'isBlockedForBuyingGlow': string;
  'itemDescription': string;
  'lockContainer': string;
  'name': string;
  'price': string;
  'viewDetailsTitle': string;
  'viewIcon': string;
}
export const cssExports: CssExports;
export default cssExports;
