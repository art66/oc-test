import { IProps } from '../../types'

export const initialState: IProps = {
  uniqueId: '79898-87',
  message: 'Test error message',
  onClose: () => {}
}

export const withLinkState: IProps = {
  uniqueId: '79898-87',
  message: 'Test <a src="/test">error message</a>',
  onClose: () => {}
}

export const classNameState: IProps = {
  ...initialState,
  className: 'testClassName'
}
