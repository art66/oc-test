import React from 'react'
import { shallow } from 'enzyme'

import BuyError from '..'
import { initialState, withLinkState, classNameState } from './mocks'

describe('<BuyError />', () => {
  it('should render', () => {
    const Component = shallow<BuyError>(<BuyError {...initialState} />, { disableLifecycleMethods: true })

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyError')
    const BuyErrorChildren = Component.children()

    expect(BuyErrorChildren.at(0).prop('className')).toBe('message')
    expect(BuyErrorChildren.at(0).prop('dangerouslySetInnerHTML')).toEqual({ __html: 'Test error message' })
    expect(BuyErrorChildren.at(1).prop('className')).toBe('close')

    expect(Component).toMatchSnapshot()
  })

  it('should render text with link', () => {
    const Component = shallow<BuyError>(<BuyError {...withLinkState} />, { disableLifecycleMethods: true })

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyError')
    const BuyErrorChildren = Component.children()

    expect(BuyErrorChildren.at(0).prop('className')).toBe('message')
    expect(BuyErrorChildren.at(0).prop('dangerouslySetInnerHTML')).toEqual({
      __html: 'Test <a src="/test">error message</a>'
    })
    expect(BuyErrorChildren.at(1).prop('className')).toBe('close')

    expect(Component).toMatchSnapshot()
  })

  it('should render with extra class name', () => {
    const Component = shallow<BuyError>(<BuyError {...classNameState} />, { disableLifecycleMethods: true })

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyError testClassName')
    const BuyErrorChildren = Component.children()

    expect(BuyErrorChildren.at(0).prop('className')).toBe('message')
    expect(BuyErrorChildren.at(0).prop('dangerouslySetInnerHTML')).toEqual({ __html: 'Test error message' })
    expect(BuyErrorChildren.at(1).prop('className')).toBe('close')

    expect(Component).toMatchSnapshot()
  })

  it('should focus on render', () => {
    const focus = jest.fn()
    const Component = shallow<BuyError>(<BuyError {...initialState} />, { disableLifecycleMethods: true })
    const instance = Component.instance()

    // @ts-ignore
    instance._messageRef = { current: { focus } }

    instance.componentDidMount()

    expect(focus).toHaveBeenCalledTimes(1)

    expect(focus).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle click on close button', () => {
    const onClose = jest.fn()
    const event = {}
    const state = { ...initialState, onClose }
    const Component = shallow<BuyError>(<BuyError {...state} />, { disableLifecycleMethods: true })

    Component.find('.close').simulate('click', event)

    expect(onClose).toHaveBeenCalledTimes(1)

    expect(onClose.mock.calls[0][0]).toBe(event)

    expect(onClose).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
