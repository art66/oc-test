import React, { PureComponent } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { CROSS_ICON_CONFIG } from '../../constants'
import styles from './index.cssmodule.scss'
import { IProps } from './types'

export class BuyError extends PureComponent<IProps> {
  _messageRef: React.RefObject<HTMLParagraphElement>

  constructor(props) {
    super(props)

    this._messageRef = React.createRef()
  }

  componentDidMount() {
    this._messageRef.current.focus()
  }

  render() {
    const { className, message, onClose } = this.props

    return (
      <div className={cn(styles.buyError, className)} ref={this._messageRef} tabIndex={0} aria-label='Error'>
        <p className={styles.message} tabIndex={0} aria-label={message} dangerouslySetInnerHTML={{ __html: message }} />
        <button type='button' className={styles.close} onClick={onClose} aria-label='Close'>
          <SVGIconGenerator {...CROSS_ICON_CONFIG} />
        </button>
      </div>
    )
  }
}

export default BuyError
