export interface IProps {
  className?: string
  uniqueId: string
  message: string
  onClose: (event: React.MouseEvent<HTMLElement>) => void
}
