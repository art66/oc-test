export interface IProps {
  className?: string
  amount: number
  buyAmount: number
  name: string
  disabled?: boolean
  onAmountChange: (buyAmount: number) => void
  onBuy: (event: React.MouseEvent<HTMLElement>) => void
}
