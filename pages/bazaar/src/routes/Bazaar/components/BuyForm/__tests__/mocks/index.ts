import { IProps } from '../../types'

export const initialState: IProps = {
  buyAmount: 8000,
  amount: 10000,
  name: 'AK-74',
  onAmountChange: () => {},
  onBuy: () => {}
}

export const classNameExtendState: IProps = {
  ...initialState,
  className: 'testClassName'
}

export const disabledState: IProps = {
  ...initialState,
  buyAmount: 0
}
