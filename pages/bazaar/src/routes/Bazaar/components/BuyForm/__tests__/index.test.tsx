import React from 'react'
import { shallow } from 'enzyme'

import BuyForm from '../'
import NumberInput from '../../../../../components/NumberInput'
import { initialState, classNameExtendState, disabledState } from './mocks'

describe('<BuyForm />', () => {
  it('should render', () => {
    const Component = shallow<BuyForm>(<BuyForm {...initialState} />)
    const component = Component.instance()

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyForm')
    const FormChildren = Component.children()
    expect(FormChildren.at(0).prop('className')).toBe('field')
    const InputFieldChildren = FormChildren.at(0).children()
    expect(InputFieldChildren.at(0).prop('className')).toBe('buyAmountInput')
    expect(InputFieldChildren.at(0).find(NumberInput).length).toBe(1)
    const NumberInputChildren = InputFieldChildren.at(0).find(NumberInput)
    expect(NumberInputChildren.prop('className')).toBe('buyAmountInput')
    expect(NumberInputChildren.prop('value')).toBe('8000')
    expect(NumberInputChildren.prop('max')).toBe(10000)
    expect(NumberInputChildren.prop('onChange')).toBe(component._onBuyAmoutChangeHandler)
    expect(FormChildren.at(1).prop('className')).toBe('field')
    const BuyButtonChildren = FormChildren.at(1).children()
    expect(BuyButtonChildren.at(0).prop('className')).toBe('buy')
    expect(BuyButtonChildren.at(0).prop('disabled')).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })

  it('should render with class name extended', () => {
    const Component = shallow<BuyForm>(<BuyForm {...classNameExtendState} />)
    const component = Component.instance()

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyForm testClassName')
    const FormChildren = Component.children()
    expect(FormChildren.at(0).prop('className')).toBe('field')
    const InputFieldChildren = FormChildren.at(0).children()
    expect(InputFieldChildren.at(0).prop('className')).toBe('buyAmountInput')
    expect(InputFieldChildren.at(0).find(NumberInput).length).toBe(1)
    const NumberInputChildren = InputFieldChildren.at(0).find(NumberInput)
    expect(NumberInputChildren.prop('className')).toBe('buyAmountInput')
    expect(NumberInputChildren.prop('value')).toBe('8000')
    expect(NumberInputChildren.prop('max')).toBe(10000)
    expect(NumberInputChildren.prop('onChange')).toBe(component._onBuyAmoutChangeHandler)
    expect(FormChildren.at(1).prop('className')).toBe('field')
    const BuyButtonChildren = FormChildren.at(1).children()
    expect(BuyButtonChildren.at(0).prop('className')).toBe('buy')
    expect(BuyButtonChildren.at(0).prop('disabled')).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })

  it('should render with disabled buy button', () => {
    const Component = shallow<BuyForm>(<BuyForm {...disabledState} />)
    const component = Component.instance()

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyForm')
    const FormChildren = Component.children()
    expect(FormChildren.at(0).prop('className')).toBe('field')
    const InputFieldChildren = FormChildren.at(0).children()
    expect(InputFieldChildren.at(0).prop('className')).toBe('buyAmountInput')
    expect(InputFieldChildren.at(0).find(NumberInput).length).toBe(1)
    const NumberInputChildren = InputFieldChildren.at(0).find(NumberInput)
    expect(NumberInputChildren.prop('className')).toBe('buyAmountInput')
    expect(NumberInputChildren.prop('value')).toBe('0')
    expect(NumberInputChildren.prop('max')).toBe(10000)
    expect(NumberInputChildren.prop('onChange')).toBe(component._onBuyAmoutChangeHandler)
    expect(FormChildren.at(1).prop('className')).toBe('field')
    const BuyButtonChildren = FormChildren.at(1).children()
    expect(BuyButtonChildren.at(0).prop('className')).toBe('buy')
    expect(BuyButtonChildren.at(0).prop('disabled')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should handle click on buy', () => {
    const onBuy = jest.fn()
    const event = {}
    const state = { ...initialState, onBuy }
    const Component = shallow<BuyForm>(<BuyForm {...state} />)

    Component.find('.buy').simulate('click', event)

    expect(onBuy).toHaveBeenCalledTimes(1)

    expect(onBuy.mock.calls[0][0]).toBe(event)

    expect(onBuy).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
