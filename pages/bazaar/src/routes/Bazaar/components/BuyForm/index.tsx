import React, { PureComponent } from 'react'
import cn from 'classnames'
import { NumberInput } from '../../../../components'
import { IProps } from './types'
import { AMOUNT_MAX_VALUE } from '../../../../constants'
import styles from './index.cssmodule.scss'

export class BuyForm extends PureComponent<IProps> {
  _getRemoveMaxLimit() {
    const { amount } = this.props

    const maxLimit = Math.min(amount, AMOUNT_MAX_VALUE)

    return maxLimit
  }

  _onBuyAmoutChangeHandler = buyAmount => {
    const { onAmountChange } = this.props

    onAmountChange(buyAmount)
  }

  render() {
    const { className, buyAmount, name, disabled, onBuy } = this.props

    return (
      <div className={cn(styles.buyForm, className)}>
        <div className={styles.field}>
          <NumberInput
            className={styles.buyAmountInput}
            value={buyAmount?.toString()}
            max={this._getRemoveMaxLimit()}
            disabled={disabled}
            onChange={this._onBuyAmoutChangeHandler}
            ariaLabel={`Amount of ${name}`}
          />
        </div>
        <div className={styles.field}>
          <button
            type='button'
            className={styles.buy}
            onClick={onBuy}
            disabled={disabled || buyAmount < 1}
            aria-label={`Buy: ${name}`}
          >
            Buy
          </button>
        </div>
      </div>
    )
  }
}

export default BuyForm
