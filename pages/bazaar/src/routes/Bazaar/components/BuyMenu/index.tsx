import React, { PureComponent } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import BuyForm from '../BuyForm'
import { CROSS_ICON_CONFIG } from '../../constants'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import { numberToCurrency } from '../../../../utils'

export class BuyMenu extends PureComponent<IProps> {
  _getPrice = () => {
    const { price } = this.props

    return numberToCurrency(price)
  }

  render() {
    const { className, name, amount, buyAmount, onClose, onBuyAmountChange, onBuy } = this.props

    return (
      <div className={cn(styles.buyMenu, className)}>
        <div className={styles.info}>
          <p /* className={styles.infoText} */>
            <span className={styles.name}>{name}</span>
            <span className={styles.price}>${this._getPrice()}</span>
            <span className={styles.amount}>({amount} in stock)</span>
          </p>
          <button className={styles.close} aria-label="Close" onClick={onClose} autoFocus={true}>
            <SVGIconGenerator {...CROSS_ICON_CONFIG} />
          </button>
        </div>
        <BuyForm buyAmount={buyAmount} amount={amount} name={name} onAmountChange={onBuyAmountChange} onBuy={onBuy} />
      </div>
    )
  }
}

export default BuyMenu
