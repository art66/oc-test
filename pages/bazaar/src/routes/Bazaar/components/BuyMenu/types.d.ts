export interface IProps {
  className?: string
  name: string
  price: number
  amount: number
  buyAmount: number
  onBuyAmountChange: (buyAmount: number) => void
  onBuy: (event: React.MouseEvent<HTMLElement>) => void
  onClose: (event: React.MouseEvent<HTMLElement>) => void
}
