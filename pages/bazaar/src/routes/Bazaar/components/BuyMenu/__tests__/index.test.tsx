import React from 'react'
import { shallow } from 'enzyme'

import BuyMeny from '../'
import { BuyForm } from '../../'
import { initialState, classNameExtendState } from './mocks'

describe('<BuyMeny />', () => {
  it('should render', () => {
    const Component = shallow<BuyMeny>(<BuyMeny {...initialState} />)

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyMenu')
    const BuyMenyChildren = Component.children()
    expect(BuyMenyChildren.length).toBe(2)
    expect(BuyMenyChildren.at(0).prop('className')).toBe('info')
    const InfoChildren = BuyMenyChildren.at(0).children()
    expect(InfoChildren.length).toBe(2)
    // expect(InfoChildren.at(0).prop('className')).toBe('infoText')
    const InfoTextChildren = InfoChildren.at(0).children()
    expect(InfoTextChildren.length).toBe(3)
    expect(InfoTextChildren.at(0).prop('className')).toBe('name')
    expect(InfoTextChildren.at(0).text()).toBe('Lead Pipe')
    expect(InfoTextChildren.at(1).prop('className')).toBe('price')
    expect(InfoTextChildren.at(1).text()).toBe('$20,000,000,000')
    expect(InfoTextChildren.at(2).prop('className')).toBe('amount')
    expect(InfoTextChildren.at(2).text()).toBe('(10000 in stock)')
    expect(InfoChildren.at(1).prop('className')).toBe('close')
    expect(BuyMenyChildren.at(1).is(BuyForm)).toBeTruthy()
    expect(BuyMenyChildren.at(1).prop('buyAmount')).toBe(8000)
    expect(BuyMenyChildren.at(1).prop('amount')).toBe(10000)
    expect(BuyMenyChildren.at(1).prop('onAmountChange')).toEqual(initialState.onBuyAmountChange)
    expect(BuyMenyChildren.at(1).prop('onBuy')).toEqual(initialState.onBuy)

    expect(Component).toMatchSnapshot()
  })

  it('should render with class name extended', () => {
    const Component = shallow<BuyMeny>(<BuyMeny {...classNameExtendState} />)

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyMenu testClassName')
    const BuyMenyChildren = Component.children()
    expect(BuyMenyChildren.length).toBe(2)
    expect(BuyMenyChildren.at(0).prop('className')).toBe('info')
    const InfoChildren = BuyMenyChildren.at(0).children()
    expect(InfoChildren.length).toBe(2)
    // expect(InfoChildren.at(0).prop('className')).toBe('infoText')
    const InfoTextChildren = InfoChildren.at(0).children()
    expect(InfoTextChildren.length).toBe(3)
    expect(InfoTextChildren.at(0).prop('className')).toBe('name')
    expect(InfoTextChildren.at(0).text()).toBe('Lead Pipe')
    expect(InfoTextChildren.at(1).prop('className')).toBe('price')
    expect(InfoTextChildren.at(1).text()).toBe('$20,000,000,000')
    expect(InfoTextChildren.at(2).prop('className')).toBe('amount')
    expect(InfoTextChildren.at(2).text()).toBe('(10000 in stock)')
    expect(InfoChildren.at(1).prop('className')).toBe('close')
    expect(BuyMenyChildren.at(1).is(BuyForm)).toBeTruthy()
    expect(BuyMenyChildren.at(1).prop('buyAmount')).toBe(8000)
    expect(BuyMenyChildren.at(1).prop('amount')).toBe(10000)
    expect(BuyMenyChildren.at(1).prop('onAmountChange')).toEqual(initialState.onBuyAmountChange)
    expect(BuyMenyChildren.at(1).prop('onBuy')).toEqual(initialState.onBuy)

    expect(Component).toMatchSnapshot()
  })

  it('should handle click on close', () => {
    const onClose = jest.fn()
    const event = {}
    const state = { ...initialState, onClose }
    const Component = shallow<BuyMeny>(<BuyMeny {...state} />)

    Component.find('.close').simulate('click', event)

    expect(onClose).toHaveBeenCalledTimes(1)

    expect(onClose.mock.calls[0][0]).toBe(event)

    expect(onClose).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
