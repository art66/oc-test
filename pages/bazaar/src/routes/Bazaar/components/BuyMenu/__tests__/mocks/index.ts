import { IProps } from '../../types'

export const initialState: IProps = {
  name: 'Lead Pipe',
  price: 20000000000,
  buyAmount: 8000,
  amount: 10000,
  onBuyAmountChange: () => {},
  onBuy: () => {},
  onClose: () => {}
}

export const classNameExtendState: IProps = {
  ...initialState,
  className: 'testClassName'
}
