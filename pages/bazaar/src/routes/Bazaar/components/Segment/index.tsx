import React, { PureComponent } from 'react'
import cn from 'classnames'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

export class Segment extends PureComponent<IProps> {
  _getClassName = () => {
    const { className, padding } = this.props

    return cn(
      styles.segment,
      {
        [styles.noPadding]: padding === 'none'
      },
      className
    )
  }

  render() {
    const { children } = this.props

    return <div className={this._getClassName()}>{children}</div>
  }
}

export default Segment
