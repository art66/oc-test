import React from 'react'
import { shallow } from 'enzyme'

import Segment from '../'
import { initialState, classNameExtendsState, noPaddingState, noPaddingClassNameExtendsState } from './mocks'

describe('<Segment />', () => {
  it('should render', () => {
    const Component = shallow(<Segment {...initialState} />)

    expect(Component.length).toBe(1)
    expect(Component.children().text()).toBe('Test content')

    expect(Component).toMatchSnapshot()
  })

  it('should render with children component', () => {
    const Children = () => <div className='unique' />
    const Component = shallow(
      <Segment {...initialState}>
        <Children />
      </Segment>
    )

    expect(Component.prop('className')).toBe('segment')
    expect(Component.children().html()).toBe('<div class="unique"></div>')

    expect(Component).toMatchSnapshot()
  })

  it('should render with extra class name', () => {
    const Children = () => <div className='unique' />
    const Component = shallow(
      <Segment {...classNameExtendsState}>
        <Children />
      </Segment>
    )

    expect(Component.prop('className')).toBe('segment testClassName')
    expect(Component.children().html()).toBe('<div class="unique"></div>')

    expect(Component).toMatchSnapshot()
  })

  it('should render with no padding', () => {
    const Children = () => <div className='unique' />
    const Component = shallow(
      <Segment {...noPaddingState}>
        <Children />
      </Segment>
    )

    expect(Component.prop('className')).toBe('segment noPadding')
    expect(Component.children().html()).toBe('<div class="unique"></div>')

    expect(Component).toMatchSnapshot()
  })

  it('should render with no padding class name extends', () => {
    const Children = () => <div className='unique' />
    const Component = shallow(
      <Segment {...noPaddingClassNameExtendsState}>
        <Children />
      </Segment>
    )

    expect(Component.prop('className')).toBe('segment noPadding testClassName')
    expect(Component.children().html()).toBe('<div class="unique"></div>')

    expect(Component).toMatchSnapshot()
  })
})
