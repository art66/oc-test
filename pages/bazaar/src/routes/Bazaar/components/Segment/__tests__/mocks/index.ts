import { IProps } from '../../types'

export const initialState: IProps = {
  children: 'Test content'
}

export const classNameExtendsState: IProps = {
  ...initialState,
  className: 'testClassName'
}

export const noPaddingState: IProps = {
  ...initialState,
  padding: 'none'
}

export const noPaddingClassNameExtendsState: IProps = {
  ...noPaddingState,
  className: 'testClassName'
}
