export interface IProps {
  children: JSX.Element[] | JSX.Element | string
  className?: string
  padding?: 'none'
}
