import { createSelector } from 'reselect'
import { IState } from '../../../../store/types'
import { ISearchBar } from '../../modules/types/state'
import { capitalizeFirstLetter } from '../../../../utils'
// import { getMediaType } from '../../../../modules/selectors'

export const getFiltersInfo = (state: IState): ISearchBar => {
  const { sortBy, orderBy, search } = state.bazaar.searchBar

  return { sortBy, orderBy, search }
}

export const getLoaderInfo = createSelector([getFiltersInfo /* , getMediaType */], (
  { sortBy, orderBy, search } /* , mediaType */
): string => {
  const mainText = `ordered by ${capitalizeFirstLetter(sortBy)} ${orderBy === 'asc' ? 'ascending' : 'descending'}${
    search ? ` and matching: "${search}"` : ''
  }`

  // if (mediaType === 'desktop') {
  //   return `Showing items in Wowzas bazaar ${mainText}`
  // }

  return `Items ${mainText}`
})
