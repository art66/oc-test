import React from 'react'
import { WindowScroller } from 'react-virtualized'
import { shallow } from 'enzyme'

import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import { Items } from '../'
import {
  initialState,
  isLoadingState,
  isLoadingSameState,
  isLoadingMoreState,
  isLoadingMoreAndSameState,
  categoriesState,
  withViewState,
  categoriesWithViewState
} from './mocks'

describe('<Items />', () => {
  it('should render', () => {
    const Component = shallow<Items>(<Items {...initialState} />)

    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const LoadingSameComponent = ComponentChildren.at(0)
    expect(LoadingSameComponent.prop('className')).toBe('loadingSame')
    expect(LoadingSameComponent.children().length).toBe(1)
    const LoadingIndicatorComponent = LoadingSameComponent.children().at(0)
    expect(LoadingIndicatorComponent.prop('className')).toBe('loaderText')
    expect(LoadingIndicatorComponent.text()).toBe('Test text')
    const WindowScrollerComponent = ComponentChildren.at(1)
    expect(WindowScrollerComponent.is(WindowScroller)).toBeTruthy()
    expect(WindowScrollerComponent.prop('scrollingResetTimeInterval')).toBe(500)

    expect(Component).toMatchSnapshot()
  })

  it('should render loader on loading', () => {
    const Component = shallow<Items>(<Items {...isLoadingState} />)

    expect(Component.is(LoadingIndicator)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render loader for the same filter', () => {
    const Component = shallow<Items>(<Items {...isLoadingSameState} />)

    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const LoadingSameComponent = ComponentChildren.at(0)
    expect(LoadingSameComponent.prop('className')).toBe('loadingSame')
    expect(LoadingSameComponent.children().length).toBe(1)
    const LoadingIndicatorComponent = LoadingSameComponent.children().at(0)
    expect(LoadingIndicatorComponent.is(LoadingIndicator)).toBeTruthy()
    const WindowScrollerComponent = ComponentChildren.at(1)
    expect(WindowScrollerComponent.is(WindowScroller)).toBeTruthy()
    expect(WindowScrollerComponent.prop('scrollingResetTimeInterval')).toBe(500)

    expect(Component).toMatchSnapshot()
  })

  it('should render with loader for the more items', () => {
    const Component = shallow<Items>(<Items {...isLoadingMoreState} />)

    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(3)
    const LoadingSameComponent = ComponentChildren.at(0)
    expect(LoadingSameComponent.prop('className')).toBe('loadingSame')
    expect(LoadingSameComponent.children().length).toBe(1)
    const LoadingIndicatorComponent = LoadingSameComponent.children().at(0)
    expect(LoadingIndicatorComponent.prop('className')).toBe('loaderText')
    expect(LoadingIndicatorComponent.text()).toBe('Test text')
    const WindowScrollerComponent = ComponentChildren.at(1)
    expect(WindowScrollerComponent.is(WindowScroller)).toBeTruthy()
    expect(WindowScrollerComponent.prop('scrollingResetTimeInterval')).toBe(500)
    const LoadingMoreComponent = ComponentChildren.at(2)
    expect(LoadingMoreComponent.is(LoadingIndicator)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render with two loader for the same filter and more items', () => {
    const Component = shallow<Items>(<Items {...isLoadingMoreAndSameState} />)

    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(3)
    const LoadingSameComponent = ComponentChildren.at(0)
    expect(LoadingSameComponent.prop('className')).toBe('loadingSame')
    expect(LoadingSameComponent.children().length).toBe(1)
    const LoadingIndicatorComponent = LoadingSameComponent.children().at(0)
    expect(LoadingIndicatorComponent.is(LoadingIndicator)).toBeTruthy()
    const WindowScrollerComponent = ComponentChildren.at(1)
    expect(WindowScrollerComponent.is(WindowScroller)).toBeTruthy()
    expect(WindowScrollerComponent.prop('scrollingResetTimeInterval')).toBe(500)
    const LoadingMoreComponent = ComponentChildren.at(2)
    expect(LoadingMoreComponent.is(LoadingIndicator)).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should _isRowLoaded method works correctly', () => {
    const Component = shallow<Items>(<Items {...initialState} />)
    const component = Component.instance()

    expect(component._isRowLoaded({ index: 5 })).toBe(true)
    expect(component._isRowLoaded({ index: 11 })).toBe(true)
    expect(component._isRowLoaded({ index: 12 })).toBe(false)

    expect(Component).toMatchSnapshot()
  })

  it('should _setViewHeight method works correct', () => {
    const setViewHeightAction = jest.fn()
    const recomputeRowHeights = jest.fn()
    const Component = shallow<Items>(<Items {...{ ...initialState, setViewHeightAction }} />)
    const component = Component.instance()

    component._recomputeRowHeights = recomputeRowHeights

    component._setViewHeight()

    expect(recomputeRowHeights).toHaveBeenCalledTimes(1)

    expect(setViewHeightAction).toMatchSnapshot()
    expect(recomputeRowHeights).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _deactivateItemView method works correctly', () => {
    const setActiveViewUniqueIdAction = jest.fn()
    const recomputeRowHeights = jest.fn()
    const Component = shallow<Items>(<Items {...{ ...initialState, setActiveViewUniqueIdAction }} />)
    const component = Component.instance()

    component._recomputeRowHeights = recomputeRowHeights

    component._deactivateItemView()

    expect(setActiveViewUniqueIdAction).toHaveBeenCalledTimes(1)
    expect(setActiveViewUniqueIdAction.mock.calls[0]).toEqual([null])

    expect(recomputeRowHeights).toHaveBeenCalledTimes(1)

    expect(setActiveViewUniqueIdAction).toMatchSnapshot()
    expect(recomputeRowHeights).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _activateItemView method works correctly', () => {
    const setActiveViewUniqueIdAction = jest.fn()
    const recomputeRowHeights = jest.fn()
    const forceUpdateGrid = jest.fn()
    const Component = shallow<Items>(<Items {...{ ...initialState, setActiveViewUniqueIdAction }} />)
    const component = Component.instance()

    component._recomputeRowHeights = recomputeRowHeights

    expect(component._listRef).toBeTruthy()

    component._listRef = {
      current: {
        forceUpdateGrid
      }
    }

    component._activateItemView('54768-87455')

    expect(setActiveViewUniqueIdAction).toHaveBeenCalledTimes(1)
    expect(setActiveViewUniqueIdAction.mock.calls[0]).toEqual(['54768-87455'])

    expect(recomputeRowHeights).toHaveBeenCalledTimes(1)
    expect(forceUpdateGrid).toHaveBeenCalledTimes(1)

    expect(setActiveViewUniqueIdAction).toMatchSnapshot()
    expect(recomputeRowHeights).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method works correctly', () => {
    const Component = shallow<Items>(<Items {...initialState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 1 })).toBe(73)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method works correctly when item', () => {
    const Component = shallow<Items>(<Items {...initialState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 1 })).toBe(73)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method works correctly when item which category header', () => {
    const Component = shallow<Items>(<Items {...categoriesState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 2 })).toBe(108)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method works correctly when item which view', () => {
    const Component = shallow<Items>(<Items {...withViewState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 4 })).toBe(199)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method works correctly when item which category header and view', () => {
    const Component = shallow<Items>(<Items {...categoriesWithViewState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 2 })).toBe(234)

    expect(Component).toMatchSnapshot()
  })
})
