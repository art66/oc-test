import { IProps } from '../../types'

import rows, { desktopRowsWithView } from '../../../../modules/__tests__/mocks/desktopRows'
import mobileRows, { mobileRowsWithView } from '../../../../modules/__tests__/mocks/mobileRows'
import { categorizedRows, categoryWithViewRows } from '../../../../modules/__tests__/mocks/categorizedRows'
import {
  categorizedRowsMobile,
  categoryRowsWithViewMobile
} from '../../../../modules/__tests__/mocks/categorizedRowsMobile'
import { viewInfoWithSettings } from '../../../../../../modules/__tests__/mocks/viewInfo'
import { DESKTOP_WIDTH } from '../../../../../../constants'

export const initialState: IProps = {
  isLoading: false,
  isLoadingMore: false,
  isLoadingSame: false,
  userId: '535378',
  ownerName: 'love_intent',
  listWidth: DESKTOP_WIDTH,
  rows,
  rowsTotal: 5,
  itemsInRow: 3,
  activeViewId: null,
  formData: {},
  viewHeight: viewInfoWithSettings.height,
  mediaType: 'desktop',
  loaderInfo: 'Test text',
  isDarkMode: false,
  bazaarItemsMoreRequestAction: () => {},
  bazaarItemInfoRequestAction: () => {},
  setActiveViewUniqueIdAction: () => {},
  setViewHeightAction: () => {},
  removeItemAction: () => {},
  updateItemAmountAction: () => {},
  updateFormDataAction: () => {},
  onImgBarFocusAction: () => {},
  changeActiveBuyTypeAction: () => {}
}

export const isLoadingState: IProps = {
  ...initialState,
  isLoading: true
}

export const noRowsState: IProps = {
  ...initialState,
  rowsTotal: 0
}

export const isLoadingSameState: IProps = {
  ...initialState,
  isLoadingSame: true
}

export const isLoadingMoreState: IProps = {
  ...initialState,
  isLoadingMore: true
}

export const isLoadingMoreAndSameState: IProps = {
  ...initialState,
  isLoadingSame: true,
  isLoadingMore: true
}

export const deviceState: IProps = {
  ...initialState,
  rows: mobileRows
}

export const withViewState: IProps = {
  ...initialState,
  rows: desktopRowsWithView
}

export const deviceWithViewState: IProps = {
  ...deviceState,
  rows: mobileRowsWithView
}

export const categoriesState: IProps = {
  ...initialState,
  rows: categorizedRows
}

export const categoriesDeviceState: IProps = {
  ...initialState,
  rows: categorizedRowsMobile
}

export const categoriesWithViewState: IProps = {
  ...initialState,
  rows: categoryWithViewRows
}

export const categoriesWithViewDeviceState: IProps = {
  ...initialState,
  rows: categoryRowsWithViewMobile
}

export const activeViewIdState: IProps = {
  ...initialState,
  activeViewId: '806-0'
}
