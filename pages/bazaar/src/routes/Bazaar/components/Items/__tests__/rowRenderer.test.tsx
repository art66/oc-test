import React from 'react'
import { shallow } from 'enzyme'

import { Items } from '../'
import {
  initialState,
  deviceState,
  withViewState,
  categoriesState,
  categoriesDeviceState,
  deviceWithViewState,
  categoriesWithViewState,
  categoriesWithViewDeviceState
} from './mocks'
import Item from '../../Item'

describe('<Items /> row renderer', () => {
  it('should render row', () => {
    const Component = shallow<Items>(<Items {...initialState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 2, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(1)
    const RowItemsComponent = ItemChildren.at(0)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(3)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('12-3591872972')
    expect(RowItemsChildren.at(1).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(1).key()).toBe('12-3591872973')
    expect(RowItemsChildren.at(2).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(2).key()).toBe('12-3591872974')

    expect(Component).toMatchSnapshot()
  })

  it('should render row on device', () => {
    const Component = shallow<Items>(<Items {...deviceState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 2, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(1)
    const RowItemsComponent = ItemChildren.at(0)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(1)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('12-3591872967')

    expect(Component).toMatchSnapshot()
  })

  it('should render row with view', () => {
    const Component = shallow<Items>(<Items {...withViewState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 4, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(2)
    const RowItemsComponent = ItemChildren.at(0)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(3)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('12-3591872978')
    expect(RowItemsChildren.at(1).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(1).key()).toBe('12-3591872979')
    expect(RowItemsChildren.at(2).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(2).key()).toBe('12-3591872980')
    const ViewComponent = ItemChildren.at(1)
    expect(ViewComponent.prop('className')).toBe('view')

    expect(Component).toMatchSnapshot()
  })

  it('should render row with view on device', () => {
    const Component = shallow<Items>(<Items {...deviceWithViewState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 14, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(2)
    const RowItemsComponent = ItemChildren.at(0)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(1)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('12-3591872980')
    const ViewComponent = ItemChildren.at(1)
    expect(ViewComponent.prop('className')).toBe('view')

    expect(Component).toMatchSnapshot()
  })

  it('should render row with category', () => {
    const Component = shallow<Items>(<Items {...categoriesState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 2, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(2)
    const RowCategoryComponent = ItemChildren.at(0)
    expect(RowCategoryComponent.prop('className')).toBe('category')
    expect(RowCategoryComponent.text()).toBe('Clothing')
    const RowItemsComponent = ItemChildren.at(1)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(2)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('404-0')
    expect(RowItemsChildren.at(1).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(1).key()).toBe('806-0')

    expect(Component).toMatchSnapshot()
  })

  it('should render row with category on device', () => {
    const Component = shallow<Items>(<Items {...categoriesDeviceState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 3, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(2)
    const RowCategoryComponent = ItemChildren.at(0)
    expect(RowCategoryComponent.prop('className')).toBe('category')
    expect(RowCategoryComponent.text()).toBe('Clothing')
    const RowItemsComponent = ItemChildren.at(1)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(1)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('404-0')

    expect(Component).toMatchSnapshot()
  })

  it('should render row with category and view', () => {
    const Component = shallow<Items>(<Items {...categoriesWithViewState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 2, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(3)
    const RowCategoryComponent = ItemChildren.at(0)
    expect(RowCategoryComponent.prop('className')).toBe('category')
    expect(RowCategoryComponent.text()).toBe('Clothing')
    const RowItemsComponent = ItemChildren.at(1)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(2)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('404-0')
    expect(RowItemsChildren.at(1).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(1).key()).toBe('806-0')
    const ViewComponent = ItemChildren.at(2)
    expect(ViewComponent.prop('className')).toBe('view')

    expect(Component).toMatchSnapshot()
  })

  it('should render row with category and view on device', () => {
    const Component = shallow<Items>(<Items {...categoriesWithViewDeviceState} />)
    const component = Component.instance()
    const props = { key: 'xx-1', index: 1, style: { top: 50 } }

    const ItemComponent = shallow(component._renderRow(props))
    expect(ItemComponent.is('div')).toBeTruthy()
    expect(ItemComponent.key()).toBe('xx-1')
    expect(ItemComponent.prop('className')).toBe('row')
    expect(ItemComponent.prop('style')).toEqual({ top: 50 })
    const ItemChildren = ItemComponent.children()
    expect(ItemChildren.length).toBe(3)
    const RowCategoryComponent = ItemChildren.at(0)
    expect(RowCategoryComponent.prop('className')).toBe('category')
    expect(RowCategoryComponent.text()).toBe('Candy')
    const RowItemsComponent = ItemChildren.at(1)
    expect(RowItemsComponent.prop('className')).toBe('rowItems')
    const RowItemsChildren = RowItemsComponent.children()
    expect(RowItemsChildren.length).toBe(1)
    expect(RowItemsChildren.at(0).is(Item)).toBeTruthy()
    expect(RowItemsChildren.at(0).key()).toBe('556-0')
    const ViewComponent = ItemChildren.at(2)
    expect(ViewComponent.prop('className')).toBe('view')

    expect(Component).toMatchSnapshot()
  })
})
