import React from 'react'
import { shallow } from 'enzyme'

import { Items } from '..'
import { initialState, activeViewIdState } from './mocks'

describe('<Items /> view renderer', () => {
  it('should render view', () => {
    const Component = shallow<Items>(<Items {...activeViewIdState} />)
    const component = Component.instance()

    const viewComponent = component._renderView(true)
    expect(viewComponent).toBeTruthy()
    expect(viewComponent.props.className).toBe('view')
    expect(viewComponent.props.uniqueId).toBe('806-0')
    expect(viewComponent.props.onClose).toBe(component._deactivateItemView)
    expect(viewComponent.props.onHeightUpdate).toBe(component._setViewHeight)

    expect(Component).toMatchSnapshot()
  })

  it('shouldn\'t render view', () => {
    const Component = shallow<Items>(<Items {...initialState} />)
    const component = Component.instance()

    const viewComponent = component._renderView(false)
    expect(viewComponent).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })
})
