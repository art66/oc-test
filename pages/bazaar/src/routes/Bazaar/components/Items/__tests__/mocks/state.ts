import {
  state as globalState,
  tabletState as globalTabletState,
  mobileState as globalMobileState
} from '../../../../../../modules/__tests__/mocks/state'
import list from '../../../../modules/__tests__/mocks/items'

import { IState } from '../../../../../../store/types'

export const state: IState = {
  ...globalState,
  browser: {
    mediaType: 'desktop'
  },
  bazaar: {
    ...globalState.bazaar,
    searchBar: { sortBy: 'default', orderBy: 'asc', search: '' },
    // @ts-ignore
    bazaarItems: {
      list,
      total: list.length
    },
    activeView: {
      uniqueId: null,
      itemId: null,
      armoryId: null
    }
  },
  // @ts-ignore
  common: {
    views: {},
    viewsSettings: {}
  }
}

export const tabletState: IState = {
  ...state,
  browser: {
    ...globalTabletState.browser
  }
}

export const mobileState: IState = {
  ...state,
  browser: {
    ...globalMobileState.browser
  }
}

export const withViewState: IState = {
  ...state,
  bazaar: {
    ...state.bazaar,
    bazaarItems: {
      ...state.bazaar.bazaarItems
    },
    activeView: {
      ...state.bazaar.bazaarItems,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const tabletWithViewState: IState = {
  ...tabletState,
  bazaar: {
    ...tabletState.bazaar,
    activeView: {
      ...tabletState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const mobileWithViewState: IState = {
  ...mobileState,
  bazaar: {
    ...mobileState.bazaar,
    activeView: {
      ...mobileState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const categorizedState: IState = {
  ...state,
  bazaar: {
    ...state.bazaar,
    searchBar: { ...state.bazaar.searchBar, sortBy: 'category' }
  }
}

export const categorizedTabletState: IState = {
  ...state,
  browser: {
    mediaType: 'tablet'
  },
  bazaar: {
    ...state.bazaar,
    searchBar: { ...state.bazaar.searchBar, sortBy: 'category' }
  }
}

export const categorizedMobileState: IState = {
  ...state,
  browser: {
    mediaType: 'mobile'
  },
  bazaar: {
    ...state.bazaar,
    searchBar: { ...state.bazaar.searchBar, sortBy: 'category' }
  }
}

export const categorizedWithViewState: IState = {
  ...categorizedState,
  bazaar: {
    ...categorizedState.bazaar,
    activeView: {
      ...categorizedState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const tabletCategorizedWithViewState: IState = {
  ...categorizedTabletState,
  bazaar: {
    ...categorizedTabletState.bazaar,
    activeView: {
      ...categorizedTabletState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const mobileCategorizedWithViewState: IState = {
  ...categorizedMobileState,
  bazaar: {
    ...categorizedMobileState.bazaar,
    activeView: {
      ...categorizedMobileState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const categorizedDescState: IState = {
  ...categorizedState,
  bazaar: {
    ...categorizedState.bazaar,
    searchBar: {
      ...categorizedState.bazaar.searchBar,
      orderBy: 'desc'
    }
  }
}

export const categorizedTabletDescState: IState = {
  ...categorizedTabletState,
  bazaar: {
    ...categorizedTabletState.bazaar,
    searchBar: {
      ...categorizedTabletState.bazaar.searchBar,
      orderBy: 'desc'
    }
  }
}

export const categorizedMobileDescState: IState = {
  ...categorizedMobileState,
  bazaar: {
    ...categorizedMobileState.bazaar,
    searchBar: {
      ...categorizedMobileState.bazaar.searchBar,
      orderBy: 'desc'
    }
  }
}

export const categorizedDescWithViewState: IState = {
  ...categorizedWithViewState,
  bazaar: {
    ...categorizedWithViewState.bazaar,
    searchBar: {
      ...categorizedWithViewState.bazaar.searchBar,
      orderBy: 'desc'
    },
    activeView: {
      ...categorizedWithViewState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const tabletCategorizedDescWithViewState: IState = {
  ...tabletCategorizedWithViewState,
  bazaar: {
    ...tabletCategorizedWithViewState.bazaar,
    searchBar: {
      ...tabletCategorizedWithViewState.bazaar.searchBar,
      orderBy: 'desc'
    },
    activeView: {
      ...tabletCategorizedWithViewState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export const mobileCategorizedDescWithViewState: IState = {
  ...mobileCategorizedWithViewState,
  bazaar: {
    ...mobileCategorizedWithViewState.bazaar,
    searchBar: {
      ...mobileCategorizedWithViewState.bazaar.searchBar,
      orderBy: 'desc'
    },
    activeView: {
      ...mobileCategorizedWithViewState.bazaar.activeView,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
  }
}

export default state
