import { IBazaarItemInfoSettings, TItem } from '../../../../modules/types/state'
import { TFormData, TFormDataItem, TActiveBuyType } from '../../modules/types/state'

export interface IRow {
  rowItems: TItem[]
  categoryName?: string
  withView?: boolean
}

export interface IStateProps {
  isLoading: boolean
  isLoadingMore: boolean
  isLoadingSame: boolean
  userId: string
  ownerName: string
  listWidth: number
  rows: IRow[]
  formData: TFormData
  rowsTotal: number
  itemsInRow: number
  activeViewId?: string
  viewHeight?: IBazaarItemInfoSettings['height']
  mediaType: 'desktop' | 'tablet' | 'mobile'
  loaderInfo: string
}

export interface IProps extends IStateProps, IStateActions {
  isDarkMode: boolean
  setViewHeightAction?: (uniqueId: string, height: number) => void
}

export interface IStateActions {
  bazaarItemsMoreRequestAction: () => void
  bazaarItemInfoRequestAction: (uniqueId: string) => void
  setActiveViewUniqueIdAction: (uniqueId: string) => void
  removeItemAction: (uniqueId: string) => void
  updateItemAmountAction: (uniqueId: string, boughtAmount: number) => void
  updateFormDataAction: (uniqueId: string, formData: TFormDataItem) => void
  onImgBarFocusAction: (uniqueId: string) => void
  changeActiveBuyTypeAction: (uniqueId: string, activeBuyType?: TActiveBuyType) => void
}

export interface IDefaultProps {
  items: []
  isLoading: false
  rows: []
  bazaarItemsRequest: () => void
}

export interface IRenderRowItemsArgs {
  key?: string
  index: number
  style?: object
}
