import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { List, WindowScroller, InfiniteLoader } from 'react-virtualized'
import cn from 'classnames'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import {
  setActiveViewUniqueId,
  bazaarItemsMoreRequest,
  removeItem,
  updateItemAmount,
  updateFormData,
  itemImgBarFocus,
  changeActiveBuyType
} from '../../modules/actions'
import Item from '../Item'
import View from '../../../../components/View'
import { getRowHeight } from '../../modules/selectors'
import { NO_ITEMS_MESSAGE } from './constants'
import { IProps, IRenderRowItemsArgs, IStateProps, IStateActions, IDefaultProps } from './types'
import { IState as IGlobalState } from '../../../../store/types'
import { bazaarItemInfoRequest } from '../../../../modules/actions'
import styles from './index.cssmodule.scss'
import { ListMessage } from '../../../../components'
import { getScreenWidth, getMediaType } from '../../../../modules/selectors'
import { getLoaderInfo } from './selectors'
import { TActiveBuyType } from '../../modules/types/state'

export const update = {
  _recomputeRowHeights: () => {},
  _forceUpdateGrid: () => {}
}

export class Items extends PureComponent<IProps> {
  _listRef: React.RefObject<List>

  static defaultProps: IDefaultProps = {
    items: [],
    isLoading: false,
    rows: [],
    bazaarItemsRequest: () => {}
  }

  constructor(props: IProps) {
    super(props)

    this._listRef = React.createRef()

    update._recomputeRowHeights = this._recomputeRowHeights
    update._forceUpdateGrid = this._forceUpdateGrid
  }

  _recomputeRowHeights = () => {
    if (this._listRef.current) {
      /**
       * We should use this check because _listRef.current can be null when Items downloads and
       * View height changes(on View did unmount) in the same time
       */
      this._listRef.current.recomputeRowHeights()
    }
  }

  _forceUpdateGrid = () => {
    if (this._listRef.current) {
      /**
       * We should use this check because _listRef.current can be null when Items downloads and
       * View height changes(on View did unmount) in the same time
       */
      this._listRef.current.forceUpdateGrid()
    }
  }

  componentDidUpdate(prevProps: IProps) {
    if (this.props.isDarkMode !== prevProps.isDarkMode) {
      this._forceUpdateGrid()
    }
  }

  _getRowHeight = ({ index }: { index: number }) => {
    const { rows, viewHeight } = this.props
    const { [index]: row } = rows

    return getRowHeight({
      withCategory: row.categoryName,
      viewHeight: row.withView && viewHeight
    })
  }

  _activateItemView = (uniqueId: string) => {
    const { setActiveViewUniqueIdAction } = this.props

    setActiveViewUniqueIdAction(uniqueId)

    this._recomputeRowHeights()
    this._forceUpdateGrid()
  }

  _deactivateItemView = () => {
    const { setActiveViewUniqueIdAction } = this.props

    setActiveViewUniqueIdAction(null)

    this._recomputeRowHeights()
  }

  _setViewHeight = () => {
    this._recomputeRowHeights()
  }

  _loadMoreRows = () => {
    const { bazaarItemsMoreRequestAction } = this.props

    bazaarItemsMoreRequestAction()
  }

  _onItemRemove = uniqueId => {
    const { removeItemAction } = this.props

    removeItemAction(uniqueId)

    this._recomputeRowHeights()
    this._forceUpdateGrid()
  }

  _updateFormData = (uniqueId, data) => {
    const { updateFormDataAction } = this.props

    updateFormDataAction(uniqueId, data)

    this._recomputeRowHeights()
    this._forceUpdateGrid()
  }

  _onImgBarFocus = (uniqueId: string) => {
    const { onImgBarFocusAction } = this.props

    onImgBarFocusAction(uniqueId)

    this._recomputeRowHeights()
    this._forceUpdateGrid()
  }

  _onImgBarBlur = (uniqueId: string) => {
    const { updateFormDataAction } = this.props

    updateFormDataAction(uniqueId, { imgBarFocused: false })

    this._recomputeRowHeights()
    this._forceUpdateGrid()
  }

  _changeActiveBuyType = (uniqueId: string, activeBuyType?: TActiveBuyType) => {
    const { changeActiveBuyTypeAction } = this.props

    changeActiveBuyTypeAction(uniqueId, activeBuyType)

    this._recomputeRowHeights()
    this._forceUpdateGrid()
  }

  _isRowLoaded = ({ index }: { index: number }) => {
    return this.props.rows.length > index
  }

  _renderView = (withView: boolean) => {
    const { activeViewId } = this.props

    if (!withView) {
      return null
    }

    return (
      <View
        className={styles.view}
        key={activeViewId}
        uniqueId={activeViewId}
        onClose={this._deactivateItemView}
        onHeightUpdate={this._setViewHeight}
        noImage={true}
      />
    )
  }

  _renderRowItems = (rowIndex: number) => {
    const {
      userId,
      ownerName,
      activeViewId,
      rows,
      mediaType,
      formData,
      isDarkMode,
      updateItemAmountAction
    } = this.props
    const {
      [rowIndex]: { rowItems }
    } = rows

    return rowItems.map(item => {
      const { uniqueId } = item
      const { [uniqueId]: formDataItem } = formData
      const viewActive = activeViewId === uniqueId

      return (
        <Item
          key={uniqueId}
          {...item}
          {...formDataItem}
          userId={userId}
          ownerName={ownerName}
          className={cn(styles.item, { [styles.viewActive]: viewActive })}
          mediaType={mediaType}
          isDarkMode={isDarkMode}
          onViewActivate={this._activateItemView}
          onViewDeactivate={this._deactivateItemView}
          removeItem={this._onItemRemove}
          updateItemAmount={updateItemAmountAction}
          updateFormData={this._updateFormData}
          onImgBarFocus={this._onImgBarFocus}
          changeActiveBuyType={this._changeActiveBuyType}
        />
      )
    })
  }

  _renderRow = ({ key, index, style }: IRenderRowItemsArgs) => {
    const { rows } = this.props
    const { [index]: row } = rows

    return (
      <div key={key} style={style} className={styles.row}>
        {row.categoryName && <header className={styles.category}>{row.categoryName}</header>}
        <div className={styles.rowItems}>{this._renderRowItems(index)}</div>
        {this._renderView(row.withView)}
      </div>
    )
  }

  _renderLoaderInfo() {
    const { isLoadingSame, loaderInfo } = this.props

    if (isLoadingSame) {
      return <LoadingIndicator />
    }

    return <span className={styles.loaderText}>{loaderInfo}</span>
  }

  render() {
    const { rows, rowsTotal, listWidth, isLoading, isLoadingMore } = this.props

    if (isLoading) {
      return <LoadingIndicator />
    }

    return (
      <>
        <div className={styles.loadingSame}>{this._renderLoaderInfo()}</div>
        {rowsTotal ? (
          <WindowScroller scrollingResetTimeInterval={500}>
            {
              /* tslint:disable-next-line: jsx-no-multiline-js */
              ({ height, isScrolling, onChildScroll, scrollTop }) => {
                return (
                  <InfiniteLoader
                    isRowLoaded={this._isRowLoaded}
                    loadMoreRows={this._loadMoreRows}
                    rowCount={rowsTotal}
                    minimumBatchSize={100}
                    threshold={10}
                  >
                    {
                      /* tslint:disable-next-line: jsx-no-multiline-js */
                      ({ onRowsRendered }) => {
                        return (
                          <List
                            ref={this._listRef}
                            autoHeight={true}
                            width={listWidth}
                            height={height}
                            isScrolling={isScrolling}
                            scrollTop={scrollTop}
                            onScroll={onChildScroll}
                            onRowsRendered={onRowsRendered}
                            rowCount={rows.length}
                            overscanRowCount={20}
                            rowHeight={this._getRowHeight}
                            rowRenderer={this._renderRow}
                          />
                        )
                      }
                    }
                  </InfiniteLoader>
                )
              }
            }
          </WindowScroller>
        ) : (
          <ListMessage>{NO_ITEMS_MESSAGE}</ListMessage>
        )}
        {isLoadingMore && <LoadingIndicator />}
      </>
    )
  }
}

const mapStateToProps = (state: IGlobalState) => {
  const { isLoading, isLoadingMore, isLoadingSame } = state.bazaar.bazaarItems
  const { uniqueId: activeViewId } = state.bazaar.activeView
  const { [activeViewId]: viewSettings } = state.common.viewsSettings

  const stateProps: IStateProps = {
    mediaType: getMediaType(state),
    ownerName: state.common.owner.data.playername,
    userId: state.common.owner.data.userId,
    activeViewId,
    viewHeight: viewSettings && viewSettings.height,
    rows: state.bazaar.bazaarItems.rows,
    rowsTotal: state.bazaar.bazaarItems.totalRows,
    itemsInRow: state.bazaar.bazaarItems.itemsInRow,
    listWidth: getScreenWidth(state),
    isLoading,
    isLoadingMore,
    isLoadingSame,
    loaderInfo: getLoaderInfo(state),
    formData: state.bazaar.bazaarItems.formData
  }

  return stateProps
}

const mapDispatchToProps: IStateActions = {
  setActiveViewUniqueIdAction: setActiveViewUniqueId,
  bazaarItemInfoRequestAction: bazaarItemInfoRequest,
  bazaarItemsMoreRequestAction: bazaarItemsMoreRequest,
  removeItemAction: removeItem,
  updateItemAmountAction: updateItemAmount,
  updateFormDataAction: updateFormData,
  onImgBarFocusAction: itemImgBarFocus,
  changeActiveBuyTypeAction: changeActiveBuyType
}

export default connect(mapStateToProps, mapDispatchToProps)(Items)
