import React, { PureComponent } from 'react'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import { numberToCurrency } from '../../../../utils'
import { CROSS_ICON_CONFIG } from '../../constants'
import { SVGIconGenerator } from '@torn/shared/SVG'

export class BuyConfirmation extends PureComponent<IProps> {
  _getTotalPrice = () => {
    const { buyAmount, price } = this.props
    const totalCost = buyAmount * price

    return numberToCurrency(totalCost)
  }

  _getAveragePercent = () => {
    const { price, averagePrice } = this.props

    const averageTotal = Math.round((100 / averagePrice) * price)

    return averageTotal
  }

  _onYes = () => {
    const { onYes } = this.props

    onYes()
  }

  _onNo = () => {
    const { onNo } = this.props

    onNo()
  }

  _renderAverageCosts = () => {
    const { price, averagePrice } = this.props

    if (!(averagePrice > 0 && price >= averagePrice * 3)) {
      return null
    }

    return <span className={styles.average}>This is {this._getAveragePercent()}% above market value</span>
  }

  render() {
    const { className, uniqueId, name, buyAmount } = this.props

    const buyConfirmationMsgId = `buy-confirmation-msg-${uniqueId}`

    return (
      <div className={cn(styles.buyConfirmation, className)}>
        <button className={styles.close} onClick={this._onNo} aria-label="Close" autoFocus={true}>
          <SVGIconGenerator {...CROSS_ICON_CONFIG} />
        </button>
        <p className={styles.message} id={buyConfirmationMsgId}>
          <span className={styles.summary}>
            <span className={styles.name}>
              Buy {buyAmount} x {name}&nbsp;
            </span>
            <span className={styles.totalPrice}>
              for&nbsp;<span className={styles.sum}>${this._getTotalPrice()}</span>?
            </span>
          </span>
          {this._renderAverageCosts()}
        </p>
        <div className={styles.control}>
          <button
            className={styles.button}
            onClick={this._onYes}
            aria-labelledby={buyConfirmationMsgId}
            aria-label="Yes"
          >
            Yes
          </button>
          <button className={cn(styles.button, styles.gray)} aria-label="No" onClick={this._onNo}>
            No
          </button>
        </div>
      </div>
    )
  }
}

export default BuyConfirmation
