import React from 'react'
import { shallow } from 'enzyme'

import BuyConfirmation from '../'
import { initialState, classNameState, lessAveragePriceState } from './mocks'

describe('<BuyConfirmation />', () => {
  it('should render', () => {
    const Component = shallow<BuyConfirmation>(<BuyConfirmation {...initialState} />)
    const component = Component.instance()

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyConfirmation')
    const BuyConfirmationChildren = Component.children()
    expect(BuyConfirmationChildren.length).toBe(3)
    expect(BuyConfirmationChildren.at(0).prop('className')).toBe('close')
    expect(BuyConfirmationChildren.at(0).prop('onClick')).toBe(component._onNo)
    expect(BuyConfirmationChildren.at(0).prop('autoFocus')).toBeTruthy()
    expect(BuyConfirmationChildren.at(1).prop('id')).toBe('buy-confirmation-msg-677788-7677')
    const BuyConfirmationMsgChildren = BuyConfirmationChildren.at(1).children()
    expect(BuyConfirmationMsgChildren.length).toBe(1)
    const SummaryComponent = BuyConfirmationMsgChildren.at(0)
    expect(SummaryComponent.at(0).prop('className')).toBe('summary')
    expect(SummaryComponent.at(0).text()).toEqual('Buy 8000 x Lead Pipe\u00a0for\u00a0$16,000,000?')
    const ControlComponent = BuyConfirmationChildren.at(2)
    expect(ControlComponent.prop('className')).toBe('control')
    const ControlChildren = ControlComponent.children()
    expect(ControlChildren.length).toBe(2)
    expect(ControlChildren.at(0).prop('className')).toBe('button')
    expect(ControlChildren.at(0).prop('onClick')).toBe(component._onYes)
    expect(ControlChildren.at(0).text()).toBe('Yes')
    expect(ControlChildren.at(1).prop('className')).toBe('button gray')
    expect(ControlChildren.at(1).prop('onClick')).toBe(component._onNo)
    expect(ControlChildren.at(1).text()).toBe('No')

    expect(Component).toMatchSnapshot()
  })

  it('should render className extend', () => {
    const Component = shallow<BuyConfirmation>(<BuyConfirmation {...classNameState} />)
    const component = Component.instance()

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyConfirmation testClassName')
    const BuyConfirmationChildren = Component.children()
    expect(BuyConfirmationChildren.length).toBe(3)
    expect(BuyConfirmationChildren.at(0).prop('className')).toBe('close')
    expect(BuyConfirmationChildren.at(0).prop('onClick')).toBe(component._onNo)
    expect(BuyConfirmationChildren.at(0).prop('autoFocus')).toBeTruthy()
    expect(BuyConfirmationChildren.at(1).prop('id')).toBe('buy-confirmation-msg-677788-7677')
    const BuyConfirmationMsgChildren = BuyConfirmationChildren.at(1).children()
    expect(BuyConfirmationMsgChildren.length).toBe(1)
    const SummaryComponent = BuyConfirmationMsgChildren.at(0)
    expect(SummaryComponent.at(0).prop('className')).toBe('summary')
    expect(SummaryComponent.at(0).text()).toEqual('Buy 8000 x Lead Pipe\u00a0for\u00a0$16,000,000?')
    const ControlComponent = BuyConfirmationChildren.at(2)
    expect(ControlComponent.prop('className')).toBe('control')
    const ControlChildren = ControlComponent.children()
    expect(ControlChildren.length).toBe(2)
    expect(ControlChildren.at(0).prop('className')).toBe('button')
    expect(ControlChildren.at(0).prop('onClick')).toBe(component._onYes)
    expect(ControlChildren.at(0).text()).toBe('Yes')
    expect(ControlChildren.at(1).prop('className')).toBe('button gray')
    expect(ControlChildren.at(1).prop('onClick')).toBe(component._onNo)
    expect(ControlChildren.at(1).text()).toBe('No')

    expect(Component).toMatchSnapshot()
  })

  it('should render on less average price', () => {
    const Component = shallow<BuyConfirmation>(<BuyConfirmation {...lessAveragePriceState} />)
    const component = Component.instance()

    expect(Component.length).toBe(1)
    expect(Component.prop('className')).toBe('buyConfirmation')
    const BuyConfirmationChildren = Component.children()
    expect(BuyConfirmationChildren.length).toBe(3)
    expect(BuyConfirmationChildren.at(0).prop('className')).toBe('close')
    expect(BuyConfirmationChildren.at(0).prop('onClick')).toBe(component._onNo)
    expect(BuyConfirmationChildren.at(0).prop('autoFocus')).toBeTruthy()
    expect(BuyConfirmationChildren.at(1).prop('id')).toBe('buy-confirmation-msg-677788-7677')
    const SummaryComponent = BuyConfirmationChildren.at(1)
      .children()
      .at(0)
    expect(SummaryComponent.at(0).prop('className')).toBe('summary')
    expect(SummaryComponent.at(0).text()).toEqual('Buy 8000 x Lead Pipe\u00a0for\u00a0$16,000,000?')
    const AverageComponent = BuyConfirmationChildren.at(1)
      .children()
      .at(1)
    expect(AverageComponent.prop('className')).toBe('average')
    expect(AverageComponent.text()).toEqual('This is 2000% above market value')
    const ControlComponent = BuyConfirmationChildren.at(2)
    expect(ControlComponent.prop('className')).toBe('control')
    const ControlChildren = ControlComponent.children()
    expect(ControlChildren.length).toBe(2)
    expect(ControlChildren.at(0).prop('className')).toBe('button')
    expect(ControlChildren.at(0).prop('onClick')).toBe(component._onYes)
    expect(ControlChildren.at(0).text()).toBe('Yes')
    expect(ControlChildren.at(1).prop('className')).toBe('button gray')
    expect(ControlChildren.at(1).prop('onClick')).toBe(component._onNo)
    expect(ControlChildren.at(1).text()).toBe('No')

    expect(Component).toMatchSnapshot()
  })

  it('should handle click on close button', () => {
    const onNo = jest.fn()
    const event = {}
    const state = { ...initialState, onNo }
    const Component = shallow<BuyConfirmation>(<BuyConfirmation {...state} />)

    Component.find('button')
      .at(0)
      .simulate('click', event)

    expect(onNo).toHaveBeenCalledTimes(1)

    expect(onNo).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle click on yes button', () => {
    const onYes = jest.fn()
    const event = {}
    const state = { ...initialState, onYes }
    const Component = shallow<BuyConfirmation>(<BuyConfirmation {...state} />)

    Component.find('button')
      .at(1)
      .simulate('click', event)

    expect(onYes).toHaveBeenCalledTimes(1)

    expect(onYes).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should handle click on no button', () => {
    const onNo = jest.fn()
    const event = {}
    const state = { ...initialState, onNo }
    const Component = shallow<BuyConfirmation>(<BuyConfirmation {...state} />)

    Component.find('button')
      .at(2)
      .simulate('click', event)

    expect(onNo).toHaveBeenCalledTimes(1)

    expect(onNo).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
