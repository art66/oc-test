import { IProps } from '../../types'

export const initialState: IProps = {
  className: '',
  uniqueId: '677788-7677',
  name: 'Lead Pipe',
  price: 2000,
  buyAmount: 8000,
  averagePrice: 1500,
  onYes() {},
  onNo() {}
}

export const classNameState: IProps = {
  ...initialState,
  className: 'testClassName'
}

export const lessAveragePriceState: IProps = {
  ...initialState,
  averagePrice: 100
}
