export interface IProps {
  className?: string
  uniqueId: string
  name: string
  price: number
  buyAmount: number
  averagePrice: number
  onYes: () => void
  onNo: () => void
}
