import React, { PureComponent } from 'react'
import cn from 'classnames'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

export class Description extends PureComponent<IProps> {
  _getHtmlSettings = () => {
    const { children } = this.props

    return { __html: children }
  }

  render() {
    const { children } = this.props

    if (!children) {
      return null
    }

    return <div className={cn(styles.description, 'unreset')} dangerouslySetInnerHTML={this._getHtmlSettings()} />
    // FIXME: These styles should be in cssmodule
  }
}

export default Description
