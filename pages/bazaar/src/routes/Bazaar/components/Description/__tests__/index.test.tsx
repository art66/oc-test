import React from 'react'
import { shallow } from 'enzyme'

import Description from '../'
import { initialState } from './mocks'

describe('<Description />', () => {
  it('should render', () => {
    const Component = shallow(<Description {...initialState} />)

    expect(Component.find('.description').length).toBe(1)
    expect(
      Component.find('.description')
        .render()
        .html()
    ).toBe('<h1>Test content</h1>')

    expect(Component).toMatchSnapshot()
  })

  it('should not render', () => {
    const Component = shallow(<Description />)

    expect(Component.html()).toBeNull()

    expect(Component).toMatchSnapshot()
  })
})
