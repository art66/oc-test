import {
  setActiveViewUniqueId,
  openBazaar,
  closeBazaar,
  openBazaarRequest,
  openBazaarSuccess,
  openBazaarFailure,
  closeBazaarRequest,
  closeBazaarSuccess,
  closeBazaarFailure,
  sortByChange,
  searchChange,
  orderByChange,
  bazaarItemsRequest,
  bazaarItemsSuccess,
  bazaarItemsFailure,
  bazaarItemsMoreRequest,
  bazaarItemsMoreSuccess,
  bazaarItemsMoreFailure,
  bazaarItemsSameRequest,
  bazaarItemsSameSuccess,
  bazaarItemsSameFailure,
  removeItem,
  updateItemAmount,
  initBazaar,
  initedBazaar,
  setRowsData
} from '../actions'
import {
  SET_ACTIVE_VIEW_ID,
  OPEN_BAZAAR,
  CLOSE_BAZAAR,
  OPEN_BAZAAR_REQUEST,
  OPEN_BAZAAR_SUCCESS,
  OPEN_BAZAAR_FAILURE,
  CLOSE_BAZAAR_REQUEST,
  CLOSE_BAZAAR_SUCCESS,
  CLOSE_BAZAAR_FAILURE,
  SORT_BY_CHANGE,
  ORDER_BY_CHANGE,
  SEARCH_CHANGE,
  BAZAAR_ITEMS_REQUEST,
  BAZAAR_ITEMS_FAILURE,
  BAZAAR_ITEMS_MORE_REQUEST,
  BAZAAR_ITEMS_MORE_SUCCESS,
  BAZAAR_ITEMS_MORE_FAILURE,
  BAZAAR_ITEMS_SUCCESS,
  BAZAAR_ITEMS_SAME_REQUEST,
  BAZAAR_ITEMS_SAME_FAILURE,
  BAZAAR_ITEMS_SAME_SUCCESS,
  UPDATE_ITEM_AMOUNT,
  INIT_BAZAAR,
  INITED_BAZAAR,
  SET_ROWS_DATA,
  REMOVE_ITEM
} from '../constants'
import { TSortBy, TOrderBy } from '../types/state'

describe('Bazaar items actions', () => {
  it('should handle initBazaar action', () => {
    const expectedAction = {
      type: INIT_BAZAAR
    }

    expect(initBazaar()).toEqual(expectedAction)
  })

  it('should handle initedBazaar action', () => {
    const expectedAction = {
      type: INITED_BAZAAR
    }

    expect(initedBazaar()).toEqual(expectedAction)
  })

  it('should handle openBazaar action', () => {
    const expectedAction = {
      type: OPEN_BAZAAR
    }

    expect(openBazaar()).toEqual(expectedAction)
  })

  it('should handle closeBazaar action', () => {
    const expectedAction = {
      type: CLOSE_BAZAAR
    }

    expect(closeBazaar()).toEqual(expectedAction)
  })

  it('should handle openBazaarRequest action', () => {
    const expectedAction = {
      type: OPEN_BAZAAR_REQUEST
    }

    expect(openBazaarRequest()).toEqual(expectedAction)
  })

  it('should handle openBazaarSuccess action', () => {
    const expectedAction = {
      type: OPEN_BAZAAR_SUCCESS
    }

    expect(openBazaarSuccess()).toEqual(expectedAction)
  })

  it('should handle openBazaarFailure action', () => {
    const expectedAction = {
      type: OPEN_BAZAAR_FAILURE
    }

    expect(openBazaarFailure()).toEqual(expectedAction)
  })

  it('should handle closeBazaarRequest action', () => {
    const expectedAction = {
      type: CLOSE_BAZAAR_REQUEST
    }

    expect(closeBazaarRequest()).toEqual(expectedAction)
  })

  it('should handle closeBazaarSuccess action', () => {
    const expectedAction = {
      type: CLOSE_BAZAAR_SUCCESS
    }

    expect(closeBazaarSuccess()).toEqual(expectedAction)
  })

  it('should handle closeBazaarFailure action', () => {
    const expectedAction = {
      type: CLOSE_BAZAAR_FAILURE
    }

    expect(closeBazaarFailure()).toEqual(expectedAction)
  })

  it('should handle sortByChange action', () => {
    const sortBy = 'primary' as TSortBy
    const expectedAction = {
      type: SORT_BY_CHANGE,
      sortBy
    }

    expect(sortByChange(sortBy)).toEqual(expectedAction)
  })

  it('should handle searchChange action', () => {
    const search = 'a search string'
    const expectedAction = {
      type: SEARCH_CHANGE,
      search
    }

    expect(searchChange(search)).toEqual(expectedAction)
  })

  it('should handle orderByChange action', () => {
    const orderBy = 'desc' as TOrderBy
    const expectedAction = {
      type: ORDER_BY_CHANGE,
      orderBy
    }

    expect(orderByChange(orderBy)).toEqual(expectedAction)
  })

  it('should handle bazaarItemsRequest action', () => {
    const expectedAction = {
      type: BAZAAR_ITEMS_REQUEST
    }

    expect(bazaarItemsRequest()).toEqual(expectedAction)
  })

  it('should handle bazaarItemsRequest action', () => {
    const expectedAction = {
      type: BAZAAR_ITEMS_REQUEST
    }

    expect(bazaarItemsRequest()).toEqual(expectedAction)
  })

  it('should handle bazaarItemsSuccess action', () => {
    const payload = { list: [], total: 40, amount: 25 }
    const expectedAction = {
      type: BAZAAR_ITEMS_SUCCESS,
      ...payload
    }

    expect(bazaarItemsSuccess(payload)).toEqual(expectedAction)
  })

  it('should handle bazaarItemsFailure action', () => {
    const expectedAction = {
      type: BAZAAR_ITEMS_FAILURE
    }

    expect(bazaarItemsFailure()).toEqual(expectedAction)
  })

  it('should handle bazaarItemsMoreRequest action', () => {
    const expectedAction = {
      type: BAZAAR_ITEMS_MORE_REQUEST
    }

    expect(bazaarItemsMoreRequest()).toEqual(expectedAction)
  })

  it('should handle bazaarItemsMoreSuccess action', () => {
    const payload = { list: [], total: 40, amount: 25 }
    const expectedAction = {
      type: BAZAAR_ITEMS_MORE_SUCCESS,
      ...payload
    }

    expect(bazaarItemsMoreSuccess(expectedAction)).toEqual(expectedAction)
  })

  it('should handle bazaarItemsMoreFailure action', () => {
    const expectedAction = {
      type: BAZAAR_ITEMS_MORE_FAILURE
    }

    expect(bazaarItemsMoreFailure()).toEqual(expectedAction)
  })

  it('should handle bazaarItemsSameRequest action', () => {
    const expectedAction = {
      type: BAZAAR_ITEMS_SAME_REQUEST
    }

    expect(bazaarItemsSameRequest()).toEqual(expectedAction)
  })

  it('should handle bazaarItemsSameSuccess action', () => {
    const payload = { list: [], total: 40, amount: 25 }
    const expectedAction = {
      type: BAZAAR_ITEMS_SAME_SUCCESS,
      ...payload
    }

    expect(bazaarItemsSameSuccess(payload)).toEqual(expectedAction)
  })

  it('should handle bazaarItemsSameFailure action', () => {
    const expectedAction = {
      type: BAZAAR_ITEMS_SAME_FAILURE
    }

    expect(bazaarItemsSameFailure()).toEqual(expectedAction)
  })

  it('should handle setActiveViewUniqueId action', () => {
    const uniqueId = '4635556'
    const expectedAction = {
      type: SET_ACTIVE_VIEW_ID,
      uniqueId
    }

    expect(setActiveViewUniqueId(uniqueId)).toEqual(expectedAction)
  })

  it('should handle removeItem action', () => {
    const uniqueId = '4635556'
    const expectedAction = {
      type: REMOVE_ITEM,
      uniqueId
    }

    expect(removeItem(uniqueId)).toEqual(expectedAction)
  })

  it('should handle updateItemAmount action', () => {
    const uniqueId = '4635556'
    const amount = 635
    const expectedAction = {
      type: UPDATE_ITEM_AMOUNT,
      uniqueId,
      amount
    }

    expect(updateItemAmount(uniqueId, amount)).toEqual(expectedAction)
  })

  it('should handle setRowsData action', () => {
    const rowsData = {
      rows: [],
      totalRows: 0,
      itemsInRow: 3
    }
    const expectedAction = {
      type: SET_ROWS_DATA,
      rows: [],
      totalRows: 0,
      itemsInRow: 3
    }

    expect(setRowsData(rowsData)).toEqual(expectedAction)
  })
})
