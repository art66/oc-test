import { runSaga } from 'redux-saga'
import fetchMock from 'fetch-mock'
import bazaarItemsRequestSaga, {
  onBazaarItemsRequestSaga,
  onBazaarItemsRequestMoreSaga,
  onBazaarItemsSortByChangedSaga,
  onBazaarItemsSearchChangedSaga,
  onBazaarItemsOrderByChangedSaga
} from '../sagas/bazaarItemsRequest'
import { globalInitialState, globalInitialLoadableState, globalInitialWithViewState } from './mocks/state'
import { itemsData, expectedItemsData, itemsDataWithError } from './mocks/items'
import {
  bazaarItemsSuccess,
  bazaarItemsFailure,
  bazaarItemsMoreSuccess,
  bazaarItemsMoreFailure,
  bazaarItemsSameSuccess,
  bazaarItemsSameFailure,
  unsetActiveViewUniqueId,
  bazaarItemsSameUpdateRequest,
  bazaarItemsUpdateRequest
} from '../actions'
import { BAZAAR_ITEMS_URL } from './mocks/fetchMockUrls'
import { showInfoBox } from '../../../../modules/actions'

describe('Bazaar bazaarItemsRequestSaga sagas', () => {
  afterEach(fetchMock.reset)

  it('Should handle bazaarItemsRequestSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    const result = await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      bazaarItemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([])

    expect(result).toEqual(expectedItemsData)
  })

  it('Should handle bazaarItemsRequestSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsDataWithError, {
      overwriteRoutes: true
    })

    try {
      await runSaga(
        {
          dispatch: action => dispatched.push(action),
          getState: () => globalInitialState
        },
        bazaarItemsRequestSaga
      ).toPromise()

      expect(false).toBeTruthy()
    } catch (error) {
      expect(error).toEqual('Test error')

      expect(dispatched).toEqual([showInfoBox({ msg: 'Test error', color: 'red' })])
    }
  })

  it('Should handle bazaarItemsRequestSaga failure', async () => {
    const dispatched = []

    fetchMock.get(
      BAZAAR_ITEMS_URL,
      () => {
        throw 'Error message'
      },
      {
        overwriteRoutes: true
      }
    )

    try {
      await runSaga(
        {
          dispatch: action => dispatched.push(action),
          getState: () => globalInitialState
        },
        bazaarItemsRequestSaga
      ).toPromise()

      expect(false).toBeTruthy()
    } catch (error) {
      expect(error).toEqual('Error message')

      expect(dispatched).toEqual([showInfoBox({ msg: 'Error message', color: 'red' })])
    }
  })

  it('Should handle onBazaarItemsRequestSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsRequestSaga with unset active view successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialWithViewState
      },
      onBazaarItemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([unsetActiveViewUniqueId(), bazaarItemsSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsRequestSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsDataWithError, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Test error', color: 'red' }), bazaarItemsFailure()])
  })

  it('Should handle onBazaarItemsRequestSaga failure', async () => {
    const dispatched = []

    fetchMock.get(
      BAZAAR_ITEMS_URL,
      () => {
        throw 'Error message'
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Error message', color: 'red' }), bazaarItemsFailure()])
  })

  it('Should handle onBazaarItemsRequestMoreSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsRequestMoreSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsMoreSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsRequestMoreSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsDataWithError, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsRequestMoreSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Test error', color: 'red' }), bazaarItemsMoreFailure()])
  })

  it('Should handle onBazaarItemsRequestMoreSaga failure', async () => {
    const dispatched = []

    fetchMock.get(
      BAZAAR_ITEMS_URL,
      () => {
        throw 'Error message'
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsRequestMoreSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Error message', color: 'red' }), bazaarItemsMoreFailure()])
  })

  it('Should handle onBazaarItemsSortByChangedSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsSortByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsUpdateRequest(), bazaarItemsSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsSortByChangedSaga with deactivate view successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialWithViewState
      },
      onBazaarItemsSortByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([
      unsetActiveViewUniqueId(),
      bazaarItemsUpdateRequest(),
      bazaarItemsSuccess(expectedItemsData)
    ])
  })

  it('Should handle onBazaarItemsSortByChangedSaga with delay', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialLoadableState
      },
      onBazaarItemsSortByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsUpdateRequest(), bazaarItemsSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsSortByChangedSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsDataWithError, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsSortByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([
      bazaarItemsUpdateRequest(),
      showInfoBox({ msg: 'Test error', color: 'red' }),
      bazaarItemsFailure()
    ])
  })

  it('Should handle onBazaarItemsSortByChangedSaga failure', async () => {
    const dispatched = []

    fetchMock.get(
      BAZAAR_ITEMS_URL,
      () => {
        throw 'Error message'
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsSortByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([
      bazaarItemsUpdateRequest(),
      showInfoBox({ msg: 'Error message', color: 'red' }),
      bazaarItemsFailure()
    ])
  })

  it('Should handle onBazaarItemsSearchChangedSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsSearchChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsSameUpdateRequest(), bazaarItemsSameSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsSearchChangedSaga with delay', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialLoadableState
      },
      onBazaarItemsSearchChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsSameUpdateRequest(), bazaarItemsSameSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsSearchChangedSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsDataWithError, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsSearchChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([
      bazaarItemsSameUpdateRequest(),
      showInfoBox({ msg: 'Test error', color: 'red' }),
      bazaarItemsSameFailure()
    ])
  })

  it('Should handle onBazaarItemsSearchChangedSaga failure', async () => {
    const dispatched = []

    fetchMock.get(
      BAZAAR_ITEMS_URL,
      () => {
        throw 'Error message'
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsSearchChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([
      bazaarItemsSameUpdateRequest(),
      showInfoBox({ msg: 'Error message', color: 'red' }),
      bazaarItemsSameFailure()
    ])
  })

  it('Should handle onBazaarItemsOrderByChangedSaga with delay successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialLoadableState
      },
      onBazaarItemsOrderByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsSameUpdateRequest(), bazaarItemsSameSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsOrderByChangedSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsOrderByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([bazaarItemsSameUpdateRequest(), bazaarItemsSameSuccess(expectedItemsData)])
  })

  it('Should handle onBazaarItemsOrderByChangedSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(BAZAAR_ITEMS_URL, itemsDataWithError, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsOrderByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([
      bazaarItemsSameUpdateRequest(),
      showInfoBox({ msg: 'Test error', color: 'red' }),
      bazaarItemsSameFailure()
    ])
  })

  it('Should handle onBazaarItemsOrderByChangedSaga failure', async () => {
    const dispatched = []

    fetchMock.get(
      BAZAAR_ITEMS_URL,
      () => {
        throw 'Error message'
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      onBazaarItemsOrderByChangedSaga
    ).toPromise()

    expect(dispatched).toEqual([
      bazaarItemsSameUpdateRequest(),
      showInfoBox({ msg: 'Error message', color: 'red' }),
      bazaarItemsSameFailure()
    ])
  })
})
