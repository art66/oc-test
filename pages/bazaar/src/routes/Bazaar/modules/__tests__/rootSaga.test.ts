import rootSaga from '../saga'
import { takeLatest, debounce } from 'redux-saga/effects'
import {
  openBazaarRequestSaga,
  closeBazaarRequestSaga,
  onBazaarItemsRequestSaga,
  onBazaarItemsRequestMoreSaga,
  onBazaarItemsSortByChangedSaga,
  onBazaarItemsSearchChangedSaga,
  onBazaarItemsOrderByChangedSaga,
  onBazaarItemsRequestSameSaga,
  normalizeItemsSaga
} from '../sagas'
import {
  OPEN_BAZAAR,
  CLOSE_BAZAAR,
  BAZAAR_ITEMS_REQUEST,
  BAZAAR_ITEMS_MORE_REQUEST,
  SORT_BY_CHANGE,
  SEARCH_CHANGE,
  ORDER_BY_CHANGE,
  SET_ACTIVE_VIEW_ID,
  INIT_BAZAAR,
  BAZAAR_ITEMS_SAME_REQUEST,
  UPDATE_ITEM_AMOUNT,
  UNSET_ACTIVE_VIEW_ID,
  BAZAAR_ITEMS_SAME_FAILURE,
  BAZAAR_ITEMS_SAME_SUCCESS,
  BAZAAR_ITEMS_MORE_FAILURE,
  BAZAAR_ITEMS_MORE_SUCCESS,
  BAZAAR_ITEMS_FAILURE,
  BAZAAR_ITEMS_SUCCESS,
  REMOVE_ITEM
} from '../constants'
import bazaarItemInfoRequestSaga from '../../../../modules/sagas/bazaarItemInfoRequest'
import { BAZAAR_PAGE_ID } from '../../../../constants'
import setPageIdSaga from '../../../../modules/sagas/setPageId'
import { initBazaarItemsSaga } from '../sagas/initSaga'

it('The root saga should react to actions', () => {
  const generator = rootSaga()

  expect(generator.next().value).toEqual(takeLatest(INIT_BAZAAR, initBazaarItemsSaga))

  expect(generator.next().value).toEqual(takeLatest(INIT_BAZAAR, setPageIdSaga, BAZAAR_PAGE_ID))

  expect(generator.next().value).toEqual(debounce(100, OPEN_BAZAAR, openBazaarRequestSaga))

  expect(generator.next().value).toEqual(debounce(100, CLOSE_BAZAAR, closeBazaarRequestSaga))

  expect(generator.next().value).toEqual(takeLatest(BAZAAR_ITEMS_REQUEST, onBazaarItemsRequestSaga))

  expect(generator.next().value).toEqual(takeLatest(BAZAAR_ITEMS_MORE_REQUEST, onBazaarItemsRequestMoreSaga))

  expect(generator.next().value).toEqual(takeLatest(BAZAAR_ITEMS_SAME_REQUEST, onBazaarItemsRequestSameSaga))

  expect(generator.next().value).toEqual(takeLatest(SORT_BY_CHANGE, onBazaarItemsSortByChangedSaga))

  expect(generator.next().value).toEqual(takeLatest(ORDER_BY_CHANGE, onBazaarItemsOrderByChangedSaga))

  expect(generator.next().value).toEqual(takeLatest(SEARCH_CHANGE, onBazaarItemsSearchChangedSaga))

  expect(generator.next().value).toEqual(takeLatest(SET_ACTIVE_VIEW_ID, bazaarItemInfoRequestSaga))

  expect(generator.next().value).toEqual(
    takeLatest(
      [
        BAZAAR_ITEMS_SUCCESS,
        BAZAAR_ITEMS_FAILURE,
        BAZAAR_ITEMS_MORE_SUCCESS,
        BAZAAR_ITEMS_MORE_FAILURE,
        BAZAAR_ITEMS_SAME_SUCCESS,
        BAZAAR_ITEMS_SAME_FAILURE,
        SET_ACTIVE_VIEW_ID,
        UNSET_ACTIVE_VIEW_ID,
        REMOVE_ITEM,
        UPDATE_ITEM_AMOUNT,
        'redux-responsive/CALCULATE_RESPONSIVE_STATE'
      ],
      normalizeItemsSaga
    )
  )

  expect(generator.next().done).toBe(true)
})
