import fetchMock from 'fetch-mock'
import { runSaga } from 'redux-saga'
import openBazaarRequestSaga from '../sagas/openBazaarRequest'
import { globalInitialState } from './mocks/state'
import { OPEN_BAZAAR_REQUEST_URL } from '../constants'
import { openBazaarRequest, openBazaarSuccess, openBazaarFailure } from '../actions'
import { setRequestInfoBox } from '../../../../modules/actions'

describe('Bazaar modules openBazaarRequestSaga saga', () => {
  afterEach(fetchMock.reset)

  it('Should handle successfully', async () => {
    const dispatched = []

    fetchMock.get(OPEN_BAZAAR_REQUEST_URL, { text: 'Test message' }, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      openBazaarRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      setRequestInfoBox({ loading: true }),
      openBazaarRequest(),
      openBazaarSuccess(),
      setRequestInfoBox({ loading: false, msg: 'Test message', color: 'green' })
    ])
  })

  it('Should handle failure with registered error', async () => {
    const dispatched = []

    fetchMock.get(
      OPEN_BAZAAR_REQUEST_URL,
      { error: 'Test registered error' },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      openBazaarRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      setRequestInfoBox({ loading: true }),
      openBazaarRequest(),
      openBazaarFailure(),
      setRequestInfoBox({ loading: false, msg: 'Test registered error', color: 'red' })
    ])
  })

  it('Should handle failure', async () => {
    const dispatched = []

    fetchMock.get(
      OPEN_BAZAAR_REQUEST_URL,
      () => {
        throw 'Failure error'
      },
      { overwriteRoutes: true }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      openBazaarRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      setRequestInfoBox({ loading: true }),
      openBazaarRequest(),
      openBazaarFailure(),
      setRequestInfoBox({ loading: false, msg: 'Failure error', color: 'red' })
    ])
  })
})
