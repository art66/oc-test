import fetchMock from 'fetch-mock'
import { runSaga } from 'redux-saga'
import closeBazaarRequestSaga from '../sagas/closeBazaarRequest'
import { globalInitialState } from './mocks/state'
import { CLOSE_BAZAAR_REQUEST_URL } from '../constants'
import { closeBazaarRequest, closeBazaarSuccess, closeBazaarFailure } from '../actions'
import { setRequestInfoBox } from '../../../../modules/actions'

describe('Bazaar modules closeBazaarRequestSaga sagas', () => {
  afterEach(fetchMock.reset)

  it('Should handle successfully', async () => {
    const dispatched = []

    fetchMock.get(CLOSE_BAZAAR_REQUEST_URL, { text: 'Closed' }, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      closeBazaarRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      setRequestInfoBox({ loading: true }),
      closeBazaarRequest(),
      closeBazaarSuccess(),
      setRequestInfoBox({ loading: false, msg: 'Closed', color: 'green' })
    ])
  })

  it('Should handle failure with registered error', async () => {
    const dispatched = []

    fetchMock.get(
      CLOSE_BAZAAR_REQUEST_URL,
      { error: 'Test registered error' },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      closeBazaarRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      setRequestInfoBox({ loading: true }),
      closeBazaarRequest(),
      closeBazaarFailure(),
      setRequestInfoBox({ loading: false, msg: 'Test registered error', color: 'red' })
    ])
  })

  it('Should handle failure', async () => {
    const dispatched = []

    fetchMock.get(
      CLOSE_BAZAAR_REQUEST_URL,
      () => {
        throw 'Failure error'
      },
      { overwriteRoutes: true }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalInitialState
      },
      closeBazaarRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      setRequestInfoBox({ loading: true }),
      closeBazaarRequest(),
      closeBazaarFailure(),
      setRequestInfoBox({ loading: false, msg: 'Failure error', color: 'red' })
    ])
  })
})
