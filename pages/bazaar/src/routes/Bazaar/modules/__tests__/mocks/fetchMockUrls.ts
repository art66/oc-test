import { createBazaarItemsUrl } from '../../urls'
import { ASC_ORDER, SORT_BY_DEFAULT_VALUE } from '../../../constants'

export const BAZAAR_ITEMS_URL = createBazaarItemsUrl({
  loadFrom: 5,
  userId: '2265266',
  sortBy: SORT_BY_DEFAULT_VALUE,
  orderBy: ASC_ORDER,
  limit: 50,
  search: 'search string'
})
