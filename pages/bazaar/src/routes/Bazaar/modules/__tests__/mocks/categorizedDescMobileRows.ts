import { IRow } from '../../../components/Items/types'

export const categorizedDescMobileRows: IRow[] = [
  {
    categoryName: 'Temporary',
    rowItems: [
      {
        uniqueId: '394-0',
        itemId: '394',
        amount: 4,
        name: 'Brick',
        price: 8000,
        imgLargeUrl: '/images/items/394/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/394/medium.png',
        imgBlankUrl: '/images/items/394/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Temporary',
        bazaarId: '28151330',
        averagePrice: 505,
        minPrice: 1,
        glow: null,
        damageBonus: '48.12',
        accuracyBonus: '43.00',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    categoryName: 'Secondary',
    rowItems: [
      {
        uniqueId: '12-3591872967',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872967',
        categoryName: 'Secondary',
        bazaarId: '28151303',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.72',
        accuracyBonus: '53.97',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872968',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872968',
        categoryName: 'Secondary',
        bazaarId: '28151304',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.86',
        accuracyBonus: '55.84',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872969',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872969',
        categoryName: 'Secondary',
        bazaarId: '28151305',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.62',
        accuracyBonus: '53.38',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872971',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872971',
        categoryName: 'Secondary',
        bazaarId: '28151306',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.66',
        accuracyBonus: '53.61',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872972',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872972',
        categoryName: 'Secondary',
        bazaarId: '28151307',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.73',
        accuracyBonus: '53.14',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872973',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872973',
        categoryName: 'Secondary',
        bazaarId: '28151308',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.93',
        accuracyBonus: '53.03',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872974',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872974',
        categoryName: 'Secondary',
        bazaarId: '28151309',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.62',
        accuracyBonus: '54.57',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872975',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872975',
        categoryName: 'Secondary',
        bazaarId: '28151310',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.79',
        accuracyBonus: '54.80',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872976',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872976',
        categoryName: 'Secondary',
        bazaarId: '28151311',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.85',
        accuracyBonus: '53.45',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872977',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872977',
        categoryName: 'Secondary',
        bazaarId: '28151312',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.86',
        accuracyBonus: '53.32',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872978',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872978',
        categoryName: 'Secondary',
        bazaarId: '28151313',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.62',
        accuracyBonus: '54.75',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872979',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872979',
        categoryName: 'Secondary',
        bazaarId: '28151314',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.67',
        accuracyBonus: '54.01',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872980',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872980',
        categoryName: 'Secondary',
        bazaarId: '28151315',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.69',
        accuracyBonus: '53.26',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872981',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872981',
        categoryName: 'Secondary',
        bazaarId: '28151316',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.83',
        accuracyBonus: '53.35',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872982',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872982',
        categoryName: 'Secondary',
        bazaarId: '28151317',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.66',
        accuracyBonus: '54.55',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872983',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872983',
        categoryName: 'Secondary',
        bazaarId: '28151318',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.62',
        accuracyBonus: '54.04',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872984',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872984',
        categoryName: 'Secondary',
        bazaarId: '28151319',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.82',
        accuracyBonus: '54.16',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872985',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872985',
        categoryName: 'Secondary',
        bazaarId: '28151320',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.68',
        accuracyBonus: '53.31',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872986',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872986',
        categoryName: 'Secondary',
        bazaarId: '28151321',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.75',
        accuracyBonus: '53.11',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872987',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872987',
        categoryName: 'Secondary',
        bazaarId: '28151322',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.91',
        accuracyBonus: '54.07',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872988',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872988',
        categoryName: 'Secondary',
        bazaarId: '28151323',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '22.00',
        accuracyBonus: '53.11',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872989',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872989',
        categoryName: 'Secondary',
        bazaarId: '28151324',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.96',
        accuracyBonus: '53.35',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872990',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872990',
        categoryName: 'Secondary',
        bazaarId: '28151325',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.83',
        accuracyBonus: '53.48',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872991',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872991',
        categoryName: 'Secondary',
        bazaarId: '28151326',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.60',
        accuracyBonus: '54.01',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '12-3591872992',
        itemId: '12',
        amount: 1,
        name: 'Glock 17',
        price: 5000,
        imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/12/medium.png',
        imgBlankUrl: '/images/items/12/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '3591872992',
        categoryName: 'Secondary',
        bazaarId: '28151327',
        averagePrice: 253,
        minPrice: 1,
        glow: null,
        damageBonus: '21.67',
        accuracyBonus: '53.06',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    categoryName: 'Primary',
    rowItems: [
      {
        uniqueId: '388-4384562992',
        itemId: '388',
        amount: 1,
        name: 'Pink Mac-10',
        price: 99000000000,
        imgLargeUrl: '/images/items/388/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/388/medium.png',
        imgBlankUrl: '/images/items/388/blank.png',
        rarity: '1',
        isBlockedForBuying: false,
        armoryId: '4384562992',
        categoryName: 'Primary',
        bazaarId: '31307142',
        averagePrice: 0,
        minPrice: 1,
        glow: { color: 'FFBF00', opacity: 0.5 },
        damageBonus: '81.45',
        accuracyBonus: '45.26',
        armorBonus: '0.00',
        type: 'Weapon',
        bonuses: [
          {
            className: 'bonus-attachment-emasculated',
            title: '<b>Emasculate</b><br/>Happiness increased by 15% of maximum upon final hit'
          },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    categoryName: 'Medical',
    rowItems: [
      {
        uniqueId: '68-0',
        itemId: '68',
        amount: 1,
        name: 'Small First Aid Kit',
        price: 120000,
        imgLargeUrl: '/images/items/68/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/68/medium.png',
        imgBlankUrl: '/images/items/68/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Medical',
        bazaarId: '28151301',
        averagePrice: 20215,
        minPrice: 1,
        glow: null,
        damageBonus: '0.00',
        accuracyBonus: '0.00',
        armorBonus: '0.00',
        type: 'Item',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '731-0',
        itemId: '731',
        amount: 10,
        name: 'Empty Blood Bag',
        price: 150000,
        imgLargeUrl: '/images/items/731/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/731/medium.png',
        imgBlankUrl: '/images/items/731/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Medical',
        bazaarId: '28151302',
        averagePrice: 17452,
        minPrice: 1,
        glow: null,
        damageBonus: '0.00',
        accuracyBonus: '0.00',
        armorBonus: '0.00',
        type: 'Item',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    categoryName: 'Clothing',
    rowItems: [
      {
        uniqueId: '404-0',
        itemId: '404',
        amount: 1,
        name: 'Bandana',
        price: 80000,
        imgLargeUrl: '/images/items/404/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/404/medium.png',
        imgBlankUrl: '/images/items/404/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Clothing',
        bazaarId: '28151331',
        averagePrice: 970,
        minPrice: 1,
        glow: null,
        damageBonus: '0.00',
        accuracyBonus: '0.00',
        armorBonus: '0.00',
        type: 'Item',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '806-0',
        itemId: '806',
        amount: 1,
        name: 'Old Lady Mask',
        price: 90000,
        imgLargeUrl: '/images/items/806/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/806/medium.png',
        imgBlankUrl: '/images/items/806/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Clothing',
        bazaarId: '28151332',
        averagePrice: 24875,
        minPrice: 1,
        glow: null,
        damageBonus: '0.00',
        accuracyBonus: '0.00',
        armorBonus: '0.00',
        type: 'Item',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    categoryName: 'Candy',
    rowItems: [
      {
        uniqueId: '556-0',
        itemId: '556',
        amount: 5,
        name: 'Bag of Reindeer Droppings',
        price: 600000,
        imgLargeUrl: '/images/items/556/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/556/medium.png',
        imgBlankUrl: '/images/items/556/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Candy',
        bazaarId: '28151328',
        averagePrice: 155380,
        minPrice: 1,
        glow: null,
        damageBonus: '0.00',
        accuracyBonus: '0.00',
        armorBonus: '0.00',
        type: 'Item',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    rowItems: [
      {
        uniqueId: '529-0',
        itemId: '529',
        amount: 5,
        name: 'Bag of Chocolate Truffles',
        price: 800000,
        imgLargeUrl: '/images/items/529/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/529/medium.png',
        imgBlankUrl: '/images/items/529/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Candy',
        bazaarId: '28151329',
        averagePrice: 154005,
        minPrice: 1,
        glow: null,
        damageBonus: '0.00',
        accuracyBonus: '0.00',
        armorBonus: '0.00',
        type: 'Item',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  },
  {
    categoryName: 'Alcohol',
    rowItems: [
      {
        uniqueId: '541-0',
        itemId: '541',
        amount: 150,
        name: 'Bottle of Stinky Swamp Punch',
        price: 2000000,
        imgLargeUrl: '/images/items/541/large.png?v=1528808940574',
        imgMediumUrl: '/images/items/541/medium.png',
        imgBlankUrl: '/images/items/541/blank.png',
        rarity: 0,
        isBlockedForBuying: false,
        armoryId: '0',
        categoryName: 'Alcohol',
        bazaarId: '28151333',
        averagePrice: 459429,
        minPrice: 1,
        glow: null,
        damageBonus: '0.00',
        accuracyBonus: '0.00',
        armorBonus: '0.00',
        type: 'Item',
        bonuses: [
          { className: 'bonus-attachment-blank-bonus-25', title: null },
          { className: 'bonus-attachment-blank-bonus-25', title: null }
        ]
      }
    ]
  }
]

export const categorizedDescWithViewMobileRows: IRow[] = [
  ...categorizedDescMobileRows.slice(0, 13),
  {
    ...categorizedDescMobileRows[13],
    withView: true
  },
  ...categorizedDescMobileRows.slice(14)
]
