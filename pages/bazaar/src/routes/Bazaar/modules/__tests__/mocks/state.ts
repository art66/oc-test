import { IBazaarState } from '../../types/state'
import { SORT_BY_DEFAULT_VALUE, ASC_ORDER } from '../../../constants'
import { items as list } from './items'
import { IState } from '../../../../../store/types'
import { globalInitialState as defaultGlobalInitialState } from '../../../../../modules/__tests__/mocks/state'

export const initialState: IBazaarState = {
  isLoading: false,
  bazaarState: {
    isLoading: false
  },
  searchBar: {
    sortBy: SORT_BY_DEFAULT_VALUE,
    orderBy: ASC_ORDER,
    search: ''
  },
  bazaarItems: {
    limit: 60,
    total: 0,
    loadFrom: 0,
    list: [],
    rows: [],
    formData: {},
    totalRows: 0,
    itemsInRow: 3,
    isLoading: false,
    isLoadingMore: false,
    isLoadingSame: false,
    error: null
  },
  activeView: {
    uniqueId: null
  }
}

export const globalInitialState: IState = {
  ...defaultGlobalInitialState,
  bazaar: {
    ...initialState,
    searchBar: {
      ...initialState.searchBar,
      sortBy: SORT_BY_DEFAULT_VALUE,
      orderBy: ASC_ORDER,
      search: 'search string'
    },
    bazaarItems: {
      ...initialState.bazaarItems,
      limit: 50,
      loadFrom: 5,
      list
    }
  }
}

export const globalInitialLoadableState: IState = {
  ...globalInitialState,
  bazaar: {
    ...globalInitialState.bazaar,
    bazaarItems: {
      ...globalInitialState.bazaar.bazaarItems,
      isLoading: true
    }
  }
}

export const globalInitialWithViewState: IState = {
  ...globalInitialState,
  bazaar: {
    ...globalInitialState.bazaar,
    activeView: {
      uniqueId: '553-5633'
    }
  }
}

export const globalWithoutOwnerState: IState = {
  ...globalInitialState,
  common: {
    ...globalInitialState.common,
    owner: { ...globalInitialState.common.owner, data: { ...globalInitialState.common.owner.data, userId: null } }
  }
}

export const globalEmptyViewsState: IState = {
  ...globalInitialState,
  common: {
    ...globalInitialState.common,
    views: {}
  },
  bazaar: {
    ...globalInitialState.bazaar,
    activeView: {
      ...globalInitialState.bazaar.activeView,
      uniqueId: '68-0'
    }
  }
}

export const globalFulfilledViewsState: IState = {
  ...globalInitialState,
  common: {
    ...globalInitialState.common,
    views: { '68-0': { id: '68' } },
    viewsSettings: { '68-0': { height: 0, isLoading: false } }
  },
  bazaar: {
    ...globalInitialState.bazaar,
    bazaarItems: {
      ...globalInitialState.bazaar.bazaarItems
    },
    activeView: {
      ...globalInitialState.bazaar.activeView,
      uniqueId: '68-0'
    }
  }
}

export const globalDifferentViewsState: IState = {
  ...globalInitialState,
  common: {
    ...globalInitialState.common,
    views: { '68-0': { id: '68' } },
    viewsSettings: { '68-0': { height: 0, isLoading: false } }
  },
  bazaar: {
    ...globalInitialState.bazaar,
    activeView: {
      ...globalInitialState.bazaar.activeView,
      uniqueId: '68-0'
    }
  }
}
