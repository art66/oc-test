export const bazaarData = {
  totalQuantity: 34,
  name: 'New nameadas',
  owner: { userId: '2265266', playername: 'love__intent' },
  state: {
    isYours: true,
    isClosed: true,
    isBazaarExists: true,
    message: 'Your bazaar is now <span class="bold">Closed</span>.',
    color: 'green'
  },
  headerContent: {
    links: [
      { link: 'imarket.php', class: 'item', icon: 'item', title: 'Item Market' },
      { link: 'item.php', class: 'folder', icon: 'folder', title: 'Your Items' },
      { link: '#', class: 'bazaar-state-closed active', icon: 'bazaar-state-closed', title: 'Closed' },
      { link: '#', class: 'bazaar-state-open', icon: 'bazaar-state-open', title: 'Open' },
      { link: 'bazaar.php#/p=add', class: 'add-items', icon: 'add-items', title: 'Add items' },
      { link: 'bazaar.php#/p=manage', class: 'manage-items', icon: 'manage-items', title: 'Manage items' },
      { link: 'bazaar.php#/p=personalize', class: 'personalize', icon: 'personalize', title: 'Personalize' },
      { link: 'profiles.php?XID=2265266', class: 'back', icon: 'back', title: 'Your Profile' }
    ],
    title: 'New nameadas'
  },
  announcement: 'Test description',
  error: false,
  bazaarId: '2265266'
}

export const bazaarDataError = {
  ...bazaarData,
  error: 'Test error message'
}
