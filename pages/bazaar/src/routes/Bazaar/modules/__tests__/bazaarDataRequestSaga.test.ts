import { runSaga } from 'redux-saga'
import fetchMock from 'fetch-mock'

import bazaarDataRequestSaga from '../sagas/bazaarDataRequest'
import {
  metadataRequest,
  ownerRequest,
  bazaarDataRequest,
  metadataResponseSuccess,
  ownerResponseSuccess,
  bazaarDataResponseSuccess,
  // metadataResponseFailure,
  // ownerResponseFailure,
  // bazaarDataResponseFailure,
  setRequestInfoBox
} from '../../../../modules/actions'
import { createBazaarDataUrl } from '../urls'
import { bazaarData, bazaarDataError } from './mocks/bazaarData'

describe('Bazaar modules bazaarDataRequest saga', () => {
  afterEach(fetchMock.reset)

  it('Should handle with custom user successfully', async () => {
    const dispatched = []

    fetchMock.get(createBazaarDataUrl({ userId: null }), bazaarData, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: 'Test description',
        name: 'New nameadas'
      }),
      setRequestInfoBox({ color: 'green', msg: 'Your bazaar is now <span class="bold">Closed</span>.' })
    ])
  })

  it('Should handle with bazaar infobox successfully', async () => {
    const dispatched = []

    fetchMock.get(createBazaarDataUrl({ userId: null }), bazaarData, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: { msg: 'Test message' } } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: 'Test description',
        name: 'New nameadas'
      }),
      setRequestInfoBox({ color: 'green', msg: 'Your bazaar is now <span class="bold">Closed</span>.' })
    ])
  })

  it('Should handle failure with registered error', async () => {
    const dispatched = []

    fetchMock.get(createBazaarDataUrl({ userId: null }), bazaarDataError, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: 'Test description',
        name: 'New nameadas'
      }),
      // metadataResponseFailure(),
      // ownerResponseFailure(),
      // bazaarDataResponseFailure(),
      setRequestInfoBox({ color: 'red', msg: 'Test error message' })
    ])
  })

  it('Should handle when request failure', async () => {
    const dispatched = []

    fetchMock.get(
      createBazaarDataUrl({ userId: null }),
      () => {
        throw 'Error message'
      },
      { overwriteRoutes: true }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      // metadataResponseFailure(),
      // ownerResponseFailure(),
      // bazaarDataResponseFailure(),
      setRequestInfoBox({ msg: 'Error message', color: 'red' })
    ])
  })

  it('Should handle with custom user successfully', async () => {
    const dispatched = []

    window.history.pushState({}, null, '?userId=2138917#/')

    fetchMock.get(createBazaarDataUrl({ userId: 2138917 }), bazaarData, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: 'Test description',
        name: 'New nameadas'
      }),
      setRequestInfoBox({ msg: 'Your bazaar is now <span class="bold">Closed</span>.', color: 'green' })
    ])
  })

  it('Should handle successfully with custom user and bazaar infobox', async () => {
    const dispatched = []

    window.history.pushState({}, null, '?userId=2138917#/')

    fetchMock.get(createBazaarDataUrl({ userId: 2138917 }), bazaarData, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: { msg: 'Test message' } } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: 'Test description',
        name: 'New nameadas'
      }),
      setRequestInfoBox({ msg: 'Your bazaar is now <span class="bold">Closed</span>.', color: 'green' })
    ])
  })

  it('Should handle failure with custom user and registered error', async () => {
    const dispatched = []

    window.history.pushState({}, null, '?userId=2138917#/')

    fetchMock.get(createBazaarDataUrl({ userId: 2138917 }), bazaarDataError, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: 'Test description',
        name: 'New nameadas'
      }),
      // metadataResponseFailure(),
      // ownerResponseFailure(),
      // bazaarDataResponseFailure(),
      setRequestInfoBox({ color: 'red', msg: 'Test error message' })
    ])
  })

  it('Should handle failure', async () => {
    const dispatched = []

    window.history.pushState({}, null, '?userId=2138917#/')

    fetchMock.get(
      createBazaarDataUrl({ userId: 2138917 }),
      () => {
        throw 'Error message'
      },
      { overwriteRoutes: true }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      bazaarDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      // metadataResponseFailure(),
      // ownerResponseFailure(),
      // bazaarDataResponseFailure(),
      setRequestInfoBox({ msg: 'Error message', color: 'red' })
    ])
  })
})
