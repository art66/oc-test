import { ACTION_HANDLERS } from '../reducers'
import { initialState } from './mocks/state'
import { items as list, expectedItemsAfterRemove, expectedItemsAfterAmountUpdate } from './mocks/items'
import {
  SORT_BY_CHANGE,
  SEARCH_CHANGE,
  ORDER_BY_CHANGE,
  BAZAAR_ITEMS_SUCCESS,
  BAZAAR_ITEMS_MORE_SUCCESS,
  BAZAAR_ITEMS_SAME_SUCCESS,
  SET_ACTIVE_VIEW_ID,
  SET_ROWS_DATA,
  REMOVE_ITEM,
  UPDATE_ITEM_AMOUNT
  // SET_ACTIVE_VIEW_HEIGHT,
  // BAZAAR_ITEM_INFO_REQUEST,
  // BAZAAR_ITEM_INFO_SUCCESS,
  // BAZAAR_ITEM_INFO_FAILURE
} from '../constants'
import { ASC_ORDER, DESC_ORDER } from '../../constants'
import { TSortBy, TOrderBy, IBazaarState } from '../types/state'
// import { viewInfoWithExtras, viewInfoInitialState } from './mocks/viewInfo'

describe('Reducer action handlers', () => {
  it('Should INIT_BAZAAR mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      isLoading: false,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 90,
        isLoading: false,
        error: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      isLoading: true,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 0,
        isLoading: true,
        error: null
      }
    }

    expect(ACTION_HANDLERS.INIT_BAZAAR(passedState)).toEqual(expectedState)
  })

  it('Should INITED_BAZAAR mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      isLoading: true
    }
    const expectedState: IBazaarState = {
      ...initialState,
      isLoading: false
    }

    expect(ACTION_HANDLERS.INITED_BAZAAR(passedState)).toEqual(expectedState)
  })

  it('Should OPEN_BAZAAR_REQUEST mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: false
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: true
      }
    }

    expect(ACTION_HANDLERS.OPEN_BAZAAR_REQUEST(passedState)).toEqual(expectedState)
  })

  it('Should OPEN_BAZAAR_SUCCESS mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: false
      }
    }

    expect(ACTION_HANDLERS.OPEN_BAZAAR_SUCCESS(passedState)).toEqual(expectedState)
  })

  it('Should OPEN_BAZAAR_FAILURE mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: false
      }
    }

    expect(ACTION_HANDLERS.OPEN_BAZAAR_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should CLOSE_BAZAAR_REQUEST mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: false
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: true
      }
    }

    expect(ACTION_HANDLERS.CLOSE_BAZAAR_REQUEST(passedState)).toEqual(expectedState)
  })

  it('Should CLOSE_BAZAAR_SUCCESS mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: false
      }
    }

    expect(ACTION_HANDLERS.CLOSE_BAZAAR_SUCCESS(passedState)).toEqual(expectedState)
  })

  it('Should CLOSE_BAZAAR_FAILURE mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarState: {
        ...initialState.bazaarState,
        isLoading: false
      }
    }

    expect(ACTION_HANDLERS.CLOSE_BAZAAR_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should SORT_BY_CHANGE mutate state correctly', () => {
    const sortBy = 'category' as TSortBy
    const passedState: IBazaarState = {
      ...initialState,
      searchBar: {
        ...initialState.searchBar,
        sortBy: 'default' as TSortBy,
        orderBy: ASC_ORDER as TOrderBy
      }
    }
    const action = {
      type: SORT_BY_CHANGE as typeof SORT_BY_CHANGE,
      sortBy
    }
    const expectedState: IBazaarState = {
      ...initialState,
      searchBar: {
        ...initialState.searchBar,
        sortBy
      }
    }

    expect(ACTION_HANDLERS.SORT_BY_CHANGE(passedState, action)).toEqual(expectedState)
  })

  it('Should SORT_BY_CHANGE mutate state correctly when no values passed', () => {
    const sortBy = null
    const passedState: IBazaarState = {
      ...initialState,
      searchBar: {
        ...initialState.searchBar,
        sortBy: 'cost' as TSortBy,
        orderBy: DESC_ORDER as TOrderBy
      }
    }
    const action = {
      type: SORT_BY_CHANGE as typeof SORT_BY_CHANGE,
      sortBy
    }
    const expectedState: IBazaarState = {
      ...initialState,
      searchBar: {
        ...initialState.searchBar,
        sortBy: 'default',
        orderBy: ASC_ORDER
      }
    }

    expect(ACTION_HANDLERS.SORT_BY_CHANGE(passedState, action)).toEqual(expectedState)
  })

  it('Should SEARCH_CHANGE mutate state correctly', () => {
    const search = 'some search value'
    const passedState: IBazaarState = {
      ...initialState,
      searchBar: {
        ...initialState.searchBar,
        search: null
      }
    }
    const action = {
      type: SEARCH_CHANGE as typeof SEARCH_CHANGE,
      search
    }
    const expectedState: IBazaarState = {
      ...initialState,
      searchBar: {
        ...initialState.searchBar,
        search
      }
    }

    expect(ACTION_HANDLERS.SEARCH_CHANGE(passedState, action)).toEqual(expectedState)
  })

  it('Should ORDER_BY_CHANGE mutate state correctly', () => {
    const orderBy = 'DESC' as TOrderBy
    const passedState: IBazaarState = {
      ...initialState
    }
    const action = {
      type: ORDER_BY_CHANGE as typeof ORDER_BY_CHANGE,
      orderBy
    }
    const expectedState: IBazaarState = {
      ...initialState,
      searchBar: {
        ...initialState.searchBar,
        orderBy
      }
    }

    expect(ACTION_HANDLERS.ORDER_BY_CHANGE(passedState, action)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_REQUEST mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 50,
        isLoading: false,
        error: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 0,
        isLoading: true,
        error: null
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_REQUEST(passedState)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_SUCCESS mutate state correctly', () => {
    const action = {
      type: BAZAAR_ITEMS_SUCCESS as typeof BAZAAR_ITEMS_SUCCESS,
      list,
      total: 90,
      amount: 30
    }
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoading: true,
        list: list.slice(10, 18),
        total: 10,
        loadFrom: 8
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        list,
        total: 90,
        loadFrom: 30,
        isLoading: false,
        error: null
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_SUCCESS(passedState, action)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_FAILURE mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoading: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoading: false,
        error: true
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_MORE_REQUEST mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 50,
        isLoadingMore: false,
        error: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 50,
        isLoadingMore: true,
        error: null
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_MORE_REQUEST(passedState)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_MORE_SUCCESS mutate state correctly', () => {
    const action = {
      type: BAZAAR_ITEMS_MORE_SUCCESS as typeof BAZAAR_ITEMS_MORE_SUCCESS,
      list: list.slice(10, 18),
      total: 90,
      amount: 30
    }
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoadingMore: true,
        list,
        total: 90,
        loadFrom: 8
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        list: [...list, ...list.slice(10, 18)],
        total: 90,
        loadFrom: 38,
        isLoadingMore: false,
        error: null
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_MORE_SUCCESS(passedState, action)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_MORE_FAILURE mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoadingMore: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoadingMore: false
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_MORE_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_SAME_REQUEST mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 50,
        isLoadingSame: false,
        error: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        loadFrom: 0,
        isLoadingSame: true,
        error: null
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_SAME_REQUEST(passedState)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_SAME_SUCCESS mutate state correctly', () => {
    const action = {
      type: BAZAAR_ITEMS_SAME_SUCCESS as typeof BAZAAR_ITEMS_SAME_SUCCESS,
      list: list.slice(10, 18),
      total: 90,
      amount: 30
    }
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoadingSame: true,
        list,
        total: 90,
        loadFrom: 8
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        list: list.slice(10, 18),
        total: 90,
        loadFrom: 30,
        isLoadingSame: false,
        error: null
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_SAME_SUCCESS(passedState, action)).toEqual(expectedState)
  })

  it('Should BAZAAR_ITEMS_SAME_FAILURE mutate state correctly', () => {
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoadingSame: true
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        isLoadingSame: false
      }
    }

    expect(ACTION_HANDLERS.BAZAAR_ITEMS_SAME_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should SET_ACTIVE_VIEW_ID mutate state correctly', () => {
    const action = {
      type: SET_ACTIVE_VIEW_ID as typeof SET_ACTIVE_VIEW_ID,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
    const passedState: IBazaarState = {
      ...initialState,
      activeView: {
        uniqueId: '12-3591872980',
        itemId: '3591872980',
        armoryId: '12'
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      activeView: {
        uniqueId: '12-3591872980',
        itemId: '3591872980',
        armoryId: '12'
      }
    }

    expect(ACTION_HANDLERS.SET_ACTIVE_VIEW_ID(passedState, action)).toEqual(expectedState)
  })

  // it('Should SET_ACTIVE_VIEW_HEIGHT mutate state correctly', () => {
  //   const action = {
  //     type: SET_ACTIVE_VIEW_HEIGHT as typeof SET_ACTIVE_VIEW_HEIGHT,
  //     uniqueId: 'jyfvghb775',
  //     height: 89
  //   }
  //   const passedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {
  //           ...initialState.bazaarItems.viewStates.views,
  //           jyfvghb775: {
  //             ...initialState.bazaarItems.viewStates.views.jyfvghb775,
  //             height: 5
  //           }
  //         }
  //       }
  //     }
  //   }
  //   const expectedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {
  //           ...initialState.bazaarItems.viewStates.views,
  //           jyfvghb775: {
  //             ...initialState.bazaarItems.viewStates.views.jyfvghb775,
  //             height: 89
  //           }
  //         }
  //       }
  //     }
  //   }

  //   expect(ACTION_HANDLERS.SET_ACTIVE_VIEW_HEIGHT(passedState, action)).toEqual(expectedState)
  // })

  // it('Should BAZAAR_ITEM_INFO_REQUEST mutate state correctly', () => {
  //   const action = {
  //     type: BAZAAR_ITEM_INFO_REQUEST as typeof BAZAAR_ITEM_INFO_REQUEST,
  //     uniqueId: 'juin78ubj'
  //   }
  //   const passedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {}
  //       }
  //     }
  //   }
  //   const expectedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {
  //           ...initialState.bazaarItems.viewStates.views,
  //           juin78ubj: viewInfoInitialState
  //         }
  //       }
  //     }
  //   }

  //   expect(ACTION_HANDLERS.BAZAAR_ITEM_INFO_REQUEST(passedState, action)).toEqual(expectedState)
  // })

  // it('Should BAZAAR_ITEM_INFO_SUCCESS mutate state correctly', () => {
  //   const action = {
  //     type: BAZAAR_ITEM_INFO_SUCCESS as typeof BAZAAR_ITEM_INFO_SUCCESS,
  //     uniqueId: 'juin78ubj',
  //     itemInfo: { ...viewInfoWithExtras, uniqueId: 'juin78ubj' }
  //   }
  //   const passedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {
  //           ...initialState.bazaarItems.viewStates.views,
  //           juin78ubj: viewInfoInitialState
  //         }
  //       }
  //     }
  //   }
  //   const expectedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {
  //           ...initialState.bazaarItems.viewStates.views,
  //           juin78ubj: {
  //             ...initialState.bazaarItems.viewStates.views.juin78ubj,
  //             ...viewInfoWithExtras,
  //             uniqueId: 'juin78ubj'
  //           }
  //         }
  //       }
  //     }
  //   }

  //   expect(ACTION_HANDLERS.BAZAAR_ITEM_INFO_SUCCESS(passedState, action)).toEqual(expectedState)
  // })

  // it('Should BAZAAR_ITEM_INFO_FAILURE mutate state correctly', () => {
  //   const action = {
  //     type: BAZAAR_ITEM_INFO_FAILURE as typeof BAZAAR_ITEM_INFO_FAILURE,
  //     uniqueId: 'juin78ubj'
  //   }
  //   const passedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {
  //           ...initialState.bazaarItems.viewStates.views,
  //           juin78ubj: {
  //             ...viewInfoInitialState,
  //             isLoading: true
  //           }
  //         }
  //       }
  //     }
  //   }
  //   const expectedState: IBazaarState = {
  //     ...initialState,
  //     bazaarItems: {
  //       ...initialState.bazaarItems,
  //       viewStates: {
  //         ...initialState.bazaarItems.viewStates,
  //         views: {
  //           ...initialState.bazaarItems.viewStates.views,
  //           juin78ubj: {
  //             ...viewInfoInitialState,
  //             isLoading: false
  //           }
  //         }
  //       }
  //     }
  //   }

  //   expect(ACTION_HANDLERS.BAZAAR_ITEM_INFO_FAILURE(passedState, action)).toEqual(expectedState)
  // })

  // FIXME

  it('Should SET_ROWS_DATA mutate state correctly', () => {
    const action = {
      type: SET_ROWS_DATA as typeof SET_ROWS_DATA,
      rows: [],
      totalRows: 0,
      itemsInRow: 3
    }
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        rows: [],
        totalRows: 0,
        itemsInRow: 3
      }
    }

    expect(ACTION_HANDLERS.SET_ROWS_DATA(passedState, action)).toEqual(expectedState)
  })

  it('Should REMOVE_ITEM mutate state correctly', () => {
    const action = {
      type: REMOVE_ITEM as typeof REMOVE_ITEM,
      uniqueId: '12-3591872978'
    }
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        list,
        loadFrom: 5,
        total: 50
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        list: expectedItemsAfterRemove,
        loadFrom: 4,
        total: 49
      }
    }

    expect(ACTION_HANDLERS.REMOVE_ITEM(passedState, action)).toEqual(expectedState)
  })

  it('Should UPDATE_ITEM_AMOUNT mutate state correctly', () => {
    const action = {
      type: UPDATE_ITEM_AMOUNT as typeof UPDATE_ITEM_AMOUNT,
      uniqueId: '731-0',
      amount: 5
    }
    const passedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        list,
        loadFrom: 5,
        total: 50
      }
    }
    const expectedState: IBazaarState = {
      ...initialState,
      bazaarItems: {
        ...initialState.bazaarItems,
        list: expectedItemsAfterAmountUpdate,
        loadFrom: 5,
        total: 50
      }
    }

    expect(ACTION_HANDLERS.UPDATE_ITEM_AMOUNT(passedState, action)).toEqual(expectedState)
  })
})
