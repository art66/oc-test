import {
  getBazaarItemsData,
  getFilters,
  getViewActiveId,
  getBazaarTotalItems,
  getBazaarSortBy,
  getRowHeight,
  getItemsInRow,
  getBazaarItemsRowsTotal,
  getBazaarRows
} from '../selectors'
import state, {
  tabletState,
  mobileState,
  categorizedState,
  categorizedMobileState,
  categorizedTabletState,
  withViewState,
  tabletWithViewState,
  mobileWithViewState,
  categorizedWithViewState,
  tabletCategorizedWithViewState,
  mobileCategorizedWithViewState,
  categorizedDescState,
  categorizedTabletDescState,
  categorizedMobileDescState,
  categorizedDescWithViewState,
  tabletCategorizedDescWithViewState,
  mobileCategorizedDescWithViewState
} from '../../components/Items/__tests__/mocks/state'
import { desktopRows, desktopRowsWithView } from './mocks/desktopRows'
import { mobileRows, mobileRowsWithView } from './mocks/mobileRows'
import { categorizedRows, categorizedWithViewRows } from './mocks/categorizedRows'
import { categorizedRowsMobile, categorizedRowsWithViewMobile } from './mocks/categorizedRowsMobile'
import { categorizedDescRows, categorizedDescWithViewRows } from './mocks/categorizedDescRows'
import { categorizedDescMobileRows, categorizedDescWithViewMobileRows } from './mocks/categorizedDescMobileRows'

describe('Items/selectors', () => {
  it('getBazaarItemsData', () => {
    expect(getBazaarItemsData(state)).toEqual(state.bazaar.bazaarItems.list)
  })

  it('getFilters', () => {
    expect(getFilters(state)).toEqual(state.bazaar.searchBar)
  })

  it('getViewActiveId', () => {
    expect(getViewActiveId(state)).toBe(state.bazaar.activeView.uniqueId)
  })

  it('getBazaarTotalItems', () => {
    expect(getBazaarTotalItems(state)).toBe(state.bazaar.bazaarItems.total)
  })

  it('getBazaarSortBy', () => {
    expect(getBazaarSortBy(state)).toBe(state.bazaar.searchBar.sortBy)
  })

  it('getBazaarSortBy', () => {
    expect(getBazaarSortBy(state)).toBe(state.bazaar.searchBar.sortBy)
  })

  it('getRowHeight simple row', () => {
    expect(getRowHeight({})).toBe(73)
  })

  it('getRowHeight with category', () => {
    expect(getRowHeight({ withCategory: true })).toBe(108)
  })

  it('getRowHeight with view', () => {
    expect(getRowHeight({ viewHeight: 50 })).toBe(123)
  })

  it('getRowHeight with category and view', () => {
    expect(getRowHeight({ withCategory: true, viewHeight: 50 })).toBe(158)
  })

  it('getItemsInRow on desktop', () => {
    expect(getItemsInRow(state)).toBe(3)
  })

  it('getItemsInRow on tablet', () => {
    expect(getItemsInRow(tabletState)).toBe(1)
  })

  it('getItemsInRow on mobile', () => {
    expect(getItemsInRow(mobileState)).toBe(1)
  })

  it('getBazaarItemsRowsTotal on desktop', () => {
    expect(getBazaarItemsRowsTotal(state)).toBe(12)
  })

  it('getBazaarItemsRowsTotal on tablet', () => {
    expect(getBazaarItemsRowsTotal(tabletState)).toBe(34)
  })

  it('getBazaarItemsRowsTotal on mobile', () => {
    expect(getBazaarItemsRowsTotal(mobileState)).toBe(34)
  })

  it('getBazaarRows on desktop', () => {
    expect(getBazaarRows(state)).toEqual(desktopRows)
  })

  it('getBazaarRows on tablet', () => {
    expect(getBazaarRows(tabletState)).toEqual(mobileRows)
  })

  it('getBazaarRows on mobile', () => {
    expect(getBazaarRows(mobileState)).toEqual(mobileRows)
  })

  it('getBazaarRows on desktop with view', () => {
    expect(getBazaarRows(withViewState)).toEqual(desktopRowsWithView)
  })

  it('getBazaarRows on tablet with view', () => {
    expect(getBazaarRows(tabletWithViewState)).toEqual(mobileRowsWithView)
  })

  it('getBazaarRows on mobile with view', () => {
    expect(getBazaarRows(mobileWithViewState)).toEqual(mobileRowsWithView)
  })

  it('getBazaarRows with categories on desktop', () => {
    expect(getBazaarRows(categorizedState)).toEqual(categorizedRows)
  })

  it('getBazaarRows with categories on tablet', () => {
    expect(getBazaarRows(categorizedTabletState)).toEqual(categorizedRowsMobile)
  })

  it('getBazaarRows with categories on mobile', () => {
    expect(getBazaarRows(categorizedMobileState)).toEqual(categorizedRowsMobile)
  })

  it('getBazaarRows with categories and view on desktop', () => {
    expect(getBazaarRows(categorizedWithViewState)).toEqual(categorizedWithViewRows)
  })

  it('getBazaarRows with categories and view on tablet', () => {
    expect(getBazaarRows(tabletCategorizedWithViewState)).toEqual(categorizedRowsWithViewMobile)
  })

  it('getBazaarRows with categories and view on mobile', () => {
    expect(getBazaarRows(mobileCategorizedWithViewState)).toEqual(categorizedRowsWithViewMobile)
  })

  it('getBazaarRows with categories desc order on desktop', () => {
    expect(getBazaarRows(categorizedDescState)).toEqual(categorizedDescRows)
  })

  it('getBazaarRows with categories desc order on tablet', () => {
    expect(getBazaarRows(categorizedTabletDescState)).toEqual(categorizedDescMobileRows)
  })

  it('getBazaarRows with categories desc order on mobile', () => {
    expect(getBazaarRows(categorizedMobileDescState)).toEqual(categorizedDescMobileRows)
  })

  it('getBazaarRows with categories desc order and view on desktop', () => {
    expect(getBazaarRows(categorizedDescWithViewState)).toEqual(categorizedDescWithViewRows)
  })

  it('getBazaarRows with categories desc order and view on tablet', () => {
    expect(getBazaarRows(tabletCategorizedDescWithViewState)).toEqual(categorizedDescWithViewMobileRows)
  })

  it('getBazaarRows with categories desc and view on mobile', () => {
    expect(getBazaarRows(mobileCategorizedDescWithViewState)).toEqual(categorizedDescWithViewMobileRows)
  })
})
