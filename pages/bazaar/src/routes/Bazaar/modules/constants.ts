import { ROOT_URL } from '../../../constants'

// ------------------------------------
// URLs
// ------------------------------------

export const BAZAAR_DATA_URL = `${ROOT_URL}&step=getBazaarContent`
export const OPEN_BAZAAR_REQUEST_URL = `${ROOT_URL}&step=openBazaar`
export const CLOSE_BAZAAR_REQUEST_URL = `${ROOT_URL}&step=closeBazaar`
export const BAZAAR_ITEMS_REQUEST_URL = `${ROOT_URL}&step=getBazaarItems`
export const BAZAAR_BUY_TEM_URL = `${ROOT_URL}&step=buyItem`

// ------------------------------------
// Action types
// ------------------------------------

export const INIT_BAZAAR = 'INIT_BAZAAR'
export const INITED_BAZAAR = 'INITED_BAZAAR'
export const OPEN_BAZAAR = 'OPEN_BAZAAR'
export const CLOSE_BAZAAR = 'CLOSE_BAZAAR'
export const OPEN_BAZAAR_REQUEST = 'OPEN_BAZAAR_REQUEST'
export const OPEN_BAZAAR_SUCCESS = 'OPEN_BAZAAR_SUCCESS'
export const OPEN_BAZAAR_FAILURE = 'OPEN_BAZAAR_FAILURE'
export const CLOSE_BAZAAR_REQUEST = 'CLOSE_BAZAAR_REQUEST'
export const CLOSE_BAZAAR_SUCCESS = 'CLOSE_BAZAAR_SUCCESS'
export const CLOSE_BAZAAR_FAILURE = 'CLOSE_BAZAAR_FAILURE'
export const SORT_BY_CHANGE = 'SORT_BY_CHANGE'
export const SEARCH_CHANGE = 'SEARCH_CHANGE'
export const ORDER_BY_CHANGE = 'ORDER_BY_CHANGE'
export const BAZAAR_ITEMS_REQUEST = 'BAZAAR_ITEMS_REQUEST'
export const BAZAAR_ITEMS_SUCCESS = 'BAZAAR_ITEMS_SUCCESS'
export const BAZAAR_ITEMS_FAILURE = 'BAZAAR_ITEMS_FAILURE'
export const BAZAAR_ITEMS_MORE_REQUEST = 'BAZAAR_ITEMS_MORE_REQUEST'
export const BAZAAR_ITEMS_MORE_SUCCESS = 'BAZAAR_ITEMS_MORE_SUCCESS'
export const BAZAAR_ITEMS_MORE_FAILURE = 'BAZAAR_ITEMS_MORE_FAILURE'
export const BAZAAR_ITEMS_SAME_REQUEST = 'BAZAAR_ITEMS_SAME_REQUEST'
export const BAZAAR_ITEMS_SAME_SUCCESS = 'BAZAAR_ITEMS_SAME_SUCCESS'
export const BAZAAR_ITEMS_SAME_FAILURE = 'BAZAAR_ITEMS_SAME_FAILURE'
export const SET_ACTIVE_VIEW_ID = 'SET_ACTIVE_VIEW_ID'
export const UNSET_ACTIVE_VIEW_ID = 'UNSET_ACTIVE_VIEW_ID'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const UPDATE_ITEM_AMOUNT = 'UPDATE_ITEM_AMOUNT'
export const BAZAAR_ITEMS_UPDATE_REQUEST = 'BAZAAR_ITEMS_UPDATE_REQUEST'
export const BAZAAR_ITEMS_SAME_UPDATE_REQUEST = 'BAZAAR_ITEMS_SAME_UPDATE_REQUEST'
export const SET_ROWS_DATA = 'SET_ROWS_DATA'
export const UPDATE_FORM_DATA = 'UPDATE_FORM_DATA'

export const BUY_AMOUNT_DEFAULT = 1

export const BUY_MENU_VIEW_TYPE = 'BUY_MENU_VIEW_TYPE'
export const BUY_CONFIRMATION_VIEW_TYPE = 'BUY_CONFIRMATION_VIEW_TYPE'
export const BUY_BOUGHT_INFO_VIEW_TYPE = 'BUY_BOUGHT_INFO_VIEW_TYPE'
export const BUY_DENIED_VIEW_TYPE = 'BUY_DENIED_VIEW_TYPE'

export const CATEGORY_ROW_HEIGHT = 35
export const ITEMS_ROW_HEIGHT = 73

export const BAZAAR_CATEGORIES = [
  'Alcohol',
  'Armor',
  'Artifacts',
  'Books',
  'Boosters',
  'Candy',
  'Cars',
  'Clothing',
  'Collectibles',
  'Drugs',
  'Electronics',
  'Energy Drink',
  'Enhancer',
  'Flowers',
  'Jewelry',
  'Medical',
  'Melee',
  'Miscellaneous',
  'Plushies',
  'Primary',
  'Secondary',
  'Special',
  'Supply Packs',
  'Temporary',
  'Viruses'
]
