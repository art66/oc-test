import { TItem } from '../../../../modules/types/state'
import { IRow } from '../../components/Items/types'
import {
  BUY_MENU_VIEW_TYPE,
  BUY_CONFIRMATION_VIEW_TYPE,
  BUY_BOUGHT_INFO_VIEW_TYPE,
  BUY_DENIED_VIEW_TYPE
} from '../constants'

export type TActiveBuyType =
  | typeof BUY_MENU_VIEW_TYPE
  | typeof BUY_CONFIRMATION_VIEW_TYPE
  | typeof BUY_BOUGHT_INFO_VIEW_TYPE
  | typeof BUY_DENIED_VIEW_TYPE

export type TFormDataItem = {
  buyAmount?: number
  boughtAmount?: number
  activeBuyType?: TActiveBuyType
  isBuying?: boolean
  buyErrorMessage?: string
  imgBarFocused?: boolean
}

export type TFormData = {
  [key: string]: TFormDataItem
}

export interface IBazaarItems {
  limit: number
  total: number
  loadFrom: number
  list: TItem[]
  rows: IRow[]
  formData: TFormData
  totalRows: number
  itemsInRow: number
  error?: boolean
  isLoading: boolean
  isLoadingMore: boolean
  isLoadingSame: boolean
}

export type TSortBy = 'default' | 'name' | 'category' | 'cost' | 'value' | 'rarity'

export type TOrderBy = 'asc' | 'desc'

export interface ISearchBar {
  sortBy: TSortBy
  search: string
  orderBy: TOrderBy
}

export interface IBazaarState {
  isLoading: boolean
  bazaarState: {
    isLoading: boolean
  }
  searchBar: ISearchBar
  bazaarItems: IBazaarItems
  activeView: {
    uniqueId?: string
    itemId?: string
    armoryId?: string
  }
}

type TBuyMenuType =
  | typeof BUY_MENU_VIEW_TYPE
  | typeof BUY_CONFIRMATION_VIEW_TYPE
  | typeof BUY_BOUGHT_INFO_VIEW_TYPE
  | typeof BUY_DENIED_VIEW_TYPE
