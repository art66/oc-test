import {
  OPEN_BAZAAR,
  CLOSE_BAZAAR,
  OPEN_BAZAAR_REQUEST,
  OPEN_BAZAAR_SUCCESS,
  OPEN_BAZAAR_FAILURE,
  CLOSE_BAZAAR_REQUEST,
  CLOSE_BAZAAR_SUCCESS,
  CLOSE_BAZAAR_FAILURE,
  SORT_BY_CHANGE,
  SEARCH_CHANGE,
  ORDER_BY_CHANGE,
  BAZAAR_ITEMS_REQUEST,
  BAZAAR_ITEMS_SUCCESS,
  BAZAAR_ITEMS_FAILURE,
  BAZAAR_ITEMS_MORE_REQUEST,
  BAZAAR_ITEMS_MORE_SUCCESS,
  BAZAAR_ITEMS_MORE_FAILURE,
  BAZAAR_ITEMS_SAME_REQUEST,
  BAZAAR_ITEMS_SAME_SUCCESS,
  BAZAAR_ITEMS_SAME_FAILURE,
  SET_ACTIVE_VIEW_ID,
  UNSET_ACTIVE_VIEW_ID,
  INIT_BAZAAR,
  INITED_BAZAAR,
  UPDATE_ITEM_AMOUNT,
  BAZAAR_ITEMS_UPDATE_REQUEST,
  BAZAAR_ITEMS_SAME_UPDATE_REQUEST,
  SET_ROWS_DATA,
  REMOVE_ITEM,
  UPDATE_FORM_DATA
} from '../constants'
import { ISearchBar, IBazaarItems, TActiveBuyType } from './state'
import { IRow } from '../../components/Items/types'
import { TFormDataItem } from './state'

export type TInitBazaarPayload = {
  type: typeof INIT_BAZAAR
}

export type TInitBazaar = () => TInitBazaarPayload

export type TInitedBazaarPayload = {
  type: typeof INITED_BAZAAR
}

export type TInitedBazaar = () => TInitedBazaarPayload

export interface IOpenBazaar {
  type: typeof OPEN_BAZAAR
}

export interface ICloseBazaar {
  type: typeof CLOSE_BAZAAR
}

export interface IOpenBazaarRequest {
  type: typeof OPEN_BAZAAR_REQUEST
}

export interface IOpenBazaarSuccess {
  type: typeof OPEN_BAZAAR_SUCCESS
}

export interface IOpenBazaarFailure {
  type: typeof OPEN_BAZAAR_FAILURE
}

export interface ICloseBazaarRequest {
  type: typeof CLOSE_BAZAAR_REQUEST
}

export interface ICloseBazaarSuccess {
  type: typeof CLOSE_BAZAAR_SUCCESS
}

export interface ICloseBazaarFailure {
  type: typeof CLOSE_BAZAAR_FAILURE
}

export interface ISortByChange {
  type: typeof SORT_BY_CHANGE
  sortBy: ISearchBar['sortBy']
}

export interface IOrderByChange {
  type: typeof ORDER_BY_CHANGE
  orderBy: ISearchBar['orderBy']
}

export interface ISearchChange {
  type: typeof SEARCH_CHANGE
  search: string
}

export interface IBazaarItemsPayload {
  list: IBazaarItems['list']
  total: number
  amount: number
}

export interface IBazaarItemsRequest {
  type: typeof BAZAAR_ITEMS_REQUEST
}

export interface IBazaarItemsUpdateRequest {
  type: typeof BAZAAR_ITEMS_UPDATE_REQUEST
}

export interface IBazaarItemsSameUpdateRequest {
  type: typeof BAZAAR_ITEMS_SAME_UPDATE_REQUEST
}

export interface IBazaarItemsSuccess extends IBazaarItemsPayload {
  type: typeof BAZAAR_ITEMS_SUCCESS
}

export interface IBazaarItemsFailure {
  type: typeof BAZAAR_ITEMS_FAILURE
}

export interface IBazaarItemsMoreRequest {
  type: typeof BAZAAR_ITEMS_MORE_REQUEST
}

export interface IBazaarItemsMoreSuccess extends IBazaarItemsPayload {
  type: typeof BAZAAR_ITEMS_MORE_SUCCESS
}

export interface IBazaarItemsMoreFailure {
  type: typeof BAZAAR_ITEMS_MORE_FAILURE
}

export interface IBazaarItemsSameRequest {
  type: typeof BAZAAR_ITEMS_SAME_REQUEST
}

export interface IBazaarItemsSameSuccess extends IBazaarItemsPayload {
  type: typeof BAZAAR_ITEMS_SAME_SUCCESS
}

export interface IBazaarItemsSameFailure {
  type: typeof BAZAAR_ITEMS_SAME_FAILURE
}

export type TSetActiveViewPayload = {
  type: typeof SET_ACTIVE_VIEW_ID
  uniqueId?: string
  itemId?: string
  armoryId?: string
}

export type TSetActiveViewAction = (uniqueId: string) => TSetActiveViewPayload

export type TUnsetActiveViewPayload = {
  type: typeof UNSET_ACTIVE_VIEW_ID
}

export type TUnsetActiveViewAction = () => TUnsetActiveViewPayload

export type TRemoveItemPayload = {
  type: typeof REMOVE_ITEM
  uniqueId: string
}

export type TRemoveItem = (uniqueId: string) => TRemoveItemPayload

export type TUpdateItemAmountPayload = {
  type: typeof UPDATE_ITEM_AMOUNT
  uniqueId: string
  amount: number
}

export type TUpdateItemAmount = (uniqueId: string, amount: number) => TUpdateItemAmountPayload

export type TSetRowsDataPayload = {
  type: typeof SET_ROWS_DATA
  rows: IRow[]
  totalRows: number
  itemsInRow: number
}

export type TSetRowsData = ({
  rows,
  totalRows,
  itemsInRow
}: {
  rows: IRow[]
  totalRows: number
  itemsInRow: number
}) => TSetRowsDataPayload

export type TUpdateFormDataPayload = {
  type: typeof UPDATE_FORM_DATA
  uniqueId: string
  data: TFormDataItem
}

export type TUpdateFormData = (uniqueId: string, data: TFormDataItem) => TUpdateFormDataPayload

export type TItemImgBarFocus = (uniqueId: string) => void

export type TChangeActiveBuyType = (uniqueId: string, activeBuyType?: TActiveBuyType) => void
