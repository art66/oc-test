import { createSelector } from 'reselect'

import { getMediaType } from '../../../modules/selectors'
import { SORT_BY_CATEGORY_VALUE } from '../constants'
import { BAZAAR_CATEGORIES, ITEMS_ROW_HEIGHT, CATEGORY_ROW_HEIGHT } from './constants'

import { IState } from '../../../store/types'
import { ISearchBar } from './types/state'
import { IRow } from '../components/Items/types'
import { TNormalizedItems } from '../../../modules/types/normalizers'

const createCategoriesTemplate = (categoriesList: string[]) => {
  return categoriesList.reduce((categories, categoryName) => {
    categories[categoryName] = []

    return categories
  }, {})
}

export const getBazaarItemsData = (state: IState) => state.bazaar.bazaarItems.list
export const getFilters = (state: IState) => state.bazaar.searchBar
export const getBazaarTotalItems = (state: IState) => state.bazaar.bazaarItems.total
export const getBazaarSortBy = (state: IState) => state.bazaar.searchBar.sortBy
export const getViewActiveId = (state: IState) => state.bazaar.activeView.uniqueId

export const getRowHeight = createSelector([data => data], ({ withCategory, viewHeight }): number => {
  let height = ITEMS_ROW_HEIGHT

  if (withCategory) {
    height += CATEGORY_ROW_HEIGHT
  }

  if (viewHeight) {
    height += viewHeight
  }

  return height
})

export const getItemsInRow = createSelector([getMediaType], (mediaType): number => {
  switch (mediaType) {
    case 'desktop': {
      return 3
    }
    default: {
      return 1
    }
  }
})

const getItemRows = createSelector([data => data], ({ items, itemsInRow, viewItemUniqueId, categoryName }) => {
  const rows = []
  const length = items.length
  let itemsIndex = 0

  if (categoryName) {
    itemsIndex = itemsInRow

    const rowItems = items.slice(0, itemsIndex)

    rows.push({ categoryName, rowItems })
  }

  for (; itemsIndex < length; itemsIndex += itemsInRow) {
    const rowItems = items.slice(itemsIndex, itemsIndex + itemsInRow)

    rows.push({ rowItems })
  }

  if (viewItemUniqueId) {
    let viewItemIndex = -1
    items.forEach((item, index) => {
      if (viewItemUniqueId === item.uniqueId) {
        viewItemIndex = index
      }
    })

    if (viewItemIndex >= 0) {
      const viewItemRowIndex = Math.floor(viewItemIndex / itemsInRow)
      const { [viewItemRowIndex]: viewItemRow } = rows

      const rowWithView = {
        ...viewItemRow,
        withView: true
      }

      rows.splice(viewItemRowIndex, 1, rowWithView)
    }
  }

  return rows
})

const getBazaarCategoryRow = createSelector(
  [data => data],
  ({ itemsByCategories, itemsInRow, viewItemUniqueId, categoryName }) => {
    const { [categoryName]: items } = itemsByCategories

    return getItemRows({ items, itemsInRow, viewItemUniqueId, categoryName })
  }
)

const isCategoryExists = createSelector([data => data], (categoryName: string): boolean => {
  return BAZAAR_CATEGORIES.includes(categoryName)
})

const getItemsByCategories = createSelector([data => data], (items: IRow['rowItems']) => {
  const categoriesTemplate = createCategoriesTemplate(BAZAAR_CATEGORIES)

  const categoryItems = items.reduce((categories, item) => {
    isCategoryExists(item.categoryName) && categories[item.categoryName].push(item)

    return categories
  }, categoriesTemplate)

  return categoryItems
})

const getBazaarCategories = createSelector([data => data], orderBy => {
  if (orderBy === 'desc') {
    return [...BAZAAR_CATEGORIES].reverse()
  }

  return [...BAZAAR_CATEGORIES]
})

export const getBazaarCategoryRows = createSelector(
  [data => data],
  ({ items, itemsInRow, viewItemUniqueId, orderBy }) => {
    const itemsByCategories = getItemsByCategories(items)

    const bazaarCategories = getBazaarCategories(orderBy)

    const rows = bazaarCategories.reduce((constructedRows, categoryName) => {
      if (!(itemsByCategories[categoryName].length > 0)) {
        return constructedRows
      }

      const categoryRows = getBazaarCategoryRow({ categoryName, itemsByCategories, itemsInRow, viewItemUniqueId })

      return constructedRows.concat(categoryRows)
    }, [])

    return rows
  }
)

export const getBazaarRows = createSelector(
  [getBazaarItemsData, getItemsInRow, getFilters, getViewActiveId],
  (items: TNormalizedItems['list'], itemsInRow: number, filters: ISearchBar, viewItemUniqueId: string): IRow[] => {
    if (filters.sortBy === SORT_BY_CATEGORY_VALUE) {
      return getBazaarCategoryRows({ items, itemsInRow, viewItemUniqueId, orderBy: filters.orderBy })
    }

    return getItemRows({ items, itemsInRow, viewItemUniqueId })
  }
)

export const getBazaarItemsRowsTotal = createSelector([getBazaarRows, getBazaarTotalItems], (rows, total): number => {
  const itemsInRows = rows.reduce((itemsInRows, row) => {
    return itemsInRows + row.rowItems.length
  }, 0)

  if (itemsInRows < total) {
    return rows.length + 1
  }

  return rows.length
})

export const getBazaarRowsData = createSelector(
  [getBazaarRows, getBazaarItemsRowsTotal, getItemsInRow],
  (rows, totalRows, itemsInRow) => {
    return {
      rows,
      totalRows,
      itemsInRow
      // uniqueKey: Date.now().toString()
    }
  }
)
