import qs from 'qs'
import { BAZAAR_DATA_URL, BAZAAR_ITEMS_REQUEST_URL } from './constants'

export const createBazaarDataUrl = ({ userId }) => `${BAZAAR_DATA_URL}&${qs.stringify({ ID: userId })}`

export const createBazaarItemsUrl = ({ loadFrom, userId, sortBy, orderBy, limit, search }) =>
  `${BAZAAR_ITEMS_REQUEST_URL}&${qs.stringify({
    start: loadFrom,
    ID: userId,
    order: sortBy,
    by: orderBy,
    categorised: 0,
    limit,
    searchname: search
  })}`
