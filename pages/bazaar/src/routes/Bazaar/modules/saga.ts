import { takeLatest, debounce } from 'redux-saga/effects'
import {
  openBazaarRequestSaga,
  closeBazaarRequestSaga,
  onBazaarItemsOrderByChangedSaga,
  onBazaarItemsSearchChangedSaga,
  onBazaarItemsSortByChangedSaga,
  onBazaarItemsRequestSaga,
  onBazaarItemsRequestMoreSaga,
  onBazaarItemsRequestSameSaga,
  normalizeItemsSaga
} from './sagas'
import {
  OPEN_BAZAAR,
  CLOSE_BAZAAR,
  BAZAAR_ITEMS_REQUEST,
  BAZAAR_ITEMS_MORE_REQUEST,
  SORT_BY_CHANGE,
  SEARCH_CHANGE,
  ORDER_BY_CHANGE,
  SET_ACTIVE_VIEW_ID,
  INIT_BAZAAR,
  BAZAAR_ITEMS_SAME_REQUEST,
  BAZAAR_ITEMS_SUCCESS,
  BAZAAR_ITEMS_FAILURE,
  BAZAAR_ITEMS_MORE_SUCCESS,
  BAZAAR_ITEMS_MORE_FAILURE,
  BAZAAR_ITEMS_SAME_SUCCESS,
  BAZAAR_ITEMS_SAME_FAILURE,
  UNSET_ACTIVE_VIEW_ID,
  REMOVE_ITEM,
  UPDATE_ITEM_AMOUNT
} from './constants'
import bazaarItemInfoRequestSaga from '../../../modules/sagas/bazaarItemInfoRequest'
import { BAZAAR_PAGE_ID } from '../../../constants'
import setPageIdSaga from '../../../modules/sagas/setPageId'
import { initBazaarItemsSaga } from './sagas/initSaga'

export default function* bazaarRoot() {
  yield takeLatest(INIT_BAZAAR, initBazaarItemsSaga)
  yield takeLatest(INIT_BAZAAR, setPageIdSaga, BAZAAR_PAGE_ID)
  yield debounce(100, OPEN_BAZAAR, openBazaarRequestSaga)
  yield debounce(100, CLOSE_BAZAAR, closeBazaarRequestSaga)
  yield takeLatest(BAZAAR_ITEMS_REQUEST, onBazaarItemsRequestSaga)
  yield takeLatest(BAZAAR_ITEMS_MORE_REQUEST, onBazaarItemsRequestMoreSaga)
  yield takeLatest(BAZAAR_ITEMS_SAME_REQUEST, onBazaarItemsRequestSameSaga)
  yield takeLatest(SORT_BY_CHANGE, onBazaarItemsSortByChangedSaga)
  yield takeLatest(ORDER_BY_CHANGE, onBazaarItemsOrderByChangedSaga)
  yield takeLatest(SEARCH_CHANGE, onBazaarItemsSearchChangedSaga)
  yield takeLatest(SET_ACTIVE_VIEW_ID, bazaarItemInfoRequestSaga)
  yield takeLatest(
    [
      BAZAAR_ITEMS_SUCCESS,
      BAZAAR_ITEMS_FAILURE,
      BAZAAR_ITEMS_MORE_SUCCESS,
      BAZAAR_ITEMS_MORE_FAILURE,
      BAZAAR_ITEMS_SAME_SUCCESS,
      BAZAAR_ITEMS_SAME_FAILURE,
      SET_ACTIVE_VIEW_ID,
      UNSET_ACTIVE_VIEW_ID,
      REMOVE_ITEM,
      UPDATE_ITEM_AMOUNT,
      'redux-responsive/CALCULATE_RESPONSIVE_STATE'
    ],
    normalizeItemsSaga
  )
}
