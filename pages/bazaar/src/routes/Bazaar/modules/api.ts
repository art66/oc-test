import { fetchUrl } from '@torn/shared/utils'
import { BAZAAR_BUY_TEM_URL } from './constants'

export const buyItemRequest = async ({ userId, bazaarId, itemId, buyAmount, price, beforeval }) => {
  try {
    const buyItemData = await fetchUrl(BAZAAR_BUY_TEM_URL, {
      userID: userId,
      id: bazaarId,
      itemID: itemId,
      amount: buyAmount,
      price,
      beforeval
    })

    if (!buyItemData.success) {
      throw buyItemData.text
    }

    if (buyItemData.error) {
      throw buyItemData.error
    }

    return buyItemData
  } catch (error) {
    throw error
  }
}
