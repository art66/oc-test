import { createReducer } from '../../../modules/utils'
import {
  OPEN_BAZAAR_REQUEST,
  OPEN_BAZAAR_SUCCESS,
  OPEN_BAZAAR_FAILURE,
  CLOSE_BAZAAR_REQUEST,
  CLOSE_BAZAAR_SUCCESS,
  CLOSE_BAZAAR_FAILURE,
  SORT_BY_CHANGE,
  SEARCH_CHANGE,
  ORDER_BY_CHANGE,
  BAZAAR_ITEMS_REQUEST,
  BAZAAR_ITEMS_SUCCESS,
  BAZAAR_ITEMS_FAILURE,
  BAZAAR_ITEMS_MORE_REQUEST,
  BAZAAR_ITEMS_MORE_SUCCESS,
  BAZAAR_ITEMS_MORE_FAILURE,
  BAZAAR_ITEMS_SAME_REQUEST,
  BAZAAR_ITEMS_SAME_SUCCESS,
  BAZAAR_ITEMS_SAME_FAILURE,
  SET_ACTIVE_VIEW_ID,
  INIT_BAZAAR,
  INITED_BAZAAR,
  UPDATE_ITEM_AMOUNT,
  BAZAAR_ITEMS_UPDATE_REQUEST,
  BAZAAR_ITEMS_SAME_UPDATE_REQUEST,
  UNSET_ACTIVE_VIEW_ID,
  SET_ROWS_DATA,
  REMOVE_ITEM,
  UPDATE_FORM_DATA
} from './constants'
import { SORT_BY_DEFAULT_VALUE, ASC_ORDER } from '../constants'
import { IBazaarState } from './types/state'
import {
  ISortByChange,
  ISearchChange,
  IOrderByChange,
  IBazaarItemsSuccess,
  IBazaarItemsMoreSuccess,
  IBazaarItemsSameSuccess,
  TSetActiveViewPayload,
  TRemoveItemPayload,
  TSetRowsDataPayload,
  TUpdateItemAmountPayload,
  TUpdateFormDataPayload
} from './types/actions'

export const initialState: IBazaarState = {
  isLoading: false,
  bazaarState: {
    isLoading: false
  },
  searchBar: {
    sortBy: SORT_BY_DEFAULT_VALUE,
    orderBy: ASC_ORDER,
    search: ''
  },
  bazaarItems: {
    limit: 60,
    total: 0,
    loadFrom: 0,
    list: [],
    rows: [],
    formData: {},
    // FIXME: Need to write tests
    totalRows: 0,
    itemsInRow: 0,
    isLoading: false,
    isLoadingMore: false,
    isLoadingSame: false,
    error: null
  },
  activeView: {
    uniqueId: null,
    itemId: null,
    armoryId: null
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------

export const ACTION_HANDLERS = {
  [INIT_BAZAAR]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      isLoading: true,
      bazaarItems: {
        ...state.bazaarItems,
        loadFrom: 0,
        isLoading: true,
        error: null
      }
    }
  },
  [INITED_BAZAAR]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      isLoading: false
    }
  },
  [OPEN_BAZAAR_REQUEST]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarState: {
        ...state.bazaarState,
        isLoading: true
      }
    }
  },
  [OPEN_BAZAAR_SUCCESS]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarState: {
        ...state.bazaarState,
        isLoading: false
      }
    }
  },
  [OPEN_BAZAAR_FAILURE]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarState: {
        isLoading: false
      }
    }
  },
  [CLOSE_BAZAAR_REQUEST]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarState: {
        isLoading: true
      }
    }
  },
  [CLOSE_BAZAAR_SUCCESS]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarState: {
        isLoading: false
      }
    }
  },
  [CLOSE_BAZAAR_FAILURE]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarState: {
        isLoading: false
      }
    }
  },
  [SORT_BY_CHANGE]: (state: IBazaarState, action: ISortByChange): IBazaarState => {
    return {
      ...state,
      searchBar: {
        ...state.searchBar,
        sortBy: action.sortBy || SORT_BY_DEFAULT_VALUE,
        orderBy: ASC_ORDER
      }
    }
  },
  [SEARCH_CHANGE]: (state: IBazaarState, action: ISearchChange): IBazaarState => {
    return {
      ...state,
      searchBar: {
        ...state.searchBar,
        search: action.search
      }
    }
  },
  [ORDER_BY_CHANGE]: (state: IBazaarState, action: IOrderByChange): IBazaarState => {
    return {
      ...state,
      searchBar: {
        ...state.searchBar,
        orderBy: action.orderBy
      }
    }
  },
  [BAZAAR_ITEMS_REQUEST]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        loadFrom: 0,
        isLoading: true,
        error: null
      }
    }
  },
  [BAZAAR_ITEMS_SUCCESS]: (state: IBazaarState, action: IBazaarItemsSuccess): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        isLoading: false,
        isLoadingSame: false,
        list: action.list,
        formData: {},
        total: action.total,
        loadFrom: action.amount
      }
    }
  },
  [BAZAAR_ITEMS_FAILURE]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        isLoading: false,
        isLoadingSame: false,
        list: [],
        formData: {},
        total: 0,
        loadFrom: 0,
        error: true
      }
    }
  },
  [BAZAAR_ITEMS_UPDATE_REQUEST]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        loadFrom: 0,
        isLoadingSame: true,
        error: null
      }
    }
  },
  [BAZAAR_ITEMS_MORE_REQUEST]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        isLoadingMore: true,
        error: null
      }
    }
  },
  [BAZAAR_ITEMS_MORE_SUCCESS]: (state: IBazaarState, action: IBazaarItemsMoreSuccess): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        isLoadingMore: false,
        list: [...state.bazaarItems.list, ...action.list],
        total: action.total,
        loadFrom: state.bazaarItems.loadFrom + action.amount
      }
    }
  },
  [BAZAAR_ITEMS_MORE_FAILURE]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        isLoadingMore: false,
        list: [],
        formData: {},
        total: 0,
        loadFrom: 0
      }
    }
  },
  [BAZAAR_ITEMS_SAME_REQUEST]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        loadFrom: 0,
        isLoadingSame: true,
        error: null
      }
    }
  },
  [BAZAAR_ITEMS_SAME_UPDATE_REQUEST]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        loadFrom: 0,
        isLoadingSame: true,
        error: null
      }
    }
  },
  [BAZAAR_ITEMS_SAME_SUCCESS]: (state: IBazaarState, action: IBazaarItemsSameSuccess): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        isLoadingSame: false,
        list: action.list,
        total: action.total,
        loadFrom: action.amount
      }
    }
  },
  [BAZAAR_ITEMS_SAME_FAILURE]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        isLoadingSame: false,
        list: [],
        total: 0,
        loadFrom: 0
      }
    }
  },
  [SET_ACTIVE_VIEW_ID]: (state: IBazaarState, action: TSetActiveViewPayload): IBazaarState => {
    return {
      ...state,
      activeView: {
        ...state.activeView,
        uniqueId: action.uniqueId,
        itemId: action.itemId,
        armoryId: action.armoryId
      }
    }
  },
  [REMOVE_ITEM]: (state: IBazaarState, action: TRemoveItemPayload): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        list: state.bazaarItems.list.filter(item => item.uniqueId !== action.uniqueId),
        loadFrom: state.bazaarItems.loadFrom - 1,
        total: state.bazaarItems.total - 1
      }
    }
  },
  [UPDATE_ITEM_AMOUNT]: (state: IBazaarState, action: TUpdateItemAmountPayload): IBazaarState => {
    const nextList = state.bazaarItems.list.map(item => {
      if (item.uniqueId === action.uniqueId) {
        return {
          ...item,
          amount: item.amount - action.amount
        }
      }

      return item
    })

    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        list: nextList
      }
    }
  },
  // FIX<E: Test it
  [UNSET_ACTIVE_VIEW_ID]: (state: IBazaarState): IBazaarState => {
    return {
      ...state,
      activeView: {
        uniqueId: null,
        itemId: null,
        armoryId: null
      }
    }
  },
  [SET_ROWS_DATA]: (state: IBazaarState, action: TSetRowsDataPayload): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        rows: action.rows,
        totalRows: action.totalRows,
        itemsInRow: action.itemsInRow
      }
    }
  },
  [UPDATE_FORM_DATA]: (state: IBazaarState, action: TUpdateFormDataPayload): IBazaarState => {
    return {
      ...state,
      bazaarItems: {
        ...state.bazaarItems,
        formData: {
          ...state.bazaarItems.formData,
          [action.uniqueId]: {
            ...state.bazaarItems.formData[action.uniqueId],
            ...action.data
          }
        }
      }
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------

export default createReducer(initialState, ACTION_HANDLERS)
