import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { closeBazaarRequest, closeBazaarSuccess, closeBazaarFailure } from '../actions'
import { CLOSE_BAZAAR_REQUEST_URL } from '../constants'
import { setRequestInfoBox } from '../../../../modules/actions'

export default function* closeBazaarRequestSaga() {
  try {
    yield put(setRequestInfoBox({ loading: true }))

    yield put(closeBazaarRequest())

    const closeBazaarData = yield fetchUrl(CLOSE_BAZAAR_REQUEST_URL, null, null)

    if (closeBazaarData.error) {
      throw closeBazaarData.error
    }

    yield put(closeBazaarSuccess())

    yield put(setRequestInfoBox({ loading: false, msg: closeBazaarData.text, color: 'green' }))
  } catch (error) {
    const message = error.message || error.toString()

    yield put(closeBazaarFailure())
    yield put(setRequestInfoBox({ loading: false, msg: message, color: 'red' }))
  }
}
