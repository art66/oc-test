import { put, all, select } from 'redux-saga/effects'
import { IState } from '../../../../store/types'
import { showInfoBox, setRequestInfoBox, leaveRequestInfoBox } from '../../../../modules/actions'
import bazaarDataRequestSaga from './bazaarDataRequest'
import { onBazaarItemsRequestSaga } from './bazaarItemsRequest'
import { initedBazaar } from '../actions'

export function* initBazaarItemsSaga() {
  try {
    let state: IState = yield select()
    if (!state.common.leaveRequestInfoBox) {
      yield put(showInfoBox(null))
    } else {
      yield put(leaveRequestInfoBox(false))
    }
    yield put(setRequestInfoBox({ loading: true }))

    yield all([bazaarDataRequestSaga(), onBazaarItemsRequestSaga()])

    state = yield select()

    yield put(setRequestInfoBox({ ...state.common.requestInfoBox, loading: false }))

    yield put(initedBazaar())
  } catch (error) {
    const state: IState = yield select()

    yield put(setRequestInfoBox({ ...state.common.requestInfoBox, loading: false }))
  }
}
