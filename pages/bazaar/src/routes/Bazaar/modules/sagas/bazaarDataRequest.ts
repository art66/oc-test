import { put, select } from 'redux-saga/effects'
import { fetchUrl, getURLParameterByName } from '@torn/shared/utils'
import {
  metadataRequest,
  metadataResponseSuccess,
  // metadataResponseFailure,
  ownerRequest,
  ownerResponseSuccess,
  // ownerResponseFailure,
  bazaarDataRequest,
  bazaarDataResponseSuccess,
  // bazaarDataResponseFailure,
  setRequestInfoBox
} from '../../../../modules/actions'
import {
  normalizeMetadata,
  normalizeOwner,
  normalizeFormData as normalizeBazaarData
} from '../../../../modules/normalizers'
import { createBazaarDataUrl } from '../urls'
import { IState } from '../../../../store/types'

export default function* bazaarDataRequestSaga() {
  try {
    const userId = getURLParameterByName('userId')

    yield put(metadataRequest())
    yield put(ownerRequest())
    yield put(bazaarDataRequest())

    const bazaarData = yield fetchUrl(createBazaarDataUrl({ userId }), null, null)

    const normalizedMetadata = normalizeMetadata(bazaarData)
    const normalizedOwner = normalizeOwner(bazaarData)
    const normalizedBazaarData = normalizeBazaarData(bazaarData)

    yield put(metadataResponseSuccess(normalizedMetadata))
    yield put(ownerResponseSuccess(normalizedOwner))
    yield put(bazaarDataResponseSuccess(normalizedBazaarData))

    if (bazaarData.error) {
      throw bazaarData.error
    }

    if (bazaarData.state.message) {
      const state: IState = yield select()

      yield put(
        setRequestInfoBox({ ...state.common.infoBox, msg: bazaarData.state.message, color: bazaarData.state.color })
      )
    }
  } catch (error) {
    const message = error.message || error.toString()

    // yield put(metadataResponseFailure())
    // yield put(ownerResponseFailure())
    // yield put(bazaarDataResponseFailure())

    yield put(setRequestInfoBox({ msg: message, color: 'red' }))
  }
}
