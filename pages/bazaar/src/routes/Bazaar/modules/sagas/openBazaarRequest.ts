import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { openBazaarRequest, openBazaarSuccess, openBazaarFailure } from '../actions'
import { OPEN_BAZAAR_REQUEST_URL } from '../constants'
import { setRequestInfoBox } from '../../../../modules/actions'

export default function* openBazaarRequestSaga() {
  try {
    yield put(setRequestInfoBox({ loading: true }))

    yield put(openBazaarRequest())

    const openBazaarData = yield fetchUrl(OPEN_BAZAAR_REQUEST_URL, null, null)

    if (openBazaarData.error) {
      throw openBazaarData.error
    }

    yield put(openBazaarSuccess())

    yield put(setRequestInfoBox({ loading: false, msg: openBazaarData.text, color: 'green' }))
  } catch (error) {
    const message = error.message || error.toString()

    yield put(openBazaarFailure())
    yield put(setRequestInfoBox({ loading: false, msg: message, color: 'red' }))
  }
}
