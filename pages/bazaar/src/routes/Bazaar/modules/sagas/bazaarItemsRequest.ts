import { put, delay, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  bazaarItemsSuccess,
  bazaarItemsFailure,
  bazaarItemsMoreSuccess,
  bazaarItemsMoreFailure,
  bazaarItemsSameSuccess,
  bazaarItemsSameFailure,
  unsetActiveViewUniqueId,
  bazaarItemsUpdateRequest,
  bazaarItemsSameUpdateRequest
} from '../actions'
import { createBazaarItemsUrl } from '../urls'
import getStateOnOwnerReceived from '../../../../modules/sagas/getStateOnOwnerReceived'
import { IState } from '../../../../store/types'
import normalizeItemsData from '../../../../modules/normalizers/normalizeItemsData'
import { showInfoBox } from '../../../../modules/actions'

export default function* bazaarItemsRequestSaga() {
  try {
    const state: IState = yield getStateOnOwnerReceived()
    const { searchBar } = state.bazaar
    const { loadFrom, limit } = state.bazaar.bazaarItems

    const bazaarItemsData = yield fetchUrl(
      createBazaarItemsUrl({
        loadFrom,
        userId: state.common.owner.data.userId,
        sortBy: searchBar.sortBy,
        orderBy: searchBar.orderBy,
        limit,
        search: searchBar.search
      }),
      null,
      null
    )

    if (bazaarItemsData.error) {
      throw bazaarItemsData.error
    }

    const normalizedBazaarItemsData = normalizeItemsData({ /* uniqueKey: Date.now().toString(), */ ...bazaarItemsData })

    return normalizedBazaarItemsData
  } catch (error) {
    const message = error.message || error.toString()

    yield put(showInfoBox({ msg: message, color: 'red' }))

    throw error
  }
}

export function* onBazaarItemsRequestSaga() {
  try {
    const state: IState = yield select()

    if (state.bazaar.activeView.uniqueId) {
      yield put(unsetActiveViewUniqueId())
    }

    const result = yield bazaarItemsRequestSaga()

    yield put(bazaarItemsSuccess(result))
  } catch (error) {
    yield put(bazaarItemsFailure())
  }
}

export function* onBazaarItemsRequestMoreSaga() {
  try {
    const result = yield bazaarItemsRequestSaga()

    yield put(bazaarItemsMoreSuccess(result))
  } catch (error) {
    yield put(bazaarItemsMoreFailure())
  }
}

export function* onBazaarItemsRequestSameSaga() {
  try {
    const result = yield bazaarItemsRequestSaga()

    yield put(bazaarItemsSameSuccess(result))
  } catch (error) {
    yield put(bazaarItemsSameFailure())
  }
}

export function* onBazaarItemsSortByChangedSaga() {
  try {
    const state: IState = yield select()

    if (state.bazaar.activeView.uniqueId) {
      yield put(unsetActiveViewUniqueId())
    }

    yield put(bazaarItemsUpdateRequest())

    if (state.bazaar.bazaarItems.isLoading) {
      yield delay(150)
    }

    const result = yield bazaarItemsRequestSaga()

    yield put(bazaarItemsSuccess(result))
  } catch (error) {
    yield put(bazaarItemsFailure())
  }
}

export function* onBazaarItemsOrderByChangedSaga() {
  try {
    const state: IState = yield select()

    yield put(bazaarItemsSameUpdateRequest())

    if (state.bazaar.bazaarItems.isLoading) {
      yield delay(150)
    }

    const result = yield bazaarItemsRequestSaga()

    yield put(bazaarItemsSameSuccess(result))
  } catch (error) {
    yield put(bazaarItemsSameFailure())
  }
}

export function* onBazaarItemsSearchChangedSaga() {
  try {
    yield put(bazaarItemsSameUpdateRequest())

    yield delay(250)

    const result = yield bazaarItemsRequestSaga()

    yield put(bazaarItemsSameSuccess(result))
  } catch (error) {
    yield put(bazaarItemsSameFailure())
  }
}
