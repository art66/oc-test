import { put, select } from 'redux-saga/effects'
import { getBazaarRowsData } from '../selectors'
import { IState } from '../../../../store/types'
import { setRowsData } from '../actions'
import { update } from '../../components/Items'

export default function* normalizeItemsSaga() {
  const state: IState = yield select()

  const rowsData = getBazaarRowsData(state)

  yield put(setRowsData(rowsData))

  update._recomputeRowHeights()
  update._forceUpdateGrid()
}
