import {
  INIT_BAZAAR,
  OPEN_BAZAAR,
  CLOSE_BAZAAR,
  OPEN_BAZAAR_REQUEST,
  OPEN_BAZAAR_SUCCESS,
  OPEN_BAZAAR_FAILURE,
  CLOSE_BAZAAR_REQUEST,
  CLOSE_BAZAAR_SUCCESS,
  CLOSE_BAZAAR_FAILURE,
  SORT_BY_CHANGE,
  SEARCH_CHANGE,
  ORDER_BY_CHANGE,
  BAZAAR_ITEMS_REQUEST,
  BAZAAR_ITEMS_SUCCESS,
  BAZAAR_ITEMS_FAILURE,
  BAZAAR_ITEMS_MORE_REQUEST,
  BAZAAR_ITEMS_MORE_SUCCESS,
  BAZAAR_ITEMS_MORE_FAILURE,
  BAZAAR_ITEMS_SAME_REQUEST,
  BAZAAR_ITEMS_SAME_SUCCESS,
  BAZAAR_ITEMS_SAME_FAILURE,
  SET_ACTIVE_VIEW_ID,
  UNSET_ACTIVE_VIEW_ID,
  INITED_BAZAAR,
  REMOVE_ITEM,
  BAZAAR_ITEMS_UPDATE_REQUEST,
  BAZAAR_ITEMS_SAME_UPDATE_REQUEST,
  SET_ROWS_DATA,
  UPDATE_ITEM_AMOUNT,
  UPDATE_FORM_DATA
} from './constants'
import {
  IOpenBazaar,
  ICloseBazaar,
  IOpenBazaarRequest,
  IOpenBazaarSuccess,
  IOpenBazaarFailure,
  ICloseBazaarRequest,
  ICloseBazaarSuccess,
  ICloseBazaarFailure,
  ISortByChange,
  ISearchChange,
  IOrderByChange,
  IBazaarItemsRequest,
  IBazaarItemsSuccess,
  IBazaarItemsFailure,
  IBazaarItemsPayload,
  IBazaarItemsMoreRequest,
  IBazaarItemsMoreSuccess,
  IBazaarItemsMoreFailure,
  IBazaarItemsSameRequest,
  IBazaarItemsSameSuccess,
  IBazaarItemsSameFailure,
  TSetActiveViewAction,
  TInitBazaar,
  TUnsetActiveViewAction,
  TInitedBazaar,
  TRemoveItem,
  TUpdateItemAmount,
  IBazaarItemsUpdateRequest,
  IBazaarItemsSameUpdateRequest,
  TSetRowsData,
  TUpdateFormData,
  TItemImgBarFocus,
  TChangeActiveBuyType
} from './types/actions'
import { IState } from '../../../store/types'

export const initBazaar: TInitBazaar = () => {
  return {
    type: INIT_BAZAAR
  }
}

export const initedBazaar: TInitedBazaar = () => {
  return {
    type: INITED_BAZAAR
  }
}

export const openBazaar = (): IOpenBazaar => {
  return {
    type: OPEN_BAZAAR
  }
}

export const closeBazaar = (): ICloseBazaar => {
  return {
    type: CLOSE_BAZAAR
  }
}

export const openBazaarRequest = (): IOpenBazaarRequest => {
  return {
    type: OPEN_BAZAAR_REQUEST
  }
}

export const openBazaarSuccess = (): IOpenBazaarSuccess => {
  return {
    type: OPEN_BAZAAR_SUCCESS
  }
}

export const openBazaarFailure = (): IOpenBazaarFailure => {
  return {
    type: OPEN_BAZAAR_FAILURE
  }
}

export const closeBazaarRequest = (): ICloseBazaarRequest => {
  return {
    type: CLOSE_BAZAAR_REQUEST
  }
}

export const closeBazaarSuccess = (): ICloseBazaarSuccess => {
  return {
    type: CLOSE_BAZAAR_SUCCESS
  }
}

export const closeBazaarFailure = (): ICloseBazaarFailure => {
  return {
    type: CLOSE_BAZAAR_FAILURE
  }
}

export const sortByChange = (sortBy: ISortByChange['sortBy']): ISortByChange => {
  return {
    type: SORT_BY_CHANGE,
    sortBy
  }
}

export const searchChange = (search: ISearchChange['search']): ISearchChange => {
  return {
    type: SEARCH_CHANGE,
    search
  }
}

export const orderByChange = (orderBy: IOrderByChange['orderBy']): IOrderByChange => {
  return {
    type: ORDER_BY_CHANGE,
    orderBy
  }
}

export const bazaarItemsRequest = (): IBazaarItemsRequest => {
  return {
    type: BAZAAR_ITEMS_REQUEST
  }
}

export const bazaarItemsSuccess = ({ list, total, amount }: IBazaarItemsPayload): IBazaarItemsSuccess => {
  return {
    type: BAZAAR_ITEMS_SUCCESS,
    list,
    total,
    amount
  }
}

export const bazaarItemsFailure = (): IBazaarItemsFailure => {
  return {
    type: BAZAAR_ITEMS_FAILURE
  }
}

export const bazaarItemsUpdateRequest = (): IBazaarItemsUpdateRequest => {
  return {
    type: BAZAAR_ITEMS_UPDATE_REQUEST
  }
}

export const bazaarItemsMoreRequest = (): IBazaarItemsMoreRequest => {
  return {
    type: BAZAAR_ITEMS_MORE_REQUEST
  }
}

export const bazaarItemsMoreSuccess = ({ list, total, amount }: IBazaarItemsPayload): IBazaarItemsMoreSuccess => {
  return {
    type: BAZAAR_ITEMS_MORE_SUCCESS,
    list,
    total,
    amount
  }
}

export const bazaarItemsMoreFailure = (): IBazaarItemsMoreFailure => {
  return {
    type: BAZAAR_ITEMS_MORE_FAILURE
  }
}

export const bazaarItemsSameRequest = (): IBazaarItemsSameRequest => {
  return {
    type: BAZAAR_ITEMS_SAME_REQUEST
  }
}

export const bazaarItemsSameUpdateRequest = (): IBazaarItemsSameUpdateRequest => {
  return {
    type: BAZAAR_ITEMS_SAME_UPDATE_REQUEST
  }
}

export const bazaarItemsSameSuccess = ({ list, total, amount }: IBazaarItemsPayload): IBazaarItemsSameSuccess => {
  return {
    type: BAZAAR_ITEMS_SAME_SUCCESS,
    list,
    total,
    amount
  }
}

export const bazaarItemsSameFailure = (): IBazaarItemsSameFailure => {
  return {
    type: BAZAAR_ITEMS_SAME_FAILURE
  }
}

export const setActiveViewUniqueId: TSetActiveViewAction = uniqueId => {
  return {
    type: SET_ACTIVE_VIEW_ID,
    uniqueId
  }
}

export const unsetActiveViewUniqueId: TUnsetActiveViewAction = () => {
  return {
    type: UNSET_ACTIVE_VIEW_ID
  }
}

export const removeItem: TRemoveItem = uniqueId => {
  return {
    type: REMOVE_ITEM,
    uniqueId
  }
}

export const updateItemAmount: TUpdateItemAmount = (uniqueId, amount) => {
  return {
    type: UPDATE_ITEM_AMOUNT,
    uniqueId,
    amount
  }
}

export const setRowsData: TSetRowsData = ({ rows, totalRows, itemsInRow }) => {
  return {
    type: SET_ROWS_DATA,
    rows,
    totalRows,
    itemsInRow
  }
}

export const updateFormData: TUpdateFormData = (uniqueId, data) => {
  return {
    type: UPDATE_FORM_DATA,
    uniqueId,
    data
  }
}

export const itemImgBarFocus: TItemImgBarFocus = uniqueId => (dispatch, getState) => {
  const {
    bazaar: {
      bazaarItems: { formData }
    }
  }: IState = getState()

  Object.keys(formData).forEach(formDataItemUniqueId => {
    const formDataItem = formData[formDataItemUniqueId]

    if (formDataItem.imgBarFocused) {
      dispatch(updateFormData(formDataItemUniqueId, { imgBarFocused: false }))
    }
  })

  dispatch(updateFormData(uniqueId, { imgBarFocused: true }))
}
// TODO: Should add tests

export const changeActiveBuyType: TChangeActiveBuyType = (uniqueId, activeBuyType) => (dispatch, getState) => {
  const {
    bazaar: {
      bazaarItems: { formData }
    }
  }: IState = getState()

  Object.keys(formData).forEach(formDataItemUniqueId => {
    const formDataItem = formData[formDataItemUniqueId]

    if (formDataItem.activeBuyType) {
      dispatch(updateFormData(formDataItemUniqueId, { activeBuyType: null }))
    }
  })

  dispatch(updateFormData(uniqueId, { activeBuyType }))
}
// TODO: Should add tests
