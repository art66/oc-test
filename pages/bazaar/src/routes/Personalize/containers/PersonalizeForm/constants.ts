import { TActionRadios } from './types'

export const ACTION_RADIOS: TActionRadios = [
  {
    value: '0',
    id: 'bazaar-opened',
    label: 'Open'
  },
  {
    value: '1',
    id: 'bazaar-closed',
    label: 'Closed'
  }
]
