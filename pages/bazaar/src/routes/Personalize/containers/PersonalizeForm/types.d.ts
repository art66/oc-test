import { TFormDataSubmitErorr, IFormData } from '../../modules/types/state'
import { IFormDataChangedProps } from '../../modules/types/actions'
import { TBazaarDataError, IBazaarData } from '../../../../modules/types/state'
import { IState as IReduxState } from '../../../../store/types'
import { TGetContent } from '../../components/FormEditor/types'

export interface IActionRadio {
  value: string
  id: string
  label: string
}

export type TActionRadios = IActionRadio[]

export interface IProps extends TMapStateProps, TMapDispatchToProps {
  isLoading: boolean
  isSubmitting: boolean
  data: IFormData
  loadError: TBazaarDataError
  submitError: TFormDataSubmitErorr
  push: (url: string) => void
}

export interface IState {
  getDescription: TGetContent
}

export type TMapStateProps = {
  isLoading: boolean
  isSubmitting: boolean
  data: IBazaarData
}

export type TMapStateToProps = (state: IReduxState) => TMapStateProps

export type TMapDispatchToProps = {
  formDataChanged: (formData: IFormDataChangedProps) => void
  formDataSubmit: (formData: IFormDataChangedProps, push: (url: string) => void) => void
}
