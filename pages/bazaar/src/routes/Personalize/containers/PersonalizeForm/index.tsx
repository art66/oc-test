import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Button from '@torn/shared/components/Button'
import { Form, FormLabel, FormInput, FormRadio, FormEditor, FormGroup, FormField } from '../../components'
import { formDataChanged as formDataChangedAction, formDataSubmit as formDataSubmitAction } from '../../modules/actions'
import { ACTION_RADIOS } from './constants'
import { IProps, IState, TMapStateToProps, TMapDispatchToProps } from './types'

export class PersonalizeForm extends PureComponent<IProps, IState> {
  static defaultProps: IProps = {
    isLoading: false,
    isSubmitting: false,
    data: {
      name: '',
      description: '',
      action: ''
    },
    loadError: null,
    submitError: null,
    formDataChanged: () => {},
    formDataSubmit: () => {},
    push: () => {}
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      getDescription: () => ''
    }
  }

  _onChangeName = (_event, name) => {
    const { formDataChanged } = this.props

    formDataChanged({ name })
  }

  _onChangeAction = (_event, action) => {
    const { formDataChanged } = this.props

    formDataChanged({ action })
  }

  _onSubmit = () => {
    const {
      data: { name, action },
      formDataSubmit,
      formDataChanged,
      push
    } = this.props
    const { getDescription } = this.state

    const description = getDescription()

    formDataChanged({ description })

    formDataSubmit({ name, action, description }, push)
  }

  _renderActionRadios = (): JSX.Element[] => {
    const {
      data: { action }
    } = this.props

    return ACTION_RADIOS.map(actionRadio => (
      <FormRadio
        key={actionRadio.value}
        id={actionRadio.id}
        name='action'
        value={actionRadio.value}
        onChange={this._onChangeAction}
        checked={action === actionRadio.value}
        ariaLabel={actionRadio.label}
      >
        {actionRadio.label}
      </FormRadio>
    ))
  }

  render() {
    const {
      // isLoading,
      // TODO: Should be a loader, because form data taked asynchronously
      isSubmitting,
      data: { name, description }
    } = this.props

    // @ts-ignore
    const error = window.isInputNameInvalid({ name })

    return (
      <Form onSubmit={this._onSubmit}>
        <FormField single={true}>
          <FormLabel htmlFor='bazaar-name'>Bazaar Name:</FormLabel>
          <FormInput
            id='bazaar-name'
            name='name'
            value={name}
            error={error}
            onChange={this._onChangeName}
            autoComplete='off'
            size={25}
            maxLength={25}
            ariaLabel='Bazaar name'
          />
        </FormField>
        <FormField>
          <FormLabel>Bazaar Description:</FormLabel>
          <FormEditor
            key='description'
            name='description'
            value={description}
            setContentGetter={getDescription => this.setState({ getDescription })}
            iconOutside={true}
            ariaLabel='Bazaar description'
          />
        </FormField>
        <FormGroup inline={true}>
          <Button disabled={isSubmitting || error} aria-label='Change details' type='submit'>
            CHANGE DETAILS
          </Button>
          {this._renderActionRadios()}
        </FormGroup>
      </Form>
    )
  }
}

const mapStateToProps: TMapStateToProps = state => ({
  isLoading: state.common.bazaarData.isLoading,
  isSubmitting: state.personalize.form.isSubmitting,
  data: state.personalize.form.data
})

const mapDispatchToProps: TMapDispatchToProps = {
  formDataChanged: formDataChangedAction,
  formDataSubmit: formDataSubmitAction
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalizeForm)
