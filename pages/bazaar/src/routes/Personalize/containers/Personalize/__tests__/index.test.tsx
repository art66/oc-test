import React from 'react'
import { mount, shallow } from 'enzyme'
import { Personalize } from '..'
import { Panel, PanelContent } from '../../../components'
import PersonalizeForm from '../../PersonalizeForm'
import { defaultState, loadingState, errorState, doesntExistsState } from './mocks'
import { PanelHeader } from '../../../../../components'
import { mockWindowProperty } from '../../../../../../../../utils/test-utils'

describe('<Personalize />', () => {
  const isInputNameInvalid = jest.fn()

  mockWindowProperty('isInputNameInvalid', isInputNameInvalid)

  it('should render completely filled', () => {
    const Component = shallow(<Personalize {...defaultState} />)

    expect(Component.find(Panel).length).toBe(1)
    expect(Component.find(Panel).find(PanelHeader).length).toBe(1)
    expect(Component.find(Panel).find(PanelContent).length).toBe(1)
    expect(Component.find(Panel).find(PanelContent).find(PersonalizeForm).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render on loading', () => {
    const Component = mount(<Personalize {...loadingState} />)

    expect(Component.html()).toBe(null)

    expect(Component).toMatchSnapshot()
  })

  it('should render hidden', () => {
    const Component = mount(<Personalize {...errorState} />)

    expect(Component.html()).toBe(null)

    expect(Component).toMatchSnapshot()
  })

  it('should render doesn\'t exists state', () => {
    const Component = mount(<Personalize {...doesntExistsState} />)

    expect(Component.html()).toBe(null)

    expect(Component).toMatchSnapshot()
  })
})
