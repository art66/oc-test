import { IProps } from '../../types'

export const defaultState: IProps = {
  isLoading: false,
  isShowing: true,
  isBazaarExists: true,
  initPersonalize: () => {},
  setPageId: () => {},
  history: {
    push: () => {}
  }
}

export const loadingState: IProps = {
  ...defaultState,
  isLoading: true
}

export const errorState: IProps = {
  ...defaultState,
  isShowing: false
}

export const doesntExistsState: IProps = {
  ...defaultState,
  isBazaarExists: false
}

export const defaultStore = {
  common: {
    bazaarData: {
      isLoading: false,
      data: {
        name: '',
        description: '',
        action: ''
      },
      error: null
    }
  },
  personalize: {
    form: {
      isSubmitting: false,
      data: {
        name: '',
        description: '',
        action: ''
      },
      error: null
    }
  }
}
