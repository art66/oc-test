import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { initPersonalize as initPersonalizeAction } from '../../modules/actions'
import { Panel, PanelContent } from '../../components'
import PersonalizeForm from '../PersonalizeForm'
import { IProps, TMapStateToProps, TMapDispatchToProps } from './types'
import { PERSONALIZE_PAGE_ID } from '../../../../constants'
import { setPageId as setPageIdAction } from '../../../../modules/actions'
import { PanelHeader } from '../../../../components'

export class Personalize extends PureComponent<IProps> {
  componentDidMount() {
    const { initPersonalize, setPageId } = this.props

    initPersonalize()

    setPageId(PERSONALIZE_PAGE_ID)
  }

  render() {
    const { isLoading, isShowing, isBazaarExists } = this.props

    if (!isShowing || isLoading || !isBazaarExists) {
      return null
    }

    return (
      <Panel>
        <PanelHeader>Personalize</PanelHeader>
        <PanelContent>
          <PersonalizeForm push={this.props.history.push} />
        </PanelContent>
      </Panel>
    )
  }
}

const mapStateToProps: TMapStateToProps = state => {
  return {
    isLoading: state.common.bazaarData.isLoading,
    isShowing: !!state.common.owner.data.userId,
    isBazaarExists: state.common.metadata.data?.isBazaarExists
  }
}

const mapDispatchToProps: TMapDispatchToProps = {
  initPersonalize: initPersonalizeAction,
  setPageId: setPageIdAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Personalize)
