import { IState } from '../../../../store/types'
import { TPageID } from '../../../../modules/types/state'

export interface IProps extends TMapStateProps, TMapDispatchToProps {
  history: {
    push: (url: string) => void
  }
}

export type TMapStateProps = {
  isLoading: boolean
  isShowing: boolean
  isBazaarExists: boolean
}

export type TMapStateToProps = (state: IState) => TMapStateProps

export type TMapDispatchToProps = {
  initPersonalize: () => void
  setPageId: (pageId: TPageID) => void
}
