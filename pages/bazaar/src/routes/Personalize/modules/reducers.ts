import { createReducer } from '../../../modules/utils'
import {
  PERSONALIZED_FORM_DATA_CHANGED,
  FORM_DATA_SUBMIT,
  FORM_DATA_SUBMIT_SUCCESS,
  FORM_DATA_SUBMIT_FAILURE
} from './constants'
import { IPersonalizeState } from './types/state'
import { IFormDataChanged, IFormDataSubmitSuccess } from './types/actions'

export const initialState: IPersonalizeState = {
  form: {
    isSubmitting: false,
    data: {
      name: '',
      description: '',
      action: ''
    },
    error: null
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------

const ACTION_HANDLERS = {
  [PERSONALIZED_FORM_DATA_CHANGED]: (state: IPersonalizeState, action: IFormDataChanged): IPersonalizeState => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          ...action.formData
        }
      }
    }
  },
  [FORM_DATA_SUBMIT]: (state: IPersonalizeState): IPersonalizeState => {
    return {
      ...state,
      form: {
        ...state.form,
        isSubmitting: true,
        error: null
      }
    }
  },
  [FORM_DATA_SUBMIT_SUCCESS]: (state: IPersonalizeState, action: IFormDataSubmitSuccess): IPersonalizeState => {
    return {
      ...state,
      form: {
        ...state.form,
        data: action.formData,
        isSubmitting: false
      }
    }
  },
  [FORM_DATA_SUBMIT_FAILURE]: (state: IPersonalizeState): IPersonalizeState => {
    return {
      ...state,
      form: {
        ...state.form,
        isSubmitting: false,
        error: true
      }
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------

export default createReducer(initialState, ACTION_HANDLERS)
