import {
  PERSONALIZED_FORM_DATA_CHANGED,
  FORM_DATA_SUBMIT,
  FORM_DATA_SUBMIT_SUCCESS,
  FORM_DATA_SUBMIT_FAILURE,
  INIT_PERSONALIZE
} from './constants'
import {
  IFormDataChanged,
  IFormDataSubmit,
  IFormDataSubmitSuccess,
  IFormDataSubmitFailure,
  TInitPersonalize
} from './types/actions'

export const initPersonalize: TInitPersonalize = () => {
  return {
    type: INIT_PERSONALIZE
  }
}

export const formDataChanged = (formData): IFormDataChanged => {
  return {
    type: PERSONALIZED_FORM_DATA_CHANGED,
    formData
  }
}

export const formDataSubmit = (formData, push): IFormDataSubmit => {
  return {
    type: FORM_DATA_SUBMIT,
    formData,
    push
  }
}

export const formDataSubmitSuccess = (formData): IFormDataSubmitSuccess => {
  return {
    type: FORM_DATA_SUBMIT_SUCCESS,
    formData
  }
}

export const formDataSubmitFailure = (): IFormDataSubmitFailure => {
  return {
    type: FORM_DATA_SUBMIT_FAILURE
  }
}
