import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { formDataSubmitSuccess, formDataSubmitFailure } from '../actions'
import { setBazaarData, showInfoBox } from '../../../../modules/actions'
import { FORM_DATA_SUBMIT_URL } from '../constants'
import { IFormDataSubmit } from '../types/actions'

const normalizeFormSubmitData = ({ name, description, action }) => ({
  name,
  descr: description,
  action,
  step: 'changePersonalize'
})

export default function* formDataSubmit(action: IFormDataSubmit) {
  try {
    yield put(showInfoBox({ loading: true }))

    const normalizedFormSubmitData = normalizeFormSubmitData(action.formData)

    const data = yield fetchUrl(FORM_DATA_SUBMIT_URL, normalizedFormSubmitData, null)

    if (!data.success) {
      throw data.text
    }

    yield put(formDataSubmitSuccess(action.formData))
    yield put(setBazaarData(action.formData))

    yield put(showInfoBox({ loading: false, msg: data.text, color: 'green' }))

    action.push('/')
  } catch (error) {
    const message = error.message || error.toString()

    yield put(formDataSubmitFailure())

    yield put(showInfoBox({ loading: false, msg: message, color: 'red' }))
  }
}
