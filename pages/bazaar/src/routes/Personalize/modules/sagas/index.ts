import pageDataRequest from './pageDataRequest'
import formDataSubmit from './formDataSubmit'

export { pageDataRequest, formDataSubmit }
