import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  metadataRequest,
  metadataResponseSuccess,
  metadataResponseFailure,
  ownerRequest,
  ownerResponseSuccess,
  ownerResponseFailure,
  bazaarDataRequest,
  bazaarDataResponseSuccess,
  bazaarDataResponseFailure,
  showInfoBox,
  setRequestInfoBox,
  leaveRequestInfoBox
} from '../../../../modules/actions'
import { normalizeMetadata, normalizeOwner, normalizeBazaarData } from '../../../../modules/normalizers'
import { hideInfobox } from '../../../../modules/sagas'
import { PERSONALIZE_FORM_DATA_URL } from '../../../../constants'
import { formDataChanged } from '../actions'
import { normalizePersonalizeFormData } from '../normalizers'

export default function* pageDataRequest() {
  try {
    yield put(showInfoBox(null))
    yield put(setRequestInfoBox({ loading: true }))
    yield put(metadataRequest())
    yield put(ownerRequest())
    yield put(bazaarDataRequest())

    const personalizeData = yield fetchUrl(PERSONALIZE_FORM_DATA_URL, null, null)

    const normalizedMetadata = normalizeMetadata(personalizeData)
    const normalizedOwner = normalizeOwner(personalizeData)
    const normalizedBazaarData = normalizeBazaarData(personalizeData)

    yield put(metadataResponseSuccess(normalizedMetadata))
    yield put(ownerResponseSuccess(normalizedOwner))
    yield put(bazaarDataResponseSuccess(normalizedBazaarData))

    if (personalizeData.state.isBazaarExists) {
      const normalizedPersonalizeFormData = normalizePersonalizeFormData(personalizeData)

      yield put(formDataChanged(normalizedPersonalizeFormData))
    }

    if (personalizeData.error) {
      yield put(setRequestInfoBox({ msg: personalizeData.error, color: 'red' }))
    } else if (personalizeData.infobox.message) {
      yield put(setRequestInfoBox({ msg: personalizeData.infobox.message, color: personalizeData.infobox.color }))
    } else {
      yield hideInfobox()
    }

    yield put(leaveRequestInfoBox(true))

    // FIXME: Should redirect
  } catch (error) {
    const message = error.message || error.toString()

    yield put(metadataResponseFailure())
    yield put(ownerResponseFailure())
    yield put(bazaarDataResponseFailure())

    yield put(setRequestInfoBox({ msg: message, color: 'red' }))
  }
}
