import {
  FORM_DATA_CHANGED,
  FORM_DATA_SUBMIT,
  FORM_DATA_SUBMIT_SUCCESS,
  FORM_DATA_SUBMIT_FAILURE,
  INIT_PERSONALIZE
} from '../constants'
import { IFormData } from './state'

export type TInitPersonalizePayload = {
  type: typeof INIT_PERSONALIZE
}

export type TInitPersonalize = () => TInitPersonalizePayload

export interface IFormDataChangedProps {
  name?: string
  description?: string
  action?: string
}

export interface IFormDataChanged {
  type: typeof FORM_DATA_CHANGED
  formData: IFormDataChangedProps
}

export interface IFormDataSubmit {
  type: typeof FORM_DATA_SUBMIT
  formData: IFormData
  push: (url: string) => void
}

export interface IFormDataSubmitSuccess {
  type: typeof FORM_DATA_SUBMIT_SUCCESS
  formData: IFormData
}

export interface IFormDataSubmitFailure {
  type: typeof FORM_DATA_SUBMIT_FAILURE
}
