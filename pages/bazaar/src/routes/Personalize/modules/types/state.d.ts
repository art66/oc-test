export interface IFormData {
  name: string
  description: string
  action: string
}

export type TFormDataSubmitErorr = boolean | null

export interface IForm {
  isSubmitting: boolean
  data: IFormData
  error: TFormDataSubmitErorr
}

export interface IPersonalizeState {
  form: IForm
}
