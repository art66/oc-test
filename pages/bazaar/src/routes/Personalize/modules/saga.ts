import { takeLatest, debounce } from 'redux-saga/effects'

import { pageDataRequest, formDataSubmit } from './sagas'
import { FORM_DATA_SUBMIT, INIT_PERSONALIZE } from './constants'
import setPageIdSaga from '../../../modules/sagas/setPageId'
import { PERSONALIZE_PAGE_ID } from '../../../constants'

export default function* personalizeRoot() {
  yield takeLatest(INIT_PERSONALIZE, pageDataRequest)
  yield takeLatest(INIT_PERSONALIZE, setPageIdSaga, PERSONALIZE_PAGE_ID)
  yield debounce(100, FORM_DATA_SUBMIT, formDataSubmit)
}
