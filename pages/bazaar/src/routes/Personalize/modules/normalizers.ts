import lodashUnescape from 'lodash.unescape'

export const normalizePersonalizeFormData = ({ personalize }) => {
  if (!personalize) {
    return {
      name: '',
      description: '',
      action: null
    }
  }

  return {
    name: personalize.bazaarname,
    description: lodashUnescape(personalize.bazaardesc),
    action: personalize.bazaarclosed
  }
}
