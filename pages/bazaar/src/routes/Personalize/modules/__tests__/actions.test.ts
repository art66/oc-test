import {
  formDataChanged,
  formDataSubmit,
  formDataSubmitSuccess,
  formDataSubmitFailure,
  initPersonalize
} from '../actions'
import {
  PERSONALIZED_FORM_DATA_CHANGED,
  FORM_DATA_SUBMIT,
  FORM_DATA_SUBMIT_SUCCESS,
  FORM_DATA_SUBMIT_FAILURE,
  INIT_PERSONALIZE
} from '../constants'
import { formData } from './mocks'

describe('bazaar personalize modules actions', () => {
  it('should handle initPersonalize action', () => {
    const expectedAction = {
      type: INIT_PERSONALIZE
    }

    expect(initPersonalize()).toEqual(expectedAction)
  })

  it('should handle formDataChanged action', () => {
    const expectedAction = {
      type: PERSONALIZED_FORM_DATA_CHANGED,
      formData
    }

    expect(formDataChanged(formData)).toEqual(expectedAction)
  })

  it('should handle formDataSubmit action', () => {
    const push = () => {}
    const expectedAction = {
      type: FORM_DATA_SUBMIT,
      formData,
      push
    }

    expect(formDataSubmit(formData, push)).toEqual(expectedAction)
  })

  it('should handle formDataSubmitSuccess action', () => {
    const expectedAction = {
      type: FORM_DATA_SUBMIT_SUCCESS,
      formData
    }

    expect(formDataSubmitSuccess(formData)).toEqual(expectedAction)
  })

  it('should handle formDataSubmitFailure action', () => {
    const expectedAction = {
      type: FORM_DATA_SUBMIT_FAILURE
    }

    expect(formDataSubmitFailure()).toEqual(expectedAction)
  })
})
