export const PERSONALIZE_INFO_MESSAGE =
  'Here you can give your bazaar a name and a description, people will see these when they come to your shop.'
