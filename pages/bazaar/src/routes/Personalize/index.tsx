import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import rootStore from '../../store/createStore'
import reducer from './modules/reducers'
import saga from './modules/saga'

const Preloader = () => <AnimationLoad />

export default Loadable({
  loader: async () => {
    const Personalize = await import('./containers/Personalize')

    injectReducer(rootStore, { key: 'personalize', reducer })

    injectSaga({ key: 'personalize', saga })

    return Personalize
  },
  loading: Preloader
})
