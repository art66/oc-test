import React from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

export class FormRadio extends React.PureComponent<IProps> {
  static defaultProps = {
    value: ''
  }

  _onChange = event => {
    const { onChange } = this.props
    const {
      target: { value }
    } = event

    onChange(event, value)
  }

  render() {
    const { id, name, children, value, checked, ariaLabel } = this.props

    return (
      <div>
        <input
          id={id}
          className={styles.formRadio}
          type="radio"
          name={name}
          value={value}
          onChange={this._onChange}
          checked={checked}
          arial-label={ariaLabel}
          tabIndex={0}
        />
        <label className={styles.formRadioLabel} htmlFor={id}>
          {children}
        </label>
      </div>
    )
  }
}

export default FormRadio
