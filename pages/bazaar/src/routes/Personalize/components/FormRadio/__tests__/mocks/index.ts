import { IProps } from '../../types'

const initialState: IProps = {
  id: 'test-id',
  name: 'testName',
  children: 'Test label',
  value: 'test_value',
  onChange: () => {},
  checked: true
}

export const emptyState: IProps = {}

export default initialState
