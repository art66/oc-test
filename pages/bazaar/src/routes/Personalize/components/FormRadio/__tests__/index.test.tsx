import React from 'react'
import { mount } from 'enzyme'

import FormRadio from '../'
import initialState, { emptyState } from './mocks'

describe('<FormRadio />', () => {
  it('should render completely filled', () => {
    const Component = mount(<FormRadio {...initialState} />)

    expect(Component.find('.formRadio').length).toBe(1)
    expect(Component.find('.formRadioLabel').length).toBe(1)
    expect(Component.find('.formRadio').prop('id')).toBe('test-id')
    expect(Component.find('.formRadio').prop('type')).toBe('radio')
    expect(Component.find('.formRadio').prop('name')).toBe('testName')
    expect(Component.find('.formRadio').prop('value')).toBe('test_value')
    expect(Component.find('.formRadio').prop('checked')).toBe(true)
    expect(Component.find('.formRadioLabel').prop('htmlFor')).toBe('test-id')
    expect(Component.find('.formRadioLabel').text()).toBe('Test label')

    expect(Component).toMatchSnapshot()
  })

  it('should render default values', () => {
    const Component = mount(<FormRadio {...emptyState} />)

    expect(Component.find('.formRadio').length).toBe(1)
    expect(Component.find('.formRadioLabel').length).toBe(1)
    expect(Component.find('.formRadio').prop('value')).toBe('')

    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener', () => {
    const onChange = jest.fn()
    const value = 'test_value'
    const event = { target: { value } }
    const state = { ...initialState, onChange }
    const Component = mount(<FormRadio {...state} />)

    expect(Component.find('.formRadio').length).toBe(1)

    Component.find('.formRadio').simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][1]).toBe('test_value')
  })
})
