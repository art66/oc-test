export interface IProps {
  id?: string
  name?: string
  children?: string
  value?: string
  onChange?: (event: React.ChangeEvent<HTMLInputElement>, value: string) => void
  checked?: boolean
  ariaLabel?: string
}
