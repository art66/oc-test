import React from 'react'
import { mount } from 'enzyme'

import PanelContent from '../'

describe('<PanelContent />', () => {
  it('should render with children component', () => {
    const Children = () => <div className='unique' />
    const Component = mount(
      <PanelContent>
        <Children />
      </PanelContent>
    )

    expect(Component.find('.panelContent').length).toBe(1)
    expect(Component.find('.panelContent').find(Children).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
