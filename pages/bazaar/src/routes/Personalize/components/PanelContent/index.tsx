import React from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const PanelContent: React.SFC<IProps> = (props): JSX.Element => {
  const { children } = props

  return <div className={styles.panelContent}>{children}</div>
}

export default PanelContent
