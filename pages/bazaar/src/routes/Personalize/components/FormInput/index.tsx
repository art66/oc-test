import React from 'react'
import cn from 'classnames'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

export class FormInput extends React.PureComponent<IProps> {
  static defaultProps = {
    type: 'text',
    value: '',
    autoComplete: 'on'
  }

  _onChange = event => {
    const { onChange } = this.props
    const {
      target: { value }
    } = event

    onChange(event, value)
  }

  render() {
    const { id, name, type, value, maxLength, size, autoComplete, ariaLabel, error } = this.props

    return (
      <input
        id={id}
        className={cn(styles.formInput, error && styles.error)}
        type={type}
        name={name}
        value={value}
        onChange={this._onChange}
        maxLength={maxLength}
        size={size}
        autoComplete={autoComplete}
        aria-label={ariaLabel}
        data-lpignore='true'
      />
    )
  }
}

export default FormInput
