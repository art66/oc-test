import { IProps } from '../../types'

const initialState: IProps = {
  id: 'bazaar-name',
  type: 'text',
  name: 'testName',
  value: 'test bazaar',
  onChange: () => {},
  autoComplete: 'off',
  size: 50,
  maxLength: 50
}

export const emptyState: IProps = {}

export default initialState
