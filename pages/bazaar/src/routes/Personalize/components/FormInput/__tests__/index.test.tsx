import React from 'react'
import { mount } from 'enzyme'

import FormInput from '../'
import initialState, { emptyState } from './mocks'

describe('<FormInput />', () => {
  it('should render completely filled', () => {
    const Component = mount(<FormInput {...initialState} />)

    expect(Component.find('.formInput').length).toBe(1)

    expect(Component.find('.formInput').prop('id')).toBe(initialState.id)
    expect(Component.find('.formInput').prop('type')).toBe(initialState.type)
    expect(Component.find('.formInput').prop('name')).toBe(initialState.name)
    expect(Component.find('.formInput').prop('value')).toBe(initialState.value)
    expect(Component.find('.formInput').prop('autoComplete')).toBe(initialState.autoComplete)
    expect(Component.find('.formInput').prop('size')).toBe(initialState.size)
    expect(Component.find('.formInput').prop('maxLength')).toBe(initialState.maxLength)

    expect(Component).toMatchSnapshot()
  })

  it('should render default values', () => {
    const Component = mount(<FormInput {...emptyState} />)

    expect(Component.find('.formInput').length).toBe(1)

    expect(Component.find('.formInput').prop('type')).toBe('text')
    expect(Component.find('.formInput').prop('value')).toBe('')
    expect(Component.find('.formInput').prop('autoComplete')).toBe('on')

    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener', () => {
    const onChange = jest.fn()
    const value = 'test value'
    const event = { target: { value } }
    const state = { ...initialState, onChange }
    const Component = mount(<FormInput {...state} />)

    expect(Component.find('.formInput').length).toBe(1)

    Component.find('.formInput').simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][1]).toBe('test value')
  })
})
