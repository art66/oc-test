type TType = 'text'

type TValue = string

type TAutoComplete = 'on' | 'off'

export interface IProps {
  id?: string
  type?: TType
  name?: string
  value?: TValue
  onChange?: (event: React.ChangeEvent<HTMLInputElement>, value: TValue) => void
  maxLength?: number
  size?: number
  autoComplete?: TAutoComplete
  ariaLabel?: string
  error: boolean
}
