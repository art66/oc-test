export interface IProps {
  children: string | React.ReactChild | React.ReactChild[]
  htmlFor?: string
  ariaLabel?: string
}
