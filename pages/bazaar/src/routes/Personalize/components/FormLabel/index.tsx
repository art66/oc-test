import React from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const FormLabel: React.SFC<IProps> = (props): JSX.Element => {
  const { children, htmlFor, ariaLabel } = props

  return (
    <label className={styles.formLabel} htmlFor={htmlFor} aria-label={ariaLabel}>
      {children}
    </label>
  )
}

export default FormLabel
