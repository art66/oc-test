import React from 'react'
import { mount } from 'enzyme'

import FormLabel from '../'

describe('<FormLabel />', () => {
  it('should render with children component', () => {
    const description = 'Test text'
    const Component = mount(<FormLabel>{description}</FormLabel>)

    expect(Component.find('.formLabel').length).toBe(1)
    expect(Component.find('.formLabel').text()).toBe(description)

    expect(Component).toMatchSnapshot()
  })
})
