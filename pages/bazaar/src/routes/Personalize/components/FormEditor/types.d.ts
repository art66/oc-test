type TValue = string

export type TGetContent = () => string

export interface IProps {
  id?: string
  name?: string
  value?: TValue
  iconOutside?: boolean
  ariaLabel?: string
  onChange?: (event: React.ChangeEvent<HTMLInputElement>, value: TValue) => void
  setContentGetter: (TGetContent) => void
}

export interface IState {
  active: boolean
}
