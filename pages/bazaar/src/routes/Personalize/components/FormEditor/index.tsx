import React, { RefObject } from 'react'
import cn from 'classnames'
import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'

export class FormEditor extends React.PureComponent<IProps, IState> {
  editor: { destroy: () => void } = null
  containerRef: RefObject<HTMLDivElement> = React.createRef()

  static defaultProps = {
    value: ''
  }

  constructor(props: any) {
    super(props)

    this.state = {
      active: false
    }
  }

  componentDidMount() {
    setTimeout(() => {
      // @ts-ignore: Cannot find name 'initializeTinyMCE'
      initializeTinyMCE(null, {
        selector: '#bazaarPersonalize',
        afterInit: editor => {
          const { setContentGetter } = this.props
          const { active } = this.state

          if (!active) {
            const iframe = document.querySelector(
              '.mce-tinymce.mce-container.mce-panel .mce-container-body > .mce-edit-area.mce-panel iframe'
            )

            if (!iframe) return

            iframe.removeAttribute('title')
            iframe.setAttribute('tabindex', '0')
            iframe.setAttribute('aria-label', 'Rich Text Area. Press ALT-INSERT to start writing')
          }

          if (editor) {
            this.editor = editor

            setContentGetter(() => editor.getContent())
          }
        }
      })
    })
  }

  componentWillUnmount() {
    this.editor?.destroy()
  }

  getFormEditorClassName = () => {
    const { iconOutside } = this.props
    const { active } = this.state

    return cn(styles.formEditor, { [styles.active]: active, [styles.iconOutside]: iconOutside })
  }

  getTogglerAriaLabel = () => {
    const { active } = this.state

    return `${active ? 'Hide' : 'Show'} text editor toolbar`
  }

  _handleActivatorClick = event => {
    const { active } = this.state

    event.preventDefault()

    this.setState({ active: !active })

    const editorContainerElement = this.containerRef?.current?.querySelector(
      '.mce-tinymce .mce-container-body > .mce-edit-area.mce-panel iframe'
    )

    if (active) {
      editorContainerElement?.classList.add('mce-minimized')
    } else {
      editorContainerElement?.classList.remove('mce-minimized')
    }
  }

  render() {
    const { value, ariaLabel } = this.props

    return (
      <div ref={this.containerRef} className={this.getFormEditorClassName()}>
        <button
          type='button'
          className={styles.activator}
          onClick={this._handleActivatorClick}
          aria-label={this.getTogglerAriaLabel()}
        >
          <i className={styles.icon} />
        </button>
        <textarea className='tiny-area' id='bazaarPersonalize' defaultValue={value} aria-label={ariaLabel} />
      </div>
    )
  }
}

export default FormEditor
