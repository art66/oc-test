import React from 'react'
import cn from 'classnames'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const Panel: React.SFC<IProps> = (props): JSX.Element => {
  const { children, className } = props

  return <div className={cn(styles.panel, className)}>{children}</div>
}

export default Panel
