import React from 'react'
import { mount } from 'enzyme'

import Panel from '../'

describe('<Panel />', () => {
  it('should render with children component', () => {
    const Children = () => <div className='unique' />
    const Component = mount(
      <Panel>
        <Children />
      </Panel>
    )

    expect(Component.find('.panel').length).toBe(1)
    expect(Component.find('.panel').find(Children).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
