export interface IProps {
  children: (JSX.Element | JSX.Element[])[]
  inline?: boolean
}
