import React from 'react'
import { mount } from 'enzyme'

import FormGroup from '../'
import initialState, { inlineState } from './mocks'

describe('<FormGroup />', () => {
  it('should render children', () => {
    const FirstChildren = () => <div className='unique' />
    const SecondChildren = () => <div className='unique' />
    const Component = mount(
      <FormGroup {...initialState}>
        <FirstChildren />
        <SecondChildren />
      </FormGroup>
    )

    expect(Component.find('.formGroup').length).toBe(1)
    expect(Component.find('.formGroup').find(FirstChildren).length).toBe(1)
    expect(Component.find('.formGroup').find(SecondChildren).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render inline', () => {
    const FirstChildren = () => <div className='unique' />
    const SecondChildren = () => <div className='unique' />
    const Component = mount(
      <FormGroup {...inlineState}>
        <FirstChildren />
        <SecondChildren />
      </FormGroup>
    )

    expect(Component.find('.formGroup').length).toBe(1)
    expect(Component.find('.formGroup.inline').length).toBe(1)
    expect(Component.find('.formGroup').find(FirstChildren).length).toBe(1)
    expect(Component.find('.formGroup').find(SecondChildren).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
