const initialState = {}

export const inlineState = {
  ...initialState,
  inline: true
}

export default initialState
