// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'formGroup': string;
  'globalSvgShadow': string;
  'inline': string;
}
export const cssExports: CssExports;
export default cssExports;
