import React from 'react'
import cn from 'classnames'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const FornGroup: React.SFC<IProps> = (props): JSX.Element => {
  const { children, inline } = props

  const className = cn(styles.formGroup, { [styles.inline]: inline })

  return <div className={className}>{children}</div>
}

export default FornGroup
