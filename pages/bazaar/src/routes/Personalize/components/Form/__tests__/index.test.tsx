import React from 'react'
import { mount } from 'enzyme'

import Form from '../'

describe('<Form />', () => {
  it('should render with children component', () => {
    const FirstChildren = () => <div className='unique' />
    const SecondChildren = () => <div className='unique' />
    const Component = mount(
      <Form>
        <FirstChildren />
        <SecondChildren />
      </Form>
    )

    expect(Component.find('form').length).toBe(1)
    expect(Component.find('form').find(FirstChildren).length).toBe(1)
    expect(Component.find('form').find(SecondChildren).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should call onSubmit listener', () => {
    const onSubmit = jest.fn()
    const preventDefault = jest.fn()
    const event = { preventDefault }
    const state = { onSubmit }
    const Children = () => <div className='unique' />
    const Component = mount(
      <Form {...state}>
        <Children />
      </Form>
    )

    expect(Component.find('form').length).toBe(1)

    Component.find('form').simulate('submit', event)

    expect(preventDefault).toHaveBeenCalledTimes(1)
    expect(onSubmit).toHaveBeenCalledTimes(1)

    expect(preventDefault).toMatchSnapshot()
  })
})
