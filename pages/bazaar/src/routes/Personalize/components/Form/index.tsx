import React from 'react'

import { IProps } from './types'

export class Form extends React.PureComponent<IProps> {
  _onSubmit = event => {
    const { onSubmit } = this.props

    event.preventDefault()

    onSubmit(event)
  }

  render() {
    const { children } = this.props

    return <form onSubmit={this._onSubmit}>{children}</form>
  }
}

export default Form
