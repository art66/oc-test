export interface IProps {
  children: React.ReactChild[] | React.ReactChild
  onSubmit?: (event: React.FormEvent<HTMLFormElement>) => void
}
