import React from 'react'
import { mount } from 'enzyme'

import FormField from '../'

describe('<FormField />', () => {
  it('should render with children component', () => {
    const FirstChildren = () => <div className='unique' />
    const SecondChildren = () => <div className='unique' />
    const Component = mount(
      <FormField>
        <FirstChildren />
        <SecondChildren />
      </FormField>
    )

    expect(Component.find('.formField').length).toBe(1)
    expect(Component.find('.formField').find(FirstChildren).length).toBe(1)
    expect(Component.find('.formField').find(SecondChildren).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
