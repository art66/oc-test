import React from 'react'
import cn from 'classnames'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const FormField: React.SFC<IProps> = (props): JSX.Element => {
  const { children, single } = props

  const className = cn(styles.formField, { [styles.single]: single })

  return <div className={className}>{children}</div>
}

export default FormField
