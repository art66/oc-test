import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import actions from '../../../modules/actions'
import { ADD_PAGE_ID } from '../../../constants'

declare function Inventory(
  node: any,
  options: {
    submitChangeLogHandler: (logs: any, html: string) => void
  }
): void

export class Add extends PureComponent<any> {
  inventoryInstance = null
  componentDidMount() {
    const { setPageId } = this.props

    setPageId(ADD_PAGE_ID)

    // eslint-disable-next-line no-undef
    Inventory(this.inventoryInstance, {
      submitChangeLogHandler: this.submitChangeLogHandler
    })
  }

  submitChangeLogHandler = (logs, _htmlLogs) => {
    console.log(logs)
    const { history } = this.props

    history.push({
      pathname: '/manage',
      state: logs
    })
  }

  render() {
    return <div ref={this.bindInventoryRef} />
  }

  bindInventoryRef = instance => {
    this.inventoryInstance = instance
  }
}

const mapStateToProps = null

const mapDispatchToProps = {
  setPageId: actions.setPageId
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Add)
