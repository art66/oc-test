import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

import { injectReducer } from '../../store/rootReducer'
import { injectSaga } from '../../store/middleware/saga'
import rootStore from '../../store/createStore'
import reducer from './modules/reducers'
import saga from './modules/saga'

const Preloader = () => <AnimationLoad />

export default Loadable({
  loader: async () => {
    const Manage = await import('./containers/Manage')

    injectReducer(rootStore, { key: 'manage', reducer })

    injectSaga({ key: 'manage', saga })

    return Manage
  },
  loading: Preloader
})
