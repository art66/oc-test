import { LogError } from '../../../../helpers'

export function normalizeError(
  error: string | LogError
): {
  message: string
  log?: string[]
} {
  if (typeof error === 'string') {
    return { message: error }
  }

  return error
}
