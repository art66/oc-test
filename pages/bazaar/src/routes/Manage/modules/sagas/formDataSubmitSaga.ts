import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { SUBMIT_FORMDATA_URL } from './constants'
import { submitFormDataFailure, submitFormDataSuccess, itemsRequest } from '../actions'
import { IState } from '../../../../store/types'
import { normalizeFormData } from '../normalizers'
import { showInfoBox } from '../../../../modules/actions'
import { normalizeError } from './normalizers'
import { LogError } from '../../../../helpers'

export default function* formDataSubmitSaga() {
  try {
    yield put(showInfoBox({ loading: true }))

    const {
      manage: {
        items: { list },
        form: { data: formData }
      }
    }: IState = yield select()

    const normalizedFormData = normalizeFormData(formData, list)

    const responseData = yield fetchUrl(SUBMIT_FORMDATA_URL, normalizedFormData, null)

    if (!responseData.success) {
      throw new LogError(responseData.error || responseData.text, responseData.errors || responseData.logs)
    } else {
      yield put(showInfoBox({ loading: false, msg: responseData.text, color: 'green', log: responseData.logs }))
    }

    yield put(submitFormDataSuccess())

    yield put(itemsRequest())
  } catch (error) {
    const { message, log } = normalizeError(error)

    yield put(submitFormDataFailure())

    yield put(showInfoBox({ loading: false, msg: message, color: 'red', log }))
  }
}
