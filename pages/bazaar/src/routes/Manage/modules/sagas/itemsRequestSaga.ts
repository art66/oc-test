import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { itemsSuccessAction, itemsFailureAction, itemsRequestMoreSuccess, itemsRequestMoreFailure } from '../actions'
import normalizeItemsData from '../../../../modules/normalizers/normalizeItemsData'
import getStateOnOwnerReceived from '../../../../modules/sagas/getStateOnOwnerReceived'
import { IState } from '../../../../store/types'
import { createItemsRequestUrl } from './urls'
import { showInfoBox } from '../../../../modules/actions'

export function* itemsRequest() {
  try {
    const state: IState = yield getStateOnOwnerReceived()
    const { loadFrom, limit } = state.manage.items

    const itemsData = yield fetchUrl(
      createItemsRequestUrl({
        loadFrom,
        userId: state.common.owner.data.userId,
        limit
      }),
      null,
      null
    )

    if (itemsData.error) {
      throw itemsData.error
    }

    const normalizedItemsData = normalizeItemsData({ uniqueKey: Date.now().toString(), ...itemsData })

    return normalizedItemsData
  } catch (error) {
    const message = error.message || error.toString()

    yield put(showInfoBox({ msg: message, color: 'red' }))

    throw error
  }
}

export function* itemsRequestSaga() {
  try {
    const result = yield itemsRequest()

    yield put(itemsSuccessAction(result))
  } catch (error) {
    yield put(itemsFailureAction())
  }
}

export function* itemsRequestMoreSaga() {
  try {
    const result = yield itemsRequest()

    yield put(itemsRequestMoreSuccess(result))
  } catch (error) {
    yield put(itemsRequestMoreFailure())
  }
}
