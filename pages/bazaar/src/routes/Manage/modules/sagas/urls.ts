import qs from 'qs'
import { ITEMS_REQUEST_URL } from './constants'

export const createItemsRequestUrl = ({ loadFrom, userId, limit }) =>
  `${ITEMS_REQUEST_URL}&${qs.stringify({
    start: loadFrom,
    ID: userId,
    categorised: 0,
    limit
  })}`
