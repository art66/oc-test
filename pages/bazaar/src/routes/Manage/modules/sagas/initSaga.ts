import { put, all, select } from 'redux-saga/effects'
import { IState } from '../../../../store/types'
import { showInfoBox, setRequestInfoBox } from '../../../../modules/actions'
import pageDataRequestSaga from './pageDataRequestSaga'
import { itemsRequestSaga } from './itemsRequestSaga'

export function* initManageItemsSaga() {
  try {
    yield put(showInfoBox(null))
    yield put(setRequestInfoBox({ loading: true }))

    yield all([pageDataRequestSaga(), itemsRequestSaga()])

    const state: IState = yield select()

    yield put(setRequestInfoBox({ ...state.common.infoBox, loading: false }))
  } catch (error) {
    const state: IState = yield select()

    yield put(setRequestInfoBox({ ...state.common.infoBox, loading: false }))
  }
}
