import { ROOT_URL } from '../../../../constants'

export const MANAGE_URL = `${ROOT_URL}&step=getManageContent`
export const ITEMS_REQUEST_URL = `${ROOT_URL}&step=getBazaarItems`
export const SUBMIT_FORMDATA_URL = `${ROOT_URL}&step=manageItems`
