import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  metadataRequest,
  metadataResponseSuccess,
  // metadataResponseFailure,
  ownerRequest,
  ownerResponseSuccess,
  // ownerResponseFailure,
  bazaarDataRequest,
  bazaarDataResponseSuccess,
  // bazaarDataResponseFailure,
  showInfoBox
} from '../../../../modules/actions'
import {
  normalizeMetadata,
  normalizeOwner,
  normalizeFormData as normalizeBazaarData
} from '../../../../modules/normalizers'
import { MANAGE_URL } from './constants'
import { IState } from '../../../../store/types'

export default function* pageDataRequestSaga() {
  try {
    yield put(metadataRequest())
    yield put(ownerRequest())
    yield put(bazaarDataRequest())

    const bazaarData = yield fetchUrl(MANAGE_URL, null, null)

    const normalizedMetadata = normalizeMetadata(bazaarData)
    const normalizedOwner = normalizeOwner(bazaarData)
    const normalizedBazaarData = normalizeBazaarData(bazaarData)

    yield put(metadataResponseSuccess(normalizedMetadata))
    yield put(ownerResponseSuccess(normalizedOwner))
    yield put(bazaarDataResponseSuccess(normalizedBazaarData))

    if (bazaarData.error) {
      throw bazaarData.error
    }

    if (bazaarData.state.message) {
      const state: IState = yield select()

      yield put(showInfoBox({ ...state.common.infoBox, msg: bazaarData.state.message, color: bazaarData.state.color }))
    }
  } catch (error) {
    const message = error.message || error.toString()

    // yield put(metadataResponseFailure())
    // yield put(ownerResponseFailure())
    // yield put(bazaarDataResponseFailure())

    yield put(showInfoBox({ msg: message, color: 'red' }))
  }
}
