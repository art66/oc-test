import { TNormalizeFormData } from './types'

export const normalizeFormData: TNormalizeFormData = (formData, list) => {
  return {
    items: Object.keys(formData).map(key => {
      const { bazaarID, itemID, sort, remove, price } = formData[key]

      const formDataItem = {
        bazaarID,
        itemID,
        remove,
        price
      }

      if (sort) {
        let sortIndex = -1

        list.forEach((item, index) => {
          if (formData[key].bazaarID === item.bazaarId && formData[key].itemID === item.itemId) {
            sortIndex = index
          }
        })

        return {
          ...formDataItem,
          sort: sortIndex
        }
      }

      return formDataItem
    })
  }
}
