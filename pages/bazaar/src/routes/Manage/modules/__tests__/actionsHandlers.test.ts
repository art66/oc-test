import { initialState, listState } from './mocks/state'
import { LIST_MOCKS } from './mocks/list'
import { TManageState } from '../types'
import {
  ITEMS_SUCCESS,
  ITEMS_REQUEST_MORE_SUCCESS,
  SET_ACTIVE_VIEW_ID,
  ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID
} from '../constants'
import { ACTION_HANDLERS } from '../reducers'

describe('Reducer action handlers', () => {
  it('Should INIT_MANAGE mutate state correctly', () => {
    const passedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        loadFrom: 50,
        isLoading: false
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        loadFrom: 0,
        isLoading: true
      }
    }

    expect(ACTION_HANDLERS.INIT_MANAGE(passedState)).toEqual(expectedState)
  })

  it('Should ITEMS_REQUEST mutate state correctly', () => {
    const passedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        loadFrom: 50,
        isLoading: false
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        loadFrom: 0,
        isLoading: true
      }
    }

    expect(ACTION_HANDLERS.ITEMS_REQUEST(passedState)).toEqual(expectedState)
  })

  it('Should ITEMS_SUCCESS mutate state correctly', () => {
    const action = {
      type: ITEMS_SUCCESS as typeof ITEMS_SUCCESS,
      list: LIST_MOCKS,
      total: 90,
      amount: 30,
      uniqueKey: 'gyu6789gv'
    }
    const passedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        isLoading: true,
        list: LIST_MOCKS.slice(10, 18),
        total: 10,
        loadFrom: 8,
        uniqueKey: 'gdj7thrd'
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        list: LIST_MOCKS,
        total: 90,
        amount: 30,
        loadFrom: 30,
        isLoading: false,
        uniqueKey: 'gyu6789gv'
      }
    }

    expect(ACTION_HANDLERS.ITEMS_SUCCESS(passedState, action)).toEqual(expectedState)
  })

  it('Should ITEMS_FAILURE mutate state correctly', () => {
    const passedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        isLoading: true
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        isLoading: false
      }
    }

    expect(ACTION_HANDLERS.ITEMS_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should ITEMS_REQUEST_MORE mutate state correctly', () => {
    const passedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        loadFrom: 50,
        isLoadingMore: false
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        loadFrom: 50,
        isLoadingMore: true
      }
    }

    expect(ACTION_HANDLERS.ITEMS_REQUEST_MORE(passedState)).toEqual(expectedState)
  })

  it('Should ITEMS_REQUEST_MORE_SUCCESS mutate state correctly', () => {
    const action = {
      type: ITEMS_REQUEST_MORE_SUCCESS as typeof ITEMS_REQUEST_MORE_SUCCESS,
      list: LIST_MOCKS.slice(10, 18),
      total: 90,
      amount: 30,
      uniqueKey: 'huyhioj9'
    }
    const passedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        isLoadingMore: true,
        list: LIST_MOCKS,
        total: 90,
        loadFrom: 8,
        uniqueKey: 'yy7gtjk'
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        list: [...LIST_MOCKS, ...LIST_MOCKS.slice(10, 18)],
        total: 90,
        amount: 30,
        loadFrom: 38,
        isLoadingMore: false,
        uniqueKey: 'huyhioj9'
      }
    }

    expect(ACTION_HANDLERS.ITEMS_REQUEST_MORE_SUCCESS(passedState, action)).toEqual(expectedState)
  })

  it('Should ITEMS_REQUEST_MORE_FAILURE mutate state correctly', () => {
    const passedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        isLoadingMore: true
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      items: {
        ...initialState.items,
        isLoadingMore: false
      }
    }

    expect(ACTION_HANDLERS.ITEMS_REQUEST_MORE_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should SET_ACTIVE_VIEW_ID mutate state correctly', () => {
    const action = {
      type: SET_ACTIVE_VIEW_ID as typeof SET_ACTIVE_VIEW_ID,
      uniqueId: '12-3591872980',
      itemId: '3591872980',
      armoryId: '12'
    }
    const passedState: TManageState = {
      ...initialState,
      activeView: {
        uniqueId: '12-3591872980',
        itemId: '3591872980',
        armoryId: '12'
      }
    }
    const expectedState: TManageState = {
      ...initialState,
      activeView: {
        uniqueId: '12-3591872980',
        itemId: '3591872980',
        armoryId: '12'
      }
    }

    expect(ACTION_HANDLERS.SET_ACTIVE_VIEW_ID(passedState, action)).toEqual(expectedState)
  })

  it('Should ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID mutate state correctly', () => {
    const action = {
      type: ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID as typeof ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID,
      uniqueId: '12-3591872980'
    }
    const passedState: TManageState = {
      ...initialState,
      activeItemMobileMenuUniqueId: null
    }
    const expectedState: TManageState = {
      ...initialState,
      activeItemMobileMenuUniqueId: '12-3591872980'
    }

    expect(ACTION_HANDLERS.ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID(passedState, action)).toEqual(expectedState)
  })

  it('Should ITEMS_FORMDATA_SUBMIT mutate state correctly', () => {
    const passedState: TManageState = {
      ...listState,
      form: {
        ...listState.form,
        isSubmitting: true
      }
    }
    const expectedState: TManageState = {
      ...listState,
      form: {
        ...listState.form,
        isSubmitting: true
      }
    }

    expect(ACTION_HANDLERS.ITEMS_FORMDATA_SUBMIT(passedState)).toEqual(expectedState)
  })

  it('Should ITEMS_FORMDATA_SUCCESS mutate state correctly', () => {
    const passedState: TManageState = {
      ...listState,
      form: {
        ...listState.form,
        isSubmitting: false
      }
    }
    const expectedState: TManageState = {
      ...listState,
      form: {
        ...listState.form,
        isSubmitting: false
      }
    }

    expect(ACTION_HANDLERS.ITEMS_FORMDATA_SUCCESS(passedState)).toEqual(expectedState)
  })

  it('Should ITEMS_FORMDATA_FAILURE mutate state correctly', () => {
    const passedState: TManageState = {
      ...listState,
      items: {
        ...listState.items
      },
      form: {
        ...listState.form,
        isSubmitting: false
      }
    }
    const expectedState: TManageState = {
      ...listState,
      form: {
        ...listState.form,
        isSubmitting: false
      }
    }

    expect(ACTION_HANDLERS.ITEMS_FORMDATA_FAILURE(passedState)).toEqual(expectedState)
  })

  it('Should UNDO_ITEMS_FORMDATA mutate state correctly', () => {
    const passedState: TManageState = {
      ...listState
    }
    const expectedState: TManageState = {
      ...listState,
      items: {
        ...listState.items
      },
      form: {
        ...listState.form,
        data: {}
      }
    }

    expect(ACTION_HANDLERS.UNDO_ITEMS_FORMDATA(passedState)).toEqual(expectedState)
  })

  it('Should UNSET_ACTIVE_VIEW_ID mutate state correctly', () => {
    const passedState: TManageState = {
      ...listState
    }
    const expectedState: TManageState = {
      ...listState,
      activeView: {
        uniqueId: null,
        itemId: null,
        armoryId: null
      }
    }

    expect(ACTION_HANDLERS.UNSET_ACTIVE_VIEW_ID(passedState)).toEqual(expectedState)
  })
})
