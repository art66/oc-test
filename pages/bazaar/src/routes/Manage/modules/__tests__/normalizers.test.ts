import { normalizeFormData } from '../normalizers'
import { formData, formDataSubmitBody } from './mocks/formData'
import { LIST_MOCKS } from './mocks/list'

describe('Bazaar manage items actions', () => {
  it('should handle normalizeFormData works correctly', () => {
    expect(normalizeFormData(formData, LIST_MOCKS)).toEqual(formDataSubmitBody)
  })
})
