import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import {
  itemsRequest,
  itemsSuccessAction,
  itemsFailureAction,
  itemsRequestMore,
  itemsRequestMoreSuccess,
  itemsRequestMoreFailure,
  setActiveViewUniqueId,
  setActiveItemMobileMenuUniqueId,
  itemRemove,
  itemPriceChanged,
  submitFormData,
  submitFormDataSuccess,
  submitFormDataFailure,
  undoFormData,
  initManage,
  setItems,
  formDataChanged,
  unsetActiveViewUniqueId
} from '../actions'
import {
  ITEMS_REQUEST,
  ITEMS_SUCCESS,
  ITEMS_FAILURE,
  ITEMS_REQUEST_MORE,
  ITEMS_REQUEST_MORE_SUCCESS,
  ITEMS_REQUEST_MORE_FAILURE,
  SET_ACTIVE_VIEW_ID,
  ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID,
  ITEMS_FORMDATA_SUBMIT,
  ITEMS_FORMDATA_SUCCESS,
  ITEMS_FORMDATA_FAILURE,
  UNDO_ITEMS_FORMDATA,
  INIT_MANAGE,
  FORM_DATA_CHANGED,
  SET_ITEMS,
  UNSET_ACTIVE_VIEW_ID
} from '../constants'
import { IState } from '../../../../store/types'
import { itemsData } from './mocks/items'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Bazaar manage items actions', () => {
  it('should handle initManage action', () => {
    const expectedAction = {
      type: INIT_MANAGE
    }

    expect(initManage()).toEqual(expectedAction)
  })

  it('should handle itemsRequest action', () => {
    const expectedAction = {
      type: ITEMS_REQUEST
    }

    expect(itemsRequest()).toEqual(expectedAction)
  })

  it('should handle itemsSuccessAction action', () => {
    const payload = { list: [], total: 40, amount: 25, uniqueKey: '674647' }
    const expectedAction = {
      type: ITEMS_SUCCESS,
      ...payload
    }

    expect(itemsSuccessAction(payload)).toEqual(expectedAction)
  })

  it('should handle itemsFailureAction action', () => {
    const expectedAction = {
      type: ITEMS_FAILURE
    }

    expect(itemsFailureAction()).toEqual(expectedAction)
  })

  it('should handle itemsRequestMore action', () => {
    const expectedAction = {
      type: ITEMS_REQUEST_MORE
    }

    expect(itemsRequestMore()).toEqual(expectedAction)
  })

  it('should handle itemsRequestMoreSuccess action', () => {
    const payload = { list: [], total: 40, amount: 25, uniqueKey: '674647' }
    const expectedAction = {
      type: ITEMS_REQUEST_MORE_SUCCESS,
      ...payload
    }

    expect(itemsRequestMoreSuccess(payload)).toEqual(expectedAction)
  })

  it('should handle itemsRequestMoreFailure action', () => {
    const expectedAction = {
      type: ITEMS_REQUEST_MORE_FAILURE
    }

    expect(itemsRequestMoreFailure()).toEqual(expectedAction)
  })

  it('should handle setActiveViewUniqueId action', () => {
    const uniqueId = '78656-878'
    const expectedAction = {
      type: SET_ACTIVE_VIEW_ID,
      uniqueId
    }

    expect(setActiveViewUniqueId(uniqueId)).toEqual(expectedAction)
  })

  it('should handle setActiveItemMobileMenuUniqueId action', () => {
    const uniqueId = '78656-878'
    const expectedAction = {
      type: ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID,
      uniqueId
    }

    expect(setActiveItemMobileMenuUniqueId(uniqueId)).toEqual(expectedAction)
  })

  it('should handle setItems action', () => {
    const items = { list: [], total: 40, amount: 25, uniqueKey: '674647' }
    const expectedAction = {
      type: SET_ITEMS,
      items
    }

    expect(setItems(items)).toEqual(expectedAction)
  })

  it('should handle formDataChanged action', () => {
    const uniqueId = '556-0'
    const data = { price: 503, remove: 5, sort: 8, priceIsInvalid: true, isInvalid: true }
    const expectedAction = {
      type: FORM_DATA_CHANGED,
      uniqueId,
      bazaarID: '28151328',
      itemID: '556',
      data
    }

    const store = mockStore({ manage: { items: itemsData } } as IState)

    store.dispatch(formDataChanged(uniqueId, data))

    expect(store.getActions()).toEqual([expectedAction])
  })

  it('should handle itemPriceChanged successfuly action', () => {
    const uniqueId = '12-3591872975'
    const data = { price: 1250, priceString: '1,250', priceIsInvalid: false, isInvalid: false }

    const expectedActions = [
      {
        type: FORM_DATA_CHANGED,
        uniqueId,
        bazaarID: '28151310',
        itemID: '12',
        data
      }
    ]
    const store = mockStore({ manage: { items: itemsData } } as IState)

    store.dispatch(itemPriceChanged(1, 1250, '1,250'))

    expect(store.getActions()).toEqual(expectedActions)
  })

  // it('should handle itemPriceChanged failure action', () => {
  //   const uniqueId = '12-3591872975'
  //   const data = { price: 5, priceIsInvalid: true, isInvalid: true }

  //   const expectedActions = [
  //     {
  //       type: FORM_DATA_CHANGED,
  //       uniqueId,
  //       bazaarID: '28151310',
  //       itemID: '12',
  //       data
  //     }
  //   ]
  //   const store = mockStore({ manage: { items: itemsData } } as IState)

  //   store.dispatch(itemPriceChanged(1, 5))

  //   expect(store.getActions()).toEqual(expectedActions)
  // })
  // TODO: Disabled because we disable min price

  it('should handle itemRemove action', () => {
    const uniqueId = '12-3591872975'

    const expectedActions = [
      {
        type: FORM_DATA_CHANGED,
        uniqueId,
        bazaarID: '28151310',
        itemID: '12',
        data: {
          remove: 5
        }
      }
    ]
    const store = mockStore({ manage: { items: itemsData } } as IState)

    store.dispatch(itemRemove(1, 5))

    expect(store.getActions()).toEqual(expectedActions)
  })

  // it('should handle itemDrag action', () => {
  //   const uniqueId = '12-3591872977'

  //   const expectedActions = [
  //     {
  //       type: FORM_DATA_CHANGED,
  //       uniqueId,
  //       data: {
  //         sort: 5
  //       }
  //     },
  //     {
  //       type: SET_ITEMS,
  //       items: {
  //         list: draggedItemsData.list
  //       }
  //     }
  //   ]
  //   const store = mockStore({ manage: { items: itemsData } } as IState)

  //   store.dispatch(itemDrag(3, 5))

  //   expect(store.getActions()).toEqual(expectedActions)
  // })
  // TODO: Should resolve uniqueKey issue an uncomment it

  it('should handle submitFormData action', () => {
    const expectedAction = {
      type: ITEMS_FORMDATA_SUBMIT
    }

    expect(submitFormData()).toEqual(expectedAction)
  })

  it('should handle submitFormDataSuccess action', () => {
    const expectedAction = {
      type: ITEMS_FORMDATA_SUCCESS
    }

    expect(submitFormDataSuccess()).toEqual(expectedAction)
  })

  it('should handle submitFormDataFailure action', () => {
    const expectedAction = {
      type: ITEMS_FORMDATA_FAILURE
    }

    expect(submitFormDataFailure()).toEqual(expectedAction)
  })

  it('should handle undoFormData action', () => {
    const expectedAction = {
      type: UNDO_ITEMS_FORMDATA
    }

    expect(undoFormData()).toEqual(expectedAction)
  })

  it('should handle unsetActiveViewUniqueId action', () => {
    const expectedAction = {
      type: UNSET_ACTIVE_VIEW_ID
    }

    expect(unsetActiveViewUniqueId()).toEqual(expectedAction)
  })
})
