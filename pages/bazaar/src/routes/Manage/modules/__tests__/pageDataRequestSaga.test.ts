import { runSaga } from 'redux-saga'
import fetchMock from 'fetch-mock'

import pageDataRequestSaga from '../sagas/pageDataRequestSaga'
import {
  metadataRequest,
  ownerRequest,
  bazaarDataRequest,
  metadataResponseSuccess,
  ownerResponseSuccess,
  bazaarDataResponseSuccess,
  // metadataResponseFailure,
  // ownerResponseFailure,
  // bazaarDataResponseFailure,
  showInfoBox
} from '../../../../modules/actions'
import { pageData, pageDataError, pageDataWithInfobox } from './mocks/pageData'
import { MANAGE_URL } from '../sagas/constants'

describe('Bazaar modules pageDataRequestSaga saga', () => {
  afterEach(fetchMock.reset)

  it('Should handle with custom user successfully', async () => {
    const dispatched = []

    fetchMock.get(MANAGE_URL, pageData, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      pageDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: '',
        name: 'New nameadas'
      })
    ])
  })

  it('Should handle with custom user successfully and infobox', async () => {
    const dispatched = []

    fetchMock.get(MANAGE_URL, pageDataWithInfobox, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: null } })
      },
      pageDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: '',
        name: 'New nameadas'
      }),
      showInfoBox({ msg: 'Test message', color: 'green' })
    ])
  })

  it('Should handle with bazaar infobox successfully', async () => {
    const dispatched = []

    fetchMock.get(MANAGE_URL, pageData, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ common: { infoBox: { msg: 'Test message' } } })
      },
      pageDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: '',
        name: 'New nameadas'
      })
    ])
  })

  it('Should handle failure with registered error', async () => {
    const dispatched = []

    fetchMock.get(MANAGE_URL, pageDataError, { overwriteRoutes: true })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ manage: { infoBox: null } })
      },
      pageDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      metadataResponseSuccess({
        isBazaarExists: true,
        isClosed: true,
        isYours: true
      }),
      ownerResponseSuccess({
        playername: 'love__intent',
        userId: '2265266'
      }),
      bazaarDataResponseSuccess({
        action: true,
        description: '',
        name: 'New nameadas'
      }),
      showInfoBox({ color: 'red', msg: 'Test error message' })
    ])
  })

  it('Should handle when request failure', async () => {
    const dispatched = []

    fetchMock.get(
      MANAGE_URL,
      () => {
        throw 'Error message'
      },
      { overwriteRoutes: true }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ manage: { infoBox: null } })
      },
      pageDataRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([
      metadataRequest(),
      ownerRequest(),
      bazaarDataRequest(),
      // metadataResponseFailure(),
      // ownerResponseFailure(),
      // bazaarDataResponseFailure(),
      showInfoBox({ msg: 'Error message', color: 'red' })
    ])
  })
})
