export const pageData = {
  headerContent: {
    links: [
      { link: '#/p=main', className: 'back-to', icon: 'back', title: 'Back' },
      { link: '#/p=personalize', className: 'personalize', icon: 'personalize', title: 'Personalize' },
      { link: '#/p=add', className: 'add-items', icon: 'add-items', title: 'Add items' }
    ],
    title: 'New nameadas'
  },
  name: 'New nameadas',
  owner: { userId: '2265266', playername: 'love__intent' },
  state: { isYours: true, isClosed: true, isBazaarExists: true },
  totalQuantity: '31'
}

export const pageDataWithInfobox = {
  ...pageData,
  state: { ...pageData.state, message: 'Test message', color: 'green' }
}

export const pageDataError = {
  ...pageData,
  error: 'Test error message'
}
