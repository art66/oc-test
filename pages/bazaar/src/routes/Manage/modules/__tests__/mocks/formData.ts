import { TFormData, TNormalizedFormData } from '../../types'

export const formData: TFormData = {
  '12-3591872969': { bazaarID: '28151305', itemID: '12', remove: 1, price: 342423, sort: true },
  '12-3591872975': { bazaarID: '28151310', itemID: '12', sort: true, remove: 1 },
  '12-3591872976': { bazaarID: '28151311', itemID: '12', price: 342342342, remove: 1 },
  '556-0': { bazaarID: '28151328', itemID: '556', remove: 5, price: 435436 },
  '529-0': { bazaarID: '28151329', itemID: '529', remove: 4, price: 542, sort: true },
  '388-4384562992': { bazaarID: '32013525', itemID: '388', remove: 0, price: 99999999999, sort: true }
}

export const formDataSubmitBody: TNormalizedFormData = {
  items: [
    { bazaarID: '28151305', itemID: '12', remove: 1, price: 342423, sort: 2 },
    { bazaarID: '28151310', itemID: '12', sort: 6, remove: 1 },
    { bazaarID: '28151311', itemID: '12', price: 342342342, remove: 1 },
    { bazaarID: '28151328', itemID: '556', remove: 5, price: 435436 },
    { bazaarID: '28151329', itemID: '529', remove: 4, price: 542, sort: 25 },
    { bazaarID: '32013525', itemID: '388', remove: 0, price: 99999999999, sort: 30 }
  ]
}
