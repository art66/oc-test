import { TPureItems } from '../../../../../modules/types/normalizers'

export const pureItemsData: TPureItems = {
  uniqueKey: '4678635',
  list: [
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151309',
      armoryID: '3591872974',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.62',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.57',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151310',
      armoryID: '3591872975',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.79',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.80',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151311',
      armoryID: '3591872976',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.85',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.45',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151312',
      armoryID: '3591872977',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.86',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.32',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151313',
      armoryID: '3591872978',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.62',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.75',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151314',
      armoryID: '3591872979',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.67',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.01',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151315',
      armoryID: '3591872980',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.69',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.26',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151316',
      armoryID: '3591872981',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.83',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.35',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151317',
      armoryID: '3591872982',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.66',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.55',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151318',
      armoryID: '3591872983',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.62',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.04',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151319',
      armoryID: '3591872984',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.82',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.16',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151320',
      armoryID: '3591872985',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.68',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.31',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151321',
      armoryID: '3591872986',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.75',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.11',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151322',
      armoryID: '3591872987',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.91',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.07',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151323',
      armoryID: '3591872988',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '22.00',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.11',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151324',
      armoryID: '3591872989',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.96',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.35',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151325',
      armoryID: '3591872990',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.83',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.48',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151326',
      armoryID: '3591872991',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.60',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '54.01',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151327',
      armoryID: '3591872992',
      itemID: '12',
      amount: '1',
      price: '5000',
      dmg: '21.67',
      type2: 'Secondary',
      name: 'Glock 17',
      accuracy: '53.06',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '355',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151328',
      armoryID: '0',
      itemID: '556',
      amount: '5',
      price: '600000',
      dmg: '0',
      type2: 'Candy',
      name: 'Bag of Reindeer Droppings',
      accuracy: '0',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '154913',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151329',
      armoryID: '0',
      itemID: '529',
      amount: '5',
      price: '800000',
      dmg: '0',
      type2: 'Candy',
      name: 'Bag of Chocolate Truffles',
      accuracy: '0',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '151124',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151330',
      armoryID: '0',
      itemID: '394',
      amount: '4',
      price: '8000',
      dmg: '48.12',
      type2: 'Temporary',
      name: 'Brick',
      accuracy: '43.00',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '478',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151331',
      armoryID: '0',
      itemID: '404',
      amount: '1',
      price: '80000',
      dmg: '0',
      type2: 'Clothing',
      name: 'Bandana',
      accuracy: '0',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '807',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151332',
      armoryID: '0',
      itemID: '806',
      amount: '1',
      price: '90000',
      dmg: '0',
      type2: 'Clothing',
      name: 'Old Lady Mask',
      accuracy: '0',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '21328',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '28151333',
      armoryID: '0',
      itemID: '541',
      amount: '150',
      price: '20000000',
      dmg: '0',
      type2: 'Alcohol',
      name: 'Bottle of Stinky Swamp Punch',
      accuracy: '0',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '338899',
      glow: null
    },
    {
      bonuses: [
        { class: 'bonus-attachment-blank-bonus-25', title: null },
        { class: 'bonus-attachment-blank-bonus-25', title: null }
      ],
      type: 'Weapon',
      arm: '0',
      bazaarID: '32013525',
      armoryID: '4384562992',
      itemID: '388',
      amount: '1',
      price: '99999999999',
      dmg: '81.45',
      type2: 'Primary',
      name: 'Pink Mac-10',
      accuracy: '45.26',
      rarity: 0,
      isBlockedForBuying: false,
      averageprice: '0',
      glow: { color: 'FFBF00', opacity: 0.5 }
    }
  ],
  total: 31
}
