import { TManageState } from '../../types'
import { LIST_MOCKS, draggedList } from './list'
import { formData } from './formData'
import { IState } from '../../../../../store/types'
import { globalInitialState as defaultGlobalInitialState } from '../../../../../modules/__tests__/mocks/state'

export const initialState: TManageState = {
  items: {
    isLoading: false,
    isLoadingMore: false,
    loadFrom: 0,
    limit: 50,
    list: [],
    amount: 0,
    total: 0,
    uniqueKey: null
  },
  form: {
    data: {},
    isSubmitting: false,
    error: null,
    errors: null
  },
  activeView: {
    uniqueId: null,
    itemId: null,
    armoryId: null
  },
  activeItemMobileMenuUniqueId: null
}

export const listState: TManageState = {
  ...initialState,
  items: {
    ...initialState.items,
    list: LIST_MOCKS
  }
}

export const activeViewState: TManageState = {
  ...initialState,
  activeView: {
    ...initialState.activeView,
    uniqueId: '404-0'
  }
}

export const formDataState: TManageState = {
  ...initialState,
  form: {
    ...initialState.form,
    data: formData
  }
}

export const draggedState: TManageState = {
  ...initialState,
  items: {
    ...initialState.items,
    list: draggedList
  },
  form: {
    ...initialState.form,
    data: {
      '12-3591872974': {
        bazaarID: '28151309',
        itemID: '12',
        sort: true
      }
    }
  }
}

export const globalState: IState = {
  ...defaultGlobalInitialState,
  manage: initialState
}

export const globalActiveViewState: IState = {
  ...defaultGlobalInitialState,
  manage: activeViewState
}
