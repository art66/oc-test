import { runSaga } from 'redux-saga'
import fetchMock from 'fetch-mock'
import { createItemsRequestUrl } from '../sagas/urls'
import { globalState, globalActiveViewState } from './mocks/state'
import { itemsRequest, itemsRequestSaga, itemsRequestMoreSaga } from '../sagas/itemsRequestSaga'
import { pureItemsData } from './mocks/pureItems'
import { itemsData } from './mocks/items'
import { itemsSuccessAction, itemsFailureAction, itemsRequestMoreSuccess, itemsRequestMoreFailure } from '../actions'
import { showInfoBox } from '../../../../modules/actions'

describe('Bazaar Manage items modules itemsRequestSaga sagas', () => {
  afterEach(fetchMock.reset)

  it('Should handle itemsRequest successfully', async () => {
    const dispatched = []

    fetchMock.get(createItemsRequestUrl({ loadFrom: 5, userId: '2265266', limit: 50 }), pureItemsData, {
      overwriteRoutes: true
    })

    const result = await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({
          ...globalState,
          manage: {
            ...globalState.manage,
            items: {
              ...globalState.manage.items,
              loadFrom: 5,
              limit: 50
            }
          }
        })
      },
      itemsRequest
    ).toPromise()

    expect(dispatched).toEqual([])

    expect(result).toEqual(itemsData)
  })

  it('Should handle itemsRequest failure with known error', async () => {
    const dispatched = []

    fetchMock.get(
      createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }),
      { ...pureItemsData, error: 'Test known error' },
      {
        overwriteRoutes: true
      }
    )

    try {
      await runSaga(
        {
          dispatch: action => dispatched.push(action),
          getState: () => globalState
        },
        itemsRequest
      ).toPromise()

      expect(false).toBeTruthy()
    } catch (error) {
      expect(dispatched).toEqual([showInfoBox({ msg: 'Test known error', color: 'red' })])
    }
  })

  it('Should handle itemsRequest failure with unknown error', async () => {
    const dispatched = []

    fetchMock.get(
      createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }),
      () => {
        throw new Error('Test unknown error')
      },
      {
        overwriteRoutes: true
      }
    )

    try {
      await runSaga(
        {
          dispatch: action => dispatched.push(action),
          getState: () => globalState
        },
        itemsRequest
      ).toPromise()

      expect(false).toBeTruthy()
    } catch (error) {
      expect(dispatched).toEqual([showInfoBox({ msg: 'Test unknown error', color: 'red' })])
    }
  })

  it('Should handle itemsRequestSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }), pureItemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalState
      },
      itemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([itemsSuccessAction(itemsData)])
  })

  it('Should handle itemsRequestSaga successfully and remove active view', async () => {
    const dispatched = []

    fetchMock.get(createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }), pureItemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalActiveViewState
      },
      itemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([itemsSuccessAction(itemsData)])
  })

  it('Should handle itemsRequestSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(
      createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }),
      { ...pureItemsData, error: 'Test known error' },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalState
      },
      itemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Test known error', color: 'red' }), itemsFailureAction()])
  })

  it('Should handle itemsRequestSaga failure with unknown error', async () => {
    const dispatched = []

    fetchMock.get(
      createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }),
      () => {
        throw new Error('Test unknown error')
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalState
      },
      itemsRequestSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Test unknown error', color: 'red' }), itemsFailureAction()])
  })

  it('Should handle itemsRequestMoreSaga successfully', async () => {
    const dispatched = []

    fetchMock.get(createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }), pureItemsData, {
      overwriteRoutes: true
    })

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalState
      },
      itemsRequestMoreSaga
    ).toPromise()

    expect(dispatched).toEqual([itemsRequestMoreSuccess(itemsData)])
  })

  it('Should handle itemsRequestMoreSaga failure with known error', async () => {
    const dispatched = []

    fetchMock.get(
      createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }),
      { ...pureItemsData, error: 'Test known error' },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalState
      },
      itemsRequestMoreSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Test known error', color: 'red' }), itemsRequestMoreFailure()])
  })

  it('Should handle itemsRequestMoreSaga failure with unknown error', async () => {
    const dispatched = []

    fetchMock.get(
      createItemsRequestUrl({ loadFrom: 0, userId: '2265266', limit: 50 }),
      () => {
        throw new Error('Test unknown error')
      },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => globalState
      },
      itemsRequestMoreSaga
    ).toPromise()

    expect(dispatched).toEqual([showInfoBox({ msg: 'Test unknown error', color: 'red' }), itemsRequestMoreFailure()])
  })
})
