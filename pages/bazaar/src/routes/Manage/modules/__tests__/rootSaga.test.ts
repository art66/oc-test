import rootSaga from '../saga'
import { takeLatest, debounce } from 'redux-saga/effects'
import { ITEMS_REQUEST, ITEMS_REQUEST_MORE, SET_ACTIVE_VIEW_ID, ITEMS_FORMDATA_SUBMIT, INIT_MANAGE } from '../constants'
import { itemsRequestSaga, itemsRequestMoreSaga } from '../sagas/itemsRequestSaga'
import bazaarItemInfoRequestSaga from '../../../../modules/sagas/bazaarItemInfoRequest'
import formDataSubmitSaga from '../sagas/formDataSubmitSaga'
import setPageIdSaga from '../../../../modules/sagas/setPageId'
import { MANAGE_PAGE_ID } from '../../../../constants'
import { initManageItemsSaga } from '../sagas/initSaga'

describe('Bazaar Manage items root saga', () => {
  it('The root saga should react to actions', () => {
    const generator = rootSaga()

    expect(generator.next().value).toEqual(takeLatest(INIT_MANAGE, initManageItemsSaga))

    expect(generator.next().value).toEqual(takeLatest(INIT_MANAGE, setPageIdSaga, MANAGE_PAGE_ID))

    expect(generator.next().value).toEqual(takeLatest(ITEMS_REQUEST, itemsRequestSaga))

    expect(generator.next().value).toEqual(takeLatest(ITEMS_REQUEST_MORE, itemsRequestMoreSaga))

    expect(generator.next().value).toEqual(takeLatest(SET_ACTIVE_VIEW_ID, bazaarItemInfoRequestSaga))

    expect(generator.next().value).toEqual(debounce(250, ITEMS_FORMDATA_SUBMIT, formDataSubmitSaga))

    expect(generator.next().done).toBe(true)
  })
})
