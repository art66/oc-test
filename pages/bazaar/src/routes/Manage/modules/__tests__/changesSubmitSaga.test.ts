import fetchMock from 'fetch-mock'
import { runSaga } from 'redux-saga'
import formDataSubmitSaga from '../sagas/formDataSubmitSaga'
import { SUBMIT_FORMDATA_URL } from '../sagas/constants'
import { submitFormDataSuccess, submitFormDataFailure, itemsRequest } from '../actions'
import { formDataState } from './mocks/state'
import { globalInitialState } from '../../../Bazaar/modules/__tests__/mocks/state'
import { showInfoBox } from '../../../../modules/actions'

describe('Bazaar Manage items modules formDataSubmitSaga sagas', () => {
  afterEach(fetchMock.reset)

  it('Should handle successfully', async () => {
    const dispatched = []

    fetchMock.post(
      SUBMIT_FORMDATA_URL,
      { success: true, text: 'All ok test text', logs: ['Test log 1', 'Test log 2', 'Test log 3'] },
      { overwriteRoutes: true }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({
          ...globalInitialState,
          manage: formDataState
        })
      },
      formDataSubmitSaga
    ).toPromise()

    expect(dispatched).toEqual([
      showInfoBox({ loading: true }),
      showInfoBox({
        loading: false,
        msg: 'All ok test text',
        color: 'green',
        log: ['Test log 1', 'Test log 2', 'Test log 3']
      }),
      submitFormDataSuccess(),
      itemsRequest()
    ])
  })

  it('Should handle failure with registered error', async () => {
    const dispatched = []

    fetchMock.post(
      SUBMIT_FORMDATA_URL,
      { text: 'Test other registered error' },
      {
        overwriteRoutes: true
      }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({
          ...globalInitialState,
          manage: formDataState
        })
      },
      formDataSubmitSaga
    ).toPromise()

    expect(dispatched).toEqual([
      showInfoBox({ loading: true }),
      submitFormDataFailure(),
      showInfoBox({ loading: false, msg: 'Test other registered error', color: 'red' })
    ])
  })

  it('Should handle failure', async () => {
    const dispatched = []

    fetchMock.post(
      SUBMIT_FORMDATA_URL,
      () => {
        throw new Error('Failure error')
      },
      { overwriteRoutes: true }
    )

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({
          ...globalInitialState,
          manage: formDataState
        })
      },
      formDataSubmitSaga
    ).toPromise()

    expect(dispatched).toEqual([
      showInfoBox({ loading: true }),
      submitFormDataFailure(),
      showInfoBox({ loading: false, msg: 'Failure error', color: 'red' })
    ])
  })
})
