import { takeLatest, debounce } from 'redux-saga/effects'
import { ITEMS_REQUEST, ITEMS_REQUEST_MORE, SET_ACTIVE_VIEW_ID, ITEMS_FORMDATA_SUBMIT, INIT_MANAGE } from './constants'
import { itemsRequestSaga, itemsRequestMoreSaga } from './sagas/itemsRequestSaga'
import bazaarItemInfoRequestSaga from '../../../modules/sagas/bazaarItemInfoRequest'
import formDataSubmitSaga from './sagas/formDataSubmitSaga'
import setPageIdSaga from '../../../modules/sagas/setPageId'
import { MANAGE_PAGE_ID } from '../../../constants'
import { initManageItemsSaga } from './sagas/initSaga'

export default function* bazaarRoot() {
  yield takeLatest(INIT_MANAGE, initManageItemsSaga)
  yield takeLatest(INIT_MANAGE, setPageIdSaga, MANAGE_PAGE_ID)
  yield takeLatest(ITEMS_REQUEST, itemsRequestSaga)
  yield takeLatest(ITEMS_REQUEST_MORE, itemsRequestMoreSaga)
  yield takeLatest(SET_ACTIVE_VIEW_ID, bazaarItemInfoRequestSaga)
  yield debounce(250, ITEMS_FORMDATA_SUBMIT, formDataSubmitSaga)
}
