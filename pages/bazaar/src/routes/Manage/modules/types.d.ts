import { TItem } from '../../../modules/types/state'
import { TNormalizedItems } from '../../../modules/types/normalizers'
import {
  ITEMS_REQUEST,
  ITEMS_SUCCESS,
  ITEMS_FAILURE,
  SET_ACTIVE_VIEW_ID,
  ITEMS_REQUEST_MORE,
  ITEMS_REQUEST_MORE_SUCCESS,
  ITEMS_REQUEST_MORE_FAILURE,
  ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID,
  ITEMS_FORMDATA_SUBMIT,
  ITEMS_FORMDATA_SUCCESS,
  ITEMS_FORMDATA_FAILURE,
  UNDO_ITEMS_FORMDATA,
  INIT_MANAGE,
  SET_ITEMS,
  FORM_DATA_CHANGED,
  UNSET_ACTIVE_VIEW_ID
} from './constants'

export type TInitManagePayload = {
  type: typeof INIT_MANAGE
}

export type TInitManage = () => TInitManagePayload

export type TFormDataItem = {
  bazaarID: string
  itemID: string
  sort?: boolean
  remove?: number
  price?: number
  priceString?: string
  priceIsInvalid?: boolean
  isInvalid?: boolean
  isChanged?: boolean
}

export type TFormData = {
  [key: string]: TFormDataItem
}

export type TItemsList = TItem[]

export type TItems = {
  isLoading: boolean
  isLoadingMore: boolean
  loadFrom: number
  limit: number
  list: TItemsList
  amount: number
  total: number
  uniqueKey: string
}

export type TActiveView = {
  uniqueId: string
  itemId: string
  armoryId: string
}

export type TManageState = {
  items: TItems
  form: {
    data: TFormData
    isSubmitting: boolean
    error?: string
    errors?: string[]
  }
  activeView: TActiveView
  activeItemMobileMenuUniqueId: string
}

export type TItemsRequestPayload = {
  type: typeof ITEMS_REQUEST
}

export type TItemsRequest = () => TItemsRequestPayload

export type TItemsSuccessPayload = {
  type: typeof ITEMS_SUCCESS
  list: TItem[]
  total: number
  amount: number
  uniqueKey: string
}

export type TSetItemsData = {
  list: TItem[]
  total?: number
  amount?: number
  uniqueKey: string
}

export type TSetItemsPayload = {
  type: typeof SET_ITEMS
  items: TSetItemsData
}

export type TSetItems = (items: TSetItemsData) => TSetItemsPayload

export type TFormChangedData = {
  price?: number
  remove?: number
  sort?: boolean
  priceIsInvalid?: boolean
  isInvalid?: boolean
}

export type TFormDataChangedPayload = {
  type: typeof FORM_DATA_CHANGED
  bazaarID: string
  itemID: string
  uniqueId: string
  data: TFormChangedData
}

export type TFormDataChanged = (uniqueId: string, data: TFormChangedData) => TFormDataChangedPayload

export type TItemsSuccessAction = (payload: TNormalizedItems) => TItemsSuccessPayload

export type TItemsFailurePayload = {
  type: typeof ITEMS_FAILURE
}

export type TItemsFailureAction = () => TItemsFailurePayload

export type TItemsRequestMorePayload = {
  type: typeof ITEMS_REQUEST_MORE
}

export type TItemsRequestMore = () => TItemsRequestMorePayload

export type TItemsRequestMoreSuccessPayload = {
  type: typeof ITEMS_REQUEST_MORE_SUCCESS
  list: TItem[]
  total: number
  amount: number
  uniqueKey: string
}

export type TItemsRequestMoreSuccessAction = (payload: TNormalizedItems) => TItemsRequestMoreSuccessPayload

export type TItemsRequestMoreFailurePayload = {
  type: typeof ITEMS_REQUEST_MORE_FAILURE
}

export type TItemsRequestMoreFailureAction = () => TItemsRequestMoreFailurePayload

export type TSetActiveViewPayload = {
  type: typeof SET_ACTIVE_VIEW_ID
  uniqueId?: string
  itemId?: string
  armoryId?: string
}

export type TSetActiveViewAction = (uniqueId?: string, itemId?: string, armoryId?: string) => TSetActiveViewPayload

export type TSetActiveItemMobileMenuUniqueIdPayload = {
  type: typeof ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID
  uniqueId: string
}

export type TSetActiveItemMobileMenuUniqueId = (uniqueId?: string) => TSetActiveItemMobileMenuUniqueIdPayload

export type TSubmitFormData = () => {
  type: typeof ITEMS_FORMDATA_SUBMIT
}

export type TSubmitFormDataSuccess = () => {
  type: typeof ITEMS_FORMDATA_SUCCESS
}

export type TSubmitFormDataFailure = () => {
  type: typeof ITEMS_FORMDATA_FAILURE
}

export type TUndoFormData = () => {
  type: typeof UNDO_ITEMS_FORMDATA
}

export type TNormalizedFormDataItem = {
  bazaarID: string
  itemID: string
  sort?: number
  remove?: number
  price?: number
  priceString?: string
}

export type TNormalizedFormData = {
  items: TNormalizedFormDataItem[]
}

export type TNormalizeFormData = (formData: TFormData, list: TItemsList) => TNormalizedFormData

export type TUnsetActiveViewPayload = {
  type: typeof UNSET_ACTIVE_VIEW_ID
}

export type TUnsetActiveViewAction = () => TUnsetActiveViewPayload
