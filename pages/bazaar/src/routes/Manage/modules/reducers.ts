import { createReducer } from '../../../modules/utils'
import {
  TManageState,
  TItemsSuccessPayload,
  TSetActiveViewPayload,
  TSetActiveItemMobileMenuUniqueIdPayload,
  TItemsRequestMoreSuccessPayload,
  TFormDataChangedPayload,
  TSetItemsPayload
} from './types'
import {
  ITEMS_REQUEST,
  ITEMS_SUCCESS,
  ITEMS_FAILURE,
  ITEMS_REQUEST_MORE,
  ITEMS_REQUEST_MORE_SUCCESS,
  ITEMS_REQUEST_MORE_FAILURE,
  SET_ACTIVE_VIEW_ID,
  ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID,
  ITEMS_FORMDATA_SUBMIT,
  ITEMS_FORMDATA_SUCCESS,
  ITEMS_FORMDATA_FAILURE,
  UNDO_ITEMS_FORMDATA,
  INIT_MANAGE,
  FORM_DATA_CHANGED,
  SET_ITEMS,
  UNSET_ACTIVE_VIEW_ID
} from './constants'

export const initialState: TManageState = {
  items: {
    isLoading: false,
    isLoadingMore: false,
    loadFrom: 0,
    limit: 50,
    list: [],
    amount: 0,
    total: 0,
    uniqueKey: null
  },
  form: {
    data: {},
    isSubmitting: false,
    error: null,
    errors: null
  },
  activeView: {
    uniqueId: null,
    itemId: null,
    armoryId: null
  },
  activeItemMobileMenuUniqueId: null
}

export const ACTION_HANDLERS = {
  [INIT_MANAGE]: (state: TManageState): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        loadFrom: 0,
        isLoading: true
      }
    }
  },
  [ITEMS_REQUEST]: (state: TManageState): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        loadFrom: 0,
        isLoading: true
      }
    }
  },
  [ITEMS_SUCCESS]: (state: TManageState, action: TItemsSuccessPayload): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        isLoading: false,
        list: action.list,
        total: action.total,
        loadFrom: action.amount,
        amount: action.amount,
        uniqueKey: action.uniqueKey
      }
    }
  },
  [ITEMS_FAILURE]: (state: TManageState): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        isLoading: false,
        list: [],
        total: 0,
        loadFrom: 0,
        amount: 0,
        uniqueKey: null
      }
    }
  },
  [ITEMS_REQUEST_MORE]: (state: TManageState): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        isLoadingMore: true
      }
    }
  },
  [ITEMS_REQUEST_MORE_SUCCESS]: (state: TManageState, action: TItemsRequestMoreSuccessPayload): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        isLoadingMore: false,
        list: [...state.items.list, ...action.list],
        total: action.total,
        loadFrom: state.items.loadFrom + action.amount,
        amount: state.items.amount + action.amount,
        uniqueKey: action.uniqueKey
      }
    }
  },
  [ITEMS_REQUEST_MORE_FAILURE]: (state: TManageState): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        isLoadingMore: false
      }
    }
  },
  [SET_ACTIVE_VIEW_ID]: (state: TManageState, action: TSetActiveViewPayload): TManageState => {
    return {
      ...state,
      activeView: {
        ...state.activeView,
        uniqueId: action.uniqueId,
        itemId: action.itemId,
        armoryId: action.armoryId
      }
    }
  },
  [ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID]: (
    state: TManageState,
    action: TSetActiveItemMobileMenuUniqueIdPayload
  ): TManageState => {
    return {
      ...state,
      activeItemMobileMenuUniqueId: action.uniqueId
    }
  },
  [SET_ITEMS]: (state: TManageState, action: TSetItemsPayload): TManageState => {
    return {
      ...state,
      items: {
        ...state.items,
        ...action.items
      }
    }
  },
  [FORM_DATA_CHANGED]: (state: TManageState, action: TFormDataChangedPayload): TManageState => {
    const nextItemData = {
      ...state.form.data[action.uniqueId],
      ...action.data
    }

    const currentItem = state.items.list.find(item => item.uniqueId === action.uniqueId)
    const isFormItemDataChanged = !!(
      nextItemData.sort
      || nextItemData.remove
      || (nextItemData.price && nextItemData.price !== currentItem.price)
    )

    return {
      ...state,
      form: {
        ...state.form,
        data: {
          ...state.form.data,
          [action.uniqueId]: {
            ...nextItemData,
            bazaarID: action.bazaarID,
            itemID: action.itemID,
            isChanged: isFormItemDataChanged
          }
        }
      }
    }
  },
  [ITEMS_FORMDATA_SUBMIT]: (state: TManageState): TManageState => {
    return {
      ...state,
      form: {
        ...state.form,
        isSubmitting: true,
        error: null,
        errors: null
      }
    }
  },
  [ITEMS_FORMDATA_SUCCESS]: (state: TManageState): TManageState => {
    return {
      ...state,
      form: {
        ...state.form,
        isSubmitting: false,
        data: {}
      }
    }
  },
  [ITEMS_FORMDATA_FAILURE]: (state: TManageState): TManageState => {
    return {
      ...state,
      form: {
        ...state.form,
        isSubmitting: false,
        error: null,
        errors: null
      }
    }
  },
  [UNDO_ITEMS_FORMDATA]: (state: TManageState): TManageState => {
    return {
      ...state,
      form: {
        ...state.form,
        data: {}
      }
    }
  },
  [UNSET_ACTIVE_VIEW_ID]: (state: TManageState): TManageState => {
    return {
      ...state,
      activeView: {
        uniqueId: null,
        itemId: null,
        armoryId: null
      }
    }
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
