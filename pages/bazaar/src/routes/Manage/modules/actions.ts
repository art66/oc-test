import {
  ITEMS_REQUEST,
  ITEMS_SUCCESS,
  ITEMS_FAILURE,
  SET_ACTIVE_VIEW_ID,
  ITEMS_REQUEST_MORE_FAILURE,
  ITEMS_REQUEST_MORE_SUCCESS,
  ITEMS_REQUEST_MORE,
  ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID,
  FORM_DATA_CHANGED,
  ITEMS_FORMDATA_SUBMIT,
  ITEMS_FORMDATA_SUCCESS,
  ITEMS_FORMDATA_FAILURE,
  UNDO_ITEMS_FORMDATA,
  INIT_MANAGE,
  SET_ITEMS,
  UNSET_ACTIVE_VIEW_ID
} from './constants'
import {
  TItemsRequest,
  TItemsSuccessAction,
  TItemsFailureAction,
  TSetActiveViewAction,
  TItemsRequestMore,
  TItemsRequestMoreSuccessAction,
  TItemsRequestMoreFailureAction,
  TSetActiveItemMobileMenuUniqueId,
  TSubmitFormData,
  TSubmitFormDataSuccess,
  TSubmitFormDataFailure,
  TUndoFormData,
  TInitManage,
  TSetItems,
  TFormDataChangedPayload,
  TUnsetActiveViewAction
} from './types'
import { IState } from '../../../store/types'

export const initManage: TInitManage = () => {
  return {
    type: INIT_MANAGE
  }
}

export const itemsRequest: TItemsRequest = () => {
  return {
    type: ITEMS_REQUEST
  }
}

export const itemsSuccessAction: TItemsSuccessAction = ({ list, total, amount, uniqueKey }) => {
  return {
    type: ITEMS_SUCCESS,
    list,
    total,
    amount,
    uniqueKey
  }
}

export const itemsFailureAction: TItemsFailureAction = () => {
  return {
    type: ITEMS_FAILURE
  }
}

export const itemsRequestMore: TItemsRequestMore = () => {
  return {
    type: ITEMS_REQUEST_MORE
  }
}

export const itemsRequestMoreSuccess: TItemsRequestMoreSuccessAction = ({ list, total, amount, uniqueKey }) => {
  return {
    type: ITEMS_REQUEST_MORE_SUCCESS,
    list,
    total,
    amount,
    uniqueKey
  }
}

export const itemsRequestMoreFailure: TItemsRequestMoreFailureAction = () => {
  return {
    type: ITEMS_REQUEST_MORE_FAILURE
  }
}

export const setActiveViewUniqueId: TSetActiveViewAction = uniqueId => {
  return {
    type: SET_ACTIVE_VIEW_ID,
    uniqueId
  }
}

export const setActiveItemMobileMenuUniqueId: TSetActiveItemMobileMenuUniqueId = uniqueId => {
  return {
    type: ACTIVE_ITEM_MOBILE_MENU_UNIQUE_ID,
    uniqueId
  }
}

export const setItems: TSetItems = items => {
  return {
    type: SET_ITEMS,
    items
  }
}

export const formDataChanged = (uniqueId, data) => (dispatch, getState): TFormDataChangedPayload => {
  const {
    manage: {
      items: { list }
    }
  }: IState = getState()

  const foundItem = list.find(item => item.uniqueId === uniqueId)

  return dispatch({
    type: FORM_DATA_CHANGED,
    bazaarID: foundItem.bazaarId,
    itemID: foundItem.itemId,
    uniqueId,
    data
  })
}

export const itemPriceChanged = (index: number, price: number, priceString: string) => (dispatch, getState) => {
  const {
    manage: {
      items: {
        list: {
          [index]: { minPrice, uniqueId }
        }
      }
    }
  }: IState = getState()

  const priceIsInvalid = price < minPrice
  const isInvalid = priceIsInvalid

  dispatch(formDataChanged(uniqueId, { price, priceString, priceIsInvalid, isInvalid }))
}

export const itemRemove = (index: number, amount: number) => (dispatch, getState) => {
  const {
    manage: {
      items: {
        list: {
          [index]: { uniqueId }
        }
      }
    }
  }: IState = getState()

  dispatch(formDataChanged(uniqueId, { remove: amount }))
}

export const itemDrag = (sourceIndex: number, destinationIndex: number) => (dispatch, getState) => {
  const {
    manage: {
      items: {
        list,
        list: {
          [sourceIndex]: { uniqueId }
        }
      }
    }
  }: IState = getState()

  const nextList = [...list]
  const [item] = nextList.splice(sourceIndex, 1)
  nextList.splice(destinationIndex, 0, item)

  dispatch(formDataChanged(uniqueId, { sort: true }))
  dispatch(setItems({ list: nextList, uniqueKey: Date.now().toString() }))
}

export const submitFormData: TSubmitFormData = () => {
  return {
    type: ITEMS_FORMDATA_SUBMIT
  }
}

export const submitFormDataSuccess: TSubmitFormDataSuccess = () => {
  return {
    type: ITEMS_FORMDATA_SUCCESS
  }
}

export const submitFormDataFailure: TSubmitFormDataFailure = () => {
  return {
    type: ITEMS_FORMDATA_FAILURE
  }
}

export const undoFormData: TUndoFormData = () => {
  return {
    type: UNDO_ITEMS_FORMDATA
  }
}

export const unsetActiveViewUniqueId: TUnsetActiveViewAction = () => {
  return {
    type: UNSET_ACTIVE_VIEW_ID
  }
}
