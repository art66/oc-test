import {
  getItems,
  getActiveViewUniqueId,
  getActiveItemMobileMenuUniqueId,
  getRowHeight,
  getRows,
  getActiveItemMobileMenuIndex,
  getItemFormData
} from '../selectors'
import {
  withListState,
  list,
  withActiveViewState,
  withActiveMobileMenuState,
  withListAndActiveViewState,
  withViewRows,
  withListAndActiveMobileMenuState,
  state
} from './mocks'
import { rows } from './mocks/rows'

describe('Items/selectors', () => {
  it('getItems', () => {
    expect(getItems(withListState)).toEqual(list)
  })

  it('getActiveViewUniqueId', () => {
    expect(getActiveViewUniqueId(withActiveViewState)).toEqual('12-3591872977')
  })

  it('getActiveItemMobileMenuUniqueId', () => {
    expect(getActiveItemMobileMenuUniqueId(withActiveMobileMenuState)).toEqual('731-0')
  })

  it('getRowHeight default', () => {
    expect(getRowHeight({ viewHeight: null, mobileMenuActive: false })).toEqual(36)
  })

  it('getRowHeight with view', () => {
    expect(getRowHeight({ viewHeight: 45, mobileMenuActive: false })).toEqual(81)
  })

  it('getRowHeight with mobile menu', () => {
    expect(getRowHeight({ viewHeight: null, mobileMenuActive: true })).toEqual(126)
  })

  it('getRowHeight with view and mobile menu', () => {
    expect(getRowHeight({ viewHeight: 50, mobileMenuActive: true })).toEqual(176)
  })

  it('getRows', () => {
    expect(getRows(withListState)).toEqual(rows)
  })

  it('getRows when view active', () => {
    expect(getRows(withListAndActiveViewState)).toEqual(withViewRows)
  })

  it('getRows if no items available', () => {
    expect(getRows(state)).toEqual([])
  })

  it('getActiveItemMobileMenuIndex', () => {
    expect(getActiveItemMobileMenuIndex(withListAndActiveMobileMenuState)).toEqual(8)
  })

  it('getActiveItemMobileMenuIndex on deactivate', () => {
    expect(getActiveItemMobileMenuIndex(state)).toEqual(-1)
  })

  it('getItemFormData with no formData', () => {
    expect(
      getItemFormData({
        remove: null,
        price: null,
        defaultPrice: 1000
      })
    ).toEqual({ changed: false, price: 1000, priceString: '1,000', removeAmount: null })
  })

  it('getItemFormData with remove amount changed', () => {
    expect(
      getItemFormData({
        remove: 18,
        price: null,
        defaultPrice: 600
      })
    ).toEqual({ changed: true, price: 600, priceString: '600', removeAmount: 18 })
  })

  it('getItemFormData with price changed', () => {
    expect(
      getItemFormData({
        remove: null,
        price: 568,
        defaultPrice: 1000
      })
    ).toEqual({ changed: true, price: 568, priceString: '568', removeAmount: null })
  })

  it('getItemFormData with remove amount and price changed', () => {
    expect(
      getItemFormData({
        remove: 4,
        price: 568,
        priceString: '568',
        defaultPrice: 1000
      })
    ).toEqual({ changed: true, price: 568, priceString: '568', removeAmount: 4 })
  })
})
