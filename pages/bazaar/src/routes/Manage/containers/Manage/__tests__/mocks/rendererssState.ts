import { TRenderInfiniteLoaderSettings } from '../../types'

export const infiniteLoaderState: TRenderInfiniteLoaderSettings = {
  height: 500,
  isScrolling: false,
  onChildScroll: () => {},
  scrollTop: 300
}
