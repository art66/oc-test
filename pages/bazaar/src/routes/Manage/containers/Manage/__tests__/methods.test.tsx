import React from 'react'
import { Manage } from '..'
import { shallow } from 'enzyme'
import {
  initialState,
  activeViewIdState,
  withViewState,
  withViewWithoutHeightState,
  withMobileMenuDesktopState,
  withMobileMenuMobileState
} from './mocks/state'
import { rowData } from './mocks'
// import { delay } from './helpers'

describe('<Manage /> methods', () => {
  it('should _onUndoChages method works correctly', () => {
    const undoFormDataAction = jest.fn()
    const forceUpdateGrid = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, undoFormDataAction }} />)
    const component = Component.instance()

    component._forceUpdateGrid = forceUpdateGrid

    component._onUndoChages()

    expect(undoFormDataAction).toHaveBeenCalledTimes(1)

    expect(forceUpdateGrid).toHaveBeenCalledTimes(1)

    expect(undoFormDataAction).toMatchSnapshot()
    expect(forceUpdateGrid).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _onSubmit method works correctly', () => {
    const submitFormDataAction = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, submitFormDataAction }} />)
    const component = Component.instance()

    component._onSubmit({ preventDefault() {} })

    expect(submitFormDataAction).toHaveBeenCalledTimes(1)

    expect(submitFormDataAction).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _onPriceChanged method works correctly', () => {
    const itemPriceChangedAction = jest.fn()
    const forceUpdateGrid = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, itemPriceChangedAction }} />)
    const component = Component.instance()

    component._forceUpdateGrid = forceUpdateGrid

    component._onPriceChanged(5, 504, '504')

    expect(itemPriceChangedAction).toHaveBeenCalledTimes(1)
    expect(itemPriceChangedAction.mock.calls[0]).toEqual([5, 504, '504'])

    expect(forceUpdateGrid).toHaveBeenCalledTimes(1)

    expect(itemPriceChangedAction).toMatchSnapshot()
    expect(forceUpdateGrid).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _onItemRemove method works correctly', () => {
    const itemRemoveAction = jest.fn()
    const forceUpdateGrid = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, itemRemoveAction }} />)
    const component = Component.instance()

    component._forceUpdateGrid = forceUpdateGrid

    component._onItemRemove(5, 4)

    expect(itemRemoveAction).toHaveBeenCalledTimes(1)
    expect(itemRemoveAction.mock.calls[0]).toEqual([5, 4])

    expect(forceUpdateGrid).toHaveBeenCalledTimes(1)

    expect(itemRemoveAction).toMatchSnapshot()
    expect(forceUpdateGrid).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _onSortEnd method works correctly', () => {
    const itemDragAction = jest.fn()
    const forceUpdateGrid = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, itemDragAction }} />)
    const component = Component.instance()

    component._forceUpdateGrid = forceUpdateGrid

    component._onSortEnd({ oldIndex: 5, newIndex: 4 })

    expect(itemDragAction).toHaveBeenCalledTimes(1)
    expect(itemDragAction.mock.calls[0]).toEqual([5, 4])

    expect(forceUpdateGrid).toHaveBeenCalledTimes(1)

    expect(itemDragAction).toMatchSnapshot()
    expect(forceUpdateGrid).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _deactivateItemView method works correctly', () => {
    const setActiveViewUniqueIdAction = jest.fn()
    const recomputeRowHeight = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, setActiveViewUniqueIdAction }} />)
    const component = Component.instance()

    component._recomputeRowHeight = recomputeRowHeight

    component._deactivateItemView()

    expect(setActiveViewUniqueIdAction).toHaveBeenCalledTimes(1)
    expect(setActiveViewUniqueIdAction.mock.calls[0]).toEqual([null])

    expect(recomputeRowHeight).toHaveBeenCalledTimes(1)

    expect(setActiveViewUniqueIdAction).toMatchSnapshot()
    expect(recomputeRowHeight).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _activateItemView method activate view', () => {
    const setActiveViewUniqueIdAction = jest.fn()
    const recomputeRowHeight = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, setActiveViewUniqueIdAction }} />)
    const component = Component.instance()

    component._recomputeRowHeight = recomputeRowHeight

    component._activateItemView('388-4384562992')

    expect(setActiveViewUniqueIdAction).toHaveBeenCalledTimes(1)
    expect(setActiveViewUniqueIdAction.mock.calls[0]).toEqual(['388-4384562992'])

    expect(recomputeRowHeight).toHaveBeenCalledTimes(1)

    expect(setActiveViewUniqueIdAction).toMatchSnapshot()
    expect(recomputeRowHeight).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _activateItemView method deactivate view', () => {
    const setActiveViewUniqueIdAction = jest.fn()
    const recomputeRowHeight = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...activeViewIdState, setActiveViewUniqueIdAction }} />)
    const component = Component.instance()

    component._recomputeRowHeight = recomputeRowHeight

    component._activateItemView('388-4384562992')

    expect(setActiveViewUniqueIdAction).toHaveBeenCalledTimes(1)
    expect(setActiveViewUniqueIdAction.mock.calls[0]).toEqual([null])

    expect(recomputeRowHeight).toHaveBeenCalledTimes(1)

    expect(setActiveViewUniqueIdAction).toMatchSnapshot()
    expect(recomputeRowHeight).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _setViewHeight method works correctly', () => {
    const recomputeRowHeight = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    component._recomputeRowHeight = recomputeRowHeight

    component._setViewHeight()

    expect(recomputeRowHeight).toHaveBeenCalledTimes(1)

    expect(recomputeRowHeight).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _loadMoreRows method works correctly', () => {
    const itemsRequestMoreAction = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...initialState, itemsRequestMoreAction }} />)
    const component = Component.instance()

    component._loadMoreRows()

    expect(itemsRequestMoreAction).toHaveBeenCalledTimes(1)

    expect(itemsRequestMoreAction).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _isRowLoaded method works correctly', () => {
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    expect(component._isRowLoaded({ index: 10 })).toBeTruthy()
    expect(component._isRowLoaded({ index: 14 })).toBeTruthy()
    expect(component._isRowLoaded({ index: 15 })).toBeFalsy()
    expect(component._isRowLoaded({ index: 40 })).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowData method works correctly', () => {
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    expect(component._getRowData({ index: 10 })).toEqual(rowData)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method return default height', () => {
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 10 })).toEqual(36)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method return with view height', () => {
    const Component = shallow<Manage>(<Manage {...withViewState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 16 })).toEqual(86)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method return without view height', () => {
    const Component = shallow<Manage>(<Manage {...withViewWithoutHeightState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 16 })).toEqual(36)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method return with mobile menu height on desktop', () => {
    const Component = shallow<Manage>(<Manage {...withMobileMenuDesktopState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 5 })).toEqual(36)

    expect(Component).toMatchSnapshot()
  })

  it('should _getRowHeight method return with mobile menu height on mobile', () => {
    const Component = shallow<Manage>(<Manage {...withMobileMenuMobileState} />)
    const component = Component.instance()

    expect(component._getRowHeight({ index: 5 })).toEqual(126)

    expect(Component).toMatchSnapshot()
  })

  it('should _setActiveItemMobileMenuUniqueId method works', () => {
    const setActiveItemMobileMenuUniqueIdAction = jest.fn()
    const recomputeRowHeight = jest.fn()
    const Component = shallow<Manage>(<Manage {...{ ...activeViewIdState, setActiveItemMobileMenuUniqueIdAction }} />)
    const component = Component.instance()

    component._recomputeRowHeight = recomputeRowHeight

    component._setActiveItemMobileMenuUniqueId('388-4384562992')

    expect(setActiveItemMobileMenuUniqueIdAction).toHaveBeenCalledTimes(1)
    expect(setActiveItemMobileMenuUniqueIdAction.mock.calls[0]).toEqual(['388-4384562992'])

    expect(recomputeRowHeight).toHaveBeenCalledTimes(1)

    expect(setActiveItemMobileMenuUniqueIdAction).toMatchSnapshot()
    expect(recomputeRowHeight).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _forceUpdateGrid method works', () => {
    const forceUpdateGrid = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    component._listRef = null

    component._forceUpdateGrid()

    expect(forceUpdateGrid).toHaveBeenCalledTimes(0)

    component._listRef = { current: null }

    component._forceUpdateGrid()

    expect(forceUpdateGrid).toHaveBeenCalledTimes(0)

    component._listRef = { current: { forceUpdateGrid } }

    component._forceUpdateGrid()

    expect(forceUpdateGrid).toHaveBeenCalledTimes(1)

    expect(forceUpdateGrid).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _recomputeRowHeight method works', () => {
    const recomputeRowHeights = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    component._listRef = null

    component._recomputeRowHeight()

    expect(recomputeRowHeights).toHaveBeenCalledTimes(0)

    component._listRef = { current: null }

    component._recomputeRowHeight()

    expect(recomputeRowHeights).toHaveBeenCalledTimes(0)

    component._listRef = { current: { recomputeRowHeights } }

    component._recomputeRowHeight()

    expect(recomputeRowHeights).toHaveBeenCalledTimes(1)

    expect(recomputeRowHeights).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _unbindListBugFixer method works', () => {
    const clearInterval = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    component._bindListBugFixerIntervalId = 105

    const defaultClearInterval = window.clearInterval
    window.clearInterval = clearInterval
    try {
      component._unbindListBugFixer()
    } finally {
      window.clearInterval = defaultClearInterval
    }

    expect(clearInterval).toHaveBeenCalledTimes(1)
    expect(clearInterval.mock.calls[0]).toEqual([105])

    expect(clearInterval).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should _bindListBugFixer method works', async () => {
    const dispatchEvent = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    // const component = Component.instance()

    const defaultDispatchEvent = window.dispatchEvent
    try {
      // component._bindListBugFixer()
      // await delay(3500)
      // window.dispatchEvent = dispatchEvent
      // expect(dispatchEvent).toHaveBeenCalledTimes(0)
      // await delay(1500)
      // expect(dispatchEvent).toHaveBeenCalledTimes(0)
      // window.scroll({ top: 500 })
      // await delay(1500)
      // expect(dispatchEvent).toHaveBeenCalledTimes(0)
      // expect(dispatchEvent.mock.calls[0][0]).toEqual(new CustomEvent('resize'))
      // await delay(1500)
      // expect(dispatchEvent).toHaveBeenCalledTimes(1)
      // TODO: Should works
    } finally {
      window.dispatchEvent = defaultDispatchEvent
    }

    expect(dispatchEvent).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  }, 15000)

  it('should componentWillUnmount method works', () => {
    const unbindListBugFixer = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    component._unbindListBugFixer = unbindListBugFixer

    component.componentWillUnmount()

    expect(unbindListBugFixer).toHaveBeenCalledTimes(1)

    expect(unbindListBugFixer).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should componentDidUpdate skip bug fixer', () => {
    const dispatchEvent = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    const defaultDispatchEvent = window.dispatchEvent
    window.dispatchEvent = dispatchEvent
    try {
      component.componentDidUpdate(initialState)
    } finally {
      window.dispatchEvent = defaultDispatchEvent
    }

    expect(dispatchEvent).toHaveBeenCalledTimes(0)

    expect(dispatchEvent).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should componentDidUpdate handle bug fixer', () => {
    const dispatchEvent = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    const defaultDispatchEvent = window.dispatchEvent
    window.dispatchEvent = dispatchEvent
    try {
      component.componentDidUpdate({ ...initialState, rows: [] })
    } finally {
      window.dispatchEvent = defaultDispatchEvent
    }

    expect(dispatchEvent).toHaveBeenCalledTimes(1)

    expect(dispatchEvent).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should componentDidMount method works', () => {
    const initManage = jest.fn()
    const bindListBugFixer = jest.fn()
    const Component = shallow<Manage>(<Manage {...initialState} />)
    Component.setProps({ initManage })
    const component = Component.instance()

    component._bindListBugFixer = bindListBugFixer

    component.componentDidMount()

    expect(initManage).toHaveBeenCalledTimes(1)
    expect(bindListBugFixer).toHaveBeenCalledTimes(1)

    expect(initManage).toMatchSnapshot()
    expect(bindListBugFixer).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
