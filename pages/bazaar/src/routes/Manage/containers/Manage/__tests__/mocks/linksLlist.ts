export const headerLinks = [
  {
    key: 'name',
    className: 'name',
    title: 'Name',
    arialLevel: 5
  },
  {
    key: 'details',
    className: 'details',
    title: 'Details',
    arialLevel: null
  },
  {
    key: 'rrp',
    className: 'rrp',
    title: 'RRP',
    arialLevel: null
  },
  {
    key: 'remove',
    className: 'remove',
    title: 'Remove',
    arialLevel: null
  },
  {
    key: 'price',
    className: 'price',
    title: 'Price (PU)',
    arialLevel: null
  }
]
