import { TRows } from '../../types'

export const rows: TRows = [
  {
    data: {
      uniqueId: '394-0',
      itemId: '394',
      amount: 4,
      name: 'Brick',
      price: 8008,
      imgMediumUrl: '/images/items/394/medium.png',
      imgLargeUrl: '/images/items/394/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/394/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '0',
      type: 'Weapon',
      categoryName: 'Temporary',
      bazaarId: '28151330',
      averagePrice: 408,
      minPrice: 4 * Math.ceil(408 * 0.25),
      glow: null,
      damageBonus: '48.12',
      accuracyBonus: '43',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '404-0',
      itemId: '404',
      amount: 1,
      name: 'Bandana',
      price: 80007,
      imgMediumUrl: '/images/items/404/medium.png',
      imgLargeUrl: '/images/items/404/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/404/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '0',
      type: 'Item',
      categoryName: 'Clothing',
      bazaarId: '28151331',
      averagePrice: 866,
      minPrice: 1 * Math.ceil(866 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '806-0',
      itemId: '806',
      amount: 1,
      name: 'Old Lady Mask',
      price: 90009,
      imgMediumUrl: '/images/items/806/medium.png',
      imgLargeUrl: '/images/items/806/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/806/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '0',
      type: 'Item',
      categoryName: 'Clothing',
      bazaarId: '28151332',
      averagePrice: 21306,
      minPrice: 1 * Math.ceil(21306 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '556-0',
      itemId: '556',
      amount: 5,
      name: 'Bag of Reindeer Droppings',
      price: 600007,
      imgMediumUrl: '/images/items/556/medium.png',
      imgLargeUrl: '/images/items/556/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/556/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '0',
      type: 'Item',
      categoryName: 'Candy',
      bazaarId: '28151328',
      averagePrice: 138486,
      minPrice: 5 * Math.ceil(138486 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '529-0',
      itemId: '529',
      amount: 5,
      name: 'Bag of Chocolate Truffles',
      price: 800009,
      imgMediumUrl: '/images/items/529/medium.png',
      imgLargeUrl: '/images/items/529/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/529/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '0',
      type: 'Item',
      categoryName: 'Candy',
      bazaarId: '28151329',
      averagePrice: 139289,
      minPrice: 5 * Math.ceil(139289 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '646-5445138841',
      itemId: '646',
      amount: 1,
      name: 'Hiking Boots',
      price: 1000001,
      imgMediumUrl: '/images/items/646/medium.png',
      imgLargeUrl: '/images/items/646/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/646/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '5445138841',
      type: 'Armour',
      categoryName: 'Defensive',
      bazaarId: '33025699',
      averagePrice: 6569,
      minPrice: 1 * Math.ceil(6569 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '26',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872969',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5030995,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872969',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151305',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.62',
      accuracyBonus: '53.38',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872972',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 876879569,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872972',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151307',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.73',
      accuracyBonus: '53.14',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '731-0',
      itemId: '731',
      amount: 10,
      name: 'Empty Blood Bag',
      price: 1500000447,
      imgMediumUrl: '/images/items/731/medium.png',
      imgLargeUrl: '/images/items/731/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/731/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '0',
      type: 'Item',
      categoryName: 'Medical',
      bazaarId: '28151302',
      averagePrice: 17375,
      minPrice: 10 * Math.ceil(17375 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872967',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 6666666647,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872967',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151303',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.72',
      accuracyBonus: '53.97',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '541-0',
      itemId: '541',
      amount: 150,
      name: 'Bottle of Stinky Swamp Punch',
      price: 56564564569,
      imgMediumUrl: '/images/items/541/medium.png',
      imgLargeUrl: '/images/items/541/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/541/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '0',
      type: 'Item',
      categoryName: 'Alcohol',
      bazaarId: '28151333',
      averagePrice: 377043,
      minPrice: 150 * Math.ceil(377043 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '388-4384562992',
      itemId: '388',
      amount: 1,
      name: 'Pink Mac-10',
      price: 99999999998,
      imgMediumUrl: '/images/items/388/medium.png',
      imgLargeUrl: '/images/items/388/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/388/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '4384562992',
      type: 'Weapon',
      categoryName: 'Primary',
      bazaarId: '32013525',
      averagePrice: 0,
      minPrice: 1 * Math.ceil(0 * 0.25),
      glow: { color: 'FFBF00', opacity: 0.5 },
      damageBonus: '81.45',
      accuracyBonus: '45.26',
      armorBonus: '0',
      bonuses: [
        {
          className: 'bonus-attachment-emasculated',
          title: '<b>Emasculate</b><br/>Happiness increased by 15% of maximum upon final hit'
        },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872973',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872973',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151308',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.93',
      accuracyBonus: '53.03',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872974',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872974',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151309',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.62',
      accuracyBonus: '54.57',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872975',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872975',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151310',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.79',
      accuracyBonus: '54.8',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872976',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872976',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151311',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.85',
      accuracyBonus: '53.45',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872977',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872977',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151312',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.86',
      accuracyBonus: '53.32',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872978',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872978',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151313',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.62',
      accuracyBonus: '54.75',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872979',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872979',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151314',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.67',
      accuracyBonus: '54.01',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872980',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872980',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151315',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.69',
      accuracyBonus: '53.26',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872981',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872981',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151316',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.83',
      accuracyBonus: '53.35',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872982',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872982',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151317',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.66',
      accuracyBonus: '54.55',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872988',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872988',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151323',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '22',
      accuracyBonus: '53.11',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872983',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872983',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151318',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.62',
      accuracyBonus: '54.04',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872984',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872984',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151319',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.82',
      accuracyBonus: '54.16',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872985',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872985',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151320',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.68',
      accuracyBonus: '53.31',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872986',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 50000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872986',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151321',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.75',
      accuracyBonus: '53.11',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872987',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872987',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151322',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.91',
      accuracyBonus: '54.07',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872989',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872989',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151324',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.96',
      accuracyBonus: '53.35',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872990',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872990',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151325',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.83',
      accuracyBonus: '53.48',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872991',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872991',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151326',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.6',
      accuracyBonus: '54.01',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '12-3591872992',
      itemId: '12',
      amount: 1,
      name: 'Glock 17',
      price: 5000,
      imgMediumUrl: '/images/items/12/medium.png',
      imgLargeUrl: '/images/items/12/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/12/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '3591872992',
      type: 'Weapon',
      categoryName: 'Secondary',
      bazaarId: '28151327',
      averagePrice: 250,
      minPrice: 1 * Math.ceil(250 * 0.25),
      glow: null,
      damageBonus: '21.67',
      accuracyBonus: '53.06',
      armorBonus: '0',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  },
  {
    data: {
      uniqueId: '650-4745638267',
      itemId: '650',
      amount: 1,
      name: 'Leather Gloves',
      price: 1000000,
      imgMediumUrl: '/images/items/650/medium.png',
      imgLargeUrl: '/images/items/650/large.png?v=1528808940574',
      imgBlankUrl: '/images/items/650/large.png?v=1528808940574',
      rarity: 0,
      isBlockedForBuying: false,
      armoryId: '4745638267',
      type: 'Armour',
      categoryName: 'Defensive',
      bazaarId: '33025700',
      averagePrice: 603,
      minPrice: 1 * Math.ceil(603 * 0.25),
      glow: null,
      damageBonus: '0',
      accuracyBonus: '0',
      armorBonus: '21',
      bonuses: [
        { className: 'bonus-attachment-blank-bonus-25', title: null },
        { className: 'bonus-attachment-blank-bonus-25', title: null }
      ]
    }
  }
]
