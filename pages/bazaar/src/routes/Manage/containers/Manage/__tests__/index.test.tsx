import React from 'react'
import { shallow } from 'enzyme'
import Button from '@torn/shared/components/Button'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import { Manage } from '..'
import {
  initialState,
  isUserNotAvailableState,
  isLoadingMoreState,
  isMobileState,
  doesntExistsState
} from './mocks/state'
import Panel from '../../../../Personalize/components/Panel'
import { headerLinks } from './mocks/linksLlist'
import { SortableContainer } from '../hocs'
import { PanelHeader } from '../../../../../components'

describe('<Manage />', () => {
  it('should render', () => {
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    expect(Component.is('form')).toBeTruthy()
    const PanelComponent = Component.children()
    expect(PanelComponent.is(Panel)).toBeTruthy()
    expect(PanelComponent.prop('className')).toEqual('items')
    const ComponentChildren = PanelComponent.children()
    expect(ComponentChildren.length).toBe(4)
    const MainHeaderComponent = ComponentChildren.at(0)
    expect(MainHeaderComponent.is(PanelHeader)).toBeTruthy()
    expect(MainHeaderComponent.prop('children')).toBe('Manage your Bazaar')
    const ConfirmationComponent = ComponentChildren.at(1)
    expect(ConfirmationComponent.prop('className')).toBe('confirmation')
    expect(ConfirmationComponent.prop('role')).toBe('heading')
    expect(ConfirmationComponent.prop('aria-level')).toBe(5)
    expect(ConfirmationComponent.prop('aria-label')).toBe('Confirmation section')
    const ConfirmationChildren = ConfirmationComponent.children()
    expect(ConfirmationChildren.length).toBe(2)
    const SaveFormDataComponent = ConfirmationChildren.at(0)
    expect(SaveFormDataComponent.is(Button)).toBeTruthy()
    expect(SaveFormDataComponent.prop('children')).toBe('SAVE CHANGES')
    const UndoFormDataComponent = ConfirmationChildren.at(1)
    expect(UndoFormDataComponent.prop('className')).toBe('undo')
    expect(UndoFormDataComponent.prop('onClick')).toBe(component._onUndoChages)
    expect(UndoFormDataComponent.text()).toBe('Undo changes')
    const HeaderComponent = ComponentChildren.at(2)
    expect(HeaderComponent.prop('className')).toEqual('cellsHeader')
    expect(HeaderComponent.prop('role')).toEqual('row')
    const HeaderChildren = HeaderComponent.children()
    expect(HeaderChildren.length).toBe(5)
    HeaderChildren.forEach((HeaderCellComponent, index) => {
      expect(HeaderCellComponent.prop('className')).toBe(headerLinks[index].className)
      expect(HeaderCellComponent.text()).toBe(headerLinks[index].title)
    })
    const SortableContainerComponent = ComponentChildren.at(3)
    expect(SortableContainerComponent.is(SortableContainer)).toBeTruthy()
    expect(SortableContainerComponent.prop('onSortEnd')).toBe(component._onSortEnd)
    expect(SortableContainerComponent.prop('useWindowAsScrollContainer')).toBeTruthy()
    expect(SortableContainerComponent.prop('useDragHandle')).toBeTruthy()
    const SortableContainerChildren = SortableContainerComponent.children()
    expect(SortableContainerChildren.length).toBe(1)
    const WindowScrollerComponent = SortableContainerChildren.at(0)
    expect(WindowScrollerComponent.prop('scrollingResetTimeInterval')).toBe(500)
    expect(WindowScrollerComponent.prop('children')).toBe(component._renderInfiniteLoader)

    expect(Component).toMatchSnapshot()
  })

  it('should render on mobile', () => {
    const Component = shallow<Manage>(<Manage {...isMobileState} />)
    const component = Component.instance()

    expect(Component.is('form')).toBeTruthy()
    const PanelComponent = Component.children()
    expect(PanelComponent.is(Panel)).toBeTruthy()
    expect(PanelComponent.prop('className')).toEqual('items')
    const ComponentChildren = PanelComponent.children()
    expect(ComponentChildren.length).toBe(4)
    const MainHeaderComponent = ComponentChildren.at(0)
    expect(MainHeaderComponent.is(PanelHeader)).toBeTruthy()
    expect(MainHeaderComponent.prop('children')).toBe('Manage your Bazaar')
    const ConfirmationComponent = ComponentChildren.at(1)
    expect(ConfirmationComponent.prop('className')).toBe('confirmation')
    expect(ConfirmationComponent.prop('role')).toBe('heading')
    expect(ConfirmationComponent.prop('aria-level')).toBe(5)
    expect(ConfirmationComponent.prop('aria-label')).toBe('Confirmation section')
    const ConfirmationChildren = ConfirmationComponent.children()
    expect(ConfirmationChildren.length).toBe(2)
    const SaveFormDataComponent = ConfirmationChildren.at(0)
    expect(SaveFormDataComponent.is(Button)).toBeTruthy()
    expect(SaveFormDataComponent.prop('children')).toBe('SAVE CHANGES')
    const UndoFormDataComponent = ConfirmationChildren.at(1)
    expect(UndoFormDataComponent.prop('className')).toBe('undo')
    expect(UndoFormDataComponent.prop('onClick')).toBe(component._onUndoChages)
    expect(UndoFormDataComponent.text()).toBe('Undo changes')
    const HeaderComponent = ComponentChildren.at(2)
    expect(HeaderComponent.prop('className')).toEqual('cellsHeader')
    expect(HeaderComponent.prop('role')).toEqual('row')
    const HeaderChildren = HeaderComponent.children()
    expect(HeaderChildren.length).toBe(1)
    const HeaderCellComponent = HeaderChildren.at(0)
    expect(HeaderCellComponent.prop('className')).toBe('manage')
    expect(HeaderCellComponent.prop('aria-level')).toBe(5)
    expect(HeaderCellComponent.text()).toBe('Manage items')
    const SortableContainerComponent = ComponentChildren.at(3)
    expect(SortableContainerComponent.is(SortableContainer)).toBeTruthy()
    expect(SortableContainerComponent.prop('onSortEnd')).toBe(component._onSortEnd)
    expect(SortableContainerComponent.prop('useWindowAsScrollContainer')).toBeTruthy()
    expect(SortableContainerComponent.prop('useDragHandle')).toBeTruthy()
    const SortableContainerChildren = SortableContainerComponent.children()
    expect(SortableContainerChildren.length).toBe(1)
    const WindowScrollerComponent = SortableContainerChildren.at(0)
    expect(WindowScrollerComponent.prop('scrollingResetTimeInterval')).toBe(500)
    expect(WindowScrollerComponent.prop('children')).toBe(component._renderInfiniteLoader)

    expect(Component).toMatchSnapshot()
  })

  it("should render when user isn't available", () => {
    const Component = shallow<Manage>(<Manage {...isUserNotAvailableState} />)

    expect(Component.type()).toBe(null)

    expect(Component).toMatchSnapshot()
  })

  it("should render when bazaar doesn't exists", () => {
    const Component = shallow<Manage>(<Manage {...doesntExistsState} />)

    expect(Component.type()).toBe(null)

    expect(Component).toMatchSnapshot()
  })

  it('should render when is loading more', () => {
    const Component = shallow<Manage>(<Manage {...isLoadingMoreState} />)
    const component = Component.instance()

    expect(Component.is('form')).toBeTruthy()
    const PanelComponent = Component.children()
    expect(PanelComponent.is(Panel)).toBeTruthy()
    expect(PanelComponent.prop('className')).toEqual('items')
    const ComponentChildren = PanelComponent.children()
    expect(ComponentChildren.length).toBe(5)
    const MainHeaderComponent = ComponentChildren.at(0)
    expect(MainHeaderComponent.is(PanelHeader)).toBeTruthy()
    expect(MainHeaderComponent.prop('children')).toBe('Manage your Bazaar')
    const ConfirmationComponent = ComponentChildren.at(1)
    expect(ConfirmationComponent.prop('className')).toBe('confirmation')
    expect(ConfirmationComponent.prop('role')).toBe('heading')
    expect(ConfirmationComponent.prop('aria-level')).toBe(5)
    expect(ConfirmationComponent.prop('aria-label')).toBe('Confirmation section')
    const ConfirmationChildren = ConfirmationComponent.children()
    expect(ConfirmationChildren.length).toBe(2)
    const SaveFormDataComponent = ConfirmationChildren.at(0)
    expect(SaveFormDataComponent.is(Button)).toBeTruthy()
    expect(SaveFormDataComponent.prop('children')).toBe('SAVE CHANGES')
    const UndoFormDataComponent = ConfirmationChildren.at(1)
    expect(UndoFormDataComponent.prop('className')).toBe('undo')
    expect(UndoFormDataComponent.prop('onClick')).toBe(component._onUndoChages)
    expect(UndoFormDataComponent.text()).toBe('Undo changes')
    const HeaderComponent = ComponentChildren.at(2)
    expect(HeaderComponent.prop('className')).toEqual('cellsHeader')
    expect(HeaderComponent.prop('role')).toEqual('row')
    const HeaderChildren = HeaderComponent.children()
    expect(HeaderChildren.length).toBe(5)
    HeaderChildren.forEach((HeaderCellComponent, index) => {
      expect(HeaderCellComponent.prop('className')).toBe(headerLinks[index].className)
      expect(HeaderCellComponent.text()).toBe(headerLinks[index].title)
    })
    const SortableContainerComponent = ComponentChildren.at(3)
    expect(SortableContainerComponent.is(SortableContainer)).toBeTruthy()
    expect(SortableContainerComponent.prop('onSortEnd')).toBe(component._onSortEnd)
    expect(SortableContainerComponent.prop('useWindowAsScrollContainer')).toBeTruthy()
    expect(SortableContainerComponent.prop('useDragHandle')).toBeTruthy()
    const SortableContainerChildren = SortableContainerComponent.children()
    expect(SortableContainerChildren.length).toBe(1)
    const WindowScrollerComponent = SortableContainerChildren.at(0)
    expect(WindowScrollerComponent.prop('scrollingResetTimeInterval')).toBe(500)
    expect(WindowScrollerComponent.prop('children')).toBe(component._renderInfiniteLoader)
    const LoadingMoreComponent = ComponentChildren.at(4)
    expect(LoadingMoreComponent.is(LoadingIndicator)).toBeTruthy()
    const LoadingMoreChildren = LoadingMoreComponent.children()
    expect(LoadingMoreChildren.length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
