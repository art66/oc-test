import React from 'react'
import { shallow } from 'enzyme'
import { List } from 'react-virtualized'
import { Manage } from '..'
import {
  initialState,
  activeViewIdState,
  isMobileState,
  withFormDataState,
  withMobileMenuUniqueIdDesktopState,
  withMobileMenuUniqueIdMobileState
} from './mocks/state'
import { infiniteLoaderState } from './mocks/rendererssState'

describe('<Manage /> render handlers', () => {
  it('should render _renderInfiniteLoader layout', () => {
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    const ListComponent = shallow(component._renderInfiniteLoader(infiniteLoaderState))
    expect(ListComponent.is(List)).toBeTruthy()
    // expect(ListComponent.key()).toBe('565454765789')
    expect(ListComponent.prop('autoHeight')).toBeTruthy()
    expect(ListComponent.prop('width')).toBe(768)
    expect(ListComponent.prop('height')).toBe(500)
    expect(ListComponent.prop('isScrolling')).toBeFalsy()
    expect(ListComponent.prop('onScroll')).toBe(infiniteLoaderState.onChildScroll)
    expect(ListComponent.prop('scrollTop')).toBe(300)
    expect(ListComponent.prop('rowCount')).toBe(15)
    // @ts-ignore
    expect(ListComponent.prop('rowHeight')).toBe(component._getRowHeight)
    // @ts-ignore
    expect(ListComponent.prop('rowRenderer')).toBe(component._renderRow)
    // @ts-ignore
    expect(ListComponent.prop('noRowsRenderer')).toBe(component._renderNoItems)
    expect(typeof ListComponent.prop('onRowsRendered')).toBe('function')
    expect(component._listRef).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderNoItems layout', () => {
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    const ListMessageComponent = shallow(component._renderNoItems())
    expect(ListMessageComponent.is('div')).toBeTruthy()
    expect(ListMessageComponent.text()).toBe('There are no items on the bazaar right now')

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderRow layout', () => {
    const props = { key: '46-3', index: 5, style: { left: '30px' } }
    const Component = shallow<Manage>(<Manage {...initialState} />)
    const component = Component.instance()

    const sortableComponent = component._renderRow(props)
    expect(sortableComponent.key).toBe('46-3')
    expect(sortableComponent.props.index).toBe(5)
    const rowComponent = sortableComponent.props.children
    expect(rowComponent.props.className).toBe('row')
    expect(rowComponent.props.style).toEqual({ left: '30px' })
    expect(rowComponent.props.children.length).toBe(2)
    const itemComponent = rowComponent.props.children[0]
    expect(itemComponent.props.removeAmount).toBeUndefined()
    expect(itemComponent.props.accuracyBonus).toBe('0')
    expect(itemComponent.props.activateItemMobileMenu).toBe(component._setActiveItemMobileMenuUniqueId)
    expect(itemComponent.props.active).toBeFalsy()
    expect(itemComponent.props.amount).toBe(1)
    expect(itemComponent.props.armorBonus).toBe('26')
    expect(itemComponent.props.armoryId).toBe('5445138841')
    expect(itemComponent.props.averagePrice).toBe(6569)
    expect(itemComponent.props.bazaarId).toBe('33025699')
    expect(itemComponent.props.bonuses).toEqual([
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      },
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      }
    ])
    expect(itemComponent.props.categoryName).toBe('Defensive')
    expect(itemComponent.props.changed).toBeFalsy()
    expect(itemComponent.props.damageBonus).toBe('0')
    expect(itemComponent.props.glow).toBeNull()
    expect(itemComponent.props.imgLargeUrl).toBe('/images/items/646/large.png?v=1528808940574')
    expect(itemComponent.props.imgMediumUrl).toBe('/images/items/646/medium.png')
    expect(itemComponent.props.index).toBe(5)
    expect(itemComponent.props.isMobile).toBeFalsy()
    expect(itemComponent.props.itemId).toBe('646')
    expect(itemComponent.props.mobileMenuActive).toBeFalsy()
    expect(itemComponent.props.name).toBe('Hiking Boots')
    expect(itemComponent.props.price).toBe(1000001)
    expect(itemComponent.props.type).toBe('Armour')
    expect(itemComponent.props.uniqueId).toBe('646-5445138841')
    expect(itemComponent.props.onItemRemove).toBe(component._onItemRemove)
    expect(itemComponent.props.onPriceChange).toBe(component._onPriceChanged)
    expect(itemComponent.props.setActiveViewUniqueId).toEqual(component._activateItemView)
    const viewComponent = rowComponent.props.children[1]
    expect(viewComponent).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderRow layout on mobile', () => {
    const props = { key: '46-3', index: 5, style: { left: '30px' } }
    const Component = shallow<Manage>(<Manage {...isMobileState} />)
    const component = Component.instance()

    const sortableComponent = component._renderRow(props)
    expect(sortableComponent.key).toBe('46-3')
    expect(sortableComponent.props.index).toBe(5)
    const rowComponent = sortableComponent.props.children
    expect(rowComponent.props.className).toBe('row')
    expect(rowComponent.props.style).toEqual({ left: '30px' })
    expect(rowComponent.props.children.length).toBe(2)
    const itemComponent = rowComponent.props.children[0]
    expect(itemComponent.props.removeAmount).toBeUndefined()
    expect(itemComponent.props.accuracyBonus).toBe('0')
    expect(itemComponent.props.activateItemMobileMenu).toBe(component._setActiveItemMobileMenuUniqueId)
    expect(itemComponent.props.active).toBeFalsy()
    expect(itemComponent.props.amount).toBe(1)
    expect(itemComponent.props.armorBonus).toBe('26')
    expect(itemComponent.props.armoryId).toBe('5445138841')
    expect(itemComponent.props.averagePrice).toBe(6569)
    expect(itemComponent.props.bazaarId).toBe('33025699')
    expect(itemComponent.props.bonuses).toEqual([
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      },
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      }
    ])
    expect(itemComponent.props.categoryName).toBe('Defensive')
    expect(itemComponent.props.changed).toBeFalsy()
    expect(itemComponent.props.damageBonus).toBe('0')
    expect(itemComponent.props.glow).toBeNull()
    expect(itemComponent.props.imgLargeUrl).toBe('/images/items/646/large.png?v=1528808940574')
    expect(itemComponent.props.imgMediumUrl).toBe('/images/items/646/medium.png')
    expect(itemComponent.props.index).toBe(5)
    expect(itemComponent.props.isMobile).toBeTruthy()
    expect(itemComponent.props.itemId).toBe('646')
    expect(itemComponent.props.mobileMenuActive).toBeFalsy()
    expect(itemComponent.props.name).toBe('Hiking Boots')
    expect(itemComponent.props.price).toBe(1000001)
    expect(itemComponent.props.type).toBe('Armour')
    expect(itemComponent.props.uniqueId).toBe('646-5445138841')
    expect(itemComponent.props.onItemRemove).toBe(component._onItemRemove)
    expect(itemComponent.props.onPriceChange).toBe(component._onPriceChanged)
    expect(itemComponent.props.setActiveViewUniqueId).toEqual(component._activateItemView)
    const viewComponent = rowComponent.props.children[1]
    expect(viewComponent).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderRow layout with view', () => {
    const props = { key: '46-3', index: 5, style: { left: '30px' } }
    const Component = shallow<Manage>(<Manage {...isMobileState} />)
    const component = Component.instance()

    const sortableComponent = component._renderRow(props)
    expect(sortableComponent.key).toBe('46-3')
    expect(sortableComponent.props.index).toBe(5)
    const rowComponent = sortableComponent.props.children
    expect(rowComponent.props.className).toBe('row')
    expect(rowComponent.props.style).toEqual({ left: '30px' })
    expect(rowComponent.props.children.length).toBe(2)
    const itemComponent = rowComponent.props.children[0]
    expect(itemComponent.props.removeAmount).toBeUndefined()
    expect(itemComponent.props.accuracyBonus).toBe('0')
    expect(itemComponent.props.activateItemMobileMenu).toBe(component._setActiveItemMobileMenuUniqueId)
    expect(itemComponent.props.active).toBeFalsy()
    expect(itemComponent.props.amount).toBe(1)
    expect(itemComponent.props.armorBonus).toBe('26')
    expect(itemComponent.props.armoryId).toBe('5445138841')
    expect(itemComponent.props.averagePrice).toBe(6569)
    expect(itemComponent.props.bazaarId).toBe('33025699')
    expect(itemComponent.props.bonuses).toEqual([
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      },
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      }
    ])
    expect(itemComponent.props.categoryName).toBe('Defensive')
    expect(itemComponent.props.changed).toBeFalsy()
    expect(itemComponent.props.damageBonus).toBe('0')
    expect(itemComponent.props.glow).toBeNull()
    expect(itemComponent.props.imgLargeUrl).toBe('/images/items/646/large.png?v=1528808940574')
    expect(itemComponent.props.imgMediumUrl).toBe('/images/items/646/medium.png')
    expect(itemComponent.props.index).toBe(5)
    expect(itemComponent.props.isMobile).toBeTruthy()
    expect(itemComponent.props.itemId).toBe('646')
    expect(itemComponent.props.mobileMenuActive).toBeFalsy()
    expect(itemComponent.props.name).toBe('Hiking Boots')
    expect(itemComponent.props.price).toBe(1000001)
    expect(itemComponent.props.type).toBe('Armour')
    expect(itemComponent.props.uniqueId).toBe('646-5445138841')
    expect(itemComponent.props.onItemRemove).toBe(component._onItemRemove)
    expect(itemComponent.props.onPriceChange).toBe(component._onPriceChanged)
    expect(itemComponent.props.setActiveViewUniqueId).toEqual(component._activateItemView)
    const viewComponent = rowComponent.props.children[1]
    expect(viewComponent).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderRow layout with formData', () => {
    const props = { key: '46-3', index: 5, style: { left: '30px' } }
    const Component = shallow<Manage>(<Manage {...withFormDataState} />)
    const component = Component.instance()

    const sortableComponent = component._renderRow(props)
    expect(sortableComponent.key).toBe('46-3')
    expect(sortableComponent.props.index).toBe(5)
    const rowComponent = sortableComponent.props.children
    expect(rowComponent.props.className).toBe('row')
    expect(rowComponent.props.style).toEqual({ left: '30px' })
    expect(rowComponent.props.children.length).toBe(2)
    const itemComponent = rowComponent.props.children[0]
    expect(itemComponent.props.removeAmount).toBe(10)
    expect(itemComponent.props.accuracyBonus).toBe('0')
    expect(itemComponent.props.activateItemMobileMenu).toBe(component._setActiveItemMobileMenuUniqueId)
    expect(itemComponent.props.active).toBeFalsy()
    expect(itemComponent.props.amount).toBe(1)
    expect(itemComponent.props.armorBonus).toBe('26')
    expect(itemComponent.props.armoryId).toBe('5445138841')
    expect(itemComponent.props.averagePrice).toBe(6569)
    expect(itemComponent.props.bazaarId).toBe('33025699')
    expect(itemComponent.props.bonuses).toEqual([
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      },
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      }
    ])
    expect(itemComponent.props.categoryName).toBe('Defensive')
    expect(itemComponent.props.changed).toBeTruthy()
    expect(itemComponent.props.damageBonus).toBe('0')
    expect(itemComponent.props.glow).toBeNull()
    expect(itemComponent.props.imgLargeUrl).toBe('/images/items/646/large.png?v=1528808940574')
    expect(itemComponent.props.imgMediumUrl).toBe('/images/items/646/medium.png')
    expect(itemComponent.props.index).toBe(5)
    expect(itemComponent.props.isMobile).toBeFalsy()
    expect(itemComponent.props.itemId).toBe('646')
    expect(itemComponent.props.mobileMenuActive).toBeFalsy()
    expect(itemComponent.props.name).toBe('Hiking Boots')
    expect(itemComponent.props.price).toBe(52345)
    expect(itemComponent.props.type).toBe('Armour')
    expect(itemComponent.props.uniqueId).toBe('646-5445138841')
    expect(itemComponent.props.onItemRemove).toBe(component._onItemRemove)
    expect(itemComponent.props.onPriceChange).toBe(component._onPriceChanged)
    expect(itemComponent.props.setActiveViewUniqueId).toEqual(component._activateItemView)
    const viewComponent = rowComponent.props.children[1]
    expect(viewComponent).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderRow layout with mobile menu active', () => {
    const props = { key: '46-3', index: 5, style: { left: '30px' } }
    const Component = shallow<Manage>(<Manage {...withMobileMenuUniqueIdMobileState} />)
    const component = Component.instance()

    const sortableComponent = component._renderRow(props)
    expect(sortableComponent.key).toBe('46-3')
    expect(sortableComponent.props.index).toBe(5)
    const rowComponent = sortableComponent.props.children
    expect(rowComponent.props.className).toBe('row')
    expect(rowComponent.props.style).toEqual({ left: '30px' })
    expect(rowComponent.props.children.length).toBe(2)
    const itemComponent = rowComponent.props.children[0]
    expect(itemComponent.props.accuracyBonus).toBe('0')
    expect(itemComponent.props.activateItemMobileMenu).toBe(component._setActiveItemMobileMenuUniqueId)
    expect(itemComponent.props.active).toBeTruthy()
    expect(itemComponent.props.amount).toBe(1)
    expect(itemComponent.props.armorBonus).toBe('26')
    expect(itemComponent.props.armoryId).toBe('5445138841')
    expect(itemComponent.props.averagePrice).toBe(6569)
    expect(itemComponent.props.bazaarId).toBe('33025699')
    expect(itemComponent.props.bonuses).toEqual([
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      },
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      }
    ])
    expect(itemComponent.props.categoryName).toBe('Defensive')
    expect(itemComponent.props.changed).toBeFalsy()
    expect(itemComponent.props.damageBonus).toBe('0')
    expect(itemComponent.props.glow).toBeNull()
    expect(itemComponent.props.imgLargeUrl).toBe('/images/items/646/large.png?v=1528808940574')
    expect(itemComponent.props.imgMediumUrl).toBe('/images/items/646/medium.png')
    expect(itemComponent.props.index).toBe(5)
    expect(itemComponent.props.isMobile).toBeTruthy()
    expect(itemComponent.props.itemId).toBe('646')
    expect(itemComponent.props.mobileMenuActive).toBeTruthy()
    expect(itemComponent.props.name).toBe('Hiking Boots')
    expect(itemComponent.props.price).toBe(1000001)
    expect(itemComponent.props.type).toBe('Armour')
    expect(itemComponent.props.uniqueId).toBe('646-5445138841')
    expect(itemComponent.props.onItemRemove).toBe(component._onItemRemove)
    expect(itemComponent.props.onPriceChange).toBe(component._onPriceChanged)
    expect(itemComponent.props.setActiveViewUniqueId).toEqual(component._activateItemView)
    const viewComponent = rowComponent.props.children[1]
    expect(viewComponent).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderRow layout with mobile menu active on desktop', () => {
    const props = { key: '46-3', index: 5, style: { left: '30px' } }
    const Component = shallow<Manage>(<Manage {...withMobileMenuUniqueIdDesktopState} />)
    const component = Component.instance()

    const sortableComponent = component._renderRow(props)
    expect(sortableComponent.key).toBe('46-3')
    expect(sortableComponent.props.index).toBe(5)
    const rowComponent = sortableComponent.props.children
    expect(rowComponent.props.className).toBe('row')
    expect(rowComponent.props.style).toEqual({ left: '30px' })
    expect(rowComponent.props.children.length).toBe(2)
    const itemComponent = rowComponent.props.children[0]
    expect(itemComponent.props.accuracyBonus).toBe('0')
    expect(itemComponent.props.activateItemMobileMenu).toBe(component._setActiveItemMobileMenuUniqueId)
    expect(itemComponent.props.active).toBeFalsy()
    expect(itemComponent.props.amount).toBe(1)
    expect(itemComponent.props.armorBonus).toBe('26')
    expect(itemComponent.props.armoryId).toBe('5445138841')
    expect(itemComponent.props.averagePrice).toBe(6569)
    expect(itemComponent.props.bazaarId).toBe('33025699')
    expect(itemComponent.props.bonuses).toEqual([
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      },
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      }
    ])
    expect(itemComponent.props.categoryName).toBe('Defensive')
    expect(itemComponent.props.changed).toBeFalsy()
    expect(itemComponent.props.damageBonus).toBe('0')
    expect(itemComponent.props.glow).toBeNull()
    expect(itemComponent.props.imgLargeUrl).toBe('/images/items/646/large.png?v=1528808940574')
    expect(itemComponent.props.imgMediumUrl).toBe('/images/items/646/medium.png')
    expect(itemComponent.props.index).toBe(5)
    expect(itemComponent.props.isMobile).toBeFalsy()
    expect(itemComponent.props.itemId).toBe('646')
    expect(itemComponent.props.mobileMenuActive).toBeFalsy()
    expect(itemComponent.props.name).toBe('Hiking Boots')
    expect(itemComponent.props.price).toBe(1000001)
    expect(itemComponent.props.type).toBe('Armour')
    expect(itemComponent.props.uniqueId).toBe('646-5445138841')
    expect(itemComponent.props.onItemRemove).toBe(component._onItemRemove)
    expect(itemComponent.props.onPriceChange).toBe(component._onPriceChanged)
    expect(itemComponent.props.setActiveViewUniqueId).toEqual(component._activateItemView)
    const viewComponent = rowComponent.props.children[1]
    expect(viewComponent).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderView layout', () => {
    const Component = shallow<Manage>(<Manage {...activeViewIdState} />)
    const component = Component.instance()

    const viewComponent = component._renderView(true, '388-4384562992')
    expect(viewComponent).toBeTruthy()
    expect(viewComponent.props.className).toBe('view')
    expect(viewComponent.props.uniqueId).toBe('388-4384562992')
    expect(viewComponent.props.onClose).toBe(component._deactivateItemView)
    expect(viewComponent.props.onHeightUpdate).toBe(component._setViewHeight)

    expect(Component).toMatchSnapshot()
  })

  it('should render _renderView without layout', () => {
    const Component = shallow<Manage>(<Manage {...activeViewIdState} />)
    const component = Component.instance()

    const viewComponent = component._renderView(false, '388-4384562992')
    expect(viewComponent).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })
})
