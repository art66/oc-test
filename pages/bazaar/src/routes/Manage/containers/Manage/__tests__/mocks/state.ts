import { rows } from './rows'
import { IProps } from '../../types'
import { withViewRows } from '.'

export const initialState: IProps = {
  isDesktop: true,
  screenWidth: 768,
  isUserAvailable: true,
  rows,
  isLoadingMore: false,
  isBazaarExists: true,
  dataUniqueKey: '565454765789',
  total: 60,
  amount: 15,
  activeViewId: null,
  viewHeight: null,
  activeItemMobileMenuUniqueId: null,
  activeItemMobileMenuIndex: null,
  formData: {},
  isChanged: false,
  isValid: true,
  itemsRequestMoreAction: () => {},
  setActiveViewUniqueIdAction: () => {},
  setActiveItemMobileMenuUniqueIdAction: () => {},
  itemDragAction: () => {},
  itemRemoveAction: () => {},
  itemPriceChangedAction: () => {},
  submitFormDataAction: () => {},
  undoFormDataAction: () => {},
  initManage: () => {}
}

export const isMobileState: IProps = {
  ...initialState,
  isDesktop: false
}

export const isUserNotAvailableState: IProps = {
  ...initialState,
  isUserAvailable: false
}

export const isLoadingMoreState: IProps = {
  ...initialState,
  isLoadingMore: true
}

export const activeViewIdState: IProps = {
  ...initialState,
  activeViewId: '388-4384562992'
}

export const withViewState: IProps = {
  ...initialState,
  activeViewId: '12-3591872976',
  viewHeight: 50,
  rows: withViewRows
}

export const withViewWithoutHeightState: IProps = {
  ...initialState,
  activeViewId: '12-3591872976',
  rows: withViewRows
}

export const withMobileMenuDesktopState: IProps = {
  ...initialState,
  activeItemMobileMenuIndex: 5
}

export const withMobileMenuMobileState: IProps = {
  ...initialState,
  isDesktop: false,
  activeItemMobileMenuIndex: 5
}

export const withMobileMenuUniqueIdDesktopState: IProps = {
  ...initialState,
  activeItemMobileMenuUniqueId: '646-5445138841'
}

export const withMobileMenuUniqueIdMobileState: IProps = {
  ...initialState,
  isDesktop: false,
  activeItemMobileMenuUniqueId: '646-5445138841'
}

export const withFormDataState: IProps = {
  ...initialState,
  formData: {
    '646-5445138841': {
      bazaarID: '33025699',
      itemID: '646',
      sort: true,
      remove: 10,
      price: 52345
    }
  }
}

export const doesntExistsState: IProps = {
  ...initialState,
  isBazaarExists: false
}
