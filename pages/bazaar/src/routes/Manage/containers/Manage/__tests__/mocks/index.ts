import { state as globalState } from '../../../../../../modules/__tests__/mocks/state'
import { list } from './list'
import { IState } from '../../../../../../store/types'
import { rows } from './rows'
import { TRows, TRowData } from '../../types'

export { list }

export const state: IState = {
  ...globalState,
  manage: {
    items: {
      isLoading: false,
      isLoadingMore: false,
      list: [],
      total: 0,
      loadFrom: 0,
      limit: 50,
      amount: 0,
      uniqueKey: null
    },
    form: {
      data: {},
      isSubmitting: false,
      error: null,
      errors: null
    },
    activeView: {
      uniqueId: null,
      itemId: null,
      armoryId: null
    },
    activeItemMobileMenuUniqueId: null
  }
}

export const withListState: IState = {
  ...state,
  manage: {
    ...state.manage,
    items: {
      ...state.manage.items,
      list,
      total: list.length
    }
  }
}

export const withActiveViewState: IState = {
  ...state,
  manage: {
    ...state.manage,
    activeView: {
      ...state.manage.activeView,
      uniqueId: '12-3591872977'
    }
  }
}

export const withListAndActiveViewState: IState = {
  ...state,
  manage: {
    ...state.manage,
    items: {
      ...withListState.manage.items
    },
    activeView: {
      ...withActiveViewState.manage.activeView
    }
  }
}

export const withActiveMobileMenuState: IState = {
  ...state,
  manage: {
    ...state.manage,
    activeItemMobileMenuUniqueId: '731-0'
  }
}

export const withListAndActiveMobileMenuState: IState = {
  ...state,
  manage: {
    ...state.manage,
    items: {
      ...withListState.manage.items
    },
    activeItemMobileMenuUniqueId: withActiveMobileMenuState.manage.activeItemMobileMenuUniqueId
  }
}

export const withViewRows: TRows = [
  ...rows.slice(0, 16),
  {
    ...rows[16],
    withView: true
  },
  ...rows.slice(17)
]

export const rowData: TRowData = {
  data: {
    accuracyBonus: '0',
    amount: 150,
    armorBonus: '0',
    armoryId: '0',
    averagePrice: 377043,
    minPrice: 150 * Math.ceil(377043 * 0.25),
    bazaarId: '28151333',
    bonuses: [
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      },
      {
        className: 'bonus-attachment-blank-bonus-25',
        title: null
      }
    ],
    categoryName: 'Alcohol',
    damageBonus: '0',
    glow: null,
    imgLargeUrl: '/images/items/541/large.png?v=1528808940574',
    imgMediumUrl: '/images/items/541/medium.png',
    imgBlankUrl: '/images/items/541/large.png?v=1528808940574',
    rarity: 0,
    isBlockedForBuying: false,
    itemId: '541',
    name: 'Bottle of Stinky Swamp Punch',
    price: 56564564569,
    type: 'Item',
    uniqueId: '541-0'
  }
}
