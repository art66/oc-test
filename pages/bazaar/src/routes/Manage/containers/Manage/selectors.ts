import { createSelector } from 'reselect'
import {
  TRow,
  TGetItemFormData,
  TGetActiveItemMobileMenuIndex,
  TGetRowsSelector,
  TGetRowHeightSelector,
  TGetItems,
  TGetActiveViewUniqueId,
  TGetActiveItemMobileMenuUniqueId,
  TIsFormDataValid
} from './types'
import { TItem } from '../../../../modules/types/state'
import { numberToCurrency } from '../../../../utils'

export const getItems: TGetItems = state => state.manage.items.list
export const getActiveViewUniqueId: TGetActiveViewUniqueId = state => state.manage.activeView.uniqueId
export const getActiveItemMobileMenuUniqueId: TGetActiveItemMobileMenuUniqueId = state =>
  state.manage.activeItemMobileMenuUniqueId

export const getRowHeight: TGetRowHeightSelector = createSelector(
  [data => data],
  ({ viewHeight, mobileMenuActive }): number => {
    let rowHeight = 36

    if (viewHeight) {
      rowHeight += viewHeight
    }

    if (mobileMenuActive) {
      rowHeight += 90
    }

    return rowHeight
  }
)

export const getRows: TGetRowsSelector = createSelector(
  [getItems, getActiveViewUniqueId],
  (items: TItem[], activeViewUniqueId: string): TRow[] => {
    const rows = items.map(item => {
      return {
        data: item
      }
    })

    if (activeViewUniqueId) {
      let viewItemIndex = -1
      items.forEach((item, index) => {
        if (activeViewUniqueId === item.uniqueId) {
          viewItemIndex = index
        }
      })

      if (viewItemIndex >= 0) {
        const { [viewItemIndex]: viewItemRow } = rows

        const rowWithView = {
          ...viewItemRow,
          withView: true
        }

        rows.splice(viewItemIndex, 1, rowWithView)
      }
    }

    return rows
  }
)

export const getActiveItemMobileMenuIndex: TGetActiveItemMobileMenuIndex = createSelector(
  [getItems, getActiveItemMobileMenuUniqueId],
  (items: TItem[], activeItemMobileMenuUniqueId: string) => {
    return items.findIndex(item => item.uniqueId === activeItemMobileMenuUniqueId)
  }
)

export const getItemFormData: TGetItemFormData = createSelector(
  [data => data],
  ({ remove, price: changedPrice, defaultPrice, priceString: changedPriceString }) => {
    const removeAmount = remove
    const price = typeof changedPrice === 'number' ? changedPrice : defaultPrice
    const priceString =
      typeof changedPriceString === 'string'
        ? changedPriceString
        : typeof price === 'number'
        ? numberToCurrency(price)
        : ''
    const changed = !!(removeAmount || (price && price !== defaultPrice))

    return {
      removeAmount,
      price,
      priceString,
      changed
    }
  }
)

export const isFormDataValid: TIsFormDataValid = createSelector([data => data], formData => {
  return !Object.entries(formData).some(([, itemValiation]) => itemValiation.isInvalid)
})
