import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { List, WindowScroller, InfiniteLoader } from 'react-virtualized'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import Button from '@torn/shared/components/Button'
import { Panel } from '../../../Personalize/components'
import { View, PanelHeader, ListMessage } from '../../../../components'
import styles from './index.cssmodule.scss'
import {
  TRenderRow,
  TRenderHeader,
  TMapStateToProps,
  IProps,
  TIsRowLoaded,
  TLoadMoreRows,
  TGetRowData,
  TMapDispatchToProps,
  TOnSetViewHeight,
  TRenderView,
  TRenderNoItems,
  TDeactivateItemView,
  TGetRowHeight,
  TSetActiveItemMobileMenuUniqueId,
  TActivateItemView,
  TRenderHeaderColumns,
  TOnItemRemove,
  TOnUndoChages,
  TOnSortEnd,
  TRecomputeRowHeight,
  TForceUpdateGrid,
  TBindListBugFixer,
  TUnbindListBugFixer,
  TRenderInfiniteLoader
} from './types'
import { Item } from '../../components/Item'
import {
  initManage as initManageAction,
  itemsRequestMore,
  setActiveViewUniqueId,
  setActiveItemMobileMenuUniqueId,
  itemDrag,
  itemRemove,
  itemPriceChanged,
  submitFormData,
  undoFormData
} from '../../modules/actions'
import { getRows, getRowHeight, getActiveItemMobileMenuIndex, getItemFormData, isFormDataValid } from './selectors'
import { NO_ITEMS_MESSAGE } from './constants'
import { getScreenWidth, getMediaType } from '../../../../modules/selectors'
import { SortableElement, SortableContainer } from './hocs'
import { DESKTOP_TYPE } from '../../../../constants'

export class Manage extends PureComponent<IProps> {
  _listRef: React.RefObject<List>
  _bindListBugFixerIntervalId: number

  constructor(props: IProps) {
    super(props)

    this._listRef = React.createRef()
  }

  componentDidMount() {
    const { initManage } = this.props

    initManage()

    this._bindListBugFixer()
    /*
     * TODO: Should use it for fix a bug when top content height changed
     *       and on scroll to bottom are blank items on top of the list
     * */
  }

  componentDidUpdate(prevProps: IProps) {
    if (this.props.rows !== prevProps.rows) {
      window.dispatchEvent(new CustomEvent('scroll'))
      // TODO: Should use it for fix a bug when scroll on bottom and after refresh page are blank items
    }
  }

  componentWillUnmount() {
    this._unbindListBugFixer()
  }

  _disabled() {
    const { isChanged, isValid } = this.props

    return !isChanged || isValid
  }

  _bindListBugFixer: TBindListBugFixer = () => {
    let bodyScrollHeight = document.body.scrollHeight

    this._bindListBugFixerIntervalId = window.setInterval(() => {
      if (document.body.scrollHeight !== bodyScrollHeight) {
        window.dispatchEvent(new CustomEvent('resize'))

        bodyScrollHeight = document.body.scrollHeight
      }
    }, 1000)
  }

  _unbindListBugFixer: TUnbindListBugFixer = () => {
    window.clearInterval(this._bindListBugFixerIntervalId)
  }

  _recomputeRowHeight: TRecomputeRowHeight = () => {
    if (this._listRef && this._listRef.current) {
      this._listRef.current.recomputeRowHeights()
    }
  }

  _forceUpdateGrid: TForceUpdateGrid = () => {
    if (this._listRef && this._listRef.current) {
      this._listRef.current.forceUpdateGrid()
    }
  }

  _setActiveItemMobileMenuUniqueId: TSetActiveItemMobileMenuUniqueId = uniqueId => {
    const { setActiveItemMobileMenuUniqueIdAction } = this.props

    setActiveItemMobileMenuUniqueIdAction(uniqueId)

    this._recomputeRowHeight()
  }

  _getRowHeight: TGetRowHeight = ({ index }) => {
    const { viewHeight, isDesktop, activeItemMobileMenuIndex } = this.props

    const { withView } = this._getRowData({ index })

    return getRowHeight({
      viewHeight: withView ? viewHeight : 0,
      mobileMenuActive: !isDesktop && activeItemMobileMenuIndex === index
    })
  }

  _getRowData: TGetRowData = ({ index }) => this.props.rows[index]

  _isRowLoaded: TIsRowLoaded = ({ index }) => {
    return this.props.amount > index
  }

  _loadMoreRows: TLoadMoreRows = () => {
    const { itemsRequestMoreAction } = this.props

    itemsRequestMoreAction()
  }

  _setViewHeight: TOnSetViewHeight = () => {
    this._recomputeRowHeight()
  }

  _activateItemView: TActivateItemView = uniqueId => {
    const { activeViewId, setActiveViewUniqueIdAction } = this.props

    if (uniqueId !== activeViewId) {
      setActiveViewUniqueIdAction(uniqueId)
    } else {
      setActiveViewUniqueIdAction(null)
    }

    this._recomputeRowHeight()
  }

  _deactivateItemView: TDeactivateItemView = () => {
    const { setActiveViewUniqueIdAction } = this.props

    setActiveViewUniqueIdAction(null)

    this._recomputeRowHeight()
  }

  _onSortEnd: TOnSortEnd = ({ oldIndex, newIndex }) => {
    const { itemDragAction } = this.props

    itemDragAction(oldIndex, newIndex)

    this._forceUpdateGrid()
  }

  _onItemRemove: TOnItemRemove = (index, amount) => {
    const { itemRemoveAction } = this.props

    itemRemoveAction(index, amount)
    // FIXME: We should use item uniqueId

    this._forceUpdateGrid()
  }

  _onPriceChanged = (index: number, price: number, priceString: string) => {
    const { itemPriceChangedAction } = this.props

    itemPriceChangedAction(index, price, priceString)
    // FIXME: We should use item uniqueId

    this._forceUpdateGrid()
  }

  _onSubmit = event => {
    const { submitFormDataAction } = this.props

    submitFormDataAction()

    event.preventDefault()
  }

  _onUndoChages: TOnUndoChages = () => {
    const { undoFormDataAction } = this.props

    undoFormDataAction()

    this._forceUpdateGrid()
  }

  _renderHeaderColumns: TRenderHeaderColumns = () => {
    const { isDesktop } = this.props

    if (!isDesktop) {
      return (
        <li className={styles.manage} role='heading' aria-level={5}>
          Manage items
        </li>
      )
    }

    return (
      <>
        <li className={styles.name} role='heading' aria-level={5}>
          Name
        </li>
        <li className={styles.details}>Details</li>
        <li className={styles.rrp}>RRP</li>
        <li className={styles.remove}>Remove</li>
        <li className={styles.price}>Price (PU)</li>
      </>
    )
  }

  _renderHeader: TRenderHeader = () => {
    return (
      <ul className={styles.cellsHeader} role='row'>
        {this._renderHeaderColumns()}
      </ul>
    )
  }

  _renderView: TRenderView = (withView, uniqueId) => {
    if (!withView) {
      return null
    }

    return (
      <View
        className={styles.view}
        uniqueId={uniqueId}
        onHeightUpdate={this._setViewHeight}
        onClose={this._deactivateItemView}
      />
    )
  }

  _renderRow: TRenderRow = ({ key, index, style }) => {
    const { data, withView } = this._getRowData({ index })
    const { isDesktop, activeItemMobileMenuUniqueId, formData } = this.props
    const mobileMenuActive = !isDesktop && activeItemMobileMenuUniqueId === data.uniqueId
    const { [data.uniqueId]: formDataItem } = formData
    const itemFormData = getItemFormData({ ...formDataItem, defaultPrice: data.price })

    return (
      <SortableElement key={key} index={index}>
        <div className={styles.row} style={style}>
          <Item
            {...data}
            {...itemFormData}
            priceIsInvalid={formDataItem && formDataItem.priceIsInvalid}
            index={index}
            active={withView || mobileMenuActive}
            isMobile={!isDesktop}
            mobileMenuActive={mobileMenuActive}
            setActiveViewUniqueId={this._activateItemView}
            activateItemMobileMenu={this._setActiveItemMobileMenuUniqueId}
            onItemRemove={this._onItemRemove}
            onPriceChange={this._onPriceChanged}
          />
          {this._renderView(withView, data.uniqueId)}
        </div>
      </SortableElement>
    )
  }

  _renderNoItems: TRenderNoItems = () => {
    return <ListMessage>{NO_ITEMS_MESSAGE}</ListMessage>
  }

  _renderInfiniteLoader: TRenderInfiniteLoader = ({ height, isScrolling, onChildScroll, scrollTop }) => {
    const { total, /* dataUniqueKey,  */ amount, screenWidth } = this.props

    return (
      <InfiniteLoader
        isRowLoaded={this._isRowLoaded}
        loadMoreRows={this._loadMoreRows}
        rowCount={total}
        minimumBatchSize={100}
        threshold={10}
      >
        {/* tslint:disable-next-line: jsx-no-multiline-js */}
        {({ onRowsRendered }) => (
          <List
            // key={dataUniqueKey}
            // It removed temporarily. Because the Drag-and-drop plugin doesn't work when the key was updating.
            autoHeight={true}
            width={screenWidth}
            height={height}
            isScrolling={isScrolling}
            onScroll={onChildScroll}
            scrollTop={scrollTop}
            rowCount={amount}
            rowHeight={this._getRowHeight}
            rowRenderer={this._renderRow}
            noRowsRenderer={this._renderNoItems}
            onRowsRendered={onRowsRendered}
            ref={this._listRef}
          />
        )}
      </InfiniteLoader>
    )
  }

  render() {
    const { isLoadingMore, isUserAvailable, total, isBazaarExists } = this.props

    if (!isUserAvailable || !total || !isBazaarExists) {
      return null
    }

    return (
      <form onSubmit={this._onSubmit}>
        <Panel className={styles.items}>
          <PanelHeader>Manage your Bazaar</PanelHeader>
          <div className={styles.confirmation} role='heading' aria-level={5} aria-label='Confirmation section'>
            <Button disabled={this._disabled()}>SAVE CHANGES</Button>
            <button type='button' className={styles.undo} onClick={this._onUndoChages}>
              Undo changes
            </button>
          </div>
          {this._renderHeader()}
          <SortableContainer onSortEnd={this._onSortEnd} useWindowAsScrollContainer={true} useDragHandle={true}>
            <WindowScroller scrollingResetTimeInterval={500}>
              {/* tslint:disable-next-line: jsx-no-multiline-js */}
              {this._renderInfiniteLoader}
            </WindowScroller>
          </SortableContainer>
          {isLoadingMore && <LoadingIndicator />}
        </Panel>
      </form>
    )
  }
}

const mapStateToProps: TMapStateToProps = state => {
  const { uniqueId: activeViewId } = state.manage.activeView
  const { [activeViewId]: viewSettings } = state.common.viewsSettings
  const mediaType = getMediaType(state)
  const {
    activeItemMobileMenuUniqueId,
    items: { isLoadingMore, uniqueKey, total, amount },
    form: { data: formData }
  } = state.manage

  return {
    isDesktop: mediaType === DESKTOP_TYPE,
    screenWidth: getScreenWidth(state),
    isUserAvailable: typeof state.common.owner.data.userId === 'string',
    rows: getRows(state),
    isLoadingMore,
    dataUniqueKey: uniqueKey,
    total,
    amount,
    activeViewId,
    viewHeight: viewSettings && viewSettings.height,
    activeItemMobileMenuUniqueId,
    activeItemMobileMenuIndex: getActiveItemMobileMenuIndex(state),
    formData,
    isChanged: Object.keys(formData).some(key => formData[key].isChanged),
    isValid: !isFormDataValid(formData),
    isBazaarExists: state.common.metadata.data?.isBazaarExists
  }
}

const mapDispatchToProps: TMapDispatchToProps = {
  initManage: initManageAction,
  itemsRequestMoreAction: itemsRequestMore,
  setActiveViewUniqueIdAction: setActiveViewUniqueId,
  setActiveItemMobileMenuUniqueIdAction: setActiveItemMobileMenuUniqueId,
  itemDragAction: itemDrag,
  itemRemoveAction: itemRemove,
  itemPriceChangedAction: itemPriceChanged,
  submitFormDataAction: submitFormData,
  undoFormDataAction: undoFormData
}

export default connect(mapStateToProps, mapDispatchToProps)(Manage)
