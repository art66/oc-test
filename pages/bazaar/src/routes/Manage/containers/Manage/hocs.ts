import { sortableContainer, sortableElement } from 'react-sortable-hoc'

// tslint:disable-next-line
export const SortableContainer = sortableContainer(({ children }) => {
  return children
})
// TODO: Ignored because tslint required camelCase variable name

// tslint:disable-next-line
export const SortableElement = sortableElement(({ children }) => {
  return children
})
// TODO: Ignored because tslint required camelCase variable name
