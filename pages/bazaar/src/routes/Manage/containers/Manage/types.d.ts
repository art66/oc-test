import { TFormData, TItemsList } from '../../modules/types'
import { IState } from '../../../../store/types'
import { TItem } from '../../../../modules/types/state'

export type TRowData = TRow

export type THeaderRowRendererArgs = {
  columns: JSX.Element[]
  style: React.CSSProperties
}

export type TRenderHeader = () => JSX.Element

export type TGetRowDataArgs = { index: number }

export type TGetRowHeight = ({ index }: { index: number }) => number

export type TGetRowData = (params: TGetRowDataArgs) => TRow

export type TRowRendererArgs = {
  key?: string
  index: number
  style?: React.CSSProperties
}

export type TRenderRow = (params: TRowRendererArgs) => JSX.Element

export type TStateProps = {
  isDesktop: boolean
  screenWidth: number
  isLoadingMore: boolean
  dataUniqueKey: string
  total: number
  amount: number
  rows: TRow[]
  viewHeight: number
  activeItemMobileMenuUniqueId?: string
  activeItemMobileMenuIndex?: number
  activeViewId?: string
  formData: TFormData
  isUserAvailable: boolean
  isChanged: boolean
  isValid: boolean
  isBazaarExists: boolean
}

export type TMapStateToProps = (state: IState) => TStateProps

export interface IProps extends TStateProps, TMapDispatchToProps {}

export type TSetActiveItemMobileMenuUniqueId = (uniqueId: string) => void

export type TIsRowLoadedArgs = { index: number }

export type TIsRowLoaded = (params: TIsRowLoadedArgs) => boolean

export type TLoadMoreRows = () => any

export type TMapDispatchToProps = {
  itemsRequestMoreAction: () => void
  setActiveViewUniqueIdAction: (uniqueId: string) => void
  setActiveItemMobileMenuUniqueIdAction: (uniqueId: string) => void
  itemDragAction: (oldIndex: number, newIndex: number) => void
  itemRemoveAction: (index: number, amount: number) => void
  itemPriceChangedAction: (index: number, price: number, priceString: string) => void
  submitFormDataAction: () => void
  undoFormDataAction: () => void
  initManage: () => void
}

export type TRow = {
  data: TItem
  withView?: boolean
}

export type TOnSetViewHeight = () => any

export type TRenderView = (withView: boolean, uniqueId: string) => JSX.Element

export type TActivateItemView = (uniqueId: string) => void

export type TDeactivateItemView = () => void

export type TRenderNoItems = () => JSX.Element

export type TRenderHeaderColumns = () => JSX.Element

export type TOnItemRemove = (index: number, amount: number) => void

export type TOnPriceChanged = (index: string, price: number) => void

export type TGetItemFormDataArgs = { remove?: number; price?: number; priceString?: string; defaultPrice: number }

export type TItemChange = {
  removeAmount: number
  price: number
  priceString: string
  changed: boolean
}

export type TGetItemFormData = (args: TGetItemFormDataArgs) => TItemChange

export type TIsFormDataValid = (formData: TFormData) => boolean

export type TOnUndoChages = () => void

export type TOnSortEndSettings = {
  oldIndex: number
  newIndex: number
}

export type TOnSortEnd = (settings: TOnSortEndSettings) => void

export type TRecomputeRowHeight = () => void

export type TForceUpdateGrid = () => void

export type TBindListBugFixer = () => void

export type TUnbindListBugFixer = () => void

export type TGetActiveItemMobileMenuIndex = (state: IState) => number

export type TRows = TRow[]

export type TGetRowsSelector = (state: IState) => TRows

export type TGetRowHeightSelectorSettings = { viewHeight: number; mobileMenuActive: boolean }

export type TGetRowHeightSelector = (settings: TGetRowHeightSelectorSettings) => number

export type TGetItems = (state: IState) => TItemsList

export type TGetActiveViewUniqueId = (state: IState) => string

export type TGetActiveItemMobileMenuUniqueId = (state: IState) => string

export type TRenderInfiniteLoaderSettings = {
  height: number
  isScrolling: boolean
  onChildScroll: Function
  scrollTop: number
}

export type TRenderInfiniteLoader = (settings: TRenderInfiniteLoaderSettings) => JSX.Element
