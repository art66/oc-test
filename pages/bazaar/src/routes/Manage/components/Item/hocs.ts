import { sortableHandle } from 'react-sortable-hoc'

// tslint:disable-next-line
export const SortableHandle = sortableHandle(({ children }) => {
  return children
})
// TODO: Ignored because tslint required camelCase variable name
