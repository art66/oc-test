import React, { PureComponent } from 'react'
import cn from 'classnames'
import getItemGlowClassName from '@torn/shared/utils/getItemGlowClassName'
import {
  IProps,
  TState,
  TOnRemoveAmoutChange,
  TRenderBonuses,
  TOnActivateView,
  TActivateItemMobileMenu,
  TRenderMobileMenuActivators,
  TRenderPrice,
  TRenderRemove,
  TRenderRrp,
  TRenderDescription,
  TRenderDraggableButton,
  TRenderContent,
  TRenderBonusesCell,
  TGetRrpValue,
  TRenderRemoveInput,
  TRenderPriceInput,
  TOnBlur,
  TGetTooltipId,
  TRenderExtraBonuses,
  TRenderExtraBonus,
  TIsBonusHasTooltip,
  TAddBonuesTooltip,
  TRenderDamageBonus,
  TRenderSimpleBonuses,
  TShowBonuses,
  TRenderArmorBonus,
  TRemoveBonuesTooltip,
  TRenderAccuracyBonus,
  TRenderTooltip
} from './types'
import styles from './index.cssmodule.scss'
import { NumberInput } from '../../../../components'
import { WEAPON_TYPE, ARMOUR_TYPE } from './constants'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import MoneyInput from '@torn/shared/components/MoneyInput'
import { VIEW_ICON_CONFIG } from '../../../Bazaar/constants'
import { getRrp, showBonusesSelector } from './selectors'
import { SortableHandle } from './hocs'
import { numberToCurrency } from '../../../../utils'
import { AMOUNT_MAX_VALUE } from '../../../../constants'
import { MAX_MONEY_LIMIT } from '../../../../../../../globals/constants'

export class Item extends PureComponent<IProps, TState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      itemChanged: this.props.changed
    }
  }

  static getDerivedStateFromProps(props: IProps, state: TState) {
    if (state.itemChanged && !props.changed) {
      return {
        itemChanged: props.changed
      }
    }

    return null
  }

  componentDidMount() {
    this._addBonuesTooltip()
  }

  componentWillUnmount() {
    this._removeBonusesTooltip()
  }

  _showBonuses: TShowBonuses = () => {
    const { type } = this.props

    return showBonusesSelector(type)
  }

  _addBonuesTooltip: TAddBonuesTooltip = () => {
    const { bonuses } = this.props

    bonuses.forEach((bonus, index) => {
      if (this._isBonusHasTooltip(bonus.className)) {
        tooltipsSubscriber.subscribe({
          ID: this._getTooltipId(bonus.className, index),
          child: this._renderTooltip(bonus.title)
        })
      }
    })
  }

  _removeBonusesTooltip: TRemoveBonuesTooltip = () => {
    const { bonuses } = this.props

    bonuses.forEach((bonus, index) => {
      if (this._isBonusHasTooltip(bonus.className)) {
        tooltipsSubscriber.unsubscribe({
          ID: this._getTooltipId(bonus.className, index),
          child: this._renderTooltip(bonus.title)
        })
      }
    })
  }

  _getTooltipId: TGetTooltipId = (className, index) => {
    const { uniqueId } = this.props

    if (this._isBonusHasTooltip(className)) {
      return `tooltip-${uniqueId}-${index}`
    }
  }

  _isBonusHasTooltip: TIsBonusHasTooltip = className => {
    return !className.includes('bonus-attachment-blank-bonus-25')
  }

  _getPriceString = () => {
    const { price } = this.props

    return numberToCurrency(price)
  }

  _getClassName = () => {
    const { className, active, mobileMenuActive } = this.props
    const { itemChanged } = this.state

    return cn(
      styles.item,
      { [styles.changed]: itemChanged, [styles.active]: active, [styles.mobileMenuActive]: mobileMenuActive },
      className
    )
  }

  _onRemoveAmoutChange: TOnRemoveAmoutChange = value => {
    const { index, onItemRemove } = this.props

    onItemRemove(index, value)
  }

  _onPriceChange = (priceString: string) => {
    const { index, onPriceChange } = this.props

    onPriceChange(index, parseInt(priceString.replace(/\,/g, ''), 10), priceString)
  }

  _onActivateView: TOnActivateView = () => {
    const { uniqueId, setActiveViewUniqueId } = this.props

    setActiveViewUniqueId(uniqueId)
  }

  _onBlur: TOnBlur = () => {
    const { changed } = this.props

    this.setState({
      itemChanged: changed
    })
  }

  _activateItemMobileMenu: TActivateItemMobileMenu = () => {
    const { uniqueId, activateItemMobileMenu, mobileMenuActive } = this.props

    if (!mobileMenuActive) {
      activateItemMobileMenu(uniqueId)
    } else {
      activateItemMobileMenu(null)
    }
  }

  _getRrpValue: TGetRrpValue = () => {
    const { averagePrice } = this.props

    return getRrp(averagePrice)
  }

  _getAriaLabel = price => {
    return `Price: ${price}`
  }

  _getRemoveMaxLimit() {
    const { amount } = this.props

    const maxLimit = amount < AMOUNT_MAX_VALUE ? amount : AMOUNT_MAX_VALUE

    return maxLimit
  }

  _renderTooltip: TRenderTooltip = title => {
    return <span dangerouslySetInnerHTML={{ __html: title }} />
  }

  _renderImage = () => {
    const { imgMediumUrl, name, rarity } = this.props

    return (
      <div className={styles.imgContainer} onClick={this._onActivateView}>
        <img className={getItemGlowClassName(rarity)} src={imgMediumUrl} alt={name} />
      </div>
    )
  }

  _renderBottomMobileMenu = () => {
    const { mobileMenuActive, isMobile } = this.props

    if (mobileMenuActive && isMobile) {
      return (
        <div className={styles.bottomMobileMenu}>
          <div className={styles.rrpMobile}>
            <span className={styles.valueDesc}>RRP:&nbsp;</span>
            <span>{this._getRrpValue()}</span>
          </div>
          <div className={styles.removeMobile}>
            <span className={styles.valueDesc}>Remove:&nbsp;</span>
            {this._renderRemoveInput()}
          </div>
          <div className={styles.priceMobile}>
            <span className={styles.valueDesc}>Price per unit:&nbsp;</span>
            {this._renderPriceInput()}
          </div>
        </div>
      )
    }
  }

  _renderDraggableButton: TRenderDraggableButton = () => {
    return (
      <SortableHandle>
        <div className={styles.draggableIconContainer}>
          <i className={styles.draggableIcon} />
        </div>
      </SortableHandle>
    )
  }

  _renderDescription: TRenderDescription = () => {
    const { name, amount } = this.props

    return (
      <div className={styles.desc} onClick={this._onActivateView} role='heading' aria-level={6}>
        <span>
          <b>{name}</b> x{amount}
        </span>
      </div>
    )
  }

  _renderExtraBonus: TRenderExtraBonus = (index, bonus) => {
    return (
      <i
        key={index}
        id={this._getTooltipId(bonus.className, index)}
        className={cn(styles.extraBonusIcon, bonus.className)}
      />
    )
  }

  _renderExtraBonuses: TRenderExtraBonuses = () => {
    const { bonuses } = this.props

    if (!(bonuses.length > 0)) {
      return null
    }

    return <div className={styles.bonus}>{bonuses.map((bonus, index) => this._renderExtraBonus(index, bonus))}</div>
  }

  _renderArmorBonus: TRenderArmorBonus = () => {
    const { armorBonus } = this.props

    if (!armorBonus) {
      return null
    }

    return (
      <div className={styles.bonus}>
        <i className={styles.defenceBonusIcon} />
        <span className={styles.bonusValue}>{armorBonus}</span>
      </div>
    )
  }

  _renderAccuracyBonus: TRenderAccuracyBonus = () => {
    const { accuracyBonus } = this.props

    if (!accuracyBonus) {
      return null
    }

    return (
      <div className={styles.bonus}>
        <i className={styles.accuracyBonusIcon} />
        <span className={styles.bonusValue}>{accuracyBonus}</span>
      </div>
    )
  }

  _renderDamageBonus: TRenderDamageBonus = () => {
    const { damageBonus } = this.props

    if (!damageBonus) {
      return null
    }

    return (
      <div className={cn(styles.bonus, styles.noBorder)}>
        <i className={styles.damageBonusIcon} />
        <span className={styles.bonusValue}>{damageBonus}</span>
      </div>
    )
  }

  _renderSimpleBonuses: TRenderSimpleBonuses = () => {
    const { type } = this.props

    if (type === WEAPON_TYPE) {
      return (
        <>
          {this._renderDamageBonus()}
          {this._renderAccuracyBonus()}
        </>
      )
    }

    if (type === ARMOUR_TYPE) {
      return this._renderArmorBonus()
    }

    return null
  }

  _renderBonuses: TRenderBonuses = () => {
    if (!this._showBonuses()) {
      return null
    }

    return (
      <>
        {this._renderSimpleBonuses()}
        {this._renderExtraBonuses()}
      </>
    )
  }

  _renderBonusesCell: TRenderBonusesCell = () => {
    return <div className={styles.bonuses}>{this._renderBonuses()}</div>
  }

  _renderRrp: TRenderRrp = () => {
    return <div className={styles.rrp}>{this._getRrpValue()}</div>
  }

  _renderRemoveInput: TRenderRemoveInput = () => {
    const { removeAmount, name } = this.props

    return (
      <NumberInput
        className={styles.removeAmountInput}
        value={removeAmount?.toString()}
        max={this._getRemoveMaxLimit()}
        onChange={this._onRemoveAmoutChange}
        onBlur={this._onBlur}
        ariaLabel={`Remove: ${name}`}
      />
    )
  }

  _renderRemove: TRenderRemove = () => {
    return <div className={styles.remove}>{this._renderRemoveInput()}</div>
  }

  _renderPriceInput: TRenderPriceInput = () => {
    const { priceString, priceIsInvalid } = this.props

    return (
      <MoneyInput
        disableReadOnly={true}
        disableSignButton={true}
        disableOnMountInputFocus={true}
        value={priceString}
        onChange={this._onPriceChange}
        userMoney={MAX_MONEY_LIMIT}
        onBlur={this._onBlur}
        customStyles={{
          inputBlock: cn(styles.inputBlock, { [styles.invalid]: priceIsInvalid }),
          input: styles.input
        }}
      />
    )
  }

  _renderPrice: TRenderPrice = () => {
    return <div className={styles.price}>{this._renderPriceInput()}</div>
  }

  _renderMobileMenuActivators: TRenderMobileMenuActivators = () => {
    const { mobileMenuActive } = this.props

    return (
      <div className={styles.menuActivators}>
        <button type='button' className={styles.iconContainer} onClick={this._onActivateView} aria-label='Info'>
          <SVGIconGenerator {...VIEW_ICON_CONFIG} />
        </button>
        <button
          type='button'
          className={styles.iconContainer}
          onClick={this._activateItemMobileMenu}
          aria-label='Manage'
        >
          <span className={cn(styles.arrowIcon, { [styles.active]: mobileMenuActive })} />
        </button>
      </div>
    )
  }

  _renderContent: TRenderContent = () => {
    const { isMobile } = this.props

    if (isMobile) {
      return (
        <div className={styles.mobileContainer}>
          <div className={styles.visiblePanel}>
            {this._renderDraggableButton()}
            {this._renderImage()}
            {this._renderDescription()}
            {this._renderMobileMenuActivators()}
          </div>
          {this._renderBottomMobileMenu()}
        </div>
      )
    }

    return (
      <>
        {this._renderDraggableButton()}
        {this._renderImage()}
        {this._renderDescription()}
        {this._renderBonusesCell()}
        {this._renderRrp()}
        {this._renderRemove()}
        {this._renderPrice()}
      </>
    )
  }

  render() {
    const { name } = this.props

    return (
      <div className={this._getClassName()} tabIndex={0} aria-label={name}>
        {this._renderContent()}
      </div>
    )
  }
}
