export const REMOVE_AMOUNT_DEFAULT_VALUE = 0

export const PRICE_MIN_VALUE = 1

export const WEAPON_TYPE = 'Weapon'
export const ARMOUR_TYPE = 'Armour'
