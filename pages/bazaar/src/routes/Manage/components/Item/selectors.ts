import { createSelector } from 'reselect'
import { WEAPON_TYPE, ARMOUR_TYPE } from './constants'
import { TShowBonusesSelector } from './types'
import { numberToCurrency } from '../../../../utils'

export const getRrp = createSelector([data => data], (value: number): string => {
  if (value > 0) {
    return `$${numberToCurrency(value)}`
  }

  return 'N/A'
})

export const showBonusesSelector: TShowBonusesSelector = createSelector([data => data], (type: string): boolean => {
  return type === WEAPON_TYPE || type === ARMOUR_TYPE
})
