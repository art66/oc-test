import React from 'react'
import { shallow } from 'enzyme'
import { Item } from '..'
import {
  mobileMenuActiveState,
  mobileState,
  mobileWithClassNameState,
  mobileActiveState,
  mobileWithRemoveAmountState,
  mobilePriceAmountState
} from './mocks'
import { NumberInput } from '../../../../../components'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { VIEW_ICON_CONFIG } from '../../../../Bazaar/constants'
import { SortableHandle } from '../hocs'
import MoneyInput from '@torn/shared/components/MoneyInput'

describe('<Item />', () => {
  it('should render with mobile state', () => {
    const Component = shallow<Item>(<Item {...mobileState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(1)
    const MobileContainerComponent = ComponentChildren.at(0)
    expect(MobileContainerComponent.prop('className')).toBe('mobileContainer')
    const MobileContainerChildren = MobileContainerComponent.children()
    expect(MobileContainerChildren.length).toBe(1)
    const VisiblePanelComponent = MobileContainerChildren.at(0)
    expect(VisiblePanelComponent.prop('className')).toBe('visiblePanel')
    const VisiblePanelChildren = VisiblePanelComponent.children()
    expect(VisiblePanelChildren.length).toBe(4)
    // Draggable button
    const SortableHandleComponent = VisiblePanelChildren.at(0)
    expect(SortableHandleComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = SortableHandleComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = VisiblePanelChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = VisiblePanelChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bottom menus activators
    const MenuActivatorsComponent = VisiblePanelChildren.at(3)
    expect(MenuActivatorsComponent.prop('className')).toBe('menuActivators')
    const MenuActivatorsChildren = MenuActivatorsComponent.children()
    expect(MenuActivatorsChildren.length).toBe(2)
    const ActivateViewComponent = MenuActivatorsChildren.at(0)
    expect(ActivateViewComponent.is('button')).toBeTruthy()
    expect(ActivateViewComponent.prop('className')).toBe('iconContainer')
    expect(ActivateViewComponent.prop('onClick')).toBe(component._onActivateView)
    const ActivateViewChildren = ActivateViewComponent.children()
    expect(ActivateViewChildren.length).toBe(1)
    const ActivateViewIconComponent = ActivateViewChildren.at(0)
    expect(ActivateViewIconComponent.is(SVGIconGenerator)).toBeTruthy()
    expect(ActivateViewIconComponent.props()).toEqual(VIEW_ICON_CONFIG)
    const ActivateFormComponent = MenuActivatorsChildren.at(1)
    expect(ActivateFormComponent.is('button')).toBeTruthy()
    expect(ActivateFormComponent.prop('className')).toBe('iconContainer')
    expect(ActivateFormComponent.prop('onClick')).toBe(component._activateItemMobileMenu)

    expect(Component).toMatchSnapshot()
  })

  it('should render with mobile and a custom class name state', () => {
    const Component = shallow<Item>(<Item {...mobileWithClassNameState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item testStyles')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(1)
    const MobileContainerComponent = ComponentChildren.at(0)
    expect(MobileContainerComponent.prop('className')).toBe('mobileContainer')
    const MobileContainerChildren = MobileContainerComponent.children()
    expect(MobileContainerChildren.length).toBe(1)
    const VisiblePanelComponent = MobileContainerChildren.at(0)
    expect(VisiblePanelComponent.prop('className')).toBe('visiblePanel')
    const VisiblePanelChildren = VisiblePanelComponent.children()
    expect(VisiblePanelChildren.length).toBe(4)
    // Draggable button
    const SortableHandleComponent = VisiblePanelChildren.at(0)
    expect(SortableHandleComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = SortableHandleComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = VisiblePanelChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = VisiblePanelChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bottom menus activators
    const MenuActivatorsComponent = VisiblePanelChildren.at(3)
    expect(MenuActivatorsComponent.prop('className')).toBe('menuActivators')
    const MenuActivatorsChildren = MenuActivatorsComponent.children()
    expect(MenuActivatorsChildren.length).toBe(2)
    const ActivateViewComponent = MenuActivatorsChildren.at(0)
    expect(ActivateViewComponent.is('button')).toBeTruthy()
    expect(ActivateViewComponent.prop('className')).toBe('iconContainer')
    expect(ActivateViewComponent.prop('onClick')).toBe(component._onActivateView)
    const ActivateViewChildren = ActivateViewComponent.children()
    expect(ActivateViewChildren.length).toBe(1)
    const ActivateViewIconComponent = ActivateViewChildren.at(0)
    expect(ActivateViewIconComponent.is(SVGIconGenerator)).toBeTruthy()
    expect(ActivateViewIconComponent.props()).toEqual(VIEW_ICON_CONFIG)
    const ActivateFormComponent = MenuActivatorsChildren.at(1)
    expect(ActivateFormComponent.is('button')).toBeTruthy()
    expect(ActivateFormComponent.prop('className')).toBe('iconContainer')
    expect(ActivateFormComponent.prop('onClick')).toBe(component._activateItemMobileMenu)

    expect(Component).toMatchSnapshot()
  })

  it('should render with mobile active state', () => {
    const Component = shallow<Item>(<Item {...mobileActiveState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item active')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(1)
    const MobileContainerComponent = ComponentChildren.at(0)
    expect(MobileContainerComponent.prop('className')).toBe('mobileContainer')
    const MobileContainerChildren = MobileContainerComponent.children()
    expect(MobileContainerChildren.length).toBe(1)
    const VisiblePanelComponent = MobileContainerChildren.at(0)
    expect(VisiblePanelComponent.prop('className')).toBe('visiblePanel')
    const VisiblePanelChildren = VisiblePanelComponent.children()
    expect(VisiblePanelChildren.length).toBe(4)
    // Draggable button
    const SortableHandleComponent = VisiblePanelChildren.at(0)
    expect(SortableHandleComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = SortableHandleComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = VisiblePanelChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = VisiblePanelChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bottom menus activators
    const MenuActivatorsComponent = VisiblePanelChildren.at(3)
    expect(MenuActivatorsComponent.prop('className')).toBe('menuActivators')
    const MenuActivatorsChildren = MenuActivatorsComponent.children()
    expect(MenuActivatorsChildren.length).toBe(2)
    const ActivateViewComponent = MenuActivatorsChildren.at(0)
    expect(ActivateViewComponent.is('button')).toBeTruthy()
    expect(ActivateViewComponent.prop('className')).toBe('iconContainer')
    expect(ActivateViewComponent.prop('onClick')).toBe(component._onActivateView)
    const ActivateViewChildren = ActivateViewComponent.children()
    expect(ActivateViewChildren.length).toBe(1)
    const ActivateViewIconComponent = ActivateViewChildren.at(0)
    expect(ActivateViewIconComponent.is(SVGIconGenerator)).toBeTruthy()
    expect(ActivateViewIconComponent.props()).toEqual(VIEW_ICON_CONFIG)
    const ActivateFormComponent = MenuActivatorsChildren.at(1)
    expect(ActivateFormComponent.is('button')).toBeTruthy()
    expect(ActivateFormComponent.prop('className')).toBe('iconContainer')
    expect(ActivateFormComponent.prop('onClick')).toBe(component._activateItemMobileMenu)

    expect(Component).toMatchSnapshot()
  })

  // TODO Changedstate

  it('should render with mobile menu active state', () => {
    const Component = shallow<Item>(<Item {...mobileMenuActiveState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item mobileMenuActive')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(1)
    const MobileContainerComponent = ComponentChildren.at(0)
    expect(MobileContainerComponent.prop('className')).toBe('mobileContainer')
    const MobileContainerChildren = MobileContainerComponent.children()
    expect(MobileContainerChildren.length).toBe(2)
    const VisiblePanelComponent = MobileContainerChildren.at(0)
    expect(VisiblePanelComponent.prop('className')).toBe('visiblePanel')
    const VisiblePanelChildren = VisiblePanelComponent.children()
    expect(VisiblePanelChildren.length).toBe(4)
    // Draggable button
    const SortableHandleComponent = VisiblePanelChildren.at(0)
    expect(SortableHandleComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = SortableHandleComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = VisiblePanelChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = VisiblePanelChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bottom menus activators
    const MenuActivatorsComponent = VisiblePanelChildren.at(3)
    expect(MenuActivatorsComponent.prop('className')).toBe('menuActivators')
    const MenuActivatorsChildren = MenuActivatorsComponent.children()
    expect(MenuActivatorsChildren.length).toBe(2)
    const ActivateViewComponent = MenuActivatorsChildren.at(0)
    expect(ActivateViewComponent.is('button')).toBeTruthy()
    expect(ActivateViewComponent.prop('className')).toBe('iconContainer')
    expect(ActivateViewComponent.prop('onClick')).toBe(component._onActivateView)
    const ActivateViewChildren = ActivateViewComponent.children()
    expect(ActivateViewChildren.length).toBe(1)
    const ActivateViewIconComponent = ActivateViewChildren.at(0)
    expect(ActivateViewIconComponent.is(SVGIconGenerator)).toBeTruthy()
    expect(ActivateViewIconComponent.props()).toEqual(VIEW_ICON_CONFIG)
    const ActivateFormComponent = MenuActivatorsChildren.at(1)
    expect(ActivateFormComponent.is('button')).toBeTruthy()
    expect(ActivateFormComponent.prop('className')).toBe('iconContainer')
    expect(ActivateFormComponent.prop('onClick')).toBe(component._activateItemMobileMenu)
    // Bottom mobile menu
    const BottomMobileMenuComponent = MobileContainerChildren.at(1)
    expect(BottomMobileMenuComponent.prop('className')).toBe('bottomMobileMenu')
    const BottomMobileMenuChildren = BottomMobileMenuComponent.children()
    expect(BottomMobileMenuChildren.length).toBe(3)
    // RRP
    const RrpMobileComponent = BottomMobileMenuChildren.at(0)
    expect(RrpMobileComponent.prop('className')).toBe('rrpMobile')
    const RrpMobileChildren = RrpMobileComponent.children()
    expect(RrpMobileChildren.length).toBe(2)
    const RrpMobileDecsComponent = RrpMobileChildren.at(0)
    expect(RrpMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(RrpMobileDecsComponent.text()).toBe('RRP:\u00a0')
    const RrpValueDecsComponent = RrpMobileChildren.at(1)
    expect(RrpValueDecsComponent.text()).toBe('$54,567,687')
    // Remove input
    const RemoveMobileComponent = BottomMobileMenuChildren.at(1)
    expect(RemoveMobileComponent.prop('className')).toBe('removeMobile')
    const RemoveMobileChildren = RemoveMobileComponent.children()
    expect(RemoveMobileChildren.length).toBe(2)
    const RemoveMobileDecsComponent = RemoveMobileChildren.at(0)
    expect(RemoveMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(RemoveMobileDecsComponent.text()).toBe('Remove:\u00a0')
    const RemoveInput = RemoveMobileChildren.at(1)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price input
    const PriceMobileComponent = BottomMobileMenuChildren.at(2)
    expect(PriceMobileComponent.prop('className')).toBe('priceMobile')
    const PriceMobileChildren = PriceMobileComponent.children()
    expect(PriceMobileChildren.length).toBe(2)
    const PriceMobileDecsComponent = PriceMobileChildren.at(0)
    expect(PriceMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(PriceMobileDecsComponent.text()).toBe('Price per unit:\u00a0')
    const MoneyInputComponent = PriceMobileChildren.at(1)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with mobile and remove amount state', () => {
    const Component = shallow<Item>(<Item {...mobileWithRemoveAmountState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item mobileMenuActive')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(1)
    const MobileContainerComponent = ComponentChildren.at(0)
    expect(MobileContainerComponent.prop('className')).toBe('mobileContainer')
    const MobileContainerChildren = MobileContainerComponent.children()
    expect(MobileContainerChildren.length).toBe(2)
    const VisiblePanelComponent = MobileContainerChildren.at(0)
    expect(VisiblePanelComponent.prop('className')).toBe('visiblePanel')
    const VisiblePanelChildren = VisiblePanelComponent.children()
    expect(VisiblePanelChildren.length).toBe(4)
    // Draggable button
    const SortableHandleComponent = VisiblePanelChildren.at(0)
    expect(SortableHandleComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = SortableHandleComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = VisiblePanelChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = VisiblePanelChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bottom menus activators
    const MenuActivatorsComponent = VisiblePanelChildren.at(3)
    expect(MenuActivatorsComponent.prop('className')).toBe('menuActivators')
    const MenuActivatorsChildren = MenuActivatorsComponent.children()
    expect(MenuActivatorsChildren.length).toBe(2)
    const ActivateViewComponent = MenuActivatorsChildren.at(0)
    expect(ActivateViewComponent.is('button')).toBeTruthy()
    expect(ActivateViewComponent.prop('className')).toBe('iconContainer')
    expect(ActivateViewComponent.prop('onClick')).toBe(component._onActivateView)
    const ActivateViewChildren = ActivateViewComponent.children()
    expect(ActivateViewChildren.length).toBe(1)
    const ActivateViewIconComponent = ActivateViewChildren.at(0)
    expect(ActivateViewIconComponent.is(SVGIconGenerator)).toBeTruthy()
    expect(ActivateViewIconComponent.props()).toEqual(VIEW_ICON_CONFIG)
    const ActivateFormComponent = MenuActivatorsChildren.at(1)
    expect(ActivateFormComponent.is('button')).toBeTruthy()
    expect(ActivateFormComponent.prop('className')).toBe('iconContainer')
    expect(ActivateFormComponent.prop('onClick')).toBe(component._activateItemMobileMenu)
    // Bottom mobile menu
    const BottomMobileMenuComponent = MobileContainerChildren.at(1)
    expect(BottomMobileMenuComponent.prop('className')).toBe('bottomMobileMenu')
    const BottomMobileMenuChildren = BottomMobileMenuComponent.children()
    expect(BottomMobileMenuChildren.length).toBe(3)
    // RRP
    const RrpMobileComponent = BottomMobileMenuChildren.at(0)
    expect(RrpMobileComponent.prop('className')).toBe('rrpMobile')
    const RrpMobileChildren = RrpMobileComponent.children()
    expect(RrpMobileChildren.length).toBe(2)
    const RrpMobileDecsComponent = RrpMobileChildren.at(0)
    expect(RrpMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(RrpMobileDecsComponent.text()).toBe('RRP:\u00a0')
    const RrpValueDecsComponent = RrpMobileChildren.at(1)
    expect(RrpValueDecsComponent.text()).toBe('$54,567,687')
    // Remove input
    const RemoveMobileComponent = BottomMobileMenuChildren.at(1)
    expect(RemoveMobileComponent.prop('className')).toBe('removeMobile')
    const RemoveMobileChildren = RemoveMobileComponent.children()
    expect(RemoveMobileChildren.length).toBe(2)
    const RemoveMobileDecsComponent = RemoveMobileChildren.at(0)
    expect(RemoveMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(RemoveMobileDecsComponent.text()).toBe('Remove:\u00a0')
    const RemoveInput = RemoveMobileChildren.at(1)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '5',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price input
    const PriceMobileComponent = BottomMobileMenuChildren.at(2)
    expect(PriceMobileComponent.prop('className')).toBe('priceMobile')
    const PriceMobileChildren = PriceMobileComponent.children()
    expect(PriceMobileChildren.length).toBe(2)
    const PriceMobileDecsComponent = PriceMobileChildren.at(0)
    expect(PriceMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(PriceMobileDecsComponent.text()).toBe('Price per unit:\u00a0')
    const MoneyInputComponent = PriceMobileChildren.at(1)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with mobile and price amount state', () => {
    const Component = shallow<Item>(<Item {...mobilePriceAmountState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item mobileMenuActive')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(1)
    const MobileContainerComponent = ComponentChildren.at(0)
    expect(MobileContainerComponent.prop('className')).toBe('mobileContainer')
    const MobileContainerChildren = MobileContainerComponent.children()
    expect(MobileContainerChildren.length).toBe(2)
    const VisiblePanelComponent = MobileContainerChildren.at(0)
    expect(VisiblePanelComponent.prop('className')).toBe('visiblePanel')
    const VisiblePanelChildren = VisiblePanelComponent.children()
    expect(VisiblePanelChildren.length).toBe(4)
    // Draggable button
    const SortableHandleComponent = VisiblePanelChildren.at(0)
    expect(SortableHandleComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = SortableHandleComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = VisiblePanelChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = VisiblePanelChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bottom menus activators
    const MenuActivatorsComponent = VisiblePanelChildren.at(3)
    expect(MenuActivatorsComponent.prop('className')).toBe('menuActivators')
    const MenuActivatorsChildren = MenuActivatorsComponent.children()
    expect(MenuActivatorsChildren.length).toBe(2)
    const ActivateViewComponent = MenuActivatorsChildren.at(0)
    expect(ActivateViewComponent.is('button')).toBeTruthy()
    expect(ActivateViewComponent.prop('className')).toBe('iconContainer')
    expect(ActivateViewComponent.prop('onClick')).toBe(component._onActivateView)
    const ActivateViewChildren = ActivateViewComponent.children()
    expect(ActivateViewChildren.length).toBe(1)
    const ActivateViewIconComponent = ActivateViewChildren.at(0)
    expect(ActivateViewIconComponent.is(SVGIconGenerator)).toBeTruthy()
    expect(ActivateViewIconComponent.props()).toEqual(VIEW_ICON_CONFIG)
    const ActivateFormComponent = MenuActivatorsChildren.at(1)
    expect(ActivateFormComponent.is('button')).toBeTruthy()
    expect(ActivateFormComponent.prop('className')).toBe('iconContainer')
    expect(ActivateFormComponent.prop('onClick')).toBe(component._activateItemMobileMenu)
    // Bottom mobile menu
    const BottomMobileMenuComponent = MobileContainerChildren.at(1)
    expect(BottomMobileMenuComponent.prop('className')).toBe('bottomMobileMenu')
    const BottomMobileMenuChildren = BottomMobileMenuComponent.children()
    expect(BottomMobileMenuChildren.length).toBe(3)
    // RRP
    const RrpMobileComponent = BottomMobileMenuChildren.at(0)
    expect(RrpMobileComponent.prop('className')).toBe('rrpMobile')
    const RrpMobileChildren = RrpMobileComponent.children()
    expect(RrpMobileChildren.length).toBe(2)
    const RrpMobileDecsComponent = RrpMobileChildren.at(0)
    expect(RrpMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(RrpMobileDecsComponent.text()).toBe('RRP:\u00a0')
    const RrpValueDecsComponent = RrpMobileChildren.at(1)
    expect(RrpValueDecsComponent.text()).toBe('$54,567,687')
    // Remove input
    const RemoveMobileComponent = BottomMobileMenuChildren.at(1)
    expect(RemoveMobileComponent.prop('className')).toBe('removeMobile')
    const RemoveMobileChildren = RemoveMobileComponent.children()
    expect(RemoveMobileChildren.length).toBe(2)
    const RemoveMobileDecsComponent = RemoveMobileChildren.at(0)
    expect(RemoveMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(RemoveMobileDecsComponent.text()).toBe('Remove:\u00a0')
    const RemoveInput = RemoveMobileChildren.at(1)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price input
    const PriceMobileComponent = BottomMobileMenuChildren.at(2)
    expect(PriceMobileComponent.prop('className')).toBe('priceMobile')
    const PriceMobileChildren = PriceMobileComponent.children()
    expect(PriceMobileChildren.length).toBe(2)
    const PriceMobileDecsComponent = PriceMobileChildren.at(0)
    expect(PriceMobileDecsComponent.prop('className')).toBe('valueDesc')
    expect(PriceMobileDecsComponent.text()).toBe('Price per unit:\u00a0')
    const MoneyInputComponent = PriceMobileChildren.at(1)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '789,504',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })
})
