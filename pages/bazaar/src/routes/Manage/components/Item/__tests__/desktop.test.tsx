import React from 'react'
import { shallow } from 'enzyme'
import { Item } from '..'
import {
  initialState,
  withClassNameState,
  withBonusesState,
  activeState,
  withRemoveAmountState,
  priceAmountState,
  changedState,
  withExtraBonusesListState,
  withWeaponBonusesState,
  withArmourBonusesState,
  withArmourExtraBonusesListState,
  withWeaponExtraBonusesListState
} from './mocks'
import { NumberInput } from '../../../../../components'
import { SortableHandle } from '../hocs'
import MoneyInput from '@torn/shared/components/MoneyInput'

describe('<Item />', () => {
  it('should render', () => {
    const Component = shallow<Item>(<Item {...initialState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    // Drag button
    const SortableHandleComponent = ComponentChildren.at(0)
    expect(SortableHandleComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = SortableHandleComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with class name', () => {
    const Component = shallow<Item>(<Item {...withClassNameState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item testStyles')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with bonuses', () => {
    const Component = shallow<Item>(<Item {...withBonusesState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render weapon with bonuses', () => {
    const Component = shallow<Item>(<Item {...withWeaponBonusesState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(3)
    // Demage bonus
    const DmgBonusComponent = BonusesChildren.at(0)
    expect(DmgBonusComponent.prop('className')).toBe('bonus noBorder')
    const DmgBonusChildren = DmgBonusComponent.children()
    expect(DmgBonusChildren.length).toBe(2)
    const DmgextraBonusIconComponent = DmgBonusChildren.at(0)
    expect(DmgextraBonusIconComponent.is('i')).toBeTruthy()
    expect(DmgextraBonusIconComponent.prop('className')).toBe('damageBonusIcon')
    expect(DmgextraBonusIconComponent.children().length).toBe(0)
    const DmgBonusValueComponent = DmgBonusChildren.at(1)
    expect(DmgBonusValueComponent.is('span')).toBeTruthy()
    expect(DmgBonusValueComponent.prop('className')).toBe('bonusValue')
    expect(DmgBonusValueComponent.text()).toBe('644')
    // Accuracy bonus
    const AccuracyBonusComponent = BonusesChildren.at(1)
    expect(AccuracyBonusComponent.prop('className')).toBe('bonus')
    const AccuracyBonusChildren = AccuracyBonusComponent.children()
    expect(AccuracyBonusChildren.length).toBe(2)
    const accuracyBonusIconComponent = AccuracyBonusChildren.at(0)
    expect(accuracyBonusIconComponent.is('i')).toBeTruthy()
    expect(accuracyBonusIconComponent.prop('className')).toBe('accuracyBonusIcon')
    expect(accuracyBonusIconComponent.children().length).toBe(0)
    const AccuracyBonusValueComponent = AccuracyBonusChildren.at(1)
    expect(AccuracyBonusValueComponent.is('span')).toBeTruthy()
    expect(AccuracyBonusValueComponent.prop('className')).toBe('bonusValue')
    expect(AccuracyBonusValueComponent.text()).toBe('8832')
    // Blank bonus
    const BlankBonusComponent = BonusesChildren.at(2)
    expect(BlankBonusComponent.prop('className')).toBe('bonus')
    const BlankBonusChildren = BlankBonusComponent.children()
    expect(BlankBonusChildren.length).toBe(2)
    const BlankextraBonusIconComponent = BlankBonusChildren.at(0)
    expect(BlankextraBonusIconComponent.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent.prop('className')).toBe('extraBonusIcon bonus-attachment-blank-bonus-25')
    expect(BlankextraBonusIconComponent.children().length).toBe(0)
    const BlankextraBonusIconComponent2 = BlankBonusChildren.at(1)
    expect(BlankextraBonusIconComponent2.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent2.prop('className')).toBe('extraBonusIcon bonus-attachment-blank-bonus-25')
    expect(BlankextraBonusIconComponent2.children().length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render armor with bonuses', () => {
    const Component = shallow<Item>(<Item {...withArmourBonusesState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(2)
    // Armor bonus
    const ArmorComponent = BonusesChildren.at(0)
    expect(ArmorComponent.prop('className')).toBe('bonus')
    const ArmorChildren = ArmorComponent.children()
    expect(ArmorChildren.length).toBe(2)
    const ArmorIconComponent = ArmorChildren.at(0)
    expect(ArmorIconComponent.is('i')).toBeTruthy()
    expect(ArmorIconComponent.prop('className')).toBe('defenceBonusIcon')
    expect(ArmorIconComponent.children().length).toBe(0)
    const ArmorValueComponent = ArmorChildren.at(1)
    expect(ArmorValueComponent.is('span')).toBeTruthy()
    expect(ArmorValueComponent.prop('className')).toBe('bonusValue')
    expect(ArmorValueComponent.text()).toBe('758')
    // Blank bonus
    const BlankBonusComponent = BonusesChildren.at(1)
    expect(BlankBonusComponent.prop('className')).toBe('bonus')
    const BlankBonusChildren = BlankBonusComponent.children()
    expect(BlankBonusChildren.length).toBe(2)
    const BlankextraBonusIconComponent = BlankBonusChildren.at(0)
    expect(BlankextraBonusIconComponent.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent.prop('className')).toBe('extraBonusIcon bonus-attachment-blank-bonus-25')
    expect(BlankextraBonusIconComponent.children().length).toBe(0)
    const BlankextraBonusIconComponent2 = BlankBonusChildren.at(1)
    expect(BlankextraBonusIconComponent2.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent2.prop('className')).toBe('extraBonusIcon bonus-attachment-blank-bonus-25')
    expect(BlankextraBonusIconComponent2.children().length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with extra bonuses list', () => {
    const Component = shallow<Item>(<Item {...withExtraBonusesListState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render weapon with extra bonuses list', () => {
    const Component = shallow<Item>(<Item {...withWeaponExtraBonusesListState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(3)
    // Demage bonus
    const DmgBonusComponent = BonusesChildren.at(0)
    expect(DmgBonusComponent.prop('className')).toBe('bonus noBorder')
    const DmgBonusChildren = DmgBonusComponent.children()
    expect(DmgBonusChildren.length).toBe(2)
    const DmgextraBonusIconComponent = DmgBonusChildren.at(0)
    expect(DmgextraBonusIconComponent.is('i')).toBeTruthy()
    expect(DmgextraBonusIconComponent.prop('className')).toBe('damageBonusIcon')
    expect(DmgextraBonusIconComponent.children().length).toBe(0)
    const DmgBonusValueComponent = DmgBonusChildren.at(1)
    expect(DmgBonusValueComponent.is('span')).toBeTruthy()
    expect(DmgBonusValueComponent.prop('className')).toBe('bonusValue')
    expect(DmgBonusValueComponent.text()).toBe('644')
    // Accuracy bonus
    const AccuracyBonusComponent = BonusesChildren.at(1)
    expect(AccuracyBonusComponent.prop('className')).toBe('bonus')
    const AccuracyBonusChildren = AccuracyBonusComponent.children()
    expect(AccuracyBonusChildren.length).toBe(2)
    const accuracyBonusIconComponent = AccuracyBonusChildren.at(0)
    expect(accuracyBonusIconComponent.is('i')).toBeTruthy()
    expect(accuracyBonusIconComponent.prop('className')).toBe('accuracyBonusIcon')
    expect(accuracyBonusIconComponent.children().length).toBe(0)
    const AccuracyBonusValueComponent = AccuracyBonusChildren.at(1)
    expect(AccuracyBonusValueComponent.is('span')).toBeTruthy()
    expect(AccuracyBonusValueComponent.prop('className')).toBe('bonusValue')
    expect(AccuracyBonusValueComponent.text()).toBe('8832')
    // Blank bonus
    const BlankBonusComponent = BonusesChildren.at(2)
    expect(BlankBonusComponent.prop('className')).toBe('bonus')
    const BlankBonusChildren = BlankBonusComponent.children()
    expect(BlankBonusChildren.length).toBe(2)
    const BlankextraBonusIconComponent = BlankBonusChildren.at(0)
    expect(BlankextraBonusIconComponent.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent.prop('className')).toBe('extraBonusIcon bonus-attachment-blank-bonus-25')
    expect(BlankextraBonusIconComponent.children().length).toBe(0)
    const BlankextraBonusIconComponent2 = BlankBonusChildren.at(1)
    expect(BlankextraBonusIconComponent2.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent2.prop('className')).toBe('extraBonusIcon bonus-attachment-emasculated')
    expect(BlankextraBonusIconComponent2.children().length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render armor with extra bonuses list', () => {
    const Component = shallow<Item>(<Item {...withArmourExtraBonusesListState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(2)
    // Armor bonus
    const ArmorComponent = BonusesChildren.at(0)
    expect(ArmorComponent.prop('className')).toBe('bonus')
    const ArmorChildren = ArmorComponent.children()
    expect(ArmorChildren.length).toBe(2)
    const ArmorIconComponent = ArmorChildren.at(0)
    expect(ArmorIconComponent.is('i')).toBeTruthy()
    expect(ArmorIconComponent.prop('className')).toBe('defenceBonusIcon')
    expect(ArmorIconComponent.children().length).toBe(0)
    const ArmorValueComponent = ArmorChildren.at(1)
    expect(ArmorValueComponent.is('span')).toBeTruthy()
    expect(ArmorValueComponent.prop('className')).toBe('bonusValue')
    expect(ArmorValueComponent.text()).toBe('758')
    // Blank bonus
    const BlankBonusComponent = BonusesChildren.at(1)
    expect(BlankBonusComponent.prop('className')).toBe('bonus')
    const BlankBonusChildren = BlankBonusComponent.children()
    expect(BlankBonusChildren.length).toBe(2)
    const BlankextraBonusIconComponent = BlankBonusChildren.at(0)
    expect(BlankextraBonusIconComponent.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent.prop('className')).toBe('extraBonusIcon bonus-attachment-blank-bonus-25')
    expect(BlankextraBonusIconComponent.children().length).toBe(0)
    const BlankextraBonusIconComponent2 = BlankBonusChildren.at(1)
    expect(BlankextraBonusIconComponent2.is('i')).toBeTruthy()
    expect(BlankextraBonusIconComponent2.prop('className')).toBe('extraBonusIcon bonus-attachment-emasculated')
    expect(BlankextraBonusIconComponent2.children().length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with active state', () => {
    const Component = shallow<Item>(<Item {...activeState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item active')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with remove amount', () => {
    const Component = shallow<Item>(<Item {...withRemoveAmountState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '5',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with updated price', () => {
    const Component = shallow<Item>(<Item {...priceAmountState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '789,504',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render with changed state', () => {
    const Component = shallow<Item>(<Item {...changedState} />)
    const component = Component.instance()

    expect(Component.is('div')).toBeTruthy()
    expect(Component.prop('className')).toBe('item changed')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(7)
    const DraggableHandlerComponent = ComponentChildren.at(0)
    expect(DraggableHandlerComponent.is(SortableHandle)).toBeTruthy()
    const DraggableButtonComponent = DraggableHandlerComponent.children().at(0)
    expect(DraggableButtonComponent.prop('className')).toBe('draggableIconContainer')
    const DraggableButtonChildren = DraggableButtonComponent.children()
    expect(DraggableButtonChildren.length).toBe(1)
    const DraggableIconComponent = DraggableButtonChildren.at(0)
    expect(DraggableIconComponent.is('i')).toBeTruthy()
    expect(DraggableIconComponent.prop('className')).toBe('draggableIcon')
    expect(DraggableButtonChildren.children().length).toBe(0)
    // Image
    const ImageContainerComponent = ComponentChildren.at(1)
    expect(ImageContainerComponent.prop('className')).toBe('imgContainer')
    expect(ImageContainerComponent.prop('onClick')).toBe(component._onActivateView)
    // TODO: Should check ref
    const ImageContainerChildren = ImageContainerComponent.children()
    expect(ImageContainerChildren.length).toBe(1)
    const ImageComponent = ImageContainerChildren.at(0)
    expect(ImageComponent.is('img')).toBeTruthy()
    expect(ImageComponent.props()).toEqual({
      src: '/images/items/68/medium.png',
      alt: 'Lead Pipe',
      className: ''
    })
    // Description
    const DescComponent = ComponentChildren.at(2)
    expect(DescComponent.prop('className')).toBe('desc')
    expect(DescComponent.prop('onClick')).toBe(component._onActivateView)
    expect(DescComponent.prop('role')).toBe('heading')
    expect(DescComponent.prop('aria-level')).toBe(6)
    const DescChildren = DescComponent.children()
    expect(DescChildren.length).toBe(1)
    const DescTextComponent = DescChildren.at(0)
    expect(DescTextComponent.is('span')).toBeTruthy()
    expect(DescTextComponent.text()).toBe('Lead Pipe x8000')
    // Bonuses
    const BonusesComponent = ComponentChildren.at(3)
    expect(BonusesComponent.prop('className')).toBe('bonuses')
    const BonusesChildren = BonusesComponent.children()
    expect(BonusesChildren.length).toBe(0)
    // Rrp
    const RrpComponent = ComponentChildren.at(4)
    expect(RrpComponent.prop('className')).toBe('rrp')
    expect(RrpComponent.text()).toBe('$54,567,687')
    // Remove
    const RemoveComponent = ComponentChildren.at(5)
    expect(RemoveComponent.prop('className')).toBe('remove')
    const RemoveChildren = RemoveComponent.children()
    expect(RemoveChildren.length).toBe(1)
    const RemoveInput = RemoveChildren.at(0)
    expect(RemoveInput.is(NumberInput)).toBeTruthy()
    expect(RemoveInput.props()).toEqual({
      className: 'removeAmountInput',
      value: '0',
      min: 0,
      max: 8000,
      onChange: component._onRemoveAmoutChange,
      onBlur: component._onBlur,
      ariaLabel: 'Remove: Lead Pipe'
    })
    // Price
    const PriceComponent = ComponentChildren.at(6)
    expect(PriceComponent.prop('className')).toBe('price')
    const PriceChildren = PriceComponent.children()
    expect(PriceChildren.length).toBe(1)
    const MoneyInputComponent = PriceChildren.at(0)
    expect(MoneyInputComponent.is(MoneyInput)).toBeTruthy()
    // expect(MoneyInputComponent.props()).toEqual({
    //   className: 'priceInput',
    //   value: '20,000,000,000',
    //   min: 0,
    //   onChange: component._onPriceChange,
    //   onBlur: component._onBlur,
    //   getAriaLabel: component._getAriaLabel
    // })

    expect(Component).toMatchSnapshot()
  })

  it('should render tooltip', () => {
    const Component = shallow<Item>(<Item {...changedState} />)
    const component = Component.instance()

    const TooltipComponent = shallow(component._renderTooltip('<button>test title</button>'))

    expect(TooltipComponent.is('span')).toBeTruthy()
    expect(TooltipComponent.html()).toBe('<span><button>test title</button></span>')

    expect(Component).toMatchSnapshot()
  })
})
