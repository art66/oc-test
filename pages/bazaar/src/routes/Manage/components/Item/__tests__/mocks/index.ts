import { IProps } from '../../types'

export const initialState: IProps = {
  index: 0,
  uniqueId: '373-1',
  itemId: '1',
  bazaarId: '7853458',
  armoryId: '46785',
  name: 'Lead Pipe',
  price: 20000000000,
  priceString: '20,000,000,000',
  amount: 8000,
  imgLargeUrl: '/images/items/68/large.png?v=1528808940574',
  imgMediumUrl: '/images/items/68/medium.png',
  imgBlankUrl: '/images/items/68/medium.png',
  rarity: 0,
  isBlockedForBuying: false,
  glow: null,
  categoryName: 'weapons',
  averagePrice: 54567687,
  damageBonus: '0',
  accuracyBonus: '0',
  armorBonus: '0',
  isMobile: false,
  type: 'Other',
  active: false,
  removeAmount: 0,
  changed: false,
  minPrice: 8000 * Math.ceil(54567687 * 0.25),
  bonuses: [
    { className: 'bonus-attachment-blank-bonus-25', title: null },
    { className: 'bonus-attachment-blank-bonus-25', title: null }
  ],
  setActiveViewUniqueId() {},
  activateItemMobileMenu() {},
  onItemRemove() {},
  onPriceChange() {}
}

export const withClassNameState: IProps = {
  ...initialState,
  className: 'testStyles'
}

export const withBonusesState: IProps = {
  ...initialState,
  damageBonus: '644',
  accuracyBonus: '8832',
  armorBonus: '758'
}

export const withWeaponBonusesState: IProps = {
  ...withBonusesState,
  type: 'Weapon'
}

export const withArmourBonusesState: IProps = {
  ...withBonusesState,
  type: 'Armour'
}

export const withExtraBonusesListState: IProps = {
  ...withBonusesState,
  bonuses: [
    withBonusesState.bonuses[0],
    {
      className: 'bonus-attachment-emasculated',
      title: '<b>Emasculate</b><br/>Happiness increased by 15% of maximum upon final hit'
    }
  ]
}

export const withWeaponExtraBonusesListState: IProps = {
  ...withWeaponBonusesState,
  bonuses: [
    withWeaponBonusesState.bonuses[0],
    {
      className: 'bonus-attachment-emasculated',
      title: '<b>Emasculate</b><br/>Happiness increased by 15% of maximum upon final hit'
    }
  ]
}

export const withArmourExtraBonusesListState: IProps = {
  ...withArmourBonusesState,
  bonuses: [
    withArmourBonusesState.bonuses[0],
    {
      className: 'bonus-attachment-emasculated',
      title: '<b>Emasculate</b><br/>Happiness increased by 15% of maximum upon final hit'
    }
  ]
}

export const activeState: IProps = {
  ...initialState,
  active: true
}

export const withRemoveAmountState: IProps = {
  ...initialState,
  removeAmount: 5
}

export const priceAmountState: IProps = {
  ...initialState,
  priceString: '789,504'
}
export const changedState: IProps = {
  ...initialState,
  changed: true
}

export const mobileState: IProps = {
  ...initialState,
  isMobile: true
}

export const mobileWithClassNameState: IProps = {
  ...mobileState,
  className: 'testStyles'
}

export const mobileActiveState: IProps = {
  ...mobileState,
  active: true
}

export const mobileChangedState: IProps = {
  ...mobileState,
  changed: true
}

export const mobileMenuActiveState: IProps = {
  ...mobileState,
  mobileMenuActive: true
}

export const mobileWithRemoveAmountState: IProps = {
  ...mobileMenuActiveState,
  removeAmount: 5
}

export const mobilePriceAmountState: IProps = {
  ...mobileMenuActiveState,
  priceString: '789,504'
}
