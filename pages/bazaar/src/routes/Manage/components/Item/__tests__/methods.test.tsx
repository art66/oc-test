import React from 'react'
import { Item } from '..'
import { initialState } from './mocks'
import { shallow } from 'enzyme'

describe('<Item />', () => {
  it('should handle remove amout change', () => {
    const onItemRemove = jest.fn()
    const Component = shallow<Item>(<Item {...{ ...initialState, onItemRemove }} />)
    const component = Component.instance()

    component._onRemoveAmoutChange(7675)

    expect(onItemRemove).toHaveBeenCalledTimes(1)
    expect(onItemRemove.mock.calls[0]).toEqual([0, 7675])

    expect(onItemRemove).toMatchSnapshot()
  })

  it('should handle price change', () => {
    const onPriceChange = jest.fn()
    const Component = shallow<Item>(<Item {...{ ...initialState, onPriceChange }} />)
    const component = Component.instance()

    component._onPriceChange('7,689,678,745')

    expect(onPriceChange).toHaveBeenCalledTimes(1)
    expect(onPriceChange.mock.calls[0]).toEqual([0, 7689678745, '7,689,678,745'])

    expect(onPriceChange).toMatchSnapshot()
  })

  it('should handle view activation', () => {
    const setActiveViewUniqueId = jest.fn()
    const Component = shallow<Item>(<Item {...{ ...initialState, setActiveViewUniqueId }} />)
    const component = Component.instance()

    component._onActivateView()

    expect(setActiveViewUniqueId).toHaveBeenCalledTimes(1)
    expect(setActiveViewUniqueId.mock.calls[0]).toEqual(['373-1'])

    expect(setActiveViewUniqueId).toMatchSnapshot()
  })

  it('should handle on blur', () => {
    const Component = shallow<Item>(<Item {...initialState} />)
    const component = Component.instance()

    component._onBlur()

    expect(Component.state('itemChanged')).toBeFalsy()

    Component.setState({ itemChanged: true })

    component._onBlur()

    expect(Component.state('itemChanged')).toBeFalsy()

    Component.setState({ itemChanged: true })
    Component.setProps({ changed: true })

    component._onBlur()

    expect(Component.state('itemChanged')).toBeTruthy()

    Component.setState({ itemChanged: false })
    Component.setProps({ changed: true })

    component._onBlur()

    expect(Component.state('itemChanged')).toBeTruthy()
  })

  it('should handle mobile menu activation', () => {
    const activateItemMobileMenu = jest.fn()
    const Component = shallow<Item>(<Item {...{ ...initialState, activateItemMobileMenu }} />)
    const component = Component.instance()

    component._activateItemMobileMenu()

    expect(activateItemMobileMenu).toHaveBeenCalledTimes(1)
    expect(activateItemMobileMenu.mock.calls[0]).toEqual(['373-1'])

    Component.setProps({ mobileMenuActive: true })

    component._activateItemMobileMenu()

    expect(activateItemMobileMenu).toHaveBeenCalledTimes(2)
    expect(activateItemMobileMenu.mock.calls[1]).toEqual([null])

    expect(activateItemMobileMenu).toMatchSnapshot()
  })
})
