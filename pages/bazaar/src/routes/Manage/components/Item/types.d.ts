import { ReactFragment } from 'react'
import { TItem, TBonus } from '../../../../modules/types/state'

export interface IProps extends TItem {
  className?: string
  isMobile: boolean
  mobileMenuActive?: boolean
  active: boolean
  index: number
  removeAmount: number
  price: number
  priceString: string
  changed: boolean
  priceIsInvalid?: boolean
  activateItemMobileMenu: (uniqueId: string) => any
  onItemRemove: (index: number, amount: number) => any
  onPriceChange: (index: number, price: number, priceString: string) => void
  setActiveViewUniqueId: (uniqueId: string) => any
}

export type TState = {
  itemChanged: boolean
}

export type TOnRemoveAmoutChange = (value: number) => any

export type TRenderBonuses = () => ReactFragment | null

export type TOnBlur = () => void

export type TOnPriceBlur = () => any

export type TOnActivateView = () => any

export type TRenderImage = () => JSX.Element

export type TActivateItemMobileMenu = () => void

export type TRenderDraggableButton = () => JSX.Element

export type TRenderbottomMobileMenu = () => JSX.Element

export type TRenderDescription = () => JSX.Element

export type TRenderBonusesCell = () => JSX.Element

export type TRenderRrp = () => JSX.Element

export type TRenderRemove = () => JSX.Element

export type TRenderPrice = () => JSX.Element

export type TRenderMobileMenuActivators = () => JSX.Element

export type TRenderContent = () => ReactFragment

export type TRenderPriceInput = () => JSX.Element

export type TRenderRemoveInput = () => JSX.Element

export type TGetRrpValue = () => string

export type TGetTooltipId = (className: string, index: number) => null | string

export type TIsBonusHasTooltip = (className: string) => boolean

export type TRenderExtraBonus = (index: number, bonus: TBonus) => JSX.Element

export type TRenderExtraBonuses = () => JSX.Element

export type TAddBonuesTooltip = () => void

export type TRemoveBonuesTooltip = () => void

export type TRenderDamageBonus = () => JSX.Element

export type TRenderAccuracyBonus = () => JSX.Element

export type TRenderArmorBonus = () => JSX.Element

export type TRenderSimpleBonuses = () => JSX.Element

export type TShowBonusesSelector = (type: string) => boolean

export type TShowBonuses = () => boolean

export type TRenderTooltip = (htmlString: string) => JSX.Element
