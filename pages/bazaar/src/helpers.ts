export class LogError extends Error {
  log: string[]

  constructor(message: string, log: string[]) {
    super(message)

    this.log = log
  }
}
