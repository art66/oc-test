import { IProps } from '../../types'

export const initialState: IProps = {
  value: '5',
  ariaLabel: 'AK74 Gold',
  onChange: () => {}
}

export const classNameState: IProps = {
  ...initialState,
  className: 'testClassName'
}

export const minlimitsState: IProps = {
  ...initialState,
  min: 5
}

export const maxLimitsState: IProps = {
  ...initialState,
  max: 10
}

export const limitsState: IProps = {
  ...initialState,
  ...minlimitsState,
  ...maxLimitsState
}
