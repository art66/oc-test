import React from 'react'
import { shallow } from 'enzyme'

import NumberInput from '../'
import { delay } from '../../../utils'

import { initialState, classNameState, minlimitsState, maxLimitsState, limitsState } from './mocks'

describe('<NumberInput />', () => {
  it('should render', () => {
    const Component = shallow(<NumberInput {...initialState} />)

    expect(Component.length).toBe(1)
    expect(Component.prop('type')).toBe('text')
    expect(Component.prop('className')).toBe('numberInput')
    expect(Component.prop('value')).toBe('5')
    expect(Component.prop('aria-label')).toBe('AK74 Gold')

    expect(Component).toMatchSnapshot()
  })

  it('should render with extra class name', () => {
    const Component = shallow(<NumberInput {...classNameState} />)

    expect(Component.length).toBe(1)
    expect(Component.prop('type')).toBe('text')
    expect(Component.prop('className')).toBe('numberInput testClassName')
    expect(Component.prop('value')).toBe('5')
    expect(Component.prop('aria-label')).toBe('AK74 Gold')

    expect(Component).toMatchSnapshot()
  })

  it('should render witn min limit', () => {
    const Component = shallow(<NumberInput {...minlimitsState} />)

    expect(Component.length).toBe(1)
    expect(Component.prop('type')).toBe('text')
    expect(Component.prop('className')).toBe('numberInput')
    expect(Component.prop('value')).toBe('5')
    expect(Component.prop('aria-label')).toBe('AK74 Gold')

    expect(Component).toMatchSnapshot()
  })

  it('should render witn max limit', () => {
    const Component = shallow(<NumberInput {...maxLimitsState} max={50} />)

    expect(Component.length).toBe(1)
    expect(Component.prop('type')).toBe('text')
    expect(Component.prop('className')).toBe('numberInput')
    expect(Component.prop('value')).toBe('5')
    expect(Component.prop('aria-label')).toBe('AK74 Gold')

    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener', () => {
    const onChange = jest.fn()
    const value = '105'
    const event = { target: { value } }
    const state = { ...initialState, onChange }
    const Component = shallow(<NumberInput {...state} />)

    expect(Component.length).toBe(1)

    Component.simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][0]).toBe(105)
    expect(onChange.mock.calls[0][1]).toBe('105')
    expect(onChange.mock.calls[0][2]).toEqual(event)

    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener with min limit amd value less than min', async () => {
    const onChange = jest.fn()
    const value = '0'
    const event = { target: { value } }
    const state = { ...minlimitsState, onChange }
    const Component = shallow(<NumberInput {...state} />)

    expect(Component.length).toBe(1)

    Component.simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][0]).toBe(0)
    expect(onChange.mock.calls[0][1]).toBe('0')
    expect(onChange.mock.calls[0][2]).toEqual(event)

    await delay(250)

    expect(onChange).toHaveBeenCalledTimes(2)

    expect(onChange.mock.calls[1][0]).toBe(5)
    expect(onChange.mock.calls[1][1]).toBe('5')
    expect(onChange.mock.calls[1][2]).toEqual(event)

    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener with max limit and value less than max', () => {
    const onChange = jest.fn()
    const value = '0'
    const event = { target: { value } }
    const state = { ...maxLimitsState, onChange }
    const Component = shallow(<NumberInput {...state} />)

    expect(Component.length).toBe(1)

    Component.simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][0]).toBe(0)
    expect(onChange.mock.calls[0][1]).toBe('0')
    expect(onChange.mock.calls[0][2]).toEqual(event)

    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener with max limit and value more than max', async () => {
    const onChange = jest.fn()
    const value = '105'
    const event = { target: { value } }
    const state = { ...maxLimitsState, onChange }
    const Component = shallow(<NumberInput {...state} />)

    expect(Component.length).toBe(1)

    Component.simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][0]).toBe(105)
    expect(onChange.mock.calls[0][1]).toBe('105')
    expect(onChange.mock.calls[0][2]).toEqual(event)

    await delay(250)

    expect(onChange).toHaveBeenCalledTimes(2)

    expect(onChange.mock.calls[1][0]).toBe(10)
    expect(onChange.mock.calls[1][1]).toBe('10')
    expect(onChange.mock.calls[1][2]).toEqual(event)

    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener with limits and value less than min limit', async () => {
    const onChange = jest.fn()
    const value = '0'
    const event = { target: { value } }
    const state = { ...limitsState, onChange }
    const Component = shallow(<NumberInput {...state} />)

    expect(Component.length).toBe(1)

    Component.simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][0]).toBe(0)
    expect(onChange.mock.calls[0][1]).toBe('0')
    expect(onChange.mock.calls[0][2]).toEqual(event)

    await delay(250)

    expect(onChange).toHaveBeenCalledTimes(2)

    expect(onChange.mock.calls[1][0]).toBe(5)
    expect(onChange.mock.calls[1][1]).toBe('5')
    expect(onChange.mock.calls[1][2]).toEqual(event)

    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener with limits and value more than max limit', async () => {
    const onChange = jest.fn()
    const value = '105'
    const event = { target: { value } }
    const state = { ...limitsState, onChange }
    const Component = shallow(<NumberInput {...state} />)

    expect(Component.length).toBe(1)

    Component.simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][0]).toBe(105)
    expect(onChange.mock.calls[0][1]).toBe('105')
    expect(onChange.mock.calls[0][2]).toEqual(event)

    await delay(250)

    expect(onChange).toHaveBeenCalledTimes(2)

    expect(onChange.mock.calls[1][0]).toBe(10)
    expect(onChange.mock.calls[1][1]).toBe('10')
    expect(onChange.mock.calls[1][2]).toEqual(event)

    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener without value', async () => {
    const onChange = jest.fn()
    const value = undefined
    const event = { target: { value } }
    const state = { ...limitsState, onChange }
    const Component = shallow(<NumberInput {...state} />)

    expect(Component.length).toBe(1)

    Component.simulate('change', event)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange.mock.calls[0][0]).toBe(null)
    expect(onChange.mock.calls[0][1]).toEqual('')
    expect(onChange.mock.calls[0][2]).toEqual(event)

    await delay(250)

    expect(onChange).toHaveBeenCalledTimes(1)

    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
