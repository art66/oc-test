export interface IProps {
  className?: string
  value: string
  min?: number
  max?: number
  ariaLabel?: string
  disabled?: boolean
  onChange?: (value: number, valueString: string, event?: React.ChangeEvent<HTMLInputElement>) => any
  onBlur?: () => any
  onFocus?: () => any
  getAriaLabel?: (price: string) => string
}
