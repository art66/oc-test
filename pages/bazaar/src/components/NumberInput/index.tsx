import React, { PureComponent } from 'react'
import cn from 'classnames'
import { IProps } from './types'
import styles from './index.cssmodule.scss'
import createMoneyFormatter from '../../utils/formatMoney'

const moneyFormatter = createMoneyFormatter()

export default class NumberInput extends PureComponent<IProps> {
  _fixeValueTimeoutId = null

  static defaultProps = {
    value: '',
    min: 0
  }

  _getAriaLabel = (): string => {
    const { value, getAriaLabel, ariaLabel } = this.props

    if (getAriaLabel) {
      return getAriaLabel(value)
    }

    return ariaLabel
  }

  _onChangeHandler = event => {
    const { min, max, onChange } = this.props

    const pureValue = event.target.value || ''

    const [intValue, inputValue] = moneyFormatter(pureValue)

    onChange(intValue, inputValue, event)
    // FIXME: It should update the value with delay.

    clearTimeout(this._fixeValueTimeoutId)

    if (typeof min === 'number' && typeof intValue === 'number' && intValue < min) {
      this._fixeValueTimeoutId = setTimeout(() => {
        onChange(min, min.toString(), event)
      }, 150)
    } else if (typeof max === 'number' && typeof intValue === 'number' && intValue > max) {
      this._fixeValueTimeoutId = setTimeout(() => {
        onChange(max, max.toString(), event)
      }, 150)
    }
  }

  render() {
    const { className, value, disabled, onBlur, onFocus } = this.props

    return (
      <input
        type='text'
        className={cn(styles.numberInput, className)}
        onChange={this._onChangeHandler}
        onBlur={onBlur}
        onFocus={onFocus}
        value={value}
        aria-label={this._getAriaLabel()}
        data-lpignore='true'
        disabled={disabled}
      />
    )
  }
}
