import React from 'react'
import styles from './index.cssmodule.scss'

const ListMessage: React.SFC = (props): JSX.Element => {
  return <div className={styles.listMessage}>{props.children}</div>
}

export default ListMessage
