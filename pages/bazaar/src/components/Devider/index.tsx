import React from 'react'

import styles from './index.cssmodule.scss'

const Devider: React.SFC = (): JSX.Element => {
  return <hr className={styles.devider} />
}

export default Devider
