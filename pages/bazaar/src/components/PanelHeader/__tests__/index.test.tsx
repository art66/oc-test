import React from 'react'
import { mount } from 'enzyme'

import PanelHeader from '..'

describe('<PanelHeader />', () => {
  it('should render with title', () => {
    const title = 'Test title'
    const Component = mount(<PanelHeader>{title}</PanelHeader>)

    expect(Component.find('.panelHeader').length).toBe(1)
    expect(Component.find('.panelHeader').text()).toBe(title)

    expect(Component).toMatchSnapshot()
  })
})
