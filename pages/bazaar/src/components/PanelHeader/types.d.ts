type TColor = 'brown' | 'black' | 'gray' | 'red' | 'green'

export interface IProps {
  children: string
  color?: TColor
}
