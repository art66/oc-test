import React from 'react'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const PanelHeader: React.SFC<IProps> = (props: IProps): JSX.Element => {
  const { children } = props

  return (
    <div className={styles.panelHeader} role='heading' aria-level={5}>
      {children}
    </div>
  )
}

export default PanelHeader
