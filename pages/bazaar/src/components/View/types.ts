import { IBazaarItemInfo } from '../../modules/types/state'

export interface IStateProps {
  info?: IBazaarItemInfo
  isLoading: boolean
}

export interface IStateActions {
  setViewHeightAction: (uniqueId: string, height: number) => void
}

export interface IProps extends IStateProps, IStateActions {
  className?: string
  uniqueId: string
  onClose: (event: React.MouseEvent<HTMLElement>) => void
  onHeightUpdate: () => any
  noImage?: boolean
}
