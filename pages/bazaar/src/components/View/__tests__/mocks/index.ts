import { IProps } from '../../types'

export const initialState: IProps = {
  id: '57854',
  rowIndex: 0,
  info: {
    isLoading: true,
    id: '108',
    itemCirculation: '17,514',
    itemCost: '$1,100,000',
    itemID: '108',
    itemInfo:
      'This Uzi is a compact, boxy, light-weight sub machine gun' +
      ' used throughout the world as a police and special forces firearm.',
    itemInfoContent:
      '↵            <div class="m-bottom10">↵                <span ' +
      'class="bold">The 9mm Uzi</span> is a  SMG Weapon.↵          ' +
      '  </div>↵            This Uzi is a compact, boxy, light-weig' +
      'ht sub machine gun used throughout the world as a police and' +
      ' special forces firearm.↵            ',
    itemName: '9mm Uzi',
    itemRareIcon: 'common-rarity',
    itemRareTitle: '<p>Common</p> 17,514 total in circulation',
    itemSell: '$600,000',
    itemType: 'Weapon',
    itemValue: '$599,833',
    test: null
  },
  onClose: () => {},
  setViewHeight: () => {},
  unsetViewHeight: () => {}
}
