import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import ResizeObserver from 'resize-observer-polyfill'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import styles from './index.cssmodule.scss'
import { IState as IGlobalState } from '../../store/types'
import { IProps, IStateProps, IStateActions } from './types'
import { IBazaarItemInfoSettings } from '../../modules/types/state'
import { setActiveViewHeight } from '../../modules/actions'
import convertImageToCanvas from '@torn/shared/utils/canvas/convertImageToCanvas'

export class View extends PureComponent<IProps> {
  static defaultProps: IProps = {
    uniqueId: null,
    onClose: () => {},
    setViewHeightAction: () => {},
    onHeightUpdate: () => {},
    info: {
      id: null,
      itemCirculation: null,
      itemCost: null,
      itemID: null,
      itemInfo: null,
      itemInfoContent: null,
      itemName: null,
      itemRareIcon: null,
      itemRareTitle: null,
      itemSell: null,
      itemType: null,
      itemValue: null,
      test: null,
      extras: [],
      glow: null
    },
    isLoading: false
  }

  ref: React.RefObject<HTMLDivElement>
  infoRef: React.RefObject<HTMLDivElement>
  _canvasImage: HTMLCanvasElement = null

  _resizeObserver = new ResizeObserver(() => {
    this._updateHeight()
  })

  constructor(props: IProps) {
    super(props)

    this.ref = React.createRef()
    this.infoRef = React.createRef()
  }

  componentDidMount() {
    this._updateHeight()

    this._resizeObserver.observe(this.ref.current)

    this._bindTemplate()
  }

  componentWillUnmount() {
    this._resizeObserver.unobserve(this.ref.current)
    this._resizeObserver.disconnect()

    this._removeImgGlow()
  }

  componentDidUpdate() {
    this._bindTemplate()
  }

  _bindTemplate = () => {
    // @ts-ignore
    const $infoRef = $(this.infoRef.current)

    // @ts-ignore
    itemInfoHandler($infoRef)
    // FIXME: Temporary solution for Handlebars

    this._addImgGlow()
  }

  _updateHeight = () => {
    const { uniqueId, setViewHeightAction, onHeightUpdate } = this.props

    const { height } = this.ref.current.getBoundingClientRect()

    setViewHeightAction(uniqueId, height)

    onHeightUpdate()
  }

  _onInfoClick = event => {
    const { onClose } = this.props

    if (event.target.className.includes('close-icon')) {
      onClose(event)
    }
  }

  _addImgGlow = () => {
    const { info } = this.props
    const imgSrc = `/images/items/${info.itemID}/large.png?v=1528808940574`
    const imgContainer = document.querySelector('.show-item-info .img-wrap')
    const img = document.querySelector('.show-item-info .img-wrap img')

    if (!!(info.glow && !this._canvasImage && imgContainer && img)) {
      this._canvasImage = convertImageToCanvas({
        ...info.glow,
        imgSrc,
        offsetX: 0,
        offsetY: 0
      })

      this._canvasImage.className = styles.canvasGlow

      imgContainer.append(this._canvasImage)

      if (img) {
        imgContainer.removeChild(img)
      }
    }
  }

  _removeImgGlow = () => {
    const { info } = this.props
    const imgContainer = document.querySelector('.show-item-info .img-wrap')

    if (!!(info.glow && imgContainer && this._canvasImage && imgContainer.contains(this._canvasImage))) {
      imgContainer.removeChild(this._canvasImage)

      this._canvasImage = null
    }
  }

  _renderTemplate = () => {
    const { info, noImage } = this.props

    // @ts-ignore
    const template = Handlebars.templates['iteminfo-template-wrap']
    // FIXME: Temporary solution, should be written in React
    const html = template({ ...info, noThump: noImage })

    return (
      <div
        className={cn(styles.info, 'show-item-info')}
        ref={this.infoRef}
        dangerouslySetInnerHTML={{ __html: html }}
        onClick={this._onInfoClick}
      />
    )
  }

  _renderInfo = () => {
    const { isLoading } = this.props

    if (isLoading) {
      return <LoadingIndicator />
    }

    return (
      <div className="items-list-wrap">
        <div className="items-list">{this._renderTemplate()}</div>
      </div>
    )
  }

  render() {
    const { className } = this.props

    return (
      <div ref={this.ref} className={cn(styles.view, className)}>
        {this._renderInfo()}
      </div>
    )
  }
}

const mapStateToProps = (state: IGlobalState, props: IProps) => {
  const data = state.common.views[props.uniqueId]
  const settings = state.common.viewsSettings[props.uniqueId] || ({ isLoading: false } as IBazaarItemInfoSettings)

  const stateProps: IStateProps = {
    info: data,
    isLoading: settings.isLoading
  }

  return stateProps
}

const mapDispatchToProps: IStateActions = {
  setViewHeightAction: setActiveViewHeight
}

export default connect(mapStateToProps, mapDispatchToProps)(View)
