import tooltipStyles from './tooltip.cssmodule.scss'

export const tooltipConfig = {
  showTooltip: true,
  addArrow: true,
  position: { x: 'center', y: 'top' },
  manualCoodsFix: { fixX: -11 },
  overrideStyles: { ...tooltipStyles }
}
