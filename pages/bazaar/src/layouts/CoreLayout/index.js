import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import InfoBox from '@torn/shared/components/InfoBox'
import DebugInfo from '@torn/shared/components/DebugBox'
import AppHeader from '@torn/shared/components/AppHeader'
import Tooltip from '@torn/shared/components/TooltipNew'

import { getHeadersData } from '../../modules/selectors'
import { linkChage } from '../../modules/actions'

import styles from './index.cssmodule.scss'
import { tooltipConfig } from './constants'

class CoreLayout extends PureComponent {
  _renderAppHeader = () => {
    const { appID, pageID /* , metadata, owner */, headersData, linkCallback } = this.props

    // const _isHeaderLinksLoading = typeof pageID !== 'number' || metadata.isLoading || owner.isLoading

    // if (_isHeaderLinksLoading) {
    //   return <>Loading</>
    // }
    // TODO: It's better to have the loader in AppHeader component

    return <AppHeader appID={appID} pageID={pageID} clientProps={headersData} linkCallback={linkCallback} />
  }

  render() {
    const { dbg, requestInfoBox, infoBox, children } = this.props

    return (
      <div className={styles['core-layout']}>
        {this._renderAppHeader()}
        <InfoBox {...requestInfoBox} showDangerously />
        <InfoBox {...infoBox} showDangerously />
        {children}
        {dbg && <DebugInfo debugMessage={dbg.msg} />}
        <Tooltip {...tooltipConfig} />
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
  appID: PropTypes.string.isRequired,
  pageID: PropTypes.number,
  dbg: PropTypes.string,
  infoBox: PropTypes.object,
  requestInfoBox: PropTypes.object,
  tutorial: PropTypes.object,
  location: PropTypes.object,
  linkCallback: PropTypes.func,
  headersData: PropTypes.object
}

CoreLayout.defaultProps = {
  tutorial: {},
  location: {
    pathname: ''
  },
  linkCallback: () => {}
}

const mapStateToProps = state => {
  return {
    appID: state.common.appID,
    pageID: state.common.pageID,
    dbg: state.common.dbg,
    requestInfoBox: state.common.requestInfoBox,
    infoBox: state.common.infoBox,
    headersData: getHeadersData(state)
  }
}

const mapDispatchToProps = {
  linkCallback: linkChage
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CoreLayout)
)
