export const ROOT_URL = '/bazaar.php?sid=bazaarData'

export const BAZAAR_PAGE_ID = 0
export const ADD_PAGE_ID = 1
export const MANAGE_PAGE_ID = 2
export const PERSONALIZE_PAGE_ID = 3

export const PERSONALIZE_FORM_DATA_URL = `${ROOT_URL}&step=getPersonalizeContent`

export const MOBILE_TYPE = 'mobile'
export const TABLET_TYPE = 'tablet'
export const DESKTOP_TYPE = 'desktop'

export const DESKTOP_WIDTH = 784
export const TABLET_WIDTH = 386
export const MOBILE_WIDTH = 320

export const AMOUNT_MAX_VALUE = 10000
