import { SECOND, MINUTE, HOUR, DAY } from '../constants'

const { numberFormat, $, getCurrentTimestamp } = window

export {
  $,
  numberFormat,
  getCurrentTimestamp
}

export function plusZero(num) {
  return num < 10 ? '0' + num : num
}

export function dateTimeFormat(timestamp) {
  const d = new Date(timestamp)
  const year = d.getUTCFullYear()
  const month = plusZero(d.getUTCMonth() + 1)
  const date = plusZero(d.getUTCDate())
  const hours = plusZero(d.getUTCHours())
  const minutes = plusZero(d.getUTCMinutes())
  const seconds = plusZero(d.getUTCSeconds())
  return `${year}-${month}-${date} ${hours}:${minutes}:${seconds}`
}

export function shortMoneyFormat(num) {
  const billion = 'b'
  const million = 'm'
  const thousand = 'k'
  let result = false
  if (num < 1000) {
    result = num
  } else if (num < 1000000) {
    if (num < 10000) {
      result = Math.floor(num / 100) / 10 + thousand
    } else {
      result = Math.round(num / 1000) + thousand
    }
  } else if (num < 1000000000) {
    result = Math.floor(num / 100000)
    result = (result < 100 ? result / 10 : Math.round(result / 10)) + million
  } else if (num >= 1000000000) {
    result = Math.floor(num / 100000000)
    result = (result < 100 ? result / 10 : Math.round(result / 10)) + billion
  }
  if (!result) {
    return numberFormat(num)
  } else {
    return result
  }
}

export function decimalToFractionalOdd(decimal) {
  const p = 1 / decimal

  for (let diff = 0.00001; diff <= 0.001; diff *= 10) {
    for (let x = 1; x <= 1000; x++) {
      for (let y = 1; y <= 1000; y++) {
        let pf = y / (y + x)
        if (Math.abs(p - pf) < diff) {
          return x + '/' + y
        }
      }
    }
  }
}

export function decimalOdd(decimal) {
  if (decimal < 10) {
    return decimal.toFixed(2)
  } else if (decimal < 100) {
    return decimal.toFixed(1)
  } else {
    return decimal
  }
}

// function numberFormat(number) {
//   return globalNumberFormat(number)
// }

export function getCountdown(timestamp, now) {
  const day = Math.floor((timestamp - now) / DAY)
  if (day > 0) {
    return day + 'd'
  }

  const hour = Math.floor((timestamp - now) / HOUR)
  if (hour > 0) {
    return hour + 'h'
  }

  const minute = Math.floor((timestamp - now) / MINUTE)
  if (minute > 0) {
    return minute + 'm'
  }

  const second = Math.floor((timestamp - now) / SECOND)
  if (second > 0) {
    return second + 's'
  }
}

export function finishTimestamp(ts) {
  return dateTimeFormat(new Date(Math.floor(ts / MINUTE) * MINUTE).getTime())
}

export function sportNameByAlias(alias) {
  return (alias[0].toUpperCase() + alias.slice(1)).split('-').join(' ')
}

export function dayDateFormat(timestamp) {
  const d = new Date(timestamp)
  const year = d
    .getUTCFullYear()
    .toString()
    .substr(-2)
  const month = plusZero(d.getUTCMonth() + 1)
  const date = plusZero(d.getUTCDate())
  return `${date}/${month}/${year}`
}

export function betTitle(bet) {
  const team = bet.team
  let betState = ''
  let title = ''
  let marketPart = `(${bet.handicap != 0 ? (bet.handicap > 0 ? '+' : '') + bet.handicap + ' handicap' : bet.marketName})`

  if (bet.result === 'won') {
    betState = 'won'
    title = `Won $${numberFormat(bet.moneyGain)} (x${bet.priceTraded})
    from a $${numberFormat(bet.amount)} bet on ${team} (${bet.marketName})`
  } else if (bet.result === 'lost') {
    betState = 'lost'
    title = `Lost $${numberFormat(bet.amount)} (x${bet.priceTraded}) bet on ${team} ${marketPart}`
  } else if (bet.result === 'refunded') {
    betState = 'refunded'
    title = `Refunded $${numberFormat(bet.amount)} (x${bet.priceTraded}) bet on ${team} ${marketPart}`
  } else {
    title = `Pending $${numberFormat(bet.amount)} (x${bet.priceTraded}) bet on ${team} ${marketPart}`
    betState = 'pending'
  }
  const symbol = betState === 'refunded' || betState === 'pending' ? '' : betState === 'lost' ? '-' : '+'
  // const symbol = ''
  const withdraw = ['won', 'refunded'].indexOf(bet.result) >= 0 && !bet.withdrawn ? 'withdraw' : ''

  return {
    symbol,
    title,
    betState,
    withdraw
  }
}
