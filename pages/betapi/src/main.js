import React from 'react'
import { render } from 'react-dom'
import { hashHistory } from 'react-router'
import configureStore from './store/configureStore'
import AppContainer from './containers/AppContainer'
import { syncHistoryWithStore } from 'react-router-redux'

const store = configureStore()
const history = syncHistoryWithStore(hashHistory, store)

render(<AppContainer store={store} history={history} />, document.getElementById('react-root'))
