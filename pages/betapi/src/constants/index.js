// export const BASE_URL = '/bookies_logout.php?step=betfair'
export const BASE_URL = '/bookies.php?step=betfair'

export const TIME_FORMAT_NAME = 'TCT'

export const SECOND = 1000
export const MINUTE = 60000
export const HOUR = 3600000
export const DAY = 86400000

export const YOUR_BETS_ALIAS = 'your-bets'
export const POPULAR_ALIAS = 'popular'

export const MIN_SEARCH_LENGTH = 3
export const MIN_MATCHE_NUMBER = 25
