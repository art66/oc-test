import { fetchUrl } from '@torn/shared/utils'

// ------------------------------------
// Constants
// ------------------------------------
export const EMIT_REQUEST = 'EMIT_REQUEST'
export const SPORTS_LIST_LOADED = 'SPORTS_LIST_LOADED'
export const SPORT_MATCHES_LOADED = 'SPORT_MATCHES_LOADED'
export const OPEN_MATCH_ODDS = 'OPEN_MATCH_ODDS'
export const SHOW_DEBUG_INFO = 'SHOW_DEBUG_INFO'
export const HIDE_DEBUG_INFO = 'HIDE_DEBUG_INFO'

// ------------------------------------
// Actions
// ------------------------------------

export const loadGameBoxesList = () => dispatch => {
  fetchUrl('entity=sports')
    .then(json => dispatch(gameBoxesListLoaded(json)))
    .then(json => dispatch(handleNewHash(true)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const setHashHandler = () => dispatch => {
  window.addEventListener('hashchange', () => dispatch(handleNewHash()), false)
}

export const handleNewHash = (firstLoad = false) => dispatch => {
  var hash = window.location.hash.replace(/^#\/?|\/$/g, '').split('/')

  if (hash[0] === 'odds') {
    console.log('loading: ' + hash.toString())
    if (hash.length === 2 || firstLoad) {
      dispatch(loadsportMatches(hash))
    } else if (hash.length === 3) {
      dispatch(matchOddsOpend(hash[2]))
    }
  }
}

export const loadsportMatches = hash => (dispatch, getState) => {
  dispatch(emitRequest())
  let url = ''
  for (let i = 0; i < hash.length; i++) {
    if (i === 0) {
      url = `entity=${hash[i]}`
    } else {
      url += `&option${i}=${hash[i]}`
    }
  }
  fetchUrl(url)
    .then(json => dispatch(sportMatchesLoaded(json)))
    .then(json => {
      const state = getState().app
      console.log(state)
    })
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const openMatchOdds = matchID => dispatch => {
  console.log(matchID)
  dispatch(matchOddsOpend(matchID))
}

export function emitRequest() {
  return {
    type: EMIT_REQUEST
  }
}

export const gameBoxesListLoaded = json => ({
  type: SPORTS_LIST_LOADED,
  json: json,
  meta: 'ajax'
})

export const sportMatchesLoaded = json => ({
  type: SPORT_MATCHES_LOADED,
  json: json,
  meta: 'ajax'
})

export const matchOddsOpend = matchID => ({
  type: OPEN_MATCH_ODDS,
  matchID: matchID
})

export const showDebugInfo = ({ msg }) => ({
  type: SHOW_DEBUG_INFO,
  loading: false,
  msg: msg
})

export const hideDebugInfo = () => ({
  type: HIDE_DEBUG_INFO
})

export const actions = {
  gameBoxesListLoaded,
  loadsportMatches,
  matchOddsOpend,

  setHashHandler,
  showDebugInfo
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [EMIT_REQUEST]: state => ({
    ...state,
    loading: true
  }),

  [SPORTS_LIST_LOADED]: (state, action) => {
    return {
      ...state,
      ...action.json,
      gameBoxesListLoaded: true
    }
  },

  [SPORT_MATCHES_LOADED]: (state, action) => ({
    ...state,
    ...action.json,
    loading: false,
    sportMatchesLoaded: true
  }),

  [OPEN_MATCH_ODDS]: (state, action) => ({
    ...state,
    activeMatchID: action.matchID
  }),

  [SHOW_DEBUG_INFO]: (state, action) => ({
    ...state,
    dbg: action.msg
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  gameBoxesList: [],
  sportMatchesLoaded: false,
  activeMatchID: false,
  activeSportMatchesAlias: ''
}

export default function(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
