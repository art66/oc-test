import { handleActions } from 'redux-actions'
import { getCurrentTimestamp } from '../utils'

const initialState = {
  gotContent: false,
  now: getCurrentTimestamp(),
  currentPage: 0,
  currentSearchPage: -1,
  search: '',
  competitionBox: '',
  loadedMatchesAmountBySearch: {},
  moreGamesLoading: false,
  withdrawMatchBet: '',
  gameBoxesList: [],
  userData: {},
  statsData: undefined
}

export default handleActions(
  {
    'set now': function(state, action) {
      return {
        ...state,
        now: action.payload.now,
        userData: {
          ...state.userData,
          money: Number.isNaN(action.payload.userMoney) ? undefined : action.payload.userMoney
        }
      }
    },

    'set page mode': function(state, action) {
      return {
        ...state,
        pageMode: action.payload.pageMode
      }
    },

    'data loaded': function(state, action) {
      return {
        ...state,
        gotContent: true,
        ...action.payload.json
      }
    },

    // '@@router/LOCATION_CHANGE'(state, action) {
    //   return {
    //     ...state
    //   }
    // },

    'game box loaded': function(state, action) {
      return {
        ...state,
        currentPage: 0,
        currentSearchPage: -1,
        search: '',
        competitionBox: '',
        chosenShowMore: false,
        loadedMatchesAmountBySearch: {},
        ...action.payload.json
      }
    },

    'more game box loaded': function(state, action) {
      const boxIndex = state.gameBoxesList.findIndex(box => box.alias === action.payload.gameBox)
      const gameBox = state.gameBoxesList[boxIndex]
      const matchIDs = {}
      const allMatches = [...gameBox.matches, ...action.payload.json.matches]
      const uniqueMatches = []

      for (let i in allMatches) {
        const match = allMatches[i]

        if (!matchIDs[match.ID]) {
          uniqueMatches.push(match)
          matchIDs[match.ID] = true
        }
      }
      return {
        ...state,
        currentPage: action.payload.page + 1,
        // currentSearchPage: action.payload.page + 1,
        loadedMatchesAmountBySearch: {
          [state.competitionBox + '_' + state.search]: action.payload.json.matches.length
        },
        gameBoxesList: [
          ...state.gameBoxesList.slice(0, boxIndex),
          {
            ...gameBox,
            matches: uniqueMatches,
            loadedMatchesAmount: action.payload.json.matches.length
          },
          ...state.gameBoxesList.slice(boxIndex + 1)
        ],
        moreGamesLoading: false
      }
    },

    'more games loading': function(state, action) {
      return {
        ...state,
        moreGamesLoading: true,
        currentSearchPage: state.currentSearchPage + 1
      }
    },

    'game loaded': function(state, action) {
      return {
        ...state,
        ...action.payload.json
      }
    },

    'game markets loading': function(state, action) {
      const boxIndex = state.gameBoxesList.findIndex(box => box.alias === action.payload.gameBox)
      const gameBox = state.gameBoxesList[boxIndex]
      const matchIndex = gameBox.matches.findIndex(match => match.ID === action.payload.gameID)
      const match = gameBox.matches[matchIndex]

      return {
        ...state,
        gameBoxesList: [
          ...state.gameBoxesList.slice(0, boxIndex),
          {
            ...gameBox,
            matches: [
              ...gameBox.matches.slice(0, matchIndex),
              {
                ...match,
                markets: undefined
              },
              ...gameBox.matches.slice(matchIndex + 1)
            ]
          },
          ...state.gameBoxesList.slice(boxIndex + 1)
        ]
      }
    },

    'game markets loaded': function(state, action) {
      const boxIndex = state.gameBoxesList.findIndex(box => box.alias === action.payload.gameBox)
      const gameBox = state.gameBoxesList[boxIndex]
      const matchIndex = gameBox.matches.findIndex(match => match.ID === action.payload.gameID)
      const match = gameBox.matches[matchIndex]

      return {
        ...state,
        gameBoxesList: [
          ...state.gameBoxesList.slice(0, boxIndex),
          {
            ...gameBox,
            matches: [
              ...gameBox.matches.slice(0, matchIndex),
              {
                ...match,
                markets: action.payload.json.markets
              },
              ...gameBox.matches.slice(matchIndex + 1)
            ]
          },
          ...state.gameBoxesList.slice(boxIndex + 1)
        ],
        activeBet: ''
      }
    },

    'bet selected': function(state, action) {
      return {
        ...state,
        activeBet: action.payload.runner.marketId + '-' + action.payload.runner.selectionId
      }
    },

    'game loading': function(state, action) {
      return {
        ...state,
        activeGame: action.payload.gameID,
        gameLoading: true,
        activeBet: ''
      }
    },

    'bet loading': function(state, action) {
      return {
        ...state,
        betLoading: true
      }
    },

    'bet loaded': function(state, action) {
      const boxIndex = state.gameBoxesList.findIndex(box => box.alias === action.payload.gameBox)
      const gameBox = state.gameBoxesList[boxIndex]
      const matchIndex = gameBox.matches.findIndex(match => match.ID === action.payload.gameID)

      return {
        ...state,
        gameBoxesList: [
          ...state.gameBoxesList.slice(0, boxIndex),
          {
            ...gameBox,
            matches: [
              ...gameBox.matches.slice(0, matchIndex),
              action.payload.json.match,
              ...gameBox.matches.slice(matchIndex + 1)
            ]
          },
          ...state.gameBoxesList.slice(boxIndex + 1)
        ],
        userData: action.payload.json.userData,
        betLoading: false
      }
    },

    'withdrawing bet': function(state, action) {
      return {
        ...state,
        withdrawMatchBet: action.payload.gameID
      }
    },

    'withdrawn bet': function(state, action) {
      const boxIndex = state.gameBoxesList.findIndex(box => box.alias === action.payload.gameBox)
      const gameBox = state.gameBoxesList[boxIndex]
      const matchIndex = gameBox.matches.findIndex(match => match.ID === action.payload.gameID)

      return {
        ...state,
        gameBoxesList: [
          ...state.gameBoxesList.slice(0, boxIndex),
          {
            ...gameBox,
            matches: [
              ...gameBox.matches.slice(0, matchIndex),
              action.payload.json.match,
              ...gameBox.matches.slice(matchIndex + 1)
            ]
          },
          ...state.gameBoxesList.slice(boxIndex + 1)
        ],
        userData: action.payload.json.userData,
        withdrawMatchBet: ''
      }
    },

    'withdrawing all': function(state, action) {
      return {
        ...state,
        userData: {
          ...state.userData,
          withdrawing: true
        }
      }
    },

    'all bet withdrawn': function(state, action) {
      return {
        ...state,
        userData: {
          ...state.userData,
          withdrawing: false
        },
        ...action.payload.json
      }
    },

    'bring info box': function(state, action) {
      return {
        ...state,
        infoBox: {
          msg: action.payload.msg,
          color: action.payload.color
        }
      }
    },
    'hide info box': function(state, action) {
      return {
        ...state,
        infoBox: null
      }
    },
    'set competition box': function(state, action) {
      return {
        ...state,
        competitionBox: action.payload.competitionBox,
        currentSearchPage: -1
      }
    },
    'set search': function(state, action) {
      const search = action.payload.search.trim().toLocaleLowerCase()

      return {
        ...state,
        search,
        competitionBox: search ? '' : state.competitionBox,
        currentSearchPage: -1
      }
    },

    'set show more live matches': function(state, action) {
      return {
        ...state,
        chosenShowMore: true
      }
    },

    'stats data loading': function(state, action) {
      return {
        ...state,
        search: '',
        competitionBox: '',
        statsData: undefined
      }
    },

    'stats data loaded': function(state, action) {
      return {
        ...state,
        statsData: {
          topStats: action.payload.json.topStats
        },
        gameBoxesList: action.payload.json.gameBoxesList
      }
    }
  },
  initialState
)
