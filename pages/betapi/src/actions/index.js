import { createAction } from 'redux-actions'
import { fetchUrl } from '@torn/shared/utils'
import { BASE_URL } from '../constants'

export function updateNow(now, userMoney) {
  return dispatch => {
    dispatch(setNow(now, userMoney))
  }
}

export function initPageMode(dispatch, getState) {
  let isResponsive = (document.body.classList.contains('d') && document.body.classList.contains('r'))
  let mediaType = getState().browser.mediaType === 'infinity' ? 'desktop' : getState().browser.mediaType
  let pageMode = isResponsive ? mediaType : 'desktop'
  dispatch(setPageMode(pageMode))
}

export function getInitialData(gameBox = '', isStats = false) {
  return (dispatch, getState)=> {
    return fetchUrl(BASE_URL + `&action=getState&gamebox=${gameBox}&isstats=${isStats}`).then(json => {
      initPageMode(dispatch, getState)
      dispatch(dataLoaded(json))
    })
    // .catch((error) => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const setPageMode = createAction('set page mode', (pageMode) => ({ pageMode }))

export function loadGameBoxData(gameBox, isStats = false) {
  return dispatch => {
    return fetchUrl(BASE_URL + `&action=getState&gamebox=${gameBox}&isstats=${isStats}`).then(json => {
      dispatch(gameBoxLoaded(json, gameBox))
    })
    // .catch((error) => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export function loadMoreGameBoxData(gameBox, currentPage, search = '', competition = '', isStats = false) {
  return dispatch => {
    dispatch(moreGamesLoading())
    return fetchUrl(
      BASE_URL +
        `&action=loadMatches&gamebox=${gameBox}&page=${currentPage}` +
        `&search=${search}&competition=${competition}&isstats=${isStats}`
    ).then(json => {
      dispatch(gameMoreBoxLoaded(json, gameBox, currentPage))
    })
  }
}

export const gameMoreBoxLoaded = createAction('more game box loaded', (json, gameBox, page) => ({
  json,
  gameBox,
  page,
  meta: 'ajax'
}))

export function loadGameData(gameBox, gameID) {
  return dispatch => {
    dispatch(gameMarketsLoading(gameBox, gameID))
    return fetchUrl(BASE_URL + '&action=getEventMarkets&gamebox=' + gameBox + '&gameid=' + gameID).then(json => {
      dispatch(gameMarketsLoaded(json, gameBox, gameID))
    })
  }
}

export function selectGame(sportAlias, gameID) {
  return dispatch => {
    dispatch(gameLoading(gameID))
    if (gameID) {
      return fetchUrl(BASE_URL + '&action=getState&gamebox=' + sportAlias + '&gameid=' + gameID).then(json => {
        dispatch(gameLoaded(json, sportAlias, gameID))
      })
      // .catch((error) => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
    }
  }
}

export function selectBet(runner) {
  return dispatch => {
    dispatch(betSelected(runner))
  }
}

export function betOnGame(sportAlias, gameID, marketId, selectionId, amount, lastPriceTraded, handicap = 0) {
  return dispatch => {
    dispatch(betLoading())
    return fetchUrl(BASE_URL + '&action=addBet&gamebox=' + sportAlias + '&gameid=' + gameID, {
      data: { marketId, selectionId, amount, lastPriceTraded, handicap }
    }).then(json => {
      dispatch(betLoaded(json, sportAlias, gameID, marketId, selectionId))
    })
    // .catch((error) => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export function withdrawBet(matchID, gameBox) {
  return dispatch => {
    dispatch(withdrawingBet(matchID))
    return fetchUrl(BASE_URL + '&action=withdrawBet', {
      gameid: matchID
    }).then(json => {
      dispatch(betWithdrawn(json, matchID, gameBox))
    })
  }
}

export function withdrawAll(gameBox, gameID) {
  return dispatch => {
    dispatch(withdrawingAll())
    return fetchUrl(BASE_URL + `&action=withdrawAll&gamebox=${gameBox}&gameid=${gameID}`, {}).then(json => {
      dispatch(allBetWithdrawn(json))
    })
  }
}

export const withdrawingAll = createAction('withdrawing all', () => ({}))

export const allBetWithdrawn = createAction('all bet withdrawn', json => ({
  json
}))

export function setCompetitionBox(competitionBox) {
  return dispatch => {
    dispatch(setSearchByCompetition(competitionBox))
  }
}

export const setSearchByCompetition = createAction('set competition box', competitionBox => ({
  competitionBox
}))

export function setSearch(search) {
  return dispatch => {
    dispatch(setMatchSearch(search))
  }
}

export const setMatchSearch = createAction('set search', search => ({
  search
}))

export function showMoreLiveMatches() {
  return dispatch => {
    dispatch(setShowMoreLiveMatches())
  }
}

export const setShowMoreLiveMatches = createAction('set show more live matches', () => ({}))

export function loadStatsData(gameBox) {
  return (dispatch, getState) => {
    dispatch(statsDataLoading())
    return fetchUrl(BASE_URL + `&action=getStats&gamebox=${gameBox}`).then(json => {
      initPageMode(dispatch, getState)
      dispatch(statsDataLoaded(json))
    })
  }
}

export const statsDataLoading = createAction('stats data loading', () => ({}))

export const statsDataLoaded = createAction('stats data loaded', json => ({
  json
}))

export const withdrawingBet = createAction('withdrawing bet', gameID => ({
  gameID
}))

export const betWithdrawn = createAction('withdrawn bet', (json, gameID, gameBox) => ({
  json,
  gameID,
  gameBox
}))

export const betSelected = createAction('bet selected', runner => ({
  runner
}))

export const setNow = createAction('set now', (now, userMoney) => ({ now, userMoney }))

export const gameLoaded = createAction('game loaded', (json, sportAlias, gameID) => ({
  json,
  sportAlias,
  gameID,
  meta: 'ajax'
}))

export const gameMarketsLoading = createAction('game markets loading', (gameBox, gameID) => ({
  gameBox,
  gameID
}))

export const gameMarketsLoaded = createAction('game markets loaded', (json, gameBox, gameID) => ({
  json,
  gameBox,
  gameID,
  meta: 'ajax'
}))

export const gameBoxLoaded = createAction('game box loaded', (json, sportAlias) => ({ json, sportAlias, meta: 'ajax' }))

export const moreGamesLoading = createAction('more games loading')

export const dataLoaded = createAction('data loaded', json => ({
  json,
  meta: 'ajax'
}))

export const gameLoading = createAction('game loading', gameID => ({ gameID }))

export const betLoading = createAction('bet loading', () => ({}))

export const betLoaded = createAction('bet loaded', (json, gameBox, gameID, marketId, selectionId) => ({
  json,
  gameBox,
  gameID,
  marketId,
  selectionId
}))

export const bringInfoBox = createAction('bring info box')

export const hideInfoBox = createAction('hide info box')

export function showInfoBox(argsObj) {
  return dispatch => {
    console.error(argsObj.msg)
    dispatch(bringInfoBox(argsObj))

    if (!argsObj.persistent) {
      setTimeout(() => dispatch(hideInfoBox()), 5000)
    }
  }
}
