import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Router, Route, Redirect } from 'react-router'
import { Provider } from 'react-redux'
import { Main, GameBox, Markets, Stats } from '../components'
import { GameBoxStats } from '../components/Stats/components'

class AppContainer extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

  render() {
    const { history, store } = this.props

    return (
      <Provider store={store}>
        <div style={{ height: '100%' }}>
          <Router history={history}>
            <Redirect from="/" to="/your-bets" />
            <Redirect from="/stats" to="/stats/football" />
            <Route path="/stats" component={Stats}>
              <Route path="/stats/:activeGameBox" component={GameBoxStats} />
            </Route>
            <Route path="/" component={Main}>
              <Route path="/:activeGameBox" component={GameBox} history={history}>
                <Route path="/:activeGameBox/:activeGameID" component={Markets} />
              </Route>
            </Route>
          </Router>
        </div>
      </Provider>
    )
  }
}

export default AppContainer
