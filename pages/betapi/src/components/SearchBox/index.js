import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { MIN_SEARCH_LENGTH } from '../../constants'
import Dropdown from '@torn/shared/components/Dropdown'

const ALL = 'All'

class SearchBox extends Component {
  onChange(event) {
    if (event.target.value.length >= MIN_SEARCH_LENGTH) {
      this.props.setSearch(event.target.value)
    } else {
      this.props.setSearch('')
    }
  }

  setSearchField(value) {
    this.refs.search.value = value
    this.props.setSearch(value)
  }

  render() {
    const { competitions, competitionBox, setCompetitionBox } = this.props

    const defaultComp = {
      id: ALL,
      name: ALL
    }

    return (
      <div className="search-box">
        <input
          className="search-input m-bottom5"
          name="search"
          ref="search"
          autoComplete="off"
          onChange={event => this.onChange(event)}
        />
        {competitions.map(item => (
          <div
            className={'search-label' + (competitionBox === item ? ' selected' : '')}
            key={item}
            onClick={() => setCompetitionBox(item === competitionBox ? '' : item)}
          >
            {item}
          </div>
        ))}
        <Dropdown
          placeholder={'Select competition'}
          list={[defaultComp].concat(
            competitions.map(item => ({
              id: name,
              name: item
            }))
          )}
          selected={competitionBox ? { id: competitionBox, name: competitionBox } : undefined}
          onChange={value => setCompetitionBox(value.name === 'All' ? '' : value.name)}
        />
      </div>
    )
  }
}

SearchBox.propTypes = {
  competitions: PropTypes.array,
  competitionBox: PropTypes.string,
  setCompetitionBox: PropTypes.func,
  setSearch: PropTypes.func
}

export default SearchBox
