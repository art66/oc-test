import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import { UserBets } from '..'
import { YOUR_BETS_ALIAS } from '../../constants'
import { getCountdown, dateTimeFormat, finishTimestamp, sportNameByAlias } from '../../utils'

class Game extends Component {
  _getMatchState(match) {
    const { now } = this.props

    const wonBet = this.getBet(match.bets, 'won')
    const refundedBet = this.getBet(match.bets, 'refunded')

    const refundedOrWon = wonBet && !wonBet.withdrawn || refundedBet && !refundedBet.withdrawn

    if (match.time > now && !refundedOrWon) {
      return (
        <span className='state countdown' title={`Due to start at ${dateTimeFormat(match.time)}`}>
          {getCountdown(match.time, now)}
        </span>
      )
    } else if (match.isFinished === 1 || refundedOrWon) {

      if (wonBet || refundedBet) {
        if ((wonBet && wonBet.withdrawn) || (refundedBet && refundedBet.withdrawn)) {
          return (
            <i className='state gm-state-icon won' title={`Finished at ${finishTimestamp(match.finishTimestamp)}`} />
          )
        }
        return (
            <span className='special' style={{ marginTop: '3px' }}>
              <button
                className='windraw-icon-button'
                role='button'
                onClick={e => {
                  e.preventDefault()
                  this.props.withdrawBet(this.props.match.ID, this.props.activeGameBox)
                }}
              >
                <i className='state gm-withdraw-icon' />
              </button>
            </span>
        )
      }
      return (
          <i
            className={`state gm-state-icon ${match.isRefunded ? 'lost' : 'won'}`}
            title={`Finished at ${finishTimestamp(match.finishTimestamp)}`}
          />
      )
    }
    return <i className='state gm-state-icon waiting' title='Match is in progress' />
  }

  getBet(bets, result = 'won') {
    let wonBet = false

    for (const i in bets) {
      if (bets[i].result === result) {
        wonBet = bets[i]
        break
      }
    }
    return wonBet
  }

  _getMatchBetState(match) {
    const { now, activeGameBox } = this.props

    if (match.time > now || match.bets === 0 || activeGameBox !== YOUR_BETS_ALIAS) {
      return <span />
    }
    return (
      <ul
        className='stick-wrap right m-left10'
        style={{
          width: 100,
          textAlign: 'right'
        }}
      >
        <li>
          {(() => {
            if (match.isFinished === 0) {
              return <span>Pending</span>
            }
            const wonBet = this.getBet(match.bets, 'won')

            if (wonBet) {
              if (wonBet.withdrawn === 0) {
                return (
                    <span className='special'>
                      <button
                        className='torn-btn'
                        style={{ padding: '0 15px 0 10px' }}
                        onClick={e => {
                          e.preventDefault()
                          this.props.withdrawBet(this.props.match.ID)
                        }}
                      >
                        WITHDRAW
                      </button>
                    </span>
                )
              }
              return <span>WITHDRAWN</span>
            }
            return <span>LOST</span>
          })()}
        </li>
      </ul>
    )
  }
  // TODO: We don't use it

  getAmountOfBets(bets) {
    if (!bets) {
      return 0
    }
    let amount = 0
    const types = {}

    for (const i in bets) {
      if (!types[bets[i].result]) {
        amount++
        types[bets[i].result] = true
      }
    }
    return amount
  }

  render() {
    const { match, alias, activeGame, withdrawMatchBet, now, children } = this.props
    const isActive = match.time > now || match.ID === activeGame

    const winner = match.winners[0]
    const names = match.name.split(winner)
    const competition = match.competition ? `${match.competition} - ` : ''

    return (
      <li className={isActive ? `c-pointer${match.ID === activeGame ? ' active' : ''}` : 'disabled'}>
        {withdrawMatchBet === match.ID ? (
          <img
            className='ajax-placeholder m-top10 m-bottom10'
            src='/images/v2/main/ajax-loader.gif'
            style={{ paddingTop: 2 }}
          />
        ) : (
          <Link to={`${alias}/${activeGame === match.ID ? '' : match.ID}`}>
            <ul className={`pop-game clearfix ${isActive ? 't-gray-3' : 't-gray-9'}`}>
              <li className='game' title={competition + sportNameByAlias(match.name)}>
                <i className={`gm-${match.alias.split('-').join('')}-icon`} />
              </li>
              <li className='team t-overflow'>
                {!match.bets || <UserBets match={match} />}
                <div className={`name t-overflow bets-num-${this.getAmountOfBets(match.bets)}`}>
                  <span className='competition'>{competition}</span>
                  {names.length === 1 ?
                    names.join('') :
                    names.map((name, index) => (
                        <span key={index}>
                          {name === '' ? (
                            <span className='t-winner-bold'>{winner}</span>
                          ) : (
                            <span key={index}>{name}</span>
                          )}
                        </span>
                    ))}
                </div>
              </li>
              <li className='state-wrap'>{this._getMatchState(match)}</li>
            </ul>
          </Link>
        )}
        <div className='info-wrap' style={{ display: match.ID === activeGame ? 'block' : 'none' }}>
          {(() => {
            if (match.ID === activeGame) {
              return children
            }
          })()}
        </div>
      </li>
    )
  }
}

Game.propTypes = {
  match: PropTypes.object,
  alias: PropTypes.string,
  now: PropTypes.number,
  activeGame: PropTypes.string,
  activeGameBox: PropTypes.string,
  withdrawBet: PropTypes.func,
  withdrawMatchBet: PropTypes.string,
  children: PropTypes.element
}

export default Game
