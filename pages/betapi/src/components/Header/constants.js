export const DEFAULT = {
  titles: {
    default: {
      ID: 0,
      title: 'Bookie'
    }
  },
  tutorials: {
    activateOnLoad: true,
    hideByDefault: true,
    default: {
      ID: 0,
      items: [
        {
          title: 'Bookie tutorial',
          text:
            'Here you can bet on real-world sports matches. Simply select a match and place your bet. Winnings will usually be credited to you within 24 hours of the match ending. Winnings or returned money are sent to you in the form of a cashiers check which can be accessed from your bank.'
        }
      ]
    }
  }
}

export const FULL = {
  links: {
    default: {
      ID: 0,
      items: [
        {
          title: 'Statistics',
          href: '#/stats/football',
          label: 'statistics',
          icon: 'Statistics'
        },
        {
          title: 'Back to Casino',
          href: 'casino.php',
          label: 'back-to-casino',
          icon: 'Back'
        }
      ]
    }
  }
}

export const BACK = {
  links: {
    default: {
      ID: 0,
      items: [
        {
          title: 'Back to Bookie',
          href: '#/',
          label: 'back-to-bookie',
          icon: 'Back'
        }
      ]
    }
  }
}
