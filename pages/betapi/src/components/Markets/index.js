import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Odds } from '../../components'
import { selectBet, betOnGame, loadGameData } from '../../actions'
import { finishTimestamp, dateTimeFormat } from '../../utils'
import { TIME_FORMAT_NAME } from '../../constants'

class Markets extends Component {
  componentDidMount() {
    this.loadGameData()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.params.activeGameID === this.props.params.activeGameID) {
      return false
    }
    this.loadGameData()
  }

  loadGameData() {
    this.props.loadGameData(this.props.params.activeGameBox, this.props.params.activeGameID)
  }

  render() {
    const {
      match,
      now,
      userData,
      betLoading,
      betOnGame,
      selectBet,
      activeBet,
      pageMode,
      params,
      loadGameData
    } = this.props

    const activeGameBox = params.activeGameBox
    const activeGame = params.activeGameID

    return match.markets === undefined ? (
      <img className="ajax-placeholder m-top15 m-bottom10" src="/images/v2/main/ajax-loader.gif" />
    ) : match.markets.msg || match.markets.length === 0 ? (
      <ul className="bets-wrap clearfix">
        <li className="title">{match.markets.msg ? match.markets.msg : 'Markets for this match were removed'}</li>
      </ul>
    ) : (
      <ul className="bets-wrap clearfix">
        <li className="title">
          {match.name}
        </li>

        {!match.markets ||
          match.markets.length < 1 ||
          match.markets.map((market, i) => (
            <Odds
              isFirst={i === 0}
              key={market.marketId}
              market={market}
              match={match}
              now={now}
              userData={userData}
              betLoading={betLoading}
              betOnGame={betOnGame}
              selectBet={selectBet}
              loadGameData={loadGameData}
              activeBet={activeBet}
              pageMode={pageMode}
              params={params}
            />
          ))}
      </ul>
    )
  }
}

Markets.propTypes = {
  match: PropTypes.object,
  now: PropTypes.number,
  userData: PropTypes.object,
  betLoading: PropTypes.bool,
  betOnGame: PropTypes.func,
  selectBet: PropTypes.func,
  loadGameData: PropTypes.func,
  activeBet: PropTypes.string,
  pageMode: PropTypes.string,
  params: PropTypes.object
}

const mapStateToProps = (state, props) => ({
  match: state.app.gameBoxesList
    .find(item => item.alias === props.params.activeGameBox)
    .matches.find(match => match.ID === props.params.activeGameID),
  now: state.app.now,
  userData: state.app.userData,
  betLoading: state.app.betLoading,
  activeBet: state.app.activeBet,
  pageMode: state.app.pageMode
})

const mapActionsToProps = {
  selectBet,
  betOnGame,
  loadGameData
}

export default connect(mapStateToProps, mapActionsToProps)(Markets)
