import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { shortMoneyFormat } from '../../utils'

class UserBet extends Component {
  render() {
    const { bet, symbol, title, betState, withdraw } = this.props

    return (
      <li className={`stick ${betState} ${withdraw}`}>
        <div className="figure" />
        <span className="text" title={title}>
          {symbol}${betState === 'won' ? shortMoneyFormat(bet.moneyGain) : shortMoneyFormat(bet.amount)}
        </span>
      </li>
    )
  }
}

UserBet.propTypes = {
  bet: PropTypes.object,
  symbol: PropTypes.string,
  title: PropTypes.string,
  betState: PropTypes.string,
  withdraw: PropTypes.string
}

export default UserBet
