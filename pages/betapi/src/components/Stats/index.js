import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './styles/statistics.css'
import { Header } from '../../components'
import { TopStats, SportStats } from './components'
import { loadStatsData } from '../../actions'

class Stats extends Component {
  componentDidMount() {
    this.props.loadStatsData(this.props.params.activeGameBox)
  }

  render() {
    const { params, children, statsData, gameBoxesList, pageMode } = this.props
    return (
      <div>
        <Header isStats />
        {statsData ? (
          <div className='stats-main-wrap'>
            <TopStats topStats={statsData.topStats} pageMode={pageMode} />
            <hr className='page-head-delimiter m-top10' />
            <SportStats
              children={children}
              gameBoxesList={gameBoxesList}
              activeGameBox={params.activeGameBox}
              pageMode={pageMode}
            />
          </div>
        ) : (
          <img className='ajax-placeholder m-top10 m-bottom10' src='/images/v2/main/ajax-loader.gif' />
        )}
      </div>
    )
  }
}

Stats.propTypes = {
  params: PropTypes.object,
  children: PropTypes.element,
  statsData: PropTypes.object,
  gameBoxesList: PropTypes.array,
  loadStatsData: PropTypes.func,
  pageMode: PropTypes.string
}

const mapStateToProps = (state, props) => ({
  statsData: state.app.statsData,
  gameBoxesList: state.app.gameBoxesList,
  pageMode: state.app.pageMode
})

const mapActionsToProps = {
  loadStatsData
}

export default connect(mapStateToProps, mapActionsToProps)(Stats)
