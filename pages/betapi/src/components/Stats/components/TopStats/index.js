import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { $, numberFormat as nf } from '../../../../utils'

class TopStats extends Component {
  componentDidMount() {
    if (this.props.pageMode !== 'desktop') {
      $(this.refs.wrapper).tabs()
    }
  }

  // componentDidUpdate() {
  //   if (this.props.pageMode !== 'desktop') {
  //     $(this.refs.wrapper).tabs()
  //   } else {
  //     $(this.refs.wrapper).tabs('destroy')
  //   }
  // }

  render() {
    const {
      totalMatchInProgress,
      totalMatchAvailable,
      totalMatchCompleted,
      totalMatchCancelled,
      totalMoneyWon,
      totalMoneyLost,
      winLossRatio,
      totalMoneyRefunded,
      betsMade,
      betsWon,
      betsLost,
      betsCancelled,

      yourBetsMade,
      yourBetsWon,
      yourBetsLost,
      yourBetsCancelled,
      yourTotalMoneyWon,
      yourTotalMoneyLost,
      yourWinLossRatio,
      yourTotalMoneyRefunded,
      yourBiggestWon,
      yourBiggestLost
    } = this.props.topStats

    return (
      <div>
        <div className="stats-wrap m-top10" ref="wrapper">
          <div className="tabs-wrap d-hide">
            <ul className="tabs-title border-round bold">
              <li className="left-top-round">
                <a href="#overall-stats">Overall statistics</a>
                <div className="shadow-right" />
              </li>
              <li className="last right-top-round">
                <a href="#your-stats">Your statistics</a>
                <div className="shadow-left" />
              </li>
              <li className="clear" />
            </ul>
          </div>

          <div id="overall-stats" className="overall-stats-wrap left">
            <div className="title-black top-round">Overall Statistics</div>
            <ul className="cont-gray bottom-round">
              <li className="title white-grad t-hide">
                <ul className="item">
                  <li className="stat">Stat</li>
                  <li className="stat-value">Value</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Games in-progress<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(totalMatchInProgress)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Games available<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(totalMatchAvailable)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Games completed<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(totalMatchCompleted)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Games cancelled<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(totalMatchCancelled)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Total money won<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(totalMoneyWon)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Total money lost<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(totalMoneyLost)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Money Win/Loss ratio<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(winLossRatio)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Total money refunded<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(totalMoneyRefunded)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Bets made<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(betsMade)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Bets won<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(betsWon)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Bets lost<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(betsLost)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li className="last">
                <ul className="item">
                  <li className="stat">
                    Bets cancelled<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(betsCancelled)}</li>
                  <li className="clear" />
                </ul>
              </li>
            </ul>
          </div>
          <div id="your-stats" className="your-stats-wrap left">
            <div className="title-black top-round">Your Statistics</div>
            <ul className="cont-gray bottom-round">
              <li className="title white-grad t-hide">
                <ul className="item">
                  <li className="stat">Stat</li>
                  <li className="stat-value">Value</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Bets made<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(yourBetsMade)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Bets won<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(yourBetsWon)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Bets lost<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(yourBetsLost)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Bets refunded<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(yourBetsCancelled)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Total money won<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(yourTotalMoneyWon)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Total money lost<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(yourTotalMoneyLost)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Money won/lost ratio<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">{nf(yourWinLossRatio)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Total money refunded<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(yourTotalMoneyRefunded)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Biggest win<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(yourBiggestWon)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li>
                <ul className="item">
                  <li className="stat">
                    Biggest loss<span className="m-show">:</span>
                  </li>
                  <li className="stat-value">${nf(yourBiggestLost)}</li>
                  <li className="clear" />
                </ul>
              </li>
              <li className="">
                <ul className="item">
                  <li className="stat" />
                  <li className="stat-value" />
                  <li className="clear" />
                </ul>
              </li>
              <li className="last">
                <ul className="item">
                  <li className="stat" />
                  <li className="stat-value" />
                  <li className="clear" />
                </ul>
              </li>
            </ul>
          </div>
          <div className="clear" />
        </div>
      </div>
    )
  }
}

TopStats.propTypes = {
  topStats: PropTypes.object,
  pageMode: PropTypes.string
}

export default TopStats
