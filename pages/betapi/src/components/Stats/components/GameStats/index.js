import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { sportNameByAlias, dayDateFormat, shortMoneyFormat } from '../../../../utils'

class GameStats extends Component {
  getAmountOfBets(bets) {
    if (!bets) {
      return 0
    }
    let amount = 0
    const types = {}
    for (let i in bets) {
      if (!types[bets[i].state]) {
        amount++
        types[bets[i].state] = true
      }
    }
    return amount
  }

  render() {
    const { match } = this.props

    let winner = match.winners[0]
    let names = match.name.split(winner)

    const bets = []
    if (match.refunded > 0) {
      bets.push({
        state: 'refunded'
      })
    } else {
      bets.push({
        state: 'won'
      })
      bets.push({
        state: 'lost'
      })
    }

    const competition = match.competition ? match.competition + ' - ' : ''

    return (
      <li className={'disabled'}>
        <ul className={'pop-game clearfix t-gray-9'}>
          <li className="game" title={competition + sportNameByAlias(match.name)}>
            <i className={'gm-' + match.alias.split('-').join('') + '-icon'} />
          </li>
          <li className="team t-overflow">
            <ul className="stick-wrap right">
              {bets.map((bet, index) => (
                <li key={index} className={'stick ' + bet.state}>
                  <div className="figure" />
                  <span className="text">${shortMoneyFormat(match[bet.state] || 0)}</span>
                </li>
              ))}
            </ul>
            <div className={'name t-overflow bets-num-' + this.getAmountOfBets(bets)}>
              <span className="competition">{competition}</span>
              {names.length === 1
                ? names.join('')
                : names.map((name, index) => (
                    <span key={index}>
                      {name === '' ? <span className="t-winner-bold">{winner}</span> : <span key={index}>{name}</span>}
                    </span>
                  ))}
            </div>
          </li>
          <li className="state-wrap">{dayDateFormat(match.finishTimestamp)}</li>
        </ul>
      </li>
    )
  }
}

GameStats.propTypes = {
  match: PropTypes.object,
  alias: PropTypes.string,
  now: PropTypes.number,
  withdrawMatchBet: PropTypes.string
}

export default GameStats
