import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { GameStats } from '..'
import { loadGameBoxData, loadMoreGameBoxData } from '../../../../actions'
// import { sportNameByAlias } from '../../../../utils'
import { MIN_MATCHE_NUMBER, POPULAR_ALIAS } from '../../../../constants'

class GameBoxStats extends Component {
  scrollListener(e) {
    const { params } = this.props

    if (
      params.activeGameBox !== POPULAR_ALIAS
      && document.documentElement.scrollHeight === window.innerHeight + Math.round(window.scrollY)
      && !this.props.moreGamesLoading
      && this.props.sport.loadedMatchesAmount > 0
    ) {
      // const { currentPage, search, competitionBox } = this.props
      const { currentPage } = this.props

      this.props.loadMoreGameBoxData(params.activeGameBox, currentPage, '', '', true)
    }
  }

  componentDidMount() {
    this.scrollListener = this.scrollListener.bind(this)
    window.addEventListener('scroll', this.scrollListener, false)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollListener, false)
  }

  loadGameBoxData() {
    this.props.loadGameBoxData(this.props.params.activeGameBox, true)
  }

  selectMatches(sport, competitionBox, search) {
    return [...sport.matches]
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillUpdate(props) {
  componentDidUpdate(prevProps) {
    const {
      sport,
      params,
      moreGamesLoading,
      currentSearchPage,
      search,
      competitionBox,
      loadedMatchesAmountBySearch,
      loadMoreGameBoxData
    } = this.props

    if (!sport.matches) {
      if (prevProps.params.activeGameBox === this.props.params.activeGameBox) {
        return false
      }
      this.loadGameBoxData()
      return
    }
    const sportMaches = this.selectMatches(sport, competitionBox, search)
    const loadedMatchesAmount = loadedMatchesAmountBySearch[`${competitionBox}_${search}`]

    if (
      sportMaches.length < MIN_MATCHE_NUMBER
      && !moreGamesLoading
      && (loadedMatchesAmount === undefined || loadedMatchesAmount > 0)
      && params.activeGameBox !== POPULAR_ALIAS
    ) {
      loadMoreGameBoxData(params.activeGameBox, currentSearchPage, search, competitionBox, true)
    }
  }

  render() {
    const { sport, moreGamesLoading, competitionBox, search, params } = this.props

    if (!sport.matches) {
      return <img className='ajax-placeholder m-top15 m-bottom10' src='/images/v2/main/ajax-loader.gif' />
    }

    const matches = this.selectMatches(sport, competitionBox, search)

    if (moreGamesLoading) {
      matches[matches.length - 1] = {
        loadingRow: true
      }
    }

    return (
      <div id={sport.alias}>
        <div className='date-tabs-wrap'>
          <div className='bookie-date-boxes'>
            <ul className='pop-list'>
              {matches.length > 0 ? (
                matches.map(match => match.loadingRow ? (
                    <li key={-1}>
                      <img
                        className='ajax-placeholder m-top10 m-bottom10'
                        src='/images/v2/main/ajax-loader.gif'
                        style={{ paddingTop: 2 }}
                      />
                    </li>
                  ) : match.title ? (
                    <li key={match.key} className={match.className} style={{ cursor: 'auto' }}>
                      <ul className='pop-game clearfix text-a-center'>
                        <li className='team t-overflow' style={{ width: '100%', padding: '0 0 0 0' }}>
                          <span>{match.title}</span>
                        </li>
                      </ul>
                    </li>
                  ) : (
                    <GameStats key={match.ID} match={match} />
                  ))
              ) : (
                <li>
                  <ul className='pop-game clearfix text-a-center'>
                    <li className='team t-overflow' style={{ width: '100%', padding: '0 0 0 0' }}>
                      {moreGamesLoading ? (
                        <img src='/images/v2/main/ajax-loader.gif' />
                      ) : params.activeGameBox === 'your-bets' && !search && !competitionBox ? (
                        <span>You haven't placed any bets yet</span>
                      ) : (
                        <span>No match found</span>
                      )}
                    </li>
                  </ul>
                </li>
              )}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

GameBoxStats.propTypes = {
  loadGameBoxData: PropTypes.func,
  loadMoreGameBoxData: PropTypes.func,
  sport: PropTypes.object,
  currentPage: PropTypes.number,
  currentSearchPage: PropTypes.number,
  moreGamesLoading: PropTypes.bool,
  competitionBox: PropTypes.string,
  loadedMatchesAmountBySearch: PropTypes.object,
  search: PropTypes.string,
  params: PropTypes.object
}

const mapStateToProps = (state, props) => ({
  sport: state.app.gameBoxesList.find(item => item.alias === props.params.activeGameBox),
  currentPage: state.app.currentPage,
  currentSearchPage: state.app.currentSearchPage,
  moreGamesLoading: state.app.moreGamesLoading,
  competitionBox: state.app.competitionBox,
  loadedMatchesAmountBySearch: state.app.loadedMatchesAmountBySearch,
  search: state.app.search
})

const mapActionsToProps = {
  loadGameBoxData,
  loadMoreGameBoxData
}

export default connect(mapStateToProps, mapActionsToProps)(GameBoxStats)
