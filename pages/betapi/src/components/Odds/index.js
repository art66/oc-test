import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Bet } from '../../components'
import { finishTimestamp, dateTimeFormat } from '../../utils'
import { TIME_FORMAT_NAME } from '../../constants'

class Odds extends Component {

  render() {
    const {
      match,
      market,
      now,
      isFirst,
      userData,
      betLoading,
      betOnGame,
      selectBet,
      activeBet,
      pageMode,
      params
    } = this.props

    const activeGameBox = params.activeGameBox
    const activeGame = params.activeGameID

    let bpp = 0;
    if (market.odds && Array.isArray(market.odds)) {
      market.odds.map(odd => {
        bpp += 1/odd.lastPriceTraded
      })
    }

    return  market.odds.msg || market.odds.length === 0 ? (
      <ul className="bets-wrap clearfix">
        <li className="title">{market.odds.msg ? market.odds.msg : 'Odds for this market were removed'}</li>
      </ul>
    ) : (
      <ul className="bets-wrap clearfix">
        <li className="bets">
          <div className={'bet market-name-cell' + (isFirst ? ' first' : '')}>
            <span className="bold">{market.marketName}</span>{' market '}
            {(() => {
              if (market.time > now) {
                return (
                  <span>
                    {' '}
                    is due to start at
                    <span className="bold">
                      {' '}
                      {dateTimeFormat(market.time)} {TIME_FORMAT_NAME}
                    </span>
                  </span>
                )
              } else if (market.isFinished === 1) {
                return (
                  <span>
                    finished at{' '}
                    <span className="bold">
                      {finishTimestamp(market.finishTimestamp)} {TIME_FORMAT_NAME}
                    </span>
                  </span>
                )
              } else {
                return (
                  <span>
                    is in progress and started at{' '}
                    <span className="bold">
                      {dateTimeFormat(market.time)} {TIME_FORMAT_NAME}
                    </span>
                  </span>
                )
              }
            })()}
          </div>
        </li>

        {!market.odds ||
          market.odds.length < 1 ||
          market.odds.map((odd, i) => (
            <Bet
              key={i}
              match={match}
              odd={odd}
              now={now}
              userData={userData}
              betLoading={betLoading}
              betOnGame={betOnGame}
              activeGameBox={activeGameBox}
              activeGame={activeGame}
              selectBet={selectBet}
              activeBet={activeBet}
              pageMode={pageMode}
            />
          ))}
      </ul>
    )
  }
}

Odds.propTypes = {
  match: PropTypes.object,
  market: PropTypes.object,
  isFirst: PropTypes.bool,
  now: PropTypes.number,
  userData: PropTypes.object,
  betLoading: PropTypes.bool,
  activeBet: PropTypes.string,
  pageMode: PropTypes.string,
  params: PropTypes.object
}

export default Odds
