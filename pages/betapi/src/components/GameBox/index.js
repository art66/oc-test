import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Game, SearchBox } from '../../components'
import {
  loadGameBoxData,
  loadMoreGameBoxData,
  withdrawBet,
  setCompetitionBox,
  setSearch,
  showMoreLiveMatches
} from '../../actions'
import { sportNameByAlias } from '../../utils'
import { MIN_SEARCH_LENGTH, MIN_MATCHE_NUMBER, POPULAR_ALIAS, YOUR_BETS_ALIAS } from '../../constants'

class GameBox extends Component {
  scrollListener(e) {
    const { params } = this.props
    if (
      // params.activeGameBox !== POPULAR_ALIAS &&
      document.documentElement.scrollHeight === window.innerHeight + Math.round(window.scrollY) &&
      !this.props.moreGamesLoading &&
      this.props.sport.loadedMatchesAmount > 0
    ) {
      const { currentPage, search, competitionBox } = this.props
      this.props.loadMoreGameBoxData(params.activeGameBox, currentPage, search, competitionBox)
    }
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  //   const { sport, route } = this.props
  //   const isYourBets = sport.alias === 'your-bets'
  //   const noMatch = sport.matchAmount === 0
  //   if (isYourBets && noMatch) {
  //     route.history.push('/popular')
  //   }
  // }

  componentDidMount() {
    const { sport, route } = this.props
    const isYourBets = sport.alias === 'your-bets'
    const noMatch = sport.matchAmount === 0

    if (isYourBets && noMatch) {
      route.history.push('/popular')
    }

    // this.loadGameBoxData()
    this.scrollListener = this.scrollListener.bind(this)
    window.addEventListener('scroll', this.scrollListener, false)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollListener, false)
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillUpdate(props) {
    componentDidUpdate(prevProps) {
      const {
        sport,
        params,
        moreGamesLoading,
        currentSearchPage,
        search,
        competitionBox,
        loadedMatchesAmountBySearch,
        loadMoreGameBoxData
      } = this.props

      if (prevProps.params.activeGameBox !== params.activeGameBox) {
        this.loadGameBoxData()
      }

      if (!sport.matches) {
        return
      }

      const sportMaches = this.selectMatches(sport, competitionBox, search)
      const loadedMatchesAmount = loadedMatchesAmountBySearch[`${competitionBox}_${search}`]

      if (
        sportMaches.length < MIN_MATCHE_NUMBER &&
        !moreGamesLoading &&
        (loadedMatchesAmount === undefined || loadedMatchesAmount > 0)
        // && params.activeGameBox !== POPULAR_ALIAS
      ) {
        loadMoreGameBoxData(params.activeGameBox, currentSearchPage, search, competitionBox)
      }
    }

  loadGameBoxData() {
    this.props.loadGameBoxData(this.props.params.activeGameBox)
  }

  _checkBetsResult(match, competitionBox) {
    if (this.props.params.activeGameBox === YOUR_BETS_ALIAS && match.bets) {
      competitionBox = competitionBox.toLocaleLowerCase()
      for (let i in match.bets) {
        if (match.bets[i].result == competitionBox) {
          return true
        }
      }
      return false
    }
    return false
  }

  selectMatches(sport, competitionBox, search) {
    const sportMatches = []
    for (let i = 0; i < sport.matches.length; i++) {
      const match = sport.matches[i]
      const matchResult = this._checkBetsResult(match, competitionBox)
      if (
        !competitionBox ||
        match.competition === competitionBox ||
        match.countryCode === competitionBox ||
        matchResult
      ) {
        if (
          search.length < MIN_SEARCH_LENGTH ||
          match.competition.toLocaleLowerCase().indexOf(search) > -1 ||
          match.name.toLocaleLowerCase().indexOf(search) > -1 ||
          matchResult
        ) {
            sportMatches.push(match)
        }
      }
    }

    return sportMatches
  }

  render() {
    const {
      sport,
      now,
      moreGamesLoading,
      competitionBox,
      search,
      chosenShowMore,
      params,
      withdrawBet,
      withdrawMatchBet,
      setCompetitionBox,
      setSearch,
      showMoreLiveMatches,
      children
    } = this.props

    if (!sport.matches) {
      return <img className="ajax-placeholder m-top15 m-bottom10" src="/images/v2/main/ajax-loader.gif" />
    }

    const sportMatches = this.selectMatches(sport, competitionBox, search)
    const currentMatches = sportMatches.filter(match => match.time < now && !match.isFinished)
    const finishedMatches = sportMatches.filter(match => match.time < now && match.isFinished)
    const futureMatches = sportMatches.filter(match => match.time > now)
    const sportAlias = sportNameByAlias(sport.alias).toUpperCase()
    const noMatches = currentMatches.length === 0 ? 'NO ' : ''
    const isYourBets = sport.alias === 'your-bets'
    let matches = []

    if (params.activeGameBox == POPULAR_ALIAS) {
      futureMatches.sort((a, b) => (a.time < b.time ? -1 : 1))
    }

    const matchName = sport.matchName ? sport.matchName.toUpperCase() : `${sportAlias} MATCHES`

    const isActiveGame = currentMatches.slice(5).filter(match => match.ID === params.activeGameID).length > 0

    if (currentMatches.length > 0) {
      matches = matches.concat([
        {
          title: isYourBets ? `YOUR LIVE BETS` : `${noMatches} LIVE ${matchName}`,
          key: 0,
          className: 'live title'
        },
        ...(chosenShowMore || isActiveGame ? currentMatches : currentMatches.slice(0, 5))
      ])
    }

    if (!chosenShowMore && currentMatches.length > 5 && !isActiveGame) {
      matches.push({ title: `Show more`, key: 'show-more', className: 'show-more', onClick: showMoreLiveMatches })
    }

    if (futureMatches.length > 0) {
      matches = matches.concat([
        {
          title: isYourBets ? `YOUR UPCOMING BETS` : `UPCOMING ${matchName}`,
          key: 1,
          className: 'upcomming title'
        },
        ...futureMatches
      ])
    }

    if (finishedMatches.length > 0) {
      matches = matches.concat([
        {
          title: isYourBets ? `YOUR COMPLETED BETS` : `COMPLETED ${matchName}`,
          key: 2,
          className: 'completed title'
        },
        ...finishedMatches
      ])
    }

    if (moreGamesLoading) {
      matches[matches.length - 1] = {
        loadingRow: true
      }
    }

    return (
      <div id={sport.alias}>
        <div className="date-tabs-wrap">
          <div className="bookie-date-boxes">
            <SearchBox
              competitions={sport.competitions}
              competitionBox={competitionBox}
              setCompetitionBox={setCompetitionBox}
              setSearch={setSearch}
            />
            <ul className="pop-list">
              {matches.length > 0 ? (
                matches.map(
                  match =>
                    match.loadingRow ? (
                      <li key={-1}>
                        <img
                          className="ajax-placeholder m-top10 m-bottom10"
                          src="/images/v2/main/ajax-loader.gif"
                          style={{ paddingTop: 2 }}
                        />
                      </li>
                    ) : match.title ? (
                      <li key={match.key} className={match.className} onClick={match.onClick}>
                        <ul className="pop-game clearfix text-a-center">
                          <li className="team t-overflow matches-category-header" style={{ width: '100%', padding: '0 0 0 0' }}>
                            <span>{match.title}</span>
                          </li>
                        </ul>
                      </li>
                    ) : (
                      <Game
                        key={match.ID}
                        match={match}
                        alias={sport.alias}
                        activeGameBox={params.activeGameBox}
                        now={now}
                        activeGame={params.activeGameID}
                        withdrawBet={withdrawBet}
                        withdrawMatchBet={withdrawMatchBet}
                        children={children}
                      />
                    )
                )
              ) : (
                <li>
                  <ul className="pop-game clearfix text-a-center">
                    <li className="team t-overflow" style={{ width: '100%', padding: '0 0 0 0' }}>
                      {moreGamesLoading ? (
                        <img src="/images/v2/main/ajax-loader.gif" />
                      ) : params.activeGameBox === 'your-bets' && !search && !competitionBox ? (
                        <span>{"You haven't placed any bets yet"}</span>
                      ) : (
                        <span>No match found</span>
                      )}
                    </li>
                  </ul>
                </li>
              )}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

GameBox.propTypes = {
  loadGameBoxData: PropTypes.func,
  loadMoreGameBoxData: PropTypes.func,
  withdrawBet: PropTypes.func,
  setCompetitionBox: PropTypes.func,
  setSearch: PropTypes.func,
  showMoreLiveMatches: PropTypes.func,
  sport: PropTypes.object,
  now: PropTypes.number,
  currentPage: PropTypes.number,
  currentSearchPage: PropTypes.number,
  moreGamesLoading: PropTypes.bool,
  withdrawMatchBet: PropTypes.string,
  competitionBox: PropTypes.string,
  loadedMatchesAmountBySearch: PropTypes.object,
  search: PropTypes.string,
  chosenShowMore: PropTypes.bool,
  params: PropTypes.object,
  route: PropTypes.object,
  children: PropTypes.element
}

const mapStateToProps = (state, props) => ({
  sport: state.app.gameBoxesList.find(item => item.alias === props.params.activeGameBox),
  currentPage: state.app.currentPage,
  currentSearchPage: state.app.currentSearchPage,
  now: state.app.now,
  withdrawMatchBet: state.app.withdrawMatchBet,
  moreGamesLoading: state.app.moreGamesLoading,
  competitionBox: state.app.competitionBox,
  loadedMatchesAmountBySearch: state.app.loadedMatchesAmountBySearch,
  search: state.app.search,
  chosenShowMore: state.app.chosenShowMore
})

const mapActionsToProps = {
  loadGameBoxData,
  loadMoreGameBoxData,
  withdrawBet,
  setCompetitionBox,
  setSearch,
  showMoreLiveMatches
}

export default connect(mapStateToProps, mapActionsToProps)(GameBox)
