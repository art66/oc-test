import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Header, Sports } from '..'
import '../../styles/bookie_popular_matches.scss'

import { getInitialData, updateNow, withdrawAll } from '../../actions'

class Main extends Component {
  constructor(props) {
    super(props)
    props.getInitialData(props.params.activeGameBox)
  }

  render() {
    const {
      gameBoxesList,
      userData,
      pageMode,
      gotContent,
      gameBoxLoading,
      updateNow,
      withdrawAll,
      children,
      params,
      errorMsg
    } = this.props

    const activeGameBox = params.activeGameBox || 'favourites'

    return (
      <div>
        <Header />
        <div>
          {!gotContent ? (
            <img className='ajax-placeholder m-top10' src='/images/v2/main/ajax-loader.gif' />
          ) : errorMsg ? (
            <div>
              <div className='info-msg-cont red border-round m-top10'>
                <div className='info-msg border-round'>
                  <i className='info-icon' />
                  <div className='delimiter'>
                    <div className='msg right-round'>
                      <span>{errorMsg}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <Sports
              children={children}
              gameBoxesList={gameBoxesList}
              userData={userData}
              pageMode={pageMode}
              activeGameBox={activeGameBox}
              activeGameID={params.activeGameID}
              gameBoxLoading={gameBoxLoading}
              updateNow={updateNow}
              withdrawAll={withdrawAll}
            />
          )}
        </div>
      </div>
    )
  }
}

Main.propTypes = {
  params: PropTypes.object,
  getInitialData: PropTypes.func,
  withdrawAll: PropTypes.func,
  gameBoxesList: PropTypes.array,
  userData: PropTypes.object,
  pageMode: PropTypes.string,
  gotContent: PropTypes.bool,
  gameBoxLoading: PropTypes.bool,
  errorMsg: PropTypes.string,
  updateNow: PropTypes.func,
  children: PropTypes.element
}

const mapStateToProps = state => ({
  gameBoxesList: state.app.gameBoxesList,
  gotContent: state.app.gotContent,
  gameBoxLoading: state.app.gameBoxLoading,
  userData: state.app.userData,
  pageMode: state.app.pageMode,
  errorMsg: state.app.errorMsg
})

const mapActionsToProps = {
  getInitialData,
  updateNow,
  withdrawAll
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Main)
