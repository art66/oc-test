import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { UserBet } from '../../components'
import { betTitle } from '../../utils'

const types = ['pending', 'won', 'refunded', 'lost']

class UserBets extends Component {
  groupByResult(bets) {
    const response = []
    types.map(() => response.push([]))
    bets.map(bet => response[types.indexOf(bet.result)].push(bet))
    return response
  }

  render() {
    const { match } = this.props

    const results = this.groupByResult(match.bets)
    const groupBets = []
    for (let i in results) {
      const result = results[i]
      if (result.length === 0) {
        continue
      }
      const item = {
        moneyGain: 0,
        amount: 0,
        titles: []
      }
      for (let j in result) {
        const bet = result[j]
        const { symbol, title, betState, withdraw } = betTitle(bet)
        item.symbol = symbol
        item.betState = betState
        item.withdraw = withdraw
        item.titles.push(title)
        item.amount += bet.amount
        item.moneyGain += bet.moneyGain
      }
      groupBets.push(item)
    }

    return (
      <ul className="stick-wrap right">
        {groupBets.map((bet, index) => {
          return (
            <UserBet
              bet={bet}
              key={index}
              symbol={bet.symbol}
              title={bet.titles.join('<br>')}
              betState={bet.betState}
              withdraw={bet.withdraw}
            />
          )
        })}
      </ul>
    )
  }
}

UserBets.propTypes = {
  match: PropTypes.object
}

export default UserBets
