import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { UserBet } from '..'
import { $, decimalToFractionalOdd, numberFormat, decimalOdd, betTitle } from '../../utils'

const MAX_BET_LIMIT = 10000000

class Bet extends Component {
  constructor(props) {
    super(props)

    this._betRef = React.createRef()
    this._betButtonRef = React.createRef()

    this.state = {
      bet: false,
      blocked: true,
      betValue: 0,
      activeMessage: ''
    }
  }

  componentDidMount() {
    const { _betRef, _betButtonRef } = this
    const $bet = $(_betRef.current)

    $bet.tornInputMoney({
      buttonElement: $(_betButtonRef.current),
      buttonDisabledClass: 'disabled',
      strictMode: false
    })

    $bet
      .parents('.bet-cell.input-wrap')
      .find('.input-money-symbol')
      .click(() => {
        this.onChange({
          target: _betRef.current
        })
      })
  }

  onConfirm(activeGameBox, activeGame, marketId, selectionId, amount, lastPriceTraded, handicap) {
    this.props.betOnGame(activeGameBox, activeGame, marketId, selectionId, amount, lastPriceTraded, handicap)
    this.setState({
      activeMessage: 'response'
    })
  }

  onReject() {
    this.props.selectBet({})
    this.setState({
      blocked: false,
      activeMessage: 'confirm'
    })
  }

  onBet() {
    if (this.state.blocked) {
      return
    }

    this.setState({
      bet: true
    })

    this.props.selectBet(this.props.odd)
  }

  _getBetValue(inputEl) {
    const value = parseInt(inputEl.value.split(',').join(''))
    const isValue = !(isNaN(value) || value <= 0)

    return {
      value,
      isValue
    }
  }

  onChange(event) {
    const { value, isValue } = this._getBetValue(event.target)

    this.setState({
      bet: false,
      blocked: !isValue,
      betValue: isValue ? value : 0
    })
  }

  _onBlurFocus(target) {
    setTimeout(() => {
      const { isValue } = this._getBetValue(target)

      this.setState({
        blocked:
          $(target)
            .parents('.input-money-group')
            .hasClass('error') || !isValue
      })
    }, 0)
  }

  onKeyPress(event) {
    if (event.charCode === 13) {
      this.onBet()
    }
  }

  _getAmount() {
    return this.state.betValue
  }

  _getBetStrLength() {
    return (numberFormat(this._getAmount()) + this.props.odd.name).length
  }

  _getConfirmMsg(odd) {
    return (
      <span>
        Are you sure you want to bet <span className='bold'> ${numberFormat(this._getAmount())} </span>
        on <span className='bold'>{odd.name}</span> to win? Your potential winnings are
        <span className='bold'>{` $${numberFormat(Math.round(this._getAmount() * (odd.lastPriceTraded - 1)))}`}</span>.
      </span>
    )
  }

  _getShortConfirmMsg(odd) {
    return (
      <span>
        Are you sure you want to bet <span className='bold'> ${numberFormat(this._getAmount())} </span>
        on{' '}
        <span className='bold' title={odd.name}>
          {odd.name.toString().substr(0, 6)}...{' '}
        </span>
        to win? Your potential winnings are
        <span className='bold'>{` $${numberFormat(Math.round(this._getAmount() * (odd.lastPriceTraded - 1)))}`}</span>.
      </span>
    )
  }

  _getMoneyLimit(money) {
    if (money === undefined) {
      return money
    }

    return Math.min(money, MAX_BET_LIMIT)
  }

  render() {
    const { match, odd, now, userData, betLoading, activeGameBox, activeGame, activeBet, pageMode } = this.props
    const { blocked, activeMessage } = this.state
    const isActive = activeBet === `${odd.marketId}-${odd.selectionId}`
    const moneyLimit = this._getMoneyLimit(userData.money)

    return (
      <li className={`bets${odd.status === 'WINNER' ? ' bg-green ' : ''}`}>
        <div className='bet clearfix'>
          <div className='cells-wrap left clearfix'>
            <div
              className={`bet-cell result${match.time < now ? ' closed' : ''}`}
              style={{
                display: isActive ? 'none' : 'block'
              }}
            >
              {odd.lastPriceTraded > 1 || match.time < now
                ? odd.name === 'The Draw'
                  ? 'Draw'
                  : odd.name + (odd.handicap ? ` (${odd.handicap > 0 ? '+' : ''}${odd.handicap})` : '')
                : 'Odds are currently unavailable for this bet'}
              {!match.bets || (
                <ul className='stick-wrap right'>
                  {match.bets.map((bet, index) => {
                    if (bet.selectionId === odd.selectionId && bet.marketId === odd.marketId) {
                      const { symbol, title, betState } = betTitle(bet)

                      return <UserBet bet={bet} key={index} symbol={symbol} title={title} betState={betState} />
                    }
                  })}
                </ul>
              )}
            </div>
            <div className='bet-cell odds fractional'>
              <span className='label bold t-show'>Odds:</span>{' '}
              {odd.numerator && odd.denominator
                ? `${odd.numerator}/${odd.denominator}`
                : decimalToFractionalOdd(odd.lastPriceTraded)}
            </div>
            <div className='bet-cell odds'>
              <span className='label bold t-show'>Multiplier:</span> x{decimalOdd(odd.lastPriceTraded)}
            </div>
          </div>
          {(!isActive && !activeBet) || (
            <div
              className='confirm-wrap'
              style={{
                display: isActive ? 'block' : 'none'
              }}
            >
              {betLoading ? (
                <img
                  className='ajax-placeholder left m-bottom10'
                  style={{ marginTop: 12 }}
                  src='/images/v2/main/ajax-loader.gif'
                />
              ) : odd.betResponse && activeMessage === 'response' ? (
                <div className={`bold ${odd.betResponse.success ? 't-green' : 't-red'}`}>
                  {odd.betResponse.msg}
                  {odd.betResponse.askToBet ? (
                    <span
                      className='t-blue bet-no bold m-left10 c-pointer'
                      onClick={() => {
                        this.onConfirm(
                          activeGameBox,
                          activeGame,
                          odd.marketId,
                          odd.selectionId,
                          this._getAmount(),
                          odd.lastPriceTraded,
                          odd.handicap
                        )
                      }}
                    >
                      {' '}
                      [Place bet anyway]
                    </span>
                  ) : (
                    <span
                      className='t-blue bet-no bold m-left10 c-pointer'
                      onClick={() => {
                        this.onReject()
                      }}
                    >
                      {' '}
                      Ok
                    </span>
                  )}
                </div>
              ) : (
                <div>
                  {pageMode === 'desktop' && this._getBetStrLength() >= 20 ? (
                    <div className='t-overflow left' style={{ maxWidth: 550 }}>
                      {this._getShortConfirmMsg(odd)}
                    </div>
                  ) : (
                    this._getConfirmMsg(odd)
                  )}
                  <span
                    className={`link-wrap${pageMode === 'desktop' && this._getBetStrLength() >= 20 ? ' left' : ''}`}
                  >
                    <span
                      className='t-blue bet-yes bold m-left10 c-pointer'
                      onClick={() => {
                        this.onConfirm(
                          activeGameBox,
                          activeGame,
                          odd.marketId,
                          odd.selectionId,
                          this._getAmount(),
                          odd.lastPriceTraded,
                          odd.handicap
                        )
                      }}
                    >
                      {' '}
                      Yes
                    </span>
                    <span
                      className='t-blue bet-no bold m-left10 c-pointer'
                      onClick={() => {
                        this.onReject()
                      }}
                    >
                      {' '}
                      No
                    </span>
                  </span>
                </div>
              )}
            </div>
          )}
          {match.time < now || odd.lastPriceTraded <= 1 || (
            <div
              className='bet-cell input-wrap'
              style={{
                display: isActive ? 'none' : 'block'
              }}
            >
              <input
                type='text'
                className='amount'
                name='amount'
                ref={this._betRef}
                data-money={moneyLimit}
                data-limit={moneyLimit}
                size='10'
                autoComplete='off'
                maxLength='15'
                disabled={match.time < now}
                onKeyUp={e => this.onChange(e)}
                onChange={e => this.onChange(e)}
                onKeyPress={e => this.onKeyPress(e)}
                onBlur={e => this._onBlurFocus(e.target)}
                onFocus={e => this._onBlurFocus(e.target)}
              />
              <button
                className={`input-btn${blocked ? ' disabled' : ''}`}
                ref={this._betButtonRef}
                style={{
                  marginLeft: 0,
                  height: 24,
                  padding: '4px 8px'
                }}
                onClick={() => this.onBet()}
              >
                BET
              </button>
            </div>
          )}
        </div>
      </li>
    )
  }
}

Bet.propTypes = {
  match: PropTypes.object,
  odd: PropTypes.object,
  now: PropTypes.number,
  userData: PropTypes.object,
  activeGameBox: PropTypes.string,
  activeGame: PropTypes.string,
  betLoading: PropTypes.bool,
  betOnGame: PropTypes.func,
  selectBet: PropTypes.func,
  activeBet: PropTypes.string,
  pageMode: PropTypes.string
}

export default Bet
