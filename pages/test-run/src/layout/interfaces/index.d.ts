import { match, RouteComponentProps } from 'react-router'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

import { IInitialState } from '../../interfaces/IStore'

export interface IProps extends RouteComponentProps, IInitialState {
  checkManualMode: (status: boolean) => void
  infoBoxShow: (msg: string) => void
  mediaType: TMediaType
  children: any
  match: match<{ myParam: string }>
}

export interface IContainerStore {
  common: IInitialState
  browser: any
}
