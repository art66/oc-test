import React, { useEffect, useRef } from 'react'
import styled from 'styled-components'
import { Map } from 'maptalks'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`

const ScenarioFloorplan = ({ layers = []}) => {
  const mapRef = useRef(null)
  
  const actionLayerRef = useRef(null)

  useEffect(() => {
    if (!mapRef.current) {
      const map = new Map('map', {
        center: [0, 0],
        zoom: 3.5,
        draggable: false,
        touchZoom: false,
        doubleClickZoom: false,
        scrollWheelZoom: false,
      })

      layers.forEach(layer => {
        map.addLayer(layer)
      })

      mapRef.current = map
    }
  }, [ mapRef.current, layers ])

  return (
    <Wrapper id='map' style={{ width: '100%', height: '100%' }} />
  )
}

export default ScenarioFloorplan
