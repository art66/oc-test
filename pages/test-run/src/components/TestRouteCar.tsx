import React, { useEffect, useRef } from "react";
import styled from "styled-components";
// import { RoutePlayer } from "../lib/test_lib/routePlayer";
// import { route, route2, route3, route4, route5,route6 } from "../lib/test_lib/route";
// import {
//   elementInMap,
//   // createTextMarker,
//   // optionsTooltip
// } from "../lib/test_lib/options";
import {
  Map,
  ImageLayer,
  MultiLineString,
  VectorLayer,
  Marker,
  animation
} from "maptalks";

import "./tooltip.css";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const path = [
  [-19.401499713892008, -0.43326711138107044, ],
  [-20.354002053760837, 0.09060500848022457, ],
  [-21.068378808662487, 0.37635304178789397, ],
  [-22.116131382518233, 0.9954321883134014],
  [-23.02100860539349, 1.4239617114192527],
  [-24.068761179249236, 1.8524115780267891],
  [-25.068888636111524, 2.042801910366933],
  [-26.164266326960615, 2.09039604993157],
  [-27.069143549835985, 1.9476094334000038],
  [-27.783520304737635, 1.5191805199866337],
  [-28.11689612369173, 0.9001949977669312],
  [-28.59314729362609, 0.04297992521836136],
  [-28.732336329081818, -0.7118354080430151],
  [-28.915091119792635, -1.6025743273282842],
  [-28.960779817470325, -2.378807481537052],
  [-29.006468515148015, -2.9036674881704414],
  [-28.93793546863151, -3.565094626207383],
  [-28.93793546863151, -4.476609706280755],
  [-28.869402422114945, -5.5916470678195935],
  [-28.732336329081818, -6.4549315592340974],
  [-28.38967109649917, -7.294088049962141],
  [-28.184071956949538, -7.54327218344136],
  [-27.635807584817258, -8.131674374546918],
  [-26.927632770813034, -8.877243177232003],
  [-26.173769259131063, -9.44106786237441],
  [-25.33185033262214, -9.852281380349126],
  [-24.311507752788884, -10.156774114472512],
  [-23.414843061420243, -10.43057018423832],
  [-22.54909784216784, -10.795256239046665],
  [-21.80703051138005, -11.038135563817917],
  [-21.219560541172996, -11.311134727045186],
  [-20.168298489223616, -11.674728244578858],
  [-19.364392214203463, -12.128548936821261],
  [-18.313130162254083, -12.521237389232141],
  [-17.509223887234043, -12.943464719316779],
  [-16.519800779516913, -13.244619848108215],
  [-15.65405556026451, -13.635564042017137],
  [-14.819229813128231, -13.905840301528968],
  [-13.737048289062614, -14.535249083985377],
  [-13.211417263087924, -14.714754917872568],
  [-12.34567204383552, -15.1330251460937],
  [-11.510846296699242, -15.431286216605798],
  [-10.521423188982226, -15.87787306749371],
  [-9.037288527406531, -16.35314287549957],
];

const Floorplan = () => {
  const mapRef = useRef(null);
  const layerRef = useRef(null);
  const markerRef = useRef(null);

  var rotation: any;
  var lastA = -1;

  function updateMarker(x: any, y: any, a: any) {
    if (lastA === -1) {
      lastA = a;
    } else if (Math.abs(a - lastA) > 5) {
      lastA = a;
    }
    mapRef.current.setBearing(null);
    rotation = lastA;
    markerRef.current.setCoordinates([x, y]);
    markerRef.current.updateSymbol({
      markerRotation: rotation,
    });
  }
  var sh: any;
  var i: number = 0;
  function cc() {
    if (i >= path.length) {
      clearInterval(sh);
      return;
    }
    updateMarker(path[i][0], path[i][1], path[i][2]);
    i++;
  }

  useEffect(() => {
    if (!mapRef.current) {
      const map = new Map("map", {
        center: [0, 0],
        zoom: 3.64,
        // draggable: false,
        // touchZoom: false,
        // doubleClickZoom: false,
        // scrollWheelZoom: false,
      });

      const imageLayer = new ImageLayer("images", [
        {
          url: "background/scene2/img_12.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_11.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_10.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_9.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_8.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_7.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_6.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_5.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_4.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_3.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
      ]);
      map.addLayer(imageLayer);

      var multiline = new MultiLineString([path], {
        arrowStyle: null, // arrow-style : now we only have classic
        arrowPlacement: "vertex-last", // arrow's placement: vertex-first, vertex-last, vertex-firstlast, point
        visible: true,
        editable: true,
        cursor: null,
        shadowBlur: 0,
        shadowColor: "black",
        draggable: false,
        dragShadow: false, // display a shadow during dragging
        drawOnAxis: null, // force dragging stick on a axis, can be: x, y
        symbol: {
          lineColor: "#0f0",
          lineWidth: 1,
          lineDasharray: [2, 2],
        },
      });

      new VectorLayer("vectorL", multiline).addTo(map);

      var layer = new VectorLayer("vector").addTo(map);

      var marker = new Marker([-19.401499713892008, -0.43326711138107044], {
        symbol: {
          markerFile: "assets/car.svg",
          // markerRotation: 65
        },
      }).addTo(layer);

      // var targetStyles = {
      //   'symbol': {
      //     // 'markerDx': 20,
      //     // 'markerDy': 10,
      //     'markerRotation' : 245
      //   }
      // };
      // animate by maptalks.animation.Animation
      // var player = animation.Animation.animate(
      //   targetStyles, {
      //     duration: 1500,
      //     easing: 'out'
      //   },
      //   // callback of each frame
      
      //   function step(frame) {
      //     if (frame.state.playState === 'running') {
      //       marker.updateSymbol(frame.styles.symbol);
      //     }
      //   }
      // );

      const backGroundTree = new ImageLayer("bgTree", [
        {
          url: "background/scene2/img_2.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
      ]);
      map.addLayer(backGroundTree);

      sh = setTimeout(() => {
        setInterval(cc, 40);
        // player.play()
      }, 1000);
      // map event
      map.on("click", function (param) {
        console.log([param.coordinate.x, param.coordinate.y]);
      });

      console.log(marker);

      mapRef.current = map;
      layerRef.current = layer;
      markerRef.current = marker;
    }
  }, [mapRef, layerRef, markerRef]);

  return <Wrapper id="map" style={{ width: "100%", height: "100%" }} />;
};

export default Floorplan;
