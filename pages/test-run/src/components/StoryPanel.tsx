import React, { useCallback } from 'react'
import styled from 'styled-components'
import { Message } from '../types'


const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  /* width: 100%; */
  height: 250px;
  background: #333333;
  overflow-y: auto;
  overflow-x: hidden;
  box-shadow: inset 1px 1px 0px #FFFFFF14;
  border-bottom-right-radius: 5px;
  &::-webkit-scrollbar {
    width: 5px;
  }
  &::-webkit-scrollbar-track {
    border-radius: 5px;
  }
  &::-webkit-scrollbar-thumb {
    background: #000000; 
    border-radius: 3px;
  }
  &::-webkit-scrollbar-thumb:hover {
    background: #000000; 
  }
`

const MessageItemWrapper = styled.div`
  position: relative;
  &:nth-child(odd) {
    background: #454545;
  }
  > div {
    white-space: break-spaces;
    &.is-overlay {
      position: sticky;
      /* background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.7) 100%); */
    }
    display: flex;
    justify-content: flex-start;
    align-items: center;
    padding: 8px 12px;
    > img {
      margin-right: 10px;
    }
    p {
      white-space: break-spaces;
      color: white;
      &.name {
        font-weight: bold;
        font-size: 10px;
        margin-bottom: 8px;
      }
      &.description {
        font-size: 10px;
        text-align: left;
      }
    }
    > .objective {
      display: flex;
      > p {
        color: #f6ba00;
      }
    }
    > .system-message {
      > p {
        color: #57c2fe;
      }
    }
  }
`
const IconWrapper = styled.img`
  width: 12px;
  height: 12px;
`

type ComponentPropsT = {
  messages: Message[]
}

const getBackgroundClass = (type: string): string => {
  if (type === 'objective') return 'objective'
  if (type === 'skill-checking') return 'skill-checking'
  if (type === 'success') return 'success'
  if (type === 'failed') return 'failed'
  if (type === 'system-message') return 'system-message'
}

const getIcon = (type: string): any => {
  if (type === 'objective') return "/icons/objective.png"
  if (type === 'skill-checking') return "/icons/task.png'"
  if (type === 'success') return "/icons/success.png"
  if (type === 'failed') return "/icons/failed.png"
  if (type === 'system-message') return "/icons/notify.png"
}

const MessageItem = ({ message }: { message: Message }) => {

  return (
    <MessageItemWrapper>
      <div className={message.type === 'objective' ? 'is-overlay' : undefined}>
        <IconWrapper src={getIcon(message.type)} />
        <div className={getBackgroundClass(message.type)}>
          {
            message.type === 'objective' && <p className='name'>{ message.name }</p>
          }
          <p className='description'>{ message.description }</p>
        </div>
      </div>
    </MessageItemWrapper>
  )
}

const StoryPanel = ({ messages }: ComponentPropsT) => {

  const renderMessageItems = useCallback(() => {
    return messages.map(((message, index) => {
      return <MessageItem message={message} key={index} />
    }))
  }, [ messages ])

  return (
    <Wrapper>
      {
        renderMessageItems()
      }
    </Wrapper>
  )
}

export default StoryPanel
