import styled from "styled-components";

export const CurrentLevel = styled.div`
  font-family: Arial, sans-serif;
  font-weight: bold;
  margin: 0;
  margin-left: 10px;
`;
export const MaxLevel = styled.h6`
  font-family: Arial, sans-serif;
  font-weight: bold;
  display: flex;
  align-items: end;
  padding-bottom: 7%;
  margin: 0;
  letter-spacing: 1px;
  margin-left: 2px;
`;
export const LevelWrapper = styled.div`
  display: flex;
  justify-content: right;
  margin-bottom: 0px;
`;
export const StatusBar = styled.div`
  opacity: 0.3;
  width: 45px;
  height: 4px;
  background: linear-gradient(90deg, #7eda00, #fcc418, #ff8787);
`;
export const ProcessBar = styled.div`
  position: absolute;
  top: 0;
`;
export const BarWrapper = styled.div`
  width: 100%;
  position: relative;
`;
export const Wrapper = styled.div`
`;
