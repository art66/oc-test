import React from "react";
import {
  Wrapper,
  LevelWrapper,
  CurrentLevel,
  MaxLevel,
  BarWrapper,
  StatusBar,
  ProcessBar,
} from "./LevelStyle";

type LevelProps = {
  level: number;
  max: number;
};
const findingLevel = (level: number): any => {
  switch (level) {
    case 1:
    case 2:
    case 3:
      return {
        positionProcess: 30,
        bgColor: "#87bf32",
        width: 33.34,
      };
    case 4:
    case 5:
    case 6:
      return {
        positionProcess: 15,
        bgColor: "#fcc419",
        width: 33.34,
      };
    case 7:
    case 8:
    case 9:
      return {
        positionProcess: 0,
        bgColor: "#ff8787",
        width: 33.34,
      };

    default:
      return {
        bgColor: "#ff4e14",
        width: 100,
      };
  }
};
const currentLevelStyle = (bgColor: string) => {
  return {
    color: bgColor,
    // textShadow: `0 0 0px ${bgColor},
    // 0 0 2px ${bgColor},
    // 0 0 1px ${bgColor},
    // 0 0 5px ${bgColor}`,
  };
};

const processBarStyle = (objLevel: any) => {
  return {
    width: `${objLevel.width}%`,
    height: `4px`,
    background: `linear-gradient(
      90deg,
      ${objLevel.bgColor},
      ${objLevel.bgColor}
    )`,
    // boxShadow: `0 0 0px ${objLevel.bgColor},
    // 0 0 2px ${objLevel.bgColor},
    // 0 0 5px ${objLevel.bgColor},
    // 0 0 10px ${objLevel.bgColor}`,
    right: `${objLevel.positionProcess}px`,
  };
};

const LevelIcon = ({ level = 1, max = 10}: LevelProps) => {
  const currentLevel = findingLevel(level);
  return (
    <Wrapper>
      <LevelWrapper>
        <CurrentLevel style={currentLevelStyle(currentLevel.bgColor)}>
          {level}
        </CurrentLevel>
        <MaxLevel style={currentLevelStyle(currentLevel.bgColor)}>
          /{max}
        </MaxLevel>
      </LevelWrapper>
      <BarWrapper>
        <StatusBar></StatusBar>
        <ProcessBar
          style={processBarStyle(currentLevel)}
        />
      </BarWrapper>
    </Wrapper>
  );
};

export default LevelIcon;
