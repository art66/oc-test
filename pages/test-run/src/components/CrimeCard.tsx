import React, { useCallback, useEffect, useRef, useState } from "react"
import styled from "styled-components"
import { ImageLayer, Marker, VectorLayer, Map, LineString, Polygon } from "maptalks"
import { RoutePlayer } from "../utils/routePlayer"
// import { 
//   // elementInMap, 
//   // createTextMarker, 
//   optionsTooltip
//  } from "./options"
import {parkPath} from "./path"
import ScenarioInfo from "./ScenarioInfo"
import PlayerSlot from "./PlayerSlot"
import StoryPanel from "./StoryPanel"

import scenePlayer from "../utils/scenePlayer"

import "./tooltip.css"

const Wrapper = styled.div`
  margin-bottom: 20px;
`
const MapWrapper = styled.div`
  width: 100%;
  height: 100%;
`

const PlayerSlotWrapper = styled.div`
  padding: 10px;
  display: flex;
  justify-content: space-between;
  flex-flow: wrap;
  border-top: 1px solid black;
  box-shadow: inset 0px 1px 0px #FFFFFF14;
  gap: 10px;
  background: var(--background-color);
  @media screen and (max-width: 320px) {
    display: grid;
    grid-template-columns: repeat(2, minmax(33.33%, 1fr) );
  }
`

const ScenarioAction = styled.div`
  display: flex;
  border-top: 1px solid black;
  box-shadow: inset 0px 1px 0px #FFFFFF14;
  border-bottom-left-radius: 5px;
  @media screen and (max-width: 320px) {
    display: block;
  }
`

const FloorplanWrapper = styled.div`
  flex: 2;
  max-width: 500px;
  height: 250px;
  background: var(--background-color);
  box-shadow: 0px 1px 0px #FFFFFF14;
  border-right: 1px solid black;
  border-bottom-left-radius: 5px;
  @media screen and (max-width: 320px) {
    height: 160px;
    max-width: 320px;
  }
`

const StoryPanelWrapper = styled.div`
  flex: 1;
`

// const handleHoverTooltip = (polygon: any, tooltipText: Object, coord: any) => {
//   polygon
//     .on("mouseover", function (e: any) {
//       polygon.setInfoWindow(optionsTooltip(tooltipText))
//       polygon.openInfoWindow(coord)
//     })
//     .on("mouseout", function () {
//       polygon.closeInfoWindow()
//     })
// }
const CrimeCard = ({ animations = [] }: { animations: any }): JSX.Element => {
  const [playerSlots, setPlayerSlots] = useState([
    {
      player: {},
      successChance: 70,
      roleName: "Cracker",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 97,
      roleName: "Driver",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 67,
      roleName: "Shooter",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 75,
      roleName: "Hacker",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 50,
      roleName: "Shooter",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 98,
      roleName: "Shooter",
      percent: 0,
      status: "empty-slot",
    },
  ])

  const [scenarioPhaseStatus, setScenarioPhaseStatus] = useState("recruiting");
  const [scenarioPhaseEndTime] = useState(new Date("2022-05-05"));
  const [level] = useState(3);
  const [stories, setStories] = useState([]);
  const [players, setPlayers] = useState([]);

  const mapRef = useRef(null)
  const actionLayerRef = useRef(null)

  const [localPlayer] = useState({
    badgeUrl: "https://awardimages.torn.com/2750828-13443-large.png",
  })


  const markerRef = useRef(null);

  var rotation: any;
  var lastA = -1;

  function updateMarker(x: any, y: any, a: any) {
    if (lastA === -1) {
      lastA = a;
      console.log(lastA);
    } else if (Math.abs(a - lastA) > 5) {
      lastA = a;
      console.log(markerRef.current._symbol);
    }
    mapRef.current.setBearing(null);
    rotation = lastA;
    markerRef.current.setCoordinates([x, y]);
    markerRef.current.updateSymbol({
      markerRotation: rotation,
    });
  }
  var sh: any;
  var i: number = 0;
  function cc(path:any) {
    if (i >= path.length) {
      clearInterval(sh);
      return;
    }
    updateMarker(path[i][0], path[i][1], path[i][2]);
    i++;
  }

  // const imgScene1 = [
  //   {
  //     url: "background/scene1/Map_Layer_1.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  //   {
  //     url: "background/scene1/Map_Layer_2.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  //   {
  //     url: "background/scene1/Map_Layer_3.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  //   {
  //     url: "background/scene1/Map_Layer_4.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  //   {
  //     url: "background/scene1/Map_Layer_5.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  //   {
  //     url: "background/scene1/Map_Layer_6.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  //   {
  //     url: "background/scene1/Map_Layer_7.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  //   {
  //     url: "background/scene1/Map_Layer_8.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  // ]
  
  // const bgTree1 = [
  //   {
  //     url: "background/scene1/Map_Layer_9.png",
  //     extent: [-25, 12.5, 25, -12.5],
  //     opacity: 1,
  //   },
  // ]
  
  const imgScene2  = [
    {
      url: "background/scene2/img_12.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    {
      url: "background/scene2/img_11.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    {
      url: "background/scene2/img_10.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    {
      url: "background/scene2/img_9.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    {
      url: "background/scene2/img_8.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    {
      url: "background/scene2/img_7.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    {
      url: "background/scene2/img_6.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    {
      url: "background/scene2/img_5.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
    // {
    //   url: "background/scene2/img_4.png",
    //   extent: [-25, 12.5, 25, -12.5],
    //   opacity: 1,
    // },
    {
      url: "background/scene2/img_3.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
  ]

  const bgTree2 = [
    {
      url: "background/scene2/img_2.png",
      extent: [-25, 12.5, 25, -12.5],
      opacity: 1,
    },
  ]
  // var bgMap;
  // var bgMapTree;
  // if(scene === 1){
  //   bgMap = imgScene1
  //   bgMapTree = bgTree1
  // } else if(scene === 2){
  //   bgMap = imgScene2
  //   bgMapTree = bgTree2
  // }

  useEffect(() => {
    if (!mapRef.current) {
      const map = new Map("map", {
        center: [0, 0],
        zoom: 3.5,
        // zoom: 3.86,
        // draggable: false,
        // touchZoom: false,
        // doubleClickZoom: false,
        // scrollWheelZoom: false,
      })
      const imageLayer = new ImageLayer("imageLayer", imgScene2)
      const actionLayer = new VectorLayer("actionLayer").setId("actionLayer")

      console.log(imageLayer);
      var center = map.getCenter();
      
      var wrapperMap = new Polygon([
        center.add(-25, 12.5),
        center.add(25, 12.5),
        center.add(25, -12.5),
        center.add(-25, -12.5)
      ], {
        symbol : {
          polygonFill : '#fff',
          polygonOpacity : 0,
          lineColor : '',
          lineWidth : 0,
        }
      }).addTo(actionLayer)

      map.fitExtent(wrapperMap.getExtent(), 0.87);

      new LineString(
        [
          [-12.4244140625, 4.7406753847783705],
          [-10.4244140625, 4.7406753847783705],
        ],
        {
          id: "sliding_door1",
          symbol: {
            lineColor: "#FFFFFF",
            opacity: 0,
          },
        }
      ).addTo(actionLayer)

      new LineString(
        [
          [17.181738281249977, -0.4965758144294625],
          [18.969433593749955, -1.2910040262295297],
        ],
        {
          id: "sliding_door2",
          symbol: {
            lineColor: "#4b4b4b",
            opacity: 1,
          },
        }
      ).addTo(actionLayer)

      new LineString(
        [
          [ -0.791015625, -2.8991526985043095 ],
          [ 0.52734375, -3.030812122664372 ],
        ],
        {
          id: "door1",
          symbol: {
            lineColor: "#FFFFFF",
            lineWidth: 1,
            opacity: 0,
          },
        }
      ).addTo(actionLayer)

      new Marker([6.4599609375, 0.6591651462894674], {
        id: "item1",
        symbol: {
          markerFile: "players/P4.png",
          markerWidth: 5,
          markerHeight: 5,
          opacity: 0,
        },
      }).addTo(actionLayer)

      new Marker([3.5595703125, -6.708253968671528], {
        id: "item2",
        symbol: {
          markerFile: "players/P4.png",
          markerWidth: 5,
          markerHeight: 5,
          opacity: 0,
        },
      }).addTo(actionLayer)

      new Marker([2.584903496275956, -3.030812122664372], {
        id: "guard",
        symbol: {
          markerFile: "players/P8.png",
          markerWidth: 8,
          markerHeight: 8,
          opacity: 0,
        },
      }).addTo(actionLayer)

      var car = new Marker([-9.479687499999955, 20.221488238926014], {
        id:"car",
        symbol: {
          markerFile: "assets/car.svg",
          markerRotation: 155
        },
      }).addTo(actionLayer);

      map.on("click", function (param) {
        console.log([param.coordinate.x, param.coordinate.y])
      })

      map.addLayer(imageLayer)
      map.addLayer(actionLayer)
      animations.forEach((animation) => {
        animation.timelines.forEach((timeline) => {
          if (timeline?.animation?.vars?.motionPath) {
            const playerName = timeline.animation.target.split("-")[0]
            if(playerName === "car"){
              const car = new RoutePlayer(
                { path: timeline.animation.vars.motionPath.path },
                map,
                actionLayer,
                {
                  id: timeline.animation.target,
                  markerFile: `assets/${playerName}.svg`,
                  markerWidth: 25,
                  markerHeight: 50,
                  markerRotation: 155
                }
              )
              setPlayers((old) => [...old, car])
            } else {
              const player = new RoutePlayer(
                { path: timeline.animation.vars.motionPath.path },
                map,
                actionLayer,
                {
                  id: timeline.animation.target,
                  markerFile: `players/${playerName}.png`,
                  markerWidth: 8,
                  markerHeight: 10,
                }
              )
              setPlayers((old) => [...old, player])
            }
          }
        })
      })

      // Create all elements layer & handleOpen tooltip
      // var polygon: any, tooltipText: any, coord: any
      // elementInMap.forEach((ele, idx) => {
      //   ele.forEach((el) => {
      //     polygon = el.polygon
      //     tooltipText = el.tooltipText
      //     coord = el.coord
      //     handleHoverTooltip(polygon, tooltipText, coord)
      //   })
      //   new VectorLayer(`elements_${idx}`, polygon).addTo(map)
      // })

      // North Text Marker
      // var northText = new VectorLayer("northText").addTo(map)
      // createTextMarker(
      //   "North\nEntrance",
      //   [-18.0615234375, 10.358151400943683],
      //   "left"
      // ).addTo(northText)

      // // South Text Marker
      // var southText = new VectorLayer("southText").addTo(map)
      // createTextMarker(
      //   "South\nEntrance",
      //   [0.46142578125, -9.449061826881433],
      //   "right"
      // ).addTo(southText)

      // Tree Background Layer
      const backGroundTree = new ImageLayer("bgTree", bgTree2)
      map.addLayer(backGroundTree)

      sh = setTimeout(() => {
        setInterval(() => cc(parkPath),100)
      }, 5000);

      mapRef.current = map
      markerRef.current = car
      actionLayerRef.current = actionLayer
    }
  }, [ animations, sh ])

  const doAnimation = useCallback((target, vars) => {
    const object = actionLayerRef.current.getGeometryById(target)
    
    if (vars.translate) {
      object.animate(
        { translate: vars.translate },
        { duration: vars.duration * 1000 }
      )
    }
    if (vars.symbol) {
      object.animate(
        { symbol: vars.symbol },
        { duration: vars.duration * 1000 }
      )
    }
    if (vars.degree) {
      rotateDoor({ object, duration: vars.duration, degree: vars.degree, centerRotate: vars.centerRotate })
    }
    if (vars.motionPath) {
      const player = players.find((player) => player.id === target)
      if (player.play) {
        player.play()
        player.hideTrail()
        player.hideRoute()
      }
      if (vars.hide) {
        setTimeout(() => {
          player.remove()
        }, vars.duration * 1000)
      }
    }
  }, [ players ])

  const rotateDoor = ({ object, duration = 2, centerRotate = 'start', frame = 24, degree = 90 }) => {
    const secondPerFrame = duration / frame
    const degreePerFrame = degree / frame
    const centerRotateIndex = centerRotate === 'start' ? 0 : 1
    
    const center = [
      object.getCoordinates()[centerRotateIndex].x,
      object.getCoordinates()[centerRotateIndex].y,
    ]
    for (let i = 0; i < frame; i++) {
      
      setTimeout(() => {
        object.rotate(degreePerFrame, center)
      }, secondPerFrame * 1000 * i)
    } 
  }

  const animationPlayer = useCallback((timeline) => {
    if (timeline.type === "animation") {
      const { target, vars } = timeline.animation
      doAnimation(target, vars)
    }
    if (timeline.type === "story") {
      setStories((old) => [...old, timeline.story])
    }
  }, [ doAnimation ])

  useEffect(() => {
    let scene = null
    if (mapRef.current && actionLayerRef.current) {
      scene = scenePlayer(animations, animationPlayer)
      scene.play()
    }

    return () => {
      if (scene) scene.kill()
    }
  }, [ animationPlayer, animations ])

  useEffect(() => {
    const isAllAvailable = playerSlots.every(
      (playerSlot) => playerSlot.status !== "empty-slot"
    )
    if (isAllAvailable) {
      setScenarioPhaseStatus("planning-active")
    }
  }, [ playerSlots ])

  const onClickJoin = useCallback(
    (index) => {
      const newPlayerSlots = playerSlots.map((playerSlot, i) => {
        if (index === i) {
          return {
            ...playerSlot,
            player: localPlayer,
            status: "progress",
          }
        }
        return playerSlot
      })
      setPlayerSlots(() => newPlayerSlots)
    },
    [ setPlayerSlots, playerSlots, localPlayer ]
  )

  const renderPlayerSlot = useCallback(() => {
    return playerSlots.map((playerSlot, index) => {
      return (
        <PlayerSlot
          key={index}
          player={playerSlot.player}
          successChance={playerSlot.successChance}
          roleName={playerSlot.roleName}
          percent={playerSlot.percent}
          status={playerSlot.status}
          onJoinButtonClick={() => onClickJoin(index)}
        />
      )
    })
  }, [playerSlots, onClickJoin])

  return (
    <Wrapper>
      <ScenarioInfo
        title="Money Heise"
        description="Money Heise"
        status={scenarioPhaseStatus}
        endTime={scenarioPhaseEndTime}
        level={level}
      />
      <PlayerSlotWrapper>{renderPlayerSlot()}</PlayerSlotWrapper>
      {scenarioPhaseStatus !== "planning-active" && (
        <ScenarioAction>
          <FloorplanWrapper>
            <MapWrapper id="map" style={{ width: "100%", height: "100%" }} />
          </FloorplanWrapper>
          <StoryPanelWrapper>
            <StoryPanel messages={stories} />
          </StoryPanelWrapper>
        </ScenarioAction>
      )}
    </Wrapper>
  )
}

export default CrimeCard
