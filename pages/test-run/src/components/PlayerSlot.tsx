import React from 'react'
import styled from 'styled-components'

import ProgressClock from './Clocks/ProgressClock'

import {
  Player
} from '../types'

const Wrapper = styled.div`
  width: 119px;
  height: 50px;
  border: 1px solid #111111;
  border-radius: 7px;
  background: ${(props: { backgroundColor: string }) => props.backgroundColor};
  display: flex;
  flex-direction: column;
  @media screen and (max-width: 578px) {
    width: 100%;
    float: left;
  }
`

const TopWrapper = styled.div`
  display: flex;
  padding: 4px;
  border-bottom: 1px solid #000000;
  justify-content: space-evenly;
  align-items: center;
`

const Box = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
`

const ClockWrapper = styled(Box)`
  display: flex;
  align-items: center;
  > div {
    width: 10px;
    height: 10px;
    margin-right: auto;
  }
`

const RoleTextWrapper = styled(Box)`
  font-size: 9px;
  color: #ffffff;
  font-weight: bold;
  width: 100%;
`

const SuccessChanceTextWrapper = styled(Box)`
  > * {
    margin-left: auto;
    font-size: 11px;
    text-align: right;
    color: ${(props: { textColor: string }) => props.textColor};
  }
`

const PlayerBadgeImageWrapper = styled.img`
  width: 100%;
`

const BottomWrapper = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
`


const JoinButton = styled.button`
  /* background: transparent linear-gradient(180deg, #e2e2e2 0%, #9f9f9f 100%) 0 0 no-repeat; */
  background: none;
  width: 100%;
  border: 0;
  padding: 2px;
  margin: 0 4px;
  border-radius: 4px;
  cursor: pointer;
  > p {
    color: #74C0FC;
    font-size: 12px;
    letter-spacing: 1px;
  }
  /* &:hover {
    background: transparent linear-gradient(180deg, #ffffff 0%, #9f9f9f 100%) 0 0 no-repeat;
  }
  &:active {
    background: transparent linear-gradient(180deg, #9f9f9f 0%, #aaaaaa 100%) 0 0 no-repeat;
  } */
`

const getBackgroundColor = (player: Player, successChance: number): string => {
  if (!player.badgeUrl) return '#333333'
  if (successChance < 70) return '#473732'
  if (successChance <= 90) return '#48422d'
  if (successChance > 90) return '#3f442d'
}

const getSuccessChanceColor = (successChance: number): string => {
  if (successChance < 70) return '#fe8685'
  if (successChance <= 90) return '#fbc417'
  if (successChance > 90) return '#95d82c'
}

type ComponentPropsT = {
  player: Player;
  successChance: number;
  roleName: string;
  onJoinButtonClick: any;
  percent?: number;
  status: string;
}

const PlayerSlot = ({
  player = {},
  successChance = 0,
  roleName = 'role',
  onJoinButtonClick = () => null,
  percent = 0,
  status = 'empty-slot',
} : ComponentPropsT
) => {

  return (
    <Wrapper backgroundColor={getBackgroundColor(player, successChance)}>
      <TopWrapper>
        <ClockWrapper>
          <div>
            <ProgressClock
              percent={percent}
              status={status}
            />
          </div>
        </ClockWrapper>
        <RoleTextWrapper>{ roleName.toLocaleUpperCase() }</RoleTextWrapper>
        <SuccessChanceTextWrapper textColor={getSuccessChanceColor(successChance)}>
          <span>{ successChance }</span>
        </SuccessChanceTextWrapper>
      </TopWrapper>
      <BottomWrapper>
        {
          player.badgeUrl ?
          <PlayerBadgeImageWrapper src={player.badgeUrl} />
          :
          <JoinButton onClick={onJoinButtonClick}>
            <p>
              Join
            </p>
          </JoinButton>
        }
      </BottomWrapper>
    </Wrapper>
  )
}

export default PlayerSlot
