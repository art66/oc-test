import React, { useEffect, useRef } from "react";
import styled from "styled-components";
import { RoutePlayer } from "../lib/test_lib/routePlayer";
import { route, route2, route3, route4, route5, route6 } from "../mockData/route";
// import { elementInMap, createTextMarker, optionsTooltip } from "../utils/options";
import { Map, ImageLayer, VectorLayer, Marker, animation } from "maptalks";

import "./tooltip.css";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const handleHoverTooltip = (polygon: any, tooltipText: Object) => {
  // polygon
  //   .on("mouseover", function (e: any) {
  //     polygon.setInfoWindow(optionsTooltip(tooltipText));
  //     polygon.openInfoWindow(e.coordinate);
  //   })
  //   .on("mouseout", function () {
  //     polygon.closeInfoWindow();
  //   });
};

const Floorplan = () => {
  const mapRef = useRef(null);
  const layer = useRef(null);

  useEffect(() => {
    if (!mapRef.current) {
      const map = new Map("map", {
        center: [0, 0],
        zoom: 3.64,
        // draggable: false,
        // touchZoom: false,
        // doubleClickZoom: false,
        // scrollWheelZoom: false,
      });
      const imageLayer = new ImageLayer("images", [
        {
          url: "background/scene2/img_12.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_11.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_10.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_9.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_8.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_7.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_6.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_5.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_4.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        {
          url: "background/scene2/img_3.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
        //#region scene 1
        // {
        //   url: "background/scene1/Map_Layer_1.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        // {
        //   url: "background/scene1/Map_Layer_2.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        // {
        //   url: "background/scene1/Map_Layer_3.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        // {
        //   url: "background/scene1/Map_Layer_4.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        // {
        //   url: "background/scene1/Map_Layer_5.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        // {
        //   url: "background/scene1/Map_Layer_6.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        // {
        //   url: "background/scene1/Map_Layer_7.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        // {
        //   url: "background/scene1/Map_Layer_8.png",
        //   extent: [-30, 15.5, 30, -15.5],
        //   opacity: 1,
        // },
        //#endregion
      ]);
      map.addLayer(imageLayer);

      var player1 = new RoutePlayer(route, map, {
        markerFile: "players/P1.png",
        markerWidth: 10,
        markerHeight: 10,
      });

      var player2 = new RoutePlayer(route2, map, {
        markerFile: "players/P2.png",
        markerWidth: 10,
        markerHeight: 10,
      });

      var player3 = new RoutePlayer(route3, map, {
        markerFile: "players/P3.png",
        markerWidth: 10,
        markerHeight: 10,
      });

      var player4 = new RoutePlayer(route4, map, {
        markerFile: "players/P4.png",
        markerWidth: 10,
        markerHeight: 10,
      });

      var player5 = new RoutePlayer(route5, map, {
        markerFile: "players/P5.png",
        markerWidth: 10,
        markerHeight: 10,
      });

      var PlayerCar = new RoutePlayer(route6, map, {
        markerFile: "assets/car.svg",
        markerWidth: 25,
        markerHeight: 50,
        markerRotation: 150
      });

      // playing player 1
      player1.play();
      player1.hideTrail();
      player1.hideRoute();

      // playing player 2
      player2.play();
      player2.hideTrail();
      player2.hideRoute();

      // playing player 3
      player3.play();
      player3.hideTrail();
      player3.hideRoute();

      // playing player 4
      player4.play();
      player4.hideTrail();
      player4.hideRoute();

      // playing player 5
      player5.play();
      player5.hideTrail();
      player5.hideRoute();
      
      // playing car 1
      PlayerCar.play();
      PlayerCar.hideTrail();

      var layer = new VectorLayer("vectorM").addTo(map);

      var marker = new Marker([-29.055917212119653, 1.2095617185578647], {
        symbol: {
          markerFile: "assets/car.svg",
          markerRotation: 150
        },
      }).addTo(layer);

      var targetStyles = {
        'symbol': {
          // 'markerDx': 22.5,
          // 'markerDy': 80,
          'markerRotation' : 65
        }
      };
      // animate by maptalks.animation.Animation
      var player = animation.Animation.animate(
        targetStyles, {
          duration: 1500,
          easing: 'out'
        },
        // callback of each frame
      
        function step(frame) {
          console.log(frame);
          
          if (frame.state.playState === 'running') {
            marker.updateSymbol(frame.styles.symbol);
          }
        }
      );

      
      // PlayerCar.hideRoute();

      // Create all elements layer & handleOpen tooltip
      // var polygon: any, tooltipText: any;
      // elementInMap.forEach((ele, idx) => {
      //   ele.forEach((el) => {
      //     polygon = el.polygon;
      //     tooltipText = el.tooltipText;
      //     handleHoverTooltip(polygon, tooltipText);
      //   });
      //   new VectorLayer(`elements_${idx}`, polygon).addTo(map);
      // });

      // North Text Marker
      // var northText = new VectorLayer("northText").addTo(map);
      // createTextMarker(
      //   "North\nEntrance",
      //   [-20.07234724039654, 12.328549995988709],
      //   "left"
      // ).addTo(northText);

      // // South Text Marker
      // var southText = new VectorLayer("southText").addTo(map);
      // createTextMarker(
      //   "South\nEntrance",
      //   [-1.20849609375, -11.845847044118472],
      //   "right"
      // ).addTo(southText);

      // Tree Layer background
      // const backGroundTree = new ImageLayer("bgTree", [
      //   {
      //     url: "background/scene1/Map_Layer_9.png",
      //     extent: [-30, 15.5, 30, -15.5],
      //     opacity: 1,
      //   },
      // ]);
      // map.addLayer(backGroundTree);
      const backGroundTree = new ImageLayer("bgTree", [
        {
          url: "background/scene2/img_2.png",
          extent: [-30, 15, 30, -15.5],
          opacity: 1,
        },
      ]);

      setTimeout(() =>{
        player.play()
      }, 2000)
      map.addLayer(backGroundTree);

      // map event
      map.on("click", function (param) {
        console.log([param.coordinate.x, param.coordinate.y]);
      });

      mapRef.current = map;
      layer.current = layer;
    }
  }, [mapRef, layer]);

  return <Wrapper id="map" style={{ width: "100%", height: "100%" }} />;
};

export default Floorplan;
