import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Pie = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  display: grid;
  place-items: center;
  box-shadow: inset 0 0 5px 2px #616060;
  img.border-watch{
    width: 100%;
    height: 100%;
  }
`;