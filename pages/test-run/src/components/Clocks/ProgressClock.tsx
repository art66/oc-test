import React from "react";
import { Wrapper, Pie } from "./Clock";
const statusColor = (status: string) => {
  var firstColor: string, secondColor: string;
  if (status === "available") {
    firstColor = "#5f9825";
    secondColor = "#558921";
  }
  if (status === "progress") {
    firstColor = "#75d9dd";
    secondColor = "#75d9dd";
  }
  if (status === "empty-slot") {
    firstColor = "#aeaeae";
    secondColor = "#6f6f6f";
  }
  if (status === "pre-occupied") {
    firstColor = "#c1572f";
    secondColor = "#b8452d";
  }
  return {
    background: `linear-gradient(
      180deg,
      ${firstColor} 0%,
      ${secondColor} 100%
    )`,
  };
};
const pieStyles = (percent: number) => {
  return {
    background: `conic-gradient(#75d9dd ${percent * 3.6}deg,
    #172130 ${percent * 3.6}deg)`,
  };
};

type ClockProps = {
  percent: number;
  status: string;
};
const ProgressClock = ({ percent = 0, status = "" }: ClockProps) => {
  return (
    <Wrapper style={statusColor(status)}>
      {status === "progress" && (
        <Pie style={pieStyles(percent)}>
          <img src="/icons/images/clock.png" alt="" className="border-watch" />
        </Pie>
      )}
      {status === "undetermined" && (
        <img src="/icons/question.png" alt="" width="10" height="10"/>
      )}
      {status === "escaped" && (
        <img src="/icons/green-tick.png" alt="" width="10" height="10"/>
      )}
      {status === "jailed" && (
        <img src="/icons/red-cross.png" alt="" width="10" height="10"/>
      )}
    </Wrapper>
  );
};

export default ProgressClock;
