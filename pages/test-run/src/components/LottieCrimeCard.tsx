import React, { useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { Player } from "@lottiefiles/react-lottie-player";

import ScenarioInfo from "./ScenarioInfo";
import PlayerSlot from "./PlayerSlot";
import StoryPanel from "./StoryPanel";

import scene_1 from "../mockData/lottie/1_Prelude.json";
import scene_2 from "../mockData/lottie/2_0.json";
import scene_3 from "../mockData/lottie/3_A1.json";
import scene_4 from "../mockData/lottie/4_A1-C1.json";
import scene_5 from "../mockData/lottie/5_A1-C1F.json";
import scene_6 from "../mockData/lottie/6_A1-C2.json";
import scene_7 from "../mockData/lottie/7_A1-C3.json";
import scene_8 from "../mockData/lottie/8_A1-C3F.json";
import scene_9_1 from "../mockData/lottie/9_A1F.json";
import scene_9_2 from "../mockData/lottie/9_A1FCont-1.json";
import scene_9_3 from "../mockData/lottie/9_A1FCont-2.json";

const scenes = [
  { id: 1, scene: scene_1, message: {type: 'objective', description: 'Get passed security.'}},
  { id: 2, scene: scene_2, message: { type: 'skill-checking', description: '<P1> and <P2> do the disguises.\n<P3> keeps a lookout.' } },
  { id: 3, scene: scene_3, message: { type: 'skill-checking', description: '<P1> and <P2> decide to take the service entrance to the building.' }},
  { id: 4, scene: scene_4, message: { type: 'skill-checking', description: '<P1> greets the security officer as they  walk towards the security checkpoint.' } },
  { id: 5, scene: scene_5, message: { type: 'success', description: 'The security officer stops them in their tracks. He asks for their ID.' } },
  { id: 6, scene: scene_6, message: { type: 'skill-checking', description: '<P1> and <P2> hand over their fake employee\'s IDs.' } },
  { id: 7, scene: scene_7, message: { type: 'skill-checking', description: '<P2> agrees with the security officer. The officer proceeds with the call.' } },
  { id: 8, scene: scene_8, message: { type: 'failed', description: 'Nobody is answering. The officer instructs them to wait a bit longer.' } },
  { id: 9, scene: scene_9_1, message: { type: 'failed', description: '<P1> and <P2> tell the officer they will just come another day.' } },
  { id: 10, scene: scene_9_2 },
  { id: 11, scene: scene_9_3 },
];

const Wrapper = styled.div`
  margin-bottom: 20px;
  max-width: 784px;
`

const PlayerSlotWrapper = styled.div`
  padding: 10px 0;
  display: flex;
  justify-content: space-between;
  flex-flow: wrap;
  border-top: 1px solid black;
  box-shadow: inset 0px 1px 0px #ffffff14;
  gap: 10px;
  background: var(--background-color);

  @media screen and (max-width: 578px) {
    display: grid;
    grid-template-columns: repeat(3, minmax(auto, 1fr) );
  }
  
  @media screen and (max-width: 320px) {
    gap: 5px;
    padding: 10px 0;
  }
`;

const ScenarioAction = styled.div`
  display: flex;
  border-top: 1px solid black;
  box-shadow: inset 0px 1px 0px #ffffff14;
  border-bottom-left-radius: 5px;
  @media screen and (max-width: 578px) {
    flex-direction: column;
  }
  @media screen and (max-width: 320px) {
    display: block;
  }
`;

const FloorplanWrapper = styled.div`
  flex: 2;
  max-width: 500px;
  height: 250px;
  background: var(--background-color);
  box-shadow: 0px 1px 0px #ffffff14;
  border-right: 1px solid black;
  border-bottom-left-radius: 5px;
  @media screen and (max-width: 578px) {
    max-width: 100%;
  }
  @media screen and (max-width: 320px) {
    height: 160px;
  }
`;

const StoryPanelWrapper = styled.div`
  max-width: 100%;
  flex: 1;
`;

const CrimeCard = ({ animations = [] }: { animations: any }): JSX.Element => {
  const [playerSlots, setPlayerSlots] = useState([
    {
      player: {},
      successChance: 70,
      roleName: "Robber",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 97,
      roleName: "Robber",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 67,
      roleName: "Robber",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 67,
      roleName: "Robber",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 67,
      roleName: "Lookout",
      percent: 0,
      status: "empty-slot",
    },
    {
      player: {},
      successChance: 67,
      roleName: "Lookout",
      percent: 0,
      status: "empty-slot",
    },
  ]);

  const [scenarioPhaseStatus, setScenarioPhaseStatus] = useState("recruiting");
  const [scenarioPhaseEndTime] = useState(new Date("2022-05-05"));
  const [level] = useState(3);
  const [stories, setStories] = useState([]);
  const [currentSceneIndex, setCurrentSceneIndex] = useState(0);
  const [localPlayer] = useState({
    badgeUrl: "/tagIcons/P1.png",
  })

  useEffect(() => {
    const isAllJoin = playerSlots.every(
      (playerSlot) => playerSlot.status !== "empty-slot"
    )
    const isAllAvailable = playerSlots.every(
      (playerSlot) => playerSlot.status === "empty-slot"
    )
    const isSomeJoin = playerSlots.some(
      (playerSlot) => playerSlot.status === "empty-slot"
    )
    if (isAllAvailable) {
      setScenarioPhaseStatus("recruiting")
    }
    else if (isSomeJoin) {
      setScenarioPhaseStatus("planning-active")
    }
    else if (isAllJoin) {
      setScenarioPhaseStatus("reveal")
    }
  }, [ playerSlots ])


  const onClickJoin = useCallback(
    (index) => {
      const newPlayerSlots = playerSlots.map((playerSlot, i) => {
        if (index === i) {
          return {
            ...playerSlot,
            player: {
              badgeUrl: `/tagIcons/P${i + 1}.png`
            },
            status: "progress",
          };
        }
        return playerSlot;
      });
      setPlayerSlots(() => newPlayerSlots);
    },
    [setPlayerSlots, playerSlots, localPlayer]
  );

  const renderPlayerSlot = useCallback(() => {
    return playerSlots.map((playerSlot, index) => {
      return (
        <PlayerSlot
          key={index}
          player={playerSlot.player}
          successChance={playerSlot.successChance}
          roleName={playerSlot.roleName}
          percent={playerSlot.percent}
          status={playerSlot.status}
          onJoinButtonClick={() => onClickJoin(index)}
        />
      );
    });
  }, [playerSlots, onClickJoin]);

  useEffect(() => {
    setCurrentSceneIndex(0);
  }, []);

  useEffect(() => {
    if (scenes[currentSceneIndex].message) {
      const newStories = [scenes[currentSceneIndex].message, ...stories];
      setStories(newStories);
    }
  }, [ currentSceneIndex ])
  
  const onSceneEnd = useCallback((state) => {
    if (state === 'complete' && currentSceneIndex < scenes.length - 1) {
      setTimeout(() => {
        setCurrentSceneIndex((old) => old + 1)
      }, 3000)
    }
    if (currentSceneIndex === scenes.length - 1) {
      setScenarioPhaseStatus("failed")
    }
  }, [ currentSceneIndex, setStories, setCurrentSceneIndex])

  return (
    <Wrapper>
      <ScenarioInfo
        title="ATM Robbery"
        description="We have acquired a key, from a trusted contact, that is supposed to open up any ATM. Assemble a crew and hit the ATMs in the <Bankname> bank at the nearby mall."
        status={scenarioPhaseStatus}
        endTime={scenarioPhaseEndTime}
        level={level}
      />
      <PlayerSlotWrapper>{renderPlayerSlot()}</PlayerSlotWrapper>
      {(scenarioPhaseStatus === "reveal" || scenarioPhaseStatus === "failed") && (
        <ScenarioAction>
          <FloorplanWrapper>
            {scenes[currentSceneIndex].scene && (
              <Player
                loop={false}
                autoplay
                keepLastFrame
                src={scenes[currentSceneIndex].scene}
                onEvent={onSceneEnd}
              />
            )}
          </FloorplanWrapper>
          <StoryPanelWrapper>
            <StoryPanel messages={stories} />
          </StoryPanelWrapper>
        </ScenarioAction>
      )}
    </Wrapper>
  );
};

export default CrimeCard;
