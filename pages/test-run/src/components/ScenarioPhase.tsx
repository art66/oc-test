import React, { useEffect, useState } from "react";
// import Recruiting from "../assets/icons/phase/recruiting.png"
import styled from "styled-components";
// if (status === "planning-active") return "/icons/phase/planning-active.png";
// if (status === "planning-paused") return "/icons/phase/planning-paused.png";
// if (status === "initiating") return "/icons/phase/initiating.png";
// if (status === "expiring") return "/icons/phase/expiring.png";
// if (status === "expired") return "/icons/phase/expired.png";
// if (status === "reveal") return "/icons/phase/reveal.png";
// if (status === "failed") return "/icons/phase/failed.png";
// if (status === "success") return "/icons/phase/success.png";

const Wrapper = styled.div`
  width: 100%;
  height: 90px;
  position: relative;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-image: url("/background/scenario-map-0001.png");
  .container {
    width: 146px;
    height: 100%;
    border-radius: 2px;
    display: flex;
    opacity: 0.8;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    @media screen and (max-width: 578px) {
      width: 100%;
    }
    &.recruiting {
      padding: 0 17px;
    }

    > .status-text {
      text-align: center;
      font-size: 18px;
      font-weight: bold;
      z-index: 10;
      letter-spacing: 1px;
    }

    &.recruiting,
    &.planning-active {
      background: linear-gradient(
        to bottom,
        rgb(54, 101, 144) 5%,
        rgb(29, 62, 84) 50%,
        rgb(12, 32, 41) 80%
      );
      > .status-text {
        color: #83f1ff;
        text-shadow: 0 0 10px #64b8cb;
      }
    }
    &.planning-paused,
    &.initiating,
    &.expiring {
      background: linear-gradient(
        to bottom,
        rgba(211, 184, 35, 1) 10%,
        rgba(206, 189, 70, 1) 80%,
        rgba(219, 210, 104, 1) 100%
      );
      &::before {
        background: linear-gradient(
          to bottom,
          rgba(255, 255, 255, 0) 0%,
          rgba(255, 255, 255, 0.3) 100%
        );
      }
      > .status-text {
        color: #222221;
      }
    }

    &.expired {
      background: linear-gradient(
        to bottom,
        rgb(179, 179, 179) 5%,
        rgb(155, 155, 155) 80%,
        rgb(127, 127, 127) 99%
      );
      > .status-text {
        color: #870f09;
        text-shadow: 0 0 10px #a24b47;
      }
    }
    &.reveal {
      background: linear-gradient(
        to bottom,
        rgb(81, 79, 24) 5%,
        rgb(61, 60, 38) 80%,
        rgb(43, 43, 40) 99%
      );
      > .status-text {
        color: #ffffff;
        text-shadow: 0 0 10px #aea43b;
      }
    }
    &.failed {
      background: linear-gradient(
        to bottom,
        rgb(193, 63, 46) 5%,
        rgb(143, 60, 42) 50%,
        rgb(81, 22, 13) 90%
      );
      > .status-text {
        color: #ffffff;
        text-shadow: 0 0 10px #64b8cb;
      }
    }
    &.success {
      background: linear-gradient(
        to bottom,
        rgb(70, 113, 36) 5%,
        rgb(64, 95, 32) 50%,
        rgb(26, 41, 12) 90%
      );
      > .status-text {
        color: #ffffff;
        text-shadow: 0 0 10px #6a892a;
      }
    }
  }
`;

const IconWrapper = styled.div`
  width: 45px;
  height: 45px;
  border-radius: 20px;
  z-index: 2;
  margin-bottom: 8px;
`;

const getColorClass = (status: string): string => {
  if (status === "recruiting") return "recruiting";
  if (status === "planning-active") return "planning-active";
  if (status === "planning-paused") return "planning-paused";
  if (status === "initiating") return "initiating";
  if (status === "expiring") return "expiring";
  if (status === "expired") return "expired";
  if (status === "reveal") return "reveal";
  if (status === "failed") return "failed";
  if (status === "success") return "success";
};

const getStatusText = (status: string): string => {
  if (status === "recruiting") return "RECRUITING";
  if (status === "initiating") return "INITIATING";
  if (status === "expired") return "EXPIRED";
  if (status === "reveal") return "IN PROGRESS";
  if (status === "failed") return "FAILED";
  if (status === "success") return "SUCCESS";
};

const isShowTimer = (status: string): boolean => {
  if (status === "planning-active") return true;
  if (status === "planning-paused") return true;
  if (status === "expiring") return true;

  return false;
};

const getIconPath = (status: string): string => {
  if (status === "recruiting") return "/icons/phase/recruiting.png"
  if (status === "planning-active") return "/icons/phase/planning-active.png";
  if (status === "planning-paused") return "/icons/phase/planning-paused.png";
  if (status === "initiating") return "/icons/phase/initiating.png";
  if (status === "expiring") return "/icons/phase/expiring.png";
  if (status === "expired") return "/icons/phase/expired.png";
  if (status === "reveal") return "/icons/phase/reveal.png";
  if (status === "failed") return "/icons/phase/failed.png";
  if (status === "success") return "/icons/phase/success.png";
};

type ComponentPropsT = {
  status: string;
  endTime?: Date;
};

const ScenarioPhase = ({
  status = "recruiting",
  endTime = new Date(),
}: ComponentPropsT) => {
  return (
    <Wrapper>
      <div className={`container ${getColorClass(status)}`}>
        {/* icon */}
        <IconWrapper>
          <img src={getIconPath(status)} alt={`${status}-icon`} />
        </IconWrapper>
        {isShowTimer(status) ? (
          <Timer endTime={endTime} />
        ) : (
          <p className="status-text">{getStatusText(status)}</p>
        )}
      </div>
    </Wrapper>
  );
};

const Timer = ({ endTime }: { endTime: Date }) => {
  const [remainingTimeText, setRemainingTimeText] = useState("00:00:00:00");

  const addZero = (number: number): string => {
    return `0${number}`.slice(-2);
  };

  const timer = setInterval(() => {
    const now = new Date().getTime();
    const distance = endTime.getTime() - now;

    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    const text = `${addZero(days)}:${addZero(hours)}:${addZero(
      minutes
    )}:${addZero(seconds)}`;

    setRemainingTimeText(text);
    if (distance < 0) {
      clearInterval(timer);
    }
  }, 1000);

  useEffect(() => {
    return () => clearInterval(timer);
  }, [timer]);

  return <p className="status-text">{remainingTimeText}</p>;
};

export default ScenarioPhase;
