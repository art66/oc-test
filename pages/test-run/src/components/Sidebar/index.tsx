import React from 'react'
import ReactDOM from 'react-dom'
import manageAppWrapLayout, { checkIsPageWithoutSidebar } from '@torn/shared/utils/manageAppWrapLayout'
import SidebarGlobal from '@torn/sidebar/src/containers/SidebarAsync'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/services/rootReducer'

import { PATHS_WITHOUT_SIDEBAR } from '../../constants'

class Sidebar extends React.Component {
  render() {
    // it's a hack for managing sidebar/non-sidebar layout for mixed CT&CTE SPA-like app.
    manageAppWrapLayout({ pagesWithoutSidebar: PATHS_WITHOUT_SIDEBAR })

    if (checkIsPageWithoutSidebar(PATHS_WITHOUT_SIDEBAR)) {
      return null
    }

    return ReactDOM.createPortal(
      <SidebarGlobal rootStore={rootStore} injector={injectReducer} />,
      document.querySelector('#sidebarroot')
    )
  }
}

export default Sidebar
