// importing interfaces from global torn's scope
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

// that is the props that component will receive only from ancestor component
export interface IOwnProps {
  mediaType?: TMediaType
  disableMobileGradient?: boolean
  isAdaptive?: boolean
}

// that is the props that component will directly from redux store to be lifted them up to its global interface
export interface IProps {
  isMapJoinedActive?: boolean
  isRowHovered?: boolean
  isRowActive?: boolean | number
  data?: boolean | string | number
  dimensions?: object
  filter?: object
}
