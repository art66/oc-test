import React from "react";
import styled from "styled-components";

import ScenarioPhase from "./ScenarioPhase";
import DifficultyLevel from "./DifficultyLevel/LevelIcon";

const Wrapper = styled.div`
  display: flex;
  background: #333333;
  border-top-right-radius: 5px;
  @media screen and (max-width: 578px) {
    flex-direction: column;
    justify-content: space-around;
  }
`;

const ScenarioPhaseWrapper = styled.div`
  max-width: 146px;
  height: 90px;
  @media screen and (max-width: 578px) {
    max-width: 100%;
  }
`;

const DetailWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-top-right-radius: 5px;
  width: calc(100% - 96px);
  @media screen and (max-width: 578px) {
    width: 100%;
  }
  @media screen and (max-width: 320px) {
    width: 100%;
  }
`;

const TitleWrapper = styled.div`
  height: 34px;
  background: transparent linear-gradient(180deg, #555555 0%, var(--unnamed-color-333333) 100%) 0% 0% no-repeat padding-box;
  background: transparent linear-gradient(180deg, #555555 0%, #333333 100%) 0% 0% no-repeat padding-box;
  border-radius: 0px 5px 0px 0px;
  padding: 0 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  > p {
    font-size: 14px;
    color: white;
  }
`;

const DescriptionWrapper = styled.div`
  flex-grow: 1;
  font-size: 12px;
  padding: 12px 16px;
  background: #333333;
  color: white;
  align-items: center;
  overflow-y: auto;
  border-top: 1px solid black;
  box-shadow: inset 0px 1px 0px #FFFFFF14;
`;

const DifficultLevelWrapper = styled.div`
  margin: 4px 0;
`;

type ComponentPropsT = {
  title: string;
  description: string;
  status: string;
  endTime?: Date;
  level: number;
};

const ScenarioInfo = ({
  title = "",
  description = "",
  status = "",
  endTime = new Date(),
  level = 1,
}: ComponentPropsT) => {
  return (
    <Wrapper>
      <ScenarioPhaseWrapper>
        <ScenarioPhase status={status} endTime={endTime} />
      </ScenarioPhaseWrapper>
      <DetailWrapper>
        <TitleWrapper>
          <p>{title}</p>
          <DifficultLevelWrapper>
            <DifficultyLevel level={level} max={10}/>
          </DifficultLevelWrapper>
        </TitleWrapper>
        <DescriptionWrapper>
          <p>{description}</p>
        </DescriptionWrapper>
      </DetailWrapper>
    </Wrapper>
  );
};

export default ScenarioInfo;
