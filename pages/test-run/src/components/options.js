import { Marker, Polygon, Coordinate } from "maptalks";
import {
  hummingWayCoords,
  sneakerShopCoords,
  gadgetStoreCoords,
  artGalleryCoords,
  uniqloCoords,
  watchBoutiqueCoords,
} from "./coordinatePolygon";
// function
export const optionsTooltip = (data) => {
  var options_data = {
    title: "Map InfoWindow",
    content: `
                <div class='tooltip'>
                    <div class='title'>${data.title}</div>
                    <p class='description'>${data.description}</p>
                </div>`,
    opacity: 0,
    autoPan: false,
    dx: data.dx,
    dy: data.dy,
    custom: true,
    autoOpenOn: null,
    autoCloseOn: "",
  };
  return options_data;
};

export const optionPolygon = () => {
  var polygon = {
    visible: true,
    editable: true,
    cursor: "pointer",
    shadowBlur: 0,
    shadowColor: "black",
    draggable: true,
    dragShadow: false,
    drawOnAxis: null,
    symbol: {
      lineWidth: 0,
      polygonFill: "rgb(0,0,0)",
      polygonOpacity: 0.01,
    },
  };
  return polygon;
};

export const createTextMarker = (textName, coordinate, align) => {
  var text = new Marker(coordinate, {
    properties: {
      name: textName,
    },
    symbol: {
      textFaceName: "Arial",
      textName: "{name}",
      textWeight: "normal",
      textStyle: "normal",
      textSize: 5,
      textFont: null, 
      textFill: "#fff",
      textOpacity: 1,
      textHaloFill: "#fff",
      textHaloRadius: 0,
      textWrapWidth: null,
      textWrapCharacter: "\n",
      textLineSpacing: 1,
      textDx: 0,
      textDy: 0,
      textHorizontalAlignment: align, 
      textVerticalAlignment: align, 
      textAlign: align, 
    },
  });
  return text;
};

// options
export var elementInMap = [
  [
    {
      polygon: new Polygon([hummingWayCoords], optionPolygon()),
      tooltipText: {
        title: "Hummingway",
        description: "(High Fashion Boutique)",
        dx: 0,
        dy: -20,
        coord: new Coordinate({ x : -8.080539373378087, y : 11.005904459659433 })
      },
    },
  ],
  [
    {
      polygon: new Polygon([sneakerShopCoords], optionPolygon()),
      tooltipText: {
        title: "That Sneaker Shop",
        description: "(Sneaker Store)",
        dx: 0,
        dy: -20,
        coord: new Coordinate({ x : 0.7585812472966609, y : 11.135287077054244 })
      },
    },
  ],
  [
    {
      polygon: new Polygon([gadgetStoreCoords], optionPolygon()),
      tooltipText: {
        title: "Gadget Store",
        description: "",
        dx: 20,
        dy: 20,
        coord: new Coordinate({ x : 12.895881204044144, y : 1.0546279422759142 })
      },
    },
  ],
  [
    {
      polygon: new Polygon([artGalleryCoords], optionPolygon()),
      tooltipText: {
        title: "Art Gallery",
        description: "",
        dx: 30,
        dy: 35,
        coord: new Coordinate({ x : 8.54228448042818, y : -9.709057068618193 })
      },
    },
  ],
  [
    {
      polygon: new Polygon([uniqloCoords], optionPolygon()),
      tooltipText: {
        title: "UNIQLO",
        description: "(Apparel Store)",
        dx:-50,
        dy: 30,
        coord: new Coordinate({ x : -20.16287016490014, y : -9.449061826881433 })
      },
    },
  ],
  [
    {
      polygon: new Polygon([watchBoutiqueCoords], optionPolygon()),
      tooltipText: {
        title: "Watch Boutique",
        description: "",
        dx: -20,
        dy: 0,
        coord: new Coordinate({ x : -20.690578836381974, y : 4.083452772038612 })
      },
    },
  ],
];
