type Message = {
  type: string;
  name: string;
  description: string;
}

export default Message
