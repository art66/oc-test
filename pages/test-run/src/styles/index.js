import Global from './global'
import Fonts from './fonts'
import Colors from './colors'

const Styles = () => {
  return (
    <>
      <Fonts />
      <Global />
      <Colors />
    </>
  );
};

export default Styles;
