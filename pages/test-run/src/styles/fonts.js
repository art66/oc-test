import { createGlobalStyle } from 'styled-components'

import FjallaOne from '../assets/fonts/FjallaOne-Regular.ttf'

export default createGlobalStyle`
  @font-face {
    font-family: 'FjallaOne-Regular';
    src: url(${FjallaOne}) format('truetype');
    font-weight: normal;
    font-style: normal;
  }
`
