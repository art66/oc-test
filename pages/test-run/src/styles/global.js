import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  html, body, #root, #App {
    width: 100%;
    height: 100%;
  }
  
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  body {
    font-family: 'Arial';
  }

  button {
    font-family: 'Arial';
  }

  input {
    font-family: 'Arial';
  }
`;
