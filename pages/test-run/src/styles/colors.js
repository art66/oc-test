import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  :root {
    --white-color: #ffffff;
    --background-color: #333333;
    --grey-light-mode: #444444;
    --red-light-mode: #D93600;
    --yellow-light-mode: #B28500;
    --green-light-mode: #698C00;
    --unnamed-color-888888: #888888;
    --green-cell: #A3D900;
    --red-cell: #FF5C26;
    --yellow-cell: #FFBF00;
    --red-dark-mode: #FF8787;
    --yellow-dark-mode: #FCC419;
    --green-dark-mode: #94D82D;
    --unnamed-color-00000000: #00000000;
    --unnamed-color-333333: #333333;
  }
`;
