import { Store } from 'redux'

// @ts-ignore
const initWS = ({ getState, dispatch }: Store) => {
  // @ts-ignore global Websocket & Centrifuge handler!!!
  const handler = new WebsocketHandler('someNamespace')

  handler.setActions({
    someAction: _payload => {}
  })
}

export default initWS
