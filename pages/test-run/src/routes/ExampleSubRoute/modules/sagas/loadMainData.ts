// @ts-nocheck
import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { loadDataSaved } from '../actions'

function* loadMainData() {
  // const payload = yield fetchUrl('/someURL.php')

  // yield put(loadDataSaved(payload))
}

export default loadMainData
