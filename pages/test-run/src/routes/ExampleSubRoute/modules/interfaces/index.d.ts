export interface IType {
  type: string
}

export interface IFetchAttempt {
  payload: object
}

export interface IFetchSaved {
  payload: object
}

export interface IState {
  payload: object
}
