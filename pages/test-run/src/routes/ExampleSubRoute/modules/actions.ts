import {
  FETCH_ATTEMPT,
  FETCH_SAVED
} from '../constants'

import {
  IType,
  IFetchAttempt,
  IFetchSaved
} from './interfaces'

export const loadDataAttempt = (payload: IFetchAttempt): IFetchAttempt & IType => ({
  payload,
  type: FETCH_ATTEMPT
})

export const loadDataSaved = (payload: IFetchAttempt): IFetchSaved & IType => ({
  payload,
  type: FETCH_SAVED
})
