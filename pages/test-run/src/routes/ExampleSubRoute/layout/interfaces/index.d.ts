import { IFetchAttempt } from '../../modules/interfaces'

export interface IProps {
  fetchAttempt: (payload: IFetchAttempt) => void
}
