import { all } from 'redux-saga/effects'

import mainRouteSaga from '../../routes/MainRoute/modules/sagas'
import exampleSubRouteSaga from '../../routes/ExampleSubRoute/modules/sagas'

export default function* rootSaga() {
  yield all([mainRouteSaga(), exampleSubRouteSaga()])
}
