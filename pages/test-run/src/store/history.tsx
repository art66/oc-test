// that's the main module for in-browser history manipulation for SPA apps
import { createBrowserHistory } from 'history'

const history = createBrowserHistory()

export default history
