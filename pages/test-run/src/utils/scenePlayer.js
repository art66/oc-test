import gsap from 'gsap'

const scenePlayer = (animations, callback) => {
  const t = gsap.timeline()
  animations.forEach((animation) => {
    animation.timelines.forEach(timeline => {
      t.add(() => callback(timeline), timeline.position)
    })
  })

  return t
}

export default scenePlayer
