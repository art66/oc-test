import React, { Component } from 'react'
import { HashRouter, Switch, Route } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import { unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'

import AppLayout from '../layout'
import { MainRoute, ExampleSubRoute } from '../routes'

import { IProps } from './interfaces'

// Our main container for app handling.
// We're using react-router-dom and connected-react-router package for SPA react-redux routing
class AppContainer extends Component<IProps> {
  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  render() {
    const { store, history } = this.props

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <HashRouter>
            <AppLayout>
              <Switch>
                <Route exact={true} path='/' component={MainRoute} />
                <Route path='/exampleSubRoute' component={ExampleSubRoute} />
              </Switch>
            </AppLayout>
          </HashRouter>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default AppContainer
