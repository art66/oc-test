export const scenario = [
  {
    scenarios: {
      scenario_id: "0001-1",
      scenario_name: "ATM Robbery",
      short_intro_text:
        "We have acquired a key, from a trusted contact, that is supposed to open up any ATM. Assemble a crew and hit the ATMs in the <Bankname> bank at the nearby mall.",
      full_intro_text: `
                We have recently acquired a key from a trusted contact, <Wikiboy>. 
                Our intel suggests that ATMs all come with a stock lock installed, and that some banks haven't invested the money to change them. 
                The <mall> mall is shutting down soon, giving us the perfect time to proceed with the operation. 
                Crowds will be minimal but there will still be security on-site. Assemble a crew, infiltrate the mall, unlock the ATMs and retrieve the money. 
                Do not get caught.`,
      short_result_text: "",
      scene_name: "Community Mall",
      scenes: [
        // First Event
        {
          id: "",
          scene: {},
          message: {
            type: "objective",
            description: "Get passed security.",
          },
        },
        {
          id: "0",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P1> and <P2> don the disguises.<P3> keeps a lookout.",
          },
          next_id: "A1",
        },
        {
          id: "A1",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P1> and <P2> decide to take the service entrance to the building.",
          },
          next_id: "A1-C1",
        },
        {
          id: "A1-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P1> and <P2> decide to take the service entrance to the building.",
          },
          failed_id: "A1-C1F",
          pass_id: "A1-C1P",
          player_skill_check: ["P1"],
        },
        {
          id: "A1-C1P",
          scene: {},
          message: {
            type: "success",
            description:
              "The security officer seems unphased. <P1> and <P2> proceed.",
          },
          next_id: "A2",
        },
        {
          id: "A1-C1F",
          scene: {},
          message: {
            type: "failed",
            description:
              "The security officer stops them in their tracks. He asks for their ID.",
          },
          next_id: "A1-C2",
        },
        {
          id: "A1-C2",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> and <P2> hand over their fake employee IDs.",
          },
          pass_id: "A1-C2P",
          failed_id: "A1-C2F",
          player_skill_check: ["P1", "P2"],
        },
        {
          id: "A1-C2P",
          scene: {},
          message: {
            type: "success",
            description:
              "The security officer takes a quick glance at the IDs. With a quick nod he waves you in.",
          },
          next_id: "A2",
        },
        {
          id: "A1-C2F",
          scene: {},
          message: {
            type: "failed",
            description:
              "The officer tells you to wait while he verifies your identities with the bank.",
          },
          next_id: "A1-C3",
        },
        {
          id: "A1-C3",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P2> agrees with the security officer. The officer proceeds with the call.",
          },
          pass_id: "A1-C3P",
          failed_id: "A1-C3F",
          player_skill_check: ["P2"],
        },
        {
          id: "A1-C3P",
          scene: {},
          message: {
            type: "success",
            description:
              "Nobody is answering. The officer decides that they probably aren't lying.",
          },
          next_id: "A3",
        },
        {
          id: "A1-C3F",
          scene: {},
          message: {
            type: "failed",
            description:
              "Nobody is answering. The officer instructs them to wait a bit longer.",
          },
          failed_id: "A1F",
        },
        {
          id: "A1F",
          scene: {},
          message: {
            type: "failed",
            description:
              "<P1> and <P2> tell the officer they will just come another day.",
          },
          is_ended: true,
        },
        // Second Event
        {
          id: "",
          scene: {},
          message: {
            type: "objective",
            description: "Unlock the first ATM.",
          },
        },
        {
          id: "A2",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "There's plenty of time left. <P1> and <P2> head to the ATMs.",
          },
          next_id: "A2-C1",
        },
        {
          id: "A2-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P2> takes out the key and attempts to unlock the ATM.",
          },
          player_skill_check: ["P2"],
          pass_id: "A2-C1P",
          failed_id: "A2-C1F",
        },
        {
          id: "A2-C1P",
          scene: {},
          message: {
            type: "success",
            description:
              "The ATM opens up. <P2> starts stuffing the cash into the duffle bag.",
          },
          next_id: "A3",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A2-C1F",
          scene: {},
          message: {
            type: "failed",
            description: "The key doesn't work. <P2> fails to unlock the ATM.",
          },
          next_id: "A2-C2",
        },
        {
          id: "A2-C2",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> takes the key and tries to unlock the ATM.",
          },
          player_skill_check: ["P1"],
          pass_id: "A2-C2P",
          failed_id: "A2-C2F",
        },
        {
          id: "A2-C2P",
          scene: {},
          message: {
            type: "success",
            description:
              "The ATM opens up. <P1> starts stuffing the cash into the duffle bag.",
          },
          next_id: "A3",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A2-C2F",
          scene: {},
          message: {
            type: "failed",
            description:
              "<P1> meets the same fate. The key doesn't work with this ATM.",
          },
          next_id: "A2-C3",
        },
        {
          id: "A2-C3",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P2> suggests they try the ATM located near the service entrance.",
          },
          player_skill_check: ["P2"],
          pass_id: "A2-C3P",
          failed_id: "A2-C3F",
        },
        {
          id: "A2-C3P",
          scene: {},
          message: {
            type: "success",
            description: "<P1> agrees with <P2>.",
          },
          next_id: "C1",
        },
        {
          id: "A2-C3F",
          scene: {},
          message: {
            type: "failed",
            description: "<P1> wishes to try again.",
          },
          failed_id: "A2F",
        },
        {
          id: "A2F",
          scene: {},
          message: {
            type: "failed",
            description:
              "<P3> calls in. Bank staff have started arriving at the mall.",
          },
          failed_id: "A2FCont",
        },
        {
          id: "A2F",
          scene: {},
          message: {
            type: "failed",
            description: "<P1> and <P2> safely escaped.",
          },
          is_ended: true,
        },
        // third Event
        {
          id: "",
          message: {
            type: "objective",
            description: "Unlock the second ATM.",
          },
        },
        {
          id: "A3",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "There's still some time left. <P1> starts unlocking the next ATM along.",
          },
          next_id: "A3-C1",
        },
        {
          id: "A3-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P1> grabs the key from <P2> and attempts to unlock the next ATM.",
          },
          player_skill_check: ["P1"],
          pass_id: "A3-C1P",
          failed_id: "A3-C1F",
        },
        {
          id: "A3-C1P",
          scene: {},
          message: {
            type: "success",
            description:
              "The ATM opens up. <P1> starts placing the cash into the bag.",
          },
          next_id: "A4",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A3-C1F",
          scene: {},
          message: {
            type: "failed",
            description: "The key doesn't work. <P1> fails to unlock the ATM.",
          },
          pass_id: "A3-C2P",
        },
        {
          id: "A3-C2",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P2> takes the key and attempts to open up the ATM.",
          },
          pass_id: "A3-C2P",
          failed_id: "A3-C2F",
          player_skill_check: ["P2"],
        },
        {
          id: "A3-C2P",
          scene: {},
          message: {
            type: "success",
            description: "<P2> manages to unlock and open up the ATM.",
          },
          next_id: "A4",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A3-C2F",
          scene: {},
          message: {
            type: "failed",
            description: "<P2> fails to unlock the ATM.",
          },
          next_id: "A3-C3",
        },
        {
          id: "A3-C3",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> forcefully pushes the key in.",
          },
          pass_id: "A3-C3P",
          failed_id: "A3-C3F",
          player_skill_check: ["P1"],
        },
        {
          id: "A3-C3P",
          scene: {},
          message: {
            type: "success",
            description: "Finally, the key works and ATM opens up.",
          },
          next_id: "C1",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A3-C3F",
          scene: {},
          message: {
            type: "failed",
            description: "<P1> fails to open the ATM. <P2> grabs the key.",
          },
          next_id: "A3-C4",
        },
        {
          id: "A3-C4",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P3> calls in. Bank staff have started arriving at the mall.",
          },
          pass_id: "A3-C4P",
          failed_id: "A3-C4F",
          player_skill_check: ["P2"],
        },
        {
          id: "A3-C4P",
          scene: {},
          message: {
            type: "success",
            description:
              "<P1> and <P2> both get on their feet and sneak out unnoticed.",
          },
          next_id: "A6",
        },
        {
          id: "A3-C4F",
          scene: {},
          message: {
            type: "failed",
            description: "<P2> is focused on the lock. <P1> heads out first.",
          },
          failed_id: "A3F",
        },
        {
          id: "A3F",
          scene: {},
          message: {
            type: "failed",
            description:
              "Security arrives and detains <P2>. <P2> is arrested by the police.",
          },
          is_ended: true,
        },
        // fourth Event
        {
          id: "",
          message: {
            type: "objective",
            description: "Unlock the third ATM.",
          },
        },
        {
          id: "A4",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "There's barely any time left. <P2> starts unlocking the next ATM.",
          },
          next_id: "A4-C1",
        },
        {
          id: "A4-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P2> proceeds with unlocking the ATM.",
          },
          pass_id: "A4-C1P",
          failed_id: "A4-C1F",
          player_skill_check: ["P2"],
        },
        {
          id: "A4-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P2> proceeds with unlocking the ATM.",
          },
          pass_id: "A4-C1P",
          failed_id: "A4-C1F",
          player_skill_check: ["P2"],
        },
        {
          id: "A4-C1P",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "The third ATM opens up. <P2> packs the money into the bag.",
          },
          next_id: "A5",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A4-C1F",
          scene: {},
          message: {
            type: "failed",
            description: "The key doesn't work. <P2> fails to open the ATM.",
          },
          next_id: "A4-C2",
        },
        {
          id: "A4-C2",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P1> snatches the key and attempts to unlock the ATM.",
          },
          pass_id: "A4-C2P",
          failed_id: "A4-C2F",
          player_skill_check: ["P1"],
        },
        {
          id: "A4-C2P",
          scene: {},
          message: {
            type: "success",
            description:
              "<P1> unlocks the ATM and packs the money into the bag.",
          },
          next_id: "A6",
        },
        {
          id: "A4-C2P",
          scene: {},
          message: {
            type: "success",
            description:
              "<P1> unlocks the ATM and packs the money into the bag.",
          },
          next_id: "A6",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A4-C2F",
          scene: {},
          message: {
            type: "failed",
            description: "<P1> fails to unlock the ATM.",
          },
          next_id: "B1",
        },
        // fifth Event
        {
          id: "",
          message: {
            type: "objective",
            description: "Unlock the last ATM.",
          },
        },
        {
          id: "A5",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> thinks they can hit one last ATM. ",
          },
          next_id: "A5-C1",
        },
        {
          id: "A5-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> attempts to unlock the last ATM.",
          },
          pass_id: "A5-12P",
          failed_id: "A5-C1F",
          player_skill_check: ["P1"],
        },
        {
          id: "A5-C1P",
          scene: {},
          message: {
            type: "success",
            description:
              "<P1> unlocks the last ATM successfully and packs the money away.",
          },
          next_id: "A6",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $20,000 - 25,000",
          },
        },
        {
          id: "A5-C1F",
          scene: {},
          message: {
            type: "failed",
            description: "<P1> is unable to unlock it successfully.",
          },
          next_id: "B1",
        },
        // sixth Event
        {
          id: "",
          message: {
            type: "objective",
            description: "Contact P3.",
          },
        },
        {
          id: "A6",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> and <P2> head to the meet up point.",
          },
          next_id: "A7",
        },
        {
          id: "A6Cont",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P3> isn't there.",
          },
        },
        // seventh Event
        {
          id: "",
          message: {
            type: "objective",
            description: "Get away.",
          },
        },
        {
          id: "A7",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P2> calls <P3> and tells them to bring the van.",
          },
          next_id: "A7-C1",
        },
        {
          id: "A7-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P3> reaches to grab his parking ticket, but it is missing.",
          },
          pass_id: "A7-C2P",
          failed_id: "A7-C1F",
          player_skill_check: ["P3"],
        },
        {
          id: "A7-C1P",
          scene: {},
          message: {
            type: "success",
            description:
              "<P3> breaks through the parking gate. Nobody seems to notice.",
          },
          next_id: "A7S1",
        },
        {
          id: "A7-C1F",
          scene: {},
          message: {
            type: "failed",
            description:
              "<P3> breaks through the parking gate. An alarm blares from the parking booth.",
          },
          next_id: "A7S2",
        },
        {
          id: "A7S1",
          scene: {},
          message: {
            type: "success",
            description: "The van arrives. The crew board the van.",
          },
          next_id: "A7S1Cont",
        },
        {
          id: "A7S1Cont",
          scene: {},
          message: {
            type: "success",
            description: "<P3> drives them off.",
          },
          is_success: true,
        },
        {
          id: "A7S2",
          scene: {},
          message: {
            type: "success",
            description: "<P1> and <P2> flee on their own.",
          },
          is_success: true,
        },
        {
          id: "A7S2Cont",
          scene: {},
          message: {
            type: "success",
            description: "<P3> drives away to divert the attention elsewhere.",
          },
        },
        // eighth Event
        {
          id: "",
          message: {
            type: "objective",
            description: "Convince the crew to leave.",
          },
        },
        {
          id: "B1",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P3> calls in to check on the status of the operation.",
          },
          next_id: "B1-C1",
        },
        {
          id: "B1-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description:
              "<P3> urges the crew to get out of the building before bank staff arrive.",
          },
          pass_id: "B1-C1P",
          failed_id: "B1-C1F",
          player_skill_check: ["P3"],
        },
        {
          id: "B1-C1P",
          scene: {},
          message: {
            type: "success",
            description:
              "<P1> and <P2> agree and proceed to pack up and leave.",
          },
          next_id: "A6",
        },
        {
          id: "B1-C1F",
          scene: {},
          message: {
            type: "success",
            description:
              "Bank security have arrived and have spotted the two crew members.",
          },
          failed_id: "B1F",
        },
        {
          id: "B1F",
          scene: {},
          message: {
            type: "failed",
            description:
              "<P1> and <P2> are detained. They are both arrested by the police.",
          },
          is_ended: true,
        },
        // ninth Event
        {
          id: "",
          message: {
            type: "objective",
            description: "Unlock the new ATM machine.",
          },
        },
        {
          id: "C1",
          scene: {},
          message: {
            type: "success",
            description: "<P1> and <P2> head towards the service entrance.",
          },
          next_id: "C1-C1",
        },
        {
          id: "C1Cont",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> calls <P3> to stand by.",
          },
        },
        {
          id: "C1-C1",
          scene: {},
          message: {
            type: "skill-checking",
            description: "<P1> attempts to unlock this last ATM.",
          },
          pass_id: "C1-C1P",
          failed_id: "C1-C1F",
          player_skill_check: ["P1"],
        },
        {
          id: "C1-C1P",
          scene: {},
          message: {
            type: "success",
            description: "The ATM unlocks. <P1> begins packing the cash away.",
          },
          next_id: "C1S1",
        },
        {
          id: "",
          message: {
            type: "system-message",
            description: "Gain $50,000 - 70,000",
          },
        },
        {
          id: "C1-C1F",
          scene: {},
          message: {
            type: "failed",
            description: "<P1> fails to unlock the ATM.",
          },
          next_id: "C1S2",
        },
        {
          id: "C1S1",
          scene: {},
          message: {
            type: "success",
            description: "The van arrives. <P1> and <P2> hop into the back.",
          },
          is_success: true,
        },
        {
          id: "C1S1Cont-1",
          scene: {},
          message: {
            type: "skill-checking",
            description: "They speed away and escape the area.",
          },
        },
        {
          id: "C1S1Cont-2",
          scene: {},
          message: {
            type: "skill-checking",
            description: "Security are completely unaware of what's occurred.",
          },
        },
        {
          id: "C1S2",
          scene: {},
          message: {
            type: "success",
            description:
              "The van arrives. <P1> and <P2> rush out empty handed.",
          },
          is_success: true,
        },
        {
          id: "C1S2Cont",
          scene: {},
          message: {
            type: "skill-checking",
            description: "They hop into the van and escape the area.",
          },
        },
      ],
      required_item: "ATM key",
      required: true,
      players: [
        {
          id: 1,
          role: "Robber",
        },
        {
          id: 2,
          role: "Robber",
        },
        {
          id: 3,
          role: "Lookout",
        },
      ],
      planning_time: 72, // hours
      cash_gains: {
        min: 50000,
        max: 100000,
      },
      respect_gains: "TBD",
      crime_exp_req: 10000,
    },
  },
];
