export const animations = [
  [
    {
      ref: "scene_1",
      timelines: [
        {
          type: "animation",
          animation: {
            target: "item2",
            vars: {
              symbol: { opacity: 0 },
              // duration: 0,
            },
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P1-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-14.04052734375, 7.623886853120069, 0],
                  [-11.29248046875, 5.75237013917328, 1000000],
                  [-11.29248046875, 5.75237013917328, 5000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "story",
          story: {
            type: "objective",
            name: "Don Vito Crescendo",
            description: "Rob The Mall",
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P2-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-1.1865234375, -0.7909904981540024, 0],
                  [-3.2958984375, -1.1864386394451856, 300000],
                  [-4.74609375, -1.4500404973607885, 600000],
                  [-6.1962890625, -2.2406396093827254, 900000],
                  [-9.0966796875, -2.8991526985043095, 1200000],
                  [-10.283203125, -2.635788574166611, 1500000],
                  [-10.9423828125, -1.0546279422758573, 1800000],
                  [-11.07421875, 0, 2100000],
                  [-11.4697265625, 3.225691524418056, 2400000],
                  [-11.4697265625, 3.225691524418056, 4000000],
                ],
              },
              duration: 7,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "sliding_door1",
            vars: {
              translate: [2, 0],
              duration: 2,
            },
          },
          position: 3,
        },
        {
          type: "animation",
          animation: {
            target: "door1",
            vars: {
              degree: 90,
              duration: 2,
            },
          },
          position: 3,
        },
        {
          type: "animation",
          animation: {
            target: "door1",
            vars: {
              degree: -90,
              duration: 2,
            },
          },
          position: 6,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Open door." },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P1-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-11.29248046875, 5.75237013917328, 0],
                  [-11.04052734375, 0.75237013917328, 1000000],
                  [-11.04052734375, 0.75237013917328, 2000000],
                  [-10.4150390625, -0.7909904981540024, 2400000],
                  [-9.8876953125, -2.108898659243124, 2800000],
                  [-8.0419921875, -2.108898659243124, 3200000],
                  [-5.009765625, -1.4500404973607885, 3600000],
                  [-2.900390625, -1.318243056862002, 4000000],
                  [-1.318359375, -0.659165146289439, 4400000],
                  [1.4501953125, -0.3955046715319952, 4800000],
                  [4.482421875, 0.13183582116661796, 5200000],
                  [5.009765625, 0.26367094433666693, 5600000],
                  [5.009765625, 0.26367094433666693, 7000000],
                  [5.009765625, 0.26367094433666693, 9000000],
                ],
              },
              duration: 11,
              ease: "power3.easeOut",
            },
          },
          position: 5,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Walk through door." },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P2-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-11.4697265625, 3.225691524418056, 0],
                  [-10.4150390625, -0.7909904981540024, 2400000],
                  [-9.8876953125, -2.108898659243124, 2800000],
                  [-8.0419921875, -2.108898659243124, 3200000],
                  [-5.009765625, -1.4500404973607885, 3600000],
                  [-2.900390625, -1.318243056862002, 4000000],
                  [-1.318359375, -0.659165146289439, 4400000],
                  [1.4501953125, -0.3955046715319952, 4800000],
                  [4.482421875, 0.00183582116661796, 5200000],
                  [5.009765625, 0.03367094433666693, 7000000],
                ],
              },
              duration: 9,
              ease: "power3.easeOut",
            },
          },
          position: 7,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Find item." },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "sliding_door1",
            vars: {
              translate: [-2, 0],
              duration: 2,
            },
          },
          position: "=-1",
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Close the door." },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "item1",
            vars: {
              symbol: { opacity: 0 },
              duration: 2,
            },
          },
          position: 14,
        },
        {
          type: "story",
          story: { type: "success", description: "Achieve item." },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P2-3",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [5.009765625, 0.03367094433666693, 0],
                  [4.482421875, 0.00183582116661796, 300000],
                  [1.4501953125, -0.3955046715319952, 600000],
                  [-1.318359375, -0.659165146289439, 900000],
                  [-2.900390625, -1.318243056862002, 1200000],
                  [-5.009765625, -1.4500404973607885, 1500000],
                  [-8.0419921875, -2.108898659243124, 1800000],
                  [-9.8876953125, -2.108898659243124, 2100000],
                  [-10.4150390625, -0.7909904981540024, 2400000],
                  [-11.4697265625, 3.225691524418056, 3000000],
                  [-11.4697265625, 3.225691524418056, 5000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: 16,
        },
        {
          type: "animation",
          animation: {
            target: "P1-3",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [5.009765625, 0.26367094433666693, 0],
                  [5.009765625, 0.26367094433666693, 300000],
                  [4.482421875, 0.13183582116661796, 600000],
                  [1.4501953125, -0.3955046715319952, 900000],
                  [-1.318359375, -0.659165146289439, 1200000],
                  [-2.900390625, -1.318243056862002, 1500000],
                  [-5.009765625, -1.4500404973607885, 1800000],
                  [-8.0419921875, -2.108898659243124, 2100000],
                  [-9.8876953125, -2.108898659243124, 2400000],
                  [-10.4150390625, -0.790990498154002, 2700000],
                  [-10.4150390625, -0.790990498154002, 3000000],
                  [-10.4150390625, -0.790990498154002, 5000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "+=0",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Get back to the door.",
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "sliding_door1",
            vars: {
              translate: [2, 0],
              duration: 2,
            },
          },
          position: 19,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Open door." },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P1-4",
            vars: {
              motionPath: {
                path: [
                  [-10.4150390625, -0.790990498154002, 0],
                  [-11.29248046875, 5.75237013917328, 1000000],
                  [-14.04052734375, 7.623886853120069, 3000000],
                ],
              },
              duration: 3,
              ease: "power3.easeOut",
            },
          },
          position: 21,
        },
        {
          type: "animation",
          animation: {
            target: "P2-4",
            vars: {
              motionPath: {
                path: [
                  [-11.4697265625, 3.225691524418056, 0],
                  [-11.29248046875, 4.75237013917328, 1000000],
                  [-13.54052734375, 7.023886853120069, 3000000],
                ],
              },
              duration: 3,
              ease: "power3.easeOut",
            },
          },
          position: "+=0",
        },
        {
          type: "story",
          story: { type: "system-message", description: "Mission complete." },
          position: "=+0",
        },
      ],
    },
  ],
  [
    {
      ref: "scene_10",
      timelines: [
        {
          type: "animation",
          animation: {
            target: "door1",
            vars: {
              degree: 90,
              duration: 2,
              centerRotate: 'start'
            },
          },
          position: 3,
        },
      ]
    },
    {
      ref: "scene_10",
      timelines: [
        {
          type: "animation",
          animation: {
            target: "door1",
            vars: {
              degree: -90,
              duration: 2,
              centerRotate: 'start'
            },
          },
          position: 6,
        },
      ]
    },
  ],
  [
    {
      ref: "scene_2",
      timelines: [
        {
          type: "animation",
          animation: {
            target: "item1",
            vars: {
              symbol: { opacity: 0 },
              // duration: 0,
            },
          },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "P1-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-14.04052734375, 7.623886853120069, 0],
                  [-11.29248046875, 5.75237013917328, 1000000],
                  [-11.29248046875, 5.75237013917328, 5000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "story",
          story: {
            type: "objective",
            name: "Don Vito Crescendo",
            description: "Rob The Mall",
          },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "P2-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-12.185969025594204, -0.3955046715319952, 0],
                  [-12.449734606341849, -1.7136116598836395, 600000],
                  [-12.185969025594204, -3.294082228312817, 900000],
                  [-11.130906702603397, -3.951940856157563, 1200000],
                  [-9.68019600849118, -4.609278084409823, 1500000],
                  // [-10.283203125, -2.635788574166611, 1500000],
                  // [-10.9423828125, -1.0546279422758573, 1800000],
                  // [-11.07421875, 0, 2100000],
                  // [-11.4697265625, 3.225691524418056, 2400000],
                  // [-11.4697265625, 3.225691524418056, 4000000],
                ],
              },
              duration: 7,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "sliding_door1",
            vars: {
              translate: [2, 0],
              duration: 2,
            },
          },
          position: 3,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Open door." },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "P1-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-11.29248046875, 5.75237013917328, 0],
                  [-11.04052734375, 0.75237013917328, 1000000],
                  [-11.04052734375, 0.75237013917328, 2000000],
                  [-10.4150390625, -0.7909904981540024, 2400000],
                  [-9.8876953125, -2.108898659243124, 2800000],
                  [-8.0419921875, -2.108898659243124, 3200000],
                  [-5.009765625, -1.4500404973607885, 3600000],
                  [-2.900390625, -1.318243056862002, 4000000],
                  [-1.318359375, -0.659165146289439, 4400000],
                  [1.4501953125, -0.3955046715319952, 4800000],
                  [2.848669077023601, -0.13183582116661796, 5000000],
                  [3.50808302889277, -0.659165146289439, 5200000],
                  [3.7718486096405286, -1.318243056862002, 5400000],
                  [3.50808302889277, -1.977146553712572, 5600000],
                  // [4.482421875, 0.13183582116661796, 5200000 ],
                  // [5.009765625, 0.26367094433666693, 5600000 ],
                  // [5.009765625, 0.26367094433666693, 7000000 ],
                  // [5.009765625, 0.26367094433666693, 9000000 ],
                  // [5.009765625, 0, 10000000],
                  // [5.537109375, -0.7909904981540024,11000000],
                  // [5.80078125, -1.7136116598836395,12000000],
                  // [5.80078125, -2.2406396093827254, 13000000]
                ],
              },
              duration: 11,
              ease: "power3.easeOut",
            },
          },
          position: 5,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Walk through door." },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "P2-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-9.68019600849118, -4.609278084409823, 0],
                  [-8.888899266248131, -4.083452772038612, 2000000],
                  [-7.701954152883673, -3.294082228312817, 2400000],
                  [-5.855595087649817, -2.108898659243124, 2800000],
                  [-3.745470441668431, -1.318243056862002, 3200000],
                  [-1.2396974245654064, -0.659165146289439, 3600000],
                  [1.5298411732852628, -0.26367094433666693, 4000000],
                  [3.6399658192666493, -0.13183582116661796, 4400000],
                  [3.903731400014294, -0.13183582116661796, 4800000],
                  [4.299379771135818, -0.13183582116661796, 5000000],
                  // [4.482421875, 0.00183582116661796, 5200000 ],
                  // [5.009765625, 0.03367094433666693, 7000000 ],
                ],
              },
              duration: 9,
              ease: "power3.easeOut",
            },
          },
          position: 7,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Find item." },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "sliding_door1",
            vars: {
              translate: [-2, 0],
              duration: 2,
            },
          },
          position: "=-1",
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Close the door." },
          position: "=+0",
        },

        // {
        //   type: "animation",
        //   animation: {
        //     target: "item1",
        //     "vars": {
        //       "symbol": { opacity: 0 },
        //       "duration": 2,
        //     },
        //   },
        //   "position": 14
        // },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 2 waiting for Player 1 to talk with guard.",
          },
          position: 14,
        },
        {
          type: "story",
          story: {
            type: "success",
            description: "Player 1 successfully talked to the guard.",
          },
          position: 16,
        },

        {
          type: "animation",
          animation: {
            target: "P2-3",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [4.299379771135818, -0.13183582116661796, 0],
                  [4.482421875, 0.00183582116661796, 300000],
                  [1.4501953125, -0.3955046715319952, 600000],
                  [-1.318359375, -0.659165146289439, 900000],
                  [-2.900390625, -1.318243056862002, 1200000],
                  [-5.009765625, -1.4500404973607885, 1500000],
                  [-8.0419921875, -2.108898659243124, 1800000],
                  [-9.8876953125, -2.108898659243124, 2100000],
                  [-10.4150390625, -0.7909904981540024, 2400000],
                  [-11.4697265625, 3.225691524418056, 3000000],
                  [-11.4697265625, 3.225691524418056, 5000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "P1-3",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [3.50808302889277, -1.977146553712572, 0],
                  [5.009765625, 0.26367094433666693, 150000],
                  [5.009765625, 0.26367094433666693, 300000],
                  [4.482421875, 0.13183582116661796, 600000],
                  [1.4501953125, -0.3955046715319952, 900000],
                  [-1.318359375, -0.659165146289439, 1200000],
                  [-2.900390625, -1.318243056862002, 1500000],
                  [-5.009765625, -1.4500404973607885, 1800000],
                  [-8.0419921875, -2.108898659243124, 2100000],
                  [-9.8876953125, -2.108898659243124, 2400000],
                  [-10.4150390625, -0.790990498154002, 2700000],
                  [-10.4150390625, -0.790990498154002, 3000000],
                  [-10.4150390625, -0.790990498154002, 5000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "+=0",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Get back to the door.",
          },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "sliding_door1",
            vars: {
              translate: [2, 0],
              duration: 2,
            },
          },
          position: 19,
        },
        {
          type: "story",
          story: { type: "skill-checking", description: "Open door." },
          position: "=+0",
        },

        {
          type: "animation",
          animation: {
            target: "P1-4",
            vars: {
              motionPath: {
                path: [
                  [-10.4150390625, -0.790990498154002, 0],
                  [-11.29248046875, 5.75237013917328, 1000000],
                  [-14.04052734375, 7.623886853120069, 3000000],
                ],
              },
              duration: 3,
              ease: "power3.easeOut",
            },
          },
          position: 21,
        },

        {
          type: "animation",
          animation: {
            target: "P2-4",
            vars: {
              motionPath: {
                path: [
                  [-11.4697265625, 3.225691524418056, 0],
                  [-11.29248046875, 4.75237013917328, 1000000],
                  [-13.54052734375, 7.023886853120069, 3000000],
                ],
              },
              duration: 3,
              ease: "power3.easeOut",
            },
          },
          position: "+=0",
        },
        {
          type: "story",
          story: { type: "system-message", description: "Mission complete." },
          position: "=+0",
        },
        // {
        //   type: "animation",
        //   animation: {
        //     target: "wall1",
        //       "vars": {
        //       "symbol": { opacity: 0 },
        //       "duration": 1,
        //     },
        //   },
        //   "position": 4
        // },
        // {
        //   type: "story",
        //   story: { type: 'system-message', description: 'Bob acquired [the prison key]', },
        //   position: "=+1",
        // },
        // {
        //   type: "story",
        //   story: { type: 'failed', description: 'Bob reaches into the warden’s jacket pocket and finds the prison key.', },
        //   position: "=+1",
        // },
      ],
    },
  ],
  [
    {
      ref: "scene_3",
      timelines: [
        {
          type: "animation",
          animation: {
            target: "item1",
            vars: {
              symbol: { opacity: 0 },
              // duration: 0,
            },
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P1-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [5.1416015625, 3.425691524418056, 0],
                  [6.1962890625, 5.134714634014443, 1000000],
                  [7.119140625, 6.1842461612805835, 2000000],
                  [8.8330078125, 6.839169626342795, 3000000],
                  [10.810546875, 6.970049417296224, 4000000],
                  [12.5244140625, 6.1842461612805835, 5000000],
                  [13.3154296875, 5.134714634014443, 6000000],
                  [13.5791015625, 3.425691524418056, 7000000],
                  [13.974609375, 1.3182430568620305, 8000000],
                  [13.0517578125, -0.3955046715319952, 9000000],
                ],
              },
              duration: 10,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },

        {
          type: "story",
          story: {
            type: "objective",
            name: "Don Vito Crescendo",
            description: "Rob The Mall",
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P3-1",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [-19.775390625, -11.393879232967407, 0],
                  [-18.984375, -8.92848706266551, 1000000],
                  [-16.2158203125, -11.65223640411537, 2000000],
                  [-19.775390625, -11.393879232967407, 3000000],
                ],
              },
              duration: 10,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "P7-1",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [-3.2958984375, 11.781325296112271, 0],
                  [-1.318359375, 11.393879232967407, 1000000],
                  [1.318359375, 10.876464994816303, 2000000],
                  [4.0869140625, 10.228437266155964, 3000000],
                  [5.80078125, 11.65223640411537, 4000000],
                ],
              },
              duration: 10,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "P2-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-26.2353515625, 12.425847783029127, 0],
                  [-21.09375, 11.135287077054244, 1000000],
                  [-17.666015625, 9.968850608546092, 2000000],
                ],
              },
              duration: 7,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 1 explores the mall.",
          },
          position: "=+0",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 3 and Player 4 look around the mall.",
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P2-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-17.666015625, 9.968850608546092, 0],
                  [-14.8974609375, 9.709057068618193, 500000],
                  [-13.447265625, 9.058702156392172, 1000000],
                  [-12.2607421875, 8.01571599786908, 2000000],
                  [-11.9970703125, 6.708253968671528, 3000000],
                  [-11.7333984375, 5.790896812871978, 4000000],
                ],
              },
              duration: 7,
              ease: "power3.easeOut",
            },
          },
          position: "=+7",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 2 walk to the door.",
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "sliding_door1",
            vars: {
              translate: [2, 0],
              duration: 2,
            },
          },
          position: 10,
        },

        {
          type: "animation",
          animation: {
            target: "P1-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [13.0517578125, -0.3955046715319952, 0],
                  [10.4150390625, -0.9228116626856888, 1000000],
                  [8.701171875, -1.1864386394451856, 2000000],
                  [6.9873046875, -1.318243056862002, 3000000],
                  [5.9326171875, -1.977146553712572, 4000000],
                  [5.6689453125, -3.1624555302378496, 5000000],
                  [6.4599609375, -3.6888551431470376, 6000000],
                  [6.1962890625, -5.134714634014443, 7000000],
                  [5.9326171875, -5.922044619883309, 8000000],
                  [4.74609375, -5.922044619883309, 9000000],
                  [3.427734375, -5.922044619883309, 10000000],
                ],
              },
              duration: 19,
              ease: "power3.easeOut",
            },
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P2-3",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-11.7333984375, 5.790896812871978, 0],
                  [-11.4697265625, 3.6888551431470376, 500000],
                  [-11.337890625, 1.7136116598836395, 1000000],
                  [-11.2060546875, -0.13183582116661796, 1500000],
                  [-10.9423828125, -2.5040852618529073, 2000000],
                  [-8.8330078125, -2.8991526985043095, 2500000],
                  [-7.119140625, -1.977146553712572, 3000000],
                  [-4.482421875, -1.318243056862002, 3500000],
                  [-1.9775390625, -0.3955046715319952, 4000000],
                  [0.791015625, 0.3955046715320236, 4500000],
                  [2.63671875, 0.6591651462894674, 5000000],
                  [4.3505859375, -0.3955046715319952, 5500000],
                  [3.5595703125, -1.1864386394451856, 6000000],
                  [3.2958984375, -1.8453839885731895, 6500000],
                ],
              },
              duration: 20,
              ease: "power3.easeOut",
            },
          },
          position: "=+4",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 2 walk through the door.",
          },
          position: "=+0",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 1 walk to the target item.",
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "item2",
            vars: {
              symbol: { opacity: 0 },
              duration: 2,
            },
          },
          position: "=+10",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 2 attracts the attention of the guard.",
          },
          position: "=-4",
        },
        {
          type: "story",
          story: {
            type: "success",
            description: "Player 1 successfully stole the item.",
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P1-3",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [3.427734375, -5.922044619883309, 0],
                  [5.009765625, -5.528510525692781, 500000],
                  [6.4599609375, -5.134714634014443, 1000000],
                  [6.7236328125, -3.557282726541274, 1500000],
                  [6.064453125, -2.767477951092104, 2000000],
                  [6.4599609375, -1.4500404973607885, 2500000],
                  [5.9326171875, -0.5273363048114845, 3000000],
                  [3.2958984375, 0.6591651462894674, 3500000],
                  [0.6591796875, 0.3955046715320236, 4000000],
                  [-0.1318359375, -1.4500404973607885, 4500000],
                  [-1.1865234375, -6.44631774945762, 5000000],
                  [-1.9775390625, -11.65223640411537, 5500000],
                ],
              },
              duration: 20,
              ease: "power3.easeOut",
            },
          },
          position: "=+5",
        },

        {
          type: "animation",
          animation: {
            target: "P2-4",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [3.2958984375, -1.8453839885731895, 0],
                  [3.69140625, -0.3955046715319952, 500000],
                  [2.373046875, 0.13183582116661796, 1000000],
                  [0.3955078125, -0.26367094433666693, 1500000],
                  [-0.52734375, -1.977146553712572, 2000000],
                  [-0.9228515625, -5.528510525692781, 2500000],
                  [-1.9775390625, -8.537565350804016, 3000000],
                  [-2.5048828125, -10.876464994816303, 3500000],
                ],
              },
              duration: 25,
              ease: "power3.easeOut",
            },
          },
          position: "=+5",
        },
        {
          type: "story",
          story: {
            type: "skill-checking",
            description: "Player 1 and Player 2 leave the mall.",
          },
          position: "=+0",
        },
        {
          type: "story",
          story: { type: "system-message", description: "Mission complete." },
          position: "=+4",
        },
      ],
    },
  ],
];
