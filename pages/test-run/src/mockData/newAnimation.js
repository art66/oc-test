export const animations = [
  [
    {
      ref: "scene_1",
      timelines: [
        {
          type: "animation",
          animation: {
            target: "car-1",
            vars: {
              //   degree: 90,
              //   duration: 2,
              //   centerRotate: "start",
              hide: true,
              motionPath: {
                path: [
                  [-15.691992187499977, 17.119792500787042, 0],
                  [-17.082421875000023, 15.020348309312595, 500000],
                  [-18.274218750000045, 12.318535941662134, 1000000],
                  [-19.86328125, 9.58861736880661, 1500000],
                  [-22.34619140625, 4.464713015717649, 2000000],
                  [-23.438671874999955, 1.886670675444151, 2500000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "P4-1-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [26.680126953124955, 0.4965758144294625, 0],
                  [24.680126953124955, 0.4965758144294625, 1000000],
                  [23.488330078125045, -0.19863241461720804, 2000000],
                  [22.395849609374977, 0, 3000000],
                  [20.608154296875, 0.29794787590063265, 4000000],
                  [18.174902343749977, -0.4965758144294625, 5000000],
                ],
              },
              duration: 6,
              ease: "power3.easeOut",
            },
          },
          position: "=+2",
        },
        {
          type: "animation",
          animation: {
            target: "P3-1-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-17.430029296874977, -2.184431546370689, 0],
                  [-17.430029296874977, -2.184431546370689, 100000],
                ],
              },
              duration: 20,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "NPC1-1-1",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [4.419580078125023, 2.581352855004326, 0],
                  [4.419580078125023, 2.581352855004326, 100000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "NPC1-2-1",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [15.245068359374955, -0.5958876949228795, 0],
                  [15.245068359374955, -0.5958876949228795, 100000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "NPC1-3-1",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [14.947119140624977, -1.6881, 0],
                  [14.947119140624977, -1.6881, 100000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: 0,
        },
        {
          type: "animation",
          animation: {
            target: "sliding_door2",
            vars: {
              translate: [1.8, -0.9],
              duration: 0.5,
            },
          },
          position: 7,
        },
        {
          type: "animation",
          animation: {
            target: "P4-1-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [18.174902343749977, -0.4965758144294625, 0],
                  [18.174902343749977, -0.9931143307373134, 500000],
                  [17.976269531249955, -1.688134608778796, 1000000],
                  [17.777636718750045, -2.0851840833221047, 1500000],
                  [17.579003906250023, -2.6805645606664257, 2000000],
                  [16.784472656250045, -2.4821334037305576, 2500000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+1",
        },
        {
          type: "animation",
          animation: {
            target: "sliding_door2",
            vars: {
              translate: [-1.8, 0.9],
              duration: 0.5,
            },
          },
          position: 9,
        },
        {
          type: "animation",
          animation: {
            target: "P4-2-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-19.333593781571835, 2.2836724522908014, 0],
                  [-18.43974612532179, 2.4821334037305576, 1000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+1",
        },
        {
          type: "animation",
          animation: {
            target: "P4-3-1",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-19.333593781571835, 1.092414276817948, 0],
                  [-20.22744143782188, -0.8938114012776168, 1000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P4-1-3",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [16.784472656250045, -2.4821334037305576, 0],
                  [16.618945280928187, -3.275655522344721, 500000],
                  [16.420312468428165, -3.6721903666307014, 1000000],
                  [16.122363249678187, -3.8703929500047423, 2000000],
                  [15.228515593428142, -3.4739437693648654, 3000000],
                  [13.937402312178165, -2.878963547909592, 4000000],
                  [13.639453093428187, -3.6721903666307014, 5000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+3",
        },
        {
          type: "animation",
          animation: {
            target: "P4-2-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-18.43974612532179, 2.4821334037305576, 0],
                  [-19.73085940657188, 3.275655522344749, 1000000],
                  [-20.624707062821813, 3.275655522344749, 1500000],
                  [-21.319921906571835, 2.6805645606663973, 2000000],
                  [-21.617871125321813, 1.6881346087787676, 2500000],
                  [-21.121289094071813, -1.0924142768179195, 3000000],
                  [-20.128125031571813, -2.2836724522907446, 3500000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+2",
        },
        {
          type: "animation",
          animation: {
            target: "P4-3-2",
            vars: {
              hide: true,
              motionPath: {
                path: [
                  [-20.22744143782188, -0.8938114012776168, 0],
                  [-20.326757844071835, -1.5888588225646174, 1000000],
                  [-20.028808625321858, -2.0851840833221047, 1500000],
                  [-19.532226594071858, -2.6805645606664257, 2000000],
                  [-19.134960969071813, -3.176496519654876, 2500000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P4-2-3",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [-20.128125031571813, -2.2836724522907446, 0],
                  [-20.22744143782188, -3.275655522344721, 500000],
                  [-19.73085940657188, -3.7712973077116203, 800000],
                  [-19.333593781571835, -4.266656631826777, 1000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+5",
        },
        {
          type: "animation",
          animation: {
            target: "P4-3-3",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [-19.134960969071813, -3.176496519654876, 0],
                  [-18.638378937821813, -3.275655522344721, 1000000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+0",
        },
        {
          type: "animation",
          animation: {
            target: "P3-1-2",
            vars: {
              hide: false,
              motionPath: {
                path: [
                  [-17.430029296874977, -2.184431546370689, 0],
                  [-18.141796906571813, -4.16760913397934, 500000],
                ],
              },
              duration: 5,
              ease: "power3.easeOut",
            },
          },
          position: "=+0",
        },
      ],
    },
  ],
];
