import Layout from '@torn/questionnaire/src/components/Layout'
import ContentTitle from '@torn/shared/components/ContentTitle'
import InfoBox from '@torn/shared/components/InfoBox'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getInitialState } from '../actions'
import CasusFoederis from '../components/CasusFoederis'

const topLinks = [
  {
    icon: 'folder-icon',
    title: 'Your Items',
    link: '/item.php'
  },
  {
    icon: 'missions-top-icon',
    title: 'Missions',
    link: '/loader.php?sid=missions'
  }
]

class App extends Component {
  componentDidMount() {
    this.props.getInitialState()
  }

  getLinks = () => (this.props.completed ? topLinks : [])

  render() {
    const { completed, gotContent, infoBox } = this.props

    return (
      <div>
        <ContentTitle title='The Contract' links={this.getLinks()} />
        {infoBox ? <InfoBox msg={infoBox.msg} color={infoBox.color} /> : null}
        {gotContent && (
          <Layout>
            <CasusFoederis />
          </Layout>
        )}
        {completed && (
          <Layout>
            <h2 className='completed'>This agreement is now signed</h2>
            <h3>Please return it to the overseer at your earliest convenience</h3>
          </Layout>
        )}
      </div>
    )
  }
}

App.propTypes = {
  completed: PropTypes.bool,
  getInitialState: PropTypes.func,
  gotContent: PropTypes.bool,
  infoBox: PropTypes.object
}

const mapStateToProps = state => ({
  completed: state.app.completed,
  gotContent: state.app.gotContent,
  infoBox: state.app.infoBox
})

const mapActionsToProps = {
  getInitialState
}

export default connect(mapStateToProps, mapActionsToProps)(App)
