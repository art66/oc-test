import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'

export const Page1 = ({ playername, subjectNumber }) => (
  <div>
    <p>
      <b>1)</b> For the purposes of this agreement <b>'Confidential Activities'</b> shall mean any and all instructions
      given to the recipient, {playername}, hitherto known as <b>Test Subject {subjectNumber}</b>, by{' '}
      <b>Professor Amanda Ravenscroft</b>, hitherto known as <b>The Overseer</b>.
    </p>
    <p>
      <b>2)</b> <b>Test Subject {subjectNumber}</b> agrees not to make public any mission statements, correspondence,
      hazardous materials or bodily fluids in relation to said <b>Confidential Activities.</b>
    </p>
    <p>
      <b>3)</b> Should <b>Test Subject {subjectNumber}</b> find his or herself incarcerated, hospitalised, or otherwise
      detained, they shall refute all knowledge of these <b>Confidential Activities</b> and endeavour to acquire all
      bodily fluids provided whilst imprisoned.
    </p>
    <p>
      <b>4)</b> Whilst <b>The Overseer</b> shall attempt to maintain a duty of care to{' '}
      <b>Test Subject {subjectNumber}</b>, they accept no legal responsibility for loss of limbs, life, dignity or
      mental cognitivity.{' '}
    </p>
    <p>
      <b>5)</b> Should <b>Test Subject {subjectNumber}</b> encounter other <b>Test Subjects</b> belonging to{' '}
      <b>The Overseer</b>, they agree to avoid interference or interaction with them, especially with{' '}
      <b>Test Subject 104</b>, regardless of how distressing their screams may be.{' '}
    </p>
    <p>
      <b>6)</b> <b>Test Subject 228</b> agrees to undertake each <b>Confidential Activity</b> to the best of their
      abilities. Failure to do so may incur a penalty from the following punishments as deemed appropriate:
    </p>
    <b>
      <ul>
        <li>a) Nonpayment of agreed funds</li>
        <li>b) Withdrawal of food and water</li>
        <li>c) Enhanced distress techniques</li>
        <li>d) Termination of subject's consciousness</li>
      </ul>
    </b>
  </div>
)

Page1.propTypes = {
  playername: PropTypes.string,
  subjectNumber: PropTypes.number
}

Page1.defaultProps = {
  playername: '{playername}',
  subjectNumber: '{subjectNumber}'
}

const mapStateToProps = state => ({
  playername: state.app.playername,
  subjectNumber: state.app.subjectNumber,
  pageNumber: state.app.pageNumber,
  mediaType: state.browser.mediaType
})

export default connect(mapStateToProps)(Page1)
