import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { submit } from '../../actions'
import Footer from '@torn/questionnaire/src/components/Footer'
import Page1 from './Page1'
import Page2 from './Page2'

export class CasusFoederis extends Component {
  componentDidMount() {
    require.ensure(
      ['paper'],
      require => {
        require('paper')
      },
      'paper'
    )
  }
  _handPages() {
    const { pageNumber, mediaType } = this.props
    if (mediaType === 'desktop') {
      if (pageNumber === 1) {
        return <Page1 />
      } else {
        return <Page2 />
      }
    } else {
      return (
        <div>
          <Page1 />
          <Page2 />
        </div>
      )
    }
  }
  _submit = () => {
    const { submit } = this.props
    submit('signed')
  }
  render() {
    const { signed } = this.props
    return (
      <div className="casus-foederis-wrap">
        <div className="casus-foederis-body">{this._handPages()}</div>
        <Footer submit={this._submit} disabled={!signed} type="casusfoederis" />
      </div>
    )
  }
}

CasusFoederis.propTypes = {
  pageNumber: PropTypes.number,
  mediaType: PropTypes.string,
  signed: PropTypes.bool,
  submit: PropTypes.func
}

const mapStateToProps = state => ({
  pageNumber: state.app.pageNumber,
  signed: state.app.signed,
  mediaType: state.browser.mediaType
})

const mapActionsToProps = {
  submit
}

export default connect(mapStateToProps, mapActionsToProps)(CasusFoederis)
