import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { sign } from '../../actions'

const MIN_PATH_LENGTH = 100
const CANVAS_SIZE = {
  desktop: { width: '450px', height: '100px' },
  tablet: { width: '366px', height: '100px' },
  phone: { width: '300px', height: '100px' }
}
/* global paper */
export class Page2 extends Component {
  init = () => {
    const { sign } = this.props
    paper.setup(this.refs.canvas)
    const tool = new paper.Tool()
    let totalLength = 0
    let path
    tool.onMouseDown = event => {
      if (path) {
        path.selected = false
      }
      path = new paper.Path({
        segments: [event.point],
        strokeColor: '#868695',
        strokeWidth: 1
      })
    }
    tool.onMouseDrag = event => {
      path.add(event.point)
    }
    tool.onMouseUp = event => {
      path.simplify(1)
      totalLength += path.length
      if (totalLength > MIN_PATH_LENGTH) {
        sign()
      }
    }
  }
  componentDidMount() {
    require.ensure(
      ['paper'],
      require => {
        const paper = require('paper')
        window.paper = paper
        this.init()
      },
      'paper'
    )
  }
  componentWillUnmount() {
    paper.project.remove()
  }
  render() {
    const { playername, subjectNumber, timestamp, mediaType } = this.props

    return (
      <div>
        <p>
          <b>7)</b> <b>Test Subject {subjectNumber}</b> henceforth signs over all material, commercial, and legal rights
          to <b>The Overseer</b> regarding their intellectual property, monies gained in the pursuit of{' '}
          <b>The Overseer's</b> goals, and each of their major internal organs, minus the pancreas, in perpetuity.
        </p>
        <p>
          <b>8)</b> <b>The Overseer</b> retains the right to generate partial replicas of{' '}
          <b>Test Subject {subjectNumber}</b> using acquired genetic material. These shall subsequently be known as{' '}
          <b>Test Subject {subjectNumber} a</b>, <b>{subjectNumber} b</b>, and so on and so forth, should these
          experimental life­forms prove viable.{' '}
        </p>
        <p>
          <b>9)</b> <b>Test Subject {subjectNumber}</b> agrees to retain a satisfactory physique and health status.
          Should <b>Test Subject {subjectNumber}</b> present his or herself to the Overseer in less than peak condition,
          they agree to rigorous physical exertion until either fitness or expiration is achieved, whichever is sooner.{' '}
        </p>
        <p>
          <b>10)</b> The agreed punishment for the breaking of one or more clauses in this agreement is the fracturing
          of one or more limbs belonging to <b>Test Subject {subjectNumber}</b>.{' '}
        </p>
        <br />
        <br />
        <div className="signature-wrap clearfix">
          <span>Signed, Miss Amanda Ravenscroft</span>
          <img className="signature-amanda" src="images/v2/missions/questionnaire/amanda_signature.png" />
        </div>
        <div className="signature-wrap clearfix">
          <span className="signature-autor">Signed, {playername}</span>
          <div className="signature" style={{ ...CANVAS_SIZE[mediaType] }}>
            <canvas ref="canvas" {...CANVAS_SIZE[mediaType]} />
          </div>
        </div>
        <div className="q-date">Date: {timestamp}</div>
      </div>
    )
  }
}

Page2.propTypes = {
  playername: PropTypes.string,
  subjectNumber: PropTypes.number,
  timestamp: PropTypes.string,
  mediaType: PropTypes.string,
  sign: PropTypes.func
}

Page2.defaultProps = {
  playername: '{playername}',
  subjectNumber: '{subjectNumber}',
  timestamp: '26/07/2016'
}

const mapStateToProps = state => ({
  playername: state.app.playername,
  subjectNumber: state.app.subjectNumber,
  timestamp: state.app.timestamp,
  mediaType: state.browser.mediaType
})

const mapActionsToProps = {
  sign
}

export default connect(mapStateToProps, mapActionsToProps)(Page2)
