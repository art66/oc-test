export function fetchUrl(url, data) {
  const config = {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
  }
  if (data) {
    config.method = 'post'
    config.body = new FormData()
    config.body.append('data', JSON.stringify(data))
  }

  return fetch(url, config)
    .then(response =>
      response.text().then(text => {
        try {
          var json = JSON.parse(text)
          if (json.reult && json.result.error) {
            var error = json.reuslt.error
          }
        } catch (e) {
          error = 'Server responded with: ' + text
        }
        if (error) {
          throw new Error(error)
        } else {
          return json
        }
      })
    )
    .catch(e => console.error(e))
}
