import { handleActions } from 'redux-actions'

const initialState = {
  gotContent: false,
  pageNumber: 1,
  answers: {}
}

const _filter = data => ({
  playername: data.playername,
  status: data.status,
  timestamp: data.timestamp,
  subjectNumber: data.subjectNumber
})
const isCompleted = action => {
  const status = action.payload.json.status
  if (status === 'deny' || status === 'signed') {
    return true
  }
  return false
}
export default handleActions(
  {
    'data loaded'(state, action) {
      if (!isCompleted(action)) {
        return {
          ...state,
          ..._filter(action.payload.json),
          gotContent: true
        }
      } else {
        return {
          ...status,
          completed: true
        }
      }
    },
    'bring info box'(state, action) {
      return {
        ...state,
        infoBox: {
          msg: action.payload.msg,
          color: action.payload.color
        }
      }
    },
    'hide info box'(state, action) {
      return {
        ...state,
        infoBox: null
      }
    },
    'go to page'(state, action) {
      return {
        ...state,
        pageNumber: action.payload || state.pageNumber
      }
    },
    submited(state, action) {
      if (!isCompleted(action)) {
        return {
          ...state,
          ..._filter(action.payload.json),
          pageNumber: 1
        }
      } else {
        return {
          ...status,
          gotContent: false,
          completed: true
        }
      }
    },
    sign(state) {
      return {
        ...state,
        signed: true
      }
    }
  },
  initialState
)
