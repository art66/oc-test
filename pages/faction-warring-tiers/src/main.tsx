import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import App from './components/App'
import IWindow from './interfaces/IWindow'

declare const window: IWindow

window.renderWarringTiers = (size, factionId, warringTiersID, rootId) => {
  const MOUNT_NODE = document.getElementById(rootId)

  ReactDOM.render(<App size={size} factionId={factionId} warringTiersID={warringTiersID} />, MOUNT_NODE)
}

// This code is excluded from production bundle
// @ts-ignore
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    // eslint-disable-next-line global-require
    const MOUNT_NODE = document.getElementById('faction-warring-tiers-root')

    const renderError = error => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    const render = () => {
      try {
        window.renderWarringTiers('medium', 4113, 'test', 'faction-warring-tiers-root')
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept()
    render()
  }
}
