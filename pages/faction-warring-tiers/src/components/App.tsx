import React from 'react'
import WarringTiers from '@torn/shared/components/WarringTiers'

interface IProps {
  factionId: number
  size: 'big' | 'medium'
  warringTiersID: string
}

export const App = (props: IProps) => {
  return (
    <div className='faction-warring-tiers'>
      <WarringTiers size={props.size} factionId={props.factionId} warringTiersID={props.warringTiersID} />
    </div>
  )
}

export default App
