export default interface IWindow extends Window {
  renderWarringTiers?: (size: 'big' | 'medium', factionId: number, warringTiersID: string, rootId: string) => void
}
