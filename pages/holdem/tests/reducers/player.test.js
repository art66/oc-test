import playersReducer from '../../src/reducers/players'
import * as actionTypes from '../../src/actions/actionTypes'

describe('player reducer', () => {
  test('adds a player', () => {
    let state
    state = playersReducer(
      {
        '1': {
          userID: 1,
          playername: 'a'
        }
      },
      {
        type: actionTypes.PLAYER_JOIN,
        serverState: {
          eventType: 'playerJoin',
          player: {
            '8': {
              userID: 1435333,
              playername: 'toshykazu'
            }
          }
        }
      }
    )
    expect(state).toEqual({
      '1': {
        userID: 1,
        playername: 'a'
      },
      '8': {
        userID: 1435333,
        playername: 'toshykazu'
      }
    })
  })
})
