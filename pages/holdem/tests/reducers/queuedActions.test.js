import queuedAction from '../../src/reducers/queuedAction'
import * as actionTypes from '../../src/actions/actionTypes'

describe('test TOGGLE_QUEUE', () => {
  test('if state.action and action is undefined, result is undefined', () => {
    const state = {}
    const action = { type: actionTypes.TOGGLE_QUEUE }
    expect(queuedAction(state, action)).toEqual({})
  })
  test('if state.action is defined and action is undefined, result is undefined', () => {
    const state = { action: 'someaction' }
    const action = { type: actionTypes.TOGGLE_QUEUE }
    expect(queuedAction(state, action)).toEqual({})
  })
  test('if state.action is defined and action is defined, result is defined', () => {
    const state = { action: 'someaction' }
    const action = { type: actionTypes.TOGGLE_QUEUE, payload: 'anotheraction' }
    expect(queuedAction(state, action)).toEqual({ action: 'anotheraction' })
  })
  test('if state.action === action, result is гтвуаштув', () => {
    const state = { action: 'someaction' }
    const action = { type: actionTypes.TOGGLE_QUEUE, payload: 'someaction' }
    expect(queuedAction(state, action)).toEqual({})
  })
})
