import React from 'react'
import renderer from 'react-test-renderer'
// import this in order test to run
import * as containers from '../../src/containers'
import { SID_WITH_SIDEBAR } from '../../src/constants'

import { TopMenu } from '../../src/components'

describe('TopMenu test', () => {
  it('Should be two links "Pop out" and "Expand"', () => {
    const component = renderer.create(<TopMenu sid={SID_WITH_SIDEBAR} />)
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('Should be one link "Unexpand"', () => {
    const component = renderer.create(<TopMenu />)
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('No links at all', () => {
    const component = renderer.create(<TopMenu popped />)
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})
