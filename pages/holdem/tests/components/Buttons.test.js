import React from 'react'
import renderer from 'react-test-renderer'
import { Buttons } from '../../src/components/Buttons/Buttons'

describe('Buttons test', () => {
  it('should contain nothing when there is no props.buttons', () => {
    const component = renderer.create(<Buttons buttons={[]} />)
    const tree = component.toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('should render buttons', () => {
    const buttons = [
      {
        label: 'Bet',
        action: () => {}
      },
      {
        label: 'Call',
        action: () => {}
      },
      {
        label: 'Fold',
        action: () => {}
      }
    ]
    const component = renderer.create(<Buttons buttons={buttons} />)
    const tree = component.toJSON()

    expect(tree).toMatchSnapshot()
  })
})
