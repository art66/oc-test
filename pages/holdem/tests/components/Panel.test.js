import React from 'react'
import '../../src/containers'
import { Panel, Log } from '../../src/components'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { shallow } from 'enzyme'

configure({ adapter: new Adapter() })

describe('(Component) Panel', () => {
  test('Renders logs if show.logs === true ', () => {
    const props = {
      mediaType: 'large',
      holdem: {},
      ui: {
        logs: true
      },
      actions: {}
    }
    const _wrapper = shallow(<Panel {...props} />)
    const logs = _wrapper.find(Log)
    expect(logs).toBeDefined()
  })
})
