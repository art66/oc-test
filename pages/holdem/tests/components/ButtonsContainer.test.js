/* eslint-disable max-nested-callbacks */
import { mergeProps } from '../../src/containers/Buttons'

describe('containers/Buttons mergeProps function', () => {
  test('buttons array contains same amount of entries as acceptedActions', () => {
    const stateProps = {
      acceptedActions: ['raise', 'call', 'fold'],
      amISat: true
    }

    expect(mergeProps(stateProps, {}).buttons.length).toEqual(3)
  })
  describe('buttons shape', () => {
    test('my turn case', () => {
      const stateProps = {
        acceptedActions: ['raise', 'call', 'fold'],
        amISat: true,
        canLeaveNow: false,
        betAmount: 10,
        myTurn: true
      }

      const expected = [{ label: 'raise to  $10' }, { label: 'call' }, { label: 'fold' }]

      const result = mergeProps(stateProps, {}).buttons

      result.forEach((_, i) => {
        expect(result[i].label).toEqual(expected[i].label)
        expect(result[i].queueable).toEqual(false)
      })
    })
    test('not my turn case', () => {
      const stateProps = {
        acceptedActions: ['raise', 'call', 'fold'],
        amISat: true,
        canLeaveNow: false,
        betAmount: 10,
        myTurn: false
      }

      const expected = [{ label: 'raise $10' }, { label: 'call' }, { label: 'fold' }]

      const result = mergeProps(stateProps, {}).buttons

      result.forEach((_, i) => {
        expect(result[i].label).toEqual(expected[i].label)
        expect(result[i].queueable).toEqual(true)
      })
    })
  })
})
