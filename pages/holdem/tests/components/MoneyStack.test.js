import 'array.prototype.fill'
import { factoring } from '../../src/components/MoneyStack/helpers.js'

describe('(Component) MoneyStack', () => {
  it('factoring test', () => {
    expect(factoring(15)).toEqual([[10], [1, 1, 1, 1, 1]])
    expect(factoring(1002)).toEqual([[1000], [1, 1]])
    expect(factoring(10502)).toEqual([[10000], [100, 100, 100, 100, 100], [1, 1]])
    expect(factoring(100000000)).toEqual([[100000000]])
    expect(factoring(100030009)).toEqual([[100000000], [10000, 10000, 10000], [1, 1, 1, 1, 1, 1, 1, 1, 1]])
  })
})
