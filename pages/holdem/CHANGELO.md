# Holdem - Torn


# 1.0.1
 * Fixed irregular fast card flipping.

# 1.0.0
 * Refactored app structure.
 * Fixed HMR support.
