import React, { PropsWithChildren } from 'react'
import { Provider } from 'react-redux'

type TProps = PropsWithChildren<{ store: any }>

const StoreProvider = ({ children, store }: TProps) => <Provider store={store}>{children}</Provider>

export default StoreProvider
