import { InfoBox } from '@torn/shared/hoc'
import PersistentInfoBox from '@torn/shared/components/InfoBox'
import React, { Component } from 'react'
import { GatewayProvider } from 'react-gateway'
import { connect, ConnectedProps } from 'react-redux'
import { initApp } from '../actions'
import DevPanel from '../components/DevPanel/DevPanel'
import TableLayout from '../components/TableLayout/TableLayout'
import TopMenu from '../components/TopMenu/TopMenu'
import Defs from '../svg/Defs'

class App extends Component<TReduxProps> {
  componentDidMount() {
    this.props.initApp()
  }

  render() {
    return (
      <GatewayProvider>
        <>
          <Defs />
          <TopMenu />
          <PersistentInfoBox
            msg={
              '<div class="info-msg-cont gray common-action-error border-round"><div class="info-msg border-round">'
              + '<i class="info-icon"></i><div class="delimiter"><div class="msg right-round">'
              + 'Will you help us to improve Torn Poker? We\'re looking for your ideas and feedback, '
              + 'join us in discussions on the '
              + '<a target="blank" href="/forums.php#/p=forums&f=69&b=0&a=0">Poker forum!</a></div></div></div></div>'
            }
          />
          <InfoBox />
          <TableLayout />
          <DevPanel />
        </>
      </GatewayProvider>
    )
  }
}

const connector = connect(null, {
  initApp
})

type TReduxProps = ConnectedProps<typeof connector>

export default connector(App)
