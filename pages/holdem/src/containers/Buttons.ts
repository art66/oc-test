import { shortMoney } from '@torn/shared/utils'
import isEqual from 'lodash/isEqual'
import { connect } from 'react-redux'
import { makeMove, sitOutImBackClick, toggleQueue, waitButtonClick } from '../actions'
import { Buttons } from '../components/Buttons/Buttons'
import {
  canLeaveNow,
  getBetAmount,
  getMaxBet,
  getMinBet,
  getPlayerCallAmount,
  getPreselectButtons,
  getSitOutButtons,
  getWaitButtons,
  isItMyTurn
} from '../selectors'
import { TAppState } from '../store/rootReducer'

const mapStateToProps = (state: TAppState) => ({
  waitButtons: getWaitButtons(state),
  preselectButtons: getPreselectButtons(state),
  sitOutButtons: getSitOutButtons(state),
  betAmount: getBetAmount(state),
  callAmount: getPlayerCallAmount(state),
  canLeaveNow: canLeaveNow(state),
  myTurn: isItMyTurn(state),
  queuedAction: state.queuedAction.action,
  buttonPressed: state.queuedAction.buttonPressed,
  minBet: getMinBet(state),
  maxBet: getMaxBet(state)
})

type TStateProps = ReturnType<typeof mapStateToProps>

const mapActionsToProps = {
  makeMove,
  toggleQueue,
  waitButtonClick,
  sitOutImBackClick
}

type TActionProps = typeof mapActionsToProps

const getLabel = (name, minBet, betAmount, callAmount, maxBet) => {
  if (name === 'Allin') {
    return 'All-in'
  }
  if (['Bet', 'Raise to'].includes(name)) {
    return betAmount === maxBet ? 'All-in' : `${name} ${shortMoney(betAmount || minBet, '$')}`
  }

  if (name === 'Call') {
    return callAmount ? `${name} ${shortMoney(callAmount || minBet, '$')}` : name
  }

  return name
}

// export for testing purpose,
// returns only Leave button if user is not sat at the table && can Leave
// or map over acceptedActions otherwise
export const mergeProps = (props: TStateProps, actions: TActionProps) => {
  let buttons = []

  if (props.canLeaveNow) {
    const { waitbb, paybutton } = props.waitButtons

    buttons = [
      {
        label: 'Leave',
        action: () => actions.makeMove(['leave'])
      },
      {
        label: props.sitOutButtons.name,
        action: () => actions.sitOutImBackClick({ step: props.sitOutButtons.action })
      }
    ]

    if (waitbb) {
      buttons.push({
        label: waitbb.name,
        queueable: true,
        queued: waitbb.pressed,
        action: () => actions.waitButtonClick({ step: waitbb.step, pressed: !waitbb.pressed })
      })
    }

    if (paybutton) {
      buttons.push({
        label: paybutton.name,
        queueable: true,
        queued: paybutton.pressed,
        action: () => actions.waitButtonClick({ step: paybutton.step, pressed: !paybutton.pressed })
      })
    }
  } else {
    buttons = props.preselectButtons.map(({ name, values: actionButtons }) => {
      const queued = !props.myTurn && isEqual(props.queuedAction, actionButtons)
      const action = () => {
        if (!props.myTurn) {
          actions.toggleQueue(actionButtons)
        } else {
          actions.makeMove(actionButtons)
        }
      }

      return {
        label: getLabel(name, props.minBet, props.betAmount, props.callAmount, props.maxBet),
        pressed: actionButtons.some(a => a === props.buttonPressed),
        queueable: !props.myTurn,
        queued,
        action
      }
    })
  }

  return { buttons: buttons.filter(({ label }) => label) }
}

export default connect(mapStateToProps, mapActionsToProps, mergeProps)(Buttons)
