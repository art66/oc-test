// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'additionalBox': string;
  'avatarPositioner': string;
  'default': string;
  'detailsBox': string;
  'folded': string;
  'globalSvgShadow': string;
  'hand': string;
  'icon': string;
  'money': string;
  'moneyIcon': string;
  'name': string;
  'opponent': string;
  'player': string;
  'playerPositioner': string;
  'playerPositioner-0': string;
  'playerPositioner-1': string;
  'playerPositioner-2': string;
  'playerPositioner-3': string;
  'playerPositioner-4': string;
  'playerPositioner-5': string;
  'playerPositioner-6': string;
  'playerPositioner-7': string;
  'playerPositioner-8': string;
  'playerWrapper': string;
  'pot': string;
  'potString': string;
  'state': string;
  'statusIcon': string;
  'winner': string;
}
export const cssExports: CssExports;
export default cssExports;
