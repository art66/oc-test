import { select, withKnobs } from '@storybook/addon-knobs'
import React from 'react'
import StoreProvider from '../../containers/StoreProvider'
import { configureStore } from '../../store/createStore'
import { mockState } from '../../fixtures/store.mock'
import { setAppType } from '../../actions'
import { SelfPlayerLayout, TProps } from './SelfPlayerLayout'
import { OpponentPlayerLayout } from './OpponentPlayerLayout'

type TAppearance = Extract<TProps, 'appearance'>

const store = configureStore(mockState)

const playerProps: TProps = {
  appearance: 'active' as const,
  userID: 2554198,
  playername: 'romankr_test71',
  status: 'All In',
  money: 10000000,
  pot: 25,
  avatar: '/images/profile_man.jpg',
  hand: ['spades-10', 'diamonds-9'],
  statusIcon: 'call'
}

const withProvider = story => <StoreProvider store={store}>{story()}</StoreProvider>

const WrapperClassNames = ['full', 'sidebar']

const PlayerSelfTemplate = props => {
  const wrapperClassName = select('Full screen?', WrapperClassNames, 'full')

  store.dispatch(setAppType(wrapperClassName))
  return (
    <div
      id='appWrapper'
      className={wrapperClassName}
      style={{
        position: 'relative',
        background: 'url(https://dev-www.torn.com/casino/holdem/images/floor_bg.png)',
        width: '80vw',
        height: '500px'
      }}
    >
      <SelfPlayerLayout
        {...props}
        appearance={select<TAppearance>(
          'Appearance',
          ['winner', 'active', 'default', 'folded'],
          playerProps.appearance as TAppearance
        )}
        hand={select('Show cards', { yes: playerProps.hand, no: [] }, playerProps.hand)}
      />
    </div>
  )
}

const PlayerOpponentTemplate = props => {
  const wrapperClassName = select('Full screen?', WrapperClassNames, 'full')

  store.dispatch(setAppType(wrapperClassName))
  return (
    <div
      id='appWrapper'
      className={wrapperClassName}
      style={{
        position: 'relative',
        background: 'url(https://dev-www.torn.com/casino/holdem/images/floor_bg.png)',
        width: '80vw',
        height: '500px'
      }}
    >
      <OpponentPlayerLayout
        {...props}
        showCards={true}
        appearance={select<TAppearance>(
          'Appearance',
          ['winner', 'active', 'default', 'folded'],
          playerProps.appearance as TAppearance
        )}
        hand={select('Show cards', { yes: playerProps.hand, no: [] }, playerProps.hand)}
      />
    </div>
  )
}

export const Self = PlayerSelfTemplate.bind({})
export const Opponent = PlayerOpponentTemplate.bind({})

export default {
  title: 'Holdem/Player',
  decorators: [withProvider, withKnobs],
  args: playerProps
}
