import { toMoney } from '@torn/shared/utils'
import cn from 'classnames'
import React from 'react'
import { selectComponent } from '../../hoc/selectComponent'
import { TAppearance, THand } from '../../interfaces'
import { MoneyGrayIcon } from '../../svg/MoneyGrayIcon'
import { MoneyGreenIcon } from '../../svg/MoneyGreenIcon'
import { PlayerIcon } from '../../svg/PlayerIcon'
import { CardPair } from '../CardPair/CardPair'
import { renderStatusIcon } from '../Icons/StatusIcons'
import { PlayerAvatar } from '../PlayerAvatar/PlayerAvatar'
import style from './styles/opponent.cssmodule.scss'
import { useStatus } from './hooks'

export type TProps = {
  appearance: TAppearance
  userID: number
  showCards: boolean
  avatar: string
  hand: THand
  money: number
  playername: string
  pot: number
  status: string
  statusIcon: string
  index: number
}

const OpponentPlayerDesktopLayout = (props: TProps) => (
  <div className={cn(style.playerPositioner, style[`playerPositioner-${props.index}`])}>
    <div className={`${style.opponent} ${style[props.appearance]}`} id={`player-${props.userID}`}>
      <div className={style.playerWrapper}>
        <PlayerAvatar avatar={props.avatar} appearance={props.appearance} className={style.avatarPositioner} framed />
        <CardPair type='opponent' pair={props.hand} className={style.hand} />
        <div className={style.state}>{useStatus(props)}</div>
        <div className={style.detailsBox}>
          <p className={style.name}>{props.playername}</p>
          <p className={style.money}>{toMoney(props.money, '$')}</p>
        </div>
      </div>
    </div>
  </div>
)

const OpponentPlayerTabletLayout = (props: TProps) => (
  <div className={cn(style.playerPositioner, style[`playerPositioner-${props.index}`])}>
    <div className={`${style.opponent} ${style[props.appearance]}`} id={`player-${props.userID}`}>
      <div className={style.playerWrapper}>
        <div className={style.additionalBox}>
          <i className={style.moneyIcon}>
            <MoneyGreenIcon />
          </i>
          <div className={style.potString}>{!isNaN(props.money) && toMoney(props.money, '$')}</div>
        </div>
        <div className={style.detailsBox}>
          <PlayerAvatar avatar={props.avatar} appearance={props.appearance} className={style.avatarPositioner} />
          <CardPair type='opponent' pair={props.hand} className={style.hand} />
          <p className={style.state}>{useStatus(props)}</p>
          <p className={style.name}>{props.playername}</p>
        </div>
      </div>
    </div>
  </div>
)

const OpponentPlayerMobileLayout = (props: TProps) => {
  const status = useStatus(props)

  return (
    <div className={cn(style.playerPositioner, style[`playerPositioner-${props.index}`])}>
      <div className={`${style.opponent} ${style[props.appearance]}`} id={`player-${props.userID}`}>
        {props.hand.length ? (
          <>
            <div className={style.name}>
              <i className={style.icon}>
                <PlayerIcon />
              </i>
              {props.playername}
            </div>
            <CardPair type='opponent' pair={props.hand} className={style.hand} />
          </>
        ) : (
          <>
            <div className={style.state}>
              <i className={style.statusIcon}>{renderStatusIcon(props.statusIcon)}</i>
              {status}
            </div>
            <div className={style.name}>
              <i className={style.icon}>
                <PlayerIcon />
              </i>
              {props.playername}
            </div>
            {!isNaN(props.money) && (
              <div className={style.money}>
                <i className={style.icon}>
                  <MoneyGreenIcon />
                </i>
                {toMoney(props.money, '$')}
              </div>
            )}
            {!isNaN(props.pot) && (
              <div className={style.pot}>
                <i className={style.icon}>
                  <MoneyGrayIcon />
                </i>
                {toMoney(props.pot, '$')}
              </div>
            )}
          </>
        )}
      </div>
    </div>
  )
}

export const OpponentPlayerLayout = selectComponent({
  desktop_full: OpponentPlayerDesktopLayout,
  desktop_sidebar: OpponentPlayerTabletLayout,
  tablet_full: OpponentPlayerTabletLayout,
  tablet_sidebar: OpponentPlayerMobileLayout,
  mobile: OpponentPlayerMobileLayout
})
