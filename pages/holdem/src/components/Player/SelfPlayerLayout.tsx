import { toMoney } from '@torn/shared/utils'
import React from 'react'
import { selectComponent } from '../../hoc/selectComponent'
import { TAppearance, THand } from '../../interfaces'
import { MoneyGrayIcon } from '../../svg/MoneyGrayIcon'
import { MoneyGreenIcon } from '../../svg/MoneyGreenIcon'
import { PlayerIcon } from '../../svg/PlayerIcon'
import { CardPair } from '../CardPair/CardPair'
import { renderStatusIcon } from '../Icons/StatusIcons'
import { PlayerAvatar } from '../PlayerAvatar/PlayerAvatar'
import style from './styles/self.cssmodule.scss'
import { useStatus } from './hooks'

export type TProps = {
  appearance: TAppearance
  userID: number
  avatar: string
  money: number
  playername: string
  pot: number
  hand: THand
  status: string
  statusIcon: string
}

const SelfPlayerDesktopLayout = (props: TProps) => {
  const status = useStatus(props)

  return (
    <div className={`${style.self} ${style[props.appearance]}`} id={`player-${props.userID}`}>
      <div className={style.playerWrapper}>
        <div className={style.flexRow}>
          <PlayerAvatar avatar={props.avatar} appearance={props.appearance} className={style.avatarPositioner} framed />
          <CardPair type='self' pair={props.hand} className={style.hand} />
        </div>
        <div className={style.detailsBox}>
          <p>
            {props.playername}
            {status ? ` - ${status}` : ''}
          </p>
          <p>{toMoney(props.money, '$')}</p>
        </div>
      </div>
    </div>
  )
}

const SelfPlayerTabletLayout = (props: TProps) => (
  <div className={`${style.self} ${style[props.appearance]}`} id={`player-${props.userID}`}>
    <div className={style.playerWrapper}>
      <div className={style.state}>
        <div className={style.pot}>
          <i className={style.moneyIcon}>
            <MoneyGreenIcon />
          </i>
          {!isNaN(props.money) && toMoney(props.money, '$')}
        </div>
        {useStatus(props)}
      </div>
      <div className={style.details}>
        {props.avatar && (
          <PlayerAvatar avatar={props.avatar} appearance={props.appearance} className={style.avatarPositioner} framed />
        )}
        <CardPair type='self' pair={props.hand} className={style.hand} />
      </div>
    </div>
  </div>
)

const SelfPlayerMobileLayout = (props: TProps) => (
  <div className={`${style.self} ${style[props.appearance]}`} id={`player-${props.userID}`}>
    <CardPair type='self' pair={props.hand} className={style.hand} />
    <div className={style.details}>
      <div className={style.detailsItem}>
        <i className={style.icon}>{renderStatusIcon(props.statusIcon)}</i>
        <p>{useStatus(props)}</p>
      </div>
      <div className={style.detailsItem}>
        <i className={style.icon}>
          <PlayerIcon />
        </i>

        <p>{props.playername}</p>
      </div>
      <div className={style.detailsItem}>
        <i className={style.icon}>
          <MoneyGreenIcon />
        </i>
        <p>{toMoney(props.money, '$')}</p>
      </div>
      {!isNaN(props.pot) && (
        <div className={style.detailsItem}>
          <i className={style.icon}>
            <MoneyGrayIcon />
          </i>
          <p>{toMoney(props.pot, '$')}</p>
        </div>
      )}
    </div>
  </div>
)

export const SelfPlayerLayout = selectComponent({
  desktop_full: SelfPlayerDesktopLayout,
  desktop_sidebar: SelfPlayerTabletLayout,
  tablet_full: SelfPlayerTabletLayout,
  tablet_sidebar: SelfPlayerMobileLayout,
  mobile: SelfPlayerMobileLayout
})
