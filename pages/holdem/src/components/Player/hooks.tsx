import { useMemo } from 'react'
import { makeGetPlayerStatus, TPlayerStatusProps } from '../../selectors/players'
import { useAppSelector } from '../../store/useAppSelector'

export const useStatus = (props: TPlayerStatusProps) => {
  const statusSelector = useMemo(makeGetPlayerStatus, [])

  return useAppSelector(state => statusSelector(state, props))
}
