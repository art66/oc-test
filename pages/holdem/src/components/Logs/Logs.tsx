import cn from 'classnames'
import React from 'react'
import { Scrollbars } from 'react-custom-scrollbars'
import { connect, ConnectedProps } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { MEDIA_TYPES } from '../../constants'
import { getLog, getTurnPlayer, getUserID, isItMyTurn } from '../../selectors'
import { TAppState } from '../../store/rootReducer'
import { renderLogIcon } from '../Icons/LogIcons'
import { getPreviousGameEndIndex } from './helpers'
import opacity from './opacityTransition.cssmodule.scss'
import scrollbarCss from './scrollbar.cssmodule.scss'
import style from './style.cssmodule.scss'
import SuiteUnicode from './SuiteUnicode/SuiteUnicode'

const SCROLL_OFFSET = 40

type TStatusProps = {
  playerName: string
  isItMyTurn: boolean
}

const Status = (props: TStatusProps) => (
  <div className={cn(style.statusWrapper)}>
    <TransitionGroup component={null}>
      {props.playerName && (
        <CSSTransition key={props.playerName} classNames={{ ...opacity }} timeout={400}>
          <div className={style.status}>
            <i className={style.thinkingIcon}>{renderLogIcon('thinking')}</i>
            <span>{props.isItMyTurn ? 'Your turn...' : `${props.playerName} is thinking...`}</span>
          </div>
        </CSSTransition>
      )}
    </TransitionGroup>
  </div>
)

type TProps = TReduxProps

type TState = {
  manualScroll: boolean
  lastOldRecordIndex: number
}

const suitsReplaceMap = {
  clubs: () => <SuiteUnicode suite='♣' />,
  spades: () => <SuiteUnicode suite='♠' />,
  diamonds: () => <SuiteUnicode suite='♦' />,
  hearts: () => <SuiteUnicode suite='♥' />
}

const suitsRegExp = /(clubs|spades|diamonds|hearts)/g

const replaceSuitWithComponent = (str: string) => {
  return str.split(suitsRegExp).map((item, index) => {
    const Component = item.match(suitsRegExp) ? suitsReplaceMap[item] : () => <span>{item}</span>

    return <Component key={index} />
  })
}

class Logs extends React.PureComponent<TProps, TState> {
  readonly state: TState = {
    manualScroll: false,
    lastOldRecordIndex: null
  }

  private readonly scrollbarRef = React.createRef<Scrollbars>()

  static getDerivedStateFromProps(nextProps) {
    return {
      lastOldRecordIndex: getPreviousGameEndIndex(nextProps.log)
    }
  }

  componentDidMount() {
    this.scrollToLatest()
  }

  componentDidUpdate() {
    if (!this.state.manualScroll) {
      this.scrollToLatest()
    }
  }

  getScrollOffset = () => {
    const { scrollTop, scrollHeight, clientHeight } = this.scrollbarRef.current.getValues()

    return this.props.mediaType === MEDIA_TYPES.TYPE_LARGE ? scrollHeight - clientHeight - scrollTop : scrollTop
  }

  onScroll = () => {
    if (this.getScrollOffset() > SCROLL_OFFSET) {
      this.setState({ manualScroll: true })
    } else {
      this.setState({ manualScroll: false })
    }
  }

  handleShowRecent = () => {
    this.setState({ manualScroll: false })
  }

  scrollToLatest = () => {
    this.props.mediaType === MEDIA_TYPES.TYPE_LARGE ?
      this.scrollbarRef.current.scrollToBottom() :
      this.scrollbarRef.current.scrollToTop()
  }

  buildMessageClassName = (userID: string, meta: string, messageIndex: number) =>
    cn(style.message, style[meta], {
      [style.old]: this.state.lastOldRecordIndex > messageIndex,
      [style.current]: this.props.userID === parseInt(userID, 10)
    })

  renderMessages = () =>
    this.props.log.map((item, index) => (
      <div
        className={this.buildMessageClassName(item.userID, item.meta, index)}
        key={`${item.timestamp}-${item.author}-${item.message}`}
      >
        <i className={style.logIcon}>{renderLogIcon(item.icon)}</i>
        <em>{item.author}</em>
        <span>{replaceSuitWithComponent(item.message)}</span>
      </div>
    ))

  renderShowRecentButton = () =>
    this.state.manualScroll && (
      <button type='button' className={style.showRecent} onClick={this.handleShowRecent}>
        Show recent
      </button>
    )

  render() {
    return (
      <div className={style.logsPositioner}>
        <div className={style.logs}>
          <div className={style.logListContainer}>
            <Scrollbars
              ref={this.scrollbarRef}
              className={style.scrollArea}
              renderTrackVertical={props => <div {...props} className={scrollbarCss.track} />}
              renderThumbVertical={props => <div {...props} className={scrollbarCss.thumb} />}
              onScrollStop={this.onScroll}
            >
              <div className={style.messagesWrap}>{this.renderMessages()}</div>
            </Scrollbars>
            {this.renderShowRecentButton()}
          </div>
          <Status playerName={this.props.turnPlayer?.playername} isItMyTurn={this.props.isItMyTurn} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: TAppState) => ({
  log: getLog(state),
  isItMyTurn: isItMyTurn(state),
  turnPlayer: getTurnPlayer(state),
  userID: getUserID(state),
  mediaType: state.browser.mediaType
})

const connector = connect(mapStateToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(Logs)
