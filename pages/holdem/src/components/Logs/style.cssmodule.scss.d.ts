// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'black': string;
  'current': string;
  'globalSvgShadow': string;
  'logIcon': string;
  'logListContainer': string;
  'logs': string;
  'logsPositioner': string;
  'message': string;
  'messagesWrap': string;
  'old': string;
  'red': string;
  'scrollArea': string;
  'showRecent': string;
  'soundWrap': string;
  'state': string;
  'status': string;
  'statusWrapper': string;
  'suite': string;
  'svgIcon': string;
  'thinkingIcon': string;
  'won': string;
}
export const cssExports: CssExports;
export default cssExports;
