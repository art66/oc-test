import cn from 'classnames'
import React from 'react'
import style from './style.cssmodule.scss'

type TProps = {
  suite: string
}

const isSuitRed = suite => ['♦', '♥'].includes(suite)

const SuiteUnicode = (props: TProps) => (
  <span className={cn(style.suite, { [style.red]: isSuitRed(props.suite) })}>{props.suite}</span>
)

export default SuiteUnicode
