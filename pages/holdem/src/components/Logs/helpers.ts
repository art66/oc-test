import { ILogRecord } from '../../interfaces'

export const getPreviousGameEndIndex = (log: ILogRecord[] = []) =>
  log.length
  - log
    .slice(0, -1)
    .reverse()
    .findIndex(el => el.meta === 'won')
  - 1
