/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { PureComponent } from 'react'
import cn from 'classnames'
import s from './index.cssmodule.scss'

interface IProps {
  label: string
  children: React.ReactNode
}

interface IState {
  expanded: boolean
}

class Dropdown extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      expanded: true
    }
  }

  _handleTrigger = () => {
    this.setState((prevState: IState) => {
      return {
        expanded: !prevState.expanded
      }
    })
  }

  render() {
    const { label, children } = this.props
    const { expanded } = this.state

    return (
      <div className={cn(s.wrapper, { [s.expanded]: expanded })}>
        <div onClick={this._handleTrigger} className={s.trigger}>
          <span className={s.label}>{label}</span>
          <span className={s.arrow} />
        </div>
        {expanded && <div className={s.content}>{children}</div>}
      </div>
    )
  }
}

export default Dropdown
