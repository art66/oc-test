import { ReactText } from 'react'
import { IPlayers, ITable } from '../../../../interfaces'

export type TPlayersCards = {
  [K in keyof IPlayers]?: ReactText[]
}

export interface IProps {
  table: ITable
  players: IPlayers
}

export interface IState {
  data: {
    turnTime: ReactText
    maxPlayers: ReactText
    testPlayers: ReactText
    blindAmount: ReactText
    currentPlayer: ReactText
    currentDealer: ReactText
    currentRound: ReactText
    cards: TPlayersCards & {
      community: ReactText[]
    }
  }
}
