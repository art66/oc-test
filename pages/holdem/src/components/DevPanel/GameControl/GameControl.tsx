import cn from 'classnames'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { IPlayer } from '../../../interfaces'
import { getPlayers, getTableState } from '../../../selectors'
import { api } from '../../../services'
import { arraysEqual } from '../../../utils'
import CardPicker from '../CardPicker/CardPicker'
import s from './index.cssmodule.scss'
import { IProps, IState, TPlayersCards } from './types'

const defaultData: Readonly<IState['data']> = {
  turnTime: '',
  maxPlayers: '',
  testPlayers: '',
  blindAmount: '',
  currentPlayer: '',
  currentDealer: '',
  currentRound: '',
  cards: {
    community: ['', '', '', '', '']
  }
}

class GameControl extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      data: defaultData
    }
  }

  componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>): void {
    const { players } = this.props
    const { data } = this.state
    const { players: prevPlayers } = prevProps
    const {
      data: { cards }
    } = prevState

    if (!arraysEqual(Object.keys(players), Object.keys(prevPlayers))) {
      const playersCards = Object.keys(players).reduce(
        (acc, key) => ({
          ...acc,
          [key]: cards[key] ?? ['', '']
        }),
        {}
      )

      this._updatePlayersCards(data, playersCards)
    }
  }

  _request = async (requestData: any) => {
    try {
      await api.gameControl(requestData)
    } catch (e) {
      console.error(e)
    }
  }

  _updatePlayersCards = (dataObject: IState['data'], playersCards: TPlayersCards) => {
    this.setState({
      data: {
        ...dataObject,
        cards: {
          community: dataObject.cards.community,
          ...playersCards
        }
      }
    })
  }

  _getPlayersForDropdown = () => {
    const { players } = this.props

    return Object.keys(players).map(key => {
      return (
        <option key={players[key].userID} value={key}>
          {players[key].playername}
        </option>
      )
    })
  }

  _handleReset = () => {
    const { players } = this.props

    const playersCards: TPlayersCards = Object.keys(players).reduce(
      (acc, key) => ({
        ...acc,
        [key]: ['', '']
      }),
      {}
    )

    this._updatePlayersCards(defaultData, playersCards)
  }

  _handleRefresh = () => {
    const {
      table: { tableID }
    } = this.props

    this._request({ step: 'refresh', tableId: tableID })
  }

  _handleSubmit = () => {
    const {
      table: { tableID }
    } = this.props
    const { data } = this.state

    this._request({ ...data, tableId: tableID })
  }

  _handleValueChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { data } = this.state

    this.setState({
      data: {
        ...data,
        [e.target.id]: Number(e.target.value)
      }
    })
  }

  _handleCommunityCardChange = (value: number, index: number) => {
    const { data } = this.state
    const { community } = data.cards

    this.setState({
      data: {
        ...data,
        cards: {
          ...data.cards,
          community: [...community.slice(0, index), value, ...community.slice(index + 1, community.length)]
        }
      }
    })
  }

  _handleUserCardChange = (value: number, userIndex: number, index: number) => {
    const { data } = this.state
    const { cards } = data

    this.setState({
      data: {
        ...data,
        cards: {
          ...cards,
          [userIndex]: [
            ...cards[userIndex].slice(0, index),
            value,
            ...cards[userIndex].slice(index + 1, cards[userIndex].length)
          ]
        }
      }
    })
  }

  _handleUserKick = userId => {
    const {
      table: { tableID }
    } = this.props

    this._request({ step: 'remove', tableId: tableID, userId })
  }

  _renderCommunityCardsPicker = () => {
    const {
      data: { cards }
    } = this.state
    const onChange = (index: number) => (value: number) => this._handleCommunityCardChange(value, index)

    return (
      <div className={cn(s.cardsFieldWrap, s.community)}>
        <div className={s.cardsFieldLabel}>
          <span>Community cards</span>
        </div>
        <div className={s.cardsFieldValueWrap}>
          <CardPicker value={cards.community[0]} onChange={onChange(0)} />
          <CardPicker value={cards.community[1]} onChange={onChange(1)} />
          <CardPicker value={cards.community[2]} onChange={onChange(2)} />
          <CardPicker value={cards.community[3]} onChange={onChange(3)} />
          <CardPicker value={cards.community[4]} onChange={onChange(4)} />
        </div>
      </div>
    )
  }

  _renderUsersCardsPicker = () => {
    const { players } = this.props
    const {
      data: { cards }
    } = this.state
    const onChange = (key: number, index: number) => (value: number) => this._handleUserCardChange(value, key, index)
    const onKick = (userId: number) => () => this._handleUserKick(userId)

    return Object.keys(players).map(key => {
      const player: IPlayer = players[key]

      return (
        <div key={player.userID} className={s.cardsFieldWrap}>
          <div className={s.cardsFieldLabel}>
            <span>{`${player.playername} cards`}</span>
          </div>
          <div className={s.cardsFieldValueWrap}>
            <CardPicker value={cards?.[key]?.[0]} onChange={onChange(+key, 0)} />
            <CardPicker value={cards?.[key]?.[1]} onChange={onChange(+key, 1)} />
            <button type='button' onClick={onKick(player.userID)}>
              Kick
            </button>
          </div>
        </div>
      )
    })
  }

  render() {
    const { data } = this.state
    const playersSelectOptions: React.ReactNodeArray = this._getPlayersForDropdown()

    return (
      <div className={s.gameControl}>
        <div className={s.inputsSection}>
          <div className={s.fieldWrap}>
            <label className={s.filedLabel} htmlFor='turnTime'>
              Turn time
            </label>
            <input
              id='turnTime'
              className={s.fieldInput}
              type='number'
              min={5}
              max={995}
              step={5}
              value={data.turnTime}
              onChange={this._handleValueChange}
            />
          </div>
          <div className={s.fieldWrap}>
            <label className={s.filedLabel} htmlFor='maxPlayers'>
              Max players
            </label>
            <input
              id='maxPlayers'
              className={s.fieldInput}
              type='number'
              min={2}
              max={9}
              value={data.maxPlayers}
              onChange={this._handleValueChange}
            />
          </div>
          <div className={s.fieldWrap}>
            <label className={s.filedLabel} htmlFor='testPlayers'>
              Test players
            </label>
            <input
              id='testPlayers'
              className={s.fieldInput}
              type='number'
              min={1}
              max={9}
              value={data.testPlayers}
              onChange={this._handleValueChange}
            />
          </div>
          <div className={s.fieldWrap}>
            <label className={s.filedLabel} htmlFor='blindAmount'>
              Blind amount
            </label>
            <input
              id='blindAmount'
              className={s.fieldInput}
              type='number'
              min={50}
              value={data.blindAmount}
              onChange={this._handleValueChange}
            />
          </div>
          <div className={s.fieldWrap}>
            <label className={s.filedLabel} htmlFor='currentPlayer'>
              Current player
            </label>
            <select
              id='currentPlayer'
              className={s.fieldInput}
              value={data.currentPlayer}
              onChange={this._handleValueChange}
            >
              <option disabled={true} label='disabled' value='disabled' />
              {playersSelectOptions}
            </select>
          </div>
          <div className={s.fieldWrap}>
            <label className={s.filedLabel} htmlFor='currentDealer'>
              Current dealer
            </label>
            <select
              id='currentDealer'
              className={s.fieldInput}
              value={data.currentDealer}
              onChange={this._handleValueChange}
            >
              <option disabled={true} label='disable' value='disabled' />
              {playersSelectOptions}
            </select>
          </div>
          <div className={s.fieldWrap}>
            <label className={s.filedLabel} htmlFor='currentRound'>
              Current round
            </label>
            <input
              id='currentRound'
              className={s.fieldInput}
              type='number'
              min={1}
              max={4}
              value={data.currentRound}
              onChange={this._handleValueChange}
            />
          </div>
        </div>
        <div className={s.cardsSection}>
          {this._renderCommunityCardsPicker()}
          {this._renderUsersCardsPicker()}
        </div>
        <div className={s.actions}>
          <button type='button' onClick={this._handleSubmit}>
            Save
          </button>
          <button type='button' onClick={this._handleReset}>
            Reset form
          </button>
          <button type='button' onClick={this._handleRefresh}>
            New game
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  table: getTableState(state),
  players: getPlayers(state)
})

export default connect(mapStateToProps)(GameControl)
