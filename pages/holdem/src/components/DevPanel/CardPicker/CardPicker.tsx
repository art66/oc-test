import React, { PureComponent, ReactText } from 'react'
import s from './index.cssmodule.scss'

interface IProps {
  value?: ReactText
  onChange: (value: number) => void
}

class CardPicker extends PureComponent<IProps> {
  static defaultProps = {
    value: null
  }

  _handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { onChange } = this.props

    onChange(Number(e.target.value))
  }

  _renderOptions = () => {
    const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    const suits = ['♠', '♦', '♥', '♣']
    const options = [<option disabled={true} key='disabled' label='disabled' value='0' />]

    let index = 1

    ranks.forEach(rank => {
      suits.forEach(suit => {
        options.push(<option key={`${rank}${suit}`} value={index}>{`${rank}${suit}`}</option>)
        index += 1
      })
    })

    return options
  }

  render() {
    const { value } = this.props

    return (
      <select className={s.cardSelect} value={value || 0} onChange={this._handleChange}>
        {this._renderOptions()}
      </select>
    )
  }
}

export default CardPicker
