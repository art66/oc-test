import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Dropdown from './Dropdown/Dropdown'
import GameControl from './GameControl/GameControl'
import PlayersControl from './PlayersControl/PlayersControl'

interface IProps {
  isDev: boolean
}

class DevPanel extends PureComponent<IProps> {
  render() {
    const { isDev } = this.props

    if ((!location.host.includes('dev') && !location.host.includes('localhost')) || !isDev) {
      return false
    }

    return (
      <div className='adminPanel'>
        <Dropdown label='Game control'>
          <GameControl />
        </Dropdown>
        <Dropdown label='Players control'>
          <PlayersControl />
        </Dropdown>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isDev: state.self.identity.isDev
})

export default connect(mapStateToProps)(DevPanel)
