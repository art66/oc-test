import { LegacyMoneyInput } from '@torn/shared/components/LegacyMoneyInput/LegacyMoneyInput'
import { shortMoney } from '@torn/shared/utils'
import cn from 'classnames'
import isEqual from 'lodash/isEqual'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { testPlayerAction, toggleQueueTestPlayer, updateWaitButtons } from '../../../../actions'
import { IPlayer, ITable } from '../../../../interfaces'
import { getTableState, getTestPlayersQueuedActions, getTurn } from '../../../../selectors'
import { sanitizeInput } from '../../../../utils/sanitizeInput'
import Button from '../../../Button/Button'
import s from './index.cssmodule.scss'

interface IProps {
  player: IPlayer
  position: number
  currentTurn: number
  table: ITable
  queuedActions: { [key in number]: any }
  makeAction: (data: any) => void
  toggleQueue: (data: { position: number; actions: string[]; amount: number; tableId: string; userID: number }) => void
  waitBBClick: (data: { position: number; step: string; pressed: boolean }) => void
}

interface IState {
  betAmount: number
}

class Control extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      betAmount: 0
    }
  }

  _isCurrentTurn = (): boolean => {
    const {
      player: { userID },
      currentTurn
    } = this.props

    return userID === currentTurn
  }

  _getLabel = (name, minBet, betAmount, callAmount, maxBet) => {
    if (['Bet', 'Raise to'].includes(name)) {
      return betAmount === maxBet ? 'All-in' : `${name} ${shortMoney(betAmount || minBet, '$')}`
    }

    if (['Call'].includes(name)) {
      return callAmount ? `${name} ${shortMoney(callAmount || minBet, '$')}` : name
    }

    return name
  }

  _getButtons = () => {
    const { player, position, queuedActions, table, toggleQueue } = this.props
    const { betAmount } = this.state
    const { isGameFinished, waitButtons, sitOutButtons, preselectButtons, amountCall, maxBet, minBet } = player

    let buttons = []

    if (isGameFinished) {
      const { waitbb, paybutton } = waitButtons

      buttons = [
        {
          label: 'Leave',
          action: () => this._handleRequest({ step: 'leave' })
        },
        {
          label: sitOutButtons.name,
          action: () => this._handleRequest({ step: sitOutButtons.action })
        }
      ]

      if (waitbb) {
        buttons.push({
          label: waitbb.name,
          queueable: true,
          queued: waitbb.pressed,
          action: () => this._handleWaitButtonClick({ step: 'waitbb', pressed: !waitbb.pressed })
        })
      }

      if (paybutton) {
        buttons.push({
          label: paybutton.name,
          queueable: true,
          queued: paybutton.pressed,
          action: () => this._handleWaitButtonClick({ step: 'paybutton', pressed: !paybutton.pressed })
        })
      }
    } else {
      buttons = preselectButtons.map(button => {
        const { name, values } = button
        const queued = !this._isCurrentTurn() && isEqual(queuedActions[position].actions, values)
        const action = () => {
          if (!this._isCurrentTurn()) {
            toggleQueue({
              position,
              actions: values,
              amount: betAmount || undefined,
              tableId: table.tableID,
              userID: player.userID
            })
          } else {
            this._handleMove(values)
          }
        }

        return {
          label: this._getLabel(name, minBet, betAmount, amountCall, maxBet),
          pressed: false,
          queueable: !this._isCurrentTurn(),
          queued,
          action
        }
      })
    }

    return buttons
  }

  _handleRequest = async (requestData: any) => {
    const {
      player: { userID },
      table: { tableID },
      position,
      makeAction
    } = this.props

    makeAction({
      ...requestData,
      position,
      userID,
      tableId: tableID
    })
  }

  _handleMove = (action: string[]) => {
    const { betAmount } = this.state

    this._handleRequest({
      step: 'makeMove',
      amount: betAmount || undefined,
      action
    })
  }

  _handleWaitButtonClick = (requestData: any) => {
    const { position, waitBBClick } = this.props

    waitBBClick({ position, ...requestData })
    this._handleRequest(requestData)
  }

  _handleBetChange = ({ value }) => {
    this.setState({ betAmount: sanitizeInput(value) })
  }

  _renderButtons = (): React.ReactElement => {
    return (
      <div className={s.buttons}>
        {this._getButtons().map((buttonProps, i) => (
          <Button key={i} {...buttonProps} />
        ))}
      </div>
    )
  }

  _renderBetInput = (): React.ReactElement => {
    const { player } = this.props
    const { betAmount } = this.state

    return (
      <LegacyMoneyInput onChange={this._handleBetChange} value={betAmount || player.minBet} maxValue={player.maxBet} />
    )
  }

  render() {
    const { player } = this.props

    return (
      <div className={cn(s.playerWrap, { [s.active]: this._isCurrentTurn() })}>
        <span className={s.playerName}>{player.playername}</span>
        {this._renderButtons()}
        {this._renderBetInput()}
      </div>
    )
  }
}

const mapStateProps = state => ({
  currentTurn: getTurn(state),
  table: getTableState(state),
  queuedActions: getTestPlayersQueuedActions(state)
})

const mapActionsProps = {
  waitBBClick: updateWaitButtons,
  toggleQueue: toggleQueueTestPlayer,
  makeAction: testPlayerAction
}

export default connect(mapStateProps, mapActionsProps)(Control)
