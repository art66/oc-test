import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { IPlayer, IPlayers } from '../../../interfaces'
import { getPlayers } from '../../../selectors'
import Control from './Control/Control'
import style from './index.cssmodule.scss'

interface IProps {
  players: IPlayers
}

interface IState {
  betAmount: number
}

class PlayersControl extends PureComponent<IProps, IState> {
  _renderPlayersControls = (): React.ReactNodeArray => {
    const { players } = this.props

    return Object.keys(players).map(key => {
      const player: IPlayer = players[key]

      return <Control key={player.userID} player={player} position={+key} />
    })
  }

  render() {
    return <div className={style.playersControl}>{this._renderPlayersControls()}</div>
  }
}

const mapStateProps = state => ({
  players: getPlayers(state)
})

export default connect(mapStateProps)(PlayersControl)
