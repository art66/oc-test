import { select, withKnobs } from '@storybook/addon-knobs'
import React, { useState } from 'react'
import { BetPanel, TProps } from './BetPanel'
import { setAppType } from '../../actions'
import StoreProvider from '../../containers/StoreProvider'
import { configureStore } from '../../store/createStore'
import { mockState } from '../../fixtures/store.mock'

const store = configureStore(mockState)

const withProvider = story => <StoreProvider store={store}>{story()}</StoreProvider>

const WrapperClassNames = ['full', 'sidebar']

const BetPanelProps: Partial<TProps> = {
  maxBet: 10000,
  minBet: 10,
  totalPot: 1000,
  myTurn: true,
  bigBlind: 1000,
  communityCards: [],
  acceptedActions: [],
  round: ''
}

const BetPanelTemplate = (props: TProps) => {
  const [betAmount, setBetAmount] = useState(400)
  const wrapperClassName = select('Full screen?', WrapperClassNames, 'full')

  store.dispatch(setAppType(wrapperClassName))

  return (
    <div className={wrapperClassName}>
      <BetPanel {...props} betAmount={betAmount} setBetAmount={setBetAmount as any} />
    </div>
  )
}

export const Default = BetPanelTemplate.bind({})

export default {
  title: 'Holdem/BetPanel',
  decorators: [withProvider, withKnobs],
  args: BetPanelProps
}
