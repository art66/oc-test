import flowRight from 'lodash/flowRight'
import { defaultTo } from 'lodash/fp'
import get from 'lodash/fp/get'
import React, { PureComponent } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { setBetAmount } from '../../actions'
import {
  getAcceptedActions,
  getBetAmount,
  getCurrentRound,
  getMaxBet,
  getMinBet,
  getMyMoney,
  getMyPot,
  getTotalPot,
  isItMyTurn
} from '../../selectors'
import { TAppState } from '../../store/rootReducer'
import { sanitizeInput } from '../../utils/sanitizeInput'
import { BetInput } from '../BetInput/BetInput'
import { BetOptions } from '../BetOptions/BetOptions'
import style from './style.cssmodule.scss'

export type TProps = TReduxProps

export class BetPanel extends PureComponent<TProps> {
  validateInput = flowRight(defaultTo(0), value => Math.min(value, this.props.maxBet))
  updateBetAmount = flowRight(this.props.setBetAmount, this.validateInput)
  updateBetAmountSanitized = flowRight(this.updateBetAmount, sanitizeInput, get('value'))

  componentDidMount() {
    this.props.setBetAmount(this.props.minBet)
  }

  componentDidUpdate(prevProps: TProps) {
    const { myTurn, minBet, round, betAmount } = this.props

    // TODO: extract this logic outside
    if ((!prevProps.myTurn && myTurn && betAmount < minBet) || prevProps.round !== round) {
      this.updateBetAmount(minBet)
    }
  }

  increase = () => this.props.setBetAmount(this.props.betAmount + this.props.bigBlind)
  decrease = () => this.props.setBetAmount(Math.max(0, this.props.betAmount - this.props.bigBlind))

  render() {
    return (
      <div className={style.betPanel}>
        <BetOptions
          minBet={this.props.minBet}
          maxBet={this.props.maxBet}
          bigBlind={this.props.bigBlind}
          isPreflop={!this.props.communityCards?.length}
          totalPot={this.props.totalPot}
          shouldConsiderMinBet={!this.props.acceptedActions.includes('bet')}
          moneyAvailable={this.props.myMoney + this.props.myPot}
          updateBetAmount={this.updateBetAmount}
        />
        <BetInput
          increase={this.increase}
          decrease={this.decrease}
          bigBlind={this.props.bigBlind}
          value={this.props.betAmount}
          maxValue={this.props.maxBet}
          onChange={this.updateBetAmountSanitized}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: TAppState) => ({
  betAmount: getBetAmount(state),
  maxBet: getMaxBet(state),
  minBet: getMinBet(state),
  myMoney: getMyMoney(state),
  myPot: getMyPot(state),
  totalPot: getTotalPot(state),
  myTurn: isItMyTurn(state),
  bigBlind: state.table.bigBlind,
  communityCards: state.table.communityCards,
  acceptedActions: getAcceptedActions(state),
  round: getCurrentRound(state)
})

const mapActionsToProps = {
  setBetAmount
}

const connector = connect(mapStateToProps, mapActionsToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(BetPanel)
