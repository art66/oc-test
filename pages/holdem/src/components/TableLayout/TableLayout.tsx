import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { getAmISat } from '../../selectors'
import { TAppState } from '../../store/rootReducer'
import { isSidebarPresent } from '../../utils'
import CommunityCards from '../CommunityCards/CommunityCards'
import Dealer from '../Dealer/Dealer'
import { NewRoundCountDown } from '../NewRoundCountDown/NewRoundCountDown'
import Panel from '../Panel/Panel'
import Players from '../Players/Players'
import PopupSelector from '../PopupSelector/PopupSelector'
import { Stacks } from '../Stacks/Stacks'
import TableBackground from '../TableBackground/TableBackground'
import WatcherPanel from '../WatcherPanel/WatcherPanel'
import style from './style.cssmodule.scss'

type TProps = TReduxProps

const FullScreenLayout = React.lazy(() => import('../FullScreenLayout/FullScreenLayout'))

const TableLayout = (props: TProps) => (
  <main className={props.withSidebar ? 'sidebar' : 'full'}>
    <React.Suspense fallback={<></>}>{!props.withSidebar && <FullScreenLayout />}</React.Suspense>
    <div className={style.holdemWrapper}>
      <TableBackground>
        <CommunityCards />
        <Players />
        <Dealer />
        <Stacks />
        <NewRoundCountDown />
        <PopupSelector />
      </TableBackground>
      {props.amISet ? <Panel /> : <WatcherPanel />}
    </div>
  </main>
)

const mapStateToProps = (state: TAppState) => ({
  withSidebar: isSidebarPresent(),
  amISet: getAmISat(state)
})

const connector = connect(mapStateToProps)

type TReduxProps = ConnectedProps<typeof connector>
export default connector(TableLayout)
