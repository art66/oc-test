// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'avatarCanvas': string;
  'avatarContainer': string;
  'avatarFrame': string;
  'default': string;
  'folded': string;
  'framed': string;
  'globalSvgShadow': string;
  'winner': string;
}
export const cssExports: CssExports;
export default cssExports;
