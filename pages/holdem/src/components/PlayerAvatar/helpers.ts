export const convertImgToBase64URL = (url: string): Promise<string> =>
  new Promise(resolve => {
    const img = new Image()

    img.crossOrigin = 'crossOriginMode'
    const canvas = document.createElement('CANVAS') as HTMLCanvasElement
    const ctx = canvas.getContext('2d')

    img.onload = () => {
      canvas.height = img.height
      canvas.width = img.width
      ctx.drawImage(img, 0, 0)

      resolve(canvas.toDataURL().toString())
    }
    img.src = url
  })
