import cn from 'classnames'
import React, { memo, useEffect, useState } from 'react'
import { TAppearance } from '../../interfaces'
import { convertImgToBase64URL } from './helpers'
import style from './style.cssmodule.scss'

interface IProps {
  appearance: TAppearance
  avatar: string
  className: string
  framed?: boolean
}

export const PlayerAvatar = memo((props: IProps) => {
  const [avatarSrc, setAvatarSrc] = useState('')

  useEffect(() => {
    let avatarMounted = true;

    (async () => {
      const base64URL = await convertImgToBase64URL(props.avatar)

      if (avatarMounted) setAvatarSrc(base64URL)
    })()

    return () => {
      avatarMounted = false
    }
  }, [])

  return (
    <div
      className={cn(style.avatarContainer, props.className, style[props.appearance], { [style.framed]: props.framed })}
    >
      <div className={style.avatarFrame}>
        <img className={style.avatarCanvas} src={avatarSrc} alt='player avatar' />
      </div>
    </div>
  )
})
