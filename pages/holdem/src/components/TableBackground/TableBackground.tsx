import React, { Component, PropsWithChildren } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { MEDIA_TYPES, TABLE_SIZES, TABLE_SKIN_SET } from '../../constants'
import { getTableID } from '../../selectors'
import { TAppState } from '../../store/rootReducer'
import { isSidebarPresent } from '../../utils'
import style from './style.cssmodule.scss'

type TProps = PropsWithChildren<TReduxProps>

class TableBackground extends Component<TProps> {
  getTableSize = () => {
    const { withSidebar, mediaType } = this.props

    switch (mediaType) {
      case MEDIA_TYPES.TYPE_LARGE:
        return withSidebar ? TABLE_SIZES.MEDIUM : TABLE_SIZES.LARGE
      case MEDIA_TYPES.TYPE_MEDIUM:
        return withSidebar ? TABLE_SIZES.MEDIUM_SIDEBAR : TABLE_SIZES.MEDIUM
      case MEDIA_TYPES.TYPE_SMALL:
        return TABLE_SIZES.SMALL
      default:
        return TABLE_SIZES.MEDIUM
    }
  }

  getFrameUrl = () => `/casino/holdem/images/frames/${this.getTableSize()}_frame.png`

  getBackgroundImageUrl = () => {
    const background = this.props.tablesBg ? 'tables' : 'tables_colour'
    const size = this.getTableSize()

    return `/casino/holdem/images/${background}/${size}/${size}_${
      TABLE_SKIN_SET[this.props.currentTableID] || TABLE_SKIN_SET[2]
    }.png`
  }

  getTableBackground = () => `url(${this.getFrameUrl()}),url(${this.getBackgroundImageUrl()})`

  render() {
    const { currentTableID } = this.props

    return (
      <>
        <div className={style.tableWrap} id='tableWrap'>
          <div
            id='table'
            className={style.table}
            style={{ backgroundImage: !isNaN(currentTableID) ? this.getTableBackground() : '' }}
          >
            {this.props.children}
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = (state: TAppState) => ({
  mediaType: state.browser.mediaType,
  currentTableID: getTableID(state),
  withSidebar: isSidebarPresent(),
  tablesBg: state.ui.tablesBg
})

const connector = connect(mapStateToProps)

type TReduxProps = ConnectedProps<typeof connector>
export default connector(TableBackground)
