import { LegacyMoneyInput } from '@torn/shared/components/LegacyMoneyInput/LegacyMoneyInput'
import { toMoney } from '@torn/shared/utils'
import React, { Component, createRef } from 'react'
import ReactDOM from 'react-dom'
import ReactDOMServer from 'react-dom/server'
import { DollarIcon } from '../../svg/DollarIcon'
import { MinusIcon } from '../../svg/MinusIcon'
import { PlusIcon } from '../../svg/PlusIcon'
import style from './style.cssmodule.scss'

type TProps = {
  value: number
  maxValue: number
  onChange: (data: { value: string; error: boolean }) => void
  increase: () => void
  decrease: () => void
  bigBlind: number
  disabled?: boolean
}

export class BetInput extends Component<TProps> {
  private wrapperRef = createRef<HTMLDivElement>()

  componentDidMount() {
    // the element '.input-money-group' is not yet rendered at first render
    // so we need to rerender this component in order to render the buttons
    this.forceUpdate()
  }

  renderButtons = () =>
    this.wrapperRef.current
    && ReactDOM.createPortal(
      <>
        <button
          type='button'
          className={style.minusBtn}
          onClick={this.props.decrease}
          title={`Decrease bet by $${toMoney(this.props.bigBlind)}`}
        >
          <MinusIcon />
        </button>
        <button
          type='button'
          className={style.plusBtn}
          onClick={this.props.increase}
          title={`Increase bet by $${toMoney(this.props.bigBlind)}`}
        >
          <PlusIcon />
        </button>
      </>,
      $(this.wrapperRef.current).find('.input-money-group')[0]
    )

  render() {
    return (
      <div className={style.betInputWrap} ref={this.wrapperRef}>
        <LegacyMoneyInput
          value={toMoney(this.props.value)}
          maxValue={this.props.maxValue}
          onChange={this.props.onChange}
          symbol={ReactDOMServer.renderToString(<DollarIcon />)}
        />
        {this.renderButtons()}
      </div>
    )
  }
}
