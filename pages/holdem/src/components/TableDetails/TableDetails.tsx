/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import { toMoney } from '@torn/shared/utils'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { ILobbyTable } from '../../interfaces'
import { getPlayersCount } from '../../selectors'
import { api } from '../../services'
import { Popup } from '../Popup/Popup'
import style from './style.cssmodule.scss'

interface IState {
  details: Partial<ILobbyTable>
}

const TableDetails = () => {
  const [details, setDetails] = useState<IState['details']>({})
  const playersCount = useSelector(getPlayersCount)

  useEffect(() => {
    api.details().then(setDetails)
  }, [])

  return (
    <Popup className={style.tableDetailsWrap} header={details.name || 'Loading...'}>
      <div className={style.content}>
        <p>
          <em>Players: </em>
          <span>
            {playersCount} / {details.maxplayers}
          </span>
        </p>
        <p>
          <em>Stakes: </em>
          <span>{details.stakes}</span>
        </p>
        <p>
          <em>Buy-in: </em>
          <span>{`${toMoney(details.minMoney)} / ${toMoney(details.maxMoney)}`}</span>
        </p>
        <p>
          <em>Speed: </em>
          <span>{`${details.speed} seconds`}</span>
        </p>
      </div>
    </Popup>
  )
}

export default TableDetails
