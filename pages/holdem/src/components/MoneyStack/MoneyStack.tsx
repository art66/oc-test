import cn from 'classnames'
import React from 'react'
import { factoring } from './helpers'
import style from './style.cssmodule.scss'

interface IProps {
  money: number
  limit?: number
}

export const MoneyStack = ({ money = 0, limit }: IProps) => (
  <div className={style.moneyStackWrapper}>
    {factoring(money)
      .slice(0, limit)
      .map((stack, i) => (
        <div className={style.stack} key={i}>
          {stack.map((value, j) => (
            <div key={j} className={cn(style.chip, style[`chip-${value}`])} />
          ))}
        </div>
      ))}
  </div>
)
