export const factoring = (money: number) =>
  money
    .toString()
    .split('')
    .reverse()
    .map(Number)
    .map((n, power) => Array(n).fill(10 ** power))
    .filter(array => array.length)
    .reverse()
