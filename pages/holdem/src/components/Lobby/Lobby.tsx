/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import cn from 'classnames'
import React, { Component } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { closePopup, joinTable } from '../../actions'
import { getAmISat, getTableID } from '../../selectors'
import { TAppState } from '../../store/rootReducer'
import { BetsIcon } from '../../svg/BetsIcon'
import { HourglassIcon } from '../../svg/HourglassIcon'
import { TablesIcon } from '../../svg/TablesIcon'
import { Popup } from '../Popup/Popup'
import style from './style.cssmodule.scss'

type TProps = TReduxProps
class Lobby extends Component<TProps> {
  makeJoinHandler = params => () => {
    if (!this.props.amISat) {
      this.props.joinTable(params)
      this.props.close()
    }
  }

  render() {
    const { tables, currentTableID, amISat } = this.props

    return (
      <Popup className={style.lobbyWrap}>
        <table className={style.table}>
          <tbody>
            <tr className={style.row}>
              <td className={style.name}>
                <span className={`${style.lobby__text} ${style.bold}`}>Lobby List</span>
              </td>
              <td className={style.stake}>
                <i className={style.lobbyIcon}>
                  <BetsIcon />
                </i>
              </td>
              <td className={style.speed}>
                <i className={style.lobbyIcon}>
                  <HourglassIcon />
                </i>
              </td>
              <td className={style.players}>
                <i className={style.lobbyIcon}>
                  <TablesIcon />
                </i>
              </td>
            </tr>
          </tbody>
        </table>
        <div className={cn(style.content, 'scroll-area scrollbar-gray')}>
          <table className={style.table}>
            <tbody>
              {tables.map(({ ID: tableID, channel, name, players, maxplayers, stakes, speed }) => (
                <tr
                  className={cn(style.row, {
                    [style.active]: !amISat,
                    [style.current]: currentTableID === Number(tableID)
                  })}
                  onClick={this.makeJoinHandler({ tableID, channel })}
                  key={name}
                >
                  <td className={style.name}>
                    <span className={`${style.lobby__text} ${style.bold}`}>{name}</span>
                  </td>
                  <td className={style.stake}>
                    <span className={style.lobby__text}>{`${stakes}`}</span>
                  </td>
                  <td className={style.speed}>
                    <span className={style.lobby__text}>{speed}</span>
                  </td>
                  <td className={style.players}>
                    <span className={style.lobby__text}>{`${players}/${maxplayers}`}</span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </Popup>
    )
  }
}

const mapStateToProps = (state: TAppState) => ({
  amISat: getAmISat(state),
  currentTableID: getTableID(state),
  tables: state.lobby.tables
})

const mapActionsToProps = {
  joinTable,
  close: closePopup
}

const connector = connect(mapStateToProps, mapActionsToProps)

type TReduxProps = ConnectedProps<typeof connector>
export default connect(mapStateToProps, mapActionsToProps)(Lobby)
