import { toMoney } from '@torn/shared/utils'
import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { selectComponent } from '../../hoc/selectComponent'
import { getCurrentRound } from '../../selectors'
import { getLastRoundPots, getPlayersPot, getPlayersWining } from '../../selectors/players'
import { MoneyStack } from '../MoneyStack/MoneyStack'
import { RoundPot } from '../RoundPot/RoundPot'
import { TotalPot } from '../TotalPot/TotalPot'
import potTransition from './potTransition.cssmodule.scss'
import style from './style.cssmodule.scss'
import winningsTransition from './winningsTransition.cssmodule.scss'

const usePrevious = value => {
  const ref = useRef()

  useEffect(() => {
    ref.current = value
  })
  return ref.current
}

const PotWithPlayersPot = () => {
  const playersPot = useSelector(getPlayersPot)
  const playersLastRoundPot = useSelector(getLastRoundPots)
  const playersWinnings = useSelector(getPlayersWining)
  const round = useSelector(getCurrentRound)
  const [timeToAnimateThePots, setTimeToAnimateThePots] = useState(false)
  const prevRound = usePrevious(round)

  useEffect(() => {
    setTimeToAnimateThePots(prevRound && round !== prevRound)
  }, [prevRound, round])

  return (
    <>
      <TransitionGroup component={null}>
        {(timeToAnimateThePots ? playersLastRoundPot : playersPot).map(({ position, value }) => (
          <CSSTransition key={position} timeout={{ enter: 0, exit: 800 }} classNames={potTransition}>
            <div className={style[`player-pot-${position}`]}>
              <MoneyStack money={value} limit={3} />
              <div>{Boolean(value) && toMoney(value, '$')}</div>
            </div>
          </CSSTransition>
        ))}
      </TransitionGroup>

      <TransitionGroup component={null}>
        {playersWinnings.map(({ position, value }) => (
          <CSSTransition key={position} timeout={{ enter: 800, exit: 0 }} classNames={winningsTransition}>
            <div className={style[`player-pot-${position}`]}>
              <MoneyStack money={value} limit={3} />
              <div>{Boolean(value) && toMoney(value, '$')}</div>
            </div>
          </CSSTransition>
        ))}
      </TransitionGroup>
    </>
  )
}

const PlayersPots = selectComponent({
  desktop_full: PotWithPlayersPot,
  desktop_sidebar: PotWithPlayersPot,
  tablet_full: PotWithPlayersPot,
  tablet_sidebar: React.Fragment,
  mobile: React.Fragment
})

export const Stacks = () => (
  <div className={style.potsWrapper}>
    <RoundPot />
    <TotalPot />
    <PlayersPots />
  </div>
)
