import ContentTitle from '@torn/shared/components/ContentTitle'
import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { toggleSound, toggleTables } from '../../actions'
import { SID_FULL, SID_WITH_SIDEBAR } from '../../constants'
import { getSoundStatus } from '../../selectors'
import { FullScreenEnterIcon } from '../../svg/FullScreenEnterIcon'
import { FullScreenExitIcon } from '../../svg/FullScreenExitIcon'
import { PopoutIcon } from '../../svg/PopoutIcon'
import { SoundOffIcon } from '../../svg/SoundOffIcon'
import { SoundOnIcon } from '../../svg/SoundOnIcon'
import { StatisticsIcon } from '../../svg/StatisticsIcon'
import { TableSkinOffIcon } from '../../svg/TableSkinOffIcon'
import { TableSkinOnIcon } from '../../svg/TableSkinOnIcon'
import { getPopped, getSid } from '../../utils'
import './style.scss'

type TLink = {
  action?: () => void
  icon: React.ReactElement
  title: string
  linkClassName: string
  link?: string
}

type TProps = TReduxProps

const TopMenu = (props: TProps) => {
  const popout = () => {
    const { width } = screen
    const w = 1200
    const h = 800
    const defaultOptions = 'top=0,fullscreen=0,location=0,menubar=0,scrollbars=yes,status=0,toolbar=0,resizable=yes'

    location.assign('/casino.php')
    window.open(
      `?sid=${SID_FULL}&popped`,
      'showpicture',
      `width=${w},height=${h},left=${(width - w) / 2},${defaultOptions}`
    )
  }

  const expand = () => {
    location.assign(`?sid=${SID_FULL}`)
  }

  const unexpand = () => {
    location.assign(`?sid=${SID_WITH_SIDEBAR}`)
  }

  let links: TLink[] = [
    {
      action: props.toggleSound,
      icon: props.soundOn ? SoundOnIcon() : SoundOffIcon(),
      title: 'Sound',
      linkClassName: 'toggleSound'
    },
    {
      action: props.toggleTables,
      icon: props.tablesBg ? TableSkinOnIcon() : TableSkinOffIcon(),
      title: 'Theme',
      linkClassName: 'toggleTheme'
    },
    {
      icon: StatisticsIcon(),
      title: 'Statistics',
      link: '/loader.php?sid=viewPokerStats',
      linkClassName: ''
    }
  ]

  if (!props.popped) {
    links = [
      {
        action: popout,
        icon: PopoutIcon(),
        title: 'Pop out',
        linkClassName: ''
      },
      {
        action: props.sid === SID_WITH_SIDEBAR ? expand : unexpand,
        icon: props.sid === SID_WITH_SIDEBAR ? FullScreenEnterIcon() : FullScreenExitIcon(),
        title: 'Fullscreen',
        linkClassName: 'fullScreen'
      },
      ...links
    ]
  }

  return <ContentTitle title='Poker' links={links} bottomHr={true} />
}

const mapStateToProps = state => ({
  popped: getPopped(),
  soundOn: getSoundStatus(state),
  sid: getSid(),
  tablesBg: state.ui.tablesBg
})

const mapActionsToProps = {
  toggleSound,
  toggleTables
}

const connector = connect(mapStateToProps, mapActionsToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(TopMenu)
