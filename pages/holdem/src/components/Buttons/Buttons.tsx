import React from 'react'
import Button, { TProps as TButtonProps } from '../Button/Button'
import s from './style.cssmodule.scss'

type TProps = {
  buttons: TButtonProps[]
}

export const Buttons = (props: TProps) => (
  <div className={s.buttonsWrap}>
    {props.buttons.map(buttonProps => (
      <Button key={buttonProps.label} {...buttonProps} />
    ))}
  </div>
)
