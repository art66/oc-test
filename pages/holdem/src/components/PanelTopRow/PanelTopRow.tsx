import React from 'react'
import BetPanel from '../BetPanel/BetPanel'
import { DetailsButton, HandsButton, LobbyButton, SoundButton } from '../MenuButtons/MenuButtons'
import style from './style.cssmodule.scss'

export const PanelTopRow = () => (
  <div className={style.panelTopRow}>
    <div className={style['toggle-button-1-1']}>
      <LobbyButton className={style.toggleButton} />
    </div>
    <div className={style['toggle-button-2-1']}>
      <DetailsButton className={style.toggleButton} />
    </div>
    <div className={style['toggle-button-1-2']}>
      <HandsButton className={style.toggleButton} />
    </div>
    <div className={style['toggle-button-2-2']}>
      <SoundButton className={style.toggleButton} />
    </div>
    <div className={style.betPanelPositioner}>
      <BetPanel />
    </div>
  </div>
)
