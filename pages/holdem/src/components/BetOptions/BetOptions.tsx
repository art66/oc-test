/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import cn from 'classnames'
import clamp from 'lodash/fp/clamp'
import React from 'react'
import style from './style.cssmodule.scss'

const calculatePot = (addMinBet: boolean, totalPot: number, minBet: number) => (potFraction: number) =>
  Math.ceil(totalPot * potFraction) + (addMinBet ? minBet : 0)

type TBetOptionsProps = {
  minBet: number
  maxBet: number
  bigBlind: number
  isPreflop: boolean
  totalPot: number
  moneyAvailable: number
  shouldConsiderMinBet: boolean
  updateBetAmount: (value: number) => void
}

export const BetOptions = (props: TBetOptionsProps) => {
  const validateValue = clamp(props.minBet, props.moneyAvailable)
  const calc = calculatePot(props.shouldConsiderMinBet, props.totalPot, props.minBet)
  const creteOnClickHandler = (value: number) => () => {
    props.updateBetAmount(validateValue(value))
  }

  return (
    <ul className={cn(style.betOptions)}>
      <li onClick={creteOnClickHandler(props.minBet)}>Min</li>
      {props.isPreflop && <li onClick={creteOnClickHandler(props.bigBlind * 3)}>3BB</li>}
      <li onClick={creteOnClickHandler(calc(1 / 2))}>1/2</li>
      {!props.isPreflop && <li onClick={creteOnClickHandler(calc(3 / 4))}>3/4</li>}
      <li onClick={creteOnClickHandler(calc(1))}>Pot</li>
      <li onClick={creteOnClickHandler(props.maxBet)}>Max</li>
    </ul>
  )
}
