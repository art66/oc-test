import React from 'react'
import { useSelector } from 'react-redux'
import { getDisplayNewRoundCountDown, getTimeLeft } from '../../selectors'
import style from './style.cssmodule.scss'

export const NewRoundCountDown = () => {
  const displayNewRoundCountDown = useSelector(getDisplayNewRoundCountDown)
  const timeLeft = useSelector(getTimeLeft)

  return (
    displayNewRoundCountDown && <div className={style.newRoundCountDown}>{`Starting next round in: ${timeLeft}`}</div>
  )
}
