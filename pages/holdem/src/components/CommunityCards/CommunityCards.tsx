import cn from 'classnames'
import find from 'lodash/find'
import React, { PureComponent } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { TCard, TCommunityCards } from '../../interfaces'
import { getCombination, getCommunityCards, getIsShowdownTime, getPhase, getTotalPot } from '../../selectors'
import Card from '../Card/Card'
import style from './style.cssmodule.scss'

// const CARDS_APPEARANCE_DELAY = 200
// const CARDS_APPEARANCE_DELAY_AFTER_POT = 650

interface IState {
  cards: TCard[]
  isVisible: boolean
}

export class CommunityCards extends PureComponent<TReduxProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      cards: [],
      isVisible: false
    }
  }

  componentDidUpdate(prevProps: Readonly<TReduxProps>) {
    const ended = this.props.phase === 'ended'
    const started = this.props.phase === 'Started'
    const lastMove = prevProps.phase === 'Started' && ended

    if (this.state.cards?.length < this.props.cards?.length) {
      setTimeout(() => {
        this.setNewCards(this.props.cards.slice(0, this.state.cards?.length + 1) as TCommunityCards)
      }, 300)
    } else if (this.state.cards.toString() !== this.props.cards.toString()) {
      this.setNewCards(this.props.cards)
    }
    this.setVisibility(lastMove || started || (ended && this.props.cards.length > 0))
  }

  // TODO find out why we need different delays for revealing the cards
  // getDelay = (prevProps: TReduxProps) => {
  //   if (prevProps.pot < this.props.pot && !(prevProps.phase === 'ended') && this.state.cards?.length !== 0) {
  //     return CARDS_APPEARANCE_DELAY_AFTER_POT
  //   } else if (prevProps.phase === 'ended' && [0, 3, 4].includes(this.state.cards?.length)) {
  //     return 1200
  //   }
  //   return CARDS_APPEARANCE_DELAY
  // }

  setNewCards = (cards: TCommunityCards) => {
    this.setState({ cards })
  }

  setVisibility = (isVisible: boolean) => {
    this.setState({ isVisible })
  }

  renderCards = () => {
    const { cards, isVisible } = this.state
    const showCombination = this.props.combination?.length > 0 && this.props.showdown && cards?.length === 5
    const cardsList = []

    if (!cards || !this.props.combination) {
      return null
    }

    for (let i = 0; i < 5; i++) {
      const card = cards[i]
      const cardInCombination = find(this.props.combination, c => c === card)
      const decoratorClass = (showCombination && (cardInCombination ? 'glow' : 'dimOut')) || null

      cardsList.push(
        <li className={cn(style.cardWrap, { [style.cardInStack]: !isVisible }, { [style.card]: isVisible })} key={i}>
          <Card cardname={card} type='community' decoratorClass={decoratorClass} />
        </li>
      )
    }

    return cardsList
  }

  render() {
    return (
      <div className={style.communityCards}>
        <ul>{this.renderCards()}</ul>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  cards: getCommunityCards(state),
  combination: getCombination(state),
  phase: getPhase(state),
  pot: getTotalPot(state),
  showdown: getIsShowdownTime(state)
})
const connector = connect(mapStateToProps, null)

type TReduxProps = ConnectedProps<typeof connector>
export default connector(CommunityCards)
