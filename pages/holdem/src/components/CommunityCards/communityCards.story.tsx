import { number } from '@storybook/addon-knobs'
import React from 'react'
import StoreProvider from '../../containers/StoreProvider'
import { TCommunityCards } from '../../interfaces'
import { configureStore } from '../../store/createStore'
import { CommunityCards } from './CommunityCards'

const cardsArray = ['clubs-A', 'diamonds-A', 'spades-3', 'diamonds-A', 'hearts-A']

const initialState = {
  betAmount: 100,
  hand: ['diamonds-A', 'hearts-Q'],
  identity: { userID: 1435333 },
  ui: { withSidebar: false },
  lobby: { tables: [] },
  table: {
    communityCards: ['diamonds-A', 'hearts-Q', 'spades-10', 'clubs-2', 'clubs-A'],
    maxBet: 100,
    minBet: 10,
    phase: 'ended',
    totalPot: 1000,
    timeleft: 30,
    turn: 1435333,
    userAction: 'check'
  },
  players: {
    1: {
      userID: 1613939,
      playername: 'hakh',
      state: 0,
      acceptedActions: ['raise', 'call', 'fold'],
      money: 1049014,
      pot: 5,
      place: 1,
      minBet: 11,
      maxBet: 1049014,
      minRaise: 15,
      maxRaise: 1049014,
      isDealer: false,
      isSmallBlind: true,
      isBigBlind: false,
      avatar: '//dev-www.torn.com/images/profile_man.jpg',
      winnings: 0,
      hand: []
    },
    8: {
      userID: 1435333,
      playername: 'toshykazu',
      state: 0,
      acceptedActions: ['raise', 'check', 'fold'],
      money: 9880,
      pot: 10,
      place: 8,
      minBet: 11,
      maxBet: 9880,
      minRaise: 10,
      maxRaise: 9880,
      isDealer: true,
      isSmallBlind: false,
      isBigBlind: true,
      avatar: '//dev-www.torn.com/images/profile_man.jpg',
      winnings: 0,
      hand: []
    }
  },
  log: [],
  queuedAction: {},
  browser: { mediaType: 'large' }
}

const label = 'Cards'
const defaultValue = 0
const options = {
  range: true,
  min: 0,
  max: 5,
  step: 1
}

const store = configureStore(initialState)
const withProvider = story => <StoreProvider store={store}>{story()}</StoreProvider>

export const Defaults = () => (
  <div className='full'>
    <CommunityCards
      cards={cardsArray.slice(0, number(label, defaultValue, options)) as TCommunityCards}
      combination={[]}
      phase='Started'
      pot={2000}
      showdown={true}
    />
  </div>
)

Defaults.story = {
  name: 'defaults'
}

export default {
  title: 'Holdem/CommunityCards',
  decorators: [withProvider]
}
