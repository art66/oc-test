import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { getDealerPosition } from '../../selectors'
import s from './style.cssmodule.scss'

type TProps = TReduxProps
export const Dealer = ({ position }: TProps) =>
  position !== null && <div className={`${s.dealer} ${s[`position-${position}`]}`} />

const mapStateToProps = state => ({
  position: getDealerPosition(state)
})

const connector = connect(mapStateToProps, null)

type TReduxProps = ConnectedProps<typeof connector>
export default connector(Dealer)
