import { BigBlind } from '../../svg/BigBlind'
import { CheckCall } from '../../svg/CheckCall'
import { Dealer } from '../../svg/Dealer'
import { PlayerFolded } from '../../svg/PlayerFolded'
import { PlayerJoined } from '../../svg/PlayerJoined'
import { PlayerLeft } from '../../svg/PlayerLeft'
import { Raise } from '../../svg/Raise'
import { SmallBlind } from '../../svg/SmallBlind'
import { Thinking } from '../../svg/Thinking'

export const StatusIconsMap = {
  bet: Raise,
  raise: Raise,
  allIn: Raise,
  bigBlind: BigBlind,
  call: CheckCall,
  check: CheckCall,
  dealer: Dealer,
  fold: PlayerFolded,
  folded: PlayerFolded,
  smallBlind: SmallBlind,
  thinking: Thinking,
  sittingOut: PlayerLeft,
  waitingBB: PlayerJoined
}

export const renderStatusIcon = (icon: string) => StatusIconsMap[icon]?.()
