import { BigBlind } from '../../svg/BigBlind'
import { CheckCall } from '../../svg/CheckCall'
import { Dealer } from '../../svg/Dealer'
import { Flop } from '../../svg/Flop'
import { PlayerFolded } from '../../svg/PlayerFolded'
import { PlayerJoined } from '../../svg/PlayerJoined'
import { PlayerLeft } from '../../svg/PlayerLeft'
import { Preflop } from '../../svg/Preflop'
import { Raise } from '../../svg/Raise'
import { River } from '../../svg/River'
import { SmallBlind } from '../../svg/SmallBlind'
import { Thinking } from '../../svg/Thinking'
import { Turn } from '../../svg/Turn'

export const LogIconsMap = {
  left: PlayerLeft,
  joined: PlayerJoined,
  dealer: Dealer,
  two_cards: Preflop,
  three_cards: Flop,
  four_cards: Turn,
  five_cards: River,
  small_blind: SmallBlind,
  big_blind: BigBlind,
  folded: PlayerFolded,
  checked_called: CheckCall,
  raised_bet: Raise,
  thinking: Thinking
}

export const renderLogIcon = (icon: string) => LogIconsMap[icon]?.()
