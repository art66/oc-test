import { toMoney } from '@torn/shared/utils'
import React from 'react'
import { useSelector } from 'react-redux'
import { getIsPotVisible, getPotValueForCurrentRound } from '../../selectors'
import { MoneyStack } from '../MoneyStack/MoneyStack'
import style from './style.cssmodule.scss'

export const RoundPot = () => {
  const isPotVisible = useSelector(getIsPotVisible)
  const roundPot = useSelector(getPotValueForCurrentRound)

  return (
    Boolean(isPotVisible && roundPot) && (
      <div className={style.roundPotWrap}>
        <div>
          <MoneyStack money={roundPot} />
        </div>
        <div className={style.roundPot}>{toMoney(roundPot, '$')}</div>
      </div>
    )
  )
}
