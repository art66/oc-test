import { useOutsideClickCallback } from '@torn/shared/hooks/useOutsideClickCallback'
import cn from 'classnames'
import React, { PropsWithChildren, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { closePopup } from '../../actions'
import style from './style.cssmodule.scss'

type TProps = PropsWithChildren<{
  header?: string
  className?: string
}>

export const Popup = (props: TProps) => {
  const ref = useRef(null)
  const dispatch = useDispatch()
  const close = event => {
    event.stopPropagation()
    dispatch(closePopup())
  }

  useOutsideClickCallback(ref, close)

  return (
    <div className={cn(style.popupWrapper, props.className)} ref={ref}>
      {props.header && <div className={style.header}>{props.header}</div>}
      {props.children}
    </div>
  )
}
