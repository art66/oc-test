import cn from 'classnames'
import React from 'react'
import { useSelector } from 'react-redux'
import { TCard } from '../../interfaces'
import { getSuitsColors } from '../../selectors/ui'

interface IProps {
  cardname: TCard
  styles: any
}

const CardFace = ({ cardname, styles }: IProps) => {
  const [suit, rank] = cardname.split('-')
  const suiteType = useSelector(getSuitsColors) === '2-colors' ? 'twoColors' : 'fourColors'

  return (
    <div
      className={cn(
        styles[cardname === 'back' ? 'back' : suiteType],
        rank && styles[`${suit}-${rank}`],
        styles.cardSize
      )}
    />
  )
}

export default CardFace
