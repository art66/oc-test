import { select, withKnobs } from '@storybook/addon-knobs'
import React from 'react'
import { GatewayProvider } from 'react-gateway'
import { setAppType } from '../../actions'
import StoreProvider from '../../containers/StoreProvider'
import { mockState } from '../../fixtures/store.withButtons'
import { configureStore } from '../../store/createStore'
import Panel from './Panel'

const store = configureStore(mockState)

const withProvider = story => <StoreProvider store={store}>{story()}</StoreProvider>

const PanelTemplate = () => {
  const wrapperClassName = select('Full screen?', ['full', 'sidebar'], 'full')

  store.dispatch(setAppType(wrapperClassName))
  return (
    <GatewayProvider>
      <div
        className={wrapperClassName}
        style={{
          position: 'relative',
          width: '80vw',
          height: '500px'
        }}
      >
        <div style={{ position: 'absolute', bottom: 0, left: 0, right: 0, margin: 'auto', width: 'fit-content' }}>
          <Panel />
        </div>
      </div>
    </GatewayProvider>
  )
}

export const Default = PanelTemplate.bind({})

export default {
  title: 'Holdem/Panel',
  decorators: [withProvider, withKnobs]
}
