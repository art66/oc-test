import cn from 'classnames'
import React from 'react'
import { GatewayDest } from 'react-gateway'
import { useSelector } from 'react-redux'
import Buttons from '../../containers/Buttons'
import { getSelfPlayerProps } from '../../selectors/selfPlayerProps'
import Logs from '../Logs/Logs'
import { PanelTopRow } from '../PanelTopRow/PanelTopRow'
import style from './style.cssmodule.scss'

const Panel = () => {
  const selfPlayer = useSelector(getSelfPlayerProps)

  return (
    <div className={cn(style.panelPositioner, style[selfPlayer?.appearance])}>
      <div className={style.panel}>
        <PanelTopRow />
        <Buttons />
        <Logs />
      </div>
      <GatewayDest className={style.playerMeGateway} name='MyPlayer' />
    </div>
  )
}

export default Panel
