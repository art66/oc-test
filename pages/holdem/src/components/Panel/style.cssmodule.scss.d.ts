// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'globalSvgShadow': string;
  'panel': string;
  'panelPositioner': string;
  'playerMeGateway': string;
  'pulse': string;
  'winner': string;
}
export const cssExports: CssExports;
export default cssExports;
