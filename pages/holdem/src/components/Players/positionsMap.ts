const positionsMap: { [key: number]: number[] } = {
  2: [3, 6],
  3: [0, 4, 5],
  4: [1, 4, 5, 8],
  5: [0, 2, 4, 5, 7],
  6: [1, 2, 3, 6, 7, 8],
  7: [0, 1, 2, 3, 6, 7, 8],
  8: [1, 2, 3, 4, 5, 6, 7, 8],
  9: [0, 1, 2, 3, 4, 5, 6, 7, 8]
}

export default positionsMap
