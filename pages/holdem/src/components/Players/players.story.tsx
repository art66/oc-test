import { select, withKnobs } from '@storybook/addon-knobs'
import React from 'react'
import { setAppType } from '../../actions'
import StoreProvider from '../../containers/StoreProvider'
import { mockState } from '../../fixtures/store.mock'
import { configureStore } from '../../store/createStore'
import '../../styles/story-style.scss' // takes effect on every story
import { Dealer } from '../Dealer/Dealer'
import { Players, TProps } from './Players'
import './players-story-style.scss'

const store = configureStore(mockState)
const users = [2156017, 2156012, 2156013, 2156014, 2156015, 2156016, 2313413, 2156011]

const playerProps = {
  userID: 2156017,
  status: 'Small Blind',
  state: 'active',
  appearance: 'active',
  revealButton: false,
  money: 10000000,
  pot: 50,
  totalPot: 50,
  amountCall: 50,
  minBet: 200,
  maxBet: 28180,
  minRaise: 200,
  maxRaise: 28180,
  isSitOut: false,
  isDealer: true,
  isSmallBlind: true,
  isBigBlind: false,
  isGameFinished: false,
  avatar: '/images/profile_man.jpg',
  winnings: 0,
  hand: ['clubs-A', 'hearts-A']
}

const playersProps = {
  amISat: false,
  players: Object.fromEntries(
    [...Array(9)].map((_, index) => [
      index,
      { ...playerProps, userID: users[index], playername: `Peter ${index}`, index }
    ])
  ),
  positions: [0, 1, 2, 3, 4, 5, 6, 7, 8]
}

const withProvider = story => <StoreProvider store={store}>{story()}</StoreProvider>

type TTemplateProps = {
  wrapperClass: 'full' | 'sidebar'
  wrapperWidth: number
  wrapperHeight: number
  tableUrl: string
}
const WrapperClassNames = ['full', 'sidebar']
const PlayersTemplate = (props: TTemplateProps & TProps) => {
  const wrapperClassName = select('Full screen?', WrapperClassNames, 'full')

  store.dispatch(setAppType(wrapperClassName))
  return (
    <div className='players'>
      <div className={wrapperClassName}>
        <Players {...props} />
      </div>
    </div>
  )
}

export const Default = PlayersTemplate.bind({})

const PlayersWithDealerTemplate = (props: TTemplateProps & TProps) => {
  const wrapperClassName = select('Full screen?', WrapperClassNames, 'full')

  store.dispatch(setAppType(wrapperClassName))
  return (
    <div className='players'>
      <div className={wrapperClassName}>
        <Players {...props} />
        <Dealer position={0} />
        <Dealer position={1} />
        <Dealer position={2} />
        <Dealer position={3} />
        <Dealer position={4} />
        <Dealer position={5} />
        <Dealer position={6} />
        <Dealer position={7} />
        <Dealer position={8} />
      </div>
    </div>
  )
}

export const WithDealerButton = PlayersWithDealerTemplate.bind({})

export default {
  title: 'Holdem/Players',
  decorators: [withProvider, withKnobs],
  args: playersProps
}
