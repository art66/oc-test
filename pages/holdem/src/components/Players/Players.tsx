import React from 'react'
import { Gateway } from 'react-gateway'
import { connect, ConnectedProps } from 'react-redux'
import { getAmISat, getPlayerPositions } from '../../selectors'
import { getPlayersProps } from '../../selectors/players'
import { TAppState } from '../../store/rootReducer'
import { OpponentPlayerLayout } from '../Player/OpponentPlayerLayout'
import { SelfPlayerLayout } from '../Player/SelfPlayerLayout'
import { SitInButton } from '../SitInButton/SitInButton'
import style from './style.cssmodule.scss'

export type TProps = TReduxProps

export const Players = (props: TProps) => (
  <div className={style.players}>
    {props.positions.map(position => {
      const player = props.players[position]

      if (player) {
        return player.self ? (
          <Gateway into='MyPlayer' key='MyPlayer'>
            <SelfPlayerLayout {...player} />
          </Gateway>
        ) : (
          <OpponentPlayerLayout {...player} key={player.playername} />
        )
      }
      if (!props.amISat) {
        return <SitInButton position={position} key={position} />
      }
      return null
    })}
  </div>
)

const mapStateToProps = (state: TAppState) => ({
  amISat: getAmISat(state),
  players: getPlayersProps(state),
  positions: getPlayerPositions(state)
})

const connector = connect(mapStateToProps, null)

type TReduxProps = ConnectedProps<typeof connector>
export default connector(Players)
