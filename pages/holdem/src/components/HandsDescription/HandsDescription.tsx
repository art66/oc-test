import cn from 'classnames'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setSuitsColor } from '../../actions'
import { TSuitsColors } from '../../reducers/types'
import { getSuitsColors } from '../../selectors/ui'
import { Popup } from '../Popup/Popup'
import style from './style.cssmodule.scss'

const HandsDescription = () => {
  const suitsColors = useSelector(getSuitsColors)
  const dispatch = useDispatch()
  const createClickHandler = (color: TSuitsColors) => () => dispatch(setSuitsColor(color))

  return (
    <Popup className={style.pokerHandsPositioner}>
      <div className={style.pokerHandsWrap}>
        <div className={cn(style.combinationTable, suitsColors === '2-colors' ? style.twoColors : style.fourColors)}>
          <ul className={style.combinationLabels}>
            <li>Royal Flush</li>
            <li>Straight Flush</li>
            <li>Four of a Kind</li>
            <li>Full House</li>
            <li>Flush</li>
            <li>Straight</li>
            <li>Three of a Kind</li>
            <li>Two Pairs</li>
            <li>One Pair</li>
            <li>High Hand</li>
          </ul>
        </div>
        <div className={style.suiteSwitch}>
          <button type='button' className={style.button4colors} onClick={createClickHandler('4-colors')}>
            4 suit colours
          </button>
          <button type='button' className={style.button2colors} onClick={createClickHandler('2-colors')}>
            2 suit colours
          </button>
        </div>
      </div>
    </Popup>
  )
}

export default HandsDescription
