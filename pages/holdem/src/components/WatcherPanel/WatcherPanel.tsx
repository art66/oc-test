import React from 'react'
import Logs from '../Logs/Logs'
import { DetailsButton, HandsButton, LobbyButton, SoundButton } from '../MenuButtons/MenuButtons'
import style from './style.cssmodule.scss'

const WatcherPanel = () => (
  <div className={style.watcherPanel}>
    <div className={style.panelTopRow}>
      <LobbyButton className={style.toggleButton} />
      <DetailsButton className={style.toggleButton} />
      <HandsButton className={style.toggleButton} />
      <SoundButton className={style.toggleButton} />
    </div>
    <Logs />
  </div>
)

export default WatcherPanel
