import React from 'react'
import { useSelector } from 'react-redux'
import { TAppState } from '../../store/rootReducer'
import BuyIn from '../BuyIn/BuyIn'
import HandsDescription from '../HandsDescription/HandsDescription'
import Lobby from '../Lobby/Lobby'
import TableDetails from '../TableDetails/TableDetails'
import s from './style.cssmodule.scss'

const renderMenuItem = activeMenuItem => {
  switch (activeMenuItem) {
    case 'table-details':
      return <TableDetails />
    case 'lobby':
      return <Lobby />
    case 'hands-desc':
      return <HandsDescription />
    case 'buy-in':
      return <BuyIn />
    default:
      return null
  }
}

const PopupSelector = () => {
  const activeMenuItem = useSelector((state: TAppState) => state.ui.activeMenuItem)

  return <div className={s.popupPositioner}>{renderMenuItem(activeMenuItem)}</div>
}

export default PopupSelector
