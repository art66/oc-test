import { boolean } from '@storybook/addon-knobs'
import noop from 'lodash/noop'
import React from 'react'
import Button from './Button'

export const ButtonTest = () => {
  const queueable = boolean('queueable', false)
  const queued = boolean('queued', false)

  return (
    <div style={{ width: ' 320px', display: 'flex' }}>
      <Button label='Raise' queueable={queueable} queued={queued} action={noop} />
      <Button label='Bet' queueable={queueable} queued={queued} action={noop} />
      <Button label='Check' queueable={queueable} queued={queued} action={noop} />
    </div>
  )
}

ButtonTest.story = {
  name: 'Default'
}

export default {
  title: 'Holdem/Button'
}
