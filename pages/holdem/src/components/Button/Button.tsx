import cn from 'classnames'
import React from 'react'
import style from './style.cssmodule.scss'

export type TProps = {
  label: string
  pressed?: boolean
  queueable?: boolean
  queued?: boolean
  action: () => void
}

const Button = (props: TProps) => {
  const handleMove = () => {
    if (!props.pressed) {
      props.action()
    }
  }

  return (
    <button
      type='button'
      className={cn(style.btn, {
        [style.pressed]: props.pressed,
        [style.queueable]: props.queueable,
        [style.queued]: props.queued
      })}
      onClick={handleMove}
    >
      {props.label}
    </button>
  )
}

export default Button
