import { select, withKnobs } from '@storybook/addon-knobs'
import React from 'react'
import { setAppType } from '../../actions'
import StoreProvider from '../../containers/StoreProvider'
import { mockState } from '../../fixtures/store.mock'
import { configureStore } from '../../store/createStore'
import BuyIn from './BuyIn'

const store = configureStore(mockState)

const withProvider = story => <StoreProvider store={store}>{story()}</StoreProvider>

const WrapperClassNames = ['full', 'sidebar']

const BuyInTemplate = () => {
  const wrapperClassName = select('Full screen?', WrapperClassNames, WrapperClassNames[0])

  store.dispatch(setAppType(wrapperClassName))
  return (
    <div className={wrapperClassName}>
      <BuyIn />
    </div>
  )
}

export const Default = BuyInTemplate.bind({})

export default {
  title: 'Holdem/BuyIn',
  decorators: [withProvider, withKnobs]
}
