export const inRange = (val: number, min: number, max: number) => val >= min && val <= max
