import { LegacyMoneyInput } from '@torn/shared/components/LegacyMoneyInput/LegacyMoneyInput'
import { toMoney } from '@torn/shared/utils'
import React, { Component } from 'react'
import ReactDOMServer from 'react-dom/server'
import { connect, ConnectedProps } from 'react-redux'
import { closePopup, sitIn } from '../../actions'
import { api } from '../../services'
import { DollarIcon } from '../../svg/DollarIcon'
import { sanitizeInput } from '../../utils/sanitizeInput'
import { Popup } from '../Popup/Popup'
import { inRange } from './helpers'
import style from './style.cssmodule.scss'

type TProps = TReduxProps

interface IState {
  isBettingAllowed: boolean
  initialBuyInValue: number
  dataFetched: boolean
}

export class BuyIn extends Component<TProps, IState> {
  readonly state: IState = {
    isBettingAllowed: false,
    initialBuyInValue: 0,
    dataFetched: false
  }

  private maxBet = 0
  private minBet = 0
  private userMoney: number = null
  private buyInValue = 0

  componentDidMount() {
    // fetching this at every mount
    // because this data is changing often
    api.getBuyInLimit().then(buyInLimits => {
      this.maxBet = buyInLimits.maxMoney
      this.minBet = buyInLimits.minMoney
      this.userMoney = buyInLimits.money

      const initialBuyInValue = Math.min(this.userMoney, this.maxBet)

      return this.setState({
        dataFetched: true,
        initialBuyInValue,
        isBettingAllowed: this.isBetInRange(initialBuyInValue)
      })
    })
  }

  handleAmountChange = ({ value }) => {
    this.buyInValue = sanitizeInput(value)

    this.setState({ isBettingAllowed: this.isBetInRange(this.buyInValue) })
  }

  handleOkClick = () => {
    if (this.state.isBettingAllowed) {
      this.props.sitIn(this.buyInValue)
      this.props.closePopup()
    }
  }

  isBetInRange = (amount: number) => inRange(amount, this.minBet, this.maxBet)

  render() {
    return (
      <Popup className={style.buyInWrap} header='Buy-in Amount'>
        <div className={style.contentWrapper}>
          <div className={style.message}>
            Between ${toMoney(this.minBet)} and ${toMoney(this.maxBet)}
          </div>
          <div className={style.betInput}>
            <LegacyMoneyInput
              disabled={!this.state.dataFetched}
              maxValue={Math.min(this.userMoney, this.maxBet)}
              value={this.state.initialBuyInValue}
              onChange={this.handleAmountChange}
              symbol={ReactDOMServer.renderToString(<DollarIcon />)}
            />
          </div>
          <div className={style.buttonsWrap}>
            <button
              type='button'
              disabled={!this.state.isBettingAllowed}
              className={style.btn}
              onClick={this.handleOkClick}
            >
              Ok
            </button>
            <button type='button' className={style.btn} onClick={this.props.closePopup}>
              Cancel
            </button>
          </div>
        </div>
      </Popup>
    )
  }
}

const mapActionsToProps = {
  closePopup,
  sitIn
}

const connector = connect(null, mapActionsToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(BuyIn)
