import cn from 'classnames'
import sample from 'lodash/sample'
import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { playSound } from '../../actions'
import { TCard } from '../../interfaces'
import CardFace from '../CardFace/CardFace'
import cardsCommunity from './cards-community.cssmodule.scss'
import cardsHandMy from './cards-hand-my.cssmodule.scss'
import cardsHand from './cards-hand.cssmodule.scss'
import flipStyles from './flipper.cssmodule.scss'

const SIDE_FRONT = 'front'
const SIDE_BACK = 'back'
const STYLES_HOLDER = {
  opponent: cardsHand,
  self: cardsHandMy,
  community: cardsCommunity
}

export type TCardType = keyof typeof STYLES_HOLDER

interface IProps {
  cardname?: TCard
  type?: TCardType
  decoratorClass: 'dimOut' | 'glow'
}

interface IState {
  activeFace: 'front' | 'back'
  card: TCard
  isCardsFlip: boolean
}

class Card extends React.PureComponent<IProps & TReduxProps, IState> {
  private styles = STYLES_HOLDER[this.props.type]

  constructor(props) {
    super(props)

    this.state = {
      activeFace: SIDE_BACK,
      card: SIDE_BACK,
      isCardsFlip: false
    }
  }

  componentDidMount() {
    this._initiateCardState()
  }

  componentDidUpdate(_, prevState) {
    const { cardname = SIDE_BACK } = this.props
    const cardSide = cardname !== SIDE_BACK ? SIDE_FRONT : SIDE_BACK

    this._updateCardState(cardSide)
    if (this.state.isCardsFlip !== prevState.isCardsFlip) {
      this._playFlipSound(cardSide)
    }
  }

  _initiateCardState = () => {
    const { cardname = SIDE_BACK } = this.props

    this.setState({
      card: cardname
    })
  }

  _updateCardState = cardSide => {
    const { cardname = SIDE_BACK } = this.props

    this.setState(
      {
        activeFace: cardSide,
        card: cardname
      },
      () => this._checkCardFlip(cardSide)
    ) // only fires card's flip after its physical change
  }

  _checkCardFlip = cardSide => {
    setTimeout(() => {
      const { isCardsFlip: isCardsFlipPrev } = this.state
      const isCardsFlipNext = cardSide !== SIDE_BACK

      if (isCardsFlipPrev !== isCardsFlipNext) {
        this.setState({
          isCardsFlip: isCardsFlipNext
        })
      }
    }, 50) // postponed timer gives a guarantee for in-time card's flip.
  }

  _getRandomCardSound = () =>
    sample(['card_coming_1', 'card_coming_2', 'card_coming_3', 'card_coming_4', 'card_coming_5'] as const)

  _playFlipSound = cardSide => {
    if (cardSide === SIDE_BACK) {
      return
    }

    this.props.playSound(this._getRandomCardSound())
  }

  _getOwnClasses = () =>
    cn(
      [this.styles.card],
      [this.styles.cardSize],
      [this.styles.cardMargins],
      [flipStyles.flipperWrap],
      this.styles[this.props.decoratorClass]
    )

  render() {
    const { activeFace, isCardsFlip, card } = this.state
    const wrapperClass = this._getOwnClasses()

    return (
      <div className={`${wrapperClass} ${!isCardsFlip ? flipStyles.flipped : ''}`}>
        <div className={flipStyles.flipper}>
          <div className={flipStyles.front}>
            <CardFace styles={this.styles} cardname={activeFace === SIDE_BACK ? SIDE_BACK : card} />
          </div>
          <div className={flipStyles.back}>
            <CardFace styles={this.styles} cardname={SIDE_BACK} />
          </div>
        </div>
      </div>
    )
  }
}

const connector = connect(null, { playSound })

type TReduxProps = ConnectedProps<typeof connector>
export default connector(Card)
