import noop from 'lodash/noop'
import React from 'react'
import { useDispatch } from 'react-redux'
import { openBuyInPopup } from '../../actions'
import style from './style.cssmodule.scss'

type TProps = {
  position: number
}

export const SitInButton = (props: TProps) => {
  const dispatch = useDispatch()
  const createOnClickAction = (position: number) => () => {
    dispatch(openBuyInPopup(position))
  }

  return (
    <div className={style[`buttonPosition-${props.position}`]}>
      <button
        type='button'
        aria-label='sit in'
        className={style.sitInButton}
        onClick={createOnClickAction(props.position)}
        onKeyDown={noop}
      >
        Sit In
      </button>
    </div>
  )
}
