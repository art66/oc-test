import React from 'react'
import { useSelector } from 'react-redux'
import { THand } from '../../interfaces'
import { getCombination, getIsShowdownTime } from '../../selectors'
import Card, { TCardType } from '../Card/Card'

type TCardPairProps = {
  pair: THand
  type: Extract<TCardType, 'opponent' | 'self'>
  className: string
}

export const CardPair = (props: TCardPairProps) => {
  const combination = useSelector(getCombination)
  const showdown = useSelector(getIsShowdownTime)

  const cardInCombination = card => combination.some(c => c === card)

  const getDecoratorClass = card => {
    if (cardInCombination(card) && showdown) {
      return 'glow' as const
    }
  }

  return (
    <div className={props.className}>
      <Card
        key={props.pair[0]}
        cardname={props.pair[0]}
        type={props.type}
        decoratorClass={getDecoratorClass(props.pair[0])}
      />
      <Card
        key={props.pair[1]}
        cardname={props.pair[1]}
        type={props.type}
        decoratorClass={getDecoratorClass(props.pair[1])}
      />
    </div>
  )
}
