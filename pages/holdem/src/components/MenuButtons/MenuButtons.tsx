import cn from 'classnames'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { closePopup, openPopup, toggleSound } from '../../actions'
import { TPopupItem } from '../../reducers/types'
import { getActiveMenuItem, getSoundStatus } from '../../selectors'
import { DetailsIcon } from '../../svg/DetailsIcon'
import { HandsIcon } from '../../svg/HandsIcon'
import { LobbyIcon } from '../../svg/LobbyIcon'
import { MuteIcon } from '../../svg/MuteIcon'
import { UnmuteIcon } from '../../svg/UnmuteIcon'
import style from './style.cssmodule.scss'

type TProps = {
  className: string
}
const useActionCreator = (action: TPopupItem) => {
  const dispatch = useDispatch()
  const activeMenuItem = useSelector(getActiveMenuItem)

  return () => dispatch(activeMenuItem === action ? closePopup() : openPopup(action))
}

export const DetailsButton = (props: TProps) => (
  <button type='button' onClick={useActionCreator('table-details')} className={cn(style.toggleButton, props.className)}>
    <DetailsIcon />
  </button>
)

export const LobbyButton = (props: TProps) => (
  <button type='button' onClick={useActionCreator('lobby')} className={cn(style.toggleButton, props.className)}>
    <LobbyIcon />
  </button>
)

export const HandsButton = (props: TProps) => (
  <button type='button' onClick={useActionCreator('hands-desc')} className={cn(style.toggleButton, props.className)}>
    <HandsIcon />
  </button>
)

export const SoundButton = (props: TProps) => {
  const dispatch = useDispatch()
  const soundOn = useSelector(getSoundStatus)
  const action = () => dispatch(toggleSound())

  return (
    <button type='button' onClick={action} className={cn(style.toggleButton, props.className)}>
      {soundOn ? <MuteIcon /> : <UnmuteIcon />}
    </button>
  )
}
