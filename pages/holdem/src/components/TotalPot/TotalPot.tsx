import { toMoney } from '@torn/shared/utils'
import React from 'react'
import { useSelector } from 'react-redux'
import { getIsPotVisible, getTotalPot } from '../../selectors'
import style from './style.cssmodule.scss'

export const TotalPot = () => {
  const isPotVisible = useSelector(getIsPotVisible)
  const totalPot = useSelector(getTotalPot)

  return (
    isPotVisible && (
      <div className={style.totalPotWrap}>
        <div className={style.totalPot}>
          <b>POT: </b>
          {toMoney(totalPot, '$')}
        </div>
      </div>
    )
  )
}
