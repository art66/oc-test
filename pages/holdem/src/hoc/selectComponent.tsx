import React from 'react'
import { useSelector } from 'react-redux'
import { TLayoutType, getLayoutType } from '../selectors'

type TComponentsMap<TProps> = { [K in TLayoutType]: React.ComponentType<TProps> }

export const selectComponent = <TOwnProps, >(componentsMap: TComponentsMap<TOwnProps>) => (props: TOwnProps) => {
  const layoutType = useSelector(getLayoutType)

  const SelectedComponent = componentsMap[layoutType]

  return <SelectedComponent {...props} />
}
