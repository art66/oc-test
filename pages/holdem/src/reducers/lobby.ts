import { handleActions } from 'redux-actions'
import * as actionTypes from '../actions/actionTypes'
import { ILobby } from '../interfaces'
import { TSetLobbyDataAction, TLobbyEventAction } from './types'
import { getServerState } from '../selectors'

const initialState: ILobby = {
  tables: []
}

export default handleActions(
  {
    [actionTypes.SET_LOBBY_DATA]: (state: Readonly<ILobby>, action: TSetLobbyDataAction) => {
      return {
        ...state,
        tables: action.payload.tables
      }
    },
    [actionTypes.LOBBY_CHANNEL_EVENT]: (state: Readonly<ILobby>, action: TLobbyEventAction) => {
      return {
        ...state,
        tables: getServerState(action.response).tables
      }
    }
  },
  initialState
)
