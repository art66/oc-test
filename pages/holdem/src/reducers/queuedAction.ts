import { handleActions } from 'redux-actions'
import * as actionTypes from '../actions/actionTypes'

const initialState = {
  action: [],
  testPlayers: {
    0: { actions: [] },
    1: { actions: [] },
    2: { actions: [] },
    3: { actions: [] },
    4: { actions: [] },
    5: { actions: [] },
    6: { actions: [] },
    7: { actions: [] },
    8: { actions: [] },
    9: { actions: [] }
  }
}

const arraysEqual = (first, second) => {
  return first.every(e => second.includes(e)) && second.every(e => first.includes(e))
}

export default handleActions(
  {
    [actionTypes.START_NEXT_GAME]: state => {
      return {
        ...state,
        action: []
      }
    },
    [actionTypes.PRESS_BUTTON]: (state, action) => {
      return {
        ...state,
        buttonPressed: action.payload
      }
    },
    [actionTypes.UNPRESS_BUTTON]: state => {
      return {
        ...state,
        buttonPressed: undefined
      }
    },
    [actionTypes.TOGGLE_QUEUE]: (state, action) => {
      return {
        ...state,
        action: !action.payload || arraysEqual(state.action, action.payload) ? [] : action.payload
      }
    },
    [actionTypes.TOGGLE_QUEUE_TEST_PLAYER]: (state, action) => {
      const { position, actions } = action.payload

      return {
        ...state,
        testPlayers: {
          ...state.testPlayers,
          [position]:
            !actions || arraysEqual(state.testPlayers[position].actions, actions) ? { actions: [] } : action.payload
        }
      }
    },
    [actionTypes.PLAYER_MADE_MOVE]: state => {
      return {
        ...state,
        buttonPressed: undefined
      }
    },
    [actionTypes.MAKE_A_MOVE]: state => {
      return {
        ...state,
        action: []
      }
    },
    [actionTypes.TEST_PLAYER_ACTION]: (state, action) => {
      const { position } = action.payload

      return {
        ...state,
        testPlayers: {
          ...state.testPlayers,
          [position]: { actions: [] }
        }
      }
    }
  },
  initialState
)
