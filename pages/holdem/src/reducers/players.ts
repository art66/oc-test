import { handleActions } from 'redux-actions'
import { IPlayer, IPlayers } from '../interfaces'
import { TAction, TPublicChannelAction } from './types'
import * as actionTypes from '../actions/actionTypes'

const initialState = {}

function getAllFromServer(state: IPlayers, action: TPublicChannelAction<{ players: IPlayers }>) {
  return action.serverState.players || state
}

export default handleActions(
  {
    [actionTypes.PLAYER_MADE_MOVE]: getAllFromServer,
    [actionTypes.GET_STATE]: getAllFromServer,
    [actionTypes.ASK_STATE_SUCCESS]: (state: IPlayers, action: TAction<{ players: IPlayers }>) => {
      return action.payload.players || state
    },
    [actionTypes.PLAYER_JOIN]: (state: IPlayers, action: TPublicChannelAction<{ player: IPlayer }>) => {
      const { serverState } = action

      return {
        ...state,
        ...serverState.player
      }
    },
    [actionTypes.UPDATE_PLAYER]: (state: IPlayers, action: TPublicChannelAction<{ player: IPlayer }>) => {
      return {
        ...state,
        ...action.serverState.player
      }
    },
    [actionTypes.REMOVE_PLAYER]: (state: IPlayers, action: TPublicChannelAction<{ player: number[] }>) => {
      const { player } = action.serverState

      return Object.keys(state).reduce((acc, key) => {
        if (!player.includes(+key)) {
          return {
            ...acc,
            [key]: state[key]
          }
        }

        return acc
      }, {})
    },
    [actionTypes.UPDATE_WAIT_BUTTONS]: (
      state: IPlayers,
      action: TAction<{ position: number; step: string; pressed: boolean }>
    ) => {
      const { position, step, pressed } = action.payload

      return {
        ...state,
        [position]: {
          ...state[position],
          waitButtons: {
            ...state[position].waitButtons,
            [step]: {
              ...state[position].waitButtons[step],
              pressed
            }
          }
        }
      }
    }
  },
  initialState
)
