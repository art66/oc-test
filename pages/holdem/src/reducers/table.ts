import pick from 'lodash/fp/pick'
import { handleActions } from 'redux-actions'
import { ITable } from '../interfaces'
import { TAction, TPublicChannelAction } from './types'
import * as actionTypes from '../actions/actionTypes'

const initialState = {
  round: 0
}

const tableProps = pick([
  'amountCall',
  'bigBlind',
  'maxBet',
  'minBet',
  'phase',
  'totalPot',
  'timeleft',
  'turn',
  'communityCards',
  'raised',
  'round',
  'userAction',
  'dealer',
  'roundPot'
])

const mergeState = (state: Readonly<ITable>, action: TPublicChannelAction<Partial<ITable>>) => ({
  ...state,
  ...tableProps(action.serverState)
})

export default handleActions(
  {
    [actionTypes.TICK]: (state: Readonly<ITable>, action: TAction<number>) => {
      return {
        ...state,
        timeleft: action.payload
      }
    },
    [actionTypes.SUBSCRIBE_TO_TABLE]: (state: Readonly<ITable>, action: TAction<{ tableID: number }>) => {
      return {
        ...state,
        tableID: action.payload.tableID
      }
    },
    [actionTypes.ASK_STATE_SUCCESS]: (state: Readonly<ITable>, action: TAction<Partial<ITable>>) => {
      return {
        ...state,
        ...tableProps(action.payload)
      }
    },
    [actionTypes.UPDATE_PLAYER]: mergeState,
    [actionTypes.REMOVE_PLAYER]: mergeState,
    [actionTypes.PLAYER_JOIN]: mergeState,
    [actionTypes.PLAYER_MADE_MOVE]: mergeState,
    [actionTypes.GET_STATE]: mergeState
  },
  initialState
)
