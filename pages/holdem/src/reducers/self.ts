import { handleActions } from 'redux-actions'
import { ISelf, THandIDs } from '../interfaces'
import { TAction, TPublicChannelAction, TPrivateChannelAction } from './types'
import { getServerState } from '../selectors'
import * as actionTypes from '../actions/actionTypes'

const initialState: ISelf = {
  satAtTheTable: false,
  betAmount: 0,
  hand: [],
  handIDs: {
    prevHandID: undefined,
    currentHandID: undefined
  },
  identity: {
    isDev: false
  },
  prevBetAmount: 0,
  prevCallAmount: 0,
  prevAction: '',
  sitPosition: -1
}

const handleIDsUpdate = (state: THandIDs, token: string): THandIDs => {
  if (state.currentHandID === undefined && state.prevHandID === undefined) {
    return {
      ...state,
      currentHandID: token
    }
  }

  if (state.currentHandID !== token) {
    return {
      prevHandID: state.currentHandID,
      currentHandID: token
    }
  }

  return state
}

export default handleActions(
  {
    [actionTypes.SAVE_CREDENTIALS]: (state: Readonly<ISelf>, action: TAction<{ userID: number; isDev: boolean }>) => {
      return {
        ...state,
        identity: {
          userID: action.payload.userID,
          isDev: action.payload.isDev
        }
      }
    },
    [actionTypes.PRIVATE_CHANNEL_EVENT]: (state: Readonly<ISelf>, action: TPrivateChannelAction) => {
      return {
        ...state,
        hand: getServerState(action.response).hand,
        handIDs: handleIDsUpdate(state.handIDs, action.response.data.message.token)
      }
    },
    [actionTypes.GET_STATE]: (state: Readonly<ISelf>, action: TPublicChannelAction<{ token: string }>) => {
      return {
        ...state,
        handIDs: handleIDsUpdate(state.handIDs, action.serverState.token)
      }
    },
    [actionTypes.ASK_STATE_SUCCESS]: (state: Readonly<ISelf>, action: TAction<{ token: string }>) => {
      return {
        ...state,
        handIDs: handleIDsUpdate(state.handIDs, action.payload.token)
      }
    },
    [actionTypes.PLAYER_MADE_MOVE]: (state: Readonly<ISelf>, action: TPublicChannelAction<{ token: string }>) => {
      return {
        ...state,
        handIDs: handleIDsUpdate(state.handIDs, action.serverState.token)
      }
    },
    [actionTypes.START_NEXT_GAME]: (state: Readonly<ISelf>) => {
      return {
        ...state,
        betAmount: 0
      }
    },
    [actionTypes.SET_BET_AMOUNT]: (state: Readonly<ISelf>, action: TAction<number>) => {
      return {
        ...state,
        betAmount: action.payload
      }
    },
    [actionTypes.SET_PREV_ACTION]: (state: Readonly<ISelf>, action: TAction<number>) => {
      return {
        ...state,
        prevAction: action.payload || initialState.prevAction
      }
    },
    [actionTypes.SET_PREV_BET_AMOUNT]: (state: Readonly<ISelf>, action: TAction<number>) => {
      return {
        ...state,
        prevBetAmount: action.payload || initialState.prevBetAmount
      }
    },
    [actionTypes.SET_PREV_CALL_AMOUNT]: (state: Readonly<ISelf>, action: TAction<number>) => {
      return {
        ...state,
        prevCallAmount: action.payload || initialState.prevCallAmount
      }
    },
    [actionTypes.SET_SIT_POSITION]: (state: Readonly<ISelf>, action: TAction<number>) => {
      return {
        ...state,
        sitPosition: action.payload ?? initialState.sitPosition
      }
    },
    [actionTypes.SET_SELF_PLAYER_SAT]: (state: Readonly<ISelf>) => {
      return {
        ...state,
        satAtTheTable: true
      }
    },
    [actionTypes.SET_SELF_PLAYER_AWAY]: (state: Readonly<ISelf>) => {
      return {
        ...state,
        satAtTheTable: false
      }
    }
  },
  initialState
)
