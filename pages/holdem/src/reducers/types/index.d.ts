import * as a from '../../actions/actionTypes'
import * as i from '../../interfaces'

export type TAction<P, T = string> = {
  type: T
  payload: P
}

export type TPublicChannelAction<P, T = string> = {
  type: T
  serverState: {
    [key in string]?: any
  } &
    P
}

export type TResponsePayloadAction<P, T = string> = {
  type: T
  response: {
    channel: string
    timestamp: string
    uid: string
    data: {
      message: P
    }
  }
}

export type TSetLobbyDataAction = TAction<i.ILobby, typeof a.SET_LOBBY_DATA>
export type TLobbyEventAction = TResponsePayloadAction<i.ILobby, typeof a.SET_LOBBY_DATA>

export type TGetLogAction = TPublicChannelAction<{ chatLog: i.ILogRecord[] }, typeof a.GET_STATE>
export type TLogsAction = TPublicChannelAction<
  {
    data: i.ILogRecord
    eventType: 'chat'
  },
  typeof a.LOGS
>

export type TPrivateChannelAction = TResponsePayloadAction<
  {
    eventType: 'getState'
    hand: i.THand
    token: string
  },
  typeof a.PRIVATE_CHANNEL_EVENT
>

export type TPopupItem = 'lobby' | 'table-details' | 'hands-desc' | 'buy-in'

export type TSuitsColors = '2-colors' | '4-colors'
