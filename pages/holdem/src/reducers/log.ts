import { handleActions } from 'redux-actions'
import { ILogRecord } from '../interfaces'
import { TAction, TGetLogAction, TLogsAction } from './types'
import * as actionTypes from '../actions/actionTypes'

const initialState: ILogRecord[] = []

export default handleActions(
  {
    [actionTypes.GET_STATE]: (_, action: TGetLogAction) => {
      return action.serverState.chatLog || []
    },
    [actionTypes.ASK_STATE_SUCCESS]: (
      _,
      action: TAction<{ chatLog: ILogRecord[] }, typeof actionTypes.ASK_STATE_SUCCESS>
    ) => {
      return action.payload.chatLog || []
    },
    [actionTypes.LOGS]: (state: Readonly<ILogRecord[]>, action: TLogsAction) => {
      const newMessage = action.serverState.data

      return [...state, newMessage]
    }
  },
  initialState
)
