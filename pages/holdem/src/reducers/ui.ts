import { handleActions } from 'redux-actions'
import { setSuitsColor } from '../actions'
import * as actionTypes from '../actions/actionTypes'
import { HOLDEM_SOUND_STATUS } from '../constants'
import { TAction, TPopupItem, TSuitsColors } from './types'

export interface IStateUI {
  tablesBg: boolean
  sound: boolean
  activeMenuItem?: TPopupItem
  suitsColors: TSuitsColors
}

export const initialState: IStateUI = {
  tablesBg: true,
  sound: localStorage.getItem(HOLDEM_SOUND_STATUS) === 'true',
  suitsColors: '4-colors'
}

export default handleActions(
  {
    [actionTypes.TOGGLE_SOUND]: (state: IStateUI): IStateUI => ({
      ...state,
      sound: !state.sound
    }),
    [actionTypes.OPEN_POPUP]: (state: IStateUI, action: TAction<TPopupItem>): IStateUI => ({
      ...state,
      activeMenuItem: action.payload
    }),
    [actionTypes.CLOSE_POPUP]: (state: IStateUI): IStateUI => ({
      ...state,
      activeMenuItem: null
    }),
    [actionTypes.TOGGLE_TABLES]: (state: IStateUI): IStateUI => ({
      ...state,
      tablesBg: !state.tablesBg
    }),
    [actionTypes.SET_SUITS_COLOR]: (state: IStateUI, action: ReturnType<typeof setSuitsColor>): IStateUI => ({
      ...state,
      suitsColors: action.payload
    })
  },
  initialState
)
