import { handleActions } from 'redux-actions'
import { setAppType } from '../actions'
import { SET_APP_TYPE } from '../actions/actionTypes'
import { isSidebarPresent } from '../utils'

export type TAppType = 'sidebar' | 'full'

const getAppType = (): TAppType => (isSidebarPresent() ? 'sidebar' : 'full')
const initialState = getAppType()

export default handleActions(
  {
    [SET_APP_TYPE]: (_, action: ReturnType<typeof setAppType>) => action.payload
  },
  initialState
)
