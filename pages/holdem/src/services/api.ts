import { ADMIN_URL, ROOT_URL, TEST_DATA_URL } from '../constants'
import { getIsTestTable } from '../selectors'
import store from '../store/createStore'
import { fetchUrl } from '../utils'

let isTestTable = false

const testTableListener = () => {
  isTestTable = getIsTestTable(store.getState())
}

try {
  setTimeout(() => store.subscribe(testTableListener), 0)
} catch (e) {
  console.error(e)
}

type TRequestData = {
  step: string
} & {
  [key: string]: any
}

const getUrl = url => url || (isTestTable ? TEST_DATA_URL : ROOT_URL)

export const callApi = (data: TRequestData, url = ROOT_URL) => {
  return fetchUrl(getUrl(url), data)
}

export const getCredentials = () => callApi({ step: 'connect' })
export const getLobbyData = () => callApi({ step: 'lobby' })
export const getGameState = data => callApi({ step: 'getState', ...data })
export const joinTable = () => callApi({ step: 'join' })
export const sitIn = (positionID, amount) => callApi({ step: 'sitin', positionID, amount })
export const makeMove = data => callApi({ step: 'makeMove', ...data })
export const leave = () => callApi({ step: 'leave' })
export const sitOut = () => callApi({ step: 'sitout' })
export const imBack = () => callApi({ step: 'imback' })
export const timeout = () => callApi({ step: 'timeout' })
export const getBuyInLimit = () => callApi({ step: 'getbuyinlimit' })
export const startNextGame = () => callApi({ step: 'nextGame' })
export const checkSitOut = () => callApi({ step: 'kicksitout' })
export const waitBigBlind = (pressed: boolean) => callApi({ step: 'waitbb', pressed })
export const payButton = (pressed: boolean) => callApi({ step: 'paybutton', pressed })
export const details = () => callApi({ step: 'details' })
export const testCards = () => callApi({ step: 'testcards' })
export const logCards = data => callApi({ step: 'logger', ...data })
export const gameControl = data => callApi(data, ADMIN_URL)
