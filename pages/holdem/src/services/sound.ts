import { Howl } from 'howler'

export const tracksList = [
  'card_coming_1',
  'card_coming_2',
  'card_coming_3',
  'card_coming_4',
  'card_coming_5',
  'new_game',
  'game_end',
  'call',
  'bet',
  'raise',
  'allin',
  'fold',
  'check',
  'tick_1',
  'tick_2',
  'you_won',
  'player_won',
  'your_turn',
  'button',
  'chips_moving_into_pot'
] as const

export type TSoundID = typeof tracksList[number]

const actionsWithoutSound = ['leave']

export const sounds = []

const path = '/audio/holdem'

export const loadSounds = () => {
  tracksList.forEach(track => {
    sounds.push({
      soundID: track,
      sound: new Howl({ src: [`${path}/${track}.mp3`, `${path}/${track}.ogg`, `${path}/${track}.wav`] })
    })
  })
}

export const play = (soundID: string) => {
  try {
    if (actionsWithoutSound.includes(soundID.toString())) {
      return
    }

    const sound = sounds.find(snd => snd.soundID.toString() === soundID.toString())

    if (sound) {
      sound.sound.play()
    } else {
      console.error(`There is no sound with ID "${soundID}" in trackList`)
    }
  } catch (e) {
    console.error('Error occurred when playing sound')
  }
}
