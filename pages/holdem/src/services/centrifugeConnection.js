import * as actionTypes from '../actions/actionTypes'

const LOBBY_CHANNEL_NAME = 'holdemlobby'

const log = (type, response) => {
  const { message } = response.data
  const { eventType } = message

  if (__DEV__) {
    console.groupCollapsed(type + (eventType ? ` => ${eventType}` : ''))
    console.log(message)
    console.groupEnd()
  }
}

class CentrifugeConnection {
  getCentrifuge(options) {
    if (!this.centrifuge) {
      if (!options) {
        throw new Error('no options provided for getCentrifuge()')
      }
      this.centrifuge = new window.Centrifuge(options)
    }
    return this.centrifuge
  }

  connect(credentials) {
    const options = `${credentials.url}/connection/websocket`
    const centrifuge = this.getCentrifuge(options)

    centrifuge.setToken(credentials.token)

    return new Promise((resolve, reject) => {
      centrifuge.on('connect', () => resolve(centrifuge))
      centrifuge.on('error', error => reject(error))
      centrifuge.connect()
    })
  }

  setChannels({ userID, channel: PublicChannel }) {
    const PRIVATE_CHANNEL_NAME = `${PublicChannel}#${userID}`

    const centrifuge = this.getCentrifuge()

    // resubscribe for public channel
    if (this.publicSubscription) {
      this.publicSubscription.unsubscribe()
    }
    this.publicSubscription = centrifuge.subscribe(PublicChannel, this.publicEmitter)

    // resubscribe for private channel
    if (this.privateSubscription) {
      this.privateSubscription.unsubscribe()
    }
    this.privateSubscription = centrifuge.subscribe(PRIVATE_CHANNEL_NAME, this.privateEmitter)

    // for lobby subscribe only once
    // since channel name won't change
    if (!this.lobbySubscription) {
      this.lobbySubscription = centrifuge.subscribe(LOBBY_CHANNEL_NAME, this.lobbyEmitter)
    }
  }

  publicEmitter = response => {
    this.emitter(actionTypes.PUBLIC_CHANNEL_EVENT, response)
  }

  privateEmitter = response => {
    this.emitter(actionTypes.PRIVATE_CHANNEL_EVENT, response)
  }

  lobbyEmitter = response => {
    this.emitter(actionTypes.LOBBY_CHANNEL_EVENT, response)
  }

  emitter(type, response) {
    log(type, response)
    this.emit({ type, response })
  }

  setEmitter(emit) {
    this.emit = emit
  }

  unsubscribe() {
    this.publicSubscription.unsubscribe()
    this.privateSubscription.unsubscribe()
    this.lobbySubscription.unsubscribe()
  }
}

export default new CentrifugeConnection()
