import { THand, TCards } from './pokerCommon'

export interface IPlayer {
  acceptedActions: string[]
  amountCall: number
  avatar: string
  hand: THand
  isBigBlind: boolean
  isDealer: boolean
  isGameFinished: boolean
  isSitOut: boolean
  isSmallBlind: boolean
  minBet: number
  maxBet: number
  minRaise: number
  maxRaise: number
  money: number
  place: number
  playername: string
  pot: number
  preselectButtons: any //todo: describe
  revealButton: boolean
  sitOutButtons: any
  state: string
  status: string
  totalPot: number
  userID: number
  waitButtons: any
  winnings: number
  bestCombination?: TCards
  appearance: TAppearance
  statusIcon: string
  showCards: boolean
  self: boolean
  index: number
}

export interface IPlayers {
  [key: number]: IPlayer
}

export type TAppearance = 'winner' | 'active' | 'folded' | 'default'
