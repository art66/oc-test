import { THand } from './pokerCommon'

export type TBuyInLimit = {
  minMoney: number
  maxMoney: number
}

export type THandIDs = {
  prevHandID: string | undefined
  currentHandID: string | undefined
}

export interface ISelf {
  satAtTheTable: boolean
  betAmount: number
  hand: THand
  handIDs: THandIDs
  identity: {
    userID?: number
    isDev: boolean
  }
  prevBetAmount: number
  prevCallAmount: number
  prevAction: string
  sitPosition: number
}
