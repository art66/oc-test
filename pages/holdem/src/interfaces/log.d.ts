export interface ILogRecord {
  author: string
  color: string
  icon: string
  meta: string
  message: string
  old?: boolean
  timestamp: string
  userID: string
}
