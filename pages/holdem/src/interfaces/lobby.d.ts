export interface ILobbyTable {
  channel: string
  ID: number
  owner: number
  name: string
  maxplayers: number
  stakes: string
  betlimit: string
  minmoney: number
  maxmoney: number
  speed: number
  type: string
  password: string | number
  players: number
  minMoney: number
  maxMoney: number
}

export interface ILobby {
  tables: ILobbyTable[]
}
