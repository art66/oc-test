import { TCommunityCards } from './pokerCommon'

export type TPhase = 'Started' | 'ready' | 'ended' | 'inactive'

export interface ITable {
  amountCall: number
  bigBlind: number
  communityCards: TCommunityCards
  phase: TPhase
  raised: boolean
  round: string
  tableID: string
  timeleft: number
  totalPot: number
  turn: number
  maxplayers: number
  userAction: string
  maxBet: number
  minBet: number
  dealer: number
  roundPot: number
}
