export const BUTTON_PRESS_DELAY = 300
export const PLAYER_MOVE_DELAY = 2000
export const KICK_SITTING_OUT_DELAY = 150000
export const DELAY_PLAY_NEW_GAME_SOUND = 1500
export const SECONDS_LEFT_RESOLVE_SHOWDOWN = 3
export const MAX_PLAYERS = 9
export const ROOT_URL = '/loader.php?sid=holdemData'
export const ADMIN_URL = '/loader.php?sid=holdemAdmin'
export const TEST_DATA_URL = '/loader.php?sid=holdemTestData'
export const USER_MONEY_URL = '/inputMoneyAction.php'
export const SID_FULL = 'holdemFull'
export const SID_WITH_SIDEBAR = 'holdem'
export const SOCKET_URL = 'wss://dev-www.torn.com:5002'
export const HOLDEM_SOUND_STATUS = 'HOLDEM_SOUND_STATUS'
export const GAME_ENDED = 'ended'
export const GAME_READY = 'ready'
export const GAME_STARTED = 'Started'
export const GAME_INACTIVE = 'inactive'
export const TABLE_SKIN_SET = {
  2: 'newbie_corner',
  3: 'hobo_holdem',
  4: 'five_star_man',
  5: 'sprinkles',
  6: 'gatling_gun',
  7: 'quickdraw',
  8: 'tight_knit',
  9: 'ballsy',
  10: 'pound_it',
  11: 'old_n_slow',
  12: 'tripod',
  13: 'slow_cooker',
  14: 'fire_pit',
  15: 'high_rollers',
  16: 'oligarch',
  17: 'fourplay',
  18: 'duel_at_dawn',
  19: 'juan_on_juan',
  20: 'boom_or_bust',
  21: 'old_folks_home',
  22: 'comatose_cove',
  23: 'periodic',
  24: 'easy_street',
  25: 'cats_chance',
  26: 'broke_jokes',
  27: 'six_of_the_best',
  28: 'river_wizard'
}
export const STATUS_SITTING_OUT = 'Sitting out'
export const STATUS_WAITING = 'Waiting'
export const STATUS_WAITING_BLIND = 'Waiting BB'
export const STATUS_ACTIVE = 'Active'
export const STATUS_FOLD = 'Fold'
export const STATUS_FOLDED = 'Folded'
export const STATUS_WINNER = 'Winner'
export const STATUSES_OUT_OF_GAME = [
  STATUS_SITTING_OUT,
  STATUS_WAITING,
  STATUS_WAITING_BLIND,
  STATUS_FOLD,
  STATUS_FOLDED
]
export const STATUSES_HIDDEN_CARDS = [STATUS_SITTING_OUT, STATUS_WAITING, STATUS_WAITING_BLIND]
export const STATUSES_FOLDED = [STATUS_FOLD, STATUS_FOLDED]
export const KEY_CODE_MOUSE_LEFT = 1
export const PAGE_TITLE_BLINK_DELAY = 1000
export const PAGE_TITLE_DEFAULT = 'Poker | TORN'
export const PAGE_TITLE_YOUR_TURN = 'Your turn | TORN'
export const MEDIA_TYPES = {
  TYPE_SMALL: 'small',
  TYPE_MEDIUM: 'medium',
  TYPE_LARGE: 'large'
}
export const TABLE_SIZES = {
  SMALL: 320,
  MEDIUM: 578,
  MEDIUM_SIDEBAR: '578_s',
  LARGE: 976
}

export const TEST_TABLES_IDS = [29, 30]

export const STORAGE_KEY_SUITS_COLORS = 'holdem_suits_type'
