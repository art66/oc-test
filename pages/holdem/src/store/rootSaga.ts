import { all } from 'redux-saga/effects'
import automaticMoveSaga from '../sagas/automaticMoveSaga'
import initAppSaga from '../sagas/initAppSaga'
import initLobbyData from '../sagas/initLobbyData'
import initWSLoop from '../sagas/initWSLoop'
import logCards from '../sagas/logCards'
import moveSaga from '../sagas/moveSaga'
import openBuyInSaga from '../sagas/openBuyInSaga'
import pageTitleSaga from '../sagas/pageTitleSaga'
import publicChannel from '../sagas/publicChannel'
import setSuitsColors from '../sagas/setSuitsColorsSaga'
import sitInWatcher from '../sagas/sitInWatcher'
import sitOutImBack from '../sagas/sitOutImBack'
import soundSaga from '../sagas/soundSaga'
import soundToggleSaga from '../sagas/soundToggleSaga'
import testPlayersSaga from '../sagas/testPlayersSaga'
import timeOutSaga from '../sagas/timeOutSaga'
import waitButtonsSaga from '../sagas/waitButtons'
import watchAskState from '../sagas/watchAskState'
import { watchSelfSatStateSaga } from '../sagas/watchSelfSatStateSaga'
import watchTableChange from '../sagas/watchTableChange'

export default function* rootSaga() {
  yield all([
    initAppSaga(),
    initLobbyData(),
    initWSLoop(),
    watchAskState(),
    watchTableChange(),
    automaticMoveSaga(),
    publicChannel(),
    sitInWatcher(),
    timeOutSaga(),
    moveSaga(),
    waitButtonsSaga(),
    soundSaga(),
    soundToggleSaga(),
    sitOutImBack(),
    logCards(),
    pageTitleSaga(),
    testPlayersSaga(),
    watchSelfSatStateSaga(),
    openBuyInSaga(),
    setSuitsColors()
  ])
}
