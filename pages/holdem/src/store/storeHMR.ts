import makeRootReducer from './rootReducer'

const activateStoreHMR = (store: any) => {
  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer(makeRootReducer())
    })
  }
}

export default activateStoreHMR
