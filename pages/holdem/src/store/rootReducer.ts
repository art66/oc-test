import infoBox from '@torn/shared/middleware/infoBox'
import { combineReducers } from 'redux'
import { createResponsiveStateReducer, IBrowser } from 'redux-responsive'
import { ILobby, ILogRecord, IPlayers, ISelf, ITable } from '../interfaces'
import appType, { TAppType } from '../reducers/appType'
import lobby from '../reducers/lobby'
import log from '../reducers/log'
import players from '../reducers/players'
import queuedAction from '../reducers/queuedAction'
import self from '../reducers/self'
import table from '../reducers/table'
import ui, { IStateUI } from '../reducers/ui'

// TODO: add types where any is used
export type TAppState = {
  browser: IBrowser
  infoBox: any
  lobby: ILobby
  log: ILogRecord[]
  players: IPlayers
  queuedAction: any
  table: ITable
  ui: IStateUI
  self: ISelf
  appType: TAppType
}

const makeRootReducer = () =>
  combineReducers<TAppState>({
    browser: createResponsiveStateReducer({
      small: 600,
      medium: 1000,
      large: 5000
    }),
    infoBox,
    lobby,
    log,
    players,
    queuedAction,
    table,
    ui,
    self,
    appType
  })

export default makeRootReducer
