import { applyMiddleware, compose, createStore, Store } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import createSagaMiddleware from 'redux-saga'

import makeRootReducer from './rootReducer'
import rootSaga from './rootSaga'
import activateStoreHMR from './storeHMR'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    getCookie: any
    devToolsExtension: any
  }
}

const sagaMiddleware = createSagaMiddleware()

export const configureStore = (initialState: object = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [sagaMiddleware]
  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  if (__DEV__) {
    const { __REDUX_DEVTOOLS_EXTENSION__ } = window

    if (__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store: Store<any> = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer as any, applyMiddleware(...middleware), ...enhancers)
  )

  sagaMiddleware.run(rootSaga)
  activateStoreHMR(store)

  return store
}

export default configureStore()
