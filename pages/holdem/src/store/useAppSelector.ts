import { TypedUseSelectorHook, useSelector } from 'react-redux'
import { TAppState } from './rootReducer'

export const useAppSelector: TypedUseSelectorHook<TAppState> = useSelector
