import { createSelector } from 'reselect'
import { getUserID } from '.'
import { IPlayer } from '../interfaces'
import { getPlayersProps } from './players'

export const getSelfPlayerProps = createSelector(
  [getPlayersProps, getUserID],
  (players, userID) => Object.values(players).find((player: IPlayer) => player.userID === userID) as IPlayer
)
