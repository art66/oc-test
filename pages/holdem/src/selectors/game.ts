import flatten from 'lodash/flatten'
import inRange from 'lodash/inRange'
import { createSelector } from 'reselect'
import positionsMap from '../components/Players/positionsMap'
import { GAME_ENDED, GAME_INACTIVE, GAME_STARTED, SECONDS_LEFT_RESOLVE_SHOWDOWN, TEST_TABLES_IDS } from '../constants'
import { IPlayers, TCards, TPhase } from '../interfaces'
import { TAppState } from '../store/rootReducer'

export const getTableMaxBet = (state: TAppState) => state.table.maxBet
export const getTableMinBet = (state: TAppState) => state.table.minBet
export const getCommunityCards = (state: TAppState) => state.table.communityCards
export const getTimeLeft = (state: TAppState) => state.table.timeleft
export const getTableState = (state: TAppState) => state.table
export const getTableID = (state: TAppState) => Number(state.table.tableID)
export const getTotalPot = (state: TAppState) => state.table.totalPot
export const getCallAmount = (state: TAppState) => state.table.amountCall
export const getPhase = (state: TAppState) => state.table.phase as TPhase
export const isRaised = (state: TAppState) => state.table.raised
export const getUserAction = (state: TAppState) => state.table.userAction
export const getTurn = (state: TAppState) => state.table.turn
export const getPlayers = (state: TAppState) => (state.players || {}) as IPlayers
export const getTables = (state: TAppState) => state.lobby.tables
export const getDealerIndex = (state: TAppState) => state.table.dealer
export const getCurrentRound = (state: TAppState) => state.table.round

export const getIsShowdownTime = createSelector([getPhase, getTimeLeft], (phase, timeleft) => {
  return phase === GAME_ENDED && timeleft <= SECONDS_LEFT_RESOLVE_SHOWDOWN
})

export const getPlayersCount = createSelector(getPlayers, players => Object.keys(players).length)
export const getActivePlayersCount = createSelector(getPlayers, players =>
  Object.keys(players).reduce((acc, key) => acc + Number(!players[key].isSitOut), 0))

export const getTurnPlayer = createSelector([getPlayers, getTurn], (players, turn) =>
  Object.values(players).find(player => turn && player.userID === turn))

export const getTable = createSelector([getTableID, getTables], (tableID, tables) =>
  tables.find(table => table.ID === tableID))

export const getIsTestTable = createSelector(getTableID, tableID => TEST_TABLES_IDS.includes(tableID))

export const getMaxPlayers = createSelector([getTable], table => table?.maxplayers)

export const getPlayerPositions = createSelector(
  [getMaxPlayers],
  maxPlayers => positionsMap[maxPlayers] || positionsMap[9]
)

export const getCombination = createSelector([getPlayers, getPhase], (players, phase) => {
  const winnersIndexes = Object.keys(players).filter(key => players[key].winnings > 0)

  if (!players || phase !== 'ended' || !winnersIndexes.length) {
    return [] as TCards
  }

  return Array.from(flatten(winnersIndexes.map(index => players[index].bestCombination))) as TCards
})

export const getDisplayNewRoundCountDown = createSelector(
  [getPhase, getActivePlayersCount, getTimeLeft],
  (phase, playersCount, timeLeft) =>
    ![GAME_INACTIVE, GAME_STARTED].includes(phase) && playersCount > 1 && inRange(timeLeft, 1, 4)
)

export const getPotValueForCurrentRound = (state: TAppState) => state.table.roundPot

export const getIsPotVisible = createSelector(
  [getPhase, getActivePlayersCount, getIsShowdownTime],
  (phase, playersCount, isShowdownTime) =>
    !isShowdownTime && [GAME_STARTED, GAME_ENDED].includes(phase) && playersCount > 1
)
