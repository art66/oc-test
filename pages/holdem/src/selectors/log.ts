import { createSelector } from 'reselect'
import { TAppState } from '../store/rootReducer'
import getDateTime from '../utils/getDateTime'
import { getIsShowdownTime } from './game'

export const getRawLogs = (state: TAppState) => state.log
export const getLog = createSelector([getRawLogs, getIsShowdownTime], (rawLogs, isShowDownTime) =>
  rawLogs.filter((log, index, { length }) => isShowDownTime || !(index === length - 1 && log.meta === 'won')))

export const getStateForLog = state => {
  const time = getDateTime()
  const {
    betAmount,
    hand,
    identity,
    players,
    queuedAction,
    table,
    prevBetAmount,
    prevCallAmount,
    prevAction,
    sitPosition,
    handIDs
  } = state

  return {
    betAmount,
    hand,
    identity,
    players,
    queuedAction,
    table,
    prevBetAmount,
    prevCallAmount,
    prevAction,
    sitPosition,
    handIDs,
    time
  }
}
