import { get } from 'lodash/fp'
import { createSelector } from 'reselect'
import { GAME_ENDED, GAME_INACTIVE, GAME_READY, GAME_STARTED } from '../constants'
import { IPlayer } from '../interfaces'
import { TAppState } from '../store/rootReducer'
import { positionWithOffset } from '../utils/positionWithOffset'
import { getDealerIndex, getPhase, getPlayers, getPlayersCount, getTurn } from './game'

export const getBetAmount = (state: TAppState) => state.self.betAmount
export const getHand = (state: TAppState) => state.self.hand
export const getPrevAction = (state: TAppState) => state.self.prevAction
export const getPrevBetAmount = (state: TAppState) => state.self.prevBetAmount
export const getPrevCallAmount = (state: TAppState) => state.self.prevCallAmount
export const getUserID = (state: TAppState) => state.self.identity.userID
export const getSitPosition = (state: TAppState) => state.self.sitPosition
export const getQueuedAction = (state: TAppState) => state.queuedAction.action
export const getTestPlayersQueuedActions = (state: TAppState) => state.queuedAction.testPlayers

const defaultSelfPlayer = {
  minBet: 0,
  acceptedActions: [],
  preselectButtons: [],
  sitOutButtons: {},
  waitButtons: {}
}

export const getSelfPlayer = createSelector(
  [getPlayers, getUserID],
  (players, userID) => (Object.values(players).find(player => player.userID === userID) || defaultSelfPlayer) as IPlayer
)

export const getMyIndex = createSelector([getUserID, getPlayers], (userID, players) => {
  const myIndex = Object.keys(players).find(key => players[key].userID === userID)

  return myIndex ? Number(myIndex) : -1
})

export const getAmISat = (state: TAppState) => state.self.satAtTheTable
export const isItMyTurn = createSelector(getUserID, getTurn, (userID, turn) => turn && turn === userID)
export const getMaxBet = createSelector(getSelfPlayer, get('maxBet'))
export const getMinBet = createSelector(getSelfPlayer, get('minBet'))
export const getMyMoney = createSelector(getSelfPlayer, get('money'))
export const getMyPot = createSelector(getSelfPlayer, get('pot'))
export const getAcceptedActions = createSelector(getSelfPlayer, get('acceptedActions'))
export const getPreselectButtons = createSelector(getSelfPlayer, get('preselectButtons'))
export const getSitOutButtons = createSelector(getSelfPlayer, get('sitOutButtons'))
export const getWaitButtons = createSelector(getSelfPlayer, get('waitButtons'))
export const canLeaveNow = createSelector(
  [getPhase, getPlayersCount, getSelfPlayer],
  (phase, playerCount, selfPlayer) =>
    [GAME_ENDED, GAME_READY, GAME_INACTIVE].includes(phase) || playerCount === 1 || selfPlayer.isGameFinished
)

export const getPlayerCallAmount = createSelector(getSelfPlayer, player => {
  const amountCall = player ? player.amountCall : 0

  return !amountCall && amountCall !== 0 ? 0 : amountCall
})

export const getDealerPosition = createSelector(
  [getDealerIndex, getMyIndex, getAmISat, getPhase],
  (dealerIndex, myIndex, amISat, phase) => {
    if (isNaN(dealerIndex) || phase !== GAME_STARTED || dealerIndex == null) {
      return null
    }

    if (dealerIndex === myIndex) {
      return 'self'
    }

    return positionWithOffset(dealerIndex, myIndex, amISat)
  }
)
