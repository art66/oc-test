import { createSelector } from 'reselect'
import get from 'lodash/fp/get'
import identity from 'lodash/fp/identity'

/**
 * Used for server response and not for redux state
 */
const getMessage = get('data.message')

export const getServerState = createSelector(identity, getMessage)
