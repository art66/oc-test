import camelCase from 'lodash/camelCase'
import { flowRight, get, isEqual, nthArg, over, spread } from 'lodash/fp'
import { createSelector } from 'reselect'
import { getPhase } from '.'
import { STATUSES_HIDDEN_CARDS, STATUS_ACTIVE } from '../constants'
import { TPhase } from '../interfaces'
import { IPlayer, IPlayers, TAppearance } from '../interfaces/players'
import { TAppState } from '../store/rootReducer'
import { positionWithOffset } from '../utils/positionWithOffset'
import { getIsShowdownTime, getPlayers, getTimeLeft, getTurn } from './game'
import { getAmISat, getHand, getMyIndex } from './me'

export const isSelf = (player: IPlayer, myIndex: number, amISat: boolean) =>
  positionWithOffset(player.place, myIndex, amISat) === 0 && amISat

const getPlayerAppearance = (player: IPlayer, turn: number, phase: TPhase, isShowDownTime: boolean): TAppearance => {
  if (phase === 'inactive') {
    return 'default'
  }
  if (player.userID === turn) {
    return 'active'
  } else if (player.winnings && isShowDownTime) {
    return 'winner'
  } else if (player.isGameFinished) {
    return 'folded'
  }
  return 'default'
}

const getOwnProps = nthArg(1)

const getUserIdFromProps = flowRight(get('userID'), getOwnProps)

const getStatusFromProps = flowRight(get('status'), getOwnProps)

const getIsHisTurn = flowRight(
  spread((turn, userID) => userID === turn),
  over([getTurn, getUserIdFromProps])
)

const getCountDown = flowRight(
  spread((isHisTurn, timeLeft) => isHisTurn && `${timeLeft} SEC`),
  over([getIsHisTurn, getTimeLeft as any])
)

const getIsActiveStatus = createSelector(getStatusFromProps, isEqual(STATUS_ACTIVE))

export type TPlayerStatusProps = {
  status: string
  userID: number
}

export const makeGetPlayerStatus = () =>
  createSelector<TAppState, TPlayerStatusProps, string, boolean, string, string>(
    [getCountDown, getIsActiveStatus, getStatusFromProps],
    (countDown, isActiveStatus, status) => countDown || (!isActiveStatus && status)
  )

const getPlayerStatusIcon = (player: IPlayer, turn) => {
  if (player.status === STATUS_ACTIVE) {
    return null
  }
  return player.userID === turn ? 'thinking' : camelCase(player.status)
}

export const getPlayersProps = createSelector(
  [getPlayers, getTurn, getMyIndex, getAmISat, getHand, getPhase, getIsShowdownTime],
  (players, turn, myIndex, amISat, handOfSelf, phase, isShowDownTime) =>
    Object.entries(players).reduce((acc, [position, player]: [string, IPlayer]) => {
      const self = isSelf(player, myIndex, amISat)

      acc[position] = {
        ...player,
        self,
        index: positionWithOffset(player.place, myIndex, amISat),
        appearance: getPlayerAppearance(player, turn, phase, isShowDownTime),
        statusIcon: getPlayerStatusIcon(player, turn),
        showCards: !STATUSES_HIDDEN_CARDS.includes(player.status),
        hand: self ? handOfSelf : (!player.isGameFinished && player.hand) || [undefined, undefined]
      }
      return acc
    }, {} as IPlayers)
)

export const getPlayersPot = createSelector([getPlayersProps], players =>
  Object.values(players)
    .filter(({ pot }) => Boolean(pot))
    .map(({ index, pot }) => ({ position: index, value: pot })))

export const getLastRoundPots = createSelector([getPlayersProps], players =>
  Object.values(players)
    .filter(({ lastRoundPot }) => Boolean(lastRoundPot))
    .map(({ index, lastRoundPot }) => ({ position: index, value: lastRoundPot })))

export const getPlayersWining = createSelector([getPlayersProps, getIsShowdownTime], (players, isShowdownTime) =>
  Object.values(players)
    .map(({ index, winnings }) => ({ position: index, value: isShowdownTime ? winnings : 0 }))
    .filter(({ value }) => Boolean(value)))
