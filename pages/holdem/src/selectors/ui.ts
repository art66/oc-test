import { createSelector } from 'reselect'
import { TMediaType } from '../interfaces'
import { TAppType } from '../reducers/appType'
import { TAppState } from '../store/rootReducer'
import { getTableMaxBet, getTableMinBet } from './game'
import { getAcceptedActions, isItMyTurn } from './me'

export const getActiveMenuItem = (state: TAppState) => state.ui.activeMenuItem
export const getSoundStatus = (state: TAppState) => state.ui.sound

export const isBetPanelActive = createSelector(
  [getAcceptedActions, isItMyTurn, getTableMaxBet, getTableMinBet],
  (acceptedActions, myTurn, maxBet, minBet) =>
    (acceptedActions.includes('raise') || acceptedActions.includes('bet')) && myTurn && minBet !== maxBet
)

const getMediaType = (state: TAppState) => state.browser.mediaType as TMediaType
const getAppType = (state: TAppState) => state.appType

export const getSuitsColors = (state: TAppState) => state.ui.suitsColors

export type TLayoutType = 'desktop_full' | 'desktop_sidebar' | 'tablet_full' | 'tablet_sidebar' | 'mobile'

const mediaTypeMapping = (mediaType: TMediaType, appType: TAppType): TLayoutType => {
  switch (mediaType) {
    case 'large':
      return appType === 'sidebar' ? 'desktop_sidebar' : 'desktop_full'
    case 'medium':
      return appType === 'sidebar' ? 'tablet_sidebar' : 'tablet_full'
    case 'small':
      return 'mobile'
    default:
      throw new Error('media type type not correct')
  }
}

export const getLayoutType = createSelector([getMediaType, getAppType], mediaTypeMapping)
