import React from 'react'

export const Preflop = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='9' height='3' viewBox='0 0 9 3'>
    <path d='M855,429v3h3v-3Zm-6,0v3h3v-3Z' transform='translate(-849 -429)' fill='#ddd' />
  </svg>
)
