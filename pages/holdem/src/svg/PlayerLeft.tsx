import React from 'react'

export const PlayerLeft = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='10' height='8' viewBox='0 0 10 8'>
    <path d='M852,411v-2l-3,3,3,3v-2h4v-2Zm7-3v8h-6v-1h5v-6h-5v-1Z' transform='translate(-849 -408)' fill='#ddd' />
  </svg>
)
