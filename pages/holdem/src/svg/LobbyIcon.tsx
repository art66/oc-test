/* eslint-disable max-len */
import React from 'react'

export const LobbyIcon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    width='16'
    height='16'
    viewBox='0 0 16 16'
  >
    <g transform='matrix(1, 0, 0, 1, 0, 0)' filter='url(#gaussianBlur)'>
      <path
        d='M-2530,3259v-3h16v3Zm0-6v-3h16v3Zm0-6v-3h16v3Z'
        transform='translate(2530 -3244)'
        fill='url(#iconsGradientDefault)'
      />
    </g>
  </svg>
)
