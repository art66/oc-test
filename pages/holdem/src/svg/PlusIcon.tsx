/* eslint-disable max-len */
import React from 'react'

export const PlusIcon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    width='10'
    height='11'
    viewBox='0 0 10 11'
  >
    <g transform='matrix(1, 0, 0, 1, 0, 0)'>
      <path d='M947-1860v-4h-4v-2h4v-4h2v4h4v2h-4v4Z' transform='translate(-943 1870)' />
    </g>
  </svg>
)
