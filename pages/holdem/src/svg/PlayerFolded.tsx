/* eslint-disable max-len */
import React from 'react'

export const PlayerFolded = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='9' height='9' viewBox='0 0 9 9'>
    <path
      d='M858,541.57l-3.118-3.078,3.075-3.106L856.57,534l-3.079,3.119-3.116-3.076L849,535.418l3.12,3.09-3.077,3.117L850.418,543l3.089-3.119,3.107,3.076Z'
      transform='translate(-849 -534)'
      fill='#ddd'
    />
  </svg>
)
