/* eslint-disable max-len */
import React from 'react'

export const MoneyGrayIcon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    width='18'
    height='17.998'
    viewBox='0 0 18 17.998'
  >
    <defs>
      <linearGradient id='MoneyGrayIcon_Gradient' x1='0.5' x2='0.5' y2='1' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#e5e5d9' />
        <stop offset='1' stopColor='#b0b0a1' />
      </linearGradient>
      <filter id='MoneyGrayIcon_Blur' x='0' y='0' width='18' height='17.998' filterUnits='userSpaceOnUse'>
        <feOffset in='SourceAlpha' />
        <feGaussianBlur stdDeviation='1' result='c' />
        <feFlood floodOpacity='0.651' />
        <feComposite operator='in' in2='c' />
        <feComposite in='SourceGraphic' />
      </filter>
    </defs>
    <g transform='matrix(1, 0, 0, 1, 0, 0)' filter='url(#MoneyGrayIcon_Blur)'>
      <path
        d='M-8926,3256a6.006,6.006,0,0,1-6-6,6.006,6.006,0,0,1,6-6,6.006,6.006,0,0,1,6,6A6.006,6.006,0,0,1-8926,3256Zm-2.236-3.944-.334,1.208a5.078,5.078,0,0,0,2.2.534v.734h.735v-.774c1.382-.195,2.205-.98,2.2-2.1,0-1.344-1.183-1.856-2.326-2.351l-.016-.006c-.185-.08-.376-.163-.556-.247a.783.783,0,0,1-.485-.825c.072-.336.414-.554.912-.583.054,0,.107,0,.16,0a5.1,5.1,0,0,1,1.635.347l.265-1.21a6.9,6.9,0,0,0-1.792-.31v-.747h-.735v.784a2.23,2.23,0,0,0-2.193,2.094c0,1.456,1.364,1.9,2.267,2.2a4.9,4.9,0,0,1,.51.185c.424.19.657.5.625.818a.82.82,0,0,1-.71.659,2.127,2.127,0,0,1-.466.048A5.436,5.436,0,0,1-8928.237,3252.053Z'
        transform='translate(8935 -3241)'
        fill='url(#MoneyGrayIcon_Gradient)'
      />
    </g>
  </svg>
)
