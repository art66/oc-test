/* eslint-disable max-len */
import React from 'react'

export const PlayerIcon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    width='18'
    height='18'
    viewBox='0 0 18 18'
  >
    <defs>
      <linearGradient id='PlayerIcon_Gradient' x1='0.5' x2='0.5' y2='1' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#e5e5d9' />
        <stop offset='1' stopColor='#b0b0a1' />
      </linearGradient>
      <filter id='PlayerIcon_Blur' x='0' y='0' width='18' height='18' filterUnits='userSpaceOnUse'>
        <feOffset in='SourceAlpha' />
        <feGaussianBlur stdDeviation='1' result='c' />
        <feFlood floodOpacity='0.651' />
        <feComposite operator='in' in2='c' />
        <feComposite in='SourceGraphic' />
      </filter>
    </defs>
    <g transform='matrix(1, 0, 0, 1, 0, 0)' filter='url(#PlayerIcon_Blur)'>
      <path
        d='M10.411,9.048c-1.72-.4-3.32-.745-2.545-2.209C10.226,2.383,8.492,0,6,0,3.459,0,1.768,2.475,4.134,6.839c.8,1.473-.863,1.821-2.545,2.209C.052,9.4-.005,10.166,0,11.5L0,12H12l0-.485c.006-1.344-.046-2.111-1.588-2.468Z'
        transform='translate(3 3)'
        fill='url(#PlayerIcon_Gradient)'
      />
    </g>
  </svg>
)
