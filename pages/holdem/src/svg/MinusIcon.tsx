/* eslint-disable max-len */
import React from 'react'

export const MinusIcon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    width='10'
    height='3'
    viewBox='0 0 10 3'
  >
    <g transform='matrix(1, 0, 0, 1, 0, 0)'>
      <rect width='10' height='2' />
    </g>
  </svg>
)
