/* eslint-disable max-len */
import React from 'react'

export const TableSkinOffIcon = () => (
  <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15 15.94'>
    <g>
      <g className='cls-1'>
        <path
          className='cls-2'
          d='M18,3h-12L5,1H3l1.1,2H0v14h11.4l1.6,3h2l-1.6-3H18V3z M6.4,7.5c0,0.9,0.3,1.9,1.2,2.4L8.3,11v2.9h1.1 c0.1,0,0.3,0,0.4,0l0.8,1.6l-9.1,0V4.6l3.4,0L6.4,7.5z M9.6,9.8c1.4-0.7,1.5-2.6,0.9-3.7l-1,1.2L8.6,6.1L8.1,6.9L6.9,4.6l9.6,0 v10.9l-3.9,0L11.3,13c0.4-0.4,0.7-1,0.7-1.7v-2L10.3,11L9.6,9.8z M7.5,13.9v-2.4L5.3,9.3v2C5.3,12.6,6.3,13.6,7.5,13.9z'
        />
      </g>
      <path
        className='cls-3'
        d='M18,2h-12L5,0H3l1.1,2H0v14h11.4l1.6,3h2l-1.6-3H18V2z M6.4,6.5c0,0.9,0.3,1.9,1.2,2.4L8.3,10v2.9h1.1 c0.1,0,0.3,0,0.4,0l0.8,1.6l-9.1,0V3.6l3.4,0L6.4,6.5z M9.6,8.8c1.4-0.7,1.5-2.6,0.9-3.7l-1,1.2L8.6,5.1L8.1,5.9L6.9,3.6l9.6,0 v10.9l-3.9,0L11.3,12c0.4-0.4,0.7-1,0.7-1.7v-2L10.3,10L9.6,8.8z M7.5,12.9v-2.4L5.3,8.3v2C5.3,11.6,6.3,12.6,7.5,12.9z'
      />
    </g>
  </svg>
)
