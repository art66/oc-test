import React from 'react'

export const Flop = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='9' height='9' viewBox='0 0 9 9'>
    <path d='M852,450h3v-3h-3Zm6,0h-3v3h3Zm-9-3h3v-3h-3Z' transform='translate(-849 -444)' fill='#ddd' />
  </svg>
)
