import React from 'react'

export const CheckCall = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='10' height='8' viewBox='0 0 10 8'>
    <path
      d='M857.452,551l-4.818,4.87-2.087-2.11L849,555.325,852.635,559,859,552.564Z'
      transform='translate(-849 -551)'
      fill='#ddd'
    />
  </svg>
)
