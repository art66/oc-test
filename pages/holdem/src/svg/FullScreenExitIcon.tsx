/* eslint-disable max-len */
import React from 'react'

export const FullScreenExitIcon = () => (
  <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15 15.94'>
    <g>
      <g className='cls-1'>
        <path
          className='cls-2'
          d='M17.08,3.83,14.26,1l-3,3L9,1.74V9.07h7.33L14.08,6.82ZM8,10.07H.67l2.25,2.25L0,15.24l2.83,2.83,2.92-2.92L8,17.4Z'
        />
      </g>
      <path
        className='cls-3'
        d='M17.08,2.83,14.26,0l-3,3L9,.74V8.07h7.33L14.08,5.82ZM8,9.07H.67l2.25,2.25L0,14.24l2.83,2.83,2.92-2.92L8,16.4Z'
      />
    </g>
  </svg>
)
