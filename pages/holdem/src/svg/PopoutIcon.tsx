import React from 'react'

export const PopoutIcon = () => (
  <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15 15.94'>
    <g>
      <g className='cls-1'>
        <path className='cls-2' d='M0,5V16H13V14H2V5ZM5,5h9v6H5ZM3,13H16V1H3Z' />
      </g>
      <path className='cls-3' d='M0,4V15H13V13H2V4ZM5,4h9v6H5ZM3,12H16V0H3Z' />
    </g>
  </svg>
)
