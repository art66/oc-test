/* eslint-disable max-len */
import React from 'react'

export const TableSkinOnIcon = () => (
  <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15 15.94'>
    <g>
      <g className='cls-1'>
        <path
          className='cls-2'
          d='M16.5,4.6v10.9h-15V4.6H16.5z M18,3H0v14h18V3z M7.5,13.9c-1.2-0.2-2.3-1.3-2.3-2.5v-2l2.3,2.1V13.9z M9,12.2 V10c2.1-0.4,2.2-2.7,1.5-3.9l-1,1.2L8.6,6.1L7.8,7.3L6.7,6.1C6.1,7.3,6.2,9.6,8.3,10v3.9h1.1c1.4,0,2.6-1.1,2.6-2.6v-2L9,12.2z'
        />
      </g>
      <path
        className='cls-3'
        d='M16.5,3.6v10.9h-15V3.6H16.5z M18,2H0v14h18V2z M7.5,12.9c-1.2-0.2-2.3-1.3-2.3-2.5v-2l2.3,2.1V12.9z M9,11.2 V9c2.1-0.4,2.2-2.7,1.5-3.9l-1,1.2L8.6,5.1L7.8,6.3L6.7,5.1C6.1,6.3,6.2,8.6,8.3,9v3.9h1.1c1.4,0,2.6-1.1,2.6-2.6v-2L9,11.2z'
      />
    </g>
  </svg>
)
