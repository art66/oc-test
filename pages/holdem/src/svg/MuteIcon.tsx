/* eslint-disable max-len */
import React from 'react'

export const MuteIcon = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='22' height='23' viewBox='0 0 22 23'>
    <g transform='matrix(1, 0, 0, 1, 0, 0)'>
      <path
        d='M387.719,540.694c-1.473-3.479-3.843-6.13-5.043-5.634-2.036.84,1.214,4.876-8.787,9-.864.356-1.083,1.783-.722,2.636s1.543,1.7,2.407,1.345c.149-.061.7-.241.7-.241.617.841,1.263.342,1.492.877.275.642.874,2.037,1.077,2.512s.665.913,1,.784l1.907-.737a.642.642,0,0,0,.406-.872c-.143-.335-.731-.433-.9-.824s-.716-1.642-.873-2.037c-.215-.537.241-.973.9-1.043,4.552-.482,5.4,2.372,6.953,1.733C389.436,547.7,389.192,544.174,387.719,540.694Zm-.513,5.645c-.266.11-2.057-1.325-3.2-4.03s-1-5.177-.735-5.287,2.013,1.621,3.158,4.326S387.473,546.229,387.206,546.339Z'
        transform='translate(-370 -532)'
      />
    </g>
  </svg>
)
