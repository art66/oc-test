import React from 'react'

export const Defs = () => (
  <svg xmlns='http://www.w3.org/2000/svg' style={{ height: 0 }}>
    <defs>
      <filter id='gaussianBlur' x='0' y='0' width='24' height='24' filterUnits='userSpaceOnUse'>
        <feOffset in='SourceAlpha' />
        <feGaussianBlur stdDeviation='1' result='c' />
        <feFlood floodOpacity='0.651' />
        <feComposite operator='in' in2='c' />
        <feComposite in='SourceGraphic' />
      </filter>
      <linearGradient id='iconsGradientDefault' x1='0.5' y1='1' x2='0.5' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#686859' />
        <stop offset='1' stopColor='#9B9B8C' />
      </linearGradient>
      <linearGradient id='iconsGradientHover' x1='0.5' y1='1' x2='0.5' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#353526' />
        <stop offset='1' stopColor='#686859' />
      </linearGradient>
      <linearGradient id='iconsGradientDMDefault' x1='0.5' y1='1' x2='0.5' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#666666' />
        <stop offset='1' stopColor='#999999' />
      </linearGradient>
      <linearGradient id='iconsGradientDMHover' x1='0.5' y1='1' x2='0.5' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#888888' />
        <stop offset='1' stopColor='#DDDDDD' />
      </linearGradient>
      <linearGradient id='lobbyIconsGradientLight' x1='0.5' y1='1' x2='0.5' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#8a8a7b' />
        <stop offset='1' stopColor='#acac9d' />
      </linearGradient>
      <linearGradient id='statusIconsGradient' x1='0.5' y1='1' x2='0.5' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#f0f0e1' />
        <stop offset='1' stopColor='#dfdfd0' />
      </linearGradient>
      <linearGradient id='inputIconsFill' x1='0.5' y1='1' x2='0.5' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#666666' />
        <stop offset='1' stopColor='#888888' />
      </linearGradient>
      <filter id='lightShadow' x='0' y='0' width='100%' height='100' filterUnits='userSpaceOnUse'>
        <feOffset dy='1' in='SourceAlpha' />
        <feGaussianBlur result='b' />
        <feFlood floodColor='#fff' />
        <feComposite operator='in' in2='b' />
        <feComposite in='SourceGraphic' />
      </filter>
      <filter id='darkShadow' x='0' y='0' width='100%' height='100' filterUnits='userSpaceOnUse'>
        <feOffset dy='1' in='SourceAlpha' />
        <feGaussianBlur result='b' />
        <feFlood floodColor='#000' />
        <feComposite operator='in' in2='b' />
        <feComposite in='SourceGraphic' />
      </filter>
    </defs>
  </svg>
)

export default Defs
