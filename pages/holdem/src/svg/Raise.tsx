import React from 'react'

export const Raise = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='6' height='10' viewBox='0 0 6 10'>
    <path d='M852,573v5h2v-5h2l-3-5-3,5Z' transform='translate(-850 -568)' fill='#ddd' />
  </svg>
)
