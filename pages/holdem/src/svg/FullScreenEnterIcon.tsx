/* eslint-disable max-len */
import React from 'react'

export const FullScreenEnterIcon = () => (
  <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15 15.94'>
    <g>
      <g className='cls-1'>
        <path
          className='cls-2'
          d='M15,.94V7.81L12.89,5.7,10.15,8.44,7.5,5.79l2.74-2.74L8.13.94ZM7.58,11,4.93,8.37l-2.82,2.8L0,9.06v6.88H6.87L4.76,13.83Z'
        />
      </g>
      <path
        className='cls-3'
        d='M15,0V6.87L12.89,4.76,10.15,7.5,7.5,4.85l2.74-2.74L8.13,0ZM7.58,10.09,4.93,7.43,2.11,10.24,0,8.13V15H6.87L4.76,12.89Z'
      />
    </g>
  </svg>
)
