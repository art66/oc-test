/* eslint-disable max-len */
import React from 'react'

export const MoneyGreenIcon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    width='20'
    height='20'
    viewBox='0 0 20 20'
  >
    <defs>
      <linearGradient id='a' x1='0.5' x2='0.5' y2='1' gradientUnits='objectBoundingBox'>
        <stop offset='0' stopColor='#5c940d' />
        <stop offset='1' stopColor='#a9e34b' />
      </linearGradient>
      <filter id='b' x='0' y='0' width='20' height='20' filterUnits='userSpaceOnUse'>
        <feOffset in='SourceAlpha' />
        <feGaussianBlur stdDeviation='1' result='c' />
        <feFlood floodOpacity='0.651' />
        <feComposite operator='in' in2='c' />
        <feComposite in='SourceGraphic' />
      </filter>
      <linearGradient id='d' y1='1' y2='0' xlinkHref='#a' />
      <filter id='e' x='4' y='2' width='12' height='16.286' filterUnits='userSpaceOnUse'>
        <feOffset in='SourceAlpha' />
        <feGaussianBlur stdDeviation='1' result='f' />
        <feFlood floodOpacity='0.451' />
        <feComposite operator='in' in2='f' />
        <feComposite in='SourceGraphic' />
      </filter>
    </defs>
    <g transform='translate(-294 -132.964)'>
      <g transform='matrix(1, 0, 0, 1, 294, 132.96)' filter='url(#b)'>
        <circle cx='7' cy='7' r='7' transform='translate(3 3)' fill='url(#a)' />
      </g>
      <circle cx='6' cy='6' r='6' transform='translate(298 136.964)' fill='url(#d)' />
      <g transform='matrix(1, 0, 0, 1, 294, 132.96)' filter='url(#e)'>
        <path
          d='M12.429,15.383v.9h-.857V15.43A5.933,5.933,0,0,1,9,14.807L9.39,13.4a5.234,5.234,0,0,0,2.764.463c.985-.223,1.186-1.236.1-1.724-.8-.372-3.238-.69-3.238-2.78a2.6,2.6,0,0,1,2.558-2.443V6h.857v.873a7.922,7.922,0,0,1,2.093.36l-.31,1.412a5.486,5.486,0,0,0-2.094-.4c-1.276.075-1.389,1.179-.5,1.642,1.467.69,3.381,1.2,3.381,3.04,0,1.473-1.151,2.256-2.571,2.455Z'
          transform='translate(-2 -1)'
          fill='#fff'
        />
      </g>
    </g>
  </svg>
)
