import React from 'react'

export const PlayerJoined = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='10' height='8' viewBox='0 0 10 8'>
    <path d='M852,393v-2l4,3-4,3v-2h-3v-2Zm1-3v1h5v6h-5v1h6v-8Z' transform='translate(-849 -390)' fill='#ddd' />
  </svg>
)
