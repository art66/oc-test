import React from 'react'

export const River = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='9' height='9' viewBox='0 0 9 9'>
    <path
      d='M852,486h3v-3h-3Zm6,0h-3v3h3Zm-3-6v3h3v-3Zm-3,9v-3h-3v3Zm-3-6h3v-3h-3Z'
      transform='translate(-849 -480)'
      fill='#ddd'
    />
  </svg>
)
