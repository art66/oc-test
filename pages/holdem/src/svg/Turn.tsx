import React from 'react'

export const Turn = () => (
  <svg xmlns='http://www.w3.org/2000/svg' width='9' height='9' viewBox='0 0 9 9'>
    <path d='M855,468v3h3v-3Zm0-6v3h3v-3Zm-6,6v3h3v-3Zm0-6v3h3v-3Z' transform='translate(-849 -462)' fill='#ddd' />
  </svg>
)
