/* eslint-disable max-statements */
import { show } from '@torn/shared/middleware/infoBox'
import { all, call, cancel, delay, fork, put, select, take } from 'redux-saga/effects'
import * as actions from '../actions'
import * as actionTypes from '../actions/actionTypes'
import { GAME_ENDED, GAME_READY /* , PLAYER_MOVE_DELAY */, GAME_STARTED } from '../constants'
import { getActivePlayersCount, getPhase, getPlayerCallAmount, getTimeLeft } from '../selectors'
import { api } from '../services'

function* countDown(timeleft, cb) {
  let count = 0
  let lastTime = new Date()

  while (timeleft > Math.floor(count / 1000)) {
    count += Date.now() - lastTime.getTime()
    lastTime = new Date()
    yield put(actions.timeleftTick(timeleft - Math.floor(count / 1000)))
    yield delay(1000)
  }

  yield call(cb)
}

function* timeout() {
  yield put(actions.timeout())

  try {
    yield call(api.timeout)
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
  }
}

function* startNextGame() {
  yield put(actions.startNextGame())

  try {
    yield call(api.startNextGame)
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
  }
}

export default function* timeOutSaga() {
  let previousPhase
  let timeoutTask

  while (true) {
    yield take([
      actionTypes.PLAYER_MADE_MOVE,
      actionTypes.PLAYER_JOIN,
      actionTypes.UPDATE_PLAYER,
      actionTypes.REMOVE_PLAYER,
      actionTypes.ASK_STATE_SUCCESS,
      actionTypes.GET_STATE
    ])

    const [pc, phase, timeleft] = yield all([select(getActivePlayersCount), select(getPhase), select(getTimeLeft)])
    const timerIsRunning = pc > 1 && !isNaN(timeleft)

    if (previousPhase !== GAME_STARTED && phase === GAME_STARTED) {
      const callAmount = yield select(getPlayerCallAmount)

      yield put(actions.setPrevCallAmount(callAmount))
    }

    if (timeoutTask) {
      yield cancel(timeoutTask)

      timeoutTask = null
    }

    if (timerIsRunning && phase === GAME_STARTED) {
      timeoutTask = yield fork(countDown, timeleft, timeout)
    } else if (timerIsRunning && (phase === GAME_ENDED || phase === GAME_READY)) {
      yield put(actions.setBetAmount(0))
      timeoutTask = yield fork(countDown, timeleft, startNextGame)
    }

    previousPhase = phase
  }
}
