import { put, select, takeLatest } from 'redux-saga/effects'
import { setSelfPlayerAway, setSelfPlayerSat } from '../actions'
import * as actionTypes from '../actions/actionTypes'
import { getPlayers } from '../selectors/game'
import { getUserID } from '../selectors/me'

function* watchSelfSatState() {
  const userID = yield select(getUserID)
  const players: ReturnType<typeof getPlayers> = yield select(getPlayers)
  const selfPlayerSat = Object.values(players).some(player => player.userID === userID)

  yield selfPlayerSat ? put(setSelfPlayerSat()) : put(setSelfPlayerAway())
}

// TODO: check which of these actions cana actually
// lead to changing self player sat state
export function* watchSelfSatStateSaga() {
  yield takeLatest(
    [
      actionTypes.PLAYER_MADE_MOVE,
      actionTypes.GET_STATE,
      actionTypes.ASK_STATE_SUCCESS,
      actionTypes.PLAYER_JOIN,
      actionTypes.UPDATE_PLAYER,
      actionTypes.REMOVE_PLAYER,
      actionTypes.UPDATE_WAIT_BUTTONS
    ],
    watchSelfSatState
  )
}
