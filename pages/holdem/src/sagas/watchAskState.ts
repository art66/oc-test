import { call, put, select, takeLatest } from 'redux-saga/effects'
import { ASK_STATE } from '../actions/actionTypes'
import { getTableID, getStateForLog } from '../selectors'
import { api } from '../services'
import { askStateSuccess } from '../actions'

function* watchAskStateSaga() {
  const tableID = yield select(getTableID)
  const stateLog = yield select(getStateForLog)
  const gameState = yield call(api.getGameState, { tableID, stateLog })

  yield put(askStateSuccess(gameState.payload))
}

export default function* watchAskState() {
  yield takeLatest(ASK_STATE, watchAskStateSaga)
}
