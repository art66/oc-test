import { select, takeLatest } from 'redux-saga/effects'
import { PLAYER_MADE_MOVE, GET_STATE } from '../actions/actionTypes'
import { PAGE_TITLE_BLINK_DELAY, PAGE_TITLE_DEFAULT, PAGE_TITLE_YOUR_TURN } from '../constants'
import { getUserID, getTimeLeft } from '../selectors'

function* pageTitleSaga(action) {
  const userID = yield select(getUserID)
  const timeLeft = yield select(getTimeLeft)
  const { turn } = action.serverState

  let intervalID
  let ticks = 0

  const resetToDefault = () => {
    clearInterval(intervalID)
    intervalID = null
    window.removeEventListener('focus', resetToDefault)
    document.title = PAGE_TITLE_DEFAULT
  }

  if (turn === userID && !intervalID && document.hidden) {
    window.addEventListener('focus', resetToDefault)

    intervalID = setInterval(() => {
      if (timeLeft === ticks) {
        resetToDefault()

        return false
      }

      document.title = ticks % 2 === 0 ? PAGE_TITLE_YOUR_TURN : PAGE_TITLE_DEFAULT
      ticks += 1
    }, PAGE_TITLE_BLINK_DELAY)
  }
}

export default function* pageTitle() {
  yield takeLatest([PLAYER_MADE_MOVE, GET_STATE], pageTitleSaga)
}
