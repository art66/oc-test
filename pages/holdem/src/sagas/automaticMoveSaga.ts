import { takeLatest, select, put, all } from 'redux-saga/effects'
import { getQueuedAction, getAcceptedActions, getUserAction, isItMyTurn } from '../selectors'
import * as actions from '../actions'
import * as actionTypes from '../actions/actionTypes'
import { pickMove } from './helpers/pickMove'

function* automaticMoveSaga() {
  const [myTurn, acceptedActions, lastAction] = yield all([
    select(isItMyTurn),
    select(getAcceptedActions),
    select(getUserAction)
  ])
  const prevUserRaised = ['raise', 'bet', 'allin'].includes(lastAction)

  let queuedAction = yield select(getQueuedAction)

  if (!queuedAction.length) return

  if (prevUserRaised && queuedAction.find(act => ['call', 'check'].includes(act))) {
    queuedAction = queuedAction.filter(action => !['call', 'check'].includes(action))

    yield put(actions.toggleQueue(queuedAction))
  }

  if (myTurn) {
    if (queuedAction.length) {
      yield put(actions.makeMove([pickMove(queuedAction, acceptedActions)]))
    } else {
      yield put(actions.toggleQueue())
    }
  }
}

export default function* automaticMove() {
  yield takeLatest(actionTypes.PLAYER_MADE_MOVE, automaticMoveSaga)
}
