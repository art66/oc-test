import { take, put, call, fork, takeLatest } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import { show, hideInfoBox } from '@torn/shared/middleware/infoBox'
import { INIT_WS } from '../actions/actionTypes'
import * as actions from '../actions'
import { api, centrifugeConnection } from '../services'

function* wsLoop(socketChannel) {
  while (true) {
    const action = yield take(socketChannel)

    yield put(action)
  }
}

function createSocketChannel() {
  return eventChannel(emit => {
    centrifugeConnection.setEmitter(emit)
    return () => {
      centrifugeConnection.unsubscribe()
    }
  })
}

function* initWS() {
  try {
    const credentials = yield call(api.getCredentials)

    yield put(actions.saveCredentials(credentials))
    yield call([centrifugeConnection, centrifugeConnection.connect], credentials)

    const socketChannel = yield call(createSocketChannel)

    yield fork(wsLoop, socketChannel)
    yield put(actions.joinTable({ tableID: credentials.table, channel: credentials.channel }))
    yield put(hideInfoBox())
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
  }
}

export default function* initWSLoop() {
  yield takeLatest(INIT_WS, initWS)
}
