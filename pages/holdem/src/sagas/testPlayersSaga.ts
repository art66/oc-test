/* eslint-disable max-depth */
/* eslint-disable no-restricted-syntax */
/* eslint-disable max-statements */
import { all, put, select, takeLatest } from 'redux-saga/effects'
import * as actions from '../actions'
import * as actionTypes from '../actions/actionTypes'
import { TEST_DATA_URL } from '../constants'
import {
  getAcceptedActions,
  getIsTestTable,
  getPlayers,
  getTestPlayersQueuedActions,
  getTurn,
  getUserAction,
  isItMyTurn
} from '../selectors'
import { fetchUrl } from '../utils'
import { pickMove } from './helpers/pickMove'

function* testPlayerAutomaticMove() {
  const [acceptedActions, lastAction, isTestTable] = yield all([
    select(isItMyTurn),
    select(getAcceptedActions),
    select(getUserAction),
    select(getIsTestTable)
  ])
  const prevUserRaised = ['raise', 'bet', 'allin'].includes(lastAction)

  if (isTestTable) {
    const testPlayersQueuedAction = yield select(getTestPlayersQueuedActions)
    const players = yield select(getPlayers)
    const currentTurn = yield select(getTurn)

    for (const position in testPlayersQueuedAction) {
      if (!players[position]) continue

      const playerPreselect = testPlayersQueuedAction[position]
      const { tableId, userID, amount } = playerPreselect
      let { actions: preselectedActions } = playerPreselect

      if (prevUserRaised && preselectedActions.find(act => ['call', 'check'].includes(act))) {
        preselectedActions = preselectedActions.filter(action => !['call', 'check'].includes(action))

        yield put(actions.toggleQueueTestPlayer({ position: +position, actions: preselectedActions }))
      }

      if (players[position].userID === currentTurn) {
        if (preselectedActions.length) {
          yield put(
            actions.testPlayerAction({
              tableId,
              userID,
              action: pickMove(preselectedActions, acceptedActions),
              position,
              step: 'makeMove',
              amount: amount || undefined
            })
          )
        } else {
          yield put(actions.toggleQueueTestPlayer({ position, actions: preselectedActions }))
        }
      }
    }
  }
}

function* testPlayerMove(action) {
  try {
    yield fetchUrl(TEST_DATA_URL, action.payload)
  } catch (error) {
    console.error(error)
  }
}

export default function* testPlayersSaga() {
  yield takeLatest(actionTypes.PLAYER_MADE_MOVE, testPlayerAutomaticMove)
  yield takeLatest(actionTypes.TEST_PLAYER_ACTION, testPlayerMove)
}
