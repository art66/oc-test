import { call, put, takeLatest } from 'redux-saga/effects'
import { show, hideInfoBox } from '@torn/shared/middleware/infoBox'
import { SITOUT_IMBACK_CLICK } from '../actions/actionTypes'
import { api } from '../services'

function* sitOutImBackSaga(action) {
  try {
    yield call(action.payload.step === 'sitout' ? api.sitOut : api.imBack)
    yield put(hideInfoBox())
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
  }
}

export default function* sitOutImBack() {
  yield takeLatest(SITOUT_IMBACK_CLICK, sitOutImBackSaga)
}
