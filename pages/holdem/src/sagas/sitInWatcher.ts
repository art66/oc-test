import { hideInfoBox, show } from '@torn/shared/middleware/infoBox'
import { call, put, select, takeLatest } from 'redux-saga/effects'
import { SIT_IN } from '../actions/actionTypes'
import { getSitPosition } from '../selectors'
import { api } from '../services'

function* sitInSaga(action) {
  try {
    const sitPosition = yield select(getSitPosition)

    yield call(api.sitIn, sitPosition, action.payload.moneyAmount)
    yield put(hideInfoBox())
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
  }
}

export default function* sitIn() {
  yield takeLatest(SIT_IN, sitInSaga)
}
