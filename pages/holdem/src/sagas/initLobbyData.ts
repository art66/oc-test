import { put, call, takeLatest } from 'redux-saga/effects'
import { show, hideInfoBox } from '@torn/shared/middleware/infoBox'
import { INIT_LOBBY } from '../actions/actionTypes'
import { setLobbyData } from '../actions'
import { api } from '../services'

function* init() {
  try {
    const lobbyData = yield call(api.getLobbyData)

    yield put(setLobbyData(lobbyData))
    yield put(hideInfoBox())
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
  }
}

export default function* initLobby() {
  yield takeLatest(INIT_LOBBY, init)
}
