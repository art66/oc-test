import { call, put, select, takeLatest } from 'redux-saga/effects'
import { show, hideInfoBox } from '@torn/shared/middleware/infoBox'
import { askState } from '../actions'
import { getServerState } from '../selectors'
import * as actionTypes from '../actions/actionTypes'

const ACTION_TYPES = {
  getState: actionTypes.GET_STATE,
  playerMakeMove: actionTypes.PLAYER_MADE_MOVE,
  chat: actionTypes.LOGS,
  playerJoin: actionTypes.PLAYER_JOIN,
  updatePlayer: actionTypes.UPDATE_PLAYER,
  removePlayer: actionTypes.REMOVE_PLAYER
}

function* publicChannelSaga(action) {
  try {
    const { response } = action
    const serverState = yield call(getServerState, response)
    const { eventType } = serverState
    const state = yield select()
    const type = ACTION_TYPES[eventType]

    if (
      eventType === 'playerMakeMove'
      && state.self.handIDs.currentHandID
      && state.self.handIDs.currentHandID !== response.data.message.token
    ) {
      yield put(askState())
    }

    if (type) yield put({ type, serverState })
    yield put(hideInfoBox())
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
  }
}

export default function* publicChannel() {
  yield takeLatest(actionTypes.PUBLIC_CHANNEL_EVENT, publicChannelSaga)
}
