export const pickMove = (queuedAction, acceptedActions) => {
  if (queuedAction[0] !== 'callAny') {
    return queuedAction[0]
  } else if (acceptedActions.includes('call')) {
    return 'call'
  }
  return 'allin'
}
