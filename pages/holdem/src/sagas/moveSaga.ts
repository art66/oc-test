import { hideInfoBox, show } from '@torn/shared/middleware/infoBox'
import { call, delay, put, select, takeLatest } from 'redux-saga/effects'
import * as actions from '../actions'
import * as actionTypes from '../actions/actionTypes'
import { BUTTON_PRESS_DELAY /* , PLAYER_MOVE_DELAY */ } from '../constants'
import { getAcceptedActions, getBetAmount, getStateForLog } from '../selectors'
import { api } from '../services'

function* moveSaga(action) {
  try {
    const { payload: playerActions } = action
    const acceptableActions = yield select(getAcceptedActions)
    const stateLog = yield select(getStateForLog)
    const playerAction = playerActions.find(act => acceptableActions.includes(act)) || playerActions[0]

    yield put(actions.pressButton(playerAction))

    yield delay(BUTTON_PRESS_DELAY) // TODO: check for possibility to put delay and api call in a race

    if (playerAction === 'leave') {
      yield call(api.leave)
    } else {
      const amount = ['raise', 'bet'].includes(playerAction) ? yield select(getBetAmount) : undefined
      const requestData = { action: playerAction, amount, stateLog }

      yield call(api.makeMove, { ...requestData })
    }

    yield put(hideInfoBox())
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
    yield put(actions.unpressButton())
  }
}

export default function* move() {
  yield takeLatest(actionTypes.MAKE_A_MOVE, moveSaga)
}
