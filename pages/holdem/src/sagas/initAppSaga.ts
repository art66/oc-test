import { call, put, takeLatest } from 'redux-saga/effects'
import { initLobby, initWS, setSuitsColor } from '../actions'
import * as actionTypes from '../actions/actionTypes'
import { STORAGE_KEY_SUITS_COLORS } from '../constants'
import { TSuitsColors } from '../reducers/types'
import { sound } from '../services'

function* initApp() {
  yield call(sound.loadSounds)
  yield put(initLobby())
  yield put(initWS())
  yield put(setSuitsColor(localStorage.getItem(STORAGE_KEY_SUITS_COLORS) as TSuitsColors))
}

export default function* initAppSaga() {
  yield takeLatest(actionTypes.INIT_APP, initApp)
}
