import { takeLatest } from 'redux-saga/effects'
import * as actionTypes from '../actions/actionTypes'
import { STORAGE_KEY_SUITS_COLORS } from '../constants'

function* setSuitsColorsSaga(action) {
  yield localStorage.setItem(STORAGE_KEY_SUITS_COLORS, action.payload)
}

export default function* setSuitsColors() {
  yield takeLatest(actionTypes.SET_SUITS_COLOR, setSuitsColorsSaga)
}
