import { cancel, delay, fork, put, select, take } from 'redux-saga/effects'
import * as actions from '../actions'
import * as actionTypes from '../actions/actionTypes'
import {
  DELAY_PLAY_NEW_GAME_SOUND,
  GAME_ENDED,
  GAME_INACTIVE,
  GAME_STARTED,
  SECONDS_LEFT_RESOLVE_SHOWDOWN
} from '../constants'
import { getPhase, getPlayers, getSoundStatus, getTimeLeft, getUserID } from '../selectors'
import { sound } from '../services'

function* playActionSaga() {
  while (true) {
    const { payload: soundID } = yield take(actionTypes.PLAY_SOUND)
    const soundon = yield select(getSoundStatus)

    soundon && sound.play(soundID)
  }
}

function* playerMove() {
  while (true) {
    const {
      serverState: { phase, userAction }
    } = yield take(actionTypes.PLAYER_MADE_MOVE)

    yield put(actions.playSound(userAction))

    if (phase === GAME_ENDED) {
      const players = yield select(getPlayers)
      const me = yield select(getUserID)
      const winnerPosition = Object.keys(players).find(player => players[player].winnings > 0)
      const endRoundSound = players[winnerPosition].userID === me ? 'you_won' : 'player_won'
      const timeLeft = yield select(getTimeLeft)

      yield delay(timeLeft * 1000 - SECONDS_LEFT_RESOLVE_SHOWDOWN * 1000)
      yield put(actions.playSound(endRoundSound))
      yield delay(DELAY_PLAY_NEW_GAME_SOUND)

      const newPhase = yield select(getPhase)

      if (newPhase !== GAME_INACTIVE) {
        yield put(actions.playSound('new_game'))
      }
    }
  }
}

function* tickSound() {
  let lastTask

  while (true) {
    const {
      serverState: { timeleft, phase, turn }
    } = yield take([actionTypes.PLAYER_MADE_MOVE, actionTypes.GET_STATE])

    const userID = yield select(getUserID)

    if (lastTask) {
      yield cancel(lastTask)
    }

    if (phase === GAME_STARTED && turn === userID) {
      yield put(actions.playSound('your_turn'))
      lastTask = yield fork(tickLoop, timeleft)
    }
  }
}

function* tickLoop(timeleft) {
  const loopValues = {
    slow: {
      trigger: 15,
      step: 1
    },
    medium: {
      trigger: 10,
      step: 0.9
    },
    fast: {
      trigger: 5,
      step: 0.8
    }
  }

  let inter = 0
  let counter = 0

  do {
    if (timeleft <= loopValues.fast.trigger) {
      inter = loopValues.fast.step
    } else if (timeleft <= loopValues.medium.trigger) {
      inter = loopValues.medium.step
    } else {
      inter = loopValues.slow.step
    }

    if (timeleft <= loopValues.slow.trigger) {
      counter % 2 === 0 ? yield put(actions.playSound('tick_1')) : yield put(actions.playSound('tick_2'))
    }

    counter += 1
    timeleft -= inter

    yield delay(inter * 1000)
  } while (timeleft > 0)
}

export default function* soundSaga() {
  yield fork(playActionSaga)
  yield fork(tickSound)
  yield fork(playerMove)
}
