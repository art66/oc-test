import { call, cancel, delay, fork, put, select, take } from 'redux-saga/effects'
import * as actions from '../actions'
import { GAME_INACTIVE, KICK_SITTING_OUT_DELAY } from '../constants'
import { getPhase, getUserID } from '../selectors'
import { api, centrifugeConnection } from '../services'

function* checkSitOut() {
  while (true) {
    yield delay(KICK_SITTING_OUT_DELAY)

    const phase = yield select(getPhase)

    if (phase === GAME_INACTIVE) {
      yield call(api.checkSitOut)
    }
  }
}

export default function* watchTableChange() {
  let sitOutTask

  while (true) {
    const {
      payload: { channel }
    } = yield take(actions.joinTable)
    const userID = yield select(getUserID)

    yield call([centrifugeConnection, centrifugeConnection.setChannels], { userID, channel })

    yield put(actions.askState())

    yield delay(1000)

    const phase = yield select(getPhase)

    if (phase === GAME_INACTIVE) {
      yield call(api.checkSitOut)
    }

    if (sitOutTask) {
      yield cancel(sitOutTask)
    }

    sitOutTask = yield fork(checkSitOut)
  }
}
