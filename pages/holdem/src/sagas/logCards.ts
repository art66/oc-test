import { call, select, takeLatest } from 'redux-saga/effects'
import { getServerState, getUserID, getTableID } from '../selectors'
import { logCards } from '../services/api'
import * as actionTypes from '../actions/actionTypes'
import getDateTime from '../utils/getDateTime'

function* cardsLoggerSaga(action) {
  try {
    const { response } = action
    const serverState = yield call(getServerState, response)
    const { hand, token } = serverState
    const userID = yield select(getUserID)
    const tableID = yield select(getTableID)
    const time = getDateTime()
    const requestData = { hand, token, userID, tableID, time }

    yield call(logCards, requestData)
  } catch (error) {
    console.error(error)
  }
}

export default function* cardsLogger() {
  yield takeLatest(actionTypes.PRIVATE_CHANNEL_EVENT, cardsLoggerSaga)
}
