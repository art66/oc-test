import { hideInfoBox } from '@torn/shared/middleware/infoBox'
import { put, takeLatest } from 'redux-saga/effects'
import { openBuyInPopup, openPopup, setSitPosition } from '../actions'
import { OPEN_BUY_IN_POPUP } from '../actions/actionTypes'

function* openBuyInSaga(action: ReturnType<typeof openBuyInPopup>) {
  yield put(setSitPosition(action.payload))
  yield put(hideInfoBox())
  yield put(openPopup('buy-in'))
}

export default function* openBuyIn() {
  yield takeLatest(OPEN_BUY_IN_POPUP, openBuyInSaga)
}
