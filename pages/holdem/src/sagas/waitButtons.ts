import { call, put, select, takeLatest } from 'redux-saga/effects'
import { show, hideInfoBox } from '@torn/shared/middleware/infoBox'
import { WAIT_BUTTON_CLICK } from '../actions/actionTypes'
import { unpressButton, updateWaitButtons } from '../actions'
import { getMyIndex } from '../selectors'
import { api } from '../services'

function* waitButtonsSaga(action) {
  try {
    const {
      payload: { step, pressed }
    } = action
    const position = yield select(getMyIndex)

    yield put(updateWaitButtons({ position, step, pressed }))
    yield call(step === 'waitbb' ? api.waitBigBlind : api.payButton, pressed)
    yield put(hideInfoBox())
  } catch (error) {
    yield put(show({ msg: error, color: 'red' }))
    yield put(unpressButton())
  }
}

export default function* waitButtons() {
  yield takeLatest(WAIT_BUTTON_CLICK, waitButtonsSaga)
}
