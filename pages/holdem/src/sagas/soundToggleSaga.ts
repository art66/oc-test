import { select, takeLatest, put } from 'redux-saga/effects'
import { HOLDEM_SOUND_STATUS } from '../constants'
import { TOGGLE_SOUND } from '../actions/actionTypes'
import { playSound } from '../actions'
import { getSoundStatus } from '../selectors'

function* soundToggleSaga() {
  const soundon = yield select(getSoundStatus)

  yield put(playSound('button'))
  localStorage.setItem(HOLDEM_SOUND_STATUS, soundon)
}

export default function* soundToggle() {
  yield takeLatest(TOGGLE_SOUND, soundToggleSaga)
}
