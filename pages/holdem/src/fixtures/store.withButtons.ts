export const mockState = {
  browser: {
    _responsiveState: true,
    width: 1130,
    height: 1089,
    lessThan: {
      small: false,
      medium: false,
      large: false,
      infinity: true
    },
    greaterThan: {
      small: true,
      medium: true,
      large: false,
      infinity: false
    },
    mediaType: 'large',
    orientation: 'landscape',
    breakpoints: {
      small: 600,
      medium: 1000,
      large: 5000,
      infinity: null
    }
  },
  infoBox: {},
  lobby: {
    tables: [
      {
        ID: 6,
        owner: 0,
        name: 'Gatling Gun',
        maxplayers: 9,
        stakes: '1k / $500',
        betlimit: 'N',
        minmoney: 500,
        maxmoney: 100000,
        speed: 15,
        type: 'default',
        password: 0,
        channel: 'holdem6',
        players: 7,
        minMoney: 10000,
        maxMoney: 100000
      },
      {
        ID: 10,
        owner: 0,
        name: 'Pound It',
        maxplayers: 9,
        stakes: '250k / $125k',
        betlimit: 'N',
        minmoney: 100000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem10',
        players: 7,
        minMoney: 2500000,
        maxMoney: 25000000
      },
      {
        ID: 9,
        owner: 0,
        name: 'Ballsy',
        maxplayers: 9,
        stakes: '25k / $12.5k',
        betlimit: 'N',
        minmoney: 10000,
        maxmoney: 2147483647,
        speed: 15,
        type: 'default',
        password: 0,
        channel: 'holdem9',
        players: 6,
        minMoney: 250000,
        maxMoney: 2500000
      },
      {
        ID: 25,
        owner: 0,
        name: 'Cat\'s Chance',
        maxplayers: 9,
        stakes: '2.5m / $1.25m',
        betlimit: 'N',
        minmoney: 250000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem25',
        players: 4,
        minMoney: 25000000,
        maxMoney: 250000000
      },
      {
        ID: 20,
        owner: 0,
        name: 'Boom or Bust',
        maxplayers: 9,
        stakes: '50k / $25k',
        betlimit: 'N',
        minmoney: 10000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem20',
        players: 3,
        minMoney: 500000,
        maxMoney: 5000000
      },
      {
        ID: 28,
        owner: 0,
        name: 'River Wizard',
        maxplayers: 9,
        stakes: '1m / $500k',
        betlimit: 'N',
        minmoney: 50000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem28',
        players: 3,
        minMoney: 10000000,
        maxMoney: 100000000
      },
      {
        ID: 31,
        owner: 0,
        name: 'test 3',
        maxplayers: 9,
        stakes: '50 / $25',
        betlimit: 'N',
        minmoney: 100,
        maxmoney: 2147483647,
        speed: 8888,
        type: 'default',
        password: 0,
        channel: 'holdem31',
        players: 2,
        minMoney: 500,
        maxMoney: 5000
      },
      {
        ID: 2,
        owner: 0,
        name: 'Newbie Corner',
        maxplayers: 9,
        stakes: '10 / $5',
        betlimit: 'N',
        minmoney: 10,
        maxmoney: 100000,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem2',
        players: 1,
        minMoney: 100,
        maxMoney: 1000
      },
      {
        ID: 3,
        owner: 0,
        name: 'Hobo Holdem',
        maxplayers: 9,
        stakes: '25 / $12.5',
        betlimit: 'N',
        minmoney: 100,
        maxmoney: 100000,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem3',
        players: 0,
        minMoney: 250,
        maxMoney: 2500
      },
      {
        ID: 30,
        owner: 0,
        name: 'test 2',
        maxplayers: 9,
        stakes: '50 / $25',
        betlimit: 'N',
        minmoney: 100,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem30',
        players: 0,
        minMoney: 500,
        maxMoney: 5000
      },
      {
        ID: 29,
        owner: 0,
        name: 'test 1',
        maxplayers: 5,
        stakes: '50 / $25',
        betlimit: 'N',
        minmoney: 100,
        maxmoney: 2147483647,
        speed: 100,
        type: 'default',
        password: 0,
        channel: 'holdem29',
        players: 0,
        minMoney: 500,
        maxMoney: 5000
      },
      {
        ID: 26,
        owner: 0,
        name: 'Broke Jokes',
        maxplayers: 9,
        stakes: '50 / $25',
        betlimit: 'N',
        minmoney: 5,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem26',
        players: 0,
        minMoney: 500,
        maxMoney: 5000
      },
      {
        ID: 4,
        owner: 0,
        name: 'Five Star Man',
        maxplayers: 5,
        stakes: '100 / $50',
        betlimit: 'N',
        minmoney: 100,
        maxmoney: 100000,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem4',
        players: 0,
        minMoney: 1000,
        maxMoney: 10000
      },
      {
        ID: 5,
        owner: 0,
        name: 'Sprinkles',
        maxplayers: 9,
        stakes: '250 / $125',
        betlimit: 'N',
        minmoney: 100,
        maxmoney: 10000000,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem5',
        players: 0,
        minMoney: 2500,
        maxMoney: 25000
      },
      {
        ID: 24,
        owner: 0,
        name: 'E-asy Street',
        maxplayers: 9,
        stakes: '500 / $250',
        betlimit: 'N',
        minmoney: 250,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem24',
        players: 0,
        minMoney: 5000,
        maxMoney: 50000
      },
      {
        ID: 7,
        owner: 0,
        name: 'Quickdraw',
        maxplayers: 9,
        stakes: '2.5k / $1.25k',
        betlimit: 'N',
        minmoney: 1000,
        maxmoney: 100000,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem7',
        players: 0,
        minMoney: 25000,
        maxMoney: 250000
      },
      {
        ID: 8,
        owner: 0,
        name: 'Tight Knit',
        maxplayers: 6,
        stakes: '5k / $2.5k',
        betlimit: 'N',
        minmoney: 1000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem8',
        players: 0,
        minMoney: 50000,
        maxMoney: 500000
      },
      {
        ID: 27,
        owner: 0,
        name: 'Six of the Best',
        maxplayers: 6,
        stakes: '10k / $5k',
        betlimit: 'N',
        minmoney: 5000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem27',
        players: 0,
        minMoney: 100000,
        maxMoney: 1000000
      },
      {
        ID: 17,
        owner: 0,
        name: 'Fourplay',
        maxplayers: 4,
        stakes: '100k / $50k',
        betlimit: 'N',
        minmoney: 250000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem17',
        players: 0,
        minMoney: 1000000,
        maxMoney: 10000000
      },
      {
        ID: 18,
        owner: 0,
        name: 'Duel at Dawn',
        maxplayers: 2,
        stakes: '100k / $50k',
        betlimit: 'N',
        minmoney: 100000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem18',
        players: 0,
        minMoney: 1000000,
        maxMoney: 10000000
      },
      {
        ID: 23,
        owner: 0,
        name: 'Periodic',
        maxplayers: 9,
        stakes: '100k / $50k',
        betlimit: 'N',
        minmoney: 10000,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem23',
        players: 0,
        minMoney: 1000000,
        maxMoney: 10000000
      },
      {
        ID: 11,
        owner: 0,
        name: 'Old \'n Slow',
        maxplayers: 6,
        stakes: '100k / $50k',
        betlimit: 'N',
        minmoney: 100000,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem11',
        players: 0,
        minMoney: 1000000,
        maxMoney: 10000000
      },
      {
        ID: 21,
        owner: 0,
        name: 'Old Folks Home',
        maxplayers: 6,
        stakes: '500k / $250k',
        betlimit: 'N',
        minmoney: 100000,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem21',
        players: 0,
        minMoney: 5000000,
        maxMoney: 50000000
      },
      {
        ID: 22,
        owner: 0,
        name: 'Comatose Cove',
        maxplayers: 6,
        stakes: '1m / $500k',
        betlimit: 'N',
        minmoney: 100000,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem22',
        players: 0,
        minMoney: 10000000,
        maxMoney: 100000000
      },
      {
        ID: 12,
        owner: 0,
        name: 'Tripod',
        maxplayers: 3,
        stakes: '1m / $500k',
        betlimit: 'N',
        minmoney: 500000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem12',
        players: 0,
        minMoney: 10000000,
        maxMoney: 100000000
      },
      {
        ID: 19,
        owner: 0,
        name: 'Juan on Juan',
        maxplayers: 2,
        stakes: '5m / $2.5m',
        betlimit: 'N',
        minmoney: 1000000,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem19',
        players: 0,
        minMoney: 50000000,
        maxMoney: 500000000
      },
      {
        ID: 13,
        owner: 0,
        name: 'Slow Cooker',
        maxplayers: 9,
        stakes: '5m / $2.5m',
        betlimit: 'N',
        minmoney: 1000000,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem13',
        players: 0,
        minMoney: 50000000,
        maxMoney: 500000000
      },
      {
        ID: 15,
        owner: 0,
        name: 'High Rollers',
        maxplayers: 9,
        stakes: '10m / $5m',
        betlimit: 'N',
        minmoney: 2500000,
        maxmoney: 2147483647,
        speed: 30,
        type: 'default',
        password: 0,
        channel: 'holdem15',
        players: 0,
        minMoney: 100000000,
        maxMoney: 1000000000
      },
      {
        ID: 14,
        owner: 0,
        name: 'Fire Pit',
        maxplayers: 9,
        stakes: '25m / $12.5m',
        betlimit: 'N',
        minmoney: 1000000,
        maxmoney: 2147483647,
        speed: 15,
        type: 'default',
        password: 0,
        channel: 'holdem14',
        players: 0,
        minMoney: 250000000,
        maxMoney: 2500000000
      },
      {
        ID: 16,
        owner: 0,
        name: 'Oligarch',
        maxplayers: 5,
        stakes: '100m / $50m',
        betlimit: 'N',
        minmoney: 10000000,
        maxmoney: 2147483647,
        speed: 60,
        type: 'default',
        password: 0,
        channel: 'holdem16',
        players: 0,
        minMoney: 1000000000,
        maxMoney: 10000000000
      }
    ]
  },
  log: [
    {
      author: 'The turn: ',
      userID: '0',
      message: 'Kdiamonds',
      color: 'green',
      meta: 'state',
      icon: 'four_cards',
      timestamp: '1628192948'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'checked',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1628192960'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'bet $50',
      color: 'black',
      meta: 'action',
      icon: 'raised_bet',
      timestamp: '1628192998'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'called $50',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1628193005'
    },
    {
      author: 'The river: ',
      userID: '0',
      message: '10diamonds',
      color: 'green',
      meta: 'state',
      icon: 'five_cards',
      timestamp: '1628193005'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'checked',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1628193009'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'bet $50',
      color: 'black',
      meta: 'action',
      icon: 'raised_bet',
      timestamp: '1628193011'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'raised $100 to $100',
      color: 'black',
      meta: 'action',
      icon: 'raised_bet',
      timestamp: '1628193013'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'called $50',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1628193015'
    },
    {
      author: 'romankr_test71',
      userID: '0',
      message: ' reveals [4diamonds, 2clubs] (Pair of Tens)',
      color: 'red',
      meta: 'state',
      icon: '',
      timestamp: '1628193015'
    },
    {
      author: 'Game',
      userID: '0',
      message: '7ba3ef35807321772a45f0ba64687c started',
      color: 'green',
      meta: 'state',
      icon: 'two_cards',
      timestamp: '1628193023'
    },
    {
      author: 'The preflop',
      userID: '0',
      message: 'Two cards dealt to each player',
      color: 'green',
      meta: 'state',
      icon: 'two_cards',
      timestamp: '1628193023'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'posted small blind $25',
      color: 'black',
      meta: 'action',
      icon: 'small_blind',
      timestamp: '1628193023'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'posted big blind $50',
      color: 'black',
      meta: 'action',
      icon: 'big_blind',
      timestamp: '1628193023'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'folded',
      color: 'grey',
      meta: 'action',
      icon: 'folded',
      timestamp: '1628201996'
    },
    {
      author: 'romankr_test71',
      userID: '0',
      message: ' won $75 Did not show hand',
      color: 'red',
      meta: 'won',
      icon: '',
      timestamp: '1628201996'
    },
    {
      author: 'Game',
      userID: '0',
      message: 'aeb4cbb3fcb1f6131feb8a0b6c1568 started',
      color: 'green',
      meta: 'state',
      icon: 'two_cards',
      timestamp: '1628761392'
    },
    {
      author: 'The preflop',
      userID: '0',
      message: 'Two cards dealt to each player',
      color: 'green',
      meta: 'state',
      icon: 'two_cards',
      timestamp: '1628761393'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'posted small blind $25',
      color: 'black',
      meta: 'action',
      icon: 'small_blind',
      timestamp: '1628761393'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'posted big blind $50',
      color: 'black',
      meta: 'action',
      icon: 'big_blind',
      timestamp: '1628761393'
    },
    {
      author: 'romankr_test71',
      userID: '1435333',
      message: 'folded',
      color: 'grey',
      meta: 'action',
      icon: 'folded',
      timestamp: '1628770392'
    },
    {
      author: 'toshykazu',
      userID: '0',
      message: ' won $75 Did not show hand',
      color: 'red',
      meta: 'won',
      icon: '',
      timestamp: '1628770392'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'left the table',
      color: 'grey',
      meta: 'joinleft',
      icon: 'left',
      timestamp: '1628839039'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'joined the table',
      color: 'grey',
      meta: 'joinleft',
      icon: 'joined',
      timestamp: '1628839070'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'left the table',
      color: 'grey',
      meta: 'joinleft',
      icon: 'left',
      timestamp: '1628839093'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'joined the table',
      color: 'grey',
      meta: 'joinleft',
      icon: 'joined',
      timestamp: '1628842677'
    },
    {
      author: 'Game',
      userID: '0',
      message: 'd11d41349a4db894a161d3345015b3 started',
      color: 'green',
      meta: 'state',
      icon: 'two_cards',
      timestamp: '1628842694'
    },
    {
      author: 'The preflop',
      userID: '0',
      message: 'Two cards dealt to each player',
      color: 'green',
      meta: 'state',
      icon: 'two_cards',
      timestamp: '1628842695'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'posted small blind $25',
      color: 'black',
      meta: 'action',
      icon: 'small_blind',
      timestamp: '1628842695'
    },
    {
      author: 'romankr_test72',
      userID: '2554199',
      message: 'posted big blind $50',
      color: 'black',
      meta: 'action',
      icon: 'big_blind',
      timestamp: '1628842695'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'raised $50 to $100',
      color: 'black',
      meta: 'action',
      icon: 'raised_bet',
      timestamp: '1628842731'
    },
    {
      author: 'romankr_test71',
      userID: '2554198',
      message: 'called $75',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1628842737'
    },
    {
      author: 'romankr_test72',
      userID: '1435333',
      message: 'folded',
      color: 'grey',
      meta: 'action',
      icon: 'folded',
      timestamp: '1628851629'
    },
    {
      author: 'The flop: ',
      userID: '0',
      message: 'Ahearts, Adiamonds, 8spades',
      color: 'green',
      meta: 'state',
      icon: 'three_cards',
      timestamp: '1628851629'
    },
    {
      author: 'toshykazu',
      userID: '2072301',
      message: 'checked',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1628920839'
    },
    {
      author: 'catherineis',
      userID: '2072301',
      message: 'joined the table',
      color: 'grey',
      meta: 'joinleft',
      icon: 'joined',
      timestamp: '1628920844'
    },
    {
      author: 'romankr_test71',
      userID: '2072301',
      message: 'checked',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1628953853'
    },
    {
      author: 'The turn: ',
      userID: '0',
      message: '6clubs',
      color: 'green',
      meta: 'state',
      icon: 'four_cards',
      timestamp: '1628953853'
    },
    {
      author: 'toshykazu',
      userID: '1435333',
      message: 'checked',
      color: 'black',
      meta: 'action',
      icon: 'checked_called',
      timestamp: '1629098443'
    }
  ],
  players: {
    1: {
      userID: 1435333,
      playername: 'toshykazu',
      status: 'Small Blind',
      state: '0',
      acceptedActions: ['raise', 'call', 'fold'],
      preselectButtons: [
        {
          name: 'Raise to',
          values: ['raise']
        },
        {
          name: 'Call',
          values: ['call']
        },
        {
          name: 'Fold',
          values: ['fold']
        }
      ],
      waitButtons: [],
      sitOutButtons: {
        action: 'sitout',
        name: 'Sit out',
        status: ''
      },
      revealButton: false,
      money: 28130,
      pot: 50,
      totalPot: 50,
      amountCall: 50,
      place: 0,
      minBet: 200,
      maxBet: 28180,
      minRaise: 200,
      maxRaise: 28180,
      isSitOut: false,
      isDealer: true,
      isSmallBlind: true,
      isBigBlind: false,
      isGameFinished: false,
      avatar: '/images/profile_man.jpg',
      winnings: 0,
      hand: ['clubs-A', 'hearts-A']
    },
    7: {
      userID: 2313413,
      playername: 'ruslanv',
      status: 'Big Blind',
      state: '0',
      acceptedActions: ['raise', 'check', 'fold'],
      preselectButtons: [
        {
          name: 'Call Any / Check',
          values: ['check', 'callAny']
        },
        {
          name: 'Check',
          values: ['check']
        },
        {
          name: 'Check / Fold',
          values: ['check', 'fold']
        }
      ],
      waitButtons: [],
      sitOutButtons: {
        action: 'sitout',
        name: 'Sit out',
        status: ''
      },
      revealButton: false,
      money: 18800,
      pot: 100,
      totalPot: 100,
      amountCall: 0,
      place: 7,
      minBet: 200,
      maxBet: 18900,
      minRaise: 200,
      maxRaise: 18900,
      isSitOut: false,
      isDealer: false,
      isSmallBlind: false,
      isBigBlind: true,
      isGameFinished: false,
      avatar: '/images/profile_man.jpg',
      winnings: 0,
      hand: []
    },
    8: {
      userID: 2156011,
      playername: 'userapptest3',
      status: 'Small Blind',
      state: '0',
      acceptedActions: ['raise', 'call', 'fold'],
      preselectButtons: [
        {
          name: 'Raise to',
          values: ['raise']
        },
        {
          name: 'Call',
          values: ['call']
        },
        {
          name: 'Fold',
          values: ['fold']
        }
      ],
      waitButtons: [],
      sitOutButtons: {
        action: 'sitout',
        name: 'Sit out',
        status: ''
      },
      revealButton: false,
      money: 28130,
      pot: 50,
      totalPot: 50,
      amountCall: 50,
      place: 0,
      minBet: 200,
      maxBet: 28180,
      minRaise: 200,
      maxRaise: 28180,
      isSitOut: false,
      isDealer: true,
      isSmallBlind: true,
      isBigBlind: false,
      isGameFinished: false,
      avatar: '/images/profile_man.jpg',
      winnings: 0,
      hand: []
    }
  },
  queuedAction: {
    action: [],
    testPlayers: {
      0: {
        actions: []
      },
      1: {
        actions: []
      },
      2: {
        actions: []
      },
      3: {
        actions: []
      },
      4: {
        actions: []
      },
      5: {
        actions: []
      },
      6: {
        actions: []
      },
      7: {
        actions: []
      },
      8: {
        actions: []
      },
      9: {
        actions: []
      }
    }
  },
  table: {
    userAction: '',
    amountCall: 50,
    bigBlind: 50,
    communityCards: ['diamonds-A', 'hearts-Q', 'spades-10', 'clubs-2', 'clubs-A'],
    phase: 'Started',
    raised: false,
    round: '',
    totalPot: 1000,
    tableID: '31',
    timeleft: 100,
    turn: 1,
    maxplayers: 4,
    minBet: 50,
    maxBet: 1000
  },
  ui: {
    tablesBg: true,
    sound: false
  },
  self: {
    satAtTheTable: false,
    betAmount: 0,
    hand: [],
    handIDs: {
      prevHandID: undefined,
      currentHandID: undefined
    },
    identity: {
      userID: 1435333,
      isDev: true
    },
    prevBetAmount: 0,
    prevCallAmount: 0,
    prevAction: '',
    sitPosition: -1
  },
  appType: 'full'
}
