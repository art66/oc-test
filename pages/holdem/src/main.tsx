import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import App from './containers/App'
import StoreProvider from './containers/StoreProvider'
import store from './store/createStore'

declare global {
  interface Window {
    state: any
  }
}

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('react-root')

let render = () => {
  ReactDOM.render(
    <StoreProvider store={store}>
      <App />
    </StoreProvider>,
    MOUNT_NODE
  )
}

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderApp = render
    const renderError = (error: any) => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./containers/StoreProvider', render)
  }
}

// ========================================================
// Go!
// ========================================================
render()
