export const INIT_APP = 'APP/INIT_APP'
export const SET_APP_TYPE = 'APP/SET_APP_TYPE'
export const ASK_CREDENTIALS = 'APP/ASK_CREDENTIALS'
export const INIT_LOBBY = 'APP/INIT_LOBBY'
export const INIT_WS = 'APP/INIT_WS'
export const ASK_STATE = 'APP/ASK_STATE'
export const ASK_STATE_SUCCESS = 'APP/ASK_STATE_SUCCESS'
export const SAVE_CREDENTIALS = 'APP/SAVE_CREDENTIALS'
export const SET_LOBBY_DATA = 'APP/SET_LOBBY_DATA'
export const TICK = 'APP/TICK'
export const TIMEOUT = 'APP/TIMEOUT'
export const START_NEXT_GAME = 'APP/START_NEXT_GAME'
export const PLAY_SOUND = 'APP/PLAY_SOUND'
export const SET_SELF_PLAYER_SAT = 'APP/SET_SELF_PLAYER_SAT'
export const SET_SELF_PLAYER_AWAY = 'APP/SET_SELF_PLAYER_AWAY'

export const MAKE_A_MOVE = 'USER_ACTION/MAKE_A_MOVE'
export const PRESS_BUTTON = 'USER_ACTION/PRESS_BUTTON'
export const UNPRESS_BUTTON = 'USER_ACTION/UNPRESS_BUTTON'
export const WAIT_BUTTON_CLICK = 'USER_ACTION/WAIT_BUTTON_CLICK'
export const SITOUT_IMBACK_CLICK = 'USER_ACTION/SITOUT_IMBACK_CLICK'
export const UPDATE_PLAYER = 'USER_ACTION/UPDATE_PLAYER'
export const REMOVE_PLAYER = 'USER_ACTION/REMOVE_PLAYER'
export const UPDATE_WAIT_BUTTONS = 'USER_ACTION/UPDATE_WAIT_BUTTONS'
export const SET_BET_AMOUNT = 'USER_ACTION/SET_BET_AMOUNT'
export const SET_PREV_ACTION = 'USER_ACTION/SET_PREV_ACTION'
export const SET_PREV_BET_AMOUNT = 'USER_ACTION/SET_PREV_BET_AMOUNT'
export const SET_PREV_CALL_AMOUNT = 'USER_ACTION/SET_PREV_CALL_AMOUNT'
export const OPEN_BUY_IN_POPUP = 'USER_ACTION/OPEN_BUY_IN_POPUP'
export const SET_SIT_POSITION = 'USER_ACTION/SET_SIT_POSITION'
export const SIT_IN = 'USER_ACTION/SIT_IN'
export const SUBSCRIBE_TO_TABLE = 'USER_ACTION/SUBSCRIBE_TO_TABLE'
export const OPEN_POPUP = 'USER_ACTION/OPEN_POPUP'
export const CLOSE_POPUP = 'USER_ACTION/CLOSE_POPUP'
export const TOGGLE_QUEUE = 'USER_ACTION/TOGGLE_QUEUE'
export const TOGGLE_QUEUE_TEST_PLAYER = 'TEST_PLAYER/TOGGLE_QUEUE_TEST_PLAYER'
export const TEST_PLAYER_ACTION = 'TEST_PLAYER/TEST_PLAYER_ACTION'
export const TOGGLE_SOUND = 'USER_ACTION/TOGGLE_SOUND'
export const TOGGLE_TABLES = 'USER_ACTION/TOGGLE_TABLES'
export const SET_SUITS_COLOR = 'USER_ACTION/SET_SUITS_COLOR'

export const LOBBY_CHANNEL_EVENT = 'SERVER/LOBBY_CHANNEL'
export const PRIVATE_CHANNEL_EVENT = 'SERVER/PRIVATE_CHANNEL'
export const PUBLIC_CHANNEL_EVENT = 'SERVER/PUBLIC_CHANNEL'

export const GET_STATE = 'SERVER/GET_STATE'
export const PLAYER_JOIN = 'SERVER/PLAYER_JOIN'
export const PLAYER_MADE_MOVE = 'SERVER/PLAYER_MADE_MOVE'
export const LOGS = 'SERVER/LOGS'
