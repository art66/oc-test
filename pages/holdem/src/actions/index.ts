import { showInfoBox } from '@torn/shared/middleware/infoBox'
import { createAction } from 'typesafe-actions'
import { TPopupItem, TSuitsColors } from '../reducers/types'
import { TSoundID } from '../services/sound'
import * as actionTypes from './actionTypes'

// app actions
export const initApp = createAction(actionTypes.INIT_APP)()
export const setAppType = createAction(actionTypes.SET_APP_TYPE, appType => appType)()
export const saveCredentials = createAction(actionTypes.SAVE_CREDENTIALS, (credentials: unknown) => credentials)()
export const initLobby = createAction(actionTypes.INIT_LOBBY)()
export const initWS = createAction(actionTypes.INIT_WS)()
export const setLobbyData = createAction(actionTypes.SET_LOBBY_DATA, (lobbyData: unknown) => lobbyData)()
export const setSelfPlayerSat = createAction(actionTypes.SET_SELF_PLAYER_SAT)()
export const setSelfPlayerAway = createAction(actionTypes.SET_SELF_PLAYER_AWAY)()

// time related actions
export const timeleftTick = createAction(actionTypes.TICK, (timeleft: number) => timeleft)()
export const timeout = createAction(actionTypes.TIMEOUT)()
export const startNextGame = createAction(actionTypes.START_NEXT_GAME)()

// user actions
export const makeMove = createAction(actionTypes.MAKE_A_MOVE, (moves: string[]) => moves)()
export const openPopup = createAction(actionTypes.OPEN_POPUP, (activeItem: TPopupItem) => activeItem)()
export const closePopup = createAction(actionTypes.CLOSE_POPUP)()
export const toggleQueue = createAction(actionTypes.TOGGLE_QUEUE, (buttonsQueue?: unknown[]) => buttonsQueue)()
export const toggleQueueTestPlayer = createAction(
  actionTypes.TOGGLE_QUEUE_TEST_PLAYER,
  (params: { position: unknown; actions: unknown }) => params
)()
export const testPlayerAction = createAction(
  actionTypes.TEST_PLAYER_ACTION,
  (params: { tableId: unknown; userID: unknown; action: unknown; position: unknown; step: unknown; amount: unknown }) =>
    params
)()
export const toggleTables = createAction(actionTypes.TOGGLE_TABLES)()
export const waitButtonClick = createAction(
  actionTypes.WAIT_BUTTON_CLICK,
  (params: { step: string; pressed: boolean }) => params
)()
export const sitOutImBackClick = createAction(actionTypes.SITOUT_IMBACK_CLICK, (params: { step: string }) => params)()
export const updatePlayer = createAction(actionTypes.UPDATE_PLAYER)()
export const removePlayer = createAction(actionTypes.REMOVE_PLAYER)()
export const updateWaitButtons = createAction(
  actionTypes.UPDATE_WAIT_BUTTONS,
  (params: { position: unknown; step: unknown; pressed: unknown }) => params
)()
export const setBetAmount = createAction(actionTypes.SET_BET_AMOUNT, (money: number) => money)()
export const setPrevAction = createAction(actionTypes.SET_PREV_ACTION)()
export const setPrevBetAmount = createAction(actionTypes.SET_PREV_BET_AMOUNT)()
export const setPrevCallAmount = createAction(actionTypes.SET_PREV_CALL_AMOUNT, (money: number) => money)()
export const openBuyInPopup = createAction(actionTypes.OPEN_BUY_IN_POPUP, (position: number) => position)()
export const setSitPosition = createAction(actionTypes.SET_SIT_POSITION, (position: number) => position)()
export const sitIn = createAction(actionTypes.SIT_IN, (moneyAmount: number) => ({
  moneyAmount
}))()
export const askState = createAction(actionTypes.ASK_STATE)()
export const askStateSuccess = createAction(actionTypes.ASK_STATE_SUCCESS, (somePayload: unknown) => somePayload)()
export const joinTable = createAction(
  actionTypes.SUBSCRIBE_TO_TABLE,
  (params: { tableID: unknown; channel: unknown }) => params
)()
export const askCredentials = createAction(actionTypes.ASK_CREDENTIALS)()
export const pressButton = createAction(actionTypes.PRESS_BUTTON, (button: string) => button)()
export const toggleSound = createAction(actionTypes.TOGGLE_SOUND)()
export const playSound = createAction(actionTypes.PLAY_SOUND, (soundId: TSoundID) => soundId)()
export const unpressButton = createAction(actionTypes.UNPRESS_BUTTON)()
export const setSuitsColor = createAction(actionTypes.SET_SUITS_COLOR, (suitsColors: TSuitsColors) => suitsColors)()

export const showInfoBoxMessage = error => dispatch => dispatch(showInfoBox({ msg: error, color: 'red' }))
