const arraysEqual = (first: any[], second: any[]): boolean => {
  return first.every(e => second.includes(e)) && second.every(e => first.includes(e))
}

export default arraysEqual
