const isEmpty = obj => {
  if (obj === null) return true
  if (obj === undefined) return true
  if (obj.length > 0) return false
  if (obj.length === 0) return true
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false
  }
  return true
}

export default isEmpty
