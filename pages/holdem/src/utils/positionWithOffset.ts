import { MAX_PLAYERS } from '../constants'

export const positionWithOffset = (position: number, myIndex: number, amISat: boolean) =>
  (amISat ? (position - myIndex + MAX_PLAYERS) % MAX_PLAYERS : position)
