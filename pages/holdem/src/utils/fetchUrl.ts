const fetchUrl = (url: string, data: object = {}) =>
  fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' },
    body: JSON.stringify(data)
  })
    .then(response => response.text())
    .then(text => {
      let json: any

      try {
        json = JSON.parse(text)
      } catch (e) {
        // eslint-disable-next-line no-throw-literal
        throw 'Malformed response'
      }
      if (json && json.error) {
        throw json.message || json.error
      }
      if (json.content) {
        throw json.content
      }
      return json
    })
    .catch(e => {
      throw e
    })

export default fetchUrl
