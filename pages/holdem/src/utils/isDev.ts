const isDev = () => {
  return location.href.includes('dev') || location.href.includes('localhost')
}

export default isDev
