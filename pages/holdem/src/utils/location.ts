import { SID_FULL, SID_WITH_SIDEBAR } from '../constants'

const getSid = () => {
  return location.href.includes(SID_FULL) ? SID_FULL : SID_WITH_SIDEBAR
}

const getPopped = () => {
  return location.href.includes('popped')
}

const isSidebarPresent = () => getSid() === SID_WITH_SIDEBAR

export { getSid, getPopped, isSidebarPresent }
