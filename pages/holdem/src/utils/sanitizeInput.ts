const stripComas = (val: string | number) => val?.toString().replace(/,/g, '')

export const sanitizeInput = (value: string) => parseInt(stripComas(value), 10)
