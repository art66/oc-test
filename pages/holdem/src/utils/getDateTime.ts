const checkZero = (data: number) => (data.toString().length === 1 ? `0${data}` : data)

export default function getUTCDateTime() {
  const currentTime = new Date()
  const day = checkZero(currentTime.getUTCDate())
  const month = checkZero(currentTime.getUTCMonth() + 1)
  const year = checkZero(currentTime.getUTCFullYear())
  const hour = checkZero(currentTime.getUTCHours())
  const minutes = checkZero(currentTime.getUTCMinutes())
  const seconds = checkZero(currentTime.getUTCSeconds())

  return `${day}-${month}-${year} ${hour}:${minutes}:${seconds}`
}
