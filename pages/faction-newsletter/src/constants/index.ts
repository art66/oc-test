// --------------------------
// REDUX NAMESPACES
// --------------------------
export const MEDIA_SCREEN_CHANGED = 'redux-responsive/CALCULATE_RESPONSIVE_STATE'
export const ROUTE_HEADER_SET = 'ROUTE_HEADER_SET'
export const MANUAL_DESKTOP_MODE = 'MANUAL_DESKTOP_MODE'
export const FETCH_FACTION_POSITIONS = 'faction-newsletter/FETCH_FACTION_POSITIONS'
export const SET_FACTION_POSITIONS = 'faction-newsletter/SET_FACTION_POSITIONS'
export const SELECT_POSITION = 'faction-newsletter/SELECT_POSITION'
export const UPDATE_SELECTED_POSITIONS = 'faction-newsletter/UPDATE_SELECTED_POSITIONS'
export const TOGGLE_FACTION_DESCRIPTION = 'faction-newsletter/TOGGLE_FACTION_DESCRIPTION'
export const SET_FACTION_DESCRIPTION_STATE = 'faction-newsletter/SET_FACTION_DESCRIPTION_STATE'
export const UPDATE_TITLE = 'faction-newsletter/UPDATE_TITLE'
export const UPDATE_NEWSLETTER_TEXT = 'faction-newsletter/UPDATE_NEWSLETTER_TEXT'
export const SEND_NEWSLETTER = 'faction-newsletter/SEND_NEWSLETTER'
export const SET_LOADING = 'faction-newsletter/SET_LOADING'
export const CLOSE_MESSAGE = 'faction-newsletter/CLOSE_MESSAGE'
export const SET_MESSAGE = 'faction-newsletter/SET_MESSAGE'

export const EDITOR_CONTENT_PREFIX = 'EditorContent-'
export const EDITOR_CONTENT_ID = 'faction-newsletter-text'

export const ALL_ID = 'all'

// --------------------------
// APP HEADER PATHS IDs
// --------------------------
export const PATHS_ID = {
  '': 1,
  someSubRoute: 2
}
