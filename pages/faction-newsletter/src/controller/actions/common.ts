import * as a from '../../constants'
import IDropdownItem from '../../interfaces/IDropdownItem'
import IMessage from '../../interfaces/IMessage'

export const fetchFactionPositions = () => ({
  type: a.FETCH_FACTION_POSITIONS
})

export const setFactionPositions = factionNewsletter => ({
  type: a.SET_FACTION_POSITIONS,
  factionNewsletter
})

export const selectPosition = (selectedPositions: IDropdownItem[]) => ({
  type: a.SELECT_POSITION,
  selectedPositions
})

export const updateSelectedPositions = (selectedPositions: IDropdownItem[]) => ({
  type: a.UPDATE_SELECTED_POSITIONS,
  selectedPositions
})

export const updateTitleInput = (value: string) => ({
  type: a.UPDATE_TITLE,
  value
})

export const updateNewsletterText = (value: string) => ({
  type: a.UPDATE_NEWSLETTER_TEXT,
  value
})

export const sendNewsletter = () => ({
  type: a.SEND_NEWSLETTER
})

export const setLoading = (value: boolean) => ({
  type: a.SET_LOADING,
  value
})

export const setMessage = (message: IMessage) => ({
  type: a.SET_MESSAGE,
  message
})

export const closeMessage = () => ({
  type: a.CLOSE_MESSAGE
})
