import { takeEvery, put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getTitleText, getSelectedPositions, getNewsletterText } from '../selectors'
import { setFactionPositions, setMessage, setLoading, updateSelectedPositions } from '../actions/common'

import * as a from '../../constants'

declare function addRFC(url: string): string

function* fetchFactionPositions() {
  try {
    const data = yield fetchUrl(addRFC('/factions.php?step=controlsNewsletterPositions'))

    if (data.success) {
      yield put(setFactionPositions(data))
    }
  } catch (err) {
    console.log(err)
  }
}

function* sendNewsletter() {
  try {
    const subject = yield select(getTitleText)
    const positions = yield select(getSelectedPositions)
    const newsletter = yield select(getNewsletterText)

    yield put(setLoading(true))

    const data = yield fetchUrl(addRFC('/factions.php?step=controlsNewsletter'), {
      filterOptions: positions.map(position => position.ID).join(','),
      subject,
      newsletter
    })

    if (data.success) {
      localStorage.setItem(`${a.EDITOR_CONTENT_PREFIX}${a.EDITOR_CONTENT_ID}`, '')
    }

    yield put(setMessage(data))
    yield put(setLoading(false))
  } catch (err) {
    console.log(err)
  }
}

function* selectPosition(action) {
  const previousSelectedPositions = yield select(getSelectedPositions)
  const currentSelectedPosition = action.selectedPositions

  if (currentSelectedPosition.length > previousSelectedPositions.length) {
    const newPosition = currentSelectedPosition.slice(-1)[0]

    if (newPosition.ID === a.ALL_ID) {
      yield put(updateSelectedPositions([newPosition]))
      return
    }

    yield put(updateSelectedPositions(currentSelectedPosition.filter(x => x.ID !== a.ALL_ID)))
    return
  }

  yield put(updateSelectedPositions(currentSelectedPosition))
}

export default function* factionProfileInfo() {
  yield takeEvery(a.FETCH_FACTION_POSITIONS, fetchFactionPositions)
  yield takeEvery(a.SEND_NEWSLETTER, sendNewsletter)
  yield takeEvery(a.SELECT_POSITION, selectPosition)
}
