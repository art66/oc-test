export const getFactionNewsletter = state => state.app.factionNewsletter
export const getPositions = state => state.app.factionNewsletter.positions
export const getSelectedPositions = state => state.app.selectedPositions
export const getTitleText = state => state.app.titleInput
export const getNewsletterText = state => state.app.newsletterText
export const getLoading = state => state.app.loading
export const getMessage = state => state.app.message
