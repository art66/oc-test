import initialState from '../../store/initialState'

import { IType } from '../../interfaces/IController'
import { IInitialState } from '../../interfaces/IStore'

import * as a from '../../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [a.SET_FACTION_POSITIONS]: (state: IInitialState, action) => {
    return {
      ...state,
      factionNewsletter: {
        positions: action.factionNewsletter.positions
      },
      selectedPositions: [action.factionNewsletter.positions[0]]
    }
  },
  [a.UPDATE_SELECTED_POSITIONS]: (state: IInitialState, action) => {
    return {
      ...state,
      selectedPositions: action.selectedPositions
    }
  },
  [a.UPDATE_TITLE]: (state: IInitialState, action) => {
    return {
      ...state,
      titleInput: action.value
    }
  },
  [a.UPDATE_NEWSLETTER_TEXT]: (state: IInitialState, action) => {
    return {
      ...state,
      newsletterText: action.value
    }
  },
  [a.SET_LOADING]: (state: IInitialState, action) => {
    return {
      ...state,
      loading: action.value
    }
  },
  [a.CLOSE_MESSAGE]: (state: IInitialState) => {
    return {
      ...state,
      message: null
    }
  },
  [a.SET_MESSAGE]: (state: IInitialState, action) => {
    return {
      ...state,
      message: action.message
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
