import { IInitialState } from '../interfaces/IStore'

const initialState: IInitialState = {
  isDesktopManualLayout: false,
  factionNewsletter: null,
  titleInput: 'Faction Newsletter',
  loading: false,
  message: null
}

export default initialState
