export type TDebugMessage = string | object

export interface IType {
  type: string
}

export interface ILocationChange extends IType {
  payload: {
    location: {
      hash: string
    }
  }
}

export interface IManualDesktopStatus extends IType {
  status: boolean
}
