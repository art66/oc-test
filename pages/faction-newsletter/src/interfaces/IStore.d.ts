import IFactionNewsletter from './IFactionNewsletter'
import IMessage from './IMessage'

export interface IInitialState {
  isDesktopManualLayout?: any
  factionNewsletter: IFactionNewsletter
  titleInput: string
  loading: boolean
  message: IMessage
}
