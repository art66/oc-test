export default interface IMessage {
  success: boolean
  text: string
}
