import IDropdownItem from './IDropdownItem'

export interface IFactionNewsletter {
  positions: IDropdownItem[]
}
