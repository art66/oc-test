import { IInitialState } from '../../interfaces/IStore'
import { IFactionNewsletter } from '../../interfaces/IFactionNewsletter'
import { IMessage } from '../../interfaces/IMessage'

export interface IProps extends IInitialState {
  fetchFactionPositions: () => void
  factionNewsletter: IFactionNewsletter
  loading: boolean
  message: IMessage
  titleInput?: string
}

export interface IContainerStore {
  app: IInitialState
}
