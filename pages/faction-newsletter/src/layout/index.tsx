import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import FactionNewsletter from '../components/FactionNewsletter'
import { fetchFactionPositions } from '../controller/actions/common'
import { getFactionNewsletter, getLoading, getMessage } from '../controller/selectors'
import s from './index.cssmodule.scss'

const AppLayout = () => {
  const dispatch = useDispatch()
  const factionNewsletter = useSelector(getFactionNewsletter)
  const loading = useSelector(getLoading)
  const message = useSelector(getMessage)

  useEffect(() => {
    dispatch(fetchFactionPositions())
  }, [dispatch])

  return (
    <div className={s.appWrap}>
      <div className={s.bodyWrap}>
        <FactionNewsletter factionNewsletter={factionNewsletter} loading={loading} message={message} />
      </div>
    </div>
  )
}

export default AppLayout
