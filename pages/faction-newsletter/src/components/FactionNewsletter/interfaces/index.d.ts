import { IFactionNewsletter } from '../../../interfaces/IFactionNewsletter'
import { IMessage } from '../../../interfaces/IMessage'

export interface IProps {
  factionNewsletter: FactionNewsletter
  message: IMessage
  loading: boolean
}
