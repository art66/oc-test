import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { getSelectedPositions, getTitleText, getNewsletterText } from '../../controller/selectors'
import { sendNewsletter } from '../../controller/actions/common'

const SendButton = () => {
  const dispatch = useDispatch()
  const selectedPositions = useSelector(getSelectedPositions)
  const titleText = useSelector(getTitleText)
  const newsletterText = useSelector(getNewsletterText)

  const disable = !selectedPositions.length || (!titleText && !newsletterText)

  const handleClick = () => {
    if (!disable) {
      dispatch(sendNewsletter())
    }
  }

  return (
    <span className={cn('btn-wrap silver m-top10', { disable })}>
      <span>
        <input type='submit' className='torn-btn' value='SEND' onClick={handleClick} />
      </span>
    </span>
  )
}

export default SendButton
