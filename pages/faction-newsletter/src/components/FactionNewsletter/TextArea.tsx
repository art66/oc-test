import React, { useCallback, useEffect, useLayoutEffect } from 'react'
import { useDispatch } from 'react-redux'
import { TOGGLE_DARK_MODE_EVENT } from '@torn/header/src/constants'
import { updateNewsletterText } from '../../controller/actions/common'
import { EDITOR_CONTENT_ID } from '../../constants'

declare function initializeTinyMCE(onAnimationEnd, options): null
declare const tinymce
const getEditor = () => tinymce.editors.find(e => e.id === EDITOR_CONTENT_ID)

const TextArea = () => {
  const dispatch = useDispatch()
  const initTinyMCE = useCallback(() => {
    initializeTinyMCE(null, {
      selector: `#${EDITOR_CONTENT_ID}`,
      afterInit: editor => {
        if (editor) {
          editor.on('change', e => {
            dispatch(updateNewsletterText(e.level.content))
          })

          dispatch(updateNewsletterText(editor.getContent()))
        }
      }
    })
  }, [dispatch])
  const reinitOnChange = useCallback(() => {
    setTimeout(() => {
      getEditor().on('change', e => {
        dispatch(updateNewsletterText(e.level.content))
      })
    }, 0)
  }, [dispatch])

  useEffect(() => {
    initTinyMCE()

    window.addEventListener(TOGGLE_DARK_MODE_EVENT, reinitOnChange)

    return () => {
      window.removeEventListener(TOGGLE_DARK_MODE_EVENT, reinitOnChange)
    }
  }, [initTinyMCE, reinitOnChange])

  useLayoutEffect(() => {
    return () => {
      getEditor()?.destroy()
    }
  }, [])

  return <textarea id={EDITOR_CONTENT_ID} className='tiny-area' name='newsletter' />
}

export default TextArea
