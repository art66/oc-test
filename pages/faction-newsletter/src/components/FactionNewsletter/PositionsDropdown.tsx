import React, { useCallback } from 'react'
import Dropdown from '@torn/shared/components/Dropdown'
import { useDispatch, useSelector } from 'react-redux'
import { getPositions, getSelectedPositions } from '../../controller/selectors'
import { selectPosition } from '../../controller/actions/common'
import IDropdownItem from '../../interfaces/IDropdownItem'

const PositionsDropdown = () => {
  const dispatch = useDispatch()
  const positions = useSelector(getPositions)
  const selectedPositions = useSelector(getSelectedPositions)

  const handleSelect = useCallback(
    (value: IDropdownItem[]) => {
      dispatch(selectPosition(value))
    },
    [dispatch]
  )

  return <Dropdown list={positions} selected={selectedPositions} onChange={handleSelect} multiselect={true} />
}

export default PositionsDropdown
