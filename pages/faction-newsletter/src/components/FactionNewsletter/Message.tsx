import React from 'react'
import cn from 'classnames'
import { useDispatch } from 'react-redux'
import { closeMessage } from '../../controller/actions/common'
import IMessage from '../../interfaces/IMessage'
import s from './index.cssmodule.scss'

interface IProps {
  message: IMessage
}

const Message = (props: IProps) => {
  const dispatch = useDispatch()
  const {
    message: { success, text }
  } = props

  const handleClick = () => {
    dispatch(closeMessage())
  }

  const handlePress = e => {
    if (e.key === 'Enter') {
      dispatch(closeMessage())
    }
  }

  return (
    <div className={s.message}>
      <span
        className={cn('m-right10 bold', {
          't-green': success,
          't-red': !success
        })}
      >
        {text}
      </span>
      <span className='c-pointer bold'>
        <span
          role='button'
          tabIndex={0}
          className='wai-support t-blue h'
          onKeyPress={handlePress}
          onClick={handleClick}
        >
          {' '}
          Okay
        </span>
      </span>
    </div>
  )
}

export default Message
