import React from 'react'
import cn from 'classnames'
import PositionsDropdown from './PositionsDropdown'
import TextArea from './TextArea'
import TitleInput from './TitleInput'
import SendButton from './SendButton'
import s from './index.cssmodule.scss'

const Newsletter = () => {
  return (
    <>
      <div className='desc'>
        <div className='info'>
          From here you can send off a newsletter to all of the members of your faction at once.
        </div>
      </div>
      <div className='letter-wrap'>
        <div className={cn('small-select-menu-wrap t-big-select-menu', s.topFields)}>
          <TitleInput />
          <div className='select-wrap newsletter-filter-options left'>
            <PositionsDropdown />
            <div className='select-list newsletter-filter' />
          </div>
          <span className={cn('t-blue h show-tiny-panels right c-pointer', s.editorIcon)}>
            <i className='icon-options' />
          </span>
          <div className='clear' />
        </div>
        <TextArea />
        <SendButton />
      </div>
    </>
  )
}

export default Newsletter
