import React, { ChangeEvent } from 'react'
import cn from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import { getTitleText } from '../../controller/selectors'
import { updateTitleInput } from '../../controller/actions/common'
import s from './index.cssmodule.scss'

const TitleInput = () => {
  const dispatch = useDispatch()
  const titleText = useSelector(getTitleText)

  const handleInput = (e: ChangeEvent<HTMLInputElement>) => {
    dispatch(updateTitleInput(e.target.value))
  }

  return (
    <div className='left m-right10'>
      <input type='text' className={cn(s.titleField)} value={titleText} onChange={handleInput} />
    </div>
  )
}

export default TitleInput
