import React from 'react'
import Preloader from '@torn/shared/components/Preloader'
import Newsletter from './Newsletter'
import Message from './Message'
import s from './index.cssmodule.scss'

import { IProps } from './interfaces'

const FactionNewsletter = (props: IProps) => {
  const { factionNewsletter, loading, message } = props

  if (!factionNewsletter || loading) {
    return (
      <div className={s.preloader}>
        <Preloader />
      </div>
    )
  }

  if (message) {
    return <Message message={message} />
  }

  return <Newsletter />
}

export default FactionNewsletter
