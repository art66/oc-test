import PropTypes from 'prop-types'
import React from 'react'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'
import '../../styles/account_recovery.scss'

export const CoreLayout = ({ children }) => (
  <div>
    <div className='core-layout__viewport'>{children}</div>
  </div>
)

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired
}

export default CoreLayout
