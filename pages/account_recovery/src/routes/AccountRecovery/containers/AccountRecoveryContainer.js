import { connect } from 'react-redux'
import {
  initAccountRecovery,
  getPotentionalUsers,
  unsetIncorrectFields,
  decrementProgress,
  incrementProgress,
  updateFormData,
  updateArrayFormData,
  resetPassword,
  searchForAccount,
  checkDonationOrderNumber,
  saveAccount,
  saveNewEmail,
  validateIP,
  saveAssocEmails,
  saveAssocPasswords,
  saveSecretAnswer,
  saveDateOfBirth,
  saveField,
  unsetFailure,
  loadSecretQuestion,
  completeRecovery,
  validateRecaptchaV2,
  validateRecaptchaV3
} from '../modules/accountrecovery'

import AccountRecovery from '../components/AccountRecovery'

const mapDispatchToProps = {
  initAccountRecovery,
  getPotentionalUsers,
  unsetIncorrectFields,
  decrementProgress,
  incrementProgress,
  updateFormData,
  updateArrayFormData,
  resetPassword,
  searchForAccount,
  checkDonationOrderNumber,
  saveAccount,
  saveNewEmail,
  validateIP,
  saveAssocEmails,
  saveAssocPasswords,
  saveSecretAnswer,
  saveDateOfBirth,
  saveField,
  unsetFailure,
  loadSecretQuestion,
  completeRecovery,
  validateRecaptchaV2,
  validateRecaptchaV3
}

const mapStateToProps = state => ({
  fetching: state.accountrecovery.fetching,
  progress: state.accountrecovery.progress,
  recoveryCompleted: state.accountrecovery.recoveryCompleted,
  potentionalUsers: state.accountrecovery.potentionalUsers,
  account: state.accountrecovery.account,
  forgottenPasswordEmail: state.accountrecovery.forgottenPasswordEmail,
  resetPasswordResult: state.accountrecovery.resetPasswordResult,
  searchAccount: state.accountrecovery.searchAccount,
  newEmail: state.accountrecovery.newEmail,
  newEmailConfirm: state.accountrecovery.newEmailConfirm,
  assocEmails: state.accountrecovery.assocEmails,
  assocPasswords: state.accountrecovery.assocPasswords,
  countrySignup: state.accountrecovery.countrySignup,
  donationOrderNumber: state.accountrecovery.donationOrderNumber,
  secretAnswer: state.accountrecovery.secretAnswer,
  dateOfBirthDay: state.accountrecovery.dateOfBirthDay,
  dateOfBirthMonth: state.accountrecovery.dateOfBirthMonth,
  dateOfBirthYear: state.accountrecovery.dateOfBirthYear,
  ipAddress: state.accountrecovery.ipAddress,
  donation: state.accountrecovery.donation,
  incorrectFields: state.accountrecovery.incorrectFields,
  recoveryFailed: state.accountrecovery.recoveryFailed,
  secretQuestion: state.accountrecovery.secretQuestion,
  success: state.accountrecovery.success
})

/*  Note: mapStateToProps is where you should use `reselect` to create selectors, ie:

    import { createSelector } from 'reselect'
    const counter = (state) => state.counter
    const tripleCount = createSelector(counter, (count) => count * 3)
    const mapStateToProps = (state) => ({
      counter: tripleCount(state)
    })

    Selectors can compute derived data, allowing Redux to store the minimal possible state.
    Selectors are efficient. A selector is not recomputed unless one of its arguments change.
    Selectors are composable. They can be used as input to other selectors.
    https://github.com/reactjs/reselect    */

export default connect(mapStateToProps, mapDispatchToProps)(AccountRecovery)
