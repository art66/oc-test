import { fetchUrl } from '../../../utils'
/* global getAction, getUrlParam */
// import _ from 'lodash'

// ------------------------------------
// Constants
// ------------------------------------
import { PROGRESS_MAX_COUNT } from '../../../constants'

const SET_RECOVERY_DATA = 'SET_RECOVERY_DATA'
const SET_POTENTIONAL_USERS = 'SET_POTENTIONAL_USERS'
const SET_INCORRECT_FIELDS = 'SET_INCORRECT_FIELDS'
const UNSET_INCORRECT_FIELDS = 'UNSET_INCORRECT_FIELDS'
const GET_AJAX_ERROR = 'GET_AJAX_ERROR'
const DECREMENT_PROGRESS = 'DECREMENT_PROGRESS'
const INCREMENT_PROGRESS = 'INCREMENT_PROGRESS'
const UPDATE_FORM_DATA = 'UPDATE_FORM_DATA'
const UPDATE_ARRAY_FORM_DATA = 'UPDATE_ARRAY_FORM_DATA'
const START_SUBMIT_FORM = 'START_SUBMIT_FORM'
const SET_RESET_PASSWORD_RESULT = 'SET_RESET_PASSWORD_RESULT'
const SET_ACCOUNT = 'SET_ACCOUNT'
const START_FETCHING_ACCOUNT = 'START_FETCHING_ACCOUNT'
const ACCOUNT_FETCHED = 'ACCOUNT_FETCHED'
const START_FETCHING_DONATION = 'START_FETCHING_DONATION'
const SET_DONATION_RESULT = 'SET_DONATION_RESULT'
const START_VALIDATING_IP = 'START_VALIDATING_IP'
const IP_VALIDATED = 'IP_VALIDATED'
const RECOVERY_FAILED = 'RECOVERY_FAILED'
const UNSET_FAILURE = 'UNSET_FAILURE'
const SET_SECRET_QUESTION = 'SET_SECRET_QUESTION'
const SHOW_RECOVERY_RESULT = 'SHOW_RECOVERY_RESULT'
const SET_SAME_EMAIL_INFO = 'SET_SAME_EMAIL_INFO'

// ------------------------------------
// Middlewares
// ------------------------------------

export const getAjaxError = error => {
  console.error(error)
  return {
    type: GET_AJAX_ERROR,
    error
  }
}

export const initAccountRecovery = () => dispatch => {
  return fetchUrl('initAccountRecovery').then(
    json => dispatch(setRecoveryData(json)),
    error => dispatch(getAjaxError(error))
  )
}

export const resetPassword = () => (dispatch, getState) => {
  const email = getState().accountrecovery.forgottenPasswordEmail

  if (!email || !email.includes('@')) {
    return dispatch(setIncorrectFields(['forgottenPasswordEmail']))
  }

  dispatch(startSubmitForm())
  getAction({
    type: 'post',
    action: '/page.php?sid=passwordResetInit',
    data: { email },
    success: data => {
      const result = {
        ...data,
        msg: data.msg ? data.msg : ''
      }

      dispatch(setResetPasswordResult(result))
    }
  })
}

export const searchForAccount = value => (dispatch, getState) => {
  if (getState().accountrecovery.fetchingAccount) {
    return
  }

  dispatch(startFetchingAccount())

  return fetchUrl('searchForAccount', { value: value }).then(
    json => {
      dispatch(setAccount(json))
      dispatch(accountFetched())
      const inputAccountValue = getState().accountrecovery.searchAccount

      if (value !== inputAccountValue) {
        dispatch(searchForAccount(inputAccountValue))
      }
    },
    error => dispatch(getAjaxError(error))
  )
}

export const getPotentionalUsers = () => dispatch => {
  return fetchUrl('getPotentionalUsers').then(
    json => dispatch(setPotentionalUsers(json.potentionalUsers)),
    error => dispatch(getAjaxError(error))
  )
}

export const checkDonationOrderNumber = orderNumber => dispatch => {
  dispatch(startFetchingDonation())

  return fetchUrl('checkDonationOrderNumber', { orderNumber }).then(
    json => dispatch(setDonationResult(json)),
    error => dispatch(getAjaxError(error))
  )
}

function checkAccount(account) {
  return account && account.userID ? [] : ['searchAccount']
}

export const saveAccount = () => (dispatch, getState) => {
  const { account } = getState().accountrecovery
  const incorrectFields = checkAccount(account)

  if (incorrectFields.length) {
    return dispatch(setIncorrectFields(incorrectFields))
  }

  dispatch(startSubmitForm())

  return fetchUrl('saveAccount', { userID: account.userID }).then(
    json => dispatch(json && json.recoveryFailed ? recoveryFailed(json) : incrementProgress()),
    error => dispatch(getAjaxError(error))
  )
}

function checkNewEmail(email, emailConfirm) {
  const incorrectFields = []

  if (!email || !email.includes('@')) {
    incorrectFields.push('newEmail')
  }

  if (email !== emailConfirm) {
    incorrectFields.push('newEmailConfirm')
  }

  return incorrectFields
}

export const saveNewEmail = () => (dispatch, getState) => {
  const state = getState().accountrecovery
  const email = state.newEmail
  const emailConfirm = state.newEmailConfirm
  const incorrectFields = checkNewEmail(email, emailConfirm)

  if (incorrectFields.length) {
    return dispatch(setIncorrectFields(incorrectFields))
  }

  dispatch(startSubmitForm())

  return fetchUrl('saveNewEmail', { email }).then(
    json => {
      dispatch(json && json.recoveryFailed ? recoveryFailed(json) : incrementProgress())
      dispatch(setSameEmailInfo(json))
    },
    error => dispatch(getAjaxError(error))
  )
}

export const validateIP = () => (dispatch) => {
  dispatch(startValidatingIp())

  return fetchUrl('validateIP').then(json => dispatch(ipValidated(json)), error => dispatch(getAjaxError(error)))
}

export const saveAssocEmails = () => (dispatch, getState) => {
  const { assocEmails } = getState().accountrecovery

  dispatch(startSubmitForm())

  return fetchUrl('saveAssocEmails', { assocEmails }).then(
    () => dispatch(incrementProgress()),
    error => dispatch(getAjaxError(error))
  )
}

export const saveAssocPasswords = () => (dispatch, getState) => {
  const { assocPasswords } = getState().accountrecovery

  dispatch(startSubmitForm())

  return fetchUrl('saveAssocPasswords', { assocPasswords }).then(
    () => dispatch(incrementProgress()),
    error => dispatch(getAjaxError(error))
  )
}

export const saveSecretAnswer = () => (dispatch, getState) => {
  const { secretAnswer } = getState().accountrecovery

  dispatch(startSubmitForm())

  return fetchUrl('saveSecretAnswer', { secretAnswer }).then(
    () => dispatch(incrementProgress()),
    error => dispatch(getAjaxError(error))
  )
}

export const validateRecaptchaV2 = recaptchaResponse => (dispatch, getState) => {
  getAction({
    type: 'post',
    action: '/account_recovery.php?q=validateRecaptchaV2',
    data: {
      'g-recaptcha-response': recaptchaResponse
    },
    success: resp => {
      if (resp && resp.recoveryFailed) {
        return dispatch(recoveryFailed(resp))
      }

      const step = getUrlParam('step')

      dispatch(incrementProgress())

      if (step === 'search' && getState().accountrecovery.progress === 2) {
        dispatch(incrementProgress())
      }
    },
    error: resp => {
      console.error(resp)
    }
  })
}

export const validateRecaptchaV3 = token => dispatch => {
  getAction({
    type: 'post',
    action: '/account_recovery.php?q=validateRecaptchaV3',
    data: {
      [token.key]: token.value
    },
    success: resp => {
      if (resp && resp.recoveryFailed) {
        return dispatch(recoveryFailed(resp))
      }
    },
    error: resp => {
      console.error(resp)
    }
  })
}

function checkDateOfBirth(day, month, year) {
  const incorrectFields = []

  if (day && (day < 1 || day > 31)) {
    incorrectFields.push('dateOfBirthDay')
  }

  if (month && (month < 1 || month > 12)) {
    incorrectFields.push('dateOfBirthMonth')
  }
  /* global getCurrentTimestamp */
  if (year && (year < 1 || year > new Date(getCurrentTimestamp()).getFullYear())) {
    incorrectFields.push('dateOfBirthYear')
  }

  return incorrectFields
}

export const saveDateOfBirth = () => (dispatch, getState) => {
  const state = getState().accountrecovery
  const day = state.dateOfBirthDay
  const month = state.dateOfBirthMonth
  const year = state.dateOfBirthYear
  let dateOfBirth = `${day}-${month}-${year}`

  dateOfBirth = dateOfBirth.includes('undefined') ? null : dateOfBirth
  const incorrectFields = checkDateOfBirth(day, month, year)

  if (incorrectFields.length) {
    return dispatch(setIncorrectFields(incorrectFields))
  }

  dispatch(startSubmitForm())

  return fetchUrl('saveDateOfBirth', { dateOfBirth }).then(
    () => dispatch(incrementProgress()),
    error => dispatch(getAjaxError(error))
  )
}

export const saveField = field => (dispatch, getState) => {
  const value = getState().accountrecovery[field]

  dispatch(startSubmitForm())

  return fetchUrl('saveField', { field, value }).then(
    () => dispatch(incrementProgress()),
    error => dispatch(getAjaxError(error))
  )
}

export const loadSecretQuestion = () => (dispatch, getState) => {
  const { secretQuestion } = getState().accountrecovery

  if (secretQuestion) {
    return
  }

  return fetchUrl('getSecretQuestion').then(
    json => dispatch(setSecretQuestion(json)),
    error => dispatch(getAjaxError(error))
  )
}

export const completeRecovery = () => (dispatch) => {
  dispatch(startSubmitForm())

  return fetchUrl('completeRecovery').then(
    json => dispatch(showRecoveryResult(json)),
    error => dispatch(getAjaxError(error))
  )
}

export const recoverDeletedAccount = (recoveryKey) => (dispatch) => {
  dispatch(startSubmitForm())

  return fetchUrl('recoverDeletedAccount', { recoveryKey }).then(data => dispatch(showRecoveryResult(data)))
}

export const cancelRecovery = () => () => {
  // dispatch(startSubmitForm())

  return fetchUrl('cancelRecovery')
}

// ------------------------------------
// Actions
// ------------------------------------
const setRecoveryData = data => ({
  type: SET_RECOVERY_DATA,
  data
})

export const setIncorrectFields = fields => ({
  type: SET_INCORRECT_FIELDS,
  fields
})

export const unsetIncorrectFields = fields => ({
  type: UNSET_INCORRECT_FIELDS,
  fields
})

export const decrementProgress = () => ({
  type: DECREMENT_PROGRESS
})

export const incrementProgress = () => ({
  type: INCREMENT_PROGRESS
})

export const updateFormData = (name, value) => ({
  type: UPDATE_FORM_DATA,
  name,
  value
})

export const updateArrayFormData = (name, value, index) => ({
  type: UPDATE_ARRAY_FORM_DATA,
  name,
  value,
  index
})

const startSubmitForm = () => ({
  type: START_SUBMIT_FORM
})

const setResetPasswordResult = data => ({
  type: SET_RESET_PASSWORD_RESULT,
  success: data.success,
  msg: data.msg
})

const startFetchingAccount = () => ({
  type: START_FETCHING_ACCOUNT
})

const accountFetched = () => ({
  type: ACCOUNT_FETCHED
})

const setAccount = account => ({
  type: SET_ACCOUNT,
  account
})

const setPotentionalUsers = users => ({
  type: SET_POTENTIONAL_USERS,
  users
})

const startFetchingDonation = () => ({
  type: START_FETCHING_DONATION
})

const setDonationResult = data => ({
  type: SET_DONATION_RESULT,
  isDonationValid: data.isDonationValid
})

const startValidatingIp = () => ({
  type: START_VALIDATING_IP
})

const ipValidated = data => ({
  type: IP_VALIDATED,
  isIpValid: data.isIpValid
})

const recoveryFailed = data => ({
  type: RECOVERY_FAILED,
  recoveryFailed: data.recoveryFailed
})

export const unsetFailure = () => ({
  type: UNSET_FAILURE
})

const setSecretQuestion = data => ({
  type: SET_SECRET_QUESTION,
  secretQuestion: data
})

const showRecoveryResult = data => ({
  type: SHOW_RECOVERY_RESULT,
  success: data.success,
  score: data.score,
  requiredScore: data.requiredScore,
  scores: data.scores,
  fetching: false
})

const setSameEmailInfo = data => ({
  type: SET_SAME_EMAIL_INFO,
  isEmailSame: data ? data.isEmailSame : false
})

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_RECOVERY_DATA]: (state, action) => {
    return {
      ...state,
      progress: action.data.progress,
      account: action.data.account || {},
      newEmail: action.data.newEmail || state.newEmail
    }
  },

  [SET_POTENTIONAL_USERS]: (state, action) => {
    return {
      ...state,
      potentionalUsers: action.users
    }
  },

  [SET_INCORRECT_FIELDS]: (state, action) => {
    return {
      ...state,
      incorrectFields: [...state.incorrectFields, ...action.fields]
    }
  },

  [UNSET_INCORRECT_FIELDS]: (state, action) => {
    return {
      ...state,
      incorrectFields: state.incorrectFields.filter(field => !action.fields.includes(field))
    }
  },

  [DECREMENT_PROGRESS]: (state) => {
    return {
      ...state,
      fetching: false,
      progress: state.progress > 1 ? state.progress - 1 : state.progress
    }
  },

  [INCREMENT_PROGRESS]: (state) => {
    return {
      ...state,
      fetching: false,
      progress: state.progress < PROGRESS_MAX_COUNT ? state.progress + 1 : state.progress
    }
  },

  [UPDATE_FORM_DATA]: (state, action) => {
    return {
      ...state,
      [action.name]: action.value
    }
  },

  [UPDATE_ARRAY_FORM_DATA]: (state, action) => {
    let fieldsArray = [...state[action.name]]

    fieldsArray = fieldsArray || []
    fieldsArray[action.index] = action.value

    return {
      ...state,
      [action.name]: fieldsArray
    }
  },

  [START_SUBMIT_FORM]: (state) => {
    return {
      ...state,
      fetching: true
    }
  },

  [SET_RESET_PASSWORD_RESULT]: (state, action) => {
    return {
      ...state,
      fetching: false,
      resetPasswordResult: {
        success: action.success,
        msg: action.msg
      }
    }
  },

  [SET_ACCOUNT]: (state, action) => {
    return {
      ...state,
      account: action.account || {}
    }
  },

  [START_FETCHING_ACCOUNT]: (state) => {
    return {
      ...state,
      fetchingAccount: true
    }
  },

  [ACCOUNT_FETCHED]: (state) => {
    return {
      ...state,
      fetchingAccount: false
    }
  },

  [START_FETCHING_DONATION]: (state) => {
    return {
      ...state,
      donation: {
        fetching: true
      }
    }
  },

  [SET_DONATION_RESULT]: (state, action) => {
    return {
      ...state,
      donation: {
        isValid: action.isDonationValid,
        fetching: false
      }
    }
  },

  [START_VALIDATING_IP]: (state) => {
    return {
      ...state,
      ipAddress: {
        fetching: true
      }
    }
  },

  [IP_VALIDATED]: (state, action) => {
    return {
      ...state,
      ipAddress: {
        isValid: action.isIpValid,
        fetching: false
      }
    }
  },

  [RECOVERY_FAILED]: (state, action) => {
    return {
      ...state,
      recoveryFailed: action.recoveryFailed,
      searchAccount: '',
      newEmail: '',
      newEmailConfirm: '',
      assocEmails: [],
      assocPasswords: [],
      countrySignup: '',
      donationOrderNumber: '',
      secretAnswer: '',
      dateOfBirthDay: '',
      dateOfBirthMonth: '',
      dateOfBirthYear: ''
    }
  },

  [UNSET_FAILURE]: (state) => {
    return {
      ...state,
      fetching: false,
      recoveryFailed: null
    }
  },

  [SET_SECRET_QUESTION]: (state, action) => {
    return {
      ...state,
      secretQuestion: action.secretQuestion
    }
  },

  [SHOW_RECOVERY_RESULT]: (state, action) => {
    return {
      ...state,
      progress: 13,
      success: action.success,
      recoveryCompleted: true,
      score: action.score,
      requiredScore: action.requiredScore,
      scores: action.scores,
      // account: {},
      searchAccount: '',
      // newEmail: '',
      newEmailConfirm: '',
      assocEmails: [],
      assocPasswords: [],
      countrySignup: '',
      donationOrderNumber: '',
      secretAnswer: '',
      dateOfBirthDay: '',
      dateOfBirthMonth: '',
      dateOfBirthYear: ''
    }
  },

  [SET_SAME_EMAIL_INFO]: (state, action) => {
    return {
      ...state,
      account: {
        ...state.account,
        isEmailSame: action.isEmailSame
      }
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  progress: 0,
  recoveryCompleted: false,
  resetPasswordResult: {},
  searchAccount: '',
  account: {},
  fetching: false,
  newEmail: '',
  ipAddress: {
    fetching: false
  },
  donation: {
    isValid: false,
    fetching: false
  },
  isDonationValid: false,
  assocEmails: [],
  assocPasswords: [],
  incorrectFields: []
}

export default function accountRecoveryReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
