import PropTypes from 'prop-types'
import React, { Component } from 'react'
import LoadingScreen from './steps/LoadingScreen'
import Captcha from './steps/Captcha'
import ForgotPassword from './steps/ForgotPassword'
import SearchForAccount from './steps/SearchForAccount'
import NewEmail from './steps/NewEmail'
import IPValidation from './steps/IPValidation'
import AssociatedEmails from './steps/AssociatedEmails'
import AssociatedPasswords from './steps/AssociatedPasswords'
import SignupGeolocation from './steps/SignupGeolocation'
import DonationOrderNumber from './steps/DonationOrderNumber'
import SecretAnswer from './steps/SecretAnswer'
import DateOfBirth from './steps/DateOfBirth'
import RecoveryFailed from './RecoveryFailed'
import CompleteRecovery from './steps/CompleteRecovery'
import RecoveryResult from './steps/RecoveryResult'
import RecoverDeletedAccount from './steps/RecoverDeletedAccount'
import Title from './Title'
/* global getUrlParam */

const STEPS = {
  0: LoadingScreen,
  1: Captcha,
  2: ForgotPassword,
  3: SearchForAccount,
  4: NewEmail,
  5: IPValidation,
  6: AssociatedEmails,
  7: AssociatedPasswords,
  8: SignupGeolocation,
  9: DonationOrderNumber,
  10: SecretAnswer,
  11: DateOfBirth,
  12: CompleteRecovery,
  13: RecoveryResult
}

class AccountRecovery extends Component {
  componentDidMount() {
    this.props.initAccountRecovery().then(() => {
      let step = getUrlParam('step')
      let progress = this.props.progress

      if (step === 'search' && progress === 2) {
        this.props.incrementProgress()
      } else if (progress === 1) {

      }
    })
  }

  handleInputChange = event => {
    this.props.unsetIncorrectFields([event.target.name])
    this.props.updateFormData(event.target.name, event.target.value)
  }

  handleArrayInputChange = (event, index) => {
    this.props.updateArrayFormData(event.target.name, event.target.value, index)
  }

  getCurrentStepComponent = () => {
    const { recoveryFailed, account, progress, recoveryCompleted } = this.props

    if (recoveryFailed) {
      return (
        <RecoveryFailed
          recoveryFailed={this.props.recoveryFailed}
          account={this.props.account}
          unsetFailure={this.props.unsetFailure}
        />
      )
    } else if (account.deleted && progress > 4 && !recoveryCompleted) {
      return (
        <RecoverDeletedAccount />
      )
    }

    let Step = STEPS[progress]

    return (
      <Step
        {...this.props}
        handleInputChange={this.handleInputChange}
        handleArrayInputChange={this.handleArrayInputChange}
        checkField={this.checkField}
      />
    )
  }

  checkField = name => {
    const { incorrectFields } = this.props

    if (!incorrectFields) {
      return true
    }

    return !incorrectFields.includes(name)
  }

  render() {
    const { progress, account, recoveryCompleted } = this.props
    const currentComponent = this.getCurrentStepComponent()

    return (
      <div className="account-recovery-wrap">
        <Title progress={progress} accountDeleted={account.deleted} recoveryCompleted={recoveryCompleted} />
        {currentComponent}
      </div>
    )
  }
}

AccountRecovery.propTypes = {
  progress: PropTypes.number.isRequired,
  recoveryCompleted: PropTypes.bool,
  potentionalUsers: PropTypes.array,
  account: PropTypes.object,
  fetching: PropTypes.bool,
  newEmail: PropTypes.string,
  ipAddress: PropTypes.object,
  donation: PropTypes.object,
  incorrectFields: PropTypes.array,
  recoveryFailed: PropTypes.string,
  initAccountRecovery: PropTypes.func.isRequired,
  unsetIncorrectFields: PropTypes.func.isRequired,
  updateFormData: PropTypes.func.isRequired,
  updateArrayFormData: PropTypes.func.isRequired,
  searchForAccount: PropTypes.func.isRequired,
  checkDonationOrderNumber: PropTypes.func.isRequired,
  saveAccount: PropTypes.func.isRequired,
  saveNewEmail: PropTypes.func.isRequired,
  validateIP: PropTypes.func.isRequired,
  saveDateOfBirth: PropTypes.func.isRequired,
  saveField: PropTypes.func.isRequired,
  unsetFailure: PropTypes.func.isRequired,
  incrementProgress: PropTypes.func,
  validateRecaptchaV2: PropTypes.func,
  validateRecaptchaV3: PropTypes.func
}

export default AccountRecovery
