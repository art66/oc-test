import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from '../Progress'
import Field from '../Field'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

class NewEmail extends Component {
  handleSubmit = e => {
    e.preventDefault()
    this.props.saveNewEmail()
  }

  render() {
    const {
      progress,
      newEmail,
      newEmailConfirm,
      account,
      decrementProgress,
      handleInputChange,
      checkField
    } = this.props

    return (
      <div className="new-email content cont-gray bottom-round">
        <div className="left-cont">
          <p className="text-block">
            If identity verification is successful, the login email address associated with
            <a href={account.link} className="nav-link">
              {' '}
              {account.playername} [{account.userID}]{' '}
            </a>
            can be changed and the additional security features will be reset. If you do not wish to change your email
            address, please enter the current one.
          </p>
          <p className="text-block">
            Please enter the email address you wish this account to be associated with, ensuring that it is secure and
            only you have access to it. If it's different from the current one assigned to the account, the ownership of
            this Torn account will be transferred to it.
          </p>
        </div>
        <div className="right-cont">
          <div className="progress-wrap">
            <Progress progress={progress} />
          </div>
          <div className="main-cont">
            <form onSubmit={this.handleSubmit}>
              <div className="input-group">
                <Field
                  name="newEmail"
                  type="email"
                  id="new-email-address"
                  value={newEmail || ''}
                  label="New email address:"
                  autoFocus="true"
                  onChange={handleInputChange}
                  checkField={checkField}
                />
                <Field
                  name="newEmailConfirm"
                  type="email"
                  id="new-email-address-confirm"
                  value={newEmailConfirm || ''}
                  label="Confirm new email address:"
                  onChange={handleInputChange}
                  checkField={checkField}
                />
              </div>
              <div className="buttons">
                <TornBtn value="THIS IS MY NEW EMAIL ADDRESS" loading={this.props.fetching} />
                <BackLink onClick={decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

NewEmail.propTypes = {
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  newEmailConfirm: PropTypes.string,
  account: PropTypes.object.isRequired,
  decrementProgress: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  checkField: PropTypes.func,
  saveNewEmail: PropTypes.func,
  fetching: PropTypes.bool
}

export default NewEmail
