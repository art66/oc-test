import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Field from '../Field'
import TornBtn from '../TornBtn'
import CancelLink from '../CancelLink'

class ForgotPassword extends Component {
  getContent = () => {
    const { forgottenPasswordEmail, resetPasswordResult, handleInputChange, checkField, fetching } = this.props

    if (resetPasswordResult.success) {
      return (
        <div className='reset-password-result validated'>
          <i className='ip-validation-icon' />
          <span className={`text ${resetPasswordResult.msg && !resetPasswordResult.success ? 'error' : ''}`}>
            {resetPasswordResult.msg}
          </span>
        </div>
      )
    }
    return (
      <form onSubmit={this._handleSubmit}>
        <h5 className='title'>I only need to reset my password</h5>
        <Field
          name='forgottenPasswordEmail'
          type='email'
          id='forgotten-password-email'
          value={forgottenPasswordEmail || ''}
          classNameWrap={
            `forgotten-password-email-input-wrap ${
              resetPasswordResult.msg && !resetPasswordResult.success ? 'error' : ''}`
          }
          label={
            resetPasswordResult.msg
              || `If you've only forgotten your password, please enter your email
              address below and we'll send you a link to reset it.`
          }
          placeholder='Enter email address'
          autoFocus='true'
          onChange={handleInputChange}
          checkField={checkField}
        />
        <TornBtn value='RESET PASSWORD' loading={fetching} />
        <CancelLink />
      </form>
    )
  }

  _handleSubmit = e => {
    e.preventDefault()
    const { resetPassword } = this.props

    resetPassword()
  }

  render() {
    const { incrementProgress } = this.props

    return (
      <div className='content cont-gray bottom-round forgot-password-step'>
        <div className='forgot-password-form-wrap'>{this.getContent()}</div>
        <div className='recovery-link-wrap'>
          <h5 className='title'>I need to recover or unlock my account</h5>
          <p className='text-wrap'>
            If you have been locked out of your account by failing too many security questions or your email address is
            forgotten or inaccessible, you'll need to complete the account recovery process by clicking one of the
            options at the bottom.
          </p>
          <TornBtn value='RECOVER LOST ACCOUNT' onClick={() => incrementProgress()} />
          <TornBtn value='UNLOCK ACCOUNT' onClick={() => incrementProgress()} />
        </div>
      </div>
    )
  }
}

ForgotPassword.propTypes = {
  forgottenPasswordEmail: PropTypes.string,
  resetPasswordResult: PropTypes.object,
  fetching: PropTypes.bool,
  resetPassword: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  checkField: PropTypes.func.isRequired,
  incrementProgress: PropTypes.func.isRequired
}

export default ForgotPassword
