import React, { Component } from 'react'
import { connect } from 'react-redux'
import { incrementProgress } from '../../modules/accountrecovery'
/* global $, grecaptcha, getRecaptchaV3Token */

interface IProps {
  incrementProgress(): void
  validateRecaptchaV2(recaptchaResponse: any): void
  validateRecaptchaV3(token: string): void
}

class Captcha extends Component<IProps> {
  recaptchaContainer: HTMLDivElement

  componentDidMount() {
    if (location.hostname === 'localhost') {
      return this.props.incrementProgress()
    }

    this.renderCaptchaV2()
    this.initRecaptchaV3()
  }

  renderCaptchaV2 = () => {
    (window as any).grecaptcha.render(
      this.recaptchaContainer,
      {
        sitekey: '6LeBSF4UAAAAAHLG3mxZCsyM12dW83D1qJI_RrUZ',
        callback: this.recaptchaV2Callback
      }
    )
  }

  recaptchaV2Callback = (response) => {
    $('.ui-tooltip').remove()
    this.props.validateRecaptchaV2(response)
  }

  initRecaptchaV3 = () => {
    (window as any).getRecaptchaV3Token('accountrecovery')
      .catch(error => {
        console.error(error)
      })
      .then(token => {
        this.props.validateRecaptchaV3(token)
      })
  }

  render() {
    return (
      <div className='content cont-gray bottom-round forgot-password-step'>
        <div className='p10'>
          <div id='recaptcha-container' ref={el => { this.recaptchaContainer = el }} />
        </div>
      </div>
    )
  }
}

export default connect(
  () => ({}),
  { incrementProgress }
)(Captcha)
