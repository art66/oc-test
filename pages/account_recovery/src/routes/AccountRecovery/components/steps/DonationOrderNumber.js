import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from '../Progress'
import Info from '../Info'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

class DonationOrderNumber extends Component {
  constructor(props) {
    super(props)
    this.props.loadSecretQuestion()
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.saveField('donationOrderNumber')
  }

  handleInputChange = e => {
    this.props.checkDonationOrderNumber(e.target.value)
    this.props.handleInputChange(e)
  }

  render() {
    const { progress, donationOrderNumber, newEmail, account, donation, decrementProgress } = this.props

    return (
      <div className="donation-order-number content cont-gray bottom-round">
        <div className="left-cont">
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className="right-cont">
          <div className="progress-wrap">
            <Progress progress={progress} />
          </div>
          <div className="main-cont">
            <h5 className="title">Donation order number</h5>
            <p className="text-block">
              Do you remember ever making a donation to Torn? Please look through your records on PayPal, Amazon, Google
              Checkout, Zong, BitCoin, or any payment confirmations you might have received by email. Try entering the
              key, transaction ID, receipt ID, invoice ID, profile ID or subscription ID to see if you can find a match.
            </p>
            <form onSubmit={this.handleSubmit}>
              <div className="input-group">
                <div
                  className={
                    'input-wrap ' + (donation.fetching ? 'loading' : donation.isValid ? 'validated' : 'unvalidated')
                  }
                >
                  <i
                    className="donation-validation-icon"
                    title={
                      !donation.isValid
                        ? `Some payments have multiple ID types associated with them
                                               (Receipt ID, Invoice ID and TransactionID). Perhaps you can try another?`
                        : 'OK'
                    }
                  />
                  <input
                    type="text"
                    className="input-text validated-input"
                    name="donationOrderNumber"
                    value={donationOrderNumber || ''}
                    onChange={this.handleInputChange}
                  />
                </div>
              </div>
              <div className="buttons">
                <TornBtn value="NEXT" loading={this.props.fetching} />
                <BackLink onClick={decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

DonationOrderNumber.propTypes = {
  donationOrderNumber: PropTypes.string,
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  donation: PropTypes.object.isRequired,
  decrementProgress: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  saveField: PropTypes.func.isRequired,
  checkDonationOrderNumber: PropTypes.func.isRequired,
  fetching: PropTypes.bool,
  loadSecretQuestion: PropTypes.func
}

export default DonationOrderNumber
