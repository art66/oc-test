import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from '../Progress'
import Info from '../Info'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

class AssociatedPasswords extends Component {
  handleSubmit = e => {
    e.preventDefault()
    this.props.saveAssocPasswords()
  }

  render() {
    const { progress, assocPasswords, decrementProgress, newEmail, account, handleArrayInputChange } = this.props

    return (
      <div className="associated-passwords content cont-gray bottom-round">
        <div className="left-cont">
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className="right-cont">
          <div className="progress-wrap">
            <Progress progress={progress} />
          </div>
          <div className="main-cont">
            <h5 className="title">Passwords associated with this account</h5>
            <p className="text-block">
              Do you remember any passwords that have currently or previously been used on this account? Exact case
              sensitive matches are required.
            </p>
            <form onSubmit={this.handleSubmit}>
              <div className="input-group half">
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocPasswords"
                    className="input-text"
                    value={assocPasswords[0] || ''}
                    autoFocus="true"
                    onChange={e => handleArrayInputChange(e, 0)}
                  />
                </div>
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocPasswords"
                    className="input-text"
                    value={assocPasswords[1] || ''}
                    placeholder="Leave blank if not required"
                    onChange={e => handleArrayInputChange(e, 1)}
                  />
                </div>
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocPasswords"
                    className="input-text"
                    value={assocPasswords[2] || ''}
                    placeholder="Leave blank if not required"
                    onChange={e => handleArrayInputChange(e, 2)}
                  />
                </div>
              </div>
              <div className="input-group half">
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocPasswords"
                    className="input-text"
                    value={assocPasswords[3] || ''}
                    placeholder="Leave blank if not required"
                    onChange={e => handleArrayInputChange(e, 3)}
                  />
                </div>
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocPasswords"
                    className="input-text"
                    value={assocPasswords[4] || ''}
                    placeholder="Leave blank if not required"
                    onChange={e => handleArrayInputChange(e, 4)}
                  />
                </div>
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocPasswords"
                    className="input-text"
                    value={assocPasswords[5] || ''}
                    placeholder="Leave blank if not required"
                    onChange={e => handleArrayInputChange(e, 5)}
                  />
                </div>
              </div>
              <div className="buttons">
                <TornBtn value="NEXT" loading={this.props.fetching} />
                <BackLink onClick={decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

AssociatedPasswords.propTypes = {
  assocPasswords: PropTypes.array,
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
  decrementProgress: PropTypes.func.isRequired,
  handleArrayInputChange: PropTypes.func.isRequired,
  saveAssocPasswords: PropTypes.func.isRequired
}

export default AssociatedPasswords
