import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from '../Progress'
import Info from '../Info'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

class AssociatedEmails extends Component {
  handleSubmit = e => {
    e.preventDefault()
    this.props.saveAssocEmails()
  }

  render() {
    const { progress, assocEmails, decrementProgress, newEmail, account, handleArrayInputChange } = this.props

    return (
      <div className="associated-emails content cont-gray bottom-round">
        <div className="left-cont">
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className="right-cont">
          <div className="progress-wrap">
            <Progress progress={progress} />
          </div>
          <div className="main-cont">
            <h5 className="title">Email addresses associated with this account</h5>
            <p className="text-block">
              Do you remember any email addresses that have currently or previously been used on this account? Please
              try to be as accurate as possible.
            </p>
            <form onSubmit={this.handleSubmit}>
              <div className="input-group">
                <div className="input-wrap">
                  <input
                    type="email"
                    name="assocEmails"
                    className="input-text"
                    value={assocEmails[0] || ''}
                    onChange={e => handleArrayInputChange(e, 0)}
                    autoFocus="true"
                  />
                </div>
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocEmails"
                    className="input-text"
                    value={assocEmails[1] || ''}
                    placeholder="Leave blank if not required"
                    onChange={e => handleArrayInputChange(e, 1)}
                  />
                </div>
                <div className="input-wrap">
                  <input
                    type="text"
                    name="assocEmails"
                    className="input-text"
                    value={assocEmails[2] || ''}
                    placeholder="Leave blank if not required"
                    onChange={e => handleArrayInputChange(e, 2)}
                  />
                </div>
              </div>
              <div className="buttons">
                <TornBtn value="NEXT" loading={this.props.fetching} />
                <BackLink onClick={decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

AssociatedEmails.propTypes = {
  assocEmails: PropTypes.array,
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
  decrementProgress: PropTypes.func.isRequired,
  handleArrayInputChange: PropTypes.func.isRequired,
  saveAssocEmails: PropTypes.func.isRequired
}

export default AssociatedEmails
