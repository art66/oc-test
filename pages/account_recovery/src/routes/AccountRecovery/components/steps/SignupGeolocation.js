import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from '../Progress'
import Info from '../Info'
import Dropdown from '@torn/shared/components/Dropdown'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'
import { fetchUrl } from '../../../../utils'

class SignupGeolocation extends Component {
  constructor(props) {
    super(props)
    this.state = { countries: [] }
    this.loadCountries()
  }

  loadCountries = () => {
    return fetchUrl('getCountries').then(
      json =>
        this.setState({
          countries: json.map(country => ({ name: country.country_name }))
        }),
      error => {
        console.log(error)
      }
    )
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.saveField('countrySignup')
  }

  handleDropdownChange = (value, props) => {
    this.props.updateFormData(props.name, value.name)
  }

  render() {
    const { progress, countrySignup, decrementProgress, newEmail, account } = this.props
    const selectedCountry = this.state.countries.find(country => country.name === countrySignup)

    return (
      <div className="signup-geolocation content cont-gray bottom-round">
        <div className="left-cont">
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className="right-cont">
          <div className="progress-wrap">
            <Progress progress={progress} />
          </div>
          <div className="main-cont">
            <h5 className="title">Geographical location of signup</h5>
            <p className="text-block">Enter the country you were in when this account was first registered.</p>
            <form onSubmit={this.handleSubmit}>
              <Dropdown
                className="react-dropdown-default"
                list={this.state.countries || []}
                selected={selectedCountry}
                name="countrySignup"
                placeholder="Select country"
                onInit={this.handleDropdownChange}
                onChange={this.handleDropdownChange}
              />
              <div className="buttons">
                <TornBtn value="NEXT" loading={this.props.fetching} />
                <BackLink onClick={decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

SignupGeolocation.propTypes = {
  countrySignup: PropTypes.string,
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
  decrementProgress: PropTypes.func.isRequired,
  saveField: PropTypes.func.isRequired,
  updateFormData: PropTypes.func.isRequired
}

export default SignupGeolocation
