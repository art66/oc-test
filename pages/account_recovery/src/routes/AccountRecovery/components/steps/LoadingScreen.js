import React, { Component } from 'react'

class LoadingScreen extends Component {
  render() {
    return (
      <div className="loading-screen loading content cont-gray bottom-round">
        <div className="preloader-wrap">
          <div className="dzsulb-preloader preloader-fountain">
            <div id="fountainG_1" className="fountainG" />
            <div id="fountainG_2" className="fountainG" />
            <div id="fountainG_3" className="fountainG" />
            <div id="fountainG_4" className="fountainG" />
          </div>
        </div>
      </div>
    )
  }
}

LoadingScreen.propTypes = {}

export default LoadingScreen
