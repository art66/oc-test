import React, { ChangeEvent, Component } from 'react'
import { connect } from 'react-redux'
import { decrementProgress, recoverDeletedAccount } from '../../modules/accountrecovery'
import Progress from '../Progress'
import Field from '../Field'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

const RECOVERY_KEY_PART_LENGTH = 4

interface IProps {
  fetching: boolean
  decrementProgress(): void
  recoverDeletedAccount(recoveryKey: string): void
}

interface IState {
  recoveryKeyParts: string[]
}

class RecoverDeletedAccount extends Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = {
      recoveryKeyParts: []
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    const recoveryKey = this.state.recoveryKeyParts.join('-')
    this.props.recoverDeletedAccount(recoveryKey)
  }

  handleInputChange = (e: ChangeEvent<HTMLInputElement>, part: number) => {
    e.preventDefault()
    const { value } = e.target
    const recoveryKeyParts = [...this.state.recoveryKeyParts]
    recoveryKeyParts[part] = value.length <= RECOVERY_KEY_PART_LENGTH ? value.toUpperCase() : this.state.recoveryKeyParts[part]
    this.setState({ recoveryKeyParts })

    if (value.length === RECOVERY_KEY_PART_LENGTH && part < 3) {
      const inputElements = document.getElementsByClassName('recovery-key-input') as HTMLCollectionOf<HTMLInputElement>

      for (let i = 0; i < inputElements.length; i++) {
        if (i === part + 1) {
          inputElements[i].focus()
        }
      }
    }
  }

  checkField = (value: string) => {
    return value.length > RECOVERY_KEY_PART_LENGTH
  }

  render() {
    const { fetching } = this.props

    return (
      <div className='date-of-birth content cont-gray bottom-round'>
        <div className='left-cont'>
          <p>This account has been deleted.</p>
          <p className='m-top20'>
            A <span className='bold'>recovery key</span> was provided upon deletion, you will have received it by email.
            You are only authorised to recover this account if you were the initial creator and owner of it, otherwise
            you will be in breach of our rules and the account can be banned at any time in the future.
          </p>
        </div>
        <div className='right-cont'>
          <div className='progress-wrap'>
            <Progress progress={0} />
          </div>
          <div className='main-cont'>
            <h5 className='title'>Please enter recovery key</h5>
            <form onSubmit={this.handleSubmit}>
              <div className='input-group'>
                <Field
                  type='text'
                  name='recoveryKey1'
                  id='recoveryKey1'
                  value={this.state.recoveryKeyParts[0] || ''}
                  className='recovery-key-input'
                  classNameWrap='input-recovery-key-wrap'
                  placeholder=''
                  onChange={e => this.handleInputChange(e, 0)}
                  checkField={this.checkField}
                />
                <Field
                  type='text'
                  name='recoveryKey2'
                  id='recoveryKey2'
                  value={this.state.recoveryKeyParts[1] || ''}
                  className='recovery-key-input'
                  classNameWrap='input-recovery-key-wrap'
                  placeholder=''
                  onChange={e => this.handleInputChange(e, 1)}
                  checkField={this.checkField}
                />
                <Field
                  type='text'
                  name='recoveryKey3'
                  id='recoveryKey3'
                  value={this.state.recoveryKeyParts[2] || ''}
                  className='recovery-key-input'
                  classNameWrap='input-recovery-key-wrap'
                  placeholder=''
                  onChange={e => this.handleInputChange(e, 2)}
                  checkField={this.checkField}
                />
                <Field
                  type='text'
                  name='recoveryKey4'
                  id='recoveryKey4'
                  value={this.state.recoveryKeyParts[3] || ''}
                  className='recovery-key-input'
                  classNameWrap='input-recovery-key-wrap'
                  placeholder=''
                  onChange={e => this.handleInputChange(e, 3)}
                  checkField={this.checkField}
                />
              </div>
              <div className='buttons'>
                <TornBtn value='NEXT' loading={fetching} />
                <BackLink onClick={this.props.decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    fetching: state.accountrecovery.fetching
  }),
  {
    decrementProgress,
    recoverDeletedAccount
  }
)(RecoverDeletedAccount)
