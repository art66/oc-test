import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Autocomplete from '@torn/shared/components/Autocomplete'
import Progress from '../Progress'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

/* eslint-disable react/no-unescaped-entities */
class SearchForAccount extends Component {
  constructor(props) {
    super(props)
    this.props.getPotentionalUsers()
    this.state = {
      autocompleteUsers: [],
      fetchingAutocomplete: false
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.saveAccount()
  }

  pastePlayerInInput = player => {
    this.props.updateFormData('searchAccount', player)
    this.props.searchForAccount(player)
  }

  loadAutocompleteData = query => {
    const action = `${window.location.protocol}//${window.location.hostname}/autocompleteUserAjaxAction.php`

    this.setState({
      fetchingAutocomplete: true
    })
    /* global $ */
    /* global addRFC */
    $.ajax({
      url: addRFC(action),
      dataType: 'json',
      data: { q: query, option: 'ac-all' },
      success: data => {
        if (!data) {
          return
        }

        this.setState({
          autocompleteUsers: data,
          fetchingAutocomplete: false
        })

        if (query !== this.props.searchAccount) {
          this.loadAutocompleteData(this.props.searchAccount)
        }
      }
    })
  }

  handleAutocompleteInputChange = e => {
    this.props.handleInputChange(e)

    if (!this.state.fetchingAutocomplete) {
      this.loadAutocompleteData(e.target.value)
    }

    this.props.searchForAccount(e.target.value)
  }

  handleAutocompleteSelect = selected => {
    this.props.updateFormData('searchAccount', `${selected.name} [${selected.id}]`)
    this.props.searchForAccount(`${selected.name} [${selected.id}]`)
  }

  buildPotentionalUsersList = () => {
    const users = this.props.potentionalUsers

    if (!users) {
      return
    }

    return users.map((user, index) => (
      <span key={index} className='potentional-user'>
        <a
          href={`/profiles.php?XID=${user.userID}`}
          className='nav-link'
          onClick={e => {
            e.preventDefault()
            this.pastePlayerInInput(e.target.innerHTML)
          }}
        >
          {`${user.playername} [${user.userID}]`}
        </a>
        {index !== users.length - 1 ? ', ' : ''}
      </span>
    ))
  }

  render() {
    const { progress, account, searchAccount } = this.props
    const potentionalUsersList = this.buildPotentionalUsersList()

    return (
      <div className='search-for-account content cont-gray bottom-round'>
        <div className='left-cont'>
          <p className='text-block'>
            This process will verify your identity and change the email address associated with an account and/or reset
            its additional security features. If you have forgotten your password, you can simply use the “Forgot
            Password” tool to reset it. If you have forgotten both your email address and your password, you can reset
            your email address using this tool and then request a password reset afterwards.
          </p>
          <p className='text-block text-red'>
            Email changing / security reset attempts are limited, so ensure you fill out all of your information to the
            best of your ability on the first attempt. After changing your email, you will not be able to do so again
            for one month.
          </p>
        </div>
        <div className='right-cont'>
          <div className='progress-wrap'>
            <Progress progress={progress} />
          </div>
          <div className='main-cont'>
            <p className='text-block'>
              If you still have access to the email address assigned to the account you're trying to recover, you can
              use the 'Reset Password' feature on the previous page.
            </p>
            <h5 className='title'>Search for the Torn account</h5>
            <p className='text-block'>
              Please enter the name or account ID of the account below. If you’re having trouble remembering the name or
              ID of your account, perhaps you can find references to it in old emails, or maybe someone you know has the
              account on their friends list?
              {potentionalUsersList && ' Alternatively, perhaps it is one of these: '}
              {potentionalUsersList}
              {potentionalUsersList && ' ?'}
            </p>
            <form onSubmit={this.handleSubmit}>
              <div className='input-group'>
                <Autocomplete
                  name='searchAccount'
                  value={searchAccount}
                  inputClassName='input-text'
                  autoFocus='true'
                  list={this.state.autocompleteUsers}
                  targets='users'
                  onInput={this.handleAutocompleteInputChange}
                  onSelect={this.handleAutocompleteSelect}
                />
              </div>
              <div className='account-info-wrap'>
                {account && account.userID && (
                  <ul className='account-info'>
                    <li>Level: {account.level}</li>
                    <li>Account age: {account.age} days</li>
                    <li>Last online: {account.lastOnline} days ago</li>
                  </ul>
                )}
              </div>
              <div className='buttons'>
                <TornBtn value='NEXT' loading={this.props.fetching} disabled={!account.userID} />
                <BackLink onClick={this.props.decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

SearchForAccount.propTypes = {
  searchAccount: PropTypes.string,
  potentionalUsers: PropTypes.array,
  progress: PropTypes.number.isRequired,
  account: PropTypes.object.isRequired,
  saveAccount: PropTypes.func.isRequired,
  searchForAccount: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  fetching: PropTypes.bool,
  getPotentionalUsers: PropTypes.func,
  updateFormData: PropTypes.func,
  decrementProgress: PropTypes.func
}

export default SearchForAccount
