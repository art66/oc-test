import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from '../Progress'
import Info from '../Info'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

class SecretAnswer extends Component {
  constructor(props) {
    super(props)
    if (!props.secretQuestion) {
      this.props.loadSecretQuestion()
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.saveSecretAnswer()
  }

  render() {
    const {
      progress,
      secretAnswer,
      account,
      newEmail,
      secretQuestion,
      handleInputChange,
      decrementProgress
    } = this.props

    return (
      <div className="secret-answer content cont-gray bottom-round">
        <div className="left-cont">
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className="right-cont">
          <div className="progress-wrap">
            <Progress progress={progress} />
          </div>
          <div className="main-cont">
            <h5 className="title">Secret question</h5>
            <p className="text-block">If you don't remember setting a secret question, you can leave this blank.</p>
            <form onSubmit={this.handleSubmit}>
              <div className="input-group">
                <div className="input-wrap">
                  <label htmlFor="secretAnswerInput">{secretQuestion}:</label>
                  <input
                    type="text"
                    id="secretAnswerInput"
                    className="input-text"
                    name="secretAnswer"
                    value={secretAnswer || ''}
                    onChange={handleInputChange}
                  />
                </div>
              </div>
              <div className="buttons">
                <TornBtn value="NEXT" loading={this.props.fetching} />
                <BackLink onClick={decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

SecretAnswer.propTypes = {
  secretAnswer: PropTypes.string,
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  decrementProgress: PropTypes.func.isRequired,
  saveField: PropTypes.func.isRequired,
  secretQuestion: PropTypes.string,
  fetching: PropTypes.bool,
  handleInputChange: PropTypes.func.isRequired,
  loadSecretQuestion: PropTypes.func.isRequired,
  saveSecretAnswer: PropTypes.func.isRequired
}

export default SecretAnswer
