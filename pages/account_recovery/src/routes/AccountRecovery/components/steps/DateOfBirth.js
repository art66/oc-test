import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Dropdown from '@torn/shared/components/Dropdown'
import Progress from '../Progress'
import Info from '../Info'
import Field from '../Field'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

const MONTHS = [
  { id: '01', name: 'January' },
  { id: '02', name: 'February' },
  { id: '03', name: 'March' },
  { id: '04', name: 'April' },
  { id: '05', name: 'May' },
  { id: '06', name: 'June' },
  { id: '07', name: 'July' },
  { id: '08', name: 'August' },
  { id: '09', name: 'September' },
  { id: '10', name: 'October' },
  { id: '11', name: 'November' },
  { id: '12', name: 'December' }
]

class DateOfBirth extends Component {
  handleSubmit = e => {
    e.preventDefault()
    this.props.saveDateOfBirth()
  }

  handleSelectChange = (data) => {
    this.props.updateFormData('dateOfBirthMonth', data.id)
  }

  render() {
    const {
      progress,
      dateOfBirthDay,
      dateOfBirthMonth,
      dateOfBirthYear,
      account,
      newEmail,
      decrementProgress,
      handleInputChange,
      checkField,
      unsetIncorrectFields
    } = this.props

    return (
      <div className='date-of-birth content cont-gray bottom-round'>
        <div className='left-cont'>
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className='right-cont'>
          <div className='progress-wrap'>
            <Progress progress={progress} />
          </div>
          <div className='main-cont'>
            <h5 className='title'>Date of birth</h5>
            <p className='text-block'>If you don't remember setting your date of birth, you can leave this blank.</p>
            <form onSubmit={this.handleSubmit}>
              <div className='input-group'>
                <Field
                  type='tel'
                  name='dateOfBirthDay'
                  id='dateOfBirthDay'
                  value={dateOfBirthDay || ''}
                  classNameWrap='input-day-wrap'
                  placeholder='dd'
                  onChange={handleInputChange}
                  checkField={checkField}
                  unsetIncorrectFields={unsetIncorrectFields}
                />
                <div className='input-wrap i-block'>
                  <Dropdown
                    name='dateOfBirthMonth'
                    id='dateOfBirthMonth'
                    placeholder='Month'
                    selected={MONTHS.find(item => dateOfBirthMonth === item.id)}
                    list={MONTHS}
                    onChange={this.handleSelectChange}
                  />
                </div>
                <Field
                  type='tel'
                  name='dateOfBirthYear'
                  id='dateOfBirthYear'
                  value={dateOfBirthYear || ''}
                  classNameWrap='input-year-wrap'
                  placeholder='yyyy'
                  onChange={handleInputChange}
                  checkField={checkField}
                  unsetIncorrectFields={unsetIncorrectFields}
                />
              </div>
              <div className='buttons'>
                <TornBtn value='NEXT' loading={this.props.fetching} />
                <BackLink onClick={decrementProgress} />
                <CancelLink />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

DateOfBirth.propTypes = {
  dateOfBirthDay: PropTypes.string,
  dateOfBirthMonth: PropTypes.string,
  dateOfBirthYear: PropTypes.string,
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  decrementProgress: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  saveDateOfBirth: PropTypes.func.isRequired,
  checkField: PropTypes.func.isRequired,
  unsetIncorrectFields: PropTypes.func.isRequired,
  fetching: PropTypes.bool,
  updateFormData: PropTypes.func
}

export default DateOfBirth
