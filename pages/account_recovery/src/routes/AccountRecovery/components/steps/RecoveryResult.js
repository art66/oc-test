import PropTypes from 'prop-types'
import React, { Component } from 'react'

class RecoveryResult extends Component {
  handleSubmit = e => {
    e.preventDefault()
    this.props.completeRecovery()
  }

  getText = () => {
    const { success, account, newEmail } = this.props

    if (success) {
      return !account.isEmailSame
        ? `Thank you. The email address associated with ${account.playername}
                [${account.userID}] has been updated to ${newEmail}.
                You will have to validate this new email address by clicking the link
                we've just sent you. You will then be able to login, or request a password
                reset if required.`
        : `Thank you.  The security settings on the account ${account.playername} [${account.userID}] have
                been cleared. Once you have logged in, please enable two-factor authentication
                with a mobile phone number and and set your secret question & date of birth again.`
    } else {
      return `We're sorry, the information provided is not accurate enough to
              complete the email change process successfully.`
    }
  }

  render() {
    const { success } = this.props
    const color = success ? 'green' : 'red'

    return (
      <div>
        <div className={'info-msg-cont border-round m-top10 ' + color}>
          <div className="info-msg border-round">
            <i className="info-icon" />
            <div className="delimiter">
              <div className="msg right-round">{this.getText()}</div>
            </div>
          </div>
        </div>
        <hr className="page-head-delimiter m-top10 m-bottom10" />
      </div>
    )
  }
}

RecoveryResult.propTypes = {
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  completeRecovery: PropTypes.func.isRequired,
  success: PropTypes.oneOfType([PropTypes.number, PropTypes.bool])
}

export default RecoveryResult
