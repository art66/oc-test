import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from '../Progress'
import Info from '../Info'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

class IPValidation extends Component {
  // deprecated Since React v.16.5
  // UNSAFE_componentWillMount() {
  //   this.props.validateIP()
  // }

  componentDidMount() {
    this.props.validateIP()
  }

  render() {
    const { progress, ipAddress, decrementProgress, newEmail, account, incrementProgress } = this.props

    return (
      <div className="ip-validation content cont-gray bottom-round">
        <div className="left-cont">
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className="right-cont">
          <div className="progress-wrap">
            <Progress progress={progress} />
          </div>
          <div className="main-cont">
            <div
              className={
                'ip-validation-result ' +
                (ipAddress.fetching ? 'loading' : ipAddress.isValid ? 'validated' : 'unvalidated')
              }
            >
              <i className="ip-validation-icon" />
              <span className="text">
                {ipAddress.fetching
                  ? 'Checking IP address...'
                  : !ipAddress.isValid
                    ? `We recommend that you try to recover your account from a computer / IP address that you’ve
                      frequently accessed your Torn account on in the past. The IP address you’re currently on has
                      NOT frequently accessed this account in the past, so the likelihood of account recovery is
                      decreased.`
                    : `The IP address you’re currently on has frequently accessed this account in the past.
                      The likelihood of account recovery is increased.`}
              </span>
            </div>
            <div className="buttons">
              <TornBtn value="NEXT" onClick={incrementProgress} />
              <BackLink onClick={decrementProgress} />
              <CancelLink />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

IPValidation.propTypes = {
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  ipAddress: PropTypes.object.isRequired,
  decrementProgress: PropTypes.func.isRequired,
  validateIP: PropTypes.func.isRequired,
  incrementProgress: PropTypes.func.isRequired
}

export default IPValidation
