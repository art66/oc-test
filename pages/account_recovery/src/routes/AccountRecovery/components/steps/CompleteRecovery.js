import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Info from '../Info'
import TornBtn from '../TornBtn'
import BackLink from '../BackLink'
import CancelLink from '../CancelLink'

class CompleteRecovery extends Component {
  handleSubmit = e => {
    e.preventDefault()
    this.props.completeRecovery()
  }

  render() {
    const { account, newEmail, decrementProgress } = this.props

    return (
      <div className="complete-recovery content cont-gray bottom-round">
        <div className="left-cont">
          <Info newEmail={newEmail} account={account} />
        </div>
        <div className="right-cont">
          <div className="main-cont">
            <div className="middle-block">
              <p className="text-block bold">
                Are you sure you have entered every available detail to the best of your knowledge?
              </p>
              <form onSubmit={this.handleSubmit}>
                <div className="buttons">
                  <TornBtn value="COMPLETE VERIFICATION" loading={this.props.fetching} />
                  <BackLink onClick={decrementProgress} />
                  <CancelLink />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CompleteRecovery.propTypes = {
  progress: PropTypes.number.isRequired,
  newEmail: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
  decrementProgress: PropTypes.func.isRequired,
  completeRecovery: PropTypes.func.isRequired
}

export default CompleteRecovery
