import PropTypes from 'prop-types'
import React, { Component } from 'react'

class Progress extends Component {
  getProgressContent(progress) {
    let progressValues = []

    for (let i = 1; i < 10; i++) {
      progressValues.push(
        <li key={i} className={i <= progress ? 'completed' : ''}>
          <span className="value">{i}</span>
        </li>
      )
    }

    return progressValues
  }

  render() {
    const { progress } = this.props
    const content = this.getProgressContent(progress - 2)

    return <ul className="progress">{content}</ul>
  }
}

Progress.propTypes = {
  progress: PropTypes.number.isRequired
}

export default Progress
