import PropTypes from 'prop-types'
import React, { Component } from 'react'

class BackLink extends Component {
  handleClick = e => {
    e.preventDefault()
    this.props.onClick()
  }

  render() {
    return (
      <a href="#" className={'nav-link back ' + (this.props.className || '')} onClick={this.handleClick}>
        Back
      </a>
    )
  }
}

BackLink.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  loading: PropTypes.bool,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func
}

export default BackLink
