import PropTypes from 'prop-types'
import React, { Component } from 'react'

class Title extends Component {
  getTitle() {
    const { accountDeleted, progress, recoveryCompleted } = this.props

    if (accountDeleted && progress > 4 && !recoveryCompleted) {
      return 'Enter recovery key'
    }

    const TITLES = {
      0: 'Account Recovery',
      1: 'Account Recovery',
      2: 'Account Recovery',
      3: 'Search for the account',
      4: 'Enter new Email Address',
      5: 'IP validation',
      6: 'Email addresses associated with account',
      7: 'Passwords associated with account',
      8: 'Geographical location of signup',
      9: 'Donation order number',
      10: 'Secret question answer',
      11: 'Date of birth',
      12: 'Date of birth',
      13: null
    }

    return TITLES[progress]
  }

  render() {
    const title = this.getTitle()

    if (!title) {
      return <div />
    }

    return <div className="title-black top-round">{title}</div>
  }
}

Title.propTypes = {
  progress: PropTypes.number.isRequired,
  accountDeleted: PropTypes.bool,
  recoveryCompleted: PropTypes.bool
}

export default Title
