import PropTypes from 'prop-types'
import React, { Component } from 'react'

class TornBtn extends Component {
  render() {
    const { value, type, disabled, className, loading, onClick } = this.props

    return (
      <button
        type={type || 'submit'}
        className={'torn-btn ' + (className || '') + (loading ? 'loading ' : '') + (disabled ? 'disabled' : '')}
        onClick={onClick}
      >
        {value}
        <div className="preloader-wrap">
          <div className="dzsulb-preloader preloader-fountain">
            <div id="fountainG_1" className="fountainG" />
            <div id="fountainG_2" className="fountainG" />
            <div id="fountainG_3" className="fountainG" />
            <div id="fountainG_4" className="fountainG" />
          </div>
        </div>
      </button>
    )
  }
}

TornBtn.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  loading: PropTypes.bool,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func
}

export default TornBtn
