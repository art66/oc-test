import PropTypes from 'prop-types'
import React, { Component } from 'react'

class Info extends Component {
  render() {
    const { account, newEmail } = this.props

    return (
      <div className="">
        <p className="text-block">
          We’ll now need to ask some questions to confirm that you are the owner of the account
          <a href="{account.link}" className="nav-link">
            {' '}
            {account.playername} [{account.userID}]{' '}
          </a>
          in order to
          {account.isEmailSame
            ? ' reset your additional security features.'
            : ` change the email address associated with it to `}
          {!account.isEmailSame && <span className="bold">{newEmail}.</span>}
        </p>
        <p className="text-block">
          Please complete as many of these sections as you can. On some questions, the answers don’t need to be perfect
          - just try to be as accurate as you can. The fields can be left blank if required. Identity confirmation
          attempts are limited, so it is recommended you try to answer as many as possible, as accurately as possible.
        </p>
        <p className="text-block">
          If you’re completing this verification process on a computer / IP address which has successfully logged in to
          this account on many occasions in the past, the chances of a successful verification are increased.
        </p>
      </div>
    )
  }
}

Info.propTypes = {
  account: PropTypes.object,
  newEmail: PropTypes.string.isRequired
}

export default Info
