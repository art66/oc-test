import PropTypes from 'prop-types'
import React, { Component } from 'react'
import BackLink from './BackLink'

class RecoveryFailed extends Component {
  getText = () => {
    const { recoveryFailed, unsetFailure } = this.props
    let text = <div>Account recovery is currently disabled for this account, please try again later.</div>

    switch (recoveryFailed) {
      case 'emailIncorrect':
        text = 'The email address you entered was incorrect'
        break
      case 'emailExists':
        text = 'The email address you entered already exists under a different account.'
        break
      case 'emailDisposable':
        text = 'Your email address has been rejected. We do not allow disposable email addresses.'
        break
      case 'recaptcha':
        text = 'Invalid captcha. Please submit a support ticket.'
        break
      default:
    }

    return (
      <div>
        <div>{text}</div>
        <BackLink className="i-block m-top10" onClick={unsetFailure} />
      </div>
    )
  }

  render() {
    return <div className="recovery-failed content cont-gray bottom-round">{this.getText()}</div>
  }
}

RecoveryFailed.propTypes = {
  recoveryFailed: PropTypes.string,
  unsetFailure: PropTypes.func
}

export default RecoveryFailed
