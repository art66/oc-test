import PropTypes from 'prop-types'
import React, { Component } from 'react'

class Field extends Component {
  render() {
    const {
      type,
      name,
      value,
      id,
      className,
      classNameWrap,
      placeholder,
      label,
      autoFocus,
      onChange,
      checkField
    } = this.props

    return (
      <div className={'input-wrap ' + classNameWrap + ' ' + (!checkField(name) ? 'incorrect' : '')}>
        {label && <label htmlFor={id}>{label}</label>}
        <input
          type={type || 'text'}
          name={name}
          value={value}
          id={id}
          className={'input-text ' + (className || '')}
          placeholder={placeholder || ''}
          onChange={onChange}
          autoFocus={autoFocus}
        />
      </div>
    )
  }
}

Field.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  id: PropTypes.string,
  className: PropTypes.string,
  classNameWrap: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  autoFocus: PropTypes.string,
  onChange: PropTypes.func,
  checkField: PropTypes.func
}

export default Field
