import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { fetchUrl } from '../../../utils'

class CancelLink extends Component {
  handleClick = e => {
    e.preventDefault()
    e.persist()
    fetchUrl('cancelRecovery').then(() => {
      window.location = e.target.href
    })
  }

  render() {
    return (
      <a href="/" className={'nav-link cancel ' + (this.props.className || '')} onClick={this.handleClick}>
        Cancel
      </a>
    )
  }
}

CancelLink.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func
}

export default CancelLink
