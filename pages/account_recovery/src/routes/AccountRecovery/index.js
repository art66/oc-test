import { injectReducer } from '../../store/reducers'

export default store => ({
  // path : 'accountrecovery',
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure(
      [],
      require => {
        /*  Webpack - use require callback to define
          dependencies for bundling   */
        const AccountRecovery = require('./containers/AccountRecoveryContainer').default
        const reducer = require('./modules/accountrecovery').default

        /*  Add the reducer to the store on key 'accountrecovery'  */
        injectReducer(store, { key: 'accountrecovery', reducer })

        /*  Return getComponent   */
        cb(null, AccountRecovery)

        /* Webpack named bundle   */
      },
      'accountrecovery'
    )
  }
})
