import React, { Component } from 'react'
import { connect } from 'react-redux'
import AppHeader from '@torn/shared/components/AppHeader'
import { userIsHospital, userIsJail, userIsTraveling } from '../../utils'

interface IProps {
  children: any
  location: string
  isLoggedIn: boolean
}

class CoreLayout extends Component<IProps> {
  renderTitle = () => {
    const { isLoggedIn } = this.props

    const chroniclesLinksRegular = [
      {
        id: '1',
        title: 'Newspaper',
        href: `${window.location.origin}/newspaper.php#!/`,
        icon: 'NewsTicker',
        label: 'Newspaper-link'
      },
      {
        id: '2',
        title: 'City',
        href: `${window.location.origin}/city.php`,
        icon: 'City',
        label: 'City-link'
      }
    ]

    const chroniclesLinksHospitalJailTraveling = [
      {
        id: '1',
        title: 'Newspaper',
        href: `${window.location.origin}/newspaper.php#!/`,
        icon: 'NewsTicker',
        label: 'Newspaper-link'
      }
    ]

    const chroniclesLinks = userIsHospital() || userIsJail() || userIsTraveling() ?
      chroniclesLinksHospitalJailTraveling :
      chroniclesLinksRegular

    const headerProps = {
      titles: {
        default: {
          ID: 0,
          title: location.hash === '#/' ? 'Chronicle Archives' : ''
        }
      },
      links: {
        default: {
          ID: 0,
          items: location.hash === '#/' ?
            chroniclesLinks :
            [
              {
                id: '0',
                title: 'Back to Chronicle Archives',
                href: '/',
                icon: 'Back',
                label: 'Back-link',
                isSPALink: true
              }
            ]
        },
        list: []
      }
    }

    return isLoggedIn && <AppHeader clientProps={headerProps} appID='0' />
  }

  renderBody = () => {
    const { children } = this.props

    return (
      <div className='core-layout__viewport'>
        {children}
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.renderTitle()}
        {this.renderBody()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  location: state.chronicles.location,
  isLoggedIn: state.chronicles.isLoggedIn
})

export default connect(mapStateToProps, null)(CoreLayout)
