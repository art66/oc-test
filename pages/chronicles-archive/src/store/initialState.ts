import IChronicle from '../interfaces/IChronicle'
import { UNREAD_STATUS } from '../routes/Chronicles/constants'
import { userIsLoggedOut } from '../utils'

interface IInitialState {
  chronicles: IChronicle[]
  isLoggedIn: boolean
  location: string
}

const initialState: IInitialState = {
  chronicles: [
    {
      id: 1,
      slug: '/',
      status: UNREAD_STATUS,
      title: 'Founding'
    },
    {
      id: 2,
      slug: '/',
      status: UNREAD_STATUS,
      title: 'Factions'
    },
    {
      id: 3,
      slug: '/',
      status: UNREAD_STATUS,
      title: 'Employment'
    },
    {
      id: 4,
      slug: '/',
      status: UNREAD_STATUS,
      title: 'Markets'
    },
    {
      id: 5,
      slug: '/',
      status: UNREAD_STATUS,
      title: 'Real Estate'
    }
  ],
  location: '#/',
  isLoggedIn: !userIsLoggedOut()
}

export default initialState
