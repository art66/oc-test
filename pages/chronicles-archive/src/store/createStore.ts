import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import makeRootReducer from './rootReducer'
import rootSaga from './rootSaga'
import history from './history'

const sagaMiddleware = createSagaMiddleware()

const rootStore = (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [routerMiddleware(history), thunk, sagaMiddleware]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  if (__DEV__) {
    const { __REDUX_DEVTOOLS_EXTENSION__ } = window

    if (__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    // @ts-ignore
    makeRootReducer(),
    initialState,
    compose(
      responsiveStoreEnhancer,
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  sagaMiddleware.run(rootSaga)
  // @ts-ignore
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      // @ts-ignore
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }

  return store
}

export default rootStore()
