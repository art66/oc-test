import { Store, combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import { connectRouter } from 'connected-react-router'
import chronicles from '../routes/Chronicles/reducers'
import history from './history'

interface IStore {
  asyncReducers?: any
}

interface IProps {
  key: string
  reducer: any
}

export const makeRootReducer = (asyncReducers: object) => {
  return combineReducers({
    ...asyncReducers,
    chronicles,
    router: connectRouter(history),
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    })
  })
}

export const injectReducer = (store: IStore & Store<any>, { key, reducer }: IProps) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return
  store.asyncReducers[key] = reducer

  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
