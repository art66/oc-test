import { all } from 'redux-saga/effects'
import chroniclesSaga from '../routes/Chronicles/sagas'

export default function* rootSaga() {
  yield all([chroniclesSaga()])
}
