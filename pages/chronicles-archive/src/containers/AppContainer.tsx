import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import CoreLayout from '../layouts/CoreLayout'
import { Chronicles, TheEmployment, TheBirthOfTorn, TheFactions, TheMarkets, TheRealEstate } from '../routes'

interface IProps {
  store: any
  history: any
}

class AppContainer extends Component<IProps> {
  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <CoreLayout>
          <Switch>
            <Route
              path='/'
              component={Chronicles}
              exact={true}
            />
            <Route path='/Employment' component={TheEmployment} />
            <Route path='/TheBirthOfTorn' component={TheBirthOfTorn} />
            <Route path='/Factions' component={TheFactions} />
            <Route path='/TheMarkets' component={TheMarkets} />
            <Route path='/RealEstate' component={TheRealEstate} />
          </Switch>
        </CoreLayout>
      </Provider>
    )
  }
}

export default AppContainer
