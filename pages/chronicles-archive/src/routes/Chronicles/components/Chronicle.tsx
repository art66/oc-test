import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { SVGIconGenerator } from '@torn/shared/SVG'
import chroniclesIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/chroniclesArchive'
import cn from 'classnames'
import IChronicle from '../../../interfaces/IChronicle'
import {
  COUNTS_OF_ICONS,
  READ_STATUS,
  UNREAD_STATUS,
  ICON_PRESET_STATUS_CHRONICLE,
  ICON_SUBTYPE_STATUS_CHRONICLE
} from '../constants'

import s from './styles.cssmodule.scss'

interface IProps {
  chronicle: IChronicle
  inactive: boolean
}

class Chronicle extends Component<IProps> {
  getAriaLabel = () => {
    const { chronicle: { title, status } } = this.props

    return `Chronicle name: ${title}. Status: ${status}`
  }

  getChronicleHref = () => {
    const { chronicle, inactive } = this.props
    const url = `/${chronicle.slug}`

    return inactive ? '#' : url
  }

  renderReadIcon = () => {
    const { chronicle } = this.props
    const isRead = chronicle.status === READ_STATUS
    const randomIcon = Math.floor(Math.random() * COUNTS_OF_ICONS) + 1

    return (
      isRead && (
        <SVGIconGenerator
          iconsHolder={chroniclesIconsHolder}
          customClass={s.statusIcon}
          iconName={`ReadStatus${randomIcon}`}
          preset={{ type: ICON_PRESET_STATUS_CHRONICLE, subtype: ICON_SUBTYPE_STATUS_CHRONICLE }}
        />
      )
    )
  }

  render() {
    const { chronicle, inactive } = this.props
    const chronicleStyles = cn({
      [s.chronicle]: true,
      [s.inactive]: inactive
    })
    const chronicleTitleStyles = cn({
      [s.chronicleTitle]: true,
      [s.unread]: chronicle.status === UNREAD_STATUS,
      [s.read]: chronicle.status === READ_STATUS
    })
    const papersChronicleStyles = cn({
      [s.papersChronicle]: true,
      [s[chronicle.slug]]: chronicle.slug !== ''
    })

    return (
      <li key={chronicle.id} className={`${chronicleStyles}`}>
        <Link
          to={this.getChronicleHref()}
          className={s.chronicleLink}
          aria-label={this.getAriaLabel()}
          tabIndex={inactive ? -1 : 0}
        >
          <div className={`${papersChronicleStyles}`} />
          <div className={s.chronicleBox}>
            <div className={`${chronicleTitleStyles}`}>
              <span>{chronicle.title}</span>
              {this.renderReadIcon()}
            </div>
            <div className={s.drawerHandle} />
          </div>
        </Link>
      </li>
    )
  }
}

export default Chronicle
