import React, { Component } from 'react'
import IChronicle from '../../../interfaces/IChronicle'
import Chronicle from './Chronicle'
import {
  NUMBER_OF_CHRONICLES,
  CHRONICLES_IN_ONE_ROW_DESKTOP,
  CHRONICLES_IN_ONE_ROW_TABLET,
  INACTIVE_CHRONICLE
} from '../constants'
import './vars.scss'
import './DM_vars.scss'
import s from './styles.cssmodule.scss'

interface IProps {
  chronicles: IChronicle[]
  fetchChronicles: () => void
  changeLocation: (x: string) => void
  browser: {
    mediaType: string
  }
}

interface IState {
  chronicles: IChronicle[]
}

class Chronicles extends Component<IProps, IState> {
  componentDidMount() {
    const { fetchChronicles, changeLocation } = this.props

    fetchChronicles()
    changeLocation(window.location.hash)
  }

  generateInactiveChronicles = () => {
    const { chronicles } = this.props

    let { browser: { mediaType } } = this.props

    mediaType = document.body.classList.contains('r') ? mediaType : 'desktop'

    const isDesktop = mediaType === 'desktop'

    const chroniclesInRow = isDesktop ? CHRONICLES_IN_ONE_ROW_DESKTOP : CHRONICLES_IN_ONE_ROW_TABLET
    const chroniclesRow = chronicles.length % chroniclesInRow === 0
    let numberInactiveChronicles = 0

    if (isDesktop && !chroniclesRow) {
      numberInactiveChronicles = NUMBER_OF_CHRONICLES - chronicles.length
    } else if (!isDesktop && !chroniclesRow) {
      numberInactiveChronicles = 1
    }

    const inactiveChronicles = []

    for (let i = 0; i < numberInactiveChronicles; i++) {
      inactiveChronicles.push({
        ...INACTIVE_CHRONICLE,
        id: chronicles.length + 1 + i
      })
    }

    return chronicles.concat(inactiveChronicles)
  }

  renderChronicles = () => {
    const chroniclesList = this.generateInactiveChronicles()

    return chroniclesList.map(item => <Chronicle key={item.id} chronicle={item} inactive={item.slug === ''} />)
  }

  render() {
    return (
      <div className={s.chronicles}>
        <div className={s.topPage} />
        <div className={s.content}>
          <ul className={s.chronicleList}>
            {this.renderChronicles()}
          </ul>
        </div>
        <div className={s.bottomPage} />
      </div>
    )
  }
}

export default Chronicles
