import { changeReadStatus } from '../actions'
import ITornWindow from '../../../interfaces/ITornWindow'

function initWS(dispatch: any) {
  const win = window as ITornWindow
  // @ts-ignore
  const handler = new win.WebsocketHandler('chroniclesHub')

  handler.setActions({
    updateStatus: (data: any) => {
      dispatch(changeReadStatus(data))
    }
  })
}

export default initWS
