import { createAction } from 'redux-actions'
import { SET_CHRONICLES_DATA, CHANGE_READ_STATUS, FETCH_CHRONICLES_DATA, CHANGE_LOCATION } from './actionsTypes'

export const fetchChronicles = createAction(FETCH_CHRONICLES_DATA)
export const setChroniclesData = createAction(SET_CHRONICLES_DATA, data => data)
export const changeReadStatus = createAction(CHANGE_READ_STATUS, (id, status) => ({ id, status }))
export const changeLocation = createAction(CHANGE_LOCATION, location => location)
