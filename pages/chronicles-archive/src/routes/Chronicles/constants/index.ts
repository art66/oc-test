export const NUMBER_OF_CHRONICLES = 10
export const CHRONICLES_IN_ONE_ROW_DESKTOP = 5
export const CHRONICLES_IN_ONE_ROW_TABLET = 2
export const COUNTS_OF_ICONS = 2
export const READ_STATUS = 'read'
export const UNREAD_STATUS = 'unread'
export const TIME_IN_SECONDS = 30
export const INACTIVE_CHRONICLE = {
  id: 0,
  slug: '',
  status: UNREAD_STATUS,
  title: ''
}

export const ICON_PRESET_STATUS_CHRONICLE = 'chroniclesArchive'
export const ICON_SUBTYPE_STATUS_CHRONICLE = 'DEFAULT_PROPERTIES'

export const THE_BIRTH_OF_TORN = 'TheBirthOfTorn'
export const THE_EMPLOYMENT = 'Employment'
export const THE_MARKETS = 'TheMarkets'
export const THE_FACTIONS = 'Factions'
export const REAL_ESTATE = 'RealEstate'

export const DARK_THEME = 'dark'

export const THE_BIRTH_OF_TORN_PATH_IMAGES = '/images/v2/chronicles_archives/chronicles/the_birth_of_torn'
export const THE_EMPLOYMENT_PATH_IMAGES = '/images/v2/chronicles_archives/chronicles/employment'
export const THE_MARKETS_PATH_IMAGES = '/images/v2/chronicles_archives/chronicles/the_markets'
export const THE_FACTIONS_PATH_IMAGES = '/images/v2/chronicles_archives/chronicles/factions'
export const REAL_ESTATE_PATH_IMAGES = '/images/v2/chronicles_archives/chronicles/real_estate'
