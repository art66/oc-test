import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'
import { injectReducer } from '../../store/rootReducer'
import reducer from './reducers'
import rootStore from '../../store/createStore'
import initWS from './handlers/websockets'

const PreloaderComponent = () => <AnimationLoad />

const ChroniclesRoute = Loadable({
  loader: async () => {
    const Chronicles = await import('./containers')

    return Chronicles
  },
  render(asyncComponent: any) {
    const { default: Chronicles } = asyncComponent

    initWS(rootStore.dispatch)
    injectReducer(rootStore, { reducer, key: 'chronicles' })

    return <Chronicles />
  },
  loading: PreloaderComponent
})

export default ChroniclesRoute
