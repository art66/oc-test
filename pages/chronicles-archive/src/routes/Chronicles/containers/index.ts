import { connect } from 'react-redux'
import Chronicles from '../components/Chronicles'
import { fetchChronicles, changeLocation } from '../actions'

const mapDispatchToProps = {
  fetchChronicles,
  changeLocation
}

const mapStateToProps = state => ({
  chronicles: state.chronicles.chronicles,
  location: state.chronicles.location,
  browser: state.browser
})

export default connect(mapStateToProps, mapDispatchToProps)(Chronicles)
