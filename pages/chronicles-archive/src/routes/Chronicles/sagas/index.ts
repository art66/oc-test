import { put, takeEvery } from 'redux-saga/effects'
import { fetchUrl } from '../../../utils'
import { FETCH_CHRONICLES_DATA } from '../actions/actionsTypes'
import { setChroniclesData } from '../actions'

function* fetchChroniclesData() {
  try {
    const data = yield fetchUrl('/archives.php?step=getChronicles')

    yield put(setChroniclesData(data))
  } catch (e) {
    console.log(e)
  }
}

export default function* chroniclesSaga() {
  yield takeEvery(FETCH_CHRONICLES_DATA, fetchChroniclesData)
}
