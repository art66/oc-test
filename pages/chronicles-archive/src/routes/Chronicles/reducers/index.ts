import initialState from '../../../store/initialState'
import { CHANGE_READ_STATUS, SET_CHRONICLES_DATA, CHANGE_LOCATION } from '../actions/actionsTypes'

const ACTION_HANDLERS = {
  [SET_CHRONICLES_DATA]: (state, { payload }) => {
    return {
      ...state,
      chronicles: payload.chronicles,
      isLoggedIn: payload.isLoggedIn
    }
  },
  [CHANGE_READ_STATUS]: (state, { payload: { id, status } }) => {
    return {
      ...state,
      chronicles: state.chronicles.map(item => ({
        ...item,
        status: item.id === id ? status : item.status
      }))
    }
  },
  [CHANGE_LOCATION]: (state, { payload }) => {
    return {
      ...state,
      location: payload
    }
  }
}

const reducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
