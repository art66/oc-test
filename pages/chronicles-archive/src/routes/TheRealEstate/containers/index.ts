import { connect } from 'react-redux'
import TheRealEstate from '../components/TheRealEstate'
import { changeLocation, fetchChronicles } from '../../Chronicles/actions'
import { REAL_ESTATE } from '../../Chronicles/constants'

const mapDispatchToProps = {
  changeLocation,
  fetchChronicles
}

const mapStateToProps = state => ({
  chronicle: state.chronicles.chronicles.find(item => {
    return item.slug === REAL_ESTATE
  }),
  browser: state.browser,
  isLoggedIn: state.chronicles.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(TheRealEstate)
