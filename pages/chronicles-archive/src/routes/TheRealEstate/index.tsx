import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

const PreloaderComponent = () => <AnimationLoad />

const TheRealEstateRoute = Loadable({
  loader: async () => {
    const TheRealEstate = await import('./containers')

    return TheRealEstate
  },
  render(asyncComponent: any) {
    const { default: TheRealEstate } = asyncComponent

    return <TheRealEstate />
  },
  loading: PreloaderComponent
})

export default TheRealEstateRoute
