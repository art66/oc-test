import React, { Component } from 'react'
import cn from 'classnames'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import { fetchUrl } from '../../../utils'
import IChronicle from '../../../interfaces/IChronicle'
import { DARK_THEME, TIME_IN_SECONDS, REAL_ESTATE_PATH_IMAGES } from '../../Chronicles/constants'
import '../../Chronicles/components/vars.scss'
import '../../Chronicles/components/DM_vars.scss'
import s from './styles.cssmodule.scss'

interface IProps extends TWithThemeInjectedProps {
  changeLocation: (x: string) => void
  fetchChronicles: () => void
  chronicle: IChronicle
  browser: {
    mediaType: string
  }
  isLoggedIn: boolean
}

class TheRealEstate extends Component<IProps> {
  startReadTime: number
  timer: number

  constructor(props: any) {
    super(props)
    const { changeLocation, fetchChronicles } = this.props

    fetchChronicles()
    changeLocation(window.location.hash)
  }

  componentDidUpdate() {
    const { chronicle } = this.props

    if (chronicle) {
      this.startTimer()
    }
  }

  componentWillUnmount() {
    this.clearTimer()
  }

  getImage = (name: string, styleClass: string) => {
    let {
      browser: { mediaType }
    } = this.props
    const isDarkMode = this.props.theme === DARK_THEME
    const darkImageFolder = isDarkMode ? '/dark_mode' : ''

    mediaType = document.body.classList.contains('r') ? mediaType : 'desktop'

    return (
      <img
        className={cn(s.articleImage, s[styleClass])}
        src={`${REAL_ESTATE_PATH_IMAGES}${darkImageFolder}/${mediaType}/images/${name}_@1x.png`}
      />
    )
  }

  clearTimer = () => {
    clearInterval(this.timer)
  }

  startTimer = () => {
    const {
      chronicle: { id },
      isLoggedIn
    } = this.props

    this.clearTimer()

    if (isLoggedIn) {
      this.timer = window.setInterval(() => {
        fetchUrl('/archives.php?step=updateSpentTime', {
          chronicleId: id,
          timestamp: TIME_IN_SECONDS
        })
      }, TIME_IN_SECONDS * 1000)
    }
  }

  render() {
    return (
      <article className={s.chronicleRealEstate}>
        <div className={cn(s.page, s.part1)}>
          <div className={cn(s.articleBlock, s.mainBackground, s.pdBottom45)}>
            <div className={s.headerEffect} />
            <div className={s.headerShadowEffect} />
            <h5 className={s.header}>Unreal Estate</h5>
            <div className={cn(s.titleDivider)} />
            {this.getImage('real_estate', 'realEstate')}
            <div className={cn(s.container)}>
              <p className={s.mgTop5}>
                The biggest post-war threat to the security of the United States was not communism, but hedonism.
                Ordinary Americans became a regular visitor to bordellos and breweries now that their time on the front
                line was over, and when the local offerings failed to satiate their increasingly depraved desires, Torn
                City was often their first port of <span className={cn(s.typoPoint)}>call</span>
              </p>
              <div className={s.paperEffect1} />
              <p>
                The booming American economy of the 1950s meant that your average Joe and Jane had money burning a hole
                in their pocket. This was soon replaced with a burning sensation in and around the groin, as many
                elected to spend their newfound wealth on drugs in the morning and ladies of the evening. Of course, the
                best of both were on offer in Torn, and vice tourism swole our city&#39;s population massively towards
                the end of the decade. Many of these visitors elected to stay in the city afterwards; not because they
                were enamoured by the place, but because they were too drunk, broke or paralyzed to make it to the
                airport.
              </p>
            </div>
            <div className={cn(s.container, s.paragraphEffect1)}>
              <p>
                In response, Torn&#39;s elected representatives purchased vast swathes of land to house its
                ever-increasing population. This rapid growth frightened the American Government. If Torn were to swell
                to the size of Dallas, Houston or, God forbid, New York, there would be no hope of controlling
                America&#39;s most delinquent territory. Intimidating your bratty little brother is a piece of cake when
                he&#39;s a toddler. But, when that same kid is eighteen years old, three hundred pounds, and an hour
                overdue his morphine fix, keeping the big bastard in line is easier said than
                <span className={s.typoDone}>done.</span>
              </p>
            </div>
            <div className={cn(s.container, s.toCurtailParagraphr)}>
              <p>
                To curtail Torn&#39;s influence its federal budget was slashed, with the aim being to starve out the
                city and prevent it from becoming a <span className={s.noWrap}>real-life</span> Sodom and Gomorrah -
                with twice the sodomy. Congress hoped that Torn&#39;s authorities would be forced to abandon their
                ambitious real estate plans to pay for law enforcement and medical services, thus halting its moral
                decline, but our politicians had other ideas. Our elected representatives knew full well that the city
                couldn&#39;t go without a functioning hospital for more than five minutes. However, the city of Torn
                could cope just fine without policemen.
              </p>
            </div>
            {this.getImage('special_officer', 'specialOfficer')}
            <div className={cn(s.container)}>
              <p className={cn(s.mgTop13, s.effectPostProhibition)}>
                In the post-prohibition years, this <span className={s.noWrap}>laissez-faire</span> attitude to
                criminality was Torn&#39;s greatest asset and its biggest flaw. The role of the police began to border
                on ceremonial, with only a handful of arrests being made every week for show. Essentially, the city of
                Torn decided to gamble on continued migration and write off the enforcement of law as an unnecessary
                expense, as if it were an <span className={s.noWrap}>inner-city</span> art project aimed at no-good
                teens with a cough syrup addiction.
              </p>
              <p>
                The abrupt nature of Torn&#39;s dismissal from the union threw the city&#39;s finances into further
                chaos. Gone were the federal funds required to run basic public services, and so too were the hordes of
                immoral interlopers destined to occupy our city, as the US placed a temporary restriction on travel to
                Torn in <span className={s.noWrap}>the immediate</span> aftermath of secession. This measure compounded
                the city&#39;s misery by preventing contractors scheduled to work
                <span className={s.noWrap}> here from</span> fulfilling their agreements.
              </p>
            </div>
          </div>
          <div className={s.endEffectPart1} />
          <div className={s.endCorner} />
        </div>
        <div className={cn(s.page, s.part2)}>
          <div className={cn(s.articleBlock, s.subBackground)}>
            {this.getImage('closed_down', 'closedDown')}
            <div className={cn(s.container)}>
              <p className={s.mgTop13}>
                Few politicians chose to remain in Torn after its expulsion. Those who did were inexperienced and naive,
                making them <span className={s.noWrap}>ill-equipped</span> to deal with an inflation crisis the likes of
                which wouldn&#39;t be seen again until Kylie Jenner&#39;s lips. Torn&#39;s last remaining administrators
                made the unusual decision to spend their way out of trouble by pressing on with the housing proposals.
                But with no external contractors able to enter Torn for the time being, the city&#39;s leaders{' '}
                <span className={s.noWrap}>took lead</span> on the project instead.
                <span className={s.tookLeadTypo1}> took lead on the</span>
                <span className={s.tookLeadTypo2}> project instead. took the lead on the project instead.</span>
              </p>
              <p>
                Then, as it is now, Torn City was home to billionaire drug cartels and
                <span className={s.noWrap}> filthy-rich</span> oil barons with extravagant tastes. Ordinary houses would
                never suffice for such people, or so the Council said. Their initial proposals were reasonably modest,
                with communities of simple two and three-bedroom homes planned for the inner fringes of{' '}
                <span className={s.typoSector7}>Sector 7.</span> But after everyone had had their say, the costs soon
                went up, with expensive adornments and lavish furnishings added wherever possible.
              </p>
              <div className={s.paperEffect2} />
              <p>
                Ring someone&#39;s doorbell, and you&#39;d hear Beethoven&#39;s Piano Concerto No.4 played in its
                entirety. Step inside, and you&#39;d be greeted by <span className={s.noWrap}>a Persian</span> rug
                hand-woven from angora wool. Need to pay an urgent visit to the can? Once the
                <span className={s.noWrap}>high-end </span> Japanese bidet has finished asking about your day and
                measuring your cholesterol, you would be offered a choice of still or sparkling with which to wash your
                posterior. Trust me, you haven&#39;t lived until you&#39;ve felt the gentle sensation of carbonated
                water about your nether regions.
              </p>
            </div>
            {this.getImage('gold_tap', 'goldTap')}
            <div className={cn(s.container, s.theseAdditionsBg)}>
              <p className={s.mgTop5}>
                These additions inflated the value of a single home beyond all reasonable proportions. Legend has it
                that the prices were so <span className={s.noWrap}>eye-watering</span> Torn&#39;s riverbeds rose by 40%
                <span className={s.noWrap}> that year.</span> The Council&#39;s initial budget was sufficient for the
                construction of 100,000 <span className={s.noWrap}>modest-sized </span> houses. Instead, they built a
                handful of ostentatious <span className={s.noWrap}>beach-front </span>properties, fulfilling less than a
                tenth of the original quota. There were nowhere near enough homes to meet public demand, so with the
                Council so clearly incapable, the mobs stepped in to meet the city&#39;s needs.
              </p>
              <p>
                But of course, these privately financed dwellings had to be <span className={s.typoMore}>more </span>
                ostentatious than those provided by the Torn Council. Because if there&#39;s one thing Torn&#39;s mobs
                love more than kneecapping a debtor, it&#39;s trying to outshine their rivals. A sprawling mass of
                luxury Chalets were built in <span className={s.noWrapDesktop}>and around</span> the City by Torn&#39;s
                criminal classes, who appeared to segue seamlessly from home invasions to home decor.
              </p>
            </div>
            <div className={s.endEffectPart2} />
          </div>
        </div>
        <div className={cn(s.page, s.part3)}>
          <div className={cn(s.articleBlock, s.mainBackground, s.withPaddingTop)}>
            <div className={cn(s.container, s.howeverParagrapher)}>
              <p className={s.mgTop0}>
                <span className={s.clear}>This trend of petulant </span>
                <span className={s.noWrap}>one-upmanship</span> kicked off a rather costly game of &#39;Keeping up with
                the Joneses&#39;, as when owning a Chalet became passé, the city&#39;s mobs were encouraged to build
                exotic Villas to rival them. Those not content with a Villa <span className={s.noWrap}>were then </span>
                offered presidential Penthouses, before our city&#39;s richest lobbied the mob to outdo these
                <span className={s.noWrap}> fancy-pants</span> apartments with a town full of Mansions. This was swiftly
                followed
                <span className={cn(s.clearLine, s.noClearLineMobile)}>by the construction of </span>
                <span className={s.noWrap}>multi-million</span> dollar Ranches and the bewildering decision to erect
                thousands upon thousands of Palaces - despite nobody in Torn being fancy enough to deserve one.
              </p>
            </div>
            {this.getImage('exotic_villas', 'exoticVillas')}
            <div className={cn(s.container, s.clear)}>
              <p>
                Torn thus became a forerunner to the garish new money glamour of Dubai and Abu Dhabi, where even dog
                refuse bins are trimmed with gold leaf <span className={s.noWrapDesktop}>these days</span>. And having
                satisfied the demands of Torn&#39;s richest, the mob <span className={s.noWrapDesktop}>was able </span>
                to slide into power easier than a lubed up pinkie. By the late 80s gangsters and criminals had
                infiltrated city hall&#39;s every office and orifice, and their greed truly knew no limits. Now they had
                control of Torn&#39;s budget, the mob seemed intent on squeezing every last drop from the chapped teats
                of a cash cow whose milk ducts had almost run dry.
              </p>
            </div>
            <div className={cn(s.container, s.exoticVillasBg)}>
              <p>
                But if we thought the stupidity would stop there, we were guilty of underestimating Torn&#39;s
                commitment to gratuitous displays of wealth. The vulgarity
                <span className={s.tornPalacesTypo}> of Torn&#39;s Palaces</span> was somehow topped by a rash of gaudy,
                <span className={s.noWrap}> medieval-style</span> Castles - Castles so obscenely large that they took up
                the last remnants of the city&#39;s buildable land. If you wanted to go further than that, as one
                drunken commentator of the time remarked, you&#39;d need to construct a
                <span className={s.noWrap}>sky-city </span> made of gold, or start dredging the ocean for dirt to build
                your own private island in the middle of the sea.
              </p>
            </div>
            {this.getImage('key_island', 'keyIsland')}
            <div className={cn(s.container)}>
              <p>
                In the past few decades, countless private islands have sprung up in the middle of the South Seas on the
                edge of Torn&#39;s territorial waters, having been constructed en masse at the behest of its most
                <span className={s.noWrap}> well-heeled</span> citizens. It is speculated there are thousands of
                man-made islands now in existence off <span className={s.noWrapDesktop}>the coast of</span> Torn, and
                nobody is quite sure how developers have managed to fit so many within its limited maritime boundaries.
              </p>
            </div>
            {this.getImage('new_lands', 'newLands')}
            <div className={cn(s.container)}>
              <p className={cn(s.paragraphEffect3, s.mgTop0)}>
                At first, it was suspected that only a handful of islands were actually in existence, with Torn&#39;s
                citizens too drunk or stupid to notice they were all purchasing timeshares in the same plots of land.
                This notion, while feasible due to the perpetually inebriated state of the population, is perhaps less
                likely than the idea that Torn has quietly extended its ocean borders without telling anyone. If this
                turns out to be the case, it remains to be seen how long the United States and Cuba will tolerate our
                illegal encroachment into international <span className={s.watersTypo}>waters.</span>
              </p>
              <p>
                Of course, this isn&#39;t the final chapter in Torn City&#39;s Real Estate story, not when its citizens
                have such an absurd dedication to showing off. In 2011 a portfolio of twelve unique properties was
                auctioned off for between 12 and 23 billion dollars each, with bigwigs including David, BigNemesis,
                Healen and Bogie amongst the few citizens wealthy enough to afford these grandiose dwellings. Given the
                demanding nature of Torn&#39;s people, you wouldn&#39;t rule out something like this happening again.
              </p>
            </div>
            {this.getImage('unique_sea_fort', 'uniqueSeaFort')}
            <div className={cn(s.container)}>
              <p className={s.effectOpposite}>
                At the opposite end of the scale, those who have only just arrived in the city have traditionally been
                afforded <span className={s.noWrap}>shanty-like</span> Shacks as a starter home. This initiative was the
                brainchild of Chedburn, who pushed for a more sustainable form of social housing provision after
                stumbling over no fewer than <span className={s.noWrap}> two-hundred</span> homeless people on his first
                day in the city. Today, thanks to Chedburn&#39;s altruistic nature, Torn is the only city in the world
                with no homeless people - its inhabitants may look,
                <span className={s.smellTypo}> smell and act homeless</span>, but they are not without homes.
              </p>
            </div>
            {this.getImage('sweet_shack', 'sweetShack')}
            <div className={cn(s.container)}>
              <p>
                Torn&#39;s commitment to providing a roof over every head sets it apart from the rest of the world.
                These rustic, <span className={s.noWrap}>rent-free</span> shacks do not compare with the Palaces and
                Private Islands in terms of quality, but they outstrip them entirely in the impact they&#39;ve had on
                the city&#39;s people. For even if you lose the shirt on your back, the fillings in your teeth and the
                dignity in your heart, you&#39;ll always know there&#39;s a bed waiting for
                <span className={s.somewhereTornTypo}> you somewhere in </span>Torn city.
              </p>
              <p>
                Granted, that bed probably contains a guy waiting to cut your fingers off, but beggars can&#39;t be
                choosers. It&#39;s hard to point with a set of stumps.
              </p>
            </div>
          </div>
          <div className={cn(s.finalImageWrapper, s.mainBackground)}>
            {this.getImage('the_end', 'endEffectPart3')}
          </div>
        </div>
      </article>
    )
  }
}

export default withTheme(TheRealEstate)
