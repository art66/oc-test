import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

const PreloaderComponent = () => <AnimationLoad />

const TheEmploymentRoute = Loadable({
  loader: async () => {
    const TheEmployment = await import('./containers')

    return TheEmployment
  },
  render(asyncComponent: any) {
    const { default: TheEmployment } = asyncComponent

    return <TheEmployment />
  },
  loading: PreloaderComponent
})

export default TheEmploymentRoute
