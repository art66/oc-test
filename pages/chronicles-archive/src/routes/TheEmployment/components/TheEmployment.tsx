import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import { fetchUrl, isDesktopMode } from '../../../utils'
import IChronicle from '../../../interfaces/IChronicle'
import { DARK_THEME, TIME_IN_SECONDS, THE_EMPLOYMENT_PATH_IMAGES } from '../../Chronicles/constants'
import '../../Chronicles/components/vars.scss'
import '../../Chronicles/components/DM_vars.scss'
import './index.css'

interface IProps extends TWithThemeInjectedProps {
  changeLocation: (x: string) => void
  fetchChronicles: () => void
  chronicle: IChronicle
  isLoggedIn: boolean
}

class TheEmployment extends Component<IProps> {
  startReadTime: number
  timer: number

  constructor(props: any) {
    super(props)
    const { changeLocation, fetchChronicles } = this.props

    fetchChronicles()
    changeLocation(window.location.hash)
  }

  componentDidUpdate() {
    const { chronicle } = this.props

    if (chronicle) {
      this.startTimer()
    }
  }

  componentWillUnmount() {
    this.clearTimer()
  }

  startTimer = () => {
    const {
      chronicle: { id },
      isLoggedIn
    } = this.props

    this.clearTimer()

    if (isLoggedIn) {
      this.timer = window.setInterval(() => {
        fetchUrl('/archives.php?step=updateSpentTime', {
          chronicleId: id,
          timestamp: TIME_IN_SECONDS
        })
      }, TIME_IN_SECONDS * 1000)
    }
  }

  clearTimer = () => {
    clearInterval(this.timer)
  }

  render() {
    const hideOnDesktopMode = isDesktopMode()
    const { theme } = this.props
    const isDarkMode = theme === DARK_THEME
    const darkImageFolder = isDarkMode ? '/dark_mode' : ''

    return (
      <div className='chronicles-employment'>
        <div className='page page-1'>
          <h5>
            <span>Day Jobs </span>
            <span>and Daylight Robbery</span>
          </h5>
          <div className='bg-effects'>
            <div className='img' />
            <div className='img' />
            <div className='img' />
          </div>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/stealing_money.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/stealing_money.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-1'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/stealing_money.png`}
              alt=''
            />
          </picture>
          <p>
            Torn City is one of the few places in the world where traditional employment is optional rather than
            necessary, as here you can get by quite comfortably with the occasional mugging, smuggling and trading scam
            if you so desire.
          </p>
          <p>
            And it&#39;s a good job too - pun intended - for here, the depth of employable talent is shallower than a
            child&#39;s paddling pool. Those who choose to live in a place like this often lack the ability to spell
            their own name, let alone fill in an application form - a situation hardly helped by the fact that by 11 am
            your average Torn citizen has suffered approximately fourteen blows to the head.
          </p>
          <p>
            Torn&#39;s people have never been keen on the traditional 9 to 5. Most argue that regular employment would
            get in the way of getting off your face - or removing someone else&#39;s with a butterfly knife - and is
            therefore incompatible with the lifestyle the inhabitants have come to expect. Thankfully, this sorry state
            of affairs is slowly starting to ease, with Tornfolk becoming ever more industrious thanks to the efforts
            of the city&#39;s leaders.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/jobs_fair_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/jobs_fair_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-2'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/jobs_fair.png`}
              alt=''
            />
          </picture>
          <p>
            Torn City&#39;s expulsion from the United States in the 1960s was swiftly followed by the kind of brain
            drain Pol Pot could have only dreamed of. The intelligentsia were the first to leave, followed by the
            students, the middle classes and the skilled labourers. Left behind were the dregs of society, a mixture of
            spit and booze incapable of performing even the most menial tasks. Industry collapsed overnight, along with
            sanitation, education and the basic provision of food and utilities.
          </p>
          <p className='mark mark-1'>
            In their place came a trading and bartering system which would later become the Item Market. On the
            backstreets of Torn City, people could exchange whatever they&#39;d stolen that day for the supplies and
            services they needed to live out the week. This setup suited everyone&#39;s needs for a while, with the
            traders providing the goods, and the Mafia overseeing maintenance of the city&#39;s districts - charging
            exorbitant fees to their residents for the privilege.
          </p>
          <p>
            However, when Factions assumed control of the city in the 1970s, they discovered its entire infrastructure
            was plagued by corruption and ineptitude. The plumbers weren&#39;t qualified. The doctors used guesswork.
            The city&#39;s electrical grid had been designed by a guy who reckoned he knew what he was doing because he
            was &#34;good at radios and stuff&#34;. If Torn was to develop into a metropolis replete with industry and
            a strong economy, it had to do better. The people must be brought up to standard - some might even have to
            get a proper job.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/torn_job_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/torn_job_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-3'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/torn_job.png`}
              alt=''
            />
          </picture>
          <p>
            To combat the city&#39;s skills shortage, the Torn Council worked together to provide publicly funded jobs
            within six specific sectors; Education, Legal, Gambling, Medical, Military and, for some reason, Groceries.
            These industries were chosen when the Council was comprised of Faction leaders, and there were some very
            specific reasons for targeting them, none of which reflect well upon the populace.
          </p>
          <p>
            Staff were hired for the Education sector when it became clear that many hundreds of children still remained
            in the city, despite their presence being outlawed by the Mafia a decade earlier. Most citizens were firmly
            against the idea of reintroducing youngsters to Torn life; not through fear, but because they had become
            accustomed to the ambience of gunshots and explosions. Nobody wanted this serene atmosphere punctured by the
            screaming nonsense of children.
          </p>
          <p>
            Makeshift schools were therefore set up in shacks on the edge of Torn to allow its youth to receive an
            education without fear of being caught. And with Torn College remaining officially closed until
            Chedburn&#39;s arrival in the late 90s, these rudimentary classrooms remained Torn&#39;s only place of
            learning for several decades. As a result, many adults attended these classes to brush up on their own
            knowledge, forced to share desks and textbooks with young people a third their age.
          </p>
        </div>
        <div className='page page-2'>
          <div className='bg-effects'>
            <div className='img' />
            <div className='img' />
            <div className='img' />
          </div>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/schooldesk_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/schooldesk_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-1'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/schooldesk.png`}
              alt=''
            />
          </picture>
          <p>
            The explanation for the Council-funded grocery positions is less certain. In Torn City there is, has and
            always will be a plethora of private merchants ready to rip you off for a roll of damp two-ply. Nobody is
            quite sure why the Council sought to create more Grocery Stores, but the answer may lie in the rumour that
            their employees are heavily monitored at all times. Whether this scheme was created to conduct research on
            the frequency of thefts or as a means of occupying our city&#39;s most numb-skulled brutes, we chroniclers
            have been unable to discern.
          </p>
          <p className='mark mark-1'>
            The reasoning behind the Council&#39;s heavy investment in the legal and medical sectors is clearer. With a
            higher daily turnover of patients than anywhere else on Earth, the Torn City Hospital would collapse without
            the tens of thousands of people paid to work there by the authorities. However, while the stress of constant
            violence has helped transform TCH into one of the world&#39;s most advanced medical facilities, the courts
            are another story altogether.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/lady_justice.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/lady_justice.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-2'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/lady_justice.png`}
              alt=''
            />
          </picture>
          <p className='mark mark-2'>
            The city&#39;s laws are an indecipherable muddle, the sentences doled out are ridiculously short, and
            constant breakouts mean that jail terms are nothing more than a minor inconvenience. This situation would be
            dispiriting for any respectable judge or barrister, but in Torn, no such people exist. The majority of those
            in the legal profession are there to learn how to avoid prosecution in future, and this attitude goes all
            the way to the top - it is often the case that the judge who passes your sentence will have spent more time
            in jail than you ever will.
          </p>
          <p>
            If truth be told, the enforcement of law in Torn is nothing more than a facade for the benefit of the
            outside world. Many nations refused to allow entry to travellers from Torn before its courts were
            established. But somehow, the illusory imposition of justice seems to have stopped the city from descending
            into abject chaos. In that respect, the law could be said to have fulfilled its purpose.
          </p>
          <p>
            Torn&#39;s publicly sponsored military sector was created in response to the frequent outbursts of violence
            which have plagued this city since its expulsion. Like the Council, members of the Torn Military were drawn
            from all Factions in a bid to create an impartial, nonpartisan force capable of quelling both internal and
            external threats. Thankfully, its soldiers haven&#39;t seen a minute of actual combat, since Torn has never
            been invaded and no one Faction has ever been deemed so powerful as to require action.
          </p>
          <p>
            Because of their inactivity, many have called for the disbandment of Torn&#39;s Army in recent years. This
            move has been vociferously opposed by the rank and file, though, who have become rather attached to their
            fancy uniforms and cheap supply of explosives.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/military_personnel_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/military_personnel_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-3'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/military_personnel.png`}
              alt=''
            />
          </picture>
        </div>
        <div className='page page-3'>
          <div className='bg-effects'>
            <div className='img' />
            <div className='img' />
            <div className='img' />
          </div>
          <p className='mark mark-1'>
            Finally, the Casino came under public ownership for one reason and one reason alone - corruption. At least
            that&#39;s what everyone reckons; and if you think about it, isn&#39;t that more important than the truth?
            By employing ordinary citizens within Torn&#39;s only gambling outlet, the Council - whose positions were
            unpaid - were better able to manipulate the city&#39;s addicts for their own personal gain. It has long
            been suspected that games at the Casino have been rigged in favor of the house. If true, those who profited
            would stand to have made trillions from this venture.
          </p>
          <p>
            Another possibility is that the existence of a publicly-funded Torn Casino constitutes a tax on the stupid,
            enabling the Council to fund ventures it cannot afford through donations alone. Either way, someone is
            making a lot of money from the city&#39;s most hopeful loss-chasers, and it sure as hell isn&#39;t the poor
            saps who work there.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/the_city_goes_legit_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/the_city_goes_legit_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-1'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/the_city_goes_legit.png`}
              alt=''
            />
          </picture>
          <p>
            The prospect of Council-sponsored positions drew many migrants to Torn City in the early 1970s, with
            Americans joined by Mexicans, Europeans and Indians in helping to repopulate this now-sparsely settled
            region. Few locals objected to this influx of foreigners, but those who did elected to build a barrier to
            try and keep them out. Due to their lack of skills and motivation, the resulting &#34;wall&#34; consisted of
            eighteen bricks placed in a line on the far eastern side of the dump. The &#34;Wall&#34; has now become a
            monument to the city&#39;s ignorant past, and today, citizens pay homage to their forebears by urinating
            upon it from end to end in a procedure known locally as a &#34;Dirty Hasselhoff&#34;.
          </p>
          <p>
            Along with their skills and knowledge, Torn&#39;s migrants also brought a cultural shift which affected the
            way people felt about employment. And thanks to their influence, the idea of legitimate work now appeals to
            roughly 50% of Torn&#39;s citizens, the rest being content to earn a crust through more nefarious means.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/census_of_tc_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/census_of_tc_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-2'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/census_of_tc.png`}
              alt=''
            />
          </picture>
          <p>
            As Torn&#39;s population increased and its workforce became reliable, many Council employees sought to
            better themselves by moving on to the private sector. But this was easier said than done, since outside
            qualifications have always been deemed irrelevant in Torn, with the city&#39;s used to determine your
            employability.
          </p>
          <p>
            Positions at non-Council companies can only be acquired once you have built up your reputation in one of
            Torn&#39;s six starter jobs. This system allows Torn to continue functioning as it does, by relying on
            newcomers to staff its hospitals and courts before they move on and are replaced by the next generation of
            migrants. However, back in the 1980&#39;s non-Council jobs were almost impossible to acquire, even for
            outsiders who had proven themselves, as most of Torn&#39;s businesses were kept strictly off the records
            for one very good reason; tax avoidance.
          </p>
          <p>
            After Torn City left the United States, it ceased enforcing the traditional tariffs and laws foisted upon
            private companies, and local business owners got used to the idea of keeping 100% of their profits. In the
            years that followed, flower shops, amusement parks and the rest were explained away as private, non-profit
            ventures. Strip clubs were rebranded as aerobics classes, toy shops were private teddy bear collections, and
            firework stores claimed to be paramilitary organisations hoarding explosives for a terror attack.
          </p>
        </div>
        <div className='page page-4'>
          <div className='bg-effects'>
            <div className='img' />
          </div>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/torn_secret_cover_sheet_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/torn_secret_cover_sheet_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-1'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/torn_secret_cover_sheet.png`}
              alt=''
            />
          </picture>
          <p>
            The few businesses prepared to admit their existence were located on the East Side of the city, but for the
            most part, Torn&#39;s commercial side sat firmly in the shadows, meaning it was nigh on impossible for
            newcomers to obtain anything but starter roles. Business owners rarely employed anyone outside their
            immediate circle of friends and family out of fears they would be exposed and taxed, and this nepotistic,
            self-preservative approach persisted throughout the 80s and 90s.
          </p>
          <p className='mark mark-1'>
            It was only in the early 2000s that Torn&#39;s industries finally became registered and documented.
            Chedburn, having taken charge of Torn several years prior, pushed to establish the voluntarily funded
            Donator House to allay citizens&#39; taxation concerns. He also offered directors a number of perks and
            benefits in an attempt to bribe them out into the open, with grand riches on offer for firms that
            toed the line.
          </p>
          <p>
            This approach worked, and the majority of Torn&#39;s companies chose to register with the authorities
            within a matter of days. One of the unforeseen consequences of this move was a massive turnover in staff
            for almost every company in the city. The benefit of hiring your friends and family is that you can pay
            them next to nothing, because who else would employ these incompetent boobs? However, when the city went
            legitimate, its people were free to work for whomever they wished, and this meant that wages rose for the
            first time since secession.
          </p>
          <p>
            The salary boom was further compounded by the bonuses offered by Chedburn to registered businesses. Staff
            from each and every industry knew their bosses were raking it in with these new subsidies, and naturally,
            they wanted a cut of the action.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/ride_attendant_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/ride_attendant_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-2'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/ride_attendant.png`}
              alt=''
            />
          </picture>
          <p>
            The legacy of this city-wide pay hike can still be seen today, with even the most average employees at a
            1-star Firework Stand commanding hundreds of thousands of dollars per day. This remained the case even when
            company subsidies were reduced in the 2010&#39;s - a move which has come to be known as being Chedded - with
            many young firms forced to go into debt in their early years just to keep hold of their staff.
          </p>
          <p className='mark mark-2'>
            The employment situation is now very much a seller&#39;s market, and this won&#39;t change anytime
            soon, since traditional work has never been necessary to achieve success in Torn City. People earned a
            living before the city went legit, and they&#39;ll do so again when it all goes down the drain. Plenty of
            brain-dead bruisers have rejected the prospect of a working life to instead focus on other, more enjoyable
            pursuits. Hitmen, prison busters, traders, scammers; all of these are legitimate roles in Torn, and the
            beauty of this city is that anyone can succeed regardless of their skills or experience.
          </p>
          <p>
            If you have a weapon, you&#39;ll earn. If you&#39;re strong, you&#39;ll prey on the weak. If you&#39;re
            smart, you&#39;ll squeeze money from the stupid. It really is that simple. The richest people in Torn City
            did not make their fortune by working hard in a clothes store; they did so by hoodwinking and ripping-off
            their fellow citizens. Crime is the only career you&#39;ll ever need in this place. And if anyone asks
            for your qualifications then feel free to show them, because they&#39;re most likely to be found in
            the barrel of a gun.
          </p>
          <picture>
            {hideOnDesktopMode && (
              <>
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/320/images/torn_companies_320.png`}
                  media='(max-width: 600px)'
                />
                <source
                  srcSet={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/578/images/torn_companies_578.png`}
                  media='(max-width: 1000px)'
                />
              </>
            )}
            <img
              className='img-3'
              src={`${THE_EMPLOYMENT_PATH_IMAGES}${darkImageFolder}/976/images/torn_companies.png`}
              alt=''
            />
          </picture>
        </div>
      </div>
    )
  }
}

const TheEmploymentWithTheme = withTheme(TheEmployment)

export default connect(
  (state: any) => ({
    browser: state.browser
  }),
  {}
)(TheEmploymentWithTheme)
