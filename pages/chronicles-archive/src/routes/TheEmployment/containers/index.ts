import { connect } from 'react-redux'
import TheEmployment from '../components/TheEmployment'
import { changeLocation, fetchChronicles } from '../../Chronicles/actions'
import { THE_EMPLOYMENT } from '../../Chronicles/constants'

const mapDispatchToProps = {
  changeLocation,
  fetchChronicles
}

const mapStateToProps = state => ({
  chronicle: state.chronicles.chronicles.find(item => {
    return item.slug === THE_EMPLOYMENT
  }),
  isLoggedIn: state.chronicles.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(TheEmployment)
