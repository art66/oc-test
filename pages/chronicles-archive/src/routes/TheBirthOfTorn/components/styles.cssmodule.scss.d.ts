// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'americanFlag': string;
  'billOfCongress': string;
  'block': string;
  'bootleggerMugshots': string;
  'brochure': string;
  'candyWrap': string;
  'congressman': string;
  'congressmanCaption': string;
  'container': string;
  'desktopImage': string;
  'donatorHouseBlueprint': string;
  'effect1': string;
  'effect2': string;
  'effect3': string;
  'effect4': string;
  'extendedImageCaption': string;
  'fullWidth': string;
  'globalSvgShadow': string;
  'governmentFile': string;
  'hammerAndSickle': string;
  'hangingAndBurial': string;
  'illicitBooze': string;
  'imageCaption': string;
  'imageHalfWidth': string;
  'inebriated': string;
  'investigationPapers': string;
  'labResults': string;
  'line': string;
  'mBottom20': string;
  'mBottom30': string;
  'mBottom40': string;
  'mTop20': string;
  'mTop30': string;
  'mTop40': string;
  'mTopMinus25': string;
  'mainBackground': string;
  'map': string;
  'maritalLow': string;
  'meeting': string;
  'mobileImage': string;
  'noTaxation': string;
  'old': string;
  'pBottom15': string;
  'pBottom20': string;
  'pBottom25': string;
  'pBottom30': string;
  'pTop15': string;
  'pTop20': string;
  'pTop25': string;
  'pTop30': string;
  'presidentShot': string;
  'proletariat': string;
  'separateText': string;
  'serveThePeople': string;
  'signatures': string;
  'small': string;
  'star': string;
  'subBackground': string;
  'tabletImage': string;
  'textCenter': string;
  'textHalfWidth': string;
  'theBirthOfTorn': string;
  'visual': string;
  'volsteadActBlock': string;
  'volsteadActImage': string;
  'wantedPosters': string;
  'westTown': string;
  'withPaddingBottom': string;
  'withPaddingBottomSmall': string;
  'withPaddingTop': string;
  'withPaddingTopBig': string;
}
export const cssExports: CssExports;
export default cssExports;
