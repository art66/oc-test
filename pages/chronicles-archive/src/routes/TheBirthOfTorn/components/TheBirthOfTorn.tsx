import React, { Component } from 'react'
import cn from 'classnames'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import { fetchUrl } from '../../../utils'
import IChronicle from '../../../interfaces/IChronicle'
import { DARK_THEME, TIME_IN_SECONDS, THE_BIRTH_OF_TORN_PATH_IMAGES } from '../../Chronicles/constants'
import '../../Chronicles/components/vars.scss'
import '../../Chronicles/components/DM_vars.scss'
import s from './styles.cssmodule.scss'

interface IProps extends TWithThemeInjectedProps {
  changeLocation: (x: string) => void
  fetchChronicles: () => void
  chronicle: IChronicle
  isLoggedIn: boolean
}

class TheBirthOfTorn extends Component<IProps> {
  startReadTime: number
  timer: number

  constructor(props: any) {
    super(props)
    const { changeLocation, fetchChronicles } = this.props

    fetchChronicles()
    changeLocation(window.location.hash)
  }

  componentDidUpdate() {
    const { chronicle } = this.props

    if (chronicle) {
      this.startTimer()
    }
  }

  componentWillUnmount() {
    this.clearTimer()
  }

  startTimer = () => {
    const {
      chronicle: { id },
      isLoggedIn
    } = this.props

    this.clearTimer()

    if (isLoggedIn) {
      this.timer = window.setInterval(() => {
        fetchUrl('/archives.php?step=updateSpentTime', {
          chronicleId: id,
          timestamp: TIME_IN_SECONDS
        })
      }, TIME_IN_SECONDS * 1000)
    }
  }

  clearTimer = () => {
    clearInterval(this.timer)
  }

  render() {
    const { theme } = this.props
    const isDarkMode = theme === DARK_THEME
    const darkImageFolder = isDarkMode ? '/dark_mode' : ''

    return (
      <article className={s.theBirthOfTorn}>
        <div className={s.mainBackground}>
          <div className={s.container}>
            <figure className={s.effect1} />
            <h5>The Birth of Torn</h5>
            <div className={cn(s.line, s.old)} />
            <p>
              The Chronicles you are about to read have been compiled over several decades by a group of historians who
              wish to remain unknown. Our motives may have been suspect, our minds addled by narcotics, but our goal was
              simple; to find answers to some of the most intriguing questions about our little corner of the world.
              Where is Torn City? How did it come to exist? Who is the mysterious Chedburn? And why are toilet rolls so
              expensive?
            </p>
            <p>
              And yet, as you shall see if you read on, these Chronicles leave us with few resolutions and more
              uncertainty than if we hadn&#39;t bothered at all. This muddled record of the city&#39;s history can be
              partially attributed to the <figure id={s.inebriated} /> state in which it was compiled, but it is also
              due to how much of Torn&#39;s lore consists of hearsay, rumors and conjecture. There are scant historical
              records to speak of and even fewer clear memories upon which to draw thanks to our people&#39;s penchant
              for inebriating substances. Therefore, we present the history of Torn City to you with the following
              caveat:
            </p>
            <p className={s.separateText}>
              <span className={s.block}>Not everything you read here may be true,</span>
              but our lies tell a story more accurate than facts ever could
            </p>
            <figure className={cn(s.signatures, s.textCenter)}>
              <img
                className={cn(s.desktopImage)}
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/1_976_six_anonymous_signatures.png`}
              />
              <img
                className={cn(s.tabletImage, s.mobileImage)}
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/1_320_six_anonymous_signatures.png`}
              />
              <figcaption className={s.imageCaption}>
                The signatures of Torn City&#39;s anonymous chroniclers, who are now considerably less anonymous.
              </figcaption>
            </figure>
          </div>
        </div>
        <figure className={s.effect2} />
        <div className={s.subBackground}>
          <div className={s.container}>
            <figure id={s.americanFlag}>
              <img src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}/images/2_976_riots_in_a_city_street.png`} />
              <figcaption className={s.imageCaption}>
                Anti-American sentiment persists in Torn City to this day.
              </figcaption>
            </figure>
          </div>
        </div>
        <div className={cn(s.mainBackground, s.withPaddingBottom)}>
          <div className={s.container}>
            <h6>The Founding of Torn City</h6>
            <p>
              To say that Torn was ever founded is inaccurate, as this implies any forethought took place before its
              construction. It would be more accurate to say that Torn festered from the wretched remains of a failed
              metropolis, like gangrene in an open wound, as an otherwise healthy nation became gravely weakened by the
              toxic inhabitants of one lonely forgotten place.
            </p>
            <p>
              Thought to reside somewhere along the coast of Louisiana and Texas, confirmation of Torn&#39;s precise
              location is known but difficult to come by, with the authorities none too keen to direct anyone towards
              this city of vicious moral reprobates. Ask around, and most people won&#39;t know where you&#39;re talking
              about. And even if they do, they certainly won&#39;t help you get there.
            </p>
            <figure id={s.map}>
              <img src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}/images/3_976_old_map_louisiana_and_texas.png`} />
            </figure>
            <p>
              The legend of Jeffery Torn establishing this settlement was a ruse conjured up to explain the
              unexplainable; a series of untruths used to disguise Torn&#39;s true beginnings from a naive populace
              eager to believe it was ever home to something good. This city was never good. It is, has and always will
              be a place where the dregs of society congregate. A magnet for filth, a hovel repellant to those with even
              a shred of morality.
            </p>
            <p>
              Torn is twinned in spirit with San Pedro Sula, Maturin and Distrito Central as if it were their bastard
              brother. Ostracised like North Korea and shunned more than Raqqa, you&#39;ll find Torn City on maps and
              charts if you search hard enough. But only the morbidly curious ever try, and only the brave dare visit.
            </p>
          </div>
        </div>
        <div className={cn(s.subBackground, s.withPaddingTop, s.withPaddingBottom)}>
          <div className={s.container}>
            <figure className={s.effect3} />
            <figure id={s.billOfCongress}>
              <img
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/4_976_a_bill_of_congress.png`}
              />
            </figure>
            <p>
              The United States re-took possession of the Neutral Ground just a few decades later when it annexed the
              Republic of Texas, but while it remains within the continental US territory geographically, this region
              and its inhabitants were expelled from the Union once more at some point during the mid 20th Century. This
              event left a wound on both city and nation. Torn&#39;s ousting was and is one of the most shameful acts in
              American history, with many keen to forget how quickly Uncle Sam disowned his most disappointing child.
            </p>
            <p>
              By the time it came to vote on Torn&#39;s expulsion, the people of the United States had long tired of
              hearing stories about &#34;neutral folk&#34; making trouble. The city&#39;s reputation became so toxic
              that neighbouring states would bar entry to her citizens. And so, as with any diseased limb, Torn was
              brutally cut off from the rest of the country, surgically removed with devastating precision and left to
              rot for the vultures to feed off.
            </p>
            <p>
              These days the legend of Torn has become just that, a legend. A place of such unbelievable barbarity it
              rivals the most hellish realms mankind has ever dreamt of. Most outsiders know nothing about the true
              nature of the city, and those who do are content to pretend it simply no longer exists - like racism, or a
              set of lace panties found in a husband&#39;s glovebox.
            </p>
            <p>
              Only recently have stories of this isolated community begun to take hold in the outside world, and even
              then, such reports were dismissed as conspiratorial nonsense believed only by those who spend too much
              time in their mother&#39;s basement. But how did this happen? What did the people of Torn do to deserve
              being shunned by their countrymen?
            </p>
          </div>
        </div>
        <div className={s.mainBackground}>
          <div className={s.container}>
            <h6>Life Before Torn</h6>
            <figure id={s.westTown}>
              <img
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/5_976_wild_west_town_scene.png`}
              />
            </figure>
            <div className={cn(s.line, s.old, 'm-bottom20 m-top20')} />
            <figure id={s.hangingAndBurial} className={cn(s.textCenter)}>
              <img
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/6_976_public_hanging_and_burial.png`}
              />
              <figcaption className={s.imageCaption}>A public hanging and burial</figcaption>
            </figure>
            <p>
              To explain why Torn was abandoned by the American people, we must first paint a picture of what this city
              was like before its decay into complete moral turpitude. By all accounts, this was never a particularly
              pleasant place, and what would later become Torn City is first mentioned in the early 1800s as a series of
              temporary shacks where outlaws could safely do business. By safely, we mean that criminal activities could
              be conducted away from the prying eyes of Johnny Law - Torn has never been &#34;safe&#34; in any sense of
              the word.
            </p>
            <p>
              This outpost could be found in the middle of the Neutral Ground between Spanish Texas and the state of
              Louisiana - the latter having just been purchased by the Americans. The Neutral Ground remained outside
              the jurisdiction of both Spain and the US from 1806 to 1821, until the Adams-Onis Treaty recognised
              America&#39;s claim. Throughout its fifteen years of neutrality, this territory bore witness to all manner
              of lawlessness, with thieves, murderers and con men seeking refuge here to avoid the attention of the
              Spanish and United States military.
            </p>
            <figure id={s.wantedPosters}>
              <img
                className='m-bottom20'
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/7_976_wanted_posters.png`}
              />
            </figure>
            <p>
              Expeditions from both nations were repeatedly sent into the area to expel these outlaws, but time and time
              again, the reprobates returned. Even after Adams-Onis came into effect and the Americans took possession,
              this place continued to provide trouble for law enforcement on both sides of the border. Brothels,
              smugglers, and bootleggers had all set up shop in the Neutral Ground long, long ago; some fancy-pants
              piece of paper wasn&#39;t going to change a thing.
            </p>
            <p>
              The East Texas Regulator Moderator War fought between 1839 and 1844 stemmed directly from the anarchic
              situation in the old Neutral Ground. When Texas was accepted into the United States in 1845, the territory
              seemed to calm down a little. Homes were built, a clinic too, and the first signs of modern infrastructure
              began to develop. While chaos and disorder continued to stain the landscape, some folks made a concerted
              effort to turn this into a distinctly ordinary place.
            </p>
            <div id={s.volsteadActBlock}>
              <img
                id={s.volsteadActImage}
                className={s.imageHalfWidth}
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/8_976_the_volstead_act.png`}
              />
              <p className={s.textHalfWidth}>
                An ordinary place it almost became, as while the criminal element remained, the end of the 19th Century
                saw emerging coastal townships begin to attract ordinary people who had ordinary jobs and lived
                perfectly ordinary lives. Some people ran dance halls; others ran crime rings. Some sought solace in the
                arms of a priest; others looked for salvation between the thighs of a prostitute. It was, like any
                community, home to both good and evil - albeit tipped slightly in favour of the latter.
              </p>
            </div>
            <p>
              Then, with the era of prohibition, the balance shifted further. This little chunk of Americana started to
              turn very sour indeed.
            </p>
            <p>
              The trouble began when a group of entrepreneurial underworld hustlers took advantage of the nationwide
              alcohol ban by starting up an illicit booze trade. In Torn, like much of America, speakeasies were
              tolerated so long as they were quiet. But here the laws were so rarely applied that one could set up shop
              outside the Police Station and receive no more than a sideways glance and a knowing wink.
            </p>
          </div>
          <figure id={s.bootleggerMugshots} className={s.textCenter}>
            <img
              className={s.desktopImage}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/9_976_bootlegger_mugshots.png`}
            />
            <img
              className={cn(s.tabletImage, s.mobileImage)}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/9_578_bootlegger_mugshots.png`}
            />
            <div className={s.container}>
              <figcaption className={cn(s.imageCaption, s.fullWidth)}>
                On the rare occasions bootleggers were taken into custody, their arrest often coincided with a party at
                the stationhouse. To protect their profits, many rum-runners would go dark in the days preceding an
                officer&#39;s birthday.
              </figcaption>
            </div>
          </figure>
          <figure id={s.illicitBooze} className={s.textCenter}>
            <img
              className={s.imageHalfWidth}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/10_976_illicit_booze_confiscated.png`}
            />
            <div className={s.container}>
              <figcaption className={cn(s.imageCaption, s.fullWidth)}>
                To protect their federal funding, the local police would stage photographs of themselves disposing of
                illegal booze. In truth, this booze was disposed of, making its way to the sewers via a detour around
                the urinary tract of local lawmen
              </figcaption>
            </div>
          </figure>
        </div>
        <div className={cn(s.subBackground, s.pTop15, s.pBottom15)}>
          <div className={s.container}>
            <p>
              This lax attitude toward alcoholic excess attracted visitors from all over the country, swelling the
              settlement&#39;s population to that of a city. Sadly, when the Prohibition era ended, so too, did the flow
              of money. Torn needed a new gimmick, a fresh vice; something like booze but twice as indecent. Drugs,
              weapons and gambling soon took the place of the liquor trade, and all three began to flourish out in the
              open. One notoriously brazen tourist brochure demonstrated as much by advertising Torn as somewhere you
              could &#34;come for the blackjack, stay for the crack.&#34;
            </p>
          </div>
        </div>
        <div className={cn(s.mainBackground, s.withPaddingBottomSmall)}>
          <figure id={s.brochure} className={cn(s.textCenter)}>
            <img
              className={s.imageHalfWidth}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/11_976_brochure.png`}
            />
            <figcaption className={s.imageCaption}>
              The aforementioned brochure advertising bordellos, blackjack and booze on offer in Torn City.
            </figcaption>
          </figure>
          <div className={s.container}>
            <p>
              Given that this occurred during the 1930s, and that crack cocaine wasn&#39;t available until the 1980s,
              the crack to which this advert refers was of an altogether more libidinous variety.
            </p>
            <p>
              So, as the rest of the nation placed further restrictions upon these dangerous industries, Torn responded
              by loosening the belt even more, until its trousers dangled well below an acceptable level. Torn&#39;s
              tempting array of narcotics and nookie made it a magnet for those seeking to indulge their vices. The
              loners, the oddballs, the unhinged and the immoral all pitched up for a night in Torn City to satiate a
              myriad of tawdry desires. And unfortunately for her inhabitants, theirs was no passing visit.
            </p>
            <p>
              This influx of immoral newcomers seemed to spark off a new era of lawlessness among the natives as if it
              were a dormant gene awakened from decades of suppression. Fights took place every hour of the day on every
              street in the city. Drugs were traded and taken in front of impotent officials. The entire city felt like
              it was home to one never-ending riot, and there was little the law could do to intervene.
            </p>
            <figure id={s.maritalLow}>
              <img
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/12_976_newspaper_martial_law.png`}
              />
            </figure>
            <p>
              Having welcomed their new underhand underclass with open arms in the name of profit, the Police no longer
              held respect in the eyes of Torn&#39;s people. You can&#39;t turn a blind eye to crime and expect anything
              less than contempt in return, and this disdain was shared by both the American people and their
              representatives in Congress. Martial law was declared; it didn&#39;t work. Troops moved in to restore
              order; they failed miserably.
            </p>
            <p>
              So, in an attempt to starve out Torn&#39;s criminal community once and for all, a series of brutal cuts
              were made to the city&#39;s federal funding. These measures only served to exacerbate the problem, though,
              driving a permanent wedge between the people of Torn and America. Hastily written laws were reintroduced
              locally in an attempt to curtail the mayhem, but it was too late. Respect for authority was dead. Rather
              than proving a deterrent, the prospect of jail time seemed almost aspirational to some. The stable door
              was locked long after the horse had bolted, emigrated and died of old age on a farm upstate.
            </p>
            <p>
              And so, with Torn&#39;s grubby mitts threatening to tarnish the stars and bars with a protest as dirty as
              one could ever imagine, the American people made their choice.
            </p>
            <p>Amputate.</p>
          </div>
        </div>
        <div className={s.subBackground}>
          <div className={s.container}>
            <figure id={s.congressman}>
              <img
                className={cn(s.imageHalfWidth, s.small)}
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/13_578_congressman.png`}
              />
            </figure>
            <p id={s.congressmanCaption} className={cn(s.textHalfWidth, s.extendedImageCaption)}>
              According to the records, this photograph was taken on the day Congress elected to force sedition upon
              Torn City.
              <br />
              Although it could just as easily have been a vote on dairy tariffs.
            </p>
          </div>
        </div>
        <div className={cn(s.mainBackground, s.withPaddingBottomSmall)}>
          <div className={s.container}>
            <h6>It&#39;s Not Me; It&#39;s You</h6>
            <p>
              Nobody can agree on which if any one event led to the fateful decision by Congress to renounce all claims
              to the Neutral Territory. Based on reports from the time, it could have been for any number of reasons.
              All we know is that it was pushed through hastily and the decision was unanimously accepted among most
              everyday Americans who were more than happy to wash the nation&#39;s hands clean of Torn&#39;s filth.
            </p>
            <p>
              With this domestic crisis having taken place during the height of the Cold War, it is unsurprising that it
              flew under the radar of the international community. The US Government was not keen to reveal such a dirty
              little secret amid rising tensions with the Soviet Union, mostly out of fear that the unhinged people of
              Torn might be susceptible to their overtures. If Cuba&#39;s cosy relationship with the Russians was a
              thorn in their side, the prospect of a communist-aligned Torn would have been a sickle to Uncle Sam&#39;s
              face. And so, with the rest of the world preoccupied preparing for nuclear holocaust, the city and its
              people were left to their own devices.
            </p>
            <p>
              The newly independent Torn City was despondent at first, but it didn&#39;t mope for long. After the
              initial malaise lifted Torn celebrated its freedom with a period of violence like nothing before. Its
              people were feral, its neighbours&#39; doors shut and its problems were its own. With no hope and no
              future, even the most calm-tempered turned paranoid and volatile. Any semblance of society was obliterated
              within weeks. The feeling in the air was palpable; Torn was about to explode. I&#39;d describe it as like
              a powder keg, but since Torn folk keep gunpowder in their pockets not barrels, this metaphor may be lost
              on most.
            </p>
            <p>
              Enter stage right: The Mafia. With Torn&#39;s politicians and police having abandoned this sinking ship as
              fast as their trotters could carry them, it was the mobs who sought to fill the power vacuum in their
              stead. Little did they know that Torn had become ungovernable, and even hardened criminals were shocked at
              the brutalities they witnessed on a daily basis. As rival gangs fought over control of the city, its
              people suffered appallingly from death, disease and degradation. And when you&#39;ve got nothing much to
              live for, the prospect of sleeping with the fishes seems positively relaxing.
            </p>
            <p>
              But the mobs lost their power as soon as they had acquired it. Ordinary men and women began to fight back
              against the Mafia, caring not whether they would live to see the result. Everyone knew that Torn was a
              live grenade that could go off at any moment; their only hope was that there would be enough corpses to
              muffle the explosion.
            </p>
            <figure className={s.textCenter}>
              <img
                className={s.imageHalfWidth}
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/14_976_militia.png`}
              />
              <div className={s.container}>
                <figcaption className={cn(s.imageCaption, s.fullWidth)}>
                  The self-proclaimed &#34;Order of the Old Farts&#34;, a militia formed of elderly men and women who
                  fought back against the mob. The crosses indicate those who died during the troubles.
                </figcaption>
              </div>
            </figure>
            <p>
              The tipping point came when a vicious mob confrontation ended with the tragic deaths of a group of
              innocent bystanders – twelve of them, all children, all gunned down in broad daylight while making their
              way to school. The birth-rate in Torn has always been abnormally low, and the few kids we had were like
              gold dust. To see these precious young lives snuffed out was an event which would never be forgotten, but
              nor would you find anyone keen to speak of it again.
            </p>
            <p>
              It was a tragedy that almost broke a city, and in response Torn&#39;s people evacuated their few remaining
              youths away until the chaos subsided. It never did, and to this day the presence of infants and young
              teens is prohibited in Torn City, with those who reside here doing so illicitly.
            </p>
            <figure className={cn(s.textCenter, s.mTop20)}>
              <img
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/15_976_childrens_names.png`}
              />
            </figure>
          </div>
          <figure id={s.serveThePeople} className={s.visual} />
          <figure id={s.star} className={s.visual} />
          <figure id={s.hammerAndSickle} className={s.visual} />
          <figure id={s.proletariat} className={s.visual} />
        </div>
        <div className={cn(s.subBackground, s.withPaddingTopBig)}>
          <div className={s.container}>
            <p>
              After the children were gone, Torn&#39;s citizens rose up in response to this atrocity. Nobody knows if
              any one person inspired them - if there was a figurehead his or her name remains elusive - but when mob
              leaders were executed in the street by their own men the city knew it had turned a corner. Here there was
              to be no idealistic revolution, no cleansing the city of sinners or a great criminal purge. This leopard
              couldn&#39;t change its spots, not overnight, not ever; but something had to give.
            </p>
            <p>
              Thankfully it did. The people took control, forming their own courts, their own political machinery and
              their own idea of society. Democracy wouldn&#39;t work in a place like this, so in lieu of politicians,
              Torn&#39;s people formed into Factions based on their individual needs and beliefs. These organisations
              resembled a hybrid of worker cooperative and criminal enterprise, with each having its own idea of how
              Torn City should be run.
            </p>
            <figure className={cn(s.textCenter, s.mTop20)}>
              <img
                src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/16_976_minutes.png`}
              />
            </figure>
          </div>
        </div>
        <div className={cn(s.mainBackground, s.withPaddingTop)}>
          <div className={s.container}>
            <p>
              These factions eventually banded together to form a unified leadership group known as The Council. This
              was a sound idea in principle, but soon the city&#39;s strongest Factions began to strongarm their weaker
              counterparts and wield an inordinate amount of power over the city. Before long they became a law unto
              themselves, committing the kind of daily atrocities which made the Council no different to the mob it
              sought to replace. Factions waged war over minor administrative issues, and their leaders soon realised
              Torn was in danger of lurching backwards over the cliff-edge it had just dragged itself over. This rare
              moment of self-awareness led to the realisation that Torn needed a leader. An impartial citizen whose mind
              would be unbiased, whose judgements were their own and who could steer this city towards a better place.
            </p>
            <p>
              This person could not wield the power of a President or Prime Minister, but nor could they lead in spirit
              alone. The city needed someone who would take charge not through force or flawed democracy, but by earning
              the trust of the people. Many attempted to fulfil this role by leading community projects and proposing
              public initiatives, but when all was said and done, there was one candidate who stood head and shoulders
              above the rest; one citizen who took it upon himself to slowly repair the damage inflicted on Torn City
              after years of neglect.
            </p>
            <p>Chedburn.</p>
          </div>
          <figure className={cn(s.governmentFile)}>
            <img
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/17_976_government_file_on_ched.png`}
            />
          </figure>
        </div>
        <div className={cn(s.subBackground, s.withPaddingBottom)}>
          <div className={s.container}>
            <h6>A Filthy Butterfly</h6>
            <p>
              The Torn of two decades ago is vastly different from the city it has become today. While criminal factions
              remain influential, they have nothing on the feral swarms of old. And even though Torn&#39;s underworld
              activities would be a stain on most cities, here, the people somehow make it work.
            </p>
            <p>
              Torn&#39;s necessary divorce from the rest of the country proved to be her saving grace. Without the
              jurisdiction to mete out its own unique method of punishment and appoint citizen leaders, the city would
              have consumed itself long ago. American values cannot be applied here, and so, as with everything in Torn,
              a grubby compromise had to be found.
            </p>
            <p>
              In 2003 the young upstart known as Chedburn took it upon himself to guide and reform several areas of the
              city, despite nobody knowing much about his motives or background. Since Torn was on the brink of disaster
              and he was the only capable person not addicted to narcotics, everyone just kinda went along with it. And
              it&#39;s a good job they did, for it was through Chedburn&#39;s guidance that Torn finally found its feet
              after years of staggering around blackout drunk.
            </p>
          </div>
          <figure id={s.donatorHouseBlueprint} className={cn(s.textCenter, s.mTop20)}>
            <img
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/18_976_donator_house_blueprint.png`}
            />
          </figure>
        </div>
        <div className={cn(s.mainBackground, s.pTop25, s.pBottom25)}>
          <figure id={s.noTaxation} className={s.visual} />
          <div className={s.container}>
            <p>
              When the lines between crime and pastime became blurred, it was Chedburn who decreed which laws we should
              follow. When Torn began to run out of cash having overspent on gratuitous vanity projects, it was Chedburn
              who developed the Donator House to protect Torn from taxation. Whenever a problem needed a solution,
              Chedburn was there with several. As a result, this mysterious outsider became the de facto source of
              leadership for all areas of Torn City life.
            </p>
            <p>
              In later years, Chedburn established the committee to ward off accusations that he had established a
              dictatorship. But even with a band of respected community leaders there to keep him in check, everyone
              knew that the power to guide the city lay in his hands alone. To consolidate this authority, Chedburn set
              up his own security services with wide-ranging powers over the population. These individuals,
              hand-selected by Chedburn himself, have the ability to silence or remove members of the public as they see
              fit. They act as the moral guardians of Torn in lieu of an effective police force, with their primary
              function being to exist as a bulwark against the organised groups who would seek to control the city once
              more - hence their acronym:
            </p>
            <p>Secure Torn Against Forceful Factions, or STAFF.</p>
            <p>It is widely believed that Chedburn came up with the acronym first and made sense of it later.</p>
          </div>
          <figure id={s.investigationPapers} className={cn(s.textCenter, s.mTop20)}>
            <img
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/19_976_investigation_papers.png`}
            />
          </figure>
          <div className={s.container}>
            <p>
              Admittance to these gilded positions is left to the discretion of Chedburn, with only those who can prove
              their past commitment to the city allowed to exert influence on its future. Most of Torn seems happy with
              this situation. This is their way, it is the people&#39;s way, and even the most depraved rotters know
              such an arrangement represents Torn&#39;s last hope for survival.
            </p>
          </div>
          <h6>Who Is Chedburn?</h6>
          <figure id={s.meeting} className={cn(s.textCenter, s.mBottom20)}>
            <img
              className={cn(s.desktopImage, s.tabletImage)}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}/images/20_976_meeting.png`}
            />
            <img
              className={cn(s.mobileImage)}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}/images/20_320_meeting.png`}
            />
          </figure>
          <div className={s.container}>
            <p>
              Despite never holding an official position, Chedburn is often referred to as Torn&#39;s Mayor or President
              - titles he is known to reject. Whether he sees himself as a moral guardian or benevolent influence is
              something you&#39;d have to ask him yourself, but this notoriously private individual rarely has time to
              respond to such queries.
            </p>
            <p>
              One of the biggest mysteries surrounding Torn&#39;s elusive messiah is why he insists on appointing Mayors
              and Presidents to work beneath him, despite their powers and duties being effectively less than zero.
              What&#39;s more, these impotent politicians are often shot dead not long into their tenure by Torn&#39;s
              criminal factions, as are visiting diplomats and leaders from foreign shores. Organised crimes pay well
              for those willing to wipe out a political figure, and it is speculated that Chedburn invites his enemies
              to these positions knowing full well their eventual fate.
            </p>
          </div>
          <figure id={s.presidentShot} className={cn(s.textCenter, s.mTop20, s.mBottom30)}>
            <img
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/21_976_president_shot.png`}
            />
          </figure>
          <div className={s.container}>
            <p>
              The one aspect of Chedburn&#39;s personality of which we can be certain is that he wishes to make Torn a
              fairer and more equal place. Chedburn famously intervened in the city&#39;s stock market to reduce the
              disparity between rich and poor; one of many acts which gained him the approval of the city&#39;s
              socialists, while drawing the ire of those who had hoped to make bundles of cash in secret before anyone
              noticed.
            </p>
            <p>
              Something Chedburn has never sought to change is Torn&#39;s inclination towards scumbaggery. Wherever he
              came from, it seems he fully understands just how much Torn&#39;s people love to be bastards. While
              Chedburn was attempting to reform the financial district, he simultaneously passed an edict that made
              scamming entirely legal, and it is this blend of moral bankruptcy and warped community spirit which makes
              the Chedburn-led Torn City truly unique.
            </p>
            <p>
              Nowhere else in the world will you find bazaar owners willing to trade with someone who beat them to a
              pulp two days ago. In no other city would it be possible for violent thugs from rival gangs to work side
              by side at a gentlemen&#39;s strip club. And yet somehow here it works, with Torn&#39;s economy now more
              stable than ever thanks to an uneasy alliance between crime, corporations and community.
            </p>
            <p>
              To augment this newly found prosperity, Chedburn minted Torn&#39;s first independent currency, The Torn
              City Dollars, to enable fairer trade. But despite Torn&#39;s newfound wealth, this commercial venture was
              not without its problems, and the lack of public consultation regarding the TCD&#39;s introduction has led
              to an inconsistent pricing structure still in place today.
            </p>
          </div>
        </div>
        <div className={cn(s.subBackground, s.withPaddingBottom)}>
          <figure id={s.candyWrap} className={cn(s.textCenter)}>
            <img
              className={cn(s.desktopImage, s.mTopMinus25)}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/22_976_candy_wrap.png`}
            />
            <img
              className={cn(s.tabletImage, s.mobileImage)}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/22_578_candy_wrap.png`}
            />
          </figure>
        </div>
        <div className={cn(s.mainBackground, s.pTop25, s.pBottom25)}>
          <div className={s.container}>
            <p>
              The reasons for this situation shall be discussed in a separate chronicle, but as with every problem in
              Torn, the path of culpability leads you to the door of our most unscrupulous citizens. Yet rather than
              forlornly attempting to remove a criminal element which other cities would consider distasteful,
              Chedburn&#39;s unique form of casual governance merely provides Torn&#39;s populace with choice. It is up
              to the individual to choose to be a brute or a businessman. It is up to the public whether to seek help
              from a professional or a punk. It is up to you, a citizen, to decide how low Torn will force you to go.
            </p>
            <p>
              Thanks to this Torn City now enjoys a fragile balance. Priests and pill poppers, criminals and cops; they
              all break bread together. Nobody could call it peace, but it&#39;s the best Torn can do. Despite the
              ongoing violence, people still manage to go about their daily lives; they work, they play, they get
              married, all the while knowing a brutal end could be waiting right around the corner. Drugs are rife of
              course, but in a place like this, you need a bump just to ride out the day.
            </p>
          </div>
          <h6>Initial Findings</h6>

          <figure id={s.labResults} className={cn(s.textCenter, s.mBottom30)}>
            <img
              className={cn(s.mTopMinus25)}
              src={`${THE_BIRTH_OF_TORN_PATH_IMAGES}${darkImageFolder}/images/23_976_lab_results.png`}
            />
          </figure>
          <div className={s.container}>
            <p>
              And it is here where the city known as Torn resides at present. Nobody knows its original name, the
              records having been expunged from the history books after a series of bloody events any nation would care
              to forget. Since this place was ripped with bare hands from the convulsing body of a tormented nation,
              Torn seems as good a label as any. And while it stands strong today thanks to the leadership of the
              mononymous Chedburn, Torn remains perpetually teetering on the precipice of chaos thanks to a combination
              of unhinged citizens and sheer good fortune.
            </p>
          </div>
          <figure className={s.effect4}>
            <div className={s.container}>
              <p className={s.mTop20}>But nothing lasts forever, least of all luck.</p>
            </div>
          </figure>
        </div>
      </article>
    )
  }
}

export default withTheme(TheBirthOfTorn)
