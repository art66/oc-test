import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

const PreloaderComponent = () => <AnimationLoad />

const TheBirthOfTornRoute = Loadable({
  loader: async () => {
    const TheBirthOfTorn = await import('./containers')

    return TheBirthOfTorn
  },
  render(asyncComponent: any) {
    const { default: TheBirthOfTorn } = asyncComponent

    return <TheBirthOfTorn />
  },
  loading: PreloaderComponent
})

export default TheBirthOfTornRoute
