import { connect } from 'react-redux'
import TheBirthOfTorn from '../components/TheBirthOfTorn'
import { changeLocation, fetchChronicles } from '../../Chronicles/actions'
import { THE_BIRTH_OF_TORN } from '../../Chronicles/constants'

const mapDispatchToProps = {
  changeLocation,
  fetchChronicles
}

const mapStateToProps = state => ({
  chronicle: state.chronicles.chronicles.find(item => {
    return item.slug === THE_BIRTH_OF_TORN
  }),
  isLoggedIn: state.chronicles.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(TheBirthOfTorn)
