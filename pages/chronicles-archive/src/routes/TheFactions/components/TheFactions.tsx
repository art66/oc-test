import React, { Component } from 'react'
import cn from 'classnames'
import { connect } from 'react-redux'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import { fetchUrl } from '../../../utils'
import IChronicle from '../../../interfaces/IChronicle'
import { DARK_THEME, TIME_IN_SECONDS, THE_FACTIONS_PATH_IMAGES } from '../../Chronicles/constants'
import '../../Chronicles/components/vars.scss'
import '../../Chronicles/components/DM_vars.scss'
import s from './styles.cssmodule.scss'

interface IProps extends TWithThemeInjectedProps {
  browser: {
    mediaType: string
  }
  changeLocation: (x: string) => void
  fetchChronicles: () => void
  chronicle: IChronicle
  isLoggedIn: boolean
}

class TheFactions extends Component<IProps> {
  startReadTime: number
  timer: number

  constructor(props: any) {
    super(props)
    const { changeLocation, fetchChronicles } = this.props

    fetchChronicles()
    changeLocation(window.location.hash)
  }

  componentDidUpdate() {
    const { chronicle } = this.props

    if (chronicle) {
      this.startTimer()
    }
  }

  componentWillUnmount() {
    this.clearTimer()
  }

  getImage = (name: string) => {
    let {
      browser: { mediaType }
    } = this.props
    const isDarkMode = this.props.theme === DARK_THEME
    const darkImageFolder = isDarkMode ? '/dark_mode' : ''

    let prefix = '976_'

    mediaType = document.body.classList.contains('r') ? mediaType : 'desktop'

    switch (mediaType) {
      case 'desktop':
        prefix = '976_'
        break
      case 'tablet':
        prefix = '578_'
        break
      case 'mobile':
        prefix = '320_'
        break
      default:
        break
    }

    return (
      <img
        className={cn(s.image, s[mediaType], s[name])}
        src={`${THE_FACTIONS_PATH_IMAGES}${darkImageFolder}/images/${prefix}${name}.png`}
      />
    )
  }

  startTimer = () => {
    const {
      chronicle: { id },
      isLoggedIn
    } = this.props

    this.clearTimer()

    if (isLoggedIn) {
      this.timer = window.setInterval(() => {
        fetchUrl('/archives.php?step=updateSpentTime', {
          chronicleId: id,
          timestamp: TIME_IN_SECONDS
        })
      }, TIME_IN_SECONDS * 1000)
    }
  }

  clearTimer = () => {
    clearInterval(this.timer)
  }

  render() {
    return (
      <article className={s.factions}>
        <div className={s.mainBackground}>
          <div className={s.container}>
            <figure className={cn(s.effect, s.grunge1)} />
            <h5>Mob Rule</h5>
            <div className={cn(s.line, s.old, s.half)} />
            {this.getImage('patches')}
            <p className={s.half}>
              Torn&#39;s factions are a unique hybrid of criminal enterprise and worker cooperative, offering a more
              refined, disciplined approach to the art of sectarian violence than your typical urban street gangs.
              Imagine if the Mafia promoted self-improvement opportunities and free healthcare - it&#39;s like that, but
              with nukes.
            </p>
          </div>
          {this.getImage('newspaper')}
          <div className={s.container}>
            <p>
              Each Faction has its own particular offerings and tenets, and it is these differences which explain how
              such groups first emerged in Torn. Traditional mobs and gangs have always populated the neutral strip, and
              prior to Torn&#39;s expulsion, most outlaws were content to involve themselves in nothing more than
              smuggling, racketeering and other typical gangland activities.
              <span className={cn(s.textInsertion, s.however)}>However,</span>
            </p>
            <p>
              However, when the city was forced into lawless chaos after being gouged from the Union, even the outlaws
              knew Torn must return to order or face extinction. With no politicians or councillors to lead them,
              Torn&#39;s people took matters into their own hands. And before long, bands of vigilante public servants
              had completely filled the power vacuum, with various groups maintaining responsibility for everything from
              trash <span className={cn(s.textInsertion, s.collection)}>collection</span> and pest control through to
              utility provision and repairs.
            </p>
            <p>
              Based on their own individual persuasions, beliefs and prejudices, like-minded people began to coalesce
              behind their doctrine&#39;s most charismatic proponent - or the guy with the biggest gun. These groups of
              eager volunteers comprised the earliest examples of what we now call Factions, and without them, the Torn
              we know today would never have come to exist.
            </p>
            <figure className={cn(s.effect, s.grunge2)} />
          </div>
          {this.getImage('ordinaryPeople')}
          <div className={s.container}>
            <figure className={cn(s.effect, s.grunge3)} />
            <p>
              Yet as admirable as their commitment to civic duty was, Torn&#39;s people were and have always been one
              minor disagreement away from violence. Everyone has their own individual idea of how things should be
              done, and Torn folk are no different. When two gangs of do-gooders turned up to revamp the local park,
              one would leave bloodied while the others built a jungle gym. If the sewage works weren&#39;t functioning
              correctly, one gang would head out to repair it, only for a rival to sabotage their efforts and fix it
              their own way later that night. <span className={cn(s.textInsertion, s.eachFaction)} />
            </p>
            <p>
              Frequent differences of opinion over the most trivial matters often led to street wars in the early years
              of <span className={cn(s.textInsertion, s.factionCulture)}>Faction culture</span> - one argument over a
              tariff on imported plushies almost engulfed over half of the city. This competitiveness extended to social
              issues too. Each group had their own opinion on how Torn&#39;s people should conduct themselves. Is
              scamming legal or a heartless crime? Does perpetual bullying build character or force people from the
              city? Should we pursue wars or shall we form treaties? Which laws do we respect, and which will we
              disregard?
            </p>
            <p>
              To avoid large-scale riots over such divisive issues, Torn&#39;s most populous Factions came together in
              an attempt to form some kind of understanding. Each group agreed to send a diplomatic representative to
              speak on their behalf, someone who would fight for their Faction&#39;s interests without needing to break
              out the knuckle dusters. These men and women formed a semi-political entity known as The Council, and
              if truth be told, the system was flawed from the start <span className={cn(s.textInsertion, s.dot)} />
            </p>
            {this.getImage('factionMembers')}
          </div>
        </div>
        <div className={s.subBackground}>
          <div className={s.container}>
            <p>
              Democracy in a violent state surely leads to dictatorship. Almost as soon as the Council began, the
              city&#39;s largest Factions began to eschew their civic responsibilities and draft legislation which
              further cemented their power. Weaker factions formed alliances in an attempt to force their way onto the
              Council, and a civil war quickly erupted between those in charge and Factions stuck on the periphery.
            </p>
            <span className={cn(s.textInsertion, s.highlight)} />
            <p>
              Thankfully, the Faction leaders had the foresight to restrain themselves and avoid a repeat of the
              violence of years gone by. This admirable moment of self-control is not something you&#39;d see from
              modern Faction chiefs, who are far more reactionary than their battle-hardened predecessors. The
              Council&#39;s members had all lived through the troubles wrought upon the city by infighting and
              corruption. They knew not how to fix the city, but they were well aware of what had to be done to prevent
              its destruction.
            </p>
            <figure className={cn(s.effect, s.paper)} />
            <p>
              It was determined that democracy of any kind would be unsuited to Torn&#39;s particular social climate,
              and nor was it sustainable for any one group to take control by force, since many of the city&#39;s
              Factions were equally as capable of defeating each other. The Factions agreed to let a leader naturally
              emerge from the people, and in the meantime, Torn City would be allowed to govern itself. This minimal
              interference made Torn what it is today. Some aspects of city life may seem inconsistent or downright
              bizarre, but that&#39;s what happens when you let the people have their say.
            </p>
            <p>
              Eventually, a chosen one did emerge. His name was Chedburn, the first of his name, and he remains the only
              person to succeed in uniting Torn&#39;s Factions through unanimous support.
            </p>
            <p>
              {this.getImage('polaroid')}
              This backing can be partially attributed to the fact that Chedburn allowed Factions to continue to exist,
              despite them proving a constant threat to his claims to power. In their current form, bereft of any actual
              authority, Factions act as militarised political parties. Each is convinced they know what&#39;s best for
              its members and for greater Torn. But, as anyone who has trawled Torn&#39;s message boards will attest,
              the diversity of opinion in this city is broader than the shoulders of its most hench citizens.
              Subsequently, there are thousands of individual Factions operating in Torn City today, and each of them is
              wrong about something.
            </p>
            <span className={cn(s.textInsertion, s.pencil1)} />
            <p>
              Some Factions are small five-man outfits with naive dreams of Torn domination. Others comprise a legion of
              battle-hardened warriors, with each veteran casting their beady eye at the
              <span className={cn(s.textInsertion, s.factionLeadershipRole)}>Faction&#39;s leadership role</span>. Over
              the years, many of Torn&#39;s historical Factions both large and small have succumbed to time and broken
              up or merged with more powerful groups. But whether they consist of one person or one hundred, whether
              they endure or have failed, each and every Faction has pursued one single goal above all others.
            </p>
          </div>
        </div>
        <div className={s.mainBackground}>
          <div className={s.container}>
            {this.getImage('respect')}
            <p>
              When Chedburn took charge, Torn&#39;s Factions lost the ability to compete for social and civic power; a
              pastime which, along with their criminal schemes, had kept them busy for decades. So, without the need to
              impart violence upon their rivals for political gains, Factions instead began to engage in orchestrated
              displays of brutality for the simple glory of achievement. These conflicts are no mere scuffles, though,
              and nor do they bear any resemblance to simple turf wars either. These engagements are about pride, honour
              and self-sacrifice. In Torn, respect is something to be earned not through words, but through blood.
            </p>
            <span className={cn(s.textInsertion, s.pencil2)} />
            <p>
              Torn has always been viciously competitive, and organised mass confrontations provide a handy outlet for
              the collective ambition of this city of psychopaths. Chedburn tolerates the violence and even goes so far
              as to label it a cultural activity. Breaking someone&#39;s legs has now become a terrifyingly routine
              occurrence in Torn, like going shopping, walking the dog, or taking a shower. And so, Faction warring was
              established as the city&#39;s favourite way to pass the time.
            </p>
            <p>
              Putting the members of another Faction into the hospital is the simplest method of gaining respect for
              your group. As respect is gained through victorious combat, the reputation of a Faction rises, with well
              respected Factions often developing insight into money making schemes and secret training techniques -
              hence why some are able to offer these bonuses to their members.
              <span className={cn(s.textInsertion, s.pencil3)}>Many fff</span>
            </p>
            <p>
              Many factions also provide members with the chance to boost their criminal skills and earn extra cash
              through legal or illegal means. But first and foremost, membership of a faction affords you a measure of
              esteem. If someone wants to leave you beaten and bleeding in the street, they know they&#39;ll have to
              deal with your factionmates soon enough.
            </p>
            <figure className={cn(s.effect, s.grunge4)} />
            <p>
              In return for the perks and protection, total loyalty to your brothers and sisters is expected. Should a
              conflict break out, some factions will hand out free weapons, armor and medications to their members. But
              fail to turn up to a fight, and you could find yourself ostracised or worse. And regardless of level,
              almost every faction gains you access to a close community of like minded souls. This last offering is
              key; lone wanderers don&#39;t last long in Torn, hence why most citizens ally themselves with other, less
              mentally-stable folk even in times of peace.
            </p>
            <p>
              Some brave individuals have carved out a successful existence outside the Faction lifestyle. But for many,
              their allegiances determine whether they live or die. This relationship isn&#39;t all one way, though. If
              you are to retain membership of a Faction, you must also adhere to criteria set by the leadership. Certain
              high-level factions require a minimum state of physical excellence in order to join. And, once you are
              accepted, you may be forced to maintain a certain level of wealth, participate regularly in attacks, and
              conduct yourself in a manner befitting your Faction&#39;s reputation - be this scrupulously or otherwise.
            </p>
          </div>
          {this.getImage('collectionOfTickets')}
        </div>
        <div className={cn(s.subBackground, s.second)}>
          <div className={s.container}>
            <p>
              By far the most influential single act one can use to influence your Faction&#39;s standing is the
              detonation of a Dirty Bomb. Countless radioactive explosions have occurred in the city of Torn ever since
              Factions acquired the ability to construct armories and laboratories. This is a privilege bestowed upon
              only the most powerful groups, with the necessary blueprints provided clandestinely by an unnamed
              individual - long suspected to be the elusive figure known as Anonymous.
            </p>
            <p>
              Thankfully, the level of radiation poisoning inflicted by these devices is sufficient only to warrant a
              short stay in hospital for individual patients, and a low level of mutations and growths within the
              population as a whole. Whether this is through design or chance, we have yet to discern. The author of
              this piece believes that the citizens of Torn are simply too stupid to construct a properly functioning
              Dirty Bomb, but then again, he always has been an arrogant ******.
            </p>
          </div>
          {this.getImage('dirtyBomb')}
        </div>
        <div className={cn(s.mainBackground, s.withPaddingTop)}>
          <div className={s.container}>
            <p>
              The most respected Factions use the threat of destruction via bombing or warring to corral their weaker
              rivals into various alliances or protection schemes. If you or your Faction is part of a certain group,
              this often determines how you are treated and what level of influence you have. Faction alliances have
              been known to collectively manipulate the city and embark on devious scams. Others have consistently
              banded together to force opinions and legislative changes upon Torn&#39;s populace.
            </p>
            <p>
              Factions can lose their authority just as easily as they can gain it, though, and many factors can upset
              the balance of power in Torn City; changes in legislation, corrupt leaders or the merging of rival
              Factions can and will continue to shake the deck on a regular basis. Chedburn&#39;s founding of the STAFF
              initiative also helped to prevent any one faction becoming hegemonous, with its members drawn from various
              groups in an attempt to avoid accusations of bias and corruption.
            </p>
            <p>
              Yet despite the mediating and powerful presence of Chedburn, everyone knows that a well-organised major
              Faction is easily capable of bending the city to its every whim. The might of the Factions became even
              more evident when they began a territorial incursion en masse towards the heart of Torn City centre in
              January 2017.
            </p>
            <p>
              Factions have always fought turf wars, but for the most part, their squabbles were limited to a few city
              blocks on the outskirts of the city. All of that changed in early 2017, as over 200 Factions descended
              upon the heart of the city to establish claims over more fruitful plots of land.
            </p>
          </div>
          {this.getImage('cityMap')}
          <div className={s.container}>
            <p>
              Nobody knows exactly what prompted the Factions to do this, although it is suspected that a poorly worded
              piece of legislation from the authorities may have been to blame. The document, which was aimed at
              ratifying existing claims to property, may have mistakenly imbibed Factions with the idea that ownership
              by conquest was now entirely legal.
            </p>
            <p>
              This effectively gave them carte blanche to carve up Torn like a roasted pig. After various high-profile
              areas of the city centre fell under Faction rule in April 2017, Chedburn has been unable to restore order
              over most of Torn City center. And having already wielded a considerable amount of power in the past,
              Torn&#39;s Factions are peachy keen on a return to the glory days of old.
            </p>
            <figure className={cn(s.effect, s.grunge5)} />
            <p>
              A brief taste of power is a dangerous thing; the Factions want more. Whether this situation will lead to a
              repeat of the violence of the past nobody is quite sure. Either way, the Factions now hold this city by
              the proverbial, and their vice-like grip shows no sign of easing anytime soon.
            </p>
          </div>
          <figure className={cn(s.effect, s.end)} />
        </div>
      </article>
    )
  }
}

const TheFactionsWithTheme = withTheme(TheFactions)

export default connect(
  (state: any) => ({
    browser: state.browser
  }),
  {}
)(TheFactionsWithTheme)
