import { connect } from 'react-redux'
import TheFactions from '../components/TheFactions'
import { changeLocation, fetchChronicles } from '../../Chronicles/actions'
import { THE_FACTIONS } from '../../Chronicles/constants'

const mapDispatchToProps = {
  changeLocation,
  fetchChronicles
}

const mapStateToProps = state => ({
  chronicle: state.chronicles.chronicles.find(item => {
    return item.slug === THE_FACTIONS
  }),
  isLoggedIn: state.chronicles.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(TheFactions)
