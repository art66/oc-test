import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

const PreloaderComponent = () => <AnimationLoad />

const TheFactionsRoute = Loadable({
  loader: async () => {
    const TheFactions = await import('./containers')

    return TheFactions
  },
  render(asyncComponent: any) {
    const { default: TheFactions } = asyncComponent

    return <TheFactions />
  },
  loading: PreloaderComponent
})

export default TheFactionsRoute
