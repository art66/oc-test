import { connect } from 'react-redux'
import TheMarkets from '../components/TheMarkets'
import { changeLocation, fetchChronicles } from '../../Chronicles/actions'
import { THE_MARKETS } from '../../Chronicles/constants'

const mapDispatchToProps = {
  changeLocation,
  fetchChronicles
}

const mapStateToProps = state => ({
  chronicle: state.chronicles.chronicles.find(item => {
    return item.slug === THE_MARKETS
  }),
  isLoggedIn: state.chronicles.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(TheMarkets)
