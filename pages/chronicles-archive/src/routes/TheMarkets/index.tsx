import React from 'react'
import Loadable from 'react-loadable'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

const PreloaderComponent = () => <AnimationLoad />

const TheMarketsRoute = Loadable({
  loader: async () => {
    const TheMarkets = await import('./containers')

    return TheMarkets
  },
  render(asyncComponent: any) {
    const { default: TheMarkets } = asyncComponent

    return <TheMarkets />
  },
  loading: PreloaderComponent
})

export default TheMarketsRoute
