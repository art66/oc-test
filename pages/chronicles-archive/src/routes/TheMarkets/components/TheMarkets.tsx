import React, { Component } from 'react'
import cn from 'classnames'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'
import { fetchUrl, isDesktopMode } from '../../../utils'
import IChronicle from '../../../interfaces/IChronicle'
import { DARK_THEME, TIME_IN_SECONDS, THE_MARKETS_PATH_IMAGES } from '../../Chronicles/constants'
import '../../Chronicles/components/vars.scss'
import '../../Chronicles/components/DM_vars.scss'
import s from './styles.cssmodule.scss'

interface IProps extends TWithThemeInjectedProps {
  changeLocation: (x: string) => void
  fetchChronicles: () => void
  chronicle: IChronicle
  isLoggedIn: boolean
}

class TheMarkets extends Component<IProps> {
  startReadTime: number
  timer: number

  constructor(props: any) {
    super(props)
    const { changeLocation, fetchChronicles } = this.props

    fetchChronicles()
    changeLocation(window.location.hash)
  }

  componentDidUpdate() {
    const { chronicle } = this.props

    if (chronicle) {
      this.startTimer()
    }
  }

  componentWillUnmount() {
    this.clearTimer()
  }

  startTimer = () => {
    const {
      chronicle: { id },
      isLoggedIn
    } = this.props

    this.clearTimer()

    if (isLoggedIn) {
      this.timer = window.setInterval(() => {
        fetchUrl('/archives.php?step=updateSpentTime', {
          chronicleId: id,
          timestamp: TIME_IN_SECONDS
        })
      }, TIME_IN_SECONDS * 1000)
    }
  }

  clearTimer = () => {
    clearInterval(this.timer)
  }

  render() {
    const { theme } = this.props
    const hideOnDesktopMode = isDesktopMode()
    const isDarkMode = theme === DARK_THEME
    const darkImageFolder = isDarkMode ? '/dark_mode' : ''

    return (
      <article className={s.chronicleMarkets}>
        <div className={cn(s.page, s.part1)}>
          <div className={cn(s.effects, s.blackestMarket)} />
          <div className={cn(s.effects, s.clips)} />
          <div className={cn(s.articleBlock, s.mainBackground)}>
            <h5 className={s.header}>The Blackest Market</h5>
            <div className={cn(s.titleDivider)} />
            <picture>
              {hideOnDesktopMode && (
                <>
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}/320/images/shopping_backets.png`}
                    media='(max-width: 600px)'
                  />
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}/578/images/shopping_backets.png`}
                    media='(max-width: 1000px)'
                  />
                </>
              )}
              <img
                className={cn(s.articleImage, s.shoppingBackets)}
                src={`${THE_MARKETS_PATH_IMAGES}/976/images/shopping_backets.png`}
                alt=''
              />
            </picture>
            <div className={cn(s.container, s.shopping)}>
              <p>
                Consumers have never had it so good. These days, you can purchase mostly anything within the space of a
                few hours. If you want a job lot of cat food, simply head on over to the supermarket. Need a new type of
                hair gel? Amazon is your friend. And if you&#39;re after an excessively damp mattress, where better
                than <span className={cn(s.textTypo, s.craigslist)}>Craigslist?</span>
              </p>
              <p>
                In Torn, the exchanging of goods for cash is equally straightforward, but here you&#39;re far more
                likely to do business in person than visit a chain store or online outlet. People have always come to
                Torn to sell all manner of goods, but in the 1960s a unique trading culture began to emerge, when a
                mass exodus of its most capable citizens left those behind with a simple choice; trade or die.{' '}
                <span className={cn(s.textTypo, s.tornsTypo)}> Torn&#39;s</span>
              </p>
              <p>
                Torn&#39;s informal bartering system began with the simple exchange of items, cash and services; bread
                for pills, plumbing repairs for a massage, contract killings paid for with an intimate encounter behind
                the sweet shop, and so on and so forth. In the 1970s a more formal approach was taken when sellers
                pitched up at a newly designated area known as the Item Market, where items were offered for sale
                rather than for trade.
              </p>
            </div>
          </div>
          <div className={cn(s.articleBlock, s.mainBackground)}>
            <picture>
              {hideOnDesktopMode && (
                <>
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/320/images/market_stalls.png`}
                    media='(max-width: 600px)'
                  />
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/578/images/market_stalls.png`}
                    media='(max-width: 1000px)'
                  />
                </>
              )}
              <img
                className={cn(s.articleImage, s.marketStallsImg)}
                src={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/976/images/market_stalls.png`}
                alt=''
              />
            </picture>
            <div className={cn(s.container, s.cultureShift)}>
              <p className={s.pdRight5}>
                This culture shift came about in response to the uncertainty surrounding the city&#39;s unofficial
                currency, the Torn City Dollar. The TCD would only officially be minted and pegged to world currencies
                upon Chedburn&#39;s arrival in the early 2000&#39;s, but before then, the TCD was considered a volatile
                currency, as nobody was sure how much a Torn Dollar was actually worth. Traditional stores offered the
                stability consumers wanted by keeping fixed prices, but since no-one knew the TCD&#39;s value, they
                were able to charge exorbitant fees for everyday items to compensate - because when in doubt, mark it
                up. The legacy of this price rise can still be seen today.
              </p>
              <p className={cn(s.mgTop21, s.exchange)}>
                The <span className={s.dollars20} /> demanded in exchange for a simple box of tissues seems absurd at
                first, yet positively reasonable when compared to the $1,850,000 valuation of the same product formed
                into a convenient roll. Likewise, the $26,500 price of a cricket bat makes little sense, until you
                discover that a fruitcake costs $700,000, at which point Torn&#39;s pricing structure seems so eccentric
                that you wouldn&#39;t be surprised to see a pair of boxing gloves on sale for half a billion dollars.
              </p>
            </div>
          </div>
          <div className={cn(s.articleBlock, s.withHole)}>
            <div className={cn(s.mainBackground, s.boxingImgWrapper)}>
              <picture>
                {hideOnDesktopMode && (
                  <>
                    <source
                      srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/320/images/boxing_gloves.png`}
                      media='(max-width: 600px)'
                    />
                    <source
                      srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/578/images/boxing_gloves.png`}
                      media='(max-width: 1000px)'
                    />
                  </>
                )}
                <img
                  className={cn(s.articleImage, s.boxingGloves)}
                  src={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/976/images/boxing_gloves.png`}
                  alt=''
                />
              </picture>
            </div>
            <div className={cn(s.textWrapper, s.mainBackground, s.paragraph1)}>
              <p>
                These excessive and inconsistent markups led many citizens to abandon traditional shopping outlets in
                lieu of the black market. However, with the valuation of many items now distorted beyond reason, the
                idea that underground traders would restore sanity to proceedings was a forlorn hope. The pricing of
                Torn goods did level off eventually, but this was entirely driven by the despicable needs of Torn
                citizens.
              </p>
            </div>
            <div className={cn(s.bgHole)} />
            <div className={cn(s.textWrapper, s.mainBackground, s.paragraph2)}>
              <p>
                The simple economics of supply and demand have been corrupted in Torn, with value now determined by the
                amount of pain or pleasure an item is capable of causing. While a bottle of beer may sell for $5
                elsewhere, in Torn the <span className={cn(s.textTypo, s.charge500)}>$500 charge</span> for a tallboy
                reflects the potential earnings one can acquire thanks to the increase in nerve enabled by alcohol. And
                while a box of individual tissues may bring you a small amount of joy through the removal of snot from
                your face, this does not compare to the amusing level of depression you could cause a neighbor by
                throwing a whole roll over their property.
              </p>
            </div>
          </div>
          <div className={cn(s.articleBlock, s.mainBackground)}>
            <div className={cn(s.effects, s.simpleEconomicsBg)} />
            <div className={cn(s.wrapper, s.clear)}>
              <div className={cn(s.left, s.half)}>
                <p>
                  The demand for products capable of causing mischief and mayhem was something the traditional stores
                  failed to meet; they clung tight to the notion that Torn folk might still be interested in purchasing
                  designer clothes, high-end watches and subscriptions to an endless feed of cable sports and movies.
                  But this was a mistake, for why would anyone want to live vicariously through sports stars and action
                  heroes when you can take out someone&#39;s eyeball in the street and no-one will bat an
                  eyelid - least of all the guy whose depth perception is now irrevocably impaired.
                </p>
                <p>
                  Their failure to understand the Torn consumer was the retail industry&#39;s undoing. As such, many
                  outlets packed up shop and left the city - save for a handful of long-standing tenants on the East
                  Side. In their stead, the citizens of Torn sold products between themselves within makeshift
                  street markets; exchanging weaponry, alcohol and narcotics for cash; cash which was then used to
                  purchase weaponry, alcohol and narcotics. A perfect system. What could possibly go wrong?
                </p>
                <p>
                  In 1978 the average daily taking for a typical market stall was minus{' '}
                  <span className={cn(s.textTypo, s.moneysTypo)}>$4,388,</span> three deep tissue wounds and 0.4 broken
                  bones.
                </p>
              </div>
              <div className={cn(s.right, s.imageWrapper)}>
                <picture>
                  {hideOnDesktopMode && (
                    <>
                      <source
                        srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/320/images/takings.png`}
                        media='(max-width: 600px)'
                      />
                      <source
                        srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/578/images/takings.png`}
                        media='(max-width: 1000px)'
                      />
                    </>
                  )}
                  <img
                    className={cn(s.articleImage, s.takings)}
                    src={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/976/images/takings.png`}
                    alt=''
                  />
                </picture>
              </div>
            </div>
          </div>
        </div>
        <div className={cn(s.page, s.part2)}>
          <div className={cn(s.articleBlock)}>
            <div className={s.burnedAngle} />
            <div className={cn(s.textWrapper, s.subBackground, s.paragraph1)}>
              <p>
                At first, Torn&#39;s black market operated out on the main high streets from carts and ramshackle
                stalls. When the city&#39;s major thoroughfares became clogged with merchants, traders were moved to
                the backstreets where lighting was poor. People are capable of terrible acts when nobody is watching.
                Traders were robbed, beaten and abused by their own customers. One man was even fed through his own
                mincing machine for failing to provide a discount on a simple{' '}
                <span className={cn(s.textTypo, s.packOfSausages)}>pack of</span>
                <span className={cn(s.textTypo, s.sausages)}> sausages.</span>
              </p>
            </div>
            <div className={cn(s.textWrapper, s.subBackground, s.paragraph2)}>
              <p>
                Traders fled the profession in droves due to the violence inflicted upon them, and it didn&#39;t take
                long for their customers to realise they&#39;d bitten off and digested the only hand that ever wished
                to feed them. Without an accessible supply of drugs, alcohol and firearms - the holy trinity of
                Torn - people started to get anxious. A handful of intrepid individuals even broke into the old Torn
                College in an attempt to make their own meth and moonshine, only to end up blowing themselves apart
                into tiny, disappointed pieces.
              </p>
            </div>
            <div className={s.tornPiece} />
            <div className={cn(s.newspaperImgWrapper, s.subBackground)}>
              <picture>
                {hideOnDesktopMode && (
                  <>
                    <source
                      srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/320/images/newspaper.png`}
                      media='(max-width: 600px)'
                    />
                    <source
                      srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/578/images/newspaper.png`}
                      media='(max-width: 1000px)'
                    />
                  </>
                )}
                <img
                  className={s.articleImage}
                  src={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/976/images/newspaper.png`}
                  alt=''
                />
              </picture>
            </div>
          </div>
          <div className={cn(s.articleBlock, s.subBackground, s.thunderdome)}>
            <div className={cn(s.effects, s.newspaperBg)} />
            <div className={cn(s.textWrapper)}>
              <p className={s.pdRight5}>
                Thankfully, the Factions stepped in to address the situation towards the end of the decade. Having
                cobbled together enough money between them for a major building project, the Factions&#39; proposals
                were whittled down to two possible options, a retail outlet, or an erotic-themed thunderdome. In the end
                sense prevailed, and the Item Market was constructed in 1979 a short drive down from the city&#39;s
                beltway. This was followed by the Auction House in 1987, which was created in response to the noise
                generated from hagglers who had failed to consider the delicate eardrums of a perpetually
                hungover population.
              </p>
              <p>Plans for the thunderdome remain on hold.</p>
            </div>
            <picture>
              {hideOnDesktopMode && (
                <>
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/320/images/erotic_thunderdome.png`}
                    media='(max-width: 600px)'
                  />
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/578/images/erotic_thunderdome.png`}
                    media='(max-width: 1000px)'
                  />
                </>
              )}
              <img
                className={s.articleImage}
                src={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/976/images/erotic_thunderdome.png`}
                alt=''
              />
            </picture>
          </div>
        </div>
        <div className={cn(s.page, s.part3)}>
          <div className={cn(s.articleBlock, s.mainBackground, s.defaultShadow)}>
            <div className={cn(s.effects, s.torned)} />
            <div className={cn(s.container, s.buyMugging)}>
              <p>
                The market was intended as a safe haven for traders both amateur and professional alike; a place where
                an ordinary schmo could walk in off the street, sell the contents of their pockets and leave with enough
                money to buy themselves a date. Most importantly, it offered traders a place where items of value could
                be stored, sold and exchanged away from the lingering threat of violence. And for the most part,
                the <span className={cn(s.textTypo, s.itemMarket)}>Item Market</span> continues to serve this purpose.
              </p>
              <p>
                The listing fees charged for the protection offered by the Item Market are seen as a justifiable expense
                by the sole traders and bazaar owners who use the facility to conduct their business. In the entire
                history of the market, not one single trader has had their stock pilfered or been murdered while
                carrying out a transaction. However, the number of people assaulted after a transaction is a different
                story.
              </p>
              <div className={cn(s.effects, s.divider)} />
              <p>
                Buy mugging involves making a purchase only to relieve the seller of their money immediately afterwards,
                allowing the mugger to run off with both cash and item before the seller has had the chance to bank
                their earnings. There are no official statistics available on buy mugging due to the suppression of this
                information by the authorities. If the true figures were known, it is feared that few new traders would
                enter the profession.
              </p>
            </div>
            <div className={s.buyMuggingImgWrapper}>
              <picture>
                {hideOnDesktopMode && (
                  <>
                    <source
                      srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/320/images/buy_mugging_report.png`}
                      media='(max-width: 600px)'
                    />
                    <source
                      srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/578/images/buy_mugging_report.png`}
                      media='(max-width: 1000px)'
                    />
                  </>
                )}
                <img
                  className={s.articleImage}
                  src={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/976/images/buy_mugging_report.png`}
                  alt=''
                />
              </picture>
            </div>
          </div>
          <div className={cn(s.articleBlock, s.mainBackground)}>
            <div className={cn(s.effects, s.scratches)} />
            <div className={s.container}>
              <p className={s.pdRight12}>
                You may think that this kind of incident would be acceptable in a place like Torn, but people take
                &#34;playing shop&#34; quite seriously. Buy muggers are frowned upon by the general public and repeat
                offenders are banned <span className={s.enmasse}>en-masse</span> from many major bazaars - all those
                ill-gotten gains aren&#39;t much use if the only place you can spend them is the Sweet Shop. But
                don&#39;t let yourself believe for a minute that this hatred of buy muggers has anything to do
                with morals or ethics.
              </p>
              <p className={s.pdRight12}>
                The people of Torn may be vicious, but they aren&#39;t as stupid as they seem. They know full well that
                if the violent trading culture of old were to return, their supply of drugs, booze and weaponry would be
                shut down immediately. This act of self-preservation is understandable, since many ordinary citizens
                have done well from Torn&#39;s unique trading model, especially those who stock their stores via
                smuggling ventures.
              </p>
              <p className={s.pdRight12}>
                There are few foreign nations which accept Torn citizens, but those who do offer tourists the chance to
                purchase a range of items which cannot be bought in Torn, or which are considerably cheaper abroad.
                Plushies, drugs and medications are routinely secreted within Torn citizens in preparation for their
                flight home, and many of Torn&#39;s richest bazaar owners made their fortunes from such humble, intimate
                beginnings.
              </p>
            </div>
          </div>
          <div className={cn(s.articleBlock, s.mainBackground)}>
            <div className={cn(s.printedSentenceWrapper)}>
              <div className={cn(s.printedSentence)} />
            </div>
            <div className={cn(s.effects, s.lastParagraph)} />
            <div className={s.container}>
              <p>
                Yet even frequent fliers can find it tough to get their bazaars noticed unless they have a significant
                amount of start-up capital for merchandise. There are tens of thousands of bazaars in operation today,
                with historical bazaar sales passing{' '}
                <span className={cn(s.textTypo, s.sales)}>700,000,000 in 2016, compared to 20,000,000</span> items sold
                directly on the item market by the same date. The total earned by bazaar owners in Torn&#39;s history
                grew from $95 trillion TCD in 2016 to $119 trillion in 2017, and by mid-2020 this figure had exceeded
                $200 trillion.
              </p>
              <p>
                The success of Torn&#39;s trading culture comes in spite of there being no legislation governing what
                you can or cannot offer for sale - or perhaps this is its defining feature. There is no limit as to
                what you can acquire from Torn&#39;s eager band of part-time shopkeepers. In the absence of background
                checks, even the most unhinged individual can walk away with enough firepower to invade a small South
                American nation; something the people of Torn have attempted on several notable occasions.
              </p>
              <p className={s.pdRight12}>
                But for all its flaws and foibles, Torn&#39;s Bazaar and Item Market community is fast becoming the
                envy of the world. Not only is there ample chance for even the most incompetent seller to make money,
                but the sheer range of illegal and enticing items is enough to make even men of the cloth turn their
                heads. If you haven&#39;t yet perused Torn&#39;s Bazaar and Item Market, you would be encouraged to
                do so post-haste. There&#39;s certain to be someone eager to sell you something, whether you need
                it or not.
              </p>
            </div>
          </div>
          <div className={cn(s.finalImageWrapper, s.mainBackground)}>
            <picture>
              {hideOnDesktopMode && (
                <>
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/320/images/final_image.png`}
                    media='(max-width: 600px)'
                  />
                  <source
                    srcSet={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/578/images/final_image.png`}
                    media='(max-width: 1000px)'
                  />
                </>
              )}
              <img
                className={cn(s.articleImage, s.finalImage)}
                src={`${THE_MARKETS_PATH_IMAGES}${darkImageFolder}/976/images/final_image.png`}
                alt=''
              />
            </picture>
          </div>
        </div>
      </article>
    )
  }
}

export default withTheme(TheMarkets)
