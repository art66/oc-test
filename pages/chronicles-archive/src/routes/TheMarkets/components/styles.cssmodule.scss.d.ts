// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'articleBlock': string;
  'articleImage': string;
  'bgHole': string;
  'blackestMarket': string;
  'boxingGloves': string;
  'boxingImgWrapper': string;
  'burnedAngle': string;
  'buyMugging': string;
  'buyMuggingImgWrapper': string;
  'charge500': string;
  'chronicleMarkets': string;
  'clear': string;
  'clips': string;
  'container': string;
  'craigslist': string;
  'cultureShift': string;
  'defaultShadow': string;
  'divider': string;
  'dollars20': string;
  'effects': string;
  'enmasse': string;
  'exchange': string;
  'finalImage': string;
  'finalImageWrapper': string;
  'globalSvgShadow': string;
  'half': string;
  'header': string;
  'imageWrapper': string;
  'itemMarket': string;
  'lastParagraph': string;
  'left': string;
  'mainBackground': string;
  'marketStallsImg': string;
  'mgTop21': string;
  'moneysTypo': string;
  'newspaperBg': string;
  'newspaperImgWrapper': string;
  'packOfSausages': string;
  'page': string;
  'paragraph1': string;
  'paragraph2': string;
  'part1': string;
  'part2': string;
  'part3': string;
  'pdRight12': string;
  'pdRight5': string;
  'printedSentence': string;
  'printedSentenceWrapper': string;
  'right': string;
  'sales': string;
  'sausages': string;
  'scratches': string;
  'shopping': string;
  'shoppingBackets': string;
  'simpleEconomicsBg': string;
  'subBackground': string;
  'takings': string;
  'textTypo': string;
  'textWrapper': string;
  'thunderdome': string;
  'titleDivider': string;
  'tornPiece': string;
  'torned': string;
  'tornsTypo': string;
  'withHole': string;
  'withPaddingBottom': string;
  'withPaddingBottomSmall': string;
  'withPaddingTop': string;
  'withPaddingTopBig': string;
  'wrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
