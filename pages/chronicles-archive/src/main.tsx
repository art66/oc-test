import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import { HashRouter } from 'react-router-dom'
import rootStore from './store/createStore'
import AppContainer from './containers/AppContainer'
import history from './store/history'

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('chronicles-archives')

let render = () => {
  // @ts-ignore
  ReactDOM.render(<HashRouter><AppContainer store={rootStore} history={history} /></HashRouter>, MOUNT_NODE)
}

// ========================================================
// Developer Tools Setup
// ========================================================
if (__DEV__) {
  // @ts-ignore
  if (window.devToolsExtension) {
    window.state = rootStore.getState()
  }
}

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    const renderApp = render
    const renderError = error => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./containers/AppContainer', render)
  }
}

// ========================================================
// Go!
// ========================================================
render()
