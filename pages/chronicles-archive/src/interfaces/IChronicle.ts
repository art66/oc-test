export default interface IChronicle {
  id: number
  slug: string
  status: string
  title: string
}
