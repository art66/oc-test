export const fetchUrl = (url: string, data?: any) => {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then(response => {
      return response.text()
        .then(text => {
          let error
          let json

          try {
            json = JSON.parse(text)

            if (json && json.error) {
              error = json.error
            }
          } catch (e) {
            error = `Server responded with: ${text}`
          }

          if (error) {
            throw new Error(error.message)
          } else {
            return json
          }
        })
    })
}

export const isDesktopMode = () => {
  return document.getElementsByTagName('body')[0].classList.contains('r')
}

export const userIsJail = () => {
  return document.getElementsByTagName('body')[0].classList.contains('jail')
}

export const userIsHospital = () => {
  return document.getElementsByTagName('body')[0].classList.contains('hospital')
}

export const userIsTraveling = () => {
  return document.getElementById('computer-content-wrapper') &&
    document.getElementById('computer-content-wrapper').classList.contains('travelling')
}

export const userIsLoggedOut = () => {
  return document.getElementsByClassName('content-wrapper') &&
    document.getElementsByClassName('content-wrapper')[0].classList.contains('logged-out')
}
