import { useEffect, useState, useCallback } from 'react'
import { getCurrentTimeInSeconds } from '../utils'

export function useTimer(startTime: number, countdown: boolean) {
  const getTime = useCallback(
    timer => (countdown ? timer - getCurrentTimeInSeconds() : getCurrentTimeInSeconds() - timer),
    [countdown]
  )
  const [time, updateTime] = useState(getTime(startTime))

  useEffect(() => {
    const handleTimeUpdate = () => updateTime(getTime(startTime))
    let timer

    if (countdown) {
      if (time > 0) {
        timer = setInterval(handleTimeUpdate, 1000)
      }
    } else {
      timer = setInterval(handleTimeUpdate, 1000)
    }

    return () => clearInterval(timer)
  }, [countdown, getTime, startTime, time])

  return time
}
