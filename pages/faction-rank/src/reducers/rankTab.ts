import { Action, handleActions } from 'redux-actions'
import { TRank } from '@torn/shared/components/WarringTiers/types'
import TRankTabPayload, { TInitRankTabPayload } from './types'
import IEnlistmentPanel from '../interfaces/IEnlistmentPanel'
import IRankTab from '../interfaces/IRankTab'
import * as a from '../actions/actionTypes'

const reducer = handleActions<IRankTab, TRankTabPayload>(
  {
    [a.SET_INITIAL_DATA]: (state, action: Action<TInitRankTabPayload>) => {
      return {
        ...state,
        enlistmentPanel: action.payload.enlistmentPanel,
        currentUser: action.payload.currentUser,
        rankedTier: action.payload.rankedTier
      }
    },
    [a.SET_ENLISTMENT_PANEL]: (state, action: Action<IEnlistmentPanel>) => {
      return {
        ...state,
        enlistmentPanel: action.payload
      }
    },
    [a.UPDATE_RANK]: (state, action: Action<{ rank: TRank }>) => {
      return {
        ...state,
        rankedTier: {
          ...state.rankedTier,
          rank: action.payload.rank
        }
      }
    },
    [a.TOGGLE_UNENLIST_CONFIRMATION]: (state, action: Action<{ enabled: boolean }>) => {
      return {
        ...state,
        enlistmentPanel: {
          ...state.enlistmentPanel,
          unenlistConfirmationEnabled: action.payload.enabled
        }
      }
    }
  },
  {}
)

export default reducer
