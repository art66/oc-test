import { TRank } from '@torn/shared/components/WarringTiers/types'
import ICurrentUser from '../../interfaces/ICurrentUser'
import IEnlistmentPanel from '../../interfaces/IEnlistmentPanel'
import IRankedTier from '../../interfaces/IRankedTier'

export type TInitRankTabPayload = {
  currentUser: ICurrentUser
  enlistmentPanel: IEnlistmentPanel
  rankedTier: IRankedTier
}

export type TRankTabPayload = TInitRankTabPayload | IEnlistmentPanel | { rank: TRank } | { enabled: boolean }

export default TRankTabPayload
