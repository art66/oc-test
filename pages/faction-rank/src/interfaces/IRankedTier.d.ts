import { TRank } from '@torn/shared/components/WarringTiers/types'

export default interface IRankedTier {
  rank: TRank
}
