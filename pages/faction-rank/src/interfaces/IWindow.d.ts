export default interface IWindow extends Window {
  getCurrentTimestamp?: () => number
  WebsocketHandler?: (namespace: string, options?: { channel: string }) => void
}
