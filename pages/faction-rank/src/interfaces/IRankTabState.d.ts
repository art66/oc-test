import IRankTab from './IRankTab'

export default interface IRankTabState {
  rankTab: IRankTab
}
