export default interface ICurrentUser {
  canEnlist: boolean
}
