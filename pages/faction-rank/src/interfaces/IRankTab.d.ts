import IEnlistmentPanel from './IEnlistmentPanel'
import ICurrentUser from './ICurrentUser'
import IRankedTier from './IRankedTier'

export default interface IRankTab {
  enlistmentPanel?: IEnlistmentPanel
  currentUser?: ICurrentUser
  rankedTier?: IRankedTier
}
