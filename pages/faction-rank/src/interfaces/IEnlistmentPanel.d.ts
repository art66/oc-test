type TWarState = 'unenlisted' | 'membersRequirement' | 'matching' | 'cooldown' | 'countdown' | 'inProgress'

type TFactionInfo = {
  id: number
  name: string
}

export type TCriteria = {
  label: string
  state: 'done' | 'inProgress'
}

export default interface IEnlistmentPanel {
  state: TWarState
  currentFaction: TFactionInfo
  opponentFaction?: TFactionInfo
  timer?: number
  matchmaking?: {
    criteria: TCriteria[]
  }
  warning?: string | null
  unenlistConfirmationEnabled?: boolean
}
