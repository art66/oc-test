import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import { Provider } from 'react-redux'
import rootStore from './store/createStore'
import App from './components/App'

const MOUNT_NODE = document.getElementById('faction-rank-root')

let render = () => {
  ReactDOM.render(
    <Provider store={rootStore}>
      <App />
    </Provider>,
    MOUNT_NODE
  )
}

// This code is excluded from production bundle
// @ts-ignore
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    // eslint-disable-next-line global-require
    const renderApp = render
    const renderError = error => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        import('../../../modules/header/src/main')
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept()
  }
}

render()
