import IWindow from '../interfaces/IWindow'

declare let window: IWindow

export const getCurrentTimeInSeconds = () => {
  return Math.round(window.getCurrentTimestamp() / 1000)
}

export const plural = val => {
  return val === 1 ? '' : 's'
}

export const formatLongTime = time => {
  const days = Math.floor(time / (3600 * 24))
  const hours = Math.floor((time / 3600) % 24)
  const minutes = Math.floor((time / 60) % 60)
  const seconds = Math.floor(time % 60)
  let result = ''

  result += days > 0 ? `${days} day${plural(+days)}, ` : ''
  result += days > 0 || hours > 0 ? `${hours} hour${plural(+hours)}, ` : ''
  result += days > 0 || hours > 0 || minutes > 0 ? `${minutes} minute${plural(+minutes)} and ` : ''
  result += `${seconds} second${plural(+seconds)}`

  return result
}

export function fetchUrl(url: string, data?: any) {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      let json

      try {
        json = JSON.parse(text)

        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = `Server responded with: ${text}`
      }

      if (error) {
        throw new Error(error.message)
      } else {
        return json
      }
    })
  })
}

export const initRankedWarWS = (actionName, callback) => {
  const handler = new window.WebsocketHandler('factionRankedWars')

  handler.setActions({
    [actionName]: data => callback(data)
  })
}
