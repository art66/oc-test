import React from 'react'

const WarningIcon = () => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='20' height='18' viewBox='0 0 20 18'>
      <path
        d='M12,4,2,22H22ZM11,9h2v7H11Zm1,11.25A1.25,1.25,0,1,1,13.25,19,1.25,1.25,0,0,1,12,20.25Z'
        transform='translate(-2 -4)'
        fill='#ff7a4d'
        opacity='0.5'
      />
    </svg>
  )
}

export default WarningIcon
