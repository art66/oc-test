import { createSelector } from 'reselect'
import IRankTabState from '../interfaces/IRankTabState'

export const getEnlistmentPanel = (state: IRankTabState) => state.rankTab.enlistmentPanel
export const getCurrentUser = (state: IRankTabState) => state.rankTab.currentUser
export const getRank = (state: IRankTabState) => state.rankTab.rankedTier?.rank

export const getWarState = createSelector([getEnlistmentPanel], enlistmentPanel => enlistmentPanel.state)
export const getCurrentFaction = createSelector(
  [getEnlistmentPanel],
  enlistmentPanel => enlistmentPanel?.currentFaction
)
export const getOpponentFaction = createSelector(
  [getEnlistmentPanel],
  enlistmentPanel => enlistmentPanel.opponentFaction
)
export const getCriteria = createSelector(
  [getEnlistmentPanel],
  enlistmentPanel => enlistmentPanel.matchmaking?.criteria
)
export const getTimer = createSelector([getEnlistmentPanel], enlistmentPanel => enlistmentPanel.timer)
export const getWarning = createSelector([getEnlistmentPanel], enlistmentPanel => enlistmentPanel.warning)
export const getUnenlistConfirmationEnabled = createSelector(
  [getEnlistmentPanel],
  enlistmentPanel => enlistmentPanel.unenlistConfirmationEnabled
)

export const getCanEnlistValue = createSelector([getCurrentUser], currentUser => currentUser.canEnlist)
