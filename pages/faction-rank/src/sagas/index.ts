import { all } from 'redux-saga/effects'
import factionNewsSaga from '@torn/factionNews/src/sagas/factionNews'
import rankTab from './rankTab'

export default function* rootSaga() {
  yield all([rankTab(), factionNewsSaga()])
}
