import { takeEvery, put } from 'redux-saga/effects'
import { fetchUrl } from '../utils'
import { setEnlistmentPanel, setInitialData } from '../actions'
import * as a from '../actions/actionTypes'

function* fetchInitialData() {
  const data = yield fetchUrl('/page.php?sid=factionsRankedWarring', { step: 'init' })

  yield put(setInitialData(data.data))
}

function* toggleEnlistState(action) {
  const data = yield fetchUrl('/page.php?sid=factionsRankedWarring', {
    step: 'toggleEnlist',
    isEnlisted: action.payload.enlist
  })

  if (data.success) {
    yield put(setEnlistmentPanel(data.enlistmentPanel))
  }
}

function* fetchEnlistmentPanel() {
  const data = yield fetchUrl('/page.php?sid=factionsRankedWarring', { step: 'getEnlistingPanel' })

  if (data.success) {
    yield put(setEnlistmentPanel(data.enlistmentPanel))
  }
}

export default function* rankTab() {
  yield takeEvery(a.FETCH_INITIAL_DATA, fetchInitialData)
  yield takeEvery(a.TOGGLE_ENLIST_STATE, toggleEnlistState)
  yield takeEvery(a.FETCH_ENLISTMENT_PANEL, fetchEnlistmentPanel)
}
