import React, { useEffect } from 'react'
import cn from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import WarringTiers from '@torn/shared/components/WarringTiers'
import Preloader from '@torn/shared/components/Preloader'
import { getCurrentFaction, getRank } from '../selectors'
import { fetchInitialData, updateRank } from '../actions'
import RankedNews from './RankedNews'
import EnlistmentPanel from './EnlistmentPanel'
import s from './App.cssmodule.scss'
import '../styles/common.scss'

export const App = () => {
  const dispatch = useDispatch()
  const currentFaction = useSelector(getCurrentFaction)
  const rank = useSelector(getRank)

  useEffect(() => {
    window.addEventListener('onWarringTierUpdate', (e: CustomEvent) => dispatch(updateRank(e.detail?.rank)))
  }, [])

  useEffect(() => {
    dispatch(fetchInitialData())
  }, [])

  return (
    <div className='faction-rank-tab'>
      <hr className='delimiter-999 m-top10 m-bottom10' />
      <div className={s.panelsWrap}>
        <div className={s.enlistPanelWrap}>{currentFaction ? <EnlistmentPanel /> : <Preloader />}</div>
        <div className={s.tiersWrap}>
          {currentFaction ? (
            <>
              <WarringTiers
                size='big'
                factionId={currentFaction.id}
                warringTiersID={`faction-rank-${currentFaction.id}`}
              />
              <div className={cn(s.warringTiersGlowing, s[rank])} />
            </>
          ) : (
            <Preloader />
          )}
        </div>
      </div>
      <hr className='delimiter-999 m-top10 m-bottom10' />
      <RankedNews
        initParams={{
          url: '/page.php?sid=factionsNews&step=list',
          tabs: [{ name: 'Ranked News', type: 12, id: 'ranked_news' }]
        }}
      />
    </div>
  )
}

export default App
