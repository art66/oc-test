import React from 'react'
import { useDispatch } from 'react-redux'
import cn from 'classnames'
import { toggleUnenlistConfirmation, toggleEnlistState } from '../../actions'
import s from '../App.cssmodule.scss'

const UnenlistConfirmation = () => {
  const dispatch = useDispatch()

  const handleCancelAction = () => {
    dispatch(toggleUnenlistConfirmation(false))
  }

  const handleConfirmAction = () => {
    dispatch(toggleEnlistState(false))
  }

  return (
    <div className={s.unenlistConfirmation}>
      <div className={s.confirmMessage}>
        Are you sure you want to unenlist your faction from ranked warring? Any matchmaking progress will be lost.
      </div>
      <div>
        <button className={cn(s.confirmBtn, s.confirm)} type='button' onClick={handleConfirmAction}>
          Unenlist
        </button>
        <button className={cn(s.confirmBtn, s.unconfirm)} type='button' onClick={handleCancelAction}>
          Cancel
        </button>
      </div>
    </div>
  )
}

export default UnenlistConfirmation
