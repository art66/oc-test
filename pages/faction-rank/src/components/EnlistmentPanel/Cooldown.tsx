import React from 'react'
import cn from 'classnames'
import { useDispatch } from 'react-redux'
import { formatLongTime } from '../../utils'
import { toggleUnenlistConfirmation } from '../../actions'
import { useTimer } from '../../hooks/useTimer'
import s from '../App.cssmodule.scss'

interface IProps {
  factionName: string
  timer: number
  canEnlist: boolean
  factionID: number
}

const Cooldown = (props: IProps) => {
  const dispatch = useDispatch()
  const time = useTimer(props.timer, true)

  const handleUnenlistPanel = () => {
    if (props.canEnlist) {
      dispatch(toggleUnenlistConfirmation(true))
    }
  }

  return (
    <div className={cn(s.messageWrapper, s.cooldown)}>
      <div className={s.message}>
        <a href={`/factions.php?step=profile&ID=${props.factionID}`} className={s.factionName}>
          {props.factionName}
        </a>{' '}
        is enlisted in ranked warring
      </div>
      <div className={s.timerLabel}>Matchmaking will begin in...</div>
      <div className={s.timer}>{formatLongTime(time >= 0 ? time : 0)}</div>
      <button type='button' className='torn-btn' disabled={!props.canEnlist} onClick={handleUnenlistPanel}>
        UNENLIST
      </button>
    </div>
  )
}

export default Cooldown
