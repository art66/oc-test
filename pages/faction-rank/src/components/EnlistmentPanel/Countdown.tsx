import React, { useEffect } from 'react'
import cn from 'classnames'
import { useDispatch } from 'react-redux'
import { formatLongTime } from '../../utils'
import { useTimer } from '../../hooks/useTimer'
import { fetchEnlistmentPanel } from '../../actions'
import s from '../App.cssmodule.scss'

interface IProps {
  factionName: string
  factionID: number
  opponentFactionID: number
  opponentFactionName: string
  timer: number
}

const Countdown = (props: IProps) => {
  const dispatch = useDispatch()
  const time = useTimer(props.timer, true)

  useEffect(() => {
    if (time <= 0) {
      dispatch(fetchEnlistmentPanel())
    }
  }, [dispatch, time])

  return (
    <div className={cn(s.messageWrapper, s.countdown)}>
      <div className={s.message}>
        The ranked war between
        <br />
        <a href={`/factions.php?step=profile&ID=${props.factionID}`} className={s.factionName}>
          {props.factionName}
        </a>{' '}
        and{' '}
        <a href={`/factions.php?step=profile&ID=${props.opponentFactionID}`} className={s.factionName}>
          {props.opponentFactionName}
        </a>
        <br /> will begin shortly
      </div>
      {time >= 0 ? (
        <div className={s.timer}>{formatLongTime(time)}</div>
      ) : (
        <div className={s.timer}>{formatLongTime(0)}</div>
      )}
    </div>
  )
}

export default Countdown
