import React from 'react'
import { useDispatch } from 'react-redux'
import { toggleEnlistState } from '../../actions'
import s from '../App.cssmodule.scss'

interface IProps {
  factionName: string
  canEnlist: boolean
  factionID: number
}

const NotEnlisted = (props: IProps) => {
  const dispatch = useDispatch()

  const handleEnlistPanel = () => {
    if (props.canEnlist) {
      dispatch(toggleEnlistState(true))
    }
  }

  return (
    <div className={s.messageWrapper}>
      <div className={s.message}>
        <a href={`/factions.php?step=profile&ID=${props.factionID}`} className={s.factionName}>
          {props.factionName}
        </a>{' '}
        is not currently enlisted in ranked warring
      </div>
      <button type='button' className='torn-btn' disabled={!props.canEnlist} onClick={handleEnlistPanel}>
        ENLIST
      </button>
    </div>
  )
}

export default NotEnlisted
