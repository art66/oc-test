import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Tooltip from '@torn/shared/components/Tooltip'
import {
  getCanEnlistValue,
  getCriteria,
  getCurrentFaction,
  getOpponentFaction,
  getTimer,
  getUnenlistConfirmationEnabled,
  getWarning,
  getWarState
} from '../../selectors'
import { initRankedWarWS } from '../../utils'
import { setEnlistmentPanel } from '../../actions'
import NotEnlisted from './NotEnlisted'
import Matching from './Matching'
import MembersRequirement from './MembersRequirement'
import Countdown from './Countdown'
import InProgress from './InProgress'
import Cooldown from './Cooldown'
import WarningIcon from '../../icons/Warning'
import UnenlistConfirmation from './UnenlistConfirmation'
import s from '../App.cssmodule.scss'

const EnlistmentPanel = () => {
  const dispatch = useDispatch()
  const warState = useSelector(getWarState)
  const currentFaction = useSelector(getCurrentFaction)
  const opponentFaction = useSelector(getOpponentFaction)
  const canEnlist = useSelector(getCanEnlistValue)
  const criteria = useSelector(getCriteria)
  const timer = useSelector(getTimer)
  const warning = useSelector(getWarning)
  const unenlistConfirmationEnabled = useSelector(getUnenlistConfirmationEnabled)
  const tipStyles = {
    style: {
      background: '#444',
      boxShadow: '0px 0px 2px rgba(0, 0, 0, 0.65)',
      color: '#fff',
      textAlign: 'center',
      lineHeight: '14px'
    },
    arrowStyle: {
      color: '#444',
      borderColor: 'rgba(0, 0, 0, 0.2)'
    }
  }

  useEffect(() => {
    initRankedWarWS('updateEnlistmentPanel', data => dispatch(setEnlistmentPanel(data)))
  }, [])

  const renderPanel = () => {
    if (unenlistConfirmationEnabled) {
      return <UnenlistConfirmation />
    }

    switch (warState) {
      case 'unenlisted':
        return <NotEnlisted factionID={currentFaction.id} factionName={currentFaction.name} canEnlist={canEnlist} />
      case 'matching':
        return (
          <Matching
            factionID={currentFaction.id}
            factionName={currentFaction.name}
            criteria={criteria}
            timer={timer}
            canEnlist={canEnlist}
          />
        )
      case 'membersRequirement':
        return <MembersRequirement factionID={currentFaction.id} factionName={currentFaction.name} />
      case 'countdown':
        return (
          <Countdown
            factionID={currentFaction.id}
            factionName={currentFaction.name}
            opponentFactionID={opponentFaction.id}
            opponentFactionName={opponentFaction.name}
            timer={timer}
          />
        )
      case 'inProgress':
        return (
          <InProgress
            factionID={currentFaction.id}
            factionName={currentFaction.name}
            opponentFactionID={opponentFaction.id}
            opponentFactionName={opponentFaction.name}
          />
        )
      case 'cooldown':
        return (
          <Cooldown
            factionID={currentFaction.id}
            factionName={currentFaction.name}
            timer={timer}
            canEnlist={canEnlist}
          />
        )
      default:
        return null
    }
  }

  return (
    <>
      {warning ? (
        <div id='ranked-warring-warning' className={s.warning}>
          <WarningIcon />
          <Tooltip style={tipStyles} parent='ranked-warring-warning' arrow='center' position='top'>
            <span className={s.warningTooltipText} dangerouslySetInnerHTML={{ __html: warning }} />
          </Tooltip>
        </div>
      ) : null}
      {renderPanel()}
    </>
  )
}

export default EnlistmentPanel
