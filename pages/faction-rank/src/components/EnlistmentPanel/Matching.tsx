import React, { useEffect } from 'react'
import cn from 'classnames'
import { useDispatch } from 'react-redux'
import lottie from 'lottie-web'
import loader from '../../svgAnimations/rank_loader.json'
import { TCriteria } from '../../interfaces/IEnlistmentPanel'
import { formatLongTime } from '../../utils'
import { toggleUnenlistConfirmation } from '../../actions'
import { useTimer } from '../../hooks/useTimer'
import s from '../App.cssmodule.scss'

interface IProps {
  factionName: string
  criteria: TCriteria[]
  timer: number
  canEnlist: boolean
  factionID: number
}

const Matching = (props: IProps) => {
  const dispatch = useDispatch()
  const preloaderRef = React.useRef()
  const time = useTimer(props.timer, false)

  const handleUnenlistPanel = () => {
    if (props.canEnlist) {
      dispatch(toggleUnenlistConfirmation(true))
    }
  }

  useEffect(() => {
    lottie.loadAnimation({
      container: preloaderRef.current,
      animationData: loader
    })
  }, [])

  return (
    <div className={cn(s.messageWrapper, s.matching)}>
      <div className={cn(s.message, s.success)}>
        <a href={`/factions.php?step=profile&ID=${props.factionID}`} className={s.factionName}>
          {props.factionName}
        </a>{' '}
        is currently being
        <br /> matched with an opponent
      </div>
      <div ref={preloaderRef} className={s.preloader} />
      <ul className={s.criteriaWrapper}>
        {props.criteria.map(item => {
          return (
            <li key={item.label}>
              <span className={cn(s.criteriaIcon, s[item.state])} />
              <span className={s.criteriaText} dangerouslySetInnerHTML={{ __html: item.label }} />
            </li>
          )
        })}
      </ul>
      <div className={s.timer}>{formatLongTime(time >= 0 ? time : 0)}</div>
      <button type='button' className='torn-btn' disabled={!props.canEnlist} onClick={handleUnenlistPanel}>
        UNENLIST
      </button>
    </div>
  )
}

export default Matching
