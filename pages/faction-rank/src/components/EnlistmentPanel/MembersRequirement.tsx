import React from 'react'
import s from '../App.cssmodule.scss'

interface IProps {
  factionName: string
  factionID: number
}

const MembersRequirement = (props: IProps) => {
  return (
    <div className={s.messageWrapper}>
      <div className={s.message}>
        <a href={`/factions.php?step=profile&ID=${props.factionID}`} className={s.factionName}>
          {props.factionName}
        </a>{' '}
        requires at least 10 full members online in the last 24 hours to enlist in ranked warring
      </div>
    </div>
  )
}

export default MembersRequirement
