import React from 'react'
import cn from 'classnames'
import s from '../App.cssmodule.scss'

interface IProps {
  factionName: string
  factionID: number
  opponentFactionID: number
  opponentFactionName: string
}

const InProgress = (props: IProps) => {
  return (
    <div className={cn(s.messageWrapper, s.progress)}>
      <div className={s.message}>
        The Ranked War between{' '}
        <a className={s.factionName} href={`/factions.php?step=profile&ID=${props.factionID}`}>
          {props.factionName}
        </a>{' '}
        and
        <a href={`/factions.php?step=profile&ID=${props.opponentFactionID}`} className={s.factionName}>
          {' '}
          {props.opponentFactionName}
        </a>{' '}
        is in progress
      </div>
    </div>
  )
}

export default InProgress
