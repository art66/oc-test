import { createAction } from 'redux-actions'
import { TInitRankTabPayload } from '../reducers/types'
import IEnlistmentPanel from '../interfaces/IEnlistmentPanel'
import * as a from './actionTypes'

export const fetchInitialData = createAction(a.FETCH_INITIAL_DATA)
export const setInitialData = createAction<TInitRankTabPayload, TInitRankTabPayload>(a.SET_INITIAL_DATA, data => data)
export const toggleEnlistState = createAction<{ enlist: boolean }, boolean>(a.TOGGLE_ENLIST_STATE, enlist => ({
  enlist
}))
export const setEnlistmentPanel = createAction<IEnlistmentPanel, IEnlistmentPanel>(a.SET_ENLISTMENT_PANEL, data => data)
export const updateRank = createAction<{ rank: string }, string>(a.UPDATE_RANK, rank => ({ rank }))
export const fetchEnlistmentPanel = createAction(a.FETCH_ENLISTMENT_PANEL)
export const toggleUnenlistConfirmation = createAction(a.TOGGLE_UNENLIST_CONFIRMATION, (enabled: boolean) => ({
  enabled
}))
