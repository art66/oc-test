import { handleActions } from 'redux-actions'
import * as actionTypes from '../actions/actionTypes'

const initialState = {
  activeTab: 'goose-juice'
}

const ACTION_HANDLERS = {
  [actionTypes.SET_CANS_DATA]: (state, action) => {
    return {
      ...state,
      cans: action.payload
    }
  },
  [actionTypes.SEND_CANS_DATA]: (state, action) => {
    return {
      ...state,
      ...action.payload,
      can: action.payload
    }
  },
  [actionTypes.ACTIVATE_TAB]: (state, action) => {
    return {
      ...state,
      ...action.payload,
      activeTab: action.payload
    }
  },
  [actionTypes.UPDATE_CAN_DATA]: (state, action) => {
    return {
      ...state,
      cans: state.cans.map(can => {
        return can.name === action.payload.name ? { ...can, ...action.payload.data } : can
      })
    }
  }
}

export default handleActions(ACTION_HANDLERS, initialState)
