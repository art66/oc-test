import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import cans from './cans'

const browser = createResponsiveStateReducer({
  mobile: 600,
  tablet: 1000,
  desktop: 5000
})

export default combineReducers({ browser, cans })
