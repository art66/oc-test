import { createAction } from 'redux-actions'
import * as actionTypes from './actionTypes'

export const fetchCansData = createAction(actionTypes.FETCH_CANS_DATA)
export const fetchCanData = createAction(actionTypes.FETCH_CAN_DATA)
export const setCansData = createAction(actionTypes.SET_CANS_DATA, data => data)
export const sendCansData = createAction(actionTypes.SEND_CANS_DATA, data => data)
export const activateTab = createAction(actionTypes.ACTIVATE_TAB, data => data)
export const updateCanData = createAction(actionTypes.UPDATE_CAN_DATA, (name, data) => ({ name, data }))
