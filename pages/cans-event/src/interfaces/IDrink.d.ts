export default interface IDrink {
  name: string,
  timestamp: number | null,
  title: string,
  animationOn?: boolean
}
