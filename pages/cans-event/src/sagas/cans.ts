import { put, takeEvery } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import * as actionTypes from '../actions/actionTypes'
import { setCansData, updateCanData } from '../actions'
import { TIMER_DELAY } from '../constants'
import { fetchUrl } from '../utils'

export default interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* fetchCansData() {
  const data = yield fetchUrl('cans_event.php?step=getCans')
  yield put(setCansData(data.cans))
}

function* stopAnimation(name: string) {
  yield delay(TIMER_DELAY)
  yield put(updateCanData(name, { animationOn: false }))
}

function* fetchCanData(action: any) {
  const data = yield fetchUrl('cans_event.php?step=claimCan', { can: action.payload })
  if (data.success) {
    yield put(updateCanData(action.payload.name, { ...data.can, animationOn: true }))
    yield stopAnimation(action.payload.name)
  }
}

export default function* cans() {
  yield takeEvery(actionTypes.FETCH_CANS_DATA, fetchCansData)
  yield takeEvery(actionTypes.FETCH_CAN_DATA, fetchCanData)
}
