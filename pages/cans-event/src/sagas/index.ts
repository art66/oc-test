import { all } from 'redux-saga/effects'
import cans from './cans'

export default function* rootSaga() {
  yield all([cans()])
}
