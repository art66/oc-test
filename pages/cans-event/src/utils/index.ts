export function fetchUrl(url: string, data?: any) {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      let json

      try {
        json = JSON.parse(text)

        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = 'Server responded with: ' + text
      }

      if (error) {
        throw new Error(error.message)
      } else {
        return json
      }
    })
  })
}

export const addLeadZero = (time: number) => {
  return time < 10 ? '0' + time : time
}
