import React from 'react'

import s from './styles.cssmodule.scss'

export default function Footer() {
  return (
    <section className={s.cansEventFooter}>
      <div className={s.headerWrapper}>
        <span className={s.corner} />
        <div className={s.titleWrapper}>
          <h4 className={s.title}>
            <span className={s.titleText}>Munster Energy Corp<sup>&reg;</sup></span>
            <span className={s.lineSeparate} />
          </h4>
        </div>
      </div>
      <div className={s.contentWrapper}>
        <div className={s.description}>
          <p>
            From the company that brought you <strong>Taurine Elite</strong>, <strong>Red Cow</strong> and{' '}
            <strong>X-Mass</strong> comes a range of brand new energy drinks aimed at the conscientious millennial
            consumer. <strong>Crocozade</strong>, <strong>Goose Juice</strong> and <strong>Damp Valley</strong> contain
            only the finest ingredients found naturally occuring in nature and laboratories, and thanks to their reduced
            levels of <em>white phosphorous</em>, each beverage has been proven to have only minimal* impact upon the
            human gastrointestinal system.
          </p>
          <p>
            This means our cans are now certified as <em>“legally safe to drink”</em> by the nations of Syria, North
            Korea and the Democratic Republic of Timor-Leste**. Unfortunately we’ve had to reduce the energy provided by
            our new products, but that just means you’ll have to guzzle even more of them! We think you’ll love our
            delicious new flavours regardless, and to promote our new products, we’re giving away free samples to the
            fine people of Torn. Please claim your cans by clicking the <strong>Claim</strong> button above. You may
            take more than one can when your timer reaches zero. This promotional offering ends on 10th September 13:00
            TCT.
          </p>
          <p>
            We’re also asking you for slogan suggestions for each of our new can products. To enter, please{' '}
            <a href='/forums.php#/p=threads&f=62&t=16115775'>click here</a> and post your slogan suggestions in the
            Community Events forum. The writers of
            the three winning slogans will each receive <strong>a lifetime’s supply</strong> of that particular can***.
          </p>
          <span className={s.lineSeparate} />
        </div>
        <div className={s.items}>
          <ul className={s.listItems}>
            <li>
              <span>*1 case of multiple organ failure per 1,000 drinkers.</span>
            </li>
            <li>
              <span>**Pending approval from his excellency Prime Minister Taur Matan Ruak.</span>
            </li>
            <li>
              <span>
                ***Note, lifetime duration is defined as no more than three years due to repeated consumption of product
                and average Torn lifespan.
              </span>
            </li>
          </ul>
          <img className={s.qrCode} src='/images/v2/cans-event/footer/made_in_torn.svg' alt='QR-code - made in Torn' />
        </div>
      </div>
    </section>
  )
}
