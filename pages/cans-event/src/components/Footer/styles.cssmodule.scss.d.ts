// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'cansEventFooter': string;
  'contentWrapper': string;
  'corner': string;
  'description': string;
  'globalSvgShadow': string;
  'headerWrapper': string;
  'items': string;
  'lineSeparate': string;
  'listItems': string;
  'qrCode': string;
  'title': string;
  'titleText': string;
  'titleWrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
