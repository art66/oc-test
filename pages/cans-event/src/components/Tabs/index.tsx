import React from 'react'
import './styles.cssmodule.scss'
import cn from 'classnames'
import { connect } from 'react-redux'
import Drink from '../Drink'
import Tab from './Tab'
import { activateTab } from '../../actions'
import IDrink from '../../interfaces/IDrink'
import s from './styles.cssmodule.scss'

interface IProps {
  cans: {
    activeTab: string,
    cans: IDrink[]
  },
  tabs: IDrink[],
  activateTab: (name: string) => void
}

class Tabs extends React.Component<IProps> {
  getDrinks = () => {
    const { cans } = this.props

    return cans.cans.map(can => {
      return (
        <Drink
          key={can.name}
          item={can}
        />
      )
    })
  }

  getTabs = () => {
    const { tabs, activateTab, cans } = this.props

    return tabs.map((tab, i) => {
      const tabClass = cn({
        [s.last]: i === 0,
        [s.first]: i === (tabs.length - 1),
        [s.active]: cans.activeTab === tab.name
      })

      return (
        <Tab
          key={tab.name}
          tabClass={tabClass}
          tab={tab}
          activateTab={activateTab}
        />
      )
    })
  }

  render() {
    return (
      <div>
        <ul className={`${s.tabsList}`}>
          {this.getTabs()}
        </ul>
        <div>
          <ul className={s.drinksList}>
            {this.getDrinks()}
          </ul>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  cans: state.cans
})

const mapDispatchToProps = {
  activateTab
}

export default connect(mapStateToProps, mapDispatchToProps)(Tabs)
