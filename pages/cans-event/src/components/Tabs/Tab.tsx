import React, { Component } from 'react'
import './styles.cssmodule.scss'
import s from './styles.cssmodule.scss'

interface IProps {
  tab: {
    name: string,
    title: string
  },
  tabClass: string,
  activateTab: (name: string) => void
}

class Tab extends Component<IProps> {
  _handleClick = (e: any) => {
    const { tab, activateTab } = this.props

    activateTab(tab.name)
    e.preventDefault()
  }

  render() {
    const { tabClass, tab } = this.props

    return (
      <li
        role='presentation'
        key={tab.name}
        className={tabClass}
      >
        <button
          type='button'
          role='tab'
          onClick={this._handleClick}
          className={s.tab}
        >
          <div className={`${s.l} left`} />
          <span>{tab.title}</span>
          <div className={`${s.r} left`} />
          <div className={`${s.clear}`} />
          <div className={`${s.bg}`} />
        </button>
      </li>
    )
  }
}

export default Tab
