import React from 'react'
import { connect } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'
import Header from './Header'
import Footer from './Footer'
import Drinks from './Drinks'
import { fetchCansData } from '../actions'
import s from './style.cssmodule.scss'

interface IProps {
  browser: any,
  fetchCansData: () => void,
  cans: {
    cans: any
  }
}

class App extends React.Component<IProps> {
  componentDidMount(): void {
    const { fetchCansData } = this.props

    fetchCansData()
  }

  render() {
    const { cans } = this.props

    if (cans.cans) {
      return (
        <>
          <Header />
          <Drinks />
          <Footer />
        </>
      )
    }

    return <div className={s.preloaderWrapper}><Preloader /></div>
  }
}

const mapStateToProps = state => ({
  cans: state.cans
})

const mapDispatchToProps = {
  fetchCansData
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
