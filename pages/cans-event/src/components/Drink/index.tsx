import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Timer from '../Timer'
import { fetchCanData, updateCanData } from '../../actions'
import s from './styles.cssmodule.scss'

class Drink extends React.Component<any, any> {
  _handleClick = () => {
    const { fetchCanData, item } = this.props

    fetchCanData({ name: item.name })
  }

  isTimestampExist = () => {
    const { item, updateCanData } = this.props

    return item.timestamp ? (
      <Timer
        item={item}
        updateCanData={updateCanData}
      />
    ) : (
      <button
        type='button'
        onClick={this._handleClick}
        className={s[item.name]}
      >
        Claim
      </button>
    )
  }

  render() {
    const { items, item } = this.props
    const { activeTab } = items
    const drinkCn = cn({
      [s[item.name]]: true,
      [s.show]: activeTab === item.name,
      [s.hidden]: activeTab !== item.name,
      [s.active]: item.animationOn
    })

    return (
      <li className={drinkCn}>
        <div className={`${s.cansAction} ${s[item.name]}`}>
          <div className={`${s.claim} ${s[item.name]} `}>{this.isTimestampExist()}</div>
        </div>
      </li>
    )
  }
}

const mapStateToProps = state => ({
  items: state.cans,
  browser: state.browser
})

const mapDispatchToProps = {
  fetchCanData,
  updateCanData
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Drink)
