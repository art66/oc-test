import React from 'react'
import { connect } from 'react-redux'
import Drink from '../Drink'
import Tabs from '../Tabs'
import IDrink from '../../interfaces/IDrink'
import s from './styles.cssmodule.scss'

interface IProps {
  items: {
    cans: IDrink[]
  },
  browser: {
    lessThan: {
      desktop: boolean
    }
  }
}

class Drinks extends React.Component<IProps> {
  drinkItems = () => {
    const { items } = this.props

    return items.cans.map(item => (
      <Drink
        key={item.name}
        item={item}
      />
    ))
  }

  responsiveEnabled = () => {
    return document.body.classList.contains('r')
  }

  render() {
    const { browser, items } = this.props
    const isTablet = browser.lessThan.desktop && this.responsiveEnabled()

    return (
      <section className={s.cansEventDrinks}>
        {isTablet ? <Tabs tabs={items.cans} /> : <ul className={s.drinksList}>{this.drinkItems()}</ul>}
      </section>
    )
  }
}
const mapStateToProps = state => ({
  items: state.cans,
  browser: state.browser
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Drinks)
