import React from 'react'

import s from './styles.cssmodule.scss'

export default function Header() {
  return (
    <section className={s.cansEventHeader}>
      <div className={s.description}>
        <span className={s.pageHeadDelimiter} />
        <h5>Feeling thirsty?</h5>
        <p>
          Then why not try one of our brand new energy drinks for free! And if you like the
          taste, <span className={s.secondTextPart}>come back when your timer runs out for another free sample!</span>
        </p>
      </div>
    </section>
  )
}
