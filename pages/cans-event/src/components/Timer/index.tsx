import React from 'react'
import { addLeadZero } from '../../utils'
import { SECOND_IN_MS } from '../../constants'
import IDrink from '../../interfaces/IDrink'

import s from './styles.cssmodule.scss'

interface IProps {
  item: IDrink
  updateCanData: (name: string, object: { timestamp: number }) => void
}

interface IState {
  timestamp: number
}

export default class Timer extends React.Component<IProps, IState> {
  countDownTimer: any

  constructor(props: any) {
    super(props)
    const { item } = this.props

    this.state = {
      timestamp: item.timestamp ? item.timestamp * SECOND_IN_MS : 0
    }
  }

  formatTime() {
    const { timestamp } = this.state
    const date = new Date(timestamp)
    const hours = addLeadZero(date.getUTCHours())
    const min = addLeadZero(date.getUTCMinutes())
    const sec = addLeadZero(date.getUTCSeconds())
    const splitHours = `${hours}`.split('')
    const splitMin = `${min}`.split('')
    const splitSec = `${sec}`.split('')
    const [firstH, secondH] = splitHours
    const [firstM, secondM] = splitMin
    const [firstS, secondS] = splitSec

    return (
      <div>
        <span>{firstH}</span><span>{secondH}</span>:
        <span>{firstM}</span><span>{secondM}</span>:
        <span>{firstS}</span><span>{secondS}</span>
      </div>
    )
  }

  updateTime() {
    clearInterval(this.countDownTimer)
    this.countDownTimer = setInterval(this.decTimer, SECOND_IN_MS)
  }

  componentDidMount() {
    const { item } = this.props

    if (item.timestamp && item.timestamp > 0) {
      this.updateTime()
    }
  }

  getReceivedMsg = () => {
    const { item } = this.props

    return (
      <div className={`${s.receivedCanMsg} ${s[item.name]}`}>
        <span>you have received a free can</span>
        <span>of {item.title}</span>
      </div>
    )
  }

  getTimerMessage = () => {
    const { item } = this.props

    return (
      <div className={`${s.receivedTimer} ${s[item.name]}`}>
        <div className={s.timerMessage}>
          <span>new free can</span>
          <span>will be available in</span>
        </div>
        <span className={s.timer}>{item.timestamp ? this.formatTime() : ''}</span>
        <div className={s.timeLength}>
          <span>Hour</span>
          <span>Min</span>
          <span>Sec</span>
        </div>
      </div>
    )
  }

  decTimer = () => {
    const { timestamp } = this.state
    const { updateCanData, item } = this.props

    if (timestamp) {
      this.setState({
        timestamp: timestamp - SECOND_IN_MS
      })
    } else {
      clearInterval(this.countDownTimer)
      updateCanData(item.name, { timestamp })
    }
  }

  componentWillUnmount() {
    clearInterval(this.countDownTimer)
  }

  render() {
    const { item } = this.props

    return item && !item.animationOn ? this.getTimerMessage() : this.getReceivedMsg()
  }
}
