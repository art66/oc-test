import React from 'react'
import ReactDOM from 'react-dom'
import { createMemoryHistory } from 'react-router'
import createStore from './store/createStore'
import AppContainer from './containers/AppContainer'

const initialState = window.___INITIAL_STATE__
const store = createStore(initialState)
const MOUNT_NODE = document.getElementById('newspaperroot')

let render = () => {
  const routes = require('./routes/index').default(store)

  ReactDOM.render(<AppContainer store={store} history={createMemoryHistory()} routes={routes} />, MOUNT_NODE)
}

if (__DEV__) {
  if (window.devToolsExtension) {
    // window.devToolsExtension.open()
  }
}

if (__DEV__) {
  if (module.hot) {
    const renderApp = render
    const renderError = error => {
      const RedBox = require('redbox-react').default

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    module.hot.accept('./routes/index', () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE)
        render()
      })
    )
  }
}

render()
