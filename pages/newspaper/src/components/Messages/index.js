import React, { Component } from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'
import { fetchUrl } from '../../utils'

class Messages extends Component {
  constructor(props) {
    super(props)
    this.state = { messages: [] }
    this.loadMessages()
  }

  loadMessages = () => {
    if (!this.state.messages.length) {
      fetchUrl('step=getMessages').then(messages => this.setState({ messages }))
    }
  }

  getMessages = () =>
    this.state.messages.map((message, index) => (
      <div key={index} className={s.message}>
        <div className={s.title}>
          <a href={`/profiles.php?XID=${message.userID}`}>
            {message.playername} [{message.userID}]
          </a>
        </div>
        <p dangerouslySetInnerHTML={{ __html: message.message }} />
      </div>
    ))

  render() {
    const messages = this.getMessages()

    return <div className={cn(s.messages, 'clearfix')}>{messages}</div>
  }
}

export default Messages
