import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import FrontPage from './FrontPage'
import Article from './Article'
import Archive from './Archive'
import NewSubmission from './NewSubmission'

export const createRoutes = store => ({
  path: '/',
  component: CoreLayout,
  indexRoute: FrontPage(store),
  childRoutes: [Article(store), Archive(store), NewSubmission(store)]
})

export default createRoutes
