import { createAction, handleActions } from 'redux-actions'
import { unionBy, orderBy } from 'lodash'
import { fetchUrl } from '../../../utils'
const LAZY_LOAD_ARTICLES_LENGTH = 10

const initialState = {
  articles: [],
  loading: false,
  allArticlesLoaded: false
}

export const setArchiveState = createAction('set archive state', data => ({ data }))
export const loading = createAction('loading', (loading = true) => ({ loading }))
export const setArticleStatus = createAction('set article status', (articleID, status) => ({ articleID, status }))

export const loadArchive = range => (dispatch, getState) => {
  dispatch(loading(true))
  const articles = getState().archive.articles

  return fetchUrl('step=getArchive', { range })
    .then(data => {
      dispatch(
        setArchiveState({
          articles: orderBy(unionBy(data, articles, 'articleID'), 'published', 'desc'),
          allArticlesLoaded: data.length === 0
        })
      )
      dispatch(loading(false))
    })
    .catch(error => console.error(error))
}

export const updateArchive = () => (dispatch, getState) => {
  const articles = getState().archive.articles
  const articlesIDs = articles.map(article => article.articleID)

  return fetchUrl('step=getArchiveForUpdate', { articles: articlesIDs })
    .then(response => {
      dispatch(
        setArchiveState({
          articles: articles.map(article => ({
            ...article,
            ...response.find(item => item.articleID === article.articleID)
          }))
        })
      )
    })
    .catch(error => console.error(error))
}

export const lazyLoad = () => (dispatch, getState) => {
  let articlesLength = getState().archive.articles.length
  dispatch(loadArchive([articlesLength, articlesLength + LAZY_LOAD_ARTICLES_LENGTH]))
}

export default handleActions(
  {
    'set archive state'(state, action) {
      return {
        ...state,
        ...action.payload.data
      }
    },
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'set article status'(state, action) {
      return {
        ...state,
        articles: state.articles.map(article => ({
          ...article,
          status: article.articleID === action.payload.articleID ? action.payload.status : article.status
        }))
      }
    }
  },
  initialState
)
