import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'archive',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Archive = require('./containers/ArchiveContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'archive', reducer })
        cb(null, Archive)
      },
      'archive'
    )
  }
})
