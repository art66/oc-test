import { connect } from 'react-redux'
import { loadArchive, lazyLoad, setArticleStatus, updateArchive } from '../modules'
import Archive from '../components/Archive'

const mapDispatchToProps = {
  loadArchive,
  lazyLoad,
  setArticleStatus,
  updateArchive
}

const mapStateToProps = state => ({
  articles: state.archive.articles,
  allArticlesLoaded: state.archive.allArticlesLoaded,
  loading: state.archive.loading,
  mediaType: state.browser.mediaType
})

export default connect(mapStateToProps, mapDispatchToProps)(Archive)
