import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Block from '../../../components/Block'
import Preloader from '@torn/shared/components/Preloader'
import Messages from '../../../components/Messages'
import Tooltip from '@torn/shared/components/Tooltip'
import { truncate } from '../../../utils'
import cn from 'classnames'
import s from './styles.cssmodule.scss'
/* global $ */

class Archive extends Component {
  componentDidMount() {
    this.computerScrollbar = document.querySelector('.computer-wrap .scrollbar-inner.scroll-content')

    if (!this.props.articles.length) {
      this.props.loadArchive([0, 10])
    } else {
      this.props.updateArchive()
    }
    window.addEventListener('scroll', this.lazyLoad)

    if (this.computerScrollbar) {
      this.computerScrollbar.addEventListener('scroll', this.lazyLoad)
    }

    this.setActiveHeaderLink(window.location.href)
    this.props.router.listen(location => {
      if (location.action === 'PUSH') {
        this.setActiveHeaderLink(location.pathname)
      }
    })
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.lazyLoad)

    if (this.computerScrollbar) {
      this.computerScrollbar.removeEventListener('scroll', this.lazyLoad)
    }
  }

  setActiveHeaderLink = pathname => {
    let href =
      pathname.indexOf('archive') !== -1 || pathname.indexOf('articles') !== -1
        ? 'newspaper.php#/archive'
        : 'newspaper.php'
    let $linksList = $('.newspaper-body .page-wrap ul.page')
    $linksList.find('li').removeClass('active')
    $linksList
      .find('a[href="' + href + '"]')
      .closest('ul > li')
      .addClass('active')
  }

  lazyLoad = () => {
    const { loading, allArticlesLoaded, lazyLoad } = this.props
    const scroll = this.computerScrollbar
      ? (this.computerScrollbar.scrollTop + this.computerScrollbar.clientHeight)
      : (window.scrollY + window.outerHeight)

    if (!loading && !allArticlesLoaded && scroll > this.archiveElement.clientHeight) {
      lazyLoad()
    }
  }

  truncate = (text = '') => {
    const { mediaType } = this.props
    let length = mediaType === 'desktop' ? 330 : 160

    return truncate(text, length)
  }

  getArticles = () =>
    this.props.articles.map(article => (
      <div key={article.articleID} className={cn(s.article, s[article.status])}>
        <div className={s.innerWrap}>
          <div className={s.imageWrap}>
            <a
              href={`#!/articles/${article.articleID}`}
              onClick={() => this.props.setArticleStatus(article.articleID, 'read')}
            >
              <div
                className={s.image}
                style={{
                  backgroundImage: `url(${article.image || '/images/v2/newspaper/image_placeholder.jpg'})`
                }}
              />
            </a>
          </div>
          <div className={s.titleWrap}>
            <a
              href={`#!/articles/${article.articleID}`}
              className={cn(s.title, 't-overflow')}
              dangerouslySetInnerHTML={{ __html: article.title }}
              onClick={() => this.props.setArticleStatus(article.articleID, 'read')}
            />
            <a
              href={`#!/articles/${article.articleID}`}
              className={s.subtitle}
              onClick={() => this.props.setArticleStatus(article.articleID, 'read')}
            >
              {this.truncate(article.text)}
            </a>
            <div className={s.info}>
              Report by
              <a href={`/profiles.php?XID=${article.author.userID}`} className={s.link}>
                {article.author.playername}
              </a>
              on {article.publishedDate} ({article.publishedTimeAgo})
            </div>
          </div>
          <div className={s.views} id={'viewsCounter' + article.articleID}>
            <i className={s.viewsIcon} />
            <span className={s.counter}>{article.views || 0}</span>
            <Tooltip parent={'viewsCounter' + article.articleID} arrow="center" position="top">
              Full reads: {article.fullReads}
            </Tooltip>
          </div>
        </div>
      </div>
    ))

  render() {
    const articles = this.getArticles()
    const { loading } = this.props

    return (
      <div>
        <div
          className={s.archive}
          ref={el => {
            this.archiveElement = el
          }}
        >
          <Block loading={!this.props.articles.length}>
            {articles}
            {loading && <Preloader />}
          </Block>
        </div>
        <Messages />
      </div>
    )
  }
}

Archive.propTypes = {
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      articleID: PropTypes.number,
      views: PropTypes.number,
      author: PropTypes.object,
      title: PropTypes.string,
      text: PropTypes.string,
      image: PropTypes.string,
      status: PropTypes.string,
      publishedTimeAgo: PropTypes.string,
      publishedDate: PropTypes.string
    })
  ),
  allArticlesLoaded: PropTypes.bool,
  loading: PropTypes.bool,
  mediaType: PropTypes.string,
  loadArchive: PropTypes.func,
  lazyLoad: PropTypes.func,
  setArticleStatus: PropTypes.func,
  updateArchive: PropTypes.func
}

export default Archive
