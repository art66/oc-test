import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'
import { loadArchive, lazyLoad, setArticleStatus, updateArchive } from '../../modules/article'
import { truncate } from '../../../../utils'
import Block from '../../../../components/Block'
import s from './styles.cssmodule.scss'
const ARCHIVE_ARTICLE_HEIGHT = 51

class Archive extends Component {
  constructor(props) {
    super(props)
    this.state = { height: '100%' }
  }

  componentDidMount() {
    this.computerScrollbar = document.querySelector('.computer-wrap .scrollbar-inner.scroll-content')

    if (!this.props.archive.length) {
      this.props.loadArchive([0, 20])
    } else {
      this.props.updateArchive()
    }

    window.addEventListener('scroll', this.handleScroll)

    if (this.computerScrollbar) {
      this.computerScrollbar.addEventListener('scroll', this.handleScroll)
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)

    if (this.computerScrollbar) {
      this.computerScrollbar.removeEventListener('scroll', this.handleScroll)
    }
  }

  handleScroll = () => {
    this.lazyLoad()
    this.roundHeight()
  }

  roundHeight = () => {
    let archiveArticleHeight
    let height = this.props.articleHeight

    if (!height) {
      return
    }

    try {
      archiveArticleHeight = this.archiveElement.getElementsByTagName('article')[0].clientHeight
    } catch (error) {
      archiveArticleHeight = ARCHIVE_ARTICLE_HEIGHT
    }

    height = Math.round(height / archiveArticleHeight) * archiveArticleHeight

    this.setState({
      height: height < this.props.articleHeight ? height : height - archiveArticleHeight
    })
  }

  lazyLoad = () => {
    const { loadingArchive, lazyLoad } = this.props

    if (!this.archiveInnerElement) {
      return
    }

    const scroll = this.computerScrollbar
      ? (this.computerScrollbar.scrollTop + this.computerScrollbar.clientHeight)
      : (window.scrollY + window.outerHeight - this.archiveInnerElement.offsetTop)

    if (!loadingArchive && scroll > this.archiveInnerElement.clientHeight) {
      lazyLoad()
    }
  }

  getArticles = () =>
    this.props.archive.map(article => (
      <article key={article.articleID} className={cn(s.article, s[article.status])}>
        <a
          href={`#!/articles/${article.articleID}`}
          onClick={() => this.props.setArticleStatus(article.articleID, 'read')}
        >
          <div className={s.innerWrap}>
            <div className={s.imageWrap}>
              <div style={{ backgroundImage: `url(${article.image}` }} className={s.image} />
            </div>
            <div className={s.titleWrap}>
              <span className={s.title} dangerouslySetInnerHTML={{ __html: truncate(article.title, 50) }} />
              <span className={s.subtitle} dangerouslySetInnerHTML={{ __html: truncate(article.subtitle, 65) }} />
            </div>
          </div>
        </a>
      </article>
    ))

  render() {
    const articles = this.getArticles()

    return (
      <div
        className={s.archive}
        ref={el => {
          this.archiveElement = el
        }}
        style={{ height: this.state.height }}
      >
        <Block loading={!this.props.archive.length}>
          <div
            ref={el => {
              this.archiveInnerElement = el
            }}
          >
            {articles}
          </div>
        </Block>
      </div>
    )
  }
}

Archive.propTypes = {
  archive: PropTypes.arrayOf(
    PropTypes.shape({
      articleID: PropTypes.number,
      title: PropTypes.string,
      subtitle: PropTypes.string,
      image: PropTypes.string,
      status: PropTypes.string
    })
  ),
  loadingArchive: PropTypes.bool,
  lazyLoad: PropTypes.func,
  loadArchive: PropTypes.func,
  setArticleStatus: PropTypes.func,
  updateArchive: PropTypes.func,
  articleHeight: PropTypes.number
}

const mapDispatchToProps = {
  loadArchive,
  lazyLoad,
  setArticleStatus,
  updateArchive
}

const mapStateToProps = state => ({
  archive: state.article.archive,
  loadingArchive: state.article.loadingArchive
})

export default connect(mapStateToProps, mapDispatchToProps)(Archive)
