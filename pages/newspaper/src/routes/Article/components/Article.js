import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Block from '../../../components/Block'
import Messages from '../../../components/Messages'
import Archive from './Archive'
import Tooltip from '@torn/shared/components/Tooltip'
import s from './styles.cssmodule.scss'
import { getNewspaperTitleDate } from '../../../utils'
/* global getCurrentTimestamp, getAction, getCookie */
const MIN_SCROLL_Y_FOR_AUTO_SCROLL_TO_TOP = 500

class Article extends Component {
  constructor(props) {
    super(props)
    this.resetStartReadTime()
    this.state = { articleHeight: 1000 }
  }

  componentDidMount() {
    const { params, loadAppData } = this.props
    this.computerScrollbar = document.querySelector('.computer-wrap .scrollbar-inner.scroll-content')
    this.scrollToTop()

    window.addEventListener('scroll', this.handleScroll)
    window.addEventListener('beforeunload', this.updateArticleReadTime)
    window.addEventListener('blur', this.updateArticleReadTime)

    if (this.computerScrollbar) {
      this.computerScrollbar.addEventListener('scroll', this.handleScroll)
    }

    loadAppData(params.id).then(() => {
      this.wrapTablesForScroll()
      this.changeDocumentTitle()
    })

    this.props.router.listen(location => {
      if (
        location.action === 'PUSH' &&
        location.pathname.indexOf('articles') !== -1 &&
        window.scrollY > MIN_SCROLL_Y_FOR_AUTO_SCROLL_TO_TOP
      ) {
        this.scrollToTop('smooth')
      }
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.params.id && this.props.params.id !== prevProps.params.id) {
      this.props.setArticleStatus(this.props.params.id, 'read')
      this.updateArticleReadTime(true)
      this.props.loadAppData(this.props.params.id).then(data => {
        this.changeDocumentTitle(data.article.title)
      })
    }
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  //   window.addEventListener('beforeunload', this.updateArticleReadTime)
  //   window.addEventListener('blur', this.updateArticleReadTime)
  // }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.updateArticleReadTime)
    window.removeEventListener('blur', this.updateArticleReadTime)
    window.removeEventListener('scroll', this.handleScroll)

    if (this.computerScrollbar) {
      this.computerScrollbar.removeEventListener('scroll', this.handleScroll)
    }
  }

  changeDocumentTitle = (articleTitle = '') => {
    const { article } = this.props
    if (!article.publishedTimestamp) {
      return
    }
    const date = getNewspaperTitleDate(new Date(article.publishedTimestamp * 1000))
    let title = document.title
    title = title.replace(/\|.*\|/, '| ' + (articleTitle || article.title) + ' |')
    title = title.match(/^([^\|]*\|[^\|]*)\|.*$/)[1] + ' | ' + date
    document.title = title
  }

  scrollToTop = (behavior = 'auto') => {
    try {
      window.scroll &&
        window.scroll({
          top: 0,
          behavior: behavior
        })
    } catch (error) {
      window.scroll(0, 0)
    }
  }

  handleScroll = () => {
    this.setState({ articleHeight: this.articleContnet && this.articleContnet.clientHeight })
  }

  resetStartReadTime = () => {
    this.startReadTime = parseInt(getCurrentTimestamp() / 1000)
  }

  updateArticleReadTime = (async = false) => {
    const userID = getCookie('uid')
    const time = parseInt(getCurrentTimestamp() / 1000 - this.startReadTime)
    const articleID = this.props.params.id
    if (!userID) {
      return
    }

    this.resetStartReadTime()
    getAction({
      async,
      type: 'post',
      action: `/newspaper.php?step=updateArticleReadTime`,
      data: {
        userID,
        articleID,
        time
      }
    })
  }

  wrapTablesForScroll = () => {
    try {
      let elements = this.articleEl.getElementsByTagName('table')
      if (!elements.length) {
        return
      }

      for (let i = 0; i < elements.length; i++) {
        let el = elements[i]
        let wrapper = document.createElement('div')
        wrapper.className = s.tableWrapper
        el.parentNode.insertBefore(wrapper, el)
        wrapper.appendChild(el)
      }
    } catch (error) {
      console.error && console.error(error)
    }
  }

  render() {
    const { title, subtitle, text, author, publishedDate, publishedTimeAgo, views = 0, fullReads, loaded } = this.props.article

    return (
      <div>
        <div
          className={s.article}
          ref={el => {
            this.articleEl = el
          }}
        >
          <div
            className={s.articleContent}
            ref={el => {
              this.articleContnet = el
            }}
          >
            <Block loading={!loaded}>
              <div className={s.header}>
                <h5 className={s.title} dangerouslySetInnerHTML={{ __html: title }} />
                <div className={s.info}>
                  <a href={`/profiles.php?XID=${author.userID}`} className={s.author}>
                    {author.playername}
                  </a>
                  <span className={s.date}>
                    on {publishedDate} ({publishedTimeAgo})
                  </span>
                  <span id="viewsCounter" className={s.views}>
                    <span className={s.counter}>{views}</span>
                    <i className={s.viewsIcon} />
                    {fullReads >= 0 &&
                      <Tooltip parent="viewsCounter" arrow="center" position="top">
                        Full reads: {fullReads}
                      </Tooltip>
                    }
                  </span>
                </div>
              </div>
              {subtitle && <div className={s.subtitle} dangerouslySetInnerHTML={{ __html: subtitle }} />}
              <div className={s.text} dangerouslySetInnerHTML={{ __html: text }} />
            </Block>
          </div>
          <Archive articleHeight={this.state.articleHeight} />
        </div>
        <Messages />
      </div>
    )
  }
}

Article.propTypes = {
  article: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    text: PropTypes.string,
    author: PropTypes.shape({
      userID: PropTypes.number,
      playername: PropTypes.string
    }),
    publishedDate: PropTypes.string,
    publishedTimeAgo: PropTypes.string,
    publishedTimestamp: PropTypes.number,
    views: PropTypes.number,
    fullReads: PropTypes.number,
    subtitle: PropTypes.string,
    loaded: PropTypes.bool
  }),
  archive: PropTypes.array,
  params: PropTypes.shape({
    id: PropTypes.string
  }),
  loadAppData: PropTypes.func,
  setArticleStatus: PropTypes.func,
  updateArchive: PropTypes.func
}

export default Article
