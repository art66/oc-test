import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'articles/:id',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Article = require('./containers/ArticleContainer').default
        const reducer = require('./modules/article').default
        injectReducer(store, { key: 'article', reducer })
        cb(null, Article)
      },
      'article'
    )
  }
})
