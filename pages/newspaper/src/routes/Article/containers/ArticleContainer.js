import { connect } from 'react-redux'
import { loadAppData, setArticleStatus } from '../modules/article'
import Article from '../components/Article'

const mapDispatchToProps = {
  loadAppData,
  setArticleStatus
}

const mapStateToProps = state => ({
  article: state.article.article
})

export default connect(mapStateToProps, mapDispatchToProps)(Article)
