import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'
import { unionBy, orderBy } from 'lodash'
const LAZY_LOAD_ARTICLES_LENGTH = 20

const initialState = {
  article: {
    author: {
      userID: 0,
      playername: ''
    }
  },
  archive: []
}

export const setArticleState = createAction('set article state', data => ({ data }))
export const loading = createAction('loading', (loading = true) => ({ loading }))
export const loadingArticle = createAction('loading article', (loading = true) => ({ loading }))
export const loadingArchive = createAction('loading archive', (loadingArchive = true) => ({ loadingArchive }))
export const setArticleStatus = createAction('set article status', (articleID, status) => ({ articleID, status }))

export const loadAppData = articleID => dispatch => {
  dispatch(loadingArticle(true))

  return fetchUrl('step=getArticle', { articleID: articleID })
    .then(data => {
      dispatch(setArticleState(data))
      dispatch(loadingArticle(false))

      return data
    })
    .catch(error => console.error(error))
}

export const loadArchive = range => (dispatch, getState) => {
  dispatch(loadingArchive(true))
  const archive = getState().article.archive

  return fetchUrl('step=getArticleArchive', { range })
    .then(response => {
      dispatch(setArticleState({ archive: orderBy(unionBy(response, archive, 'articleID'), 'published', 'desc') }))
      dispatch(loadingArchive(false))
    })
    .catch(error => console.error(error))
}

export const lazyLoad = () => (dispatch, getState) => {
  let articlesLength = getState().article.archive.length
  dispatch(loadArchive([articlesLength, articlesLength + LAZY_LOAD_ARTICLES_LENGTH]))
}

export const updateArchive = () => (dispatch, getState) => {
  const articles = getState().article.archive
  const articlesIDs = articles.map(article => article.articleID)

  return fetchUrl('step=getArchiveForUpdate', { articles: articlesIDs })
    .then(response => {
      dispatch(
        setArticleState({
          articles: articles.map(article => ({
            ...article,
            ...response.find(item => item.articleID === article.articleID)
          }))
        })
      )
    })
    .catch(error => console.error(error))
}

export default handleActions(
  {
    'set article state'(state, action) {
      return {
        ...state,
        ...action.payload.data
      }
    },
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    },
    'loading article'(state, action) {
      return {
        ...state,
        article: {
          ...state.article,
          loaded: !action.payload.loading
        }
      }
    },
    'loading archive'(state, action) {
      return {
        ...state,
        loadingArchive: action.payload.loadingArchive
      }
    },
    'set article status'(state, action) {
      return {
        ...state,
        archive: state.archive.map(article => ({
          ...article,
          status: article.articleID === action.payload.articleID ? action.payload.status : article.status
        }))
      }
    }
  },
  initialState
)
