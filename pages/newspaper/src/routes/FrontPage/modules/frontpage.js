import { createAction, handleActions } from 'redux-actions'
import { fetchUrl } from '../../../utils'

const initialState = {
  leadArticle: {},
  snippets: [],
  bazaars: [],
  classifieds: {
    personals: {},
    bounties: {},
    lottery: {
      winners: []
    }
  },
  messages: [],
  pricesChart: {},
  loading: true
}

export const setAppState = createAction('set app state', data => ({ data }))
export const loading = createAction('loading', (loading = true) => ({ loading }))

export const loadAppData = () => dispatch =>
  fetchUrl('step=getFrontPage')
    .then(data => {
      dispatch(loading(true))
      dispatch(setAppState(data))
      dispatch(loading(false))
    })
    .catch(error => console.error(error))

export default handleActions(
  {
    'set app state'(state, action) {
      return {
        ...state,
        ...action.payload.data
      }
    },
    loading(state, action) {
      return {
        ...state,
        loading: action.payload.loading
      }
    }
  },
  initialState
)
