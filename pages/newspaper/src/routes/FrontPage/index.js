import { injectReducer } from '../../store/reducers'

export default store => ({
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const FrontPage = require('./containers/FrontPageContainer').default
        const reducer = require('./modules/frontpage').default
        injectReducer(store, { key: 'frontpage', reducer })
        cb(null, FrontPage)
      },
      'frontpage'
    )
  }
})
