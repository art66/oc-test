import { connect } from 'react-redux'
import { loadAppData } from '../modules/frontpage'
import FrontPage from '../components/FrontPage'

const mapDispatchToProps = {
  loadAppData
}

const mapStateToProps = state => ({
  frontPage: state.frontpage
})

export default connect(mapStateToProps, mapDispatchToProps)(FrontPage)
