import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'

class Bazaars extends Component {
  getList = () =>
    this.props.bazaars.map((item, index) => (
      <li key={index} className={s.item}>
        <a className={s.link} href={`/bazaar.php?userID=${item.userID}`}>
          {item.name}
        </a>
        <div className={s.customers}>
          <a href={`/bazaar.php?userID=${item.userID}`}>
            {item.customers}
            <img
              title={`${item.name} has had ${item.customers} customers buying ${item.items} items this week!`}
              src="/images/v2/newspaper/weekly_bazaars.svg"
            />
          </a>
        </div>
      </li>
    ))

  render() {
    const list = this.getList()

    return (
      <div className={s.bazaars}>
        <span className={s.title}>Weekly bazaars</span>
        <ul className="bazaars">{list}</ul>
      </div>
    )
  }
}

Bazaars.propTypes = {
  bazaars: PropTypes.arrayOf(
    PropTypes.shape({
      userID: PropTypes.number,
      name: PropTypes.string,
      customers: PropTypes.number,
      items: PropTypes.number
    })
  )
}

export default Bazaars
