import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Snippet from './Snippet'
import Block from '../../../../components/Block'
import s from './styles.cssmodule.scss'

class Snippets extends Component {
  getSnippets = () => this.props.snippets.map(snippet => <Snippet key={snippet.id} snippet={snippet} />)

  render() {
    const snippets = this.getSnippets()

    return (
      <div className={s.snippets}>
        <Block>{snippets}</Block>
      </div>
    )
  }
}

Snippets.propTypes = {
  snippets: PropTypes.array
}

export default Snippets
