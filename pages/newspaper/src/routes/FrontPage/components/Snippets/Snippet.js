import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { truncate } from '../../../../utils'
import s from './styles.cssmodule.scss'

class Snippet extends Component {
  render() {
    const { id, title, subtitle, image, status } = this.props.snippet

    return (
      <div className={cn(s.snippet)}>
        <a href={`#!/articles/${id}`} className={cn(s.snippetLink, s[status])}>
          <figure className={s.imageWrap}>
            <div style={{ backgroundImage: `url(${image})` }} className={s.image} />
          </figure>
          <div className={cn(s.titleWrap, s[status])}>
            <span className={cn(s.title)}>{truncate(title, 35)}</span>
            <span className={s.subtitle} dangerouslySetInnerHTML={{ __html: subtitle.substring(0, 70) + '...' }} />
          </div>
        </a>
      </div>
    )
  }
}

Snippet.propTypes = {
  snippet: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    image: PropTypes.string,
    status: PropTypes.string
  })
}

export default Snippet
