import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Block from '../../../../components/Block'
import s from './styles.cssmodule.scss'

class LeadArticle extends Component {
  render() {
    const { id, title, subtitle, image } = this.props.leadArticle

    return (
      <a href={`#!/articles/${id}`} className={cn(s.leadArticle, 'clearfix')}>
        <Block>
          <div className={s.leadImageWrap}>
            <div className={s.leadImage} style={{ backgroundImage: `url(${image})` }} />
          </div>
          <div className={s.leadText}>
            <h5 className={s.title}>{title}</h5>
            <p className={s.subtitle} dangerouslySetInnerHTML={{ __html: subtitle }} />
          </div>
        </Block>
      </a>
    )
  }
}

LeadArticle.propTypes = {
  leadArticle: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    image: PropTypes.string
  })
}

export default LeadArticle
