import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import s from './styles.cssmodule.scss'
/* global $, ajaxWrapper, toNumberFormat */

class PricesChart extends Component {
  componentDidMount() {
    this.initialiseChart('.chart-placeholder', null, null, true)
  }

  showTooltip = (x, y, contents) => {
    $("<div id='white-tooltip' class='white-tooltip'></div>")
      .append("<div class='ui-tooltip-content'>" + contents + "<div class='tooltip-arrow left bottom'></div></div>")
      .appendTo('body')
      .fadeIn(200)

    let $tooltip = $('#white-tooltip')
    let tooltipWidth = $tooltip.width()
    let tooltipHeight = $tooltip.height()
    $tooltip.css({
      position: 'absolute',
      top: y - tooltipHeight - 20,
      left: x - tooltipWidth / 2
    })
  }

  generalDataSet = {
    series: {
      lines: {
        show: true,
        fill: 0.4
      },
      points: {
        show: false
      },
      color: '#999'
    },
    grid: {
      hoverable: true,
      clickable: true,
      color: '#777',
      show: true,
      aboveData: false,
      margin: { top: 3, right: 3, bottom: 3, left: 3 },
      borderWidth: 0,
      minBorderMargin: 3
    },
    legend: {
      show: true,
      position: 'nw',
      margin: [5, 5]
    },
    selection: {
      mode: 'xy'
    }
  }

  defaultAxisDataSet = {
    yaxis: {
      position: 'right',
      font: {
        size: 10,
        lineHeight: 12,
        variant: 'small-caps',
        color: '#333'
      }
    },
    xaxis: {
      mode: 'time',
      tickSize: [],
      timeformat: '',
      font: {
        size: 12,
        lineHeight: 8,
        color: '#333'
      }
    }
  }

  toDate = timestamp => {
    let month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    let date = new Date(timestamp)

    return date.getDate() + ' ' + month[date.getMonth()] + ' ' + (date.getYear() - 100)
  }

  clone = obj => {
    if (!obj || typeof obj !== 'object') {
      return obj
    }

    let copy = obj.constructor()
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) {
        copy[attr] = this.clone(obj[attr])
      }
    }
    return copy
  }

  updateChart = (placeholder, axisData, data, portfolio) => {
    let axisDataInstance = axisData || this.defaultAxisDataSet
    $.extend(this.generalDataSet, axisDataInstance)
    let url = 'newspaper.php?step=getStocksChartData'

    let oncomplete = data => {
      let max = -1
      let min = 1000000000

      for (let i in data) {
        let item = data[i]
        if (typeof item[0] === 'number') {
          item[0] = new Date(item[0] * 1000)
        }
        min = Math.min(min, item[1])
        max = Math.max(max, item[1])
      }
      min = Math.max(min - (max - min) * 0.05, 0)
      max = max + (max - min) * 0.05

      $.extend(this.generalDataSet.yaxis, {
        ticks: [min, (min + max) / 2, max],
        min: min,
        datamin: min,
        max: max,
        datamax: max
      })

      $.plot(
        placeholder,
        [
          {
            data: data
          }
        ],
        this.generalDataSet
      )

      this.checkAxisYear()
    }

    if (url) {
      ajaxWrapper({
        url: url,
        oncomplete: resp => {
          let data = JSON.parse(resp.responseText)
          oncomplete(data)
        }
      })
    }
  }

  checkAxisYear = () => {
    let $year = $('.flot-text .year')
    $year.first().text() === $year.last().text() ? $year.hide() : $year.show()
  }

  initialiseChart = (placeholderClass, axisData, data, portfolio) => {
    let placeholder = placeholderClass
    let $placeholder = $(placeholder)
    let plot = this.updateChart(placeholder, axisData, data, portfolio)
    let previousPoint = null
    $placeholder.bind('plothover', (event, pos, item) => {
      let textTooltip = 'Price'

      if (item) {
        if (previousPoint !== item.dataIndex) {
          previousPoint = item.dataIndex
          $('#white-tooltip').remove()
          let x = Math.round(item.datapoint[0].toFixed(2))
          let y = Math.round(item.datapoint[1].toFixed(2))
          this.showTooltip(
            item.pageX,
            item.pageY,
            "<p class='bold m-bottom3'>" + this.toDate(x) + '</p>' + textTooltip + ': $' + toNumberFormat(y)
          )
        }
      } else {
        $('#white-tooltip').remove()
        previousPoint = null
      }
    })

    this.checkAxisYear()
    $('.chart-wrap').resizable()
    $(window).resize(() => {
      this.checkAxisYear()
    })

    return plot
  }

  getTooltipMessage = () => {
    const { pricesDiff } = this.props.pricesChart
    let message

    if (pricesDiff > 0) {
      message = 'went up'
    } else if (pricesDiff < 0) {
      message = 'went down'
    } else {
      message = 'did not change'
    }

    return `Average stock price ${message} in the last day`
  }

  render() {
    const { currentPrice, pricesDiff } = this.props.pricesChart
    const tooltipMessage = this.getTooltipMessage()

    return (
      <div className={s.pricesChart}>
        <a href="/page.php?sid=stocks" className={s.title}>
          {' '}
          TCSE Market Index
        </a>
        <div className={cn('chart-placeholder', s.placeholder)} />
        <p className={s.label}>Price over last 24 hours</p>
        <div className={s.statistics}>
          {currentPrice}
          <span
            className={cn(s.arrowChange, {
              [s.up]: pricesDiff > 0,
              [s.down]: pricesDiff < 0
            })}
            title={tooltipMessage}
          />
          {pricesDiff && Math.abs(pricesDiff)}
        </div>
      </div>
    )
  }
}

PricesChart.propTypes = {
  pricesChart: PropTypes.shape({
    currentPrice: PropTypes.string,
    pricesDiff: PropTypes.number
  })
}

export default PricesChart
