import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { shortMoney, toMoney } from '@torn/shared/utils'
import { truncate } from '../../../../utils'
import s from './styles.cssmodule.scss'

class Classifieds extends Component {
  getPersonals = () => {
    const { personals } = this.props.classifieds
    let hes, his, him

    if (personals.gender === 'Male') {
      hes = "He's"
      his = 'his'
      him = 'him'
    } else {
      hes = "She's"
      his = 'her'
      him = 'her'
    }

    return (
      <div className={cn(s.personals, s.classified)}>
        <a href="/personals.php#!p=search&type=2" className={s.title}>
          TC PERSONALS
        </a>
        Have you met{' '}
        <a href={`/profiles.php?XID=${personals.userID}`} className={s.link}>
          {personals.playername}
        </a>?
        <br />
        <span dangerouslySetInnerHTML={{ __html: truncate(personals.message, 25) }} />
      </div>
    )
  }

  getBounties = () => {
    const { userID, playername, reward } = this.props.classifieds.bounties
    let content

    if (!reward) {
      content = 'There are no bounties up at this time'
    } else {
      content = (
        <span>
          <a href="/bounties.php" className={s.title}>
            BOUNTIES
          </a>
          Hospitalize
          <a href={`/profiles.php?XID=${userID}`} className={s.link}>
            {' '}
            {playername}{' '}
          </a>
          and earn a ${toMoney(reward)} reward!
        </span>
      )
    }

    return <div className={cn(s.bounty, s.classified)}>{content}</div>
  }

  getLottery = () => {
    const { winners } = this.props.classifieds.lottery
    const winnersCont = winners.map((winner, index) => (
      <span key={index} className={s.winner}>
        <a href={`/profiles.php?XID=${winner.userID}`} className="t-blue">
          {winner.playername}{' '}
        </a>
        won ${shortMoney(parseInt(winner.money.replace(/,/g, '')))} on {winner.lotteryName} {winner.timeAgo} ago!
      </span>
    ))

    return (
      <div className={cn(s.classified)}>
        <a href="/casino.php" className={s.title}>
          LOTTERY
        </a>
        {winnersCont}
      </div>
    )
  }

  render() {
    return (
      <div className={s.classifieds}>
        {this.getLottery()}
        {this.getBounties()}
        {this.getPersonals()}
      </div>
    )
  }
}

Classifieds.propTypes = {
  classifieds: PropTypes.shape({
    personals: PropTypes.object,
    bounties: PropTypes.object,
    lottery: PropTypes.object
  })
}

export default Classifieds
