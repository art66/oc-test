import React, { Component } from 'react'
import PropTypes from 'prop-types'
import LeadArticle from './LeadArticle'
import Block from '../../../components/Block'
import Snippets from './Snippets'
import PricesChart from './PricesChart'
import Bazaars from './Bazaars'
import Classifieds from './Classifieds'
import Messages from '../../../components/Messages'
import s from './styles.cssmodule.scss'

class FrontPage extends Component {
  componentDidMount() {
    this.props.loadAppData()
  }

  render() {
    const { leadArticle, snippets, pricesChart, bazaars, classifieds, messages } = this.props.frontPage

    return (
      <div className={s.frontPage}>
        <div className="clearfix">
          <div className={s.mainCont}>
            <LeadArticle leadArticle={leadArticle} />
            <Snippets snippets={snippets} />
          </div>
          <div className={s.sideCont}>
            <Block>
              <div className={s.statistics}>
                <PricesChart pricesChart={pricesChart} />
                <Bazaars bazaars={bazaars} />
              </div>
              <Classifieds classifieds={classifieds} />
            </Block>
          </div>
        </div>
        <Block className={s.messagesWrap}>
          <Messages messages={messages} />
        </Block>
      </div>
    )
  }
}

FrontPage.propTypes = {
  frontPage: PropTypes.shape({
    leadArticle: PropTypes.object,
    snippets: PropTypes.array,
    pricesChart: PropTypes.object,
    bazaars: PropTypes.array,
    classifieds: PropTypes.object,
    messages: PropTypes.array
  }),
  loadAppData: PropTypes.func
}

export default FrontPage
