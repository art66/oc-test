import { injectReducer } from '../../store/reducers'

export default store => ({
  path: 'tell_your_story',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Submission = require('./containers/SubmissionContainer').default
        const reducer = require('./modules').default
        injectReducer(store, { key: 'submission', reducer })
        cb(null, Submission)
      },
      'submission'
    )
  }
})
