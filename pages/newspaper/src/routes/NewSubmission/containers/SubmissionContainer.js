import { connect } from 'react-redux'
import {} from '../modules'
import NewSubmission from '../components/NewSubmission'

const mapDispatchToProps = {}

const mapStateToProps = state => ({})

export default connect(mapStateToProps, mapDispatchToProps)(NewSubmission)
