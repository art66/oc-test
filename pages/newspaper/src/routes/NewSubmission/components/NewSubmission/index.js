import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { fetchUrl } from '../../../../utils'
import Form from '../../../../../../reporting/src/components/Form'
/* global $ */

class NewSubmission extends Component {
  componentDidMount() {
    let $pageLinks = $('#top-page-links-list')
    $pageLinks.find('.tell-your-story').addClass('to-hide')
    $pageLinks.find('.back').removeClass('to-hide')
    $('.newspaper-wrap').addClass('tell-your-story')
  }

  componentWillUnmount() {
    let $pageLinks = $('#top-page-links-list')
    $pageLinks.find('.tell-your-story').removeClass('to-hide')
    $pageLinks.find('.back').addClass('to-hide')
    $('.newspaper-wrap').removeClass('tell-your-story')
    window.showNewspaperInfoBox({})
  }

  handleSubmit = (e, data) => {
    if (!data.title || !data.text || data.title.length < 2 || data.text.length < 2) {
      window.showNewspaperInfoBox({
        msg: 'Please fill all the fields',
        color: 'red'
      })

      return
    }

    return new Promise((resolve, reject) => {
      fetchUrl('step=addSubmission', { ...data })
        .then((resp) => {
          window.showNewspaperInfoBox({
            msg: 'Your submission was sent',
            color: 'green'
          })

          resolve(resp)
        })
        .catch(error => {
          window.showNewspaperInfoBox({
            msg: error.message,
            color: 'red'
          })

          reject(error)
        })
    })
  }

  render() {
    return (
      <div>
        <Form
          withSubtitle={false}
          withLeadImage={false}
          submissionId='newspaper_new_submission'
          formTitle='Send information to reporting team'
          textLabel='Information'
          onSubmit={(e, data) => this.handleSubmit(e, data)}
          description={`Do you know something about a <span class="bold">famous faction leader</span>? Have you discovered
            <span class="bold">a weird quirk of Torn City life</span>? Or are you just looking to share some
            <span class="bold">juicy gossip</span> you've heard on the grapevine? Either way, the
            <span class="bold">Torn City Times wants to hear from you</span>. Use the submission form below
            to give us a brief outline of your story, and if it interests us, one of our reporters will be in touch
            with you shortly.`}
        />
      </div>
    )
  }
}

NewSubmission.propTypes = {}

export default NewSubmission
