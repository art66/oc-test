import PropTypes from 'prop-types'
import React, { Component } from 'react'
import InfoBox from '@torn/shared/components/InfoBox'
import { getNewspaperTitleDate } from '../../utils'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'
/* global getCurrentTimestamp, $ */

class CoreLayout extends Component {
  constructor(props) {
    super(props)
    this.state = { infoBox: {} }
  }

  componentDidMount() {
    this.changeRoute()
    window.showNewspaperInfoBox = (infoBox = {}) => {
      this.setState({ infoBox })
    }

    this.props.router.listen(location => {
      try {
        if (location.pathname.indexOf('articles') === -1) {
          const date = getNewspaperTitleDate(new Date(getCurrentTimestamp()))
          let title = document.title.replace(/\|.*\|/, '| Official Torn City Newspaper |')

          title = `${title.match(/^([^\|]*\|[^\|]*)\|.*$/)[1]} | ${date}`
          document.title = title
        }

        this.setState({ infoBox: {} })
        this.changeLinksState(location)
      } catch (e) {
        console.error(e)
      }
    })

    window.addEventListener('hashchange', e => {
      if (e.oldURL === e.newURL) {
        return
      }

      this.changeRoute()
    })
  }

  changeRoute = () => {
    this.props.router.push(location.hash.substring(location.hash.indexOf('#!') + 3))
  }

  changeLinksState = param => {
    let index = 0

    if (param.pathname.indexOf('archive') !== -1) {
      index = 1
    }

    const $links = $('.newspaper-link-wrap')

    $links.removeClass('active')
    $links.eq(index).addClass('active')
  }

  render() {
    return (
      <div>
        {this.state.infoBox && <InfoBox msg={this.state.infoBox.msg} color={this.state.infoBox.color} />}
        <div className='core-layout__viewport'>{this.props.children}</div>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
  router: PropTypes.object
}

export default CoreLayout
