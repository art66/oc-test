import React from 'react'
import { mount } from 'enzyme'

import Columns from '../'

import initialState, { tabletMode, mobileMode, manualDesktop } from './mocks'

describe('<Columns />', () => {
  it('should render basic columns markup in desktop mode', () => {
    const Component = mount(<Columns {...initialState} />)

    expect(Component.find('.columnsWrap').length).toBe(1)
    expect(Component.find('.column').length).toBe(6)

    expect(Component).toMatchSnapshot()
  })
  it('should render columns in tablet mode', () => {
    const Component = mount(<Columns {...tabletMode} />)

    expect(Component.find('.columnsWrap').length).toBe(1)
    expect(Component.find('.column').length).toBe(6)
    expect(
      Component.find('.column')
        .at(2)
        .text()
    ).toBe('Hollow')

    expect(Component).toMatchSnapshot()
  })
  it('should render columns in mobile mode', () => {
    const Component = mount(<Columns {...mobileMode} />)

    expect(Component.find('.columnsWrap').length).toBe(1)
    expect(Component.find('.column').length).toBe(6)
    expect(
      Component.find('.column')
        .at(2)
        .text()
    ).toBe('Hollow')

    expect(Component).toMatchSnapshot()
  })
  it('should render columns in manualDesktop mode', () => {
    const Component = mount(<Columns {...manualDesktop} />)

    expect(Component.find('.columnsWrap').length).toBe(1)
    expect(Component.find('.column').length).toBe(6)
    expect(
      Component.find('.column')
        .at(2)
        .text()
    ).toBe('Hollow Point')

    expect(Component).toMatchSnapshot()
  })
})
