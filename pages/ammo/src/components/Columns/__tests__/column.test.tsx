import React from 'react'
import { mount } from 'enzyme'

import Column from '../Column'

import initialState from './mocks/column'

describe('<Column />', () => {
  it('should render basic columns markup in desktop mode', () => {
    const Component = mount(<Column {...initialState} />)

    expect(Component.find('.column').length).toBe(1)
    expect(Component.find('.column').text()).toBe('test')
    expect(Component.find('.column').prop('id')).toBe('test')

    expect(Component).toMatchSnapshot()
  })
})
