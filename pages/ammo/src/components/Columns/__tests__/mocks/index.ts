const initialState = {
  mediaType: 'desktop',
  isManualDesktopMode: false
}

const tabletMode = {
  ...initialState,
  mediaType: 'tablet'
}

const mobileMode = {
  ...initialState,
  mediaType: 'mobile'
}

const manualDesktop = {
  ...initialState,
  isManualDesktopMode: true
}

export { tabletMode, mobileMode, manualDesktop }
export default initialState
