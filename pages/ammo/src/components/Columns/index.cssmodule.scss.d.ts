// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'column': string;
  'columnTitle': string;
  'columnsWrap': string;
  'flexCenter': string;
  'globalSvgShadow': string;
  'lineContainer': string;
  'section': string;
}
export const cssExports: CssExports;
export default cssExports;
