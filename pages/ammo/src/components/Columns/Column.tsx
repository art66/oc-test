import React, { memo, useEffect } from 'react'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import styles from './index.cssmodule.scss'

interface IColumn {
  title: string
  tooltip: string
}

const Column = ({ title, tooltip }: IColumn) => {
  useEffect(() => {
    if (tooltip) {
      tooltipsSubscriber.subscribe({
        child: tooltip,
        ID: title
      })
    }
  })

  return (
    <span id={tooltip ? title : ''} className={styles.column}>
      {title}
    </span>
  )
}

export default memo(Column)
