import React, { memo } from 'react'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import { COLUMN_TITLES } from '../../constants'

import Column from './Column'

import styles from './index.cssmodule.scss'

interface IColumn {
  titles: {
    desktop: string
    touchscreen?: string
  }
  tooltip: string
}

const Columns = ({ mediaType, isManualDesktopMode }) => {
  const { isDesktop } = checkMediaType(mediaType, isManualDesktopMode)

  const columns = COLUMN_TITLES.map(({ titles, tooltip }: IColumn) => {
    const { touchscreen, desktop } = titles
    const title = !isDesktop && touchscreen ? touchscreen : desktop

    return <Column key={title} title={title} tooltip={tooltip} />
  })

  return <div className={styles.columnsWrap}>{columns}</div>
}

export default memo(Columns)
