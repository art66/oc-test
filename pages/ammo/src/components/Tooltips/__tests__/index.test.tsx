import React from 'react'
import { mount } from 'enzyme'

import Tooltips from '../'

describe('<Tooltips />', () => {
  it('should render basic markup', () => {
    const Component = mount(<Tooltips />)

    expect(Component.find('Tooltip').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
