import React, { useEffect } from 'react'
import checkMediaType from '@torn/shared/utils/checkMediaType'
import isValue from '@torn/shared/utils/isValue'

import HeadingLine from '../HeadingLine'
import Columns from '../../containers/Columns'
import Rows from '../../containers/Rows'

import { ESCAPE_KEY_ID } from '../../constants'
import { IProps } from './interfaces'

const Table = ({
  mediaType,
  isManualDesktopMode,
  title,
  data,
  activeRowID,
  tableType,
  deactivateRow
}: IProps) => {
  useEffect(() => {
    window.addEventListener('keydown', handlerKeyDown)

    return () => {
      window.removeEventListener('keydown', handlerKeyDown)
    }
  }, [activeRowID])

  const handlerKeyDown = e => {
    const keyCode = e.which

    if (isValue(activeRowID) && keyCode === ESCAPE_KEY_ID) {
      e.preventDefault()
      return deactivateRow({ tableType })
    }
  }

  const { isDesktop } = checkMediaType(mediaType, isManualDesktopMode)

  return (
    <div className='table'>
      <HeadingLine title={title} />
      {isDesktop && <Columns />}
      <Rows data={data} activeRowID={activeRowID} tableType={tableType} />
    </div>
  )
}

export default Table
