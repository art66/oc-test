import { IRow, TTableTypes } from '../../../interfaces/IStore'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IProps {
  mediaType: TMediaType
  isManualDesktopMode: boolean
  tableType: TTableTypes
  activeRowID: number
  title: string
  data: IRow[],
  deactivateRow: ({ tableType }: { tableType: TTableTypes }) => {}
}
