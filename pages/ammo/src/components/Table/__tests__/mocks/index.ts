import { IProps } from '../../interfaces'

const initialState: IProps = {
  deactivateRow: ({ tableType }) => tableType,
  mediaType: 'desktop',
  isManualDesktopMode: false,
  title: 'test_title',
  data: [],
  activeRowID: null,
  tableType: 'equipped'
}

const tabletMode: IProps = {
  ...initialState,
  mediaType: 'tablet'
}

const mobileMode: IProps = {
  ...initialState,
  mediaType: 'mobile'
}

const manualDesktopMode: IProps = {
  ...initialState,
  isManualDesktopMode: true
}

export { tabletMode, mobileMode, manualDesktopMode }

export default initialState
