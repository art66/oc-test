import React from 'react'
import { shallow } from 'enzyme'

import Table from '../'

import initialState, { tabletMode, mobileMode, manualDesktopMode } from './mocks'

describe('<Table />', () => {
  it('should render basic table markup', () => {
    const Component = shallow(<Table {...initialState} />)

    expect(Component.find('.table').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').prop('title')).toBe('test_title')
    expect(Component.find('Connect(Component)').length).toBe(1)
    expect(Component.find('Connect(Rows)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render table in tablet mode', () => {
    const Component = shallow(<Table {...tabletMode} />)

    expect(Component.find('.table').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').prop('title')).toBe('test_title')
    expect(Component.find('Connect(Component)').length).toBe(0)
    expect(Component.find('Connect(Rows)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render table in mobile mode', () => {
    const Component = shallow(<Table {...mobileMode} />)

    expect(Component.find('.table').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').prop('title')).toBe('test_title')
    expect(Component.find('Connect(Component)').length).toBe(0)
    expect(Component.find('Connect(Rows)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render table in manual desktop mode', () => {
    const Component = shallow(<Table {...manualDesktopMode} />)

    expect(Component.find('.table').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').length).toBe(1)
    expect(Component.find('Memo(HeadingLine)').prop('title')).toBe('test_title')
    expect(Component.find('Connect(Component)').length).toBe(1)
    expect(Component.find('Connect(Rows)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
