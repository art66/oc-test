import React from 'react'

import Weapons from '../../containers/Outcome/Weapons'
import Trading from '../../containers/Outcome/Trading'

import styles from './index.cssmodule.scss'

import { IProps } from './interfaces'

const SELL = 'sell'
const BUY = 'buy'

const Outcome = ({ tableType, ammoID, tradingData, ammoTitle, ammoTitleShort, limits }: IProps) => {
  const { prices, weaponsList, progress } = tradingData

  const tradingBuy = progress && progress[BUY]
  const tradingSell = progress && progress[SELL]
  const tradeBuyFinish = progress && progress[`${BUY}TradeDone`]
  const tradeSellFinish = progress && progress[`${SELL}TradeDone`]
  const tradeBuyFailed = progress && progress[`${BUY}TradeFail`]
  const tradeSellFailed = progress && progress[`${SELL}TradeFail`]

  return (
    <div className={styles.outcomeWrap}>
      <div className={styles.topSection}>
        <Weapons weaponsList={weaponsList} />
      </div>
      <div className={styles.bottomSection}>
        <Trading
          tableType={tableType}
          ammoID={ammoID}
          isTrading={tradingBuy}
          isTradingDone={tradeBuyFinish}
          isTradingFail={tradeBuyFailed}
          price={prices.buy}
          type={BUY}
          limit={limits.buy}
          ammoTitle={ammoTitle}
          ammoTitleShort={ammoTitleShort}
        />
        <Trading
          tableType={tableType}
          ammoID={ammoID}
          isTrading={tradingSell}
          isTradingDone={tradeSellFinish}
          isTradingFail={tradeSellFailed}
          price={prices.sell}
          type={SELL}
          limit={limits.sell}
          ammoTitle={ammoTitle}
          ammoTitleShort={ammoTitleShort}
        />
      </div>
    </div>
  )
}

Outcome.defaultProps = {
  tradingData: {
    progress: {}
  }
}

export default Outcome
