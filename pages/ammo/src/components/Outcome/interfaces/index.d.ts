import { ITrading, TTableTypes } from '../../../interfaces/IStore'

export interface IProps {
  tableType: TTableTypes
  ammoID: number
  tradingData: ITrading
  ammoTitle: string
  ammoTitleShort: string
  limits: {
    buy: number
    sell: number
  }
}
