import React from 'react'
import { mount } from 'enzyme'

import Weapons from '../'

import initialState from './mocks'

describe('<Weapons />', () => {
  it('should render basic markup', () => {
    const Component = mount(<Weapons {...initialState} />)

    expect(Component.find('Memo(Carousel)').length).toBe(1)
    expect(Component.find('.imageWeapon').length).toBe(6)
    expect(
      Component.find('.imageWeapon')
        .at(0)
        .prop('id')
    ).toBe('30/large.png')
    expect(
      Component.find('.imageWeapon')
        .at(0)
        .prop('src')
    ).toBe('/images/items/30/large.png')

    expect(Component).toMatchSnapshot()
  })
})
