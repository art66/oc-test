import { IProps } from '../../../Weapons/interfaces'

const initialState: IProps = {
  weaponsList: [
    {
      ID: 1,
      ammoType: 21,
      image: '30',
      name: 'Gun'
    },
    {
      ID: 2,
      ammoType: 21,
      image: '31',
      name: 'Gun'
    },
    {
      ID: 3,
      ammoType: 21,
      image: '32',
      name: 'Gun'
    },
    {
      ID: 4,
      ammoType: 21,
      image: '33',
      name: 'Gun'
    },
    {
      ID: 5,
      ammoType: 21,
      image: '34',
      name: 'Gun'
    },
    {
      ID: 6,
      ammoType: 21,
      image: '35',
      name: 'Gun'
    }
  ],
  mediaType: 'desktop'
}

export default initialState
