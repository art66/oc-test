import { IWeapon } from '../../../../interfaces/IStore'

export interface IProps {
  weaponsList: IWeapon[],
  mediaType: 'desktop' | 'tablet' | 'mobile'
}
