import React, { memo } from 'react'
import ProgressiveImage from '@torn/shared/components/ProgressiveImage'
import Carousel from '@torn/shared/components/Carousel'

import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

const Weapons = ({ weaponsList, mediaType }: IProps) => {
  if (!weaponsList || weaponsList.length === 0) {
    return null
  }

  const getImgConfig = (weaponImg, weaponName) => ({
    image: {
      path: '/images/items/',
      name: weaponImg,
      ID: weaponImg,
      imgClass: styles.imageWeapon
    },
    preloader: {
      prlClass: styles.imageWeaponPreloader
    },
    tooltip: {
      child: weaponName,
      ID: weaponImg,
      customConfig: {
        manualCoodsFix: {
          fixX: 5
        }
      }
    }
  })

  const weaponsImagesList = weaponsList.map(weapon => {
    const weaponImg = `${weapon.image}/large.png`

    return (
      <ProgressiveImage
        key={weaponImg}
        {...getImgConfig(weaponImg, weapon.name)}
      />
    )
  })

  return (
    <Carousel mediaType={mediaType} scrollType='auto'>
      {weaponsImagesList}
    </Carousel>
  )
}

export default memo(Weapons)
