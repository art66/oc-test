import React, { memo, useState, useCallback, useEffect, useRef } from 'react'
import classnames from 'classnames'

import checkMediaType from '@torn/shared/utils/checkMediaType'
import DealConfirm from '@torn/shared/components/DealConfirm'
import MoneyInput from '@torn/shared/components/MoneyInput'
import { toMoney } from '@torn/shared/utils'

import { IProps } from './interfaces'

import styles from './index.cssmodule.scss'

const TRADING_LABELS = {
  buy: {
    buttonText: 'Buy'
  },
  sell: {
    buttonText: 'Sell'
  }
}

const Trading = ({
  mediaType,
  isManualDesktopMode,
  tableType,
  ammoTitle,
  ammoTitleShort,
  ammoID,
  price,
  limit,
  userMoney,
  type,
  ammoTradeAttempt,
  ammoTradeClose,
  isTrading,
  isTradingDone,
  isTradingFail,
  errorMsg
}: IProps) => {
  const [inputValue, setNewValue] = useState('')
  const [isTradingStart, startTrading] = useState(false)
  const [tradeStatus, setTradeStatus] = useState(false)

  const inputRef = useRef(null)

  useEffect(() => {
    // we need a delay here because of CSS animation. Otherwise we'll get an appear glitch on row!!
    setTimeout(() => {
      type === 'buy' && inputRef && inputRef.current && inputRef.current.focus()
    }, 250)
  }, [])

  useEffect(() => {
    if (!isTradingDone && inputValue) {
      setNewValue('')
    }

    if (!isTradingDone) {
      setTradeStatus(false)
    }
  }, [isTradingDone])

  const { buttonText } = TRADING_LABELS[type]
  const { isDesktop } = checkMediaType(mediaType, isManualDesktopMode)

  const preNormalizedValue = inputValue.replace(/,/g, '')
  const totalTradingSum = Number((Number(preNormalizedValue) * price).toFixed(1))

  const handleChange = useCallback(money => setNewValue(money), [])

  const handleTradeStart = useCallback(
    (e: any) => {
      e.preventDefault()

      const isNoInputValue = !preNormalizedValue || !Number(preNormalizedValue) || preNormalizedValue.length === 0

      if (isNoInputValue || isTrading) {
        return
      }

      startTrading(true)
    },
    [isTradingStart, isTrading, preNormalizedValue]
  )

  const handleCancelTrade = useCallback(() => {
    startTrading(false)
  }, [isTradingStart, isTrading, preNormalizedValue])

  const handleCloseTrade = useCallback(() => {
    startTrading(false)
    ammoTradeClose({ tableType, tradeType: type, ammoID })
  }, [isTradingStart, isTrading, isTradingDone, preNormalizedValue])

  const handleSaveTrade = useCallback(() => {
    setTradeStatus(true)
    ammoTradeAttempt({ tableType, tradeType: type, ammoID, amount: Number(preNormalizedValue) })
  }, [preNormalizedValue, tradeStatus, setTradeStatus])

  const totalText = () => {
    const sum = toMoney(totalTradingSum)

    return (
      <div className={styles.sumInfoWrap}>
        <div className={styles.textWrap}>
          <span className={`${styles.info} ${styles.text}`}>Total: </span>
          <span className={`${styles.price} ${styles.first}`}> ${sum === 'NaN' ? 0 : sum}</span>
        </div>
        <div className={styles.textWrap}>
          <span className={`${styles.info} ${styles.text} ${styles.cost}`}>Value: </span>
          <span className={`${styles.price} ${styles.last} ${styles.cost}`}> ${toMoney(Math.floor(price))} each</span>
        </div>
      </div>
    )
  }

  const tradeInputButton = () => {
    const tradeButtonClasses = classnames({
      [styles.tradingButton]: true,
      disabled: !Number(preNormalizedValue),
      'torn-btn': true
    })

    const isBuyTrading = type === 'buy'
    const isLessThanLimit = (userMoney / price) < limit

    const maxAmount = isBuyTrading && isLessThanLimit ? Math.floor(Number((userMoney / price))) : limit
    const customStyles = {
      inputBlock: styles.inputValue
    }

    return (
      <div className={styles.actionTradeWrap}>
        <MoneyInput
          disableReadOnly={true}
          disableSignButton={true}
          disableOnMountInputFocus={!isBuyTrading}
          onClick={handleTradeStart}
          onChange={handleChange}
          userMoney={maxAmount}
          customStyles={customStyles}
        />
        <button type='button' className={tradeButtonClasses} onClick={handleTradeStart}>
          {buttonText}
        </button>
      </div>
    )
  }

  const tradingSectionRender = () => {
    return (
      <div className={styles.tradingSection}>
        {totalText()}
        {tradeInputButton()}
      </div>
    )
  }

  const renderTouchscreenLayout = () => {
    return (
      <React.Fragment>
        <div className={styles.infoTouchWrap}>
          <div className={styles.totalTouchText}>{totalText()}</div>
        </div>
        <div className={styles.buttonsTouchWrap}>{tradeInputButton()}</div>
      </React.Fragment>
    )
  }

  const renderDesktopLayout = () => tradingSectionRender()

  const renderTrading = () => {
    return isDesktop ? renderDesktopLayout() : renderTouchscreenLayout()
  }

  const renderConfirmation = () => {
    const boughtInfo = {
      suffixLabel: 'for',
      isOwnerDisabled: true,
      isTradingPlaceDisabled: true
    }

    return (
      <DealConfirm
        error={(isTrading && { errorMessage: errorMsg }) || {}}
        price={Math.floor(price)}
        tradeLabel={type}
        itemTitle={ammoTitle}
        itemTitleShort={ammoTitleShort}
        boughtAmount={Number(preNormalizedValue)}
        boughtInfo={boughtInfo}
        isFetchActive={isTrading}
        isDealComplete={isTradingDone}
        isError={isTradingFail}
        onSave={handleSaveTrade}
        onCancel={handleCancelTrade}
        onClose={handleCloseTrade}
      />
    )
  }

  return <div className={styles.tradingWrap}>{isTradingStart ? renderConfirmation() : renderTrading()}</div>
}

export default memo(Trading)
