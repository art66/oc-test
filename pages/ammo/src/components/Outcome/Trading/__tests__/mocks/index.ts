import { IProps } from '../../interfaces'

const initialState: IProps = {
  mediaType: 'desktop',
  isManualDesktopMode: false,
  tableType: 'equipped',
  ammoID: null,
  price: 10,
  type: 'buy',
  limit: 99999999,
  ammoTitle: 'test_title',
  ammoTitleShort: 'test',
  isTrading: false,
  isTradingDone: false,
  isTradingFail: false,
  ammoTradeClose: () => {},
  ammoTradeAttempt: () => {}
}

const mobileState: IProps = {
  ...initialState,
  mediaType: 'tablet',
  isManualDesktopMode: false
}

export { mobileState }
export default initialState
