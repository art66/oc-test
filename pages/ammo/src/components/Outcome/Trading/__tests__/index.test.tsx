/* eslint-disable max-statements */
import React from 'react'
import { mount } from 'enzyme'

import Trading from '..'

import initialState, { mobileState } from './mocks'

const firstTest = 'should render the BUY component without active button, inputted value in desktop mode'
const secondTest = 'should render the BUY component without active button, inputted value in touchscreen mode'

describe('<Trading />', () => {
  it(firstTest, () => {
    const Component = mount(<Trading {...initialState} />)

    expect(Component.find('.tradingWrap').length).toBe(1)
    expect(Component.find('.tradingSection').length).toBe(1)
    expect(Component.find('.info.text').at(0).text()).toBe('Total: ')
    expect(Component.find('.price.first').at(0).text()).toBe(' $0')
    expect(Component.find('.info.text.cost').text()).toBe('Value: ')
    expect(Component.find('.price.last.cost').text()).toBe(' $10 each')
    expect(Component.find('.actionTradeWrap').length).toBe(1)
    expect(Component.find('MoneyInput').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('.tradingButton').length).toBe(1)
    expect(Component.find('.tradingButton.activeButton').length).toBe(0)

    // testing values in the buttons section
    expect(Component.find('input').prop('value')).toBe('')
    expect(
      Component.find('.tradingButton')
        .at(0)
        .text()
    ).toBe('Buy')

    expect(Component).toMatchSnapshot()
  })
  it(secondTest, () => {
    const Component = mount(<Trading {...mobileState} />)

    expect(Component).toMatchSnapshot()

    expect(Component.find('.tradingWrap').length).toBe(1)
    expect(Component.find('.infoTouchWrap').length).toBe(1)
    expect(Component.find('.info.text').at(0).text()).toBe('Total: ')
    expect(Component.find('.price.first').at(0).text()).toBe(' $0')
    expect(Component.find('.info.text.cost').text()).toBe('Value: ')
    expect(Component.find('.price.last.cost').text()).toBe(' $10 each')
    expect(Component.find('.buttonsTouchWrap').length).toBe(1)
    expect(Component.find('MoneyInput').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('.tradingButton').length).toBe(1)
    expect(Component.find('.tradingButton.activeButton').length).toBe(0)

    // testing values in the buttons section
    expect(Component.find('input').prop('value')).toBe('')
    expect(
      Component.find('.tradingButton')
        .at(0)
        .text()
    ).toBe('Buy')

    expect(Component).toMatchSnapshot()
  })
  // TODO: we can't test trading/deal switching because of the react useState abstraction.
})
