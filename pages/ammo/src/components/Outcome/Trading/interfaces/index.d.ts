import { TTableTypes } from '../../../../interfaces/IStore'

export interface IAmmoAttemptCallback {
  tableType: TTableTypes
  tradeType: 'buy' | 'sell'
  ammoID: number
  amount: number
}

export interface IAmmoCloseCallback {
  tableType: TTableTypes
  tradeType: 'buy' | 'sell'
  ammoID: number
}

export interface IProps {
  errorMsg: string
  mediaType: any
  isManualDesktopMode: boolean
  tableType: TTableTypes
  ammoID: number
  price: number
  type: 'buy' | 'sell'
  ammoTitle: string
  ammoTitleShort: string
  isTrading: boolean
  isTradingDone: boolean
  isTradingFail: boolean
  limit: number
  userMoney?: number
  ammoTradeAttempt: ({ tableType, tradeType, ammoID, amount }: IAmmoAttemptCallback) => void
  ammoTradeClose: ({ tableType, tradeType: type, ammoID }: IAmmoCloseCallback) => void
}
