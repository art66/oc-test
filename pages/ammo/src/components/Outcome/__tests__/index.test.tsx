import React from 'react'
import { shallow } from 'enzyme'

import Outcome from '../'

import initialState from './mocks'

describe('<Outcome />', () => {
  it('should render basic markup', () => {
    const Component = shallow(<Outcome {...initialState} />)

    expect(Component.find('.outcomeWrap').length).toBe(1)
    expect(Component.find('.topSection').length).toBe(1)
    expect(Component.find('.topSection > Connect(Component)').length).toBe(1)
    expect(Component.find('.bottomSection').length).toBe(1)
    expect(Component.find('.bottomSection > Connect(Component)').length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
})
