import { IProps } from '../../interfaces'

const initialState: IProps = {
  tableType: 'equipped',
  ammoID: null,
  tradingData: {
    prices: {
      buy: 0,
      sell: 0
    },
    weaponsList: null,
    progress: null
  },
  limits: {
    buy: 99999999,
    sell: 0
  },
  ammoTitle: 'test_title',
  ammoTitleShort: 'title'
}

export default initialState
