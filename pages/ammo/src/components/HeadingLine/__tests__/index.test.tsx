import React from 'react'
import { mount } from 'enzyme'

import HeadingLine from '../'

describe('<HeadingLine />', () => {
  it('should render basic markup', () => {
    const Component = mount(<HeadingLine title='test' />)

    expect(Component.find('.title-black.top-round.ng-binding').length).toBe(1)
    expect(Component.find('.title-black.top-round.ng-binding').text()).toBe('test')

    expect(Component).toMatchSnapshot()
  })
})
