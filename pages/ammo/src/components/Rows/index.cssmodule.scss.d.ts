// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'activeRow': string;
  'ammoButtonWrap': string;
  'ammoButtonWrapActive': string;
  'ammoImage': string;
  'ammoSection': string;
  'ammoSectionButton': string;
  'amountNotEmpty': string;
  'buttonClicked': string;
  'buyButton': string;
  'buySection': string;
  'clickableSection': string;
  'flexCenter': string;
  'globalSvgShadow': string;
  'isSectionActivated': string;
  'leftCell': string;
  'lineContainer': string;
  'mobileSectionsWrap': string;
  'preferenceActivation': string;
  'preferenceInProgress': string;
  'rightCell': string;
  'rowAppear': string;
  'rowSection': string;
  'rowWrap': string;
  'rowWrapActive': string;
  'row_bg__0': string;
  'row_bg__1': string;
  'row_bg__10': string;
  'row_bg__11': string;
  'row_bg__12': string;
  'row_bg__13': string;
  'row_bg__14': string;
  'row_bg__15': string;
  'row_bg__16': string;
  'row_bg__17': string;
  'row_bg__18': string;
  'row_bg__19': string;
  'row_bg__2': string;
  'row_bg__20': string;
  'row_bg__3': string;
  'row_bg__4': string;
  'row_bg__5': string;
  'row_bg__6': string;
  'row_bg__7': string;
  'row_bg__8': string;
  'row_bg__9': string;
  'rowsContainer': string;
  'section': string;
  'sectionDimenstions': string;
  'sectionWrap': string;
  'sectionsContainer': string;
  'text': string;
  'textCount': string;
  'textTitle': string;
}
export const cssExports: CssExports;
export default cssExports;
