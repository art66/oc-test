import { IRowsProps } from '../../interfaces'

const initialState: IRowsProps = {
  tableType: 'equipped',
  activeRowID: null,
  inProgress: false,
  data: [
    {
      ammoID: 2,
      itemName: 'Firegun',
      preference: {
        inProgress: false,
        nextActiveSection: null
      },
      activeCatID: null,
      rowData: {
        ammo: {
          title: 'test_title',
          titleShort: 'test',
          image: 'img_test'
        },
        standard: 0,
        hollowpoint: 0,
        piercing: 0,
        tracer: 0,
        incendiary: 0
      },
      trading: {
        progress: {
          buy: false,
          sell: false,
          buyTradeDone: false,
          sellTradeDone: false,
          buyTradeFail: false,
          sellTradeFail: false
        },
        prices: {
          buy: 0,
          sell: 0
        },
        weaponsList: []
      }
    }
  ]
}

const withoutData = {
  ...initialState,
  data: null
}

const withSkeleton = {
  ...initialState,
  inProgress: true
}

export { withoutData, withSkeleton }
export default initialState
