import { IRowProps } from '../../interfaces'

const initialState: IRowProps = {
  isManualDesktopMode: false,
  mediaType: 'desktop',
  ammoID: null,
  itemName: 'Firegun',
  preference: {
    inProgress: false,
    nextActiveSection: null
  },
  activeCatID: null,
  sections: {
    ammo: {
      title: 'test_title',
      titleShort: 'test',
      image: '34'
    },
    standard: 0,
    hollowpoint: 0,
    piercing: 0,
    tracer: 0,
    incendiary: 0
  },
  trading: {
    progress: {
      buy: false,
      sell: false,
      buyTradeDone: false,
      sellTradeDone: false,
      buyTradeFail: false,
      sellTradeFail: false
    },
    prices: {
      buy: 0,
      sell: 0
    },
    weaponsList: []
  },
  tableType: 'equipped',
  ID: 0,
  activeRowID: null,
  setCurrentActiveRow: ({ tableType, ID }) => ({ tableType, ID }),
  deactivateCurrentRow: tableType => tableType
}

const outcomeState = {
  ...initialState,
  activeRowID: 0
}

const touchscreenState = {
  ...initialState,
  mediaType: 'tablet'
}

const touchscreenActiveState = {
  ...initialState,
  activeRowID: 0,
  mediaType: 'tablet'
}

export { outcomeState, touchscreenState, touchscreenActiveState }
export default initialState
