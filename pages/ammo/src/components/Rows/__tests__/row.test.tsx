import React from 'react'
import { shallow } from 'enzyme'

import Row from '../Row'

import initialState, { outcomeState, touchscreenState, touchscreenActiveState } from './mocks/row'

describe('<Row />', () => {
  it('should render basic desktop row', () => {
    const Component = shallow(<Row {...initialState} />)

    expect(Component.find('.rowWrap.row_bg__0').length).toBe(1)
    expect(Component.find('.sectionsContainer').length).toBe(1)
    expect(Component.find('Memo(AmmoSection)').length).toBe(1)
    expect(Component.find('Memo(StandardSection)').length).toBe(1)
    expect(Component.find('Memo(StatSection)').length).toBe(4)

    expect(Component).toMatchSnapshot()
  })
  it('should render active desktop row with outcome', () => {
    const Component = shallow(<Row {...outcomeState} />)

    expect(Component.find('.rowWrap.row_bg__0').length).toBe(1)
    expect(Component.find('.sectionsContainer').length).toBe(1)
    expect(Component.find('Memo(AmmoSection)').length).toBe(1)
    expect(Component.find('Memo(StandardSection)').length).toBe(1)
    expect(Component.find('Memo(StatSection)').length).toBe(4)
    expect(Component.find('Outcome').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render basic touchscreen row', () => {
    const Component = shallow(<Row {...touchscreenState} />)

    expect(Component.find('.rowWrap.row_bg__0').length).toBe(1)
    expect(Component.find('.mobileSectionsWrap').length).toBe(1)
    expect(Component.find('Memo(AmmoSection)').length).toBe(1)
    expect(Component.find('Memo(StandardSection)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render active touchscreen row', () => {
    const Component = shallow(<Row {...touchscreenActiveState} />)

    expect(Component.find('.rowWrap.row_bg__0').length).toBe(1)
    expect(Component.find('.mobileSectionsWrap').length).toBe(1)
    expect(Component.find('Memo(AmmoSection)').length).toBe(1)
    expect(Component.find('Memo(StandardSection)').length).toBe(1)
    expect(Component.find('Outcome').length).toBe(1)
    expect(Component.find('Memo(StatSection)').length).toBe(4)

    expect(Component).toMatchSnapshot()
  })
})
