import React from 'react'
import { shallow } from 'enzyme'

import Rows from '../'

import initialState, { withoutData, withSkeleton } from './mocks'

describe('<Rows />', () => {
  it('should render basic markup', () => {
    const Component = shallow(<Rows {...initialState} />)

    expect(Component.find('.rowsContainer').length).toBe(1)
    expect(Component.find('Connect(Row)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render rows without data', () => {
    const Component = shallow(<Rows {...withoutData} />)

    expect(Component.find('.rowsContainer').length).toBe(0)
    expect(Component.find('Connect(Row)').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render rows placeholder while data in fetch progress', () => {
    const Component = shallow(<Rows {...withSkeleton} />)

    expect(Component.find('Memo(RowsSkeleton)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
