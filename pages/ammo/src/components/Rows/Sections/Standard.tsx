import React, { memo } from 'react'
import classnames from 'classnames'

import Section from '../../../containers/Rows/Section'

import styles from '../index.cssmodule.scss'
import { IStandardProps } from '../interfaces'

const BUY_LABEL = 'Buy'

const StandardSection = ({ isRowActivated, onClick }: IStandardProps) => {
  // const amountNotEmpty = value > 0

  const standardClasses = {
    button: classnames({
      [styles.buyButton]: true,
      [styles.buttonClicked]: isRowActivated
      // [styles.activeButton]: amountNotEmpty
    })
  }

  const child = (
    <React.Fragment>
      <button type='button' className={standardClasses.button} onClick={onClick}>
        {BUY_LABEL}
      </button>
    </React.Fragment>
  )

  return <Section children={child} customClass={styles.buySection} />
}

export default memo(StandardSection)
