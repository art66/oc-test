import React, { memo } from 'react'
import isValue from '@torn/shared/utils/isValue'

import Section from '../../../containers/Rows/Section'
import { IStatProps } from '../interfaces'

const StatSection = ({ value, catID, tableType, ammoID, preference, activeCatID }: IStatProps) => {
  return (
    <Section
      ID={catID}
      tableType={tableType}
      ammoID={ammoID}
      children={isValue(value) ? value : '--'}
      isRaw={true}
      isClickable={true}
      preference={preference}
      isSectionActivated={activeCatID === catID}
    />
  )
}

export default memo(StatSection)
