import React from 'react'
import { shallow } from 'enzyme'

import Stat from '../Stat'

import initialState from './mocks/stat'

describe('<Stat />', () => {
  it('should render basic stat section based on SectionFactory component', () => {
    const Component = shallow(<Stat {...initialState} />)

    expect(Component.find('Connect(Component)').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
