import React from 'react'
import { shallow } from 'enzyme'

import Standard from '../Standard'

import initialState, { activeButtonState } from './mocks/standard'

describe('<Standard />', () => {
  it('should render basic standard section with button in desktop mode', () => {
    const Component = shallow(<Standard {...initialState} />)

    expect(Component.find('Connect(Component)').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('button').text()).toBe('Buy')

    expect(Component).toMatchSnapshot()
  })
  it('should render basic standard section with active button in desktop mode', () => {
    const Component = shallow(<Standard {...activeButtonState} />)

    expect(Component.find('Connect(Component)').length).toBe(1)
    expect(Component.find('button.buttonClicked').length).toBe(1)
    expect(Component.find('button').text()).toBe('Buy')

    expect(Component).toMatchSnapshot()
  })
})
