import { IStatProps } from '../../../interfaces'

const initialState: IStatProps = {
  value: 'test_value',
  catID: 0,
  tableType: 'equipped',
  ammoID: 0,
  preference: {
    inProgress: false,
    nextActiveSection: 1
  },
  activeCatID: 2
}

export default initialState
