import React from 'react'
import { ISectionProps } from '../../../interfaces'

const initialState: ISectionProps = {
  ID: 0,
  preference: {
    inProgress: false,
    nextActiveSection: null
  },
  tableType: 'equipped',
  ammoID: 0,
  children: 981,
  isSectionActivated: false,
  customClass: '',
  isDesktop: false,
  isClickable: false,
  isRaw: true,
  setPreference: ({ tableType, ammoID, catID }) => ({ tableType, ammoID, catID })
}

const customChildren: ISectionProps = {
  ...initialState,
  isRaw: false,
  children: <span>test_child</span>
}

const clickableSection = {
  ...initialState,
  isClickable: true
}

const isProgressState = {
  ...initialState,
  isClickable: true,
  preference: {
    nextActiveSection: 1,
    inProgress: true
  },
  ID: 1
}

const customClass = {
  ...initialState,
  customClass: 'customClass'
}

const amountNotEmpty = {
  ...initialState,
  children: 0
}

export { customChildren, clickableSection, isProgressState, customClass, amountNotEmpty }
export default initialState
