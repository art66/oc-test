import { IAmmoProps } from '../../../interfaces'

const initialState: IAmmoProps = {
  value: 20,
  title: 'test_title',
  tableType: 'equipped',
  itemName: 'Firegun',
  image: '34',
  isRowActivated: false,
  isDesktop: true,
  onClick: () => {}
}

const emptyValueState = {
  ...initialState,
  value: 0,
  isDesktop: false
}

const someValueState = {
  ...initialState,
  isRowActivated: true,
  isDesktop: false
}

export { emptyValueState, someValueState }
export default initialState
