import { IStandardProps } from '../../../interfaces'

const initialState: IStandardProps = {
  isDesktop: true,
  isRowActivated: false,
  onClick: () => {}
}

const activeButtonState: IStandardProps = {
  ...initialState,
  isRowActivated: true
}

export { activeButtonState }
export default initialState
