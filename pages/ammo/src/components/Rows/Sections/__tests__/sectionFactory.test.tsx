import React from 'react'
import { shallow } from 'enzyme'

import SectionFactory from '../SectionFactory'

import initialState, {
  customChildren,
  clickableSection,
  isProgressState,
  customClass,
  amountNotEmpty
} from './mocks/sectionFactory'

describe('<SectionFactory />', () => {
  it('should render basic section with a raw value inside', () => {
    const Component = shallow(<SectionFactory {...initialState} />)

    expect(Component).toMatchSnapshot()

    expect(Component.find('div.rowSection').length).toBe(1)
    expect(Component.find('.text').length).toBe(2)
    expect(Component.find('.text').at(1).text()).toBe('981')

    expect(Component).toMatchSnapshot()
  })
  it('should render basic section with a custom value inside', () => {
    const Component = shallow(<SectionFactory {...customChildren} />)

    expect(Component.find('div.rowSection').length).toBe(1)
    expect(Component.find('span').text()).toBe('test_child')

    expect(Component).toMatchSnapshot()
  })
  it('should render a clickable section with a raw value inside', () => {
    const Component = shallow(<SectionFactory {...clickableSection} />)

    expect(Component.find('button.rowSection.clickableSection').length).toBe(1)
    expect(Component.find('span').at(1).text()).toBe('981')

    expect(Component).toMatchSnapshot()
  })
  it('should add animation class in case of fetch progress on section', () => {
    const Component = shallow(<SectionFactory {...isProgressState} />)

    expect(Component.find('button.rowSection.clickableSection.preferenceInProgress').length).toBe(1)
    expect(Component.find('span').at(1).text()).toBe('981')

    expect(Component).toMatchSnapshot()
  })
  it('should add section with a custom class', () => {
    const Component = shallow(<SectionFactory {...customClass} />)

    expect(Component.find('div.rowSection.customClass').length).toBe(1)
    expect(Component.find('span').at(1).text()).toBe('981')

    expect(Component).toMatchSnapshot()
  })
  it('should not add special class "amountNotEmpty" for section with positive values', () => {
    const Component = shallow(<SectionFactory {...amountNotEmpty} />)

    expect(Component.find('div.rowSection').length).toBe(1)
    expect(Component.find('div.rowSection.amountNotEmpty').length).toBe(0)
    expect(Component.find('span').at(1).text()).toBe('0')

    expect(Component).toMatchSnapshot()
  })
})
