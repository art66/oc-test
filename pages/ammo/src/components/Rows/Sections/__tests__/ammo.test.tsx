import React from 'react'
import { shallow } from 'enzyme'

import Ammo from '../Ammo'

import { emptyValueState, someValueState } from './mocks/ammo'

describe('<Ammo />', () => {
  it('should render basic Ammo section with empty value', () => {
    const Component = shallow(<Ammo {...emptyValueState} />)

    expect(Component.find('Connect(Component)').length).toBe(1)
    expect(Component.find('Connect(Component)').prop('customClass')).toBe('ammoSection')
    expect(Component.find('Connect(Component)').prop('customClass')).toBe('ammoSection')
    expect(Component.find('.rightCell').length).toBe(1)
    expect(Component.find('i').length).toBe(1)
    expect(Component.find('i').prop('className')).toBe('ammoImage 34')
    expect(Component.find('.rightCell > span').length).toBe(1)
    expect(Component.find('.rightCell > span').text()).toBe('test_title')
    expect(Component.find('.leftCell').length).toBe(1)
    expect(Component.find('.leftCell > span.textCount').text()).toBe('0')

    expect(Component).toMatchSnapshot()
  })
  it('should render basic Ammo section with some value', () => {
    const Component = shallow(<Ammo {...someValueState} />)

    expect(Component.find('Connect(Component)').length).toBe(1)
    expect(Component.find('Connect(Component)').prop('customClass')).toBe('ammoSection')
    expect(Component.find('Connect(Component)').prop('customClass')).toBe('ammoSection')
    expect(Component.find('.rightCell').length).toBe(1)
    expect(Component.find('i').length).toBe(1)
    expect(Component.find('i').prop('className')).toBe('ammoImage 34')
    expect(Component.find('.rightCell > span').length).toBe(1)
    expect(Component.find('.rightCell > span').text()).toBe('test_title')
    expect(Component.find('.leftCell').length).toBe(1)
    expect(Component.find('.leftCell > span.textCount').text()).toBe('20')

    expect(Component).toMatchSnapshot()
  })
})
