import React, { useEffect, memo } from 'react'
import classnames from 'classnames'
import { toMoney } from '@torn/shared/utils'
import keyDownHandler, { KEY_TYPES } from '@torn/shared/utils/keyDownHandler'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import Section from '../../../containers/Rows/Section'

import styles from '../index.cssmodule.scss'
import { IAmmoProps } from '../interfaces'

const AmmoSection = ({ tableType, itemName, title, image, value, onClick }: IAmmoProps) => {
  // const amountNotEmpty = value > 0
  useEffect(() => {
    if (tableType !== 'equipped') {
      return
    }

    tooltipsSubscriber.render({
      tooltipsList: [{
        ID: `ammo_${title}`,
        child: itemName
      }],
      type: 'mount'
    })
  }, [])

  const textClass = classnames({
    [styles.textCount]: true
    // [styles.amountNotEmpty]: amountNotEmpty
  })

  const handleKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
    keyDownHandler({
      event: e,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: onClick
        },
        {
          type: KEY_TYPES.enter,
          onEvent: onClick
        }
      ]
    })
  }

  const child = (
    <div
      id={`ammo_${title}`}
      aria-label={`Buy ${title}. Current amount: ${toMoney(value)}`}
      role='button'
      tabIndex={0}
      className={styles.ammoSectionButton}
      onClick={onClick}
      onKeyDown={handleKeyDown}
    >
      <div className={styles.rightCell}>
        <i className={`${styles.ammoImage} ${image}`} />
        <span className={styles.text}>{title}</span>
      </div>
      <div className={styles.leftCell}>
        <span className={textClass}>{toMoney(value)}</span>
      </div>
    </div>
  )

  return <Section children={child} customClass={styles.ammoSection} />
}

export default memo(AmmoSection)
