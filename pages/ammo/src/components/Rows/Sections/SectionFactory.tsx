import React, { memo, useEffect } from 'react'
import classnames from 'classnames'
import { toMoney } from '@torn/shared/utils'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import { COLUMN_TITLES } from '../../../constants'
import styles from '../index.cssmodule.scss'
import { ISectionProps } from '../interfaces'

const Section = ({
  ID,
  tableType,
  ammoID,
  children,
  customClass,
  isRaw,
  isSectionActivated,
  isClickable,
  isDesktop,
  preference,
  setPreference
}: ISectionProps) => {
  useEffect(() => {
    const { tooltip, titles: titleList } = COLUMN_TITLES[ID] || {}

    !isDesktop && tooltip && titleList && tooltipsSubscriber.subscribe({
      child: tooltip,
      ID: titleList.desktop
    })
  })

  const isChildrenAsAValue = typeof children === 'number'
  const isPositiveValueChildren = isChildrenAsAValue && children > 0
  const isClickableSection = isClickable && (isPositiveValueChildren || isSectionActivated)
  const CustomTag = isClickableSection ? 'button' : 'div'

  const sectionClass = classnames({
    [styles.rowSection]: true,
    [styles.isSectionActivated]: isSectionActivated,
    [styles.clickableSection]: isClickableSection,
    [styles.preferenceInProgress]: preference.inProgress && preference.nextActiveSection === ID,
    [styles.amountNotEmpty]: isPositiveValueChildren,
    [customClass]: customClass
  })

  const { titles } = COLUMN_TITLES.find(columnMock => columnMock.ID === ID) || {}
  const patronsAmount = toMoney(String(children))

  const handlerClick = () => !preference.inProgress && setPreference({ tableType, ammoID, catID: ID })

  const getAriaLabel = () => {
    if (!isClickableSection) {
      return ''
    }

    const leadLabel = isSectionActivated ? 'Deactivate' : 'Activate'

    return `${leadLabel} ${titles?.desktop} type with amount of ${patronsAmount}`
  }

  const defaultContentToRender = (
    <React.Fragment>
      {!isDesktop && titles && <span className={`${styles.text} ${styles.textTitle}`}>{titles.desktop}: </span>}
      <span className={styles.text}>{patronsAmount}</span>
    </React.Fragment>
  )

  return (
    <CustomTag
      aria-label={getAriaLabel()}
      id={!isDesktop && titles ? titles.desktop : ''}
      className={sectionClass}
      onClick={isClickableSection ? handlerClick : undefined}
    >
      {!isRaw ? children : defaultContentToRender}
    </CustomTag>
  )
}

Section.defaultProps = {
  preference: {}
}

export default memo(Section)
