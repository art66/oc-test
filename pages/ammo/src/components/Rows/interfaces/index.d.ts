import { IRow, IRowSections, ITrading, TTableTypes } from '../../../interfaces/IStore'

export interface IRowsProps {
  tableType: TTableTypes
  activeRowID: number
  inProgress: boolean
  data: IRow[]
}

export interface IRowProps {
  isManualDesktopMode: boolean
  mediaType: any
  ammoID: number
  itemName: string
  preference: {
    inProgress: boolean
    nextActiveSection: number
  }
  activeCatID: number
  sections: IRowSections
  trading: ITrading
  tableType: TTableTypes
  ID: number
  activeRowID: number
  setCurrentActiveRow: ({ tableType, ID }: { tableType: TTableTypes; ID: number }) => void
  deactivateCurrentRow: ({ tableType }: { tableType: TTableTypes }) => void
}

export interface IAmmoProps {
  itemName: string
  tableType: TTableTypes
  value: number
  title: string
  image: string
  isRowActivated: boolean
  // isAmountNotEmpty: boolean
  isDesktop: boolean
  onClick: () => void
}

export interface ISetPrefArgs {
  tableType: TTableTypes
  ammoID: number
  catID: number
}

export interface ISectionProps {
  ID?: number
  preference?: {
    inProgress: boolean
    nextActiveSection: number
  }
  tableType?: TTableTypes
  ammoID?: number
  children: string | number | JSX.Element
  isSectionActivated?: boolean
  customClass?: string
  isClickable?: boolean
  isDesktop: boolean
  isRaw?: boolean
  setPreference?: ({ tableType, ammoID, catID }: ISetPrefArgs) => void
}

export interface IStandardProps {
  isDesktop: boolean
  // isAmountNotEmpty: boolean
  // value: number
  isRowActivated: boolean
  onClick: () => void
}

export interface IStatProps {
  value: string | number
  catID: number
  tableType: TTableTypes
  ammoID: number
  preference: {
    inProgress: boolean
    nextActiveSection: number
  }
  activeCatID: number
}
