import React from 'react'

import Row from '../../containers/Rows/Row'
import RowsSkeleton from '../Skeletons/Rows'

import styles from './index.cssmodule.scss'

import { IRowsProps } from './interfaces'

const Rows = ({ activeRowID, inProgress, data, tableType }: IRowsProps) => {
  if (inProgress) {
    return <RowsSkeleton tableType={tableType} />
  }

  if (!data || data.length === 0) {
    return null
  }

  const rows = data.map(({ ammoID, itemName, preference, activeCatID, rowData, trading }, index) => {
    return (
      <Row
        preference={preference}
        itemName={itemName}
        ammoID={ammoID}
        activeCatID={activeCatID}
        key={`${tableType}_${ammoID}`}
        sections={rowData}
        trading={trading}
        ID={index}
        activeRowID={activeRowID}
        tableType={tableType}
      />
    )
  })

  return <div className={`${styles.rowsContainer}`}>{rows}</div>
}

export default Rows
