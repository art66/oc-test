import React from 'react'
import isValue from '@torn/shared/utils/isValue'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import Ammo from './Sections/Ammo'
import Standard from './Sections/Standard'
import Stat from './Sections/Stat'
import Outcome from '../Outcome'

import { IRowProps } from './interfaces'
import { BUY_LIMIT } from '../../constants'
import styles from './index.cssmodule.scss'

const Row = ({
  ammoID,
  itemName,
  preference,
  activeCatID,
  sections,
  trading,
  ID,
  tableType,
  activeRowID,
  setCurrentActiveRow,
  deactivateCurrentRow,
  isManualDesktopMode,
  mediaType
}: IRowProps) => {
  const { ammo, standard, hollowpoint, piercing, tracer, incendiary } = sections

  const isRowActivated = activeRowID === ID
  // const isAmountNotEmpty = standard > 0

  const { isDesktop } = checkMediaType(mediaType, isManualDesktopMode)

  const handlerClick = () => {
    if (isRowActivated) {
      deactivateCurrentRow({ tableType })

      return
    }

    setCurrentActiveRow({ tableType, ID })
  }

  const ammoSection = () => (
    <Ammo
      itemName={itemName}
      tableType={tableType}
      title={ammo.title}
      image={ammo.image}
      // isAmountNotEmpty={isAmountNotEmpty}
      isDesktop={isDesktop}
      value={standard}
      onClick={handlerClick}
      isRowActivated={isRowActivated}
    />
  )

  const standardSection = () => (
    <Standard
      isDesktop={isDesktop}
      // isAmountNotEmpty={isAmountNotEmpty}
      // value={standard}
      onClick={handlerClick}
      isRowActivated={isRowActivated}
    />
  )

  const statSection = ({ value, ID: catID }: { value: string | number; ID: number }) => {
    return (
      <Stat
        catID={catID}
        tableType={tableType}
        ammoID={ammoID}
        value={isValue(value) ? value : '--'}
        preference={preference}
        activeCatID={activeCatID}
      />
    )
  }

  const statsSections = () => {
    return (
      <React.Fragment>
        {statSection({ value: hollowpoint, ID: 2 })}
        {statSection({ value: tracer, ID: 4 })}
        {statSection({ value: piercing, ID: 3 })}
        {statSection({ value: incendiary, ID: 5 })}
      </React.Fragment>
    )
  }

  const outcomeRender = () => {
    if (!isRowActivated) {
      return null
    }

    return (
      <Outcome
        tradingData={trading}
        ammoTitle={sections?.ammo?.title}
        ammoTitleShort={sections?.ammo?.titleShort}
        tableType={tableType}
        ammoID={ammoID}
        limits={{ buy: BUY_LIMIT - standard, sell: standard }}
      />
    )
  }

  // just for clarify and more understandable code
  const buyButton = standardSection

  const ammoRowRender = () => {
    if (!isDesktop) {
      const sectionsToRender = (
        <div className={styles.sectionsContainer}>
          {statsSections()}
        </div>
      )

      return (
        <React.Fragment>
          <div className={styles.mobileSectionsWrap}>
            {ammoSection()}
            {buyButton()}
          </div>
          {outcomeRender()}
          {isRowActivated && sectionsToRender}
        </React.Fragment>
      )
    }

    return (
      <React.Fragment>
        <div className={styles.sectionsContainer}>
          {ammoSection()}
          {buyButton()}
          {statsSections()}
        </div>
        {outcomeRender()}
      </React.Fragment>
    )
  }

  return (
    <div
      className={`${styles.rowWrap} ${styles[`row_bg__${ID}`]} ${isRowActivated ? styles.activeRow : ''}`}
    >
      {ammoRowRender()}
    </div>
  )
}

export default Row
