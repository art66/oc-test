import React from 'react'
import { mount } from 'enzyme'

import Skeletons from '../Rows'

describe('<Skeletons />', () => {
  it('should render basic skeletons markup', () => {
    const Component = mount(<Skeletons />)

    expect(Component.find('.rowContainer').length).toBe(6)
    expect(Component.find('.sectionSkeleton').length).toBe(36)

    expect(Component).toMatchSnapshot()
  })
})
