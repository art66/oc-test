import React, { memo } from 'react'

import styles from './index.cssmodule.scss'
import globalStyles from '../../styles/global.cssmodule.scss'
import { TTableTypes } from '../../interfaces/IStore'

const RowsSkeleton = ({ tableType }: { tableType?: TTableTypes }) => {
  const rowsCount = tableType === 'equipped' ? 2 : 6

  const renderRows = Array.from(Array(rowsCount).keys()).map(row => {
    return (
      <div key={row} className={styles.rowContainer}>
        <div className={`${globalStyles.section} ${styles.sectionSkeleton}`}>
          <span className={styles.ammoIcon} />
          <span className={styles.ammoTitle} />
          <span className={styles.ammoValue} />
        </div>
        <div className={`${globalStyles.section} ${styles.sectionSkeleton}`} />
        <div className={`${globalStyles.section} ${styles.sectionSkeleton}`} />
        <div className={`${globalStyles.section} ${styles.sectionSkeleton}`} />
        <div className={`${globalStyles.section} ${styles.sectionSkeleton}`} />
        <div className={`${globalStyles.section} ${styles.sectionSkeleton}`} />
      </div>
    )
  })

  return <div className={styles.rowsSkeletonsWrap}>{renderRows}</div>
}

export default memo(RowsSkeleton)
