import { SHOW_DEBUG_BOX, HIDE_DEBUG_BOX, SHOW_INFO_BOX, HIDE_INFO_BOX, MANUAL_DESKTOP_MODE } from '../../constants'
import { IDebugShow, IType, IInfoShow, TDebugMessage, IManualDesktopStatus } from '../../interfaces/IController'

export const debugShow = (error: TDebugMessage): IDebugShow => ({
  msg: error,
  type: SHOW_DEBUG_BOX
})

export const debugHide = (): IType => ({
  type: HIDE_DEBUG_BOX
})

export const infoShow = (info: TDebugMessage): IInfoShow => ({
  msg: info,
  type: SHOW_INFO_BOX
})

export const infoHide = (): IType => ({
  type: HIDE_INFO_BOX
})

export const checkManualDesktopMode = ({ status }): IManualDesktopStatus => ({
  type: MANUAL_DESKTOP_MODE,
  status
})
