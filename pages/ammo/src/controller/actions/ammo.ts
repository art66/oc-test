import {
  MAIN_DATA_FETCH_ATTEMPT,
  MAIN_DATA_FETCHED,
  ROW_ACTIVATED,
  ROW_DEACTIVATED,
  TRADE_AMMO_ATTEMPT,
  TRADE_AMMO_FETCHED,
  TRADE_AMMO_CLOSE,
  TRADE_AMMO_FAILED,
  SET_PREFERENCE_ATTEMPT,
  SET_PREFERENCE_FETCHED,
  USER_MONEY_UPDATED
} from '../../constants'

import {
  IMainDataFetched,
  IType,
  IActivatedClickedRow,
  IDeactivatedClickedRow,
  ITradeUpdated,
  ITradeClose,
  ISetPreference,
  ITradeAttempt
} from '../../interfaces/IController'
import { IAmmoRowsData } from '../../interfaces/IStore'

export const mainDataFetchStart = (): IType => ({
  type: MAIN_DATA_FETCH_ATTEMPT
})

export const mainDataFetched = ({ equipped, unequipped, user }: IAmmoRowsData): IMainDataFetched => ({
  type: MAIN_DATA_FETCHED,
  equipped,
  unequipped,
  user
})

export const activateRow = ({ tableType, ID }): IActivatedClickedRow => ({
  type: ROW_ACTIVATED,
  tableType,
  ID
})

export const deactivateRow = ({ tableType }): IDeactivatedClickedRow => ({
  type: ROW_DEACTIVATED,
  tableType
})

export const ammoTradeAttempt = ({ tableType, ammoID, tradeType, amount }): ITradeAttempt => ({
  type: TRADE_AMMO_ATTEMPT,
  tableType,
  tradeType,
  ammoID,
  amount
})

export const ammoTradeClose = ({ tableType, tradeType, ammoID }): ITradeClose => ({
  type: TRADE_AMMO_CLOSE,
  tableType,
  tradeType,
  ammoID
})

export const ammoTradeFailed = ({ tableType, tradeType, ammoID, error }): ITradeClose => ({
  type: TRADE_AMMO_FAILED,
  tableType,
  tradeType,
  ammoID,
  error
})

export const ammoTradeFetched = ({ tableType, ammoID, tradeType, amount, user }): ITradeUpdated => ({
  type: TRADE_AMMO_FETCHED,
  tableType,
  tradeType,
  ammoID,
  amount,
  user
})

export const setPreferenceAttempt = ({ tableType, ammoID, activeCatID }): ISetPreference => ({
  type: SET_PREFERENCE_ATTEMPT,
  tableType,
  ammoID,
  activeCatID
})

export const setPreferenceSaved = ({ tableType, ammoID, activeCatID }): ISetPreference => ({
  type: SET_PREFERENCE_FETCHED,
  tableType,
  ammoID,
  activeCatID
})

export const updateUserMoney = (money: number): { money: number } & IType => ({
  type: USER_MONEY_UPDATED,
  money
})
