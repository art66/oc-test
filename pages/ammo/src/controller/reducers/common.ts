import { commonInitialState } from '../../store/initialState'
import locationPath from '../helpers/locationPath'

import { IDebugShow, IInfoShow, IType, ILocationChange, IManualDesktopStatus } from '../../interfaces/IController'
import { ICommon } from '../../interfaces/IStore'

import {
  PATHS_ID,
  SHOW_DEBUG_BOX,
  HIDE_DEBUG_BOX,
  SHOW_INFO_BOX,
  HIDE_INFO_BOX,
  LOCATION_CHANGE,
  MANUAL_DESKTOP_MODE
} from '../../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SHOW_DEBUG_BOX]: (state: ICommon, action: IDebugShow) => ({
    ...state,
    debug: action.msg
  }),
  [HIDE_DEBUG_BOX]: (state: ICommon) => ({
    ...state,
    debug: null
  }),
  [SHOW_INFO_BOX]: (state: ICommon, action: IInfoShow) => ({
    ...state,
    info: action.msg
  }),
  [HIDE_INFO_BOX]: (state: ICommon) => ({
    ...state,
    info: null
  }),
  [LOCATION_CHANGE]: (state: ICommon, action: ILocationChange) => {
    const currentPath = locationPath(action.payload.location.hash)

    return {
      ...state,
      debug: null,
      info: null,
      locationCurrent: currentPath,
      pageID: PATHS_ID[currentPath]
    }
  },
  [MANUAL_DESKTOP_MODE]: (state: ICommon, action: IManualDesktopStatus) => ({
    ...state,
    isManualDesktopMode: action.status
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: ICommon = commonInitialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
