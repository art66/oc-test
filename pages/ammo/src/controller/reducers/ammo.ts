import { ammoInitialState } from '../../store/initialState'

import {
  MAIN_DATA_FETCH_ATTEMPT,
  MAIN_DATA_FETCHED,
  ROW_ACTIVATED,
  ROW_DEACTIVATED,
  TRADE_AMMO_ATTEMPT,
  TRADE_AMMO_FETCHED,
  TRADE_AMMO_CLOSE,
  SET_PREFERENCE_ATTEMPT,
  SET_PREFERENCE_FETCHED,
  TRADE_AMMO_FAILED,
  USER_MONEY_UPDATED
} from '../../constants'
import { IAmmo, IRow } from '../../interfaces/IStore'
import {
  IType,
  IMainDataFetched,
  IActivatedClickedRow,
  IDeactivatedClickedRow,
  ITradeUpdated,
  ITradeClose,
  ISetPreference
} from '../../interfaces/IController'

const ACTION_HANDLERS = {
  [MAIN_DATA_FETCH_ATTEMPT]: (state: IAmmo) => ({
    ...state,
    settings: {
      ...state.settings,
      inProgress: true
    }
  }),
  [MAIN_DATA_FETCHED]: (state: IAmmo, action: IMainDataFetched) => ({
    ...state,
    settings: {
      ...state.settings,
      inProgress: false
    },
    equipped: action.equipped,
    unequipped: action.unequipped,
    user: action.user
  }),
  [ROW_ACTIVATED]: (state: IAmmo, action: IActivatedClickedRow) => ({
    ...state,
    settings: {
      ...state.settings,
      [`${action.tableType}RowActiveID`]: action.ID
    }
  }),
  [ROW_DEACTIVATED]: (state: IAmmo, action: IDeactivatedClickedRow) => ({
    ...state,
    settings: {
      ...state.settings,
      [`${action.tableType}RowActiveID`]: null
    }
  }),
  [TRADE_AMMO_ATTEMPT]: (state: IAmmo, action: ITradeUpdated) => ({
    ...state,
    [action.tableType]: state[action.tableType].map((ammo: IRow) => {
      return ammo.ammoID === action.ammoID
        ? {
            ...ammo,
            trading: {
              ...ammo.trading,
              progress: {
                ...ammo.trading.progress,
                [action.tradeType]: true
              }
            }
          }
        : ammo
    })
  }),
  [TRADE_AMMO_FETCHED]: (state: IAmmo, action: ITradeUpdated) => ({
    ...state,
    user: {
      ...state.user,
      ...action.user
    },
    settings: {
      ...state.settings,
      isTrading: false
    },
    [action.tableType]: state[action.tableType].map((ammo: IRow) => {
      return ammo.ammoID === action.ammoID
        ? {
            ...ammo,
            rowData: {
              ...ammo.rowData,
              standard: action.amount
            },
            trading: {
              ...ammo.trading,
              progress: {
                ...ammo.trading.progress,
                [action.tradeType]: false,
                [`${action.tradeType}TradeDone`]: true
              }
            }
          }
        : ammo
    })
  }),
  [TRADE_AMMO_CLOSE]: (state: IAmmo, action: ITradeClose) => ({
    ...state,
    [action.tableType]: state[action.tableType].map((ammo: IRow) => {
      return ammo.ammoID === action.ammoID
        ? {
            ...ammo,
            trading: {
              ...ammo.trading,
              progress: {
                ...ammo.trading.progress,
                [action.tradeType]: false,
                [`${action.tradeType}TradeDone`]: false,
                [`${action.tradeType}TradeFail`]: false
              }
            }
          }
        : ammo
    })
  }),
  [TRADE_AMMO_FAILED]: (state: IAmmo, action: ITradeClose) => ({
    ...state,
    error: action.error,
    [action.tableType]: state[action.tableType].map((ammo: IRow) => {
      return ammo.ammoID === action.ammoID
        ? {
            ...ammo,
            trading: {
              ...ammo.trading,
              progress: {
                ...ammo.trading.progress,
                [`${action.tradeType}TradeFail`]: true
              }
            }
          }
        : ammo
    })
  }),
  [SET_PREFERENCE_ATTEMPT]: (state: IAmmo, action: ISetPreference) => ({
    ...state,
    [action.tableType]: state[action.tableType].map((ammo: IRow) => {
      return ammo.ammoID === action.ammoID
        ? {
            ...ammo,
            preference: {
              inProgress: true,
              nextActiveSection: action.activeCatID
            }
          }
        : ammo
    })
  }),
  [SET_PREFERENCE_FETCHED]: (state: IAmmo, action: ISetPreference) => ({
    ...state,
    [action.tableType]: state[action.tableType].map((ammo: IRow) => {
      return ammo.ammoID === action.ammoID
        ? {
            ...ammo,
            activeCatID: action.activeCatID,
            preference: {
              inProgress: false,
              nextActiveSection: null
            }
          }
        : ammo
    })
  }),
  [USER_MONEY_UPDATED]: (state: IAmmo, action: { money: number }): IAmmo => ({
    ...state,
    user: {
      ...state.user,
      money: action.money
    }
  })
}

const reducer = (state: IAmmo = ammoInitialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
