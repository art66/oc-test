import { updateUserMoney } from '../actions/ammo'

const initUserMoneyWS = ({ dispatch }) => {
  const storeCallback = ({ money }) => dispatch(updateUserMoney(money))

  // @ts-ignore
  window.WebsocketHandler.addEventListener('sidebar', 'updateMoney', storeCallback)
}

export default initUserMoneyWS
