import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { setPreferenceSaved } from '../actions/ammo'
import { REQUEST_URL_ROOT, TABLES_TYPES } from '../../constants'
import { debugShow } from '../actions/common'

function* setPreferences({ tableType = TABLES_TYPES.equipped, ammoID = 1, activeCatID = 1 }: any) {
  const fetchEndpoint = `${REQUEST_URL_ROOT}&step=setPreferences&ammoID=${ammoID}&catID=${activeCatID}`

  try {
    const { ammoID: ID, catID, error } = yield fetchUrl(fetchEndpoint, null, false)

    if (error) {
      yield put(debugShow(error))
    }

    yield put(setPreferenceSaved({ tableType, ammoID: ID, activeCatID: catID }))
  } catch (e) {
    yield put(debugShow('Oops... Something bad happen during preference set. Ask staff for more info.'))
  }
}

export default setPreferences
