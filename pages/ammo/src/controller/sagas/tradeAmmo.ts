import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { ammoTradeFetched, ammoTradeFailed } from '../actions/ammo'
import { REQUEST_URL_ROOT } from '../../constants'
// import { debugShow } from '../actions/common'

function* tradeAmmo({ tableType, tradeType, ammoID, amount }: any) {
  const fetchEndpoint = `${REQUEST_URL_ROOT}&step=${tradeType}&ammoID=${ammoID}&amount=${amount}`

  try {
    const { ammoID: ID, amount: ammoAmount, error, user } = yield fetchUrl(fetchEndpoint, null, false)

    if (error) {
      // yield put(debugShow(error))
      yield put(ammoTradeFailed({ tableType, ammoID, tradeType, error }))
    }

    yield put(ammoTradeFetched({ tableType, ammoID: ID, amount: ammoAmount, tradeType, user }))
  } catch (e) {
    yield put(ammoTradeFailed({ tableType, ammoID, tradeType, error: e }))
    console.error('Oops... Something bad happen during trading. Ask staff for more info.')
  }
}

export default tradeAmmo
