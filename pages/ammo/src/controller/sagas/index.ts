import { takeLatest } from 'redux-saga/effects'

import fetchMainData from './fetchMainData'
import tradeAmmo from './tradeAmmo'

import { MAIN_DATA_FETCH_ATTEMPT, TRADE_AMMO_ATTEMPT, SET_PREFERENCE_ATTEMPT } from '../../constants'
import setPreferences from './setPreference'

export default function* sagasWatcher() {
  yield takeLatest(MAIN_DATA_FETCH_ATTEMPT, fetchMainData)
  yield takeLatest(TRADE_AMMO_ATTEMPT, tradeAmmo)
  yield takeLatest(SET_PREFERENCE_ATTEMPT, setPreferences)
}
