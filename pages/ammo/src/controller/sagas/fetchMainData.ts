import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { mainDataFetched } from '../actions/ammo'
import { REQUEST_URL_ROOT } from '../../constants'
import { debugShow } from '../actions/common'

function* fetchMainData() {
  try {
    const { ammo, user, error } = yield fetchUrl(REQUEST_URL_ROOT, null, false)

    if (error) {
      yield put(debugShow(error))
    }

    yield put(mainDataFetched({ ...ammo, user }))
  } catch (e) {
    yield put(debugShow('Oops... Something bad happen during main data fetch. Ask staff for more info.'))
  }
}

export default fetchMainData
