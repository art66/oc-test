import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'

import swRegisterer from '@torn/shared/utils/swRegisterer'
// import sw_push_activator from './utils/sw_push_activator'

import rootStore from './store/createStore'
import AppContainer from './core/AppContainer'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    state: any
  }
}

// ========================================================
// ServiceWorkers Setup
// ========================================================
swRegisterer({ sw: __DEV__ ? '/ammo_sw.js' : '/js/serviceWorkers/ammo_sw.js', scope: '/page.php?sid=ammo' })

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('react-root')

let render = () => {
  ReactDOM.render(<AppContainer store={rootStore} />, MOUNT_NODE)
}

// ========================================================
// Developer Tools Setup
// ========================================================
if (__DEV__) {
  // importing real sidebar app bundle only for developing stage
  // sw_push_activator()
  // import('../../sidebar/src/main')
  window.state = rootStore.getState()
}

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderApp = render
    const renderError = (error: any) => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./core/AppContainer', render)
  }
}

// ========================================================
// Go!
// ========================================================
render()
