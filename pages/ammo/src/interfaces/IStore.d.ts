export interface ICommon {
  isManualDesktopMode?: boolean
  locationCurrent?: string
  appID?: string
  pageID?: number
  info?: string
  debug?: string | object
  debugCloseAction?: () => void
}

export interface IBrowser {
  mediaType: string
}

export interface IRowSections {
  ammo: {
    title: string
    titleShort: string
    image: string
  }
  standard: number
  hollowpoint: number
  piercing: number
  tracer: number
  incendiary: number
}

export interface IWeapon {
  ID: number
  ammoType: number
  image: string
  name: string
}

export interface ITrading {
  progress: {
    buy: boolean
    sell: boolean
    buyTradeDone: boolean
    sellTradeDone: boolean
    buyTradeFail: boolean
    sellTradeFail: boolean
  }
  prices: {
    buy: number
    sell: number
  }
  weaponsList: IWeapon[]
}

export interface IRow {
  ammoID: number
  itemName: string
  preference: {
    inProgress: boolean
    nextActiveSection: number
  }
  activeCatID: number
  rowData: IRowSections
  trading: ITrading
}

export interface ISettings {
  inProgress: boolean
  isTrading: boolean
  isPreference: boolean
  equippedRowActiveID: number
  unequippedRowActiveID: number
}

export interface IUser {
  user: {
    money: number
  }
}

export interface IAmmoRowsData extends IUser {
  equipped: IRow[]
  unequipped: IRow[]
}

export interface IAmmo extends IAmmoRowsData, IUser {
  settings: ISettings
  error: string
}

export type TTableTypes = 'equipped' | 'unequipped'
