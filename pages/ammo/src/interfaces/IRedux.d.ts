import { ICommon, IAmmo, IBrowser } from './IStore'

export interface IRedux {
  browser: IBrowser
  common: ICommon
  ammo: IAmmo
}
