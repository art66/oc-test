import { IAmmoRowsData, TTableTypes, IUser } from './IStore'

export type TDebugMessage = string | object

export interface IType {
  type: string
}

export interface IDebugShow extends IType {
  msg: TDebugMessage
}

export interface IInfoShow extends IType {
  msg: TDebugMessage
}

export interface ILocationChange extends IType {
  payload: {
    location: {
      hash: string
    }
  }
}

export interface IMainDataFetched extends IAmmoRowsData, IType {}

export interface IActivatedClickedRow extends IType {
  tableType: TTableTypes
  ID: number
}
export interface IDeactivatedClickedRow extends IType {
  tableType: TTableTypes
}

export interface ITradeAttempt extends IType {
  tableType: TTableTypes
  tradeType: 'buy' | 'sell'
  ammoID: number
  amount: number
}

export interface ITradeUpdated extends IUser, IType {
  tableType: TTableTypes
  tradeType: 'buy' | 'sell'
  ammoID: number
  amount: number
}

export interface ITradeClose extends IType {
  tableType: TTableTypes
  tradeType: 'buy' | 'sell'
  ammoID: number
  error?: string
}

export interface ISetPreference extends IType {
  tableType: TTableTypes
  ammoID: number
  activeCatID: number
}

export interface IManualDesktopStatus extends IType {
  status: boolean
}
