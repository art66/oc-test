import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk'

import reduxLogger from '../controller/middleware/reduxLogger'
import makeRootReducer from './services/rootReducer'
import rootSaga from './services/rootSaga'
import activateStoreHMR from './services/storeHMR'

import { IAsyncReducersStore } from './interfaces'
import initUserMoneyWS from '../controller/websockets/userMoney'

// creating saga middleware for observation
const sagaMiddleware = createSagaMiddleware()

const rootStore = (initialState: object = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [sagaMiddleware, thunk]
  const enhancers = []

  if (__DEV__) {
    middleware.push(reduxLogger)

    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store: any & IAsyncReducersStore = createStore(
    makeRootReducer(),
    initialState,
    compose(
      responsiveStoreEnhancer,
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  sagaMiddleware.run(rootSaga) // starting middleware run
  store.asyncReducers = {} // activating async reducers replacement

  activateStoreHMR(store)
  initUserMoneyWS(store)

  return store
}

export default rootStore()
