import currentLocation from '../utils/getWindowLocation'
import { ICommon, IAmmo } from '../interfaces/IStore'

const commonInitialState: ICommon = {
  isManualDesktopMode: false,
  locationCurrent: currentLocation,
  appID: 'Ammo',
  pageID: 0,
  debug: null,
  info: null
}

const ammoInitialState: IAmmo = {
  user: null,
  error: null,
  settings: {
    inProgress: false,
    isTrading: false,
    isPreference: false,
    equippedRowActiveID: null,
    unequippedRowActiveID: null
  },
  equipped: [
    // {
    //   ammoID: 2,
    //   activeCatID: 3,
    //   preference: {
    //     inProgress: false,
    //     nextActiveSection: null
    //   },
    //   rowData: {
    //     ammo: {
    //       title: '9mm Parabellum Round',
    //       titleShort: '9mm',
    //       image: 'ammo-icon-9-parabellum-round'
    //     },
    //     hollowpoint: 0,
    //     incendiary: 0,
    //     piercing: 0,
    //     standard: 8,
    //     tracer: 0
    //   },
    //   trading: {
    //     progress: {
    //       buy: false,
    //       sell: false,
    //       buyTradeDone: false,
    //       sellTradeDone: false
    //       buyTradeFail: false,
    //       sellTradeFail: false
    //     },
    //     prices: {
    //       buy: 9,
    //       sell: 6.75
    //     },
    //     weaponsList: [
    //       {
    //         ID: 12,
    //         ammoType: 2,
    //         image: '12',
    //         name: 'Glock 17'
    //       }
    //     ]
    //   }
    // }
  ],
  unequipped: []
}

export { commonInitialState, ammoInitialState }
