import { all } from 'redux-saga/effects'

import sagasWatcher from '../../controller/sagas'

export default function* rootSaga() {
  yield all([sagasWatcher()])
}
