import makeRootReducer from './rootReducer'
import { Store } from 'redux'
import { IAsyncReducersStore } from '../interfaces'

const activateStoreHMR = (store: Store<any> & IAsyncReducersStore) => {
  // Hot Module Replacement for Controller/Store
  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }
}

export default activateStoreHMR
