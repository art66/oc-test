const swPushActivator = () => {
  window.addEventListener('load', () => {
    Notification.requestPermission(permission => {
      // If the user accepts, let's create a notification
      if (permission === 'granted') {
        return new Notification('Hi there!')
      }

      console.log(permission, 'permission')

      return null
    })
  })
}

export default swPushActivator
