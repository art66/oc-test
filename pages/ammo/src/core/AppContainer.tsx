import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'

import AppLayout from '../layout/AppLayout'

import { IProps } from './interfaces'

// Our main container for app handling.
// We're using react-router-dom and connected-react-router package for SPA react-redux routing
class AppContainer extends Component<IProps> {
  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <AppLayout />
      </Provider>
    )
  }
}

export default AppContainer
