// --------------------------
// REDUX NAMESPACES
// --------------------------
export const SHOW_DEBUG_BOX = 'SHOW_DEBUG_BOX'
export const HIDE_DEBUG_BOX = 'HIDE_DEBUG_BOX'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_INFO_BOX = 'HIDE_INFO_BOX'
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE'
export const MEDIA_SCREEN_CHANGED = 'redux-responsive/CALCULATE_RESPONSIVE_STATE'
export const ROUTE_HEADER_SET = 'ROUTE_HEADER_SET'
export const MAIN_DATA_FETCH_ATTEMPT = 'MAIN_DATA_FETCH_ATTEMPT'
export const MAIN_DATA_FETCHED = 'MAIN_DATA_FETCHED'
export const ROW_ACTIVATED = 'ROW_ACTIVATED'
export const ROW_DEACTIVATED = 'ROW_DEACTIVATED'
export const TRADE_AMMO_ATTEMPT = 'TRADE_AMMO_ATTEMPT'
export const TRADE_AMMO_FETCHED = 'TRADE_AMMO_FETCHED'
export const SET_PREFERENCE_ATTEMPT = 'SET_PREFERENCE_ATTEMPT'
export const SET_PREFERENCE_FETCHED = 'SET_PREFERENCE_FETCHED'
export const MANUAL_DESKTOP_MODE = 'MANUAL_DESKTOP_MODE'
export const TRADE_AMMO_CLOSE = 'TRADE_AMMO_CLOSE'
export const TRADE_AMMO_FAILED = 'TRADE_AMMO_FAILED'
export const USER_MONEY_UPDATED = 'USER_MONEY_UPDATED'

// --------------------------
// APP HEADER
// --------------------------
export const PATHS_ID = {
  '': 1
}

export const APP_HEADER = {
  active: true,
  titles: {
    default: {
      ID: 0,
      title: 'Ammo Locker'
    }
  },
  links: {
    default: {
      ID: 0,
      items: [
        {
          title: 'Your items',
          href: '/item.php',
          label: 'folder',
          icon: 'Items'
        }
      ]
    }
  },
  tutorials: {
    active: true,
    showTutorial: true,
    activateOnLoad: true,
    hideByDefault: false,
    default: {
      ID: 0,
      items: [
        {
          title: 'Ammo Tutorial',
          text: `A firearm is pretty useless without ammo.
            Buy ammunition here and you will automatically load it in to your guns before each fight.
            To use special ammunition, you can click it below to set priority.
            It will then always be used while attacking and defending until you run out,
            or priority is removed by clicking it again.`
        }
      ]
    }
  }
}

// --------------------------
// APP TABLE
// --------------------------
export const COLUMNS_COUNT = 6
export const COLUMN_TITLES = [
  {
    ID: 1,
    titles: {
      desktop: 'Ammo'
    },
    tooltip: ''
  },
  {
    ID: 0,
    titles: {
      desktop: 'Buy'
    },
    tooltip: ''
  },
  {
    ID: 2,
    titles: {
      desktop: 'Hollow Point',
      touchscreen: 'Hollow'
    },
    tooltip: 'Increases damage by 50% but decreases penetration by 50%'
  },
  {
    ID: 4,
    titles: {
      desktop: 'Tracer'
    },
    tooltip: 'Increases accuracy of shots by +10'
  },
  {
    ID: 3,
    titles: {
      desktop: 'Piercing'
    },
    tooltip: 'Increases penetration by 50%'
  },
  {
    ID: 5,
    titles: {
      desktop: 'Incendiary'
    },
    tooltip: 'Increases damage by 40%'
  }
]
export const TABLES_TYPES: { equipped: 'equipped'; unequipped: 'unequipped' } = {
  equipped: 'equipped',
  unequipped: 'unequipped'
}

// --------------------------
// FETCH API ENDPOINT
// --------------------------
export const REQUEST_URL_ROOT = '/page.php?sid=ammoPoolData'

// --------------------------
// KEY_CODES_HANDLERS
// --------------------------
export const ESCAPE_KEY_ID = 27
export const ENTER_KEY_ID = 13

// --------------------------
// TRADING_LIMITS
// --------------------------
export const BUY_LIMIT = 99999999
