const AMMO_WEAPONS_LIST = [
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
  24,
  25,
  26,
  27,
  28,
  29,
  30,
  31,
  63,
  76,
  98,
  99,
  100,
  108,
  109,
  174,
  177,
  189,
  218,
  219,
  223,
  225,
  228,
  230,
  231,
  232,
  233,
  240,
  241,
  243,
  244,
  248,
  249,
  252,
  253,
  254,
  255,
  382,
  388,
  393,
  398,
  399,
  483,
  484,
  485,
  486,
  487,
  488,
  489,
  490,
  545,
  546,
  547,
  548,
  549,
  612,
  613,
  830,
  831,
  837,
  838,
  844,
  874
]

const deleteOldCache = (caches, cache) => Promise.all(cache.map(cacheName => caches.delete(cacheName)))
const imagesListToLoad = () => AMMO_WEAPONS_LIST.map(imageID => `/images/items/${imageID}/large.png`)

self.addEventListener('install', event => {
  self.skipWaiting()

  event.waitUntil(caches.keys().then(cacheNames => deleteOldCache(caches, cacheNames)))
})

self.addEventListener('activate', event => {
  event.waitUntil(caches.open('ammo-images').then(cache => cache.addAll(imagesListToLoad())))
})

self.addEventListener('fetch', event => {
  const { request = {} } = event

  // some strange exceptions...
  const googleAllow = /(analytics)/.test(event.request.url)
  const bingAllow = /(bing)/.test(event.request.url)
  const doubleclickAllow = /(doubleclick)/.test(event.request.url)

  const isNotAnAmmoImage = request.destination !== 'image' || !/(images\/items)/i.test(request.url)

  if (isNotAnAmmoImage || googleAllow || bingAllow || doubleclickAllow) {
    return
  }

  // load real data if cache is missed
  const loadRealImagesData = () => {
    return fetch(event.request).then(response => {
      return caches.open('ammo-images').then(cache => {
        cache.put(event.request, response.clone())

        return response
      })
    })
  }

  event.respondWith(
    caches.match(event.request).then(resp => {
      return resp || loadRealImagesData()
    })
  )
})

// self.addEventListener('push', event => {
//   if (Notification.permission === 'denied' || Notification.permission === 'default') {
//     console.error(Notification.permission, Notification.requestPermission, "Permission wasn't granted. Allow a retry.")
//     return
//   }

//   console.log('SW push permission request is granted! Trying to send some push notify...')

//   try {
//     event.waitUntil(
//       self.registration.showNotification((event && event.data && event.data.text()) || 'Some Notification Here!')
//     )
//   } catch (e) {
//     throw new Error(`Error in SW: ${e}`)
//   }
// })

self.addEventListener('sync', event => {
  console.log(event.tag, 'Sync is completed!!!')
})
