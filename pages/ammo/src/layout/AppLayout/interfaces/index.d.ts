import { IAmmo, ICommon, IRow, ISettings } from '../../../interfaces/IStore'

export interface IProps extends ICommon {
  checkManualMode: (status: boolean) => void
  startMainDataFetch: () => void
  equipped: IRow[]
  unequipped: IRow[]
  equippedRowActiveID: number
  unequippedRowActiveID: number
}

export interface IContainerStore {
  common: ICommon
  ammo: IAmmo
  settings: ISettings
}
