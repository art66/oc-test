import React from 'react'
import { Store } from 'redux'
import { connect } from 'react-redux'

import AppHeader from '@torn/shared/components/AppHeader'
import DebugBox from '@torn/shared/components/DebugBox'
import InfoBox from '@torn/shared/components/InfoBox'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'

import Tooltips from '../../components/Tooltips'
import Table from '../../containers/Table'
import { debugHide, checkManualDesktopMode } from '../../controller/actions/common'
import { APP_HEADER, TABLES_TYPES } from '../../constants'
import { IProps, IContainerStore } from './interfaces'

import styles from './index.cssmodule.scss'
import '../../styles/global.cssmodule.scss'

import { mainDataFetchStart } from '../../controller/actions/ammo'

class AppLayout extends React.Component<IProps> {
  componentDidMount() {
    const { startMainDataFetch } = this.props

    startMainDataFetch()
    this._checkManualDesktopMode()
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  _checkManualDesktopMode = () => {
    const { checkManualMode } = this.props

    subscribeOnDesktopLayout(payload => checkManualMode(payload))
  }

  _renderHeader = () => {
    const { appID, pageID = 0 } = this.props

    return <AppHeader appID={appID} pageID={pageID} clientProps={APP_HEADER} />
  }

  _renderInfoDebugAreas = () => {
    const { debug, debugCloseAction, info } = this.props

    return (
      <React.Fragment>
        <Tooltips />
        {debug && <DebugBox debugMessage={debug} close={debugCloseAction} isBeatifyError={true} />}
        {info && <InfoBox msg={info} />}
      </React.Fragment>
    )
  }

  _renderBody = () => {
    const { equipped, unequipped, equippedRowActiveID, unequippedRowActiveID } = this.props

    return (
      <div className={styles.bodyWrap}>
        <Table
          title='Ammunition for your equipped weapons'
          data={equipped}
          activeRowID={equippedRowActiveID}
          tableType={TABLES_TYPES.equipped}
        />
        <hr className={styles.delimiter} />
        <Table
          title='All other ammunition'
          data={unequipped}
          activeRowID={unequippedRowActiveID}
          tableType={TABLES_TYPES.unequipped}
        />
      </div>
    )
  }

  render() {
    return (
      <div className={styles.appWrap}>
        {this._renderHeader()}
        {this._renderInfoDebugAreas()}
        {this._renderBody()}
      </div>
    )
  }
}

const mapStateToProps = (state: IContainerStore & Store<any>) => ({
  appID: state.common.appID,
  pageID: state.common.pageID,
  debug: state.common.debug,
  info: state.common.info,
  equipped: state.ammo.equipped,
  unequipped: state.ammo.unequipped,
  equippedRowActiveID: state.ammo.settings.equippedRowActiveID,
  unequippedRowActiveID: state.ammo.settings.unequippedRowActiveID
})

const mapDispatchToState = dispatch => ({
  debugCloseAction: () => dispatch(debugHide()),
  startMainDataFetch: () => dispatch(mainDataFetchStart()),
  checkManualMode: status => dispatch(checkManualDesktopMode({ status }))
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(AppLayout)
