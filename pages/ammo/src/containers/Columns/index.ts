import { connect } from 'react-redux'

import Columns from '../../components/Columns'

import { IRedux } from '../../interfaces/IRedux'

const mapStateToProps = ({ common, browser }: IRedux) => ({
  isManualDesktopMode: common.isManualDesktopMode,
  mediaType: browser.mediaType
})

export default connect(
  mapStateToProps,
  null
)(Columns)
