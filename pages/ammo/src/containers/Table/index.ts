import { connect } from 'react-redux'
import { Dispatch } from 'redux'

import Table from '../../components/Table'

import { deactivateRow } from '../../controller/actions/ammo'

import { IRedux } from '../../interfaces/IRedux'

const mapStateToProps = ({ common, browser }: IRedux) => ({
  mediaType: browser.mediaType,
  isManualDesktopMode: common.isManualDesktopMode
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  deactivateRow: ({ tableType }) => dispatch(deactivateRow({ tableType }))
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(Table)
