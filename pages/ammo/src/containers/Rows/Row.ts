import { connect } from 'react-redux'
import { Dispatch } from 'redux'

import Row from '../../components/Rows/Row'

import { activateRow, deactivateRow } from '../../controller/actions/ammo'
import { IRedux } from '../../interfaces/IRedux'

const mapStateToProps = ({ common, browser }: IRedux) => ({
  isManualDesktopMode: common.isManualDesktopMode,
  mediaType: browser.mediaType
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  setCurrentActiveRow: ({ tableType, ID }) => dispatch(activateRow({ tableType, ID })),
  deactivateCurrentRow: ({ tableType }) => dispatch(deactivateRow({ tableType }))
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(Row)
