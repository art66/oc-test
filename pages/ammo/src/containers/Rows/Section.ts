import { connect } from 'react-redux'
import { Dispatch } from 'redux'

import { IRedux } from '../../interfaces/IRedux'

import Section from '../../components/Rows/Sections/SectionFactory'

import { setPreferenceAttempt } from '../../controller/actions/ammo'

const mapStateToProps = ({ common, browser }: IRedux) => ({
  isDesktop: common.isManualDesktopMode || browser.mediaType === 'desktop'
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  setPreference: ({ tableType, ammoID, catID }) =>
    dispatch(setPreferenceAttempt({ tableType, ammoID, activeCatID: catID }))
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(Section)
