import { connect } from 'react-redux'

import Rows from '../../components/Rows'
import { IRedux } from '../../interfaces/IRedux'

const mapStateToProps = ({ ammo }: IRedux) => ({
  inProgress: ammo.settings.inProgress
})

export default connect(
  mapStateToProps,
  null
)(Rows)
