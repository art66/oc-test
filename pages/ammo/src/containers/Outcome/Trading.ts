import { connect } from 'react-redux'
import { Dispatch } from 'redux'

import Trading from '../../components/Outcome/Trading'

import { ammoTradeAttempt, ammoTradeClose } from '../../controller/actions/ammo'

import { IRedux } from '../../interfaces/IRedux'

const mapStateToProps = ({ common, ammo, browser }: IRedux) => ({
  userMoney: ammo.user.money,
  isManualDesktopMode: common.isManualDesktopMode,
  mediaType: browser.mediaType,
  isDebug: !!common.debug,
  errorMsg: ammo.error
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  ammoTradeClose: ({ tableType, tradeType, ammoID }) => dispatch(ammoTradeClose({ tableType, tradeType, ammoID })),
  ammoTradeAttempt: ({ tableType, tradeType, ammoID, amount }) =>
    dispatch(ammoTradeAttempt({ tableType, tradeType, ammoID, amount }))
})

export default connect(mapStateToProps, mapDispatchToState)(Trading)
