import { connect } from 'react-redux'

import Weapons from '../../components/Outcome/Weapons'

import { IRedux } from '../../interfaces/IRedux'

const mapStateToProps = ({ common, browser }: IRedux) => ({
  mediaType: common.isManualDesktopMode ? 'desktop' : browser.mediaType
})

export default connect(
  mapStateToProps,
  null
)(Weapons)
