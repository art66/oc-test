# Ammo - Torn


## 2.3.3
 * Improved inputs logic.

## 2.3.2
 * Fixed clickable section once they empty but active.
 * Fixed skeleton rows height.

## 2.3.1
 * Fixed input value.
 * Added error handling in case error message is received.

## 2.3.0
 * Integrated DealConfirm component inside the app.

## 2.2.1
 * Fixed buy button color while its have 0 points.

## 2.2.0
 * Updated test suits according to the brand new mock layout.

## 2.1.0
 * Improved input value handling.
 * Fixed trading dialog appear on tablet/mobile devices.

## 2.1.0
 * Added string checking ability on bad symbols input.
 * Added warning tooltips once inputted bad symbols.

## 2.0.1
 * Added money input shorter ability.

## 2.0.0
 * Added adaptive layout.
 * App is ready to release.

## 1.6.0
 * Added trading confirmation dialog section.
 * Added Controller logic of show/hide dialog section.
 * Added events listeners for keyDown to handle show/close dialog section and a whole open row by keyboard's clicks as well.

## 1.5.0
 * Ammo Controller and View logics were bind together.

## 1.4.0
 * Created first mainData load Controller logic.

## 1.3.0
 * Added first good Skeletons layout.

## 1.2.0
 * Added first good Ammo layout.

## 1.1.0
 * Added new AppHeader configs.

## 1.0.0
 * Initial commit release.
