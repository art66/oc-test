import { formatTimeNumber } from '@torn/shared/utils'

export default function formatDateTime(date: Date): string {
  const year = date.getFullYear()
  const month = formatTimeNumber(date.getMonth() + 1)
  const day = formatTimeNumber(date.getDate())
  const hours = formatTimeNumber(date.getHours())
  const minutes = formatTimeNumber(date.getMinutes())
  const seconds = formatTimeNumber(date.getSeconds())

  return `${year}/${month}/${day} ${hours}:${minutes}:${seconds}`
}