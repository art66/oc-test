import { createAction } from 'redux-actions'
import * as a from './actionTypes'

export const loading = createAction(a.LOADING, (value: boolean) => ({ loading: value }))
export const setAppState = createAction(a.SET_APP_STATE, (payload: object) => payload)
export const fetchPageData = createAction(a.FETCH_PAGE_DATA)
export const setReopenConfirmation = createAction(a.SET_REOPEN_CONFIRMATION, (show: boolean) => show)
export const reopenAccount = createAction(a.REOPEN_ACCOUNT)
export const setDeleteConfirmation = createAction(a.SET_DELETE_CONFIRMATION, (show: boolean, step: number) => ({
  show,
  step
}))
export const deleteAccount = createAction(a.DELETE_ACCOUNT)
export const setAccountDeleted = createAction(a.SET_ACCOUNT_DELETED, (recoveryKey: string) => ({ recoveryKey }))
export const updateTimeToWait = createAction(a.UPDATE_TIME_TO_WAIT)
