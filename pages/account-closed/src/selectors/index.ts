export const getFilters = state => state.campaigns.filters
export const getSelectedCampaign = state => state.campaigns.list.find(c => c.campaign === state.campaigns.selected)
