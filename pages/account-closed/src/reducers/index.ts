import { handleActions } from 'redux-actions'
import * as a from '../actions/actionTypes'

interface IState {
  loading: boolean
  timeToWait: number
  timeNow: number
  closedTime?: number
  reopenConfirmation: boolean
  deleteConfirmation: boolean
  secondDeleteConfirmation: boolean
}

interface IAction<TPayload = any> {
  type: string
  payload: TPayload
}

const initialState = {
  loading: false,
  initialLoadCompleted: false,
  timeToWait: 0,
  timeNow: Date.now() / 1000,
  closedTime: Date.now() / 1000,
  reopenConfirmation: false,
  deleteConfirmation: false,
  secondDeleteConfirmation: false,
  accountDeleted: false,
  recoveryKey: null
}

export default handleActions(
  {
    [a.LOADING]: (state: IState, action: IAction) => ({
      ...state,
      loading: action.payload.loading
    }),
    [a.SET_APP_STATE]: (state: IState, action: IAction) => ({
      ...state,
      ...action.payload
    }),
    [a.SET_REOPEN_CONFIRMATION]: (state: IState, action: IAction) => ({
      ...state,
      reopenConfirmation: action.payload
    }),
    [a.SET_DELETE_CONFIRMATION]: (state: IState, action: IAction<{ show: boolean; step: number }>) => ({
      ...state,
      deleteConfirmation: action.payload.step === 1 && action.payload.show,
      secondDeleteConfirmation: action.payload.step === 2 && action.payload.show
    }),
    [a.SET_ACCOUNT_DELETED]: (state: IState, action: IAction) => ({
      ...state,
      accountDeleted: true,
      recoveryKey: action.payload.recoveryKey
    }),
    [a.UPDATE_TIME_TO_WAIT]: (state: IState) => ({
      ...state,
      timeToWait: state.timeToWait - 1
    })
  },
  initialState
)
