import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import {
  fetchPageData,
  setReopenConfirmation,
  reopenAccount,
  setDeleteConfirmation,
  deleteAccount
} from '../actions'
import AppHeader from '@torn/shared/components/AppHeader'
import s from './styles.cssmodule.scss'
import Button from '@torn/shared/components/Button'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import AccountDeleted from './AccountDeleted'
import formatDateTime from '../utils/formatDateTime'

interface IProps {
  loading: boolean
  initialLoadCompleted: boolean
  timeToWait: number
  timeNow: number
  closedTime: number
  reopenConfirmation: boolean
  deleteConfirmation: boolean
  secondDeleteConfirmation: boolean
  accountDeleted: boolean
  logoutHash?: string
  fetchPageData(): void
  setReopenConfirmation(show: boolean): void
  reopenAccount(): void
  setDeleteConfirmation(show: boolean, step: number): void
  deleteAccount(): void
}

class AccountClosed extends Component<IProps> {
  componentDidMount() {
    this.props.fetchPageData()
    location.hash = ''
  }

  getActionsBlock = () => {
    const { reopenConfirmation, deleteConfirmation, secondDeleteConfirmation, initialLoadCompleted, logoutHash } = this.props
    const canDelete = this.props.timeToWait <= 0
    let result

    if (reopenConfirmation) {
      result = (
        <div>
          <span className={s.confirmationText}>Are you sure you want to reopen this account?</span>
          <span role='button' className={s.link} onClick={this.props.reopenAccount}>Yes</span>
          <span role='button' className={s.link} onClick={() => this.props.setReopenConfirmation(false)}>No</span>
        </div>
      )
    } else if (deleteConfirmation) {
      result = (
        <div>
          <span className={s.confirmationText}>
            Are you sure you want to delete this account? Upon deletion, your personal data will be wiped and the
            account will be <span className={s.highlightedText}>fully inaccessible</span>.
          </span>
          <span role='button' className={s.link} onClick={() => this.props.setDeleteConfirmation(true, 2)}>Yes</span>
          <span role='button' className={s.link} onClick={() => this.props.setDeleteConfirmation(false, 1)}>No</span>
        </div>
      )
    } else if (secondDeleteConfirmation) {
      result = (
        <div>
          <Button className={s.button} color='orange' onClick={this.props.deleteAccount}>DELETE ACCOUNT</Button>
          <Button className={s.button} onClick={() => this.props.setDeleteConfirmation(false, 2)}>CANCEL</Button>
        </div>
      )
    } else {
      result = (
        <div>
          <Button
            className={s.button}
            disabled={!initialLoadCompleted}
            onClick={() => this.props.setReopenConfirmation(true)}
          >
            REOPEN ACCOUNT
          </Button>
          <Button
            className={s.button}
            disabled={!canDelete || !initialLoadCompleted}
            color='orange'
            onClick={() => this.props.setDeleteConfirmation(true, 1)}
          >
            DELETE ACCOUNT
          </Button>
          <Button
            className={s.button}
            onClick={() => location.href = `/logout.php?hash=${logoutHash}`}
          >
            LOGOUT
          </Button>
        </div>
      )
    }

    return result
  }

  getAccountClosedView() {
    const { timeToWait, secondDeleteConfirmation, initialLoadCompleted, closedTime } = this.props
    const canDelete = timeToWait <= 0
    const closedDate: string = formatDateTime(new Date(closedTime * 1000))
    let youCanDeleteMessage

    if (canDelete) {
      youCanDeleteMessage = 'You can delete this account if you wish.'
    } else {
      let days, hours, minutes, seconds = timeToWait
      days = Math.floor(seconds / (3600 * 24))
      seconds  -= days * 3600 * 24
      hours   = Math.floor(seconds / 3600)
      seconds  -= hours * 3600
      minutes = Math.floor(seconds / 60)
      const time = `
        ${days && (days + ' days, ') || ''}
        ${hours && (hours + ' hours and ') || ''}
        ${minutes && (minutes + ' minutes.') || ''}
      `
      youCanDeleteMessage = `You can delete this account in <span class='bold'>${time}</span>`
    }

    const actionsBlock = this.getActionsBlock()
    let content

    if (secondDeleteConfirmation) {
      content = (
        <div>

          <p>
            Upon confirmation, you will be logged out and will
            <span className={s.highlightedText}> not be able to login again</span>.
          </p>
          <p>
            You will receive a confirmation email of this action. The following data will be deleted from the account
          </p>
          <ul className={s.list}>
            <li>- Email address</li>
            <li>- Encrypted password</li>
            <li>- IP Addresses</li>
            <li>- Profile details</li>
            <li>- Profile images</li>
            <li>- Profile signature</li>
            <li>- Forum signature</li>
            <li>- Bazaar description</li>
            <li>- Security information</li>
          </ul>
        </div>
      )
    } else {
      content = (
        <div>
          <p dangerouslySetInnerHTML={{ __html: youCanDeleteMessage }} />
          <p>
            By deleting the account,
            <span className={cn(s.importantText, { [s.highlightedText]: canDelete })}>
              all personal information will be stripped from it and it will become inaccessible.
            </span>
          </p>
          <p>
            If you wish to start a new account, you must wait the full duration and delete this account first.
          </p>
          <p>
            You may not transfer assets from this account to another should you choose to restart.
          </p>
          <p>
            You must ensure you only have access to a single undeleted account at a time.
          </p>
        </div>
      )
    }

    return (
      <div className={s.mobileTopOffset}>
        <AppHeader appID='account-closure' clientProps={{ titles: { default: { title: 'Account Closed', ID: 0 } } }} />
        <div className={s.title}>This account was closed on {closedDate}</div>
        <div className={s.content}>
          {initialLoadCompleted ? content : <LoadingIndicator />}
          <div className={s.divider} />
          {actionsBlock}
        </div>
      </div>
    )
  }

  render() {
    const { accountDeleted } = this.props

    if (accountDeleted) {
      return <AccountDeleted />
    }

    return this.getAccountClosedView()
  }
}

export default connect(
  (state: any) => ({
    loading: state.loading,
    initialLoadCompleted: state.initialLoadCompleted,
    timeToWait: state.timeToWait,
    timeNow: state.timeNow,
    closedTime: state.closedTime,
    reopenConfirmation: state.reopenConfirmation,
    deleteConfirmation: state.deleteConfirmation,
    secondDeleteConfirmation: state.secondDeleteConfirmation,
    accountDeleted: state.accountDeleted,
    logoutHash: state.logoutHash
  }),
  {
    fetchPageData,
    setReopenConfirmation,
    reopenAccount,
    setDeleteConfirmation,
    deleteAccount
  }
)(AccountClosed)
