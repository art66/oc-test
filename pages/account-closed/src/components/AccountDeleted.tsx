import React from 'react'
import { connect } from 'react-redux'
import AppHeader from '@torn/shared/components/AppHeader'
import s from './styles.cssmodule.scss'

const AccountDeleted = ({ playerName, userId, recoveryKey }) => (
  <div className={s.mobileTopOffset}>
    <AppHeader appID='account-closure' clientProps={{ titles: { default: { title: 'Account Deleted', ID: 0 } } }} />
    <div className={s.title}>Recovery Key</div>
    <div className={s.content}>
      <p>
        The account deletion process has now been completed for
        <span className={s.bold}>{playerName} [{userId}]</span>.
      </p>
      <p>
        We've just emailed you with a
        confirmation, and then deleted your personal data from the account.
      </p>
      <p className='m-top20'>
        Should you wish to regain access in the future, we're providing you with a
        <span className={s.bold}>unique recovery key</span> which should be
        kept safe.
      </p>
      <p>
        This is now the only thing linking you to the sanitized account.
      </p>
      <p className={s.recoveryKey}>
        {recoveryKey}
      </p>
      <p>
        You can now create a new account if you wish.
      </p>
      <p>
        Please be sure you only ever have access to a single
        undeleted account or you will be in breach of our rules.
      </p>
      <p className='m-top20'>
        Thank you.
      </p>
    </div>
  </div>
)

export default connect(
  (state: any) => {
    const { playerName, userId, recoveryKey } = state

    return { playerName, userId, recoveryKey }
  },
  {}
)(AccountDeleted)
