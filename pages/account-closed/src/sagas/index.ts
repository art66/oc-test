import { delay } from 'redux-saga'
import { call, put, takeLatest } from 'redux-saga/effects'
import fetchUrl from '../utils/fetchUrl'
import { loading, setAppState, setAccountDeleted, updateTimeToWait } from '../actions/'
import * as actions from '../actions/actionTypes'

interface IServerResponse {
  success: boolean
  payload?: any
  error?: string
}

function showError(error: string) {
  alert(`Error: ${error}`)
}

function* fetchPageData() {
  try {
    yield put(loading(true))
    const data: IServerResponse = yield fetchUrl('/account_closure.php?step=getAccountClosedPageData')
    yield put(loading(false))
    if (data && data.success) {
      yield put(setAppState({...data.payload, initialLoadCompleted: true}))
    } else {
      showError(data.error)
    }
  } catch (error) {
    showError(error)
  }
}

function* reopenAccount() {
  try {
    yield put(loading(true))
    const data: IServerResponse = yield fetchUrl('/account_closure.php?step=reopenAccount')
    yield put(loading(false))

    if (data && data.success) {
      location.reload()
    } else {
      showError(data.error)
    }
  } catch (error) {
    showError(error)
  }
}

function* deleteAccount() {
  try {
    yield put(loading(true))
    const data: IServerResponse = yield fetchUrl('/account_closure.php?step=deleteAccount')
    yield put(loading(false))

    if (data && data.success) {
      yield put(setAccountDeleted(data.payload.recoveryKey))
    } else {
      showError(data.error)
    }
  } catch (error) {
    showError(error)
  }
}

function* timer() {
  while (true) {
    yield call(delay, 1000)
    yield put(updateTimeToWait())
  }
}

export default function* index() {
  yield takeLatest(actions.FETCH_PAGE_DATA, fetchPageData)
  yield takeLatest(actions.REOPEN_ACCOUNT, reopenAccount)
  yield takeLatest(actions.DELETE_ACCOUNT, deleteAccount)
  yield timer()
}
