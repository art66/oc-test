import {
  SHOW_DEBUG_BOX,
  HIDE_DEBUG_BOX,
  SHOW_INFO_BOX,
  HIDE_INFO_BOX,
  ROUTE_HEADER_SET,
  CHECK_MANUAL_DESKTOP_MODE,
  GET_ROUTE_HEADER_SET
} from '../../constants'

export const debugShow = error => ({
  msg: error,
  type: SHOW_DEBUG_BOX
})

export const debugHide = () => ({
  type: HIDE_DEBUG_BOX
})

export const infoShow = info => ({
  msg: info,
  type: SHOW_INFO_BOX
})

export const infoHide = () => ({
  type: HIDE_INFO_BOX
})

export const setRouteHeader = headerData => ({
  headerData,
  type: ROUTE_HEADER_SET
})

export const getRouteHeader = () => ({
  type: GET_ROUTE_HEADER_SET
})

export const checkManualDesktop = status => ({
  status,
  type: CHECK_MANUAL_DESKTOP_MODE
})
