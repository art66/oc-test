import initialState from '../../store/initialState'
import { IInitialState } from '../../store/interfaces'
import locationPath from '../helpers/locationPath'
import unsupportedDevice from '../../utils/unsupportedDevice'
import isPageWithoutAdaptiveMode from '../../utils/isPageWithoutAdaptiveMode'

import {
  TYPE_IDS,
  SHOW_DEBUG_BOX,
  HIDE_DEBUG_BOX,
  SHOW_INFO_BOX,
  HIDE_INFO_BOX,
  LOCATION_CHANGE,
  MEDIA_SCREEN_CHANGED,
  UNSUPPORTED_DEVICE_MESSAGE,
  ROUTE_HEADER_SET,
  CHECK_MANUAL_DESKTOP_MODE
} from '../../constants'
import {SET_ICS_INIT_PARAMS, TOGGLE_ICS} from '../../../../christmastown_editor/src/store/actionTypes'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [TOGGLE_ICS]: (state, action) => {
    return {
      ...state,
      ics: {
        ...state.ics,
        show: action.payload.show
      }
    }
  },
  [SET_ICS_INIT_PARAMS]: (state, action) => {
    return {
      ...state,
      ics: {
        ...state.ics,
        initParams: action.payload.initParams
      }
    }
  },
  [CHECK_MANUAL_DESKTOP_MODE]: (state: IInitialState, action: any) => ({
    ...state,
    isDesktopLayoutSet: action.status
  }),
  [SHOW_DEBUG_BOX]: (state: IInitialState, action: any) => ({
    ...state,
    debug: action.msg
  }),
  [HIDE_DEBUG_BOX]: (state: IInitialState) => ({
    ...state,
    debug: null
  }),
  [SHOW_INFO_BOX]: (state: IInitialState, action: any) => ({
    ...state,
    info: action.msg
  }),
  [HIDE_INFO_BOX]: (state: IInitialState) => ({
    ...state,
    info: null
  }),
  [LOCATION_CHANGE]: (state: IInitialState, action: any) => {
    const currentPath = locationPath(action.payload.location.hash)

    return {
      ...state,
      debug: null,
      info: null,
      locationCurrent: currentPath,
      pageID: TYPE_IDS[currentPath]
    }
  },
  [MEDIA_SCREEN_CHANGED]: (state: IInitialState, action: any) => {
    if (isPageWithoutAdaptiveMode() && unsupportedDevice({ deviceWidth: action.innerWidth })) {
      return {
        ...state,
        info: UNSUPPORTED_DEVICE_MESSAGE
      }
    }

    return {
      ...state,
      info: isPageWithoutAdaptiveMode() ? null : state.info
    }
  },
  [ROUTE_HEADER_SET]: (state: IInitialState, action: any) => {
    return {
      ...state,
      appHeader: action.headerData
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
