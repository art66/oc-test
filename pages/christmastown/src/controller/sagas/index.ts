import { put, takeEvery } from 'redux-saga/effects'
import { setRouteHeader, debugShow } from '../actions'
import { fetchUrl, getErrorMessage } from '../../routes/ChristmasTown/utils/index'
import { GET_ROUTE_HEADER_SET } from '../../constants'

function* getTopPageLinks() {
  try {
    const data = yield fetchUrl('getTopPageLinks', { page: window.location.hash.slice(1) })

    yield put(setRouteHeader(data))
  } catch (e) {
    yield put(debugShow(getErrorMessage(e)))
  }
}

export default function* controllerSaga() {
  yield takeEvery(GET_ROUTE_HEADER_SET, getTopPageLinks)
}
