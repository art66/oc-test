import React, { Component } from 'react'
import { HashRouter, Switch, Route } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'

import AppLayout from '../layout/AppLayout'
import { ChristmasTown, MyMaps, AllMaps, AllChats } from '../routes'
import { MapEditorRoute, ParameterEditorRoute, NpcEditorRoute } from '../../../christmastown_editor/src/routes'
import { IProps } from './interfaces'

class AppContainer extends Component<IProps> {
  render() {
    const { store, history } = this.props

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <HashRouter>
            <AppLayout>
              <Switch>
                <Route exact={true} path='/' component={ChristmasTown} />
                <Route path='/mymaps' component={MyMaps} />
                <Route path='/mapeditor' render={() => <MapEditorRoute isSPA={true} />} />
                <Route path='/parametereditor' render={() => <ParameterEditorRoute isSPA={true} />} />
                <Route path='/npceditor' render={() => <NpcEditorRoute isSPA={true} />} />
                <Route path='/allmaps' component={AllMaps} />
                <Route path='/allchats' component={AllChats} />
              </Switch>
            </AppLayout>
          </HashRouter>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default AppContainer
