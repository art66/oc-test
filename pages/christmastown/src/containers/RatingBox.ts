import { connect } from 'react-redux'

import RatingBox from '../components/RatingBox'

const mapStateToProps = state => {
  return {
    isDesktopLayoutSet: state.common.isDesktopLayoutSet
  }
}

export default connect(
  mapStateToProps,
  null
)(RatingBox)
