import { connect } from 'react-redux'
import { fetchMapData, loadGame, move, quitCellEvent, setUserPosition } from '../modules'
import { collectGarbage, checkHudTabsSync } from '../actions'

import ChristmasTown from '../components/ChristmasTown'

const mapDispatchToProps = {
  fetchMapData,
  loadGame,
  move,
  quitCellEvent,
  setUserPosition,
  collectGarbage,
  checkHudTabsSync
}

const mapStateToProps = (state) => {
  const { item, cellEvent } = state.christmastown.statusArea

  return {
    userStatus: state.christmastown.userData.userStatus,
    item: item && item.item_id,
    cellEventType: cellEvent && cellEvent.type,
    infoMessage: state.christmastown.userData.infoMessage,
    christmasTownDataFetched: state.christmastown.mapData.christmasTownDataFetched,
    isAdmin: state.christmastown.mapData.user.isAdmin,
    itsaMinigameCell: state.christmastown.minigame.itsaMinigameCell,
    mediaType: state.browser.mediaType,
    showAdminPanel: state.christmastown.devPanelActivated,
    map: state.christmastown.mapData.map,
    position: state.christmastown.mapData.user.position,
    publishedMapsList: state.lobby.publishedMapsList
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChristmasTown)
