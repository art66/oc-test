import { connect } from 'react-redux'
import Creator from '../../components/Lobby/Creator'

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType,
  isDesktopLayoutSet: state.common.isDesktopLayoutSet
})

export default connect(
  mapStateToProps,
  null
)(Creator)
