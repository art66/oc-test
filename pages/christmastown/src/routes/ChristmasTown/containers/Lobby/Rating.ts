import { connect } from 'react-redux'
import Rating from '../../components/Lobby/Rating'

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType
})

export default connect(
  mapStateToProps,
  null
)(Rating)
