import { connect } from 'react-redux'
import Join from '../../components/Lobby/Join'

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType
})

export default connect(
  mapStateToProps,
  null
)(Join)
