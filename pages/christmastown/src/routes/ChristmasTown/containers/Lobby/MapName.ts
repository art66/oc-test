import { connect } from 'react-redux'
import MapName from '../../components/Lobby/MapName'

const mapStateToProps = (state) => ({
  mediaType: state.browser.mediaType
})

export default connect(mapStateToProps, null)(MapName)
