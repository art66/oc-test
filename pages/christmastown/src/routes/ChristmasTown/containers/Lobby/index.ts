import { connect } from 'react-redux'

import Lobby from '../../components/Lobby'

import { lazyMapsLoad } from '../../modules/lobby/actions'

const mapStateToProps = ({ lobby }) => ({
  userActiveMapID: lobby.settings.userActiveMapID,
  inProgress: lobby.settings.inProgress,
  showImages: lobby.settings.showImages,
  lazyLoadInProgress: lobby.settings.lazyLoad.inProgress,
  noMoreLazyData: lobby.settings.lazyLoad.noMoreData,
  publishedMapsList: lobby.publishedMapsList
})

const mapDispatchToProps = dispatch => ({
  initLazyMapsLoad: () => dispatch(lazyMapsLoad())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Lobby)
