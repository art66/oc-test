import { connect } from 'react-redux'
import { setStatusAreaItem, devToggler } from '../modules'
import UserInventory from '../components/UserInventory'

const mapStateToProps = ({ christmastown }) => ({
  selectedItemId: christmastown.statusArea.item && christmastown.statusArea.item.item_id,
  showAdminPanel: christmastown.devPanelActivated,
  isAdmin: christmastown.mapData.user.isAdmin,
  inventory: christmastown.mapData.inventory,
  newInventoryItems: christmastown.mapData.newInventoryItems
})

const mapDispatchToProps = {
  setStatusAreaItemRun: setStatusAreaItem,
  devTogglerRun: devToggler
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInventory)
