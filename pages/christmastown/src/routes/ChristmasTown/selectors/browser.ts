import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export const getMediaType = state => state.browser.mediaType as TMediaType
