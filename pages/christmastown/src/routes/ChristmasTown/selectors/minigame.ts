export const getGameAccepted = state => state.christmastown.minigame.gameAccepted
export const getGameEndResult = state => state.christmastown.minigame.gameEndResult
export const getGameOver = state => state.christmastown.minigame.gameOver
export const getLoadedMinigameName = state => state.christmastown.minigame.gameName
