import { getOpenedGateControllerForCurrentGate, getGateControllersForCurrentGate } from '../mapData'

describe('mapData', () => {
  const createState = (args) => ({
    christmastown: {
      mapData: {
        ...args
      }
    }
  })

  it('should get opened gate controllers for the current gate', () => {
    const params = {
      position: { x: 68, y: -7 },
      type: 93,
      width: 120
    }
    const gateController = { position: { x: 68, y: -10 }, status: 'opened', time: 0, radius: '9' }
    const state = createState({ gates: [gateController] })

    expect(getOpenedGateControllerForCurrentGate(state, params)).toEqual(gateController)
  })

  it('should not return anything if there are no opened gate controllers', () => {
    const params = {
      position: { x: 68, y: -7 },
      type: 93,
      width: 120
    }
    const gateController = { position: { x: 68, y: -10 }, status: 'closed', time: 0, radius: '9' }
    const state = createState({ gates: [gateController] })

    expect(getOpenedGateControllerForCurrentGate(state, params)).toEqual(undefined)
  })

  it('should not return anything if there are no gate controllers for the current gate', () => {
    const params = {
      position: { x: 666, y: 666 },
      type: 93,
      width: 120
    }
    const gateController = { position: { x: 68, y: -10 }, status: 'closed', time: 0, radius: '9' }
    const state = createState({ gates: [gateController] })

    expect(getGateControllersForCurrentGate(state, params)).toEqual([])
  })
})
