import { compose, prop, find } from 'ramda'
import { createSelector } from 'reselect'
import * as c from '../constants'
import { isInRectangle } from '../../../../../christmastown_editor/src/routes/MapEditor/utils'
import IGateController from '../interfaces/IGateController'
import { STATUS_OPENED } from '../components/ObjectsLayer/Gate/constants'

const createGatePositionRectangle = gate => {
  const { position, width, height } = gate

  return [
    position,
    { ...position, x: position.x + width / c.CELL_SIZE },
    { ...position, y: position.y - height / c.CELL_SIZE },
    { x: position.x + width / c.CELL_SIZE, y: position.y - height / c.CELL_SIZE }
  ]
}

const createGateControllerPositionRectangle = ({ radius, position: { x, y } }) => {
  const r = parseInt(radius, 10)

  return {
    leftTop: { x: x - r, y: y + r },
    rightBottom: { x: x + r, y: y - r }
  }
}

const getChristmasTown = prop('christmastown')
const getGateProperties = (_, params) => params
const getMapData = compose(prop('mapData'), getChristmasTown)
const getGateControllers = compose(prop('gates'), getMapData)
const getGateControllerWithOpenedStatus = find<IGateController>(gc => gc.status === STATUS_OPENED)

export const getGateControllersForCurrentGate = createSelector<any, any, any, IGateController[] | undefined>(
  [getGateProperties, getGateControllers],
  (gate, controllers) =>
    controllers.filter(controller =>
      createGatePositionRectangle(gate).some(pos =>
        isInRectangle(pos, createGateControllerPositionRectangle(controller))))
)

export const getOpenedGateControllerForCurrentGate = createSelector<any, any, any, IGateController | undefined>(
  getGateControllersForCurrentGate,
  getGateControllerWithOpenedStatus
)

export const getMinigameType = state => state.christmastown.statusArea.cellEvent?.miniGameType
