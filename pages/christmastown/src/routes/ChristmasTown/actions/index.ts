import { createAction } from 'redux-actions'
import * as a from '../actionTypes'

export const updateGateController = createAction(a.UPDATE_GATE_CONTROLLER, (position, gate) => ({ position, gate }))
export const updateGateControllers = createAction(a.UPDATE_GATE_CONTROLLERS, (gates, time, status) => ({
  gates,
  time,
  status
}))
export const sendChatMessage = createAction(a.SEND_CHAT_MESSAGE, message => ({ message }))
export const setUserMessage = createAction(a.SET_USER_MESSAGE, (message, userID) => ({ message, userID }))
export const collectGarbage = createAction(a.COLLECT_GARBAGE)
export const checkHudTabsSync = createAction(a.CHECK_HUD_TABS_SYNC, (mapID, position) => ({ mapID, position }))
export const synchronizeHudData = createAction(a.SYNCHRONIZE_HUD_DATA, mapData => mapData)
export const updateUserInventory = createAction(a.UPDATE_USER_INVENTORY, inventory => ({ inventory }))
export const setEnteringAreaMessage = createAction(a.SET_ENTERING_AREA_MESSAGE, (message: string) => ({ message }))
export const removeEnteringAreaMessage = createAction(a.REMOVE_ENTERING_AREA_MESSAGE)
export const movedError = createAction(a.MOVED_ERROR)
