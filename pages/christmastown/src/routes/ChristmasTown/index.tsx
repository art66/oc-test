import Loadable from 'react-loadable'
import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/helpers/rootReducer'
import { initStart } from './modules/lobby/actions'
import lobbyReducer from './modules/lobby/reducers'
import reducer from './reducers'
import './styles/fonts.scss'

const runLobbyController = () => {
  injectReducer(rootStore, { reducer: lobbyReducer, key: 'lobby' })
  rootStore.dispatch(initStart())
}

const Preloader = () => null

const ChristmasTownRoute = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const MyMaps = await import(/* webpackChunkName: "ChristmasTown" */ './layout/index')

    injectReducer(rootStore, { reducer, key: 'christmastown' })

    // because Lobby is the part of th CT we need to run its
    // controller on the very beginning along own CT's one
    runLobbyController()

    return MyMaps
  },
  loading: Preloader
})

export default ChristmasTownRoute
