import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import GameStartScreen from '../../components/GameStartScreen'
import ScoreBoard from '../../components/ScoreBoard'
import { setGameTitle } from '../../modules'
import { getGameAccepted, getGameEndResult, getGameOver } from '../../selectors/minigame'
import styles from './minigameWrapper.cssmodule.scss'
import slideAnimation from './slideAnimation.cssmodule.scss'

type TProps = {
  gameName: string
  gameTitle: string
  gameDescription?: string
}

export const AnimationDuration = 700

export const withMiniGameWrapper = (ownProps: TProps) => (WrappedComponent: React.ComponentType) => () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setGameTitle({ gameTitle: ownProps.gameTitle }))
  }, [])

  const gameAccepted = useSelector(getGameAccepted)
  const gameEndResult = useSelector(getGameEndResult)
  const gameOver = useSelector(getGameOver)

  const selectComponent = () => {
    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen
            message={`Play ${ownProps.gameTitle}?`}
            yesTitle='YES'
            noTitle='NO'
            rules={ownProps.gameDescription}
          />
        </CSSTransition>
      )
    }

    if (gameOver) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName={ownProps.gameName} title={`${ownProps.gameTitle} high scores`} {...gameEndResult} />
        </CSSTransition>
      )
    }

    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='WrappedComponent'>
        <WrappedComponent />
      </CSSTransition>
    )
  }

  return <TransitionGroup className={styles.ctMiniGameWrapper}>{selectComponent()}</TransitionGroup>
}
