import React, { useEffect } from 'react'
import rootStore from '../../../store/createStore'
import ChristmasTown from '../containers/ChristmasTown'
import initLobbyWS, { unsubscribeLobbyWS } from '../modules/lobby/websockets'
import initCTWS, { unsubscribeCTWS } from '../modules/websockets'

const AppLayout = () => {
  useEffect(() => {
    initCTWS(rootStore.dispatch)
    initLobbyWS(rootStore.dispatch)

    return () => {
      unsubscribeCTWS()
      unsubscribeLobbyWS()
    }
  })
  return <ChristmasTown />
}

export default AppLayout
