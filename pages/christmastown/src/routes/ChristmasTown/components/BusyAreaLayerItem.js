import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { CELL_SIZE } from '../constants'

export class BusyAreaLayerItem extends Component {
  shouldComponentUpdate(newProps) {
    const { busyAreaItem } = this.props

    return (
      busyAreaItem.quantity !== newProps.busyAreaItem.quantity ||
      busyAreaItem.duration !== newProps.busyAreaItem.duration
    )
  }

  render() {
    const { busyAreaItem } = this.props

    return (
      <div
        className="ct-busy-area"
        style={{
          width: CELL_SIZE,
          height: CELL_SIZE,
          left: busyAreaItem.position.x * CELL_SIZE,
          top: busyAreaItem.position.y * -CELL_SIZE
        }}
      >
        <i
          className={'ct-tracks-14 '}
          style={{
            opacity: (busyAreaItem.quantity - 3) / 10 || 0,
            animationDuration: busyAreaItem.duration + 's'
          }}
        />
      </div>
    )
  }
}

BusyAreaLayerItem.propTypes = {
  busyAreaItem: PropTypes.object.isRequired
}

export default BusyAreaLayerItem
