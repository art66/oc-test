import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

class Dialog extends Component {
  getButtons = () => {
    const { buttons } = this.props

    return buttons.map((button, index) => (
      <button key={index} onClick={() => button.onClick()} className={cn('btn-blue', s.button)}>
        {button.text}
      </button>
    ))
  }

  render() {
    const { text } = this.props
    const buttons = this.getButtons()

    return (
      <div className={s.ctDialog}>
        <div className={s.centerBlock}>
          <div className={s.text} dangerouslySetInnerHTML={{ __html: text }} />
          <div>{buttons}</div>
        </div>
      </div>
    )
  }
}

Dialog.propTypes = {
  text: PropTypes.string,
  buttons: PropTypes.array
}

export default Dialog
