import PropTypes from 'prop-types'
import React from 'react'
import { Ticker } from '@torn/shared/hoc'
import { hhmmss } from '@torn/shared/utils'
import s from './styles.cssmodule.scss'

const GameCountdownScreen = ({ timeLeft }) => (
  <div className={s.cd}>
    <div>{`Next game starts in ${hhmmss(timeLeft)}`}</div>
  </div>
)

GameCountdownScreen.propTypes = {
  resetGame: PropTypes.func,
  timeLeft: PropTypes.number
}

// export default Ticker(GameCountdownScreen, { iteratedProperty: 'timeLeft', end: 0, callbackName: 'restartGame' })
export default props => <Ticker WrappedComponent={GameCountdownScreen} wrapProps={props} options={{ iteratedProperty: 'timeLeft', end: 0, callbackName: 'restartGame' }} />
