import IPosition from '../../../interfaces/IPosition'

export interface IDefaultProps {
  setUserPosition?: () => void
  itsaMinigameCell?: boolean
  mediaType?: string
  collectGarbage?: () => void
}

export interface IProps extends IDefaultProps {
  fetchMapData: () => void
  checkHudTabsSync: (mapID: string, position: IPosition) => void
  loadGame: () => void
  move: (direction: string) => void
  showAdminPanel: boolean
  userStatus: string
  infoMessage: string
  christmasTownDataFetched: boolean
  isAdmin: boolean
  item: string
  cellEventType: string
  map: { id: string, name: string, size: any }
  publishedMapsList: []
  position: IPosition
}
