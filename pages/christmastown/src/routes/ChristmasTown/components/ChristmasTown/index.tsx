import React from 'react'
import InfoBox from '@torn/shared/components/InfoBox'

import Title from '../Title/index'
import Map from '../Map'
import StatusArea from '../StatusArea'
import UserInventory from '../../containers/UserInventory'
import AreaTooltip from '../AreaTooltip'
import MiniGamesQuickAccess from '../MiniGamesQuickAccess'
import Lobby from '../../containers/Lobby'

import { IProps, IDefaultProps } from './interfaces'
import '../../styles/christmas_town.css'

import { JAIL, HOSPITAL, PIXELS } from '../../constants'
import { IWindow } from '../../interfaces/IWindow'

const GARBAGE_COLLECTOR_INTERVAL = 15000

declare const window: IWindow

export class ChristmasTown extends React.Component<IProps> {
  private _garbageCollectorInterval: any

  static defaultProps: IDefaultProps = {
    setUserPosition: () => {},
    itsaMinigameCell: false,
    mediaType: '',
    collectGarbage: () => {}
  }

  componentDidMount() {
    const { fetchMapData } = this.props

    fetchMapData()

    window.addEventListener('beforeunload', this._leaveTown)
    window.addEventListener('focus', this._handleTabFocus)

    this._initGarbageCollectorInterval()
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this._leaveTown)
    window.removeEventListener('focus', this._handleTabFocus)
    clearInterval(this._garbageCollectorInterval)
  }

  _initGarbageCollectorInterval = () => {
    this._garbageCollectorInterval = setInterval(() => {
      const { collectGarbage } = this.props

      collectGarbage()
    }, GARBAGE_COLLECTOR_INTERVAL)
  }

  _showTooltip = (e: any) => {
    // legacy
    const tooltip = e.currentTarget.getElementsByClassName('ct-tooltip')

    tooltip[0].style.display = 'block'
    tooltip[0].style.left = tooltip[0].clientWidth / -2 + tooltip[0].parentElement.clientWidth / 2 - 2 + PIXELS
  }

  _hideTooltip = (e: any) => {
    // legacy
    const tooltip = e.currentTarget.getElementsByClassName('ct-tooltip')

    tooltip[0].style.display = 'none'
  }

  _isVisibleUserMap = () => {
    const { itsaMinigameCell, mediaType, item, cellEventType } = this.props
    const chestCell = cellEventType === 'chests'
    const isMinigame = itsaMinigameCell && cellEventType !== 'teleport'
    const isMobileMode = document.body.classList.contains('d') && document.body.classList.contains('r')

    return !isMobileMode || mediaType === 'desktop' || (!isMinigame && !chestCell && !item)
  }

  _handleTabFocus = () => {
    const { checkHudTabsSync, map, position } = this.props

    map && map.id && checkHudTabsSync(map.id, position)
  }

  _leaveTown = () => {
    return window.getAction({
      async: false,
      type: 'post',
      action: '/christmas_town.php?q=leaveTown'
    })
  }

  _renderMapDirections = () => {
    return (
      <div className='map-directions'>
        <div className='direction north' />
        <div className='direction east' />
        <div className='direction south' />
        <div className='direction west' />
      </div>
    )
  }

  _renderTitle = () => {
    const { christmasTownDataFetched } = this.props

    return <div className='title-wrap clearfix'>{christmasTownDataFetched && <Title />}</div>
  }

  _renderMap = () => {
    const { move } = this.props
    const showUserMap = this._isVisibleUserMap()

    if (!showUserMap) {
      return null
    }

    return (
      <div className='user-map-container'>
        <div role='button' tabIndex={0} className='user-map' id='user-map'>
          <div id='map'>
            <Map move={move} />
            {this._renderMapDirections()}
          </div>
          <AreaTooltip />
        </div>
      </div>
    )
  }

  _renderMiniGameQuickAccessButtons = () => {
    const { isAdmin, loadGame, showAdminPanel, map } = this.props

    if (isAdmin && showAdminPanel && map) {
      return (
        <React.Fragment>
          {this._renderHrDelimiter()}
          <MiniGamesQuickAccess loadGame={loadGame} />
        </React.Fragment>
      )
    }

    return null
  }

  _renderStatusArea = () => {
    return <StatusArea showTooltip={this._showTooltip} hideTooltip={this._hideTooltip} />
  }

  _renderUSerInventory = () => {
    return <UserInventory />
  }

  _renderCT = () => {
    const { userStatus, infoMessage, map } = this.props

    if ([JAIL, HOSPITAL].includes(userStatus)) {
      return <InfoBox msg={infoMessage} color='red' />
    }

    return (
      map && (
        <div id='ct-wrap' className='ct-wrap ct-user-wrap'>
          {this._renderTitle()}
          {this._renderMap()}
          {this._renderStatusArea()}
          {this._renderUSerInventory()}
        </div>
      )
    )
  }

  _renderLobby = () => {
    const { publishedMapsList } = this.props

    return publishedMapsList.length !== 0 && <Lobby />
  }

  _renderHrDelimiter = () => {
    const { map } = this.props

    return map && <hr className='delimiter-999' />
  }

  render() {
    return (
      <React.Fragment>
        {this._renderCT()}
        {this._renderMiniGameQuickAccessButtons()}
        {this._renderHrDelimiter()}
        {this._renderLobby()}
      </React.Fragment>
    )
  }
}

export default ChristmasTown
