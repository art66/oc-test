import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import { IOwnProps, IProps } from './interfaces'
import { SVG_JOIN } from './constants'
import styles from './index.cssmodule.scss'

class Join extends React.PureComponent<IOwnProps & IProps> {
  static defaultProps: IProps = {
    joinLink: ''
  }

  componentDidMount() {
    tooltipsSubscriber.subscribe({
      child: 'Join Map',
      ID: this._getTooltipID()
    })
  }

  _getTooltipID = () => {
    const { mapID } = this.props

    return `${mapID}-join`
  }

  render() {
    const { joinLink, mediaType } = this.props
    const { iconName, fill, dimensions } = SVG_JOIN
    const { isDesktop } = checkMediaType(mediaType)

    return (
      <div className={styles.joinBox}>
        <a id={(isDesktop && this._getTooltipID()) || ''} className={styles.link} href={joinLink} target='_self'>
          <SVGIconGenerator
            iconName={iconName}
            dimensions={dimensions}
            fill={fill}
            customClass={styles.customSVGClass}
          />
        </a>
      </div>
    )
  }
}

export default Join
