import { TMapID } from '../../../../interfaces/ILobby'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IOwnProps {
  mapID: TMapID
  mediaType: TMediaType
}
export interface IProps {
  joinLink: string
}
