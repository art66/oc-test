export const FILL_BLUE_HOVERED = 'BLUE_HOVERED'
export const FILL_BLUE_DIMMED = 'BLUE_DIMMED'
export const SVG_JOIN = {
  iconName: 'Join',
  dimensions: {
    width: 16,
    height: 16,
    viewbox: '0 0 24 24'
  },
  fill: {
    name: FILL_BLUE_DIMMED
  }
}
