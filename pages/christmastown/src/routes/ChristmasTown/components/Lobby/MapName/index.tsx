import React from 'react'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import { IProps } from './interfaces'
import styles from './index.cssmodule.scss'

class MapName extends React.PureComponent<IProps> {
  static defaultProps: IProps = {
    mapName: '',
    mapID: '',
    mediaType: 'desktop'
  }

  componentDidMount() {
    const { mapName } = this.props

    tooltipsSubscriber.subscribe({
      child: mapName,
      ID: this.getTooltipID()
    })
  }

  componentWillUnmount() {
    tooltipsSubscriber.unsubscribe({
      child: null,
      ID: this.getTooltipID()
    })
  }

  getTooltipID = () => {
    const { mapID } = this.props

    return `mapName-${mapID}`
  }

  render() {
    const { mapName, mediaType } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    return (
      <div id={!isDesktop ? this.getTooltipID() : ''} className={styles.mapNameBox}>
        {mapName}
      </div>
    )
  }
}

export default MapName
