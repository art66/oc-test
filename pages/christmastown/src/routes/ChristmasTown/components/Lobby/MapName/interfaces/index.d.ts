import { TMapID } from '../../../../interfaces/ILobby'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IProps {
  mapName: string
  mapID: TMapID
  mediaType: TMediaType
}
