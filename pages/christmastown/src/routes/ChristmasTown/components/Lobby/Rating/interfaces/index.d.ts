import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import { IProps as IRatingBoxProps } from '../../../../../../components/RatingBox/interfaces'
import { IProps as IVotes } from '../../Votes/interfaces'
import { TMapID } from '../../../../interfaces/ILobby'

export interface IOwnProps {
  mapID: TMapID
  IsRowMapJoined: IRatingBoxProps['isMapJoinedActive']
  isRowHovered: IRatingBoxProps['isRowHovered']
  mediaType: TMediaType
}

export interface IProps extends IVotes {
  rating: number
}

export interface IState {
  isRatingHovered: boolean
}
