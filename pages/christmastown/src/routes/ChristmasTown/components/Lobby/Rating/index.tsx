import React from 'react'
import Tooltip from '@torn/shared/components/Tooltip'

import RatingBox from '../../../../../containers/RatingBox'
import Votes from '../Votes'

import { IOwnProps, IProps, IState } from './interfaces'
import styles from './index.cssmodule.scss'

class Rating extends React.PureComponent<IOwnProps & IProps, IState> {
  _renderVotesTooltip = () => {
    const { votes, mapID } = this.props

    return (
      <Tooltip
        position='top'
        arrow='center'
        parent={`rating-${mapID}`}
        style={{
          style: {
            padding: '10px 0 15px 0',
            background: 'var(--ct-rating-votes-bg)',
            boxShadow: '0 0 2px var(--ct-rating-votes-wrap-shadow-color)'
          },
          arrowStyle: {
            color: 'var(--ct-rating-votes-bg)',
            borderColor: 'var(--ct-rating-votes-bg)',
            filter: 'drop-shadow(0 1px 0 var(--ct-rating-votes-arrow-shadow-color))'
          }
        }}
      >
        <Votes votes={votes} />
      </Tooltip>
    )
  }

  render() {
    const { mediaType, isRowHovered, IsRowMapJoined, rating, mapID } = this.props

    return (
      <div id={`rating-${mapID}`} className={styles.ratingBox}>
        {this._renderVotesTooltip()}
        <RatingBox
          isRowHovered={isRowHovered}
          isMapJoinedActive={IsRowMapJoined}
          data={rating}
          mediaType={mediaType}
          isAdaptive={true}
          disableMobileGradient={true}
          isMobileModeOnTablet={mediaType === 'tablet'}
        />
      </div>
    )
  }
}

export default Rating
