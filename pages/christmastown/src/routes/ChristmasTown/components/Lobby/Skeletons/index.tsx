import React from 'react'

import styles from './index.cssmodule.scss'

class Skeletons extends React.PureComponent {
  _renderRows = () => {
    const rowsCount = Array.from(Array(10).keys())

    return rowsCount.map((row) => (
      <div key={row} className={styles.rowWrap}>
        <div className={styles.nameSection} />
        <div className={styles.prizeSection} />
        <div className={styles.creatorSection}>
          <i className={styles.status} />
        </div>
        <div className={styles.playersSection} />
        <div className={styles.ratingSection} />
        <div className={styles.joinSection} />
      </div>
    ))
  }

  render() {
    return <div className={styles.rowsSkeletonWrap}>{this._renderRows()}</div>
  }
}

export default Skeletons
