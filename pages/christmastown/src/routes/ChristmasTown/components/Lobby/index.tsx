import React, { RefObject } from 'react'
import classnames from 'classnames'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import iconsHolderCT from '@torn/shared/SVG/helpers/iconsHolder/christmastown'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

import MapName from '../../containers/Lobby/MapName'
import Creator from '../../containers/Lobby/Creator'
import Players from './Players'
import Rating from '../../containers/Lobby/Rating'
import Join from '../../containers/Lobby/Join'
import Prize from './Prize'
import Skeletons from './Skeletons'

import { IProps, IState } from './interfaces'
import { IRow } from '../../interfaces/ILobby'

import styles from './index.cssmodule.scss'

interface IClassNameHolder {
  headerClasses: {
    map: string
    creator: string
    players: string
    rating: string
    join: string
    prize: string
  }
  rowClass: string
}

class Lobby extends React.Component<IProps, IState> {
  lobby: RefObject<HTMLDivElement>

  constructor(props: IProps) {
    super(props)

    this.lobby = React.createRef()
    this.state = {
      activeRowID: null
    }
  }

  componentDidMount() {
    document.addEventListener('scroll', this._handlerLazyLoadByScroll)
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this._handlerLazyLoadByScroll)
  }

  _classNameHolder = (isRowMapJoined?: boolean): IClassNameHolder => {
    const headerClasses = {
      map: classnames(styles.headerSection, styles.mapName),
      creator: classnames(styles.headerSection, styles.creator),
      players: classnames(styles.headerSection, styles.players),
      rating: classnames(styles.headerSection, styles.rating),
      join: classnames(styles.headerSection, styles.join),
      prize: classnames(styles.headerSection, styles.prize)
    }

    const rowClass = classnames({
      [styles.rowWrap]: true,
      [styles.rowActiveMapJoined]: isRowMapJoined
    })

    return {
      headerClasses,
      rowClass
    }
  }

  _handlerLazyLoadByScroll = () => {
    const { initLazyMapsLoad, inProgress, lazyLoadInProgress, noMoreLazyData } = this.props
    const isBottomLobby = this.isBottomLobby()

    if (!isBottomLobby || inProgress || lazyLoadInProgress || noMoreLazyData) {
      return
    }

    if (isBottomLobby) {
      initLazyMapsLoad()
    }
  }

  isBottomLobby = () => {
    const lobbyElement = this.lobby.current
    const lobbyElementTop = lobbyElement && lobbyElement.offsetTop
    const lobbyElementHeight = lobbyElement && lobbyElement.clientHeight
    const windowHeight = window.innerHeight
    const windowScroll = window.scrollY

    return windowScroll >= lobbyElementTop + lobbyElementHeight - windowHeight
  }

  _checkIsRowMapJoined = (mapID: string): boolean => {
    const { userActiveMapID } = this.props

    return userActiveMapID === mapID
  }

  _isRowHovered = (mapID: string): boolean => {
    const { activeRowID } = this.state

    return activeRowID === mapID
  }

  _handleRowOver = ({ target }: any) => {
    const row = target && target.closest('[class^="rowWrap"]')
    const rowID = (row && row.dataset.id) || null

    if (!row || !rowID) {
      return
    }

    this.setState({
      activeRowID: rowID
    })
  }

  _handleRowLeave = () => {
    this.setState({
      activeRowID: null
    })
  }

  _renderPlaceholder = () => {
    return <div className={styles.rowsPlaceholder}>There is no available maps right now.</div>
  }

  _renderHeader = () => {
    const { headerClasses } = this._classNameHolder()

    return (
      <div className={styles.headerWrap}>
        <span className={headerClasses.map}>Map</span>
        <span className={headerClasses.prize}>Prize</span>
        <span className={headerClasses.creator}>Creator</span>
        <span className={headerClasses.players}>
          <SVGIconGenerator
            customClass={styles.playerIcon}
            iconName='CTPlayers'
            iconsHolder={iconsHolderCT}
            preset={{ type: 'christmasTown', subtype: 'CT_PLAYERS' }}
          />
        </span>
        <span className={headerClasses.rating}>Rating</span>
        <span className={headerClasses.join}>Join</span>
      </div>
    )
  }

  _renderBody = () => {
    const { publishedMapsList, showImages, inProgress } = this.props

    if (inProgress) {
      return <Skeletons />
    }

    if (!publishedMapsList || publishedMapsList.length === 0) {
      return this._renderPlaceholder()
    }

    const rowsToRender = publishedMapsList.map((row: IRow) => {
      const IsRowMapJoined = this._checkIsRowMapJoined(row.mapID)
      const { rowClass } = this._classNameHolder(IsRowMapJoined)

      return (
        <div
          key={row.mapID}
          className={rowClass}
          data-id={row.mapID}
          onMouseEnter={this._handleRowOver}
          onMouseLeave={this._handleRowLeave}
        >
          <MapName mapName={row.mapName} mapID={row.mapID} />
          <Prize prizeIconSrc={row.prizeIconSrc} />
          <Creator {...row.author} showImages={showImages} mapID={row.mapID} />
          <Players playersOnline={row.playersOnline} />
          <Rating
            votes={row.votes}
            rating={row.rating}
            mapID={row.mapID}
            isRowHovered={this._isRowHovered(row.mapID)}
            IsRowMapJoined={IsRowMapJoined}
          />
          <Join mapID={row.mapID} joinLink={row.joinLink} />
        </div>
      )
    })

    return <div className={styles.bodyWrap}>{rowsToRender}</div>
  }

  _renderLazyLoader = () => {
    const { lazyLoadInProgress } = this.props

    return (
      <div className={styles.lazyPreloader}>
        {lazyLoadInProgress && <AnimationLoad dotsColor='black' dotsCount={7} />}
      </div>
    )
  }

  render() {
    return (
      <div className={styles.lobbyWrap} ref={this.lobby}>
        {this._renderHeader()}
        {this._renderBody()}
        {this._renderLazyLoader()}
      </div>
    )
  }
}

export default Lobby
