import { IRow, TShowImages, TProgress, TMapID, TUserActiveMapID } from '../../interfaces/ILobby'

export interface IProps {
  userActiveMapID: TUserActiveMapID
  inProgress: TProgress
  publishedMapsList: IRow[]
  showImages: TShowImages
  lazyLoadInProgress: boolean
  noMoreLazyData: boolean
  initLazyMapsLoad: () => void
}

export interface IState {
  activeRowID: TMapID
}
