import React from 'react'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import { SVG_ICON, USER_LABEL, FACTION_LABEL } from './constants'
import { IDefaultProps, IOwnProps, IProps } from './interfaces'
import styles from './index.cssmodule.scss'

class Creator extends React.PureComponent<IOwnProps & IProps> {
  static defaultProps: IDefaultProps = {
    factionTag: '',
    factionTagImageUrl: '',
    lastActionTimestamp: 0,
    factionRank: ''
  }

  componentDidMount() {
    this._initTooltips()
  }

  _initTooltips = () => {
    const { mapID, username, userID, factionTag } = this.props

    const tooltips = [
      {
        child: factionTag,
        ID: `${mapID}-${FACTION_LABEL}`
      },
      {
        child: `${username} [${userID}]`,
        ID: `${mapID}-${USER_LABEL}`
      }
    ]

    tooltips.forEach(tooltip => {
      tooltipsSubscriber.subscribe(tooltip)
    })
  }

  _classNamesHolder = () => {
    const factionClasses = classnames({
      [styles.factionWrap]: true,
      [styles.textWrap]: !this._showImages()
    })

    const userClasses = classnames({
      [styles.userImageWrap]: true,
      [styles.textWrap]: !this._showImages()
    })

    return {
      factionClasses,
      userClasses
    }
  }

  _showImages = () => {
    const { showImages } = this.props

    return showImages
  }

  _linkDecorator = (children: JSX.Element, type: 'faction' | 'user') => {
    const { mapID, userID, factionID, mediaType } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    if (children === null) {
      return null
    }

    const href = type === USER_LABEL ? `/profiles.php?XID=${userID}` : `/factions.php?step=profile&ID=${factionID}`
    const tooltipsLinkID = isDesktop && this._showImages() ? `${mapID}-${type}` : ''

    const link = (
      <a rel='noopener noreferrer' target='_blank' id={tooltipsLinkID} className={styles.linkWrap} href={href}>
        {children}
      </a>
    )

    return link
  }

  _renderStatus = () => {
    const { onlineStatus } = this.props
    const { iconName, types, fills, filter, gradients, dimensions } = SVG_ICON

    return (
      <div className={styles.userStatusWrap}>
        <SVGIconGenerator
          iconName={iconName}
          type={types[onlineStatus]}
          fill={fills[onlineStatus]}
          gradient={gradients[onlineStatus]}
          dimensions={dimensions}
          filter={filter}
        />
      </div>
    )
  }

  _renderFactionImage = () => {
    const { factionTag, factionTagImageUrl, factionRank } = this.props
    const { factionClasses } = this._classNamesHolder()

    const factionImage = factionTagImageUrl ? (
      <img
        className={classnames(styles.factionImage, styles[factionRank])}
        src={decodeURIComponent(factionTagImageUrl)}
      />
    ) : null

    const factionName = factionTag ? <span className={styles.factionName}>{factionTag}</span> : null
    const factionRenderData = this._showImages() ? factionImage : factionName

    return <div className={factionClasses}>{this._linkDecorator(factionRenderData, FACTION_LABEL)}</div>
  }

  _renderUserImage = () => {
    const { username: name, userImageUrl } = this.props
    const { userClasses } = this._classNamesHolder()

    const userImage = userImageUrl ? <img className={styles.userImage} src={decodeURIComponent(userImageUrl)} /> : null
    const userName = name ? <span className={styles.userName}>{name}</span> : null
    const userRenderData = this._showImages() ? userImage : userName

    return <div className={userClasses}>{this._linkDecorator(userRenderData, USER_LABEL)}</div>
  }

  render() {
    const { mediaType, isDesktopLayoutSet } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    if (!isDesktop && !isDesktopLayoutSet) {
      return null
    }

    return (
      <div className={styles.creatorBox}>
        {this._renderStatus()}
        {this._renderFactionImage()}
        {this._renderUserImage()}
      </div>
    )
  }
}

export default Creator
