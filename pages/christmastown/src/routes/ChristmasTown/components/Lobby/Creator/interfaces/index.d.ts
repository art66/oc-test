import { TMapID, TOnlineStatus } from '../../../../interfaces/ILobby'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'

export interface IOwnProps {
  mapID: TMapID
  showImages: boolean
  mediaType: TMediaType
  isDesktopLayoutSet: boolean
}

export interface IDefaultProps {
  factionTag?: string
  factionTagImageUrl?: string
  lastActionTimestamp?: number
  factionRank?: TFactionRank
}

export interface IProps extends IDefaultProps {
  onlineStatus: TOnlineStatus
  userID: number
  username: string
  userImageUrl: string
  factionID: number
}
