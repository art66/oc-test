export const SVG_ICON = {
  iconName: 'OnlineStatus',
  types: {
    online: 'online',
    idle: 'idle',
    offline: 'offline'
  },
  fills: {
    online: {
      color: 'url("#svg_status_online")',
      strokeWidth: 0
    },
    idle: {
      color: 'url("#svg_status_idle")',
      strokeWidth: 0
    },
    offline: {
      color: 'url("#svg_status_offline")',
      strokeWidth: 0
    }
  },
  gradients: {
    online: {
      active: true,
      ID: 'svg_status_online',
      transform: 'translate(4003.99 5792.17) scale(11.88)',
      gradientUnits: 'userSpaceOnUse',
      coords: {
        x1: '-243.94',
        y1: '-487.37',
        x2: '-243.94',
        y2: '-486.36'
      },
      scheme: [
        {
          step: '0',
          color: '#a3d900'
        },
        {
          step: '1',
          color: '#4c6600'
        }
      ]
    },
    idle: {
      active: true,
      ID: 'svg_status_idle',
      transform: 'matrix(11.88, 0, 0, -11.88, 11469.59, 5991.42)',
      gradientUnits: 'userSpaceOnUse',
      coords: {
        x1: '-964.96',
        y1: '504.33',
        x2: '-964.96',
        y2: '503.31'
      },
      scheme: [
        {
          step: '0',
          color: '#ffbf00'
        },
        {
          step: '1',
          color: '#b25900'
        }
      ]
    },
    offline: {
      active: true,
      ID: 'svg_status_offline',
      transform: 'matrix(11.88, 0, 0, -11.88, 10388.51, 5992.42)',
      gradientUnits: 'userSpaceOnUse',
      coords: {
        x1: '-873.96',
        y1: '504.42',
        x2: '-873.96',
        y2: '503.31'
      },
      scheme: [
        {
          step: '0',
          color: '#ccc'
        },
        {
          step: '1',
          color: '#666'
        }
      ]
    }
  },
  filter: {
    active: false,
    ID: null
  },
  dimensions: {
    width: 13,
    height: 13,
    viewbox: '-2 -1 14 14'
  }
}

export const USER_LABEL = 'user'
export const FACTION_LABEL = 'faction'
