import React from 'react'

import { IProps } from './interfaces'
import styles from './index.cssmodule.scss'

class Players extends React.PureComponent<IProps> {
  static defaultProps: IProps = {
    playersOnline: 0
  }

  render() {
    const { playersOnline } = this.props

    return <div className={styles.playersBox}>{playersOnline}</div>
  }
}

export default Players
