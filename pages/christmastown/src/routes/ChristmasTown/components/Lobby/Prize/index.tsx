import React from 'react'
import { IPrize } from './interfaces'
import styles from './index.cssmodule.scss'

class Prize extends React.PureComponent<IPrize> {
  renderPrize = () => {
    const { prizeIconSrc } = this.props

    return prizeIconSrc ? (
      <img src={prizeIconSrc} className={styles.imgPrize} alt='Snow globe ornament' />
    ) : (
      <span className={styles.msgPrize}>-</span>
    )
  }

  render() {
    return <div className={styles.prizeBox}>{this.renderPrize()}</div>
  }
}

export default Prize
