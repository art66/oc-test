import React from 'react'

import { IProps } from './interfaces'
import styles from './index.cssmodule.scss'

const MAX_PERCENTAGE = 100
const MAX_BAR_WIDTH = 80

// TODO: perhaps should be placed on the same level as the other components.
// To prevent appear/hiding flashing. Need to discuss with Joe first.
class Votes extends React.PureComponent<IProps> {
  _getTotalSum = () => {
    const { votes } = this.props
    const starsCount = this._getStarsCount()

    let totalSum = 0

    starsCount.forEach(bar => {
      totalSum += votes[bar]
    })

    return totalSum
  }

  _getTotalLabel = (sumValue: number) => {
    return sumValue === 1 ? 'Vote' : 'Votes'
  }

  _getStarsCount = () => {
    const { votes } = this.props

    return Object.keys(votes)
  }

  _normalizeVotes = () => {
    const { votes } = this.props
    const starsCount = this._getStarsCount()

    let normalizedVotesSum = 0

    starsCount.forEach(key => {
      const currentStar = votes[key]
      const normalizedVote = currentStar * Number(key)

      normalizedVotesSum += normalizedVote
    })

    return normalizedVotesSum
  }

  _generateStarBars = () => {
    const { votes } = this.props
    const starsCount = this._getStarsCount()
    const total = this._getTotalSum()

    const starBarsList = []

    starsCount.forEach(key => {
      const currentStar = votes[key]

      let barWidth = 0
      let normalizedBarWidthToLayout = 0

      if (currentStar && total) {
        barWidth = Math.ceil((currentStar * MAX_PERCENTAGE) / total)
        normalizedBarWidthToLayout = (barWidth * MAX_BAR_WIDTH) / MAX_PERCENTAGE
      }

      const starBar = (
        <div key={key} className={styles.starBar}>
          <span className={styles.starCount}>{key} star</span>
          <div className={styles.starProgressWrap}>
            <span className={styles.starProgressBcg} />
            <span className={styles.starProgress} style={{ width: normalizedBarWidthToLayout }} />
          </div>
          <span className={styles.starPercentage}>{barWidth}%</span>
        </div>
      )

      starBarsList.push(starBar)
    })

    return starBarsList.reverse()
  }

  render() {
    const total = this._getTotalSum()
    const totalLabel = this._getTotalLabel(total)
    const normalizedVotes = this._normalizeVotes()

    const averageStars = normalizedVotes && total ? (this._normalizeVotes() / total).toFixed(1) : 0

    return (
      <div className={styles.votesWrap}>
        <div className={styles.totalVotes}>
          {total} {totalLabel}
        </div>
        <div className={styles.averageStars}>{averageStars} out of 5 stars</div>
        <div className={styles.starBarsWrap}>{this._generateStarBars()}</div>
      </div>
    )
  }
}

export default Votes
