import PropTypes from 'prop-types'
import React from 'react'
import classname from 'classnames'
import s from './styles.cssmodule.scss'

const CELL_SIZE = 60

export const Cell = ({ index, i, j, round, type, selected, clickHandler }) => {
  const c = classname({
    [s.cell]: true,
    [s[round]]: true,
    [s[round + '-' + type]]: true,
    [s.selected]: selected
  })
  return (
    <div
      onClick={() => clickHandler(index)}
      className={c}
      style={{ left: CELL_SIZE * i + 'px', top: CELL_SIZE * j + 'px' }}
    />
  )
}
Cell.propTypes = {
  index: PropTypes.number.isRequired,
  i: PropTypes.number,
  j: PropTypes.number,
  round: PropTypes.string,
  type: PropTypes.string,
  selected: PropTypes.bool,
  clickHandler: PropTypes.func
}

export default Cell
