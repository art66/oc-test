import React, { Component } from 'react'
import { List, Map } from 'immutable'
import * as _ from 'lodash'
import Cell from './Cell'
import s from './styles.cssmodule.scss'

const SIZE = 7
export class ItemMatch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cells: this._generateCellsMap()
    }
  }
  _generateCellsMap() {
    var cells = List()
    for (var i = 0; i < SIZE * SIZE; i++) {
      cells = cells.push(
        Map({
          type: _.random(1, 4),
          i: Math.floor(i / SIZE),
          j: i % SIZE,
          key: i
        })
      )
    }
    return cells
  }
  _clickHandler = index => {
    let { cells } = this.state
    const prevSelected = cells.find(item => item.get('selected'))
    const nowSelected = cells.get(index)
    if (prevSelected) {
      const i = nowSelected.get('i')
      const j = nowSelected.get('j')
      const previ = prevSelected.get('i')
      const prevj = prevSelected.get('j')
      if ((Math.abs(previ - i) === 1 && prevj === j) || (Math.abs(prevj - j) === 1 && previ === i)) {
        cells = cells.update(prevSelected.get('key'), cell =>
          cell
            .set('i', i)
            .set('j', j)
            .set('selected', false)
        )
        cells = cells.update(index, cell =>
          cell
            .set('i', previ)
            .set('j', prevj)
            .set('selected', false)
        )
      } else {
        cells = cells.updateIn([prevSelected.get('key'), 'selected'], selected => !selected)
        if (previ !== i && prevj !== j) {
          cells = cells.updateIn([index, 'selected'], selected => !selected)
        }
      }
    } else {
      cells = this.state.cells.updateIn([index, 'selected'], selected => !selected)
    }
    this.setState({
      cells
    })
  }
  _grid = () => {
    const { cells } = this.state
    const divs = []
    for (var i = 0; i < SIZE * SIZE; i++) {
      const c = cells.getIn([i])
      divs.push(
        <Cell
          key={c.get('key')}
          index={c.get('key')}
          i={c.get('i')}
          j={c.get('j')}
          clickHandler={this._clickHandler}
          round="star"
          selected={c.get('selected')}
          type={c.get('type')}
        />
      )
    }
    return divs
  }
  render() {
    return (
      <div className={s.bg}>
        <div className={s.board}>{this._grid()}</div>
      </div>
    )
  }
}

ItemMatch.propTypes = {}

export default ItemMatch
