import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import GameStartScreen from '../../GameStartScreen'
import ScoreBoard from '../../ScoreBoard'
import Board from './Board'
import CompleteLevelScreen from './CompleteLevel'
import { setGameTitle } from '../../../modules'
import reducer, {
  createLevel,
  createRandomFigure,
  clickOnFigure,
  setMovingBlockWidth,
  updateGameTime,
  showCompleteLevelScreen,
  setLevelResult,
  leaveGame
} from './modules'
import s from './styles.cssmodule.scss'

export { reducer }

export class SnowballShooter extends Component {
  componentWillUnmount() {
    this.props.leaveGame()
  }

  componentDidMount() {
    const { setGameTitle } = this.props

    setGameTitle({
      gameTitle: 'Snowball Shooter'
    })
  }

  render() {
    const {
      gameAccepted,
      gameEndResult,
      createLevel,
      createRandomFigure,
      snowballShooter,
      clickOnFigure,
      setMovingBlockWidth,
      movingBlockWidth,
      updateGameTime,
      setLevelResult,
      showCompleteLevelScreen
    } = this.props

    if (!gameAccepted) {
      return (
        <GameStartScreen
          message='Play Snowball Shooter?'
          yesTitle='YES'
          noTitle='NO'
          rules='Shoot the Grinch but avoid hitting Santa!'
        />
      )
    }

    if (snowballShooter.gameOver) {
      return <ScoreBoard gameName='SnowballShooter' title='Snowball Shooter high scores' {...gameEndResult} />
    }

    if (snowballShooter.completeLevelScreen) {
      return <CompleteLevelScreen level={snowballShooter.level} showCompleteLevelScreen={showCompleteLevelScreen} />
    }

    return (
      <div className={s['wrap']}>
        <Board
          updateGameTime={updateGameTime}
          createLevel={createLevel}
          snowballShooter={snowballShooter}
          clickOnFigure={clickOnFigure}
          setMovingBlockWidth={setMovingBlockWidth}
          movingBlockWidth={movingBlockWidth}
          createRandomFigure={createRandomFigure}
          setLevelResult={setLevelResult}
        />
      </div>
    )
  }
}

SnowballShooter.propTypes = {
  gameAccepted: PropTypes.bool,
  gameEndResult: PropTypes.object,
  credentials: PropTypes.object,
  snowballShooter: PropTypes.object,
  movingBlockWidth: PropTypes.number,
  setGameTitle: PropTypes.func,

  createLevel: PropTypes.func,
  createRandomFigure: PropTypes.func,
  setMovingBlockWidth: PropTypes.func,
  clickOnFigure: PropTypes.func,
  showCompleteLevelScreen: PropTypes.func,
  setLevelResult: PropTypes.func,
  updateGameTime: PropTypes.func,
  leaveGame: PropTypes.func
}

const mapStateToProps = state => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  snowballShooter: state.SnowballShooter
})

const mapActionsToProps = {
  createLevel,
  createRandomFigure,
  clickOnFigure,
  setMovingBlockWidth,
  updateGameTime,
  showCompleteLevelScreen,
  setLevelResult,
  setGameTitle,
  leaveGame
}

export default connect(mapStateToProps, mapActionsToProps)(SnowballShooter)
