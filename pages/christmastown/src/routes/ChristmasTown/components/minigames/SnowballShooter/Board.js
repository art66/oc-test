import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'
import classnames from 'classnames'
import SnowLayer from './SnowLayer'
import Figure from './Figure'

export class Board extends Component {
  constructor(props) {
    super(props)

    this.getMovingBlockWidth = this.getMovingBlockWidth.bind(this)
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  //   this.props.createLevel()
  // }

  componentDidMount() {
    this.props.createLevel()

    this.getMovingBlockWidth()
    window.addEventListener('resize', this.getMovingBlockWidth, false)
    this.runCountdown()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.getMovingBlockWidth, false)
    clearInterval(this.countdown)
  }

  getMovingBlockWidth() {
    let movingBlockWidth = this.refs.movingBlock.offsetWidth
    this.props.setMovingBlockWidth(movingBlockWidth)
  }

  runCountdown() {
    this.countdown = setInterval(() => {
      if (this.props.snowballShooter.time <= 0) {
        clearInterval(this.countdown)
        this.props.setLevelResult('lose')
      } else {
        this.props.updateGameTime(this.props.snowballShooter.time)
      }
    }, 1000)
  }

  render() {
    const c = classnames({
      [s['board']]: true,
      [s['pulse']]: this.props.snowballShooter.time <= 5
    })

    const { activeFigures, message, movingBlockWidth } = this.props.snowballShooter

    return (
      <div className={c} ref="board">
        <SnowLayer />

        <div className={s['moving-block']} ref="movingBlock">
          <Figure
            currentFigure={activeFigures[0]}
            position="top"
            clickOnFigure={this.props.clickOnFigure}
            movingBlockWidth={movingBlockWidth}
            createRandomFigure={this.props.createRandomFigure}
          />
          <Figure
            currentFigure={activeFigures[1]}
            position="middle"
            clickOnFigure={this.props.clickOnFigure}
            movingBlockWidth={movingBlockWidth}
            createRandomFigure={this.props.createRandomFigure}
          />
          <Figure
            currentFigure={activeFigures[2]}
            position="bottom"
            clickOnFigure={this.props.clickOnFigure}
            movingBlockWidth={movingBlockWidth}
            createRandomFigure={this.props.createRandomFigure}
          />
        </div>

        <div className={[s['message']]}>
          <h2 className={[s[message.type]]}>{message.text}</h2>
        </div>
      </div>
    )
  }
}

Board.propTypes = {
  createLevel: PropTypes.func,
  createRandomFigure: PropTypes.func,
  setMovingBlockWidth: PropTypes.func,
  snowballShooter: PropTypes.object,
  clickOnFigure: PropTypes.func,
  setLevelResult: PropTypes.func,
  updateGameTime: PropTypes.func
}

export default Board
