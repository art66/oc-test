export const SANTAS_COUNT = 9
export const GRINCH_COUNT = 6

// santa speed between sides (seconds)
const SANTA_MIN_SPEED = 2.5

const GRINCH_SPEED = {
  level1: 1.7,
  level2: 1.5,
  level3: 1.4,
  level4: 1.3,
  level5: 1.2
}

const GRINCH_CHANSE = 3 // 33% chanse

export function getRandomFigure(currentFigures, position, currentLevel) {
  let grinchAvailable = currentFigures.some(elem => elem.type === 'grinch')

  let figure
  if (!grinchAvailable) {
    let grinchChance = getRandomInteger(1, GRINCH_CHANSE)
    if (grinchChance === GRINCH_CHANSE) {
      figure = getGrinch(position, currentLevel)
    } else {
      figure = getSanta(currentFigures, position, currentLevel)
    }
  } else {
    figure = getSanta(currentFigures, position, currentLevel)
  }
  return figure
}

const getSanta = (currentFigures, position, currentLevel) => {
  let newSanta = false
  let santaIndex
  while (!newSanta) {
    santaIndex = getRandomInteger(1, SANTAS_COUNT)
    let santaAvailable = currentFigures.some(elem => elem.name === 'santa' + santaIndex)
    if (!santaAvailable) {
      newSanta = true
    }
  }

  let maxSpeed = GRINCH_SPEED['level' + currentLevel]
  return {
    name: 'santa' + santaIndex,
    type: 'santa',
    position,
    startSide: getRandomSide(),
    speed: getRandomFloat(maxSpeed, SANTA_MIN_SPEED)
  }
}

const getGrinch = (position, currentLevel) => {
  let grinchIndex = getRandomInteger(1, GRINCH_COUNT)
  return {
    name: 'grinch' + grinchIndex,
    type: 'grinch',
    position,
    startSide: getRandomSide(),
    speed: GRINCH_SPEED['level' + currentLevel]
  }
}

const getRandomSide = () => {
  let side = getRandomInteger(0, 1)
  return side === 0 ? 'left' : 'right'
}

const getRandomInteger = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

const getRandomFloat = (min, max) => {
  return Math.random() * (max - min) + min
}
