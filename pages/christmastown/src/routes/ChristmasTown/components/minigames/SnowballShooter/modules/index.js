import { getRandomFigure } from './helpers'
import { getEndResult, leaveGame as _leaveGame } from '../../../../modules'
import { convertTimeToPoints } from '../../../../utils'

// ------------------------------------
// Constants
// ------------------------------------
// const START_GAME = 'START_GAME'
const CREATE_LEVEL = 'CREATE_LEVEL'
const SET_FIGURE = 'SET_FIGURE'
const SET_GAME = 'SET_GAME'
const SET_MOVING_BLOCK_WIDTH = 'SET_MOVING_BLOCK_WIDTH'
const UPDATE_GAME_TIME = 'UPDATE_GAME_TIME'
const SHOW_COMPLETE_LEVEL_SCREEN = 'SHOW_COMPLETE_LEVEL_SCREEN'
const SET_LEVEL_RESULT = 'SET_LEVEL_RESULT'
const RESET_GAME = 'RESET_GAME'
const GAME_OVER = 'GAME_OVER'
// ------------------------------------
// Actions
// ------------------------------------
const _createLevel = level => ({
  type: CREATE_LEVEL,
  level
})

const _setFigure = newFigure => ({
  type: SET_FIGURE,
  newFigure
})

const _setGame = figureType => {
  return {
    type: SET_GAME,
    figureType
  }
}

export const setMovingBlockWidth = width => {
  return {
    type: SET_MOVING_BLOCK_WIDTH,
    width
  }
}

export const updateGameTime = time => {
  return {
    type: UPDATE_GAME_TIME,
    time
  }
}

export const showCompleteLevelScreen = state => {
  return {
    type: SHOW_COMPLETE_LEVEL_SCREEN,
    state
  }
}

const _setLevelResult = result => {
  return {
    type: SET_LEVEL_RESULT,
    result
  }
}

const _gameOver = () => {
  return {
    type: GAME_OVER
  }
}

const _resetGame = () => {
  return {
    type: RESET_GAME
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
export const ACTION_HANDLERS = {
  [CREATE_LEVEL]: (state, action) => {
    return {
      ...initialState,
      allTime: state.allTime,
      level: {
        number: action.level,
        complete: false,
        result: ''
      }
    }
  },
  [SET_MOVING_BLOCK_WIDTH]: (state, action) => {
    return {
      ...state,
      movingBlockWidth: action.width
    }
  },
  [SET_FIGURE]: (state, action) => {
    return {
      ...state,
      activeFigures: state.activeFigures.map(elem => {
        if (elem.position === action.newFigure.position) {
          elem = action.newFigure
        }
        return elem
      })
    }
  },
  [SET_GAME]: (state, action) => {
    if (action.figureType === 'grinch') {
      return {
        ...state,
        message: {
          text: 'Good shot! You got Grinch!',
          type: 'correct'
        }
      }
    } else if (action.figureType === 'santa') {
      return {
        ...state,
        message: {
          text: 'Oh no! That was Santa!',
          type: 'wrong'
        },
        time: state.time - 2
      }
    } else {
      return state
    }
  },
  [UPDATE_GAME_TIME]: state => {
    return {
      ...state,
      time: state.time - 1
    }
  },
  [SHOW_COMPLETE_LEVEL_SCREEN]: (state, action) => {
    return {
      ...state,
      completeLevelScreen: action.state
    }
  },
  [SET_LEVEL_RESULT]: (state, action) => {
    return {
      ...state,
      level: {
        ...state.level,
        complete: action.result === 'win'
      },
      time: 0,
      allTime: state.allTime + state.time
    }
  },
  [GAME_OVER]: state => {
    return {
      ...state,
      gameOver: true
    }
  },
  [RESET_GAME]: () => {
    return {
      ...initialState
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  level: {
    number: 0,
    complete: false,
    result: ''
  },
  activeFigures: [
    {
      position: 'top'
    },
    {
      position: 'middle'
    },
    {
      position: 'bottom'
    }
  ],
  time: 30,
  allTime: 0,
  message: {
    text: 'Shoot the Grinch but avoid hitting Santa!',
    type: 'welcome'
  },
  completeLevelScreen: false,
  gameOver: false
}

export default function snowballShooterReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// create new level
export const createLevel = () => (dispatch, getState) => {
  let currentLevel = getState().SnowballShooter.level.number
  currentLevel += 1

  dispatch(_createLevel(currentLevel))
}

export const createRandomFigure = newFigurePosition => (dispatch, getState) => {
  let currentLevel = getState().SnowballShooter.level.number
  let currentFigures = getState().SnowballShooter.activeFigures
  let newFigure = getRandomFigure(currentFigures, newFigurePosition, currentLevel)

  dispatch(_setFigure(newFigure))
  return newFigure
}

export const clickOnFigure = type => dispatch => {
  dispatch(_setGame(type))
  if (type === 'grinch') {
    dispatch(setLevelResult('win'))
  }
}

export const setLevelResult = result => (dispatch, getState) => {
  let level = getState().SnowballShooter.level.number
  dispatch(_setLevelResult(result))
  if (result === 'win') {
    if (level === 5) {
      dispatch(getPrize(5))
    } else {
      dispatch(showCompleteLevelScreen(true))
    }
  } else {
    dispatch(getPrize(level - 1))
  }
}

const getPrize = level => (dispatch, getState) => {
  dispatch(_gameOver())
  let allTime = getState().SnowballShooter.allTime
  let points = convertTimeToPoints(allTime, level)
  let data = {
    action: 'complete',
    gameType: 'gameSnowballShooter',
    result: {
      level,
      points
    }
  }
  dispatch(getEndResult(data))
}

export const leaveGame = () => dispatch => {
  dispatch(_resetGame())
  dispatch(_leaveGame())
}
