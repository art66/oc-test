import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'
// import classnames from 'classnames'
import * as images from './images.json'
import * as masks from './masks.json'

/* global minTabletSize, maxTabletSize */
export class SnowLayer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      windowSize: null
    }
    this.changeSettings = this.changeSettings.bind(this)
  }

  // deprecated since React v.16.5
  UNSAFE_componentWillMount() {
  // componentDidMount() {
    let windowSize = this.getWindowSize()
    this.updateSettings(windowSize)

    window.addEventListener('resize', this.changeSettings)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.changeSettings)
  }

  getWindowSize() {
    let size
    if (window.innerWidth <= minTabletSize) {
      size = 'mobile'
    } else if (window.innerWidth > minTabletSize && window.innerWidth < maxTabletSize) {
      size = 'tablet'
    } else if (window.innerWidth >= maxTabletSize) {
      size = 'desktop'
    }
    return size
  }

  changeSettings() {
    let windowSize = this.getWindowSize()
    if (windowSize !== this.state.windowSize) {
      this.updateSettings(windowSize)
    }
  }

  updateSettings(windowSize) {
    this.setState({
      windowSize: windowSize
    })
  }

  render() {
    if (!this.state.windowSize) {
      return null
    }

    const snowB = s['snow'] + ' ' + s['snow-bottom']
    const snowM = s['snow'] + ' ' + s['snow-middle']
    const snowT = s['snow'] + ' ' + s['snow-top']

    const snowBmask = masks[`snow-bottom-${this.state.windowSize}`]
    const snowMmask = masks[`snow-middle-${this.state.windowSize}`]
    const snowTmask = masks[`snow-top-${this.state.windowSize}`]

    const viewBoxB = `0 0 ${snowBmask.width} ${snowBmask.height}`
    const viewBoxM = `0 0 ${snowMmask.width} ${snowMmask.height}`
    const viewBoxT = `0 0 ${snowTmask.width} ${snowTmask.height}`

    return (
      <div className={s['snow-layer']}>
        <div className={snowT}>
          <img src={images[`snow-top-${this.state.windowSize}`]} alt="snow-top" draggable="false" />

          <svg viewBox={viewBoxT}>
            <path d={masks[`snow-top-${this.state.windowSize}`].data} />
          </svg>
        </div>

        <div className={snowM}>
          <img
            src={images[`snow-middle-${this.state.windowSize}`]}
            alt="snow-middle"
            draggable="false"
            data-type="snow"
          />

          <svg viewBox={viewBoxM}>
            <path d={masks[`snow-middle-${this.state.windowSize}`].data} />
          </svg>
        </div>

        <div className={snowB}>
          <img
            src={images[`snow-bottom-${this.state.windowSize}`]}
            alt="snow-bottom"
            draggable="false"
            data-type="snow"
          />

          <svg viewBox={viewBoxB}>
            <path d={masks[`snow-bottom-${this.state.windowSize}`].data} />
          </svg>
        </div>
      </div>
    )
  }
}

export default SnowLayer
