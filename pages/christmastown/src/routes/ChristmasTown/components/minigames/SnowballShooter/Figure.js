import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'
import * as images from './images.json'
import * as masks from './masks.json'

const SPRITE_STEPS = 12
const SPRITE_OFFSET = 6

export class Figure extends Component {
  constructor(props) {
    super(props)
    this.state = {
      spritePosition: 0,
      figurePosition: 0,
      selected: false
    }
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  componentDidMount() {
    this.setFigure()
  }

  componentWillUnmount() {
    this.stopUpdateFigureImage()
    clearInterval(this.animationFrame)
  }

  setFigure() {
    this.currentFigure = this.props.createRandomFigure(this.props.position)
    this.currentFigureMask = masks[this.currentFigure.name]
    this.setState({
      selected: false,
      figurePosition: 0,
      spritePosition: 0
    })
    this.moveFigure()
  }

  updateFigureImage() {
    this.currentPosition = 0
    this.step = 0

    this.updateImage()
    this.updateImageInterval = setInterval(
      this.updateImage.bind(this),
      this.currentFigure.speed * 1000 / (SPRITE_STEPS + SPRITE_OFFSET)
    )
  }

  updateImage() {
    if (this.step === SPRITE_STEPS) {
      this.stopUpdateFigureImage()
    } else {
      this.currentPosition -= this.currentFigureMask.width
      this.step++
    }
    this.setState({
      spritePosition: this.currentPosition
    })
  }

  stopUpdateFigureImage() {
    clearInterval(this.updateImageInterval)
    clearInterval(this.updateImageDelay)
    clearInterval(this.blinkInterval)
  }

  moveFigure() {
    this.stopUpdateFigureImage()

    this.updateImageDelay = setTimeout(() => {
      this.updateFigureImage()
    }, this.currentFigure.speed * 1000 / SPRITE_STEPS * (SPRITE_STEPS / SPRITE_OFFSET))

    let pos = 0
    this.animationFrame = setInterval(() => {
      let distance = this.props.movingBlockWidth - this.currentFigureMask.width
      pos += distance / (this.currentFigure.speed * 1000) * 16
      if (pos < distance) {
        this.setState({
          figurePosition: pos
        })
      } else {
        clearInterval(this.animationFrame)
        this.setFigure()
      }
    }, 16)
  }

  clickOnFigure(e) {
    e.stopPropagation()
    let type = e.target.getAttribute('data-type')
    this.stopUpdateFigureImage()
    if (!this.state.selected && type === 'santa') {
      this.props.clickOnFigure('santa')

      this.setState({
        selected: true
      })

      let blinkSteps = 5
      let spriteStep
      let currentPosition

      const blinkFigure = () => {
        if (blinkSteps) {
          if (blinkSteps % 2 === 0) {
            spriteStep = 0
          } else {
            spriteStep = 1
          }

          currentPosition = this.currentFigureMask.width * (SPRITE_STEPS + spriteStep)
          this.setState({
            spritePosition: -currentPosition
          })
          blinkSteps--
        } else {
          clearInterval(this.blinkInterval)
        }
      }

      this.blinkInterval = setInterval(() => {
        blinkFigure()
      }, 200)

      blinkFigure()
    } else if (type === 'grinch') {
      this.props.clickOnFigure('grinch')
    }
  }

  render() {
    if (!this.currentFigure) {
      return null
    }

    const { name, position, startSide } = this.currentFigure

    const figureClass = s['figure'] + ' ' + s[`figure-${position}`]

    const figureStyle = {
      width: this.currentFigureMask.width + 'px',
      height: this.currentFigureMask.height + 'px',
      backgroundImage: `url(${images[name]})`,
      backgroundPositionX: this.state.spritePosition + 'px',
      [startSide]: this.state.figurePosition + 'px'
    }

    const viewBox = `0 0 ${this.currentFigureMask.width} ${this.currentFigureMask.height}`

    return (
      <div
        className={figureClass}
        style={figureStyle}
        onMouseDown={e => {
          this.clickOnFigure(e)
        }}
      >
        <svg viewBox={viewBox}>
          <path data-type={this.currentFigureMask.type} d={this.currentFigureMask.data} />
        </svg>
      </div>
    )
  }
}

Figure.propTypes = {
  currentFigure: PropTypes.object,
  createRandomFigure: PropTypes.func,
  position: PropTypes.string,
  clickOnFigure: PropTypes.func,
  movingBlockWidth: PropTypes.number
}

export default Figure
