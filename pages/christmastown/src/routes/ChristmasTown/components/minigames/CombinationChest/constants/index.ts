export const ATTEMPTS_AMOUNT = 4
export const NUMBERS = [1, 2, 3, 4, 5, 6, 7, 8, 9]
export const CORRECT_NUMBER = 2
export const EXISTED_NUMBER = 1
export const WRONG_NUMBER = 0
export const SLIDERS = ['first', 'second', 'third']
export const GAME_TITLE = 'Combination Chest'
