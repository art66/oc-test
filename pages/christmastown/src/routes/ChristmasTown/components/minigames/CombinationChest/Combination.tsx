import React, { Component } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import 'react-dynamic-swiper/lib/styles.css'
import { Swiper, Slide } from 'react-dynamic-swiper'
import { resetCombination } from './modules'
import { ATTEMPTS_AMOUNT, NUMBERS, CORRECT_NUMBER, EXISTED_NUMBER, WRONG_NUMBER, SLIDERS } from './constants'
import s from './styles.cssmodule.scss'

interface IProps {
  ref: any
  lastCombination?: number[]
  check?: number[]
  attempt?: number
  resetCombination?: (slideIndex: number) => void
}

export class Combination extends Component<IProps> {
  swipers: {
    swiper0?: Swiper | null
    swiper1?: Swiper | null
    swiper2?: Swiper | null
  }

  init: boolean

  constructor(props: IProps) {
    super(props)
    this.swipers = {}
    this.init = false
    this.swipers = {
      swiper0: null,
      swiper1: null,
      swiper2: null
    }
  }

  componentDidUpdate() {
    const { lastCombination } = this.props

    if (this.init && lastCombination.length) {
      this.swipers.swiper0.slideTo(lastCombination[0])
      this.swipers.swiper1.slideTo(lastCombination[1])
      this.swipers.swiper2.slideTo(lastCombination[2])
      this.init = false
    }
  }

  getCombination = () => {
    const { attempt } = this.props

    if (attempt && attempt + 1 === ATTEMPTS_AMOUNT) {
      this.swipers.swiper0.detachEvents()
      this.swipers.swiper1.detachEvents()
      this.swipers.swiper2.detachEvents()
    }
    return [this.swipers.swiper0.activeIndex, this.swipers.swiper1.activeIndex, this.swipers.swiper2.activeIndex].map(
      value => {
        if (value === 10) {
          return 1
        }
        if (value === 0) {
          return 9
        }
        return value
      }
    )
  }

  getOverlayClass = (slideIndex: number) => {
    const { check } = this.props

    return classnames({
      [s['overlay']]: true,
      [s['correct']]: check && check[slideIndex] === CORRECT_NUMBER,
      [s['exist']]: check && check[slideIndex] === EXISTED_NUMBER,
      [s['wrong']]: check && check[slideIndex] === WRONG_NUMBER
    })
  }

  getSwiperSlide = () => {
    return NUMBERS.map((value, i) => {
      return (
        <Slide className={s['slide']} key={i}>
          <div className={s['number']}>{value}</div>
        </Slide>
      )
    })
  }

  getCombinationSlides = () => {
    const options = {
      slidesPerView: 1,
      direction: 'vertical',
      loop: true
    }

    return SLIDERS.map((slider, slideIndex) => {
      const overlayClass = this.getOverlayClass(slideIndex)

      return (
        <div className={s['number-block']} key={slideIndex}>
          <div className={overlayClass} />
          <Swiper
            onInitSwiper={swiper => this.updateSlides(slideIndex, swiper)}
            onTouchMove={() => this.handleResetCombination(slideIndex)}
            onSlideChangeStart={() => this.handleResetCombination(slideIndex)}
            swiperOptions={options}
            pagination={false}
            navigation={false}
            wrapperClassName={`${s['swiper']} ${slider}`}
            onTap={() => this.clickNumber(slideIndex)}
          >
            {this.getSwiperSlide()}
          </Swiper>
        </div>
      )
    })
  }

  handleResetCombination = (slideIndex: number) => {
    if (!this.init) {
      this.props.resetCombination(slideIndex)
    }
  }

  clickNumber = (slideIndex: number) => {
    const currSlider = this.swipers[`swiper${slideIndex}`]

    currSlider.slideNext()
  }

  updateSlides = (index: number, swiper: Swiper) => {
    this.swipers[`swiper${index}`] = swiper
    swiper.slideTo(index + 1)
  }

  render() {
    return <div className={s['combination']}>{this.getCombinationSlides()}</div>
  }
}

export default connect(
  (state: any) => ({
    attempt: state.CombinationChest.attempt,
    check: state.CombinationChest.check,
    lastCombination: state.CombinationChest.lastCombination
  }),
  {
    resetCombination
  },
  null,
  { forwardRef: true }
)(Combination)
