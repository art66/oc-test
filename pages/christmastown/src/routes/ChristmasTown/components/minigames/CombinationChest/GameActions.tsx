import React, { Component } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import PrizesList from '../../StatusArea/PrizesList'
import { leaveGame, fetchMapData } from '../../../modules'
import { resetChest } from './modules'
import { ATTEMPTS_AMOUNT } from './constants'
import s from './styles.cssmodule.scss'

interface IProps {
  attempt?: number
  message?: string
  opened?: boolean
  prizes?: { type: string; name: string }[]
  isChangedCombination: boolean
  leaveGame?: () => void
  fetchMapData?: () => void
  resetChest?: () => void
  unlockChest: () => void
}

class GameActions extends Component<IProps> {
  leaveGame = () => {
    this.props.fetchMapData()
    this.props.resetChest()
    this.props.leaveGame()
  }

  getUnlockButton = () => {
    const { attempt, opened, unlockChest, isChangedCombination } = this.props

    return !opened ? (
      <button
        type='button'
        className={`btn-blue ${attempt === ATTEMPTS_AMOUNT || !isChangedCombination ? 'locked' : ''}`}
        onClick={unlockChest}
        disabled={attempt === ATTEMPTS_AMOUNT || !isChangedCombination}
      >
        UNLOCK
      </button>
    ) : null
  }

  render() {
    const { message, prizes, opened } = this.props
    const messageClass = classnames({
      [s['message']]: true,
      [s['opened']]: opened
    })

    return (
      <div className={s['game-actions']}>
        {prizes ? <PrizesList prizes={prizes} /> : ''}
        <p className={messageClass} dangerouslySetInnerHTML={{ __html: message }} />
        <div className={s['buttons']}>
          {this.getUnlockButton()}
          <button type='button' className='btn-blue' onClick={this.leaveGame}>
            LEAVE
          </button>
        </div>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    attempt: state.CombinationChest.attempt,
    message: state.CombinationChest.message,
    prizes: state.CombinationChest.prizes,
    opened: state.CombinationChest.opened,
    isChangedCombination: state.CombinationChest.isChangedCombination
  }),
  {
    leaveGame,
    fetchMapData,
    resetChest
  }
)(GameActions)
