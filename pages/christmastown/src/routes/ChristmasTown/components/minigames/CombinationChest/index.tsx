import React, { Component } from 'react'
import { connect } from 'react-redux'
import Board from './Board'
import reducer, { initChest } from './modules'
import { setGameTitle } from '../../../modules'
import { GAME_TITLE } from './constants'
import s from './styles.cssmodule.scss'

export { reducer }

interface IProps {
  initChest: () => void
  setGameTitle: (param: { gameTitle: string }) => void
}

export class CombinationChest extends Component<IProps> {
  componentDidMount() {
    this.props.initChest()
    this.props.setGameTitle({
      gameTitle: GAME_TITLE
    })
  }

  render() {
    const isMobileMode = document.body.classList.contains('d') && document.body.classList.contains('r')

    return (
      <div className={s['wrap']}>
        <Board isMobileMode={isMobileMode} />
      </div>
    )
  }
}

const mapStateToProps = () => ({})

const mapActionsToProps = {
  setGameTitle,
  initChest
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CombinationChest)
