import React, { Component } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import Combination from './Combination'
import GameActions from './GameActions'
import { tryOpenChest } from './modules'
import { ATTEMPTS_AMOUNT } from './constants'
import s from './styles.cssmodule.scss'

interface IProps {
  mediaType?: string
  attempt?: number
  opened?: boolean
  isMobileMode: boolean
  tryOpenChest?: (combination: number[]) => void
}

interface IState {
  combination: number[]
  clicks: number
}

export class Board extends Component<IProps, IState> {
  combination: any

  constructor(props: IProps) {
    super(props)
    this.state = {
      combination: [],
      clicks: 0
    }
  }

  unlockChest = () => {
    const { clicks } = this.state
    const { attempt } = this.props
    const combination = this.combination.getCombination()

    // quick fix for sending double requests
    if (!clicks && clicks !== attempt) {
      this.setState({
        clicks: attempt + 1
      })
    } else {
      this.setState({
        clicks: clicks + 1
      })
    }

    if (clicks < ATTEMPTS_AMOUNT) {
      this.props.tryOpenChest(combination)
    }
  }

  render() {
    const { mediaType, opened, isMobileMode } = this.props
    const boardClass = classnames({
      [s['board']]: true,
      [s[mediaType]]: true,
      [s['mobile-mode']]: isMobileMode
    })
    const chestClass = classnames({
      [s['chest']]: true,
      [s['opened']]: opened,
      [s[mediaType]]: true
    })

    return (
      <div className={boardClass}>
        <div className={chestClass}>
          <Combination ref={el => (this.combination = el)} />
        </div>
        <GameActions unlockChest={this.unlockChest} />
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    attempt: state.CombinationChest.attempt,
    mediaType: state.browser.mediaType,
    opened: state.CombinationChest.opened
  }),
  {
    tryOpenChest
  }
)(Board)
