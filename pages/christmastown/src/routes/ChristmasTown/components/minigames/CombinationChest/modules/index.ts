import { fetchUrl, getErrorMessage } from '../../../../utils'
import { debugShow } from '../../../../../../controller/actions'
import IAction from '../../../../../../interfaces/IAction'

const INIT_CHEST = 'INIT_CHEST'
const SET_CHEST = 'SET_CHEST'
const RESET_CHEST = 'RESET_CHEST'
const RESET_COMBINATION = 'RESET_COMBINATION'

export const initChestInStore = data => ({
  type: INIT_CHEST,
  data
})

export const setChest = data => ({
  type: SET_CHEST,
  data
})

export const resetChest = () => ({
  type: RESET_CHEST
})

export const resetCombination = slideIndex => ({
  type: RESET_COMBINATION,
  slideIndex
})

const initialState = {
  message: '',
  check: [null, null, null],
  lastCombination: [],
  opened: false,
  isChangedCombination: true
}

export const ACTION_HANDLERS = {
  [INIT_CHEST]: (_, action) => {
    return {
      ...initialState,
      message: action.data.message,
      check: action.data.check.length ? action.data.check : initialState.check,
      lastCombination: action.data.lastCombination,
      attempt: action.data.attempt
    }
  },
  [SET_CHEST]: (state, action) => ({
    ...state,
    ...action.data
  }),
  [RESET_CHEST]: () => ({
    ...initialState
  }),
  [RESET_COMBINATION]: (state, action) => {
    return {
      ...state,
      check: state.check.map((value, index) => {
        if (index === action.slideIndex) {
          return null
        }
        return value
      }),
      isChangedCombination: true
    }
  }
}

export default function CombinationChestReducer(state: any = initialState, action: IAction<any>) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export const initChest = () => dispatch => {
  dispatch(getInitialData())
}

const getInitialData = () => dispatch => {
  fetchUrl('miniGameAction', {
    action: 'start',
    gameType: 'gameCombinationChest'
  })
    .then(result => {
      dispatch(initChestInStore(result))
    })
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}

export const tryOpenChest = combination => dispatch => {
  fetchUrl('miniGameAction', {
    action: 'complete',
    gameType: 'gameCombinationChest',
    result: {
      combination
    }
  })
    .then(result => {
      let data

      if (result && result.success) {
        data = {
          check: [2, 2, 2],
          message: result.message,
          opened: true,
          prizes: result.prizes
        }
      } else {
        data = {
          check: result.check,
          message: result.message,
          attempt: result.attempt,
          opened: false,
          isChangedCombination: false
        }
      }
      dispatch(setChest(data))
    })
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}
