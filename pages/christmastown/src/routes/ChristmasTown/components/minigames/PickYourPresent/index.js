/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/control-has-associated-label */
import classnames from 'classnames'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { AnimationDuration } from '../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import { getEndResult, setGameTitle } from '../../../modules'
import GameStartScreen from '../../GameStartScreen'
import ScoreBoard from '../../ScoreBoard'
import { AREAS, POSITIONS } from './constants'
import styles from '../../../hoc/withMinigameWrapper/minigameWrapper.cssmodule.scss'
import slideAnimation from '../../../hoc/withMinigameWrapper/slideAnimation.cssmodule.scss'
import s from './styles.cssmodule.scss'

export class PickYourPresent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    this.props.setGameTitle({
      gameTitle: 'Pick your present'
    })
  }

  _items = () => {
    const { mediaType } = this.props
    const { hoveredItem } = this.state

    return POSITIONS[mediaType].map((pos, i) => {
      const c = classnames({
        [s['item']]: true,
        [s['hover']]: hoveredItem === i
      })
      const filename = `present_${i + 1}${mediaType !== 'desktop' ? '_m' : ''}.png`

      return (
        <div key={i} className={c} style={{ left: pos[0], top: pos[1] }}>
          <img src={`/images/v2/christmas_town/minigames/present_pick/${filename}`} />
        </div>
      )
    })
  }

  _presentHover = (i) => {
    this.setState({
      hoveredItem: i
    })
  }

  _mouseLeave = () => {
    this.setState({
      hoveredItem: null
    })
  }

  _pickAPresent = (i) => (event) => {
    event.preventDefault()
    if (this.presentPiccked) return
    this.presentPiccked = true
    this.props.getEndResult({
      action: 'complete',
      gameType: 'gamePYPresent',
      result: {
        pickedItem: i
      }
    })
  }

  selectComponent() {
    const { gameAccepted, gameEndResult, mediaType } = this.props

    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen
            message={'Pick out a present? It\'s on us!'}
            yesTitle='UH, OKAY'
            noTitle='THANKS BUT NO THANKS'
          />
        </CSSTransition>
      )
    }
    if (gameEndResult) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName='PickYourPresent' title='Pick your present high scores' {...gameEndResult} />
        </CSSTransition>
      )
    }
    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='Board'>
        <div className={s['wrap']}>
          <img
            className={s['main-img']}
            src={`/images/v2/christmas_town/minigames/present_pick/present_pick_bg_${mediaType}.jpg`}
            useMap='#image-map'
            alt=''
          />
          <div className={s['items']}>{this._items()}</div>
          <map name='image-map'>{this.renderArea(AREAS[mediaType])}</map>
        </div>
      </CSSTransition>
    )
  }

  renderArea = (areas) =>
    areas.map((area, i) => (
      <area
        key={i}
        onClick={this._pickAPresent(i)}
        onMouseLeave={this._mouseLeave}
        onMouseEnter={() => this._presentHover(i)}
        href=''
        coords={area.join(',')}
        shape='poly'
      />
    ))

  render() {
    return <TransitionGroup className={styles.ctMiniGameWrapper}>{this.selectComponent()}</TransitionGroup>
  }
}

PickYourPresent.propTypes = {
  setGameTitle: PropTypes.func,
  mediaType: PropTypes.string,
  gameAccepted: PropTypes.bool,
  getEndResult: PropTypes.func,
  gameEndResult: PropTypes.object,
  statusArea: PropTypes.object
}

const mapStateToProps = (state) => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  mediaType: state.browser.mediaType,
  statusArea: state.christmastown.statusArea
})

const mapActionsToProps = {
  setGameTitle,
  getEndResult
}

export default connect(mapStateToProps, mapActionsToProps)(PickYourPresent)
