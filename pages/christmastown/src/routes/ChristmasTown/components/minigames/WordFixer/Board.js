/* eslint-disable @typescript-eslint/tslint/config */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { arrayMove, SortableContainer, SortableElement } from 'react-sortable-hoc'
import s from './styles.cssmodule.scss'

const Letter = SortableElement(({ value }) => <li>{value}</li>)

const renderLetter = (value, index, word) => (
  <Letter helperClass={s[`draggable-item-${word.length - 2}`]} key={`item-${index}`} index={index} value={value} />
)

const Word = SortableContainer(({ word }) => {
  return <ul className={s['lists-draggable']}>{word.map(renderLetter)}</ul>
})

export class Board extends Component {
  constructor(props) {
    super(props)
    this.onBoard = true
    this.letterMove = false
    this.onSortEnd = this.onSortEnd.bind(this)
    this.onSortMove = this.onSortMove.bind(this)
  }

  componentDidMount() {
    this.props.createLevel()

    this.runCountdown()
  }

  componentWillUnmount() {
    clearInterval(this.countdown)
    this.onBoard = false
  }

  onSortEnd({ oldIndex, newIndex }) {
    if (this.letterMove) {
      this.props.checkWord(arrayMove(this.props.wordFixer.level.word, oldIndex, newIndex))
    }
  }

  onSortMove() {
    this.letterMove = true
    if (!this.onBoard) {
      this.letterMove = false
    }
  }

  runCountdown() {
    this.countdown = setInterval(() => {
      if (this.props.wordFixer.time <= 0) {
        clearInterval(this.countdown)
        this.props.setLevelResult('lose')
      } else {
        this.props.updateGameTime(this.props.wordFixer.time)
      }
    }, 1000)
  }

  render() {
    const { word } = this.props.wordFixer.level

    return (
      <div className={s.gameBoardWrapper}>
        <div className={`${s['game-board']} ${s[`level-${word.length - 2}`]}`}>
          <ul className={s['lists-background']}>
            {word.map((_, index) => (
              <li key={index} />
            ))}
          </ul>
          <Word
            word={word}
            onSortEnd={this.onSortEnd}
            onSortMove={this.onSortMove}
            axis="x"
            lockAxis="x"
            lockToContainerEdges
            helperClass={`${s['draggable-item']} ${s[`letters-${word.length}`]}`}
          />
          <div className={s['message']}>
            <h2 className={s['info-message']}>Drag and drop letters to arrange them into a word.</h2>
          </div>
        </div>
      </div>
    )
  }
}

Board.propTypes = {
  createLevel: PropTypes.func,
  word: PropTypes.array,
  wordFixer: PropTypes.object,
  checkWord: PropTypes.func,
  setLevelResult: PropTypes.func,
  updateGameTime: PropTypes.func
}

export default Board
