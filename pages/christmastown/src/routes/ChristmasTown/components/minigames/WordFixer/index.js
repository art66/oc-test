import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { AnimationDuration } from '../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import * as ctGlobalModule from '../../../modules'
import GameStartScreen from '../../GameStartScreen'
import ScoreBoard from '../../ScoreBoard'
import Board from './Board'
import CompleteLevelScreen from './CompleteLevel'
import * as modules from './modules'
import styles from '../../../hoc/withMinigameWrapper/minigameWrapper.cssmodule.scss'
import slideAnimation from '../../../hoc/withMinigameWrapper/slideAnimation.cssmodule.scss'

export const reducer = modules.default

export class WordFixer extends Component {
  componentDidMount() {
    const { setGameTitle } = this.props

    setGameTitle({
      gameTitle: 'Word Fixer'
    })
  }

  componentWillUnmount() {
    this.props.resetGame()
  }

  selectComponent() {
    const {
      gameAccepted,
      gameEndResult,
      createLevel,
      wordFixer,
      checkWord,
      showCompleteLevelScreen,
      updateGameTime,
      setLevelResult
    } = this.props

    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen
            message='Play Word Fixer?'
            yesTitle='YES'
            noTitle='NO'
            rules='Drag and drop letters to arrange them into a word.'
          />
        </CSSTransition>
      )
    }

    if (wordFixer.gameOver) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName='WordFixer' title='Word Fixer high scores' {...gameEndResult} />
        </CSSTransition>
      )
    }

    if (wordFixer.completeLevelScreen) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='CompleteLevelScreen'>
          <CompleteLevelScreen level={wordFixer.level} showCompleteLevelScreen={showCompleteLevelScreen} />
        </CSSTransition>
      )
    }

    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='Board'>
        <Board
          createLevel={createLevel}
          wordFixer={wordFixer}
          checkWord={checkWord}
          updateGameTime={updateGameTime}
          setLevelResult={setLevelResult}
        />
      </CSSTransition>
    )
  }

  render() {
    return <TransitionGroup className={styles.ctMiniGameWrapper}>{this.selectComponent()}</TransitionGroup>
  }
}

WordFixer.propTypes = {
  gameAccepted: PropTypes.bool,
  gameEndResult: PropTypes.object,
  setGameTitle: PropTypes.func,
  createLevel: PropTypes.func,
  wordFixer: PropTypes.object,
  checkWord: PropTypes.func,
  resetGame: PropTypes.func,
  showCompleteLevelScreen: PropTypes.func,
  updateGameTime: PropTypes.func,
  setLevelResult: PropTypes.func
}

const mapStateToProps = state => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  wordFixer: state.WordFixer
})

const mapActionsToProps = {
  setGameTitle: ctGlobalModule.setGameTitle,
  setLevelResult: modules.setLevelResult,
  createLevel: modules.createLevel,
  checkWord: modules.checkWord,
  showCompleteLevelScreen: modules.showCompleteLevelScreen,
  resetGame: modules.resetGame,
  updateGameTime: modules.updateGameTime
}

export default connect(mapStateToProps, mapActionsToProps)(WordFixer)
