import { getEndResult } from '../../../../modules'
import { fetchUrl, convertTimeToPoints, getErrorMessage } from '../../../../utils'
import { setWord } from '../../Hangman/modules/index'
import { debugShow } from '../../../../../../controller/actions'

export const CREATE_GAMEBOARD = 'CREATE_GAMEBOARD'
const SET_LEVEL_RESULT = 'SET_LEVEL_RESULT'
const SHOW_COMPLETE_LEVEL_SCREEN = 'SHOW_COMPLETE_LEVEL_SCREEN'
const UPDATE_GAME_TIME = 'UPDATE_GAME_TIME'
const RESET_GAME = 'RESET_GAME'
const GAME_OVER = 'GAME_OVER'
const SET_WORD = 'SET_WORD'

const createGameBoard = (level) => ({
  type: CREATE_GAMEBOARD,
  level
})

const saveLevelResult = (result) => ({
  type: SET_LEVEL_RESULT,
  result
})

export const updateGameTime = (time) => ({
  type: UPDATE_GAME_TIME,
  time
})

export const showCompleteLevelScreen = (state) => ({
  type: SHOW_COMPLETE_LEVEL_SCREEN,
  state
})

export const resetGame = () => ({
  type: RESET_GAME
})

const gameOver = () => ({
  type: GAME_OVER
})

export const ACTION_HANDLERS = {
  [CREATE_GAMEBOARD]: (state, action) => ({
    ...initialState,
    level: {
      ...state.level,
      number: action.level
    },
    allTime: state.allTime
  }),
  [SET_WORD]: (state, action) => ({
    ...state,
    level: {
      ...state.level,
      word: action.word
    }
  }),
  [SET_LEVEL_RESULT]: (state, action) => ({
    ...state,
    level: {
      ...state.level,
      complete: action.result === 'win'
    },
    allTime: state.allTime + state.time
  }),
  [UPDATE_GAME_TIME]: (state) => ({
    ...state,
    time: state.time - 1
  }),
  [SHOW_COMPLETE_LEVEL_SCREEN]: (state, action) => ({
    ...state,
    completeLevelScreen: action.state
  }),
  [RESET_GAME]: () => ({
    ...initialState
  }),
  [GAME_OVER]: (state) => ({
    ...state,
    gameOver: true
  })
}

const initialState = {
  level: {
    number: 0,
    completed: false,
    result: '',
    word: []
  },
  maxLevel: 8,
  time: 40,
  allTime: 0,
  completeLevelScreen: false,
  gameOver: false
}

export default function WordFixerReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export const createLevel = () => (dispatch, getState) => {
  let currentLevel = getState().WordFixer.level.number

  if (currentLevel === 0) {
    dispatch(getWord())
  }
  currentLevel += 1
  dispatch(createGameBoard(currentLevel))
}

const getWord = () => (dispatch) => {
  fetchUrl('miniGameAction', {
    action: 'start',
    gameType: 'gameWordFixer'
  })
    .then((result) => {
      const word = result.progress.word.toUpperCase()
      const resultWord = word.split('')

      dispatch(setWord(resultWord))
    })
    .catch((error) => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}

export const checkWord = (word) => (dispatch, getState) => {
  if (!getState().WordFixer.completeLevelScreen) {
    dispatch(setWord(word))
  }

  fetchUrl('miniGameAction', {
    action: 'completeLevel',
    gameType: 'gameWordFixer',
    result: {
      word: word.join('').toLowerCase()
    }
  })
    .then((result) => {
      if (!result.finished && result.success) {
        const nextWord = result.progress.word.toUpperCase().split('')

        dispatch(saveLevelResult('win'))
        dispatch(showCompleteLevelScreen(true))
        dispatch(setWord(nextWord))
      } else if (result.finished && result.success) {
        dispatch(saveLevelResult('win'))
        dispatch(getPrize(8))
      }
    })
    .catch((error) => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}

const getPrize = (level) => (dispatch, getState) => {
  dispatch(gameOver())
  const { allTime } = getState().WordFixer
  const points = convertTimeToPoints(allTime, level)
  const data = {
    action: 'complete',
    gameType: 'gameWordFixer',
    result: {
      level,
      points
    }
  }

  dispatch(getEndResult(data))
}

export const setLevelResult = (result) => (dispatch, getState) => {
  const currentLevel = getState().WordFixer.level.number

  dispatch(saveLevelResult(result))
  dispatch(getPrize(currentLevel - 1))
}
