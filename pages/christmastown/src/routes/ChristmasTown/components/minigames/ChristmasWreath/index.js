import random from 'lodash/random'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import styles from '../../../hoc/withMinigameWrapper/minigameWrapper.cssmodule.scss'
import slideAnimation from '../../../hoc/withMinigameWrapper/slideAnimation.cssmodule.scss'
import { AnimationDuration } from '../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import { getEndResult, setGameControls, setGameTitle } from '../../../modules'
import GameStartScreen from '../../GameStartScreen'
import ScoreBoard from '../../ScoreBoard'
import GameControls from './GameControls'
import Item from './Item'
import s from './styles.cssmodule.scss'

const WREATH_RADIUS = 152

export class ChristmasWreath extends Component {
  constructor(props) {
    super(props)
    this.state = {
      timeLeft: 60,
      removedItems: {}
    }
    this.foundItems = []
    this.positions = this._getRandomPositions()
  }

  _getRandomPositions = () => {
    const positions = []

    for (let i = 0; i < 42; i++) {
      const r = random(45, WREATH_RADIUS - 25)
      const a = random(0, 360)
      const x = Math.round(WREATH_RADIUS + Math.cos((a * 2 * Math.PI) / 360) * r) - 10
      const y = Math.round(WREATH_RADIUS + Math.sin((a * 2 * Math.PI) / 360) * r) - 10

      positions.push([x, y])
    }
    return positions
  }

  componentDidMount() {
    const { setGameTitle } = this.props

    setGameTitle({
      gameTitle: 'Christmas Wreath'
    })
  }

  componentDidUpdate() {
    const { gameAccepted, getEndResult: newGameEndResult } = this.props

    if (gameAccepted && !this.timer) {
      this.timer = setInterval(() => {
        const { timeLeft } = this.state

        if (timeLeft <= 1) {
          clearInterval(this.timer)
          newGameEndResult({
            action: 'complete',
            gameType: 'gameChristmasWreath',
            result: {
              foundItems: this.foundItems.length
            }
          })
        }

        this.setState({
          timeLeft: timeLeft - 1
        })
      }, 1000)
    }
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps({ gameAccepted }) {
  //   const { getEndResult } = this.props
  //   if (gameAccepted && !this.timer) {
  //     this.timer = setInterval(() => {
  //       const { timeLeft } = this.state
  //       if (timeLeft <= 1) {
  //         clearInterval(this.timer)
  //         getEndResult({
  //           action: 'complete',
  //           gameType: 'gameChristmasWreath',
  //           result: {
  //             foundItems: this.foundItems.length
  //           }
  //         })
  //       }
  //       this.setState({
  //         timeLeft: timeLeft - 1
  //       })
  //     }, 1000)
  //   }
  // }
  componentWillUnmount() {
    clearInterval(this.timer)
  }

  _items = () => {
    const { removedItems } = this.state
    const items = []

    for (let i = 0; i < this.positions.length; i++) {
      items.push(
        <Item
          key={i}
          index={i}
          top={this.positions[i][0]}
          left={this.positions[i][1]}
          removed={removedItems[i]}
          foundItem={this._foundItem}
        />
      )
    }
    return items
  }

  _foundItem = i => {
    const { setGameControls } = this.props
    const { timeLeft } = this.state

    if (timeLeft <= 0) return
    setTimeout(() => {
      if (i < this.positions.length) {
        this.setState({
          removedItems: {
            ...this.state.removedItems,
            [i]: true
          }
        })
      }
    }, 1200)
    this.foundItems.indexOf(i) === -1 && this.foundItems.push(i)
    const text = `Found ${this.foundItems.length} ornament${this.foundItems.length > 1 ? 's' : ''}`

    setGameControls({
      gameControls: <GameControls>{text}</GameControls>
    })
    return true
  }

  _missClick = event => {
    event.preventDefault()
    const { timeLeft } = this.state

    if (timeLeft <= 4) return
    this.setState({
      timeLeft: timeLeft - 3,
      penalty: true
    })
    clearTimeout(this.penaltyInProgress)
    this.penaltyInProgress = setTimeout(
      () =>
        this.setState({
          penalty: false
        }),
      400
    )
  }

  selectComponent() {
    const { gameAccepted, gameEndResult } = this.props
    const { timeLeft, penalty } = this.state

    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen
            message='Feel like playing Christmas Wreath?'
            yesTitle={['SOMEWHAT', 'PERHAPS?']}
            noTitle='NOPE'
          />
        </CSSTransition>
      )
    }
    if (gameEndResult) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName='ChristmasWreath' title='Christmas Wreath high scores' {...gameEndResult} />
        </CSSTransition>
      )
    }
    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='Board'>
        <div className={s['christmas-wreath']}>
          <div className={s['time']}>
            <span>{timeLeft}</span>
            {penalty && <div className={s['penalty']}>-3</div>}
          </div>
          <div className={s['wreath']}>
            <img
              src='/images/v2/christmas_town/minigames/christams_wreath/wreath.png'
              alt='christmas wreath'
              useMap='#Map'
            />
            {this._items()}
          </div>
          <map name='Map'>
            <area
              onClick={this._missClick}
              href='#'
              shape='poly'
              coords='132,126,147,124,150,132,159,131,167,131,170,145,184,141,186,134,
            180,152,170,172,155,179,138,183,129,182,133,172,131,168,113,168,112,157,
            105,147,111,130,127,124,142,21,119,2,113,26,92,23,77,29,65,47,52,61,37,59,
            36,78,28,91,18,91,14,108,16,123,5,121,11,135,5,145,11,157,10,170,14,181,29,
            192,24,207,41,204,46,213,51,247,68,248,79,240,84,259,80,275,93,273,108,273,
            115,275,130,273,133,271,143,275,136,292,162,297,162,285,183,293,194,270,211,
            268,217,259,233,263,234,244,255,239,252,211,258,205,269,219,275,187,279,176,
            276,168,275,155,281,153,290,133,285,123,283,104,264,96,259,78,249,61,243,53,
            230,45,219,38,208,29,191,27,175,21,165,11,156,14,153,11,146,17'
            />
          </map>
          <div className={s['message']}>Find as many ornaments as quickly as you can</div>
        </div>
      </CSSTransition>
    )
  }

  render() {
    return <TransitionGroup className={styles.ctMiniGameWrapper}>{this.selectComponent()}</TransitionGroup>
  }
}

ChristmasWreath.propTypes = {
  gameEndResult: PropTypes.object,
  setGameTitle: PropTypes.func,
  setGameControls: PropTypes.func,
  getEndResult: PropTypes.func,
  gameAccepted: PropTypes.bool,
  statusArea: PropTypes.object
}

const mapStateToProps = state => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  statusArea: state.christmastown.statusArea
})

const mapActionsToProps = {
  setGameTitle,
  setGameControls,
  getEndResult
}

export default connect(mapStateToProps, mapActionsToProps)(ChristmasWreath)
