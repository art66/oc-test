import PropTypes from 'prop-types'
import React, { Component } from 'react'
import s from './styles.cssmodule.scss'

export class Item extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  _handleClick = event => {
    event.stopPropagation()
    const { foundItem, index } = this.props
    if (foundItem(index)) {
      this.setState({
        enlarge: true
      })
    }
  }
  render() {
    const { index, top, left, removed } = this.props
    if (removed) return null
    const { enlarge } = this.state
    var enlargeDirection = 'enlarge-direction-' + (top > 150 ? 'n' : 's') + (left > 150 ? 'w' : 'e')
    return (
      <div className={s['item']} style={{ top, left }} onClick={this._handleClick}>
        <img
          className={enlarge ? s['hide'] : ''}
          src={'/images/v2/christmas_town/minigames/christams_wreath/items/20x20_toy_' + (index + 1) + '.png'}
        />
        {enlarge && (
          <div className={s['enlarged-wrap'] + ' ' + s[enlargeDirection]}>
            <div className={s['cone']} />
            <div className={s['enlarged-bg']}>
              <img
                className={s['enlarged-image']}
                src={'/images/v2/christmas_town/minigames/christams_wreath/items/70x70_toy_' + (index + 1) + '.png'}
              />
            </div>
          </div>
        )}
      </div>
    )
  }
}

Item.propTypes = {
  index: PropTypes.number,
  top: PropTypes.number,
  left: PropTypes.number,
  removed: PropTypes.bool,
  foundItem: PropTypes.func
}

export default Item
