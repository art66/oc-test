import PropTypes from 'prop-types'
import React, { Component } from 'react'

export class GameControls extends Component {
  render() {
    return <div>{this.props.children}</div>
  }
}

GameControls.propTypes = {
  children: PropTypes.node
}

export default GameControls
