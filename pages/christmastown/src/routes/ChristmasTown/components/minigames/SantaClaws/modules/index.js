import { createCells, grumpyCatAvailable } from './helpers'
import { getEndResult } from '../../../../modules'
import { convertTimeToPoints } from '../../../../utils'
// ------------------------------------
// Constants
// ------------------------------------
// const START_GAME = 'START_GAME'
const CREATE_LEVEL = 'CREATE_LEVEL'
const CHECK_CELL = 'CHECK_CELL'
const SET_GAME = 'SET_GAME'
const SET_LEVEL_RESULT = 'SET_LEVEL_RESULT'
const UPDATE_GAME_TIME = 'UPDATE_GAME_TIME'
const SHOW_COMPLETE_LEVEL_SCREEN = 'SHOW_COMPLETE_LEVEL_SCREEN'
const RESET_GAME = 'RESET_GAME'
const GAME_OVER = 'GAME_OVER'

// ------------------------------------
// Actions
// ------------------------------------
// eslint-disable-next-line @typescript-eslint/naming-convention
const _createLevel = level => ({
  type: CREATE_LEVEL,
  level
})

const checkCell = cellName => {
  return {
    type: CHECK_CELL,
    cellName
  }
}

const setGame = cellType => {
  return {
    type: SET_GAME,
    cellType
  }
}

// eslint-disable-next-line @typescript-eslint/naming-convention
const _setLevelResult = result => {
  return {
    type: SET_LEVEL_RESULT,
    result
  }
}

export const updateGameTime = time => {
  return {
    type: UPDATE_GAME_TIME,
    time
  }
}

export const showCompleteLevelScreen = state => {
  return {
    type: SHOW_COMPLETE_LEVEL_SCREEN,
    state
  }
}

export const resetGame = () => {
  return {
    type: RESET_GAME
  }
}

// eslint-disable-next-line @typescript-eslint/naming-convention
const _gameOver = () => {
  return {
    type: GAME_OVER
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
export const ACTION_HANDLERS = {
  [CREATE_LEVEL]: (state, action) => {
    const newState = {
      ...initialState,
      allTime: state.allTime,
      cells: createCells(action.level, state.grumpyCat),
      level: {
        number: action.level,
        complete: false,
        result: ''
      }
    }

    newState.grumpyCat = state.grumpyCat || grumpyCatAvailable(newState.cells)
    return newState
  },
  [CHECK_CELL]: (state, action) => {
    return {
      ...state,
      cells: state.cells.map(cell => {
        const thisCell = cell.name === action.cellName

        return {
          ...cell,
          checked: thisCell || cell.checked,
          pressed: thisCell
        }
      })
    }
  },
  [SET_GAME]: (state, action) => {
    const amountAnimals = state.cells.filter(cell => cell.type !== 'toy' && !cell.checked).length

    if (action.cellType === 'toy') {
      return {
        ...state,
        message: {
          text: 'Oops, it\'s just a toy!',
          type: 'wrong'
        },
        time: state.time - 2
      }
    } else if (action.cellType === 'animal') {
      return {
        ...state,
        message: {
          text: amountAnimals > 1 ? `${amountAnimals} more left, keep going!` : 'Almost there! Look carefully.',
          type: 'correct'
        },
        time: state.time + 3
      }
    } else if (action.cellType === 'grumpy') {
      return {
        ...state,
        message: {
          text: 'Grumpy Cat Bonus! You get +5 seconds',
          type: 'grumpy'
        },
        time: state.time + 5
      }
    }
    return state
  },
  [SET_LEVEL_RESULT]: (state, action) => {
    return {
      ...state,
      level: {
        ...state.level,
        complete: action.result === 'win'
      },
      time: 0,
      allTime: state.allTime + state.time,
      cells: []
    }
  },
  [UPDATE_GAME_TIME]: state => {
    return {
      ...state,
      time: state.time - 1
    }
  },
  [SHOW_COMPLETE_LEVEL_SCREEN]: (state, action) => {
    return {
      ...state,
      completeLevelScreen: action.state
    }
  },
  [RESET_GAME]: () => {
    return {
      ...initialState
    }
  },
  [GAME_OVER]: state => {
    return {
      ...state,
      gameOver: true
    }
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  level: {
    number: 0,
    complete: false,
    result: ''
  },
  time: 10,
  allTime: 0,
  cells: [],
  message: {
    text: 'Spot all Santas with claws!',
    type: 'welcome'
  },
  completeLevelScreen: false,
  gameOver: false
}

export default function santaClawsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export const clickOnCell = cell => dispatch => {
  dispatch(checkCell(cell.name))
  dispatch(setGame(cell.type))
  dispatch(checkWin())
}

// create new level
export const createLevel = () => (dispatch, getState) => {
  let currentLevel = getState().SantaClaws.level.number

  currentLevel += 1

  dispatch(_createLevel(currentLevel))
}

const checkWin = () => (dispatch, getState) => {
  const { cells } = getState().SantaClaws
  const animals = cells.filter(cell => cell.type === 'animal' || cell.type === 'grumpy')
  const allComplete = animals.every(elem => elem.checked === true)

  if (allComplete) {
    dispatch(setLevelResult('win'))
  }
}

export const setLevelResult = result => (dispatch, getState) => {
  const level = getState().SantaClaws.level.number

  dispatch(_setLevelResult(result))
  if (result === 'win') {
    if (level === 5) {
      dispatch(getPrize(5))
    } else {
      dispatch(showCompleteLevelScreen(true))
    }
  } else {
    dispatch(getPrize(level - 1))
  }
}

const getPrize = level => (dispatch, getState) => {
  dispatch(_gameOver())
  const { allTime } = getState().SantaClaws
  const points = convertTimeToPoints(allTime, level)
  const data = {
    action: 'complete',
    gameType: 'gameSantaClaws',
    result: {
      level,
      points
    }
  }

  dispatch(getEndResult(data))
}
