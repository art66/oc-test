import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Cell from './Cell'
import s from './styles.cssmodule.scss'

export class Board extends Component {
  componentDidMount() {
    this.props.createLevel()

    this.runCountdown()
  }

  componentWillUnmount() {
    clearInterval(this.countdown)
  }

  runCountdown() {
    this.countdown = setInterval(() => {
      if (this.props.santaClaws.time <= 0) {
        clearInterval(this.countdown)
        this.props.setLevelResult('lose')
      } else {
        this.props.updateGameTime(this.props.santaClaws.time)
      }
    }, 1000)
  }

  renderCell = (cell, i) => <Cell key={i} cell={cell} clickOnCell={this.props.clickOnCell} />

  render() {
    const { cells, level } = this.props.santaClaws
    const c = classnames({
      [s['board']]: true,
      [s['pulse']]: this.props.santaClaws.time <= 5,
      [s[`level${level.number}`]]: true
    })

    return <ul className={c}>{cells.map(this.renderCell)}</ul>
  }
}

Board.propTypes = {
  createLevel: PropTypes.func,
  updateGameTime: PropTypes.func,
  setLevelResult: PropTypes.func,
  clickOnCell: PropTypes.func,
  santaClaws: PropTypes.object
}

export default Board
