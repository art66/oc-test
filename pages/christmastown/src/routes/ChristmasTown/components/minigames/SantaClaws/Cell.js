import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'
import classnames from 'classnames'
import * as images from './images.json'

export class Cell extends Component {
  clickOnCell(cell) {
    if (cell.checked) {
      return
    }
    this.props.clickOnCell(cell)
  }

  getImage(cell) {
    let imageName
    if (cell.pressed && cell.type === 'toy') {
      imageName = 'red_' + this.props.cell.name
    } else {
      imageName = this.props.cell.name
    }

    return <img src={images[imageName]} alt="image" draggable="false" />
  }

  render() {
    const { cell } = this.props
    const c = classnames({
      [s['cell']]: true,
      [s['checked']]: cell.checked || cell.pressed,
      [s['checked-toy']]: cell.checked && !cell.pressed && cell.type === 'toy',
      [s['checked-grumpy']]: cell.checked && cell.type === 'grumpy'
    })

    return (
      <li
        className={c}
        onClick={() => {
          this.clickOnCell(cell)
        }}
      >
        {this.getImage(cell)}
      </li>
    )
  }
}

Cell.propTypes = {
  clickOnCell: PropTypes.func,
  cell: PropTypes.object
}

export default Cell
