const TOYS_MAX = 42
const ANIMALS_MAX = 10
const LVL1_CELLS = 9
const LVL2_CELLS = 16
const LVL3_CELLS = 25
const LVL4_CELLS = 36
const LVL5_CELLS = 49

const createIndexes = indexes => {
  return [...new Array(indexes).fill(0)].map((_, i) => {
    return i + 1
  })
}

export function createCells(level, grumpyCat) {
  let animalsIndexes = createIndexes(ANIMALS_MAX)
  let toysIndexes = createIndexes(TOYS_MAX)
  let animalsCount
  let cellsCount

  switch (level) {
    case 1:
      animalsCount = randomCount(3, 5)
      cellsCount = LVL1_CELLS
      break
    case 2:
      animalsCount = randomCount(4, 8)
      cellsCount = LVL2_CELLS
      break
    case 3:
      animalsCount = randomCount(5, 8)
      cellsCount = LVL3_CELLS
      break
    case 4:
      animalsCount = randomCount(7, 10)
      cellsCount = LVL4_CELLS
      break
    case 5:
      animalsCount = randomCount(8, 10)
      cellsCount = LVL5_CELLS
      break
  }

  let tempArray = []
  let i = 0

  if (!grumpyCat && level === 5) {
    tempArray.push({
      name: 'grumpy_cat',
      type: 'grumpy',
      pressed: false,
      checked: false
    })
  } else if (!grumpyCat && level >= 3) {
    let chance = randomCount(0, 1)
    if (chance) {
      tempArray.push({
        name: 'grumpy_cat',
        type: 'grumpy',
        checked: false,
        pressed: false
      })
    }
  }

  while (tempArray.length < cellsCount) {
    if (i < animalsCount) {
      let randomElem = getRandomElement(animalsIndexes)
      tempArray.push({
        name: 'animal_' + animalsIndexes.splice(randomElem, 1)[0],
        type: 'animal',
        checked: false,
        pressed: false
      })
    } else {
      let randomElem = getRandomElement(toysIndexes)
      tempArray.push({
        name: 'toy_' + toysIndexes.splice(randomElem, 1)[0],
        type: 'toy',
        checked: false,
        pressed: false
      })
    }
    i++
  }

  return [...new Array(cellsCount).fill(0)].map(() => {
    let randomElem = getRandomElement(tempArray)
    return tempArray.splice(randomElem, 1)[0]
  })
}

const randomCount = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

const getRandomElement = array => {
  return Math.floor(Math.random() * array.length)
}

export const grumpyCatAvailable = array => {
  return array.some(elem => elem.type === 'grumpy')
}
