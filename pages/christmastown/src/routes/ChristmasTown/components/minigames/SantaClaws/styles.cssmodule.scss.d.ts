// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'board': string;
  'cell': string;
  'checked': string;
  'checked-grumpy': string;
  'checked-toy': string;
  'content': string;
  'correct': string;
  'globalSvgShadow': string;
  'grumpy': string;
  'items': string;
  'level': string;
  'level-end': string;
  'level1': string;
  'level2': string;
  'level3': string;
  'level4': string;
  'level5': string;
  'message': string;
  'pulse': string;
  'timer': string;
  'welcome': string;
  'wrap': string;
  'wrong': string;
}
export const cssExports: CssExports;
export default cssExports;
