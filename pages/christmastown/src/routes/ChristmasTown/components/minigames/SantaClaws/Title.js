import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as images from './images.json'
import s from './styles.cssmodule.scss'

export class Title extends Component {
  getMessage() {
    const { message } = this.props
    if (message.type === 'grumpy') {
      return (
        <h2 className={[s[message.type]]}>
          <img src={images['grumpy_cat']} alt="grumpy cat" />
          {message.text}
        </h2>
      )
    } else {
      return <h2 className={[s[message.type]]}>{message.text}</h2>
    }
  }

  render() {
    return <div className={[s['message']]}>{this.getMessage()}</div>
  }
}

Title.propTypes = {
  message: PropTypes.object
}

export default Title
