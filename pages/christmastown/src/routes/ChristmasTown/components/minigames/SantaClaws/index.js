import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import styles from '../../../hoc/withMinigameWrapper/minigameWrapper.cssmodule.scss'
import slideAnimation from '../../../hoc/withMinigameWrapper/slideAnimation.cssmodule.scss'
import { AnimationDuration } from '../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import { setGameTitle } from '../../../modules'
import GameStartScreen from '../../GameStartScreen'
import ScoreBoard from '../../ScoreBoard'
import Board from './Board'
import CompleteLevelScreen from './CompleteLevel'
import reducer, {
  clickOnCell,
  createLevel,
  resetGame,
  setLevelResult,
  showCompleteLevelScreen,
  updateGameTime
} from './modules'
import s from './styles.cssmodule.scss'
import Title from './Title'

export { reducer }

export class SantaClaws extends Component {
  componentDidMount() {
    this.props.setGameTitle({
      gameTitle: 'Santa Claws'
    })
  }

  componentWillUnmount() {
    this.props.resetGame()
  }

  selectComponent() {
    const { gameAccepted, gameEndResult, santaClaws } = this.props

    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen
            message='Play Santa Claws?'
            yesTitle='YES'
            noTitle='NO'
            rules='Spot all Santas with claws!'
          />
        </CSSTransition>
      )
    }

    if (santaClaws.gameOver) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName='SantaClaws' title='Santa Claws high scores' {...gameEndResult} />
        </CSSTransition>
      )
    }

    if (santaClaws.completeLevelScreen) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='CompleteLevelScreen'>
          <CompleteLevelScreen level={santaClaws.level} showCompleteLevelScreen={this.props.showCompleteLevelScreen} />
        </CSSTransition>
      )
    }

    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='Board'>
        <div className={s['wrap']}>
          <Title message={santaClaws.message} />
          <Board
            santaClaws={santaClaws}
            createLevel={this.props.createLevel}
            updateGameTime={this.props.updateGameTime}
            setLevelResult={this.props.setLevelResult}
            clickOnCell={this.props.clickOnCell}
          />
        </div>
      </CSSTransition>
    )
  }

  render() {
    return <TransitionGroup className={styles.ctMiniGameWrapper}>{this.selectComponent()}</TransitionGroup>
  }
}

SantaClaws.propTypes = {
  gameAccepted: PropTypes.bool,
  gameEndResult: PropTypes.object,
  setGameTitle: PropTypes.func,
  credentials: PropTypes.object,
  santaClaws: PropTypes.object,

  createLevel: PropTypes.func,
  updateGameTime: PropTypes.func,
  setLevelResult: PropTypes.func,
  clickOnCell: PropTypes.func,
  showCompleteLevelScreen: PropTypes.func,
  resetGame: PropTypes.func
}

const mapStateToProps = state => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  santaClaws: state.SantaClaws
})

const mapActionsToProps = {
  createLevel,
  updateGameTime,
  setLevelResult,
  clickOnCell,
  showCompleteLevelScreen,
  resetGame,
  setGameTitle
}

export default connect(mapStateToProps, mapActionsToProps)(SantaClaws)
