// eslint-disable-next-line complexity
export const getCouponsAmountClass = (couponsAmount: number): number => {
  let coupons = couponsAmount

  switch (true) {
    case (couponsAmount >= 15 && couponsAmount < 20):
      coupons = 15
      break
    case (couponsAmount >= 20 && couponsAmount < 30):
      coupons = 20
      break
    case (couponsAmount >= 30 && couponsAmount < 40):
      coupons = 30
      break
    case (couponsAmount >= 40 && couponsAmount < 50):
      coupons = 40
      break
    case (couponsAmount >= 50 && couponsAmount < 60):
      coupons = 50
      break
    case (couponsAmount >= 60 && couponsAmount < 70):
      coupons = 60
      break
    case (couponsAmount >= 70 && couponsAmount < 80):
      coupons = 70
      break
    case (couponsAmount >= 80 && couponsAmount < 90):
      coupons = 80
      break
    case (couponsAmount >= 90):
      coupons = 90
      break
    default:
      coupons = couponsAmount
      break
  }

  return coupons
}
