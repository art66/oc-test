import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { leaveGame, move, setGameTitle, setGameControls } from '../../../modules'
import { exchangeCoupons, resetCouponsTent, TReward } from './actions'
import { getCouponsAmountClass } from './utils'
import reducer from './modules'
import ExchangePreview from './ExchangePreview'
import ExchangeResult from './ExchangeResult'
import s from './styles.cssmodule.scss'

export { reducer }

interface IProps {
  rewards: TReward[]
  couponsAmount: number
  isExchangedCoupons: boolean
  leaveGame: () => void
  exchangeCoupons: () => void
  resetCouponsTent: () => void
  move: (direction: string) => void
  setGameTitle: (param: { gameTitle: string }) => void
  setGameControls: (param: { gameControls: JSX.Element }) => void
}

class CouponExchange extends Component<IProps> {
  componentDidMount() {
    this.props.setGameTitle({ gameTitle: 'Coupon Exchange' })
    this.props.setGameControls({ gameControls: this.getGameControls() })
    this.props.resetCouponsTent()
  }

  componentDidUpdate(prevProps: Readonly<IProps>) {
    if (prevProps.couponsAmount !== this.props.couponsAmount) {
      this.props.setGameControls({ gameControls: this.getGameControls() })
    }
  }

  getGameControls = () => {
    return (
      <p className={s.gameCouponControls}>
        You have <span className={s.coupons}>{this.props.couponsAmount}</span> Coupons
      </p>
    )
  }

  leaveCouponExchange = () => {
    this.props.leaveGame()
    this.props.move('bottom')
  }

  render() {
    return (
      <div className={cn(
        s.couponExchange,
        s[`coupon${getCouponsAmountClass(this.props.couponsAmount)}`],
        { [s.exchanged]: this.props.isExchangedCoupons }
      )}
      >
        {this.props.isExchangedCoupons ?
          <ExchangeResult
            rewards={this.props.rewards}
            leaveGame={this.leaveCouponExchange}
          /> :
          <ExchangePreview
            exchangeCoupons={this.props.exchangeCoupons}
            leaveGame={this.leaveCouponExchange}
            couponsAmount={this.props.couponsAmount}
          />}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  rewards: state.CouponExchange.rewards,
  isExchangedCoupons: state.CouponExchange.isExchangedCoupons,
  couponsAmount: state.christmastown.mapData.couponsAmount
})

const mapActionsToProps = {
  leaveGame,
  move,
  setGameTitle,
  setGameControls,
  exchangeCoupons,
  resetCouponsTent
}

export default connect(mapStateToProps, mapActionsToProps)(CouponExchange)
