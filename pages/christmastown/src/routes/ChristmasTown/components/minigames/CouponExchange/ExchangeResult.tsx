import React, { Component } from 'react'
import Rewards from './Rewards'
import { TReward } from './actions'
import s from './styles.cssmodule.scss'

interface IProps {
  rewards: TReward[]
  leaveGame: () => void
}

class ExchangeResult extends Component<IProps> {
  getRewardsAmount = () => this.props.rewards.reduce((accumulator, reward) => accumulator + reward.quantity, 0)

  render() {
    return (
      <div className={s.couponsInfo}>
        <p className={s.resultMessage}>You have exchanged your coupons for {this.getRewardsAmount()} items!</p>
        <Rewards rewards={this.props.rewards} />
        <div className={s.actions}>
          <button type='button' className='btn btn-blue' onClick={this.props.leaveGame}>Leave</button>
        </div>
      </div>
    )
  }
}

export default ExchangeResult
