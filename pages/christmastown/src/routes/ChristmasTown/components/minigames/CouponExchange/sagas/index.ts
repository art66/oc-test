import { put, takeEvery } from 'redux-saga/effects'
import { fetchUrl, getErrorMessage } from '../../../../utils'
import { debugShow } from '../../../../../../controller/actions'
import { EXCHANGE_COUPONS } from '../actions/actionTypes'
import { setCouponsPrizes } from '../actions'

function* exchangeCoupons() {
  try {
    const data = yield fetchUrl('exchangePrizeCoupons')

    yield put(setCouponsPrizes(data.rewards, data.success))
  } catch (e) {
    yield put(debugShow(getErrorMessage(e)))
  }
}

export default function* couponExchangeSaga() {
  yield takeEvery(EXCHANGE_COUPONS, exchangeCoupons)
}
