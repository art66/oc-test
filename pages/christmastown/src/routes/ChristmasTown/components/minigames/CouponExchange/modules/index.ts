import { handleActions } from 'redux-actions'
import { SET_COUPONS_PRIZES, RESET_COUPONS_TENT } from '../actions/actionTypes'
import { TReward } from '../actions'

type TState = {
  rewards: TReward[]
  isExchangedCoupons: boolean
}

type TAction<TPayload = any> = {
  type: string
  payload: TPayload
}

const initialState = {
  rewards: [],
  isExchangedCoupons: false
}

export default handleActions(
  {
    [SET_COUPONS_PRIZES](state: TState, action: TAction): TState {
      return {
        ...state,
        rewards: action.payload.rewards,
        isExchangedCoupons: action.payload.isExchanged
      }
    },
    [RESET_COUPONS_TENT]() {
      return {
        ...initialState
      }
    }
  },
  initialState
)
