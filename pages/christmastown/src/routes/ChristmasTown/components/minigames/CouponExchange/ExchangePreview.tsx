import React, { Component } from 'react'
import cn from 'classnames'
import { EXCHANGE_MESSAGE, MISSING_COUPONS_MESSAGE } from './constants'
import s from './styles.cssmodule.scss'

interface IProps {
  couponsAmount: number
  exchangeCoupons: () => void
  leaveGame: () => void
}

class ExchangePreview extends Component<IProps> {
  renderMessage = () => {
    const { couponsAmount } = this.props

    return couponsAmount ? EXCHANGE_MESSAGE : MISSING_COUPONS_MESSAGE
  }

  render() {
    const { exchangeCoupons, leaveGame, couponsAmount } = this.props

    return (
      <div className={s.couponsInfo}>
        <p className={s.message}>
          {this.renderMessage()}
        </p>
        <div className={s.actions}>
          <button type='button' className='btn btn-blue' onClick={leaveGame}>Leave</button>
          <button
            type='button'
            className={cn('btn', 'btn-blue', { locked: !couponsAmount })}
            onClick={exchangeCoupons}
          >
            Exchange
          </button>
        </div>
      </div>
    )
  }
}

export default ExchangePreview
