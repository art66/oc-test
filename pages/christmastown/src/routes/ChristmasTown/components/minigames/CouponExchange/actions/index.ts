import { createAction } from 'redux-actions'
import { EXCHANGE_COUPONS, SET_COUPONS_PRIZES, RESET_COUPONS_TENT } from './actionTypes'

export type TReward = {
  category: string
  isReceived: boolean
  item_id: string
  name: string
  quantity: number
  type: number
  type2: string
}

// ------------------------------------
// Actions
// ------------------------------------
export const exchangeCoupons = createAction(EXCHANGE_COUPONS)
export const resetCouponsTent = createAction(RESET_COUPONS_TENT)
export const setCouponsPrizes = createAction(
  SET_COUPONS_PRIZES,
  (rewards: TReward[], isExchanged: boolean) => ({ rewards, isExchanged })
)
