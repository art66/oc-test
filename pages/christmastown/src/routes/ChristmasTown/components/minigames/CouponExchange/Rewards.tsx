import React, { Component } from 'react'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { TReward } from './actions'
import DebugModeClarityIcon from '../../../../../components/DebugModeClarityIcon/DebugModeClarityIcon'
import { ITEMS_URL, CT_ITEMS_URL, ITEMS_IMG_EXTENSION, TORN_ITEMS } from '../../../constants'
import { THRESHOLD_FOR_SMALL_REWARDS } from './constants'
import s from './styles.cssmodule.scss'

interface IProps {
  rewards: TReward[]
}

class Rewards extends Component<IProps> {
  getItemImageUrl = (item, rewards) => {
    const imageSize = rewards.length > THRESHOLD_FOR_SMALL_REWARDS ? 'medium' : 'large'

    return item.category === TORN_ITEMS ?
      `${ITEMS_URL + item.type}/${imageSize}${ITEMS_IMG_EXTENSION}` :
      `${CT_ITEMS_URL}${item.category}/${item.category}-${item.type}${ITEMS_IMG_EXTENSION}`
  }

  getPrizesList = rewards =>
    rewards.map(reward => {
      return (
        <div
          key={reward.category + reward.type}
          id={reward.category + reward.type}
          className={cn(s.prize, { [s.small]: rewards.length > THRESHOLD_FOR_SMALL_REWARDS }, 'ct-with-tooltip')}
        >
          <img src={this.getItemImageUrl(reward, rewards)} />
          {!reward.isReceived && <DebugModeClarityIcon />}
          <div className={s.quantity}>{reward.quantity > 1 ? reward.quantity : ''}</div>
          <Tooltip position='bottom' arrow='center' parent={reward.category + reward.type}>
            {reward.name}
          </Tooltip>
        </div>
      )
    })

  render() {
    return <div className={s.rewards}>{this.getPrizesList(this.props.rewards)}</div>
  }
}

export default Rewards
