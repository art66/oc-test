export const COUPONS_MAX_AMOUNTS_FOR_DAY = 5
export const THRESHOLD_FOR_SMALL_REWARDS = 24
export const EXCHANGE_MESSAGE = 'Do you want to exchange all of your coupons for prizes?'
export const MISSING_COUPONS_MESSAGE = 'It\'s so sad, but you have no Prize Coupons!'
