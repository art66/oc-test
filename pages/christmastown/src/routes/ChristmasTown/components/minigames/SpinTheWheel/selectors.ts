import { createSelector } from 'reselect'
import {
  MOBILE_LAYOUT_TYPE,
  SECTORS,
  SECTOR_ANGLE,
  SPIN_STATE_TYPE,
  SPINNING_STATE_TYPE,
  TABLET_LAYOUT_TYPE,
  TStateType,
  WHEEL_MOBILE_RADIUS,
  WHEEL_RADIUS,
  WHEEL_TABLET_RADIUS
} from './constants'
import { IGlobalState } from './interfaces'

export const mediaTypeSelector = (state) => state.browser.mediaType

export const stateTypeSelector = (state: IGlobalState): TStateType => state.SpinTheWheel.stateType

export const isSpinningSelector = createSelector<IGlobalState, string, boolean>([stateTypeSelector], (stateType) => {
  return stateType === SPINNING_STATE_TYPE
})
export const spinnedSelector = createSelector<IGlobalState, string, boolean>([stateTypeSelector], (stateType) => {
  return stateType !== SPIN_STATE_TYPE
})

export const radiusSelector = createSelector<string, string, number>([mediaTypeSelector], (mediaType) => {
  if (mediaType === TABLET_LAYOUT_TYPE) {
    return WHEEL_TABLET_RADIUS
  } else if (mediaType === MOBILE_LAYOUT_TYPE) {
    return WHEEL_MOBILE_RADIUS
  }

  return WHEEL_RADIUS
})

export const sectorIdSelector = (state: IGlobalState): string => {
  return state.christmastown.minigame.gameEndResult?.sectorId || null
}

export const prizeSectorIndexSelector = createSelector<IGlobalState, string, number>([sectorIdSelector], (sectorId) => {
  const index = SECTORS.findIndex((prize) => prize.id === sectorId)

  return index !== -1 ? index : null
})

export const prizeSectorWinSelector = createSelector<IGlobalState, number, boolean>(
  [prizeSectorIndexSelector],
  (prizeIndex) => {
    if (typeof prizeIndex !== 'number') return null

    const sector = SECTORS[prizeIndex]

    return sector.win
  }
)

export const prizeSectorDegreeSelector = createSelector<number, number, number>([(index) => index], (index) => {
  return index * SECTOR_ANGLE
})
