import cn from 'classnames'
import React, { memo, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { NAUGHTY_STATE_TYPE, NICE_STATE_TYPE, SPINNING_STATE_TYPE, SPIN_STATE_TYPE } from '../../constants'
import { stateTypeSelector } from '../../selectors'
import styles from './styles.cssmodule.scss'

export default memo(function Title() {
  const stateType = useSelector(stateTypeSelector)

  const className = useMemo(
    () =>
      cn(styles.title, {
        [styles.spin]: stateType === SPIN_STATE_TYPE,
        [styles.spinning]: stateType === SPINNING_STATE_TYPE,
        [styles.naughty]: stateType === NAUGHTY_STATE_TYPE,
        [styles.nice]: stateType === NICE_STATE_TYPE
      }),
    [stateType]
  )
  const text = useMemo(() => {
    if (stateType === SPIN_STATE_TYPE) {
      return 'Spin!'
    } else if (stateType === SPINNING_STATE_TYPE) {
      return 'Spinning...'
    } else if (stateType === NAUGHTY_STATE_TYPE) {
      return 'Naughty'
    } else if (stateType === NICE_STATE_TYPE) {
      return 'Nice'
    }
  }, [stateType])

  return <div className={className}>{text}</div>
})
