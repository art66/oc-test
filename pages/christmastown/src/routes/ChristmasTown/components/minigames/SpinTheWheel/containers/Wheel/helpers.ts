export function createAnimationKeyframeText(name, { degree }) {
  return `
    @keyframes ${name} {
      from {
        opacity: 1;
        transform: translateX(-50%) translateY(-50%) rotateZ(0deg)
      }

      to {
        opacity: 0.75;
        transform: translateX(-50%) translateY(-50%) rotateZ(${degree}deg)
      }
    }';
  `
}
