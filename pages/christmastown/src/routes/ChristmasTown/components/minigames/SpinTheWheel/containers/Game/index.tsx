import React, { memo, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PreloadFontsHook from '@torn/shared/hooks/PreloadFontsHook'
import { setGameTitle } from '../../../../../modules'
import GameStartScreen from '../../../../GameStartScreen'
import ScoreBoard from '../../../../ScoreBoard'
import { resetGameAction } from '../../modules'
import Board from '../Board'
import { gameAcceptedSelector, gameEndResultSelector, gameOverSelector } from './selectors'
import styles from './styles.cssmodule.scss'

export default memo(function Game() {
  PreloadFontsHook('Mountains of Christmas')

  const gameAccepted = useSelector(gameAcceptedSelector)
  const showScore = useSelector(gameOverSelector)
  const gameEndResult = useSelector(gameEndResultSelector)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setGameTitle({ gameTitle: 'Spin The Wheel' }))

    return () => {
      dispatch(resetGameAction())
    }
  }, [])

  if (!gameAccepted) {
    return <GameStartScreen message='Play Spin The Wheel?' yesTitle='YES' noTitle='NO' />
  }

  if (showScore) {
    return (
      <ScoreBoard
        gameName='SpinTheWheel'
        title='Spin The Wheel high scores'
        {...gameEndResult}
        withRetryButton={false}
      />
    )
  }

  return (
    <div className={styles.game}>
      <Board />
    </div>
  )
})
