import { SVGIconGenerator } from '@torn/shared/SVG'
import cn from 'classnames'
import React, { memo, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { DESKTOP_LAYOUT_TYPE, SECTOR_ANGLE } from '../../constants'
import { mediaTypeSelector, prizeSectorDegreeSelector, radiusSelector } from '../../selectors'
import { IProps } from './interfaces'
import styles from './styles.cssmodule.scss'

export default memo(function Sector(props: IProps) {
  const { className, index, label, win, icon, count } = props

  const mediaType = useSelector(mediaTypeSelector)
  const radius = useSelector(radiusSelector)
  const degree = prizeSectorDegreeSelector(index)

  const sectorStyle = useMemo(
    () => ({
      width: radius,
      height: radius,
      transform: `rotateZ(${degree}deg)`
    }),
    [degree, radius]
  )

  const bodyWrapStyle = useMemo(
    () => ({
      width: radius,
      transform: `rotateZ(-${90 - SECTOR_ANGLE}deg)`
    }),
    [radius]
  )

  const bodyStyle = useMemo(
    () => ({
      width: (2 * Math.PI * radius) / count,
      transform: `rotateZ(${SECTOR_ANGLE / 2 + 90 - SECTOR_ANGLE}deg) translateX(-50%)`
    }),
    [count, radius]
  )

  const contentClassName = useMemo(() => cn(styles.content, { [styles.lose]: !win }, className), [win, className])

  const labelView = useMemo(() => {
    const textRows = label.map(textRow => (
      <span className={styles.titleRow} key={textRow}>
        {textRow}
      </span>
    ))
    const ariaLabel = `${label.join(' ')} sector`

    return (
      <p className={styles.title} tabIndex={0} aria-label={ariaLabel}>
        {textRows}
      </p>
    )
  }, [label])

  const iconView = useMemo(() => {
    const subtype =
      mediaType === DESKTOP_LAYOUT_TYPE ? icon : `${icon}${mediaType.charAt(0).toUpperCase()}${mediaType.slice(1)}`

    return <SVGIconGenerator iconName={icon} preset={{ type: 'christmasTownMiniGames', subtype }} />
  }, [icon, mediaType])

  return (
    <div className={styles.sector} style={sectorStyle}>
      <div className={styles.bodyWrap} style={bodyWrapStyle}>
        <div className={styles.body} style={bodyStyle}>
          <div className={contentClassName}>
            {labelView}
            <div className={styles.icon}>{iconView}</div>
          </div>
          <div className={styles.centerArrow} />
        </div>
        <div className={styles.rightArrow} />
      </div>
      <div className={styles.leftArrow} />
    </div>
  )
})
