import { useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NAUGHTY_STATE_TYPE, NICE_STATE_TYPE, SECTOR_ANGLE, SPINNING_STATE_TYPE } from '../../constants'
import { getResultRequestAction, setStateTypeAction, showScoreAction } from '../../modules'
import { prizeSectorDegreeSelector, prizeSectorIndexSelector, prizeSectorWinSelector } from '../../selectors'
import { MAX_WHEEL_SPIN_ANIMATION_DURATION, MIN_WHEEL_SPIN_ANIMATION_DURATION } from './constants'
import { createAnimationKeyframeText } from './helpers'
import { getRandomArbitrary } from './utils'

export function AnimationHook() {
  const [ref, setRef] = useState(null)
  const [activated, setActivated] = useState(false)
  const [active, setActive] = useState(false)
  const [started, setStarted] = useState(false)

  const dispatch = useDispatch()

  const prizeSectorIndex = useSelector(prizeSectorIndexSelector)

  const ended = useMemo(() => started && !active, [started, active])

  const onClick = useCallback(() => {
    if (activated) return

    setActivated(true)

    dispatch(setStateTypeAction(SPINNING_STATE_TYPE))

    dispatch(getResultRequestAction())
  }, [activated])

  const prizeSectorWin = useSelector(prizeSectorWinSelector)

  useEffect(() => {
    if (typeof prizeSectorIndex === 'number' && !started) {
      setStarted(true)
      setActive(true)

      const duration = getRandomArbitrary(MIN_WHEEL_SPIN_ANIMATION_DURATION, MAX_WHEEL_SPIN_ANIMATION_DURATION)

      const prizeSectorDegree = prizeSectorDegreeSelector(prizeSectorIndex)

      const degree = duration * 360 - prizeSectorDegree + getRandomArbitrary(1, SECTOR_ANGLE - 1)

      const animationName = 'spinWheelAnimation'
      const animationKeyframeText = createAnimationKeyframeText(animationName, { degree })

      const styleElement = document.createElement('style')

      styleElement.setAttribute('type', 'text/css')
      styleElement.innerHTML = animationKeyframeText

      ref.appendChild(styleElement)

      ref.addEventListener('animationend', () => {
        ref.style.transform = `translateX(-50%) translateY(-50%) rotateZ(${degree}deg)`

        setActive(false)

        dispatch(setStateTypeAction(prizeSectorWin ? NICE_STATE_TYPE : NAUGHTY_STATE_TYPE))

        setTimeout(() => {
          dispatch(showScoreAction())
        }, 2500)
      })

      ref.style.animation = `${duration}s cubic-bezier(0.42, 0, 0, 1) ${animationName}`
    }
  }, [prizeSectorIndex, started, prizeSectorWin])

  return {
    setRef,
    onClick,
    active,
    started,
    ended
  }
}
