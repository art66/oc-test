import { ISector } from './interfaces'

export const DESKTOP_LAYOUT_TYPE = 'desktop'
export const TABLET_LAYOUT_TYPE = 'tablet'
export const MOBILE_LAYOUT_TYPE = 'mobile'

export const WHEEL_RADIUS = 210
export const WHEEL_TABLET_RADIUS = 183
export const WHEEL_MOBILE_RADIUS = 150

export const SPIN_STATE_TYPE = 'SPIN_STATE_TYPE'
export const SPINNING_STATE_TYPE = 'SPINNING_STATE_TYPE'
export const NAUGHTY_STATE_TYPE = 'NAUGHTY_STATE_TYPE'
export const NICE_STATE_TYPE = 'NICE_STATE_TYPE'

export type TStateType =
  | typeof SPIN_STATE_TYPE
  | typeof SPINNING_STATE_TYPE
  | typeof NAUGHTY_STATE_TYPE
  | typeof NICE_STATE_TYPE

export const SECTORS: ISector[] = [
  {
    id: 'brokenOrnament',
    win: false,
    label: ['Broken', 'Bauble'],
    icon: 'CTBrokenOrnament'
  },
  {
    id: 'energyDrinkItem',
    win: true,
    label: ['Energy', 'Drink'],
    icon: 'CTEnergyDrink'
  },
  {
    id: 'money',
    win: false,
    label: ['$1', 'Dollar'],
    icon: 'CTMoney'
  },
  {
    id: 'alcoholItem',
    win: true,
    label: ['Alcohol'],
    icon: 'CTAlcohol'
  },
  {
    id: 'tangerineItem',
    win: false,
    label: ['Tangerine'],
    icon: 'CTTangerine'
  },
  {
    id: 'bucks',
    win: true,
    label: ['5 CT', 'Bucks'],
    icon: 'CTBucks'
  },
  {
    id: 'tightyWhitiesItem',
    win: false,
    label: ['Tightly', 'Whities'],
    icon: 'CTTightlyWhities'
  },
  {
    id: 'candyItem',
    win: true,
    label: ['Candy'],
    icon: 'CTCandy'
  }
]

export const SECTOR_ANGLE = 360 / SECTORS.length
