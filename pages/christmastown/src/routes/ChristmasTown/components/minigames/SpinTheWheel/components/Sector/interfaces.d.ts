import { ISector } from '../../interfaces';

export interface IProps extends ISector {
  className?: string
  index: number
  count: number
}
