import { put, takeLatest } from 'redux-saga/effects'
import { fetchUrl, getErrorMessage } from '../../../../utils'
import { debugShow } from '../../../../../../controller/actions'
import { setGameEndResult } from '../../../../modules'
import { getResultFailureAction, getResultRequestAction, getResultSuccessAction } from '../modules'
import { IResultData } from './interfaces'

function* getResultSaga() {
  try {
    const data: IResultData = yield fetchUrl('miniGameAction', {
      gameType: 'gameSpinTheWheel',
      action: 'complete'
    })

    yield put(setGameEndResult(data))
    yield put(getResultSuccessAction())
  } catch (error) {
    yield put(getResultFailureAction())

    yield put(debugShow(getErrorMessage(error)))
  }
}

export default function* rootSaga() {
  yield takeLatest(getResultRequestAction, getResultSaga)
}
