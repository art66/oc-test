import cn from 'classnames'
import React, { memo, useMemo } from 'react'
import { useSelector } from 'react-redux'
import Title from '../../components/Title'
import { isSpinningSelector } from '../../selectors'
import Wheel from '../Wheel'
import styles from './styles.cssmodule.scss'

export default memo(function Board() {
  const isSpinning = useSelector(isSpinningSelector)

  const flakesClassName = useMemo(() => cn(styles.flakes, { [styles.active]: isSpinning }), [isSpinning])
  const candyCaneClassName = useMemo(() => cn(styles.candyCane, { [styles.active]: isSpinning }), [isSpinning])

  return (
    <div className={styles.board}>
      <Wheel />
      <div className={flakesClassName} />
      <div className={candyCaneClassName} />
      <Title />
    </div>
  )
})
