import { createAction, createReducer, PayloadAction } from '@reduxjs/toolkit'
import { SPIN_STATE_TYPE, TStateType } from '../constants'
import { IState } from './interfaces'

export const getResultRequestAction = createAction('GET_RESULT_REQUEST')
export const getResultSuccessAction = createAction('GET_RESULT_SUCCESS')
export const getResultFailureAction = createAction('GET_RESULT_FAILURE')

export const showScoreAction = createAction('GAME_OVER')
export const setStateTypeAction = createAction<TStateType>('SET_STATE_TYPE')
export const resetGameAction = createAction('RESET_GAME')

const initialState: IState = {
  showScore: false,
  stateType: SPIN_STATE_TYPE
}

export const ACTION_HANDLERS = {
  [showScoreAction.type]: (state: IState) => {
    state.showScore = true
  },
  [setStateTypeAction.type]: (state: IState, action: PayloadAction<TStateType>) => {
    state.stateType = action.payload
  },
  [resetGameAction.type]: (state: IState) => {
    Object.assign(state, initialState)
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
