export const gameAcceptedSelector = (state: any) => state.christmastown.minigame.gameAccepted
export const gameEndResultSelector = (state: any) => state.christmastown.minigame.gameEndResult
export const gameOverSelector = (state: any) => state.SpinTheWheel.showScore
