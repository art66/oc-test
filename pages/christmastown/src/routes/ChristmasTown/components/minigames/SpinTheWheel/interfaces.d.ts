import { IState } from "./modules/interfaces";

export interface ISector {
  id: string
  win: boolean
  label: string[]
  icon: string
}

export interface IGlobalState {
  christmastown: { minigame: { gameEndResult: { sectorId: string } } }
  SpinTheWheel: IState
}
