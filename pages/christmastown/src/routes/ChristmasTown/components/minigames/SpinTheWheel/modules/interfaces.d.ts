import { TStateType } from "../interfaces";

export interface IState {
  stateType: TStateType
  showScore: boolean
}
