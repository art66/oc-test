import React, { memo, useMemo } from 'react'
import { useSelector } from 'react-redux'
import Sector from '../../components/Sector'
import { SECTORS } from '../../constants'
import { prizeSectorIndexSelector, radiusSelector, spinnedSelector } from '../../selectors'
import { AnimationHook } from './hooks'
import styles from './styles.cssmodule.scss'

export default memo(function Wheel() {
  const animation = AnimationHook()

  const prizeSectorIndex = useSelector(prizeSectorIndexSelector)
  const radius = useSelector(radiusSelector)
  const spinned = useSelector(spinnedSelector)

  const sizeStyle = useMemo(() => {
    const size = radius * 2

    return {
      height: size,
      width: size
    }
  }, [radius])

  const sectorsView = useMemo(() => {
    return SECTORS.map((sector, index) => {
      const isNotPrizeSector = animation.ended && prizeSectorIndex !== index

      return (
        <Sector
          key={sector.id}
          {...sector}
          className={isNotPrizeSector && styles.notActiveSegment}
          index={index}
          count={SECTORS.length}
        />
      )
    })
  }, [prizeSectorIndex, animation.ended])
  const activatorView = useMemo(() => {
    if (spinned) return null

    return (
      <button
        type='button'
        className={styles.wheelActivator}
        style={sizeStyle}
        onClick={animation.onClick}
        aria-label='Spin the wheel'
        autoFocus={true}
      />
    )
  }, [sizeStyle, animation.onClick, spinned])

  return (
    <>
      {activatorView}
      <div ref={animation.setRef} className={styles.wheel} style={sizeStyle}>
        {sectorsView}
      </div>
    </>
  )
})
