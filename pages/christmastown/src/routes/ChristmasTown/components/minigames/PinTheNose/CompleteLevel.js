import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { formatSeconds } from '../../../utils'
import s from './styles.cssmodule.scss'

export class CompleteLevelScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      timer: 5
    }
  }

  componentDidMount() {
    const { number } = this.props.level
    if (number !== 5) {
      this.runCountdown()
    }
  }

  componentWillUnmount() {
    clearInterval(this.countdown)
  }

  runCountdown() {
    this.countdown = setInterval(() => {
      this.setState({ timer: this.state.timer - 1 })
      if (this.state.timer <= 0) {
        clearInterval(this.countdown)
        this.props.showCompleteLevelScreen(false)
      }
    }, 1000)
  }

  render() {
    return (
      <div className={s['level-end']}>
        <div className={s['content']}>
          <h2>
            Well done <br />
            Ready for the next level?
          </h2>
          <p>Next level starts in {formatSeconds(this.state.timer)}</p>
        </div>
      </div>
    )
  }
}

CompleteLevelScreen.propTypes = {
  level: PropTypes.object,
  showCompleteLevelScreen: PropTypes.func
}

export default CompleteLevelScreen
