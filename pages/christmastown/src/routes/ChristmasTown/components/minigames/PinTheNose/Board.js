import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import s from './styles.cssmodule.scss'
import classnames from 'classnames'
import * as images from './images.json'
import * as coords from './coords.json'
import { getRandomInteger } from '../../../utils'
import {
  setSnowman,
  changeCursorState,
  createLevel,
  setLevelResult,
  decreaseTime,
  resetGame,
  setSnowmanImageVisible,
  startLevel,
  updateGameTime
} from './modules'

import { MIN_POINTS, AVERAGE_POINTS, MAX_POINTS, AMOUNT_STAGES, CURSOR_STATE_CHANGE_TIMEOUT } from './constants'

export class Board extends Component {
  constructor(props) {
    super(props)
    this.getNoseCoords = this.getNoseCoords.bind(this)
    this.getCoordsOfSnowmanNose = this.getCoordsOfSnowmanNose.bind(this)
    this.getDelta = this.getDelta.bind(this)
    this.checkHitOfCursor = this.checkHitOfCursor.bind(this)
    this.showSnowmanImage = this.showSnowmanImage.bind(this)
    this.state = {
      rotate: 0,
      stage: AMOUNT_STAGES,
      minStage: getRandomInteger(0, AMOUNT_STAGES - 2),
      iterationIsEnd: false
    }
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  componentDidMount() {
    this.props.createLevel()
  }

  componentWillUnmount() {
    clearInterval(this.timer)
    clearInterval(this.increaseStages)
    clearInterval(this.decreaseStages)
    clearTimeout(this.timeoutBlackout)
  }

  componentDidUpdate(prevProps, prevState) {
    const { level } = this.props.pinTheNose
    if (!prevState.iterationIsEnd && this.state.iterationIsEnd) {
      this.timeoutBlackout = setTimeout(() => {
        this.startStagesCounter()
      }, level.delayBetweenIteration)
    }
  }

  initPinTheNose() {
    const { coords } = this.props.pinTheNose

    let wrapperCoords = this.refs.pinTheNoseWrapper.getBoundingClientRect()
    let snowmanCoords = this.refs.pinTheNoseSnowman.getBoundingClientRect()
    this.snowmanCenter = this.getCoordsOfSnowmanCenter(wrapperCoords, snowmanCoords)
    this.snowmanNose = this.getCoordsOfSnowmanNose(wrapperCoords, snowmanCoords)

    this.setState({
      ...this.state,
      rotate: coords.rotate
    })
  }

  getCursorCoords(e) {
    let scroll = this.getScroll()
    let wrapperCoords = this.refs.pinTheNoseWrapper.getBoundingClientRect()
    return {
      x: e.pageX - wrapperCoords.left - scroll.left,
      y: e.pageY - wrapperCoords.top - scroll.top
    }
  }

  getCoordsOfSnowmanCenter(wrapperCoords, snowmanCoords) {
    const { snowman } = this.props.pinTheNose
    return {
      x: snowmanCoords.left - wrapperCoords.left + snowman.width / 2,
      y: snowmanCoords.top - wrapperCoords.top + snowman.height / 2
    }
  }

  getCoordsOfSnowmanNose(wrapperCoords, snowmanCoords) {
    const { snowman } = this.props.pinTheNose
    return {
      x: snowman.x + snowmanCoords.left - wrapperCoords.left,
      y: snowman.y + snowmanCoords.top - wrapperCoords.top
    }
  }

  getNoseCoords() {
    const { coords } = this.props.pinTheNose
    let centerX = this.snowmanCenter.x
    let centerY = this.snowmanCenter.y
    let noseX = this.snowmanNose.x
    let noseY = this.snowmanNose.y

    let startPoint = {
      x: noseX - centerX,
      y: noseY - centerY
    }

    let angleToRad = coords.rotate * Math.PI / 180

    let endX = startPoint.x * Math.cos(angleToRad) - startPoint.y * Math.sin(angleToRad)
    let endY = startPoint.x * Math.sin(angleToRad) + startPoint.y * Math.cos(angleToRad)
    endX += centerX
    endY += centerY

    return {
      x: endX,
      y: endY
    }
  }

  getDelta(e) {
    let cursorCoords = this.getCursorCoords(e)
    let noseCoords = this.getNoseCoords()

    return Math.sqrt(Math.pow(cursorCoords.x - noseCoords.x, 2) + Math.pow(cursorCoords.y - noseCoords.y, 2))
  }

  checkHitOfCursor(e) {
    const { snowman } = this.props.pinTheNose
    let delta = this.getDelta(e)
    let snowmansDelta = snowman.delta
    if (delta > snowmansDelta[0]) {
      this.props.changeCursorState()
      this.props.decreaseTime()
      setTimeout(() => {
        this.props.changeCursorState()
      }, CURSOR_STATE_CHANGE_TIMEOUT)
    } else if (delta <= snowmansDelta[0] && delta >= snowmansDelta[1] && this.state.stage !== 5) {
      this.props.setLevelResult('win', MIN_POINTS)
    } else if (delta < snowmansDelta[1] && delta >= snowmansDelta[2] && this.state.stage !== 5) {
      this.props.setLevelResult('win', AVERAGE_POINTS)
    } else if (delta < snowmansDelta[2] && this.state.stage !== 5) {
      this.props.setLevelResult('win', MAX_POINTS)
    }
  }

  getScroll() {
    let html = document.documentElement
    let body = document.body
    let scrollLeft = (html && html.scrollLeft) || (body && body.scrollLeft) || 0
    let scrollTop = (html && html.scrollTop) || (body && body.scrollTop) || 0

    return {
      left: scrollLeft,
      top: scrollTop
    }
  }

  startGameTimer() {
    this.timer = setInterval(() => {
      if (this.props.pinTheNose.time <= 0) {
        clearInterval(this.timer)
        this.props.setLevelResult('lose')
      } else {
        this.props.updateGameTime(this.props.pinTheNose.time)
      }
    }, 1000)
  }

  startStagesCounter() {
    const { level } = this.props.pinTheNose
    this.increaseStages = setInterval(() => {
      this.setState({
        ...this.state,
        stage: this.state.stage - 1,
        iterationIsEnd: false
      })

      if (this.state.stage === this.state.minStage) {
        clearInterval(this.increaseStages)

        this.decreaseStages = setInterval(() => {
          this.setState({
            ...this.state,
            stage: this.state.stage + 1
          })

          if (this.state.stage === AMOUNT_STAGES) {
            clearInterval(this.decreaseStages)
            this.setState({
              ...this.state,
              minStage: getRandomInteger(0, AMOUNT_STAGES - 2),
              iterationIsEnd: true
            })
          }
        }, level.delayBetweenChangeStages)
      }
    }, level.delayBetweenChangeStages)
  }

  showSnowmanImage() {
    const { snowman, coords, snowmanImageVisible } = this.props.pinTheNose
    if (!snowmanImageVisible) {
      this.props.setSnowmanImageVisible()
    }

    const layer1 = classnames({
      [s['layer-1']]: true,
      [s['hide']]: this.state.stage < 1
    })

    const layer2 = classnames({
      [s['layer-2']]: true,
      [s['hide']]: this.state.stage < 2
    })

    const layer3 = classnames({
      [s['layer-3']]: true,
      [s['hide']]: this.state.stage < 3
    })

    const layer4 = classnames({
      [s['layer-4']]: true,
      [s['hide']]: this.state.stage < 4,
      [s['blur']]: this.state.stage === 5
    })

    return (
      <div>
        <img
          className={s['snowman']}
          ref="pinTheNoseSnowman"
          src={images['snowman_' + snowman.id]}
          alt="snowman"
          style={{
            transform: `rotate(${this.state.rotate}deg)`,
            left: coords.left,
            top: coords.top
          }}
        />
        <div className={layer1} />
        <div className={layer2} />
        <div className={layer3} />
        <div className={layer4} />
      </div>
    )
  }

  render() {
    const { isMissClick, snowmanAvailable, snowmanImageVisible, levelStarted } = this.props.pinTheNose

    if (snowmanImageVisible && !levelStarted) {
      this.initPinTheNose()
      this.startGameTimer()
      this.startStagesCounter()
      this.props.startLevel()
    }

    const c = classnames({
      [s['background']]: true,
      [s['error']]: isMissClick
    })

    return (
      <div className={c} ref="pinTheNoseWrapper" onClick={this.checkHitOfCursor}>
        {snowmanAvailable ? this.showSnowmanImage() : ''}
      </div>
    )
  }
}

Board.propTypes = {
  setSnowman: PropTypes.func,
  changeCursorState: PropTypes.func,
  createLevel: PropTypes.func,
  updateGameTime: PropTypes.func,
  decreaseTime: PropTypes.func,
  setLevelResult: PropTypes.func,
  setSnowmanImageVisible: PropTypes.func,
  startLevel: PropTypes.func,
  pinTheNose: PropTypes.object
}

const mapStateToProps = state => ({
  pinTheNose: state.PinTheNose
})

const mapActionsToProps = {
  setSnowman,
  changeCursorState,
  createLevel,
  setLevelResult,
  decreaseTime,
  resetGame,
  setSnowmanImageVisible,
  startLevel,
  updateGameTime
}

export default connect(mapStateToProps, mapActionsToProps)(Board)
