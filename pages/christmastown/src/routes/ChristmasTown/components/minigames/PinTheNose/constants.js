export const MIN_LEFT = -70
export const MAX_LEFT = {
  desktop: 305,
  tablet: 217,
  mobile: 151
}
export const MIN_TOP = -105
export const MAX_TOP = 270
export const MAX_RIGHT_SIDE_X = {
  desktop: 392,
  tablet: 304,
  mobile: 228
}
export const MAX_BOTTOM_SIDE_Y = 100
export const MIN_TOP_SIDE_Y = -30
export const MAX_TOP_SIDE_Y = 30
export const MIN_LEFT_SIDE_X = -6
export const MIN_RIGHT_BOTTOM_ROTATE = 270
export const MAX_RIGHT_BOTTOM_ROTATE = 359
export const MIN_RIGHT_TOP_ROTATE = 180
export const MAX_RIGHT_TOP_ROTATE = 269
export const MIN_LEFT_BOTTOM_ROTATE = 0
export const MAX_LEFT_BOTTOM_ROTATE = 80
export const MIN_LEFT_TOP_ROTATE = 90
export const MAX_LEFT_TOP_ROTATE = 170
export const DEFAULT_LEFT = 50
export const DEFAULT_TOP = 70

export const MIN_POINTS = 10
export const AVERAGE_POINTS = 20
export const MAX_POINTS = 30

export const AMOUNT_STAGES = 5

export const CURSOR_STATE_CHANGE_TIMEOUT = 300

export const BOARD_WIDTH_DESKTOP = 453
export const BOARD_WIDTH_TABLET = 386
export const BOARD_WIDTH_MOBILE = 320

export const PENALTY_TIME = 5

export const INC_OF_DELAY_BETWEEN_ITERATIONS = 120
export const DEC_OF_DELAY_BETWEEN_CHANGE_STAGES = 22
