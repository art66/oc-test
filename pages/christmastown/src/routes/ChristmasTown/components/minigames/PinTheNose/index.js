import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ScoreBoard from '../../ScoreBoard'
import GameStartScreen from '../../GameStartScreen'
import CompleteLevelScreen from './CompleteLevel'
import Board from './Board'
import reducer, { showCompleteLevelScreen, setLevelResult, resetGame } from './modules'
import s from './styles.cssmodule.scss'

import { setGameTitle } from '../../../modules'

export { reducer }

export class PinTheNose extends Component {
  componentDidMount() {
    const { setGameTitle } = this.props

    setGameTitle({
      gameTitle: 'Pin the Nose'
    })
  }

  componentWillUnmount() {
    this.props.resetGame()
  }

  render() {
    const { gameAccepted, gameEndResult, gameOver, completeLevelScreen, showCompleteLevelScreen, level } = this.props

    if (!gameAccepted) {
      return <GameStartScreen message='Play Pin the Nose?' yesTitle='YES' noTitle='NO' />
    }

    if (gameOver) {
      return <ScoreBoard gameName='PinTheNose' title='Pin the Nose high scores' {...gameEndResult} />
    }

    if (completeLevelScreen) {
      return <CompleteLevelScreen level={level} showCompleteLevelScreen={showCompleteLevelScreen} />
    }

    return (
      <div className={s['wrap']}>
        <Board />
      </div>
    )
  }
}

PinTheNose.propTypes = {
  gameAccepted: PropTypes.bool,
  setGameTitle: PropTypes.func,
  gameEndResult: PropTypes.object,
  pinTheNose: PropTypes.object,
  setSnowman: PropTypes.func,
  gameOver: PropTypes.bool,
  level: PropTypes.object,
  completeLevelScreen: PropTypes.bool,
  showCompleteLevelScreen: PropTypes.func,
  setLevelResult: PropTypes.func,
  resetGame: PropTypes.func
}

const mapStateToProps = state => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  gameOver: state.PinTheNose.gameOver,
  completeLevelScreen: state.PinTheNose.completeLevelScreen,
  time: state.PinTheNose.time,
  level: state.PinTheNose.level,
  pinTheNose: state.PinTheNose
})

const mapActionsToProps = {
  setGameTitle,
  setLevelResult,
  showCompleteLevelScreen,
  resetGame
}

export default connect(mapStateToProps, mapActionsToProps)(PinTheNose)
