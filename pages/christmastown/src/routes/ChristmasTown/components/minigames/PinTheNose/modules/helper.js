import {
  MIN_LEFT,
  MAX_LEFT,
  MIN_TOP,
  MAX_TOP,
  MAX_RIGHT_SIDE_X,
  MAX_BOTTOM_SIDE_Y,
  MIN_TOP_SIDE_Y,
  MAX_TOP_SIDE_Y,
  MIN_LEFT_SIDE_X,
  MIN_RIGHT_BOTTOM_ROTATE,
  MAX_RIGHT_BOTTOM_ROTATE,
  MIN_RIGHT_TOP_ROTATE,
  MAX_RIGHT_TOP_ROTATE,
  MIN_LEFT_BOTTOM_ROTATE,
  MAX_LEFT_BOTTOM_ROTATE,
  MIN_LEFT_TOP_ROTATE,
  MAX_LEFT_TOP_ROTATE,
  DEFAULT_LEFT,
  DEFAULT_TOP,
  MIN_POINTS,
  AVERAGE_POINTS,
  MAX_POINTS,
  AMOUNT_STAGES,
  CURSOR_STATE_CHANGE_TIMEOUT,
  BOARD_WIDTH_DESKTOP,
  BOARD_WIDTH_TABLET,
  BOARD_WIDTH_MOBILE
} from '../constants'

import { getRandomInteger } from '../../../../utils'

export const getNosePosition = snowman => {
  let maxLeft
  let maxRightSideX
  let rotate = 0
  let boardWidth = document.getElementsByClassName('status-area-container')[0].clientWidth

  if (boardWidth === BOARD_WIDTH_DESKTOP) {
    maxLeft = MAX_LEFT.desktop
    maxRightSideX = MAX_RIGHT_SIDE_X.desktop
  } else if (boardWidth === BOARD_WIDTH_TABLET) {
    maxLeft = MAX_LEFT.tablet
    maxRightSideX = MAX_RIGHT_SIDE_X.tablet
  } else if (boardWidth === BOARD_WIDTH_MOBILE) {
    maxLeft = MAX_LEFT.mobile
    maxRightSideX = MAX_RIGHT_SIDE_X.mobile
  }

  let left = getRandomInteger(MIN_LEFT, maxLeft)
  let right = left + snowman.width
  let top = getRandomInteger(MIN_TOP, MAX_TOP)

  if (right > maxRightSideX && top > MAX_BOTTOM_SIDE_Y) {
    rotate = getRandomInteger(MIN_RIGHT_BOTTOM_ROTATE, MAX_RIGHT_BOTTOM_ROTATE)
  } else if (right > maxRightSideX && top < MAX_TOP_SIDE_Y) {
    rotate = getRandomInteger(MIN_RIGHT_TOP_ROTATE, MAX_RIGHT_TOP_ROTATE)
  } else if (left < MIN_LEFT_SIDE_X && top > MAX_BOTTOM_SIDE_Y) {
    rotate = getRandomInteger(MIN_LEFT_BOTTOM_ROTATE, MAX_LEFT_BOTTOM_ROTATE)
  } else if (left < MIN_LEFT_SIDE_X && top < MIN_TOP_SIDE_Y) {
    rotate = getRandomInteger(MIN_LEFT_TOP_ROTATE, MAX_LEFT_TOP_ROTATE)
  } else if (left >= MIN_LEFT_SIDE_X && right <= maxRightSideX && top >= MIN_TOP_SIDE_Y && top < MAX_TOP_SIDE_Y) {
    left = DEFAULT_LEFT
    top = DEFAULT_TOP
  } else if (top < MIN_TOP_SIDE_Y) {
    rotate = MIN_RIGHT_TOP_ROTATE
  }

  return {
    left: left,
    top: top,
    rotate: rotate
  }
}
