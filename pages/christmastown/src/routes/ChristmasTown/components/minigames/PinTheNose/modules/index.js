import { getEndResult } from '../../../../modules'
import { fetchUrl, getErrorMessage } from '../../../../utils'
import { debugShow } from '../../../../../../controller/actions'
import { getNosePosition } from './helper'
import { PENALTY_TIME, INC_OF_DELAY_BETWEEN_ITERATIONS, DEC_OF_DELAY_BETWEEN_CHANGE_STAGES } from '../constants'

export const SET_SNOWMAN = 'SET_SNOWMAN'
export const CHANGE_CURSOR_STATE = 'CHANGE_CURSOR_STATE'
export const CREATE_LEVEL = 'CREATE_LEVEL'
export const UPDATE_TIME_PIN_THE_NOSE = 'UPDATE_TIME_PIN_THE_NOSE'
export const SET_LEVEL_RESULT = 'SET_LEVEL_RESULT'
export const RESET_GAME = 'RESET_GAME'
export const GAME_OVER = 'GAME_OVER'
export const SHOW_COMPLETE_LEVEL_SCREEN = 'SHOW_COMPLETE_LEVEL_SCREEN'
export const DECREASE_TIME = 'DECREASE_TIME'
export const INCREASE_POINTS = 'INCREASE_POINTS'
export const SET_SNOWMAN_IMAGE_VISIBLE = 'SET_SNOWMAN_IMAGE_VISIBLE'
export const START_LEVEL = 'START_LEVEL'

export const _createLevel = level => ({
  type: CREATE_LEVEL,
  level
})

export const setSnowman = (snowman, coords) => ({
  type: SET_SNOWMAN,
  snowman,
  coords
})

export const setSnowmanImageVisible = () => ({
  type: SET_SNOWMAN_IMAGE_VISIBLE
})

export const decreaseTime = () => ({
  type: DECREASE_TIME
})

export const _increasePoints = points => ({
  type: INCREASE_POINTS,
  points
})

export const changeCursorState = () => ({
  type: CHANGE_CURSOR_STATE
})

export const updateGameTime = () => ({
  type: UPDATE_TIME_PIN_THE_NOSE
})

const _setLevelResult = result => {
  return {
    type: SET_LEVEL_RESULT,
    result
  }
}

export const showCompleteLevelScreen = value => {
  return {
    type: SHOW_COMPLETE_LEVEL_SCREEN,
    value
  }
}

export const startLevel = () => {
  return {
    type: START_LEVEL
  }
}

export const resetGame = () => {
  return {
    type: RESET_GAME
  }
}

const _gameOver = () => {
  return {
    type: GAME_OVER
  }
}

export const ACTION_HANDLERS = {
  [CREATE_LEVEL]: (state, action) => {
    return {
      ...initialState,
      level: {
        number: action.level,
        delayBetweenIteration: state.level.delayBetweenIteration + INC_OF_DELAY_BETWEEN_ITERATIONS,
        delayBetweenChangeStages: state.level.delayBetweenChangeStages - DEC_OF_DELAY_BETWEEN_CHANGE_STAGES
      },
      points: state.points,
      time: initialState.time - 10 * action.level
    }
  },
  [UPDATE_TIME_PIN_THE_NOSE]: state => ({
    ...state,
    time: state.time - 1
  }),
  [DECREASE_TIME]: state => ({
    ...state,
    time: state.time - PENALTY_TIME
  }),
  [INCREASE_POINTS]: (state, action) => ({
    ...state,
    points: state.points + action.points
  }),
  [SET_SNOWMAN]: (state, action) => {
    return {
      ...state,
      snowman: action.snowman,
      coords: action.coords,
      snowmanAvailable: true
    }
  },
  [CHANGE_CURSOR_STATE]: (state, action) => ({
    ...state,
    isMissClick: !state.isMissClick
  }),
  [SET_LEVEL_RESULT]: (state, action) => {
    return {
      ...state,
      level: {
        ...state.level,
        completed: action.result === 'win'
      },
      time: 0
    }
  },
  [SHOW_COMPLETE_LEVEL_SCREEN]: (state, action) => {
    return {
      ...state,
      completeLevelScreen: action.value
    }
  },
  [SET_SNOWMAN_IMAGE_VISIBLE]: state => {
    return {
      ...state,
      snowmanImageVisible: true
    }
  },
  [START_LEVEL]: state => {
    return {
      ...state,
      levelStarted: true
    }
  },
  [RESET_GAME]: () => {
    return {
      ...initialState
    }
  },
  [GAME_OVER]: state => {
    return {
      ...state,
      gameOver: true
    }
  }
}

const initialState = {
  level: {
    number: 0,
    delayBetweenIteration: 600,
    delayBetweenChangeStages: 210,
    completed: false,
    result: ''
  },
  snowmanAvailable: false,
  snowmanImageVisible: false,
  levelStarted: false,
  time: 70,
  points: 0,
  isMissClick: false,
  completeLevelScreen: false,
  gameOver: false
}

export default function PinTheNoseReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

export const createLevel = () => (dispatch, getState) => {
  let currentLevel = getState().PinTheNose.level.number
  currentLevel += 1

  dispatch(_createLevel(currentLevel))

  if (currentLevel === 1) {
    dispatch(getSnowman(false))
  } else {
    dispatch(getSnowman(true))
  }
}

const getSnowman = update => dispatch => {
  let action = update ? 'completeLevel' : 'start'
  fetchUrl('miniGameAction', {
    action: action,
    gameType: 'gamePinTheNose'
  })
  .then(result => {
    let snowman = result.progress.snowman
    let coords = getNosePosition(snowman)
    dispatch(setSnowman(snowman, coords))
  })
  .catch(error => {
    dispatch(debugShow(getErrorMessage(error)))
  })
}

export const setLevelResult = (result, points) => (dispatch, getState) => {
  let level = getState().PinTheNose.level.number
  let currentPoints = points || 0
  dispatch(_increasePoints(currentPoints))
  dispatch(_setLevelResult(result))
  if (result === 'win') {
    if (level === 5) {
      dispatch(getPrize(5))
    } else {
      dispatch(showCompleteLevelScreen(true))
    }
  } else {
    dispatch(getPrize(level - 1))
  }
}

const getPrize = level => (dispatch, getState) => {
  let points = getState().PinTheNose.points
  let data = {
    action: 'complete',
    gameType: 'gamePinTheNose',
    result: {
      level,
      points
    }
  }
  dispatch(_gameOver())
  dispatch(getEndResult(data))
}
