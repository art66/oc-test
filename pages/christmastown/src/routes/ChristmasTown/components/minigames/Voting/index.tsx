import React, { Component } from 'react'
import { connect } from 'react-redux'
import reducer, { onMouseLeaveStar, onMouseOverStar, setVote, leaveNotification, loadGameState, resetGame } from './modules'
import { leaveGame, move } from '../../../modules'
import IVoting from './interfaces/IVoting'
import Notification from './components/Notification'
import Vote from './components/Vote'
import s from './styles/styles.cssmodule.scss'

export { reducer }

interface IProps {
  voting: IVoting
  leaveGame: () => void
  move: (direction: string) => void
  setVote: (id: number) => void
  onMouseLeaveStar: () => void
  onMouseOverStar: (id: number) => void
  leaveNotification: () => void
  loadGameState: () => void
  resetGame: () => void
}

class Voting extends Component<IProps> {
  componentDidMount() {
    const { resetGame, loadGameState } = this.props

    resetGame()
    loadGameState()
  }

  render() {
    const { voting } = this.props
    const { isVoting } = voting

    return (
      <div className={s.voting}>
        {isVoting ? <Notification /> : <Vote />}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  voting: state.Voting
})

const mapActionsToProps = {
  loadGameState,
  leaveNotification,
  onMouseLeaveStar,
  onMouseOverStar,
  resetGame,
  leaveGame,
  move,
  setVote
}

export default connect(mapStateToProps, mapActionsToProps)(Voting)
