export const SET_VOTE = 'SET_VOTE'
export const SET_GAME_STATE = 'SET_GAME_STATE'
export const RESET_GAME = 'RESET_GAME'
export const LEAVE_NOTIFICATION = 'LEAVE_NOTIFICATION'
export const ON_MOUSE_OVER_STAR = 'ON_MOUSE_OVER_STAR'
export const ON_MOUSE_LEAVE_STAR = 'ON_MOUSE_LEAVE_STAR'
export const LOAD_GAME_STATE = 'LOAD_GAME_STATE'
