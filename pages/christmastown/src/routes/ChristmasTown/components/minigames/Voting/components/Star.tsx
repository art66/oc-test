import React, { Component } from 'react'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import { ACTIVE_STAR, INACTIVE_STAR, ICON_NAME, PRESET } from '../constants'
import s from '../styles/styles.cssmodule.scss'

interface IProps {
  id: number
  active: boolean
  setVote: (id: number) => void
  onMouseLeaveStar: () => void
  onMouseOverStar: (id: number) => void
}

class Star extends Component<IProps> {
  _handleClick = () => {
    const { id, setVote } = this.props

    setVote(id)
  }

  _handleMouseOver = () => {
    const { id, onMouseOverStar } = this.props

    onMouseOverStar(id)
  }

  render() {
    const { active, onMouseLeaveStar } = this.props

    return (
      <div
        className={s.star}
        onClick={this._handleClick}
        onMouseOver={this._handleMouseOver}
        onMouseLeave={onMouseLeaveStar}
      >
        <SVGIconGenerator
          iconName={ICON_NAME}
          preset={{ type: PRESET, subtype: active ? ACTIVE_STAR : INACTIVE_STAR }}
        />
      </div>
    )
  }
}

export default Star
