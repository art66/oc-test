import React, { Component } from 'react'
import Star from './Star'
import IVoting from '../interfaces/IVoting'
import { connect } from 'react-redux'
import { leaveNotification } from '../modules'
import { leaveGame, move } from '../../../../modules/index'
import s from '../styles/styles.cssmodule.scss'

interface IProps {
  leaveNotification: () => void
  leaveGame: () => void
  move: (direction: string) => void
  voting: IVoting
}

class Notification extends Component<IProps> {
  _handlerStarEvent = () => {
    return
  }

  _handleLeave = () => {
    const { move, leaveGame, leaveNotification } = this.props

    leaveNotification()
    move('bottom')
    leaveGame()
  }

  getVote = () => {
    const { voting } = this.props
    const { stars } = voting

    return stars.map(item => {
      if (item.active) {
        return (
          <Star
            key={item.id}
            id={item.id}
            active={item.active}
            setVote={this._handlerStarEvent}
            onMouseOverStar={this._handlerStarEvent}
            onMouseLeaveStar={this._handlerStarEvent}
          />
        )
      }
    })
  }

  render() {
    return (
      <>
        <h3 className={s.title}>Thanks for your vote!</h3>
        <div className={s.stars}>{this.getVote()}</div>
        <button
          type='button'
          className={s.leave}
          onClick={this._handleLeave}
        >
          Leave
        </button>
      </>
    )
  }
}

const mapStateToProps = state => ({
  voting: state.Voting
})

const mapActionsToProps = {
  leaveNotification,
  leaveGame,
  move
}

export default connect(mapStateToProps, mapActionsToProps)(Notification)
