import { createAction } from 'redux-actions'
import { handleStar } from '../utils'
import {
  SET_GAME_STATE,
  SET_VOTE,
  RESET_GAME,
  LEAVE_NOTIFICATION,
  ON_MOUSE_OVER_STAR,
  ON_MOUSE_LEAVE_STAR,
  LOAD_GAME_STATE
} from '../actionsTypes'

// ------------------------------------
// Actions
// ------------------------------------
export const setVote = createAction(SET_VOTE, vote => ({ vote }))
export const setGameState = createAction(SET_GAME_STATE, data => ({ data }))
export const resetGame = createAction(RESET_GAME)
export const loadGameState = createAction(LOAD_GAME_STATE)
export const leaveNotification = createAction(LEAVE_NOTIFICATION)
export const onMouseOverStar = createAction(ON_MOUSE_OVER_STAR, id => ({ id }))
export const onMouseLeaveStar = createAction(ON_MOUSE_LEAVE_STAR)

// ------------------------------------
// Action Handlers
// ------------------------------------
export const ACTION_HANDLERS = {
  [ON_MOUSE_OVER_STAR]: (state, action) => {
    return {
      ...state,
      hoverId: action.payload.id,
      stars: handleStar(action.payload.id)
    }
  },

  [ON_MOUSE_LEAVE_STAR]: state => {
    return {
      ...state,
      hoverId: 0,
      stars: handleStar(state.currentUserVote)
    }
  },

  [SET_VOTE]: (state, action) => {
    return {
      ...state,
      currentUserVote: action.payload.vote,
      isVoting: true,
      stars: handleStar(action.payload.vote)
    }
  },

  [LEAVE_NOTIFICATION]: state => {
    return {
      ...state,
      isVoting: false
    }
  },

  [SET_GAME_STATE]: (state, action) => {
    return {
      ...state,
      ...action.payload.data,
      stars: handleStar(action.payload.data.currentUserVote)
    }
  },

  [RESET_GAME]: () => ({
    ...initialState
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  stars: handleStar(0),
  hoverId: 0,
  averageRating: 0,
  currentUserVote: 0,
  isVoting: false
}

export default function votingReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
