import IStar from './IStar'

export default interface IVoting {
  stars: IStar[]
  mapName: string
  averageRating: number
  currentUserVote: number
  isVoting: boolean
  hoverId: number
}
