import React, { Component } from 'react'
import { connect } from 'react-redux'
import Star from './Star'
import IVoting from '../interfaces/IVoting'
import { plural } from '../../../../utils/index'
import { onMouseLeaveStar, onMouseOverStar, setVote } from '../modules'
import { leaveGame, move } from '../../../../modules/index'
import s from '../styles/styles.cssmodule.scss'

interface IProps {
  voting: IVoting
  leaveGame: () => void
  move: (direction: string) => void
  setVote: (id: number) => void
  onMouseLeaveStar: () => void
  onMouseOverStar: (id: number) => void
}

class Vote extends Component<IProps> {
  getStarts = () => {
    const { voting, setVote, onMouseOverStar, onMouseLeaveStar } = this.props
    const { stars } = voting

    return stars.map(item => {
      return (
        <Star
          key={item.id}
          id={item.id}
          active={item.active}
          setVote={setVote}
          onMouseOverStar={onMouseOverStar}
          onMouseLeaveStar={onMouseLeaveStar}
        />
      )
    })
  }

  getAverageRatingMsg = averageRating => {
    return averageRating ? `Average rating ${averageRating} star${plural(averageRating)}` : 'No ratings yet'
  }

  getCurrentUserVoteMsg = stars => {
    return stars ? (
      <>
        <span className={s.currentStars}>
          {stars} star{plural(stars)}
        </span>{' '}
        for the map
      </>
    ) : null
  }

  _handleLeave = () => {
    const { move, leaveGame } = this.props

    move('bottom')
    leaveGame()
  }

  render() {
    const { voting } = this.props
    const { averageRating, mapName, currentUserVote, hoverId } = voting
    const stars = hoverId || currentUserVote
    const averageRatingMsg = this.getAverageRatingMsg(averageRating)
    const currentUserVoteMsg = this.getCurrentUserVoteMsg(stars)

    return (
      <>
        <h2 className={s.title}>Rate {mapName}</h2>
        <p>{averageRatingMsg}</p>
        {this.getStarts()}
        <p className={s.votesMsg}>{currentUserVoteMsg}</p>
        <button type='button' className={s.leave} onClick={this._handleLeave}>
          Leave
        </button>
      </>
    )
  }
}

const mapStateToProps = state => ({
  voting: state.Voting
})

const mapActionsToProps = {
  onMouseLeaveStar,
  onMouseOverStar,
  leaveGame,
  move,
  setVote
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Vote)
