import { MAX_RATE } from '../constants'

export const handleStar = vote => {
  const stars = []

  // tslint:disable-next-line:no-increment-decrement
  for (let i = 0; i < MAX_RATE; i++) {
    stars.push({
      id: i + 1,
      active: i < vote
    })
  }

  return stars
}
