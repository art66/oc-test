import { put, takeEvery } from 'redux-saga/effects'
import { fetchUrl, getErrorMessage } from '../../../../utils'
import { fetchMapData } from '../../../../modules'
import { debugShow } from '../../../../../../controller/actions'
import { setGameState } from '../modules'
import { LOAD_GAME_STATE, SET_VOTE } from '../actionsTypes'

function* setVotes(action: { type: string; payload: { vote: number } }) {
  try {
    const data = yield fetchUrl('miniGameAction', {
      gameType: 'gameVoting',
      action: 'setVote',
      result: {
        vote: action.payload.vote
      }
    })

    yield fetchMapData()
    yield put(setGameState({ averageRating: data.averageRating, currentUserVote: action.payload.vote }))
  } catch (e) {
    yield put(debugShow(getErrorMessage(e)))
  }
}

function* loadGameState() {
  try {
    const data = yield fetchUrl('miniGameAction', {
      gameType: 'gameVoting',
      action: 'getVote'
    })

    yield put(setGameState(data))
  } catch (e) {
    yield put(debugShow(getErrorMessage(e)))
  }
}

export default function* votingSaga() {
  yield takeEvery(SET_VOTE, setVotes)
  yield takeEvery(LOAD_GAME_STATE, loadGameState)
}
