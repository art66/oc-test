export const MAX_RATE = 5
export const ACTIVE_STAR = 'ACTIVE_STAR'
export const INACTIVE_STAR = 'INACTIVE_STAR'
export const ICON_NAME = 'CTStar'
export const PRESET = 'christmasTown'
