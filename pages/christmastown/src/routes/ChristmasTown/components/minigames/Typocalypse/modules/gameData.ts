import { IGameData } from './interfaces'

export const gameData: IGameData = { nextItemStartTimeline: 0 }

export default gameData
