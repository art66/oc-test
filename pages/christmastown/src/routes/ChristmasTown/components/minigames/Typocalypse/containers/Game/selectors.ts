import { IGlobalState } from '../../interfaces'

export const gameAcceptedSelector = (state: IGlobalState) => state.christmastown.minigame.gameAccepted
export const gameEndResultSelector = (state: IGlobalState) => state.christmastown.minigame.gameEndResult
export const gameOverSelector = (state: IGlobalState) => state.Typocalypse.showScore
