import React, { memo, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PreloadFontsHook from '@torn/shared/hooks/PreloadFontsHook'
import PreloadImagesHook from '@torn/shared/hooks/PreloadImagesHook'
import GameStartScreen from '../../../../GameStartScreen'
import ScoreBoard from '../../../../ScoreBoard'
import Board from '../Board'
import styles from './styles.cssmodule.scss'
import { setGameTitle } from '../../../../../modules'
import { resetGameAction } from '../../modules'
import { gameAcceptedSelector, gameEndResultSelector, gameOverSelector } from './selectors'
import { CONVEYOR_IMAGES } from '../../constants/images'

export default memo(function Game() {
  PreloadFontsHook('Mountains of Christmas')
  PreloadImagesHook(CONVEYOR_IMAGES)

  const gameAccepted = useSelector(gameAcceptedSelector)
  const showScore = useSelector(gameOverSelector)
  const gameEndResult = useSelector(gameEndResultSelector)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setGameTitle({ gameTitle: 'Typocalypse' }))

    return () => {
      dispatch(resetGameAction())
    }
  }, [])

  if (!gameAccepted) {
    return <GameStartScreen message='Play Typocalypse?' yesTitle='YES' noTitle='NO' />
  }

  if (showScore) {
    return <ScoreBoard gameName='Typocalypse' title='Typocalypse high scores' {...gameEndResult} />
  }

  return (
    <div className={styles.game}>
      <Board />
    </div>
  )
})
