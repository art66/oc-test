import style from '../components/Gift/styles.cssmodule.scss'

const tooltipElement = document.createElement('div')

tooltipElement.className = style.text

Object.assign(tooltipElement.style, {
  position: 'absolute',
  left: '-500px',
  opacity: 0
})

export function getTextWidth(text: string) {
  document.body.appendChild(tooltipElement)

  tooltipElement.textContent = text

  const { width } = tooltipElement.getBoundingClientRect()

  document.body.removeChild(tooltipElement)

  return width
}
