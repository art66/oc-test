import React, { memo, useEffect, useRef } from 'react'
import cn from 'classnames'
import styles from './styles.cssmodule.scss'
import { IProps } from './interfaces'
import timeline from '../../helpers/timeline'
import { SPEED } from '../../constants'
import { CONVEYOR_IMAGES_LENGTH } from '../../constants/images'

export default memo(function Conveyor(props: IProps) {
  const { className, right, mediaType } = props

  const ref = useRef(null)

  const classNames = cn(styles.conveyor, !right ? styles.left : styles.right, className)

  useEffect(() => {
    const element = ref.current
    const framesCount = CONVEYOR_IMAGES_LENGTH
    const { PX_S_SPEED } = SPEED[mediaType]

    function onSpeedChange() {
      const animationDuration = (framesCount / (PX_S_SPEED * timeline.speed)).toFixed(3)

      element.style.animationDuration = `${animationDuration}s`
    }

    onSpeedChange()

    timeline.onSpeedChange(onSpeedChange)

    return () => {
      timeline.offSpeedChange(onSpeedChange)
    }
  }, [])

  return <div ref={ref} className={classNames} />
})
