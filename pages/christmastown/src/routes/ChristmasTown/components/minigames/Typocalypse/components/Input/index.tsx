import React, { memo, useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { winItemAction } from '../../modules'
import { itemsSelector } from '../../modules/selectors'
import styles from './styles.cssmodule.scss'

export default memo(() => {
  const [value, setValue] = useState('')

  const items = useSelector(itemsSelector)

  const dispatch = useDispatch()

  const onInput = useCallback(
    event => {
      const inputText = event.target.value
      const normalizedInputText = inputText.trim().toLowerCase()

      let matchItem

      for (let i = items.length - 1; i >= 0; i -= 1) {
        const item = items[i]

        if (item.textMatch === normalizedInputText) {
          matchItem = item

          break
        }
      }

      if (matchItem) {
        dispatch(winItemAction(matchItem.id))

        setValue('')
      } else {
        setValue(inputText)
      }
    },
    [items]
  )

  return (
    <input className={styles.input} type='text' value={value} onChange={onInput} autoComplete='off' autoFocus={true} />
  )
})
