import { TMediaType } from '../../modules/interfaces'

export interface IProps {
  mediaType: TMediaType
  className?: string
  right?: boolean
}
