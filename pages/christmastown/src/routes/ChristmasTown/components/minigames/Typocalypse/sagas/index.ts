import { put, select, takeLatest } from 'redux-saga/effects'
import { fetchUrl, getErrorMessage } from '../../../../utils'
import { debugShow } from '../../../../../../controller/actions'
import {
  getWordsFailureAction,
  getWordsRequestAction,
  getWordsSuccessAction,
  getResultRequestAction,
  getResultSuccessAction,
  getResultFailureAction,
  showScoreAction
} from '../modules'
import { wordsResponseNormalizer } from './normlizers'
import { scoreSelector } from '../modules/selectors'
import { setGameEndResult } from '../../../../modules'

function* getWordsSaga() {
  try {
    const response = yield fetchUrl('miniGameAction', {
      gameType: 'gameTypocalypse',
      action: 'start'
    })

    const normalizedWords = wordsResponseNormalizer(response)

    yield put(getWordsSuccessAction(normalizedWords))
  } catch (error) {
    yield put(getWordsFailureAction())

    yield put(debugShow(getErrorMessage(error)))
  }
}

function* getResultSaga() {
  const score = yield select(scoreSelector)

  try {
    const response = yield fetchUrl('miniGameAction', {
      gameType: 'gameTypocalypse',
      action: 'complete',
      result: {
        points: score
      }
    })

    yield put(setGameEndResult(response))
    yield put(getResultSuccessAction())
    yield put(showScoreAction())
  } catch (error) {
    yield put(getResultFailureAction())

    yield put(debugShow(getErrorMessage(error)))
  }
}

export default function* rootSaga() {
  yield takeLatest(getWordsRequestAction.type, getWordsSaga)
  yield takeLatest(getResultRequestAction.type, getResultSaga)
}
