import TimelineExtends from './TimelineExtends'
import normalizeFloatNumber from './normalizeFloatNumber'
import { TIMELINE_DEFAULT_SPEED } from './constants'

export default class Timeline extends TimelineExtends {
  public time = 0
  public timeline = 0
  public play = false
  private lastTime: number = null

  public start = () => {
    const { handleFrame } = this

    this.play = true
    this.lastTime = Date.now()

    handleFrame()
  }

  public stop = () => {
    this.play = false
  }

  public end = () => {
    this.stop()

    this.speed = TIMELINE_DEFAULT_SPEED
    this.time = 0
    this.timeline = 0
    this.play = false
    this.lastTime = null
  }

  private handleFrame = () => {
    const { lastTime, play, handleFrame, emit } = this

    if (play) {
      const now = Date.now()

      const currentTimeDifference = now - lastTime

      this.time += currentTimeDifference
      this.timeline += normalizeFloatNumber(currentTimeDifference * this.speed)

      this.lastTime = now

      emit()

      requestAnimationFrame(handleFrame)
    }
  }
}
