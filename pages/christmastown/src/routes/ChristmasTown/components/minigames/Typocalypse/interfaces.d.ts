import { IState, TMediaType } from './modules/interfaces'

export interface IWord {
  id: string
  text: string
  textMatch: string
}

export interface IStepDistance {
  x?: number
  y?: number
  deg?: number
  primaryProp?: string
}

export type TStepCoords = [IStepDistance, IStepDistance]

export interface IGlobalState {
  Typocalypse: IState
  christmastown: {
    minigame: {
      gameAccepted: boolean
      gameEndResult: {
        [prop: string]: string
      }
    }
  }
  browser: {
    mediaType: TMediaType
  }
}
