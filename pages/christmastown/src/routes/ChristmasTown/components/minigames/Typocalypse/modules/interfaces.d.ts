import { IWord } from '../interfaces'

export type TMediaType = 'desktop' | 'tablet' | 'mobile'

export interface IItem {
  id: string
  text: string
  imgSrc: string
  width: number
  textMatch: string
  startTimeline: number
}

export interface IState {
  items: IItem[]
  nextItem: IItem
  winItemIds: string[]
  words: IWord[]
  usedWordsCount: number
  wordsLoaded: boolean
  score: number
  showScore: boolean
}

export interface IGameData {
  nextItemStartTimeline: IItem['startTimeline']
}
