import React, { memo, useEffect, useRef, useState } from 'react'
import cn from 'classnames'
import styles from './styles.cssmodule.scss'
import { IProps } from './interfaces'
import timeline from '../../helpers/timeline'
import STEPS, { STEPS_COUNT } from './constants'
import normalizeFloatNumber from '../../utils/normalizeFloatNumber'

export default memo(function Gift(props: IProps) {
  const { id, text, startTimeline, imgSrc, isHiding, mediaType, onEnd, onHideEnd } = props

  const giftRef = useRef(null)
  const imageRef = useRef(null)
  const isHidingRef = useRef(isHiding)

  useEffect(() => {
    isHidingRef.current = isHiding
  }, [isHiding])

  const [isFinal, setIsFinal] = useState(false)

  const onTeleportEnd = () => {
    onHideEnd(id)
  }

  useEffect(() => {
    const element = giftRef.current
    const imageElement = imageRef.current
    const { STEPS_DISTANCES, STEPS_TIMES } = STEPS[mediaType]
    let step = 0
    let lastStepTime = startTimeline
    let stepDistances, stepTime, startCoords, endCoords

    function setStyle([x, y, deg]: [number, number, number]) {
      element.style.top = `${y}px`
      element.style.left = `${x}px`
      imageElement.style.transform = `rotateZ(${deg}deg)`
    }

    function updateStepParams() {
      stepDistances = STEPS_DISTANCES[step]
      stepTime = STEPS_TIMES[step]
      startCoords = stepDistances[0]
      endCoords = stepDistances[1]
    }

    function getCoords(animationPoint: number) {
      const x = normalizeFloatNumber(startCoords.x + (endCoords.x - startCoords.x) * animationPoint)
      const y = normalizeFloatNumber(startCoords.y + (endCoords.y - startCoords.y) * animationPoint)
      const deg = normalizeFloatNumber(startCoords.deg + (endCoords.deg - startCoords.deg) * animationPoint)

      return [x, y, deg] as [number, number, number]
    }

    updateStepParams()

    function onTimeChange() {
      if (isHidingRef.current) {
        timeline.off(onTimeChange)
      }

      const animationFramePoint = normalizeFloatNumber((timeline.timeline - lastStepTime) / stepTime, 3)

      if (animationFramePoint < 1) {
        const coords = getCoords(animationFramePoint)

        setStyle(coords)
      } else {
        setStyle(endCoords)

        if (step < STEPS_COUNT) {
          step += 1

          updateStepParams()

          lastStepTime = timeline.timeline

          if (step === STEPS_COUNT - 1) {
            setIsFinal(true)
          }
        } else {
          onEnd()
        }
      }
    }

    timeline.on(onTimeChange)

    return () => {
      timeline.off(onTimeChange)
    }
  }, [])

  const classNames = cn(styles.gift, {
    [styles.final]: isFinal,
    [styles.hide]: isHiding
  })

  return (
    <div ref={giftRef} className={classNames}>
      <div className={styles.content}>
        <div className={styles.tooltip}>
          <div className={styles.text}>{text}</div>
          <div className={styles.arrow} />
        </div>
        <div ref={imageRef} className={styles.imageContainer}>
          <img className={styles.image} src={imgSrc} alt={text} />
          {isFinal && <div className={styles.finalLight} />}
        </div>
        {isHiding && <div className={styles.teleport} onAnimationEnd={onTeleportEnd} />}
      </div>
    </div>
  )
})
