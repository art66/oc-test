import React, { memo, useCallback, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styles from './styles.cssmodule.scss'
import Gift from '../../components/Gift'
import Conveyor from '../../components/Conveyor'
import {
  addItemAction,
  createItemAction,
  gameOverAction,
  getWordsRequestAction,
  removeItemAction,
  removeWinAnimationItemAction
} from '../../modules'
import { itemsSelector, winItemIdsSelector, wordsLoadedSelector, mediaTypeSelector } from '../../modules/selectors'
import Input from '../../components/Input'
import timeline from '../../helpers/timeline'
import gameData from '../../modules/gameData'
import { INCREASE_SPEED_TIME } from '../../constants'

export default memo(() => {
  const items = useSelector(itemsSelector)
  const winItemIds = useSelector(winItemIdsSelector)
  const wordsLoaded = useSelector(wordsLoadedSelector)
  const mediaType = useSelector(mediaTypeSelector)

  const lastSpeedIncreaseTimeRef = useRef(INCREASE_SPEED_TIME)

  const dispatch = useDispatch()

  const onGameOver = useCallback(() => {
    dispatch(gameOverAction())

    timeline.end()

    lastSpeedIncreaseTimeRef.current = INCREASE_SPEED_TIME
  }, [])
  const onRemoveItemAction = useCallback(id => {
    dispatch(removeWinAnimationItemAction(id))
    dispatch(removeItemAction(id))
  }, [])

  useEffect(() => {
    dispatch(getWordsRequestAction())

    function onTimeChange() {
      if (timeline.timeline > gameData.nextItemStartTimeline) {
        dispatch(addItemAction())
        dispatch(createItemAction())
      }

      if (timeline.time > lastSpeedIncreaseTimeRef.current + INCREASE_SPEED_TIME) {
        timeline.increaseSpeed(mediaType)

        lastSpeedIncreaseTimeRef.current += INCREASE_SPEED_TIME
      }
    }

    timeline.on(onTimeChange)

    return () => {
      timeline.off(onTimeChange)

      timeline.end()
    }
  }, [])

  useEffect(() => {
    if (wordsLoaded) {
      dispatch(createItemAction())

      timeline.start()
    }
  }, [wordsLoaded])

  return (
    <div className={styles.board}>
      {items.map(item => {
        const isHiding = winItemIds.includes(item.id)

        return (
          <Gift
            key={item.id}
            {...item}
            isHiding={isHiding}
            mediaType={mediaType}
            onEnd={onGameOver}
            onHideEnd={onRemoveItemAction}
          />
        )
      })}
      <Conveyor className={styles.сonveyor1} mediaType={mediaType} />
      <Conveyor className={styles.сonveyor2} right={true} mediaType={mediaType} />
      <Input />
    </div>
  )
})
