import random from 'lodash.random'
import {
  GIFTS_IMAGES,
  GIFTS_LENGTH,
  LITTLE_GIFTS_LENGTH,
  MEDIUM_GIFTS_LENGTH,
  LARGE_GIFTS_LENGTH
} from '../constants/images'

export function getImg() {
  const imgIndex = random(0, GIFTS_LENGTH - 1)
  const src = GIFTS_IMAGES[imgIndex]

  let width

  if (imgIndex <= LITTLE_GIFTS_LENGTH - 1) {
    width = 30
  } else if (imgIndex <= LITTLE_GIFTS_LENGTH - 1 + MEDIUM_GIFTS_LENGTH - 1) {
    width = 50
  } else if (imgIndex <= LITTLE_GIFTS_LENGTH - 1 + MEDIUM_GIFTS_LENGTH - 1 + LARGE_GIFTS_LENGTH - 1) {
    width = 70
  }

  return {
    src,
    width
  }
}
