import { IItem } from '../../modules/interfaces'

export interface IProps extends IItem {
  isHiding: boolean
  mediaType: string
  onEnd: () => void
  onHideEnd: (id: string) => void
}
