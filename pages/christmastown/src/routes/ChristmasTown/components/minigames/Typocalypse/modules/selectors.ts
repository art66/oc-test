import { createSelector } from 'reselect'
import { IGlobalState } from '../interfaces'

export const mediaTypeSelector = (state: IGlobalState) => state.browser.mediaType
export const wordsSelector = (state: IGlobalState) => state.Typocalypse.words
export const wordsLoadedSelector = (state: IGlobalState) => state.Typocalypse.wordsLoaded
export const itemsSelector = (state: IGlobalState) => state.Typocalypse.items
export const scoreSelector = (state: IGlobalState) => state.Typocalypse.score
export const winItemIdsSelector = (state: IGlobalState) => state.Typocalypse.winItemIds
export const usedWordsCountSelector = (state: IGlobalState) => state.Typocalypse.usedWordsCount
export const nextItemSelector = (state: IGlobalState) => state.Typocalypse.nextItem

export const nextWordSelector = createSelector([wordsSelector, usedWordsCountSelector], (words, usedWordsCount) => {
  return words[usedWordsCount]
})
