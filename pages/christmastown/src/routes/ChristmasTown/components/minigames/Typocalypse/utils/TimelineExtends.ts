import normalizeFloatNumber from './normalizeFloatNumber'
import Subscriber from './Subscriber'
import { TIMELINE_DEFAULT_SPEED, TIMELINE_SPEED_MULTIPLIER } from './constants'
import { MEDIA_TYPE_SPEED_MULTIPLIERS } from '../constants'
import { TMediaType } from '../modules/interfaces'

export default class TimelineExtends extends Subscriber {
  public speedValue: number = TIMELINE_DEFAULT_SPEED
  public speedMultipliers: { [key: string]: number } = {
    desktop: TIMELINE_SPEED_MULTIPLIER * MEDIA_TYPE_SPEED_MULTIPLIERS.desktop,
    tablet: TIMELINE_SPEED_MULTIPLIER * MEDIA_TYPE_SPEED_MULTIPLIERS.tablet,
    mobile: TIMELINE_SPEED_MULTIPLIER * MEDIA_TYPE_SPEED_MULTIPLIERS.mobile
  }

  private speedSubscriber: Subscriber = new Subscriber()

  onSpeedChange = listener => {
    this.speedSubscriber.on(listener)
  }

  offSpeedChange = listener => {
    this.speedSubscriber.off(listener)
  }

  public set speed(speed) {
    this.speedValue = speed

    this.speedSubscriber.emit(this.speedValue)
  }

  public get speed() {
    return this.speedValue
  }

  public increaseSpeed = (mediaType: TMediaType) => {
    this.speed = normalizeFloatNumber(this.speed + this.speedMultipliers[mediaType] / this.speed, 2)
  }
}
