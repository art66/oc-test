export default class Subscriber {
  private listeners = []

  on = listener => {
    this.listeners.push(listener)
  }

  off = listener => {
    const index = this.listeners.indexOf(listener)

    if (index === -1) return

    this.listeners.splice(index, 1)
  }

  emit = (...args) => {
    const { listeners } = this

    listeners.forEach(listener => {
      listener(...args)
    })
  }
}
