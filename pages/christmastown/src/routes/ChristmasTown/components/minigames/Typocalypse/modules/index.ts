import { createAction, createReducer, PayloadAction } from '@reduxjs/toolkit'
import random from 'lodash.random'
import { MAX_ITEMS_LENGTH, SPEED, ITEMS_MIN_DISTANCE, ITEMS_MAX_DISTANCE } from '../constants'
import timeline from '../helpers/timeline'
import { IWord } from '../interfaces'
import normalizeFloatNumber from '../utils/normalizeFloatNumber'
import { getImg } from '../utils/getImg'
import { getTextWidth } from '../utils/getTextWidth'
import gameData from './gameData'
import { IItem, IState } from './interfaces'
import {
  wordsSelector,
  usedWordsCountSelector,
  nextWordSelector,
  nextItemSelector,
  mediaTypeSelector
} from './selectors'

export const showScoreAction = createAction('TYPOCALYPSE_GAME_OVER')
export const resetGameAction = createAction('TYPOCALYPSE_RESET_GAME')

export const increaseSpeedAction = createAction('TYPOCALYPSE_INCREASE_SPEED')

export const getWordsRequestAction = createAction('TYPOCALYPSE_GET_WORDS_REQUEST')
export const getWordsSuccessAction = createAction<IWord[]>('TYPOCALYPSE_GET_WORDS_SUCCESS')
export const getWordsFailureAction = createAction('TYPOCALYPSE_GET_WORDS_FAILURE')

export const getResultRequestAction = createAction('TYPOCALYPSE_GET_RESULT_REQUEST')
export const getResultSuccessAction = createAction('TYPOCALYPSE_GET_RESULT_SUCCESS')
export const getResultFailureAction = createAction('TYPOCALYPSE_GET_RESULT_FAILURE')

export const addNextItemAction = createAction<IItem>('TYPOCALYPSE_ADD_NEXT_ITEM')
export const addItemAction = createAction<IItem>('TYPOCALYPSE_ADD_ITEM')

export function createItemHelper(state) {
  const mediaType = mediaTypeSelector(state)
  const nextItem = nextItemSelector(state)
  const { id, text } = nextWordSelector(state)
  const textWidth = getTextWidth(text)
  const { width: imgWidth, src: imgSrc } = getImg()

  const width = imgWidth > textWidth ? imgWidth : textWidth
  const minDistance = (width + nextItem.width) / 2
  const randomDistance = random(ITEMS_MIN_DISTANCE, ITEMS_MAX_DISTANCE)
  const startTimeline = normalizeFloatNumber(
    timeline.timeline + ((minDistance + randomDistance) / SPEED[mediaType].PX_S_SPEED) * 1000
  )

  return {
    id,
    text,
    textMatch: text,
    imgSrc,
    width,
    startTimeline
  }
}

export function createItemAction() {
  return (dispatch, getState) => {
    const state = getState()
    const words = wordsSelector(state)
    const usedWordsCount = usedWordsCountSelector(state)

    const item = createItemHelper(state)

    dispatch(addNextItemAction(item))

    gameData.nextItemStartTimeline = item.startTimeline

    if (usedWordsCount > words.length - MAX_ITEMS_LENGTH) {
      dispatch(getWordsRequestAction())
    }
  }
}

export const increaseScoreAction = createAction<string>('TYPOCALYPSE_INCREASE_SCORE_ITEM')

export const winItemAction = (id: string) => {
  return dispatch => {
    dispatch(increaseScoreAction())
    dispatch(addWinAnimationItemAction(id))
  }
}

export const gameOverAction = () => {
  return dispatch => {
    dispatch(getResultRequestAction())

    dispatch(resetGameAction())
  }
}
export const removeItemAction = createAction<string>('TYPOCALYPSE_REMOVE_ITEM')
export const addWinAnimationItemAction = createAction<string>('TYPOCALYPSE_ADD_WIN_ITEM')
export const removeWinAnimationItemAction = createAction<string>('TYPOCALYPSE_REMOVE_WIN_ITEM')

const initialState: IState = {
  words: [],
  wordsLoaded: false,
  usedWordsCount: 0,
  items: [],
  nextItem: { id: null, width: 0, text: '', textMatch: '', imgSrc: null, startTimeline: 0 },
  winItemIds: [],
  score: 0,
  showScore: false
}

export const ACTION_HANDLERS = {
  [getWordsSuccessAction.type]: (state: IState, action: PayloadAction<IWord[]>) => {
    state.words = [...state.words, ...action.payload]

    state.wordsLoaded = true
  },
  [showScoreAction.type]: (state: IState) => {
    state.showScore = true
  },
  [addNextItemAction.type]: (state: IState, action: PayloadAction<IItem>) => {
    const nextItem = action.payload

    state.nextItem = nextItem
    state.usedWordsCount += 1
  },
  [addItemAction.type]: (state: IState) => {
    const item = state.nextItem

    state.items.unshift(item)
  },
  [removeItemAction.type]: (state: IState, action: PayloadAction<string>) => {
    const itemIndex = state.items.findIndex(item => item.id === action.payload)

    state.items.splice(itemIndex, 1)
  },
  [addWinAnimationItemAction.type]: (state: IState, action: PayloadAction<string>) => {
    state.winItemIds.push(action.payload)
  },
  [removeWinAnimationItemAction.type]: (state: IState, action: PayloadAction<string>) => {
    state.winItemIds.splice(state.winItemIds.indexOf(action.payload), 1)
  },
  [increaseScoreAction.type]: (state: IState) => {
    state.score += 1
  },
  [resetGameAction.type]: (state: IState) => {
    Object.assign(state, initialState)
  }
}

export default createReducer(initialState, ACTION_HANDLERS)
