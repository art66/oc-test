export default (() => {
  let id = 0

  return function createId() {
    const currentId = id

    id += 1

    return currentId
  }
})()
