import createItemId from '../utils/createId'
import { IWord } from '../interfaces'

export function wordsResponseNormalizer(response: { progress: { words } }): IWord[] {
  return response.progress.words.map(word => {
    return {
      id: createItemId(),
      text: word,
      textMatch: word.toLowerCase()
    }
  })
}
