export default function normalizeFloatNumber(number, toFixed = 1) {
  return parseFloat(number.toFixed(toFixed))
}
