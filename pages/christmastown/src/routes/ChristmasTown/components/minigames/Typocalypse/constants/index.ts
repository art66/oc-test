export const DESKTOP_LAYOUT_TYPE = 'desktop'
export const TABLET_LAYOUT_TYPE = 'tablet'
export const MOBILE_LAYOUT_TYPE = 'mobile'

export const MAX_ITEMS_LENGTH = 10

export const INCREASE_SPEED_TIME = 5000

export function getSpeed(multiplier) {
  const PX_S_SPEED = 50 * multiplier
  const DEG_S_SPEED = 40 * multiplier
  const PX_MS_SPEED = PX_S_SPEED / 1000
  const DEG_MS_SPEED = DEG_S_SPEED / 1000

  return {
    PX_S_SPEED,
    DEG_S_SPEED,
    PX_MS_SPEED,
    DEG_MS_SPEED
  }
}

export const MEDIA_TYPE_SPEED_MULTIPLIERS = {
  desktop: 1,
  tablet: 0.9,
  mobile: 0.6
}

export const DESKTOP_SPEED = getSpeed(MEDIA_TYPE_SPEED_MULTIPLIERS.desktop)
export const TABLET_SPEED = getSpeed(MEDIA_TYPE_SPEED_MULTIPLIERS.tablet)
export const MOBILE_SPEED = getSpeed(MEDIA_TYPE_SPEED_MULTIPLIERS.mobile)

export const SPEED = {
  desktop: DESKTOP_SPEED,
  tablet: TABLET_SPEED,
  mobile: MOBILE_SPEED
}

export const ITEMS_MIN_DISTANCE = 5
export const ITEMS_MAX_DISTANCE = 20
