import { createCookies, getRandomElement } from './helpers'
import { getEndResult } from '../../../../modules'
import { COOKIE_MIN_SPEED, COOKIE_MAX_SPEED } from '../constants'

// ------------------------------------
// Constants
// ------------------------------------
const CREATE_GAMEBOARD = 'CREATE_GAMEBOARD'
const SET_WINDOW_SIZE = 'SET_WINDOW_SIZE'
const SET_MOVING_BLOCK = 'SET_MOVING_BLOCK'
const SET_BASKET = 'SET_BASKET'
const ADD_POINT = 'ADD_POINT'
const DECREASE_LIVES = 'DECREASE_LIVES'
const GAME_OVER = 'GAME_OVER'
const RESET_GAME = 'RESET_GAME'
const SET_FALLING_COOKIE = 'SET_FALLING_COOKIE'

// ------------------------------------
// Actions
// ------------------------------------
export const _createGameBoard = () => ({
  type: CREATE_GAMEBOARD
})

export const setFallingCookie = index => ({
  type: SET_FALLING_COOKIE,
  index
})

export const setMovingBlock = movingBlock => {
  return {
    type: SET_MOVING_BLOCK,
    movingBlock
  }
}

export const setBasket = basket => {
  return {
    type: SET_BASKET,
    basket
  }
}

export const addPoint = () => {
  return {
    type: ADD_POINT
  }
}

const _decreaseLives = () => {
  return {
    type: DECREASE_LIVES
  }
}

const _gameOver = () => {
  return {
    type: GAME_OVER
  }
}

export const resetGame = () => {
  return {
    type: RESET_GAME
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
export const ACTION_HANDLERS = {
  [CREATE_GAMEBOARD]: state => {
    return {
      ...state,
      gameStarted: true,
      cookies: createCookies()
    }
  },
  [SET_WINDOW_SIZE]: (state, action) => {
    return {
      ...state,
      windowSize: action.windowSize
    }
  },
  [SET_FALLING_COOKIE]: (state, action) => {
    return {
      ...state,
      activeCookie: action.index
    }
  },
  [SET_MOVING_BLOCK]: (state, action) => {
    return {
      ...state,
      movingBlock: action.movingBlock
    }
  },
  [SET_BASKET]: (state, action) => {
    return {
      ...state,
      basket: action.basket
    }
  },
  [ADD_POINT]: (state) => {
    const acceleration = (state.speed - COOKIE_MAX_SPEED) / 16

    return {
      ...state,
      points: state.points + 1,
      speed: state.speed - acceleration
    }
  },
  [DECREASE_LIVES]: state => {
    return {
      ...state,
      lives: state.lives - 1
    }
  },
  [GAME_OVER]: state => {
    return {
      ...state,
      gameOver: true,
      gameStarted: false
    }
  },
  [RESET_GAME]: () => {
    return {
      ...initialState
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  cookies: [],
  gameOver: false,
  gameStarted: false,
  points: 0,
  lives: 3,
  speed: COOKIE_MIN_SPEED,
  activeCookie: null
}

export default function gingerBreadMenReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

let gameTimeInterval
export const createGameBoard = () => dispatch => {
  let cookies = createCookies()
  dispatch(_createGameBoard(cookies))
}

export const getRandomCookie = () => (dispatch, getState) => {
  let cookies = getState().GingerBreadMen.cookies
  let currentActiveCookie = getState().GingerBreadMen.activeCookie

  let randomCookie = getRandomElement(cookies)

  while (randomCookie === currentActiveCookie) {
    randomCookie = getRandomElement(cookies)
  }

  dispatch(setFallingCookie(randomCookie))
}

export const decreaseLives = () => (dispatch, getState) => {
  let lives = getState().GingerBreadMen.lives
  dispatch(_decreaseLives())
  if (lives === 1) {
    let points = getState().GingerBreadMen.points
    dispatch(getPrize(points))
  }
}

const getPrize = points => dispatch => {
  dispatch(_gameOver())
  clearInterval(gameTimeInterval)
  let data = {
    action: 'complete',
    gameType: 'gameGingerBreadMen',
    result: {
      points: points
    }
  }
  dispatch(getEndResult(data))
}
