import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { connect } from 'react-redux'
import s from './styles.cssmodule.scss'
import { cookie } from './images.json'

import { addPoint, decreaseLives, getRandomCookie } from './modules'

const SPRITE_STEPS = 10
const SPRITE_WIDTH = 62
const SPRITE_HEIGHT = 74

export class Cookie extends Component {
  constructor(props) {
    super(props)
    this.state = {
      spritePosition: 0,
      top: this.props.options.top,
      active: false
    }
    this.updateImage = this.updateImage.bind(this)
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  componentDidMount() {
    const { activeCookie, options: { index } = {} } = this.props
    const { active } = this.state

    if (index === activeCookie && !active) {
      this.updateCookie()
    }
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) === JSON.stringify(this.props)) {
      return
    }

    const { options: { index } = {} } = prevProps
    const { activeCookie } = this.props
    const { active } = this.state

    if (index === activeCookie && !active) {
      this.updateCookie()
    }
  }

  componentWillUnmount() {
    clearInterval(this.updateImageInterval)
    clearInterval(this.fallInterval)
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps(nextProps) {
  //   if (this.props.options.index === nextProps.activeCookie && !this.state.active) {
  //     this.setState({
  //       active: true
  //     })
  //     this.updateCookieImage()
  //   }
  // }

  updateCookie() {
    this.setState({ // eslint-disable-line
      active: true
    })
    this.updateCookieImage()
  }

  resetCookie() {
    clearInterval(this.updateImageInterval)
    clearInterval(this.fallInterval)
    const { options } = this.props

    this.setState({
      spritePosition: 0,
      top: options.top,
      active: false
    })
  }

  updateCookieImage() {
    clearInterval(this.updateImageInterval)
    this.spritePosition = 0
    this.step = 1
    this.updateImage()
    this.updateImageInterval = setInterval(this.updateImage, this.props.speed * 1000 / SPRITE_STEPS)
  }

  updateImage() {
    if (this.step === SPRITE_STEPS) {
      clearInterval(this.updateImageInterval)
      this.fallCookie()
    } else {
      this.spritePosition -= SPRITE_WIDTH
      this.step++
      this.setState({
        spritePosition: this.spritePosition
      })
    }
  }

  fallCookie() {
    const floor = this.props.movingBlock.height
    const distance = floor - this.props.options.top

    this.setState({
      active: true
    })
    this.fallInterval = setInterval(() => {
      if (this.state.top >= floor) {
        this.resetCookie()
        this.props.getRandomCookie()
        this.props.decreaseLives()
      } else {
        this.setState({
          top: this.state.top + distance / (this.props.speed * 1000) * 16
        })
        this.checkCollision()
      }
    }, 16)
  }

  checkCollision() {
    const cookieTop = this.state.top + SPRITE_HEIGHT
    const cookieLeft = this.props.options.left
    const cookieRight = this.props.options.left + SPRITE_WIDTH
    const basketWidth = 102
    const basketLeft = this.props.basket - basketWidth / 2
    const basketRight = this.props.basket + basketWidth / 2
    const basketTop = 360
    const cookieTopOffset = SPRITE_HEIGHT / 3
    const cookieWidthOffset = (cookieRight - cookieLeft) / 3

    if (
      cookieTop >= basketTop + cookieTopOffset
      && cookieTop <= basketTop + cookieTopOffset * 2
      && basketLeft <= cookieLeft + cookieWidthOffset
      && basketRight >= cookieRight - cookieWidthOffset
    ) {
      this.resetCookie()
      this.props.getRandomCookie()
      this.props.addPoint()
    }
  }

  render() {
    const cookieStyle = {
      top: `${this.state.top}px`,
      left: `${this.props.options.left}px`,
      backgroundImage: `url(${cookie})`,
      backgroundPositionX: `${this.state.spritePosition}px`
    }
    const c = classnames({
      [s['cookie']]: true,
      [s['falling']]: this.state.active
    })

    return <div className={c} style={cookieStyle} />
  }
}

Cookie.propTypes = {
  options: PropTypes.object,
  index: PropTypes.number,
  addPoint: PropTypes.func,
  decreaseLives: PropTypes.func,
  getRandomCookie: PropTypes.func,
  movingBlock: PropTypes.object,
  basket: PropTypes.number,
  speed: PropTypes.number,
  activeCookie: PropTypes.number
}

const mapStateToProps = state => ({
  movingBlock: state.GingerBreadMen.movingBlock,
  basket: state.GingerBreadMen.basket,
  speed: state.GingerBreadMen.speed,
  activeCookie: state.GingerBreadMen.activeCookie
})

const mapActionsToProps = {
  decreaseLives,
  addPoint,
  getRandomCookie
}

export default connect(mapStateToProps, mapActionsToProps)(Cookie)
