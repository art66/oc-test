import { cookieXPosition, rowsPositions, COOKIES_AMOUNT } from '../constants'

export function getMaxTime(cookiesArray) {
  let maxFryingTime = 0

  cookiesArray.forEach(cookie => {
    if (cookie.fryingTime) {
      maxFryingTime = maxFryingTime > cookie.fryingTime ? maxFryingTime : cookie.fryingTime
    }
  })
  return maxFryingTime
}

const createIndexes = indexes => {
  return [...(new Array(indexes)).fill(0)].map((_, i) => {
    return i
  })
}

const createCookiesRow = position => {
  return new Array(5).fill({}).map((cookie, index) => {
    return {
      ...cookie,
      left: {
        desktop: cookieXPosition.desktop[index],
        tablet: cookieXPosition.tablet[index],
        mobile: cookieXPosition.mobile[index]
      },
      top: rowsPositions[position]
    }
  })
}

let topRow = createCookiesRow('top')
let middleRow = createCookiesRow('middle')
let bottomRow = createCookiesRow('bottom')

export const createCookies = () => {
  let cookiesIndexes = createIndexes(COOKIES_AMOUNT)

  let tempArray = []
  let cookies = [...topRow, ...middleRow, ...bottomRow]

  while (tempArray.length < COOKIES_AMOUNT) {
    let randomCookieIndex = getRandomElement(cookiesIndexes)

    tempArray.push(cookies[cookiesIndexes.splice(randomCookieIndex, 1)[0]])
  }
  return tempArray
}

export const getRandomElement = array => {
  return Math.floor(Math.random() * array.length)
}
