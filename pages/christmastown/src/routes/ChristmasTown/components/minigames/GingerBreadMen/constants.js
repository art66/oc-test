export const COOKIES_AMOUNT = 15
export const COOKIE_MIN_SPEED = 1.8
export const COOKIE_MAX_SPEED = 0.1

export const tray = {
  'tray-desktop': {
    width: 453,
    height: 252
  },
  'tray-tablet': {
    width: 366,
    height: 252
  },
  'tray-mobile': {
    width: 320,
    height: 252
  }
}

export const cookieXPosition = {
  desktop: [44, 120, 196, 272, 348],
  tablet: [22, 92, 162, 232, 302],
  mobile: [14, 72, 130, 188, 246]
}

export const rowsPositions = {
  top: 10,
  middle: 88,
  bottom: 166
}
