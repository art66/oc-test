import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { AnimationDuration } from '../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import { setGameTitle } from '../../../modules'
import GameStartScreen from '../../GameStartScreen'
import ScoreBoard from '../../ScoreBoard'
import Board from './Board'
import reducer, { resetGame } from './modules'
import styles from '../../../hoc/withMinigameWrapper/minigameWrapper.cssmodule.scss'
import slideAnimation from '../../../hoc/withMinigameWrapper/slideAnimation.cssmodule.scss'
import s from './styles.cssmodule.scss'

export { reducer }

export class GingerBreadMen extends Component {
  componentWillUnmount() {
    this.props.resetGame()
  }

  componentDidMount() {
    this.props.setGameTitle({
      gameTitle: 'Gingerbread Men'
    })
  }

  selectComponent() {
    const { gameAccepted, gameEndResult } = this.props

    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen message='Play Gingerbread Men?' yesTitle='YES' noTitle='NO' />
        </CSSTransition>
      )
    }

    if (this.props.gameOver) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName='GingerBreadMen' title='Gingerbread Men high scores' {...gameEndResult} />
        </CSSTransition>
      )
    }

    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='WrappedComponent'>
        <div className={s['wrap']}>
          <Board />
        </div>
      </CSSTransition>
    )
  }

  render() {
    return <TransitionGroup className={styles.ctMiniGameWrapper}>{this.selectComponent()}</TransitionGroup>
  }
}

GingerBreadMen.propTypes = {
  gameAccepted: PropTypes.bool,
  gameEndResult: PropTypes.object,
  setGameTitle: PropTypes.func,
  resetGame: PropTypes.func,
  gameOver: PropTypes.bool
}

const mapStateToProps = (state) => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  gameOver: state.GingerBreadMen.gameOver
})

const mapActionsToProps = {
  setGameTitle,
  resetGame
}

export default connect(mapStateToProps, mapActionsToProps)(GingerBreadMen)
