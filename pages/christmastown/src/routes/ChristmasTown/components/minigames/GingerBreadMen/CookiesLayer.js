import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Cookie from './Cookie'
import s from './styles.cssmodule.scss'

import { setMovingBlock, setBasket, createGameBoard, getRandomCookie } from './modules'

export class CookiesLayer extends Component {
  constructor(props) {
    super(props)
    this.getCookiesLayer = this.getCookiesLayer.bind(this)
    this.createCookies = this.createCookies.bind(this)
    this.moveBasket = this.moveBasket.bind(this)
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  //   this.props.createGameBoard()
  //   this.props.getRandomCookie()
  // }

  componentDidMount() {
    this.props.createGameBoard()
    this.props.getRandomCookie()

    this.getCookiesLayer()
    window.addEventListener('resize', this.getCookiesLayer, false)
    this.basketTop = this.refs.basketTop
    this.basketBottom = this.refs.basketBottom
    this.props.setBasket(this.basketTop.offsetLeft)
    window.addEventListener('mousemove', this.moveBasket)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.getCookiesLayer, false)
    window.removeEventListener('mousemove', this.moveBasket, false)
  }

  getCookiesLayer() {
    this.cookiesLayerSize = {
      width: this.refs.cookiesLayer.offsetWidth,
      height: this.refs.cookiesLayer.offsetHeight
    }
    this.props.setMovingBlock(this.cookiesLayerSize)
  }

  createCookies() {
    return this.props.cookies.map((cookie, index) => {
      return this.createCookie(cookie, index)
    })
  }

  createCookie(cookie, index) {
    const options = {
      top: cookie.top,
      left: cookie.left[this.props.mediaType],
      index: index
    }

    return <Cookie options={options} key={index} />
  }

  getMouseX(e) {
    const offset = this.refs.cookiesLayer.getBoundingClientRect().left

    this.mouseX = e.pageX - offset > 0 ? e.pageX - offset : 0
  }

  startDrag() {
    this.drag = true
  }

  endDrag() {
    this.drag = false
  }

  moveBasket(e) {
    const onLeftBoard = this.basketTop.offsetWidth / 2
    const onRightBoard = this.props.movingBlock.width - this.basketTop.offsetWidth / 2

    if (this.drag) {
      this.getMouseX(e.touches[0])
    } else {
      this.getMouseX(e)
    }

    if (this.mouseX > onLeftBoard && this.mouseX < onRightBoard) {
      this.basketTop.style.left = `${this.mouseX}px`
      this.basketBottom.style.left = `${this.mouseX}px`
      this.props.setBasket(this.mouseX)
    }
  }

  render() {
    const basketB = `${s['basket']} ${s['basket-bottom']}`
    const basketT = `${s['basket']} ${s['basket-top']}`

    return (
      <div className={s['cookies-layer']} ref='cookiesLayer'>
        {this.createCookies()}
        <div
          className={basketT}
          ref='basketTop'
          onTouchStart={() => {
            this.startDrag()
          }}
          onTouchEnd={() => {
            this.endDrag()
          }}
          onTouchMove={e => {
            this.moveBasket(e)
          }}
        />
        <div
          className={basketB}
          ref='basketBottom'
          onTouchStart={() => {
            this.startDrag()
          }}
          onTouchEnd={() => {
            this.endDrag()
          }}
          onTouchMove={e => {
            this.moveBasket(e)
          }}
        />
      </div>
    )
  }
}

CookiesLayer.propTypes = {
  mediaType: PropTypes.string,
  setMovingBlock: PropTypes.func,
  setBasket: PropTypes.func,
  createGameBoard: PropTypes.func,
  getRandomCookie: PropTypes.func,
  cookies: PropTypes.array,
  movingBlock: PropTypes.object,
  basket: PropTypes.number
}

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType,
  cookies: state.GingerBreadMen.cookies,
  movingBlock: state.GingerBreadMen.movingBlock,
  basket: state.GingerBreadMen.basket
})

const mapActionsToProps = {
  setMovingBlock,
  setBasket,
  createGameBoard,
  getRandomCookie
}

export default connect(mapStateToProps, mapActionsToProps)(CookiesLayer)
