import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'
import classnames from 'classnames'
import * as images from './images.json'
import { tray } from './constants'
import CookiesLayer from './CookiesLayer'
import { connect } from 'react-redux'

export class Board extends Component {
  render() {
    const c = classnames({
      [s['board']]: true
    })

    const currentTray = 'tray-' + this.props.mediaType

    const trayStyle = {
      width: tray[currentTray].width,
      height: tray[currentTray].height,
      backgroundImage: `url(${images[currentTray]})`
    }

    return (
      <div className={c} ref="board">
        <div className={s['tray']} style={trayStyle} />
        <CookiesLayer />
      </div>
    )
  }
}

Board.propTypes = {
  mediaType: PropTypes.string
}

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType
})

export default connect(mapStateToProps)(Board)
