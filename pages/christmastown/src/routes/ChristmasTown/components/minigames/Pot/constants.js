export const POT_CHANNEL = 'potGame'

export const CELL_WIDTH = {
  desktop: 37.75,
  tablet: 32,
  mobile: 26.7
}

export const CELL_HEIGHT = {
  desktop: 40,
  tablet: 40,
  mobile: 40
}
