import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import * as _ from 'lodash'
import { Item } from '.'
import s from './styles.cssmodule.scss'

export class Items extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      newItems: []
    }
  }
  _getItems = () => {
    const { items, cellWidth, cellHeigh } = this.props
    const { newItems } = this.state
    const catCounter = { 1: 0, 2: 0, 3: 0 }
    return items.map(({ id, cat, key }, index) => {
      const n = catCounter[cat]++
      const left = cellWidth * (n % 4 + 4 * (cat - 1))
      const top = cellHeigh * (8 - Math.floor(n / 4))
      const isNewItem = _.indexOf(newItems, key) !== -1
      return (
        <Item
          key={cat + '_' + index}
          index={index}
          id={id}
          left={left}
          top={top}
          isNewItem={isNewItem}
          cellWidth={cellWidth}
        />
      )
    })
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { items } = nextProps
    const { items: oldItems } = prevState
    const newItems = _.differenceWith(items, oldItems, _.isEqual).map(({ key }) => key)

    if (newItems.length !== 0) {
      return {
        newItems
      }
    }

    return null
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps({ items }) {
  //   const { items: oldItems } = this.props
  //   const newItems = _.differenceWith(items, oldItems, _.isEqual).map(({ key }) => key)
  //   if (newItems.length !== 0) {
  //     this.setState({
  //       newItems
  //     })
  //   }
  // }
  render() {
    return <div className={s['items-wrap']}>{this._getItems()}</div>
  }
}

Items.propTypes = {
  items: PropTypes.array
}

export default Items
