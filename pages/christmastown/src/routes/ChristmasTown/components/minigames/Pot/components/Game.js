import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Select from 'react-select'
import cn from 'classnames'
import { withArticle } from '@torn/shared/utils'
import { Items, Pipe } from '.'
import { CELL_WIDTH, CELL_HEIGHT } from '../constants'
import s from './styles.cssmodule.scss'

export class Game extends Component {
  constructor(props) {
    super(props)
    props.connect()
    this.state = {
      selectedItem: props.yourItems[0] && props.yourItems[0].value
    }
  }

  _addPot = () => {
    const { addPot, requestInProgress } = this.props
    const { selectedItem } = this.state

    !requestInProgress && addPot(selectedItem)
  }

  _ddSelect = item => {
    this.setState({
      selectedItem: item.value
    })
  }

  componentWillUnmount() {
    const { disconnect } = this.props

    disconnect()
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { yourItems } = nextProps
    const { selectedItem } = prevState
    const item = yourItems.find(({ value }) => selectedItem === value)

    if (!item) {
      return {
        selectedItem: nextProps.yourItems[0] && nextProps.yourItems[0].value
      }
    }

    return null
  }

  // deprecated method since React v.16.5!
  // componentWillReceiveProps(nextProps) {
  //   // update selectedItem to first item in the list
  //   const { yourItems } = nextProps
  //   const { selectedItem } = this.state
  //   const item = yourItems.find(({ value }) => selectedItem === value)
  //   if (!item) {
  //     this.setState({
  //       selectedItem: nextProps.yourItems[0] && nextProps.yourItems[0].value
  //     })
  //   }
  // }

  handleLeaveGame = () => {
    const { leaveGame, move } = this.props

    leaveGame()
    move('bottom')
  }

  render() {
    const { shoot, potItems, yourItems, mediaType, category, itemsRemaining } = this.props
    const { selectedItem } = this.state
    const spectator = itemsRemaining === 0

    return (
      <div className={s.wrap}>
        <div className={s.board}>
          <div className={s.pipes}>
            <Pipe type='alcohol' shoot={shoot === 1} />
            <Pipe type='candy' shoot={shoot === 2} />
            <Pipe type='energy-drinks' shoot={shoot === 3} />
          </div>
          <Items items={potItems} cellWidth={CELL_WIDTH[mediaType]} cellHeigh={CELL_HEIGHT[mediaType]} />
        </div>
        <div className={s.controls}>
          {!spectator && (
            <div className={s.selectMessage}>
              {`Add ${withArticle(category)} item to have a chance to win everything:`}
            </div>
          )}
          {!spectator && (
            <Select
              clearable={false}
              className={s.dropdown}
              placeholder={yourItems.length > 0 ? 'Select one of your items' : 'No items left'}
              options={yourItems}
              value={selectedItem}
              searchable={false}
              onChange={this._ddSelect}
            />
          )}
          {!spectator
            && yourItems.length > 0 && (
            <button className={cn(s.btn, 'btn-blue big')} onClick={this._addPot}>
              Add
            </button>
          )}
          <button className={cn(s.btn, 'btn-blue big')} onClick={this.handleLeaveGame}>
            Leave
          </button>
          {itemsRemaining !== undefined && (
            <div className={s.selectMessage}>{`You have ${itemsRemaining} deposits remaining during this round`}</div>
          )}
        </div>
      </div>
    )
  }
}

Game.propTypes = {
  addPot: PropTypes.func,
  getState: PropTypes.func,
  leaveGame: PropTypes.func,
  move: PropTypes.func,
  mediaType: PropTypes.string,
  potItems: PropTypes.array,
  yourItems: PropTypes.array.isRequired
}

export default Game
