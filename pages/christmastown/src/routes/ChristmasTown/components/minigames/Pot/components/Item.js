import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { ITEMS_URL } from '../../../../constants'
import s from './styles.cssmodule.scss'

export class Item extends Component {
  render() {
    const { isNewItem, left, top, id, cellWidth } = this.props
    const style = {
      left,
      top
    }

    if (isNewItem) {
      const x = cellWidth * 1.5 - left % (4 * cellWidth)
      const y = -top

      style['transform'] = `translate3d(${x}px, ${y}px, 0)`
    }
    const c = classnames({
      [s['item']]: true,
      [s['showup']]: isNewItem
    })

    return (
      <div className={c} style={style}>
        <img src={`${ITEMS_URL + id}/small.png`} />
      </div>
    )
  }
}

Item.propTypes = {
  cellWidth: PropTypes.number,
  id: PropTypes.string,
  isNewItem: PropTypes.bool,
  left: PropTypes.number,
  top: PropTypes.number
}

export default Item
