const initialState = {
  message: 'Congratulations, you won 7 items',
  yourItemsInPot: 6,
  nextGame: 1,
  potItems: [],
  yourItems: [
    {
      itemID: '37',
      name: 'Bag of Bon Bons',
      cat: 2
    },
    {
      itemID: '527',
      name: 'Bag of Candy Kisses',
      cat: 2
    },
    {
      itemID: '210',
      name: 'Bag of Chocolate Kisses',
      cat: 2
    },
    {
      itemID: '529',
      name: 'Bag of Chocolate Truffles',
      cat: 2
    },
    {
      itemID: '556',
      name: 'Bag of Reindeer Droppings',
      cat: 2
    },
    {
      itemID: '528',
      name: 'Bag of Tootsie Rolls',
      cat: 2
    },
    {
      itemID: '36',
      name: 'Big Box of Chocolate Bars',
      cat: 2
    },
    {
      itemID: '477',
      name: 'Black Easter Egg',
      cat: 2
    },
    {
      itemID: '38',
      name: 'Box of Bon Bons',
      cat: 2
    },
    {
      itemID: '35',
      name: 'Box of Chocolate Bars',
      cat: 2
    },
    {
      itemID: '586',
      name: 'Jawbreaker',
      cat: 2
    },
    {
      itemID: '474',
      name: 'Red Easter Egg',
      cat: 2
    },
    {
      itemID: '475',
      name: 'Yellow Easter Egg',
      cat: 2
    }
  ],
  category: 'Candy',
  yourItemsInPot: 7,
  itemsRemaining: 93,

  board: {
    list: [
      {
        time: 1512236786,
        items: 7,
        user_id: 1435333,
        user: {
          image: 'https://awardimages.torn.com/1435333-72338-large.png',
          playername: 'toshykazu',
          user_id: '1435333'
        }
      },
      {
        time: 1512235191,
        items: 16,
        user_id: 1435333,
        user: {
          image: 'https://awardimages.torn.com/1435333-72338-large.png',
          playername: 'toshykazu',
          user_id: '1435333'
        }
      },
      {
        time: 1512141633,
        items: 18,
        user_id: 1720900,
        user: {
          image: 'https://awardimages.torn.com/1720900-99297-large.png',
          playername: 'Xepa4ep',
          user_id: '1720900'
        }
      },
      {
        time: 1512057223,
        items: 20,
        user_id: 1720900,
        user: {
          image: 'https://awardimages.torn.com/1720900-99297-large.png',
          playername: 'Xepa4ep',
          user_id: '1720900'
        }
      },
      {
        time: 1512057058,
        items: 17,
        user_id: 1720900,
        user: {
          image: 'https://awardimages.torn.com/1720900-99297-large.png',
          playername: 'Xepa4ep',
          user_id: '1720900'
        }
      },
      {
        time: 1512052541,
        items: 18,
        user_id: 2072285,
        user: {
          image: 'https://awardimages.torn.com/2072285-62113-large.png',
          playername: 'sergeyd',
          user_id: '2072285'
        }
      },
      {
        time: 1512051426,
        items: 20,
        user_id: 1435333,
        user: {
          image: 'https://awardimages.torn.com/1435333-72338-large.png',
          playername: 'toshykazu',
          user_id: '1435333'
        }
      },
      {
        time: 1512044679,
        items: 15,
        user_id: 1435333,
        user: {
          image: 'https://awardimages.torn.com/1435333-72338-large.png',
          playername: 'toshykazu',
          user_id: '1435333'
        }
      },
      {
        time: 1512044380,
        items: 13,
        user_id: 1435333,
        user: {
          image: 'https://awardimages.torn.com/1435333-72338-large.png',
          playername: 'toshykazu',
          user_id: '1435333'
        }
      },
      {
        time: 1512043206,
        items: 19,
        user_id: 1435333,
        user: {
          image: 'https://awardimages.torn.com/1435333-72338-large.png',
          playername: 'toshykazu',
          user_id: '1435333'
        }
      }
    ],
    textnames: false
  },
  userData: {
    userStatus: 'ok',
    message: ''
  },
  __debug__: '\n[77,0.6217300806295732,11,1.1191141451332318,false]'
}

export default initialState
