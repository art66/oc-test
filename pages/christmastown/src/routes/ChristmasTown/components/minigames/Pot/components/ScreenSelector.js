/* eslint-disable react/require-default-props */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { Game } from '.'
import { AnimationDuration } from '../../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import GameCountdownScreen from '../../../GameCountdownScreen'
import GameStartScreen from '../../../GameStartScreen'
import ScoreBoard from '../../../ScoreBoard'
import { disconnect } from '../module'
import styles from '../../../../hoc/withMinigameWrapper/minigameWrapper.cssmodule.scss'
import slideAnimation from '../../../../hoc/withMinigameWrapper/slideAnimation.cssmodule.scss'

class ScreenSelector extends Component {
  componentDidMount() {
    this.props.getState()
  }

  selectComponent() {
    const {
      addPot,
      category,
      connect,
      gameAccepted,
      gameEndResult,
      itemsRemaining,
      leaveGame,
      mediaType,
      nextGame,
      potItems,
      requestInProgress,
      restartGame,
      shoot,
      yourItems,
      move
    } = this.props

    if (nextGame) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName='Pot' title='Recent pot wins' {...gameEndResult} withLeaveButton>
            <GameCountdownScreen timeLeft={nextGame} restartGame={restartGame} leaveGame={leaveGame} />
          </ScoreBoard>
        </CSSTransition>
      )
    }

    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen message='Would you like to play Pot?' yesTitle='YES' noTitle='NO' />
        </CSSTransition>
      )
    }

    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='Game'>
        <Game
          addPot={addPot}
          category={category}
          connect={connect}
          disconnect={disconnect}
          itemsRemaining={itemsRemaining}
          leaveGame={leaveGame}
          move={move}
          mediaType={mediaType}
          potItems={potItems}
          requestInProgress={requestInProgress}
          shoot={shoot}
          yourItems={yourItems}
        />
      </CSSTransition>
    )
  }

  render() {
    return <TransitionGroup className={styles.ctMiniGameWrapper}>{this.selectComponent()}</TransitionGroup>
  }
}

ScreenSelector.propTypes = {
  gameAccepted: PropTypes.bool,
  setGameTitle: PropTypes.func,
  gameEndResult: PropTypes.object,
  gameOver: PropTypes.bool,
  category: PropTypes.string,
  connect: PropTypes.func,
  itemsRemaining: PropTypes.number,
  resetGame: PropTypes.func,
  timeLeft: PropTypes.number,
  nextGame: PropTypes.number,
  requestInProgress: PropTypes.bool,
  restartGame: PropTypes.func,
  shoot: PropTypes.number,
  addPot: PropTypes.func,
  getState: PropTypes.func,
  leaveGame: PropTypes.func,
  move: PropTypes.func,
  mediaType: PropTypes.string,
  potItems: PropTypes.array,
  yourItems: PropTypes.array.isRequired
}

export default ScreenSelector
