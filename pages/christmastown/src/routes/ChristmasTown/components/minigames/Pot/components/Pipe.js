import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import s from './styles.cssmodule.scss'

export class Pipe extends Component {
  render() {
    const { type, shoot } = this.props
    const c = classnames({
      [s['pipe']]: true,
      [s['pipe-' + type]]: true,
      [s['shoot']]: shoot
    })
    return <div className={c} />
  }
}

Pipe.propTypes = {
  type: PropTypes.string.isRequired,
  shoot: PropTypes.bool
}

export default Pipe
