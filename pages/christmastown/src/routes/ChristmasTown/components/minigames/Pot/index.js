import { connect as reduxConnect } from 'react-redux'
import { leaveGame, move } from '../../../modules'
import { ScreenSelector } from './components'
import { addPot, connect, getState, restartGame } from './module'

export { default as reducer } from './module'

const mapStateToProps = state => ({
  category: state.Pot.category,
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  itemsRemaining: state.Pot.itemsRemaining,
  mediaType: state.browser.mediaType,
  nextGame: state.Pot.nextGame,
  potItems: state.Pot.potItems,
  requestInProgress: state.Pot.requestInProgress,
  selectedItem: state.Pot.selectedItem,
  shoot: state.Pot.shoot,
  yourItems: state.Pot.yourItems
})

const mapActionsToProps = {
  addPot,
  connect,
  getState,
  leaveGame,
  restartGame,
  move
}

export default reduxConnect(mapStateToProps, mapActionsToProps)(ScreenSelector)
