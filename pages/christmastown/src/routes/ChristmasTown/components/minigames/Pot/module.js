import { fetchUrl, getErrorMessage } from '../../../utils'
import { setGameEndResult } from '../../../modules'
import { debugShow } from '../../../../../controller/actions'

import { POT_CHANNEL } from './constants'
// import initialState from './__test__/initialState'

let centrifuge, subscription

// ------------------------------------
// Constants
// ------------------------------------
const initialState = {
  potItems: [],
  yourItems: []
}

// ------------------------------------
// Constants
// ------------------------------------
const START_REQUEST = 'START_REQUEST'
const DATA_LOADED = 'DATA_LOADED'
const ADD_POT = 'ADD_POT'
const DROP_ITEM = 'DROP_ITEM'

// ------------------------------------
// Actions
// ------------------------------------
export const getState = () => dispatch => {
  fetchUrl('miniGameAction', {
    action: 'state',
    gameType: 'gamePot'
  })
  .then(result => dispatch(ajaxAction(result)))
  .catch(error => {
    dispatch(debugShow(getErrorMessage(error)))
  })
}

export const connect = () => (dispatch, getState) => {
  const { credentials } = getState().christmastown

  centrifuge = new Centrifuge(credentials.url + '/connection/websocket')
  centrifuge.setToken(credentials.token)
  subscription = centrifuge.subscribe(POT_CHANNEL, data => dispatch(onMessage(data)))
  centrifuge.connect()
}

export const disconnect = () => {
  centrifuge.disconnect()
  subscription.unsubscribe()
}

export const onMessage = resp => (dispatch, getState) => {
  const data = resp.data.message
  const { userID } = getState().christmastown.credentials
  if (data.winner) {
    // dispatch this message only for rest of users
    // because message for winner is sent via ajax request

    dispatch(dataLoaded(data.state))
    if (data.winner.user_id != userID) {
      dispatch(
        setGameEndResult({
          message: data.message,
          board: data.board
        })
      )
    }
  } else if (data.item_id) {
    dispatch(dropItem(data))
  }
}

export const ajaxAction = json => dispatch => {
  const { message, board, state = {} } = json
  dispatch(dataLoaded(state))
  if (message && message.includes('won')) {
    dispatch(
      setGameEndResult({
        message,
        board
      })
    )
  }
}

export const dataLoaded = data => ({
  type: DATA_LOADED,
  data
})

const dropItem = data => ({
  type: DROP_ITEM,
  data
})
const starRequest = () => ({
  type: START_REQUEST
})

export const addPot = selectedItem => dispatch => {
  dispatch(starRequest())
  fetchUrl('miniGameAction', {
    action: 'complete',
    gameType: 'gamePot',
    result: {
      itemID: selectedItem
    }
  })
  .then(result => dispatch(ajaxAction(result)))
  .catch(error => {
    dispatch(debugShow(getErrorMessage(error)))
  })
}

export const restartGame = () => dispatch => {
  dispatch(setGameEndResult({}))
  dispatch(getState())
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [START_REQUEST]: state => ({
    ...state,
    requestInProgress: true
  }),
  [DATA_LOADED]: (state, action) => {
    const newState = {
      ...state,
      ...action.data,
      requestInProgress: false
    }
    // override nextGame (timeLeft) only when game is availabel
    if (action.data.gameAvailable) {
      newState.nextGame = action.data.nextGame
    }
    if (action.data.potItems) {
      newState.potItems = action.data.potItems.map(({ item_id, key, cat }) => ({
        id: item_id,
        key: key,
        cat: cat
      }))
    }
    if (action.data.yourItems) {
      newState.yourItems = action.data.yourItems.map(({ itemID, name, qty }) => ({
        label: `${name} x${qty}`,
        value: itemID
      }))
    }
    return newState
  },
  [DROP_ITEM]: (state, action) => {
    return {
      ...state,
      potItems: [
        ...state.potItems,
        {
          id: action.data.item_id,
          cat: action.data.cat,
          key: action.data.key
        }
      ],
      shoot: action.data.cat
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
export default function(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
