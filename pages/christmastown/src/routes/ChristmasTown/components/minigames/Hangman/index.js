import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import styles from '../../../hoc/withMinigameWrapper/minigameWrapper.cssmodule.scss'
import slideAnimation from '../../../hoc/withMinigameWrapper/slideAnimation.cssmodule.scss'
import { AnimationDuration } from '../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import { setGameTitle } from '../../../modules'
import GameStartScreen from '../../GameStartScreen'
import ScoreBoard from '../../ScoreBoard'
import Board from './Board'
import reducer, { checkLetter, resetGame, startLevel } from './modules'
import s from './styles.cssmodule.scss'

export { reducer }

export class Hangman extends Component {
  componentDidMount() {
    this.props.setGameTitle({
      gameTitle: 'Hangman'
    })
  }

  componentWillUnmount() {
    this.props.resetGame()
  }

  selectComponent() {
    const { gameAccepted, gameEndResult } = this.props

    if (!gameAccepted) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='GameStartScreen'>
          <GameStartScreen message='Play Hangman?' yesTitle='YES' noTitle='NO' />
        </CSSTransition>
      )
    }

    if (this.props.gameIsEnd) {
      return (
        <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='ScoreBoard'>
          <ScoreBoard gameName='Hangman' title='Hangman high scores' {...gameEndResult} />
        </CSSTransition>
      )
    }

    return (
      <CSSTransition timeout={AnimationDuration} classNames={{ ...slideAnimation }} key='Board'>
        <div className={s['wrap']}>
          <Board
            startLevel={this.props.startLevel}
            alphabet={this.props.alphabet}
            word={this.props.word}
            stageOfHangman={this.props.stageOfHangman}
            checkLetter={this.props.checkLetter}
            resetGame={this.props.resetGame}
            getWord={this.props.getWord}
          />
        </div>
      </CSSTransition>
    )
  }

  render() {
    return <TransitionGroup className={styles.ctMiniGameWrapper}>{this.selectComponent()}</TransitionGroup>
  }
}

Hangman.propTypes = {
  gameAccepted: PropTypes.bool,
  setGameTitle: PropTypes.func,
  startLevel: PropTypes.func,
  alphabet: PropTypes.array,
  word: PropTypes.array,
  gameIsEnd: PropTypes.bool,
  stageOfHangman: PropTypes.number,
  checkLetter: PropTypes.func,
  resetGame: PropTypes.func,
  getWord: PropTypes.func,
  gameEndResult: PropTypes.object
}

const mapStateToProps = (state) => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  alphabet: state.Hangman.alphabet,
  word: state.Hangman.word,
  stageOfHangman: state.Hangman.stageOfHangman,
  gameIsEnd: state.Hangman.gameIsEnd
})

const mapActionsToProps = {
  setGameTitle,
  startLevel,
  checkLetter,
  resetGame
}

export default connect(mapStateToProps, mapActionsToProps)(Hangman)
