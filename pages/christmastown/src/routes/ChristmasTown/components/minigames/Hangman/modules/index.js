import { createAlphabet } from './helpers'
import { getEndResult, setGameEndResult } from '../../../../modules'
import { fetchUrl, getErrorMessage } from '../../../../utils'
import { debugShow } from '../../../../../../controller/actions'

const CREATE_GAMEBOARD = 'CREATE_GAMEBOARD'
const SET_CORRECT_LETTER = 'SET_CORRECT_LETTER'
const SET_WRONG_LETTER = 'SET_WRONG_LETTER'
const RESET_GAME = 'RESET_GAME'
const GAME_IS_END = 'GAME_IS_END'
const SET_WORD = 'SET_WORD'
const PREVENT_LETTER_SELECT = 'PREVENT_LETTER_SELECT'

export const createGameBoard = () => ({
  type: CREATE_GAMEBOARD
})

export const setWord = word => ({
  type: SET_WORD,
  word
})

export const setCorrectLetter = (indexInWord, indexInAlphabet) => ({
  type: SET_CORRECT_LETTER,
  indexInWord,
  indexInAlphabet
})

export const setWrongLetter = (indexInAlphabet, mistakes) => ({
  type: SET_WRONG_LETTER,
  indexInAlphabet,
  mistakes
})

export const gameIsEnd = () => ({
  type: GAME_IS_END
})

export const resetGame = () => {
  return {
    type: RESET_GAME
  }
}

const setPreventLetterSelect = () => {
  return {
    type: PREVENT_LETTER_SELECT
  }
}

export const ACTION_HANDLERS = {
  [CREATE_GAMEBOARD]: state => ({
    ...state,
    alphabet: createAlphabet()
  }),
  [SET_WORD]: (state, action) => ({
    ...state,
    word: action.word,
    preventLetterSelect: false
  }),
  [SET_CORRECT_LETTER]: (state, action) => {
    return {
      ...state,
      preventLetterSelect: false,
      alphabet: state.alphabet.map((letter, index) => {
        if (action.indexInAlphabet === index) {
          return {
            ...letter,
            isChosen: true
          }
        }
        return letter
      }),
      word: state.word.map((letter, index) => {
        if (index === action.indexInWord) {
          return {
            ...letter,
            isFind: true,
            letter: state.alphabet[action.indexInAlphabet].letter
          }
        }
        return letter
      })
    }
  },
  [SET_WRONG_LETTER]: (state, action) => ({
    ...state,
    preventLetterSelect: false,
    alphabet: state.alphabet.map((letter, index) => {
      if (action.indexInAlphabet === index) {
        return {
          ...letter,
          isWrong: true,
          isChosen: true
        }
      }
      return letter
    }),
    stageOfHangman: action.mistakes
  }),
  [GAME_IS_END]: state => ({
    ...state,
    gameIsEnd: true
  }),
  [RESET_GAME]: () => ({
    ...initialState
  }),
  [PREVENT_LETTER_SELECT]: state => ({
    ...state,
    preventLetterSelect: true
  })
}

const initialState = {
  alphabet: [],
  word: [],
  stageOfHangman: 0,
  gameIsEnd: false,
  preventLetterSelect: true
}

export default function HangmanReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

export const startLevel = level => dispatch => {
  dispatch(createGameBoard())
  dispatch(getWord())
}

const getWord = () => dispatch => {
  fetchUrl('miniGameAction', {
    action: 'start',
    gameType: 'gameHangman'
  })
  .then(result => {
    let words = result.progress.words
    let resultWord = []

    for (let word = 0; word < words.length; word++) {
      for (let letter = 0; letter < words[word]; letter++) {
        resultWord.push({
          isLetter: true,
          isFind: false,
          letter: ''
        })
      }
      if (word !== words.length - 1) {
        resultWord.push({
          isLetter: false,
          isFind: true,
          letter: ''
        })
      }
    }
    dispatch(setWord(resultWord))
  })
  .catch(error => {
    dispatch(debugShow(getErrorMessage(error)))
  })
}

export const checkLetter = indexInAlphabet => (dispatch, getState) => {
  const preventLetterSelect = getState().Hangman.preventLetterSelect
  if (!preventLetterSelect) {
    let letter = getState().Hangman.alphabet[indexInAlphabet]
    if (!letter.isChosen) {
      dispatch(getLetterResult(letter.letter, indexInAlphabet))
    }
  }
}

const getLetterResult = (letter, indexInAlphabet) => (dispatch, getState) => {
  dispatch(setPreventLetterSelect())
  fetchUrl('miniGameAction', {
    action: 'complete',
    gameType: 'gameHangman',
    result: {
      character: letter.toLowerCase()
    }
  })
  .then(result => {
    let isWrongLetter = result.positions.length === 0
    let mistakes = result.mistakes

    if (isWrongLetter) {
      dispatch(setWrongLetter(indexInAlphabet, mistakes))
    } else {
      result.positions.forEach(letterPosition => {
        dispatch(setCorrectLetter(letterPosition, indexInAlphabet))
      })
    }
    let allLetterFind = getState().Hangman.word.every(item => {
      return item.isFind
    })

    if (mistakes === 6 || allLetterFind) {
      dispatch(gameIsEnd())
      dispatch(setGameEndResult(result))
    }
  })
  .catch(error => {
    dispatch(debugShow(getErrorMessage(error)))
  })
}
