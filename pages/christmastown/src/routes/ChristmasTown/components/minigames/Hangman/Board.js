import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import s from './styles.cssmodule.scss'
import { KEY_CODE_OF_LETTER_A, KEY_CODE_OF_LETTER_Z } from './constants'

export class Board extends Component {
  constructor(props) {
    super(props)
    this.chooseLetterByClick = this.chooseLetterByClick.bind(this)
    this.chooseLetterByKeyPress = this.chooseLetterByKeyPress.bind(this)
  }

  // deprecated since React v.16.5
  // UNSAFE_componentWillMount() {
  //   this.props.resetGame()
  //   this.props.startLevel()
  // }

  componentDidMount() {
    this.props.resetGame()
    this.props.startLevel()

    document.body.addEventListener('keydown', this.chooseLetterByKeyPress)
  }

  componentWillUnmount() {
    document.body.removeEventListener('keydown', this.chooseLetterByKeyPress)
  }

  chooseLetterByClick(index) {
    this.props.checkLetter(index)
  }

  chooseLetterByKeyPress(e) {
    if (
      e.keyCode >= KEY_CODE_OF_LETTER_A
      && e.keyCode <= KEY_CODE_OF_LETTER_Z
      && this.boardContainer === document.activeElement
    ) {
      this.props.checkLetter(e.keyCode - KEY_CODE_OF_LETTER_A)
    }
  }

  render() {
    let { alphabet, word, stageOfHangman } = this.props
    const c = classnames({
      [s['board']]: true
    })

    const stage = classnames({
      [s['hangman']]: true,
      [s[`stage-${stageOfHangman}`]]: true
    })

    return (
      <div
        className={c}
        ref={el => (this.boardContainer = el)}
        tabIndex={0}
        onKeyUp={e => {
          this.chooseLetterByKeyPress(e)
        }}
      >
        <div className={s['hangman-wrap']}>
          <div className={stage} />
        </div>
        <ul className={s['word']}>
          {word.map((item, index) => {
            return item.isLetter ? (
              <li key={index} className={s['cell']}>
                {item.isFind ? item.letter : ''}
              </li>
            ) : (
              <li key={index} className={s['empty-cell']} />
            )
          })}
        </ul>
        <ul className={s['alphabet']}>
          {alphabet.map((item, index) => {
            const letterClass = classnames({
              [s['wrong-letter']]: item.isWrong,
              [s['correct-letter']]: !item.isWrong && item.isChosen
            })

            return (
              <li key={index} className={letterClass} onClick={() => this.chooseLetterByClick(index)}>
                {item.letter}
                <div className={s['cross-letter']} />
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}

Board.propTypes = {
  startLevel: PropTypes.func,
  alphabet: PropTypes.array,
  word: PropTypes.array,
  stageOfHangman: PropTypes.number,
  checkLetter: PropTypes.func,
  resetGame: PropTypes.func,
  getGameState: PropTypes.func
}

export default Board
