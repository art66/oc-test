export function createAlphabet() {
  let alphabet = []
  for (let i = 65; i <= 90; i++) {
    alphabet.push({
      letter: String.fromCharCode(i),
      isChosen: false,
      isWrong: false
    })
  }
  return alphabet
}
