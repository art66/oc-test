import React, { Component } from 'react'
import { connect } from 'react-redux'
import { teleport } from '../../../modules'
import StatusAreaText from '../../StatusArea/StatusAreaText'

interface IProps {
  statusArea: any,
  teleport(): void
}

class TeleportTest extends Component<IProps> {
  componentDidMount() {
    this.props.teleport()
  }

  render() {
    const { statusArea } = this.props

    return (
      <StatusAreaText statusArea={statusArea} />
    )
  }
}

export default connect(
  (state: any) => ({
    statusArea: state.christmastown.statusArea
  }),
  {
    teleport
  }
)(TeleportTest)
