import React, { Component } from 'react'
import { connect } from 'react-redux'
import { teleport, blockMap, unblockMap, leaveGame } from '../../../modules'
import Dialog from '../../Dialog/index'

interface IProps {
  parameterText: string
  teleport(): Promise<() => void>
  blockMap(): void
  unblockMap(): void
  leaveGame(): void
}

interface IState {
  atJail: boolean
}

class Jail extends Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = {
      atJail: false
    }
  }

  componentDidMount() {
    this.props.blockMap()
  }

  moveToJail = () => {
    this.props.teleport().then(() => {
      this.setState({ atJail: true })
      this.props.unblockMap()
    })
  }

  getDialog = () => {
    if (this.state.atJail) {
      return (
        <Dialog
          text={this.props.parameterText}
          buttons={[{ text: 'Okay', onClick: this.props.leaveGame }]}
        />
      )
    }

    return <Dialog text={this.props.parameterText} buttons={[{ text: 'Continue', onClick: this.moveToJail }]} />
  }

  render() {
    return this.getDialog()
  }
}


const mapStateToProps = state => {
  const trigger = state.christmastown.mapData.trigger || {}
  const cellEvent = state.christmastown.statusArea.cellEvent
  const text = (cellEvent && cellEvent.message) || trigger.message || ''

  return {
    parameterText: text
  }
}

const mapActionsToProps = {
  teleport,
  blockMap,
  unblockMap,
  leaveGame
}

export default connect(mapStateToProps, mapActionsToProps)(Jail)
