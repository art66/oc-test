import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { connect } from 'react-redux'
import { setGameTitle, leaveGame, move } from '../../../modules'
import reducer, { buyItem, showBuyDialog, hideBuyDialog, loadGameState, resetGame, toggleInformDialog } from './modules'
import s from './styles.cssmodule.scss'
import Item from './Item'

export { reducer }

class GiftShop extends Component {
  componentDidMount() {
    const { resetGame, setGameTitle, loadGameState } = this.props

    resetGame()
    setGameTitle({ gameTitle: 'Gift shop' })
    loadGameState()
  }

  getItems = () => {
    const { items, tokens } = this.props.giftShop
    const { buyItem, showBuyDialog, hideBuyDialog, toggleInformDialog } = this.props

    return items.map((item, index) => (
      <Item
        key={index}
        item={item}
        tokens={tokens}
        buyItem={buyItem}
        showBuyDialog={showBuyDialog}
        hideBuyDialog={hideBuyDialog}
        toggleInformDialog={toggleInformDialog}
      />
    ))
  }

  getItemsType = () => {
    const { shopType } = this.props.giftShop
    const types = {
      'beer tent': 'Alcohol',
      'candy store': 'Candy',
      'gift shop': 'Items',
      'ornament stall': 'Speed Ornaments'
    }

    return types[shopType && shopType.toLowerCase()] || ''
  }

  handleLeaveGame = () => {
    const { leaveGame, move } = this.props

    leaveGame()
    move('bottom')
  }

  render() {
    const items = this.getItems()
    const itemsType = this.getItemsType()

    return (
      <div className={s.giftShop}>
        {items.length > 0 && (
          <div>
            <h3 className={s.title}>Exchange your Christmas Bucks for {itemsType}!</h3>
            <div className={s.itemsList}>{items}</div>
            <div className={s.btnWrapper}>
              <button className={cn(s.btn, 'btn-blue')} onClick={this.handleLeaveGame}>
                Leave
              </button>
            </div>
          </div>
        )}
      </div>
    )
  }
}

GiftShop.propTypes = {
  setGameTitle: PropTypes.func,
  giftShop: PropTypes.object,
  buyItem: PropTypes.func,
  showBuyDialog: PropTypes.func,
  hideBuyDialog: PropTypes.func,
  loadGameState: PropTypes.func,
  resetGame: PropTypes.func,
  hideInformDialog: PropTypes.func,
  toggleInformDialog: PropTypes.func,
  leaveGame: PropTypes.func,
  move: PropTypes.func
}

const mapStateToProps = state => ({
  gameAccepted: state.christmastown.minigame.gameAccepted,
  gameEndResult: state.christmastown.minigame.gameEndResult,
  giftShop: state.GiftShop
})

const mapActionsToProps = {
  setGameTitle,
  buyItem,
  showBuyDialog,
  hideBuyDialog,
  loadGameState,
  resetGame,
  toggleInformDialog,
  leaveGame,
  move
}

export default connect(mapStateToProps, mapActionsToProps)(GiftShop)
