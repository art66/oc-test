/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/require-default-props */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import { plural } from '../../../utils'
import { ITEMS_URL, CT_ITEMS_URL } from '../../../constants'
import s from './styles.cssmodule.scss'

class Item extends Component {
  getItem = () => {
    const {
      item: { name, category, type, price, isAvailable, modifier },
      tokens
    } = this.props
    const isOrnaments = category === 'ornaments'
    const itemUrl = isOrnaments ?
      `${CT_ITEMS_URL}${category}/${category}-${type}${price > tokens || !isAvailable ? '-blank' : '-stall'}.png` :
      `${ITEMS_URL}${type}/${price > tokens ? 'blank' : 'large'}.png`

    return (
      <div className={cn({ [s.ornaments]: isOrnaments })}>
        <div className={s.imageWrap} id={`shopItem${type}`}>
          <img src={itemUrl} alt={name} />
          <Tooltip position='top' arrow='center' parent={`shopItem${type}`}>
            {name}
          </Tooltip>
        </div>
        <div className={s.info}>
          {isOrnaments && <div className={s.itemModifier}>{`+${modifier}% walk speed`}</div>}
          <div className={s.itemPrice}>{`${price} buck${plural(price)}`}</div>
          <button
            type='button'
            onClick={() => this.handleBuyClick()}
            className={cn('btn-blue', { locked: price > tokens || !isAvailable }, s.buy)}
          >
            BUY
          </button>
        </div>
      </div>
    )
  }

  getDialog = () => {
    const { item, buyItem, hideBuyDialog } = this.props
    const { name, category, price } = item
    const itemType = category === 'ornaments' ? '' : 'item'

    return (
      <div className={s.dialog}>
        <div className={s.question}>
          {`Are you sure you would like to exchange ${price} buck${plural(price)} for a ${name} ${itemType}?`}
        </div>
        <div className={s.confirmActions}>
          <a onClick={() => buyItem(item)} className={s.yes}>
            Yes
          </a>
          <a onClick={() => hideBuyDialog()} className={s.no}>
            No
          </a>
        </div>
      </div>
    )
  }

  getInformDialog = () => {
    const { item, toggleInformDialog } = this.props
    const isOrnaments = item.category === 'ornaments'

    return (
      <div className={cn(s.dialog, s.informDialog)}>
        <div className={s.question}>Added to your {isOrnaments ? 'inventory' : 'items'}.</div>
        <div className={s.confirmActions}>
          <a onClick={() => toggleInformDialog(item, false)} className={s.okay}>
            Okay
          </a>
        </div>
      </div>
    )
  }

  getContent = () => {
    const { item } = this.props

    if (item.dialog) {
      return this.getDialog()
    }
    if (item.informDialog) {
      return this.getInformDialog()
    }

    return this.getItem()
  }

  handleBuyClick = () => {
    const { item, tokens, showBuyDialog } = this.props

    if (item.price > tokens || !item.isAvailable) {
      return false
    }

    showBuyDialog(item)
  }

  render() {
    const content = this.getContent()

    return <div className={s.item}>{content}</div>
  }
}

Item.propTypes = {
  item: PropTypes.object,
  tokens: PropTypes.number,
  buyItem: PropTypes.func,
  showBuyDialog: PropTypes.func,
  hideBuyDialog: PropTypes.func,
  toggleInformDialog: PropTypes.func
}

export default Item
