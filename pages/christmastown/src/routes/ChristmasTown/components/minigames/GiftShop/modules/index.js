import { fetchUrl, getErrorMessage } from '../../../../utils'
import { fetchMapData } from '../../../../modules'
import { debugShow } from '../../../../../../controller/actions'

// ------------------------------------
// Constants
// ------------------------------------
const SHOW_BUY_DIALOG = 'shops/SHOW_BUY_DIALOG'
const HIDE_BUY_DIALOG = 'shops/HIDE_BUY_DIALOG'
const TOGGLE_INFORM_DIALOG = 'shops/TOGGLE_INFORM_DIALOG'
const BUY_ITEM = 'shops/BUY_ITEM'
const SET_GAME_STATE = 'shops/SET_GAME_STATE'
const RESET_GAME = 'shops/RESET_GAME'

// ------------------------------------
// Actions
// ------------------------------------
export const showBuyDialog = item => ({
  type: SHOW_BUY_DIALOG,
  item
})

export const hideBuyDialog = () => ({
  type: HIDE_BUY_DIALOG
})

export const toggleInformDialog = (item, show) => ({
  type: TOGGLE_INFORM_DIALOG,
  item,
  show
})

export const buyItem = item => (dispatch, getState) => {
  const { tokens } = getState().GiftShop

  if (tokens < item.tokens) {
    return false
  }

  dispatch({ type: BUY_ITEM, item })
  dispatch(toggleInformDialog(item, true))

  return fetchUrl('miniGameAction', {
    gameType: 'gameGiftShop',
    action: 'buyItem',
    result: {
      giftShopType: 'basic',
      itemType: item.type,
      itemCategory: item.category
    }
  })
    .then(() => {
      dispatch(fetchMapData())
    })
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}

export const setGameState = data => ({
  type: SET_GAME_STATE,
  state: data
})

export const loadGameState = () => (dispatch, getState) =>
  fetchUrl('miniGameAction', {
    gameType: 'gameGiftShop',
    action: 'getItems'
  })
    .then(data => {
      const tokens = getState().christmastown.mapData.inventory.find(item => item.category === 'bucks')

      dispatch(setGameState({ ...data, tokens: tokens ? tokens.quantity : 0 }))
    })
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })

export const resetGame = () => ({
  type: RESET_GAME
})

// ------------------------------------
// Initial State
// ------------------------------------
const initialState = {
  items: [],
  tokens: 0
}

// ------------------------------------
// Action Handlers
// ------------------------------------
export const ACTION_HANDLERS = {
  [SHOW_BUY_DIALOG]: (state, action) => ({
    ...state,
    items: state.items.map(item => ({
      ...item,
      dialog: item.type === action.item.type
    }))
  }),

  [HIDE_BUY_DIALOG]: state => ({
    ...state,
    items: state.items.map(item => ({
      ...item,
      dialog: false
    }))
  }),

  [TOGGLE_INFORM_DIALOG]: (state, action) => ({
    ...state,
    items: state.items.map(item => {
      return item.type === action.item.type ? { ...item, informDialog: action.show } : { ...item }
    })
  }),

  [BUY_ITEM]: (state, action) => ({
    ...state,
    tokens: state.tokens - action.item.price,
    items: state.items.map(item => {
      return action.item.category === 'ornaments' ?
        {
          ...item,
          dialog: false,
          isAvailable: item.item_id === action.item.item_id ? false : item.isAvailable
        } :
        {
          ...item,
          dialog: false
        }
    })
  }),

  [SET_GAME_STATE]: (state, action) => ({
    ...state,
    ...action.state
  }),

  [RESET_GAME]: () => ({
    ...initialState
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const giftShopReducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default giftShopReducer
