import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { teleport, move, blockMap, unblockMap } from '../../../modules'
import Dialog from '../../Dialog/index'
import s from './styles.cssmodule.scss'

class FirstAid extends Component {
  constructor(props) {
    super(props)
    this.state = {
      atFirstAidTent: false
    }
  }

  componentDidMount() {
    this.props.blockMap()
  }

  moveToFirstAidTent = () => {
    const { teleport } = this.props
    teleport().then(() => {
      this.setState({ atFirstAidTent: true })
    })
  }

  leaveFirstAidTent = () => {
    const { move, unblockMap } = this.props
    unblockMap()
    move('bottom')
  }

  getDialog = () => {
    if (this.state.atFirstAidTent) {
      return (
        <Dialog
          text="You wake up in a first aid tent"
          buttons={[{ text: 'Leave', onClick: () => this.leaveFirstAidTent() }]}
        />
      )
    }

    return <Dialog text={this.props.parameterText} buttons={[{ text: 'Continue', onClick: this.moveToFirstAidTent }]} />
  }

  render() {
    return this.getDialog()
  }
}

FirstAid.propTypes = {
  setGameTitle: PropTypes.func,
  firstAid: PropTypes.object,
  buyItem: PropTypes.func,
  showBuyDialog: PropTypes.func,
  hideBuyDialog: PropTypes.func,
  loadGameState: PropTypes.func,
  acceptGame: PropTypes.func,
  leaveGame: PropTypes.func,
  teleport: PropTypes.func,
  move: PropTypes.func,
  parameterText: PropTypes.string,
  blockMap: PropTypes.func,
  unblockMap: PropTypes.func
}

const mapStateToProps = state => {
  const trigger = state.christmastown.mapData.trigger || {}
  const cellEvent = state.christmastown.statusArea.cellEvent
  const text = (cellEvent && cellEvent.message) || trigger.message || ''

  return {
    gameAccepted: state.christmastown.minigame.gameAccepted,
    gameEndResult: state.christmastown.minigame.gameEndResult,
    parameterText: text,
    firstAid: state.FirstAid
  }
}

const mapActionsToProps = {
  teleport,
  move,
  blockMap,
  unblockMap
}

export default connect(mapStateToProps, mapActionsToProps)(FirstAid)
