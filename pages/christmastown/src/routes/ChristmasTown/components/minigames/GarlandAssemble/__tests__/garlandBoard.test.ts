import { verifySchema } from '../garlandAssembleData'
import { emptyData, validData, wrongData } from './fixtures'

describe('Given a schema that is correct', () => {
  it('verification algorithm should match it as correct', () => {
    expect(verifySchema(validData.schema, validData.endings)).toBeTruthy()
  })
})

describe('Given a schema that is Not correct', () => {
  it('verification algorithm should match it as wrong', () => {
    expect(verifySchema(wrongData.schema, wrongData.endings)).toBeFalsy()
  })
})

describe('Given an empty', () => {
  it('verification algorithm should not crash and should return false', () => {
    expect(verifySchema(emptyData.schema, emptyData.endings)).toBeFalsy()
  })
})
