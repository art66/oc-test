import { withMiniGameWrapper } from '../../../hoc/withMinigameWrapper/withMiniGameWrapper'
import { GarlandAssemble } from './components/GarlandAssemble'

export { default as reducer } from './garlandAssembleSlice'

export default withMiniGameWrapper({ gameName: 'GarlandAssemble', gameTitle: 'Garland Assemble' })(GarlandAssemble)
