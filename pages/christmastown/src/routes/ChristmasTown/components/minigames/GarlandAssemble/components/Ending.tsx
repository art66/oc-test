import React, { memo } from 'react'
import { useSelector } from 'react-redux'
import { getMediaType } from '../../../../selectors/browser'
import { getAssetsPath } from './constants'

export type TProps = {
  rotation: number
}

export const Ending = memo((props: TProps) => {
  const mediaType = useSelector(getMediaType)

  return (
    <img
      src={`${getAssetsPath(mediaType)}entrance.png`}
      style={{ transform: `rotate(${props.rotation}deg)` }}
      alt=''
    />
  )
})
