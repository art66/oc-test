export type TDirection = 't' | 'r' | 'b' | 'l'
export type TTailRotationSchema = TDirection[] | null
export type TBoardRotationSchema = TTailRotationSchema[][]

export type TTailAsset = {
  imageSrc: string
  rotation: number
  endingSides?: TDirection[]
} | null
export type TBoardAssets = TTailAsset[][]

export type TPosition = {
  x: number
  y: number
}

export type TEnding = {
  x: number
  y: number
  direction: TDirection
}

export const getNextDirection = (direction: TDirection): TDirection => {
  switch (direction) {
    case 't':
      return 'r'
    case 'r':
      return 'b'
    case 'b':
      return 'l'
    case 'l':
    default:
      return 't'
  }
}

const getPositionTowardsDirection = ({ x, y }: TPosition, direction: TDirection): TPosition => {
  switch (direction) {
    case 't':
    default:
      return {
        x,
        y: y - 1
      }
    case 'r':
      return {
        x: x + 1,
        y
      }
    case 'b':
      return {
        x,
        y: y + 1
      }
    case 'l':
      return {
        x: x - 1,
        y
      }
  }
}

const crossOpposite: { [key: string]: TDirection } = {
  l: 'r',
  r: 'l',
  t: 'b',
  b: 't'
}

const getTheOtherEnd = (directionToCheck: TDirection, tileToCheck: TDirection[]) => {
  return tileToCheck.length === 4 ?
    crossOpposite[directionToCheck] :
    tileToCheck.filter((direction) => direction !== directionToCheck)[0]
}

type TCell = {
  position: TPosition
  inDirection: TDirection
  outDirection: TDirection
}

const getNextCellFromSchema = (schema: TBoardRotationSchema) => (cell: TCell): TCell => {
  const position = getPositionTowardsDirection(cell.position, cell.outDirection)

  const ends = schema[position.y]?.[position.x]

  const inDirection = ends?.find((x) => x === crossOpposite[cell.outDirection])
  const outDirection = (inDirection && getTheOtherEnd(inDirection, ends)) || ('' as TDirection)

  return {
    position,
    inDirection,
    outDirection
  }
}

const endsMatch = (first: TCell, second: TCell) => {
  return first.outDirection === crossOpposite[second.inDirection]
}

const mockFirstCellFromEnding = (ending: TEnding, schema: TBoardRotationSchema) => ({
  position: { x: ending.x, y: ending.y },
  inDirection: ending.direction,
  outDirection: getTheOtherEnd(ending.direction, schema[ending.y][ending.x])
})

export const verifySchema = (schema: TBoardRotationSchema, endings: TEnding[]): boolean => {
  if (!schema.length || !endings.length) return false
  const getNextCell = getNextCellFromSchema(schema)
  let cell: TCell = mockFirstCellFromEnding(endings[0], schema)

  let finalEndMatched = false

  while (!finalEndMatched) {
    const nextCell = getNextCell(cell)

    if (!endsMatch(cell, nextCell)) {
      return false
    }
    cell = nextCell
    finalEndMatched =
      cell.outDirection === endings[1].direction && cell.position.x === endings[1].x && cell.position.y === endings[1].y
  }

  return true
}

export const getRotation = (direction: TDirection) => {
  switch (direction) {
    case 'l':
    default:
      return 0
    case 't':
      return 90
    case 'r':
      return 180
    case 'b':
      return 270
  }
}
