import { TBoardRotationSchema, TEnding } from '../garlandAssembleData'

export const validData = {
  schema: [
    [
      ['b', 'l'],
      ['r', 'b'],
      ['b', 'l'],
      ['r', 'b'],
      ['b', 'l']
    ],
    [
      ['t', 'r'],
      ['l', 't'],
      ['t', 'r'],
      ['l', 't'],
      ['b', 't']
    ],
    [null, ['r', 'b'], ['b', 'l'], ['r', 'b'], ['l', 't']],
    [
      ['r', 'b'],
      ['l', 't'],
      ['t', 'b'],
      ['t', 'r'],
      ['b', 'l']
    ],
    [
      ['t', 'r'],
      ['b', 'l'],
      ['t', 'r'],
      ['r', 'l'],
      ['l', 't']
    ]
  ] as TBoardRotationSchema,
  endings: [
    { x: 0, y: 0, direction: 'l' },
    { x: 1, y: 4, direction: 'b' }
  ] as TEnding[]
}

export const wrongData = {
  schema: [
    [null, ['r', 'b'], ['b', 'l'], null, null],
    [['r', 'l'], ['t', 'r', 'b', 'l'], ['l', 't'], null, null],
    [null, ['b', 't'], null, ['r', 'b'], ['b', 'l']],
    [null, ['t', 'r'], ['b', 'l'], ['t', 'r', 'b', 'l'], ['l', 't']],
    [null, null, null, ['t', 'r'], null]
  ] as TBoardRotationSchema,
  endings: [
    { x: 0, y: 1, direction: 'l' },
    { x: 3, y: 4, direction: 'b' }
  ] as TEnding[]
}

export const emptyData = {
  schema: [] as TBoardRotationSchema,
  endings: [] as TEnding[]
}
