/* eslint-disable @typescript-eslint/tslint/config */
import isNull from 'lodash/isNull'
import React, { useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getRotation, TDirection } from '../garlandAssembleData'
import { getScheme } from '../garlandAssembleSelectors'
import { tick } from '../garlandAssembleSideEffects'
import { actions, fetchInitialData } from '../garlandAssembleSlice'
import { Ending } from './Ending'
import { EmptyTile, Tile } from './Tile'
import styles from './garland.cssmodule.scss'

export const GarlandAssemble = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchInitialData())
  }, [])

  useEffect(() => {
    const intervalHandle = setInterval(() => dispatch(tick()), 1000)

    return () => clearInterval(intervalHandle)
  }, [])

  useEffect(() => () => dispatch(actions.cleanUp()), [])

  const tailsScheme = useSelector(getScheme)
  const renderEndings = useCallback(
    (endingSides: TDirection[]) =>
      endingSides?.length && endingSides.map((ending) => <Ending rotation={getRotation(ending)} key={ending} />),
    []
  )

  return (
    <div className={styles.wrapper}>
      <div className={styles.fixedSizeBoard}>
        {tailsScheme.map((tilesRow, rowIndex) => (
          <div className={styles.tileRow} key={rowIndex}>
            {tilesRow.map((tileProps, colIndex) =>
              (isNull(tileProps) ? (
                <EmptyTile key={colIndex} />
              ) : (
                <Tile
                  key={colIndex}
                  imageSrc={tileProps.imageSrc}
                  rotation={tileProps.rotation}
                  x={colIndex}
                  y={rowIndex}
                  ending={renderEndings(tileProps.endingSides)}
                />
              )))}
          </div>
        ))}
      </div>
    </div>
  )
}
