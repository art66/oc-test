import { fetchUrl } from '../../../utils'
import { TBoardAssets, TBoardRotationSchema, TDirection, TEnding, TTailRotationSchema } from './garlandAssembleData'

const action = 'miniGameAction'

type TRawTile = {
  imageName: string
  rotation: number
  connections: TTailRotationSchema
}

type TInitGameResult = {
  ends: { position: [number, number]; side: TDirection }[]
  tails: (TRawTile | null)[][]
  userData: {
    userStatus: string
    message: string
  }
}

const attachEndingData = (data: TInitGameResult, boardAssets: TBoardAssets): void =>
  data.ends.forEach(({ position: [y, x], side }) => {
    boardAssets[y][x].endingSides ? boardAssets[y][x].endingSides.push(side) : (boardAssets[y][x].endingSides = [side])
  })

const extractAssetsSchema = (data: TInitGameResult): TBoardAssets => {
  const boardAssets = data.tails.map((tailsRow) =>
    tailsRow.map(
      (tail) =>
        tail && {
          imageSrc: tail.imageName,
          rotation: tail.rotation
        }
    ))

  attachEndingData(data, boardAssets)
  return boardAssets
}

const extractRotationSchema = (data: TInitGameResult): TBoardRotationSchema =>
  data.tails.map((tailsRow) => tailsRow.map((tail) => tail && tail.connections))

const makeEnding = (end: TInitGameResult['ends'][0]): TEnding => ({
  x: end.position[1],
  y: end.position[0],
  direction: end.side
})

const convertEndingsData = (data: TInitGameResult) => data.ends.map(makeEnding)

const convertToDataModel = (data: TInitGameResult) => ({
  tailsAssets: extractAssetsSchema(data),
  tailsRotations: extractRotationSchema(data),
  endings: convertEndingsData(data)
})

export const fetchGamaInitialData = () =>
  fetchUrl(action, {
    action: 'start',
    gameType: 'gameGarlandAssemble'
  }).then(convertToDataModel)
