import { TWholeCtStateMock } from './garlandAssembleSlice'

export const getBoardLocked = (state: TWholeCtStateMock) => state.GarlandAssemble.boardLocked
export const getPuzzleSolved = (state: TWholeCtStateMock) => state.GarlandAssemble.puzzleSolved
export const getScheme = (state: TWholeCtStateMock) => state.GarlandAssemble.tailsAssets
export const getTileRotations = (state: TWholeCtStateMock) => state.GarlandAssemble.tailsRotations
export const getEndings = (state: TWholeCtStateMock) => state.GarlandAssemble.endings
export const getTimeLeft = (state: TWholeCtStateMock) => state.GarlandAssemble.time
