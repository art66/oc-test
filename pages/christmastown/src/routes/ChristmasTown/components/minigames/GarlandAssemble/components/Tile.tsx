/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import classNames from 'classnames'
import React, { memo, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getMediaType } from '../../../../selectors/browser'
import { getPuzzleSolved } from '../garlandAssembleSelectors'
import { handleTileClick } from '../garlandAssembleSideEffects'
import { getAssetsPath } from './constants'
import styles from './garland.cssmodule.scss'

type TFilledTileProps = {
  imageSrc: string
  rotation: number
  x: number
  y: number
  // eslint-disable-next-line no-undef
  ending: JSX.Element[]
}

export const Tile = memo(({ imageSrc, rotation, x, y, ending }: TFilledTileProps) => {
  const dispatch = useDispatch()
  const onClick = useCallback(() => dispatch(handleTileClick({ x, y })), [x, y])

  const mediaType = useSelector(getMediaType)
  const puzzleSolved = useSelector(getPuzzleSolved)

  return (
    <div onClick={onClick} className={classNames(styles.tile, { [styles.dimmed]: !puzzleSolved })} tabIndex={0}>
      <img style={{ transform: `rotateZ(${rotation}deg)` }} src={`${getAssetsPath(mediaType)}${imageSrc}.png`} alt='' />
      {ending}
    </div>
  )
})

export const EmptyTile = memo(() => <div className={styles.tile} />)
