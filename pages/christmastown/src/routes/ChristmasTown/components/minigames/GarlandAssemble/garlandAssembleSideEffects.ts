import { ThunkAction } from '@reduxjs/toolkit'
import { timeout } from '@torn/shared/utils/timeout'
import { endGame, getEndResult } from '../../../modules'
import { TPosition, verifySchema } from './garlandAssembleData'
import { getBoardLocked, getEndings, getPuzzleSolved, getTileRotations, getTimeLeft } from './garlandAssembleSelectors'
import { actions, TWholeCtStateMock } from './garlandAssembleSlice'

export const checkSchemaWithBackEnd = (): ThunkAction<void, TWholeCtStateMock, unknown, any> => async (
  dispatch,
  getState
) => {
  const result = (await dispatch(
    getEndResult({
      action: 'complete',
      gameType: 'gameGarlandAssemble',
      result: { schema: getTileRotations(getState()) }
    })
  )) as any

  if (result.json.gameFinished) {
    dispatch(actions.setPuzzleSolved())
    await timeout(2000)
    dispatch(endGame())
  } else if (getTimeLeft(getState()) > 0) {
    dispatch(actions.unlockBoard())
  }
}

export const handleTileClick = (position: TPosition): ThunkAction<void, TWholeCtStateMock, unknown, any> => async (
  dispatch,
  getState
) => {
  if (getBoardLocked(getState())) {
    return
  }
  await dispatch(actions.rotateTile(position))
  const isSolved = verifySchema(getTileRotations(getState()), getEndings(getState()))

  if (isSolved) {
    dispatch(actions.lockBoard())
    dispatch(checkSchemaWithBackEnd())
  }
}

export const tick = (): ThunkAction<void, TWholeCtStateMock, unknown, any> => (dispatch, getState) => {
  const timeLeft = getTimeLeft(getState())

  if (timeLeft === 1) {
    dispatch(actions.lockBoard())
    dispatch(checkSchemaWithBackEnd())
  }
  if (timeLeft >= 1 && !getPuzzleSolved(getState())) {
    dispatch(actions.decreaseTime())
  }
}
