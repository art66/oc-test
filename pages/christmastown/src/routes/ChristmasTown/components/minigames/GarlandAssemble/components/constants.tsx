const assetsPathRoot = '/images/v2/christmas_town/minigames/garland_assemble/tiles/'

const assetsPath = {
  desktop: '90x90/',
  tablet: '77x77/',
  mobile: '64x64/'
}

export const getAssetsPath = mediaType => `${assetsPathRoot}${assetsPath[mediaType] || assetsPath['desktop']}`
