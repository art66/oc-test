import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { fetchGamaInitialData } from './garlandAssembleApi'
import { getNextDirection, TBoardAssets, TBoardRotationSchema, TEnding, TPosition } from './garlandAssembleData'

type TState = {
  boardLocked: boolean
  puzzleSolved: boolean
  tailsAssets: TBoardAssets
  tailsRotations: TBoardRotationSchema
  endings: TEnding[]
  time: number
  error?: string
}

export type TWholeCtStateMock = {
  GarlandAssemble: TState
}

const boardSize = 5
const initialState: TState = {
  boardLocked: false,
  puzzleSolved: false,
  tailsAssets: Array(boardSize).fill(Array(boardSize).fill(null)),
  tailsRotations: Array(boardSize).fill(Array(boardSize).fill(null)),
  endings: [],
  time: 60
}

export const fetchInitialData = createAsyncThunk('garlandAssemble/fetchInitialData', fetchGamaInitialData)

const garlandAssembleSlice = createSlice({
  name: 'GarlandAssemble',
  initialState,
  reducers: {
    rotateTile: (state, { payload: { x, y } }: PayloadAction<TPosition>) => {
      state.tailsAssets[y][x].rotation += 90
      state.tailsRotations[y][x].forEach((direction, index) => {
        state.tailsRotations[y][x][index] = getNextDirection(direction)
      })
    },
    lockBoard: (state) => {
      state.boardLocked = true
    },
    unlockBoard: (state) => {
      state.boardLocked = false
    },
    setPuzzleSolved: (state) => {
      state.puzzleSolved = true
    },
    decreaseTime: (state) => {
      state.time--
    },
    cleanUp: (state) => {
      state.time = null
    }
  },
  extraReducers: (builder) =>
    builder
      .addCase(fetchInitialData.fulfilled, (state, { payload: { tailsAssets, tailsRotations, endings } }) => {
        state.boardLocked = initialState.boardLocked
        state.puzzleSolved = initialState.puzzleSolved
        state.time = initialState.time
        state.tailsAssets = tailsAssets
        state.tailsRotations = tailsRotations
        state.endings = endings
      })
      .addCase(fetchInitialData.rejected, (state, action) => {
        state.error = action.toString()
      })
})

export const { actions } = garlandAssembleSlice
export default garlandAssembleSlice.reducer
