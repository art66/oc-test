import React, { Component } from 'react'
import IItem from '../../interfaces/IItem'
import { CELL_SIZE, PIXELS } from '../../constants'

interface IProps {
  item: IItem
}

export class ItemsLayerItem extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps): boolean {
    const { item } = this.props
    let needUpdate = false

    try {
      needUpdate = JSON.stringify(nextProps.item) !== JSON.stringify(item)
    } catch (e) {}

    return needUpdate
  }

  render() {
    const { item } = this.props
    const maxHeight = CELL_SIZE + PIXELS
    const maxWidth = CELL_SIZE + PIXELS
    const ctItemStyle = {
      width: CELL_SIZE,
      height: CELL_SIZE,
      left: item.position.x * CELL_SIZE,
      top: item.position.y * -CELL_SIZE
    }
    const itemImageStyle = {
      maxHeight,
      maxWidth,
      position: 'absolute' as 'absolute',
      margin: 'auto',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    }

    return (
      <div className='ct-item' style={ctItemStyle}>
        <img src={item.image.url} style={itemImageStyle} />
      </div>
    )
  }
}

export default ItemsLayerItem
