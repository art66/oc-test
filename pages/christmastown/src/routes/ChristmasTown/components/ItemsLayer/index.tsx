import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemsLayerItem from './ItemsLayerItem'
import IItem from '../../interfaces/IItem'

interface IProps {
  items: IItem[]
  moveInProgress: boolean
}

class ItemsLayer extends Component<IProps> {
  positions: any

  shouldComponentUpdate(newProps: IProps) {
    if (newProps.moveInProgress) {
      return false
    }

    const positions = newProps.items.reduce((pos, item) => pos + item.position.x + item.position.y, '')
    const needUpdate = this.positions !== positions

    this.positions = positions

    return needUpdate
  }

  _getItemsList = () => {
    const { items } = this.props

    return items.map(item => {
      return <ItemsLayerItem key={`${item.position.x} ${item.position.y}`} item={item} />
    })
  }

  render() {
    return <div className='items-layer'>{this._getItemsList()}</div>
  }
}

const mapStateToProps = state => ({
  items: state.christmastown.mapData.items,
  moveInProgress: state.christmastown.mapData.moveInProgress
})

export default connect(mapStateToProps)(ItemsLayer)
