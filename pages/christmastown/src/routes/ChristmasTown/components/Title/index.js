import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { BLUE_FILL, DISABLE_FILL } from '@torn/shared/SVG/presets/icons/christmastown'
import { ANIMATIONS, KEY_CODE_ENTER, MESSAGE_MAX_LENGTH, TOOLTIP_DELAY } from '../../constants'
import { setUserField, resetLocation, makeGesture, setUserPosition } from '../../modules'
import { sendChatMessage } from '../../actions'
import GameStatus from '../GameStatus'
import { isInt } from '../../utils'
import s from './styles.cssmodule.scss'
/* global $ */

/* eslint-disable jsx-a11y/click-events-have-key-events */
export class Title extends Component {
  constructor(props) {
    super(props)
    this.state = {
      coordinatesInputX: '',
      coordinatesInputY: '',
      timeToResetSuicide: props.timeToResetSuicide,
      suicideDialog: false,
      message: ''
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { timeToResetSuicide } = prevState

    if (timeToResetSuicide !== nextProps.timeToResetSuicide) {
      return {
        timeToResetSuicide: nextProps.timeToResetSuicide
      }
    }

    return null
  }

  componentDidMount() {
    this.setResetLocationTimer(this.state.timeToResetSuicide)
  }

  componentDidUpdate(prevProps, prevState) {
    const { timeToResetSuicide } = this.props

    if (timeToResetSuicide !== prevProps.timeToResetSuicide) {
      this.setResetLocationTimer(timeToResetSuicide)
    }
  }

  componentWillUnmount() {
    this.props.setUserField('timeToResetSuicide', this.state.timeToResetSuicide)
    clearInterval(this.state.suicideTimeInterval)
  }

  getGestureIcon = animationName => {
    const type = [ANIMATIONS.HANDS_UP, ANIMATIONS.HAND_MOTION, ANIMATIONS.PULSING].includes(animationName) ?
      animationName :
      ''

    return (
      <div className={cn('img-wrap', 'demo', 'svgImageWrap', 'you', animationName, 'infiniteAnimation')}>
        <div className={cn('ellipse', 'inList')} />
        <SVGIconGenerator iconName='CTPlayer' type={type} preset={{ type: 'christmasTown', subtype: 'PLAYER' }} />
      </div>
    )
  }

  validateCoordinates = coordinate => {
    return coordinate || coordinate === 0 ? coordinate : '-'
  }

  getSuicideAndCoordinatesMarkup = () => {
    const { position } = this.props
    const { suicideDialog, timeToResetSuicide } = this.state
    const positionX = this.validateCoordinates(position.x)
    const positionY = this.validateCoordinates(position.y)

    return suicideDialog ? (
      <div className={s.dialog}>
        <span>Are you sure?</span>
        <span onClick={() => this.resetLocation()} className={s.yes}>
          Yes
        </span>
        <span onClick={() => this.hideSuicideDialog()} className={s.no}>
          No
        </span>
      </div>
    ) : (
      <div className={s.positionReset}>
        <span
          id='reset-position'
          className={cn(s.resetButton, { [s.disabled]: timeToResetSuicide > 0 })}
          onClick={() => this._handleResetClick()}
          style={{
            cursor: timeToResetSuicide > 0 ? 'default' : 'pointer'
          }}
        />
        <Tooltip parent='reset-position' arrow='center' position='top'>
          Respawn: Instantly travel back to the spawn area
        </Tooltip>
        <span className={s.position}>{`${positionX},${positionY}`}</span>
      </div>
    )
  }

  showSuicideDialog = () => {
    this.setState({ suicideDialog: true })
  }

  hideSuicideDialog = () => {
    this.setState({ suicideDialog: false })
  }

  canSuicide = () => {
    const { timeToResetSuicide } = this.state

    return timeToResetSuicide <= 0
  }

  setResetLocationTimer = (time = 0) => {
    const { timeToResetSuicide } = this.state

    clearInterval(this.suicideTimeInterval)
    if (time > 0) {
      this.suicideTimeInterval = setInterval(() => {
        this.setState({ timeToResetSuicide: timeToResetSuicide - 1 })
        if (timeToResetSuicide < 0) {
          clearInterval(this.suicideTimeInterval)
        }
      }, 1000)
    }
  }

  isValidCoords = () => {
    const { coordinatesInputX, coordinatesInputY } = this.state

    return isInt(coordinatesInputX) && isInt(coordinatesInputY)
  }

  _handleChangingUserPosition = () => {
    const { coordinatesInputX, coordinatesInputY } = this.state
    const isValidCoords = this.isValidCoords()

    if (isValidCoords) {
      this.props.setUserPosition(coordinatesInputX, coordinatesInputY)
    }
  }

  _handleKeyPressInCoordsInputs = ({ key }) => {
    if (key === 'Enter') {
      this._handleChangingUserPosition()
    }
  }

  resetLocation = () => {
    this.hideSuicideDialog()

    if (this.canSuicide()) {
      this.props.resetLocation()
    }
  }

  _handleResetClick = () => {
    if (this.canSuicide()) {
      this.showSuicideDialog()
    }
  }

  _handleCoordinatesInputChange = e => {
    const newInputValue = e.target.value
    const prevInputValue = this.state[e.target.name]
    const containOnlyNums = /^-?\d*$/.test(newInputValue)

    this.setState({
      [e.target.name]: newInputValue.length && !containOnlyNums ? prevInputValue : newInputValue
    })
  }

  _handleMessageChange = e => {
    const message = e.target.value.slice(0, MESSAGE_MAX_LENGTH).replace(/\s\s+/g, ' ')

    this.setState({ message })
  }

  _handleMessageSend = e => {
    const { message } = this.state

    if (e.keyCode === KEY_CODE_ENTER) {
      if (!message.trim().length) {
        this.setState({ message: '' })

        return
      }

      this.props.sendChatMessage(message)
      this.setState({ message: '' })
    }
  }

  render() {
    const { isAdmin, mediaType, itsaMinigameCell, gameControls, gameTitle = 'Status Area', mapName } = this.props
    const { coordinatesInputX, coordinatesInputY, message, suicideDialog } = this.state
    const suicideAndCoordinates = this.getSuicideAndCoordinatesMarkup()
    const isValidCoords = this.isValidCoords()

    return (
      <div>
        {itsaMinigameCell ? (
          <div className='status-title'>
            {gameTitle}
            {gameControls && <div className='game-controls'>{gameControls}</div>}
            <GameStatus />
          </div>
        ) : (
          <span className='status-title t-hide'>
            {mapName}
            {mediaType !== 'mobile' && suicideAndCoordinates}
            {isAdmin && (
              <div className='actions'>
                <label className='coordinates-input-label' htmlFor='coordinatesInputX'>
                  x:
                </label>
                <input
                  id='coordinatesInputX'
                  type='text'
                  className='coordinates-input'
                  name='coordinatesInputX'
                  autoComplete='off'
                  onChange={this._handleCoordinatesInputChange}
                  onKeyPress={this._handleKeyPressInCoordsInputs}
                  value={coordinatesInputX}
                />
                <label className='coordinates-input-label' htmlFor='coordinatesInputY'>
                  y:
                </label>
                <input
                  id='coordinatesInputY'
                  type='text'
                  className='coordinates-input'
                  name='coordinatesInputY'
                  autoComplete='off'
                  onChange={this._handleCoordinatesInputChange}
                  onKeyPress={this._handleKeyPressInCoordsInputs}
                  value={coordinatesInputY}
                />
                <button
                  type='button'
                  className={cn('search-icon', s.searchBtn, { [s.disable]: !isValidCoords })}
                  onClick={this._handleChangingUserPosition}
                >
                  <SVGIconGenerator
                    iconName='Search'
                    preset={{
                      type: 'christmasTown',
                      subtype: 'SEARCH'
                    }}
                    fill={isValidCoords ? BLUE_FILL : DISABLE_FILL}
                    customClass={cn(s.titleIcon, { [s.disabled]: !isValidCoords })}
                  />
                </button>
              </div>
            )}
          </span>
        )}

        {((itsaMinigameCell && mediaType === 'desktop') || !itsaMinigameCell) && (
          <span className='map-title'>
            {(!suicideDialog || mediaType === 'desktop') && (
              <span className='ct-title m-left10'>
                <input
                  type='text'
                  placeholder='Type a message...'
                  className={s.messageInput}
                  value={message}
                  onChange={this._handleMessageChange}
                  onKeyDown={this._handleMessageSend}
                />
              </span>
            )}
            <div id='makeGesture' className={s.makeGesture} style={{ display: 'block' }}>
              <SVGIconGenerator
                customClass={s.titleIcon}
                iconName='CTGesture'
                preset={{ type: 'christmasTown', subtype: 'GESTURE' }}
              />
              <Tooltip
                parent='makeGesture'
                arrow='center'
                position={mediaType === 'mobile' ? 'left' : 'top'}
                delay={TOOLTIP_DELAY}
              >
                <ul className={s.gestureList}>
                  <li onClick={() => this.props.makeGesture(ANIMATIONS.HAND_MOTION)}>
                    {this.getGestureIcon(ANIMATIONS.HAND_MOTION)}
                  </li>
                  <li onClick={() => this.props.makeGesture(ANIMATIONS.HANDS_UP)}>
                    {this.getGestureIcon(ANIMATIONS.HANDS_UP)}
                  </li>
                  <li onClick={() => this.props.makeGesture(ANIMATIONS.PULSING)}>
                    {this.getGestureIcon(ANIMATIONS.PULSING)}
                  </li>
                  <li onClick={() => this.props.makeGesture(ANIMATIONS.SHAKING)}>
                    {this.getGestureIcon(ANIMATIONS.SHAKING)}
                  </li>
                  <li onClick={() => this.props.makeGesture(ANIMATIONS.JUMPING)}>
                    {this.getGestureIcon(ANIMATIONS.JUMPING)}
                  </li>
                </ul>
              </Tooltip>
            </div>
            {$('html').hasClass('html-manual-desktop') ? null : mediaType !== 'desktop' && suicideAndCoordinates}
          </span>
        )}
      </div>
    )
  }
}

Title.propTypes = {
  mediaType: PropTypes.string,
  itsaMinigameCell: PropTypes.bool,
  gameControls: PropTypes.element,
  gameTitle: PropTypes.string,
  isAdmin: PropTypes.bool,
  position: PropTypes.object,
  setUserPosition: PropTypes.func,
  setUserField: PropTypes.func,
  resetLocation: PropTypes.func,
  timeToResetSuicide: PropTypes.number,
  makeGesture: PropTypes.func,
  sendChatMessage: PropTypes.func,
  mapName: PropTypes.string.isRequired
}

const mapStateToProps = state => ({
  mediaType: !document.body.classList.contains('r') ? 'desktop' : state.browser.mediaType,
  itsaMinigameCell: state.christmastown.minigame.itsaMinigameCell,
  gameControls: state.christmastown.minigame.gameControls,
  gameTitle: state.christmastown.minigame.gameTitle,
  position: state.christmastown.mapData.user.position,
  isAdmin: state.christmastown.mapData.user.isAdmin,
  timeToResetSuicide: state.christmastown.mapData.user.timeToResetSuicide,
  mapName: state.christmastown.mapData.mapName
})

const mapDispatchToProps = {
  setUserField,
  resetLocation,
  makeGesture,
  setUserPosition,
  sendChatMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(Title)
