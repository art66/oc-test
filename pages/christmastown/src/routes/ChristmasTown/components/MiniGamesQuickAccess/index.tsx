import React, { Component } from 'react'
import styles from './index.cssmodule.scss'

interface IProps {
  loadGame: (gameName: string) => void
}

class MiniGamesQuickAccess extends Component<IProps> {
  private loadGame = (gameName: string) => () => this.props.loadGame(gameName)

  // eslint-disable-next-line @typescript-eslint/member-ordering
  render() {
    return (
      <div className={styles.miniGamesQuickAccessWrap}>
        <div className={styles.titleWrap}>
          <span className={styles.title}>(DEV) Mini-Games Quick Access: </span>
        </div>
        <br />
        <div className={styles.miniGamesButtonsWrap}>
          <button className={styles.button} type='button' onClick={this.loadGame('Pot')}>
            Pot
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('SantaClaws')}>
            SantaClaws
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('GingerBreadMen')}>
            GingerBreadMen
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('SpinTheWheel')}>
            SpinTheWheel
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('SnowballShooter')}>
            SnowballShooter
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('Hangman')}>
            Hangman
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('PinTheNose')}>
            PinTheNose
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('WordFixer')}>
            WordFixer
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('PickYourPresent')}>
            PickYourPresent
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('GarlandAssemble')}>
            GarlandAssemble
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('ChristmasWreath')}>
            ChristmasWreath
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('Typocalypse')}>
            Typocalypse
          </button>
          <button className={styles.button} type='button' onClick={this.loadGame('CouponExchange')}>
            Coupon Exchange
          </button>
        </div>
      </div>
    )
  }
}

export default MiniGamesQuickAccess
