import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './style.scss'
import { positionsEqual } from '../../utils'
import UsersLayerItem from './UsersLayerItem'

export class UsersLayer extends Component {
  shouldComponentUpdate(newProps) {
    let diff = newProps.users.reduce(
      (pos, user) =>
        pos + user.position.x + user.position.y + user.exited + user.hospitalized + user.gesture + user.message,
      ''
    )

    diff +=
      `${newProps.currentUser.position.x}${newProps.currentUser.position.y}`
      + `${newProps.currentUser.hospitalized}${newProps.currentUser.gesture}${newProps.currentUser.message}`

    const needUpdate = this.diff !== diff

    this.diff = diff

    return needUpdate
  }

  buildUsersList = () => {
    const { users, currentUser } = this.props

    const usersOnCells = {}

    return users.map(user => {
      const cellKey = `${user.position.x}-${user.position.y}`

      if (!usersOnCells[cellKey]) {
        usersOnCells[cellKey] = users.filter(item => positionsEqual(item.position, user.position))
      }

      return (
        <UsersLayerItem
          key={user.user_id}
          user={user}
          currentUser={currentUser}
          indexInCell={usersOnCells[cellKey].findIndex(item => item.user_id === user.user_id)}
          qtyUsersOnCell={usersOnCells[cellKey].length}
        />
      )
    })
  }

  render() {
    const usersList = this.buildUsersList()
    const { users, currentUser, movingTime } = this.props
    const usersOnCell = users.filter(item => positionsEqual(item.position, currentUser.position))

    return (
      <div className='users-layer'>
        {usersList}
        <UsersLayerItem
          user={currentUser}
          currentUser={currentUser}
          movingTime={movingTime}
          indexInCell={usersOnCell.findIndex(item => item.user_id === currentUser.user_id)}
          qtyUsersOnCell={usersOnCell.length}
        />
      </div>
    )
  }
}

UsersLayer.propTypes = {
  users: PropTypes.array.isRequired,
  currentUser: PropTypes.object.isRequired,
  movingTime: PropTypes.number
}

const mapStateToProps = state => ({
  users: state.christmastown.mapData.users,
  currentUser: state.christmastown.mapData.user,
  movingTime: state.christmastown.mapData.movingTime
})

export default connect(mapStateToProps)(UsersLayer)
