/* eslint-disable max-statements */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import NpcIcon from '@torn/christmas-town-editor/src/components/NpcIcon'
import { positionsEqual, getDirectionToPosition } from '../../utils'
import { setUserGesture, removeUser, startMoving } from '../../modules'
import { setUserMessage } from '../../actions'
import { CELL_SIZE, NPC, MOVING_TIME_DEFAULT, ANIMATIONS, MESSAGE_REPOSITIONING_OFFSET } from '../../constants'

const EXIT_TRANSITION_TIME = 500
const MIN_TELEPORT_DISTANCE_CELLS = 5
const GESTURE_ANIMATION_TIME = 1000
const MESSAGE_TIMEOUT = 3000
const MESSAGE_LINE_LENGTH = 25
const DEFAULT_MARGIN = { top: 8, left: 12 }
const MARGINS = {
  0: DEFAULT_MARGIN,
  1: { top: 13, left: 5 },
  2: { top: 13, left: 18 },
  3: { top: 5, left: 7 },
  4: { top: 5, left: 16 },
  5: { top: 16, left: 12 },
  6: { top: 2, left: 2 },
  7: { top: 1, left: 11 },
  8: { top: 13, left: 5 },
  9: { top: 2, left: 21 },
  10: { top: 15, left: 1 },
  11: { top: 15, left: 23 },
  12: { top: 16, left: 8 },
  13: { top: 16, left: 16 }
}

export class UsersLayerItem extends Component {
  constructor(props) {
    super(props)

    this.timer = null
  }

  shouldComponentUpdate(newProps) {
    let usersLength = newProps.qtyUsersOnCell

    if (positionsEqual(newProps.user.position, newProps.currentUser.position)) {
      usersLength++
    }
    const needUpdate =
      this.usersLength !== usersLength
      || !positionsEqual(newProps.user.position, this.props.user.position)
      || this.props.user.exited !== newProps.user.exited
      || this.props.user.hospitalized !== newProps.user.hospitalized
      || this.props.user.gesture !== newProps.user.gesture
      || this.props.user.message !== newProps.user.message

    this.usersLength = usersLength

    return needUpdate
  }

  componentDidMount() {
    const { user } = this.props

    if (user.message) {
      clearTimeout(this.timer)
      this.timer = setTimeout(() => this.props.setUserMessage(undefined, user.user_id), MESSAGE_TIMEOUT)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { user } = this.props

    this.prevPosition = prevProps.user.position

    if (user.gesture && user.gesture.length && user.gesture !== prevProps.user.gesture) {
      setTimeout(() => {
        this.props.setUserGesture('', user.user_id)
      }, GESTURE_ANIMATION_TIME)
    }

    if (this.checkIfTeleported(user.position, this.prevPosition) && !this.checkIfCurrentUser()) {
      setTimeout(() => {
        this.props.removeUser(user)
      }, EXIT_TRANSITION_TIME)
    }

    if (user.message && user.message !== prevProps.user.message) {
      clearTimeout(this.timer)
      this.timer = setTimeout(() => this.props.setUserMessage(undefined, user.user_id), MESSAGE_TIMEOUT)
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timer)
  }

  checkIfTeleported = (position, prevPosition) =>
    position
    && prevPosition
    && (Math.abs(position.x - prevPosition.x) >= MIN_TELEPORT_DISTANCE_CELLS
      || Math.abs(position.y - prevPosition.y) >= MIN_TELEPORT_DISTANCE_CELLS)

  checkIfCurrentUser = () => parseInt(this.props.user.user_id, 10) === parseInt(this.props.currentUser.user_id, 10)

  _handleMouseDown = () => {
    const direction = this._getDirection()

    this.props.startMoving(direction)
  }

  handleMouseEnter = e => {
    e.preventDefault()
    const { user, currentUser } = this.props

    if (user.exited) {
      return
    }

    const userMargin = this._getUserMargin()
    const elementId = `ctUser${user.user_id}`

    this.showTooltip({
      content: user.user_id === currentUser.user_id ? 'You' : user.playername,
      position: user.position,
      elementId,
      margin: { top: userMargin.top, left: userMargin.left }
    })
  }

  _getUserMargin = () => {
    const { user, currentUser, indexInCell } = this.props
    const currentUserHere = positionsEqual(currentUser.position, user.position)

    return MARGINS[currentUserHere ? indexInCell + 1 : indexInCell] || MARGINS[Math.floor(Math.random() * 13)]
  }

  showTooltip = ({ content, position, margin, elementId }) => {
    const tooltipContainer = document.getElementById('tooltipContainer')
    const parentEl = document.getElementById(elementId)
    const tooltipId = `ctTooltip-${elementId}`

    if (!tooltipContainer || !parentEl || document.getElementById(tooltipId)) {
      return false
    }

    const tooltip = document.createElement('div')
    const arrow = document.createElement('div')

    tooltip.innerText = content
    tooltip.className = 'ctTooltip'
    tooltip.id = tooltipId
    tooltip.style.padding = '8px'
    tooltip.style.position = 'absolute'
    tooltip.style.zIndex = '1100'
    tooltipContainer.innerHTML = ''
    tooltipContainer.appendChild(tooltip)
    tooltip.style.left = `${position.x * CELL_SIZE + parentEl.clientWidth / 2 - tooltip.clientWidth / 2}px`
    tooltip.style.top = `${position.y * -CELL_SIZE - parentEl.clientHeight / 2 - tooltip.clientHeight / 2 - 16}px`
    tooltip.style.margin = `${margin.top || 0}px ${margin.right || 0}px ${margin.bottom || 0}px ${margin.left || 0}px`
    arrow.className = 'arrow'
    tooltip.appendChild(arrow)
  }

  hideTooltip = () => {
    const tooltipContainer = document.getElementById('tooltipContainer')

    tooltipContainer.innerHTML = ''
  }

  _getDirection = () => {
    const { user, currentUser } = this.props

    return getDirectionToPosition(currentUser.position, user.position)
  }

  _getUserClassName = () => {
    const { user, currentUser } = this.props

    if (user.user_id === currentUser.user_id) {
      return 'you'
    }

    const gender = user.gender || 'other'

    let imgID = parseInt(user.user_id.toString().slice(-1), 10)

    if (imgID <= 0 || imgID > 4) {
      imgID -= 4
      imgID = Math.abs(imgID)
    }

    if (imgID > 4) {
      imgID = 4
    }

    return `${gender.toLocaleLowerCase()}_${imgID}`
  }

  _getImageName = () => {
    const { user } = this.props

    if (!user.user_id) {
      return
    }

    return user.type === NPC ? user.npcType : this._getUserClassName()
  }

  _getImage = () => {
    const imgName = this._getImageName()

    if (this.props.user.type === NPC) {
      return (
        <div className='img-wrap'>
          <NpcIcon
            color={this.props.user.color}
            direction={this.props.user.direction}
            npcType={this.props.user.npcType}
            isMapNpc={true}
          />
        </div>
      )
    }

    const gesture = this.props.user.gesture || ''
    const type = [ANIMATIONS.HANDS_UP, ANIMATIONS.HAND_MOTION, ANIMATIONS.PULSING].includes(gesture) ? gesture : ''

    return (
      <div className={cn('img-wrap', 'svgImageWrap', imgName, gesture)}>
        <div className='ellipse' />
        <SVGIconGenerator type={type} iconName='CTPlayer' preset={{ type: 'christmasTown', subtype: 'PLAYER' }} />
      </div>
    )
  }

  _getMessagePosition = () => {
    const { user, currentUser } = this.props
    const positions = []

    if (user.position.y - currentUser.position.y > MESSAGE_REPOSITIONING_OFFSET) {
      positions.push('bottomPos')
    } else {
      positions.push('topPos')
    }
    if (user.position.x - currentUser.position.x > MESSAGE_REPOSITIONING_OFFSET) {
      positions.push('leftPos')
    }
    if (user.position.x - currentUser.position.x < -MESSAGE_REPOSITIONING_OFFSET) {
      positions.push('rightPos')
    }

    return positions
  }

  _renderUserMessage = (userPosition, movingTransitionTime) => {
    const { user } = this.props
    const { top, left } = this._getUserMargin()
    const messageOffsetLeft = DEFAULT_MARGIN.left - left
    const messageOffsetTop = DEFAULT_MARGIN.top - top
    const transformX = userPosition.x * CELL_SIZE - messageOffsetLeft
    const transformY = userPosition.y * -CELL_SIZE - messageOffsetTop
    const style = {
      transform: `translate(${transformX}px, ${transformY}px)`,
      transition: `transform ${movingTransitionTime}s linear, margin 0.5s linear`
    }

    let { message } = user

    if (
      user.message.length > MESSAGE_LINE_LENGTH
      && !/\s/.test(user.message[MESSAGE_LINE_LENGTH])
      && !/\s/.test(user.message[MESSAGE_LINE_LENGTH - 1])
    ) {
      message = `${user.message.slice(0, MESSAGE_LINE_LENGTH)}-\n${user.message.slice(MESSAGE_LINE_LENGTH)}`
    }

    return (
      <div className={cn('messageWrapper', this._getUserClassName())} style={style}>
        <div className={cn('chatBubble', this._getMessagePosition())}>
          <span className='messageText'>{message}</span>
        </div>
      </div>
    )
  }

  render() {
    const { user, movingTime } = this.props
    const userMargin = this._getUserMargin()
    const elementId = `ctUser${user.user_id}`
    const image = this._getImage()
    const movingTransitionTime = (movingTime || user.movingTime || MOVING_TIME_DEFAULT) / 1000
    const exited =
      !this.checkIfCurrentUser() && (user.exited || this.checkIfTeleported(user.position, this.prevPosition))
    const position = exited ? this.prevPosition || user.position : user.position
    const userStyle = {
      transform: `translate(${position.x * CELL_SIZE}px, ${position.y * -CELL_SIZE}px)`,
      transition: `transform ${movingTransitionTime}s linear, margin 0.5s linear`,
      width: user.type === NPC ? 0 : 8,
      height: user.type === NPC ? 0 : 14,
      marginTop: user.type === NPC ? 0 : userMargin.top,
      marginLeft: user.type === NPC ? 0 : userMargin.left,
      display: user.invisible ? 'none' : 'block'
    }

    return (
      <>
        <div
          key={user.user_id}
          onMouseDown={this._handleMouseDown}
          onMouseEnter={e => this.handleMouseEnter(e)}
          onTouchStart={e => this.handleMouseEnter(e)}
          onTouchEnd={() => this.hideTooltip()}
          onMouseLeave={() => this.hideTooltip()}
          onMouseMove={e => this.handleMouseEnter(e)}
          onContextMenu={e => e.preventDefault()}
          className={cn('ct-user', { npc: user.type === NPC }, { hospitalized: user.hospitalized, exited })}
          id={elementId}
          style={userStyle}
        >
          {image}
        </div>
        {user.message && this._renderUserMessage(position, movingTransitionTime)}
      </>
    )
  }
}

UsersLayerItem.propTypes = {
  user: PropTypes.object,
  currentUser: PropTypes.object,
  usersOnSameCell: PropTypes.array,
  movingTime: PropTypes.number,
  indexInCell: PropTypes.number,
  setUserGesture: PropTypes.func,
  removeUser: PropTypes.func,
  setUserMessage: PropTypes.func
}

export default connect(
  () => ({}),
  {
    setUserGesture,
    removeUser,
    setUserMessage,
    startMoving
  }
)(UsersLayerItem)
