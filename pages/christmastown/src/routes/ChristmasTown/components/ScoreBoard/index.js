import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { loadGame, leaveGame, move } from '../../modules'
import PrizesList from '../StatusArea/PrizesList'
import s from './styles.cssmodule.scss'
import CouponsForDay from '../../../../components/CouponsForDays'
import { COUPONS_MAX_AMOUNTS_FOR_DAY } from '../minigames/CouponExchange/constants'

export class ScoreBoard extends Component {
  constructor(props) {
    super(props)
    this.retryGame = this.retryGame.bind(this)
    this.leave = this.leave.bind(this)
    this.retry = false
  }

  openUserPage(userId) {
    const link = `${window.location.origin}/profiles.php?XID=${userId}`

    window.open(link, '_self')
  }

  retryGame() {
    this.props.leaveGame()
    this.retry = true
  }

  leave() {
    this.props.move('bottom')
    this.props.leaveGame()
  }

  componentWillUnmount() {
    if (this.retry) {
      this.props.loadGame(this.props.gameName)
    }
  }

  render() {
    const {
      message,
      board,
      prizes,
      title,
      withRetryButton,
      withLeaveButton,
      children,
      gameName,
      couponsAmountForDay,
      isPrizeCouponAvailable
    } = this.props

    return (
      <div className={s['score-board']}>
        <div>
          <p className={s['message']} dangerouslySetInnerHTML={{ __html: message }} />
          {prizes ? <PrizesList prizes={prizes} /> : ''}
        </div>
        {isPrizeCouponAvailable && (
          <CouponsForDay currentCoupons={couponsAmountForDay} maxCoupons={COUPONS_MAX_AMOUNTS_FOR_DAY} />
        )}
        <div>
          <div className={s['board']}>
            {board && board.list.length > 0 && (
              <ul className={s['users-table']}>
                <li className={s['table-header']}>
                  <div className={`${s['cell']} ${s['position']}`}>#</div>
                  <div className={s['cell']}>{title}</div>
                  <div className={`${s['cell']} ${s['score']}`}>{gameName === 'Pot' ? 'Items' : 'Score'}</div>
                </li>
                {board.list.map((user, i) => (
                  <li key={i} className={user.itIsMe ? s['current-user'] : ''}>
                    <div className={`${s['cell']} ${s['position']}`}>{user.position}</div>
                    <div
                      className={`${s['cell']} ${s['user-name']}`}
                      onClick={() => {
                        this.openUserPage(user.user_id)
                      }}
                    >
                      {board.textnames ? user.playername : <img src={user.image} alt='user' />}
                    </div>
                    <div className={cn(s['cell'], s['score'])}>{user.score}</div>
                  </li>
                ))}
              </ul>
            )}
            {children}
          </div>
          <div className='buttons'>
            {(withRetryButton || (prizes && !prizes.length)) && withRetryButton !== false && (
              <button type='button' className={cn('btn-blue', s['button'])} onClick={this.retryGame}>
                RETRY
              </button>
            )}
            {(withLeaveButton || prizes) && (
              <button type='button' className={cn('btn-blue', s['button'])} onClick={this.leave}>
                LEAVE
              </button>
            )}
          </div>
        </div>
      </div>
    )
  }
}

ScoreBoard.propTypes = {
  message: PropTypes.string,
  title: PropTypes.string,
  prizes: PropTypes.array,
  board: PropTypes.object,
  leaveGame: PropTypes.func,
  move: PropTypes.func,
  loadGame: PropTypes.func,
  children: PropTypes.node,
  gameName: PropTypes.string,
  withLeaveButton: PropTypes.bool,
  withRetryButton: PropTypes.bool,
  couponsAmountForDay: PropTypes.number,
  isPrizeCouponAvailable: PropTypes.bool
}

const mapStateToProps = state => ({
  couponsAmountForDay: state.christmastown.mapData?.couponsAmountForDay,
  isPrizeCouponAvailable: state.christmastown.mapData?.trigger?.isPrizeCouponAvailable
})

const mapActionsToProps = {
  leaveGame,
  loadGame,
  move
}

export default connect(mapStateToProps, mapActionsToProps)(ScoreBoard)
