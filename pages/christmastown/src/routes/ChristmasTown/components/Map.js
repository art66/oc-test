import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { convertUserPosToPix } from '../utils'
import ObjectsLayer from './ObjectsLayer/index'
import ItemsLayer from './ItemsLayer'
import StepsLayer from './StepsLayer/index'
import UsersLayer from './UsersLayer/UsersLayer'
import Directions from './Directions'

export class Map extends Component {
  render() {
    const { position, movingTime, dataFetched, screenColor, mapSize } = this.props
    const { mapWidth, mapHeight } = mapSize
    const userPosition = convertUserPosToPix(position, mapHeight)
    const worldStyles = {
      WebkitTransform: `translate(${userPosition.x}px, ${userPosition.y}px)`,
      transform: `translate(${userPosition.x}px, ${userPosition.y}px)`,
      width: mapWidth,
      height: mapHeight,
      transition: `transform ${movingTime / 1000}s linear`
    }

    return (
      <div className={cn('map-overview', `screen-color-${screenColor || 'transparent'}`)}>
        {dataFetched && (
          <div
            id='world'
            className='world'
            ref={worldElement => {
              this.world = worldElement
            }}
            style={worldStyles}
          >
            <div className='grid-layer'>
              <div className='substrate-layer' />
              <div className='negative-coordinates' style={{ top: mapHeight / 2 }}>
                <ObjectsLayer />
                <ItemsLayer />
                <StepsLayer />
                <UsersLayer />
                <Directions />
                <div id='tooltipContainer' />
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}

Map.propTypes = {
  mapSize: PropTypes.object.isRequired,
  position: PropTypes.object.isRequired,
  movingTime: PropTypes.number,
  screenColor: PropTypes.string,
  dataFetched: PropTypes.bool
}

const mapStateToProps = state => {
  return {
    mapSize: state.christmastown.mapData.mapSize,
    position: state.christmastown.mapData.user.position,
    movingTime: state.christmastown.mapData.movingTime,
    screenColor: state.christmastown.mapData.screenColor,
    dataFetched: state.christmastown.mapData.christmasTownDataFetched
  }
}

export default connect(mapStateToProps)(Map)
