import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
// eslint-disable-next-line no-restricted-imports
import _ from 'lodash'
import Tooltip from '@torn/shared/components/Tooltip'
import DebugModeClarityIcon from '../../../../../components/DebugModeClarityIcon/DebugModeClarityIcon'
import { ITEMS_URL, CT_ITEMS_URL, ITEMS_IMG_EXTENSION, TORN_ITEMS, MONEY_ITEM_CATEGORY } from '../../../constants'
import { getItemType } from '../../../utils'
import s from './styles.cssmodule.scss'

class PrizesList extends Component {
  getItemImageUrl = ({ category, type, quantity }) => {
    const typeItem = getItemType(category, type, quantity)

    return category === TORN_ITEMS ?
      `${ITEMS_URL + type}/large${ITEMS_IMG_EXTENSION}` :
      `${CT_ITEMS_URL}${category}/${category}-${typeItem}${ITEMS_IMG_EXTENSION}`
  }

  getPrizesList = (prizes = this.props.prizes) =>
    prizes.map((prize, index) => {
      if (prize.category === MONEY_ITEM_CATEGORY) {
        return null
      }
      // NOTE: An exception on playing "Spin The Wheel" game we should avoid image for "$1 Dollar" prize

      return (
        <div
          key={`prize${index}`}
          id={prize.category + prize.type + index}
          className={cn(s.prize, 'ct-with-tooltip', { [s.special]: prize.category !== 'tornItems' })}
        >
          <img src={this.getItemImageUrl(prize)} />
          {!prize.isReceived && <DebugModeClarityIcon />}
          <div className={s.quantity}>{prize.quantity > 1 ? prize.quantity : ''}</div>
          <Tooltip position='bottom' arrow='center' parent={prize.category + prize.type + index}>
            {prize.name}
          </Tooltip>
        </div>
      )
    })

  render() {
    const { prizes, withSnowGlobe, isXmasNpc } = this.props
    const tornItemsPrizesList = this.getPrizesList(prizes.filter(prize => prize.category === 'tornItems'))
    const specialPrizes = prizes
      .filter(prize => prize.category !== 'tornItems')
      .map(prize => ({ ...prize, quantity: prize.quantity || prizes.filter(p => p.item_id === prize.item_id).length }))
    const specialPrizesList = this.getPrizesList(_.uniqBy(specialPrizes, 'item_id'))

    return (
      <div>
        <div className={cn({ [s.prizesRow]: true, [s.withSnowGlobe]: withSnowGlobe, [s.isXmasNpc]: isXmasNpc })}>
          {specialPrizesList}
        </div>
        <div>{tornItemsPrizesList}</div>
      </div>
    )
  }
}

PrizesList.propTypes = {
  prizes: PropTypes.array,
  withSnowGlobe: PropTypes.bool,
  isXmasNpc: PropTypes.bool
}

export default PrizesList
