import React, { Component } from 'react'
import uniqBy from 'lodash/uniqBy'
import cn from 'classnames'
import { ITEMS_URL, CT_ITEMS_URL, ITEMS_IMG_EXTENSION, TORN_ITEMS } from '../../constants'
import DebugModeClarityIcon from '../../../../components/DebugModeClarityIcon/DebugModeClarityIcon'
import { getItemType } from '../../utils'

const CHEST_TYPES = {
  2: 'Bronze',
  3: 'Silver',
  4: 'Golden'
}

interface IProps {
  cellEvent: {
    chestOpened: boolean
    canOpen: boolean
    chestOpeningStart: boolean
    message: string
    chest: {
      type: number
    }
    prizes: {
      category: string
      name: string
      quantity: number
      type2: string
      // eslint-disable-next-line camelcase
      item_id: string
    }[]
  }
  openChest: () => void
  quitCellEvent: () => void
  showTooltip: (e) => void
  hideTooltip: (e) => void
}

export class ChestDialog extends Component<IProps> {
  getItemImageUrl = ({ category, type, quantity }) => {
    const typeItem = getItemType(category, type, quantity)

    return category === TORN_ITEMS ?
      `${ITEMS_URL + type}/large${ITEMS_IMG_EXTENSION}` :
      `${CT_ITEMS_URL}${category}/${category}-${typeItem}${ITEMS_IMG_EXTENSION}`
  }

  getPrizesList = prizes => {
    const { showTooltip, hideTooltip } = this.props
    const prizesList = []

    prizes.map((prize, index) => {
      return prizesList.push(
        <div
          key={`prize${index}`}
          className={cn('prize', 'ct-with-tooltip', { special: prize.category !== 'tornItems' })}
          onMouseEnter={showTooltip}
          onMouseLeave={hideTooltip}
        >
          <img src={this.getItemImageUrl(prize)} />
          {!prize.isReceived && <DebugModeClarityIcon />}
          <div className='ct-tooltip'>{prize.name + (prize.type2 ? ` - ${prize.type2}` : '')}</div>
          <div className='quantity'>{prize.quantity > 1 ? prize.quantity : ''}</div>
        </div>
      )
    })

    return prizesList
  }

  getContent = () => {
    const {
      cellEvent: { prizes, chestOpened, chestOpeningStart, canOpen, chest, message }
    } = this.props
    let content

    if (!chestOpened && !chestOpeningStart) {
      content = canOpen ? (
        <div>
          <div>Remember that key you found earlier?</div>
          <div>Perhaps it will open this chest?</div>
        </div>
      ) : (
        <div>You don&#39;t have any {CHEST_TYPES[chest.type]} keys</div>
      )
    } else if (chestOpeningStart) {
      content = (
        <div>
          <i className='saving-icon' />
        </div>
      )
    } else {
      const tornItemsPrizesList = this.getPrizesList(prizes.filter(prize => prize.category === 'tornItems'))
      const specialPrizes = prizes
        .filter(prize => prize.category !== 'tornItems')
        .map(prize => ({ ...prize, quantity: prizes.filter(p => p.item_id === prize.item_id).length }))
      const specialPrizesList = this.getPrizesList(uniqBy(specialPrizes, 'item_id'))

      content = (
        <div className='prizes-block'>
          <div>{specialPrizesList}</div>
          <div className='tornItems-prizes'>{tornItemsPrizesList}</div>
          <div className='congratulations'>{message}</div>
        </div>
      )
    }

    return content
  }

  render() {
    const { cellEvent, quitCellEvent, openChest } = this.props
    const imageClassName = cn(`chests-${cellEvent.chest.type}`, {
      opened: cellEvent.chestOpeningStart || cellEvent.chestOpened
    })

    return (
      <div className={cn('chest-dialog', `chest-${cellEvent.chest.type}-dialog`)}>
        <i className={imageClassName} />
        {this.getContent()}
        {!cellEvent.chestOpened && (
          <button type='button' className={`btn btn-blue ${!cellEvent.canOpen ? 'locked' : ''}`} onClick={openChest}>
            OPEN
          </button>
        )}
        <button type='button' className='btn btn-blue' onClick={quitCellEvent}>
          LEAVE
        </button>
      </div>
    )
  }
}

export default ChestDialog
