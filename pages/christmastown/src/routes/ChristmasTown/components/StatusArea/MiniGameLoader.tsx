import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import rootStore from '../../../../store/createStore'
import { injectReducer } from '../../../../store/helpers/rootReducer'
import { gameLoaded } from '../../modules'

let MiniGameModule = React.Fragment

export const MiniGameLoader = (props: { miniGameName: string }) => {
  const dispatch = useDispatch()
  const [, setIsGameLoaded] = useState(false)

  useEffect(() => {
    import(`../minigames/${props.miniGameName}/index`)
      .then(module => {
        module.reducer && injectReducer(rootStore, { reducer: module.reducer, key: props.miniGameName })
        MiniGameModule = module.default
      })
      .then(() => setIsGameLoaded(true))
      .then(() => dispatch(gameLoaded(props.miniGameName)))
      .catch(e => {
        console.error(`Couldn't load minigame: ${props.miniGameName}`)
        console.error(e)
      })

    return () => {
      MiniGameModule = React.Fragment
    }
  }, [dispatch, props.miniGameName])

  return <MiniGameModule />
}
