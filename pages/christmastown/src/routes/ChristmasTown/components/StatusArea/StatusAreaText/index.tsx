import React, { Component } from 'react'
import cn from 'classnames'
import NPCWithPrizes from '../NPCWithPrizes'
import { HOSPITAL, JAIL } from '../../../constants'
import s from './styles.cssmodule.scss'
import DebugModeClarityIcon from '../../../../../components/DebugModeClarityIcon/DebugModeClarityIcon'

interface IProps {
  statusArea: any
}

const xmasNpc = ['santa', 'grinch', 'penguin']

class StatusAreaText extends Component<IProps> {
  getTextWithLineBreaks = text => {
    const splitText = text.split('\n')

    return splitText.map((row, i) => {
      return <p className='paragraph' key={i} dangerouslySetInnerHTML={{ __html: row }} />
    })
  }

  getFindItemMsg = (text, itemImage, itemIsReceived) => {
    return (
      <div className={s.itemImageContainer}>
        <div className={s.title}>{text}</div>
        <div className={s.itemImage}>
          <img src={itemImage.url} />
          {!itemIsReceived && <DebugModeClarityIcon />}
        </div>
      </div>
    )
  }

  getStatusAreaContainerStyles = () => {
    const {
      statusArea: { triggerType, messageAreaImageUrl, itemImage }
    } = this.props

    return messageAreaImageUrl && !itemImage ?
      {
        color: this._getTextColor(triggerType),
        backgroundImage: `url(${messageAreaImageUrl})`
      } :
      {
        color: this._getTextColor(triggerType)
      }
  }

  _getTextColor = triggerType => {
    let textColor = '#668FA3'

    if (triggerType && (triggerType.toLowerCase() === JAIL || triggerType.toLowerCase() === HOSPITAL)) {
      textColor = '#C36A6A'
    }

    return textColor
  }

  _getStatusAreaStyles = cellEvent => {
    const npcType = cellEvent && cellEvent.npcType

    return cn(npcType ? s.npcMessageWrapper : 'text-wrapper', {
      [s.santaMessageWrapper]: cellEvent && cellEvent.prizes && cellEvent.npcType === 'santa',
      [s.withPrizes]: cellEvent && cellEvent.prizes && cellEvent.prizes.length
    })
  }

  render() {
    const { statusArea } = this.props
    const { text, triggerLink, cellEvent, itemImage, messageAreaImageUrl, itemIsReceived } = statusArea
    const npcType = cellEvent && cellEvent.npcType
    const isNpcWithSnowGlobe = (cellEvent && cellEvent.type === 'snowGlobe') || false
    const withPrizes = (cellEvent && cellEvent.prizes && cellEvent.prizes.length) || false
    const isXmasNpc = xmasNpc.includes(npcType) || false
    const statusAreaContainerClasses = cn({
      'text-container': true,
      [s.statusAreaItem]: itemImage,
      [s[npcType]]: npcType,
      [s.withSnowGlobe]: isNpcWithSnowGlobe,
      [s.prizesEmpty]: !withPrizes,
      [s.activeImageFilter]: messageAreaImageUrl && !npcType && !itemImage,
      [s.customImage]: messageAreaImageUrl && !itemImage
    })

    return (
      <div className={statusAreaContainerClasses} style={this.getStatusAreaContainerStyles()}>
        <div className={this._getStatusAreaStyles(cellEvent)}>
          {cellEvent && cellEvent.prizes && <NPCWithPrizes withSnowGlobe={isNpcWithSnowGlobe} isXmasNpc={isXmasNpc} />}
          {itemImage ? this.getFindItemMsg(text, itemImage, itemIsReceived) : this.getTextWithLineBreaks(text)}
          {triggerLink && (
            <div className='m-top10'>
              <a href={`/${triggerLink}`} className='btn btn-blue'>
                Continue
              </a>
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default StatusAreaText
