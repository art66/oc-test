import React, { Component } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { openChest, quitCellEvent } from '../../modules'
import { getLoadedMinigameName } from '../../selectors/minigame'
import ChestDialog from './ChestDialog'
import Item from './Item'
import { MiniGameLoader } from './MiniGameLoader'
import StatusAreaText from './StatusAreaText'

type TOwnProps = {
  showTooltip: (e) => void
  hideTooltip: (e) => void
}

type TProps = TReduxProps & TOwnProps

export class StatusArea extends Component<TProps> {
  renderContent() {
    const { statusArea, couponsAmountForDay } = this.props
    const { cellEvent, item } = statusArea

    if (this.props.gameName) {
      return <MiniGameLoader miniGameName={this.props.gameName} />
    } else if (cellEvent && cellEvent.type === 'chests') {
      return (
        <ChestDialog
          cellEvent={cellEvent}
          quitCellEvent={this.props.quitCellEvent}
          openChest={this.props.openChest}
          showTooltip={this.props.showTooltip}
          hideTooltip={this.props.hideTooltip}
        />
      )
    } else if (item) {
      return <Item item={item} couponsAmountForDay={couponsAmountForDay} />
    }
    return <StatusAreaText statusArea={statusArea} />
  }

  render() {
    return <div className='status-area-container'>{this.renderContent()}</div>
  }
}

const mapStateToProps = state => ({
  statusArea: state.christmastown.statusArea,
  couponsAmountForDay: state.christmastown.mapData.couponsAmountForDay,
  gameName: getLoadedMinigameName(state)
})

const mapActionsToProps = {
  quitCellEvent,
  openChest
}

const connector = connect(mapStateToProps, mapActionsToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connect(mapStateToProps, mapActionsToProps)(StatusArea)
