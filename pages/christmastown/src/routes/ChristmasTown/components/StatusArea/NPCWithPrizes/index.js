import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setGameTitle } from '../../../modules'
import PrizesList from '../PrizesList'
import s from './styles.cssmodule.scss'

class NPCWithPrizes extends Component {
  render() {
    const { items, withSnowGlobe, isXmasNpc } = this.props

    return (
      <div className={s.prizesWrap}>
        <PrizesList prizes={items} withSnowGlobe={withSnowGlobe} isXmasNpc={isXmasNpc} />
      </div>
    )
  }
}

NPCWithPrizes.propTypes = {
  items: PropTypes.array,
  message: PropTypes.string,
  setGameTitle: PropTypes.func,
  prizesClass: PropTypes.string,
  withSnowGlobe: PropTypes.bool,
  isXmasNpc: PropTypes.bool,
}

const mapStateToProps = state => ({
  items: state.christmastown.statusArea.cellEvent.prizes || []
})

const mapActionsToProps = {
  setGameTitle
}

export default connect(mapStateToProps, mapActionsToProps)(NPCWithPrizes)
