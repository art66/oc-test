// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'couponItem': string;
  'description': string;
  'globalSvgShadow': string;
  'imageWrap': string;
  'item': string;
  'title': string;
}
export const cssExports: CssExports;
export default cssExports;
