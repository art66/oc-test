/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { CT_ITEMS_URL, ITEMS_IMG_EXTENSION } from '../../../constants'
import { getItemType } from '../../../utils'
import s from './styles.cssmodule.scss'
import CouponsForDay from '../../../../../components/CouponsForDays'
import { COUPONS_MAX_AMOUNTS_FOR_DAY } from '../../minigames/CouponExchange/constants'

const CHESTS = {
  91: 'Bronze',
  92: 'Silver',
  93: 'Golden'
}

const ITEM_MODIFIER_TYPE = {
  itemSpawn: 'item spawn rate',
  speed: 'movement speed'
}

class Item extends Component {
  getDescription = () => {
    const { category, type, modifier, modifierType } = this.props.item
    let desc

    switch (category) {
      case 'ornaments':
        desc = `Having this ornament increases your ${ITEM_MODIFIER_TYPE[modifierType]} by ${modifier}%`
        break
      case 'bucks':
        desc = 'These bucks can be spent in gift shops'
        break
      case 'keys':
        desc = `This key can be used to open ${CHESTS[type]} Chests`
        break
      case 'coupon':
        desc = 'These Coupons can be exchanged for items at a Coupon Exchange Tent'
        break
      default:
        break
    }

    return desc
  }

  render() {
    const { name, category, type, quantity } = this.props.item
    const description = this.getDescription()
    const itemType = getItemType(category, type, quantity)
    const isCoupon = category === 'coupon'

    return (
      <div className={cn(s.item, { [s.couponItem]: isCoupon })}>
        <div className={s.title}>{name}</div>
        <div className={s.imageWrap}>
          <img src={`${CT_ITEMS_URL}/${category}/${category}-${itemType}-preview${ITEMS_IMG_EXTENSION}`} alt={name} />
        </div>
        <div className={s.description}>{description}</div>
        {isCoupon && (
          <CouponsForDay currentCoupons={this.props.couponsAmountForDay} maxCoupons={COUPONS_MAX_AMOUNTS_FOR_DAY} />
        )}
      </div>
    )
  }
}

Item.propTypes = {
  item: PropTypes.string,
  couponsAmountForDay: PropTypes.number
}

export default Item
