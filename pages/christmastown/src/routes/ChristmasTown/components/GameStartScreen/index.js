import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { acceptGame, leaveGame, move } from '../../modules'
import CouponsForDay from '../../../../components/CouponsForDays'
import { COUPONS_MAX_AMOUNTS_FOR_DAY } from '../minigames/CouponExchange/constants'
import s from './styles.cssmodule.scss'

export class GameStartScreen extends Component {
  handleCancellingGame = () => {
    const { leaveGame, move } = this.props

    leaveGame()
    move('bottom')
  }

  renderPrizeCouponForDay = () => {
    const { isPrizeCouponAvailable, couponsAmountForDay } = this.props
    return isPrizeCouponAvailable && (
      <div className={s.couponsForDay}>
        <CouponsForDay currentCoupons={couponsAmountForDay} maxCoupons={COUPONS_MAX_AMOUNTS_FOR_DAY} />
      </div>
    )
  }


  renderPrizeCouponMessage = () => {
    const { isPrizeCouponAvailable, isPrizeCouponReceivedToday, couponsAmountForDay } = this.props

    if (isPrizeCouponAvailable && couponsAmountForDay === COUPONS_MAX_AMOUNTS_FOR_DAY) {
      return (
        <span className={s.couponGameMessage}>
          You have already won {COUPONS_MAX_AMOUNTS_FOR_DAY} Prize Coupons today
        </span>
      )
    }


    return isPrizeCouponAvailable && (
      <span className={s.couponGameMessage}>
        {isPrizeCouponReceivedToday ?
          'You have already won your Prize Coupon on this game today' :
          'Win to claim your Prize Coupon!'
        }
      </span>
    )
  }

  render() {
    const { acceptGame, message, yesTitle, noTitle, rules } = this.props
    // box single values in arrays
    const yesTitles = typeof yesTitle === 'string' ? [yesTitle] : yesTitle
    const noTitles = typeof noTitle === 'string' ? [noTitle] : noTitle

    return (
      <div className={s['game-start-screen']}>
        <span className={s.gameMessage}>{message}</span>
        <div>
          {yesTitles.map((title, i) => (
            <button type='button' key={i} className='btn-blue big' onClick={acceptGame}>
              {title}
            </button>
          ))}
          {noTitles.map((title, i) => (
            <button type='button' key={i} className='btn-blue big' onClick={this.handleCancellingGame}>
              {title}
            </button>
          ))}
        </div>
        {this.renderPrizeCouponMessage()}
        {rules ? <div className={s['rules']}>{rules}</div> : ''}
        {this.renderPrizeCouponForDay()}
      </div>
    )
  }
}

GameStartScreen.propTypes = {
  acceptGame: PropTypes.func,
  leaveGame: PropTypes.func,
  move: PropTypes.func,
  message: PropTypes.string,
  rules: PropTypes.string,
  yesTitle: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.string), PropTypes.string]),
  noTitle: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.string), PropTypes.string]),
  couponsAmountForDay: PropTypes.number,
  isPrizeCouponAvailable: PropTypes.bool,
  isPrizeCouponReceivedToday: PropTypes.bool
}

const mapStateToProps = state => ({
  couponsAmountForDay: state.christmastown.mapData?.couponsAmountForDay,
  isPrizeCouponAvailable: state.christmastown.mapData?.trigger?.isPrizeCouponAvailable,
  isPrizeCouponReceivedToday: state.christmastown.mapData?.trigger?.isPrizeCouponReceivedToday
})

const mapActionsToProps = {
  acceptGame,
  leaveGame,
  move
}

export default connect(mapStateToProps, mapActionsToProps)(GameStartScreen)
