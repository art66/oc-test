import React from 'react'
import GameStartScreen from '.'

const store = { getState: () => {} }

export const Buttons = () => <GameStartScreen store={store} yesTitle={'yes'} noTitle={'No'} />

export const MultipleButtons = () => (
  <GameStartScreen store={store} yesTitle={['Maybe', 'Somewhat']} noTitle={['No', 'Let me alone']} />
)

export default {
  title: 'Christmastown/GameStartScreen'
}
