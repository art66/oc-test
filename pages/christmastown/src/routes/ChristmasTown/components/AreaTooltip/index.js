import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

class AreaTooltip extends Component {
  render() {
    const {
      currentArea,
      areaTooltip: { text }
    } = this.props

    return (
      <div className={cn(s.areaTooltip, { [s.visible]: currentArea, [s.hidden]: !currentArea })}>
        <div className={s.innerBlock}>{text}</div>
      </div>
    )
  }
}

AreaTooltip.propTypes = {
  areaTooltip: PropTypes.object,
  currentArea: PropTypes.string
}

const mapStateToProps = state => ({
  currentArea: state.christmastown.mapData.currentArea,
  areaTooltip: state.christmastown.mapData.areaTooltip
})

export default connect(mapStateToProps)(AreaTooltip)
