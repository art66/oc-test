import React, { Component, RefObject } from 'react'
import { Swiper, Slide } from 'react-dynamic-swiper'
import cn from 'classnames'
import { getItemType } from '../../utils'

import { INVENTORY_SLIDER_CHUNK_SIZE, CT_ITEMS_URL, ITEMS_IMG_EXTENSION } from '../../constants'
import IInventoryItem from '../../interfaces/IInventoryItem'

import 'react-dynamic-swiper/lib/styles.css'
import styles from './index.cssmodule.scss'

interface IProps {
  inventory: IInventoryItem[]
  setStatusAreaItemRun: (item: IInventoryItem) => void
  devTogglerRun: () => void
  selectedItemId: number
  showAdminPanel: boolean
  isAdmin: boolean
  newInventoryItems: IInventoryItem[]
}

interface IState {
  animationFinished: boolean
}

class UserInventory extends Component<IProps, IState> {
  quantities: number
  inventorySwiper: RefObject<Swiper>

  static defaultProps: IProps = {
    inventory: [],
    newInventoryItems: [],
    setStatusAreaItemRun: () => {},
    devTogglerRun: () => {},
    selectedItemId: null,
    showAdminPanel: false,
    isAdmin: false
  }

  componentDidMount() {
    this.inventorySwiper = React.createRef()
  }

  shouldComponentUpdate(newProps) {
    const { inventory, selectedItemId, showAdminPanel } = this.props
    const quantities = newProps.inventory.reduce((sum, item) => sum + (item.quantity || 0), '')

    const needUpdate =
      this.quantities !== quantities
      || selectedItemId !== newProps.selectedItemId
      || inventory.length !== newProps.inventory.length
      || showAdminPanel !== newProps.showAdminPanel

    this.quantities = quantities

    return needUpdate
  }

  componentDidUpdate(prevProps: Readonly<IProps>) {
    this.props.newInventoryItems.length !== 0
      && prevProps.newInventoryItems.length !== this.props.newInventoryItems.length
      && this.switchToReceivingItem(this.props.inventory)
  }

  getUrlInventoryImage = (instance) => {
    const type = getItemType(instance.category, instance.type, instance.quantity)
    const fileName = `${[instance.category, type, instance.color]
      .filter(Boolean)
      .join('-')}${ITEMS_IMG_EXTENSION}`

    return `${CT_ITEMS_URL}${instance.category}/${fileName}`
  }

  getSlider = () => {
    const { selectedItemId, inventory } = this.props
    const inventoryList = this.buildInventoryList(inventory)
    const slides = []

    inventoryList.forEach((chunk, index) => {
      slides.push(
        <Slide key={index}>
          <ul className='items-list clearfix'>
            {Object.keys(chunk).map((key) => {
              const item = chunk[key] || {}
              const isSelected = selectedItemId === item.item_id && item.item_id
              const clickItem = (e) => {
                this.handleClick(e, chunk[key])
              }

              return (
                <li
                  onKeyPress={undefined}
                  key={key}
                  onClick={clickItem}
                  className={cn(chunk[key] && chunk[key].category, { selected: isSelected })}
                >
                  {this.getImage(Object.keys(chunk[key]).length, chunk[key])}
                  <span className={cn('quantity', this.addAnimationClass(chunk[key]))}>
                    {(chunk[key] && chunk[key].quantity) || ''}
                  </span>
                </li>
              )
            })}
          </ul>
        </Slide>
      )
    })

    return slides
  }

  getImage = (length, item) => {
    return length ? (
      <img
        className={cn(styles.inventoryItem, this.addAnimationClass(item))}
        src={this.getUrlInventoryImage(item)}
        alt='Inventory Item'
      />
    ) : (
      ''
    )
  }

  handleClick = (e, item) => {
    const { setStatusAreaItemRun } = this.props

    e.preventDefault()

    if (!item || !item.item_id) {
      return false
    }

    setStatusAreaItemRun(item)

    return true
  }

  addAnimationClass = (item) => (this.props.newInventoryItems.length && item?.animation ? styles.animation : null)

  switchSlide = (newItemsSlides) =>
    newItemsSlides.reduce(
      (slide, currentSlide) =>
        (Math.floor(slide) < Math.floor(currentSlide) ? Math.floor(currentSlide) : Math.floor(slide)),
      0
    )

  switchToReceivingItem = (inventory) => {
    const newItemsSlides = inventory.reduce((array, item, index) => {
      item?.animation && array.push(index / INVENTORY_SLIDER_CHUNK_SIZE)
      return array
    }, [])
    const switchedSlide = this.switchSlide(newItemsSlides)

    this.inventorySwiper.current && this.inventorySwiper.current._swiper.slideTo(switchedSlide)
  }

  buildInventoryList = (inventory) => {
    const userInventory = Object.create(inventory)

    let inventoryList = []

    let rest = INVENTORY_SLIDER_CHUNK_SIZE

    if (userInventory.length) {
      rest = INVENTORY_SLIDER_CHUNK_SIZE - (userInventory.length % INVENTORY_SLIDER_CHUNK_SIZE)
      rest = rest !== INVENTORY_SLIDER_CHUNK_SIZE ? rest : 0
    }

    for (let index = 1; index <= rest; index++) {
      userInventory.push({})
    }

    inventoryList = userInventory.reduce((previous, current, index) => {
      if (index % INVENTORY_SLIDER_CHUNK_SIZE === 0) {
        previous.push({})
      }
      previous[previous.length - 1][index] = current

      return previous
    }, [])

    return inventoryList
  }

  _handleDevToggle = () => {
    const { devTogglerRun } = this.props

    devTogglerRun()
  }

  _devToggleMenu = () => {
    const { showAdminPanel, isAdmin } = this.props

    if (!isAdmin) {
      return null
    }

    return (
      <div
        role='button'
        onKeyDown={undefined}
        tabIndex={0}
        onClick={this._handleDevToggle}
        className={styles.devMenuToggle}
      >
        <input
          id='devCheckbox'
          type='checkbox'
          className={styles.devMenuToggleCheckbox}
          checked={showAdminPanel}
          readOnly={true}
        />
        <label htmlFor='devCheckbox' className={styles.devMenuToggleLabel}>
          DEV
        </label>
      </div>
    )
  }

  render() {
    const sliderOptions = {
      slidesPerView: 1,
      slidesPerGroup: 1,
      speed: 650,
      paginationClickable: true
    }

    return (
      <div className={`items-container ${styles.itemsContainer}`}>
        <Swiper
          swiperOptions={sliderOptions}
          navigation={false}
          pagination={this.getSlider().length > 1}
          ref={this.inventorySwiper}
        >
          {this.getSlider()}
        </Swiper>
        {this._devToggleMenu()}
      </div>
    )
  }
}

export default UserInventory
