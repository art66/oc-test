/* eslint-disable complexity */
/* eslint-disable max-statements */
import cn from 'classnames'
import uniq from 'lodash/uniq'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CELL_SIZE, USER_MAP_VIEWPORT } from '../../constants'
import { startMoving, stopMoving } from '../../modules'

const START_MOVING_TIMEOUT_TIME = 100
const MOVE_CODES = {
  ArrowDown: 40,
  ArrowRight: 39,
  ArrowUp: 38,
  ArrowLeft: 37,
  w: 87,
  a: 65,
  s: 83,
  d: 68
}

interface IProps {
  userPosition: any
  moving: boolean
  startMoving?: (direction: string) => void
  stopMoving?: () => void
}

interface IState {
  keysPressed: any
  directions?: {
    leftHovered?: boolean
    leftTopHovered?: boolean
    topHovered?: boolean
    rightTopHovered?: boolean
    rightHovered?: boolean
    rightBottomHovered?: boolean
    bottomHovered?: boolean
    leftBottomHovered?: boolean
  }
}

class Directions extends Component<IProps, IState> {
  startMovingTimeout: any
  ctWrapper: any
  mapWrapper: any
  movingDirection: string

  state = {
    keysPressed: [],
    directions: {
      leftHovered: false,
      leftTopHovered: false,
      topHovered: false,
      rightTopHovered: false,
      rightHovered: false,
      rightBottomHovered: false,
      bottomHovered: false,
      leftBottomHovered: false
    }
  }

  componentDidMount() {
    this.movingDirection = ''
    this.subscribeForKeyPresses()
  }

  componentWillUnmount() {
    this.unsubscribeFromKeyPresses()
  }

  getMainWrapper = () => {
    if (!this.ctWrapper) {
      this.ctWrapper = document.getElementById('ct-wrap')
    }

    return this.ctWrapper
  }

  getMapWrapper = () => {
    if (!this.mapWrapper) {
      this.mapWrapper = document.getElementById('user-map')
    }

    return this.mapWrapper
  }

  runStartMovingTimeout = () => {
    if (this.startMovingTimeout) {
      return
    }

    this.startMovingTimeout = setTimeout(() => {
      this.props.startMoving(this.movingDirection)
    }, START_MOVING_TIMEOUT_TIME)
  }

  stopStartMovingTimeout = () => {
    clearTimeout(this.startMovingTimeout)
    this.startMovingTimeout = null
  }

  keyPressed = keyCode => {
    return this.state.keysPressed.includes(keyCode)
  }

  keysPressed = (keyCodes = []) => {
    return keyCodes.length > 0 && this.state.keysPressed.filter(k => keyCodes.includes(k)).length >= keyCodes.length
  }

  moveOnKeyPresses = event => {
    const codes = MOVE_CODES

    const keys = this.state.keysPressed.filter(keyCode => Object.values(MOVE_CODES).includes(keyCode))

    if (keys.length) {
      event.preventDefault()
    }

    if (keys.length === 1) {
      if (this.keyPressed(codes.ArrowUp) || this.keyPressed(codes.w)) {
        this.movingDirection = 'top'
      } else if (this.keyPressed(codes.ArrowDown) || this.keyPressed(codes.s)) {
        this.movingDirection = 'bottom'
      } else if (this.keyPressed(codes.ArrowLeft) || this.keyPressed(codes.a)) {
        this.movingDirection = 'left'
      } else if (this.keyPressed(codes.ArrowRight) || this.keyPressed(codes.d)) {
        this.movingDirection = 'right'
      }

      return this.runStartMovingTimeout()
    }

    if (this.keysPressed([codes.ArrowUp, codes.ArrowLeft]) || this.keysPressed([codes.w, codes.a])) {
      this.movingDirection = 'leftTop'
    } else if (this.keysPressed([codes.ArrowUp, codes.ArrowRight]) || this.keysPressed([codes.w, codes.d])) {
      this.movingDirection = 'rightTop'
    } else if (this.keysPressed([codes.ArrowDown, codes.ArrowLeft]) || this.keysPressed([codes.s, codes.a])) {
      this.movingDirection = 'leftBottom'
    } else if (this.keysPressed([codes.ArrowDown, codes.ArrowRight]) || this.keysPressed([codes.s, codes.d])) {
      this.movingDirection = 'rightBottom'
    }

    return this.runStartMovingTimeout()
  }

  handleKeyDown = e => {
    if (document.activeElement.className !== 'user-map') return

    const { length } = this.state.keysPressed

    this.setState(prevState => ({
      keysPressed: uniq(prevState.keysPressed.concat([e.keyCode]))
    }))

    const keys = this.state.keysPressed.filter(keyCode => Object.values(MOVE_CODES).includes(keyCode))

    if (length !== keys.length && keys.length > 0) {
      this.stopStartMovingTimeout()
    }
    if (Object.values(MOVE_CODES).includes(e.keyCode)) {
      this.moveOnKeyPresses(e)
    }
  }

  handleKeyUp = e => {
    if (document.activeElement.className !== 'user-map') return

    const { length } = this.state.keysPressed

    this.setState(prevState => ({
      keysPressed: prevState.keysPressed.filter(key => key !== e.keyCode)
    }))

    const keys = this.state.keysPressed.filter(keyCode => Object.values(MOVE_CODES).includes(keyCode))

    if (keys.length === 0) {
      this.stopStartMovingTimeout()
      this.props.stopMoving()
    }

    if (length !== keys.length && keys.length > 0) {
      this.stopStartMovingTimeout()
      this.moveOnKeyPresses(e)
    }
  }

  subscribeForKeyPresses = () => {
    const ctWrapper = this.getMainWrapper()
    const mapElement = this.getMapWrapper()

    mapElement.addEventListener('blur', this._handleMapBlur)
    ctWrapper.addEventListener('keydown', this.handleKeyDown)
    ctWrapper.addEventListener('keyup', this.handleKeyUp)
    ctWrapper.focus()
  }

  unsubscribeFromKeyPresses = () => {
    this.ctWrapper.removeEventListener('keydown', this.handleKeyDown)
    this.ctWrapper.removeEventListener('keyup', this.handleKeyUp)
    this.mapWrapper.removeEventListener('blur', this.handleKeyUp)
  }

  _startMoving = (e: React.MouseEvent<HTMLElement> | string) => {
    const direction = this._getDirection(e)

    if (direction) {
      this.props.startMoving(direction)
    }
  }

  _stopMoving = () => {
    this.props.stopMoving()
  }

  // it's a bridge - we can receive both native dataset and/or the custom user payload
  _getDirection = (
    e: any // legacy
  ) => (e && e.target && e.target.dataset && e.target.dataset.direction) || e

  _getDirections = () => {
    const { directions } = this.state
    const directionsLocal = {} as IState['directions']

    Object.keys(directions).forEach(name => {
      directionsLocal[name] = false
    })

    return directionsLocal
  }

  _handleMapBlur = () => {
    this.stopStartMovingTimeout()
    this.setState({ keysPressed: [] })
    this.props.stopMoving()
  }

  _handleContextMenu = (e: React.MouseEvent<HTMLElement>) => e.preventDefault()

  _showDirection = (e: React.MouseEvent<HTMLElement> | string) => {
    const direction = this._getDirection(e)
    const directions = this._getDirections()

    switch (direction) {
      case 'left':
        directions.leftHovered = true
        break
      case 'leftTop':
        directions.leftTopHovered = true
        break
      case 'top':
        directions.topHovered = true
        break
      case 'rightTop':
        directions.rightTopHovered = true
        break
      case 'right':
        directions.rightHovered = true
        break
      case 'rightBottom':
        directions.rightBottomHovered = true
        break
      case 'bottom':
        directions.bottomHovered = true
        break
      case 'leftBottom':
        directions.leftBottomHovered = true
        break
      default:
        break
    }

    this.setState({ directions })
  }

  _hideDirections = () => {
    const directions = this._getDirections()

    this.setState({ directions })
  }

  render() {
    const { directions } = this.state
    const { moving, userPosition } = this.props
    const mapControlsStyle = {
      left: Math.round(userPosition.x - (USER_MAP_VIEWPORT - 1) / 2) * CELL_SIZE,
      top: Math.round(userPosition.y + (USER_MAP_VIEWPORT - 2) / 2) * -CELL_SIZE
    }

    return (
      <div>
        <ul className={`map-controls ${moving ? 'show' : ''}`} style={mapControlsStyle}>
          <li
            className={cn({ 'show-tip': directions.leftHovered }, 'left')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('left')}
            onTouchStart={() => this._startMoving('left')}
            onMouseEnter={() => this._showDirection('left')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
          <li
            className={cn({ 'show-tip': directions.leftTopHovered }, 'left-top')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('leftTop')}
            onTouchStart={() => this._startMoving('leftTop')}
            onMouseEnter={() => this._showDirection('leftTop')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
          <li
            className={cn({ 'show-tip': directions.topHovered }, 'top')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('top')}
            onTouchStart={() => this._startMoving('top')}
            onMouseEnter={() => this._showDirection('top')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
          <li
            className={cn({ 'show-tip': directions.rightTopHovered }, 'right-top')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('rightTop')}
            onTouchStart={() => this._startMoving('rightTop')}
            onMouseEnter={() => this._showDirection('rightTop')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
          <li
            className={cn({ 'show-tip': directions.rightHovered }, 'right')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('right')}
            onTouchStart={() => this._startMoving('right')}
            onMouseEnter={() => this._showDirection('right')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
          <li
            className={cn({ 'show-tip': directions.rightBottomHovered }, 'right-bottom')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('rightBottom')}
            onTouchStart={() => this._startMoving('rightBottom')}
            onMouseEnter={() => this._showDirection('rightBottom')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
          <li
            className={cn({ 'show-tip': directions.bottomHovered }, 'bottom')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('bottom')}
            onTouchStart={() => this._startMoving('bottom')}
            onMouseEnter={() => this._showDirection('bottom')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
          <li
            className={cn({ 'show-tip': directions.leftBottomHovered }, 'left-bottom')}
            onMouseUp={this._stopMoving}
            onTouchEnd={this._stopMoving}
            onMouseDown={() => this._startMoving('leftBottom')}
            onTouchStart={() => this._startMoving('leftBottom')}
            onMouseEnter={() => this._showDirection('leftBottom')}
            onMouseLeave={() => this._hideDirections()}
            onContextMenu={e => e.preventDefault()}
          >
            <i />
          </li>
        </ul>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userPosition: state.christmastown.mapData.user.position,
  moving: state.christmastown.mapData.movingTimeOut
})

const mapActionsToProps = dispatch => ({
  startMoving: (direction: string) => dispatch(startMoving(direction)),
  stopMoving: () => dispatch(stopMoving())
})

export default connect(mapStateToProps, mapActionsToProps)(Directions)
