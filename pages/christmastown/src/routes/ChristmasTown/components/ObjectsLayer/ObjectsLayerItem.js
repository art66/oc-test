import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { CELL_SIZE } from '../../constants'
import Gate from './Gate'
import s from './animations.cssmodule.scss'

const GATE_CATEGORY = 'Gates'
const GATES_TYPES = [1, 2, 3, 4, 93]

class ObjectsLayerItem extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { object: { hash } } = this.props

    return hash !== nextProps.object.hash
  }

  render() {
    const { object } = this.props
    const { width, height, category, type, position, onTopOfUser, animation } = object
    const animationName = animation ? animation.name : ''
    const className = cn(
      'ct-object',
      { 'top-level': onTopOfUser },
      { [s.animation]: animation },
      { [s[animationName]]: animation }
    )

    return (
      <div
        className={className}
        style={{
          width,
          height,
          left: position.x * CELL_SIZE,
          top: position.y * -CELL_SIZE
        }}
      >
        {category === GATE_CATEGORY && GATES_TYPES.includes(type) ?
          <Gate object={object} /> :
          <img
            src={`/images/v2/christmas_town/library/${category}/${type}.png`}
            alt=''
            draggable={false}
          />}
      </div>
    )
  }
}

ObjectsLayerItem.propTypes = {
  object: PropTypes.object.isRequired
}

export default ObjectsLayerItem
