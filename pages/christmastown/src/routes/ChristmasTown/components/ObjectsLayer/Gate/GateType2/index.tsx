import React from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

const GateType2 = ({ opened }) => {
  return (
    <div>
      <img src='/images/v2/christmas_town/gates/gate2/1.png' />
      <div className={cn(s.animatedPart, { [s.opened]: opened })}>
        <div className={s.movablePartLeft} />
        <div className={s.movablePartRight} />
      </div>
    </div>
  )
}

export default GateType2
