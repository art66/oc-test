import React from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

const GateType1 = ({ opened }) => {
  return (
    <div>
      <img src='/images/v2/christmas_town/gates/gate1/1.png' />
      <div className={cn(s.animatedPart, { [s.opened]: opened })}>
        <div className={s.movablePart} />
      </div>
    </div>
  )
}

export default GateType1
