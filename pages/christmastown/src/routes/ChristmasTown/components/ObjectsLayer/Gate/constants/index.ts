import GateType1 from '../GateType1'
import GateType2 from '../GateType2'
import GateType3 from '../GateType3'
import GateType4 from '../GateType4'
import GateType93 from '../GateType93'

const STATUS_CLOSED = 'closed'
const STATUS_OPENED = 'opened'

const gateComponents = {
  1: GateType1,
  2: GateType2,
  3: GateType3,
  4: GateType4,
  93: GateType93
}

export { STATUS_CLOSED, STATUS_OPENED, gateComponents }
