import React from 'react'
import * as redux from 'react-redux'
import { updateGateControllers } from '../../../../actions'
import * as mapDataSelectors from '../../../../selectors/mapData'
import Gate from '../index'
import { renderWithWrappers } from '../../../../../../../../../utils/test-utils'
import { STATUS_CLOSED } from '../constants'

describe('Gate', () => {
  const mockDispatchFn = jest.fn()
  const openedGateControllerSpy = jest.spyOn(mapDataSelectors, 'getOpenedGateControllerForCurrentGate')

  jest.spyOn(redux, 'useDispatch').mockReturnValue(mockDispatchFn)

  beforeEach(jest.useFakeTimers)

  afterAll(() => {
    jest.runOnlyPendingTimers()
    jest.useRealTimers()
    jest.resetAllMocks()
  })

  it('should render Gate 93 and not throw', () => {
    openedGateControllerSpy.mockReturnValue([] as any)
    const gate: any = { type: 93 }

    expect(() => renderWithWrappers(<Gate object={gate} />)).not.toThrow()
  })

  it('should render Gate 93 with opened gate and not throw', () => {
    const openedGateController = { position: { x: 68, y: -10 }, status: 'opened', time: 15, radius: '9' }
    const gate: any = { type: 93 }

    openedGateControllerSpy.mockReturnValue(openedGateController)

    expect(() => renderWithWrappers(<Gate object={gate} />)).not.toThrow()
  })

  it('should render Gate 93 with opened gate, dispatch updateGateControllers action', () => {
    const openedGateController = { position: { x: 68, y: -10 }, status: 'opened', time: 0, radius: '9' }
    const gate: any = { type: 93 }

    openedGateControllerSpy.mockReturnValue(openedGateController)
    renderWithWrappers(<Gate object={gate} />)

    jest.advanceTimersByTime(1000)

    expect(mockDispatchFn).toHaveBeenCalledWith(updateGateControllers([openedGateController], 0, STATUS_CLOSED))
  })
})
