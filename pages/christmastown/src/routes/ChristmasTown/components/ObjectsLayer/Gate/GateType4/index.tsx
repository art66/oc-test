import React from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

const GateType4 = ({ opened }) => {
  return (
    <div>
      <div className={cn(s.animatedPart, { [s.opened]: opened })}>
        <div className={s.movablePart} />
      </div>
    </div>
  )
}

export default GateType4
