import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { getOpenedGateControllerForCurrentGate } from '../../../selectors/mapData'
import IObject from '../../../interfaces/IObject'
import { STATUS_CLOSED, gateComponents } from './constants'
import { updateGateControllers } from '../../../actions'
import { useParamSelector } from '../../../hooks'

const Gate = ({ object }: { object: IObject }) => {
  const dispatch = useDispatch()
  const GateComponent = gateComponents[object.type]
  const openedGateController = useParamSelector(getOpenedGateControllerForCurrentGate, object)

  useEffect(() => {
    if (openedGateController) {
      let { time } = openedGateController
      const timeout = setInterval(() => {
        if (time === 0) {
          dispatch(updateGateControllers([openedGateController], time, STATUS_CLOSED))
        } else {
          time -= 1
        }
      }, 1000)

      return () => clearTimeout(timeout)
    }
  }, [openedGateController, dispatch])

  return <GateComponent opened={Boolean(openedGateController)} />
}

export default Gate
