import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import orderBy from 'lodash.orderby'
import ObjectsLayerItem from './ObjectsLayerItem'

export class ObjectsLayer extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.moveInProgress) {
      return false
    }

    const hash = nextProps.objects.reduce((hash, item) => hash + item.hash, '')
    const needUpdate = this.hash !== hash

    this.hash = hash

    return needUpdate
  }

  _getObjectsList = () => orderBy(this.props.objects, ['order'], ['asc']).map(object => (
    <ObjectsLayerItem key={object.hash} object={object} />
  ))

  render() {
    return (
      <div className='objects-layer'>
        {this._getObjectsList()}
      </div>
    )
  }
}

ObjectsLayer.propTypes = {
  objects: PropTypes.array.isRequired,
  moveInProgress: PropTypes.bool
}

const mapStateToProps = state => ({
  objects: state.christmastown.mapData.objects,
  moveInProgress: state.christmastown.mapData.moveInProgress
})

export default connect(mapStateToProps)(ObjectsLayer)

