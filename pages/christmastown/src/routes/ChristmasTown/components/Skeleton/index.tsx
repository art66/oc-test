import React from 'react'

import styles from './index.cssmodule.scss'

class Skeleton extends React.PureComponent {
  render() {
    return (
      <div className={styles.skeletonWrap}>
        <div className={styles.ctContainer}>
          <div className={styles.skeletonHeader} />
          <div className={styles.skeletonBody} />
        </div>
        <hr className='delimiter-999' />
        <div className={styles.lobbyContainer}>
          <div className={styles.skeletonHeader} />
          <div className={styles.lobbyBody} />
        </div>
      </div>
    )
  }
}

export default Skeleton
