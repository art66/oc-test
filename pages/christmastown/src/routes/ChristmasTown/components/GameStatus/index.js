import isValue from '@torn/shared/utils/isValue'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { formatSeconds } from '../../utils'
import s from './styles.cssmodule.scss'

export class GameStatus extends Component {
  constructor(props) {
    super(props)
    this.getLevelBlock = this.getLevelBlock.bind(this)
    this.getTimerBlock = this.getTimerBlock.bind(this)
  }

  getLevelBlock(level) {
    const { maxLevel } = this.props.gameState

    return (
      <h2>
        Level: {level}/{maxLevel || 5}
      </h2>
    )
  }

  getTimerBlock(time) {
    return (
      <h2 className={s['timer']}>
        <i className={s['timer-icon']} />
        {formatSeconds(time)}
      </h2>
    )
  }

  getPointsBlock(points) {
    return (
      <h2 className={s['points']}>
        <i className={s['points-icon']} />
        {points}
      </h2>
    )
  }

  getLivesBlock(lives) {
    const live1 = lives >= 1 ? s['live'] : s['dead']
    const live2 = lives >= 2 ? s['live'] : s['dead']
    const live3 = lives === 3 ? s['live'] : s['dead']

    return (
      <ul className={s['lives']}>
        <li className={live1} />
        <li className={live2} />
        <li className={live3} />
      </ul>
    )
  }

  getTokensBlock(tokens) {
    return (
      <span className={s.tokens}>
        You have <span className={s.quantity}>{tokens}</span>Bucks
      </span>
    )
  }

  // eslint-disable-next-line complexity
  getGameBlock(gameState) {
    if (gameState && gameState.points >= 0 && gameState.level && gameState.level.number !== 0) {
      return (
        <div>
          {this.getLevelBlock(gameState.level.number)}
          {this.getPointsBlock(gameState.points)}
          {this.getTimerBlock(gameState.time)}
        </div>
      )
    } else if (gameState && gameState.level && gameState.level.number !== 0) {
      return (
        <div>
          {this.getLevelBlock(gameState.level.number)}
          {this.getTimerBlock(gameState.time)}
        </div>
      )
    } else if (gameState && gameState.gameStarted && gameState.points >= 0 && gameState.lives) {
      return (
        <div>
          {this.getPointsBlock(gameState.points)}
          {this.getLivesBlock(gameState.lives)}
        </div>
      )
    } else if (gameState && gameState.tokens >= 0 && gameState.tokens !== null) {
      return <div>{this.getTokensBlock(gameState.tokens)}</div>
    } else if (gameState && isValue(gameState.time)) {
      return <div>{this.getTimerBlock(gameState.time)}</div>
    }
  }

  render() {
    const { gameState } = this.props

    return <div className={s['game-status-info']}>{this.getGameBlock(gameState)}</div>
  }
}

GameStatus.propTypes = {
  gameName: PropTypes.string,
  gameState: PropTypes.object
}

const mapStateToProps = (state) => ({
  gameName: state.christmastown.minigame.gameName,
  gameState: state[state.christmastown.minigame.gameName]
})

const mapActionsToProps = {}

export default connect(mapStateToProps, mapActionsToProps)(GameStatus)
