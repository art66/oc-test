import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { isInUserViewport } from '../../utils'
import StepsLayerItem from './StepsLayerItem'
import BusyAreaLayerItem from '../BusyAreaLayerItem'
import './style.scss'

export class StepsLayer extends Component {
  state = {
    steps: [],
    busyArea: []
  }

  static getDerivedStateFromProps(nextProps) {
    const { steps, busyArea, position } = nextProps

    return {
      steps: steps.filter(step => isInUserViewport(step.position, position)),
      busyArea: busyArea.filter(area => isInUserViewport(area.position, position))
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { steps } = this.state
    const lastStep = steps[steps.length - 1]
    const lastStepNew = nextState.steps[nextState.steps.length - 1]

    return (lastStep && lastStepNew && lastStep.id !== lastStepNew.id) || false
  }

  _getStepsList = () => {
    const { steps } = this.state

    return steps.map((step, index) => (
      <StepsLayerItem key={step.id} step={step} prevStep={steps[index - 1]} />
    ))
  }

  _getBusyAreaList = () => {
    const { busyArea } = this.state

    return busyArea.map(busyAreaItem => (
      <BusyAreaLayerItem key={`${busyAreaItem.position.x}${busyAreaItem.position.y}`} busyAreaItem={busyAreaItem} />
    ))
  }

  render() {
    return (
      <div className='steps-layer'>
        {this._getStepsList()}
        {this._getBusyAreaList()}
      </div>
    )
  }
}

StepsLayer.propTypes = {
  steps: PropTypes.array.isRequired,
  busyArea: PropTypes.array.isRequired,
  position: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  steps: state.christmastown.mapData.steps,
  busyArea: state.christmastown.mapData.busyArea,
  position: state.christmastown.mapData.user.position
})

export default connect(mapStateToProps)(StepsLayer)
