import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { CELL_SIZE, MAX_STEP_DURATION } from '../../constants'
import { isOpposite } from '../../utils'

export class StepsLayerItem extends Component {
  shouldComponentUpdate() {
    return false
  }

  getStepDirectionClassName = () => {
    const { step, prevStep } = this.props
    let currentDirection = step.direction
    let prevDirection = step.prevDirection || (prevStep ? prevStep.direction : null)
    let stepDirectionClassName = 'footprint footprint-'

    if (prevDirection && !isOpposite(currentDirection, prevDirection)) {
      stepDirectionClassName += prevDirection + '-' + currentDirection
    } else {
      stepDirectionClassName += currentDirection + '-' + currentDirection
    }

    return stepDirectionClassName
  }

  render() {
    const { step } = this.props

    return (
      <div
        className={'ct-step' + (step.newStep ? ' new' : '')}
        style={{
          width: CELL_SIZE,
          height: CELL_SIZE,
          left: step.position.x * CELL_SIZE,
          top: step.position.y * -CELL_SIZE,
          opacity: step.duration / MAX_STEP_DURATION || 0
        }}
      >
        <i className={this.getStepDirectionClassName()} style={{ animationDuration: step.duration + 's' }} />
      </div>
    )
  }
}

StepsLayerItem.propTypes = {
  step: PropTypes.object.isRequired,
  prevStep: PropTypes.object
}

export default StepsLayerItem
