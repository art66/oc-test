# ChristmasTown - Torn


# 2.4.0
  * Added beatified DEV panel layout

# 2.3.0
 * Added lazy-loading for the lobby maps load.

# 2.2.0
 * Added row highlighting for the current joined map in lobby.

# 2.1.0
 * Added final WS actions/reducers for Lobby.

# 2.0.1
 * Addded tooltip for Join button in Lobby.
 * Fixed honor layout in Lobby.
 * Fixed images dimensions in Creator Component of Lobby.

# 2.0.0
 * Added first stable GET/WS version of Lobby.
 * Significantly improved Types and Interfaces for Looby.
 * Created Action/Reducer Store for Lobby.
 * Added rows placeholder for cases with missing data.

# 1.6.2
 * Improved votes tooltip appear/hide logic.

# 1.6.1
 * Improved stars appear in CT Lobby.

# 1.6.0
 * Created first good desktop Layout in Lobby.

# 1.5.0
 * Created Header layout in Lobby.

# 1.4.0
 * Refactored core ChristmasTown component. Move it to TS.

# 1.3.0
 * RatingBox was moved to global reuseable scope.

# 1.2.0
 * Created basic Lobby structure.

# 1.1.1
 * Fixed minigames bundles load.

# 1.1.0
 * Initial commit
