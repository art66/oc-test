/* eslint-disable no-shadow */
/* eslint-disable prefer-destructuring */
/* eslint-disable complexity */
/* eslint-disable no-bitwise */
import { MAIN_URL, CELL_SIZE, HALF_USER_MAP_VIEWPORT } from '../constants'
import { FORBIDDEN_ERROR_CODE, FORBIDDEN_ERROR_TEXT } from '../../../constants'
// require('es6-promise').polyfill()
// require('isomorphic-fetch')
/* global addRFC */

export function getColor(color) {
  return `#${color}`
}

export function fetchUrl(url, data) {
  return fetch(addRFC(MAIN_URL + url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    if (response.status === FORBIDDEN_ERROR_CODE) {
      throw new Error(FORBIDDEN_ERROR_TEXT)
    }

    return response.text().then(text => {
      try {
        const json = JSON.parse(text)

        if (json?.reload) {
          window.location.href = window.location.pathname
          return
        }

        if (json?.message?.type === 'error') {
          return Promise.reject(json)
        }

        return json
      } catch (e) {
        console.log(e)
        throw new Error(`Server responded with: ${text}`)
      }
    })
  })
}

export function convertUserPosToPix(position, mapHeight) {
  const { x, y } = position

  const yInPix = y * CELL_SIZE - mapHeight / 2 + 5 * CELL_SIZE
  const xInPix = x * -CELL_SIZE + 5 * CELL_SIZE

  return { x: xInPix, y: yInPix }
}

export function isOpposite(direction, prevDirection) {
  let opposite = false

  switch (direction) {
    case 'left':
      opposite = prevDirection === 'right'
      break
    case 'right':
      opposite = prevDirection === 'left'
      break
    case 'top':
      opposite = prevDirection === 'bottom'
      break
    case 'bottom':
      opposite = prevDirection === 'top'
      break
    default:
      opposite = false
  }

  return opposite
}

export function positionsEqual(...positions) {
  return positions && positions.every(pos => pos.x === positions[0].x && pos.y === positions[0].y)
}

export function isInUserViewport(position, userPosition, size = null) {
  const VIEWPORT_DISTANCE_RESERVE = 2
  const { x, y } = position
  const leftTop = {
    x: userPosition.x - HALF_USER_MAP_VIEWPORT - VIEWPORT_DISTANCE_RESERVE,
    y: userPosition.y + HALF_USER_MAP_VIEWPORT + VIEWPORT_DISTANCE_RESERVE
  }
  const rightBottom = {
    x: userPosition.x + HALF_USER_MAP_VIEWPORT + VIEWPORT_DISTANCE_RESERVE,
    y: userPosition.y - HALF_USER_MAP_VIEWPORT - VIEWPORT_DISTANCE_RESERVE
  }

  if (!size || !size.width || !size.height) {
    return x >= leftTop.x && y <= leftTop.y && x <= rightBottom.x && y >= rightBottom.y
  }

  const rightCellPositionX = position.x + size.width / CELL_SIZE
  const bottomCellPositionY = position.y - size.height / CELL_SIZE

  for (let x = position.x; x < rightCellPositionX; x++) {
    for (let y = bottomCellPositionY; y < position.y; y++) {
      const pos = { x, y }

      if (isInUserViewport(pos, userPosition)) {
        return true
      }
    }
  }

  return false
}

export function getDirectionToPosition(positionCenter, positionToCheck, corners = true) {
  const x = positionToCheck.x + (0 - positionCenter.x)
  const y = positionToCheck.y + (0 - positionCenter.y)
  let direction

  if (corners && 2.5 * x + y < 0 && 0.5 * x + y > 0) {
    direction = 'leftTop'
  } else if (corners && -2.5 * x + y < 0 && -0.5 * x + y > 0) {
    direction = 'rightTop'
  } else if (corners && 2.5 * x + y > 0 && 0.5 * x + y < 0) {
    direction = 'rightBottom'
  } else if (corners && -2.5 * x + y > 0 && -0.5 * x + y < 0) {
    direction = 'leftBottom'
  } else if (x < 0 && y + x < 0 && y - x > 0) {
    direction = 'left'
  } else if (x > 0 && y + x > 0 && y - x < 0) {
    direction = 'right'
  } else if (y + x > 0 && y - x > 0) {
    direction = 'top'
  } else if (y + x < 0 && y - x < 0) {
    direction = 'bottom'
  } else {
    direction = ''
  }

  return direction
}

export function getNextPositionByDirection(currentPos, direction) {
  let changeCoordinates = { x: 0, y: 0 }

  switch (direction) {
    case 'left':
      changeCoordinates.x -= 1
      break
    case 'leftTop':
      changeCoordinates.x -= 1
      changeCoordinates.y += 1
      break
    case 'right':
      changeCoordinates.x += 1
      break
    case 'rightTop':
      changeCoordinates.x += 1
      changeCoordinates.y += 1
      break
    case 'top':
      changeCoordinates.y += 1
      break
    case 'bottom':
      changeCoordinates.y -= 1
      break
    case 'rightBottom':
      changeCoordinates.x += 1
      changeCoordinates.y -= 1
      break
    case 'leftBottom':
      changeCoordinates.x -= 1
      changeCoordinates.y -= 1
      break
    default:
      changeCoordinates = { x: 0, y: 0 }
  }

  return {
    x: currentPos.x + changeCoordinates.x,
    y: currentPos.y + changeCoordinates.y
  }
}

export function formatSeconds(seconds) {
  const minutes = Math.floor(seconds / 60)

  seconds %= 60
  if (seconds <= 0 && minutes <= 0) {
    return '00:00'
  }
  return `${(minutes < 10 ? '0' : '') + minutes}:${seconds < 10 ? '0' : ''}${seconds}`
}
/* global minTabletSize, maxTabletSize */

export function getWindowSize() {
  let size

  if (window.innerWidth <= minTabletSize) {
    size = 'mobile'
  } else if (window.innerWidth > minTabletSize && window.innerWidth < maxTabletSize) {
    size = 'tablet'
  } else if (window.innerWidth >= maxTabletSize) {
    size = 'desktop'
  }
  return size
}

export const getRandomInteger = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

export const getRandomFloat = (min, max) => {
  return Math.random() * (max - min) + min
}

export const convertTimeToPoints = (time, level) => {
  return Math.ceil(time * 10 * (level / 10) * level)
}

export const getOppositeDirection = direction => {
  const oppositeDirections = {
    left: 'right',
    right: 'left',
    top: 'bottom',
    bottom: 'top',
    leftTop: 'rightBottom',
    rightTop: 'leftBottom',
    rightBottom: 'leftTop',
    leftBottom: 'rightTop'
  }

  return oppositeDirections[direction]
}

export function isInt(value) {
  const x = parseFloat(value)

  return !isNaN(value) && (x | 0) === x
}

export const plural = val => {
  return val === 1 ? '' : 's'
}

export function getErrorMessage(json) {
  const error = json.message && json.message.type === 'error' && json.message.text

  console.error(error)

  return error
}

export const sortedInventoryByOrder = (first, second) => (first.order >= second.order ? 1 : -1)

export const mergeInventoryWithUpdates = (updatedInventoryItems, inventory) => {
  return updatedInventoryItems.reduce((inventoryItems, updatedItem) => {
    const found = inventoryItems.find(item => item.item_id === updatedItem.item_id)

    if (!found) {
      inventoryItems.push({ ...updatedItem, animation: true })
    }

    return inventoryItems.map(item => {
      const newItem = item.item_id === updatedItem.item_id ? updatedItem : item

      return {
        ...newItem,
        animation: item.item_id === updatedItem.item_id
      }
    })
  }, inventory)
}

export const getItemType = (itemCategory, itemType, itemQuantity) => {
  if (['bucks', 'coupon'].includes(itemCategory)) {
    return Math.min(itemQuantity, 3)
  }

  return itemType
}
