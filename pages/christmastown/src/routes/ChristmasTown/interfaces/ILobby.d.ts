import { IProps as ICreatorProps } from '../components/Lobby/Creator/interfaces'
import { IProps as IJoinProps } from '../components/Lobby/Join/interfaces'
import { IProps as IMapNameProps } from '../components/Lobby/MapName/interfaces'
import { IProps as IPlayersProps } from '../components/Lobby/Players/interfaces'
import { IProps as IRatingProps } from '../components/Lobby/Rating/interfaces'
import { IPrize } from '../components/Lobby/Prize/interfaces'
import { ILazyData } from '../modules/lobby/interfaces'

export type TMapID = string
export type TUserActiveMapID = TMapID
export type TOnlineStatus = 'online' | 'offline' | 'idle'
export type TCreatedAt = number
export type TLastEdit = number
export type TShowImages = boolean
export type TProgress = boolean
export type TType = string

export interface ILazyLoad extends ILazyData {
  noMoreData: boolean
  inProgress: boolean
}

export interface IType {
  type: string
}

export interface IRow extends IMapNameProps, IPlayersProps, IRatingProps, IJoinProps, IPrize {
  mapID: TMapID
  createdAt: TCreatedAt
  author: ICreatorProps
}

export interface ISettings {
  userActiveMapID: TUserActiveMapID
  showImages: TShowImages
  inProgress: TProgress
  lazyLoad: ILazyLoad
}

export interface IState {
  settings: ISettings
  publishedMapsList: IRow[]
}
