import IPosition from './IPosition'

export default interface IObject {
  category: string
  created: number
  hash: string
  height: number
  object_id: string
  onTopOfUser: boolean
  order: number
  position: IPosition
  type: number
  width: number
}