export type TGetActionPayload = {
  async: boolean
  type: string
  action: string
}

export interface IWindow extends Window {
  userIsTravelling?: () => boolean
  getAction?: (TGetActionPayload) => void
}
