import IPosition from './IPosition'

export default interface IGateController {
  position: IPosition
  radius: string
  status: string
  time: number
}
