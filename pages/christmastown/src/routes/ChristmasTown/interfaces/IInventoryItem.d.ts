export default interface IInventoryItem {
  category: string
  item_id: string
  name: string
  type: number
  animation: boolean
  quantity?: number
}
