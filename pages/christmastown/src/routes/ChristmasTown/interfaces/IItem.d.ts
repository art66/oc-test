import IPosition from '../interfaces/IPosition'

export default interface IItem {
  position: IPosition
  image: {
    url: string
  }
}
