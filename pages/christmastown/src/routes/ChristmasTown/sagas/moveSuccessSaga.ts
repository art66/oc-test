import { put, select, takeEvery } from 'redux-saga/effects'
import { MOVED } from '../actionTypes'
import { loadGame } from '../modules'
import { getMinigameType } from '../selectors/mapData'

function* moveSuccess() {
  const miniGameType = yield select(getMinigameType)

  if (miniGameType) {
    yield put(loadGame(miniGameType))
  }
}

export function* moveSuccessSaga() {
  yield takeEvery(MOVED, moveSuccess)
}
