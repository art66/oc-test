import { put, takeEvery } from 'redux-saga/effects'
import { fetchUrl, getErrorMessage } from '../utils'
import { debugShow } from '../../../controller/actions'
import { SEND_CHAT_MESSAGE } from '../actionTypes'

function* sendChatMessage(action: { type: string; payload: { message: string } }) {
  try {
    const { message } = action.payload

    yield fetchUrl('sendChatMessage', { message })

  } catch (e) {
    yield put(debugShow(getErrorMessage(e)))
  }
}

export default function* chat() {
  yield takeEvery(SEND_CHAT_MESSAGE, sendChatMessage)
}
