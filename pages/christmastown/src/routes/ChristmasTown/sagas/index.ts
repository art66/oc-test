import { all } from 'redux-saga/effects'
import spinTheWheelSaga from '../components/minigames/SpinTheWheel/sagas'
import typocalypseSaga from '../components/minigames/Typocalypse/sagas'
import votingSaga from '../components/minigames/Voting/sagas'
import couponExchangeSaga from '../components/minigames/CouponExchange/sagas'
import chat from './chat'
import { moveStartSaga } from './moveStartSaga'
import { moveSuccessSaga } from './moveSuccessSaga'
import tabsSync from './tabsSync'

export default function* christmasTownSaga() {
  yield all([
    couponExchangeSaga(),
    votingSaga(),
    chat(),
    tabsSync(),
    spinTheWheelSaga(),
    typocalypseSaga(),
    moveStartSaga(),
    moveSuccessSaga()
  ])
}
