import { put, select, takeEvery } from 'redux-saga/effects'
import { MOVE_INITED } from '../actionTypes'
import { leaveGame } from '../modules'
import { getLoadedMinigameName } from '../selectors/minigame'

function* moveStart() {
  const loadedMinigameName = yield select(getLoadedMinigameName)

  if (loadedMinigameName) {
    yield put(leaveGame())
  }
}

export function* moveStartSaga() {
  yield takeEvery(MOVE_INITED, moveStart)
}
