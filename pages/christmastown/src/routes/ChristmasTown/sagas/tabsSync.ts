import { put, takeEvery } from 'redux-saga/effects'

import { CHECK_HUD_TABS_SYNC } from '../actionTypes'
import { fetchUrl, getErrorMessage } from '../utils'
import { debugShow } from '../../../controller/actions'
import { synchronizeHudData } from '../actions'
import IPosition from '../interfaces/IPosition'

interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* syncHudData(action: IAction<{ mapID: string; position: IPosition }>) {
  try {
    const { mapID, position } = action.payload
    const { mapChanged, initHudData } = yield fetchUrl('checkHudSynchronization', { mapID, position })

    if (mapChanged) {
      yield put(synchronizeHudData(initHudData))
    }
  } catch (error) {
    yield put(debugShow(getErrorMessage(error)))
  }
}

export default function* tabsSync() {
  yield takeEvery(CHECK_HUD_TABS_SYNC, syncHudData)
}
