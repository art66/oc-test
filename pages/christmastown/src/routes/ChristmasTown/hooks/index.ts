import { useSelector } from 'react-redux'

export function useParamSelector(selector, ...params) {
  return useSelector((state) => selector(state, ...params))
}
