// eslint-disable-next-line no-restricted-imports
import _ from 'lodash'
import { mergeMapElements } from '../../../../../christmastown_editor/src/utils'
import { isInUserViewport, positionsEqual, sortedInventoryByOrder, mergeInventoryWithUpdates } from '../utils'
import * as a from '../actionTypes'
import {
  CELL_SIZE,
  MAP_VIEWPORT_CELLS_STOCK,
  MOVING_INTERVAL,
  MAX_STEP_DURATION,
  JAIL,
  HOSPITAL,
  MOVING_TIME_DEFAULT,
  MOVING_TIME_DIAGONAL,
  NPC,
  CELL_EVENT_TYPE_TELEPORT,
  TELEPORT_ANIMATION_FALL,
  DEFAULT_USER_SPEED
} from '../constants'
import IGateController from '../interfaces/IGateController'

interface ITornWindow extends Window {
  getCurrentTimestamp?: () => number
}

const initialState = {
  areaTooltip: {},
  movingTimeOut: 0,
  movingResponseRecieved: true,
  mapSize: {
    mapWidth: 15000,
    mapHeight: 15000
  },
  mapName: 'Christmas town',
  user: {
    position: {
      x: 0,
      y: 0
    },
    isAdmin: false
  },
  users: [],
  inventory: [],
  objects: [],
  items: [],
  parameters: [],
  steps: [],
  busyArea: [],
  gates: [],
  couponsAmount: 0,
  couponsAmountForDay: 0
}

const handleInitialData = (mapData, state) => {
  const coupons = mapData.inventory.find(item => item.category === 'coupon')

  return {
    ...state,
    moveInProgress: false,
    movingResponseRecieved: true,
    christmasTownDataFetched: true,
    mapSize: {
      mapWidth: (mapData.map.size.rightBottom.x - mapData.map.size.leftTop.x + MAP_VIEWPORT_CELLS_STOCK) * CELL_SIZE,
      mapHeight: (mapData.map.size.leftTop.y - mapData.map.size.rightBottom.y + MAP_VIEWPORT_CELLS_STOCK) * CELL_SIZE
    },
    mapName: mapData.map.name,
    map: mapData.map,
    movement: mapData.movement,
    user: {
      ...state.user,
      user_id: mapData.user.user_id,
      playername: mapData.user.playername,
      position: mapData.position,
      isAdmin: mapData.isAdmin,
      timeToResetSuicide: (mapData.user && mapData.user.timeToResetSuicide) || 0
    },
    users: mapData.users,
    objects: mapData.objects,
    parameters: mapData.parametersAround,
    steps: mapData.steps.map((step, i) => ({ ...step, id: i })),
    busyArea: mapData.busyArea,
    currentArea: mapData.currentArea,
    inventory: mapData.inventory,
    items: mapData.items,
    currentBlocking: mapData.currentBlocking,
    gates: mapData.gates || [],
    couponsAmount: coupons ? coupons?.quantity : 0,
    couponsAmountForDay: mapData.user.prizeCouponTodayReceivedAmount
  }
}

function concatGatesState(prevGates: IGateController[], nextGates: IGateController[]) {
  const newPositions = nextGates.map(gate => `${gate.position.x};${gate.position.y}`)
  const oldGates = prevGates.filter(({ position }) => !newPositions.includes(`${position.x};${position.y}`))

  return [...oldGates, ...nextGates]
}

const ACTION_HANDLERS = {
  [a.MAP_DATA_FETCHED]: (state, action) => {
    const { mapData } = action.json

    return handleInitialData(mapData, state)
  },

  [a.SYNCHRONIZE_HUD_DATA]: (state, action) => {
    const { mapData } = action.payload

    return handleInitialData(mapData, state)
  },

  [a.START_MOVING]: (state, action) => {
    return {
      ...state,
      movingTo: action.direction
    }
  },

  [a.STOP_MOVING]: state => {
    return {
      ...state,
      movingTo: null
    }
  },

  [a.SHOW_MOVING]: (state, action) => {
    const timestamp = Math.round((window as ITornWindow).getCurrentTimestamp() / 1000)
    const lastStep = state.steps[state.steps.length - 1]

    return {
      ...state,
      movingTo: action.continuously ? action.direction : '',
      moveInProgress: true,
      movingTime: action.movingTime,
      movingTimeOut: MOVING_INTERVAL,
      movingResponseRecieved: false,
      user: {
        ...state.user,
        hospitalized: false,
        position: action.nextPosition
      },
      steps: state.steps
        .filter(step => step.timestamp + MAX_STEP_DURATION > timestamp)
        .concat({
          id: (lastStep && lastStep.id + 1) || 0,
          position: state.user.position,
          direction: action.direction,
          prevDirection: state.steps[0] && state.steps[state.steps.length - 1].direction,
          duration: MAX_STEP_DURATION,
          newStep: true,
          timestamp
        })
    }
  },

  // eslint-disable-next-line complexity
  [a.MOVED]: (state, action) => {
    const { mapData } = action.json
    const { cellEvent, trigger } = mapData
    const isHospitalized =
      cellEvent && cellEvent.type === CELL_EVENT_TYPE_TELEPORT && cellEvent.animation === TELEPORT_ANIMATION_FALL
    const teleported = cellEvent && cellEvent.type === CELL_EVENT_TYPE_TELEPORT
    const newPosition = isHospitalized ? state.user.position : mapData.position
    const oldUsers = state.users.filter(user => isInUserViewport(user.position, newPosition) && user.type !== 'NPC')
    const newUsers = mapData.users
    const onlyNewUsers = _.differenceBy(_.sortBy(newUsers, 'user_id'), _.sortBy(oldUsers, 'user_id'), 'user_id')
    const users = oldUsers.concat(onlyNewUsers)
    const objectsInViewport = state.objects.filter(obj =>
      isInUserViewport(obj.position, newPosition, { width: obj.width, height: obj.height }))

    return {
      ...state,
      moveInProgress: false,
      movingResponseRecieved: true,
      mapBlocked:
        trigger && trigger.type && (trigger.type.toLowerCase() === JAIL || trigger.type.toLowerCase() === HOSPITAL),
      user: {
        ...state.user,
        isAdmin: mapData.isAdmin,
        position: teleported ? state.user.position : mapData.position,
        hospitalized: isHospitalized
      },
      users,
      objects: teleported ? state.objects : mergeMapElements(objectsInViewport, mapData.objects),
      parameters: mapData.parametersAround,
      movement: mapData.movement,
      busyArea: mapData.busyArea,
      items: teleported ? state.items : mapData.items,
      newInventoryItems: [],
      trigger,
      gates: concatGatesState(state.gates, mapData.gates || []),
      currentBlocking: mapData.currentBlocking,
      couponsAmountForDay: mapData.user.prizeCouponTodayReceivedAmount
    }
  },

  [a.MOVED_ERROR]: state => {
    return {
      ...state,
      movingResponseRecieved: true
    }
  },

  [a.SET_ENTERING_AREA_MESSAGE]: (state, action) => {
    return {
      ...state,
      currentArea: action.payload.message,
      areaTooltip: {
        timestamp: Date.now(),
        text: action.payload.message
      }
    }
  },

  [a.REMOVE_ENTERING_AREA_MESSAGE]: state => {
    return {
      ...state,
      currentArea: null
    }
  },

  [a.DECREMENT_MOVING_TIMEOUT]: state => {
    return {
      ...state,
      movingTimeOut: state.movingTimeOut > 0 ? state.movingTimeOut - 500 : 0
    }
  },

  [a.SET_MOVING_TIME]: (state, action) => {
    return {
      ...state,
      movingTime: action.time
    }
  },

  [a.UPDATE_USER_INVENTORY]: (state, action) => {
    const inventory = mergeInventoryWithUpdates(action.payload.inventory, state.inventory)
      .filter(item => item?.quantity !== 0 || item?.quantity === undefined)
      .sort(sortedInventoryByOrder)
    const coupons = action.payload.inventory.find(item => item.category === 'coupon')

    return {
      ...state,
      inventory,
      newInventoryItems: action.payload.inventory,
      couponsAmount: coupons ? coupons?.quantity : state.couponsAmount,
      couponsAmountForDay: coupons ? state.couponsAmountForDay + 1 : state.couponsAmountForDay
    }
  },

  // eslint-disable-next-line complexity,max-statements
  [a.UPDATE_USER_POSITION]: (state, action) => {
    if (state.user.user_id === action.user.user_id) {
      return {
        ...state,
        ...action.options,
        user: {
          ...state.user,
          position: action.user.position,
          exited: false
        }
      }
    }

    const { mapData } = state
    const speedCoefficient = mapData && mapData.movement && mapData.movement.modifier
    const nextParameter = state.parameters.find(p => positionsEqual(p.position, action.user.position))
    const nextSpeedTrigger = nextParameter && nextParameter.triggers && nextParameter.triggers.speed
    const changeSpeed =
      nextParameter
      && nextSpeedTrigger
      && nextSpeedTrigger.change
      && nextSpeedTrigger.change.timeout > (window as ITornWindow).getCurrentTimestamp() / 1000
      && nextSpeedTrigger.change.value
    const triggerSpeed = nextParameter && nextSpeedTrigger && nextSpeedTrigger.value
    const speedMultiplier = speedCoefficient < 0 ? 1 : speedCoefficient
    const speedModifier =
      changeSpeed * speedMultiplier || triggerSpeed * speedMultiplier || DEFAULT_USER_SPEED * speedMultiplier
    const movingTimeDiagonal = speedModifier ? MOVING_TIME_DIAGONAL / speedModifier : 0
    const movingTimeDefault = speedModifier ? MOVING_TIME_DEFAULT / speedModifier : 0
    const users = []

    let movingTime = MOVING_TIME_DEFAULT

    let userExists

    // eslint-disable-next-line array-callback-return
    state.users.map(user => {
      let newPosition = user.position

      let { exited } = user

      let { time } = user

      if (user.user_id.toString() === action.user.user_id.toString()) {
        userExists = true
        exited = false
        time = action.user.time
        newPosition = user.hospitalized ? user.position : action.user.position
        movingTime =
          user.position.x - newPosition.x !== 0 && user.position.y - newPosition.y !== 0 ?
            movingTimeDiagonal :
            movingTimeDefault
        if (user.type === 'NPC') {
          user.direction = action.user.direction
        }
      }
      users.push({
        ...user,
        exited,
        time,
        position: newPosition,
        invisible: (action.options && action.options.invisible) || false,
        movingTime
      })
    })

    if (!userExists) {
      users.push(action.user)
    }

    return {
      ...state,
      users
    }
  },

  [a.REMOVE_USER]: (state, action) => {
    return {
      ...state,
      users: state.users.filter(user => user.user_id !== action.user.user_id)
    }
  },

  [a.SET_USER_FIELD]: (state, action) => {
    return {
      ...state,
      user:
        !action.user || !action.user.user_id !== state.user.user_id ?
          state.user :
          {
            ...state.user,
            [action.field]: action.value
          },
      users:
        action.user && action.user.user_id !== state.user.user_id ?
          state.users.map(user =>
            (user.user_id !== action.user.user_id ?
              user :
              {
                ...user,
                [action.field]: action.value
              })) :
          state.users
    }
  },

  [a.SET_USER_POSITION]: (state, action) => {
    const { mapData } = action.json

    return {
      ...state,
      user: {
        ...state.user,
        position: mapData.position,
        isAdmin: mapData.isAdmin
      },
      users: mapData.users,
      objects: mapData.objects,
      parameters: mapData.parametersAround,
      busyArea: mapData.busyArea,
      items: mapData.items
    }
  },

  [a.SET_CELL_EVENT]: (state, action) => {
    const isHospitalized =
      action.cellEvent
      && action.cellEvent.type === CELL_EVENT_TYPE_TELEPORT
      && action.cellEvent.animation === TELEPORT_ANIMATION_FALL

    return {
      ...state,
      user: {
        ...state.user,
        hospitalized: isHospitalized || state.user.hospitalized
      }
    }
  },

  [a.CHEST_OPENED_BY_SOMEBODY]: (state, action) => {
    return {
      ...state,
      items: state.items.filter(item => item.hash !== action.json.hash)
    }
  },

  [a.LEAVE_GAME]: state => {
    return {
      ...state,
      mapBlocked: false
    }
  },

  [a.BLOCK_MAP]: state => {
    return {
      ...state,
      mapBlocked: true
    }
  },

  [a.UNBLOCK_MAP]: state => {
    return {
      ...state,
      mapBlocked: false
    }
  },

  [a.SET_CREDETIALS]: (state, action) => {
    return {
      ...state,
      user: {
        ...state.user,
        playername: action.credentials && action.credentials.playername,
        user_id: action.credentials && action.credentials.userID
      }
    }
  },

  [a.TELEPORT]: (state, action) => {
    return {
      ...state,
      user: {
        ...state.user,
        position: action.position
      }
    }
  },

  [a.HOSPITALIZE]: (state, action) => {
    return {
      ...state,
      movingTime: action.user.user_id === state.user.user_id ? MOVING_TIME_DEFAULT : state.movingTime,
      user: {
        ...state.user,
        hospitalized: action.user.user_id === state.user.user_id || state.user.hospitalized
      },
      users: state.users.map(user => {
        if (user.user_id === action.user.user_id) {
          return {
            ...user,
            movingTime: MOVING_TIME_DEFAULT,
            hospitalized: true
          }
        }

        return user
      })
    }
  },

  [a.LEAVE]: (state, action) => {
    return {
      ...state,
      user: {
        ...state.user,
        exited: action.user.user_id === state.user.user_id || state.user.exited
      },
      users: state.users.map(user => ({
        ...user,
        exited: user.user_id === action.user.user_id || user.exited
      }))
    }
  },

  [a.FILL_SCREEN_WITH_COLOR]: (state, action) => {
    return {
      ...state,
      screenColor: action.color
    }
  },

  [a.UPDATE_GATE_CONTROLLER]: (state, action) => {
    return {
      ...state,
      gates: state.gates.map(gate => {
        if (action.payload.position.x === gate.position.x && action.payload.position.y === gate.position.y) {
          return {
            ...gate,
            ...action.payload.gate
          }
        }

        return gate
      })
    }
  },

  [a.UPDATE_GATE_CONTROLLERS]: (state, action) => {
    const { gates, time, status } = action.payload

    return {
      ...state,
      gates: state.gates.map(gate => {
        if (gates.find(g => g.position.x === gate.position.x && g.position.y === gate.position.y)) {
          return {
            ...gate,
            time,
            status
          }
        }

        return gate
      })
    }
  },
  [a.SET_USER_GESTURE]: (state, action) => {
    if (state.user.user_id === action.payload.userID) {
      return {
        ...state,
        user: {
          ...state.user,
          gesture: action.payload.gesture
        }
      }
    }

    return {
      ...state,
      users: state.users.map(user => {
        if (user.user_id === action.payload.userID) {
          return {
            ...user,
            gesture: action.payload.gesture
          }
        }

        return user
      })
    }
  },

  [a.SET_USER_MESSAGE]: (state, action) => {
    if (+state.user.user_id === +action.payload.userID) {
      return {
        ...state,
        user: {
          ...state.user,
          message: action.payload.message
        }
      }
    }

    return {
      ...state,
      users: state.users.map(user => {
        if (+user.user_id === +action.payload.userID) {
          return {
            ...user,
            message: action.payload.message
          }
        }

        return user
      })
    }
  },

  [a.COLLECT_GARBAGE]: state => {
    const timestamp = (window as ITornWindow).getCurrentTimestamp() / 1000
    const INACTIVE_TIME = 300

    return {
      ...state,
      users: state.users
        .filter(user => !user.exited && isInUserViewport(user.position, state.user.position))
        .map(user => (timestamp - user.time < INACTIVE_TIME || user.type === NPC ? user : { ...user, exited: true }))
    }
  }
}

export default function mapDataReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
