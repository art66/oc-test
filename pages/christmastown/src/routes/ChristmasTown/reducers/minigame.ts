import {
  LOAD_GAME,
  GAME_LOADED,
  GAME_END_RESULT_LOADED,
  SET_GAME_TITLE,
  SET_GAME_CONTROLS,
  ACCEPT_GAME,
  LEAVE_GAME,
  END_GAME
} from '../actionTypes'

type TState = {
  itsaMinigameCell: boolean
  gameName?: string
  gameTitle?: string
  gameEndResult?: any
  gameControls?: any
  gameAccepted?: boolean
  gameOver?: boolean
}

const initialState: TState = {
  gameName: '',
  itsaMinigameCell: false
}

const ACTION_HANDLERS = {
  [LOAD_GAME]: (state: TState, action): TState => ({
    ...state,
    gameName: action.gameName
  }),

  [GAME_LOADED]: (state: TState, action): TState => ({
    ...state,
    gameName: action.gameName,
    itsaMinigameCell: true,
    gameOver: false
  }),

  [GAME_END_RESULT_LOADED]: (state: TState, action): TState => ({
    ...state,
    gameEndResult: action.json
  }),

  [SET_GAME_TITLE]: (state: TState, action): TState => ({
    ...state,
    gameTitle: action.gameTitle
  }),

  [SET_GAME_CONTROLS]: (state: TState, action): TState => ({
    ...state,
    gameControls: action.gameControls
  }),

  [ACCEPT_GAME]: (state: TState): TState => ({
    ...state,
    gameAccepted: true
  }),

  [LEAVE_GAME]: () => initialState,

  [END_GAME]: (state: TState): TState => ({
    ...state,
    gameOver: true
  })
}

export default function minigameReducer(state: TState = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
