import { DEV_TOGGLER } from '../actionTypes'

const initialState = false

const ACTION_HANDLERS = {
  [DEV_TOGGLER]: state => !state
}

export default function devPanelReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
