import {
  SHOW_MOVING,
  MOVED,
  SET_CELL_EVENT,
  QUIT_CELL_EVENT,
  START_OPENING_CHEST,
  CHEST_OPENED,
  CHEST_OPENED_BY_SOMEBODY,
  SET_STATUS_AREA_MSG,
  SET_STATUS_AREA_ITEM,
  SET_MESSAGE_AREA_IMAGE_URL
} from '../actionTypes'

const initialState = {
  text: 'Welcome to Christmas Town',
  itemImage: null,
  cellEvent: {}
}

const ACTION_HANDLERS = {
  [SHOW_MOVING]: state => {
    return {
      ...state,
      item: null
    }
  },

  [MOVED]: (state, action) => {
    const { mapData } = action.json
    const { trigger, cellEvent } = mapData

    return {
      ...state,
      text: (trigger && trigger.message) || cellEvent?.message || state.text,
      itemImage: (trigger && trigger.item && trigger.item.image) || null,
      itemIsReceived: trigger?.item?.isReceived,
      triggerType: trigger && trigger.type,
      triggerLink: trigger && trigger.link,
      cellEvent: mapData.cellEvent,
      messageAreaImageUrl: mapData.messageAreaImageUrl
    }
  },

  [SET_CELL_EVENT]: (state, action) => {
    const { cellEvent } = action

    return {
      ...state,
      text: (cellEvent && cellEvent.message) || state.text,
      triggerType: cellEvent && cellEvent.type,
      cellEvent: action.cellEvent
    }
  },

  [SET_MESSAGE_AREA_IMAGE_URL]: (state, action) => {
    return {
      ...state,
      messageAreaImageUrl: action.messageAreaImageUrl
    }
  },

  [QUIT_CELL_EVENT]: state => {
    return {
      ...state,
      cellEvent: {}
    }
  },

  [START_OPENING_CHEST]: state => {
    return {
      ...state,
      cellEvent: {
        ...state.cellEvent,
        chestOpeningStart: true
      }
    }
  },

  [CHEST_OPENED]: (state, action) => {
    const { prizes, message } = action.json

    return {
      ...state,
      cellEvent: {
        ...state.cellEvent,
        chestOpeningStart: false,
        chestOpened: true,
        prizes: prizes || [],
        message
      }
    }
  },

  [CHEST_OPENED_BY_SOMEBODY]: (state, action) => {
    const { message } = action.json

    return {
      ...state,
      cellEvent:
        state.cellEvent.type === 'chests' ?
          {
            ...state.cellEvent,
            chestOpeningStart: false,
            chestOpened: true,
            prizes: [],
            message
          } :
          state.cellEvent
    }
  },

  [SET_STATUS_AREA_MSG]: (state, action) => {
    return {
      ...state,
      text: action.msg || state.text
    }
  },

  [SET_STATUS_AREA_ITEM]: (state, action) => {
    const selectedItemId = (state.item && state.item.item_id) || null

    return {
      ...state,
      item: action.item.item_id === selectedItemId ? null : action.item
    }
  }
}

export default function statusAreaReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
