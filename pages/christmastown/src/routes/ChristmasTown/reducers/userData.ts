import {
  MAP_DATA_FETCHED,
  SYNCHRONIZE_HUD_DATA,
  MOVED,
  LEAVE_GAME
} from '../actionTypes'

const initialState = {
  userStatus: 'ok'
}

const ACTION_HANDLERS = {
  [MAP_DATA_FETCHED]: (_, action) => {
    return action.json.userData
  },

  [SYNCHRONIZE_HUD_DATA]: (_, action) => {
    return action.payload.userData
  },

  [MOVED]: (_, action) => {
    return action.json.userData
  },

  [LEAVE_GAME]: (_, __, fullState) => {
    return fullState.minigame.gameEndResult && fullState.minigame.gameEndResult.userData ?
      fullState.minigame.gameEndResult.userData :
      fullState.userData
  }
}

export default function userDataReducer(state: any = initialState, action: any, fullState: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action, fullState) : state
}
