import { SET_CREDETIALS } from '../actionTypes'

const initialState = {}

const ACTION_HANDLERS = {
  [SET_CREDETIALS]: (_, action) => {
    return action.credentials
  }
}

export default function credentialsReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
