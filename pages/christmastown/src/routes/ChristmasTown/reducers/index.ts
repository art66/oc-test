import mapData from './mapData'
import minigame from './minigame'
import statusArea from './statusArea'
import userData from './userData'
import credentials from './credentials'
import devPanel from './devPanel'

const rootReducer = (state: any = {}, action: any) => {
  return {
    mapData: mapData(state.mapData, action),
    minigame: minigame(state.minigame, action),
    statusArea: statusArea(state.statusArea, action),
    userData: userData(state.userData, action, state),
    credentials: credentials(state.credentials, action),
    devPanelActivated: devPanel(state.devPanelActivated, action)
  }
}

export default rootReducer
