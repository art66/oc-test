export const CELL_SIZE = 30
export const ADMIN_MAP_VIEWPORT = 21
export const USER_MAP_VIEWPORT = 11
export const HALF_USER_MAP_VIEWPORT = 5
export const MOVING_INTERVAL = 500
export const MAX_STEP_DURATION = 300
export const CELL_MIN_RANGE = 1
export const CELL_MAX_RANGE = 10
export const MAP_VIEWPORT_CELLS_STOCK = 1
export const USER_POSITION_MIN_X = 5
export const USER_POSITION_MIN_Y = 0
export const PLAYER_ICON_WIDTH = 6
export const MOVING_TIME_DEFAULT = 1000
export const MOVING_TIME_DIAGONAL = 1200
export const DEFAULT_USER_SPEED = 1
export const NPC = 'NPC'
export const TORN_ITEMS = 'tornItems'
export const MONEY_ITEM_CATEGORY = 'money'

export const ADMIN_POSITION_MIN_X = 10
export const ADMIN_POSITION_MIN_Y = 10

export const JAIL = 'jail'
export const HOSPITAL = 'hospital'
export const DEFAULT_CATEGORY_NAME = 'default'
export const DEFAULT_TRIGGER_NAME = 'no trigger'

export const MAIN_URL = '/christmas_town.php?q='

export const INVENTORY_SLIDER_CHUNK_SIZE = 10
export const PIXELS = 'px'
export const ITEMS_URL = '/images/items/'
export const CT_ITEMS_URL = `${ITEMS_URL}christmas_town/`
export const CT_ITEMS_CATEGORY_KEYS = 'keys'
export const CT_ITEMS_CATEGORY_ITEMS = 'ctItems'
export const CT_ITEMS_CATEGORY_NOTES = 'notes'
export const CT_ITEMS_CATEGORY_CHESTS = 'chests'
export const CT_ITEMS_CATEGORY_COMBINATION_CHEST = 'combinationChest'
export const TORN_CATEGORY_ITEMS = 'tornItems'
export const ITEMS_IMG_EXTENSION = '.png'
export const OFFSET_ITEMS_SIZE = 0
export const OFFSET_CT_ITEMS_SIZE = 0
export const OFFSET_KEYS_SIZE = 10
export const OFFSET_NOTES_SIZE = 10
export const OFFSET_CHESTS_SIZE = 0

export const FILL_SCREEN_DURATION = 750

export const LEFT_TOP = 'leftTop'
export const RIGHT_TOP = 'rightTop'
export const LEFT_BOTTOM = 'leftBottom'
export const RIGHT_BOTTOM = 'rightBottom'
export const TOP = 'top'
export const RIGHT = 'right'
export const BOTTOM = 'bottom'
export const LEFT = 'left'

export const GATE_CONTROL = 'gateControl'
export const GATE_INNER = 'gateInner'
export const GATE_OUTER = 'gateOuter'

export const ANIMATIONS = {
  HAND_MOTION: 'handMotion',
  HANDS_UP: 'handsUp',
  PULSING: 'pulsing',
  SHAKING: 'shaking',
  JUMPING: 'jumping'
}

export const KEY_CODE_ENTER = 13
export const MESSAGE_MAX_LENGTH = 50
export const MESSAGE_REPOSITIONING_OFFSET = 3

export const CELL_EVENT_TYPE_TELEPORT = 'teleport'
export const TELEPORT_ANIMATION_FALL = 'fall'

export const MINI_GAME_TELEPORT = 'Teleport'
export const TOOLTIP_DELAY = 500

export const AREA_TOOLTIP_DISPLAYING_DURATION = 4000
