import { takeLatest } from 'redux-saga/effects'

import { fetchLobbyData, lazyMapLoaded } from './lobby/sagas'

import { LOBBY_INIT_START, LAZY_MAPS_LOAD_ATTEMPT } from './lobby/constants'

export default function* rootSaga() {
  yield takeLatest(LOBBY_INIT_START, fetchLobbyData)
  yield takeLatest(LAZY_MAPS_LOAD_ATTEMPT, lazyMapLoaded)
}
