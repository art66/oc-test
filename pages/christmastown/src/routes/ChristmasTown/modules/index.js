/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable complexity */
/* eslint-disable max-statements */
import { isInRectangle } from '../../../../../christmastown_editor/src/routes/MapEditor/utils'
import { debugHide, debugShow, getRouteHeader, infoShow } from '../../../controller/actions'
import * as a from '../actionTypes'
import {
  AREA_TOOLTIP_DISPLAYING_DURATION,
  CELL_SIZE,
  DEFAULT_USER_SPEED,
  FILL_SCREEN_DURATION,
  GATE_INNER,
  GATE_OUTER,
  HOSPITAL,
  JAIL,
  MAP_VIEWPORT_CELLS_STOCK,
  MINI_GAME_TELEPORT,
  MOVING_TIME_DEFAULT,
  MOVING_TIME_DIAGONAL,
  USER_POSITION_MIN_X,
  USER_POSITION_MIN_Y
} from '../constants'
import { fetchUrl, getErrorMessage, getNextPositionByDirection, getOppositeDirection, positionsEqual } from '../utils'
import { movedError, removeEnteringAreaMessage, setEnteringAreaMessage } from '../actions'

/* global getCurrentTimestamp */

// ------------------------------------
// Actions
// ------------------------------------

export const fetchMapData = () => dispatch => {
  return new Promise(resolve => {
    fetchUrl('initMap')
      .then(json => {
        if (json.message && json.message.type === 'info') {
          dispatch(infoShow(json.message.text))
        }
        dispatch(mapDataFetched(json))
        resolve()
      })
      .catch(error => {
        dispatch(debugShow(getErrorMessage(error)))
      })
      .finally(() => dispatch(getRouteHeader()))
  })
}

export const resetLocation = () => dispatch => {
  dispatch(setMovingTime(0))
  fetchUrl('suicide')
    .then(() => dispatch(fillScreenWithColor('white')))
    .then(() => dispatch(fetchMapData()))
    .then(() => {
      setTimeout(() => {
        dispatch(fillScreenWithColor('transparent'))
      }, 1500)
    })
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}

export const mapDataFetched = json => ({
  type: a.MAP_DATA_FETCHED,
  json
})

export const startMoving = direction => (dispatch, getState) => {
  dispatch(move(direction, { continuously: true })).then(() => {
    const { movingTo } = getState().christmastown.mapData

    if (movingTo) {
      dispatch(startMoving(movingTo))
    }
  })
}

export const stopMoving = () => ({
  type: a.STOP_MOVING
})

let decrementInterval

function checkGatesClosure(mapData, direction) {
  const nextPosition = getNextPositionByDirection(mapData.user.position, direction)
  const currentParameter = mapData.parameters.find(p => positionsEqual(p.position, mapData.user.position))
  const nextParameter = mapData.parameters.find(p => positionsEqual(p.position, nextPosition))

  if (!currentParameter || !currentParameter.triggers.gates || !nextParameter || !nextParameter.triggers.gates) {
    return false
  }

  if (
    (currentParameter.triggers.gates.type === GATE_INNER && nextParameter.triggers.gates.type === GATE_OUTER)
    || (currentParameter.triggers.gates.type === GATE_OUTER && nextParameter.triggers.gates.type === GATE_INNER)
  ) {
    const gateControls = mapData.gates.filter(gate => {
      const radius = parseInt(gate.radius.toString(), 10)
      const rectangle = {
        leftTop: { x: gate.position.x - radius, y: gate.position.y + radius },
        rightBottom: { x: gate.position.x + radius, y: gate.position.y - radius }
      }

      return isInRectangle(mapData.user.position, rectangle)
    })

    return gateControls.every(control => control.time === 0)
  }
}

let areaTimeout

export const move = (direction, options = { continuously: false }) => (dispatch, getState) =>
  new Promise(resolve => {
    const { mapData, userData, minigame } = getState().christmastown
    let resolved = false

    if (
      mapData.movingTimeOut !== 0
      || !mapData.movingResponseRecieved
      || mapData.mapBlocked
      || userData.userStatus === JAIL
      || userData.userStatus === HOSPITAL
    ) {
      if (options.continuously && mapData.movingTo !== direction) {
        dispatch({ type: a.START_MOVING, direction })
      }

      return
    }

    const changeCoordinates = { x: 0, y: 0 }
    const userPosition = mapData.user.position
    const nextPosition = getNextPositionByDirection(userPosition, direction)
    const nextParameter = mapData.parameters.find(parameter => positionsEqual(parameter.position, nextPosition))
    const currentParameter = mapData.parameters.find(parameter => positionsEqual(parameter.position, userPosition))
    const blocked = mapData.currentBlocking[direction]
    const gatesClosed = checkGatesClosure(mapData, direction)
    const { leftTop, rightBottom } = mapData.map.size

    if (
      blocked
      || gatesClosed
      || (nextParameter && nextParameter.triggers.blocked && nextParameter.triggers.blocked.includes(direction))
      || (currentParameter
        && currentParameter.triggers.blocked
        && currentParameter.triggers.blocked.some(
          blockedDirection => blockedDirection === getOppositeDirection(direction)
        ))
      || nextPosition.x < leftTop.x
      || nextPosition.x > rightBottom.x
      || nextPosition.y < rightBottom.y
      || nextPosition.y > leftTop.y
    ) {
      const { user } = getState().christmastown.mapData

      dispatch(updateUserPosition({ ...user, position: nextPosition }, { throttle: true, mapBlocked: true }))
      dispatch(updateUserPosition({ ...user, position: userPosition }, { throttle: true, mapBlocked: false }))
      setTimeout(() => {
        dispatch(updateUserPosition(user, { throttle: false, mapBlocked: false }))
      }, 150)

      return false
    }

    const speedCoefficient = mapData && mapData.movement && mapData.movement.modifier
    const nextSpeedTrigger = nextParameter && nextParameter.triggers && nextParameter.triggers.speed
    const changeSpeed =
      nextParameter
      && nextSpeedTrigger
      && nextSpeedTrigger.change
      && nextSpeedTrigger.change.timeout > getCurrentTimestamp() / 1000
      && nextSpeedTrigger.change.value
    const triggerSpeed = nextParameter && nextSpeedTrigger && nextSpeedTrigger.value
    const speedMultiplier = speedCoefficient < 0 ? 1 : speedCoefficient

    if (minigame && minigame.gameName === MINI_GAME_TELEPORT) {
      dispatch(leaveGame())
    }

    if (speedCoefficient) {
      const speedModifier =
        changeSpeed * speedMultiplier || triggerSpeed * speedMultiplier || DEFAULT_USER_SPEED * speedMultiplier
      const movingTime =
        changeCoordinates.x !== 0 && changeCoordinates.y !== 0 ?
          MOVING_TIME_DIAGONAL / speedModifier :
          MOVING_TIME_DEFAULT / speedModifier

      dispatch(showMoving(nextPosition, direction, movingTime, options.continuously))
      clearTimeout(decrementInterval)
      decrementInterval = setTimeout(() => {
        dispatch(decrementMovingTimeOut())
        const { movingTimeOut, movingResponseRecieved } = getState().christmastown.mapData

        if (!resolved && movingTimeOut <= 0 && movingResponseRecieved) {
          resolved = true
          resolve()
        }

        clearTimeout(decrementInterval)
      }, movingTime)

      fetchUrl('move', nextPosition)
        .then(json => {
          dispatch(debugHide())
          if (json.error && json.error.closed) {
            dispatch(setStatusAreaMsg(json.error.closed))
          }

          dispatch(initMove())
          dispatch(moved(json))

          if (json?.mapData?.currentArea && json?.mapData?.currentArea !== mapData.currentArea) {
            clearTimeout(areaTimeout)
            dispatch(setEnteringAreaMessage(json?.mapData?.currentArea))
            areaTimeout = setTimeout(() => {
              dispatch(removeEnteringAreaMessage())
            }, AREA_TOOLTIP_DISPLAYING_DURATION)
          }

          if (!resolved && getState().christmastown.mapData.movingTimeOut <= 0) {
            resolved = true
            resolve()
          }
        })
        .catch(e => {
          console.error('Error: ', e)
          dispatch(movedError())
          // dispatch(fetchMapData()) // I don't think that we need debugShow() here, because of a recursive logic.
        })
    }
  })

export const showMoving = (nextPosition, direction, movingTime = MOVING_TIME_DEFAULT, continuously = false) => ({
  type: a.SHOW_MOVING,
  nextPosition,
  direction,
  movingTime,
  continuously
})

export const moved = json => ({
  type: a.MOVED,
  json
})

export const decrementMovingTimeOut = () => ({
  type: a.DECREMENT_MOVING_TIMEOUT
})

export const setMovingTime = time => ({
  type: a.SET_MOVING_TIME,
  time
})

export const updateUserPosition = (user, options = {}) => ({
  type: a.UPDATE_USER_POSITION,
  user,
  options,
  meta: {
    throttle: typeof options.throttle !== 'undefined' ? options.throttle : false
  }
})

export const removeUser = user => ({
  type: a.REMOVE_USER,
  user
})

export const setUserField = (field, value, user = null) => ({
  type: a.SET_USER_FIELD,
  field,
  value,
  user
})

export const setMapPosition = (x, y) => ({
  type: a.SET_MAP_POSITION,
  x,
  y
})

export const setCellEvent = cellEvent => ({
  type: a.SET_CELL_EVENT,
  cellEvent
})

export const quitCellEvent = () => ({
  type: a.QUIT_CELL_EVENT
})

export const openChest = () => (dispatch, getState) => {
  const { canOpen, chestOpeningStart } = getState().christmastown.statusArea.cellEvent

  if (!canOpen || chestOpeningStart) {
    return
  }

  dispatch(startOpeningChest())
  fetchUrl('openChest')
    .then(json => dispatch(chestOpened(json)))
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}

export const startOpeningChest = () => ({
  type: a.START_OPENING_CHEST
})

export const chestOpened = json => ({
  type: a.CHEST_OPENED,
  json
})

export const chestOpenedBySomebody = json => ({
  type: a.CHEST_OPENED_BY_SOMEBODY,
  json
})

export const setStatusAreaMsg = msg => ({
  type: a.SET_STATUS_AREA_MSG,
  msg
})

export const devToggler = () => ({
  type: a.DEV_TOGGLER
})

export const setUserPosition = (x, y) => (dispatch, getState) => {
  const { mapWidth, mapHeight } = getState().christmastown.mapData.mapSize

  const intX = parseInt(x, 10)

  const intY = parseInt(y, 10)

  let validX = USER_POSITION_MIN_X

  let validY = USER_POSITION_MIN_Y

  const userPositionMaxYHalf = mapHeight / CELL_SIZE / 2

  if (intX && intX >= USER_POSITION_MIN_X) {
    validX = intX <= mapWidth / CELL_SIZE ? intX : mapWidth / CELL_SIZE
  }

  if (intY) {
    validY = intY
    if (Math.abs(intY) > userPositionMaxYHalf) {
      validY = intY > 0 ? userPositionMaxYHalf : -userPositionMaxYHalf
    }
  }

  const nextPosition = {
    x: validX === mapWidth / CELL_SIZE ? mapWidth / CELL_SIZE - MAP_VIEWPORT_CELLS_STOCK : validX,
    y: validY === -userPositionMaxYHalf ? -userPositionMaxYHalf + MAP_VIEWPORT_CELLS_STOCK : validY
  }

  dispatch(startTeleportingUser())
  fetchUrl('jumpToCell', nextPosition)
    .then(json => dispatch(userTeleported(json)))
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })
}

export const startTeleportingUser = () => ({
  type: a.START_SET_USER_POSITION
})

export const userTeleported = json => ({
  type: a.SET_USER_POSITION,
  json
})

export const setCredetials = credentials => ({
  type: a.SET_CREDETIALS,
  credentials
})

/*
 MINIGAMES RELATED ACTIONS
*/
export const loadGame = gameName => ({
  type: a.LOAD_GAME,
  gameName
})

export const gameLoaded = gameName => ({
  type: a.GAME_LOADED,
  gameName
})

export const getEndResult = data => dispatch =>
  fetchUrl('miniGameAction', data)
    .then(json => dispatch(setGameEndResult(json)))
    .catch(error => {
      dispatch(debugShow(getErrorMessage(error)))
    })

export const setGameEndResult = json => ({
  type: a.GAME_END_RESULT_LOADED,
  json
})

export const setGameTitle = ({ gameTitle }) => ({
  type: a.SET_GAME_TITLE,
  gameTitle
})

export const setGameControls = ({ gameControls }) => ({
  type: a.SET_GAME_CONTROLS,
  gameControls
})

export const acceptGame = () => ({
  type: a.ACCEPT_GAME
})

export const leaveGame = () => ({
  type: a.LEAVE_GAME
})

export const endGame = () => ({
  type: a.END_GAME
})

export const blockMap = () => ({
  type: a.BLOCK_MAP
})

export const unblockMap = () => ({
  type: a.UNBLOCK_MAP
})

export const setMessageAreaImageUrl = messageAreaImageUrl => ({
  type: a.SET_MESSAGE_AREA_IMAGE_URL,
  messageAreaImageUrl
})

export const teleport = (position = null) => (dispatch, getState) => {
  return new Promise(resolve => {
    const duration = MOVING_TIME_DEFAULT * 2

    const timeStart = Date.now()

    position = position || getState().christmastown.statusArea.cellEvent.position

    if (!position) return

    dispatch(fillScreenWithColor('white'))
    setTimeout(() => {
      dispatch({ type: a.TELEPORT, position })
      dispatch(fetchMapData()).then(() => {
        const timeDiff = Date.now() - timeStart

        if (timeDiff >= duration) {
          dispatch(fillScreenWithColor('transparent'))
          resolve()
        } else {
          setTimeout(() => {
            dispatch(fillScreenWithColor('transparent'))
            resolve()
          }, duration - timeDiff)
        }
      })
    }, 750)
  })
}

export const hospitalize = user => ({
  type: a.HOSPITALIZE,
  user
})

export const leave = user => ({
  type: a.LEAVE,
  user
})

export const fillScreenWithColor = color => dispatch =>
  new Promise(resolve => {
    dispatch({ type: a.FILL_SCREEN_WITH_COLOR, color })
    setTimeout(() => {
      resolve()
    }, FILL_SCREEN_DURATION)
  })

export const setStatusAreaItem = item => ({
  type: a.SET_STATUS_AREA_ITEM,
  item
})

export const setUserGesture = (gesture, userID) => ({
  type: a.SET_USER_GESTURE,
  payload: { gesture, userID },
  meta: {
    throttle: true
  }
})

export const makeGesture = gesture => (dispatch, getState) => {
  const { user } = getState().christmastown.mapData

  dispatch(setUserGesture(gesture, user.user_id))
  fetchUrl('makeGesture', { gesture }).catch(error => {
    console.log(error)
    dispatch(debugShow(getErrorMessage(error)))
  })
}

export const initMove = () => ({ type: a.MOVE_INITED })
