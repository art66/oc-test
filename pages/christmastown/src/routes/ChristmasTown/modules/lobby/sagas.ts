import { put, select } from 'redux-saga/effects'

import { fetchUrl, getErrorMessage } from '../../utils/index'
import { getLazyConfines, getStore } from './helpers'
import { debugShow } from '../../../../controller/actions'
import { initFinish, lazyMapsLoadFinished, lazyMapsLoad } from './actions'
import lazyLoadScrollChecker from '@torn/shared/utils/lazyLoadScrollChecker'

const LOBBY_DATA_PARAMS = 'getLobbyData'

function* loadExtraLobbyDataOnceBottomHitAfterMount() {
  const { isBottomHitOnView } = lazyLoadScrollChecker()
  const {
    lobby: { settings }
  } = yield getStore(select)

  const { inProgress, lazyLoad } = settings
  const { noMoreData, inProgress: lazyLoadInProgress } = lazyLoad

  if (isBottomHitOnView && !inProgress && !lazyLoadInProgress && !noMoreData) {
    yield put(lazyMapsLoad())
  }
}

function* fetchLobbyData() {
  const { start, limit, mapIDs } = yield getLazyConfines(select)

  try {
    const payload = yield fetchUrl(LOBBY_DATA_PARAMS, { start, limit, mapIDs })

    yield put(initFinish(payload, { start, limit }))
  } catch (e) {
    yield put(debugShow(getErrorMessage(e)))
  }

  yield loadExtraLobbyDataOnceBottomHitAfterMount()
}

function* lazyMapLoaded() {
  try {
    yield fetchLobbyData()

    yield put(lazyMapsLoadFinished())
  } catch (e) {
    yield put(debugShow(getErrorMessage(e)))
  }
}

export { fetchLobbyData, lazyMapLoaded }
