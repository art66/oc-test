import { IRow, IState } from '../../interfaces/ILobby'

export const MOCK_DATA_ROWS: IRow[] = [
  // {
  //   mapID: '5d95d7f3e779898451443d54',
  //   mapName: 'qasdsad1',
  //   createdAt: 1566465464,
  //   author: {
  //     userID: 2114355,
  //     username: 'svyat770',
  //     userImageUrl: 'https://awardimages.torn.com/2066105-99767-large.png',
  //     factionID: 123123,
  //     factionTag: 'BoW',
  //     factionTagImageUrl: 'https://factiontags.torn.com/42113-72292.png',
  //     lastActionTimestamp: 1570625843,
  //     onlineStatus: 'online'
  //   },
  //   playersOnline: 25,
  //   rating: 53.6,
  //   votes: {
  //     5: 300,
  //     4: 36,
  //     3: 50,
  //     2: 2225,
  //     1: 1125
  //   },
  //   joinLink: '/christmas_town.php?q=joinToMap&mapId=5d95d7f3e779898451443d54'
  // },
  // {
  //   mapID: '5d95d7f3e779898451443d55',
  //   mapName: 'Test 1',
  //   createdAt: 1566465465,
  //   author: {
  //     userID: 2114355,
  //     username: 'Pro_er]_=_[',
  //     userImageUrl: 'https://awardimages.torn.com/892956-9532-large.png',
  //     factionID: 123123,
  //     factionTag: 'EE',
  //     factionTagImageUrl: 'https://factiontags.torn.com/8400-79822.png',
  //     lastActionTimestamp: 1570625843,
  //     onlineStatus: 'offline'
  //   },
  //   playersOnline: 678,
  //   rating: 32,
  //   votes: {
  //     5: 300,
  //     4: 36,
  //     3: 1250,
  //     2: 25,
  //     1: 3125
  //   },
  //   joinLink: '/christmas_town.php?q=joinToMap&mapId=5d95d7f3e779898451443d55'
  // },
  // {
  //   mapID: '5d95d7f3e779898451443d56',
  //   mapName: 'svyat_Test',
  //   createdAt: 1566465466,
  //   author: {
  //     userID: 2114355,
  //     username: 'Alexandro',
  //     userImageUrl: 'https://awardimages.torn.com/483201-9393-large.png',
  //     factionID: 123123,
  //     factionTag: 'Tik',
  //     factionTagImageUrl: 'https://factiontags.torn.com/11747-40753.png',
  //     lastActionTimestamp: 1570625843,
  //     onlineStatus: 'offline'
  //   },
  //   playersOnline: 12323,
  //   rating: 15,
  //   votes: {
  //     5: 3300,
  //     4: 36,
  //     3: 50,
  //     2: 1225,
  //     1: 25
  //   },
  //   joinLink: '/christmas_town.php?q=joinToMap&mapId=5d95d7f3e779898451443d56'
  // },
  // {
  //   mapID: '5d95d7f3e779898451443d57',
  //   mapName: 'xhfghfg sdg ',
  //   createdAt: 1566465467,
  //   author: {
  //     userID: 2114355,
  //     username: 'Artem',
  //     userImageUrl: 'https://awardimages.torn.com/573747-77550-large.png',
  //     factionID: 123123,
  //     factionTag: 'WoW',
  //     factionTagImageUrl: 'https://factiontags.torn.com/6834-11719.png',
  //     lastActionTimestamp: 1570625843,
  //     onlineStatus: 'idle'
  //   },
  //   playersOnline: 25,
  //   rating: 68,
  //   votes: {
  //     5: 5300,
  //     4: 36,
  //     3: 50,
  //     2: 25,
  //     1: 25
  //   },
  //   joinLink: '/christmas_town.php?q=joinToMap&mapId=5d95d7f3e779898451443d57'
  // },
  // {
  //   mapID: '5d95d7f3e779898451443d58',
  //   mapName: '1221414',
  //   createdAt: 1566465468,
  //   author: {
  //     userID: 2114355,
  //     username: 'tomilenko',
  //     userImageUrl: 'https://awardimages.torn.com/2066105-99767-large.png',
  //     factionID: 123123,
  //     factionTag: 'oOO',
  //     factionTagImageUrl: 'https://factiontags.torn.com/42113-72292.png',
  //     lastActionTimestamp: 1570625843,
  //     onlineStatus: 'online'
  //   },
  //   playersOnline: 0,
  //   rating: 2.2,
  //   votes: {
  //     5: 1775,
  //     4: 1136,
  //     3: 2375,
  //     2: 125,
  //     1: 25
  //   },
  //   joinLink: '/christmas_town.php?q=joinToMap&mapId=5d95d7f3e779898451443d58'
  // }
]

export const MOCK_DATA: IState = {
  settings: {
    userActiveMapID: undefined,
    showImages: true,
    inProgress: false,
    lazyLoad: {
      noMoreData: false,
      inProgress: false,
      start: -10,
      limit: 10
    }
  },
  publishedMapsList: MOCK_DATA_ROWS
}
