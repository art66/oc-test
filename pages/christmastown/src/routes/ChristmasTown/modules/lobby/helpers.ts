import { INCREASE_LOADED_MAPS_COUNTER } from './constants'

export const findMapByID = (mapsList: any[], currentMaID: string) =>
  mapsList.some(incomingMap => incomingMap.mapID === currentMaID)

export function* getStore(select: Function) {
  const state = yield select(store => store)

  return state
}

export function* getLazyConfines(select: Function) {
  const { lobby } = yield getStore(select)
  const {
    settings: {
      lazyLoad: { start, limit }
    },
    publishedMapsList
  } = lobby
  const mapIDs = publishedMapsList.map(map => map.mapID)

  return {
    start: start + INCREASE_LOADED_MAPS_COUNTER,
    limit,
    mapIDs
  }
}

export const sortMapsByPlayersOnlineDESC = (firstMap, secondMap) => {
  return firstMap.playersOnline <= secondMap.playersOnline ? 1 : -1
}

export const filterMaps = (mapsList, removedMapIDs) => {
  return mapsList.filter(map => {
    return removedMapIDs.every(mapID => mapID !== map.mapID)
  })
}
