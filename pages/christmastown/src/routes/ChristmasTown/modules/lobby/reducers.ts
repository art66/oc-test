import { MOCK_DATA } from './initialState'
import { filterMaps, findMapByID, sortMapsByPlayersOnlineDESC } from './helpers'

import {
  LOBBY_INIT_START,
  LOBBY_INIT_FINISHED,
  LOBBY_MAP_RENAMED,
  LOBBY_MAP_DELETED,
  MAPS_LIST_SYNCHRONIZED,
  HIGHLIGHT_JOINED_MAP,
  LAZY_MAPS_LOAD_ATTEMPT,
  LAZY_MAPS_LOAD_FINISH,
  LOBBY_MAP_ADDED,
  BANNED_MAPS_RECEIVED,
  LOBBY_UPDATE_PRIZE
} from './constants'
import { IState, IRow, TMapID } from '../../interfaces/ILobby'
import {
  IInitData,
  IRenameMap,
  IDefaultAction,
  IDeleteMap,
  IMapsListUpdate,
  IJoinedMapHighlight,
  IMapAdded,
  IBannedMaps,
  IPrizeUpdate
} from './interfaces'

const ACTION_HANDLERS = {
  [LOBBY_INIT_START]: (state: IState) => ({
    ...state,
    settings: {
      ...state.settings,
      inProgress: true
    }
  }),
  [LOBBY_INIT_FINISHED]: (state: IState, action: IInitData) => {
    const filteredMaps = filterMaps(state.publishedMapsList, action.removedMapIDs)

    return {
      ...state,
      settings: {
        ...state.settings,
        ...action.settings,
        userActiveMapID: action.settings.userActiveMapID,
        inProgress: false,
        lazyLoad: {
          ...state.settings.lazyLoad,
          noMoreData: !action.publishedMapsList || action.publishedMapsList.length === 0,
          start: action.start,
          limit: action.limit
        }
      },
      publishedMapsList: [...filteredMaps, ...action.publishedMapsList].sort(sortMapsByPlayersOnlineDESC)
    }
  },
  [LAZY_MAPS_LOAD_ATTEMPT]: (state: IState) => ({
    ...state,
    settings: {
      ...state.settings,
      lazyLoad: {
        ...state.settings.lazyLoad,
        inProgress: true
      }
    }
  }),
  [LAZY_MAPS_LOAD_FINISH]: (state: IState) => ({
    ...state,
    settings: {
      ...state.settings,
      lazyLoad: {
        ...state.settings.lazyLoad,
        inProgress: false
      }
    }
  }),
  [LOBBY_MAP_ADDED]: (state: IState, action: IMapAdded) => ({
    ...state,
    publishedMapsList: [...state.publishedMapsList, action.newMap]
  }),
  [LOBBY_MAP_RENAMED]: (state: IState, action: IRenameMap) => ({
    ...state,
    publishedMapsList: state.publishedMapsList.map((map: IRow) => {
      return map.mapID === action.mapID ?
        {
          ...map,
          mapName: action.newName
        } :
        map
    })
  }),
  [LOBBY_MAP_DELETED]: (state: IState, action: IDeleteMap) => ({
    ...state,
    publishedMapsList: state.publishedMapsList.filter((map: IRow) => map.mapID !== action.mapID)
  }),
  [MAPS_LIST_SYNCHRONIZED]: (state: IState, action: IMapsListUpdate) => {
    return {
      ...state,
      publishedMapsList: state.publishedMapsList.map((currentMap: IRow) => {
        const updateMap = (currentMapID: TMapID) => {
          const mapToUpdate = action.mapsList.find(map => map.mapID === currentMapID)

          return {
            ...currentMap,
            lastEdit: mapToUpdate.lastEdit,
            playersOnline: mapToUpdate.playersOnline,
            author: {
              ...currentMap.author,
              onlineStatus: mapToUpdate.author.onlineStatus
            },
            rating: mapToUpdate.rating,
            votes: mapToUpdate.votes
          }
        }

        return findMapByID(action.mapsList, currentMap.mapID) ? updateMap(currentMap.mapID) : currentMap
      })
    }
  },
  [HIGHLIGHT_JOINED_MAP]: (state: IState, action: IJoinedMapHighlight) => {
    return {
      ...state,
      settings: {
        ...state.settings,
        userActiveMapID: action.activeMapID
      }
    }
  },
  [BANNED_MAPS_RECEIVED]: (state: IState, action: IBannedMaps) => {
    // const stagedMaps = []

    const { publishedMapsList } = state
    const { bannedMaps } = action

    // can handle object overlap
    // publishedMapsList.forEach((currentMap: IRow) => {
    //   const isMapNotBanned = !bannedMaps.some((bannedMap: IRow) => currentMap.mapID === bannedMap.mapID)
    //   const isMapNotInStagedList = !stagedMaps.some((cleanMap: IRow) => cleanMap.mapID === currentMap.mapID)

    //   isMapNotBanned && isMapNotInStagedList && stagedMaps.push(currentMap)
    // })

    // more cleave and performance way of iteration big array of data
    // Thanks for Sergey for the pure array response, consisted of numbers
    // publishedMapsList.forEach(currentMap => !bannedMaps.includes(currentMap) && stagedMaps.push(currentMap))

    return {
      ...state,
      publishedMapsList: publishedMapsList.filter(currentMap => !bannedMaps.includes(currentMap.mapID)) // up perfom
    }
  },
  [LOBBY_UPDATE_PRIZE]: (state: IState, action: IPrizeUpdate) => {
    return {
      ...state,
      publishedMapsList: state.publishedMapsList.map(map => {
        if (map.mapID === action.mapID) {
          return {
            ...map,
            prizeIconSrc: action.prizeIconSrc
          }
        }

        return map
      })
    }
  }
}

const reducer = (state: IState = MOCK_DATA, action: IDefaultAction) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
