import {
  LOBBY_INIT_START,
  LOBBY_INIT_FINISHED,
  LOBBY_MAP_ADDED,
  LOBBY_MAP_RENAMED,
  LOBBY_MAP_DELETED,
  MAPS_LIST_SYNCHRONIZED,
  HIGHLIGHT_JOINED_MAP,
  LAZY_MAPS_LOAD_ATTEMPT,
  LAZY_MAPS_LOAD_FINISH,
  BANNED_MAPS_RECEIVED,
  LOBBY_UPDATE_PRIZE
} from './constants'
import {
  IInitData,
  IMapAdded,
  IRenameMap,
  IDeleteMap,
  IMapsListUpdate,
  IJoinedMapHighlight,
  ILazyData,
  IBannedMaps,
  IPrizeUpdate
} from './interfaces'
import { IType } from '../../interfaces/ILobby'

export const initStart = (): IType => ({
  type: LOBBY_INIT_START
})

export const initFinish = ({ settings, publishedMapsList, removedMapIDs }: IInitData, { start, limit }: ILazyData) => ({
  type: LOBBY_INIT_FINISHED,
  start,
  limit,
  settings,
  publishedMapsList,
  removedMapIDs
})

export const addMap = ({ ...newMap }: IMapAdded) => ({
  type: LOBBY_MAP_ADDED,
  newMap
})

export const renameMap = ({ mapID, newName }: IRenameMap) => ({
  type: LOBBY_MAP_RENAMED,
  mapID,
  newName
})

export const removeMap = ({ mapID }: IDeleteMap) => ({
  type: LOBBY_MAP_DELETED,
  mapID
})

export const synchronizeMapsData = (mapsList: IMapsListUpdate) => ({
  type: MAPS_LIST_SYNCHRONIZED,
  mapsList
})

export const highlightJoinedMap = ({ activeMapID }): IJoinedMapHighlight => ({
  activeMapID,
  type: HIGHLIGHT_JOINED_MAP
})

export const lazyMapsLoad = (): IType => ({
  type: LAZY_MAPS_LOAD_ATTEMPT
})

export const lazyMapsLoadFinished = (): IType => ({
  type: LAZY_MAPS_LOAD_FINISH
})

export const removeBannedMaps = ({ mapList }): IBannedMaps => ({
  bannedMaps: mapList,
  type: BANNED_MAPS_RECEIVED
})

export const updatePrize = ({ mapID, prizeIconSrc }): IPrizeUpdate => ({
  mapID,
  prizeIconSrc,
  type: LOBBY_UPDATE_PRIZE
})
