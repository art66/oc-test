import { ISettings, IRow, TType, TMapID, TOnlineStatus, TLastEdit } from '../../interfaces/ILobby'
import { IProps as IRatingProps } from '../../components/Lobby/Rating/interfaces'
import { IProps as IVotes } from '../../components/Lobby/Votes/interfaces'
import { IProps as IPlayersProps } from '../../components/Lobby/Players/interfaces'
import { IPrize } from '../../components/Lobby/Prize/interfaces'

export interface ILazyData {
  start: number
  limit: number
}

export interface IInitData extends ILazyData {
  settings: ISettings
  publishedMapsList: IRow[]
  type: TType
  removedMapIDs: string[]
}

export interface IMapAdded {
  newMap: IRow
  type: TType
}

export interface IRenameMap {
  mapID: TMapID
  newName: string
  type: TType
}

export interface IDeleteMap {
  mapID: TMapID
  type: TType
}

export interface IDefaultAction {
  type: TType
}

export interface IMapsListUpdate {
  mapsList: {
    mapID: TMapID
    lastEdit: TLastEdit
    playersOnline: IPlayersProps
    author: {
      onlineStatus: TOnlineStatus
    }
    rating: IRatingProps
    votes: IVotes
  }[]
  type: TType
}

export interface IJoinedMapHighlight {
  activeMapID: TMapID
  type: TType
}

export interface IBannedMaps {
  bannedMaps: string[]
  type: TType
}

export interface IPrizeUpdate {
  mapID: TMapID
  prizeIconSrc: IPrize
  type: TType
}
