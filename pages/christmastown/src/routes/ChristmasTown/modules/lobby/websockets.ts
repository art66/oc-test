/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import {
  addMap,
  renameMap,
  removeMap,
  synchronizeMapsData,
  highlightJoinedMap,
  removeBannedMaps,
  updatePrize
} from './actions'

let handler,
  handlerUserLobby

function initWS(dispatch: any) {
  // @ts-ignore global Websocket & Centrifuge handler!!!
  handler = new WebsocketHandler('cTownLobby', { channel: 'cTownLobby' })
  // @ts-ignore global Websocket & Centrifuge handler!!!
  handlerUserLobby = new WebsocketHandler('cTownLobby')

  handler.setActions({
    MapAdded: (payload: any) => {
      dispatch(addMap(payload.data))
    },
    MapRenamed: (payload: any) => {
      dispatch(renameMap(payload.data))
    },
    MapRemoved: (payload: any) => {
      dispatch(removeMap(payload.data))
    },
    MapsDataSynchronizeMessage: (payload: any) => {
      dispatch(synchronizeMapsData(payload.data))
    },
    ActiveHUDChanged: (payload: any) => {
      dispatch(highlightJoinedMap(payload.data))
    },
    MapsBanned: (payload: any) => {
      dispatch(removeBannedMaps(payload.data))
    }
  })

  handlerUserLobby.setActions({
    updateLobbyMapPrize: (payload: any) => {
      dispatch(updatePrize(payload.actionData))
    }
  })
}

export function unsubscribeLobbyWS() {
  handler.removeActions([
    'MapAdded',
    'MapRenamed',
    'MapRemoved',
    'MapsDataSynchronizeMessage',
    'ActiveHUDChanged',
    'MapsBanned'
  ])
  handlerUserLobby.removeActions(['updateLobbyMapPrize'])
}

export default initWS
