/* eslint-disable prefer-arrow/prefer-arrow-functions */
import {
  setCredetials,
  updateUserPosition,
  chestOpenedBySomebody,
  hospitalize,
  removeUser,
  leave,
  setCellEvent,
  teleport,
  setUserGesture,
  setMessageAreaImageUrl,
  loadGame,
  leaveGame
} from './index'
import { CELL_EVENT_TYPE_TELEPORT } from '../constants'

import { updateGateController, setUserMessage, updateUserInventory } from '../actions'

let handler

function initWS(dispatch: any) {
  // @ts-ignore global Websocket handler
  handler = new WebsocketHandler('christmasTown')
  const credentials = handler.getCredentials()

  dispatch(setCredetials(credentials))

  handler.setActions({
    move: (data: any) => {
      dispatch(updateUserPosition(data.user))
    },
    openChest: (data: any) => {
      dispatch(chestOpenedBySomebody(data))
    },
    firstAid: (data: any) => {
      const hospitalizingAnimationDuration = 1000

      dispatch(hospitalize(data.user))
      setTimeout(() => dispatch(removeUser(data.user)), hospitalizingAnimationDuration)
    },
    leave: (data: any) => {
      dispatch(leave(data.user))
    },
    cellEvent: (data: any) => {
      dispatch(setCellEvent(data.cellEvent))
      dispatch(setMessageAreaImageUrl(data.messageAreaImageUrl))

      if (data.cellEvent.type === CELL_EVENT_TYPE_TELEPORT) {
        dispatch(leaveGame())
        dispatch(loadGame(data.cellEvent.miniGameType))
      }
    },
    teleport: (data: any) => {
      dispatch(teleport(data.position))
    },
    gatesOpened: (data: any) => {
      try {
        data.cellEvent.gates.forEach((gate: any) => {
          dispatch(updateGateController(gate.position, gate))
        })
      } catch (e) {
        console.error(e)
      }
    },
    gesture: (data: any) => {
      const { gesture, userID } = data.actionData

      if (parseInt(credentials.userID, 10) === parseInt(userID, 10)) {
        return
      }

      try {
        dispatch(setUserGesture(gesture, userID))
      } catch (error) {
        console.log(error)
      }
    },
    chatMessage: data => {
      const { message, userID } = data.actionData

      dispatch(setUserMessage(message, userID))
    },
    updateInventory: data => dispatch(updateUserInventory(data.actionData))
  })
}

export function unsubscribeCTWS() {
  handler.removeActions([
    'move',
    'openChest',
    'firstAid',
    'leave',
    'cellEvent',
    'teleport',
    'gatesOpened',
    'gesture',
    'chatMessage',
    'updateInventory'
  ])
}

export default initWS
