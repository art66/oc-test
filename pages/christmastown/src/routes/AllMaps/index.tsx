import React from 'react'
import Loadable from 'react-loadable'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/helpers/rootReducer'
import { loadData } from './modules/actions'
import reducer from './modules/reducers'

import SkeletonFull from './components/Skeletons/Full'

const Preloader = () => <SkeletonFull />

const AllMapsRoute = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const AllMaps = await import(/* webpackChunkName: "allMaps" */ './layout/index')

    injectReducer(rootStore, { reducer, key: 'allMaps' })
    rootStore.dispatch(loadData())

    return AllMaps
  },
  loading: Preloader
})

export default AllMapsRoute
