import { IMaps, IState, ISettings, TProgress, TMapID } from './IState'

interface IAllMaps extends ISettings {
  maps: IMaps
  inProgress: TProgress
  adminReleaseInProgress: TProgress
  activeRowMapID: TMapID
}

export interface IReduxState extends IState {
  allMaps: IAllMaps
}
