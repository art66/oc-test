import { ISettings, TMap, TMapID } from './IState'

// ----------------
// ACTION/REDUCERS
// ----------------
export interface IType {
  type: string
}

export interface ILoadData extends IType {}
export interface ILoadDataSaved extends IType {
  payload: {
    settings: ISettings
    allMaps: {
      list: TMap[]
    }
  }
}

export interface IPollData extends IType {}

export interface ISetBanStatusAttempt {
  mapID: TMapID
  authorID: number
  isBanned: boolean
}

export interface ISetBanStatusDone extends IType {
  authorID: number
  isBanned: boolean
}
