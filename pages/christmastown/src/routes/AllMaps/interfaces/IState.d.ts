export type TProgress = boolean

export type TMapElements = {
  objects: number
  parameters: number
  areas: number
}

export type TMapManage = {
  joinLink: string
}

export type TMapID = string

export interface ISettings {
  settings: {
    restrictLayout: boolean
    activeRowMapID: TMapID
    userActiveMapID: TMapID
    adminReleaseInProgress: boolean
  }
}

export type TMap = {
  ID: TMapID
  name: string
  authorId: number
  isBanned: boolean
  lastEdit: string
  createdAt: number
  testers: number
  editors: number
  playersOnline: number
  published: {
    status: boolean
    isBanned: boolean
  }
  rating: number
  manage: TMapManage
  mapElements: TMapElements
}

export type TMapPayload = TMap & { lastEdit: number }

export interface IMaps {
  list: TMap[]
}

export interface IState extends ISettings {
  maps: IMaps
}
