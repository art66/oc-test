import { TMap } from '../interfaces/IState'

const SECTIONS_TO_RENDER = [
  'name',
  'authorId',
  'lastEdit',
  'testers',
  'editors',
  'playersOnline',
  'published',
  'mapElements',
  'rating',
  'manage'
]

const rowSectionNormalizer = (mapSections: TMap) => {
  const sectionsObjToRender = {}

  Object.keys(mapSections).forEach(key => {
    if (SECTIONS_TO_RENDER.includes(key)) {
      sectionsObjToRender[key] = mapSections[key]
    }
  })

  return sectionsObjToRender
}

export default rowSectionNormalizer
