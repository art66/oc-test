# ChristmasTown#/AllMaps - Torn


## 1.9.0
 * Added rows highlighting once user joined some map.

## 1.8.0
 * Added skeletons for AllMaps layout.

## 1.7.0
 * Added placeholder to handle empty rows layout.

## 1.6.0
 * Add sagas async request for data fetching.

## 1.5.0
 * Refactored Rows and Header layout to be able to include the search functional here.

## 1.4.0
 * Created first good Rows layout.

## 1.3.0
 * Added alpha ver of the rows layout.

## 1.2.0
 * Added header layout.

## 1.1.0
 * Updated basic AllMaps state structure.

## 1.0.0
 * Initial commit release.
