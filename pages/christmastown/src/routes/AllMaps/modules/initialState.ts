import { IState } from '../interfaces/IState'
// import getDate from '../../MyMaps/utils/getDate'

const initialState: IState = {
  settings: {
    restrictLayout: false,
    userActiveMapID: undefined,
    activeRowMapID: null,
    adminReleaseInProgress: false
  },
  maps: {
    list: null
    // [
    // {
    //   ID: '5d5a64dde7798914d53ecc2a',
    //   name: 'Awesome map name',
    //   authorId: 2307949,
    //   lastEdit: getDate(1569854489),
    //   createdAt: 1566205149,
    //   testers: 0,
    //   editors: 2,
    //   playersOnline: 1022,
    //   mapElements: {
    //     objects: 42806,
    //     parameters: 9886,
    //     areas: 10144
    //   },
    //   published: true,
    //   rating: 45.35,
    //   manage: {
    //     joinLink: '/christmas_town.php?q=joinToMap&mapID=5d5a64dde7798914d53ecc2a'
    //   }
    // },
    // {
    //   ID: 'adssvcngfhtr3243rgfdvc',
    //   name: 'gagag name',
    //   authorId: 2349,
    //   lastEdit: getDate(1569854419),
    //   createdAt: 1566205149,
    //   testers: 12,
    //   editors: 3,
    //   playersOnline: 15,
    //   mapElements: {
    //     objects: 406,
    //     parameters: 96,
    //     areas: 14
    //   },
    //   published: true,
    //   rating: 25.35,
    //   manage: {
    //     joinLink: '/christmas_town.php?q=joinToMap&mapID=5d5a64dde7798914d53ecc2a'
    //   }
    // },
    // {
    //   ID: '123123123123xzcxv',
    //   name: 'test name',
    //   authorId: 1307949,
    //   lastEdit: getDate(1569852489),
    //   createdAt: 1566205149,
    //   testers: 16,
    //   editors: 22,
    //   playersOnline: 12,
    //   mapElements: {
    //     objects: 2806,
    //     parameters: 886,
    //     areas: 20144
    //   },
    //   published: true,
    //   rating: 15.35,
    //   manage: {
    //     joinLink: '/christmas_town.php?q=joinToMap&mapID=5d5a64dde7798914d53ecc2a'
    //   }
    // },
    // {
    //   ID: '5d5a64dde77984d53ecc2a',
    //   name: '1232_123xxvf',
    //   authorId: 123124,
    //   lastEdit: getDate(1569854389),
    //   createdAt: 1566205149,
    //   testers: 18,
    //   editors: 22,
    //   playersOnline: 123,
    //   mapElements: {
    //     objects: 4286,
    //     parameters: 986,
    //     areas: 1044
    //   },
    //   published: true,
    //   rating: 95.35,
    //   manage: {
    //     joinLink: '/christmas_town.php?q=joinToMap&mapID=5d5a64dde7798914d53ecc2a'
    //   }
    // },
    // {
    //   ID: '5d5a64dde7798914decc2a',
    //   name: 'sfsdsfee',
    //   authorId: 2323,
    //   lastEdit: getDate(1569854489),
    //   createdAt: 1566205149,
    //   testers: 24,
    //   editors: 1,
    //   playersOnline: 0,
    //   mapElements: {
    //     objects: 806,
    //     parameters: 9,
    //     areas: 1014
    //   },
    //   published: false,
    //   rating: 5.35,
    //   manage: {
    //     joinLink: '/christmas_town.php?q=joinToMap&mapID=5d5a64dde7798914d53ecc2a'
    //   }
    // }
    // ]
  }
}

export default initialState
