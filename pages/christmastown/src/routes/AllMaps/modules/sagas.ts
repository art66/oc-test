import { takeLatest, put } from 'redux-saga/effects'
import { debugShow, getRouteHeader } from '../../../controller/actions'
import { LOAD_MAPS_DATA, POOLING_DATA, SET_MAP_BAN_STATUS_ATTEMPT } from '../constants'
import { loadDataSaved, setBanSaved, restrictLayout } from './actions'
import { fetchUrl } from '../../../utils/fetchURL'

function* mainDataSave() {
  try {
    const response = yield fetchUrl('getAllMapsData', null)

    if (!response || response.success === false && response.message && response.message.type === 'error') {
      yield put(debugShow(response && response.message && response.message.text))

      if (response && response.restrictLayout) {
        yield put(restrictLayout())
      }

      return
    }

    yield put(loadDataSaved(response))
  } catch (e) {
    debugShow(e)
    console.error(e)
  } finally {
    yield put(getRouteHeader())
  }
}

function* setBanStatus({ mapID, authorID, isBanned }: any) {
  try {
    const response = yield fetchUrl('setMapBanStatus', { mapID, banStatus: isBanned })

    if (!response || response.success === false && response.message && response.message.type === 'error') {
      yield put(debugShow(response && response.message && response.message.text))

      return
    }

    yield put(setBanSaved(authorID, isBanned))
  } catch (e) {
    debugShow(e)
    console.error(e)
  }
}

export default function* rootSaga() {
  yield takeLatest(SET_MAP_BAN_STATUS_ATTEMPT, setBanStatus)
  yield takeLatest(POOLING_DATA, mainDataSave)
  yield takeLatest(LOAD_MAPS_DATA, mainDataSave)
}
