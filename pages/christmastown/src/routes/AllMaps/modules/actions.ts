import {
  LOAD_MAPS_DATA,
  MAPS_DATA_SAVED,
  POOLING_DATA,
  SET_MAP_BAN_STATUS_ATTEMPT,
  SET_MAP_BAN_STATUS_DONE,
  RESTRICT_LAYOUT
} from '../constants'
import {
  ILoadData,
  ILoadDataSaved,
  IPollData,
  ISetBanStatusAttempt,
  ISetBanStatusDone,
  IType
} from '../interfaces/IModules'

export const loadData = (): ILoadData => ({
  type: LOAD_MAPS_DATA
})

export const loadDataSaved = (payload): ILoadDataSaved => ({
  payload,
  type: MAPS_DATA_SAVED
})

export const poolingStart = (): IPollData => ({
  type: POOLING_DATA
})

export const switchMapPublish = (mapID, authorID, isBanned): ISetBanStatusAttempt & IType => ({
  mapID,
  authorID,
  isBanned,
  type: SET_MAP_BAN_STATUS_ATTEMPT
})

export const setBanSaved = (authorID, isBanned): ISetBanStatusDone => ({
  authorID,
  isBanned,
  type: SET_MAP_BAN_STATUS_DONE
})

export const restrictLayout = (): IType => ({
  type: RESTRICT_LAYOUT
})
