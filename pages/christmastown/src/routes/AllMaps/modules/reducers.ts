import initialState from './initialState'
import {
  LOAD_MAPS_DATA,
  MAPS_DATA_SAVED,
  SET_MAP_BAN_STATUS_ATTEMPT,
  SET_MAP_BAN_STATUS_DONE,
  RESTRICT_LAYOUT
} from '../constants'

import { ILoadDataSaved, IType, ISetBanStatusDone, ISetBanStatusAttempt } from '../interfaces/IModules'
import { IState, TMapPayload, TMap } from '../interfaces/IState'
import getDate from '../../MyMaps/utils/getDate'

const ACTION_HANDLERS = {
  [LOAD_MAPS_DATA]: (state: IState) => ({
    ...state,
    inProgress: true
  }),
  [MAPS_DATA_SAVED]: (state: IState, action: ILoadDataSaved) => ({
    ...state,
    inProgress: false,
    settings: {
      ...state.settings,
      ...action.payload.settings
    },
    maps: {
      ...state.maps,
      list: action.payload.allMaps.list.map((map: TMapPayload) => ({
        ...map,
        published: {
          status: map.published,
          isBanned: map.isBanned
        },
        lastEdit: getDate(map.lastEdit)
      }))
    }
  }),
  [SET_MAP_BAN_STATUS_ATTEMPT]: (state: IState, action: ISetBanStatusAttempt) => ({
    ...state,
    settings: {
      ...state.settings,
      activeRowMapID: action.mapID,
      adminReleaseInProgress: true
    }
  }),
  [SET_MAP_BAN_STATUS_DONE]: (state: IState, action: ISetBanStatusDone) => ({
    ...state,
    settings: {
      ...state.settings,
      adminReleaseInProgress: false
    },
    maps: {
      ...state.maps,
      list: state.maps.list.map((map: TMap) =>
        map.authorId === action.authorID
          ? {
              ...map,
              published: {
                ...map.published,
                isBanned: action.isBanned
              }
            }
          : map
      )
    }
  }),
  [RESTRICT_LAYOUT]: (state: IState) => ({
    ...state,
    settings: {
      ...state.settings,
      restrictLayout: true
    }
  })
}

const reducer = (state: IState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
