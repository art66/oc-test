import React from 'react'

import commonStyles from '../../styles/common.cssmodule.scss'
import styles from './index.cssmodule.scss'

class SkeletonTabs extends React.PureComponent {
  render() {
    const extraEntityClass = `${styles.placeholderSection} ${styles.afterEntity}`

    return (
      <div className={styles.skeletonTabsWrap}>
        <div className={`${commonStyles.section} ${commonStyles.name} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.creator} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.last_edit} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.editors} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.testers} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.players} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.elements} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.release} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.rating} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.join} ${extraEntityClass}`} />
      </div>
    )
  }
}

export default SkeletonTabs
