import React from 'react'

import commonStyles from '../../styles/common.cssmodule.scss'
import styles from './index.cssmodule.scss'

class SkeletonRows extends React.PureComponent {
  _renderSkeletonRow = (row: number) => {
    const extraEntityClass = `${styles.placeholderSection} ${styles.afterEntity}`

    return (
      <div key={row} className={styles.skeletonRow}>
        <div className={`${commonStyles.section} ${commonStyles.name} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.creator} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.last_edit} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.editors} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.testers} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.players} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.elements} ${styles.placeholderSection}`}>
          <span className={`${commonStyles.section} ${styles.subSectionSkeleton} ${extraEntityClass}`} />
          <span className={`${commonStyles.section} ${styles.subSectionSkeleton} ${extraEntityClass}`} />
          <span className={`${commonStyles.section} ${styles.subSectionSkeleton} ${extraEntityClass}`} />
        </div>
        <div className={`${commonStyles.section} ${commonStyles.release} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.rating} ${extraEntityClass}`} />
        <div className={`${commonStyles.section} ${commonStyles.join} ${extraEntityClass}`} />
      </div>
    )
  }

  _renderRows = () => {
    const rowsCount = Array.from(Array(10).keys())

    return rowsCount.map((row: number) => this._renderSkeletonRow(row))
  }

  render() {
    return <div className={styles.skeletonRowsWrap}>{this._renderRows()}</div>
  }
}

export default SkeletonRows
