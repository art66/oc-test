import React from 'react'

import Tabs from './Tabs'
import Rows from './Rows'
import styles from './index.cssmodule.scss'

class SkeletonFull extends React.PureComponent {
  _renderHeader = () => {
    return <div className={styles.headerSkeleton} />
  }

  render() {
    return (
      <div className={styles.skeletonFullWrap}>
        {this._renderHeader()}
        <Tabs />
        <Rows />
      </div>
    )
  }
}

export default SkeletonFull
