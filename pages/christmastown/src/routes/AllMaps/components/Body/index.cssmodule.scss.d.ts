// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'column': string;
  'creator': string;
  'editors': string;
  'elements': string;
  'flexCenter': string;
  'flexCenterStart': string;
  'fontBlue': string;
  'fontCT': string;
  'globalSvgShadow': string;
  'join': string;
  'last_edit': string;
  'name': string;
  'placeholderWrap': string;
  'players': string;
  'rating': string;
  'release': string;
  'rowsContainer': string;
  'section': string;
  'tabSection': string;
  'tabsContainer': string;
  'testers': string;
}
export const cssExports: CssExports;
export default cssExports;
