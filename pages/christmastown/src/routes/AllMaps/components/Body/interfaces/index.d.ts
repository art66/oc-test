import { TMap, TProgress } from '../../../interfaces/IState'

export interface IProps {
  mapsList: TMap[]
  activeUserMapID: string
  inProgress: TProgress
}
