import React from 'react'
import classnames from 'classnames'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'

import Row from './Row'

import { TMap } from '../../interfaces/IState'
import { IProps } from './interfaces'

import { NORMALIZED_SECTIONS_LABELS } from '../../constants'

import styles from './index.cssmodule.scss'
import commonStyles from '../../styles/common.cssmodule.scss'
import SkeletonRows from '../Skeletons/Rows'

class Body extends React.Component<IProps> {
  _classNameHolder = () => {
    return {
      tabClass: classnames(commonStyles.column, styles.tabSection)
    }
  }

  _getTabsList = () => [...Object.keys(NORMALIZED_SECTIONS_LABELS).map(key => NORMALIZED_SECTIONS_LABELS[key])]

  _renderTabs = () => {
    const { tabClass } = this._classNameHolder()
    const tabs = this._getTabsList()

    return tabs.map(tabLabel => {
      return (
        <span key={tabLabel} className={`${tabClass} ${commonStyles[tabLabel]}`}>
          {firstLetterUpper({ value: tabLabel })}
        </span>
      )
    })
  }

  _renderRows = () => {
    const { mapsList = [], activeUserMapID, inProgress } = this.props

    if (inProgress) {
      return <SkeletonRows />
    }

    if (!mapsList || mapsList === null) {
      return <div className={styles.placeholderWrap}>There is no maps yet. Please, try to come back later. :(</div>
    }

    return mapsList.map((map: TMap) => {
      return <Row key={map.ID} map={map} activeUserMapID={activeUserMapID} />
    })
  }

  render() {
    return (
      <div>
        <div className={styles.tabsContainer}>{this._renderTabs()}</div>
        <div className={styles.rowsContainer}>{this._renderRows()}</div>
      </div>
    )
  }
}

export default Body
