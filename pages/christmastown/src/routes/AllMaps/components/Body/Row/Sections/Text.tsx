import React from 'react'

import { IProps } from './interfaces/IText'
import { NORMALIZED_SECTIONS_LABELS } from '../../../../constants'

import styles from './styles/text.cssmodule.scss'
import rowStyles from '../index.cssmodule.scss'

class TextSection extends React.PureComponent<IProps> {
  render() {
    const { sectionValue, className } = this.props

    const specClass = NORMALIZED_SECTIONS_LABELS[className]
    const greyText = (specClass === 'last_edit' && styles.greyText) || ''
    const textClass = `${rowStyles.sectionText} ${greyText}`

    return <span className={textClass}>{sectionValue}</span>
  }
}

export default TextSection
