import React, { PureComponent } from 'react'

import RatingBox from '../../../../../../components/RatingBox'

import { IProps } from './interfaces/IRating'

class RatingSection extends PureComponent<IProps> {
  render() {
    const {
      sectionValue,
      extraData: { isRowHovered, isMapJoinedActive }
    } = this.props

    return <RatingBox data={sectionValue} isRowHovered={isRowHovered} isMapJoinedActive={isMapJoinedActive} />
  }
}

export default RatingSection
