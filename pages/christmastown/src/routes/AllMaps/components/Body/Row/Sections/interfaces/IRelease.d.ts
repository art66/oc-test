import { ISetBanStatusAttempt } from '../../../../../interfaces/IModules'
import { TMapID } from '../../../../../interfaces/IState'

export interface IProps {
  mapID: TMapID
  authorID: number
  sectionValue: {
    status: boolean
    isBanned: boolean
  }
  inProgress: boolean
  activeRowMapID: TMapID
  setMapPublishStatus: ({ mapID, authorID, isBanned }: ISetBanStatusAttempt) => void
}
