import React from 'react'
import classnames from 'classnames'

import Text from './Sections/Text'
import Elements from './Sections/Elements'
import Release from '../../../containers/Sections/Release'
import Rating from './Sections/Rating'
import Join from './Sections/Join'

import { IProps, TKey, TSections } from './interfaces'

import styles from './index.cssmodule.scss'
import commonStyles from '../../../styles/common.cssmodule.scss'

import rowSectionNormalizer from '../../../utils/rowSectionNormalizer'
import { NORMALIZED_SECTIONS_LABELS } from '../../../constants'
import { TMap } from '../../../interfaces/IState'

const SECTION_TYPES_ADAPTER = {
  name: Text,
  creator: Text,
  last_edit: Text,
  editors: Text,
  testers: Text,
  players: Text,
  elements: Elements,
  release: Release,
  rating: Rating,
  join: Join
}

interface ISectionData {
  section: TSections
  name: TKey
  mapID: string
  authorID: number
}

class Row extends React.Component<IProps, any> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      rowIDHovered: null
    }
  }

  _classNamesHolder = (currentMap?: string) => {
    const { activeUserMapID } = this.props

    return {
      rowClass: classnames({
        [styles.rowActiveMapJoined]: activeUserMapID === currentMap,
        [styles.rowWrap]: true
      })
    }
  }

  _handlerOver = ({ target }: any) => {
    const row = target && target.closest('[class^="rowWrap"]')
    const rowID = (row && row.dataset.id) || null

    if (!row || !rowID) {
      return
    }

    this.setState({
      rowIDHovered: rowID
    })
  }

  _handlerLeave = () => {
    this.setState({
      rowIDHovered: null
    })
  }

  _getSection = ({ section, name, mapID, authorID }: ISectionData) => {
    const { rowIDHovered } = this.state
    const { activeUserMapID } = this.props

    const normalizedSectionName = NORMALIZED_SECTIONS_LABELS[name]
    const SectionComponent = SECTION_TYPES_ADAPTER[normalizedSectionName]
    let extraData = {}

    if (name === 'rating') {
      extraData = {
        isRowHovered: mapID === rowIDHovered,
        isMapJoinedActive: mapID === activeUserMapID
      }
    }

    return (
      <div
        key={name}
        className={`${commonStyles.column} ${styles.section} ${commonStyles[NORMALIZED_SECTIONS_LABELS[name]]}`}
      >
        <SectionComponent
          sectionValue={section}
          className={name}
          extraData={extraData}
          mapID={mapID}
          authorID={authorID}
        />
      </div>
    )
  }

  _renderSections = (map: TMap) => {
    const sectionsToRender = rowSectionNormalizer(map)

    return Object.keys(sectionsToRender).map((key: TKey) => {
      return this._getSection({ section: map[key], name: key, mapID: map.ID, authorID: map.authorId })
    })
  }

  render() {
    const { map } = this.props
    const { rowClass } = this._classNamesHolder(map.ID)

    return (
      <div
        key={map.ID}
        data-id={map.ID}
        className={rowClass}
        onMouseEnter={this._handlerOver}
        onMouseLeave={this._handlerLeave}
      >
        {this._renderSections(map)}
      </div>
    )
  }
}

export default Row
