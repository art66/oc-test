import React from 'react'

import { IProps } from './interfaces/IElements'

import styles from './styles/elements.cssmodule.scss'
import rowStyles from '../index.cssmodule.scss'

class ElementsSection extends React.PureComponent<IProps> {
  render() {
    const {
      sectionValue: { objects, parameters, areas }
    } = this.props

    return (
      <div className={styles.elementsWrap}>
        <span className={`${styles.objects} ${rowStyles.sectionText}`}>O: {objects}</span>
        <span className={`${styles.parameters} ${rowStyles.sectionText}`}>P: {parameters}</span>
        <span className={`${styles.areas} ${rowStyles.sectionText}`}>A: {areas}</span>
      </div>
    )
  }
}

export default ElementsSection
