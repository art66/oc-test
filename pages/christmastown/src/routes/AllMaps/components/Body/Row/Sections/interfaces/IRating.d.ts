export interface IProps {
  sectionValue: number
  extraData: {
    isRowHovered: boolean
    isMapJoinedActive: boolean
  }
}
