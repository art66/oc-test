import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'

import { IProps } from './interfaces/IJoin'
import { DARK_THEME } from '../../../../../../constants'

import styles from './styles/join.cssmodule.scss'
import rowStyles from '../index.cssmodule.scss'

export const BLUE_HOVERED = 'BLUE_HOVERED'
export const BLUE_DIMMED = 'BLUE_DIMMED'
export const BLUE_HOVERED_DARK = 'BLUE_HOVERED_DARK'
export const BLUE_DIMMED_DARK = 'BLUE_DIMMED_DARK'

class JoinSection extends React.Component<IProps & TWithThemeInjectedProps> {
  render() {
    const { sectionValue, theme } = this.props
    const isDarkMode = theme === DARK_THEME

    return (
      <div className={styles.joinWrap}>
        <span className={`${rowStyles.sectionText} ${styles.labelText}`}>Join</span>
        <a className={styles.joinLink} href={sectionValue.joinLink}>
          <SVGIconGenerator
            iconName='Join'
            fill={{ name: isDarkMode ? BLUE_DIMMED_DARK : BLUE_DIMMED }}
            dimensions={{ width: 15, height: 15, viewbox: '0 0 24 24' }}
            onHover={{ active: true, fill: { name: isDarkMode ? BLUE_HOVERED_DARK : BLUE_HOVERED } }}
          />
        </a>
      </div>
    )
  }
}

export default withTheme(JoinSection)
