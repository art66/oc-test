import React from 'react'

import Release from '../../../../../../components/Switcher'

import { IProps } from './interfaces/IRelease'

class Switcher extends React.Component<IProps> {
  render() {
    const {
      mapID,
      authorID,
      inProgress,
      setMapPublishStatus,
      activeRowMapID,
      sectionValue: { status, isBanned }
    } = this.props

    return (
      <Release
        mapID={mapID}
        authorID={authorID}
        inProgress={activeRowMapID === mapID && inProgress}
        data={{ published: status, isBanned }}
        switcherToggle={setMapPublishStatus}
      />
    )
  }
}

export default Switcher
