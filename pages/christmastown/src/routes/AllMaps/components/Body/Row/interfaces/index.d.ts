import { TMap } from '../../../../interfaces/IState'

export interface IProps {
  map: TMap
  activeUserMapID: string
}

export type TKey = string | number
export type TSections = string | object
