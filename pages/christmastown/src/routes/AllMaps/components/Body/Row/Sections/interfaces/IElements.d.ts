export interface IProps {
  sectionValue: {
    objects: number
    parameters: number
    areas: number
  }
  className: string
}
