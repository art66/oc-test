export interface IProps {
  restrictLayout?: false
  poolingFetchData: () => void
}
