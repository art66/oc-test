import React from 'react'
import { connect } from 'react-redux'

import Header from '../components/Header'
import Body from '../containers/Body'

import { poolingStart } from '../modules/actions'
import { IProps } from './interfaces'

import styles from './index.cssmodule.scss'

const POLL_TIMER_PERIOD = 60000 * 10

class AppLayout extends React.Component<IProps> {
  private _timerID: any

  componentDidMount() {
    this._requestPoolingDataUpdate()
  }

  componentWillUnmount() {
    this._destroyPoolingDataUpdate()
  }

  _requestPoolingDataUpdate = () => {
    const { poolingFetchData } = this.props

    this._timerID = setInterval(poolingFetchData, POLL_TIMER_PERIOD)
  }

  _destroyPoolingDataUpdate = () => {
    clearInterval(this._timerID)
  }

  render() {
    const { restrictLayout } = this.props

    if (restrictLayout) {
      return null
    }

    return (
      <div className={styles.appWrapper}>
        <Header />
        <Body />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  restrictLayout: state.allMaps.settings.restrictLayout
})

const mapDispatchToState = dispatch => ({
  poolingFetchData: () => dispatch(poolingStart())
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(AppLayout)
