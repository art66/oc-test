import { Dispatch } from 'redux'
import { connect } from 'react-redux'

import Release from '../../components/Body/Row/Sections/Release'
import { switchMapPublish } from '../../modules/actions'
import { IReduxState } from '../../interfaces/IRedux'
import { ISetBanStatusAttempt } from '../../interfaces/IModules'

const mapStateToProps = ({ allMaps }: IReduxState) => ({
  inProgress: allMaps.settings.adminReleaseInProgress,
  activeRowMapID: allMaps.settings.activeRowMapID
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  setMapPublishStatus: ({ mapID, authorID, isBanned }): ISetBanStatusAttempt =>
    dispatch(switchMapPublish(mapID, authorID, isBanned))
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(Release)
