import { connect } from 'react-redux'

import Body from '../components/Body'
import { IReduxState } from '../interfaces/IRedux'

const mapStateToProps = (state: IReduxState) => ({
  mapsList: state.allMaps.maps.list,
  activeUserMapID: state.allMaps.settings.userActiveMapID,
  inProgress: state.allMaps.inProgress
})

export default connect(
  mapStateToProps,
  null
)(Body)
