import * as a from '../actions/actionTypes'

const initialState = {
  messages: [],
  loading: false
}

const ACTION_HANDLERS = {
  [a.FETCH_ALL_CHATS_DATA]: () => {
    return {
      loading: true
    }
  },

  [a.FETCH_ALL_CHATS_BY_USER]: () => {
    return {
      loading: true
    }
  },

  [a.FETCH_ALL_CHATS_DATA_SUCCESS]: (_, action) => {
    return {
      messages: action.payload.messages,
      loading: false
    }
  }
}

const reducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
