import React from 'react'
import { connect } from 'react-redux'
import UsersAutocomplete from '@torn/shared/components/UsersAutocomplete'
import Preloader from '@torn/shared/components/Preloader'
import { fetchChatsByUser, fetchChats } from '../../actions'
import s from './index.cssmodule.scss'

interface IProps {
  loading: boolean,
  fetchChatsByUser: (userId: string) => void,
  fetchChats: () => void
}

class Filter extends React.PureComponent<IProps> {
  _handleUserSelect = selected => {
    const { fetchChatsByUser } = this.props

    fetchChatsByUser(selected.id)
  }

  _handleUserSelectClear = () => {
    const { fetchChats } = this.props

    fetchChats()
  }

  render() {
    const { loading } = this.props

    return (
      <div className={s.filterWrapper}>
        <UsersAutocomplete
          withCategories={true}
          placeholder='Search by user name / ID'
          onSelect={this._handleUserSelect}
          onClear={this._handleUserSelectClear}
        />
        {loading && <Preloader />}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loading: state.allChats.loading
})

const mapActionsToProps = {
  fetchChatsByUser,
  fetchChats
}

export default connect(mapStateToProps, mapActionsToProps)(Filter)
