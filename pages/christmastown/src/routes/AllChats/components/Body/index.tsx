import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import commonStyles from '../../styles/common.cssmodule.scss'
import { IMessage } from '../../interfaces/IMessage'
import Row from './Row'
import styles from './index.cssmodule.scss'

interface IProps {
  loading: boolean
  messagesList: IMessage[]
}

class Body extends React.Component<IProps> {
  _classNameHolder = () => {
    return {
      tabClass: cn(commonStyles.column, styles.tabSection)
    }
  }

  _renderTabs = () => {
    const { tabClass } = this._classNameHolder()
    const tabs = ['user', 'map', 'time', 'message']

    return tabs.map(tabLabel => {
      return (
        <span key={tabLabel} className={`${tabClass} ${commonStyles[tabLabel]}`}>
          {firstLetterUpper({ value: tabLabel })}
        </span>
      )
    })
  }

  _renderRows = () => {
    const { loading, messagesList = [] } = this.props

    if (!messagesList.length && !loading) {
      return <div className={styles.placeholderWrap}>There is no messages yet.</div>
    }

    return messagesList.map((message: IMessage) => {
      return <Row key={Math.random().toString()} message={message} />
    })
  }

  render() {
    return (
      <div>
        <div className={styles.tabsContainer}>{this._renderTabs()}</div>
        <div className={styles.rowsContainer}>{this._renderRows()}</div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  messagesList: state.allChats.messages,
  loading: state.allChats.loading
})

export default connect(
  mapStateToProps,
  null
)(Body)
