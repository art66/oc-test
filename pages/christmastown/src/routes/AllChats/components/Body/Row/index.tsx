import React from 'react'
// import classnames from 'classnames'

import styles from './index.cssmodule.scss'
import commonStyles from '../../../styles/common.cssmodule.scss'

import { IMessage } from '../../../interfaces/IMessage'

interface IProps {
  message: IMessage
}

class Row extends React.Component<IProps, any> {
  render() {
    const { message } = this.props

    return (
      <div className={styles.rowWrap}>
        <div className={`${commonStyles.column} ${styles.section} ${commonStyles.user}`}>
          <span className={styles.sectionText}>{message.user.name}</span>
        </div>
        <div className={`${commonStyles.column} ${styles.section} ${commonStyles.map}`}>
          <span className={styles.sectionText}>{message.map.name}</span>
        </div>
        <div className={`${commonStyles.column} ${styles.section} ${commonStyles.time}`}>
          <span className={styles.sectionText}>{message.time}</span>
        </div>
        <div className={`${commonStyles.column} ${styles.section} ${commonStyles.message}`}>
          <span className={styles.sectionText}>{message.message}</span>
        </div>
      </div>
    )
  }
}

export default Row
