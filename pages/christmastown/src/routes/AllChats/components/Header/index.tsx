import React from 'react'

import styles from './index.cssmodule.scss'

class Header extends React.PureComponent {
  _renderTitle = () => {
    return <span className={`${styles.title} ${styles.textBorders}`}>All Chats</span>
  }

  render() {
    return <div className={styles.headerContainer}>{this._renderTitle()}</div>
  }
}

export default Header
