export interface IMessage {
  user: {
    id: number,
    name: string
  },
  map: {
    id: number,
    name: string
  },
  time: number,
  message: string
}
