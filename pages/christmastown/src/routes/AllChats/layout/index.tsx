import React from 'react'

import Header from '../components/Header'
import Filter from '../components/Filter'
import Body from '../components/Body'

class AppLayout extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Filter />
        <Body />
      </div>
    )
  }
}

export default AppLayout
