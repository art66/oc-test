import * as a from './actionTypes'

export const fetchChats = () => ({
  type: a.FETCH_ALL_CHATS_DATA
})

export const fetchChatsByUser = userID => ({
  userID,
  type: a.FETCH_ALL_CHATS_BY_USER
})

export const fetchDataSuccess = payload => ({
  payload,
  type: a.FETCH_ALL_CHATS_DATA_SUCCESS
})
