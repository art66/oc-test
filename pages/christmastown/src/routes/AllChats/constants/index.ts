
// ------------------
// SECTIONS ROWS
// ------------------
export const NAME = 'name'
export const CREATOR = 'authorId'
export const EDIT = 'lastEdit'
export const TESTERS = 'testers'
export const EDITORS = 'editors'
export const PLAYERS = 'playersOnline'
export const PUBLISHED = 'published'
export const MAPS_ELEMENTS = 'mapElements'
export const RATING = 'rating'
export const JOIN = 'manage'

export const NORMALIZED_SECTIONS_LABELS = {
  [NAME]: NAME,
  [CREATOR]: 'creator',
  [EDIT]: 'last_edit',
  [TESTERS]: TESTERS,
  [EDITORS]: EDITORS,
  [PLAYERS]: 'players',
  [MAPS_ELEMENTS]: 'elements',
  [PUBLISHED]: 'release',
  [RATING]: RATING,
  [JOIN]: 'join'
}
