import { put, takeEvery } from 'redux-saga/effects'
import * as a from '../actions/actionTypes'
import { fetchUrl, getErrorMessage } from '../../ChristmasTown/utils'
import { debugShow } from '../../../controller/actions'
import { fetchDataSuccess } from '../actions'

function* fetchAllChats() {
  try {
    const response = yield fetchUrl('getAllChatsList')

    yield put(fetchDataSuccess(response))
  } catch (error) {
    yield put(debugShow(getErrorMessage(error)))
  }
}

function* fetchChatsByUser({ userID }: any) {
  try {
    const response = yield fetchUrl('getAllChatsList', { userID })

    yield put(fetchDataSuccess(response))
  } catch (error) {
    yield put(debugShow(getErrorMessage(error)))
  }
}

export default function* allChats() {
  yield takeEvery(a.FETCH_ALL_CHATS_DATA, fetchAllChats)
  yield takeEvery(a.FETCH_ALL_CHATS_BY_USER, fetchChatsByUser)
}
