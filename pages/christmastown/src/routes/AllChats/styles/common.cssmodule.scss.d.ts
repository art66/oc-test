// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'column': string;
  'flexCenter': string;
  'flexCenterStart': string;
  'fontBlue': string;
  'fontCT': string;
  'globalSvgShadow': string;
  'map': string;
  'message': string;
  'section': string;
  'time': string;
  'user': string;
}
export const cssExports: CssExports;
export default cssExports;
