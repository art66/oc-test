import React from 'react'
import Loadable from 'react-loadable'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/helpers/rootReducer'
import { fetchChats } from './actions'
import reducer from './reducers'

// import SkeletonFull from './components/Skeletons/Full'

const Preloader = () => <div />

const AllChatsRoute = Loadable({
  loader: async () => {
    const AllChats = await import('./layout/index')

    injectReducer(rootStore, { reducer, key: 'allChats' })
    rootStore.dispatch(fetchChats())

    return AllChats
  },
  loading: Preloader
})

export default AllChatsRoute
