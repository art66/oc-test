// ----------------------------------
// SVG COLOR CONSTANTS
// ----------------------------------
export const BLUE_HOVERED = 'BLUE_HOVERED'
export const BLUE_DIMMED = 'BLUE_DIMMED'
export const ORANGE_DELETED = 'ORANGE_DELETED'
export const BLUE_HOVERED_DARK = 'BLUE_HOVERED_DARK'
export const BLUE_DIMMED_DARK = 'BLUE_DIMMED_DARK'
export const ORANGE_DELETED_DARK = 'ORANGE_DELETED_DARK'

export const BUTTON_STATE = {
  ACTIVE: 'ACTIVE',
  HOVERED: 'CLICKED',
  CLICKED: 'DISABLED'
}

// ----------------------------------
// SVG STAR CONSTANTS
// ----------------------------------
export const STAR_ICONNAME = 'Star'
export const STARS_COUNT = 5
export const STAR_FILL_DIMMED = '#fff'
export const STAR_FILL_HOVERED = '#f1fcff'

// ----------------------------------
// REDUX STORE CONSTANTS
// ----------------------------------
export const LOAD_MAPS_DATA_ATTEMPT = 'myMaps/LOAD_MAPS_DATA_ATTEMPT'
export const MAPS_DATA_SAVED = 'myMaps/MAPS_DATA_SAVED'
export const CREATE_NEW_MAP = 'myMaps/CREATE_NEW_MAP'
export const NEW_MAP_ON_HOVER = 'myMaps/NEW_MAP_ON_HOVER'
export const NEW_MAP_ON_BLUR = 'myMaps/NEW_MAP_ON_BLUR'
export const NEW_MAP_SAVED = 'myMaps/NEW_MAP_SAVED'
export const NEW_MAP_CANCELED = 'myMaps/NEW_MAP_CANCELED'
export const NEW_INVITES_ADDED = 'myMaps/NEW_INVITES_ADDED'
export const SWITCHER_TOGGLE_ATTEMPT = 'myMaps/SWITCHER_TOGGLE_ATTEMPT'
export const SWITCHER_TOGGLE_SAVED = 'myMaps/SWITCHER_TOGGLE_SAVED'
export const CHANGE_MAP_NAME_START = 'myMaps/CHANGE_MAP_NAME_START'
export const CHANGE_MAP_NAME_ATTEMPT = 'myMaps/CHANGE_MAP_NAME_ATTEMPT'
export const CHANGE_MAP_NAME_CANCEL = 'myMaps/CHANGE_MAP_NAME_CANCEL'
export const CHANGE_MAP_NAME_SAVED = 'myMaps/CHANGE_MAP_NAME_SAVED'
export const INVITES_MAP_NAME_CHANGED = 'myMaps/INVITES_MAP_NAME_CHANGED'
export const DELETE_MAP = 'myMaps/MANAGE_MAP_TRASH'
export const DELETE_MAP_ATTEMPT = 'myMaps/DELETE_MAP_ATTEMPT'
export const SHARE_MAP = 'myMaps/MANAGE_MAP_SHARE'
export const JOIN_MAP = 'myMaps/MANAGE_MAP_JOIN'
export const SETTINGS_MAP = 'myMaps/MANAGE_MAP_SETTINGS'
export const LEAVE_MAP = 'myMaps/MANAGE_MAP_LEAVE'
export const LEAVE_MAP_ATTEMPT = 'myMaps/MANAGE_MAP_LEAVE_ATTEMPT'
export const MAP_LEAVED = 'myMaps/MAP_LEAVED'
export const RENAME_MAP = 'myMaps/MANAGE_MAP_RENAME'
export const CANCEL_DELETE_MAP = 'myMaps/CANCEL_DELETE_MAP'
export const MAP_DELETED = 'myMaps/MAP_DELETED'
export const CANCEL_LEAVE_MAP = 'myMaps/CANCEL_LEAVE_MAP'
export const CANCEL_SHARE_MAP = 'myMaps/CANCEL_SHARE_MAP'
export const MAP_SHARE_ATTEMPT = 'myMaps/MAP_SHARE_ATTEMPT'
export const MAP_SHARE_UPDATED = 'myMaps/MAP_SHARE_UPDATED'
export const MEMBER_ADDED = 'myMaps/MEMBER_ADDED'
export const MEMBER_DELETED = 'myMaps/MEMBER_DELETED'
export const CANCEL_ADD_MEMBER = 'myMaps/CANCEL_ADD_MEMBER'
export const ADD_MEMBER = 'myMaps/ADD_MEMBER'
export const CHANGE_MEMBER_ROLE = 'myMaps/CHANGE_MEMBER_ROLE'
export const CREATE_MAP_START = 'myMaps/CREATE_MAP_START'
export const CREATE_MAP_ATTEMPT = 'myMaps/CREATE_MAP_ATTEMPT'
export const SYNCHRONIZE_PLAYERS = 'myMaps/SYNCHRONIZE_PLAYERS'
export const FETCH_STAFF_ATTEMPT = 'myMaps/FETCH_STAFF_ATTEMPT'
export const FETCH_STAFF_CANCEL = 'myMaps/FETCH_STAFF_CANCEL'
export const FETCH_STAFF_SAVED = 'myMaps/FETCH_STAFF_SAVED'
export const REMOVE_REQUEST_STAGE = 'myMaps/REMOVE_REQUEST_STAGE'
export const FETCH_USER_ATTEMPT = 'myMaps/FETCH_USER_ATTEMPT'
export const FETCH_USER_SAVED = 'myMaps/FETCH_USER_SAVED'
export const CHANGE_INVITES_POSITION = 'myMaps/CHANGE_INVITES_POSITION'
export const INVITE_REMOVED = 'myMaps/INVITE_REMOVED'
export const INVITES_MAP_REMOVED = 'myMaps/INVITES_MAP_REMOVED'
export const INVITES_MAP_PUBLISHED_CHANGED = 'myMaps/INVITES_MAP_PUBLISHED_CHANGED'
export const INVITES_MAP_STAFF_STATE_UPDATED = 'myMaps/INVITES_MAP_STAFF_STATE_UPDATED'
export const OWNER_MAP_STAFF_STATE_UPDATED = 'myMaps/OWNER_MAP_STAFF_STATE_UPDATED'
export const HIGHLIGHT_JOINED_MAP = 'myMaps/HIGHLIGHT_JOINED_MAP'
export const USER_BANNED_STATUS = 'myMaps/USER_BANNED_STATUS'

// ----------------------------------
// API ENDPOINTS
// ----------------------------------
export const API = {
  CREATE_NEW_MAP: 'createNewMap',
  DELETE_MAP: 'removeMap',
  RENAME_MAP: 'renameMap',
  PUBLISH_MAP: 'updatePublishedStage',
  LEAVE_MAP: 'leavePosition',
  UPDATE_STAFF: 'updateMapStaff',
  FETCH_STAFF: 'autocompleteUserAjaxAction',
  LOAD_MAIN_DATA: 'getMyMapsData'
}

// ----------------------------------
// REDUCERS PARTICULAR CONSTANTS
// ----------------------------------
export const ID_INCREMENTOR = 1
export const NEW_MAP_ID = 1
export const JOIN_SECTION_ID = 7
export const MANAGE_STAFF_NORMALIZER = {
  editor: 'editorsOnMap',
  tester: 'testersOnMap'
}
export const MONTH_NAMES = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

// ----------------------------------
// TABS ADAPTER FOR DROPDOWN PANEL
// ----------------------------------
export const TABS_ADAPTER = {
  0: 'ac-friends',
  1: 'ac-factions',
  2: 'ac-company',
  3: 'ac-ct-all'
}

// ---------------------------------------------
// ICONS DIMENSIONS FOR TOGGLE IN SEARCH SECTION
// ---------------------------------------------
export const SEARCH_TOGGLE_ICON_NAMES = {
  editor: 'Cubes',
  tester: 'Testing'
}
export const SEARCH_TOGGLE_ICON_DIMENSIONS = {
  Testing: {
    width: 15,
    height: 15,
    viewbox: '0 0 22 24'
  },
  Cubes: {
    width: 17,
    height: 15,
    viewbox: '0 0 22 24'
  }
}

// ----------------------------------
// DEBUG MESSAGES
// ----------------------------------
export const BANNED_LABEL = 'You are currently banned. Please contact the staff.'

// ----------------------------------
// NORMALIZER ROWS DATA ENHANCER
// ----------------------------------
export const COLUMN_TITLES = {
  maps: [
    {
      title: 'Name',
      class: 'name'
    },
    {
      title: 'Last edit',
      class: 'lastedit'
    },
    {
      title: 'Editors',
      class: 'editors'
    },
    {
      title: 'Testers',
      class: 'testers'
    },
    {
      title: 'Players',
      class: 'players'
    },
    {
      title: 'Release',
      class: 'release'
    },
    {
      title: 'Rating',
      class: 'rating'
    },
    {
      title: 'Manage',
      class: 'manage'
    }
  ],
  invites: [
    {
      title: 'Name',
      class: 'nameInvites'
    },
    {
      title: 'Last edit',
      class: 'lastedit'
    },
    {
      title: 'Editors',
      class: 'editors'
    },
    {
      title: 'Testers',
      class: 'testers'
    },
    {
      title: 'Players',
      class: 'playersInvites'
    },
    {
      title: 'Release',
      class: 'release'
    },
    {
      title: 'Rating',
      class: 'rating'
    },
    {
      title: 'Manage',
      class: 'manage'
    }
  ]
}
