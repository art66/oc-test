import isValue from '@torn/shared/utils/isValue'
import getUserID from '@torn/shared/utils/getUserID'
import normalizeCurrentTabLabel from './normalizeCurrentTabLabel'

interface IUser {
  type: string
}

interface IGetUsersList {
  staffType: string
  usersList: {
    id: string
    type: string
  }[]
}

const MAIN_TAB = 'all'

const getUsersList = ({ staffType, usersList: receivedUsersList = [] }: IGetUsersList) => {
  const currentStaffTab = normalizeCurrentTabLabel(staffType)
  const isUsersReceived = isValue(receivedUsersList) && !!receivedUsersList.length

  if (!isUsersReceived) {
    return null
  }

  const preNormalizedUsersList = () => {
    // if we receive userList includes current user, we need to cut it out immediately
    const usersListWithoutCurrentUser = receivedUsersList.filter(user => user.id !== getUserID())

    return usersListWithoutCurrentUser
  }

  if (currentStaffTab === MAIN_TAB) {
    const createMainUsersList = () => {
      const usersListsTempt = {
        all: []
      }

      const pushUserToItsStore = ({ user, type = MAIN_TAB }) => {
        if (!(usersListsTempt[type] instanceof Array)) {
          usersListsTempt[type] = []
        }

        usersListsTempt[type].push(user)
      }

      preNormalizedUsersList().forEach((user: IUser) => {
        pushUserToItsStore({ user, type: !user.type ? MAIN_TAB : user.type })
      })

      return usersListsTempt
    }

    return createMainUsersList()
  }

  return {
    [currentStaffTab]: isUsersReceived ? preNormalizedUsersList() : []
  }
}

export default getUsersList
