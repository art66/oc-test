import isInvitesTable from './isInvitesTablet'

// ----------------------------------
// NORMALIZER ROWS DATA ENCHENSER
// ----------------------------------
export const normalizerHolder = (appID: number) => ({
  name: {
    type: 'text',
    name: 'Name',
    className: isInvitesTable(appID) ? 'nameInvites' : 'name'
  },
  lastEdit: {
    type: 'text',
    name: 'Last Edit',
    className: 'lastedit'
  },
  editors: {
    type: 'text',
    name: 'Editors',
    className: 'editors'
  },
  testers: {
    type: 'text',
    name: 'Testers',
    className: 'testers'
  },
  players: {
    type: 'text',
    name: 'Players',
    className: isInvitesTable(appID) ? 'playersInvites' : 'players'
  },
  published: {
    type: 'release',
    name: 'Release',
    className: 'beta'
  },
  rating: {
    type: 'rating',
    name: 'Rating',
    className: 'rating'
  },
  manage: {
    type: 'manage',
    name: 'Manage',
    className: 'manage'
  },
  leave: {
    type: 'leave',
    name: 'Leave',
    className: 'leave'
  }
})

const rowDataNormalizer = (appID: number, payload: object) => {
  const rormalizedPayloadArray = []
  const payloadKeys = (payload && Object.keys(payload)) || []

  if (!payload || payloadKeys.length === 0) {
    return null
  }

  payloadKeys.map(key => {
    const currentSectionData = {
      ...normalizerHolder(appID)[key],
      data: payload[key]
    }

    rormalizedPayloadArray.push(currentSectionData)
  })

  return rormalizedPayloadArray
}

export default rowDataNormalizer
