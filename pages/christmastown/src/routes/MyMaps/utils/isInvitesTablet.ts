const INVITES_ID = 2

const isInvitesTable = (appID: number) => {
  return appID === INVITES_ID
}

export default isInvitesTable
