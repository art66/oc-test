// interface IMapExisted {
//   uuid: string
//   name: string | number,
//   lastEdit: number,
//   testers: number,
//   editors: number,
//   playersOnline: number,
//   published: boolean,
//   rating: number,
//   manage: {
//     joinLink: string
//   }
// }

// interface IMapIncoming {
//   ID: string
// }

const checkIsMapExist = (maps: any, newMap: any) => maps.some(map => map.uuid === newMap.ID)

export default checkIsMapExist
