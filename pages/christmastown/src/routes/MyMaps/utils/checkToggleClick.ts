const checkToggleClick = (nextManageType: string, currentManageType: string, isMobileLayout: boolean) => {
  return (nextManageType === currentManageType) && isMobileLayout
}

export default checkToggleClick
