import { ID_INCREMENTOR } from '../constants'

const updateStaffCount = (staff: number, calcType: string) => {
  const staffCount = {
    staff
  }

  const calcStaffCount = () => {
    calcType === 'minus' && staff > 0 ? staffCount.staff -= ID_INCREMENTOR : staffCount.staff += ID_INCREMENTOR

    return staffCount.staff
  }

  return calcStaffCount()
}

export default updateStaffCount

// const calcStaffCount = () => {
//   if (calcType !== 'plus' && staffCount.staff >= 0) {
//     staffCount.staff -= ID_INCREMENTOR
//   } else if (calcType === 'plus') {
//     const isEditorsFull = staffType === 'testers' && staffCount.staff >= 25
//     const isTestersFull = staffType === 'editors' && staffCount.staff >= 10

//     if (isEditorsFull || isTestersFull) {
//       return
//     }

//     staffCount.staff += ID_INCREMENTOR
//   }

//   return staffCount.staff
// }

// return calcStaffCount()
