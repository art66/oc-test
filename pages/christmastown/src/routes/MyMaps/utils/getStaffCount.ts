import { ID_INCREMENTOR } from '../constants'

const getStaffCount = (
  membersList: any = [],
  testers: string = 'testersOnMap',
  editors: string = 'editorsOnMap'
) => {
  const staff = {
    [testers]: 0,
    [editors]: 0
  }

  membersList.forEach(member => {
    member.type === 'tester' ? staff[testers] += ID_INCREMENTOR : staff[editors] += ID_INCREMENTOR
  })

  return staff
}

export default getStaffCount
