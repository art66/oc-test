import getDate from '../utils/getDate'
import isInvitesTable from './isInvitesTablet'

// interface IMapIncoming {
//   ID: string
//   name: string | number,
//   createdAt: number,
//   authorId: number,
//   lastEdit: number,
//   testers: number,
//   editors: number,
//   playersOnline: number,
//   published: boolean,
//   rating: number,
//   manage: {
//     joinLink: string
//   }
// }

const mapRowDataNormalizer = (newID: number, mapData: any, appID: number) => {

  const rowData: any = {
    ID: newID,
    uuid: mapData.ID || null,
    createdAt: mapData.createdAt,
    authorID: mapData.authorId,
    sections: {
      name: mapData.name || null,
      lastEdit: getDate(mapData.lastEdit),
      editors: mapData.editors || 0,
      testers: mapData.testers || 0,
      players: mapData.playersOnline || null,
      published: mapData.published || false,
      rating: mapData.rating || null
    }
  }

  if (!isInvitesTable(appID)) {
    rowData.sections.manage = {
      joinLink: mapData.manage.joinLink || null,
      inviteLink: mapData.manage.joinLink || null,
      editorsOnMap: mapData.editors,
      testersOnMap: mapData.testers,
      membersList: []
    }
  } else {
    rowData.sections.leave = {
      position: mapData.position,
      joinLink: mapData.link
    }
  }

  return rowData
}

export default mapRowDataNormalizer
