import getKeyByValue from '@torn/shared/utils/getKeyByValue'

import { TABS_ADAPTER } from '../constants'

const getCurrentTabID = (currentTab: string) => {
  if (!TABS_ADAPTER || !currentTab) {
    return null
  }

  return Number(getKeyByValue(TABS_ADAPTER, currentTab))
}

export default getCurrentTabID
