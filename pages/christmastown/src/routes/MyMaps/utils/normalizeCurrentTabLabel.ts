const normalizeCurrentTabLabel = (currentTab: string) => {
  if (currentTab && !/(ac-)/.test(currentTab)) {
    return currentTab
  }

  const startTrimCount = currentTab === 'ac-ct-all' ? 6 : 3
  const endTrimCount = currentTab === 'ac-factions' ? 1 : 0

  const trimLeft = currentTab.substr(startTrimCount, currentTab.length)
  const trimEndCount = trimLeft.length - endTrimCount
  const trimRight = trimLeft.substr(0, trimEndCount)

  return trimRight
}

export default normalizeCurrentTabLabel
