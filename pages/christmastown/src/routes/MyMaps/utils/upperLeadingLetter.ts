const upperLeadingLetter = word => {
  const upperWord = word.substr(0, 1).toUpperCase() + word.substr(1, word.length)

  return upperWord
}

export default upperLeadingLetter
