import { MONTH_NAMES } from '../constants'
const UNIX_TO_TIME_AMPLIFIER = 1000

const getDate = (date: number): string => {
  const timeReceived = date * UNIX_TO_TIME_AMPLIFIER

  const currentDay = new Date()
  const incomingDay = new Date(timeReceived)
  const calcDate = incomingDay || new Date()

  const isTheSamDay = currentDay.toISOString().substr(0,10) === incomingDay.toISOString().substr(0,10)

  const getMonthDayDate = () => `${MONTH_NAMES[calcDate.getUTCMonth()]} ${calcDate.getUTCDate()}`
  const getHoursMinutesDate = () => {
    const hours = calcDate.getUTCHours()
    const minutes = calcDate.getUTCMinutes()

    const timeNormalizer = (timeValue: number) => timeValue < 10 ? `0${timeValue}` : `${timeValue}`

    return `${timeNormalizer(hours)}:${timeNormalizer(minutes)}`
  }

  return isTheSamDay ? getHoursMinutesDate() : getMonthDayDate()
}

export default getDate
