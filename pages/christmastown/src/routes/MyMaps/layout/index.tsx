import React from 'react'

import MyMaps from '../containers/MyMaps'
import Invites from '../containers/Invites'
import GradientStarMobile from '../components/GradientStarMobile'

import styles from './index.cssmodule.scss'

class AppLayout extends React.Component {
  // TODO: <GradientStarMobile /> should not be here. Move it to dev CTE template .php after finish
  render() {
    return (
      <React.Fragment>
        <div className={styles.appWrapper}>
          <MyMaps />
          <hr className='delimiter-999' />
          <Invites />
        </div>
        <GradientStarMobile />
      </React.Fragment>
    )
  }
}

export default AppLayout
