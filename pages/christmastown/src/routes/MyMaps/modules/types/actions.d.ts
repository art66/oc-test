import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IType {
  type: string
}

export interface IAsyncRequest {
  isRequested?: boolean
}

interface IJoinData {
  editorsOnMap: number
  testersOnMap: number
  membersList: {
    ID: number
  }[]
}

export interface IState {
  general: {
    isDesktopLayoutSet: boolean
    activeJoinedMapID: string
    loadMainData: {
      isRequested: boolean
    }
  }
  mymaps: {
    activeRowID: number
    createMap?: {
      type: string
      newMapInProgress: boolean
      newMapHovered: boolean
    }
    changeName?: {
      changeMapNameID: number
      changeMapNameInProgress: boolean
    }
    publishMap?: {
      publishMapID: number
      isRequested: IAsyncRequest['isRequested']
    }
    manageMap?: {
      rowID?: number
      manageRowID?: number
      manageType?: string
      manageMapInProgress?: boolean
    }
    addMember?: {
      rowID?: number
      currentTab: string
      addMemberRowID: number
      addMemberInProgress: boolean
    }
    searchUser?: {
      isRequested: IAsyncRequest['isRequested']
    }
    inviteUsersList: {
      all: object[]
      faction: object[]
      company: object[]
      friends: object[]
    }
    rows: {
      uuid: string
      ID: number
      temp: {
        membersList: object[]
      }
      requests: {
        isReleaseRequest: boolean
      }
      sections: {
        name: string
        className: string
        manage: IJoinData
        players: number
      }
    }[]
  }
  invites: {
    manageMap?: object
    rows?: {
      uuid: string
      sections: {
        leave: {
          position: string
          joinLink: string
        }
      }
    }[]
  }
}

export interface ILoadDataAttempt {
  isRequested: IAsyncRequest['isRequested']
}

interface IMyMapsManageData {
  joinLink: string
  staff: object[]
}

interface ICommonRowsData {
  ID: string
  name: string
  authorId: number
  editors: number
  testers: number
  lastEdit: number
  createdAt: number
  playersOnline?: number
  players?: number
  published: boolean
  rating: number
  manage?: IMyMapsManageData
  position?: string
  link?: string
}

export interface ILoadDataSaved {
  payload: {
    settings: {
      isUserBanned: boolean
      userActiveMapID: string
    }
    myMaps: {
      controlData: object
      list: ICommonRowsData[]
    }
    myInvites: {
      list: ICommonRowsData[]
    }
  }
  isRequested: IAsyncRequest['isRequested']
}

export interface ICreateNewMapStart extends INewMapHover {
  newMapInProgress: boolean
}

export interface ICreateNewMapAttempt {
  newMapName?: string
  isRequested: IAsyncRequest['isRequested']
}

export interface INewMapHover {
  newMapHovered: boolean
}

export interface INewMapSaved {
  isRequested: IAsyncRequest['isRequested']
  newMapInProgress: boolean
  newMap: {
    ID: string
    name: string | number
    lastEdit: number
    playersOnline: number
    published: boolean
    rating: number
    manage: {
      joinLink: string
    }
  }
}

export interface INewMapCanceled {
  isRequested: IAsyncRequest['isRequested']
  newMapInProgress: boolean
}

export interface INewMapInvited {
  mapData: {
    ID: string
    name: string | number
    lastEdit: number
    testers: number
    editors: number
    playersOnline: number
    published: boolean
    rating: number
    manage: {
      joinLink: string
    }
  }
}

export interface IMapNameChangeStart {
  ID: string
  changeMapNameInProgress: boolean
}

export interface IMapNameChangeAttempt {
  ID: string
  newMapName: string | number
  isRequested: IAsyncRequest['isRequested']
}

export interface IMapNameChangeCanceled {
  ID: number
  changeMapNameInProgress: boolean
}

export interface IMapNameChangeSaved {
  uuid: string
  lastEdit: number
  newName: string
  isRequested: IAsyncRequest['isRequested']
  changeMapNameInProgress: boolean
}

export interface IInvitesMapNameChangeSaved {
  mapID: string
  newName: string
}

export interface ISwitcherToggleAttempt {
  uuid: string
  rowID: number
  releaseRequest: boolean
  publishStatus: string
}

export interface ISwitcherToggle {
  uuid: string
  lastEdit: number
  releaseRequest: boolean
  publishStatus: boolean
}

export interface IManageMap extends IAsyncRequest {
  rowID: number
  mediaType: TMediaType
  manageType: string
  manageMapInProgress: boolean
  mobileLayout: boolean
}

export interface ILeaveMap {
  uuid: string
  isRequested: IAsyncRequest['isRequested']
  manageMapInProgress: IManageMap['manageMapInProgress']
}

export interface ILeaveMapCancel {
  rowID: IManageMap['rowID']
  isRequested: IAsyncRequest['isRequested']
  manageMapInProgress: IManageMap['manageMapInProgress']
}

export interface ILeaveMapAttempt {
  uuid: string
  isRequested: IAsyncRequest['isRequested']
}

export interface IShareMapAttempt {
  rowID: IManageMap['rowID']
  isRequested: IAsyncRequest['isRequested']
}

export interface IShareMapAttempt {
  isRequested: IAsyncRequest['isRequested']
  rowID: IManageMap['rowID']
}

export interface IShareMap {
  membersList: {
    type: string
  }[]
  lastEdit: number
  isRequested: IAsyncRequest['isRequested']
  manageMapInProgress: IManageMap['manageMapInProgress']
}

export interface IShareMapCancel {
  rowID: IManageMap['rowID']
  manageMapInProgress: IManageMap['manageMapInProgress']
}

export interface IDeleteMap {
  mapID: string
  isRequested: IAsyncRequest['isRequested']
  manageMapInProgress: IManageMap['manageMapInProgress']
}

export interface IDeleteMapAttempt {
  mapID: string
  rowID: IManageMap['rowID']
  isRequested: IAsyncRequest['isRequested']
}

export interface IDeleteMapCancel {
  rowID: IManageMap['rowID']
  isRequested: IAsyncRequest['isRequested']
  manageMapInProgress: IManageMap['manageMapInProgress']
}

export interface IAddMember {
  rowID: number
  isRequested: IAsyncRequest['isRequested']
  addMemberInProgress: boolean
}

export interface IDeleteMember {
  rowID: number
  memberID: number
  memberType: string
}

export interface ISaveMember {
  rowID: number
  addMemberInProgress: boolean
  member: {
    ID: number
    name: string
    type: string
  }
}

export interface IChangeMemberRole {
  rowID: number
  member: {
    memberID: number
    memberType: string
  }
}

export interface ISynchronizePlayers {
  mapID: string
  playersOnline: number
}

export interface ICurrentStaffTab {
  isRequested: IAsyncRequest['isRequested']
  currentTab: string
}

export interface IUsersList {
  isRequested: IAsyncRequest['isRequested']
  usersType: string
  usersList: {
    id: string
    type: string
  }[]
}

export interface IUserFetchedAttempt {
  searchText: string
  isRequested: IAsyncRequest['isRequested']
}

export interface IUserFetched {
  isRequested: IAsyncRequest['isRequested']
  usersType: string
  newUsersFetched: {
    id: string
    type: string
  }[]
}

export interface IRemoveRequestedStage {
  table: string
  sectionType: string
  isRequested: IAsyncRequest['isRequested']
}

export interface IChangeInvitesPosition {
  mapID: string
  role: string
}

export interface IInviteRemoved {
  ID: string
}

export interface IInvitesMapRemoved {
  mapID: string
}
export interface IInvitesPublishedChanged {
  mapID: string
  published: boolean
}

export interface IInvitesMapStaffUpdate {
  mapID: string
  testers: number
  editors: number
}

export interface IOwnerMapStaffUpdate {
  mapID: string
  testers: number
  editors: number
  newStaffState: object[]
}

export interface IJoinedMapHighlight {
  userActiveMapID: string
}

export interface IBanned {
  isUserBanned: boolean
}
