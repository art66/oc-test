import safetyFetch from './helpers/safetyFetch'

import { API } from '../../constants'

function* deleteMap({ mapID }: any) {
  yield safetyFetch({
    endpoint: API.DELETE_MAP,
    payload: {
      mapID
    },
    removeRequest: {
      table: 'mymaps',
      sectionType: 'manageMap'
    },
    errorStageMessage: 'map deleting'
  })
}

export default deleteMap
