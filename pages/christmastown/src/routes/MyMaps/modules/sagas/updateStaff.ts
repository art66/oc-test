import { put } from 'redux-saga/effects'

import getState from './helpers/getState'
import safetyFetch from './helpers/safetyFetch'

import { shareMapCancel } from '../actions'
import { API } from '../../constants'

function* updateStaff({ rowID }: any) {
    const { mymaps } = yield getState()

    const currentRow = mymaps.rows.find(row => row.ID === rowID) || []

    const checkIsMembersListNotChanged = () => {
      const membersListPresent = currentRow.temp.membersList
      const membersListToUpdate = currentRow.sections.manage.membersList
      const isMembersListNotChenged = JSON.stringify(membersListPresent) === JSON.stringify(membersListToUpdate)

      return isMembersListNotChenged
    }

    if (checkIsMembersListNotChanged()) {
      yield put(shareMapCancel(mymaps.activeRowID, false))

      return
    }

    yield safetyFetch({
      endpoint: API.UPDATE_STAFF,
      payload: {
        mapID: currentRow.uuid,
        staffState: currentRow.sections.manage.membersList
      },
      removeRequest: {
        table: 'mymaps',
        sectionType: 'manageMap'
      },
      errorStageMessage: 'staff updating'
    })
}

export default updateStaff
