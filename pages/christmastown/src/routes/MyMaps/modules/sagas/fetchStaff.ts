import { put } from 'redux-saga/effects'
import isValue from '@torn/shared/utils/isValue'

import getState from './helpers/getState'
import safetyFetch from './helpers/safetyFetch'
import normalizeCurrentTabLabel from '../../utils/normalizeCurrentTabLabel'
import createFetchURL from './helpers/createFetchURL'
import { saveUsersList, preventFetchUsersAttempt } from '../actions'

function* fetchStaff({ currentTab: staffType }: any) {
  const { mymaps } = yield getState()

  function* getStaffTabOnFirstEnter() {
    return mymaps.addMember.currentTab
  }

  const currentStaffType = isValue(staffType) ? staffType : yield getStaffTabOnFirstEnter()
  const flagToSort = normalizeCurrentTabLabel(currentStaffType)

  function* checkIfRequestDoNotNeeded() {
    const inviteUsersList = mymaps.inviteUsersList

    return isValue(inviteUsersList[flagToSort])
  }

  if (yield checkIfRequestDoNotNeeded()) {
    yield put(preventFetchUsersAttempt(false))

    return
  }

  yield safetyFetch({
    endpoint: createFetchURL(currentStaffType),
    errorStageMessage: 'staff fetching',
    fetchType: 'global',
    removeRequest: {
      table: 'mymaps',
      sectionType: 'addMember'
    },
    callback: {
      invokeCallback: saveUsersList,
      extraData: flagToSort
    }
  })
}

export default fetchStaff
