import safetyFetch from './helpers/safetyFetch'

import { API } from '../../constants'

function* publishMap({ publishStatus, uuid }: any) {
  yield safetyFetch({
    endpoint: API.PUBLISH_MAP,
    payload: {
      publishedStage: publishStatus,
      mapID: uuid
    },
    removeRequest: {
      table: 'mymaps',
      sectionType: 'publishMap'
    },
    errorStageMessage: 'map publishing'
  })
}

export default publishMap
