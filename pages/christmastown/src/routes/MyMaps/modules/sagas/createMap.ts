import safetyFetch from './helpers/safetyFetch'

import { API } from '../../constants'

function* createMap({ newMapName }: any) {
  yield safetyFetch({
    endpoint: API.CREATE_NEW_MAP,
    payload: {
      mapName: newMapName
    },
    removeRequest: {
      table: 'mymaps',
      sectionType: 'createMap'
    },
    errorStageMessage: 'map saving'
  })
}

export default createMap
