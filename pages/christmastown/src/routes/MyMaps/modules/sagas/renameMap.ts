import safetyFetch from './helpers/safetyFetch'

import { API } from '../../constants'

function* renameMap({ ID, newMapName }: any) {
  yield safetyFetch({
    endpoint: API.RENAME_MAP,
    payload: {
      mapID: ID,
      newName: newMapName
    },
    removeRequest: {
      table: 'mymaps',
      sectionType: 'changeName'
    },
    errorStageMessage: 'map renaming'
  })
}

export default renameMap
