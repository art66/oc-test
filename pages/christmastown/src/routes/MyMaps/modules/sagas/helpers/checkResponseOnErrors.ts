interface IResponse {
  response: {
    success: boolean
    message: {
      type: string,
      text: string
    }
  }
  errorStageMessage: string
}

function* checkResponseOnErrors({ response, errorStageMessage }: IResponse) {
  if (!response || response.success === false) {
    if (response && response.message && response.message.type === 'error') {
      throw new Error(response.message.text)
    }

    throw new Error(
      `Some error has happened during ${errorStageMessage} process.
      Please, ask Torn\'s Staff for help. Response: ${response}`
    )
  }
}

export default checkResponseOnErrors
