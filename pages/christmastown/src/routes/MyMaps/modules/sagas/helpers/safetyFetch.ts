import { put } from 'redux-saga/effects'
import { fetchUrl } from '../../../utils/fetchURL'
import { fetchUrl as fetchUrlGlobal } from '@torn/shared/utils'

import invokeCallback, { IInvokeCallback } from '../helpers/invokeCB'
import { debugShow, debugHide, getRouteHeader } from '../../../../../controller/actions'
import { removeRequestStage } from '../../actions'

import checkResponseOnErrors from './checkResponseOnErrors'
import { BANNED_LABEL } from '../../../constants'

interface ISafetyFetch {
  endpoint: string
  payload?: object | string
  errorStageMessage: string
  fetchType?: string
  removeRequest: {
    table: 'general' | 'mymaps' | 'invites'
    sectionType:
      | 'loadMainData'
      | 'createMap'
      | 'changeName'
      | 'addMember'
      | 'manageMap'
      | 'publishMap'
      | 'searchUser'
      | 'leaveMap'
  }
  callback?: IInvokeCallback
}

function* safetyFetch({ endpoint, payload, errorStageMessage, callback, fetchType, removeRequest }: ISafetyFetch) {
  try {
    const fetchMyMapsURL = fetchType !== 'global' ? fetchUrl : fetchUrlGlobal
    const response = yield fetchMyMapsURL(endpoint, payload)

    yield checkResponseOnErrors({ response, errorStageMessage })
    yield invokeCallback({ response, ...callback })

    // TODO: should be created as a separate helper like "checkResponseOnErrors"
    // is in favor of reducing the procedure-like stile, non-broking current abstraction
    // and to prevent a shit code at all!
    if (response.settings && response.settings.isUserBanned) {
      yield put(debugShow(BANNED_LABEL))
    } else {
      yield put(debugHide())
    }

    return response
  } catch (e) {
    yield put(debugShow((e && e.message) || e))
    yield put(removeRequestStage({ table: removeRequest.table, sectionType: removeRequest.sectionType }))
  } finally {
    yield put(getRouteHeader())
  }
}

export default safetyFetch
