import normalizeFetchParams from '@torn/shared/utils/normalizeFetchParams'

import { API } from '../../../constants'

const createFetchURL = (staffType: string, query?: string) => {
  const options = staffType && `option=${staffType}` || undefined
  const queries = query && `q=${query}` || undefined

  const normalizedParams = normalizeFetchParams([queries, options])
  const actionURL = `/${API.FETCH_STAFF}.php${normalizedParams}`

  return actionURL
}

export default createFetchURL
