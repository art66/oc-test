import { put } from 'redux-saga/effects'

export interface IInvokeCallback {
  invokeCallback: (payload: object, isRequested: boolean, extraData: any) => any
  extraData?: object | string | number | boolean
}

interface ICallback extends IInvokeCallback {
  response: object
}

function* invokeCB({ response, invokeCallback, extraData }: ICallback) {
  if (invokeCallback && typeof invokeCallback === 'function') {

    yield put(invokeCallback(response, false, extraData))
  }
}

export default invokeCB
