import { select } from 'redux-saga/effects'

function* getState() {
  const { myMapsApp: state } = yield select(store => store)

  return state
}

export default getState
