import safetyFetch from './helpers/safetyFetch'

import { API } from '../../constants'

function* leaveMap({ uuid }: any) {
  yield safetyFetch({
    endpoint: API.LEAVE_MAP,
    payload: {
      mapID: uuid
    },
    removeRequest: {
      table: 'invites',
      sectionType: 'leaveMap'
    },
    errorStageMessage: 'map leaving'
  })
}

export default leaveMap
