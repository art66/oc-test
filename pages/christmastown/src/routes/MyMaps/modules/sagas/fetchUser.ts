import safetyFetch from './helpers/safetyFetch'
import createFetchURL from './helpers/createFetchURL'
import getState from './helpers/getState'

import { saveNewUser } from '../actions'

function* fetchUser({ searchText }: any) {
  const { mymaps } = yield getState()
  const userCategory = mymaps.addMember.currentTab

  yield safetyFetch({
    endpoint: createFetchURL(userCategory, searchText),
    errorStageMessage: 'user live searching',
    fetchType: 'global',
    removeRequest: {
      table: 'mymaps',
      sectionType: 'searchUser'
    },
    callback: {
      invokeCallback: saveNewUser,
      extraData: userCategory
    }
  })
}

export default fetchUser
