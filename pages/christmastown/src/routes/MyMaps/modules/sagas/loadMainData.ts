import safetyFetch from './helpers/safetyFetch'
import { loadDataSaved } from '../actions'

import { API } from '../../constants'

function* loadMainData() {
  yield safetyFetch({
    endpoint: API.LOAD_MAIN_DATA,
    errorStageMessage: 'main data load',
    removeRequest: {
      table: 'general',
      sectionType: 'loadMainData'
    },
    callback: {
      invokeCallback: loadDataSaved
    }
  })
}

export default loadMainData
