import { takeLatest } from 'redux-saga/effects'

import loadMainData from './loadMainData'
import createMap from './createMap'
import deleteMap from './deleteMap'
import renameMap from './renameMap'
import publishMap from './publishMap'
import leaveMap from './leaveMap'
import fetchStaff from './fetchStaff'
import updateStaff from './updateStaff'
import fetchUser from './fetchUser'

import {
  LOAD_MAPS_DATA_ATTEMPT,
  DELETE_MAP_ATTEMPT,
  CREATE_MAP_ATTEMPT,
  CHANGE_MAP_NAME_ATTEMPT,
  SWITCHER_TOGGLE_ATTEMPT,
  LEAVE_MAP_ATTEMPT,
  MAP_SHARE_ATTEMPT,
  FETCH_STAFF_ATTEMPT,
  FETCH_USER_ATTEMPT
} from '../../constants'

export default function* rootSaga() {
  yield takeLatest(LOAD_MAPS_DATA_ATTEMPT, loadMainData)
  yield takeLatest(CREATE_MAP_ATTEMPT, createMap)
  yield takeLatest(DELETE_MAP_ATTEMPT, deleteMap)
  yield takeLatest(CHANGE_MAP_NAME_ATTEMPT, renameMap)
  yield takeLatest(SWITCHER_TOGGLE_ATTEMPT, publishMap)
  yield takeLatest(LEAVE_MAP_ATTEMPT, leaveMap)
  yield takeLatest(FETCH_STAFF_ATTEMPT, fetchStaff)
  yield takeLatest(MAP_SHARE_ATTEMPT, updateStaff)
  yield takeLatest(FETCH_USER_ATTEMPT, fetchUser)
}
