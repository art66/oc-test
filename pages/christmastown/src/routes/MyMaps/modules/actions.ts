import {
  LOAD_MAPS_DATA_ATTEMPT,
  MAPS_DATA_SAVED,
  NEW_MAP_ON_HOVER,
  NEW_MAP_ON_BLUR,
  NEW_MAP_SAVED,
  NEW_MAP_CANCELED,
  NEW_INVITES_ADDED,
  SWITCHER_TOGGLE_ATTEMPT,
  SWITCHER_TOGGLE_SAVED,
  CHANGE_MAP_NAME_START,
  CHANGE_MAP_NAME_ATTEMPT,
  CHANGE_MAP_NAME_CANCEL,
  CHANGE_MAP_NAME_SAVED,
  CANCEL_DELETE_MAP,
  DELETE_MAP_ATTEMPT,
  MAP_DELETED,
  CANCEL_SHARE_MAP,
  MAP_SHARE_ATTEMPT,
  MAP_SHARE_UPDATED,
  CANCEL_LEAVE_MAP,
  LEAVE_MAP_ATTEMPT,
  MAP_LEAVED,
  MEMBER_DELETED,
  CANCEL_ADD_MEMBER,
  ADD_MEMBER,
  MEMBER_ADDED,
  CHANGE_MEMBER_ROLE,
  CREATE_MAP_START,
  CREATE_MAP_ATTEMPT,
  SYNCHRONIZE_PLAYERS,
  FETCH_STAFF_ATTEMPT,
  FETCH_STAFF_CANCEL,
  FETCH_STAFF_SAVED,
  REMOVE_REQUEST_STAGE,
  FETCH_USER_ATTEMPT,
  FETCH_USER_SAVED,
  CHANGE_INVITES_POSITION,
  INVITE_REMOVED,
  INVITES_MAP_NAME_CHANGED,
  INVITES_MAP_REMOVED,
  INVITES_MAP_PUBLISHED_CHANGED,
  INVITES_MAP_STAFF_STATE_UPDATED,
  OWNER_MAP_STAFF_STATE_UPDATED,
  HIGHLIGHT_JOINED_MAP,
  USER_BANNED_STATUS
} from '../constants'
import {
  IType,
  IAsyncRequest,
  ILoadDataAttempt,
  ILoadDataSaved,
  ICreateNewMapStart,
  ICreateNewMapAttempt,
  INewMapHover,
  INewMapSaved,
  INewMapInvited,
  INewMapCanceled,
  IMapNameChangeStart,
  IMapNameChangeAttempt,
  IMapNameChangeCanceled,
  IMapNameChangeSaved,
  IInvitesMapNameChangeSaved,
  IInvitesPublishedChanged,
  ISwitcherToggleAttempt,
  ISwitcherToggle,
  IManageMap,
  ILeaveMap,
  ILeaveMapCancel,
  ILeaveMapAttempt,
  IShareMapAttempt,
  IShareMap,
  IShareMapCancel,
  IDeleteMap,
  IDeleteMapAttempt,
  IDeleteMapCancel,
  IAddMember,
  IDeleteMember,
  ISaveMember,
  IChangeMemberRole,
  ISynchronizePlayers,
  ICurrentStaffTab,
  IUsersList,
  IUserFetchedAttempt,
  IUserFetched,
  IRemoveRequestedStage,
  IChangeInvitesPosition,
  IInviteRemoved,
  IInvitesMapRemoved,
  IInvitesMapStaffUpdate,
  IOwnerMapStaffUpdate,
  IJoinedMapHighlight,
  IBanned
} from './types/actions'

export const loadDataAttempt = (isRequested): ILoadDataAttempt & IType => ({
  isRequested,
  type: LOAD_MAPS_DATA_ATTEMPT
})

export const loadDataSaved = (payload, isRequested): ILoadDataSaved & IType => ({
  payload,
  isRequested,
  type: MAPS_DATA_SAVED
})

export const createNewMapStart = (newMapHovered, newMapInProgress): ICreateNewMapStart & IType => ({
  newMapHovered,
  newMapInProgress,
  type: CREATE_MAP_START
})

export const createNewMapAttempt = (isRequested, newMapName): ICreateNewMapAttempt & IType => ({
  isRequested,
  newMapName,
  type: CREATE_MAP_ATTEMPT
})

export const newMapOnHover = (newMapHovered): INewMapHover & IType => ({
  newMapHovered,
  type: NEW_MAP_ON_HOVER
})

export const newMapOnBlur = (newMapHovered): INewMapHover & IType => ({
  newMapHovered,
  type: NEW_MAP_ON_BLUR
})

export const saveNewMap = (newMapInProgress, isRequested, newMap): INewMapSaved & IType => ({
  newMapInProgress,
  isRequested,
  newMap,
  type: NEW_MAP_SAVED
})

export const saveInvitedMap = (mapData): INewMapInvited & IType => ({
  mapData,
  type: NEW_INVITES_ADDED
})

export const cancelNewMap = (isRequested, newMapInProgress): INewMapCanceled & IType => ({
  isRequested,
  newMapInProgress,
  type: NEW_MAP_CANCELED
})

export const changeMapNameStart = (changeMapNameInProgress, ID): IMapNameChangeStart & IType => ({
  ID,
  changeMapNameInProgress,
  type: CHANGE_MAP_NAME_START
})

export const changeMapNameAttempt = (isRequested, ID, newMapName): IMapNameChangeAttempt & IType => ({
  ID,
  newMapName,
  isRequested,
  type: CHANGE_MAP_NAME_ATTEMPT
})

export const changeMapNameCancel = (changeMapNameInProgress, ID): IMapNameChangeCanceled & IType => ({
  ID,
  changeMapNameInProgress,
  type: CHANGE_MAP_NAME_CANCEL
})

export const renameMap = (
  uuid,
  lastEdit,
  newName,
  isRequested,
  changeMapNameInProgress
): IMapNameChangeSaved & IType => ({
  uuid,
  lastEdit,
  newName,
  isRequested,
  changeMapNameInProgress,
  type: CHANGE_MAP_NAME_SAVED
})

export const invitesMapNameChanged = (mapID, newName): IInvitesMapNameChangeSaved & IType => ({
  mapID,
  newName,
  type: INVITES_MAP_NAME_CHANGED
})

export const invitesMapRemoved = (mapID): IInvitesMapRemoved & IType => ({
  mapID,
  type: INVITES_MAP_REMOVED
})

export const invitesMapPublishedChanged = (mapID, published): IInvitesPublishedChanged & IType => ({
  mapID,
  published,
  type: INVITES_MAP_PUBLISHED_CHANGED
})

export const invitesMapStaffUpdate = (mapID, testers, editors): IInvitesMapStaffUpdate & IType => ({
  mapID,
  testers,
  editors,
  type: INVITES_MAP_STAFF_STATE_UPDATED
})

export const ownerMapStaffUpdate = (mapID, testers, editors, newStaffState): IOwnerMapStaffUpdate & IType => ({
  mapID,
  testers,
  editors,
  newStaffState,
  type: OWNER_MAP_STAFF_STATE_UPDATED
})

export const switcherToggleAttempt = (uuid, rowID, releaseRequest, publishStatus): ISwitcherToggleAttempt & IType => ({
  uuid,
  rowID,
  releaseRequest,
  publishStatus,
  type: SWITCHER_TOGGLE_ATTEMPT
})

export const switcherToggle = (payload, releaseRequest): ISwitcherToggle & IType => ({
  uuid: payload.mapID,
  lastEdit: payload.lastEdit,
  releaseRequest,
  publishStatus: payload.published,
  type: SWITCHER_TOGGLE_SAVED
})

export const manageMap = (payload): IManageMap & IType => ({
  rowID: payload.rowID,
  mediaType: payload.mediaType,
  manageType: payload.manageType,
  manageMapInProgress: payload.manageMapInProgress,
  mobileLayout: payload.mobileLayout,
  type: `myMaps/MANAGE_MAP_${payload.manageType}`
})

export const deleteMapCancel = (manageMapInProgress, isRequested, rowID): IDeleteMapCancel & IType => ({
  rowID: rowID,
  isRequested,
  manageMapInProgress,
  type: CANCEL_DELETE_MAP
})

export const deleteMapAttempt = (isRequested, rowID, mapID): IDeleteMapAttempt & IType => ({
  rowID,
  mapID,
  isRequested,
  type: DELETE_MAP_ATTEMPT
})

export const deleteMap = (manageMapInProgress, isRequested, mapID): IDeleteMap & IType => ({
  mapID,
  isRequested,
  manageMapInProgress: manageMapInProgress,
  type: MAP_DELETED
})

export const shareMapCancel = (manageMapInProgress, rowID): IShareMapCancel & IType => ({
  rowID: rowID,
  manageMapInProgress: manageMapInProgress,
  type: CANCEL_SHARE_MAP
})

export const shareMapAttempt = (isRequested, rowID): IShareMapAttempt & IType => ({
  rowID,
  isRequested,
  type: MAP_SHARE_ATTEMPT
})

export const shareMap = (manageMapInProgress, lastEdit, isRequested, membersList): IShareMap & IType => ({
  membersList,
  lastEdit,
  isRequested,
  manageMapInProgress: manageMapInProgress,
  type: MAP_SHARE_UPDATED
})

export const leaveMapCancel = (manageMapInProgress, isRequested, rowID): ILeaveMapCancel & IType => ({
  rowID,
  isRequested,
  manageMapInProgress,
  type: CANCEL_LEAVE_MAP
})

export const leaveMapAttempt = (isRequested, uuid): ILeaveMapAttempt & IType => ({
  uuid,
  isRequested,
  type: LEAVE_MAP_ATTEMPT
})

export const leaveMap = (manageMapInProgress, isRequested, uuid): ILeaveMap & IType => ({
  uuid,
  isRequested,
  manageMapInProgress,
  type: MAP_LEAVED
})

export const deleteMember = (memberID, memberType, rowID): IDeleteMember & IType => ({
  rowID,
  memberID,
  memberType,
  type: MEMBER_DELETED
})

export const addMemberCancel = (addMemberInProgress, rowID, isRequested): IAddMember & IType => ({
  rowID,
  isRequested,
  addMemberInProgress,
  type: CANCEL_ADD_MEMBER
})

export const addMember = (addMemberInProgress, rowID, isRequested): IAddMember & IType => ({
  rowID,
  isRequested,
  addMemberInProgress,
  type: ADD_MEMBER
})

export const memberAdded = (addMemberInProgress, rowID, member): ISaveMember & IType => ({
  rowID,
  addMemberInProgress,
  member,
  type: MEMBER_ADDED
})

export const changeMemberRole = (member, rowID): IChangeMemberRole & IType => ({
  rowID,
  member,
  type: CHANGE_MEMBER_ROLE
})

export const synchronizePlayers = (mapID, playersOnline): ISynchronizePlayers & IType => ({
  mapID,
  playersOnline,
  type: SYNCHRONIZE_PLAYERS
})

export const staffTabClickAttempt = (currentTab, isRequested): ICurrentStaffTab & IType => ({
  currentTab,
  isRequested,
  type: FETCH_STAFF_ATTEMPT
})

export const preventFetchUsersAttempt = (isRequested): IAsyncRequest & IType => ({
  isRequested,
  type: FETCH_STAFF_CANCEL
})

export const saveUsersList = (usersList, isRequested, usersType): IUsersList & IType => ({
  usersList,
  usersType,
  isRequested,
  type: FETCH_STAFF_SAVED
})

export const saveNewUserAttempt = (searchText, isRequested): IUserFetchedAttempt & IType => ({
  searchText,
  isRequested,
  type: FETCH_USER_ATTEMPT
})

export const saveNewUser = (newUsersFetched, isRequested, usersType): IUserFetched & IType => ({
  newUsersFetched,
  usersType,
  isRequested,
  type: FETCH_USER_SAVED
})

export const changeInvitePosition = (mapID, role): IChangeInvitesPosition & IType => ({
  mapID,
  role,
  type: CHANGE_INVITES_POSITION
})

export const inviteRemoved = (ID): IInviteRemoved & IType => ({
  ID,
  type: INVITE_REMOVED
})

export const removeRequestStage = ({ table, sectionType }): IRemoveRequestedStage & IType => ({
  table,
  sectionType,
  isRequested: false,
  type: REMOVE_REQUEST_STAGE
})

export const highlightJoinedMap = ({ userActiveMapID }): IJoinedMapHighlight & IType => ({
  userActiveMapID,
  type: HIGHLIGHT_JOINED_MAP
})

export const userBanned = ({ isUserBanned }): IBanned & IType => ({
  isUserBanned,
  type: USER_BANNED_STATUS
})
