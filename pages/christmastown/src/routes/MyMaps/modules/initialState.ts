// const MONTH_NAMES = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const initialState = {
  general: {
    isBanned: false,
    activeJoinedMapID: undefined,
    loadMainData: {
      isRequested: false
    }
  },
  mymaps: {
    appID: 1,
    title: 'My Maps',
    activeRowID: null,
    controlData: {
      maxEditors: 10,
      maxTesters: 25
    },
    createMap: {
      type: 'addNew',
      newMapHovered: false,
      newMapInProgress: false,
      isRequested: false
    },
    changeName: {
      changeMapNameID: null,
      isRequested: false,
      changeMapNameInProgress: false
    },
    searchUser: {
      isRequested: false
    },
    manageMap: {
      manageRowID: null,
      manageType: '',
      mobileLayout: false,
      manageMapInProgress: false
    },
    publishMap: {
      publishMapID: null,
      isRequested: false
    },
    addMember: {
      currentTab: 'ac-ct-all',
      addMemberRowID: null,
      isRequested: false,
      usersList: {
        all: [],
        friends: [],
        companies: [],
        faction: []
      },
      addMemberInProgress: false
    },
    inviteUsersList: {
      all: null,
      // [
      // {
      //   id: 2122332121112,
      //   // category: '',
      //   online: 'offline',
      //   type: null,
      //   name: 'Forex'
      // }
      // ],
      friends: null,
      // [
      // {
      //   id: 234343434,
      //   // category: '',
      //   online: 'offline',
      //   type: null,
      //   name: 'Domesa'
      // },
      // {
      //   id: 232334321,
      //   // category: 'friends',
      //   online: 'online',
      //   type: null,
      //   name: 'Alex'
      // },
      // {
      //   id: 234234232,
      //   // category: 'friends',
      //   online: 'offline',
      //   type: null,
      //   name: 'Saeex'
      // }
      // ],
      company: null,
      // [
      // {
      //   id: 23423322223,
      //   // category: 'company',
      //   online: 'online',
      //   type: null,
      //   name: 'Christ'
      // }
      // ],
      faction: null
      // [
      //   {
      //     id: 23423432432424234,
      //     // category: 'faction',
      //     online: 'offline',
      //     type: null,
      //     name: 'Fax'
      //   },
      //   {
      //     id: 23,
      //     // category: 'faction',
      //     online: 'online',
      //     type: null,
      //     name: 'Max'
      //   }
      // ]
    },
    rows: null
  },
  invites: {
    appID: 2,
    title: 'Invites',
    activeRowID: null,
    manageMap: {
      manageRowID: null,
      manageMapInProgress: false
    },
    rows: null
  }
}

export default initialState

// const rowsMyMapsMocks = {
// rows: [
// {
//   ID: 1,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   requests: {
//     isReleaseRequest: false
//   },
//   temp: {
//     membersList: row.manage.staff
//   },
//   sections: {
//     name: 'Mikssse',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: true,
//     rating: 2.35,
//     manage: {
//       editorsOnMap: 1,
//       testersOnMap: 1,
//       joinLink: 'torn.com/join?sfsdf22221sfs322',
//       inviteLink: 'torn.com/invite?df123123df',
//       membersList: [
//         {
//           ID: 12345,
//           type: 'tester',
//           name: 'Alex'
//         },
//         {
//           ID: 1231313,
//           type: 'editor',
//           name: 'Sviatolav'
//         }
//       ]
//     }
//   }
// },
// {
//   ID: 2,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   requests: {
//     isReleaseRequest: false
//   },
//   temp: {
//     membersList: row.manage.staff
//   },
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: false,
//     rating: 2.35,
//     manage: {
//       editorsOnMap: 1,
//       testersOnMap: 1,
//       joinLink: 'torn.com/join?sfsdf22221sfs322',
//       inviteLink: 'torn.com/invite?df123123df',
//       membersList: [
//         {
//           ID: 12345,
//           type: 'tester',
//           name: 'Alex'
//         },
//         {
//           ID: 1231313,
//           type: 'editor',
//           name: 'Sviatolav'
//         }
//       ]
//     }
//   }
// },
// {
//   ID: 3,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   requests: {
//     isReleaseRequest: false
//   },
//   temp: {
//     membersList: row.manage.staff
//   },
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: true,
//     rating: 2.35,
//     manage: {
//       editorsOnMap: 1,
//       testersOnMap: 1,
//       joinLink: 'torn.com/join?sfsdf22221sfs322',
//       inviteLink: 'torn.com/invite?df123123df',
//       membersList: [
//         {
//           ID: 12345,
//           type: 'tester',
//           name: 'Alex'
//         },
//         {
//           ID: 1231313,
//           type: 'editor',
//           name: 'Sviatolav'
//         }
//       ]
//     }
//   }
// }
// ]
// }

// const rowInvitesMocks = [
// {
//   ID: 1,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: false,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// },
// {
//   ID: 2,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: true,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// },
// {
//   ID: 3,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: false,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// },
// {
//   ID: 4,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: true,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// },
// {
//   ID: 5,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: true,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// },
// {
//   ID: 6,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: false,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// },
// {
//   ID: 7,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: true,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// },
// {
//   ID: 8,
//   uuid: '06592886-24af-4e71-9cca-8328ab72d70a',
//   sections: {
//     name: 'Mike',
//     lastEdit: `${MONTH_NAMES[new Date(1564755908).getMonth()]} ${new Date(1564755908).getDay()}`,
//     editors: 3223,
//     testers: 23231,
//     players: 121 || null,
//     published: false,
//     rating: 2.35,
//     leave: {
//       position: 'tester',
//       leaveLink: '/leave?123sfdsq34sf'
//     }
//   }
// }
// ]
