import isValue from '@torn/shared/utils/isValue'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import initialState from './initialState'
import getDate from '../utils/getDate'
import getStaffCount from '../utils/getStaffCount'
import getUsersList from '../utils/getUsersList'
import updateStaffCount from '../utils/updateStaffCount'
import checkToggleClick from '../utils/checkToggleClick'
import mapRowDataNormalizer from '../utils/mapRowDataNormalizer'
// import checkIsMapExist from '../utils/checkIsMapExist'

import {
  LOAD_MAPS_DATA_ATTEMPT,
  MAPS_DATA_SAVED,
  ID_INCREMENTOR,
  MANAGE_STAFF_NORMALIZER,
  CREATE_MAP_START,
  CREATE_MAP_ATTEMPT,
  NEW_MAP_ON_HOVER,
  NEW_MAP_ON_BLUR,
  NEW_MAP_SAVED,
  NEW_MAP_CANCELED,
  NEW_INVITES_ADDED,
  SWITCHER_TOGGLE_ATTEMPT,
  SWITCHER_TOGGLE_SAVED,
  CHANGE_MAP_NAME_START,
  CHANGE_MAP_NAME_ATTEMPT,
  CHANGE_MAP_NAME_CANCEL,
  CHANGE_MAP_NAME_SAVED,
  INVITES_MAP_NAME_CHANGED,
  DELETE_MAP_ATTEMPT,
  DELETE_MAP,
  CANCEL_DELETE_MAP,
  MAP_DELETED,
  SHARE_MAP,
  CANCEL_SHARE_MAP,
  MEMBER_DELETED,
  MAP_SHARE_ATTEMPT,
  MAP_SHARE_UPDATED,
  JOIN_MAP,
  SETTINGS_MAP,
  LEAVE_MAP,
  LEAVE_MAP_ATTEMPT,
  CANCEL_LEAVE_MAP,
  MAP_LEAVED,
  CANCEL_ADD_MEMBER,
  ADD_MEMBER,
  MEMBER_ADDED,
  RENAME_MAP,
  CHANGE_MEMBER_ROLE,
  SYNCHRONIZE_PLAYERS,
  FETCH_STAFF_ATTEMPT,
  FETCH_STAFF_CANCEL,
  FETCH_STAFF_SAVED,
  REMOVE_REQUEST_STAGE,
  FETCH_USER_ATTEMPT,
  FETCH_USER_SAVED,
  CHANGE_INVITES_POSITION,
  INVITE_REMOVED,
  INVITES_MAP_REMOVED,
  INVITES_MAP_PUBLISHED_CHANGED,
  INVITES_MAP_STAFF_STATE_UPDATED,
  OWNER_MAP_STAFF_STATE_UPDATED,
  HIGHLIGHT_JOINED_MAP,
  USER_BANNED_STATUS
} from '../constants'
import {
  IState,
  ILoadDataAttempt,
  ILoadDataSaved,
  ICreateNewMapStart,
  ICreateNewMapAttempt,
  INewMapHover,
  INewMapSaved,
  INewMapCanceled,
  INewMapInvited,
  IMapNameChangeStart,
  IMapNameChangeAttempt,
  IMapNameChangeCanceled,
  IMapNameChangeSaved,
  IInvitesMapNameChangeSaved,
  ISwitcherToggleAttempt,
  ISwitcherToggle,
  IManageMap,
  ILeaveMap,
  ILeaveMapAttempt,
  IAddMember,
  ISaveMember,
  IDeleteMember,
  IShareMapAttempt,
  IShareMap,
  IDeleteMap,
  IShareMapCancel,
  IDeleteMapCancel,
  IChangeMemberRole,
  ISynchronizePlayers,
  ICurrentStaffTab,
  IUsersList,
  IRemoveRequestedStage,
  IAsyncRequest,
  IUserFetchedAttempt,
  IUserFetched,
  IChangeInvitesPosition,
  IInviteRemoved,
  IInvitesMapRemoved,
  IInvitesPublishedChanged,
  IInvitesMapStaffUpdate,
  IOwnerMapStaffUpdate,
  IJoinedMapHighlight,
  IBanned
} from './types/actions'

const ACTION_HANDLERS = {
  [LOAD_MAPS_DATA_ATTEMPT]: (state: IState, action: ILoadDataAttempt) => ({
    ...state,
    general: {
      ...state.general,
      loadMainData: {
        ...state.general.loadMainData,
        isRequested: action.isRequested
      }
    }
  }),
  [MAPS_DATA_SAVED]: (state: IState, action: ILoadDataSaved) => ({
    ...state,
    general: {
      ...state.general,
      activeJoinedMapID: action.payload.settings.userActiveMapID,
      isBanned: action.payload.settings.isUserBanned,
      loadMainData: {
        ...state.general.loadMainData,
        isRequested: action.isRequested
      }
    },
    mymaps: {
      ...state.mymaps,
      controlData: action.payload.myMaps.controlData,
      rows: action.payload.myMaps.list.map((row, index) => ({
        ID: index,
        uuid: row.ID,
        createdAt: row.createdAt,
        temp: {
          membersList: row.manage.staff
        },
        sections: {
          name: row.name,
          lastEdit: getDate(row.lastEdit),
          editors: row.editors,
          testers: row.testers,
          players: row.playersOnline,
          published: row.published,
          rating: row.rating,
          manage: {
            joinLink: row.manage.joinLink,
            inviteLink: row.manage.joinLink,
            editorsOnMap: row.editors,
            testersOnMap: row.testers,
            membersList: row.manage.staff
          }
        }
      }))
    },
    invites: {
      ...state.invites,
      rows: action.payload.myInvites.list.map((row, index) => ({
        ID: index,
        uuid: row.ID,
        sections: {
          name: row.name,
          lastEdit: getDate(row.lastEdit),
          editors: row.editors,
          testers: row.testers,
          players: row.players,
          published: row.published,
          rating: row.rating,
          leave: {
            position: row.position,
            joinLink: row.link
          }
        }
      }))
    }
  }),
  [CREATE_MAP_START]: (state: IState, action: ICreateNewMapStart) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      createMap: {
        ...state.mymaps.createMap,
        newMapHovered: action.newMapHovered,
        newMapInProgress: action.newMapInProgress
      }
    }
  }),
  [CREATE_MAP_ATTEMPT]: (state: IState, action: ICreateNewMapAttempt) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      createMap: {
        ...state.mymaps.createMap,
        isRequested: action.isRequested
      }
    }
  }),
  [NEW_MAP_ON_HOVER]: (state: IState, action: INewMapHover) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      createMap: {
        ...state.mymaps.createMap,
        newMapHovered: action.newMapHovered
      }
    }
  }),
  [NEW_MAP_ON_BLUR]: (state: IState, action: INewMapHover) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      createMap: {
        ...state.mymaps.createMap,
        newMapHovered: action.newMapHovered
      }
    }
  }),
  [NEW_MAP_SAVED]: (state: IState, action: INewMapSaved) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      // activeRowID: state.mymaps.activeRowID + ID_INCREMENTOR,
      createMap: {
        ...state.mymaps.createMap,
        newMapInProgress: action.newMapInProgress,
        isRequested: action.isRequested
      },
      manageMap: {
        ...state.mymaps.manageMap,
        manageRowID: state.mymaps.manageMap.manageRowID + ID_INCREMENTOR
      },
      rows: [
        ...state.mymaps.rows,
        {
          ID: state.mymaps.rows.length,
          uuid: action.newMap.ID || null,
          createdAt: Date.now(),
          temp: {
            membersList: []
          },
          sections: {
            name: action.newMap.name || null,
            lastEdit: getDate(action.newMap.lastEdit),
            editors: 0,
            testers: 0,
            players: action.newMap.playersOnline || null,
            published: action.newMap.published || false,
            rating: action.newMap.rating || null,
            manage: {
              joinLink: action.newMap.manage.joinLink || null,
              inviteLink: action.newMap.manage.joinLink || null,
              editorsOnMap: 0,
              testersOnMap: 0,
              membersList: []
            }
          }
        }
      ]
    }
  }),
  [NEW_MAP_CANCELED]: (state: IState, action: INewMapCanceled) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      createMap: {
        ...state.mymaps.createMap,
        newMapInProgress: action.newMapInProgress,
        isRequested: action.isRequested
      }
    }
  }),
  [NEW_INVITES_ADDED]: (state: IState, action: INewMapInvited) => ({
    ...state,
    invites: {
      ...state.invites,
      rows: [...state.invites.rows, mapRowDataNormalizer(state.invites.rows.length, action.mapData, 2)]
    }
  }),
  [CHANGE_MAP_NAME_START]: (state: IState, action: IMapNameChangeStart) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: action.ID,
      changeName: {
        ...state.mymaps.changeName,
        changeMapNameID: action.ID,
        changeMapNameInProgress: action.changeMapNameInProgress
      },
      manageMap: {
        ...state.mymaps.manageMap,
        manageType: 'RENAME',
        manageRowID: action.ID,
        manageMapInProgress: true
      }
    }
  }),
  [CHANGE_MAP_NAME_ATTEMPT]: (state: IState, action: IMapNameChangeAttempt) => {
    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        changeName: {
          ...state.mymaps.changeName,
          isRequested: action.isRequested
        }
      }
    }
  },
  [CHANGE_MAP_NAME_CANCEL]: (state: IState, action: IMapNameChangeCanceled) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: null,
      changeName: {
        ...state.mymaps.changeName,
        changeMapNameID: null,
        changeMapNameInProgress: action.changeMapNameInProgress
      },
      manageMap: {
        ...state.mymaps.manageMap,
        manageType: ''
      }
    }
  }),
  [CHANGE_MAP_NAME_SAVED]: (state: IState, action: IMapNameChangeSaved) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: null,
      manageMap: {
        ...state.mymaps.manageMap,
        manageType: ''
      },
      changeName: {
        ...state.mymaps.changeName,
        changeMapNameID: null,
        changeMapNameInProgress: action.changeMapNameInProgress,
        isRequested: action.isRequested
      },
      rows: state.mymaps.rows.map(row =>
        row.uuid === action.uuid
          ? {
              ...row,
              sections: {
                ...row.sections,
                name: action.newName
                // lastEdit: getMonthDay(action.lastEdit)
              }
            }
          : row
      )
    }
  }),
  [INVITES_MAP_NAME_CHANGED]: (state: IState, action: IInvitesMapNameChangeSaved) => ({
    ...state,
    invites: {
      ...state.invites,
      rows: state.invites.rows.map(row =>
        row.uuid === action.mapID
          ? {
              ...row,
              sections: {
                ...row.sections,
                name: action.newName
              }
            }
          : row
      )
    }
  }),
  [SWITCHER_TOGGLE_ATTEMPT]: (state: IState, action: ISwitcherToggleAttempt) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      publishMap: {
        ...state.mymaps.publishMap,
        publishMapID: action.rowID,
        isRequested: action.releaseRequest
      }
    }
  }),
  [SWITCHER_TOGGLE_SAVED]: (state: IState, action: ISwitcherToggle) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: undefined,
      publishMap: {
        ...state.mymaps.publishMap,
        isRequested: action.releaseRequest
      },
      rows: state.mymaps.rows.map(row =>
        row.uuid === action.uuid
          ? {
              ...row,
              sections: {
                ...row.sections,
                published: action.publishStatus,
                players: row.sections.players || 0
                // lastEdit: getMonthDay(action.lastEdit)
              }
            }
          : row
      )
    }
  }),
  [DELETE_MAP_ATTEMPT]: (state: IState, action: any) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      manageMap: {
        ...state.mymaps.manageMap,
        isRequested: action.isRequested
      }
    }
  }),
  [DELETE_MAP]: (state: IState, action: IManageMap) => {
    const { isDesktop } = checkMediaType(action.mediaType)
    const isToggledClick = checkToggleClick(action.manageType, state.mymaps.manageMap.manageType, action.mobileLayout)

    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        activeRowID: action.rowID,
        manageMap: {
          ...state.mymaps.manageMap,
          manageRowID: action.rowID,
          manageType: !isDesktop && isToggledClick ? '' : action.manageType,
          mobileLayout: action.mobileLayout,
          manageMapInProgress: action.manageMapInProgress
        }
      }
    }
  },
  [CANCEL_DELETE_MAP]: (state: IState, action: IDeleteMapCancel) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: null,
      manageMap: {
        ...state.mymaps.manageMap,
        manageRowID: null,
        mobileLayout: false,
        manageMapInProgress: action.manageMapInProgress
      }
    }
  }),
  [MAP_DELETED]: (state: IState, action: IDeleteMap) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: null,
      manageMap: {
        ...state.mymaps.manageMap,
        mobileLayout: false,
        manageMapInProgress: action.manageMapInProgress,
        isRequested: action.isRequested
      },
      rows: state.mymaps.rows.filter(row => row.uuid !== action.mapID)
    }
  }),
  [RENAME_MAP]: (state: IState, action: IManageMap) => {
    const isToggledClick = checkToggleClick(action.manageType, state.mymaps.manageMap.manageType, action.mobileLayout)

    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        activeRowID: action.rowID,
        changeName: {
          ...state.mymaps.changeName,
          changeMapNameID: !isToggledClick ? action.rowID : null,
          changeMapNameInProgress: !isToggledClick
        },
        manageMap: {
          ...state.mymaps.manageMap,
          manageRowID: action.rowID,
          manageType: !isToggledClick ? action.manageType : '',
          mobileLayout: action.mobileLayout,
          manageMapInProgress: action.manageMapInProgress
        }
      }
    }
  },
  [SHARE_MAP]: (state: IState, action: IManageMap) => {
    const isCurrentRow = action.rowID === state.mymaps.manageMap.manageRowID
    const isManageTypeSame = checkToggleClick(action.manageType, state.mymaps.manageMap.manageType, action.mobileLayout)
    const isToggledClick = isCurrentRow && isManageTypeSame

    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        activeRowID: action.rowID,
        manageMap: {
          ...state.mymaps.manageMap,
          manageRowID: action.rowID,
          manageType: !isToggledClick ? action.manageType : '',
          mobileLayout: action.mobileLayout,
          manageMapInProgress: action.manageMapInProgress
        }
      }
    }
  },
  [CANCEL_SHARE_MAP]: (state: IState, action: IShareMapCancel) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: null,
      manageMap: {
        ...state.mymaps.manageMap,
        manageRowID: action.rowID,
        manageType: '',
        mobileLayout: false,
        isRequested: false,
        manageMapInProgress: action.manageMapInProgress
      },
      rows: state.mymaps.rows.map(row =>
        row.ID === state.mymaps.manageMap.manageRowID
          ? {
              ...row,
              sections: {
                ...row.sections,
                manage: {
                  ...row.sections.manage,
                  ...getStaffCount(row.temp.membersList),
                  membersList: row.temp.membersList
                }
              }
            }
          : row
      )
    }
  }),
  [MAP_SHARE_ATTEMPT]: (state: IState, action: IShareMapAttempt) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      manageMap: {
        ...state.mymaps.manageMap,
        isRequested: action.isRequested
      }
    }
  }),
  [MAP_SHARE_UPDATED]: (state: IState, action: IShareMap) => {
    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        activeRowID: null,
        addMember: {
          ...state.mymaps.addMember,
          manageRowID: null,
          addMemberRowID: null,
          manageMapInProgress: false
        },
        manageMap: {
          ...state.mymaps.manageMap,
          manageRowID: null,
          mobileLayout: false,
          isRequested: action.isRequested,
          manageMapInProgress: action.manageMapInProgress
        },
        rows: state.mymaps.rows.map(row =>
          row.ID === state.mymaps.manageMap.manageRowID
            ? {
                ...row,
                temp: {
                  membersList: action.membersList
                },
                sections: {
                  ...row.sections,
                  ...getStaffCount(action.membersList, 'testers', 'editors'),
                  // lastEdit: getMonthDay(action.lastEdit),
                  manage: {
                    ...row.sections.manage,
                    ...getStaffCount(action.membersList),
                    membersList: action.membersList
                  }
                }
              }
            : row
        )
      }
    }
  },
  [JOIN_MAP]: (state: IState, action: IManageMap) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      activeRowID: action.rowID,
      manageMap: {
        ...state.mymaps.manageMap,
        manageRowID: action.rowID,
        manageType: action.manageType,
        mobileLayout: action.mobileLayout,
        manageMapInProgress: action.manageMapInProgress
      }
    }
  }),
  [SETTINGS_MAP]: (state: IState, action: IManageMap) => {
    const isToggledClick = checkToggleClick(action.manageType, state.mymaps.manageMap.manageType, action.mobileLayout)

    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        activeRowID: action.rowID,
        changeName: {
          ...state.mymaps.changeName,
          changeMapNameID: !isToggledClick ? action.rowID : null
        },
        manageMap: {
          ...state.mymaps.manageMap,
          manageRowID: action.rowID,
          manageType: '',
          manageMapInProgress: action.manageMapInProgress,
          mobileLayout: action.mobileLayout
        }
      }
    }
  },
  [LEAVE_MAP]: (state: IState, action: IManageMap) => ({
    ...state,
    invites: {
      ...state.invites,
      activeRowID: action.rowID,
      manageMap: {
        ...state.mymaps.manageMap,
        manageRowID: action.rowID,
        manageType: action.manageType,
        mobileLayout: action.mobileLayout,
        manageMapInProgress: action.manageMapInProgress
      }
    }
  }),
  [LEAVE_MAP_ATTEMPT]: (state: IState, action: ILeaveMapAttempt) => ({
    ...state,
    invites: {
      ...state.invites,
      manageMap: {
        ...state.invites.manageMap,
        isRequested: action.isRequested
      }
    }
  }),
  [CANCEL_LEAVE_MAP]: (state: IState, action: IManageMap) => ({
    ...state,
    invites: {
      ...state.invites,
      activeRowID: null,
      manageMap: {
        ...state.invites.manageMap,
        manageRowID: action.rowID,
        manageType: '',
        isRequested: action.isRequested,
        manageMapInProgress: action.manageMapInProgress
      }
    }
  }),
  [MAP_LEAVED]: (state: IState, action: ILeaveMap) => ({
    ...state,
    invites: {
      ...state.invites,
      activeRowID: null,
      manageMap: {
        ...state.invites.manageMap,
        isRequested: action.isRequested,
        manageMapInProgress: action.manageMapInProgress
      },
      rows: state.invites.rows.filter(row => row.uuid !== action.uuid)
    }
  }),
  [ADD_MEMBER]: (state: IState, action: IAddMember) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      addMember: {
        ...state.mymaps.addMember,
        isRequested: action.isRequested,
        addMemberRowID: action.rowID,
        addMemberInProgress: action.addMemberInProgress
      }
    }
  }),
  [CANCEL_ADD_MEMBER]: (state: IState, action: IAddMember) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      addMember: {
        ...state.mymaps.addMember,
        isRequested: action.isRequested,
        addMemberRowID: action.rowID,
        addMemberInProgress: action.addMemberInProgress
      }
    }
  }),
  [MEMBER_DELETED]: (state: IState, action: IDeleteMember) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      rows: state.mymaps.rows.map(row =>
        row.ID === action.rowID
          ? {
              ...row,
              sections: {
                ...row.sections,
                manage: {
                  ...row.sections.manage,
                  membersList: row.sections.manage.membersList.filter(member => member.ID !== action.memberID),
                  [MANAGE_STAFF_NORMALIZER[action.memberType]]: updateStaffCount(
                    row.sections.manage[MANAGE_STAFF_NORMALIZER[action.memberType]],
                    'minus'
                  )
                }
              }
            }
          : row
      )
    }
  }),
  [MEMBER_ADDED]: (state: IState, action: ISaveMember) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      rows: state.mymaps.rows.map(row => {
        return row.ID === action.rowID
          ? {
              ...row,
              sections: {
                ...row.sections,
                manage: {
                  ...row.sections.manage,
                  ...(!row.sections.manage.membersList.some(member => member.ID === action.member.ID)
                    ? {
                        membersList: [...row.sections.manage.membersList, action.member],
                        [MANAGE_STAFF_NORMALIZER[action.member.type]]: updateStaffCount(
                          row.sections.manage[MANAGE_STAFF_NORMALIZER[action.member.type]],
                          'plus'
                        )
                      }
                    : row.sections.manage.membersList)
                }
              }
            }
          : row
      })
    }
  }),
  [CHANGE_MEMBER_ROLE]: (state: IState, action: IChangeMemberRole) => {
    const staffUpdate = ({ sections }) => {
      const staffUpdated = {
        editor: {
          editorsOnMap: updateStaffCount(sections.manage.editorsOnMap, 'plus'),
          testersOnMap: updateStaffCount(sections.manage.testersOnMap, 'minus')
        },
        tester: {
          editorsOnMap: updateStaffCount(sections.manage.editorsOnMap, 'minus'),
          testersOnMap: updateStaffCount(sections.manage.testersOnMap, 'plus')
        }
      }

      return staffUpdated
    }

    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        rows: state.mymaps.rows.map(row =>
          row.ID === action.rowID
            ? {
                ...row,
                sections: {
                  ...row.sections,
                  manage: {
                    ...row.sections.manage,
                    membersList: row.sections.manage.membersList.map(member =>
                      member.ID === action.member.memberID
                        ? {
                            ...member,
                            type: action.member.memberType
                          }
                        : member
                    ),
                    ...staffUpdate(row)[action.member.memberType]
                  }
                }
              }
            : row
        )
      }
    }
  },
  // TODO: should be finish once Sergei made the correct API
  [SYNCHRONIZE_PLAYERS]: (state: IState, action: ISynchronizePlayers) => ({
    ...state,
    ...action
  }),
  [FETCH_STAFF_ATTEMPT]: (state: IState, action: ICurrentStaffTab) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      addMember: {
        ...state.mymaps.addMember,
        isRequested: action.isRequested,
        currentTab: isValue(action.currentTab) ? action.currentTab : state.mymaps.addMember.currentTab
      }
    }
  }),
  [FETCH_STAFF_CANCEL]: (state: IState, action: IAsyncRequest) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      addMember: {
        ...state.mymaps.addMember,
        isRequested: action.isRequested
      }
    }
  }),
  [FETCH_STAFF_SAVED]: (state: IState, action: IUsersList) => {
    return {
      ...state,
      mymaps: {
        ...state.mymaps,
        addMember: {
          ...state.mymaps.addMember,
          isRequested: action.isRequested
        },
        inviteUsersList: {
          ...state.mymaps.inviteUsersList,
          ...getUsersList({
            staffType: action.usersType,
            usersList: action.usersList
          })
        }
      }
    }
  },
  [FETCH_USER_ATTEMPT]: (state: IState, action: IUserFetchedAttempt) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      searchUser: {
        ...state.mymaps.searchUser,
        isRequested: action.isRequested
      }
    }
  }),
  [FETCH_USER_SAVED]: (state: IState, action: IUserFetched) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      searchUser: {
        ...state.mymaps.searchUser,
        isRequested: action.isRequested
      },
      inviteUsersList: {
        ...state.mymaps.inviteUsersList,
        ...getUsersList({
          staffType: state.mymaps.addMember.currentTab,
          usersList: action.newUsersFetched
        })
      }
    }
  }),
  [REMOVE_REQUEST_STAGE]: (state: IState, action: IRemoveRequestedStage) => ({
    ...state,
    [action.table]: {
      ...state[action.table],
      [action.sectionType]: {
        ...state[action.table][action.sectionType],
        isRequested: action.isRequested
      }
    }
  }),
  [CHANGE_INVITES_POSITION]: (state: IState, action: IChangeInvitesPosition) => ({
    ...state,
    invites: {
      ...state.invites,
      rows: [
        ...state.invites.rows.map(row =>
          row.uuid === action.mapID
            ? {
                ...row,
                sections: {
                  ...row.sections,
                  leave: {
                    ...row.sections.leave,
                    position: action.role
                  }
                }
              }
            : row
        )
      ]
    }
  }),
  [INVITE_REMOVED]: (state: IState, action: IInviteRemoved) => ({
    ...state,
    invites: {
      ...state.invites,
      rows: state.invites.rows.filter(row => row.uuid !== action.ID)
    }
  }),
  [INVITES_MAP_REMOVED]: (state: IState, action: IInvitesMapRemoved) => ({
    ...state,
    invites: {
      ...state.invites,
      rows: state.invites.rows.filter(row => row.uuid !== action.mapID)
    }
  }),
  [INVITES_MAP_PUBLISHED_CHANGED]: (state: IState, action: IInvitesPublishedChanged) => ({
    ...state,
    invites: {
      ...state.invites,
      rows: [
        ...state.invites.rows.map(row =>
          row.uuid === action.mapID
            ? {
                ...row,
                sections: {
                  ...row.sections,
                  published: action.published
                }
              }
            : row
        )
      ]
    }
  }),
  [INVITES_MAP_STAFF_STATE_UPDATED]: (state: IState, action: IInvitesMapStaffUpdate) => ({
    ...state,
    invites: {
      ...state.invites,
      rows: [
        ...state.invites.rows.map(row =>
          row.uuid === action.mapID
            ? {
                ...row,
                sections: {
                  ...row.sections,
                  testers: action.testers,
                  editors: action.editors
                }
              }
            : row
        )
      ]
    }
  }),
  [OWNER_MAP_STAFF_STATE_UPDATED]: (state: IState, action: IOwnerMapStaffUpdate) => ({
    ...state,
    mymaps: {
      ...state.mymaps,
      rows: [
        ...state.mymaps.rows.map(row =>
          row.uuid === action.mapID
            ? {
                ...row,
                sections: {
                  ...row.sections,
                  testers: action.testers,
                  editors: action.editors,
                  manage: {
                    ...row.sections.manage,
                    testersOnMap: action.testers,
                    editorsOnMap: action.editors,
                    membersList: action.newStaffState
                  }
                }
              }
            : row
        )
      ]
    }
  }),
  [HIGHLIGHT_JOINED_MAP]: (state: IState, action: IJoinedMapHighlight) => ({
    ...state,
    general: {
      ...state.general,
      activeJoinedMapID: action.userActiveMapID
    }
  }),
  [USER_BANNED_STATUS]: (state: IState, action: IBanned) => ({
    ...state,
    general: {
      ...state.general,
      isBanned: action.isUserBanned
    },
    mymaps: {
      ...state.mymaps,
      rows: action.isUserBanned
        ? state.mymaps.rows.map(row => ({
            ...row,
            sections: {
              ...row.sections,
              published: false
            }
          }))
        : state.mymaps.rows
    }
  })
}

const reducer = (state: object = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
