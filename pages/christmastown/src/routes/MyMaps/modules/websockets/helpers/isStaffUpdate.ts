import { STAFF_LABEL } from '../constants'

const isStaffUpdate = (role: 'staff' | 'author') => {
  return role === STAFF_LABEL
}

export default isStaffUpdate
