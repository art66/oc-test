import isValue from '@torn/shared/utils/isValue'

import {
  saveNewMap,
  saveInvitedMap,
  inviteRemoved,
  deleteMap,
  renameMap,
  invitesMapNameChanged,
  invitesMapRemoved,
  invitesMapPublishedChanged,
  invitesMapStaffUpdate,
  ownerMapStaffUpdate,
  switcherToggle,
  leaveMap,
  shareMap,
  changeInvitePosition,
  synchronizePlayers,
  highlightJoinedMap,
  userBanned
} from '../actions'

import isStaffUpdate from './helpers/isStaffUpdate'
import { AUTHOR_LABEL } from './constants'
import { debugShow } from '../../../../controller/actions'
import { BANNED_LABEL } from '../../constants'

function initWS(dispatch: any) {
  // @ts-ignore global Websocket & Centrifuge handler!!!
  const handler = new WebsocketHandler('MapsManagement')

  handler.setActions({
    NewMapCreated: payload => {
      dispatch(saveNewMap(false, false, payload.data))
    },
    MapRemoved: payload => {
      const {
        data: { role, mapID }
      } = payload

      // thanks for a brilliant back-end architecture we have here such a monster procedure-like abstraction. wtf...
      if (isStaffUpdate(role)) {
        dispatch(invitesMapRemoved(mapID))

        return
      }

      dispatch(deleteMap(false, false, payload.data.mapID))
    },
    MapRenamed: payload => {
      const {
        data: { role, mapID, newName }
      } = payload

      // thanks for a brilliant back-end architecture we have here such a monster procedure-like abstraction. wtf...
      if (isStaffUpdate(role)) {
        dispatch(invitesMapNameChanged(mapID, newName))

        return
      }

      dispatch(renameMap(payload.data.mapID, payload.data.lastEdit, payload.data.newName, false, false))
    },
    PublishedStageUpdated: payload => {
      const {
        data: { role, mapID, published }
      } = payload

      // thanks for a brilliant back-end architecture we have here such a monster procedure-like abstraction. wtf...
      if (isStaffUpdate(role)) {
        dispatch(invitesMapPublishedChanged(mapID, published))

        return
      }

      dispatch(switcherToggle(payload.data, false))
    },
    LeavePosition: payload => {
      const {
        data: { role, mapID, testers, editors, newStaffState }
      } = payload
      const isAuthor = role === AUTHOR_LABEL

      // thanks for a brilliant back-end architecture we have here such a monster procedure-like abstraction. wtf...
      if (isStaffUpdate(role)) {
        dispatch(invitesMapStaffUpdate(mapID, testers, editors))

        return
      }

      if (!isStaffUpdate(role) && isAuthor) {
        dispatch(ownerMapStaffUpdate(mapID, testers, editors, newStaffState))

        return
      }

      dispatch(leaveMap(false, false, payload.data.mapID))
    },
    MapStaffUpdated: payload => {
      const {
        data: { updateStatus, mapData, ID, newPosition }
      } = payload

      // thanks for a brilliant back-end architecture we have here such a monster procedure-like abstraction. wtf...
      if (updateStatus === 'invited') {
        dispatch(saveInvitedMap(mapData))

        return
      }

      if (updateStatus === 'removed') {
        dispatch(inviteRemoved(ID))

        return
      }

      if (updateStatus === 'positionChanged') {
        dispatch(changeInvitePosition(ID, newPosition))

        return
      }

      dispatch(shareMap(false, payload.data.lastEdit, false, payload.data.newStaffState))
    },
    MapsDataSynchronizeMessage: payload => {
      dispatch(synchronizePlayers(payload.data.ID, payload.data.onlinePlayers))
    },
    ActiveMapEditorChanged: payload => {
      dispatch(highlightJoinedMap(payload.data))
    },
    UserBanned: payload => {
      dispatch(userBanned(payload.data))

      if (payload.data && isValue(payload.data.isUserBanned)) {
        const {
          data: { isUserBanned }
        } = payload
        // TODO: should be a better way to handle this.
        // Need to discuss with the team about global solution after release.
        dispatch(debugShow(isUserBanned ? BANNED_LABEL : null))
      }
    }
  })
}

export default initWS
