import React from 'react'
import Loadable from 'react-loadable'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/helpers/rootReducer'
import { loadDataAttempt } from './modules/actions'
import reducer from './modules/reducers'
import initWS from './modules/websockets'

import SkeletonFull from './components/Skeletons/Full'

const Preloader = () => <SkeletonFull />

const MyMapsRoute = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const MyMaps = await import(/* webpackChunkName: "myMaps" */ './layout/index')

    initWS(rootStore.dispatch)
    injectReducer(rootStore, { reducer, key: 'myMapsApp' })
    rootStore.dispatch(loadDataAttempt(true))

    return MyMaps
  },
  loading: Preloader
})

export default MyMapsRoute
