import React from 'react'

class GradientStarMobile extends React.PureComponent {
  render() {
    return (
      <svg width='0' height='0'>
        <linearGradient id='starMobileGradient' gradientTransform='rotate(90)'>
          <stop offset='0%' stopColor='#f6fdfe' />
          <stop offset='100%' stopColor='#f8fdff' />
        </linearGradient>
        <linearGradient id='starMobileGradientJoined' gradientTransform='rotate(90)'>
          <stop offset='0%' stopColor='#ecf8fc' />
          <stop offset='100%' stopColor='#f2fbfc' />
        </linearGradient>
      </svg>
    )
  }
}

export default GradientStarMobile
