import React, { Fragment, PureComponent } from 'react'
import classnames from 'classnames'

import Title from './Title'
import { CreateNewMap } from '../../containers'
import Outcome from '../Outcomes'

import isInvitesTable from '../../utils/isInvitesTablet'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

class Header extends PureComponent<IProps> {
  static defaultProps: IProps = {
    title: 'No Title',
    mediaType: 'desktop',
    isDesktopLayoutSet: false
  }

  _classNamesHolder = () => {
    const bottomSectionClasses = classnames({
      [styles.bottomSection]: true,
      [styles.outcomeAppear]: !this._isOutcomeShow()
    })

    return {
      bottomSectionClasses
    }
  }

  _getDelimier = () => {
    const { mediaType, isDesktopLayoutSet } = this.props

    if (mediaType !== 'desktop' && !isDesktopLayoutSet) {
      return null
    }

    return <div className={styles.extraHeaderLine} />
  }

  _renderButton = () => {
    const { appID } = this.props

    if (isInvitesTable(appID)) return null

    // const CreateNewMapComponent = isPlaceholder ? CreateNewMapReduxless

    return (
      <Fragment>
        {this._getDelimier()}
        <CreateNewMap />
      </Fragment>
    )
  }

  _checkBackgroudDisable = () => {
    const { appID, mediaType, isDesktopLayoutSet } = this.props

    const desktopLayout = mediaType === 'desktop' || isDesktopLayoutSet
    const headerOff = desktopLayout && !isInvitesTable(appID) ? styles.disableHeaderBCG : ''

    return headerOff
  }

  _isOutcomeShow = () => {
    const { newMapInProgress, appID } = this.props
    const hideOutcome = !newMapInProgress || isInvitesTable(appID)

    return hideOutcome
  }

  _renderNewMapCreationOutcome = () => {
    const { outcomeType, mediaType } = this.props

    if (this._isOutcomeShow()) {
      return null
    }

    return <Outcome outcomeType={outcomeType} mediaType={mediaType} />
  }

  render() {
    const { title } = this.props
    const { bottomSectionClasses } = this._classNamesHolder()
    const headerOff = this._checkBackgroudDisable()

    return (
      <div className={`${styles.headerContainer} ${headerOff}`}>
        <div className={styles.topSection}>
          <Title title={title} />
          {this._renderButton()}
        </div>
        <div className={bottomSectionClasses}>{this._renderNewMapCreationOutcome()}</div>
      </div>
    )
  }
}

export default Header
