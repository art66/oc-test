import React, { PureComponent } from 'react'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

class SVGPlaceholder extends PureComponent<IProps> {
  static defaultProps: IProps = {
    transform: false
  }

  _getFillColor = () => {
    return (
      <linearGradient id='placeholder-gradient' gradientTransform='rotate(90)'>
        <stop offset='0%' stopColor='#fff' />
        <stop offset='100%' stopColor='#def1f7' />
      </linearGradient>
    )
  }

  _getDarkFillColor = () => {
    return (
      <linearGradient id='placeholder-dark-gradient' gradientTransform='rotate(90)'>
        <stop offset='0%' stopColor='#535353' />
        <stop offset='100%' stopColor='#283C46' />
      </linearGradient>
    )
  }

  _getFigure = () => {
    return (
      <path
        id='elemPlaceholder'
        d='
          M 0 34
          L 0 31.5
          C 0 5, -2 1, 5 0
          L 116 0
          C 116 0, 122.5 -1, 124.5 7
          L 129 21
          C 129 21, 131 29, 139 29
          L 150 29
          L 150 34
          L 0 34
        '
      />
    )
  }

  render() {
    const { transform } = this.props

    return (
      <div className={styles.svgContainer}>
        <svg className={`${styles.svgPlaceholder} ${transform ? styles.svgPlaceholderReverse : ''}`}>
          {this._getFillColor()}
          {this._getDarkFillColor()}
          {this._getFigure()}
        </svg>
      </div>
    )
  }
}

export default SVGPlaceholder
