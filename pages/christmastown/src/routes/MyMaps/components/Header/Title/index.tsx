import React, { PureComponent } from 'react'
import SVGPlaceholder from '../SVGPlaceholder'

import { IProps } from './types'
import styles from './index.cssmodule.scss'
import headerStyles from '../index.cssmodule.scss'

class Title extends PureComponent<IProps> {
  static defaultProps: IProps = {
    title: 'No Title'
  }

  render() {
    const { title } = this.props

    return (
      <div className={styles.titleContainer}>
        <SVGPlaceholder />
        <span className={`${styles.title} ${headerStyles.titleFont}`}>{title}</span>
      </div>
    )
  }
}

export default Title
