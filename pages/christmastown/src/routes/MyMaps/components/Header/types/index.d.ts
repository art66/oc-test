import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IProps {
  title?: string
  appID?: number
  isPlaceholder?: boolean
  outcomeType?: string
  isDesktopLayoutSet: boolean
  disableHeaderBCG?: boolean
  disableButton?: boolean
  mediaType?: TMediaType
  newMapInProgress?: string
}
