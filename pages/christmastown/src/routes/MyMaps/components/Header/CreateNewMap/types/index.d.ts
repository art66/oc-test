export interface IProps {
  mediaType?: string
  inProgressMainLoad: boolean
  newMapInProgress?: boolean
  newMapHovered?: boolean
  createNewMapAction?: (newMapHovered: boolean, newMapInProgress: boolean) => void
  newMapOnHoverAction?: (status: boolean) => void
  newMapOnBlurAction?: (status: boolean) => void
}
