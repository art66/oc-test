import React, { PureComponent } from 'react'
import classnames from 'classnames'

import { IProps } from './types'
import { BUTTON_STATE } from '../../../constants'
import SVGPlaceholder from '../SVGPlaceholder'
import { SVGButtonsGenerator } from '@torn/shared/SVG'

import styles from './index.cssmodule.scss'
import headerStyles from '../index.cssmodule.scss'

const TITLE = 'Create New Map'
const SVG_BUTTON_NAME = 'CTESloped'
const SVG_BUTTON_TYPE = 'CHRISTMAS_TOWN'

class CreateNewMap extends PureComponent<IProps> {
  _classNameHolder = () => {
    const { newMapInProgress, newMapHovered } = this.props

    const buttonClass = classnames(headerStyles.titleFont, styles.button, styles.buttonCreate, {
      [styles.buttonHovered]: !newMapInProgress && newMapHovered,
      [styles.buttonClicked]: newMapInProgress
    })

    const titleClass = classnames({
      [styles.buttonTitle]: true
    })

    return {
      buttonClass,
      titleClass
    }
  }

  _handleClick = () => {
    const { newMapInProgress, inProgressMainLoad, createNewMapAction } = this.props

    if (newMapInProgress || inProgressMainLoad) return

    createNewMapAction(false, true)
  }

  _handlerMouseEnter = () => {
    const { newMapInProgress, newMapOnHoverAction } = this.props

    if (newMapInProgress) return

    newMapOnHoverAction(true)
  }

  _handlerMouseLeave = () => {
    const { newMapInProgress, newMapOnBlurAction } = this.props

    if (newMapInProgress) return

    newMapOnBlurAction(false)
  }

  _checkNewMapColorScheme = () => {
    const { newMapInProgress, inProgressMainLoad, newMapHovered } = this.props
    const { HOVERED, CLICKED, ACTIVE } = BUTTON_STATE

    if (newMapInProgress || inProgressMainLoad) {
      return CLICKED
    }

    if (newMapHovered) {
      return HOVERED
    }

    return ACTIVE
  }

  render() {
    const { mediaType } = this.props
    const { buttonClass, titleClass } = this._classNameHolder()
    const buttonState = this._checkNewMapColorScheme()

    return (
      <div className={styles.buttonContainer}>
        <SVGPlaceholder transform={true} />
        <button
          type='button'
          className={buttonClass}
          onClick={this._handleClick}
          onMouseEnter={this._handlerMouseEnter}
          onMouseLeave={this._handlerMouseLeave}
        >
          <SVGButtonsGenerator
            buttonName={SVG_BUTTON_NAME}
            buttonType={SVG_BUTTON_TYPE}
            condition={buttonState}
            mediaType={mediaType}
            customClass={styles.svgButton}
          />
          <span className={titleClass}>{TITLE}</span>
        </button>
      </div>
    )
  }
}

export default CreateNewMap
