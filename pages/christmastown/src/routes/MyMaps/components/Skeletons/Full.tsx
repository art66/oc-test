import React from 'react'

import SkeletonRows from './Rows'

import styles from './full.cssmodule.scss'
import './index.scss'

const SECTIONS_TITLES = {
  0: 'Name',
  1: 'LastEdit',
  2: 'Editors',
  3: 'Testers',
  4: 'Players',
  5: 'Release',
  6: 'Rating',
  7: 'Manage'
}

class SkeletonFull extends React.PureComponent {
  _renderSectionsTabs = (tableType?: string) => {
    const headerSection = Object.keys(SECTIONS_TITLES).map(tabID => {
      const currentSection = SECTIONS_TITLES[tabID]

      return (
        <div
          key={tabID}
          className={`${styles.tabSkeleton} ${styles[`tab${currentSection}`]} ${styles[`tab${currentSection}${tableType}`]}`}
        />
      )
    })

    return (
      <div className={styles.tabsWrap}>
        {headerSection}
      </div>
    )
  }

  _myMapsHeader = () => {
    return (
      <div className='headerContainer'>
        <div className='topSection'>
          <div className='titleContainer'>
            <div className='svgContainer'>
              <svg className='svgPlaceholder'>
                <linearGradient id='placeholder-gradient' gradientTransform='rotate(90)'>
                  <stop offset='0%' stopColor='#fff' />
                  <stop offset='100%' stopColor='#def1f7' />
                </linearGradient>
                <linearGradient id='placeholder-dark-gradient' gradientTransform='rotate(90)'>
                  <stop offset='0%' stopColor='#535353' />
                  <stop offset='100%' stopColor='#283C46' />
                </linearGradient>
                <path
                  id='elemPlaceholder'
                  d='M 0 34 L 0 31.5 C 0 5, -2 1, 5 0 L 116 0 C 116 0, 122.5 -1, 124.5 7 L 129 21 C 129 21, 131 29, 139 29 L 150 29 L 150 34 L 0 34'
                />
              </svg>
            </div>
          </div>
          <div className='extraHeaderLine' />
          <div className='buttonContainer'>
            <div className='svgContainer'>
              <svg className='svgPlaceholder svgPlaceholderReverse'>
                <linearGradient id='placeholder-gradient' gradientTransform='rotate(90)'>
                  <stop offset='0%' stopColor='#fff' />
                  <stop offset='100%' stopColor='#def1f7' />
                </linearGradient>
                <path
                  id='elemPlaceholder'
                  d='M 0 34 L 0 31.5 C 0 5, -2 1, 5 0 L 116 0 C 116 0, 122.5 -1, 124.5 7 L 129 21 C 129 21, 131 29, 139 29 L 150 29 L 150 34L 0 34'
                  fill='url(#placeholder-gradient)'
                />
              </svg>
            </div>
            <button type='button' className='titleFont button buttonCreate' />
          </div>
        </div>
      </div>
    )
  }

  _renderMyMaps = () => {
    return (
      <div className={styles.mymapsSkeleton}>
        {this._myMapsHeader()}
        {this._renderSectionsTabs()}
        <SkeletonRows tableType={1} />
      </div>
    )
  }

  _renderInvites = () => {
    return (
      <div className={styles.invitesSkeleton}>
        <div className={styles.headerWrap} />
        {this._renderSectionsTabs('Invites')}
        <SkeletonRows tableType={2} />
      </div>
    )
  }

  render() {
    return (
      <div>
        {this._renderMyMaps()}
        <hr className='delimiter-999' />
        {this._renderInvites()}
      </div>
    )
  }
}

export default SkeletonFull
