import React from 'react'

import { ISkeleton } from './types'
import isInvitesTable from '../../utils/isInvitesTablet'
import styles from './rows.cssmodule.scss'

class SkeletonRows extends React.PureComponent<ISkeleton> {
  _renderRows = (tabletType: number) => {
    const isMyMapsTable = tabletType === 1
    const rowsCount = Array.from(Array(10).keys())

    const playersClass = !isInvitesTable(tabletType) ? styles.playersSection : `${styles.playersSection} ${styles.playersSectionInvites}`
    const nameClass = !isInvitesTable(tabletType) ? styles.nameSection : `${styles.nameSection} ${styles.nameSectionInvites}`

    return rowsCount.map(row => (
      <div key={row} className={styles.rowWrap}>
        <div className={nameClass} />
        <div className={styles.lastEditSection} />
        <div className={styles.editorsSection} />
        <div className={styles.testersSection} />
        <div className={playersClass} />
        <div className={styles.releaseSection} />
        <div className={styles.ratingSection} />
        <div className={styles.manageSection}>
          <div className={`${styles.manageSubSection} ${!isMyMapsTable ? styles.leaveManageSubSection : ''}`} />
          <div className={styles.manageSubSection} />
          {isMyMapsTable && <div className={styles.manageSubSection} />}
        </div>
      </div>
    ))
  }

  render() {
    const { tableType } = this.props

    return (
      <div className={styles.rowsSkeletonWrap}>
        {this._renderRows(tableType)}
      </div>
    )
  }
}

export default SkeletonRows
