import React from 'react'
import { Header, Body } from '../../containers'

import styles from './index.cssmodule.scss'

const Invites = props => {
  const { appID, title, columnTitles, rowsData } = props

  return (
    <div className={styles.invitesContainer}>
      <Header appID={appID} title={title} />
      <Body
        appID={appID}
        columnTitles={columnTitles}
        rows={rowsData}
        placeholderTitle='Invites'
        {...props}
      />
    </div>
  )
}

export default Invites
