import React, { PureComponent } from 'react'
import { CSSTransition } from 'react-transition-group'
import getKeyByValue from '@torn/shared/utils/getKeyByValue'

import { IProps } from './types'
import styles from './index.cssmodule.scss'
import './anim.cssmodule.scss'

const ANIM_PROPS = {
  className: 'ctTooltip',
  timeIn: 1000,
  timeExit: 300
}
const TOOLTIPS_MESSAGES = {
  onlySpacesEntered: 'Name of spaces only is not allowed',
  emptyString: 'Enter new map name, please',
  maxLength: 'Max name length is reached'
}

class ToolTip extends PureComponent<IProps, any> {
  constructor(props) {
    super(props)

    // we need state to prevent message disappearing on
    // animation exit once its message actually missed due to the calculation process!
    this.state = {
      lastWarningMessage: null
    }
  }

  static getDerivedStateFromProps(nextProps: IProps) {
    const { warningCases } = nextProps
    const messageFounded = TOOLTIPS_MESSAGES[getKeyByValue(warningCases, true)]

    if (messageFounded) {
      return {
        lastWarningMessage: messageFounded
      }
    }

    return null
  }

  _showTooltip = () => {
    const { warningCases } = this.props

    return Object.values(warningCases).some(value => value === true)
  }

  _renderToolTip() {
    const { lastWarningMessage } = this.state

    return (
      <div className={`${styles.tooltipContainer} ${styles.containerFailure}`}>
        <span className={styles.tooltipTitle}>{lastWarningMessage}</span>
      </div>
    )
  }

  render() {
    const { className, timeExit, timeIn } = ANIM_PROPS

    return (
      <CSSTransition
        in={this._showTooltip()}
        classNames={className}
        timeout={{ enter: timeIn, exit: timeExit }}
        unmountOnExit={true}
      >
        {this._renderToolTip()}
      </CSSTransition>
    )
  }
}

export default ToolTip
