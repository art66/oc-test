export interface IProps {
  warningCases: object
}

export interface IState {
  showTooltip: boolean
  updateType: string
}
