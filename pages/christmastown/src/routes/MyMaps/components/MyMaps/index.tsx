import React from 'react'
import { Header, Body } from '../../containers'

import globStyles from '../../styles/glob.cssmodule.scss'
import styles from './index.cssmodule.scss'

const MyMaps = props => {
  const { appID, title, columnTitles, rowsData } = props

  return (
    <div className={`${globStyles.wrapper} ${styles.myMapsContainer}`}>
      <Header appID={appID} title={title} />
      <Body appID={appID} columnTitles={columnTitles} rows={rowsData} pTitle='Maps' {...props} />
    </div>
  )
}

export default MyMaps
