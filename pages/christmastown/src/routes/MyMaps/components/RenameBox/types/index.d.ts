import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IState {
  rowTitle?: string
  isMaxLengthExceeded?: boolean
  isOnlySpacesInputted?: boolean
  isClicked: boolean
}

export interface IProps {
  isRequested?: boolean,
  mediaType?: TMediaType
  isDesktopLayoutSet: boolean
  uuid?: string
  rowID?: number
  title?: any
  type?: string
  className?: string
  isRowHovered?: boolean
  changeMapNameID?: number
  changeMapNameInProgress?: boolean
  changeMapNameAction?: (status: boolean, ID: number) => void
  changeMapNameCancelAction?: (status: boolean, ID: number) => void
  changeMapNameSaveAction?: (isRequested: boolean, ID: string, newMapName: string | number) => void
}
