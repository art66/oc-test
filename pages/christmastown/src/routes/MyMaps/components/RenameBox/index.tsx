import React, { PureComponent, Fragment } from 'react'
import classnames from 'classnames'

import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { SVGIconGenerator } from '@torn/shared/SVG'
import InputStringChecker from '@torn/shared/utils/stringChecker'
import checkMediaType from '@torn/shared/utils/checkMediaType'
import { withTheme, TWithThemeInjectedProps } from "@torn/shared/hoc/withTheme/withTheme";

import TooltipWarning from '../TooltipWarning'

import { BLUE_HOVERED, BLUE_HOVERED_DARK } from '../../constants'
import { IProps, IState } from './types'

import styles from './index.cssmodule.scss'
import globalStyles from '../../styles/glob.cssmodule.scss'
import {DARK_THEME} from "../../../../constants";

const NO_DATA_PLACEHOLDER = 'N/A'

class RenameBox extends PureComponent<IProps & TWithThemeInjectedProps, IState> {
  private _stringChecker: any
  private _renameInput: React.RefObject<HTMLInputElement>

  constructor(props: IProps & TWithThemeInjectedProps) {
    super(props)

    this.state = {
      rowTitle: '',
      isMaxLengthExceeded: false,
      isOnlySpacesInputted: false,
      isClicked: false
    }

    this._renameInput = React.createRef()
  }

  componentDidMount() {
    const { title } = this.props

    this.setState({
      rowTitle: title
    })

    this._focusInputOnMount()
    this._initializeTooltip()
    this._initializeStringChecker()
  }

  _focusInputOnMount = () => {
    this._renameInput && this._renameInput.current && this._renameInput.current.focus()
  }

  _initializeTooltip = () => {
    const { rowID } = this.props

    tooltipsSubscriber.subscribe({
      child: 'Rename',
      ID: `${rowID}-renameBox`
    })
  }

  _initializeStringChecker = () => {
    this._stringChecker = new InputStringChecker({ maxLength: 20 })
  }

  _checkString = (value: string) => {
    const stringPayload = this._stringChecker.checkString({ text: value })

    return stringPayload
  }

  _classNameHolder = () => {
    const { rowTitle, isClicked, isMaxLengthExceeded, isOnlySpacesInputted } = this.state
    const { type, isRowHovered, mediaType } = this.props

    const buttonClass = classnames({
      [styles.editButton]: true,
      [styles.editButtonOutcome]: type === 'outcome'
    })

    const inputClass = classnames({
      [globalStyles.mainInput]: true,
      [styles.inputTextChange]: true,
      [styles.inputTextOutcome]: type === 'outcome',
      [globalStyles.inputEmpty]: !rowTitle && isClicked,
      [globalStyles.inputSpace]: isOnlySpacesInputted && isClicked,
      [globalStyles.inputMaxLengthExceeded]: isMaxLengthExceeded
    })

    const textTitleClass = classnames({
      [styles.textTitle]: true,
      [styles.squashedTitle]: mediaType=== 'tablet' && isRowHovered
    })

    return {
      inputClass,
      buttonClass,
      textTitleClass
    }
  }

  _checkIfNameNotChanged = () => {
    const { rowTitle } = this.state
    const { title } = this.props

    if (title !== rowTitle) {
      return false
    }

    this._actionClickCancel()

    return true
  }

  _setFalseClick = () => {
    this.setState({
      isClicked: true
    })
  }

  _actionClick = () => {
    const { rowTitle, isMaxLengthExceeded } = this.state
    const {
      type,
      changeMapNameAction,
      changeMapNameSaveAction,
      rowID,
      uuid,
      changeMapNameInProgress,
      changeMapNameID
    } = this.props

    const isRenameShouldSave = () => changeMapNameID === rowID && changeMapNameInProgress
    const isOutcomeLayout = type === 'outcome'
    const isStringIncorrect = !rowTitle || rowTitle.length === 0 || isMaxLengthExceeded

    if (isRenameShouldSave() && this._checkIfNameNotChanged() || isStringIncorrect) {
      this._setFalseClick()

      return
    }

    if (isRenameShouldSave() || isOutcomeLayout) {
      changeMapNameSaveAction(true, uuid, rowTitle)

      return
    }

    changeMapNameAction(true, rowID)
  }

  _actionClickCancel = () => {
    const { changeMapNameCancelAction, rowID, title } = this.props

    this.setState({
      rowTitle: title,
      isMaxLengthExceeded: false,
      isOnlySpacesInputted: false,
      isClicked: false
    })

    changeMapNameCancelAction(false, rowID)
  }

  _handlerClick = () => {
    this._actionClick()
  }

  _handlerKeyDown = e => {
    const keyCode = e.which

    if (keyCode === 27) {
      this._actionClickCancel()

      return
    }

    if (keyCode === 13) {
      this._actionClick()

      return
    }
  }

  _handleChange = e => {
    const { value } = e.target
    const { isOnlySpacesInputted, isMaxLengthExceeded, value: textNormalizedValue } = this._checkString(value)

    this.setState({
      rowTitle: textNormalizedValue,
      isMaxLengthExceeded,
      isOnlySpacesInputted,
      isClicked: false
    })
  }

  _isSaveButtonShouldBeShown = () => {
    const { changeMapNameInProgress, rowID, changeMapNameID, type } = this.props

    const rowInChangeNameProgress = changeMapNameInProgress && rowID === changeMapNameID
    const outcomeMobileLayout = type === 'outcome'

    return rowInChangeNameProgress || outcomeMobileLayout
  }

  _isCurrentRowInProgress = () => {
    const { rowID, changeMapNameID } = this.props

    return rowID === changeMapNameID
  }

  _isRenameButtonShouldBeDisabled = () => {
    const { className, isRowHovered, type } = this.props

    const notNameSection = !type && (className !== 'name')
    const rowIsNotHovered = !type && !isRowHovered

    return !this._isCurrentRowInProgress() && (notNameSection || rowIsNotHovered)
  }

  _getEditIcon = () => {
    const { isRequested, mediaType, isDesktopLayoutSet, type, theme } = this.props
    const { isDesktop } = checkMediaType(mediaType)
    const isDarkMode = theme === DARK_THEME
    const iconFill = isDarkMode ? BLUE_HOVERED_DARK : BLUE_HOVERED

    if (!type && !isDesktop && !isDesktopLayoutSet) {
      return null
    }

    if (isRequested && this._isCurrentRowInProgress()) {
      return (
        <div className={styles.loadingSpinnerWrap}>
          <span className={globalStyles.loadingSpinner} />
        </div>
      )
    }

    if (this._isSaveButtonShouldBeShown()) {
      return <SVGIconGenerator iconName={'Checked'} fill={{ name: iconFill }} />
    }

    return <SVGIconGenerator iconName={'Pen'} fill={{ name: iconFill }} />
  }

  _renderWarningToolTip = () => {
    const { rowTitle, isOnlySpacesInputted, isMaxLengthExceeded, isClicked } = this.state

    const tooltipsShowCases = {
      emptyString: !rowTitle && isClicked,
      maxLength: isMaxLengthExceeded,
      spaceEntered: isOnlySpacesInputted && isClicked
    }

    return (
      <TooltipWarning warningCases={tooltipsShowCases} />
    )
  }

  _renderEditInput = () => {
    const { rowTitle } = this.state
    const { changeMapNameInProgress, rowID, changeMapNameID, type } = this.props

    const { inputClass, textTitleClass } = this._classNameHolder()

    if ((changeMapNameInProgress && rowID === changeMapNameID) || type === 'outcome') {
      return (
        <React.Fragment>
          <input
            id='rename_box'
            type='text'
            ref={this._renameInput}
            className={inputClass}
            placeholder='enter new name...'
            value={rowTitle}
            onChange={this._handleChange}
            onKeyDown={this._handlerKeyDown}
            data-lpignore='true'
          />
          <label
            className={styles.inputLabel}
            htmlFor='rename_box'
            children={this._renderWarningToolTip()}
          />
        </React.Fragment>
      )
    }

    return <span className={textTitleClass}>{rowTitle || NO_DATA_PLACEHOLDER}</span>
  }

  _isDesktopMode = () => {
    const { mediaType, isDesktopLayoutSet } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    return isDesktop || isDesktopLayoutSet
  }

  _renderEditButton = () => {
    if (this._isRenameButtonShouldBeDisabled()) {
      return null
    }

    const { rowID } = this.props
    const { buttonClass } = this._classNameHolder()

    const editButton = (
      <button
        type='button'
        id={this._isDesktopMode() ? `${rowID}-renameBox` : ''}
        className={buttonClass}
        onClick={this._handlerClick}
        onKeyDown={this._handlerKeyDown}
      >
        {this._getEditIcon()}
      </button>
    )

    return editButton
  }

  render() {
    return (
      <Fragment>
        {this._renderEditInput()}
        {this._renderEditButton()}
      </Fragment>
    )
  }
}

export default withTheme(RenameBox)
