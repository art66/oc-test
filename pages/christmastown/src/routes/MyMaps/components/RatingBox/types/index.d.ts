import { IProps as IRatingBoxProps } from '../../../../../components/RatingBox/interfaces'

export interface IProps extends IRatingBoxProps {}
