import React, { PureComponent } from 'react'
import { IProps } from './types'

import styles from './index.cssmodule.scss'

class ColumnTitle extends PureComponent<IProps> {
  static defaultProps: IProps = {
    title: 'test',
    className: ''
  }

  render() {
    const { title, className } = this.props

    return (
      <div className={`${styles.columnContainer} ${styles[className]}`}>
        <span className={styles.title}>{title}</span>
      </div>
    )
  }
}

export default ColumnTitle
