import { ISwitcher } from '../../../../../../../components/Switcher/types'

export interface IProps {
  uuid: string
  publishMapID: number
  rowID?: number
  narrow?: boolean
  userBanned?: boolean
  inProgress: boolean
  releaseStatus: boolean
  isDisabledCheckbox?: boolean
  switcherToggle: ({ mapID, authorID, publishStatus, inProgress }: ISwitcher) => void
}
