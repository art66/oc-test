import { IProps as IGlobalProps } from '../../types'

export interface IState {
  rowTitle?: string | number
}

export interface IProps extends IGlobalProps {
  isDesktopLayoutSet: boolean
  releaseSectionData?: {
    data: boolean
  }
  changeMapNameID?: number
  changeMapNameInProgress?: boolean
  changeMapNameAction?: (status: boolean, ID: number) => void
  changeMapNameCancelAction?: (status: boolean, ID: number) => void
  changeMapNameSaveAction?: (status: boolean, ID: number, newName: string | number) => void
}
