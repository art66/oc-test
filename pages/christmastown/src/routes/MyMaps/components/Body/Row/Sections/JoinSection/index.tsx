// TODO: improve CustomTag logic to be able reused between both MyMaps and Invites tablets.

import React, { Component } from 'react'
import classnames from 'classnames'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import getCurrentPageURL from '@torn/shared/utils/getCurrentPageURL'
import checkMediaType from '@torn/shared/utils/checkMediaType'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'

import { SVGIconGenerator } from '@torn/shared/SVG'
import Switcher from '../../../../../containers/Body/Row/Switcher'
import isInvitesTable from '../../../../../utils/isInvitesTablet'

import { ORANGE_DELETED, BLUE_HOVERED, BLUE_DIMMED, BLUE_HOVERED_DARK, BLUE_DIMMED_DARK, ORANGE_DELETED_DARK } from '../../../../../constants'
import { DARK_THEME } from '../../../../../../../constants'
import { IProps, IState, IGenerateSubSection, ICustomTag } from './types'

import styles from './index.cssmodule.scss'
import commonRowStyles from '../index.cssmodule.scss'

const SUB_SECTIONS = ['share', 'join', 'trash']
const SVG_ICON_NAMES_NORMALIZER = {
  leave: 'script',
  settings: 'gear'
}
const SVG_DIMENSIONS = {
  Share: {
    width: 15,
    height: 15,
    viewbox: '0 0 24 24'
  },
  Join: {
    width: 15,
    height: 15,
    viewbox: '0 0 24 24'
  },
  Trash: {
    width: 15,
    height: 15,
    viewbox: '0 0 21 24'
  },
  Script: {},
  Gear: {}
}

class JoinSection extends Component<IProps & TWithThemeInjectedProps, IState> {
  constructor(props: IProps & TWithThemeInjectedProps) {
    super(props)

    this.state = {
      hoveredButtonName: ''
    }
  }

  componentDidMount() {
    this._initializeTooltip()
  }

  _initializeTooltip = () => {
    const { appID, rowID, payload: { data = {} } = {} } = this.props

    const { position } = data

    const generateTooltips = (table: string) => {
      const tooltips = {
        myMapsTooltips: {
          share: {
            child: 'Invite Editors or Testers',
            ID: `${appID}-${rowID}-share`
          },
          join: {
            child: 'Join',
            ID: `${appID}-${rowID}-join`
          },
          trash: {
            child: 'Delete Map',
            ID: `${appID}-${rowID}-trash`
          }
        },
        invitesTooltips: {
          leave: {
            child: `Leave ${position} position`,
            ID: `${appID}-${rowID}-leave`
          },
          join: {
            child: 'Join',
            ID: `${appID}-${rowID}-join`
          }
        }
      }

      const currentTooltipsTable = tooltips[table]

      Object.keys(currentTooltipsTable).forEach(section => {
        tooltipsSubscriber.subscribe(currentTooltipsTable[section])
      })
    }

    !isInvitesTable(appID) ? generateTooltips('myMapsTooltips') : generateTooltips('invitesTooltips')
  }

  _manageMapHandler = ({ target }) => {
    const {
      dataset: { handler }
    } = target

    const { mediaType, manageMapAction, manageRowID, manageMapInProgress, rowID } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    const actionName = handler.toUpperCase()

    const payload =
      !isDesktop && manageRowID === rowID && manageMapInProgress ?
        {
          mediaType,
          manageType: actionName,
          manageMapInProgress: false
        } :
        {
          rowID,
          mediaType,
          manageType: actionName,
          mobileLayout: actionName === 'SETTINGS',
          manageMapInProgress: true
        }

    manageMapAction(payload)
  }

  _isJoinSections = (subSectionName: string) => {
    return subSectionName === 'join'
  }

  _buttonHoverHandler = ({ target }) => {
    const {
      dataset: { handler }
    } = target

    this.setState({
      hoveredButtonName: handler
    })
  }

  _buttonLeaveHandler = () => {
    this.setState({
      hoveredButtonName: ''
    })
  }

  _classNameHolder = () => {
    const { payload, appID } = this.props

    const containerWidth = classnames({
      [commonRowStyles.section]: true,
      [styles.joinSection]: true,
      [commonRowStyles[payload.className]]: true,
      [styles.myMapsContainer]: !isInvitesTable(appID),
      [styles.invitesContainer]: isInvitesTable(appID)
    })

    return containerWidth
  }

  _isDesktopMode = () => {
    const { mediaType, isDesktopLayoutSet } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    return isDesktop || isDesktopLayoutSet
  }

  _getSVGIconFill = subSection => {
    const { hoveredButtonName } = this.state
    const {
      rowID,
      manageRowID,
      manageType = '',
      manageMapInProgress,
      myMapsMobileOutcomeLayout,
      isDesktopLayoutSet,
      mediaType,
      theme
    } = this.props

    const { isDesktop } = checkMediaType(mediaType)
    const isDarkMode = theme === DARK_THEME

    const isRowSame = manageRowID === rowID
    const isTypeSame = subSection === manageType.toLocaleLowerCase()
    const isManageTypeFired = isRowSame && isTypeSame
    const isSameSubsection = subSection === hoveredButtonName
    const showSettingsMobile = !isDesktopLayoutSet && !isDesktop && isRowSame && myMapsMobileOutcomeLayout

    let getFillName = isDarkMode ? BLUE_DIMMED_DARK : BLUE_DIMMED

    if ((showSettingsMobile && manageMapInProgress) || isManageTypeFired || isSameSubsection) {
      getFillName = isDarkMode ? BLUE_HOVERED_DARK : BLUE_HOVERED

      if (subSection === 'trash' || subSection === 'leave') {
        getFillName = isDarkMode ? ORANGE_DELETED_DARK : ORANGE_DELETED
      }
    }

    return getFillName
  }

  _generateSubSectionLayout = ({ subSection, tagCoreData }: IGenerateSubSection) => {
    const { appID, rowID } = this.props
    const {
      CustomTagName = 'button',
      children,
      customClass,
      isOnClick = true,
      linkToRedirect,
      targetWindow,
      type
    } = tagCoreData

    const fillName = this._getSVGIconFill(subSection)
    const preIconName = SVG_ICON_NAMES_NORMALIZER[subSection] || subSection
    const iconName = preIconName.substr(0, 1).toLocaleUpperCase() + preIconName.substr(1, preIconName.length)

    return (
      <CustomTagName
        data-handler={subSection}
        href={linkToRedirect}
        target={targetWindow}
        type={type}
        id={`${appID}-${rowID}-${subSection}`}
        className={`${styles.button} ${styles.imgButton} ${styles[subSection]} ${customClass && customClass}`}
        onClick={isOnClick ? this._manageMapHandler : undefined}
        onMouseEnter={this._buttonHoverHandler}
        onMouseLeave={this._buttonLeaveHandler}
      >
        {children}
        <SVGIconGenerator fill={{ name: fillName }} iconName={iconName} dimensions={SVG_DIMENSIONS[iconName]} />
      </CustomTagName>
    )
  }

  _getMyMapsSections = () => {
    const {
      payload: { data }
    } = this.props
    const { joinLink = '' } = data

    return SUB_SECTIONS.map(subSection => {
      const tagCoreData: ICustomTag = {
        CustomTagName: 'button',
        isOnClick: true,
        type: 'button'
      }

      if (this._isJoinSections(subSection)) {
        tagCoreData.CustomTagName = 'a'
        tagCoreData.linkToRedirect = `${getCurrentPageURL({ isOriginOnly: true })}${joinLink}`
        tagCoreData.targetWindow = '_self'
        tagCoreData.isOnClick = false
      }

      return (
        <div key={subSection} className={styles.subSectionContainer}>
          {this._generateSubSectionLayout({ subSection, tagCoreData })}
        </div>
      )
    })
  }

  _getInvitesSections = () => {
    const { payload } = this.props
    const {
      data: { position = '', joinLink = '' }
    } = payload

    const titleColor = classnames({
      [styles.title]: true,
      [styles[position]]: true
    })

    const buttonTagCoreData: ICustomTag = {
      CustomTagName: 'button',
      type: 'button',
      customClass: styles.invitesButton,
      children: <span className={titleColor}>{position}</span>,
      isOnClick: true
    }

    const linkTagCoreData: ICustomTag = {
      CustomTagName: 'a',
      linkToRedirect: `${getCurrentPageURL({ isOriginOnly: true })}${joinLink}`,
      targetWindow: '_self'
    }

    return (
      <React.Fragment>
        <div className={styles.invitesSubSection}>
          {this._generateSubSectionLayout({ subSection: 'leave', tagCoreData: buttonTagCoreData })}
        </div>
        <div className={styles.subSectionContainer}>
          {this._generateSubSectionLayout({ subSection: 'join', tagCoreData: linkTagCoreData })}
        </div>
      </React.Fragment>
    )
  }

  _mobileLayout = () => {
    const {
      releaseSectionData: { data: releaseStatus },
      rowID,
      uuid
    } = this.props
    const buttonTagCoreData: ICustomTag = {
      CustomTagName: 'button',
      type: 'button',
      isOnClick: true
    }

    return (
      <div className={styles.mobilelayoutContainer}>
        <Switcher rowID={rowID} uuid={uuid} releaseStatus={releaseStatus} />
        <div className={styles.settingsContainer}>
          {this._generateSubSectionLayout({ subSection: 'settings', tagCoreData: buttonTagCoreData })}
        </div>
      </div>
    )
  }

  _desktopLayout = () => {
    const { appID } = this.props

    if (isInvitesTable(appID)) {
      return this._getInvitesSections()
    }

    return this._getMyMapsSections()
  }

  _renderSubSections = () => {
    const { appID, mediaType, isDesktopLayoutSet } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    if (!isInvitesTable(appID) && !isDesktop && !isDesktopLayoutSet) {
      return this._mobileLayout()
    }

    return this._desktopLayout()
  }

  render() {
    const containerWidth = this._classNameHolder()

    return <div className={containerWidth}>{this._renderSubSections()}</div>
  }
}

export default withTheme(JoinSection)
