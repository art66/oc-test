import React from 'react'

import Release from '../../../../../../components/Switcher'

import { IProps } from './interfaces'

class Switcher extends React.Component<IProps> {
  render() {
    const {
      uuid,
      rowID,
      publishMapID,
      narrow,
      userBanned,
      inProgress,
      switcherToggle,
      releaseStatus,
      isDisabledCheckbox
    } = this.props

    return (
      <Release
        mapID={uuid}
        narrow={narrow}
        isDisabledCheckbox={isDisabledCheckbox}
        inProgress={publishMapID === rowID && inProgress}
        data={{ published: releaseStatus, dimmToggle: userBanned }}
        switcherToggle={switcherToggle}
        extraData={{ rowID }}
      />
    )
  }
}

export default Switcher
