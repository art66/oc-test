import React from 'react'
import classnames from 'classnames'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import RenameBox from '../../../../../containers/RenameBox'
import Switcher from '../../../../../containers/Body/Row/Switcher'

import { IProps, IState } from './types'
import isInvitesTable from '../../../../../utils/isInvitesTablet'

import styles from './index.cssmodule.scss'
import commonRowStyles from '../index.cssmodule.scss'

const NO_DATA_PLACEHOLDER = 'N/A'

class TextSection extends React.Component<IProps, IState> {
  _classNamesHolder = () => {
    const { data } = this.props

    const textSectionClass = classnames(
      commonRowStyles.section,
      styles[`${data.className}Section`],
      commonRowStyles[data.className],
      {
        [styles.sectionInactive]: data.className === 'lastedit' || this._checkIsPlayersInactive()
      }
    )

    return textSectionClass
  }

  _checkIsPlayersInactive = () => {
    const { data, releaseSectionData } = this.props

    return data.name === 'Players' && !releaseSectionData.data
  }

  _renderTextContainer = () => {
    const {
      appID,
      mediaType,
      releaseSectionData: { data: releaseStatus },
      rowID,
      uuid,
      data: { className, data: title },
      isRowHovered,
      isDesktopLayoutSet
    } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    if (className === 'name' && !isInvitesTable(appID) && (isDesktop || isDesktopLayoutSet)) {
      return <RenameBox rowID={rowID} uuid={uuid} isRowHovered={isRowHovered} className={className} title={title} />
    }

    const textLabel = this._checkIsPlayersInactive() ? NO_DATA_PLACEHOLDER : title

    if (isInvitesTable(appID) && className === 'playersInvites' && !isDesktop && !isDesktopLayoutSet) {
      return (
        <div className={styles.mobilelayoutContainer}>
          <span className={styles.invitesSwitcherLabel}>{textLabel}</span>
          <Switcher isDisabledCheckbox={true} releaseStatus={releaseStatus} uuid={uuid} narrow={true} />
        </div>
      )
    }

    return <span className={styles.textTitle}>{textLabel}</span>
  }

  render() {
    const textSectionClass = this._classNamesHolder()

    return <div className={textSectionClass}>{this._renderTextContainer()}</div>
  }
}

export default TextSection
