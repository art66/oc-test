export interface IProps {
  appID: number
  rowID: number
  uuid: string
  data: {
    data: boolean
    className: string
    type: string
  }
}
