import React from 'react'

import TextSection from '../../../../containers/Body/Row/Sections/TextSection'
import JoinSection from '../../../../containers/Body/Row/Sections/JoinSection'
import RatingSection from '../../../../containers/Body/Row/Sections/RatingSection'
import ReleaseSection from './ReleaseSection'

const getTextSection = (data, releaseSectionData, isRowHovered, appID, rowID, uuid) => {
  return (
    <TextSection
      key={data.data + data.name}
      appID={appID}
      data={data}
      isRowHovered={isRowHovered}
      rowID={rowID}
      uuid={uuid}
      releaseSectionData={releaseSectionData}
    />
  )
}

const getReleaseSection = (data, rowID, uuid, appID) => {
  return <ReleaseSection key={data.name} data={data} rowID={rowID} uuid={uuid} appID={appID} />
}

const getRatingSection = (data, isRowHovered, isRowActive, isMapJoinedActive, rowID) => {
  return (
    <RatingSection
      key={data.data + data.name}
      data={data}
      isRowHovered={isRowHovered}
      isMapJoinedActive={isMapJoinedActive}
      isRowActive={isRowActive}
      rowID={rowID}
    />
  )
}

const getJoinSection = (data, releaseSectionData, appID, rowID, manageType, manageMapInProgress, uuid, manageRowID) => {
  return (
    <JoinSection
      uuid={uuid}
      key={data.data + data.name}
      appID={appID}
      payload={data}
      rowID={rowID}
      manageRowID={manageRowID}
      releaseSectionData={releaseSectionData}
      manageType={manageType}
      manageMapInProgress={manageMapInProgress}
    />
  )
}

export { getTextSection, getReleaseSection, getRatingSection, getJoinSection }
