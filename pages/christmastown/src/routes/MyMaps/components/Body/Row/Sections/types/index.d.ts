import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IProps {
  appID?: number
  isRowHovered?: boolean
  isMapJoinedActive?: boolean
  rowID?: number
  uuid?: string
  isRowActive?: number
  mediaType?: TMediaType
  isDesktopLayoutSet?: boolean
  data?: {
    type?: string
    className?: string
    name?: string
    data?: boolean | string | number
  }
}
