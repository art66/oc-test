import { IProps as IGlobalProps } from '../../types'

export interface IProps extends IGlobalProps {
  payload?: {
    className?: string
    data?: {
      position?: string
      joinLink?: string
    }
  }
  isDesktopLayoutSet: boolean
  position?: string
  releaseSectionData?: {
    data: boolean
  }
  manageType?: string
  manageRowID?: number
  manageMapInProgress?: boolean
  myMapsMobileOutcomeLayout?: boolean
  manageMapAction?: (payload: object) => object
  leaveMapAction?: (payload: object) => object
}

export interface IState {
  hoveredButtonName?: string
}

export interface ICustomTag {
  CustomTagName?: 'a' | 'button'
  children?: Element | any
  linkToRedirect?: string
  targetWindow?: '_blank' | '_self'
  isOnClick?: boolean
  type?: 'button'
  customClass?: string
}

export interface IGenerateSubSection {
  subSection: string
  tagCoreData: ICustomTag
}
