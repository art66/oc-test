import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IRowData {
  ID?: number
  uuid?: string
  createdAt: number
  requests: {
    isReleaseRequest: boolean
  }
  sections?: {
    data?: {
      position?: string
    }
    published: boolean
    name?: string
    type?: string
    position?: string
  }[]
}

export interface IProps {
  appID?: number
  activeJoinedMapID?: string
  activeRowID?: number
  mediaType?: TMediaType
  isDesktopLayoutSet: boolean
  manageType?: string
  manageRowID?: number
  manageMapInProgress?: boolean
  myMapsMobileOutcomeLayout?: boolean
  rowData?: IRowData
}

export interface IState {
  updateType: string
  isRowHovered?: boolean
  mediaType: string
}
