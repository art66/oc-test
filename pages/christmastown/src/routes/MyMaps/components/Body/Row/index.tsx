import React, { Component } from 'react'
import classnames from 'classnames'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import Outcome from '.././../Outcomes'
import { getTextSection, getReleaseSection, getRatingSection, getJoinSection } from './Sections'

import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'

class Row extends Component<IProps, IState> {
  private _ref: React.RefObject<HTMLInputElement>

  constructor(props: IProps) {
    super(props)

    this.state = {
      updateType: 'props',
      isRowHovered: false,
      mediaType: null
    }

    this._ref = React.createRef()
  }

  static getDerivedStateFromProps(nextProps: IProps, prevState: IState) {
    if (prevState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    if (nextProps.mediaType !== prevState.mediaType || nextProps.activeRowID === null) {
      return {
        updateType: 'props',
        isRowHovered: false,
        mediaType: nextProps.mediaType
      }
    }

    return null
  }

  _classNameHolder = () => {
    const { isRowActive, isRowJoinedActiveMap } = this._checkRowActivePhases()

    const containerClass = classnames({
      [styles.rowContainer]: true,
      [styles.activeRow]: isRowActive,
      [styles.activeJoinedRow]: isRowJoinedActiveMap
    })

    return containerClass
  }

  _checkRowActivePhases = () => {
    const {
      rowData: { ID, uuid },
      activeRowID,
      activeJoinedMapID
    } = this.props

    return {
      isRowActive: activeRowID === ID,
      isRowJoinedActiveMap: uuid === activeJoinedMapID
    }
  }

  _handlerMouseOver = () => {
    this.setState({
      updateType: 'state',
      isRowHovered: true
    })
  }

  _handlerMouseLeave = () => {
    this.setState({
      updateType: 'state',
      isRowHovered: false
    })
  }

  _renderSections = () => {
    const { isRowHovered } = this.state

    const {
      rowData: { sections, uuid, ID },
      appID,
      manageType,
      manageRowID,
      manageMapInProgress
    } = this.props

    const { isRowActive, isRowJoinedActiveMap } = this._checkRowActivePhases()

    const noSections = !sections || sections.length === 0

    if (noSections) {
      return null
    }

    const sectionProcessor = (section: { type?: string }) => {
      const processors = {
        release: () => getReleaseSection(section, ID, uuid, appID),
        rating: () => getRatingSection(section, isRowHovered, isRowActive, isRowJoinedActiveMap, ID),
        manage: () =>
          getJoinSection(section, sections[5], appID, ID, manageType, manageMapInProgress, uuid, manageRowID),
        leave: () =>
          getJoinSection(section, sections[5], appID, ID, manageType, manageMapInProgress, uuid, manageRowID),
        text: () => getTextSection(section, sections[5], isRowHovered, appID, ID, uuid)
      }

      const sectionToProcess = processors[section.type]()

      return sectionToProcess
    }

    return sections.map(section => sectionProcessor(section))
  }

  _renderOutcome = () => {
    const {
      appID,
      rowData: { ID, uuid, sections },
      mediaType,
      isDesktopLayoutSet,
      manageType,
      manageRowID,
      myMapsMobileOutcomeLayout,
      manageMapInProgress
    } = this.props

    const { isDesktop } = checkMediaType(mediaType)

    if (ID !== manageRowID || !manageMapInProgress) return null

    const payload = !isDesktop && !isDesktopLayoutSet ? sections : sections[7].data

    return (
      <Outcome
        uuid={uuid}
        appID={appID}
        rowID={ID}
        payload={payload}
        mediaType={mediaType}
        isDesktopLayoutSet={isDesktopLayoutSet}
        mobileLayout={myMapsMobileOutcomeLayout}
        outcomeType={manageType.toLowerCase()}
      />
    )
  }

  render() {
    // const {
    //   appID,
    //   rowData: { ID }
    // } = this.props

    return (
      <div
        ref={this._ref}
        // id={`${appID}_${ID.toString()}`}
        className={this._classNameHolder()}
        onMouseOver={this._handlerMouseOver}
        onMouseLeave={this._handlerMouseLeave}
        onTouchStart={this._handlerMouseOver}
        onTouchEnd={this._handlerMouseLeave}
      >
        <div className={styles.sectionWrapper}>{this._renderSections()}</div>
        <div className={styles.outcomeWrapper}>{this._renderOutcome()}</div>
      </div>
    )
  }
}

export default Row
