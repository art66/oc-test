import React, { PureComponent } from 'react'
import classnames from 'classnames'
import isInvitesTable from '../../../../../utils/isInvitesTablet'

import Switcher from '../../../../../containers/Body/Row/Switcher'

import { IProps } from './types'

import styles from './index.cssmodule.scss'
import commonRowStyles from '../index.cssmodule.scss'

class ReleaseSection extends PureComponent<IProps> {
  _classNamesHolder = () => {
    const { data } = this.props

    const releaseSectionClass = classnames(
      commonRowStyles.section,
      styles.releaseSection,
      commonRowStyles[data.type],
      styles[data.className]
    )

    return releaseSectionClass
  }

  render() {
    const { data, rowID, uuid, appID } = this.props

    const releaseSectionClass = this._classNamesHolder()

    return (
      <div className={releaseSectionClass}>
        <Switcher releaseStatus={data.data} rowID={rowID} uuid={uuid} isDisabledCheckbox={isInvitesTable(appID)} />
      </div>
    )
  }
}

export default ReleaseSection
