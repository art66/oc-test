import React, { PureComponent } from 'react'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import RatingBox from '../../../../../../../components/RatingBox'

import { IProps } from '../types'
import styles from './index.cssmodule.scss'
import commonRowStyles from '../index.cssmodule.scss'

class RatingSection extends PureComponent<IProps> {
  render() {
    const { data, isRowHovered, isRowActive, isMapJoinedActive, mediaType, isDesktopLayoutSet } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    if (!isDesktop && !isDesktopLayoutSet) {
      return null
    }

    // console.log(isMapJoinedActive, 'isMapJoinedActive isRowActive')

    return (
      <div className={`${commonRowStyles.section} ${styles.ratingSection} ${commonRowStyles[data.className]}`}>
        <RatingBox
          data={Number(data.data)}
          isRowHovered={isRowHovered}
          isRowActive={isRowActive}
          isMapJoinedActive={isMapJoinedActive}
        />
      </div>
    )
  }
}

export default RatingSection
