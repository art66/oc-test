import { IRowData } from '../Row/types'

export interface IProps {
  appID?: number
  placeholderTitle?: string,
  activeRowID?: number
  manageType?: string
  manageRowID?: number
  manageMapInProgress?: boolean
  myMapsMobileOutcomeLayout?: boolean
  columnTitles?: {
    title: string
    class: string
  }[]
  rows?: IRowData[]
}
