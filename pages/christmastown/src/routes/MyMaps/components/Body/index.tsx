import React, { PureComponent } from 'react'
import classnames from 'classnames'

import ColumnTitle from './ColumnTitle'
import { Row } from '../../containers'
import SkeletonRows from '../Skeletons/Rows'

import rowDataNormalizer from '../../utils/rowDataNormalizer'
import isInvitesTable from '../../utils/isInvitesTablet'

import { IProps } from './types'
import { COLUMN_TITLES } from '../../constants'
import styles from './index.cssmodule.scss'
import isValue from '@torn/shared/utils/isValue'

class Body extends PureComponent<IProps> {
  _classNamesHolder = () => {
    const { appID } = this.props

    const columnClass = classnames({
      [styles.columnsWrap]: true,
      [styles.columnsWrapZIndex]: !isInvitesTable(appID)
    })

    const rowClass = classnames({
      [styles.rowsWrap]: true,
      [styles.rowsWrapZIndex]: !isInvitesTable(appID)
    })

    return {
      columnClass,
      rowClass
    }
  }

  _getColumnTitles = () => {
    const { appID } = this.props
    const titles = !isInvitesTable(appID) ? COLUMN_TITLES.maps : COLUMN_TITLES.invites

    if (!titles || titles.length === 0) {
      return null
    }

    return titles.map(item => {
      return <ColumnTitle key={item.class} title={item.title} className={item.class} />
    })
  }

  _normalizeRowsData = () => {
    const { appID, rows } = this.props

    const normalizedRows = []

    // processing server row's section payload by providing with classes/types/etc data.
    rows.forEach(row => {
      normalizedRows.push({
        ID: row.ID,
        uuid: row.uuid,
        createdAt: row.createdAt,
        requests: row.requests,
        sections: rowDataNormalizer(appID, row.sections)
      })
    })

    return normalizedRows
  }

  _getRows = () => {
    const {
      placeholderTitle = '',
      rows,
      appID,
      activeRowID,
      manageType,
      manageRowID,
      manageMapInProgress,
      myMapsMobileOutcomeLayout
    } = this.props

    if (!isValue(rows)) {
      return <SkeletonRows tableType={appID} />
    }

    if (rows && rows.length === 0) {
      const pTitle = `There is no ${placeholderTitle.toLowerCase() || 'data'} yet.`
      const pText = !isInvitesTable(appID)
        ? 'Try to create your first map ever by "Create New Map" button above!'
        : ''
      return (
        <div className={styles.rowPlaceholder}>
          <span className={styles.rowPlaceholderText}>
            {pTitle} {pText}
          </span>
        </div>
      )
    }

    const normalizedRows = this._normalizeRowsData()

    return normalizedRows
      .sort((a, b) => b.createdAt - a.createdAt)
      .map(row => {
        return (
          <Row
            appID={appID}
            key={row.ID + row.uuid}
            rowData={row}
            activeRowID={activeRowID}
            manageType={manageType}
            manageRowID={manageRowID}
            myMapsMobileOutcomeLayout={myMapsMobileOutcomeLayout}
            manageMapInProgress={manageMapInProgress}
          />
        )
    })
  }

  render() {
    const { columnClass, rowClass } = this._classNamesHolder()
    return (
      <div className={styles.bodyContainer}>
        <div className={columnClass}>{this._getColumnTitles()}</div>
        <div className={rowClass}>{this._getRows()}</div>
      </div>
    )
  }
}

export default Body
