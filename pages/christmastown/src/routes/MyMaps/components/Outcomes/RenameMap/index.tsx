import React, { PureComponent } from 'react'
import RenameBox from '../../../containers/RenameBox'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class RenameMap extends PureComponent<IProps> {
  render() {
    const { payload, rowID, uuid } = this.props

    if (!payload.length) {
      return null
    }

    const { className, data } = payload[0]

    return (
      <div className={styles.renameOutcomeWrap}>
        <RenameBox rowID={rowID} uuid={uuid} className={className} title={data} type={'outcome'} />
      </div>
    )
  }
}

export default RenameMap
