export interface IProps {
  rowID?: number
  uuid?:string
  payload?: {
    data?: any
    className?: string
  }[]
}
