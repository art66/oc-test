import React, { Component, Fragment } from 'react'
import MobileStatsHolder from '../../../containers/Outcomes/MobileStateHolder'
import ActionTabs from '../../../containers/Outcomes/ActionTabs'

import isInvitesTable from '../../../utils/isInvitesTablet'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

class SettingsMap extends Component<IProps> {
  _getActionSection = () => {
    const { appID, rowID, payload = [], manageMapAction, changeMapNameAction, changeMapNameSaveAction } = this.props

    if (isInvitesTable(appID)) {
      return null
    }

    return (
      <Fragment>
        <hr className={styles.hr} />
        <ActionTabs
          rowID={rowID}
          payload={payload}
          manageMap={manageMapAction}
          mapNameChange={{ start: changeMapNameAction, save: changeMapNameSaveAction }}
        />
      </Fragment>
    )
  }

  _getExtraToolboxSection = () => {
    const { children, uuid, outcomeType } = this.props

    const ClonedChild = React.cloneElement(children, { uuid })

    if (!children || !ClonedChild || !outcomeType) {
      return null
    }

    return (
      <React.Fragment>
        {<hr className={styles.hr} />}
        {ClonedChild}
      </React.Fragment>
    )
  }

  render() {
    const { payload = [], uuid } = this.props

    return (
      <div className={styles.settingsContainer}>
        <MobileStatsHolder payload={payload} mapID={uuid} />
        {this._getActionSection()}
        {this._getExtraToolboxSection()}
      </div>
    )
  }
}

export default SettingsMap
