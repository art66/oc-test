export interface IProps {
  children?: any
  outcomeType: string
  manageMapAction?: (payload: object) => void
  changeMapNameAction?: (changeMapNameInProgress: boolean, ID: number) => void
  changeMapNameSaveAction?: (changeMapNameInProgress: boolean, ID: number, newName: string) => void
  rowID?: number
  uuid?: string
  appID?: number
  payload?: {
    name?: string
    data?: any
  }[]
}
