import React, { Component } from 'react'

import CopyMapLink from './CopyMapLink'
import SearchPerson from './SearchPerson'
import MembersContainer from './MembersContainer'
import CommonData from './CommonData'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class ShareMap extends Component<IProps> {
  _getSearchPerson = () => {
    const {
      rowID,
      inviteUsersList,
      inProgressSearch,
      addMemberInProgress,
      addMemberRowID,
      addMemberAction,
      addMemberCancelAction,
      addMemberSavedAction,
      chooseStaffTabAction,
      saveNewUserAttemptAction,
      currentTab,
      inProgressFetch
    } = this.props

    const { isMaxEditorsOnMap, isMaxTestersOnMap } = this._getMembersCount()

    return (
      <SearchPerson
        rowID={rowID}
        membersList={this._normalizeMembersList()}
        inProgressSearch={inProgressSearch}
        inProgressFetch={inProgressFetch}
        currentTab={currentTab}
        inviteUsersList={inviteUsersList}
        addMemberRowID={addMemberRowID}
        addMemberInProgress={addMemberInProgress}
        startAction={addMemberAction}
        saveAction={addMemberSavedAction}
        chooseStaffTabAction={chooseStaffTabAction}
        cancelAction={addMemberCancelAction}
        searchUser={saveNewUserAttemptAction}
        isMaxTestersOnMap={isMaxTestersOnMap}
        isMaxEditorsOnMap={isMaxEditorsOnMap}
      />
    )
  }

  _normalizeMembersList = () => {
    const { payload } = this.props

    return payload.membersList || payload[7] && payload[7].data.membersList
  }

  _getCopyLink = () => {
    const { payload } = this.props

    const inviteLink = payload instanceof Array ? payload[7] && payload[7].data.inviteLink : payload.inviteLink

    return <CopyMapLink inviteLink={inviteLink} />
  }

  _getMembers = () => {
    const { rowID, deleteMemberAction, changeMemberRoleAction } = this.props

    const membersToProcess = this._normalizeMembersList()

    if (!membersToProcess) {
      return null
    }

    const { isMaxEditorsOnMap, isMaxTestersOnMap } = this._getMembersCount()

    return (
      <MembersContainer
        rowID={rowID}
        membersList={membersToProcess}
        deleteMember={deleteMemberAction}
        changeRole={changeMemberRoleAction}
        isMaxEditorsOnMap={isMaxEditorsOnMap}
        isMaxTestersOnMap={isMaxTestersOnMap}
      />
    )
  }

  _normalizePayloadInAdoptiveMode = () => {
    const { payload } = this.props

    if (payload instanceof Array) {

    }
  }

  _getMembersCount = () => {
    const {
      payload,
      maxTesters,
      maxEditors
    } = this.props

    let editorsCount = payload.editorsOnMap
    let testersCount = payload.testersOnMap

    if (payload.editorsOnMap === undefined || payload.testersOnMap === undefined) {
      editorsCount = (payload[7] && payload[7].data.editorsOnMap) || 0
      testersCount = (payload[7] && payload[7].data.testersOnMap) || 0
    }

    return {
      editorsCount,
      testersCount,
      maxTesters,
      maxEditors,
      isMaxEditorsOnMap: editorsCount === maxEditors,
      isMaxTestersOnMap: testersCount === maxTesters
    }
  }

  _renderTopSection = () => {
    return (
      <div className={styles.topSection}>
        <div className={styles.inputsWrapper}>
          {this._getSearchPerson()}
          {this._getCopyLink()}
        </div>
        {this._getMembers()}
      </div>
    )
  }

  _renderBottomSection = () => {
    const {
      rowID,
      shareMapAction,
      shareMapCancelAction,
      inProgressSave
    } = this.props

    const {
      maxTesters,
      maxEditors,
      editorsCount,
      testersCount
    } = this._getMembersCount()

    return (
      <div className={styles.bottomSection}>
        <CommonData
          rowID={rowID}
          maxTesters={maxTesters}
          maxEditors={maxEditors}
          inProgress={inProgressSave}
          editorsOnMap={editorsCount}
          testersOnMap={testersCount}
          saveAction={shareMapAction}
          cancelAction={shareMapCancelAction}
        />
      </div>
    )
  }

  render() {
    return (
      <div className={styles.shareMapContainer}>
        {this._renderTopSection()}
        <hr className={styles.hr} />
        {this._renderBottomSection()}
      </div>
    )
  }
}

export default ShareMap
