
interface ICategory {
  ID: number
  name: string
  category: string
  online?: boolean
}

export interface IInvitedUsers {
  all?: ICategory
  faction?: ICategory
  company?: ICategory
  friends?: ICategory
}

export interface IProps {
  membersList: {
    ID: number
  }[]
  userType?: string
  userFilter?: string
  inProgress: boolean
  inviteUsersList?: {
    id: string
    name: string
    online: string
  }[]
  currentTab: string,
  chooseTab: (currentTab: string) => void
  saveMember?: (member: object) => void
}

export interface IState {
  isButtonsFitContainer: boolean
}
