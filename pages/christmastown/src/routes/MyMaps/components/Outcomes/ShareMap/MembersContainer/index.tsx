import React, { PureComponent, Fragment } from 'react'
import classnames from 'classnames'

import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { withTheme, TWithThemeInjectedProps} from '@torn/shared/hoc/withTheme/withTheme'

import {
  BLUE_HOVERED,
  ORANGE_DELETED,
  SEARCH_TOGGLE_ICON_NAMES,
  SEARCH_TOGGLE_ICON_DIMENSIONS,
  BLUE_HOVERED_DARK,
  ORANGE_DELETED_DARK
} from '../../../../constants'
import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'
import {DARK_THEME} from "../../../../../../constants";

const SVG_CLOSE_ICON_DIMENSIONS = {
  width: 10,
  height: 10
}

class MembersContainer extends PureComponent<IProps & TWithThemeInjectedProps, IState> {
  constructor(props: IProps & TWithThemeInjectedProps) {
    super(props)

    this.state = {
      memberID: null,
      memberType: '',
      isDeleteButtonHovered: false
    }
  }

  componentDidMount() {
    this._initializeTooltip()
  }

  componentDidUpdate() {
    this._initializeTooltip()
  }

  _initializeTooltip = () => {
    const { rowID, membersList = [] } = this.props

    if (!membersList || membersList.length === 0) {
      return null
    }

    membersList.forEach(({ ID, type }) => {
      tooltipsSubscriber.subscribe({
        ID: `${rowID}-${ID}-${type}-removeStaff`,
        child: `Remove from ${type}s`
      })
    })
  }

  _classNamesHolder = () => {
    const { isMaxEditorsOnMap, isMaxTestersOnMap } = this.props

    const toggleButtonClass = classnames({
      [styles.iconButton]: true,
      [styles.allSlotsSet]: isMaxEditorsOnMap && isMaxTestersOnMap
    })

    return {
      toggleButtonClass
    }
  }

  _getMemberID = ({ target }) => {
    const {
      dataset: { id: ID, type }
    } = target

    return {
      memberID: Number(ID),
      memberType: type
    }
  }

  _makeDefaultState = () => {
    this.setState({
      isDeleteButtonHovered: false,
      memberID: null,
      memberType: ''
    })
  }

  _handlerHover = e => {
    const { memberID, memberType } = this._getMemberID(e)

    this.setState({
      isDeleteButtonHovered: true,
      memberID,
      memberType
    })
  }

  _handlerBlur = () => {
    this.setState({
      isDeleteButtonHovered: false
    })
  }

  _handlerDelete = () => {
    const { memberID, memberType } = this.state
    const { deleteMember, rowID } = this.props

    deleteMember(memberID, memberType, rowID)
    this._makeDefaultState()
  }

  _handlerToggle = ({ target }) => {
    const {
      dataset: { id: memberID, type: memberType }
    } = target
    const { changeRole, rowID, isMaxEditorsOnMap, isMaxTestersOnMap } = this.props

    if (isMaxEditorsOnMap && isMaxTestersOnMap) {
      return
    }

    const member = {
      memberID: Number(memberID),
      memberType: memberType === 'editor' ? 'tester' : 'editor'
    }

    changeRole(member, rowID)
  }

  _getColor = ID => {
    const { isDeleteButtonHovered, memberID } = this.state
    const { theme } = this.props
    const isDarkMode = theme === DARK_THEME
    const orangeFill = isDarkMode ? ORANGE_DELETED_DARK : ORANGE_DELETED
    const blueFill = isDarkMode ? BLUE_HOVERED_DARK : BLUE_HOVERED

    const fillColor = memberID === ID && isDeleteButtonHovered ? orangeFill : blueFill

    return fillColor
  }

  _getMember = member => {
    const { ID, type, name } = member
    const { rowID, theme } = this.props

    const isDarkMode = theme === DARK_THEME
    const fillColor = this._getColor(ID)
    const layoutMemberName = `${name}[${ID}]`
    const { toggleButtonClass } = this._classNamesHolder()
    const iconName = SEARCH_TOGGLE_ICON_NAMES[type]
    const iconDimensions = SEARCH_TOGGLE_ICON_DIMENSIONS[iconName]

    return (
      <Fragment key={ID + name}>
        <div className={`${styles.engadedUserContainer} ${styles.tester}`}>
          <div className={styles.svgIconWrap}>
            <button
              className={toggleButtonClass}
              type='button'
              data-id={ID}
              data-type={type}
              onClick={this._handlerToggle}
            >
              <SVGIconGenerator
                iconName={iconName}
                fill={{ name: isDarkMode ? BLUE_HOVERED_DARK : BLUE_HOVERED }}
                dimensions={iconDimensions}
              />
            </button>
          </div>
          <div className={styles.userTitleWrap}>
            <span className={styles.userTitle}>{layoutMemberName}</span>
          </div>
          <button
            id={`${rowID}-${ID}-${type}-removeStaff`}
            type='button'
            data-id={ID}
            data-type={type}
            className={styles.buttonDeleteUser}
            onClick={this._handlerDelete}
            onMouseEnter={this._handlerHover}
            onMouseLeave={this._handlerBlur}
            onTouchStart={this._handlerHover}
          >
            <SVGIconGenerator iconName='Close' fill={{ name: fillColor }} dimensions={SVG_CLOSE_ICON_DIMENSIONS} />
          </button>
        </div>
      </Fragment>
    )
  }

  _renderMembersList = membersList => {
    return membersList.map(member => {
      return this._getMember(member)
    })
  }

  render() {
    const { membersList = [] } = this.props

    if (!membersList || membersList.length === 0) {
      return null
    }

    return <div className={styles.engagedUsersHolder}>{this._renderMembersList(membersList)}</div>
  }
}

export default withTheme(MembersContainer)
