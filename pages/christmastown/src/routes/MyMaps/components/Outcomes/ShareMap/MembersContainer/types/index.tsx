export interface IMember {
  ID?: number
  type?: string
  name?: string
}
export interface IProps {
  membersList?: IMember[]
  isMaxEditorsOnMap: boolean
  isMaxTestersOnMap: boolean
  rowID?: number
  deleteMember?: (memberID: number, memberType: string, rowID: number) => void
  changeRole?: (member: object, rowID: number) => void
}

export interface IState {
  isDeleteButtonHovered?: boolean
  memberID?: number
  memberType?: string
}
