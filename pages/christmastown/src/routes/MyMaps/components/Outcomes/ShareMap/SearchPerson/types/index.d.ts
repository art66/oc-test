import { IInvitedUsers } from '../DropDownList/types'

export interface IProps {
  rowID?: number
  membersList: {
    ID: number
  }[]
  isMaxEditorsOnMap: boolean
  isMaxTestersOnMap: boolean
  addMemberRowID?: number
  addMemberInProgress?: boolean
  currentTab: string
  inProgressFetch: boolean
  inProgressSearch: boolean
  inviteUsersList?: IInvitedUsers
  startAction?: (addMemberInProgress: boolean, rowID: number, isRequested: boolean) => void
  cancelAction?: (addMemberInProgress: boolean, rowID: number, isRequested: boolean) => void
  saveAction?: (addMemberInProgress: boolean, rowID: number, member: object) => void
  chooseStaffTabAction: (currentTab: string, isRequested: boolean) => void
  searchUser: (searchText: string, isRequested: boolean) => void
}

export interface IState {
  filteredUserList?: object[]
  isSearchingUserByTyping: boolean
  isFirstSearch?: boolean
  inputValue?: string
  userType?: string
}
