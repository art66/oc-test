// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'button': string;
  'copyButton': string;
  'copyButtonHolder': string;
  'copyLink': string;
  'copyLinkHolder': string;
  'copyLinkWrap': string;
  'copyText': string;
  'flexEvently': string;
  'globalSvgShadow': string;
  'svgCopyIcon': string;
}
export const cssExports: CssExports;
export default cssExports;
