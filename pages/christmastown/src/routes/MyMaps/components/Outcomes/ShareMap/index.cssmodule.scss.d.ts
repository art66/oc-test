// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'bottomSection': string;
  'defaults': string;
  'flexColumn': string;
  'flexRow': string;
  'globalSvgShadow': string;
  'hr': string;
  'inputEmpty': string;
  'inputMaxLengthExceeded': string;
  'inputSpace': string;
  'inputsWrapper': string;
  'loadingSpinner': string;
  'mainInput': string;
  'shareMapContainer': string;
  'spinnerAnimation': string;
  'spinnerLoop': string;
  'topSection': string;
  'wrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
