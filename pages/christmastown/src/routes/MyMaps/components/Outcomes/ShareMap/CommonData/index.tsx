import React, { Component, Fragment } from 'react'
import ActionButtons from '../../ActionButtons'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const CANCEL_LABEL = 'Cancel'
const SAVE_LABEL = 'Save'
const EDITORS = 'Editors positions:'
const TESTERS = 'Testers positions:'

class CommonData extends Component<IProps> {
  _handlerCancel = () => {
    const { rowID, cancelAction } = this.props

    cancelAction(false, rowID)
  }

  _handlerSave = () => {
    const { rowID, saveAction } = this.props

    saveAction(true, rowID)
  }

  _getUsersCountOnMap = () => {
    const { editorsOnMap, testersOnMap, maxTesters, maxEditors } = this.props

    const editorsCount = `${maxEditors - editorsOnMap}/${maxEditors}.`
    const testersCount = `${maxTesters - testersOnMap}/${maxTesters}`

    return {
      editorsCount,
      testersCount
    }
  }

  render() {
    const { inProgress } = this.props
    const { editorsCount, testersCount } = this._getUsersCountOnMap()

    return (
      <Fragment>
        <div className={styles.positionsHolder}>
          <div className={styles.peopleCapacity}>
            <span className={styles.text}>{EDITORS}</span>
            <span className={styles.text}>{editorsCount}</span>
          </div>
          <div className={styles.peopleCapacity}>
            <span className={styles.text}>{TESTERS}</span>
            <span className={styles.text}>{testersCount}</span>
          </div>
        </div>
        <div className={styles.buttonsHolder}>
          <ActionButtons
            inProgress={inProgress}
            cancelLabel={CANCEL_LABEL}
            saveLabel={SAVE_LABEL}
            cancelAction={this._handlerCancel}
            saveAction={this._handlerSave}
          />
        </div>
      </Fragment>
    )
  }
}

export default CommonData
