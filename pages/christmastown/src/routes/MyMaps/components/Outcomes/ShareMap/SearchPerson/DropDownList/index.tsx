import React, { PureComponent } from 'react'
import classnames from 'classnames'

import isValue from '@torn/shared/utils/isValue'
import upperLeadingLetter from '../../../../../utils/upperLeadingLetter'
import getCurrentTabID from '../../../../../utils/getCurrentTabID'

import { IProps, IState } from './types'
import { TABS_ADAPTER } from '../../../../../constants'
import styles from './index.cssmodule.scss'
import globalStyles from '../../../../../styles/glob.cssmodule.scss'

const MINIMAL_CONTAINER_USERS_CAPACITY = 5
const TABS_LABELS = ['Friends', 'Faction', 'Company', 'All']

class DropDownList extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      isButtonsFitContainer: true
    }
  }

  componentDidMount() {
    this._setUsersCapacityFlag()
  }

  componentDidUpdate() {
    this._setUsersCapacityFlag()
  }

  _handlerTabClick = e => {
    e.preventDefault()

    const { chooseTab, currentTab } = this.props
    const currentTabID = getCurrentTabID(currentTab)

    const {
      dataset: { id: ID }
    } = e.target

    if (Number(ID) === currentTabID) {
      return
    }

    chooseTab(TABS_ADAPTER[Number(ID)])
  }

  _handlerUserClick = ({ target }) => {
    const { saveMember, userType } = this.props

    const {
      dataset: { name, id: ID }
    } = target

    const member = {
      name,
      ID: Number(ID),
      type: userType
    }

    saveMember(member)
  }

  _classNameHolder = (status?: string, isSelected?: boolean) => {
    const { inProgress } = this.props

    const onlineClass = classnames({
      [styles.onlineStatus]: true,
      [styles.userOnline]: status === 'online'
    })

    const userButtonClass = classnames({
      [styles.userButton]: true,
      [styles.alreadySelected]: isSelected
    })

    const placeholderSpinner = classnames({
      [globalStyles.loadingSpinner]: inProgress,
      [styles.loadingAnim]: inProgress
    })

    return {
      onlineClass,
      userButtonClass,
      placeholderSpinner
    }
  }

  _usersClassNameHolder = () => {
    const { isButtonsFitContainer } = this.state

    const invitesClass = classnames({
      [styles.inviteUsersList]: true,
      [styles.noScroll]: isButtonsFitContainer
    })

    return invitesClass
  }

  _tabButtonClassNameHolder = ID => {
    const { currentTab } = this.props

    const buttonClass = classnames({
      [styles.tabButton]: true,
      [styles.tabButtonClicked]: getCurrentTabID(currentTab) === ID
    })

    return buttonClass
  }

  _isShowPlaceholder = (usersToRender: object[]) => {
    const { inProgress } = this.props

    return inProgress || !usersToRender || usersToRender.length === 0
  }

  _setUsersCapacityFlag = () => {
    const { inProgress, inviteUsersList } = this.props
    const usersToRender = inviteUsersList || []

    const isShowPlaceholder = (inProgress && !usersToRender || !isValue(usersToRender)) || usersToRender.length === 0
    const isTooLessUsers = usersToRender && usersToRender.length <= MINIMAL_CONTAINER_USERS_CAPACITY

    if (isShowPlaceholder || isTooLessUsers) {
      this.setState(prevState => ({
        ...prevState,
        isButtonsFitContainer: true
      }))

      return
    }

    this.setState(prevState => ({
      ...prevState,
      isButtonsFitContainer: false
    }))
  }

  _renderTabSections = () => {
    return TABS_LABELS.map((tab, index) => {
      const buttonClass = this._tabButtonClassNameHolder(index)

      return (
        <button key={tab} className={buttonClass} data-id={index} onMouseDown={this._handlerTabClick}>
          {tab}
        </button>
      )
    })
  }

  _renderPlaceholder = (placeholderText: string, isSpinner?: boolean) => {
    const { placeholderSpinner } = this._classNameHolder()

    return (
      <div className={styles.dropDownListPlaceholder}>
        {isSpinner && <i className={placeholderSpinner}/>}
        <span className={styles.dropDownListPlaceholderText}>
          {placeholderText}
        </span>
      </div>
    )
  }

  _renderInviteUsersList = () => {
    const { userFilter, membersList, inviteUsersList, inProgress } = this.props

    if (inProgress && (!isValue(inviteUsersList) || !(inviteUsersList && inviteUsersList.length))) {
      return this._renderPlaceholder('Trying to fetch users...', true)
    }

    if (!inviteUsersList || inviteUsersList.length === 0) {
      const placeholderText = userFilter.length !== 0 ? 'No users found.' : 'There is no users in this category yet.'

      return this._renderPlaceholder(placeholderText)
    }

    return inviteUsersList.map(({ id, name, online }) => {
      const userLabel = `${upperLeadingLetter(name)}[${id}]`
      const isAlreadySelected = membersList.some(member => Number(member.ID) === Number(id))
      const { onlineClass, userButtonClass } = this._classNameHolder(online, isAlreadySelected)

      return (
        <button
          key={id + name}
          className={userButtonClass}
          data-name={name}
          data-id={id}
          onMouseDown={!isAlreadySelected ? this._handlerUserClick : undefined}
        >
          <i className={onlineClass} />
          <span className={styles.title}>{userLabel}</span>
          {isAlreadySelected && <span className={styles.alreadyInLabel}>(already in)</span>}
        </button>
      )
    })
  }

  render() {
    const invitesClass = this._usersClassNameHolder()

    return (
      <div className={styles.dropDownListWrap}>
        <div className={styles.tabsList}>{this._renderTabSections()}</div>
        <div className={invitesClass}>{this._renderInviteUsersList()}</div>
      </div>
    )
  }
}

export default DropDownList
