// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'alreadyInLabel': string;
  'alreadySelected': string;
  'button': string;
  'dropDownListPlaceholder': string;
  'dropDownListPlaceholderText': string;
  'dropDownListWrap': string;
  'flexButton': string;
  'flexColumn': string;
  'flexRow': string;
  'fonts': string;
  'globalSvgShadow': string;
  'inviteUsersList': string;
  'loadingAnim': string;
  'noScroll': string;
  'onlineStatus': string;
  'tabButton': string;
  'tabButtonClicked': string;
  'tabsList': string;
  'title': string;
  'userButton': string;
  'userOnline': string;
}
export const cssExports: CssExports;
export default cssExports;
