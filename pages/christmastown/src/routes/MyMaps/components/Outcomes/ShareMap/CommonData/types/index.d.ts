export interface IProps {
  inProgress?: boolean
  maxTesters: number
  maxEditors: number
  editorsOnMap?: number
  testersOnMap?: number
  rowID?: number
  saveAction?: (leaveMapInProgress: boolean, rowID: number) => void
  cancelAction?: (leaveMapInProgress: boolean, rowID: number) => void
}
