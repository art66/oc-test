import React, { PureComponent } from 'react'
import classnames from 'classnames'

import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { DebounceDOM } from '@torn/shared/utils/debounce'
import securedRegExpString from '@torn/shared/utils/secureRegExpString'
import { withTheme, TWithThemeInjectedProps } from "@torn/shared/hoc/withTheme/withTheme";

import getCurrentTabID from '../../../../utils/getCurrentTabID'
import normalizeCurrentTabLabel from '../../../../utils/normalizeCurrentTabLabel'
import {
  BLUE_DIMMED,
  BLUE_DIMMED_DARK,
  BLUE_HOVERED,
  BLUE_HOVERED_DARK,
  SEARCH_TOGGLE_ICON_DIMENSIONS,
  SEARCH_TOGGLE_ICON_NAMES
} from '../../../../constants'

import DropDownList from './DropDownList'

import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'
import {DARK_THEME} from "../../../../../../constants";

const SORT_TYPES = {
  0: 'friends',
  1: 'faction',
  2: 'company',
  3: 'all'
}
const SEARCH_PLACEHOLDER = 'Invite editor or tester'
const SEARCH_LOOP_DIMENSIONS = {
  width: 14,
  height: 14,
  viewbox: '0 0 14 14'
}

class SearchPerson extends PureComponent<IProps & TWithThemeInjectedProps, IState> {
  private _refInput: React.RefObject<HTMLInputElement>
  private _debounce: any

  constructor(props: IProps & TWithThemeInjectedProps) {
    super(props)

    this.state = {
      isFirstSearch: false,
      filteredUserList: null,
      isSearchingUserByTyping: false,
      inputValue: '',
      userType: 'editor'
    }

    this._refInput = React.createRef()
  }

  componentDidMount() {
    this._initializeTooltip('mount')
    this._initializeUsersSearchThrownDebounce()
  }

  componentDidUpdate() {
    this._initializeTooltip('update')
    this._forceStateUpdateOnMaxStaffCount()
  }

  componentWillUnmount() {
    this._stopUsersSearchThrownDebounce()
  }

  _initializeTooltip = (type: string) => {
    const { rowID } = this.props
    const { allStaffFull, testersFull, editorsFull } = this._checkIsSomeStaffTypeFull()

    const getTooltipMessage = () => {
      if (allStaffFull) {
        return 'All slots are set.'
      }

      if (testersFull) {
        return 'Testers are exceeded, only editors allowed.'
      }

      if (editorsFull) {
        return 'Editors are exceeded, only testers allowed.'
      }

      return 'Change staff role'
    }

    tooltipsSubscriber.render({
      tooltipsList: [
        {
          ID: `${rowID}-changeRole`,
          child: getTooltipMessage()
        }
      ],
      type
    })
  }

  _forceStateUpdateOnMaxStaffCount = () => {
    const { userType } = this.state
    const { isSomeStaffTypeFull } = this._checkIsSomeStaffTypeFull()

    if (userType !== isSomeStaffTypeFull && isSomeStaffTypeFull) {
      this.setState({
        userType: isSomeStaffTypeFull
      })
    }
  }

  _initializeUsersSearchThrownDebounce = () => {
    this._debounce = new DebounceDOM()

    this._debounce.run({
      event: 'input',
      target: this._refInput.current,
      callback: this._fetchUsersSearch,
      delay: 300
    })
  }

  _stopUsersSearchThrownDebounce = () => this._debounce.stop()

  _fetchUsersSearch = () => {
    const { inputValue } = this.state
    const { searchUser } = this.props
    const usersList = this._filterInviteUsers()

    if (inputValue && usersList && usersList.length === 0) {
      searchUser(inputValue, true)

      this.setState({
        isSearchingUserByTyping: true
      })
    } else {
      this.setState({
        isSearchingUserByTyping: false
      })
    }
  }

  _classNameHolder = () => {
    const { addMemberInProgress, isMaxEditorsOnMap, isMaxTestersOnMap } = this.props
    const { allStaffFull } = this._checkIsSomeStaffTypeFull()

    const changeRoleClass = classnames({
      [styles.button]: true,
      [styles.toggleButton]: true,
      [styles.svgIcon]: true,
      [styles.testersMax]: isMaxTestersOnMap,
      [styles.editorsMax]: isMaxEditorsOnMap,
      [styles.maxUsers]: isMaxEditorsOnMap && isMaxTestersOnMap
    })

    const inputClass = classnames({
      [styles.searchUser]: true,
      [styles.noBottomBorder]: addMemberInProgress,
      [styles.allSlotsAreOut]: allStaffFull
    })

    const enhencerLeftClass = classnames({
      [styles.searchEnhencerLeftHolder]: true,
      [styles.noBottomBorder]: addMemberInProgress
    })

    return {
      changeRoleClass,
      inputClass,
      enhencerLeftClass
    }
  }

  _getAllUsersList = () => {
    const { inviteUsersList } = this.props
    const allUsers = []

    const checkIfUserAlreadyInAllList = (key: string) => allUsers.some((user) => {
      return inviteUsersList[key].some((inviteUser) => Number(user.id) === Number(inviteUser.id))
    })

    Object.keys(inviteUsersList).forEach((key) => {
      if (inviteUsersList[key] === null || checkIfUserAlreadyInAllList(key)) {
        return
      }

      allUsers.push(...inviteUsersList[key])
    })

    return allUsers
  }

  _filterInviteUsers = () => {
    const { inputValue } = this.state
    const { inviteUsersList, currentTab } = this.props

    const flagToSort = SORT_TYPES[getCurrentTabID(currentTab)]
    const normalizedTab = normalizeCurrentTabLabel(currentTab)
    const usersList = !flagToSort || flagToSort === 'all' ? this._getAllUsersList() : inviteUsersList[normalizedTab]

    if (!usersList) {
      return null
    }

    if (!inputValue) {
      return usersList
    }

    const secureRegExpInputValue = securedRegExpString(inputValue)

    const matchTrigger = new RegExp(secureRegExpInputValue, 'ig')
    const inviteUsersListPreFiltered = usersList.filter((user) => {
      return user.id.toString().match(matchTrigger) || user.name.match(matchTrigger)
    })

    return inviteUsersListPreFiltered
  }

  _fireUsersFetchOnFirstInputClick = () => {
    const { isFirstSearch } = this.state

    if (isFirstSearch) {
      return
    }

    this.setState({
      isFirstSearch: false
    })

    this._handleChooseMembersTab(null)
  }

  _handlerChange = ({ target }) => {
    this.setState({
      inputValue: target.value
    })
  }

  _handlerStart = () => {
    const { startAction, rowID, addMemberInProgress } = this.props

    if (addMemberInProgress) {
      return
    }

    this._fireUsersFetchOnFirstInputClick() // we need to fire search on the first input click even without tabs click!
    startAction(true, rowID, true)
  }

  _handlerSave = (member) => {
    const { saveAction, rowID } = this.props

    saveAction(false, rowID, member)
  }

  _handleChooseMembersTab = (currentTab) => {
    const { chooseStaffTabAction } = this.props

    chooseStaffTabAction(currentTab, true)
  }

  _handlerCancel = () => {
    const { cancelAction, rowID } = this.props

    cancelAction(false, rowID, false)
    this._refInput && this._refInput.current.blur()
  }

  _handlerToggle = () => {
    this.setState((prevState) => ({
      ...prevState,
      userType: prevState.userType === 'editor' ? 'tester' : 'editor'
    }))
  }

  _checkIsSomeStaffTypeFull = () => {
    const { isMaxEditorsOnMap, isMaxTestersOnMap } = this.props

    const isSomeStaffTypeFull = (isMaxEditorsOnMap && 'tester') || (isMaxTestersOnMap && 'editor') || null

    return {
      isSomeStaffTypeFull,
      testersFull: isMaxTestersOnMap,
      editorsFull: isMaxEditorsOnMap,
      allStaffFull: isMaxTestersOnMap && isMaxEditorsOnMap
    }
  }

  _renderChangeRoleButton = () => {
    const { userType } = this.state
    const { rowID, theme } = this.props

    const { changeRoleClass } = this._classNameHolder()
    const { isSomeStaffTypeFull } = this._checkIsSomeStaffTypeFull()

    const currentStaffIcon = isSomeStaffTypeFull || userType
    const currentIconName = SEARCH_TOGGLE_ICON_NAMES[currentStaffIcon]
    const currentIConDimensions = SEARCH_TOGGLE_ICON_DIMENSIONS[currentIconName]

    return (
      <button
        id={`${rowID}-changeRole`}
        className={changeRoleClass}
        type='button'
        onClick={!isSomeStaffTypeFull ? this._handlerToggle : undefined}
      >
        <SVGIconGenerator
          iconName={currentIconName} fill={{ name: theme === DARK_THEME ? BLUE_HOVERED_DARK : BLUE_HOVERED }}
          dimensions={currentIConDimensions}
        />
      </button>
    )
  }

  _renderInput = () => {
    const { inputValue } = this.state
    const { inputClass } = this._classNameHolder()
    const { allStaffFull } = this._checkIsSomeStaffTypeFull()

    return (
      <input
        ref={this._refInput}
        className={inputClass}
        placeholder={SEARCH_PLACEHOLDER}
        value={!allStaffFull ? inputValue : 'All slots are engaged'}
        type='text'
        onClick={!allStaffFull ? this._handlerStart : undefined}
        onChange={!allStaffFull ? this._handlerChange : undefined}
        readOnly={allStaffFull}
      />
    )
  }

  _renderInputSearchIcon = () => {
    const { isSearchingUserByTyping } = this.state
    const { inProgressSearch, theme } = this.props

    if (inProgressSearch && isSearchingUserByTyping) {
      return <span className={styles.searchTypingLoader} />
    }

    return (
      <span className={`${styles.svgIcon} ${styles.svgIconLoop}`}>
        <SVGIconGenerator
          iconName='Search' fill={{ name: theme === DARK_THEME ? BLUE_DIMMED_DARK : BLUE_DIMMED }}
          dimensions={SEARCH_LOOP_DIMENSIONS}
        />
      </span>
    )
  }

  _renderDropDownList = () => {
    const { userType, inputValue } = this.state
    const { addMemberInProgress = false, currentTab, inProgressFetch, membersList } = this.props

    if (!addMemberInProgress) {
      return null
    }

    return (
      <DropDownList
        membersList={membersList}
        inProgress={inProgressFetch}
        userFilter={inputValue}
        inviteUsersList={this._filterInviteUsers()}
        chooseTab={this._handleChooseMembersTab}
        currentTab={currentTab}
        saveMember={this._handlerSave}
        userType={userType}
      />
    )
  }

  render() {
    const { enhencerLeftClass } = this._classNameHolder()

    return (
      <div className={styles.searchWrap}>
        <div className={enhencerLeftClass}>{this._renderChangeRoleButton()}</div>
        <div className={styles.searchUserHolder} onBlur={this._handlerCancel}>
          {this._renderInput()}
          {this._renderDropDownList()}
        </div>
        <div className={styles.searchEnhencerRightHolder}>{this._renderInputSearchIcon()}</div>
      </div>
    )
  }
}

export default withTheme(SearchPerson)
