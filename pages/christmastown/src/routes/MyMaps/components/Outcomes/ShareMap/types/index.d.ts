import { IInvitedUsers } from '../SearchPerson/DropDownList/types'

export interface IProps {
  shareMapAction?: (isRequested: boolean, rowID: number) => void
  shareMapCancelAction?: (leaveMapInProgress: boolean, rowID: number) => void
  deleteMemberAction?: (memberID: number, memberType: string, rowID: number) => void
  addMemberAction?: (addMemberInProgress: boolean, rowID: number, isRequested: boolean) => void
  addMemberCancelAction?: (addMemberInProgress: boolean, rowID: number, isRequested: boolean) => void
  addMemberSavedAction?: (addMemberInProgress: boolean, rowID: number, member: object) => void
  changeMemberRoleAction?: (member: object, rowID: number) => void
  chooseStaffTabAction: (currentTab: string) => void
  saveNewUserAttemptAction: (searchText: string, isRequested: boolean) => void
  inProgressSave?: boolean
  inProgressFetch?: boolean
  inProgressSearch?: boolean
  rowID?: number
  currentTab: string
  addMemberInProgress?: boolean
  addMemberRowID?: number
  inviteUsersList?: IInvitedUsers
  maxTesters: number
  maxEditors: number
  payload?: {
    membersList?: {
      ID: number
    }[]
    sections?: {
      data?: object[]
    }[]
    inviteLink?: string
    editorsOnMap?: number
    testersOnMap?: number
  }
}
