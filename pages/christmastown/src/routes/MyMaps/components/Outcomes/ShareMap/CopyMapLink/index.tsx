import React, { PureComponent } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import getCurrentPageURL from '@torn/shared/utils/getCurrentPageURL'
import { withTheme, TWithThemeInjectedProps } from '@torn/shared/hoc/withTheme/withTheme'

import { BLUE_HOVERED, BLUE_HOVERED_DARK } from '../../../../constants'
import { IProps } from './types'
import styles from './index.cssmodule.scss'
import { DARK_THEME } from '../../../../../../constants'

const COPY_LABEL = 'Copy link'
const LINK_PLACEHOLDER = 'There is no link yet...'

class CopyMaplink extends PureComponent<IProps & TWithThemeInjectedProps> {
  private _ref: React.RefObject<HTMLInputElement>

  constructor(props: IProps & TWithThemeInjectedProps) {
    super(props)

    this._ref = React.createRef()
  }

  _handlerCopy = () => {
    const copyText = this._ref.current as React.RefObject<HTMLInputElement> & HTMLInputElement

    copyText.select()
    copyText.setSelectionRange(0, 99999)
    document.execCommand('copy')
  }

  render() {
    const { inviteLink = '', theme } = this.props
    const isDarkMode = theme === DARK_THEME

    // console.log(inviteLink, 'inviteLink')

    return (
      <div className={styles.copyLinkWrap}>
        <div className={styles.copyLinkHolder}>
          <input
            ref={this._ref}
            className={styles.copyLink}
            placeholder={LINK_PLACEHOLDER}
            value={`${getCurrentPageURL({ isOriginOnly: true })}${inviteLink}`}
            type='text'
            readOnly={true}
          />
        </div>
        <div className={styles.copyButtonHolder}>
          <button type='button' className={styles.copyButton} onClick={this._handlerCopy}>
            <span className={styles.svgCopyIcon}>
              <SVGIconGenerator iconName='Link' fill={{ name: isDarkMode ? BLUE_HOVERED_DARK : BLUE_HOVERED }} />
            </span>
            <span className={styles.copyText}>{COPY_LABEL}</span>
          </button>
        </div>
      </div>
    )
  }
}

export default withTheme(CopyMaplink)
