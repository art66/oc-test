export interface IProps {
  inProgress?: boolean
  cancelNewMapAction?: (isRequested: boolean, newMapInProgress: boolean) => void
  saveNewMapAction?: (isRequested: boolean, newMapName: string) => void
}

export interface IState {
  isMaxLengthExceeded?: boolean
  isOnlySpacesInputted: boolean
  isClicked: boolean
  inputValue?: string
}
