import React, { PureComponent } from 'react'
import classnames from 'classnames'
import InputStringChecker from '@torn/shared/utils/stringChecker'

import ActionButtons from '../ActionButtons'
import TooltipWarning from '../../TooltipWarning'

import { IState, IProps } from './types'
import styles from './index.cssmodule.scss'
import globalStyles from '../../../styles/glob.cssmodule.scss'

const INPUT_PLACEHOLDER = 'Name your map'
const CANCEL_LABEL = 'Cancel'
const SAVE_LABEL = 'Save'
const ESCAPE_KEY_ID = 27
const ENTER_KEY_ID = 13

class AddMap extends PureComponent<IProps, IState> {
  private _stringChecker: any
  private _addMapInput: React.RefObject<HTMLInputElement>

  constructor(props: IProps) {
    super(props)

    this.state = {
      inputValue: '',
      isMaxLengthExceeded: false,
      isOnlySpacesInputted: false,
      isClicked: false
    }

    this._addMapInput = React.createRef()
  }

  componentDidMount() {
    this._focusInputOnMount()
    this._initializeStringChecker()
  }

  _initializeStringChecker = () => {
    this._stringChecker = new InputStringChecker({ maxLength: 20 })
  }

  _checkString = (value: string) => {
    const stringPayload = this._stringChecker.checkString({ text: value })

    return stringPayload
  }

  _focusInputOnMount = () => {
    this._addMapInput && this._addMapInput.current && this._addMapInput.current.focus()
  }

  _classNamesHolder = () => {
    const { inputValue, isClicked, isOnlySpacesInputted, isMaxLengthExceeded } = this.state

    const inputClass = classnames({
      [styles.input]: true,
      [globalStyles.mainInput]: true,
      [globalStyles.inputEmpty]: !inputValue && isClicked,
      [globalStyles.inputSpace]: isOnlySpacesInputted && isClicked,
      [globalStyles.inputMaxLengthExceeded]: isMaxLengthExceeded
    })

    return {
      inputClass
    }
  }

  _setFalseClick = () => {
    this.setState({
      isClicked: true
    })
  }

  _saveNewMap = () => {
    const { inputValue, isMaxLengthExceeded, isOnlySpacesInputted } = this.state
    const { saveNewMapAction } = this.props

    if (!inputValue || isOnlySpacesInputted || isMaxLengthExceeded) {
      this._setFalseClick()

      return
    }

    const normalizedMapName = inputValue.trimRight().trimLeft()

    saveNewMapAction(true, normalizedMapName)
  }

  _cancelNewMap = () => {
    const { cancelNewMapAction } = this.props

    cancelNewMapAction(false, false)
  }

  _handlerChange = e => {
    const { value } = e.target
    const { value: textNormalizedValue, isMaxLengthExceeded, isOnlySpacesInputted } = this._checkString(value)

    this.setState({
      inputValue: textNormalizedValue,
      isMaxLengthExceeded,
      isOnlySpacesInputted
    })
  }

  _handlerCancel = () => {
    this._cancelNewMap()
  }

  _handlerSave = () => {
    this._saveNewMap()
  }

  _handlerKeyDown = e => {
    const keyCode = e.which

    if (keyCode === ESCAPE_KEY_ID) {
      return this._cancelNewMap()
    }

    if (keyCode === ENTER_KEY_ID) {
      return this._saveNewMap()
    }
  }

  _renderWarningToolTip = () => {
    const { inputValue, isOnlySpacesInputted, isMaxLengthExceeded, isClicked } = this.state

    const tooltipsShowCases = {
      emptyString: !inputValue && isClicked,
      maxLength: isMaxLengthExceeded,
      onlySpacesEntered: isOnlySpacesInputted && isClicked
    }

    return (
      <TooltipWarning warningCases={tooltipsShowCases} />
    )
  }

  _renderInput = () => {
    const { inputValue } = this.state

    const { inputClass } = this._classNamesHolder()

    return (
      <React.Fragment>
        <input
          id='addMap_box'
          type='text'
          ref={this._addMapInput}
          className={inputClass}
          placeholder={INPUT_PLACEHOLDER}
          value={inputValue}
          onChange={this._handlerChange}
          onKeyDown={this._handlerKeyDown}
          data-lpignore='true'
        />
        <label
          className={styles.inputLabel}
          htmlFor='addMap_box'
          children={this._renderWarningToolTip()}
        />
      </React.Fragment>
    )
  }

  render() {
    const { inProgress } = this.props

    return (
      <div className={styles.addNewMap}>
        {this._renderInput()}
        <ActionButtons
          inProgress={inProgress}
          cancelLabel={CANCEL_LABEL}
          saveLabel={SAVE_LABEL}
          cancelAction={this._handlerCancel}
          saveAction={this._handlerSave}
        />
      </div>
    )
  }
}

export default AddMap
