export interface IProps {
  uuid?: string
  type?: string
  inProgress?: boolean
  payload?: {
    position?: string
  }
  rowID?: number
  leaveMapCancelAction?: (delateMapInProgress: boolean, isRequired: boolean, rowID: number) => void
  leaveMapAction?: (delateMapInProgress: boolean, uuid: string) => void
}
