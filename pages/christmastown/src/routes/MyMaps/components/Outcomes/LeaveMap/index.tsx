import React, { PureComponent } from 'react'
import TextMessage from '../TextMessage'

import { IProps } from './types'

const LEAVE_DESCRIPTION = 'Would you like to leave'
const TYPE = 'leave'

class LeaveMap extends PureComponent<IProps> {
  _handlerCancel = () => {
    const { leaveMapCancelAction, rowID } = this.props

    leaveMapCancelAction(false, false, rowID)
  }

  _handlerSave = () => {
    const { leaveMapAction, uuid } = this.props

    leaveMapAction(true, uuid)
  }

  render() {
    const {
      payload: { position = '' },
      inProgress
    } = this.props

    return (
      <TextMessage
        type={TYPE}
        inProgress={inProgress}
        position={position}
        description={LEAVE_DESCRIPTION}
        saveAction={this._handlerSave}
        cancelAction={this._handlerCancel}
      />
    )
  }
}

export default LeaveMap
