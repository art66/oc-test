import React, { PureComponent, Fragment } from 'react'
import classnames from 'classnames'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const SAVE = 'Save'
const CANCEL = 'Cancel'

class ActionButtons extends PureComponent<IProps> {
  _classNameHolder = () => {
    const { inProgress } = this.props

    const saveButtonClasses = classnames({
      [styles.button]: true,
      [styles.save]: true,
      [styles.inProgress]: inProgress
    })

    return {
      saveButtonClasses
    }
  }

  _handleSave = () => {
    const { saveAction, inProgress } = this.props

    if (!saveAction || inProgress) return

    saveAction()
  }

  _handleCancel = () => {
    const { cancelAction } = this.props

    if (!cancelAction) return

    cancelAction()
  }

  _cancelButton = () => {
    const { cancelLabel } = this.props

    return (
      <button className={`${styles.button} ${styles.cancel}`} type='button' onClick={this._handleCancel}>
        {cancelLabel || CANCEL}
      </button>
    )
  }

  _saveButton = () => {
    const { saveLabel, inProgress } = this.props
    const { saveButtonClasses } = this._classNameHolder()

    const labelText = saveLabel || SAVE
    const preloader = (
      <AnimationLoad
        dotsCount={5}
        dotsColor='white'
        animationDuaration='1'
        // isAdaptive={true}
      />
    )

    return (
      <button className={saveButtonClasses} type='button' onClick={this._handleSave}>
        {!inProgress ? labelText : preloader}
      </button>
    )
  }

  render() {
    return (
      <Fragment>
        {this._cancelButton()}
        {this._saveButton()}
      </Fragment>
    )
  }
}

export default ActionButtons
