export interface IProps {
  inProgress?: boolean
  cancelLabel?: string
  saveLabel?: string
  saveAction?: () => void
  cancelAction?: () => void
}
