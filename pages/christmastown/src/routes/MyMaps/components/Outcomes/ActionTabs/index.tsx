import React, { PureComponent } from 'react'
import classnames from 'classnames'

import { SVGIconGenerator } from '@torn/shared/SVG'
import getCurrentPageURL from '@torn/shared/utils/getCurrentPageURL'

import { ORANGE_DELETED, BLUE_HOVERED, BLUE_DIMMED } from '../../../constants'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

const TABS = [
  {
    name: 'Rename',
    icon: 'Pen',
    action: 'Rename'
  },
  {
    name: 'Join',
    icon: 'Join',
    action: 'Join'
  },
  {
    name: 'Invite',
    icon: 'Share',
    action: 'Share'
  },
  {
    name: 'Delete',
    icon: 'Trash',
    action: 'Trash'
  }
]

class ActionTabs extends PureComponent<IProps, any> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      clickedButtonID: null
    }
  }

  _classNameHolder = (index, name, action) => {
    const tabClass = classnames({
      [styles.tab]: true,
      [styles.tabClicked]: this._isTabClickedOrActivated(index, action),
      [styles.tabDeleteClicked]: this._isTabClickedOrActivated(index, action) && name === 'Delete'
    })

    return tabClass
  }

  _handleClick = ({ target }) => {
    const { rowID, manageMap } = this.props
    const {
      dataset: { id: ID, name }
    } = target

    this.setState({
      clickedButtonID: Number(ID)
    })

    const payload = {
      rowID,
      mobileLayout: true,
      manageType: name.toUpperCase(),
      manageMapInProgress: true
    }

    manageMap(payload)
  }

  _isTabClickedOrActivated = (index, action) => {
    const { manageType } = this.props
    const { clickedButtonID } = this.state

    return manageType && (clickedButtonID === index || manageType === action.toUpperCase())
  }

  _getFillName = (index, name, action) => {
    if (this._isTabClickedOrActivated(index, action)) {
      if (name === 'Delete') {
        return ORANGE_DELETED
      }

      return BLUE_HOVERED
    }

    return BLUE_DIMMED
  }

  _renderTabs = () => {
    const { payload } = this.props

    if (!payload.length) {
      return []
    }

    const {
      data: { joinLink }
    } = payload[7]

    return TABS.map(({ name, icon, action }, index) => {
      const CustomTag = name === 'Join' ? 'a' : 'button'
      const linkToRedirect = joinLink ? `${getCurrentPageURL({ isOriginOnly: true })}${joinLink}` : '#/'

      return (
        <CustomTag
          key={name}
          href={linkToRedirect}
          data-id={index}
          data-name={action}
          className={this._classNameHolder(index, name, action)}
          onClick={this._handleClick}
        >
          <SVGIconGenerator iconName={icon} fill={{ name: this._getFillName(index, name, action) }} />
          <span className={styles.title}>{name}</span>
        </CustomTag>
      )
    })
  }

  render() {
    return <div className={styles.actionTabsWrap}>{this._renderTabs()}</div>
  }
}

export default ActionTabs
