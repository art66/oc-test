export interface IProps {
  rowID?: number
  manageType?: string
  payload?: {
    data?: {
      joinLink?: string
    }
  }[]
  manageMap?: (payload: object) => void
  mapNameChange?: {
    start?: (changeMapNameInProgress: boolean, ID: number) => void
    save?: (changeMapNameInProgress: boolean, ID: number, newName: string) => void
  }
}
