import React, { PureComponent } from 'react'
import TextMessage from '../TextMessage'

import { IProps } from './types'

const TRASH_DESCRIPTION = 'Are you sure you would like to delete this map?'
const TYPE = 'delete'

class DeleteMap extends PureComponent<IProps> {
  _handlerCancel = () => {
    const { deleteMapCancelAction, rowID } = this.props

    deleteMapCancelAction(false, false, rowID)
  }

  _handlerSave = () => {
    const { deleteMapAction, rowID, uuid } = this.props

    deleteMapAction(true, rowID, uuid)
  }

  render() {
    const { inProgress } = this.props

    return (
      <TextMessage
        type={TYPE}
        inProgress={inProgress}
        description={TRASH_DESCRIPTION}
        saveAction={this._handlerSave}
        cancelAction={this._handlerCancel}
      />
    )
  }
}

export default DeleteMap
