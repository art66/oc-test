export interface IProps {
  uuid?: string
  inProgress?: boolean
  type?: string
  rowID?: number
  deleteMapCancelAction?: (delateMapInProgress: boolean, isRequested: boolean, rowID: number) => void
  deleteMapAction?: (isRequested: boolean, rowID: number, uuid: string) => void
}
