import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IProps {
  appID?: number
  rowID?: number
  uuid?: string
  outcomeType?: string
  isDesktopLayoutSet?: boolean
  mediaType?: TMediaType
  payload?: any
  mobileLayout?: boolean
}
