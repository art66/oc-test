import React, { PureComponent } from 'react'
import RatingBox from '../../../../../components/RatingBox'
import isValue from '@torn/shared/utils/isValue'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const RATING_SECTION = 'Rating'
const ID_FIXER = 1
const MAX_ITEM_PER_COLUMN = 2
const TITLE_HOLDER = ['Last Edit', 'Editors', 'Testers', 'Players', 'Rating']

class MobileStateHolder extends PureComponent<IProps> {
  _getColumn = (items, index) => {
    return (
      <div key={index} className={styles.columnWrap}>
        {items.map(item => item)}
      </div>
    )
  }

  _getItem = item => {
    const { name, data } = item
    const { activeJoinedMapID, mapID } = this.props

    if (name === 'Rating') {
      return (
        <div key={name + data} className={`${styles.keyValueContainer} ${styles.ratingContainer}`}>
          <div className={styles.ratingTitleWrap}>
            <span className={styles.title}>Rating: </span>
          </div>
          <RatingBox data={data} isMapJoinedActive={activeJoinedMapID === mapID} />
        </div>
      )
    }

    return (
      <div key={name + data} className={styles.keyValueContainer}>
        <span className={styles.title}>{name}: </span>
        <span className={styles.text}>{isValue(data) ? data : 'N/A'}</span>
      </div>
    )
  }

  _getSections = () => {
    const { payload = [] } = this.props

    let items = []
    const columns = []

    if (!payload.length) {
      return []
    }

    payload.forEach(section => {
      TITLE_HOLDER.forEach((title, index) => {
        if (section.name !== title) {
          return
        }

        items.push(this._getItem(section))

        if ((index + ID_FIXER) % MAX_ITEM_PER_COLUMN === 0 || section.name === RATING_SECTION) {
          columns.push(this._getColumn(items, index))
          items = []
          return
        }
      })
    })

    return columns
  }

  render() {
    return <div className={styles.mobileStatsWrap}>{this._getSections().map(item => item)}</div>
  }
}

export default MobileStateHolder
