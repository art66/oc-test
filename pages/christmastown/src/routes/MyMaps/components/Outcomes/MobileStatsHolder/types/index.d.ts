export interface IProps {
  activeJoinedMapID: string
  mapID: string
  payload?: {
    name?: string
    data?: string | number
  }[]
}
