// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'columnWrap': string;
  'globalSvgShadow': string;
  'hr': string;
  'inputEmpty': string;
  'inputMaxLengthExceeded': string;
  'inputSpace': string;
  'keyValueContainer': string;
  'loadingSpinner': string;
  'mainInput': string;
  'mobileStatsWrap': string;
  'ratingContainer': string;
  'ratingTitleWrap': string;
  'spinnerAnimation': string;
  'spinnerLoop': string;
  'text': string;
  'title': string;
  'wrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
