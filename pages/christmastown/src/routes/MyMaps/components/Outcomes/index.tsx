import React, { Component } from 'react'
// import { TransitionGroup, CSSTransition } from 'react-transition-group'
import classnames from 'classnames'
import checkMediaType from '@torn/shared/utils/checkMediaType'

import AddMap from '../../containers/Outcomes/AddMap'
import Empty from './Empty'
import RenameMap from './RenameMap'
import DeleteMap from '../../containers/Outcomes/DeleteMap'
import ShareMap from '../../containers/Outcomes/ShareMap'
import SettingsMap from '../../containers/Outcomes/SettingsMap'
import LeaveMap from '../../containers/Outcomes/LeaveMap'

import { IProps } from './types'
import styles from './index.cssmodule.scss'
import './anim.scss'

// const ANIMATION_DUARATION = 2000
const OUTCOMES_HOLDER = {
  addNew: AddMap,
  rename: RenameMap,
  trash: DeleteMap,
  share: ShareMap,
  leave: LeaveMap,
  join: null
}

class OutcomesHolder extends Component<IProps> {
  _classNameHolder = Outcome => {
    const outcomeClass = classnames({
      [styles.outcomeContainer]: true,
      [styles.noChildren]: Outcome === 'empty'
    })

    return outcomeClass
  }

  _getOutcomeLayout = Outcome => {
    const { appID, mobileLayout, outcomeType, mediaType, isDesktopLayoutSet, rowID, payload, uuid } = this.props
    const { isDesktop } = checkMediaType(mediaType)

    if (!isDesktopLayoutSet && !isDesktop && (outcomeType !== 'addNew' || mobileLayout)) {
      if (Outcome !== 'empty') {
        return (
          <SettingsMap appID={appID} uuid={uuid} rowID={rowID} outcomeType={outcomeType} payload={payload}>
            <Outcome rowID={rowID} uuid={uuid} payload={payload} />
          </SettingsMap>
        )
      }

      return <SettingsMap appID={appID} uuid={uuid} rowID={rowID} outcomeType={outcomeType} payload={payload} />
    }

    return <Outcome rowID={rowID} uuid={uuid} payload={payload} />
  }

  _renderOutcome = () => {
    const { outcomeType } = this.props

    const Outcome = OUTCOMES_HOLDER[outcomeType] || Empty
    const outcomeClass = this._classNameHolder(Outcome)

    return <div className={outcomeClass}>{this._getOutcomeLayout(Outcome)}</div>
  }

  render() {
    return (
      // <TransitionGroup component='div' className={styles.outcomeTransitionHolder}>
        // <CSSTransition classNames='currentRowOutcome' timeout={ANIMATION_DUARATION} unmountOnExit={true}>
          this._renderOutcome()
        // </CSSTransition>
      // </TransitionGroup>
    )
  }
}

export default OutcomesHolder
