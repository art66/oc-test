export interface IProps {
  type?: string
  inProgress?: boolean
  position?: string
  description?: string
  saveAction?: () => void
  cancelAction?: () => void
}
