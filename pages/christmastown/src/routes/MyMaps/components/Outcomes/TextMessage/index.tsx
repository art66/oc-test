import React, { PureComponent } from 'react'
import classnames from 'classnames'
import AnimationLoad from '@torn/shared/components/AnimationLoad'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const CANCEL_LABEL = 'Cancel'

class TextMessage extends PureComponent<IProps> {
  _handlerCancel = () => {
    const { cancelAction } = this.props

    cancelAction()
  }

  _handlerSave = () => {
    const { saveAction } = this.props

    saveAction()
  }

  _classNameHolder = () => {
    const { type, inProgress } = this.props

    const wrapper = classnames(styles[`${type}Map`], styles.textOutcomeWrapper)
    const cancelButton = classnames({
      [styles.cancel]: true,
      [styles.button]: true
    })

    const saveButton = classnames({
      [styles[type]]: true,
      [styles.button]: true,
      [styles[`${type}PreloadButton`]]: inProgress
    })

    return {
      wrapper,
      cancelButton,
      saveButton
    }
  }

  _getDescription = () => {
    const { type, position = '', description } = this.props
    const getDescription = type === 'delete' ? description : `${description} ${position} position?`

    return getDescription
  }

  _getCancelButton = () => {
    const { cancelButton } = this._classNameHolder()

    return (
      <button className={cancelButton} type='button' onClick={this._handlerCancel}>
        {CANCEL_LABEL}
      </button>
    )
  }

  _getSaveButton = () => {
    const { type, inProgress } = this.props
    const { saveButton } = this._classNameHolder()

    const typeLabel = type.substr(0, 1).toUpperCase() + type.substr(1, type.length)
    const preloader = (
      <AnimationLoad
        dotsCount={3}
        dotsColor='orange'
        animationDuaration='1'
        // isAdaptive={true}
      />
    )

    return (
      <button className={saveButton} type='button' onClick={this._handlerSave}>
        {inProgress ? preloader : typeLabel}
      </button>
    )
  }

  render() {
    const description = this._getDescription()
    const { wrapper } = this._classNameHolder()

    return (
      <div className={wrapper}>
        <div className={styles.leftSection}>
          <span className={styles.description}>{description}</span>
        </div>
        <div className={styles.rightSection}>
          {this._getSaveButton()}
          {this._getCancelButton()}
        </div>
      </div>
    )
  }
}

export default TextMessage
