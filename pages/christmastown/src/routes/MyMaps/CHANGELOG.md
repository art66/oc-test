# CT-Editor/#/MyMaps - Torn


# 2.2.1
 * Fixed broken row highlighting in desktop mode once settings button pressed.

# 2.2.0
 * Added active joined map row highlighting.

# 2.1.3
 * Fixed usersList appear in dropdown by ID key input. Updated reducers and utils correspondents.
 * Fixed normalizedTabLabel util.

# 2.1.2
 * Fixed people search process.

# 2.1.1
 * Fixed Players section layout in Invites tables, as well as its switcher appear.

# 2.0.4
 * Fixed header paddings.

# 2.0.3
 * Fixed types for mediaType prop.

# 2.0.2
 * Fixed tooltips text.

# 2.0.1
 * Fixed save name button position.
 * Fixed broken rename outcome layout in mobile mode.

# 2.0.0
 * Added full cycle of Websockets handlers for multi-tabs and P2P realtime play.

# 1.43.3
 * Fixed broken minigames bundles load

# 1.43.2
 * Refactored JoinSection component.

# 1.43.1
 * Fixed icons layout because of improved dimensions.

# 1.43.0
 * Added remove/changeRole/invite action & reducers.

# 1.42.0
 * Added inviteUsers reducers/actions functional.

# 1.41.0
 * Websockets were replaced from glob MapsManagement channel to the users particular ones.

# 1.40.0
 * Improved mapRowDataNormalizer util.

# 1.39.9
 * Fixed outcome overlay in mymaps tablet.
 * Fixed mobile stars gradient.
 * Fixed create new map button position in tablet/mobile modes.
 * Fixed tablet width for both mymaps and invites tablets.
 * Fixed <hr /> appear for SettingsMap map outcome without children.
 * Fixed Delete button unclicked fill.

# 1.39.8
 * Fixed row placeholder layout in tablet mode.
 * Fixed leading letter in the DropDown users name.

# 1.39.7
 * Fixed dropdown overlay.

# 1.39.6
 * Fixed inputs type.

# 1.39.5
 * Fixed SettingsMap typings.

# 1.39.4
 * Fixed tooltips appear on mobile mode.
 * Fixed outcome rename button action/reduce hiding.

# 1.39.3
 * Fixed joinLink receiving in mobile mode inside CopyLink Component.

# 1.39.2
 * Urgent fix for members role change.

# 1.39.1
 * Fixed underlay input layout (z-index).
 * Fixed Create New Map button appear.
 * Added filtering by userID for WS.
 * Temporary removed Animation from Outcomes inside Rows.

# 1.38.1
 * Fixed buttons loader animation.

# 1.38.0
 * Added warning tooltips for addMap and renameMap functionalities.
 * Minor refactor.

# 1.37.6
 * Fixed overloaded staff bind to the map.

# 1.37.5
 * Fixed jumped preloader animation in FF.

# 1.37.4
 * Fixed hr delimiter for FF browser.
 * Fixed switch animation "jumping" for FF browser.

# 1.37.3
 * Fixed skeletons in tablet mode.

# 1.37.2
 * Fixed unopened manage buttons and outcomes in some rare cases.

# 1.37.1
 * Added inputs length checking along with its restricted symbols input.
 * Added type checking on more than 20 symbols in the row.
 * Fixed skeleton layout on manualDesktop mode.
 * Fixed errors handling in safetyFetch util.

# 1.36.0
 * Added right SVG stars layout.

# 1.35.5
 * Added fetchUser functional (actions, sagas, reducers, components upgrades) for the staff publishing/updating.
 * Added animation preloader for SearchPerson Component.
 * Added new composition for an animation preloader inside scss.
 * Improved createFetchURL and getUsersList utils.
 * Component logic has been lifted from DropDown to SearchPerson component.

# 1.33.4
 * Fixed joinLink href anchor.

# 1.33.3
 * Fixed users fetch cancellation inside fetchSaga.

# 1.33.2
 * Improved RatingBox icons structure.

# 1.33.1
 * Added checking on empty string request for renameBox.
 * Fixed custom scroll bar styling for a whole window layout.
 * Added correct placeholder color and fixed its default value.

# 1.32.0
 * Added correct sidebar layout for MyMaps app.

# 1.31.4
 * Fixed ability to set/rename map name with spaces inside it.

# 1.31.3
 * Fixed broken star shadow appear in Safari.

# 1.31.2
 * Fixed broken manage buttons clicking after some of the is being clicked.

# 1.31.1
 * Fixed broken svgPlaceholder appear in CreateMap Component in Safari browser.

# 1.31.0
 * Added Join button for Invites Manage section.

# 1.30.0
 * Added myMaps Tooltips.

# 1.29.0
 * Added tables skeletons for preload stage.

# 1.28.0
 * Added toggle switcher animation on desktop mode.
 * Added autofocus for inputs while they're opened.
 * Fixed row hover repaint once toggle button clicked.

# 1.27.0
 * Fixed testers/editors/players count appear in case of empty map created.

# 1.26.0
 * Added adaptive layout due to manualMode ability to set it.

# 1.25.0
 * Removed timestamps on several stages.
 * Removed arfisual restrict on rows parsing (10 max before).
 * Fixed lastEdit appear for both time and data layouts.

# 1.24.0
 * Fixed inactive class for N/A appear.
 * Changed N/A appear for the all false values.
 * Fixed N/A layout in all text count columns.
 * Fixed players data appear in Invites table.
 * Fixed app crash in case of invites array receiving.

# 1.23.1
 * Added real users list fetch support, instead of mocks data using.
 * Several minor fixes in sagas and reducers.

# 1.22.0
 * Adeed timestamps for several WS responses.

# 1.21.2
 * Fixed broken DropDown appear on mobile mode.
 * Fixed broken RenameBox width on tablet mode in case of active row onHover.
 * Renamed MemberContainer in favor of better convenient naming.

# 1.21.1
 * Adedd real maxTesters and maxEditors value inside model logic, instead of hardcoded one.
 * Added DropDown list checking on is user already added inside membersList or not.
 * Fixed testers/editors calculation process inside reducers.
 * Written several reusable utils for better editors/testers calculation handling.
 * Added checking on unchanged membersList value to prevent unnecessary fetch requests.

# 1.20.1
 * Improved safetyFetch sagas util for better callback handling.
 * Fixed getState generator in updateStaff generator.

# 1.19.0
 * Added loadMainData functional (actions, sagas, reducers, components upgrades) for the staff publishing/updating.

## 1.18.1
 * Fixed the same name saving process.

## 1.18.0
 * Improved abstraction level of sagas function by moving their core to the centralized place.
 * Websocket folder was renamed on websocket.
 * Created several util helpers.

## 1.17.1
 * Added fetchStaff functional (actions, sagas, reducers, components upgrades) for the staff publishing/updating.
 * Added placeholders for DropDown list in case of missed data.
 * Fixed broken createMap warning appear on mobile mode.

## 1.16.1
 * Fixed broken Inventions/MyMaps rows firing in case if the same row in toward table wsa clicked or etc.
 * Fixed non-disappearing effects on the row once it canceled.
 * Fixed broken name changing once clicked some another row in case of already in progress someone else.
 * Fixed memory leak in DropDownList Component.
 * Created global func for Intentions/MyMaps tables checking.

## 1.15.0
 * Added beautiful placeholder in case of missing data.

## 1.14.1
 * Fixed switcher work in mobile mode.
 * Fixed row inactivation after Share Outcome progress is finished.

## 1.14.0
 * Added partially-made players update WS, along with actions and reducers.

## 1.13.3
 * Fixed Month/Day generation.
 * Fixed Testers/Editors count appear in the table.

## 1.13.2
 * Added updateStaff functional (actions, sagas, reducers, components upgrades) for the staff publishing/updating.

## 1.12.2
 * Fixed add persons on map functional.

## 1.12.1
 * Added leaveMap functional (actions, sagas, reducers, components' upgrades) for the map publishing.

## 1.12.1
 * Fixed broken Share Outcome layout in tablet mode.
 * Fixed broken loop icon inside Share Outcome input.
 * Fixed incorrect positioning of stars inside Outcome's layout in mobile mode.
 * Fixed broken change name containers width and positioning.
 * Added correct name change closing by ESC button.
 * Added CopyByClick functional to be able the user copy inviteLink just by click inside Share Outcome.

## 1.11.1
 * Refactored publishMap logic in favor of better stability.

## 1.11.0
 * Added publishMap functional (actions, sagas, reducers, components' upgrades) for the map publishing.

## 1.11.0
 * Added renameMap functional (actions, sagas, reducers, components' upgrades) for the map renaming.

## 1.10.0
 * Added buttons loading stage for Delete and Leave Outcomes.

## 1.9.0
 * Added new fallback to help whole app work correctly even on request crash.

## 1.8.1
 * Fixed (removed) link bind inside all buttons of Manage section.
 * Fixed missing classes inside Manage sections.

## 1.8.0
 * Added uuid accessibility inside Outcomes section.
 * Added deleteMap functional (actions, sagas, reducers, components' upgrades) for the map deletion.

## 1.7.2
 * Added warning message for Outcome creation.

## 1.7.1
 * Added tiny transition for create new map Outcome.
 * Minor fixes.

## 1.7.0
 * Added new helper util getMonthDay for data parsing.

## 1.6.0
 * Improved typification of btw actions and reducers in MyMaps.
 * Added new reducers and actions for async handlers.

## 1.5.0
 * Added new button's logic for async requests in Outcome sections.

## 1.4.0
 * Added debug dialog windows in MyMaps.

## 1.3.0
 * Added first stable WS in MyMaps.

## 1.2.0
 * Added first stable saga in MyMaps.

## 1.1.0
 * Imp.

## 1.0.0
 * Added webpack chunk's name.
 * Row's processing logic were switched from arrays to objects way of work.
 * Rewritten SWITCHER_TOGGLE, CHANGE_MAP_NAME_SAVED and NEW_MAP_SAVED reducers logic.
 * Fully rewritten initialsState mock store.
 * Row's titles were moved from redux store to particular constants.
 * Improved reducer's timestamp generation.
 * Improved Row's processing logic (from procedure to OOP fashion style).
 * Improved Switcher processor logic.
 * Improved Body and Row Components types.
 * Improved Section, ReleaseSection and Switcher Components types.
 * Fixed Switcher typing.
