import { connect } from 'react-redux'
import MyMaps from '../../components/MyMaps'

const mapStateToProps = ({ myMapsApp }) => ({
  appID: myMapsApp.mymaps.appID,
  title: myMapsApp.mymaps.title,
  columnTitles: myMapsApp.mymaps.titles,
  rowsData: myMapsApp.mymaps.rows,
  activeRowID: myMapsApp.mymaps.activeRowID,
  manageType: myMapsApp.mymaps.manageMap.manageType,
  manageRowID: myMapsApp.mymaps.manageMap.manageRowID,
  myMapsMobileOutcomeLayout: myMapsApp.mymaps.manageMap.mobileLayout,
  manageMapInProgress: myMapsApp.mymaps.manageMap.manageMapInProgress
})

export default connect(
  mapStateToProps,
  null
)(MyMaps)
