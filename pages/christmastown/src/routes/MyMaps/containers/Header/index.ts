import { connect } from 'react-redux'
import { Header } from '../../components'

const mapStateToProps = ({ browser, myMapsApp, common }) => ({
  newMapInProgress: myMapsApp.mymaps.createMap.newMapInProgress,
  outcomeType: myMapsApp.mymaps.createMap.type,
  isDesktopLayoutSet: common.isDesktopLayoutSet,
  mediaType: browser.mediaType
})

export default connect(
  mapStateToProps,
  null
)(Header)
