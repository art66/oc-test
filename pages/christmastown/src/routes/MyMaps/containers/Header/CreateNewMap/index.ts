import { connect } from 'react-redux'
import {
  createNewMapStart as createNewMapAction,
  newMapOnHover as newMapOnHoverAction,
  newMapOnBlur as newMapOnBLurAction
} from '../../../modules/actions'
import CreateNewMap from '../../../components/Header/CreateNewMap'

const mapStateToProps = ({ browser, myMapsApp }) => ({
  mediaType: browser.mediaType,
  inProgressMainLoad: myMapsApp.general.loadMainData.isRequested,
  newMapInProgress: myMapsApp.mymaps.createMap.newMapInProgress,
  newMapHovered: myMapsApp.mymaps.createMap.newMapHovered
})

const mapDispatchToStore = dispatch => ({
  createNewMapAction: (newMapHovered, newMapCreateInProgress) => dispatch(
    createNewMapAction(newMapHovered, newMapCreateInProgress)
  ),
  newMapOnHoverAction: onHover => dispatch(newMapOnHoverAction(onHover)),
  newMapOnBlurAction: onBlur => dispatch(newMapOnBLurAction(onBlur))
})

export default connect(
  mapStateToProps,
  mapDispatchToStore
)(CreateNewMap)
