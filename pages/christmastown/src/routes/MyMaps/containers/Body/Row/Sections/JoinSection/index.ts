import { connect } from 'react-redux'
import { manageMap as manageMapAction } from '../../../../../modules/actions'
import JoinSection from '../../../../../components/Body/Row/Sections/JoinSection'

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType,
  isDesktopLayoutSet: state.common.isDesktopLayoutSet,
  myMapsMobileOutcomeLayout: state.myMapsApp.mymaps.manageMap.mobileLayout
})

const mapDispatchToProps = dispatch => ({
  manageMapAction: payload => dispatch(manageMapAction(payload))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JoinSection)
