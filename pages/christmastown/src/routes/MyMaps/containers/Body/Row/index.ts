import Row from '../../../components/Body/Row'
import { connect } from 'react-redux'

// export default Row
const mapStateToProps = state => ({
  activeJoinedMapID: state.myMapsApp.general.activeJoinedMapID,
  mediaType: state.browser.mediaType,
  isDesktopLayoutSet: state.common.isDesktopLayoutSet
})

export default connect(
  mapStateToProps,
  null
)(Row)
