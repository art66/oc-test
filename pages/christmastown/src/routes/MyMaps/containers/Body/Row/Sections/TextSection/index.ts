import { connect } from 'react-redux'
import TextSection from '../../../../../components/Body/Row/Sections/TextSection'

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType,
  isDesktopLayoutSet: state.common.isDesktopLayoutSet,
  changeMapNameID: state.myMapsApp.mymaps.changeName.changeMapNameID
})

export default connect(
  mapStateToProps,
  null
)(TextSection)
