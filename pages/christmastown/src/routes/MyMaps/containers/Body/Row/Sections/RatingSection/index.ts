import { connect } from 'react-redux'
import RatingSection from '../../../../../components/Body/Row/Sections/RatingSection'

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType,
  isDesktopLayoutSet: state.common.isDesktopLayoutSet
})

export default connect(
  mapStateToProps,
  null
)(RatingSection)
