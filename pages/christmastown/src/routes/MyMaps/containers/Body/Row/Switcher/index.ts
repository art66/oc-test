import { connect } from 'react-redux'
import { switcherToggleAttempt as switcherToggleAction } from '../../../../modules/actions'
import Switcher from '../../../../components/Body/Row/Switcher'

const mapStateToProps = state => ({
  userBanned: state.myMapsApp.general.isBanned,
  inProgress: state.myMapsApp.mymaps.publishMap.isRequested,
  publishMapID: state.myMapsApp.mymaps.publishMap.publishMapID
})

const mapDispatchToProps = dispatch => ({
  switcherToggle: ({ mapID: uuid, inProgress: releaseRequest, publishStatus, extraData }) =>
    dispatch(switcherToggleAction(uuid, extraData.rowID, releaseRequest, publishStatus))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Switcher)
