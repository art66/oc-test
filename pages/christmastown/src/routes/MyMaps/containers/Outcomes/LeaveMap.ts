import { connect } from 'react-redux'
import LeaveMap from '../../components/Outcomes/LeaveMap'
import { leaveMapAttempt as leaveMapAction, leaveMapCancel as leaveMapCancelAction } from '../../modules/actions'

const mapStateToProps = state => ({
  inProgress: state.myMapsApp.invites.manageMap.isRequested
})

const mapDispatchToProps = dispatch => ({
  leaveMapAction: (isRequested, uuid) => dispatch(leaveMapAction(isRequested, uuid)),
  leaveMapCancelAction: (leaveMapInProgress, isRequested, rowID) => dispatch(
    leaveMapCancelAction(leaveMapInProgress, isRequested, rowID)
  )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeaveMap)
