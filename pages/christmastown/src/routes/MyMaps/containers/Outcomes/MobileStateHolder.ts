import { connect } from 'react-redux'
import MobileStatsHolder from '../../components/Outcomes/MobileStatsHolder'

const mapStateToProps = state => ({
  activeJoinedMapID: state.myMapsApp.general.activeJoinedMapID
})

export default connect(
  mapStateToProps,
  null
)(MobileStatsHolder)
