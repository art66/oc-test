import { connect } from 'react-redux'
import ActionTabs from '../../../MyMaps/components/Outcomes/ActionTabs'

const mapStateToProps = state => ({
  manageType: state.myMapsApp.mymaps.manageMap.manageType
})

export default connect(
  mapStateToProps,
  null
)(ActionTabs)
