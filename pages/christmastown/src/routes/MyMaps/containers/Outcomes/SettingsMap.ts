import { connect } from 'react-redux'
import SettingsMap from '../../components/Outcomes/SettingsMap'

// export default SettingsMap
import {
  manageMap as manageMapAction,
  changeMapNameStart as changeMapNameAction,
  changeMapNameAttempt as changeMapNameSaveAction
  // shareMap as shareMapAction,
  // shareMapCancel as shareMapCancelAction,
  // deleteMap as deleteMapAction,
  // deleteMapCancel as deleteMapCancelAction
} from '../../modules/actions'

const mapDispatchToProps = dispatch => ({
  manageMapAction: payload => dispatch(manageMapAction(payload)),
  changeMapNameAction: (changeMapNameInProgress, ID) => dispatch(changeMapNameAction(changeMapNameInProgress, ID)),
  changeMapNameSaveAction: (isRequested, ID, newName) =>
    dispatch(changeMapNameSaveAction(isRequested, ID, newName))
  // deleteMapAction: (manageMapInProgress, rowID) => dispatch(deleteMapAction(manageMapInProgress, rowID)),
  // deleteMapCancelAction: (manageMapInProgress, rowID) => dispatch(deleteMapCancelAction(manageMapInProgress, rowID)),
  // shareMapAction: (leaveMapInProgress, rowID) => dispatch(shareMapAction(leaveMapInProgress, rowID)),
  // shareMapCancelAction: (leaveMapInProgress, rowID) => dispatch(shareMapCancelAction(leaveMapInProgress, rowID))
})

export default connect(
  null,
  mapDispatchToProps
)(SettingsMap)
