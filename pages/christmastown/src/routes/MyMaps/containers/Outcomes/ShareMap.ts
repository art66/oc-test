import { connect } from 'react-redux'
import ShareMap from '../../components/Outcomes/ShareMap'
import {
  addMember as addMemberAction,
  addMemberCancel as addMemberCancelAction,
  memberAdded as addMemberSavedAction,
  deleteMember as deleteMemberAction,
  shareMapAttempt as shareMapAction,
  shareMapCancel as shareMapCancelAction,
  changeMemberRole as changeMemberRoleAction,
  staffTabClickAttempt as staffTabClickAttemptAction,
  saveNewUserAttempt as saveNewUserAttemptAction
} from '../../modules/actions'

const mapStateToProps = state => ({
  maxEditors: state.myMapsApp.mymaps.controlData.maxEditors,
  maxTesters: state.myMapsApp.mymaps.controlData.maxTesters,
  inProgressFetch: state.myMapsApp.mymaps.addMember.isRequested,
  inProgressSave: state.myMapsApp.mymaps.manageMap.isRequested,
  inviteUsersList: state.myMapsApp.mymaps.inviteUsersList,
  inProgressSearch: state.myMapsApp.mymaps.searchUser.isRequested,
  addMemberInProgress: state.myMapsApp.mymaps.addMember.addMemberInProgress,
  addMemberRowID: state.myMapsApp.mymaps.addMember.addMemberRowID,
  currentTab: state.myMapsApp.mymaps.addMember.currentTab
})

const mapDispatchToProps = dispatch => ({
  addMemberAction: (addMemberInProgress, rowID, isRequested) => dispatch(
    addMemberAction(addMemberInProgress, rowID, isRequested)
  ),
  addMemberCancelAction: (addMemberInProgress, rowID, isRequested) => dispatch(
    addMemberCancelAction(addMemberInProgress, rowID, isRequested)
  ),
  addMemberSavedAction: (addMemberInProgress, rowID, memberData) =>
    dispatch(addMemberSavedAction(addMemberInProgress, rowID, memberData)),
  deleteMemberAction: (memberID, memberType, rowID) => dispatch(deleteMemberAction(memberID, memberType, rowID)),
  shareMapAction: (isRequested, rowID) => dispatch(shareMapAction(isRequested, rowID)),
  shareMapCancelAction: (leaveMapInProgress, rowID) => dispatch(shareMapCancelAction(leaveMapInProgress, rowID)),
  changeMemberRoleAction: (member, rowID) => dispatch(changeMemberRoleAction(member, rowID)),
  chooseStaffTabAction: (currentTab, isRequested) => dispatch(staffTabClickAttemptAction(currentTab, isRequested)),
  saveNewUserAttemptAction: (searchText, isRequested) => dispatch(
    saveNewUserAttemptAction(searchText, isRequested)
    )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShareMap)
