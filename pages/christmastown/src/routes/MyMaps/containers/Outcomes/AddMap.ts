import { connect } from 'react-redux'
import AddMap from '../../components/Outcomes/AddMap'
import { createNewMapAttempt as saveNewMapAction, cancelNewMap as cancelNewMapAction } from '../../modules/actions'

const mapStateToProps = ({ myMapsApp: state }) => ({
  inProgress: state.mymaps.createMap.isRequested
})

const mapDispatchToProps = dispatch => ({
  saveNewMapAction: (isRequested, newMapName) => dispatch(saveNewMapAction(isRequested, newMapName)),
  cancelNewMapAction: (isRequested, newMapInProgress) => dispatch(cancelNewMapAction(isRequested, newMapInProgress))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddMap)
