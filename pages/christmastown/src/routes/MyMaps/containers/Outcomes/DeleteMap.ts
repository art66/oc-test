import { connect } from 'react-redux'
import DeleteMap from '../../components/Outcomes/DeleteMap'
import { deleteMapAttempt as deleteMapAction, deleteMapCancel as deleteMapCancelAction } from '../../modules/actions'

const mapStateToProps = state => ({
  inProgress: state.myMapsApp.mymaps.manageMap.isRequested
})

const mapDispatchToProps = dispatch => ({
  deleteMapAction: (isRequested, rowID, mapID) => dispatch(deleteMapAction(isRequested, rowID, mapID)),
  deleteMapCancelAction: (manageMapInProgress, isRequested, rowID) => dispatch(
    deleteMapCancelAction(manageMapInProgress, isRequested, rowID)
    )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteMap)
