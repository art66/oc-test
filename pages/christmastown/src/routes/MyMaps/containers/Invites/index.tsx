import { connect } from 'react-redux'
import Invites from '../../components/Invites'

const mapStateToProps = ({ myMapsApp }) => ({
  appID: myMapsApp.invites.appID,
  title: myMapsApp.invites.title,
  columnTitles: myMapsApp.invites.titles,
  rowsData: myMapsApp.invites.rows,
  activeRowID: myMapsApp.invites.activeRowID,
  manageType: myMapsApp.invites.manageMap.manageType,
  manageRowID: myMapsApp.invites.manageMap.manageRowID,
  manageMapInProgress: myMapsApp.invites.manageMap.manageMapInProgress
})

export default connect(
  mapStateToProps,
  null
)(Invites)
