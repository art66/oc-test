import { connect } from 'react-redux'
import {
  changeMapNameStart as changeMapNameAction,
  changeMapNameCancel as changeMapNameCancelAction,
  changeMapNameAttempt as changeMapNameSaveAction
} from '../../modules/actions'
import RenameBox from '../../components/RenameBox'

const mapStateToProps = state => ({
  mediaType: state.browser.mediaType,
  isDesktopLayoutSet: state.common.isDesktopLayoutSet,
  changeMapNameID: state.myMapsApp.mymaps.changeName.changeMapNameID,
  changeMapNameInProgress: state.myMapsApp.mymaps.changeName.changeMapNameInProgress,
  isRequested: state.myMapsApp.mymaps.changeName.isRequested
})

const mapDispatchToProps = dispatch => ({
  changeMapNameAction: (changeMapNameInProgress, ID) => dispatch(changeMapNameAction(changeMapNameInProgress, ID)),
  changeMapNameCancelAction: (changeMapNameInProgress, ID) =>
    dispatch(changeMapNameCancelAction(changeMapNameInProgress, ID)),
  changeMapNameSaveAction: (isRequested, ID, newMapName) =>
    dispatch(changeMapNameSaveAction(isRequested, ID, newMapName))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RenameBox)
