import React from 'react'
import classnames from 'classnames'
import AnimationLoad from '@torn/shared/components/AnimationLoad'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import { IProps } from './types'

import styles from './index.cssmodule.scss'

const NO_TITLE = ''
const BETA_STAGE = {
  isReleased: false,
  color: 'blue',
  status: 'beta',
  label: 'Off',
  title: 'Beta only'
}
const RELEASE_STAGE = {
  isReleased: true,
  color: 'green',
  status: 'released',
  label: 'On',
  title: 'Released'
}
const BANNED_STAGE = {
  isReleased: true,
  color: 'red',
  status: 'banned',
  label: 'Ban',
  title: 'Banned'
}

class Switcher extends React.Component<IProps> {
  componentDidMount() {
    this._initializeTooltip()
  }

  _getToggleID = () => {
    const { mapID } = this.props

    return `${mapID}-userBanned`
  }

  _initializeTooltip = () => {
    const {
      data: { dimmToggle }
    } = this.props

    if (dimmToggle) {
      tooltipsSubscriber.subscribe({
        child: `You can't publish your map while banned`,
        ID: this._getToggleID()
      })
    }
  }

  _handleClick = () => {
    const { isDisabledCheckbox, switcherToggle, inProgress, data, authorID, mapID, extraData } = this.props

    if (inProgress || isDisabledCheckbox || data.dimmToggle) {
      return
    }

    switcherToggle({
      mapID,
      authorID,
      publishStatus: !data.published,
      isBanned: !data.isBanned,
      inProgress: true,
      extraData
    })
  }

  _getSwitcherData = () => {
    const {
      data: { published, isBanned }
    } = this.props

    if (isBanned) {
      return BANNED_STAGE
    }

    return published ? RELEASE_STAGE : BETA_STAGE
  }

  _classNamesHolder = () => {
    const {
      isDisabledCheckbox,
      narrow,
      data: { isBanned, dimmToggle }
    } = this.props

    const { isReleased, status } = this._getSwitcherData()
    const switcherContainer = classnames(styles.switcherContainer, styles[status])

    const checkbox = classnames({
      [styles.checkbox]: true,
      [styles.narrow]: narrow,
      [styles.checked]: !dimmToggle && (isBanned || isReleased),
      [styles.checked__banned]: isBanned,
      [styles.dimmToggle]: dimmToggle,
      [styles.disableCheckbox]: isDisabledCheckbox
    })
    const labelClass = classnames({
      [styles.leftSection]: true,
      [styles[`${status}Label`]]: true,
      [styles.dimmLabel]: dimmToggle
      // [styles.inProgressLabel]: this._isSwitchInProgress()
    })

    return {
      switcherContainer,
      labelClass,
      checkbox
    }
  }

  _isSwitchInProgress = () => {
    const { inProgress, isDisabledCheckbox } = this.props

    return inProgress && !isDisabledCheckbox
  }

  _renderLabelSection = () => {
    const { title, color } = this._getSwitcherData()
    const { labelClass } = this._classNamesHolder()

    if (this._isSwitchInProgress()) {
      return (
        <div className={labelClass}>
          <AnimationLoad dotsCount={3} dotsColor={color} animationDuaration='1' />
        </div>
      )
    }

    return (
      <div className={labelClass}>
        <span className={styles.title}>{title || NO_TITLE}</span>
      </div>
    )
  }

  _renderToggleSection = () => {
    const { data } = this.props

    const { checkbox } = this._classNamesHolder()
    const { label } = this._getSwitcherData()

    return (
      <div className={styles.rightSection}>
        <div id={(data.dimmToggle && this._getToggleID()) || ''} className={checkbox} onClick={this._handleClick}>
          <span className={`${styles.checkboxTitle} ${styles.checkboxTitleFont}`}>{label}</span>
        </div>
      </div>
    )
  }

  render() {
    const { switcherContainer } = this._classNamesHolder()

    return (
      <div className={switcherContainer}>
        {this._renderLabelSection()}
        {this._renderToggleSection()}
      </div>
    )
  }
}

export default Switcher
