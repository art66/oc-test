export interface ISwitcherData {
  published: boolean
  dimmToggle?: boolean
  isBanned?: boolean
  className?: string
  type?: string
}

export interface ISwitcher {
  mapID: string
  authorID?: number
  publishStatus?: boolean
  isBanned?: boolean
  inProgress?: boolean
  extraData?: object
}

export interface IProps {
  isDisabledCheckbox?: boolean
  authorID?: number
  mapID?: string
  inProgress?: boolean
  narrow?: boolean
  data?: ISwitcherData
  extraData?: object
  switcherToggle?: ({ mapID, authorID, publishStatus, isBanned, inProgress, extraData }: ISwitcher) => void
}
