// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'banned': string;
  'bannedLabel': string;
  'beta': string;
  'betaLabel': string;
  'checkbox': string;
  'checkboxTitle': string;
  'checkboxTitleFont': string;
  'checked': string;
  'checked__banned': string;
  'dimmLabel': string;
  'dimmToggle': string;
  'disableCheckbox': string;
  'globalSvgShadow': string;
  'leftSection': string;
  'narrow': string;
  'released': string;
  'releasedLabel': string;
  'rightSection': string;
  'switcherContainer': string;
  'title': string;
}
export const cssExports: CssExports;
export default cssExports;
