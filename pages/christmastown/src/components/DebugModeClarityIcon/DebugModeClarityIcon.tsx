import React from 'react'
import s from './debugModeClarityIcon.cssmodule.scss'

/* eslint-disable max-len */
export const DebugModeClarityIcon = () => {
  return (
    <div className={s.debugModeIcon}>
      <svg
        xmlns='http://www.w3.org/2000/svg'
        xmlnsXlink='http://www.w3.org/1999/xlink'
        width='36'
        height='36'
        viewBox='0 0 36 36'
      >
        <defs>
          <linearGradient
            id='linear-gradient'
            x1='-1378.87'
            y1='267.71'
            x2='-1378.87'
            y2='267.74'
            gradientTransform='matrix(30, 0, 0, -30, 41384.01, 12263.48)'
            gradientUnits='userSpaceOnUse'
          >
            <stop offset='0' stopColor='#ff7a4d' />
            <stop offset='1' stopColor='#d93600' />
          </linearGradient>
        </defs>
        <g id='Layer_2' data-name='Layer 2'>
          <g id='Layer_1-2' data-name='Layer 1'>
            <g id='Path_209' data-name='Path 209' opacity='0.5' style={{ isolation: 'isolate' }}>
              <path
                d='M32,34.5a1.48,1.48,0,0,1-1-.36L18,23,5,34.14a1.48,1.48,0,0,1-1,.36,1.51,1.51,0,0,1-1.06-.44l-1-1a1.5,1.5,0,0,1-.08-2L13.08,18,1.86,5a1.5,1.5,0,0,1,.08-2l1-1A1.51,1.51,0,0,1,4,1.5a1.48,1.48,0,0,1,1,.36L18,13,31,1.86a1.48,1.48,0,0,1,1-.36,1.51,1.51,0,0,1,1.06.44l1,1a1.5,1.5,0,0,1,.08,2L22.92,18,34.14,31a1.5,1.5,0,0,1-.08,2l-1,1A1.51,1.51,0,0,1,32,34.5Z'
                fill='url(#linear-gradient)'
              />
              <path
                d='M32,3l1,1L20.94,18,33,32l-1,1L18,21.06,4,33,3,32,15.06,18,3,4,4,3,18,14.94,32,3m0-3a3,3,0,0,0-1.95.72L18,11,6,.72A3,3,0,0,0,1.88.88l-1,1A3,3,0,0,0,.73,6L11.1,18,.73,30a3,3,0,0,0,.15,4.08l1,1A3,3,0,0,0,6,35.28L18,25,30.05,35.28a3,3,0,0,0,4.07-.16l1-1A3,3,0,0,0,35.27,30L24.9,18,35.27,6a3,3,0,0,0-.15-4.08l-1-1A3,3,0,0,0,32,0Z'
                fill='#fff'
              />
            </g>
          </g>
        </g>
      </svg>
    </div>
  )
}

export default DebugModeClarityIcon
