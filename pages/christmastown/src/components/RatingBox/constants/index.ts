// ----------------------------------
// SVG STAR CONSTANTS
// ----------------------------------
export const MOBILE_STAR_DIMENSIONS = {
  width: '15.001',
  height: '14.001',
  viewbox: '0 0 15.001 14.001'
}

export const STARS_FILL_COLORS = {
  regular: '#fff',
  activeJoinRow: '#e3f5fa',
  hovered: '#f1fcff'
}

export const STARS_FILL_COLORS_DARK_MODE = {
  regular: '#333',
  activeJoinRow: '#344b5e',
  hovered: '#404F5B'
}
