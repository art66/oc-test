import React from 'react'
import classnames from 'classnames'
import checkMediaType from '@torn/shared/utils/checkMediaType'
import { TWithThemeInjectedProps, withTheme } from '@torn/shared/hoc/withTheme/withTheme'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { MOBILE_STAR_DIMENSIONS, STARS_FILL_COLORS, STARS_FILL_COLORS_DARK_MODE } from './constants'
import { DARK_THEME } from '../../constants'
import { IOwnProps, IProps } from './interfaces'
import styles from './index.cssmodule.scss'

class RatingSection extends React.Component<IOwnProps & IProps & TWithThemeInjectedProps> {
  _ratingAdapter = () => {
    const { data } = this.props
    let currentStars = 0

    if (data <= 20) {
      currentStars = 1
    } else if (data <= 40) {
      currentStars = 2
    } else if (data <= 60) {
      currentStars = 3
    } else if (data <= 80) {
      currentStars = 4
    } else if (data <= 100) {
      currentStars = 5
    }

    return currentStars
  }

  _getCurrentFill = () => {
    const { isRowHovered, isRowActive, isMapJoinedActive, theme } = this.props
    const starColors = theme === DARK_THEME ? STARS_FILL_COLORS_DARK_MODE : STARS_FILL_COLORS

    let iconFill = starColors.regular

    if (isMapJoinedActive) {
      iconFill = starColors.activeJoinRow
    } else if (isRowHovered || isRowActive) {
      iconFill = starColors.hovered
    }

    return iconFill
  }

  _classNameHolder = () => {
    const { isRowHovered, disableMobileGradient, isAdaptive, isMapJoinedActive } = this.props

    const mobileStarClass = classnames({
      [styles.svgStarAdaptive]: true,
      [styles.svgStarHovered]: isRowHovered
    })

    const desktopStarClass = classnames({
      [styles.svgStar]: true,
      [styles.svgWithMobileGradientRegular]: !disableMobileGradient,
      [styles.svgWithMobileGradientJoined]: !disableMobileGradient && isMapJoinedActive
    })

    const desktopUnderlay = classnames({
      [styles.starsProgress]: true,
      [styles.starsProgressAdaptive]: isAdaptive
    })

    return {
      mobileStarClass,
      desktopStarClass,
      desktopUnderlay
    }
  }

  _renderMobileLayout = () => {
    const { data } = this.props
    const iconFill = this._getCurrentFill()
    const ratingValue = data === 0 ? data : data.toFixed(1)

    return (
      <div className={styles.svgMobileStarWrap}>
        <div className={styles.starContainer}>
          <SVGIconGenerator
            iconName='Star'
            type='gold'
            fill={{ color: iconFill, strokeWidth: 0 }}
            dimensions={MOBILE_STAR_DIMENSIONS}
            customClass={styles.mobileStar}
          />
        </div>
        <span className={styles.mobileStarsCount}>{ratingValue}</span>
      </div>
    )
  }

  _renderSVGStarBackground = () => {
    return <div className={styles.svgStarBackground} />
  }

  _renderBackgroundsUnderlay = () => {
    const { data = 0 } = this.props

    const starsFillOverlay = (data * 100) / 5
    const { desktopUnderlay } = this._classNameHolder()

    return (
      <div className={styles.underlayWrap}>
        <div className={`${desktopUnderlay} ${styles.starsProgressBarBCG}`} />
        <div className={`${desktopUnderlay} ${styles.starsProgressBar}`} style={{ width: `${starsFillOverlay}px` }} />
      </div>
    )
  }

  _renderDesktopStars = () => {
    const { desktopStarClass } = this._classNameHolder()
    const fillColor = this._getCurrentFill()

    return (
      <div className={styles.starsWrap}>
        <svg
          className={desktopStarClass}
          xmlns='http://www.w3.org/2000/svg'
          fill={fillColor}
          width='115'
          height='33'
          viewBox='0 0 115 33'
        >
          <g transform='translate(-18 -61)'>
            <path
              transform='translate(-321 -604)'
              d='M356.5,674l2.293,4.642,5.207.706-3.79,3.574.925,5.078-4.635-2.434L351.864,688l.926-5.078L349,
              679.348l5.207-.706Zm80,0,2.293,4.642,5.207.706-3.79,3.574.925,5.078-4.635-2.434L431.864,
              688l.926-5.078L429,679.348l5.207-.706Zm-20,0,2.293,4.642,5.207.706-3.79,3.574.925,
              5.078-4.635-2.434L411.864,688l.926-5.078L409,679.348l5.207-.706Zm-40,0,2.293,4.642,5.207.706-3.79,
              3.574.925,5.078-4.635-2.434L371.864,688l.926-5.078L369,679.348l5.207-.706Zm20,0,2.293,
              4.642,5.207.706-3.79,3.574.925,5.078-4.635-2.434L391.864,688l.926-5.078L389,679.348l5.207-.706ZM339,
              665v33H454V665Z'
            />
            <path
              transform='translate(3981 2130)'
              fill='rgba(0,0,0,0.2)'
              d='M-3860.867-2046l-.2-.105-.723-3.972.156-.147.769,4.223v0Zm-9.268,0,.77-4.223.156.147-.724,
              3.972-.2.105h0Zm-10.731,0-.2-.105-.723-3.972.156-.147.769,4.223v0Zm-9.269,0,.77-4.223.156.147-.724,
              3.972-.2.105h0Zm-10.731,0-.2-.105-.724-3.972.156-.147.769,4.223v0Zm-9.269,0,.77-4.223.156.147-.724,
              3.972-.2.105h0Zm-10.731,0-.2-.105-.724-3.972.156-.147.77,4.223v0Zm-9.269,0,.771-4.223.156.147-.725,
              3.972-.2.105h0Zm-10.731,0-.2-.105-.724-3.972.156-.147.77,4.223v0Zm-9.268,0,.77-4.223.156.147-.725,
              3.972-.2.105h0Zm91.207-7.776-4.279-.581L-3865.5-2059l-2.293,4.643-4.281.581-.927-.874,5.207-.707,
              2.293-4.643,2.294,4.643,5.207.707-.927.874Zm-20,0-4.279-.581L-3885.5-2059l-2.294,
              4.643-4.28.581-.928-.874,5.208-.707,2.294-4.643,2.292,
              4.643,5.207.707-.927.874Zm-24.279-.581L-3905.5-2059l-2.294,4.643-4.28.581-.927-.874,5.207-.707,
              2.294-4.643,2.293,4.643,5.206.707-.927.874Zm-20,0L-3925.5-2059l-2.294,4.643-4.28.581-.927-.874,
              5.207-.707,2.294-4.643,2.293,4.643,5.207.707-.927.874Zm-15.72.581-4.279-.581L-3945.5-2059l-2.292,
              4.643-4.281.581-.928-.874,5.208-.707,2.292-4.643,2.294,4.643,5.207.707-.927.874Z'
            />
          </g>
        </svg>
      </div>
    )
  }

  _renderDesktopLayout = () => {
    return (
      <div className={styles.ratingBox}>
        {this._renderBackgroundsUnderlay()}
        {this._renderDesktopStars()}
      </div>
    )
  }

  render() {
    const { mediaType, isAdaptive, isDesktopLayoutSet, isMobileModeOnTablet } = this.props
    const { isMobile } = checkMediaType(mediaType)

    return !isDesktopLayoutSet && isAdaptive && (isMobile || isMobileModeOnTablet) ?
      this._renderMobileLayout() :
      this._renderDesktopLayout()
  }
}

export default withTheme(RatingSection)
