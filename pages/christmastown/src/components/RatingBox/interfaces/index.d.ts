import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IOwnProps {
  mediaType?: TMediaType
  disableMobileGradient?: boolean
  isAdaptive?: boolean
  isDesktopLayoutSet?: boolean
}

export interface IProps {
  isMapJoinedActive?: boolean
  isRowHovered?: boolean
  isRowActive?: boolean | number
  data?: number
  dimensions?: object
  filter?: object
  isMobileModeOnTablet?: boolean
}
