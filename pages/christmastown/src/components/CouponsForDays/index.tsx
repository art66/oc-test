import React from 'react'
import s from './styles.cssmodule.scss'

interface IProps {
  currentCoupons: number
  maxCoupons: number
}

const CouponsForDay = (props: IProps) => {
  const getCouponImage = () => props.currentCoupons ? Math.min(props.currentCoupons, 3) : 1

  return (
    <div className={s.couponsForDay}>
      <img
        src={`/images/items/christmas_town/coupon/coupon-${getCouponImage()}.png`}
        alt='Prize Coupon'
      />
      {props.currentCoupons} / {props.maxCoupons} received today
    </div>
  )
}

export default CouponsForDay
