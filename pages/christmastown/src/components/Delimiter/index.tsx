import React from 'react'

export default function Delimiter() {
  return (
    <hr className='page-head-delimiter m-top10 m-bottom10' />
  )
}
