import React from 'react'
import ReactDOM from 'react-dom'
import manageAppWrapLayout, { checkIsPageWithoutSidebar } from '@torn/shared/utils/manageAppWrapLayout'
import SidebarGlobal from '@torn/sidebar/src/containers/SidebarAsync'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/helpers/rootReducer'

import { PATHS_WITHOUT_SIDEBAR, PATHS_WITHOUT_SIDEBAR_TRAVEL_MODE } from '../../constants'
import { IWindow } from '../../routes/ChristmasTown/interfaces/IWindow'

const extendedWindow = window as IWindow

class Sidebar extends React.Component {
  render() {
    const pathsWithoutSidebar = extendedWindow.userIsTravelling() ?
      PATHS_WITHOUT_SIDEBAR_TRAVEL_MODE :
      PATHS_WITHOUT_SIDEBAR

    // it's a hack for managing sidebar/non-sidebar layout for mixed CT&CTE SPA-like app.
    manageAppWrapLayout({ pagesWithoutSidebar: pathsWithoutSidebar })

    if (checkIsPageWithoutSidebar(pathsWithoutSidebar)) {
      return null
    }

    return ReactDOM.createPortal(
      <SidebarGlobal rootStore={rootStore} injector={injectReducer} />,
      document.querySelector('#sidebarroot')
    )
  }
}

export default Sidebar
