import IInitParams from '@torn/ics/src/models/IInitParams'

export interface IInitialState {
  appHeader: object
  locationCurrent?: any
  appTitle?: string
  appID?: string
  pageID?: number
  debug?: {
    msg: string | object
  }
  info?: string
  isDesktopLayoutSet?: any
  debugCloseAction?: () => void
  ics?: {
    show: boolean
    initParams?: IInitParams
  }
}

export interface IAsyncReducersStore {
  asyncReducers: object
}
