export interface IStore {
  asyncReducers?: object
}

export interface IProps {
  key: string
  reducer: any
}
