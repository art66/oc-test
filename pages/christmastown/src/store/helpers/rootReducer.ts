import { Store, combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import { connectRouter } from 'connected-react-router'

import history from '../history'
import common from '../../controller/reducers'
import { IStore, IProps } from './interfaces'

export const makeRootReducer = (asyncReducers?: object) => {
  return combineReducers({
    ...asyncReducers,
    common,
    router: connectRouter(history),
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    })
  })
}

export const injectReducer = (store: IStore & Store<any>, { key, reducer }: IProps) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return
  store.asyncReducers[key] = reducer

  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
