import { all } from 'redux-saga/effects'

import christmasTownSaga from '../../routes/ChristmasTown/sagas'
import christmasTown from '../../routes/ChristmasTown/modules/sagas'
import myMaps from '../../routes/MyMaps/modules/sagas'
import allMaps from '../../routes/AllMaps/modules/sagas'
import allChats from '../../routes/AllChats/sagas'
import controllerSaga from '../../controller/sagas'

// import parametersEditor from '../../routes/ChristmasTown/modules/sagas'
import mapEditor from '../../../../christmastown_editor/src/routes/MapEditor/sagas'
import npcEditor from '../../../../christmastown_editor/src/routes/NpcEditor/sagas'

export default function* rootSaga() {
  yield all([
    controllerSaga(),
    allMaps(),
    myMaps(),
    christmasTown(),
    mapEditor(),
    christmasTownSaga(),
    npcEditor(),
    allChats()
  ])
}
