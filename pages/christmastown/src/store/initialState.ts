import currentLocation from '../utils/getWindowLocation'
import { IInitialState } from './interfaces'
import { APP_HEADER_DATA } from '../constants'

const initialState: IInitialState = {
  appHeader: APP_HEADER_DATA,
  locationCurrent: currentLocation,
  appTitle: 'ChristmasTown',
  appID: 'ChristmasTown',
  pageID: null,
  debug: null,
  info: null,
  isDesktopLayoutSet: false,
  ics: {
    show: false
  }
}

export default initialState
