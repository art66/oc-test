import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'connected-react-router'
import throttle from 'redux-throttle'
import { reduxLogger } from '../controller/middleware'
import makeRootReducer from './helpers/rootReducer'
import rootSaga from './helpers/rootSaga'
import activateStoreHMR from './helpers/storeHMR'
import history from './history'

import { IAsyncReducersStore } from './interfaces'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    getCookie: any
    devToolsExtension: any
    __REDUX_DEVTOOLS_EXTENSION__: any
  }
}

const sagaMiddleware = createSagaMiddleware()

const defaultWait = 35
const defaultThrottleOption = {
  // https://lodash.com/docs#throttle
  leading: true,
  trailing: true
}

const rootStore = (initialState: object = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [routerMiddleware(history), throttle(defaultWait, defaultThrottleOption), thunk, sagaMiddleware]

  // @ts-ignore
  if (__DEV__ && window.getCookie('uid') !== '2072301') {
    middleware.push(reduxLogger)
  }

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  // @ts-ignore
  if (__DEV__) {
    const { __REDUX_DEVTOOLS_EXTENSION__ } = window

    if (__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(__REDUX_DEVTOOLS_EXTENSION__())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store: any & IAsyncReducersStore = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer, applyMiddleware(...middleware), ...enhancers)
  )

  sagaMiddleware.run(rootSaga)
  store.asyncReducers = {}

  activateStoreHMR(store)

  return store
}

export default rootStore()
