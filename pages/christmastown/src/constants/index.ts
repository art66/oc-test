export const TYPE_IDS = {
  '': 1,
  mymaps: 2,
  mapeditor: 3,
  parametereditor: 4,
  npceditor: 5,
  allmaps: 6
}

export const SHOW_DEBUG_BOX = 'SHOW_DEBUG_BOX'
export const HIDE_DEBUG_BOX = 'HIDE_DEBUG_BOX'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_INFO_BOX = 'HIDE_INFO_BOX'
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE'
export const MEDIA_SCREEN_CHANGED = 'redux-responsive/CALCULATE_RESPONSIVE_STATE'
export const ROUTE_HEADER_SET = 'ROUTE_HEADER_SET'
export const GET_ROUTE_HEADER_SET = 'GET_ROUTE_HEADER_SET'
export const CHECK_MANUAL_DESKTOP_MODE = 'CHECK_MANUAL_DESKTOP_MODE'

export const APP_HEADER_DATA = {
  active: true,
  titles: {
    default: {
      ID: 0,
      title: 'Christmas Town'
    },
    list: [
      {
        ID: 1,
        subTitle: ''
      },
      {
        ID: 2,
        subTitle: 'My Maps'
      },
      {
        ID: 3,
        subTitle: 'Map Editor'
      },
      {
        ID: 4,
        subTitle: 'Parameters Editor'
      },
      {
        ID: 5,
        subTitle: 'NPC Editor'
      },
      {
        ID: 6,
        subTitle: 'All Maps'
      }
    ]
  },
  links: {
    default: {},
    list: []
  }
}

export const UNSUPPORTED_DEVICE_MESSAGE =
  'Sorry, the Christmas Town Editor only works with a minimum screen width of 1000px. Please use a different device.'
export const PATHS_WITHOUT_SIDEBAR = ['#/parametereditor', '#/npceditor', '#/mapeditor', '#/allmaps', '#/allchats']
export const PATHS_WITHOUT_SIDEBAR_TRAVEL_MODE = [
  '#/parametereditor',
  '#/npceditor',
  '#/mapeditor',
  '#/allmaps',
  '#/allchats',
  '#/',
  '#/mymaps'
]
export const PATHS_WITH_SUPPORT_CONTENT = ['#/', '#/mymaps']

export const FORBIDDEN_ERROR_CODE = 403
export const FORBIDDEN_ERROR_TEXT = 'Access Forbidden'
export const DARK_THEME = 'dark'
