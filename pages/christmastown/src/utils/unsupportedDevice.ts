import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import checkMediaType from '@torn/shared/utils/checkMediaType'

const DESKTOP_MIN_WIDTH = 1000

interface IUnsupportedDevice {
  mediaType?: TMediaType
  deviceWidth?: number
}

const unsupportedDevice = ({ mediaType = 'desktop', deviceWidth = 1600 }: IUnsupportedDevice) => {
  const { isDesktop } = checkMediaType(mediaType)
  const isDesktopScreenDevice = deviceWidth > DESKTOP_MIN_WIDTH

  return !isDesktop || !isDesktopScreenDevice
}

export default unsupportedDevice
