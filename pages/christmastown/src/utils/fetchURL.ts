/* global addRFC */
import { FORBIDDEN_ERROR_CODE, FORBIDDEN_ERROR_TEXT } from '../constants'

export const MAIN_URL = '/christmas_town.php?q='

export function fetchUrl(url: string, data: object) {
  // @ts-ignore
  return fetch(addRFC(MAIN_URL + url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    if (response.status === FORBIDDEN_ERROR_CODE) {
      throw new Error(FORBIDDEN_ERROR_TEXT)
    }

    return response.text().then(text => {
      try {
        const json = JSON.parse(text)

        return json
      } catch (e) {
        throw new Error(`Server responded with: ${text}`)
      }
    })
  })
}
