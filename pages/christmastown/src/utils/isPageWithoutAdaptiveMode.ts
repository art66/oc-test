import { PATHS_WITHOUT_SIDEBAR, PATHS_WITHOUT_SIDEBAR_TRAVEL_MODE } from '../constants'
import { IWindow } from '../routes/ChristmasTown/interfaces/IWindow'

const extendedWindow = window as IWindow

const isPageWithoutAdaptiveMode = () => {
  return extendedWindow.userIsTravelling && extendedWindow.userIsTravelling()
    ? PATHS_WITHOUT_SIDEBAR_TRAVEL_MODE.includes(window.location.hash)
    : PATHS_WITHOUT_SIDEBAR.includes(window.location.hash)
}

export default isPageWithoutAdaptiveMode
