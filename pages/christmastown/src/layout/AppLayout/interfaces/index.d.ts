import { match, RouteComponentProps } from 'react-router'
import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import IInitParams from '@torn/ics/src/models/IInitParams'
import { IInitialState } from '../../../store/interfaces'

export interface IProps extends RouteComponentProps, IInitialState {
  checkManualDesktopMode: (status: boolean) => void
  infoBoxShow: (msg: string) => void
  infoBoxHide: () => void
  mediaType: TMediaType
  children: any
  match: match<{ myParam: string }>
  toggleICS: (show: boolean) => void
  imageCropSystem: {
    show: boolean
    initParams: IInitParams
  }
}

export interface IContainerStore {
  common: IInitialState
  browser: any
}
