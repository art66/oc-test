import React from 'react'
import { Store } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'
import AppHeader from '@torn/shared/components/AppHeader'
import DebugBox from '@torn/shared/components/DebugBox'
import InfoBox from '@torn/shared/components/InfoBox'
import Tooltips from '../../components/Tooltips'
import Sidebar from '../../components/Sidebar'
import Delimiter from '../../components/Delimiter'
import { infoShow, infoHide, debugHide, checkManualDesktop } from '../../controller/actions'
import { toggleICS } from '../../../../christmastown_editor/src/store/actions'
import { IProps, IContainerStore } from './interfaces'
import { UNSUPPORTED_DEVICE_MESSAGE, PATHS_WITH_SUPPORT_CONTENT } from '../../constants'
import unsupportedDevice from '../../utils/unsupportedDevice'
import isPageWithoutAdaptiveMode from '../../utils/isPageWithoutAdaptiveMode'
import { addICSRootElement, removeICSRootElement } from '../../../../christmastown_editor/src/utils/icsHelper'
import ICS from '../../../../christmastown_editor/src/components/ICS'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'
import styles from './index.cssmodule.scss'
import '../../styles/global.cssmodule.scss'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'

class AppLayout extends React.PureComponent<IProps> {
  componentDidMount() {
    this._checkManualDesktopMode()
    this._checkUnsupportedDevice()
    addICSRootElement()
    document.addEventListener('onCloseICS', this.closeICS)
  }

  componentDidUpdate() {
    this._checkUnsupportedDevice()
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
    removeICSRootElement()
    document.removeEventListener('onCloseICS', this.closeICS)
  }

  closeICS = () => this.props.toggleICS(false)

  _checkManualDesktopMode = () => {
    const { checkManualDesktopMode } = this.props

    subscribeOnDesktopLayout((payload: boolean) => checkManualDesktopMode(payload))
  }

  _checkUnsupportedDevice = () => {
    const { mediaType, infoBoxShow, infoBoxHide } = this.props

    isPageWithoutAdaptiveMode()
    && unsupportedDevice({ mediaType })
    && !PATHS_WITH_SUPPORT_CONTENT.includes(window.location.hash) ?
      infoBoxShow(UNSUPPORTED_DEVICE_MESSAGE) :
      isPageWithoutAdaptiveMode() && infoBoxHide()
  }

  _renderBody = () => {
    const { children, mediaType } = this.props

    if (
      isPageWithoutAdaptiveMode()
      && unsupportedDevice({ mediaType })
      && !PATHS_WITH_SUPPORT_CONTENT.includes(window.location.hash)
    ) {
      return null
    }

    return <div className='core-layout__viewport'>{children}</div>
  }

  render() {
    const { debug, debugCloseAction, info, appID, pageID, appHeader, imageCropSystem } = this.props

    return (
      <div className={styles.appCTContainer}>
        <Tooltips />
        <Sidebar />
        <AppHeader appID={appID} pageID={pageID} clientProps={appHeader} />
        {debug && (
          <>
            <DebugBox debugMessage={debug} close={debugCloseAction} isBeatifyError={true} />
            <Delimiter />
          </>
        )}
        {info && !debug && <InfoBox msg={info} />}
        {this._renderBody()}
        {imageCropSystem.show ? <ICS initParams={imageCropSystem.initParams} /> : null}
      </div>
    )
  }
}

const mapStateToProps = (state: IContainerStore & Store<any>) => ({
  appHeader: state.common.appHeader,
  appID: state.common.appID,
  pageID: state.common.pageID,
  appTitle: state.common.appTitle,
  debug: state.common.debug,
  info: state.common.info,
  mediaType: state.browser.mediaType,
  imageCropSystem: state.common.ics
})

const mapDispatchToState = dispatch => ({
  checkManualDesktopMode: (payload: boolean) => dispatch(checkManualDesktop(payload)),
  debugCloseAction: () => dispatch(debugHide()),
  infoBoxShow: (msg: string) => dispatch(infoShow(msg)),
  infoBoxHide: () => dispatch(infoHide()),
  toggleICS: show => dispatch(toggleICS(show))
})

const connectAppToReduxStore = connect(mapStateToProps, mapDispatchToState)(AppLayout)

export default withRouter(connectAppToReduxStore)
