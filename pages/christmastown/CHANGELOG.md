# ChristmasTown - Torn


## 2.3.3
 * Improved debounce unsubscribing logic to prevent memory leak while running.

## 2.3.2
 * Improved Sidebar import and abstraction at all.
 * Fixed collision btw AllMaps and MyMaps routes saga requests.

## 2.3.1
 * Added polling timer.

## 2.2.1
 * Implemented permissions links layout in header.

## 2.2.0
 * Added MapsEditor and ParametersEditor apps injection directly inside CT app.

## 2.1.0
 * Added skeletons for CT and MyMaps apps.

## 2.0.0
 * Sidebar was injected directly into the CT app.

## 1.5.0
 * Added debugBox action & appear of its layout.

## 1.4.1
 * Fixed non-SPA links anchors in appHeader.

## 1.4.0
 * Disabled in-line legacy script in template.ejs.

## 1.3.0
 * Implemented modern routing model in the app.

## 1.2.0
 * Sidebar were replaced from SPA-like to basic back-end injection version.

## 1.1.0
 * First stable routing release.

## 1.0.0
 * Initial routing release.
