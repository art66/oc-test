export function fetchUrl(url) {
  return fetch(url, {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
  }).then(response =>
    response.text().then(text => {
      try {
        var json = JSON.parse(text)
        if (json.reult && json.result.error) {
          var error = json.reuslt.error
        }
      } catch (e) {
        error = 'Server responded with: ' + text
      }
      if (error) {
        throw new Error(error)
      } else {
        return json
      }
    })
  )
}
