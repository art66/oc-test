import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'

export class ItemMod extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  expand = (event, ID) => {
    event.preventDefault()

    const { armoury } = this.props.item

    if (this.props.item.attached) {
      this.props.actions.requestDetach(this.props.item.ID)
    } else if (armoury.length > 0) {
      this.props.actions.expand({ ID: ID })
    }
  }
  attach = (event, upgradeID, armouryID) => {
    event.preventDefault()
    this.props.actions.requestAttach(upgradeID, armouryID)
  }
  render() {
    const { lastRow, item, selected } = this.props
    const { ID, armoury, description, effectDesc, cssClass, attached } = item
    const liClass = classnames({
      selected: selected,
      attached: attached,
      disabled: armoury.length === 0,
      'last-row': lastRow
    })

    const compatibles = armoury ? (
      <ul className="dropdown">
        {armoury.map((item, i) => {
          return (
            <li key={i}>
              <a
                href="#"
                role="button"
                aria-label={'Attach to ' + item.type}
                onClick={event => this.attach(event, ID, item.armouryID)}
              >
                {item.type}
              </a>
            </li>
          )
        })}
      </ul>
    ) : null

    return (
      <li
        className={liClass}
        onClick={event => {
          this.expand(event, ID)
        }}
        role="button"
        aria-label={attached ? 'Detach ' + description : description}
        aria-disabled={armoury.length === 0}
        aria-haspopup={armoury.length !== 0}
      >
        <i className={attached ? 'detach-icon' : 'attach-icon'} />
        <i className={'mod-icon-large-' + cssClass} />
        <span className="mod-details">
          <span className="mod-title">{description}</span>
          <br />
          <span className="mod-desc">{effectDesc}</span>
        </span>
        {compatibles}
      </li>
    )
  }
}

ItemMod.propTypes = {
  item: PropTypes.object.isRequired,
  selected: PropTypes.bool,
  lastRow: PropTypes.bool,
  actions: PropTypes.object
}

export default ItemMod
