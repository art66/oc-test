import { handleActions } from 'redux-actions'

const initialState = {
  mods: [],
  dataNeverLoaded: true
}

export default handleActions(
  {
    'data loaded'(state, action) {
      return {
        ...state,
        loading: false,
        dataNeverLoaded: false,
        mods: action.payload
      }
    },

    expand(state, action) {
      return {
        ...state,
        expandedItem: state.expandedItem === action.payload.ID ? null : action.payload.ID
      }
    },

    attach(state, action) {
      const currentItemIndex = state.mods.findIndex(function(item) {
        return item.ID === action.payload.ID
      })
      return {
        ...state,
        mods: [
          ...state.mods.slice(0, currentItemIndex),
          {
            ...state.mods[currentItemIndex],
            attached: true
          },
          ...state.mods.slice(currentItemIndex + 1)
        ],
        expandedItem: null
      }
    },

    detach(state, action) {
      const currentItemIndex = state.mods.findIndex(function(item) {
        return item.ID === action.payload.ID
      })
      return {
        ...state,
        mods: [
          ...state.mods.slice(0, currentItemIndex),
          {
            ...state.mods[currentItemIndex],
            attached: false
          },
          ...state.mods.slice(currentItemIndex + 1)
        ],
        expandedItem: null
      }
    },

    'bring info box'(state, action) {
      return {
        ...state,
        infoBox: {
          msg: action.payload.msg,
          color: action.payload.color
        }
      }
    },

    'hide info box'(state, action) {
      return {
        ...state,
        infoBox: null
      }
    }
  },
  initialState
)
