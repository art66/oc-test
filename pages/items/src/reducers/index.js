import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import itemsState from './mods'

const rootReducer = combineReducers({
  browser: createResponsiveStateReducer({
    small: 600,
    medium: 1000,
    large: 2000
  }),
  itemsState
})

export default rootReducer
