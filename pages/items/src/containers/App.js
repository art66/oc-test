import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ItemMod from '../components/ItemMod.js'
import InfoBox from '@torn/shared/components/InfoBox'
import * as appActions from '../actions'

const ITEMS_PER_ROW = {
  large: 6,
  medium: 3,
  small: 2
}

class App extends Component {
  componentDidMount() {
    this.props.actions.getInitialState()
  }
  renderList() {
    const { mods, expandedItem, actions, browser } = this.props

    if (mods.length === 0) {
      return <div className="p10">Currently no weapon mods</div>
    }

    return mods.map((item, i) => {
      const itemsPerRow = ITEMS_PER_ROW[browser.mediaType]
      const totalItems = mods.length
      const lastRowIndex = totalItems - (totalItems % itemsPerRow || itemsPerRow)
      const lastRow = i >= lastRowIndex

      return <ItemMod key={i} item={item} actions={actions} selected={expandedItem === item.ID} lastRow={lastRow} />
    })
  }
  render() {
    const { infoBox, dataNeverLoaded } = this.props

    const info = infoBox ? <InfoBox msg={infoBox.msg} color={infoBox.color} /> : null
    if (dataNeverLoaded) return null

    return (
      <div>
        {info}
        <div className="title-black top-round">Mod Inventory</div>
        <div className="mod-inventory cont-gray bottom-round">
          <ul className="mods clearfix">{this.renderList()}</ul>
        </div>
      </div>
    )
  }
}

App.propTypes = {
  mods: PropTypes.array.isRequired,
  actions: PropTypes.object,
  expandedItem: PropTypes.string,
  browser: PropTypes.object,
  dataNeverLoaded: PropTypes.bool,
  infoBox: PropTypes.object
}

function mapStateToProps(state) {
  const { mods, dataNeverLoaded, expandedItem, infoBox } = state.itemsState
  return {
    mods,
    dataNeverLoaded,
    expandedItem,
    infoBox,
    browser: state.browser
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
