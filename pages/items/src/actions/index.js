import { createAction } from 'redux-actions'
import * as types from '../constants/ActionTypes'
import { fetchUrl } from '../utils'
import _ from 'lodash'

export function attachMod(text) {
  return { type: types.ATTACH_MOD, text }
}

export function getInitialState() {
  return dispatch => {
    return fetchUrl('/loader.php?sid=itemsMods&mode=json&step=getInitialData')
      .then(json => {
        dispatch(dataLoaded(parseItems(json)))
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const dataLoaded = createAction('data loaded')

const parseItems = data => {
  if (data.error) {
    throw new Error(data.error)
  }
  if (!data.upgrades) {
    throw 'Malformed server response'
  }
  if (!data.userUpgrades || Object.keys(data.userUpgrades).length === 0) {
    data.userUpgrades = []
  }

  return data.upgrades.reduce((newUpgrades, item) => {
    const itemInMyInventory = _.find(data.userUpgrades, function(myItem) {
      return myItem.upgradeID === item.ID
    })
    if (itemInMyInventory) {
      newUpgrades.push({
        ...item,
        armoury: itemInMyInventory ? itemInMyInventory.armoury : null,
        attached: itemInMyInventory ? itemInMyInventory.attached === 'true' : false
      })
    }
    return newUpgrades
  }, [])
}

export function requestAttach(upgradeID, armouryID) {
  return dispatch => {
    return fetchUrl(
      '/loader.php?sid=itemsMods&mode=json&step=attach&page=mods&upgradeID=' + upgradeID + '&armouryID=' + armouryID
    )
      .then(json => {
        if (json.result) {
          dispatch(dataLoaded(parseItems(json.result)))
        }
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export function requestDetach(upgradeID) {
  return dispatch => {
    return fetchUrl('/loader.php?sid=itemsMods&mode=json&step=detach&page=mods&upgradeID=' + upgradeID)
      .then(json => {
        if (json.result) {
          dispatch(dataLoaded(parseItems(json.result)))
        }
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const attach = createAction('attach')
export const detach = createAction('detach')

export const expand = createAction('expand')

export const bringInfoBox = createAction('bring info box')
export const hideInfoBox = createAction('hide info box')

export function showInfoBox(argsObj) {
  return dispatch => {
    dispatch(bringInfoBox(argsObj))

    if (!argsObj.persistent) {
      setTimeout(() => dispatch(hideInfoBox()), 5000)
    }
  }
}
