import React, { Component } from 'react'

class Preloader extends Component {
  render() {
    return (
      <div className="profile-preloader-wrap cont-gray">
        <div className="dzsulb-preloader preloader-fountain">
          <div className="fountainG fountainG_1" />
          <div className="fountainG fountainG_2" />
          <div className="fountainG fountainG_3" />
          <div className="fountainG fountainG_4" />
        </div>
      </div>
    )
  }
}

export default Preloader
