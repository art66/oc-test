import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ProfileBlock from './ProfileBlock'

class PersonalInformation extends Component {
  render() {
    const { info, visible } = this.props.personalInformation
    const { playerName } = this.props.user

    return (
      <div className="personal-information profile-right-wrapper right">
        <ProfileBlock title="Personal Information">
          <div className="profile-container personal-info">
            {visible ? (
              <ul className='info-table'>
                <li>
                  <div className="user-information-section">
                    <span className="bold">Real name</span>
                  </div>
                  <div className='user-info-value'>
                    {info && info.realName ? <span dangerouslySetInnerHTML={{ __html: info.realName }} /> : <span>N/A</span>}
                  </div>

                </li>
                <li>
                  <div className="user-information-section">
                    <span className="bold">Country</span>
                  </div>
                  <div className='user-info-value'>
                    {info && info.country ? <span dangerouslySetInnerHTML={{ __html: info.country }} /> : <span>N/A</span>}
                  </div>
                </li>
                <li>
                  <div className="user-information-section">
                    <span className="bold">City</span>
                  </div>
                  <div className='user-info-value'>
                    {info && info.city ? <span dangerouslySetInnerHTML={{ __html: info.city }} /> : <span>N/A</span>}
                  </div>
                </li>
                <li>
                  <div className="user-information-section">
                    <span className="bold">Age</span>
                  </div>
                  <div className='user-info-value'>
                    {info && info.age && info.age !== '0' ? info.age : <span>N/A</span>}
                  </div>
                </li>
              </ul>
            ) : (
              <p className="t-gray-9 p10">{playerName} doesn't wish to share</p>
            )}
          </div>
        </ProfileBlock>
      </div>
    )
  }
}

PersonalInformation.propTypes = {
  personalInformation: PropTypes.object,
  user: PropTypes.object
}

export default PersonalInformation
