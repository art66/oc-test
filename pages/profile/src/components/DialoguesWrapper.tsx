import React, { Component, ReactElement } from 'react'
import ProfileBlock from './ProfileBlock'
import Dialog from './ProfileButtons/Dialog'
import SendCash from './ProfileButtons/SendCash'
import AddUser from './ProfileButtons/AddUser'
import IProfileButtons from '../interfaces/IProfileButtons'
import IUser from '../interfaces/IUser'

interface IProps {
  profileButtons: IProfileButtons
  user: IUser
  currentUser: any
  content: ReactElement
  isMiniProfile?: boolean
  loading?: boolean
  lastAction?: { seconds: number | 'Unknown' }
  showDialog: (text: string, confirmAction: string, color: string) => void
  hideDialog: () => void
  confirmDialog: () => void
  addToFriendList: (reason: string) => void
  addToEnemyList: (reason: string) => void
  setMoneyData: (moneyAmount: string, moneyDesc: string, anonymous: boolean) => void
  toggleAddUserDialog: (show: boolean, actionType: string) => void
  fetchProfileData: () => void
}

class DialoguesWrapper extends Component<IProps> {
  static defaultProps = {
    isMiniProfile: false
  }

  getActiveDialog = () => {
    const {
      user,
      currentUser,
      isMiniProfile,
      confirmDialog,
      showDialog,
      hideDialog,
      setMoneyData,
      lastAction,
      toggleAddUserDialog,
      addToFriendList,
      addToEnemyList,
      profileButtons,
      fetchProfileData
    } = this.props
    const { dialog, isVisibleCashDialog, addUserDialog, buttons } = profileButtons

    let content

    let title

    if (isVisibleCashDialog) {
      title = buttons['sendMoney'].message
      content = (
        <SendCash
          currentUser={currentUser}
          user={user}
          showDialog={showDialog}
          hideDialog={hideDialog}
          setMoneyData={setMoneyData}
          lastAction={isMiniProfile ? user.lastAction : lastAction}
          fetchProfileData={fetchProfileData}
          isMiniProfile={isMiniProfile}
        />
      )
    } else if (addUserDialog && addUserDialog.show) {
      title = `Add ${user.playerName} to your ${addUserDialog.actionType.toLowerCase()} list`
      content = (
        <AddUser
          addUserDialog={addUserDialog}
          toggleAddUserDialog={toggleAddUserDialog}
          playerName={user.playerName}
          addToFriendList={addToFriendList}
          addToEnemyList={addToEnemyList}
          fetchProfileData={fetchProfileData}
        />
      )
    } else if (dialog && dialog.text) {
      content = (
        <Dialog
          dialog={dialog}
          confirmAction={confirmDialog}
          cancelAction={hideDialog}
          fetchProfileData={fetchProfileData}
        />
      )
    }

    return {
      content,
      title
    }
  }

  render() {
    const { content, isMiniProfile } = this.props
    const dialogue = this.getActiveDialog()
    const titleProfileBlock = (dialogue && dialogue.title) || 'Actions'

    return isMiniProfile ? (
      dialogue.content || content
    ) : (
      <div className='profile-buttons profile-action'>
        <ProfileBlock title={titleProfileBlock}>
          <div className='profile-container'>{dialogue.content ? dialogue.content : content}</div>
        </ProfileBlock>
      </div>
    )
  }
}

export default DialoguesWrapper
