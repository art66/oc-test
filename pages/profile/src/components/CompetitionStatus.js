import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ProfileBlock from './ProfileBlock'

class CompetitionStatus extends Component {
  render() {
    const { competitionStatus } = this.props
    const className = competitionStatus.class || ''
    const defaultMessage = 'There is no active competition'
    const competitionMessage = competitionStatus.message || defaultMessage

    return (
      <div className='competition-status profile-right-wrapper right m-top10' id='competition-profile-wrapper'>
        <ProfileBlock title='Competition Status'>
          <div className={`profile-container competition-wrap p10 ${className}`}>
            <span className='message t-gray-9' dangerouslySetInnerHTML={{ __html: competitionMessage }} />
          </div>
        </ProfileBlock>
      </div>
    )
  }
}

CompetitionStatus.propTypes = {
  competitionStatus: PropTypes.object
}

export default CompetitionStatus
