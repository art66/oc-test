import React, { PureComponent } from 'react'
import IUserIcon from '../../interfaces/IUserIcon'
import BasicInfoIcon from './BasicInfoIcon'

interface IProps {
  icons: IUserIcon[]
  isMiniProfile?: boolean
  userID: number | string
}

class BasicInfoIconsList extends PureComponent<IProps> {
  static defaultProps = {
    isMiniProfile: false
  }

  render() {
    const { icons, isMiniProfile, userID } = this.props

    return (
      <div className='icons'>
        <ul className='row basic-info big svg'>
          {icons.map(icon => (
            <BasicInfoIcon
              key={icon.id}
              isMiniProfile={isMiniProfile}
              iconProps={icon}
              userID={userID}
            />
          ))}
        </ul>
      </div>
    )
  }
}

export default BasicInfoIconsList
