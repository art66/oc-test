import React, { PureComponent } from 'react'
import ToolTip from '@torn/shared/components/Tooltip'
import IUserIcon from '../../../interfaces/IUserIcon'

declare function getCurrentTimestamp (): number

interface IProps {
  iconProps: IUserIcon
  isMiniProfile?: boolean
  userID: number | string
}

interface IState {
  timestamp: number
}

class BasicInfoIcon extends PureComponent<IProps, IState> {
  countDownTimer: any

  static defaultProps = {
    isMiniProfile: false
  }

  constructor(props: IProps) {
    super(props)
    const { iconProps } = this.props

    this.state = {
      timestamp: iconProps.timestamp ? iconProps.timestamp * 1000 - getCurrentTimestamp() : 0
    }
  }

  componentDidMount() {
    const { iconProps } = this.props

    if (iconProps.timestamp && iconProps.timestamp > 0) {
      this.updateTime()
    }
  }

  componentWillUnmount() {
    clearInterval(this.countDownTimer)
  }

  formatTime = () => {
    const { timestamp } = this.state

    let seconds = timestamp / 1000

    let minutes = Math.floor(seconds / 60)

    seconds %= 60

    let hours = Math.floor(minutes / 60)

    minutes %= 60

    seconds = Math.trunc(seconds) !== 59 ? Math.ceil(seconds) : Math.trunc(seconds)

    hours = this.addLeadZero(hours)
    minutes = this.addLeadZero(minutes)
    seconds = this.addLeadZero(seconds)

    return `${hours}:${minutes}:${seconds}`
  }

  decTimer = () => {
    const { timestamp } = this.state

    if (timestamp) {
      this.setState({
        timestamp: timestamp - 1000
      })
    }
  }

  addLeadZero = time => {
    return time < 10 ? `0${time}` : time
  }

  updateTime = () => {
    clearInterval(this.countDownTimer)
    this.countDownTimer = setInterval(this.decTimer, 1000)
  }

  render() {
    const { iconProps, isMiniProfile, userID } = this.props
    const { id, type, description, url, timestamp, title } = iconProps
    const elID = `icon${id}${isMiniProfile ? '-mini' : ''}-profile-${userID}`

    return (
      <li
        id={elID}
        className={`user-status-16-${type} left`}
      >
        {url ? <a href={url} /> : ''}
        <ToolTip
          position='top'
          arrow='center'
          parent={elID}
        >
          <span className='user-icons-tooltip user-short-description'>
            <span>{title || ''}</span>
            <span dangerouslySetInnerHTML={{ __html: description }} />
            <span>{timestamp ? `${this.formatTime()}` : ''}</span>
          </span>
        </ToolTip>
      </li>
    )
  }
}

export default BasicInfoIcon
