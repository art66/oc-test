import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { SVGIconGenerator } from '@torn/shared/SVG'
import profileIcons from '@torn/shared/SVG/helpers/iconsHolder/profile'
import { withTheme } from '@torn/shared/hoc/withTheme/withTheme'
import { ICON_TYPE } from '../../constants'
import { svgIconsDimensions } from '../../constants/svgIconsDimensions'

class ProfileButton extends Component {
  camelSizeName(str) {
    return str.replace(/\w+/g, w => w[0].toUpperCase() + w.slice(1))
  }

  transformToUpperCase(str) {
    return str.split(' ').join('_').toUpperCase()
  }

  checkStateRedOrGreen(str) {
    const color = str.split(' ')
    const isDarkMode = this.props.theme === 'dark'

    if (color[0] === 'red') {
      return isDarkMode ? 'RED_HOVER_DARK_MODE' : 'RED_HOVER'
    } else if (color[0] === 'green') {
      return isDarkMode ? 'GREEN_HOVER_DARK_MODE' : 'GREEN_HOVER'
    }
    return isDarkMode ? 'HOVER_DARK_MODE' : 'HOVER'
  }

  render() {
    const { id, name, button, className, onClick, onMouseEnter, onMouseLeave, theme } = this.props
    const isDarkMode = theme === 'dark'
    const subTypeName = this.transformToUpperCase(button.state)
    const isDisabled =
      button.state !== 'disabled' && button.state !== 'clickable disabled' && button.state !== 'cross disabled'
    const { dimensions } = svgIconsDimensions(this.camelSizeName(name))
    const disabledPreset = isDarkMode ? 'DISABLED_DARK_MODE' : 'DISABLED'
    const preset = isDarkMode ? `${subTypeName}_DARK_MODE` : subTypeName

    return (
      <a
        id={id}
        href={button.link || '#'}
        onClick={e => {
          e.preventDefault()
          onClick(e, name)
        }}
        onMouseEnter={() => onMouseEnter(name)}
        onMouseLeave={() => onMouseLeave(name)}
        className={`profile-button profile-button-${name} ${className} ${button.state || 'active'}`}
        aria-label={button.actionDescription}
      >
        <SVGIconGenerator
          customClass='profileButtonIcon'
          iconName={this.camelSizeName(name)}
          iconsHolder={profileIcons}
          preset={{
            type: ICON_TYPE,
            subtype: `${button.state === 'cross disabled' ? disabledPreset : preset}`
          }}
          dimensions={dimensions}
          filter={
            isDarkMode ?
              { active: false } :
              {
                ID: 'whiteFilter',
                active: true,
                shadow: {
                  active: true,
                  x: 0,
                  y: 1,
                  blur: 0,
                  color: 'white'
                }
              }
          }
          onHover={{
            active: isDisabled,
            preset: {
              type: ICON_TYPE,
              subtype: this.checkStateRedOrGreen(button.state)
            }
          }}
        />

        {button.state === 'cross disabled' && (
          <SVGIconGenerator
            iconsHolder={profileIcons}
            iconName='CrossDisabled'
            preset={{
              type: ICON_TYPE,
              subtype: isDarkMode ? 'CROSS_DISABLED_DARK_MODE' : 'CROSS_DISABLED'
            }}
            dimensions={{
              width: 46,
              height: 46,
              viewbox: '551.393 356 44 44'
            }}
            filter={{ active: false }}
            onHover={{ active: false }}
          />
        )}
      </a>
    )
  }
}

ProfileButton.defaultProps = {
  name: '',
  button: {},
  className: '',
  onClick: () => {},
  onMouseEnter: () => {},
  onMouseLeave: () => {}
}

ProfileButton.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  button: PropTypes.object,
  className: PropTypes.string,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  theme: PropTypes.string
}

export default withTheme(ProfileButton)
