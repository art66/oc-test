import PropTypes from 'prop-types'
import React, { Component } from 'react'
import './dialog.scss'

class Dialog extends Component {
  confirmed = false

  confirm = () => {
    if (!this.confirmed) {
      this.confirmed = true
      this.props.confirmAction()
      this.props.fetchProfileData()
    }
  }

  render() {
    const { dialog, cancelAction } = this.props

    return (
      <div className='profile-buttons-dialog'>
        <div className='center-block'>
          <div
            className={`text ${dialog.color && dialog.color !== 'gray' ? `bold t-${dialog.color}` : ''}`}
            dangerouslySetInnerHTML={{ __html: dialog.text }}
          />
          <div className='confirm-block'>
            {(dialog.confirmed || dialog.color === 'red') && (
              <button type='button' onClick={cancelAction} className='confirm-action okay t-blue h c-pointer bold'>
                Okay
              </button>
            )}
            {!dialog.confirmed && dialog.color !== 'red' && (
              <div>
                <button
                  type='button'
                  onClick={this.confirm}
                  className='confirm-action confirm-action-yes t-blue h c-pointer bold'
                >
                  Yes
                </button>
                <button
                  type='button'
                  onClick={cancelAction}
                  className='confirm-action confirm-action-no t-blue h c-pointer bold'
                >
                  No
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }
}

Dialog.propTypes = {
  dialog: PropTypes.object,
  confirmAction: PropTypes.func,
  cancelAction: PropTypes.func,
  fetchProfileData: PropTypes.func
}

export default Dialog
