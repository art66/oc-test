import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Tooltip from '@torn/shared/components/Tooltip'
import ProfileButton from './ProfileButton'
import './profileButtons.scss'
import { ADD_USER_ACTION_TYPES } from '../../constants'

/* global chat, getAction */

const BUTTON_MSG_DISPLAY_TIME = 1000

class ProfileButtons extends Component {
  static defaultProps = {
    isMiniProfile: false
  }

  initiateChat = () => {
    const { user } = this.props

    if (typeof chat === 'object') {
      chat.r(user.userID)
    }
  }

  sendMoney = () => {
    const { showSendCashDialog } = this.props

    showSendCashDialog()
  }

  addToFriendList = () => {
    const { user, toggleAddUserDialog, addToFriendList, fetchProfileData } = this.props

    if (user.isFriend) {
      addToFriendList().then(() => fetchProfileData())
    } else {
      toggleAddUserDialog(true, ADD_USER_ACTION_TYPES.friend)
    }
  }

  addToEnemyList = () => {
    const { user, toggleAddUserDialog, addToEnemyList, fetchProfileData } = this.props

    if (user.isEnemy) {
      addToEnemyList().then(() => fetchProfileData())
    } else {
      toggleAddUserDialog(true, ADD_USER_ACTION_TYPES.enemy)
    }
  }

  bust = () => {
    const { user, showDialog } = this.props
    const { userID } = user

    getAction({
      type: 'get',
      action: `/jailview.php?XID=${userID}&action=rescue&step=breakout&links=no`,
      success: resp => {
        const data = JSON.parse(resp)

        showDialog(data.msg, 'bust', data.color === 'green' ? 'gray' : data.color)
      }
    })
  }

  bail = () => {
    const { user, showDialog } = this.props

    getAction({
      type: 'get',
      action: `/jailview.php?XID=${user.userID}&action=rescue&step=buy&links=no`,
      success: resp => {
        const data = JSON.parse(resp)

        showDialog(data.msg, 'bail', data.color === 'green' ? 'gray' : data.color)
      }
    })
  }

  revive = () => {
    const { user, showDialog } = this.props

    getAction({
      type: 'get',
      action: `/revive.php?action=revive&ID=${user.userID}&links=no`,
      success: resp => {
        const data = JSON.parse(resp)

        showDialog(data.msg, 'revive', data.color || '')
      }
    })
  }

  handleonMouseEnter = buttonKey => {
    clearTimeout(this.buttonMsgTimeout)

    const { profileButtons, showButtonMsg } = this.props
    const button = profileButtons.buttons[buttonKey]

    showButtonMsg(button.message)
  }

  handleonMouseLeave = () => {
    clearTimeout(this.buttonMsgTimeout)

    const { hideButtonMsg } = this.props

    this.buttonMsgTimeout = setTimeout(() => {
      hideButtonMsg()
    }, BUTTON_MSG_DISPLAY_TIME)
  }

  handleClick = (e, buttonKey) => {
    const { profileButtons } = this.props
    const action = this[buttonKey]
    const button = profileButtons.buttons[buttonKey]

    if (button && button.state.indexOf('disabled') !== -1 && button.state.indexOf('clickable') === -1) {
      return false
    }

    if (action && typeof action === 'function') {
      action()
    } else if (button && button.link) {
      e.ctrlKey || e.metaKey ? window.open(button.link, '_blank') : window.location = button.link
    }
  }

  getButtons = () => {
    const { profileButtons: { buttons }, user: { userID }, isMiniProfile } = this.props
    const mouseListeners = isMiniProfile ? {} : {
      onMouseEnter: this.handleonMouseEnter,
      onMouseLeave: this.handleonMouseLeave
    }

    return Object.keys(buttons)
      .filter(button => buttons[button].state && buttons[button].state.indexOf('hidden') === -1)
      .map((button, key) => {
        const defaultID = `button${key}-profile-${userID}`
        const id = isMiniProfile ? `mini-${defaultID}` : defaultID

        return (
          <React.Fragment key={button}>
            <Tooltip
              active={isMiniProfile}
              position='top'
              arrow='center'
              parent={id}
            >
              {buttons[button].message}
            </Tooltip>
            <ProfileButton
              id={id}
              name={button}
              button={buttons[button]}
              onClick={this.handleClick}
              {...mouseListeners}
            />
          </React.Fragment>
        )
      })
  }

  renderDescription = () => {
    const { isMiniProfile, profileButtons } = this.props

    return isMiniProfile ? null : (
      <div id='profile-container-description' className='profile-container-description'>
        {profileButtons.activeButtonMsg || 'What would you like to do?'}
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.renderDescription()}
        <div className='buttons-wrap'>
          <div className='buttons-list'>{this.getButtons()}</div>
        </div>
        <div className='empty-block' />
      </div>
    )
  }
}

ProfileButtons.propTypes = {
  isMiniProfile: PropTypes.bool,
  profileButtons: PropTypes.object,
  fetchProfileData: PropTypes.func,
  showButtonMsg: PropTypes.func,
  hideButtonMsg: PropTypes.func,
  user: PropTypes.object,
  showDialog: PropTypes.func,
  toggleAddUserDialog: PropTypes.func,
  showSendCashDialog: PropTypes.func,
  addToFriendList: PropTypes.func,
  addToEnemyList: PropTypes.func
}

export default ProfileButtons
