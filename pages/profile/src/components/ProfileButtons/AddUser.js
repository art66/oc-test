import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './profileButtons.scss'
import { ADD_USER_ACTION_TYPES, MAX_REASON_LENGTH } from '../../constants'

class AddUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isValid: true,
      charsLeft: MAX_REASON_LENGTH
    }
  }

  handleAddUserAction = () => {
    const { addUserDialog, addToFriendList, addToEnemyList, toggleAddUserDialog, fetchProfileData } = this.props
    const reason = this.reason.value.trim()

    if (addUserDialog.actionType === ADD_USER_ACTION_TYPES.enemy) {
      addToEnemyList(reason).then(() => fetchProfileData())
    } else {
      addToFriendList(reason).then(() => fetchProfileData())
    }

    toggleAddUserDialog(false, addUserDialog.actionType)
  }

  handleReasonChange = () => {
    const reason = this.reason.value
    const end = this.reason.selectionEnd
    const differentWithMaxValue = MAX_REASON_LENGTH - reason.length
    const charsLeft = differentWithMaxValue < 0 ? 0 : differentWithMaxValue
    const isValid = reason.length <= MAX_REASON_LENGTH

    if (isValid) {
      this.setState({ isValid, charsLeft })
    } else {
      if (this.keyDown === 8 || this.keyDown === 46) {
        this.reason.value = reason
      } else {
        this.reason.value = reason.slice(0, end - 1) + reason.slice(end)
      }

      this.setState({
        isValid: this.reason.value.length <= MAX_REASON_LENGTH,
        charsLeft
      })
    }
  }

  handleCancelAddUser = () => {
    return
  }

  changeOnKeyDown = e => {
    this.keyDown = e.keyCode || e.charCode
  }

  render() {
    const { addUserDialog, toggleAddUserDialog } = this.props
    const { isValid, charsLeft } = this.state
    const inputClasses = `input-text m-top10 m-bottom10 ${!isValid ? 'not-valid-input' : ''}`
    const charsLeftClasses = `chars-left ${!isValid ? 'not-valid-msg' : ''}`

    return (
      <div className='add-user'>
        <div className='reason-wrapper'>
          <p className='description'>
            <span>Description</span>
            <span className={charsLeftClasses}>
              {isValid ? `${charsLeft} characters left` : `Up to ${MAX_REASON_LENGTH} characters`}
            </span>
          </p>
          <input
            className={inputClasses}
            ref={el => this.reason = el}
            onChange={this.handleReasonChange}
            onKeyDown={this.changeOnKeyDown}
          />
          <div className='buttons-wrapper'>
            <button
              type='button'
              className={`torn-btn ${!isValid ? 'disabled' : ''}`}
              onClick={!isValid ? this.handleCancelAddUser : this.handleAddUserAction}
            >
              ADD {addUserDialog.actionType}
            </button>
            <button
              type='button'
              className='cancel-btn t-blue c-pointer h'
              onClick={() => toggleAddUserDialog(false, addUserDialog.actionType)}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    )
  }
}

AddUser.propTypes = {
  addUserDialog: PropTypes.object,
  toggleAddUserDialog: PropTypes.func,
  fetchProfileData: PropTypes.func,
  playerName: PropTypes.string,
  addToFriendList: PropTypes.func,
  addToEnemyList: PropTypes.func
}

export default AddUser
