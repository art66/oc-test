import PropTypes from 'prop-types'
import React, { Component } from 'react'
import TornBtn from '@torn/shared/components/Button'
import {
  AMOUNT_SECONDS_IN_WEEK,
  AMOUNT_SECONDS_IN_DAY,
  AMOUNT_SECONDS_HALF_DAY,
  MAX_MESSAGE_LENGTH,
  MIN_MONEY_TO_TRIGGER_WARNING
} from '../../constants'
/* global $, initializeTooltip */

class SendCash extends Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      anonymous: false,
      inputMoneyValue: ''
    }
  }

  componentDidMount() {
    $(this.inputMoney).tornInputMoney({
      buttonElement: $('.send-cash-btn')
    })
    $('.input-money-symbol').on('click', () => {
      this.setState({
        inputMoneyValue: this.inputMoney.value
      })
    })

    if (this.props.isMiniProfile) {
      initializeTooltip(`.send-cash`, 'white-tooltip')
    }
  }

  _handleMessageChange = () => {
    const msg = this.refs.inputDesc.value

    if (msg.length > MAX_MESSAGE_LENGTH) {
      this.refs.inputDesc.value = msg.slice(0, -(msg.length - MAX_MESSAGE_LENGTH))
    }
  }

  handleSubmit(e) {
    e.preventDefault()
    if (
      this.inputMoneyButton.className.indexOf('disabled') !== -1
      || this.inputMoneyButton.className.indexOf('disable') !== -1
    ) {
      return false
    }

    const { user, lastAction } = this.props
    const { anonymous } = this.state
    const money = this.inputMoney.value
    const valueMoney = Number(money.split(',').join(''))
    const desc = this.refs.inputDesc.value
    const msgDescription = desc && desc.trim().length ? ` with the message: ${desc.trim()}` : ''
    const secondsPassedAfterUserRegister = Math.floor(new Date().getTime() / 1000) - user.signUp

    let msg = `Are you sure you would like to ${anonymous ? 'anonymously' : ''} send
               <span class='bold'>$${money}</span> to
               <span class='bold'>${user.playerName} [${user.userID}]</span>${msgDescription}?`
    const { seconds } = lastAction

    if (seconds > AMOUNT_SECONDS_IN_WEEK) {
      msg += `<div class='warning'>
              Warning: This person hasn't been online for ${Math.floor(seconds / AMOUNT_SECONDS_IN_DAY)} days
              </div>`
    } else if (secondsPassedAfterUserRegister < AMOUNT_SECONDS_HALF_DAY && valueMoney >= MIN_MONEY_TO_TRIGGER_WARNING) {
      msg += `<div class='warning'>
              Warning: Please ensure this is not a scammer impersonating someone else
              </div>`
    }

    this.props.setMoneyData(money, desc, anonymous)
    this.props.showDialog(msg, 'sendMoney')
  }

  render() {
    const { currentUser, hideDialog } = this.props
    const { inputMoneyValue } = this.state

    return (
      <div className='send-cash'>
        <div className='send-cash-text'>
          <span className='warning t-red'>Do not fall for scams! </span>
          Some players may be out to part you from your money, using any means necessary. Use the secure trade feature
          if you want to protect an exchange and check the contents very carefully.
          <a href='/wiki/Scams' className='t-blue c-pointer h'>
            {' '}
            For more info please visit here.
          </a>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className='input-wrap'>
            <input
              ref={input => {
                this.inputMoney = input
              }}
              className='send-cash-input torn-input-money'
              onChange={e => this.setState({ inputMoneyValue: e.target.value })}
              data-money={currentUser.money}
              data-lpignore='true'
            />
          </div>
          <div className='input-wrap input-message-wrap'>
            <input
              className='send-cash-message-input input-text'
              placeholder='Message (optional, up to 200 characters)'
              ref='inputDesc'
              onChange={this._handleMessageChange}
            />
          </div>
          <TornBtn
            className='send-cash-btn'
            value='SEND'
            disabled={!inputMoneyValue || inputMoneyValue === '0' || inputMoneyValue === ' '}
            refFunc={el => { this.inputMoneyButton = el }}
          />
          {currentUser.canSendAnonymously && (
            <div className='choice-container'>
              <input
                ref={input => (this.inputAnonymous = input)}
                id='anonymous'
                className='checkbox-css'
                type='checkbox'
                name='theanon'
                onChange={() => { this.setState({ anonymous: this.inputAnonymous.checked }) }}
              />
              <label htmlFor='anonymous' className='marker-css'>
                Anonymous
              </label>
            </div>
          )}
          <a
            onClick={e => {
              e.preventDefault()
              hideDialog()
            }}
            href='#'
            className='t-blue c-pointer h'
          >
            Cancel
          </a>
        </form>
      </div>
    )
  }
}

SendCash.propTypes = {
  hideDialog: PropTypes.func,
  setMoneyData: PropTypes.func,
  showDialog: PropTypes.func,
  currentUser: PropTypes.object,
  user: PropTypes.object,
  lastAction: PropTypes.object,
  profileButtons: PropTypes.object,
  isMiniProfile: PropTypes.bool
}

export default SendCash
