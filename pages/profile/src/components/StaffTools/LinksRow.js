import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Link from './Link'

class LinksRow extends Component {
  getLinks() {
    const links = this.props.links
    let list = []

    for (let name in links) {
      list.push(<Link key={name} link={links[name]} />)
    }

    return list
  }

  render() {
    const links = this.getLinks()

    return <div className="links-row">- {links}</div>
  }
}

LinksRow.propTypes = {
  links: PropTypes.array
}

export default LinksRow
