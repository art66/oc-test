import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ProfileBlock from '../ProfileBlock'

class Warnings extends Component {
  render() {
    const warnings = this.props.warnings

    return (
      <ProfileBlock title="Warnings" className="staff-tools cont-gray p10">
        <ul className="warnings-list">
          {warnings.map((warning, index) => (
            <li key={index}>
              <div dangerouslySetInnerHTML={{ __html: warning.timesent }} />
              <div dangerouslySetInnerHTML={{ __html: warning.msg }} />
            </li>
          ))}
        </ul>
      </ProfileBlock>
    )
  }
}

Warnings.propTypes = {
  warnings: PropTypes.array
}

export default Warnings
