import PropTypes from 'prop-types'
import React, { Component } from 'react'

class IP extends Component {
  render() {
    const { signUpIp, lastLoginIp } = this.props

    return (
      <ul>
        {signUpIp && <li>- Signup IP: {signUpIp}</li>}
        {lastLoginIp && <li>- Last Login IP: {lastLoginIp}</li>}
      </ul>
    )
  }
}

IP.propTypes = {
  signUpIp: PropTypes.string,
  lastLoginIp: PropTypes.string
}

export default IP
