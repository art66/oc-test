import PropTypes from 'prop-types'
import React, { Component } from 'react'

class Link extends Component {
  getLink() {
    let link = this.props.link

    if (Array.isArray(link)) {
      return (
        <span className="double-link">
          <Link link={link[0]} />
          <span className="double-link-divider">/</span>
          <Link link={link[1]} />
        </span>
      )
    }

    return (
      <a className="staff-link t-blue h" href={link.link}>
        [{link.name}]
      </a>
    )
  }

  render() {
    const link = this.getLink()

    return <span className="">{link}</span>
  }
}

Link.propTypes = {
  link: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
}

export default Link
