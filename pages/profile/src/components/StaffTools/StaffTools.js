import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ProfileBlock from '../ProfileBlock'
import LinksRow from './LinksRow'
import IP from './IP'
import Warnings from './Warnings'

class StaffTools extends Component {
  getLinksRows() {
    const linksGroups = this.props.staffTools.links
    let list = []

    for (let name in linksGroups) {
      list.push(<LinksRow key={name} links={linksGroups[name]} />)
    }

    return list
  }

  getBans() {
    let bans = this.props.staffTools.bans

    if (!bans) {
      return
    }

    const { forum, mail, multijail } = bans

    return (
      <ul className="bans m-bottom10">
        {forum && (
          <li className="m-bottom5">
            Forum banned for {forum.days} - {forum.reason}
          </li>
        )}
        {mail && (
          <li className="m-bottom5">
            Mail banned for {mail.days} days - {mail.reason}
          </li>
        )}
        {multijail && (
          <li className="m-bottom5">
            In federal jail {multijail.days} days - {multijail.reason}
          </li>
        )}
      </ul>
    )
  }

  render() {
    const { signUpIp, lastLoginIp, warnings } = this.props.staffTools
    const linksRows = this.getLinksRows()
    const bans = this.getBans()

    return (
      <div className="profile-wrapper m-top10">
        <ProfileBlock title="Staff Tools" className="staff-tools cont-gray p10 m-bottom10">
          {bans}
          {linksRows}
          <IP signUpIp={signUpIp} lastLoginIp={lastLoginIp} />
        </ProfileBlock>

        {warnings && <Warnings warnings={warnings} />}
      </div>
    )
  }
}

StaffTools.propTypes = {
  staffTools: PropTypes.object
}

export default StaffTools
