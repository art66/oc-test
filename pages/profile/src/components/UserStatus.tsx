import React, { Component } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import profileIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/profile'
import ProfileBlock from './ProfileBlock'
import ITornWindow from '../interfaces/ITornWindow'
import IUser from '../interfaces/IUser'
import IIconStatus from '../interfaces/IIconStatus'
import IUserStatus from '../interfaces/IUserStatus'
import { formattingTime } from '../utils'
import { getStatusDetails, getStatusIcon } from '../utils/userStatus'

interface IState {
  timestamp: number
}

interface IProps {
  userStatus: IUserStatus
  user: IUser
  isMiniProfile: boolean
  fetchProfileData: () => void
}

declare let window: ITornWindow

class UserStatus extends Component<IProps, IState, IIconStatus> {
  timer: number

  static defaultProps = {
    isMiniProfile: false,
    fetchProfileData: () => {}
  }

  constructor(props: IProps) {
    super(props)
    const {
      userStatus: { status }
    } = this.props

    this.timer = 0
    this.state = {
      timestamp: status.timestamp ? status.timestamp * 1000 - window.getCurrentTimestamp() : 0
    }
  }

  componentDidMount() {
    if (!this.timer) {
      this.timer = window.setInterval(this.countDown, 1000)
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  componentDidUpdate(prevProps: IProps) {
    const {
      userStatus: {
        status: { timestamp }
      }
    } = this.props

    if (prevProps.userStatus.status.timestamp !== timestamp) {
      this.updateStatusTimestamp(timestamp)
    }
  }

  updateStatusTimestamp = timestamp => {
    this.setState({
      timestamp: timestamp ? timestamp * 1000 - window.getCurrentTimestamp() : 0
    })

    if (timestamp > 0) {
      clearInterval(this.timer)
      this.timer = window.setInterval(this.countDown, 1000)
    }
  }

  countDown = () => {
    this.decreaseCounter()
    this.resetCounter()
  }

  decreaseCounter = () => {
    const { timestamp } = this.state

    if (timestamp > 0) {
      this.setState({
        timestamp: timestamp - 1000
      })
    }
  }

  resetCounter = () => {
    const { timestamp } = this.state
    const { fetchProfileData } = this.props

    if (timestamp === 0) {
      this.setState({
        timestamp: 0
      })
      fetchProfileData()
      clearInterval(this.timer)
    }
  }

  formatTime = () => {
    const { timestamp } = this.state
    const formattedTime = timestamp ? formattingTime(timestamp / 1000) : ''
    const separateTime = formattedTime.split('and')
    const formattedTimeMobile = separateTime[0] || ''
    const secondTimeElement = separateTime[1] ? `and ${separateTime[1]}` : ''
    const formattedTimeGeneral = `${separateTime[0]} ${secondTimeElement}`
    let result

    if (window.getBrowserWidth() <= window.minTabletSize) {
      result = formattedTimeMobile
    } else {
      result = formattedTimeGeneral
    }

    return result
  }

  isNPCLoot = () => {
    const {
      userStatus: { trait },
      user
    } = this.props

    return user.role === 'NPC' && trait.loot ? `loot-${trait.loot}` : ''
  }

  renderIcon = () => {
    const {
      userStatus: { status, trait },
      user
    } = this.props
    const iconStatus = getStatusIcon({ ...status, trait, user })
    const { name, type, preset } = iconStatus

    return (
      name && <SVGIconGenerator iconsHolder={profileIconsHolder} iconName={name} type={type || ''} preset={preset} />
    )
  }

  renderStatusContent = () => {
    const {
      userStatus: { status },
      isMiniProfile
    } = this.props
    const statusIcon = this.renderIcon()
    const statusDetails = getStatusDetails(status)
    const styles = `${statusDetails.style} ${this.isNPCLoot()}`

    return (
      <div className={cn('profile-container', isMiniProfile && styles)}>
        {!isMiniProfile && statusIcon}
        <div className='description'>
          <span className='main-desc'>{`${statusDetails.description} ${this.formatTime()}`}</span>
          <span
            className='sub-desc'
            dangerouslySetInnerHTML={status.description ? { __html: status.description } : null}
          />
        </div>
      </div>
    )
  }

  render() {
    const {
      userStatus: { status },
      isMiniProfile
    } = this.props
    const statusDetails = getStatusDetails(status)
    const styles = `${statusDetails.style} ${this.isNPCLoot()}`

    return isMiniProfile ? (
      this.renderStatusContent()
    ) : (
      <div className={`profile-status m-top10 ${styles}`}>
        <ProfileBlock title='Status'>{this.renderStatusContent()}</ProfileBlock>
      </div>
    )
  }
}

export default UserStatus
