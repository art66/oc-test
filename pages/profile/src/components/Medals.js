import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Swiper, Slide } from 'react-dynamic-swiper'
import ProfileBlock from './ProfileBlock'
import { MOBILE_W, TABLET_W, DESKTOP_W, TABLET_SIDEBAR_W, DESKTOP_SIDEBAR_W } from '../constants/browserDimensions.ts'
import 'react-dynamic-swiper/lib/styles.css'

/* global $, initializeAwardsTooltip, */

class Medals extends Component {
  constructor(props) {
    super(props)
    this.state = {
      options: null,
      pagination: false,
      navigation: false
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { options, navigation, pagination } = this.state
    const { medalInformation } = this.props

    return this.compareOptions(options, nextState.options)
      || pagination !== nextState.pagination || navigation !== nextState.navigation
      || JSON.stringify(medalInformation) !== JSON.stringify(nextProps.medalInformation)
  }

  componentDidMount() {
    this.medalsBlockSize = this.getMedalsBlockSize()
    this.updateSettings(this.medalsBlockSize)
    window.addEventListener('resize', this.changeSettings.bind(this))
    initializeAwardsTooltip($('.medals-container .tooltip'))
  }

  componentDidUpdate(prevProps, prevState) {
    const $medal = $('.medals-container .tooltip')

    this.medalsBlockSize = this.getMedalsBlockSize()
    this.updateSettings(this.medalsBlockSize)
    $medal.tooltip('destroy')
    initializeAwardsTooltip($medal)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.changeSettings.bind(this))
  }

  getMedalsBlockSize() {
    return this.refs.medalsBlock && this.refs.medalsBlock.offsetWidth
  }

  compareOptions = (oldOptions, newOptions) => {
    return oldOptions && newOptions && Object.keys(oldOptions).some(key => oldOptions[key] !== newOptions[key])
  }

  updateSettings(medalsBlockSize) {
    let options = {
      slidesPerView: 12,
      slidesPerGroup: 12
    }

    let pagination = false

    let navigation = true

    if (medalsBlockSize === MOBILE_W) {
      options = {
        slidesPerView: 5,
        slidesPerGroup: 5,
        paginationClickable: true
      }
      pagination = true
      navigation = false
    } else if (medalsBlockSize === TABLET_W) {
      options = {
        slidesPerView: 9,
        slidesPerGroup: 9
      }
      pagination = true
      navigation = false
    } else if (medalsBlockSize === DESKTOP_W) {
      options = {
        slidesPerView: 16,
        slidesPerGroup: 16
      }
      pagination = false
      navigation = true
    } else if (medalsBlockSize === TABLET_SIDEBAR_W) {
      options = {
        slidesPerView: 7,
        slidesPerGroup: 7,
        paginationClickable: true
      }
      pagination = true
      navigation = false
    } else if (medalsBlockSize === DESKTOP_SIDEBAR_W) {
      options = {
        slidesPerView: 12,
        slidesPerGroup: 12
      }
      pagination = false
      navigation = true
    }
    options.speed = 650
    this.setState({
      options,
      pagination,
      navigation
    })
  }

  changeSettings() {
    const medalsBlockSize = this.getMedalsBlockSize()

    if (medalsBlockSize !== this.medalsBlockSize) {
      this.medalsBlockSize = medalsBlockSize
      this.updateSettings(this.medalsBlockSize)
    }
  }

  render() {
    const { medalInformation: { medals } } = this.props
    const { options, navigation, pagination } = this.state

    return (
      <div className='profile-wrapper medals-wrapper m-top10' ref='medalsBlock'>
        <ProfileBlock title='Medals'>
          <div className='medals-container bottom-round cont-gray'>
            <Swiper
              swiperOptions={options}
              navigation={navigation}
              pagination={pagination && medals.length > options.slidesPerView}
            >
              {medals.map(medal => {
                return (
                  <Slide key={medal.name}>
                    <i className={medal.class}>
                      <i
                        className='tooltip md-icon' title={medal.name}
                        data-title={medal.description}
                      />
                    </i>
                  </Slide>
                )
              })}
            </Swiper>
          </div>
          <div className='clear' />
        </ProfileBlock>
      </div>
    )
  }
}

Medals.propTypes = {
  medalInformation: PropTypes.object
}

export default Medals
