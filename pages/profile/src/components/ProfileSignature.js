import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ProfileBlock from './ProfileBlock'

class ProfileSignature extends Component {
  render() {
    const { text } = this.props.profileSignature
    return (
      <div className="profile-wrapper m-top10">
        <div className="profile-signature">
          <ProfileBlock title="Profile Signature">
            <div
              className="profile-container profile-signature border-round paragraph unreset"
              dangerouslySetInnerHTML={{ __html: text }}
            />
          </ProfileBlock>
        </div>
      </div>
    )
  }
}

ProfileSignature.propTypes = {
  profileSignature: PropTypes.object
}

export default ProfileSignature
