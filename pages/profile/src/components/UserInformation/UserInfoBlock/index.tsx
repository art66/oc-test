import React, { ReactNode } from 'react'
import cn from 'classnames'

interface IProps {
  customClass?: string,
  label?: string,
  children: ReactNode
}

const UserInfoBlock = (props: IProps) => {
  const { customClass, label, children } = props

  return (
    <div className={cn('box-info', customClass)}>
      {label && <div className='box-name t-gray-9 bold'>{label}</div>}
      <div className='reflection' />
      <div className='block-value'>
        {children}
      </div>
    </div>
  )
}

export default UserInfoBlock
