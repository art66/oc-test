import React from 'react'
import cn from 'classnames'

interface IProps {
  level: number
}

const Level = (props: IProps) => {
  const { level } = props
  const levelReversed = String(level).split('').reverse()

  return (
    <ul className='box-value'>
      <li className='digit-r'>
        <div className='l-ear left' />
        <div className={cn('digit left', !levelReversed[2] && 'zero')}>{levelReversed[2]}</div>
        <div className='clear' />
      </li>
      <li className='digit-m'>
        <div className={cn('digit', !levelReversed[1] && 'zero')}>{levelReversed[1]}</div>
        <div className='clear' />
      </li>
      <li className='digit-l'>
        <div className='digit left'>{levelReversed[0]}</div>
        <div className='r-ear right' />
        <div className='clear' />
      </li>
      <li className='clear' />
    </ul>
  )
}

export default Level
