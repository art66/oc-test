import React, { Component } from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import IUserInformation from '../../interfaces/IUserInformation'
import ProfileBlock from '../ProfileBlock'
import UserInfoBlock from './UserInfoBlock'
import Level from './Level'
import Rank from './Rank'
import Age from './Age'

interface IProps {
  userInformation: IUserInformation
}

class UserInformation extends Component<IProps> {
  getFormattedUserName = () => {
    const { userInformation: { name } } = this.props
    const regExp = new RegExp(/[^a-zA-Z]/g)

    return name.toString().replace(regExp, '')
  }

  render() {
    const { userInformation } = this.props
    const { name, level, rank, age, image, honorTitle } = userInformation
    const formattedName = this.getFormattedUserName()

    return (
      <div className='user-information'>
        <ProfileBlock title='User Information' className='cont-gray'>
          <div className='profile-image m-top10 left'>
            <div className='user name' data-placeholder={name}>
              <Tooltip parent={`user-${formattedName}-honor`}>{`${name} (${honorTitle})`}</Tooltip>
              <img
                id={`user-${formattedName}-honor`}
                src={image.awardsImage}
                alt={`${name} (${honorTitle})`}
              />
            </div>
            <a className='profile-image-wrapper' href={image.uploadedImagesLink}>
              <div className='img-wrap'>
                <img src={image.profileImage} alt='Profile' />
              </div>
            </a>
            <div className='profile-image-information'>
              <a className='t-gray-f2 h' href={image.uploadedImagesLink}>
                {image.amountOfUploadedImages} uploaded images
              </a>
            </div>
          </div>
          <div className='profile-information-wrapper right'>
            <UserInfoBlock label='Level'>
              <Level level={level} />
            </UserInfoBlock>
            <UserInfoBlock customClass='rank' label='Rank'>
              <Rank rank={rank} />
            </UserInfoBlock>
            <UserInfoBlock customClass='age' label='Age'>
              <Age age={age} />
            </UserInfoBlock>
          </div>
          <div className='clear' />
        </ProfileBlock>
      </div>
    )
  }
}

export default UserInformation
