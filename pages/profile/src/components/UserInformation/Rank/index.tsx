import React from 'react'

interface IProps {
  rank: {
    userRank: string,
    userTitle: string
  }
}

const Rank = (props: IProps) => {
  const { rank } = props
  const { userRank, userTitle } = rank

  return (
    <ul className='rank-description box-value'>
      <li className='digit-r '>
        <div className='l-ear left' />
        <div className='digit left'>
          <div className='two-row'>
            <span className={/\s/g.test(userRank) ? 'long' : 'medium'}>{userRank}</span>
            <span className={/\s/g.test(userTitle) ? 'long' : 'medium'}>{userTitle}</span>
          </div>
        </div>
        <div className='r-ear right' />
        <div className='clear' />
      </li>
      <li className='clear' />
    </ul>
  )
}

export default Rank
