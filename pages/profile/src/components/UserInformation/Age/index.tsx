import React from 'react'

interface IProps {
  age: string
}

const Age = (props: IProps) => {
  const { age } = props
  const ageString = String(age).split('').reverse()

  return (
    <ul className='box-value'>
      <li className='digit-r '>
        <div className='l-ear left' />
        <div className='digit left'>{ageString[3]}</div>
        <div className='clear' />
      </li>
      <li className='digit-m'>
        <div className='digit'>{ageString[2]}</div>
        <div className='clear' />
      </li>
      <li className='digit-m'>
        <div className='digit'>{ageString[1]}</div>
        <div className='clear' />
      </li>
      <li className='digit-l'>
        <div className='digit left'>{ageString[0]}</div>
        <div className='r-ear right' />
        <div className='clear' />
      </li>
      <li className='clear' />
    </ul>
  )
}

export default Age
