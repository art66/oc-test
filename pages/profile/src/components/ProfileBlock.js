import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Preloader from './Preloader'

class ProfileBlock extends Component {
  render() {
    const { title, profileDataFetched, children, className } = this.props

    return (
      <div>
        <div className="title-black top-round">{title}</div>
        <div className={`cont bottom-round ${className || ''}`}>{profileDataFetched ? children : <Preloader />}</div>
      </div>
    )
  }
}

ProfileBlock.propTypes = {
  title: PropTypes.string,
  profileDataFetched: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string
}

const mapStateToProps = state => ({
  profileDataFetched: state.profile.profileDataFetched
})

export default connect(mapStateToProps)(ProfileBlock)
