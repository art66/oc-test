import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ProfileBlock from './ProfileBlock'
import BasicInfoIconsList from './BasicInfoIconsList'

class BasicInformation extends Component {
  showProperty() {
    const { basicInformation } = this.props
    const { onlineStatus, property, maritalStatus } = basicInformation
    let propertyText

    if (property.status === 'NPC' || onlineStatus === 'Offline' || property.name === 'Unknown') {
      propertyText = property.name
    } else {
      propertyText = (
        <a className='t-blue' href={property.link}>
          {property.name}
        </a>
      )
    }
    if (property.spouseProperty) {
      return (
        <span>
          {propertyText}&nbsp;(With&nbsp;
          <a className='t-blue' href={maritalStatus.link}>
            Spouse
          </a>
          )
        </span>
      )
    }
    return <span>{propertyText}</span>
  }

  showMaritalStatus() {
    const { basicInformation } = this.props
    const { maritalStatus } = basicInformation
    let text

    if (maritalStatus.status === 'Single') {
      text = 'Single'
    } else {
      let days = maritalStatus.days > 1 || maritalStatus.days === 0 ? 'days' : 'day'

      let spouse = `<a class='t-blue' href=${maritalStatus.link}>${maritalStatus.spouseName}</a>`

      if (maritalStatus.status === 'marriage') {
        text = `Married to ${spouse} for ${maritalStatus.days} ${days}`
      } else if (maritalStatus.status === 'newlywed') {
        text = `Newlywed to ${spouse} for ${maritalStatus.days} ${days}`
      } else if (maritalStatus.status === 'engaged') {
        text = `Engaged to ${spouse}`
      }
    }

    return <span title={text} dangerouslySetInnerHTML={{ __html: text }} />
  }

  showJob() {
    const { basicInformation } = this.props
    const { onlineStatus, job } = basicInformation
    let text

    if (job.companyName) {
      if (onlineStatus === 'Offline') {
        text = `${job.name} of ${job.companyName}`
      } else {
        text = `${job.name} of <a class='t-blue' href=${job.link}>${job.companyName}</a>`
      }
    } else {
      text = job.name
    }

    return <span title={text} dangerouslySetInnerHTML={{ __html: text }} />
  }

  showLastAction() {
    const { basicInformation } = this.props
    const { lastAction } = basicInformation
    let minutes, hours, days

    let lastActionText

    if (lastAction.seconds >= 60) {
      minutes = lastAction.seconds / 60
      minutes = Math.floor(minutes)
    }

    if (minutes >= 60) {
      hours = minutes / 60
      hours = Math.floor(hours)
    }

    if (hours >= 24) {
      days = hours / 24
      days = Math.floor(days)
    }

    if (lastAction.seconds < 120) {
      lastActionText = 'Now'
    } else if (minutes < 60) {
      if (minutes <= 1) {
        lastActionText = `${minutes} minute ago`
      } else {
        lastActionText = `${minutes} minutes ago`
      }
    } else if (hours < 24) {
      if (hours <= 1) {
        lastActionText = `${hours} hour ago`
      } else {
        lastActionText = `${hours} hours ago`
      }
    } else if (days <= 1) {
      lastActionText = `${days} day ago`
    } else {
      lastActionText = `${days} days ago`
    }
    // if (lastAction.seconds <= 0) {
    //   lastActionText = 'No Last Action'
    // }

    if (lastAction.seconds === 'Unknown') {
      lastActionText = 'Unknown'
    }
    return lastActionText
  }

  getFactionInfo = faction => {
    return faction.position ? (
      <span title={`${faction.position} of ${faction.name}`}>
        {`${faction.position} of `}
        <a className='t-blue' href={faction.link}>{faction.name}</a>
      </span>
    ) : (
      <a className='t-blue' href={faction.link}>{faction.name}</a>
    )
  }

  render() {
    const { basicInformation, user } = this.props
    const {
      permission,
      name,
      status,
      faction,
      life,
      awards,
      friends,
      enemies,
      forumPosts,
      icons
    } = basicInformation

    return (
      <div className='basic-information profile-left-wrapper left'>
        <ProfileBlock title='Basic Information'>
          <div className='profile-container basic-info bottom-round'>
            <BasicInfoIconsList icons={icons} userID={user.userID} />
            <ul className='info-table'>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Name</span>
                </div>
                <div className='user-info-value'>
                  <span className={'bold' + (permission ? ' permission' + permission : '')}>
                    {name.playerName} [{name.userID}]
                  </span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Role</span>
                </div>
                <div className='user-info-value'>
                  <span>{status}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Faction</span>
                </div>
                <div className='user-info-value'>
                  <span>{faction.name ? this.getFactionInfo(faction) : 'N/A'}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Job</span>
                </div>
                <div className='user-info-value'>
                  <span>{this.showJob()}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Life</span>
                </div>
                <div className='user-info-value'>
                  <span>
                    {life.state} / {life.total}
                  </span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Property</span>
                </div>
                <div className='user-info-value'>
                  <span>{this.showProperty()}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Marital status</span>
                </div>
                <div className='user-info-value'>
                  <span>{this.showMaritalStatus()}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Awards</span>
                </div>
                <div className='user-info-value'>
                  <span>{awards.amount}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Friends</span>
                </div>
                <div className='user-info-value'>
                  <span>{friends.amount}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Enemies</span>
                </div>
                <div className='user-info-value'>
                  <span>{enemies.amount}</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Forum posts</span>
                </div>
                <div className='user-info-value'>
                  <a className='t-blue' href={forumPosts && forumPosts.link ? forumPosts.link : ''}>
                    {forumPosts && forumPosts.amount ? forumPosts.amount : 0}&nbsp;
                  </a>
                  <span>({forumPosts && forumPosts.karma ? forumPosts.karma : 0} karma)</span>
                </div>
              </li>
              <li>
                <div className='user-information-section'>
                  <span className='bold'>Last action</span>
                </div>
                <div className='user-info-value'>
                  <span>{this.showLastAction()}</span>
                </div>
              </li>
            </ul>
          </div>
        </ProfileBlock>
      </div>
    )
  }
}

BasicInformation.propTypes = {
  basicInformation: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object
}

export default BasicInformation
