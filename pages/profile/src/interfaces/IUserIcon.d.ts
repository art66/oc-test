export default interface IIconStatus {
  id: number
  type: string
  marriedTo?: {
    playername?: string
  },
  title?: string,
  description?: string,
  timestamp?: number,
  period?: number,
  url?: string
}
