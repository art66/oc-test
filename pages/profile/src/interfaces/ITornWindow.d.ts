export default interface ITornWindow extends Window {
  getBrowserWidth: () => number
  minTabletSize: number
  setInterval: (func: () => void, t: number) => number
  getCurrentTimestamp: () => number
}
