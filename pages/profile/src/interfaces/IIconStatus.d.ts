export default interface IIconStatus {
  name?: string
  type?: number
  preset?: {
    type: string
    subtype: string
  }
}
