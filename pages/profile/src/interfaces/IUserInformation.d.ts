interface IUserInformationImage {
  amountOfUploadedImages: number,
  awardsImage: string,
  profileImage: string,
  uploadedImagesLink: string
}

interface IUserInformation {
  image: IUserInformationImage,
  rank: {
    userRank: string,
    userTitle: string
  },
  name: string,
  honorTitle: string,
  level: number,
  age: string,
  role: string,
}

export default IUserInformation
