type TButtonState = 'active' | 'hidden' | 'disabled'

interface IProfileButton {
  actionDescription: string,
  link: string,
  message: string | null,
  state: TButtonState
}

 export default IProfileButton
