export default interface IUser {
  playerName?: string
  userID: string | number
  lastAction?: { seconds: number | 'Unknown' },
  role: string
  displayPersonalDetails?: boolean
  isFriend: boolean
  isEnemy: boolean
  signUp: number
}
