interface IName {
  simpleName: string,
  relatedName: string
}

interface IStatus {
  type: string,
  description?: string,
  in?: IName,
  from?: IName,
  to?: IName,
  timestamp?: number,
  period?: number,
  flightType?: string
}

interface ITrait {
  loot: null | number
}

interface IUserStatus {
  status: IStatus,
  trait: ITrait
}

export default IUserStatus
