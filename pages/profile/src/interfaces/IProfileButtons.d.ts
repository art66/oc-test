import IProfileButton from './IProfileButton'

interface IDialog {
  text?: string,
  color?: string,
  confirmAction?: string
}

interface IActionDialog {
  show?: boolean,
  actionType?: string
}

interface IProfileButtons {
  buttons: {
    addToEnemyList: IProfileButton,
    addToFriendList: IProfileButton,
    attack: IProfileButton,
    bail: IProfileButton,
    bust: IProfileButton,
    initiateChat: IProfileButton,
    initiateTrade: IProfileButton,
    personalStats: IProfileButton,
    placeBounty: IProfileButton,
    report: IProfileButton,
    revive: IProfileButton,
    sendMessage: IProfileButton,
    sendMoney: IProfileButton,
    viewBazaar: IProfileButton,
    viewDisplayCabinet: IProfileButton
  },
  dialog?: IDialog,
  isVisibleCashDialog?: boolean,
  moneyAmount?: string,
  moneyDesc?: string,
  anonymous?: boolean,
  addUserDialog?: IActionDialog
}

export default IProfileButtons
