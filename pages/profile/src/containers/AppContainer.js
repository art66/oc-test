import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import CoreLayout from '../layouts/CoreLayout'
import '../styles/style.scss'

class AppContainer extends Component {
  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <div style={{ height: '100%' }}>
          <CoreLayout />
        </div>
      </Provider>
    )
  }
}

AppContainer.propTypes = {
  store: PropTypes.object.isRequired
}

export default AppContainer
