/* global getAction, refreshTopOfSidebar */

// ------------------------------------
// Constants
// ------------------------------------
const GET_AJAX_ERROR = 'GET_AJAX_ERROR'
const SHOW_DIALOG = 'SHOW_DIALOG'
const HIDE_DIALOG = 'HIDE_DIALOG'
const SET_DIALOG_RESULT = 'SET_DIALOG_RESULT'
const SET_BUTTON_STATE = 'SET_BUTTON_STATE'
const SET_BUTTONS_STATE = 'SET_BUTTONS_STATE'
const SHOW_SEND_CASH_DIALOG = 'SHOW_SEND_CASH_DIALOG'
const HIDE_SEND_CASH_DIALOG = 'HIDE_SEND_CASH_DIALOG'
const SHOW_BUTTON_MSG = 'SHOW_BUTTON_MSG'
const HIDE_BUTTON_MSG = 'HIDE_BUTTON_MSG'
const SET_MONEY_DATA = 'SET_MONEY_DATA'
const TOGGLE_ADD_USER_DIALOG = 'TOGGLE_ADD_USER_DIALOG'

// ------------------------------------
// Middlewares
// ------------------------------------

export const getAjaxError = error => {
  console.error(error)
  return {
    type: GET_AJAX_ERROR,
    error
  }
}

export const updateButtons = () => (dispatch, getState) => {
  return getAction({
    type: 'get',
    action: `/profiles.php?step=getButtonsState`,
    success: resp => {
      let data = JSON.parse(resp)
      dispatch(setButtonsState({ buttons: data.buttons }))
    }
  })
}

export const addToFriendList = reason => (dispatch, getState) => {
  let user = getState().profile.user
  let step, userID, ID

  if (user.isFriend) {
    step = 'removeFromFriends'
    userID = user.userID
  } else {
    step = 'addToFriends'
    ID = user.userID
  }

  return new Promise(resolve => {
    getAction({
      type: 'post',
      action: '/userlist.php',
      data: {
        step: step,
        ID: ID,
        userID: userID,
        about: reason
      },
      success: resp => {
        let data = JSON.parse(resp)

        if (data.success) {
          let state = user.isFriend ? 'active' : 'green active'
          dispatch(setButtonState('addToFriendList', state))
          resolve()
        }
      }
    })
  })
}

export const addToEnemyList = reason => (dispatch, getState) => {
  let user = getState().profile.user
  let step, userID, ID

  if (user.isEnemy) {
    step = 'removeFromEnemy'
    userID = user.userID
  } else {
    step = 'addToEnemy'
    ID = user.userID
  }

  return new Promise(resolve => {
    getAction({
      type: 'post',
      action: `/userlist.php`,
      data: {
        step: step,
        ID: ID,
        userID: userID,
        about: reason
      },
      success: resp => {
        const data = JSON.parse(resp)

        if (data.success) {
          const state = user.isEnemy ? 'active' : 'red active'

          dispatch(setButtonState('addToEnemyList', state))
          resolve()
        }
      }
    })
  })
}

export const bust = () => (dispatch, getState) => {
  let userID = getState().profile.user.userID

  getAction({
    type: 'get',
    action: `/jailview.php?XID=${userID}&action=rescue&step=breakout1`,
    success: resp => {
      let data = JSON.parse(resp)
      dispatch(setDialogResult({ text: data.msg, color: data.color }))
      refreshTopOfSidebar()
    }
  })
}

export const bail = () => (dispatch, getState) => {
  let userID = getState().profile.user.userID

  getAction({
    type: 'get',
    action: `/jailview.php?XID=${userID}&action=rescue&step=buy1`,
    success: resp => {
      let data = JSON.parse(resp)
      dispatch(setDialogResult({ text: data.msg, color: data.color }))
      refreshTopOfSidebar()
    }
  })
}

export const revive = () => (dispatch, getState) => {
  let userID = getState().profile.user.userID

  getAction({
    type: 'get',
    action: `/revive.php?action=revive&step=revive&ID=${userID}`,
    success: resp => {
      let data = JSON.parse(resp)
      dispatch(setDialogResult({ text: data.msg, color: data.color }))
      refreshTopOfSidebar()
    }
  })
}

export const sendMoney = () => (dispatch, getState) => {
  let state = getState().profile
  let userID = state.user.userID
  let amount = state.profileButtons.moneyAmount.replace(/,/g, '')
  let desc = state.profileButtons.moneyDesc
  let anonymous = state.profileButtons.anonymous

  getAction({
    type: 'post',
    action: '/sendcash.php',
    data: {
      step: 'cash1',
      ID: userID,
      money: amount,
      tag: desc,
      theanon: anonymous
    },
    success: resp => {
      let data = JSON.parse(resp)
      dispatch(
        setDialogResult({
          text: data.text,
          color: data.success ? 'green' : 'red'
        })
      )
      refreshTopOfSidebar()
    }
  })
}

export const confirmDialog = () => (dispatch, getState) => {
  const dialog = getState().profile.profileButtons.dialog

  if (!dialog) {
    return
  }

  const confirmActions = {
    bust: bust,
    bail: bail,
    revive: revive,
    sendMoney: sendMoney
  }

  let confirmAction = confirmActions[dialog.confirmAction]
  dispatch(confirmAction ? confirmAction() : hideDialog())
}

// ------------------------------------
// Actions
// ------------------------------------
export const setButtonState = (buttonKey, state) => ({
  type: SET_BUTTON_STATE,
  buttonKey,
  state
})

export const setButtonsState = buttons => ({
  type: SET_BUTTONS_STATE,
  buttons
})

export const showSendCashDialog = () => ({
  type: SHOW_SEND_CASH_DIALOG
})

export const toggleAddUserDialog = (show, actionType) => ({
  type: TOGGLE_ADD_USER_DIALOG,
  show,
  actionType
})

export const showDialog = (text, confirmAction, color = '') => ({
  type: SHOW_DIALOG,
  text,
  confirmAction,
  color
})

export const hideDialog = () => ({
  type: HIDE_DIALOG
})

export const setDialogResult = result => ({
  type: SET_DIALOG_RESULT,
  result
})

export const showButtonMsg = msg => ({
  type: SHOW_BUTTON_MSG,
  msg
})

export const hideButtonMsg = () => ({
  type: HIDE_BUTTON_MSG
})

export const setMoneyData = (moneyAmount, moneyDesc, anonymous) => ({
  type: SET_MONEY_DATA,
  moneyAmount,
  moneyDesc,
  anonymous
})

// ------------------------------------
// Action Handlers
// ------------------------------------
export const ACTION_HANDLERS = {
  [SET_BUTTON_STATE]: (state, action) => {
    return {
      ...state,
      buttons: {
        ...state.buttons,
        [action.buttonKey]: {
          ...state.buttons[action.buttonKey],
          state: action.state
        }
      }
    }
  },
  [SET_BUTTONS_STATE]: (state, action) => {
    return {
      ...state,
      buttons: action.buttons
    }
  },
  [SHOW_SEND_CASH_DIALOG]: (state, action) => {
    return {
      ...state,
      isVisibleCashDialog: true
    }
  },
  [HIDE_SEND_CASH_DIALOG]: (state, action) => {
    return {
      ...state,
      isVisibleCashDialog: false
    }
  },
  [TOGGLE_ADD_USER_DIALOG]: (state, action) => {
    return {
      ...state,
      addUserDialog: {
        ...state.addUserDialog,
        show: action.show,
        actionType: action.actionType
      }
    }
  },
  [SHOW_DIALOG]: (state, action) => {
    return {
      ...state,
      isVisibleCashDialog: false,
      dialog: {
        text: action.text,
        color: action.color,
        confirmAction: action.confirmAction
      }
    }
  },
  [HIDE_DIALOG]: (state, action) => {
    return {
      ...state,
      dialog: {},
      isVisibleCashDialog: false
    }
  },
  [SET_DIALOG_RESULT]: (state, action) => {
    return {
      ...state,
      isVisibleCashDialog: false,
      dialog: {
        ...state.dialog,
        text: action.result.text,
        color: action.result.color || state.dialog.color,
        confirmed: true
      }
    }
  },
  [SHOW_BUTTON_MSG]: (state, action) => {
    return {
      ...state,
      activeButtonMsg: action.msg
    }
  },
  [HIDE_BUTTON_MSG]: (state, action) => {
    return {
      ...state,
      activeButtonMsg: ''
    }
  },
  [SET_MONEY_DATA]: (state, action) => {
    return {
      ...state,
      moneyAmount: action.moneyAmount,
      moneyDesc: action.moneyDesc,
      anonymous: action.anonymous
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  buttons: {
    attack: {},
    message: {},
    sendMoney: {},
    trade: {},
    placeBounty: {},
    report: {},
    addToFriends: {},
    addToBlackList: {},
    viewStats: {},
    viewBazaar: {},
    viewDisplayCase: {},
    bustFromJail: {},
    revive: {}
  },
  dialog: {
    text: '',
    color: 'green',
    confirmAction: () => {},
    visible: false
  },
  isVisibleCashDialog: false
}

export default function profileButtonsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
