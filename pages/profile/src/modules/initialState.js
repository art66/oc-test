export default {
  profileDataFetched: false,
  profileButtons: {
    buttons: {}
  },
  user: {},
  currentUser: {},
  userInformation: {
    image: {},
    rank: {},
    name: {
      playerName: 'ivanu',
      userID: 2055008
    },
    status: 'Admin',
    faction: {
      name: 'Faction name',
      link: '/factions.php?step=profile&ID=29109'
    },
    job: {
      name: 'Director',
      companyName: 'Fitness center',
      link: '/joblist.php#/p=corpinfo&ID=61475'
    },
    life: {
      state: 100,
      total: 250
    },
    property: {
      name: 'Island',
      link: '/properties.php#/p=propertyinfo&ID=1102023&userID=2055008',
      spouseProperty: 'true'
    },
    maritalStatus: {
      status: 'marriage',
      spouseName: 'Artem',
      link: '/profiles.php?XID=1784707',
      days: 430
    },
    awards: {
      amount: 10
    },
    friends: {
      amount: 11
    },
    enemies: {
      amount: 1
    },
    forumPosts: {
      amount: 1,
      karma: 1,
      link: '/forums.php#!p=search&=1707527&f=0&y=0'
    },
    lastAction: {
      seconds: 20
    }
  },
  basicInformation: {
    icons: [],
    permission: 1,
    onlineStatus: 'Online',
    name: {
      playerName: 'ivanu',
      userID: 2055008
    },
    status: 'Admin',
    faction: {
      name: 'Faction name',
      link: '/factions.php?step=profile&ID=29109'
    },
    job: {
      name: 'Director',
      companyName: 'Fitness center',
      link: '/joblist.php#/p=corpinfo&ID=61475'
    },
    life: {
      state: 100,
      total: 250
    },
    property: {
      name: 'Island',
      link: '/properties.php#/p=propertyinfo&ID=1102023&userID=2055008',
      spouseProperty: 'true'
    },
    maritalStatus: {
      status: 'marriage',
      spouseName: 'Artem',
      link: '/profiles.php?XID=1784707',
      days: 430
    },
    awards: {
      amount: 10
    },
    friends: {
      amount: 11
    },
    enemies: {
      amount: 1
    },
    forumPosts: {
      amount: 1,
      karma: 1,
      link: '/forums.php#!p=search&=1707527&f=0&y=0'
    },
    lastAction: {
      seconds: 20
    }
  },
  personalInformation: {},
  medalInformation: {},
  competitionStatus: {},
  userStatus: {
    status: {
      type: 'ok',
      timestamp: 0
    },
    trail: {
      loot: null
    }
  }
}
