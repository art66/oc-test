import initialState from './initialState'
/* global getAction, getUrlParam, WebsocketHandler */

import profileButtons, { ACTION_HANDLERS as buttonsActionHandlers } from './profileButtons'
import {updateProfileData} from "@torn/profile-mini/src/actions";
// ------------------------------------
// Constants
// ------------------------------------
const SIX_HOURS = 21600
const POLL_INTERVAL = 30 * 1000

export const GET_AJAX_ERROR = 'GET_AJAX_ERROR'
export const UPDATE_BUTTONS = 'UPDATE_BUTTONS'
export const UPDATE_USER_STATUS = 'UPDATE_USER_STATUS'
export const SET_PROFILE_DATA = 'SET_PROFILE_DATA'

// ------------------------------------
// Middlewares
// ------------------------------------

export const getAjaxError = error => {
  console.error(error)
  return {
    type: GET_AJAX_ERROR,
    error
  }
}

export const updateButtons = () => ({
  type: UPDATE_BUTTONS
})

export const updateUserStatus = data => ({
  type: UPDATE_USER_STATUS,
  payload: data
})

const setProfileData = data => ({
  type: SET_PROFILE_DATA,
  data
})

let updateTimeout

export const fetchProfileData = () => (dispatch, getState) => {
  let userID = getUrlParam('XID')
  let userParam = userID ? `XID=${userID}` : `NID=${getUrlParam('NID')}`

  getAction({
    type: 'get',
    action: `/profiles.php?step=getProfileData&${userParam}`,
    success: resp => {
      let data = JSON.parse(resp)
      dispatch(setProfileData(data))
      clearTimeout(updateTimeout)
      window.sidebarIconsLiveTime = 0

      let timeToUpdate = data.profileButtons.timeToUpdate

      if (timeToUpdate && timeToUpdate < SIX_HOURS) {
        updateTimeout = setTimeout(() => {
          dispatch(fetchProfileData())
        }, data.profileButtons.timeToUpdate * 1000)
      }
    }
  })
}

let pollInterval
export const setPollForUpdate = () => dispatch => {
  clearInterval(pollInterval)
  pollInterval = setInterval(() => {
    dispatch(fetchProfileData())
  }, POLL_INTERVAL)
}

const updateProfile = () => dispatch => {
  dispatch(fetchProfileData())
}

export const subscribeForProfileChanges = () => () => {
  WebsocketHandler.addEventListener('sidebar', 'onHospital', updateProfile)
  WebsocketHandler.addEventListener('sidebar', 'onRevive', updateProfile)
  WebsocketHandler.addEventListener('sidebar', 'onJail', updateProfile)
  WebsocketHandler.addEventListener('sidebar', 'onLeaveFromJail', updateProfile)
  WebsocketHandler.addEventListener('sidebar', 'updateMoney', updateProfile)
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [UPDATE_BUTTONS]: (state, action) => {
    return {
      ...state,
      profileButtons: profileButtons(state.profileButtons, action)
    }
  },
  [UPDATE_USER_STATUS]: (state, action) => {
    return {
      ...state,
      userStatus: action.payload
    }
  },
  [SET_PROFILE_DATA]: (state, action) => {
    return {
      ...state,
      ...action.data,
      profileDataFetched: true,
      profileButtons: {
        ...action.data.profileButtons,
        addUserDialog: state.profileButtons.addUserDialog,
        isVisibleCashDialog: state.profileButtons.isVisibleCashDialog,
        dialog: state.profileButtons.dialog,
        moneyAmount: state.profileButtons.moneyAmount,
        moneyDesc: state.profileButtons.moneyDesc,
        anonymous: state.profileButtons.anonymous
      }
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------

export default function profileReducer(state = initialState, action) {
  let handler = ACTION_HANDLERS[action.type]

  if (!handler && buttonsActionHandlers[action.type]) {
    return {
      ...state,
      profileButtons: profileButtons(state.profileButtons, action)
    }
  }

  return handler ? handler(state, action) : state
}
