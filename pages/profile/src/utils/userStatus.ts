import {
  ABROAD,
  ABROAD_FEDERAL_JAIL,
  ABROAD_FEDERAL_JAIL_PERMANENTLY,
  ABROAD_FEDERAL_JAIL_TEMPORARILY,
  ABROAD_HOSPITAL,
  ABROAD_JAIL,
  DEAD,
  FEDERAL_JAIL,
  FEDERAL_JAIL_PERMANENTLY,
  FEDERAL_JAIL_TEMPORARILY,
  GOOD,
  HIDDEN_TRAVELING,
  HOSPITAL,
  JAIL,
  TRAVELING_FROM,
  TRAVELING_TO,
  HOSPITAL_TRAVELING_FROM,
  HOSPITAL_TRAVELING_TO,
  JAIL_TRAVELING_FROM,
  JAIL_TRAVELING_TO,
  JAIL_DESCRIPTION,
  JAIL_STYLES,
  HOSPITAL_DESCRIPTION,
  HOSPITAL_STYLES,
  FEDERAL_JAIL_PERMANENTLY_DESCRIPTION,
  FEDERAL_JAIL_TEMPORARILY_DESCRIPTION,
  ABROAD_FEDERAL_JAIL_STYLES,
  FEDERAL_JAIL_STYLES,
  HIDDEN_TRAVELING_DESCRIPTION,
  HIDDEN_TRAVELING_STYLES,
  GOOD_DESCRIPTION,
  GOOD_STYLES,
  DEAD_STYLES,
  DEFAULT_DETAILS,
  HOSPITAL_SVG,
  FEDERAL_JAIL_SVG,
  JAIL_SVG,
  TRAVELING_SVG,
  LOOT_ICON,
  PRESET_TYPE,
  DEFAULT_SVG,
  AWOKEN,
  DORMANT,
  ABROAD_AWOKEN,
  ABROAD_DORMANT
} from '../constants/userStatus'
import { addPlural } from './index'

export const getJailStatusDetails = status => {
  let statusDetails

  switch (status.type) {
    case JAIL: {
      statusDetails = {
        description: JAIL_DESCRIPTION,
        style: JAIL_STYLES
      }
      break
    }
    case ABROAD_JAIL: {
      statusDetails = {
        description: `In ${status.in.relatedName} jail for`,
        style: `abroad jail ${status.in.simpleName.toLowerCase()}`
      }
      break
    }
    case JAIL_TRAVELING_FROM: {
      statusDetails = {
        description: `Returning to ${status.to.simpleName} from ${status.from.simpleName}`,
        style: `jail travelling from ${status.from.simpleName.toLowerCase()} ${status.flightType}`
      }
      break
    }
    case JAIL_TRAVELING_TO: {
      statusDetails = {
        description: `Traveling to ${status.to.simpleName}`,
        style: `jail travelling to ${status.to.simpleName.toLowerCase()} ${status.flightType}`
      }
      break
    }
    default: {
      break
    }
  }

  return statusDetails
}

export const getHospitalStatusDetails = status => {
  let statusDetails

  switch (status.type) {
    case HOSPITAL: {
      statusDetails = {
        description: HOSPITAL_DESCRIPTION,
        style: HOSPITAL_STYLES
      }
      break
    }
    case ABROAD_HOSPITAL: {
      statusDetails = {
        description: `In ${status.in.relatedName} hospital for`,
        style: `abroad hospital ${status.in.simpleName.toLowerCase()}`
      }
      break
    }
    case HOSPITAL_TRAVELING_FROM: {
      statusDetails = {
        description: `Returning to ${status.to.simpleName} from ${status.from.simpleName}`,
        style: `hospital travelling from ${status.from.simpleName.toLowerCase()} ${status.flightType}`
      }
      break
    }
    case HOSPITAL_TRAVELING_TO: {
      statusDetails = {
        description: `Traveling to ${status.to.simpleName}`,
        style: `hospital travelling to ${status.to.simpleName.toLowerCase()} ${status.flightType}`
      }
      break
    }
    default: {
      break
    }
  }

  return statusDetails
}

export const getFederalJailStatusDetails = status => {
  let statusDetails

  switch (status.type) {
    case FEDERAL_JAIL: {
      statusDetails = {
        description: `In federal jail for ${status.period} day${addPlural(status.period)}`,
        style: 'federal-jail'
      }
      break
    }
    case FEDERAL_JAIL_TEMPORARILY: {
      statusDetails = {
        description: FEDERAL_JAIL_TEMPORARILY_DESCRIPTION,
        style: FEDERAL_JAIL_STYLES
      }
      break
    }
    case FEDERAL_JAIL_PERMANENTLY: {
      statusDetails = {
        description: FEDERAL_JAIL_PERMANENTLY_DESCRIPTION,
        style: FEDERAL_JAIL_STYLES
      }
      break
    }
    default: {
      break
    }
  }

  return statusDetails
}

export const getAbroadFederalJailStatusDetails = status => {
  let statusDetails

  switch (status.type) {
    case ABROAD_FEDERAL_JAIL: {
      statusDetails = {
        description: `In ${status.in} federal jail`,
        style: ABROAD_FEDERAL_JAIL_STYLES
      }
      break
    }
    case ABROAD_FEDERAL_JAIL_TEMPORARILY: {
      statusDetails = {
        description: FEDERAL_JAIL_TEMPORARILY_DESCRIPTION,
        style: ABROAD_FEDERAL_JAIL_STYLES
      }
      break
    }
    case ABROAD_FEDERAL_JAIL_PERMANENTLY: {
      statusDetails = {
        description: FEDERAL_JAIL_PERMANENTLY_DESCRIPTION,
        style: ABROAD_FEDERAL_JAIL_STYLES
      }
      break
    }
    default: {
      break
    }
  }

  return statusDetails
}

export const getTravelingStatusDetails = status => {
  let statusDetails

  switch (status.type) {
    case HIDDEN_TRAVELING: {
      statusDetails = {
        description: HIDDEN_TRAVELING_DESCRIPTION,
        style: HIDDEN_TRAVELING_STYLES
      }
      break
    }
    case TRAVELING_TO: {
      statusDetails = {
        description: `Traveling to ${status.to.simpleName}`,
        style: `travelling to ${status.to.simpleName.toLowerCase()} ${status.flightType}`
      }
      break
    }
    case TRAVELING_FROM: {
      statusDetails = {
        description: `Returning to ${status.to.simpleName} from ${status.from.simpleName}`,
        style: `travelling from ${status.from.simpleName.toLowerCase()} ${status.flightType}`
      }
      break
    }
    default: {
      break
    }
  }

  return statusDetails
}

export const getGeneralsStatusDetails = status => {
  let statusDetails

  switch (status.type) {
    case GOOD: {
      statusDetails = {
        description: GOOD_DESCRIPTION,
        style: GOOD_STYLES
      }
      break
    }
    case DEAD: {
      statusDetails = {
        description: '',
        style: DEAD_STYLES
      }
      break
    }
    case ABROAD: {
      statusDetails = {
        description: `In ${status.in.simpleName}`,
        style: `abroad ${status.in.simpleName.toLowerCase()}`
      }
      break
    }
    default: {
      break
    }
  }

  return statusDetails
}

export const getMaolStatusDetails = status => {
  let statusDetails

  switch (status.type) {
    case AWOKEN: {
      statusDetails = {
        description: 'Awoken',
        style: `awoken${status.image}`
      }
      break
    }
    case DORMANT: {
      statusDetails = {
        description: 'Dormant',
        style: `dormant${status.image}`
      }
      break
    }
    case ABROAD_AWOKEN: {
      statusDetails = {
        description: `Awoken in ${status.in.simpleName}`,
        style: `awoken${status.image}`
      }
      break
    }
    case ABROAD_DORMANT: {
      statusDetails = {
        description: `Dormant in ${status.in.simpleName}`,
        style: `dormant${status.image}`
      }
      break
    }
    default: {
      break
    }
  }

  return statusDetails
}

const CALLBACKS_DETAILS = [
  getGeneralsStatusDetails,
  getJailStatusDetails,
  getHospitalStatusDetails,
  getFederalJailStatusDetails,
  getAbroadFederalJailStatusDetails,
  getTravelingStatusDetails,
  getMaolStatusDetails
]

export const getStatusDetails = status => {
  let details = DEFAULT_DETAILS

  CALLBACKS_DETAILS.find(cb => {
    details = cb(status)
    return details
  })

  return details
}

const getHospitalStatusIcon = status => {
  let icon

  switch (status.type) {
    case HOSPITAL:
    case HOSPITAL_TRAVELING_TO:
    case HOSPITAL_TRAVELING_FROM:
    case ABROAD_HOSPITAL: {
      icon = HOSPITAL_SVG
      break
    }

    default: {
      icon = DEFAULT_SVG
      break
    }
  }

  return icon
}

const getJailStatusIcon = status => {
  let icon

  switch (status.type) {
    case JAIL:
    case JAIL_TRAVELING_TO:
    case JAIL_TRAVELING_FROM:
    case ABROAD_JAIL: {
      icon = JAIL_SVG
      break
    }

    default: {
      break
    }
  }

  return icon
}

const getFederalJailStatusIcon = status => {
  let icon

  switch (status.type) {
    case FEDERAL_JAIL:
    case ABROAD_FEDERAL_JAIL:
    case FEDERAL_JAIL_PERMANENTLY:
    case ABROAD_FEDERAL_JAIL_PERMANENTLY:
    case FEDERAL_JAIL_TEMPORARILY:
    case ABROAD_FEDERAL_JAIL_TEMPORARILY: {
      icon = FEDERAL_JAIL_SVG
      break
    }

    default: {
      break
    }
  }

  return icon
}

const getTravelingStatusIcon = status => {
  let icon

  switch (status.type) {
    case TRAVELING_FROM:
    case TRAVELING_TO:
    case HIDDEN_TRAVELING: {
      icon = TRAVELING_SVG
      break
    }

    default: {
      break
    }
  }

  return icon
}

const getOkStatusIcon = status => {
  let icon

  switch (status.type) {
    case GOOD: {
      icon =
        status.user.role === 'NPC' && status.trait.loot ?
          {
            name: LOOT_ICON,
            type: status.trait.loot,
            preset: {
              type: PRESET_TYPE,
              subtype: 'LOOT_STATUS'
            }
          } :
          DEFAULT_SVG
      break
    }

    default: {
      break
    }
  }

  return icon
}

const getMaolStatusIcon = () => {
  return null
}

const CALLBACKS_ICONS = [
  getOkStatusIcon,
  getTravelingStatusIcon,
  getFederalJailStatusIcon,
  getJailStatusIcon,
  getHospitalStatusIcon,
  getMaolStatusIcon
]

export const getStatusIcon = status => {
  let icon = DEFAULT_SVG

  CALLBACKS_ICONS.find(cb => {
    icon = cb(status)

    return icon
  })

  return icon
}
