import { MAIN_URL, AMOUNT_SECONDS_IN_DAY, AMOUNT_SECONDS_IN_MINUTE, AMOUNT_SECONDS } from '../constants'
// require('es6-promise').polyfill()
// require('isomorphic-fetch')
/* global addRFC */

export function getColor(color) {
  return '#' + color
}

export function fetchUrl(url, data) {
  return fetch(addRFC(MAIN_URL + url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      try {
        var json = JSON.parse(text)
        if (json && json.error) {
          error = json.error
        }
      } catch (e) {
        error = 'Server responded with: ' + text
      }

      if (error) {
        throw new Error(error)
      } else {
        return json
      }
    })
  })
}

export function isInt(value) {
  const x = parseFloat(value)

  return !isNaN(value) && (x | 0) === x
}

export const addLeadZero = num => (num < 10 ? `0${num}` : num)

export const addPlural = num => (num === 1 ? '' : 's')

export const addAnd = num => (num ? ' and' : '')

export const secondsToTime = timeStamp => {
  const days = Math.floor(timeStamp / AMOUNT_SECONDS_IN_DAY)
  const divisorForHours = timeStamp % AMOUNT_SECONDS_IN_DAY
  const hours = Math.floor(divisorForHours / AMOUNT_SECONDS_IN_MINUTE)
  const divisorForMinutes = timeStamp % AMOUNT_SECONDS_IN_MINUTE
  const minutes = Math.floor(divisorForMinutes / AMOUNT_SECONDS)
  const divisorForSeconds = divisorForMinutes % AMOUNT_SECONDS
  const seconds = Math.ceil(divisorForSeconds)

  return {
    days,
    hours,
    minutes,
    seconds
  }
}

export const formattingTime = time => {
  const { days, hours, minutes, seconds } = secondsToTime(time)

  let result = ''

  result += days > 0 ? `${days} day${addPlural(days)}` : ''
  result += hours > 0 ? `${addAnd(days)} ${hours} hour${addPlural(hours)}` : ''
  result += minutes > 0 ?
    `${addAnd(hours || minutes ? hours : minutes)} ${minutes} minute${addPlural(minutes)}` :
    ''

  result += seconds > 0 ?
    `${addAnd(minutes || hours ? seconds : minutes)} ${addLeadZero(seconds)} second${addPlural(seconds)}` :
    ''

  return result
}
