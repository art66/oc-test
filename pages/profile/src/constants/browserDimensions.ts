// sizes without sidebar (travel)
export const MOBILE_W = 320
export const TABLET_W = 578
export const DESKTOP_W = 976
// sizes with sidebar
export const TABLET_SIDEBAR_W = 386
export const DESKTOP_SIDEBAR_W = 784
