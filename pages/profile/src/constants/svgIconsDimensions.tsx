export const svgIconsDimensions = (svgIconName: string) => {
  const dimensionsByName = {
    default: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '0 0 46 46'
      }
    },
    Attack: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '101.6 178 46 46'
      }
    },
    Bail: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '802.6 177 46 46'
      }
    },
    CrossDisabled: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '551.393 356 44 44'
      }
    },
    SendMessage: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '155 178 46 46'
      }
    },
    AddToEnemyList: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '532.6 178 46 46'
      }
    },
    AddToFriendList: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '478.6 178 46 46'
      }
    },
    Bust: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '748.6 177 46 46'
      }
    },
    InitiateChat: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '209.6 178 46 46'
      }
    },
    PersonalStats: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '585.6 178 46 46'
      }
    },
    PlaceBounty: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '370.6 178 46 46'
      }
    },
    Report: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '424.6 178 46 46'
      }
    },
    Revive: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '857.6 177 46 46'
      }
    },
    SendMoney: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '262.6 178 46 46'
      }
    },
    ViewBazaar: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '640.6 178 46 46'
      }
    },
    ViewDisplayCabinet: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '694.6 178 46 46'
      }
    },
    InitiateTrade: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '317.6 177 46 46'
      }
    },
    Faction: {
      dimensions: {
        width: 12.47,
        height: 17,
        viewbox: '0 1 11.47 16'
      }
    },
    Forums: {
      dimensions: {
        width: 16,
        height: 14.67,
        viewbox: '0 1 16 14.67'
      }
    },
    Friends: {
      dimensions: {
        width: 21,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    Gym: {
      dimensions: {
        width: 46,
        height: 10,
        viewbox: '7 13 46 10'
      }
    },
    HallOfFame: {
      dimensions: {
        width: 16,
        height: 14.67,
        viewbox: '0 1 16 14.67'
      }
    },
    HalloweenWitch: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '0 1 46 46'
      }
    },
    HalloweenGrave: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '3 1 16 46'
      }
    },
    HalloweenPumpkin: {
      dimensions: {
        width: 16,
        height: 46,
        viewbox: '0 1 16 46'
      }
    },
    Home: {
      dimensions: {
        width: 17,
        height: 14,
        viewbox: '1 1 46 14'
      }
    },
    Hospital: {
      dimensions: {
        width: 16,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    Items: {
      dimensions: {
        width: 14.67,
        height: 16,
        viewbox: '0 1 14.67 16'
      }
    },
    Jail: {
      dimensions: {
        width: 17,
        height: 17,
        viewbox: '0 1 17 17'
      }
    },
    Job: {
      dimensions: {
        width: 16,
        height: 13.33,
        viewbox: '0 1 16 13.33'
      }
    },
    Mailbox: {
      dimensions: {
        width: 46,
        height: 13,
        viewbox: '0 1 46 13'
      }
    },
    Missions: {
      dimensions: {
        width: 46,
        height: 16.5,
        viewbox: '0 .5 16 14.5'
      }
    },
    MrMissTorn: {
      dimensions: {
        width: 17,
        height: 14,
        viewbox: '1.5 0 46 46'
      }
    },
    Newspaper: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '0 1 46 46'
      }
    },
    Property: {
      dimensions: {
        width: 16,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    RecruitCitizens: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '0 1 14 19'
      }
    },
    Rules: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '0 1 46 46'
      }
    },
    Staff: {
      dimensions: {
        width: 16,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    FollowLinks: {
      dimensions: {
        width: 34,
        height: 34,
        viewbox: '0 0 34 34'
      }
    },
    Star: {
      dimensions: {
        width: 34,
        height: 34,
        viewbox: '-2.3 -2.1 9 9'
      }
    },
    Ticket: {
      dimensions: {
        width: 46,
        height: 46,
        viewbox: '8 8 46 46'
      }
    },
    MrMsTorn: {
      dimensions: {
        width: 17,
        height: 14,
        viewbox: '0 0 17 14'
      }
    },
    EasterEgg: {
      dimensions: {
        width: 13,
        height: 17,
        viewbox: '0 2 13 13'
      }
    }
  }

  return dimensionsByName[svgIconName] ? dimensionsByName[svgIconName] : dimensionsByName.default
}
