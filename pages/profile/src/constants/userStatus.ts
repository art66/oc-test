// USER STATUSES
export const GOOD = 'ok'
export const ABROAD = 'abroad'
export const HOSPITAL = 'hospital'
export const DEAD = 'dead'
export const ABROAD_HOSPITAL = 'abroad-hospital'
export const JAIL = 'jail'
export const ABROAD_JAIL = 'abroad-jail'
export const HIDDEN_TRAVELING = 'hidden-traveling'
export const TRAVELING_TO = 'traveling-to'
export const TRAVELING_FROM = 'traveling-from'
export const HOSPITAL_TRAVELING_TO = 'hospital-traveling-to'
export const HOSPITAL_TRAVELING_FROM = 'hospital-traveling-from'
export const JAIL_TRAVELING_TO = 'jail-traveling-to'
export const JAIL_TRAVELING_FROM = 'jail-traveling-from'
export const FEDERAL_JAIL = 'fed-jail'
export const ABROAD_FEDERAL_JAIL = 'abroad-fed-jail'
export const FEDERAL_JAIL_TEMPORARILY = 'fed-jail-temporarily'
export const ABROAD_FEDERAL_JAIL_TEMPORARILY = 'abroad-fed-jail-temporarily'
export const FEDERAL_JAIL_PERMANENTLY = 'fed-jail-permanently'
export const ABROAD_FEDERAL_JAIL_PERMANENTLY = 'abroad-fed-jail-permanently'
export const AWOKEN = 'awoken'
export const DORMANT = 'dormant'
export const ABROAD_AWOKEN = 'abroad-awoken'
export const ABROAD_DORMANT = 'abroad-dormant'

// NAME ICONS FOR STATUSES
export const HOSPITAL_ICON = 'HospitalStatus'
export const JAIL_ICON = 'JailStatus'
export const FEDERAL_JAIL_ICON = 'FederalJailStatus'
export const TRAVELING_ICON = 'TravelingStatus'
export const LOOT_ICON = 'LootStatus'
export const PRESET_TYPE = 'profile'

// ICONS FOR SVG GENERATOR
export const HOSPITAL_SVG = {
  name: HOSPITAL_ICON,
  type: null,
  preset: {
    type: PRESET_TYPE,
    subtype: 'HOSPITAL_STATUS'
  }
}

export const FEDERAL_JAIL_SVG = {
  name: FEDERAL_JAIL_ICON,
  type: null,
  preset: {
    type: PRESET_TYPE,
    subtype: 'FEDERAL_STATUS'
  }
}

export const JAIL_SVG = {
  name: JAIL_ICON,
  type: null,
  preset: {
    type: PRESET_TYPE,
    subtype: 'JAIL_STATUS'
  }
}

export const TRAVELING_SVG = {
  name: TRAVELING_ICON,
  type: null,
  preset: {
    type: PRESET_TYPE,
    subtype: 'TRAVELLING_STATUS'
  }
}

export const DEFAULT_SVG = {
  name: '',
  type: null,
  preset: {
    type: '',
    subtype: ''
  }
}

// DETAILS OF STATUSES
export const JAIL_DESCRIPTION = 'In jail for'
export const JAIL_STYLES = 'jail'

export const HOSPITAL_DESCRIPTION = 'In hospital for'
export const HOSPITAL_STYLES = 'hospital'

export const FEDERAL_JAIL_PERMANENTLY_DESCRIPTION = 'In federal jail permanently'
export const FEDERAL_JAIL_TEMPORARILY_DESCRIPTION = 'In federal jail temporarily'
export const ABROAD_FEDERAL_JAIL_STYLES = 'abroad federal-jail'
export const FEDERAL_JAIL_STYLES = 'okay federal-jail'

export const HIDDEN_TRAVELING_DESCRIPTION = 'Traveling'
export const HIDDEN_TRAVELING_STYLES = 'hidden travelling'

export const GOOD_DESCRIPTION = 'Okay'
export const GOOD_STYLES = 'okay'

export const DEAD_DESCRIPTION = 'In federal jail for 777 days'
export const DEAD_STYLES = 'dead'

export const DEFAULT_DETAILS = {
  description: GOOD_DESCRIPTION,
  style: GOOD_STYLES
}
