export const MAIN_URL = '/profiles.php.php?q='
export const AMOUNT_SECONDS_IN_WEEK = 60 * 60 * 24 * 7
export const AMOUNT_SECONDS_IN_DAY = 60 * 60 * 24
export const AMOUNT_SECONDS_HALF_DAY = 60 * 60 * 12
export const AMOUNT_SECONDS_IN_MINUTE = 60 * 60
export const AMOUNT_SECONDS = 60
export const ADD_USER_ACTION_TYPES = {
  friend: 'FRIEND',
  enemy: 'ENEMY'
}
export const MAX_REASON_LENGTH = 40
export const MAX_MESSAGE_LENGTH = 200
export const MIN_MONEY_TO_TRIGGER_WARNING = 1000000
export const ICON_TYPE = 'profile'
