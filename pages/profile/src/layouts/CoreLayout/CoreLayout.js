import PropTypes from 'prop-types'
import React, { Component } from 'react'
import BasicInformation from '../../components/BasicInformation'
import PersonalInformation from '../../components/PersonalInformation'
import CompetitionStatus from '../../components/CompetitionStatus'
import Medals from '../../components/Medals'
import UserInformation from '../../components/UserInformation'
import ProfileSignature from '../../components/ProfileSignature'
import UserStatus from '../../components/UserStatus'
import ProfileButtons from '../../components/ProfileButtons'
import DialoguesWrapper from '../../components/DialoguesWrapper'
import StaffTools from '../../components/StaffTools'
import '../../components/ProfileButtons/DM_vars.scss'
import '../../components/ProfileButtons/vars.scss'

class Profile extends Component {
  componentDidMount() {
    this.props.fetchProfileData()
    this.props.subscribeForProfileChanges()
    this.props.setPollForUpdate()
    window.addEventListener('focus', this.props.fetchProfileData)
  }

  componentWillUnmount() {
    window.removeEventListener('focus', this.props.fetchProfileData)
  }

  render() {
    return (
      <div className='core-layout__viewport'>
        <div className='user-profile'>
          <div className='profile-wrapper'>
            <div className='profile-left-wrapper left'>
              <UserInformation
                userInformation={this.props.profile.userInformation}
                loading={this.props.profile.prodileDataFetched}
              />
            </div>
            <div className='profile-right-wrapper right'>
              <DialoguesWrapper
                user={this.props.profile.user}
                currentUser={this.props.profile.currentUser}
                profileButtons={this.props.profile.profileButtons}
                isVisiblePersonalInfo={this.props.profile.personalInformation.visible}
                loading={this.props.profile.prodileDataFetched}
                lastAction={this.props.profile.basicInformation.lastAction}
                showDialog={this.props.showDialog}
                hideDialog={this.props.hideDialog}
                showSendCashDialog={this.props.showSendCashDialog}
                showButtonMsg={this.props.showButtonMsg}
                hideButtonMsg={this.props.hideButtonMsg}
                confirmDialog={this.props.confirmDialog}
                addToFriendList={this.props.addToFriendList}
                addToEnemyList={this.props.addToEnemyList}
                setMoneyData={this.props.setMoneyData}
                toggleAddUserDialog={this.props.toggleAddUserDialog}
                fetchProfileData={this.props.fetchProfileData}
                content={
                  <ProfileButtons
                    profileButtons={this.props.profile.profileButtons}
                    showButtonMsg={this.props.showButtonMsg}
                    hideButtonMsg={this.props.hideButtonMsg}
                    user={this.props.profile.user}
                    showDialog={this.props.showDialog}
                    showSendCashDialog={this.props.showSendCashDialog}
                    addToFriendList={this.props.addToFriendList}
                    addToEnemyList={this.props.addToEnemyList}
                    toggleAddUserDialog={this.props.toggleAddUserDialog}
                    fetchProfileData={this.props.fetchProfileData}
                  />
                }
              />
              <UserStatus
                userStatus={this.props.profile.userStatus}
                fetchProfileData={this.props.fetchProfileData}
                user={this.props.profile.user}
              />
            </div>
            <div className='clear' />
          </div>

          {this.props.profile.medalInformation.medals ? (
            <Medals medalInformation={this.props.profile.medalInformation} />
          ) : (
            ''
          )}

          <div className='profile-wrapper m-top10'>
            <BasicInformation
              basicInformation={this.props.profile.basicInformation}
              user={this.props.profile.user}
              currentUser={this.props.profile.currentUser}
            />
            <PersonalInformation
              personalInformation={this.props.profile.personalInformation}
              user={this.props.profile.user}
            />
            <CompetitionStatus competitionStatus={this.props.profile.competitionStatus} />
            <div className='clear' />
          </div>

          {this.props.profile.staffTools && <StaffTools staffTools={this.props.profile.staffTools} />}

          {this.props.profile.profileSignature ? (
            <ProfileSignature profileSignature={this.props.profile.profileSignature} />
          ) : (
            ''
          )}
        </div>
      </div>
    )
  }
}

Profile.propTypes = {
  profile: PropTypes.object,
  fetchProfileData: PropTypes.func,
  updateButtons: PropTypes.func,
  updateUserStatus: PropTypes.func,
  showSendCashDialogue: PropTypes.func,
  subscribeForProfileChanges: PropTypes.func,
  showDialog: PropTypes.func,
  hideDialog: PropTypes.func,
  showSendCashDialog: PropTypes.func,
  showButtonMsg: PropTypes.func,
  hideButtonMsg: PropTypes.func,
  confirmDialog: PropTypes.func,
  addToFriendList: PropTypes.func,
  addToEnemyList: PropTypes.func,
  setMoneyData: PropTypes.func,
  setPollForUpdate: PropTypes.func,
  toggleAddUserDialog: PropTypes.func
}

export default Profile
