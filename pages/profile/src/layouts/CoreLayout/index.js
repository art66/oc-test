import { connect } from 'react-redux'
import {
  fetchProfileData,
  updateButtons,
  updateUserStatus,
  subscribeForProfileChanges,
  setPollForUpdate
} from '../../modules/profile'
import {
  showDialog,
  hideDialog,
  confirmDialog,
  showSendCashDialog,
  showButtonMsg,
  hideButtonMsg,
  addToFriendList,
  addToEnemyList,
  setMoneyData,
  toggleAddUserDialog
} from '../../modules/profileButtons'

import CoreLayout from './CoreLayout'

const mapDispatchToProps = {
  fetchProfileData,
  updateButtons,
  updateUserStatus,
  showDialog,
  hideDialog,
  confirmDialog,
  showSendCashDialog,
  showButtonMsg,
  hideButtonMsg,
  subscribeForProfileChanges,
  addToFriendList,
  addToEnemyList,
  setMoneyData,
  setPollForUpdate,
  toggleAddUserDialog
}

const mapStateToProps = state => {
  return {
    profile: state.profile
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoreLayout)
