import { combineReducers } from 'redux'
import reducer from '../modules/profile'

export const makeRootReducer = () => {
  return combineReducers({
    profile: reducer
  })
}

export default makeRootReducer
