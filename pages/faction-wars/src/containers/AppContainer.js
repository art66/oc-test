import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Router, Route } from 'react-router'
import { Provider } from 'react-redux'
import { Main, WarItemDescription } from '../components'

class AppContainer extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

  render() {
    const { history, store } = this.props

    return (
      <Provider store={store}>
        <div style={{ height: '100%' }}>
          <Router history={history}>
            <Route path='/:key' component={Main} />
            <Route path='/' component={Main}>
              <Route path='/war/:key' component={WarItemDescription} />
            </Route>
            <Route path='/faction-main' component={Main}>
              <Route path='/war/:key' component={WarItemDescription} />
            </Route>
          </Router>
        </div>
      </Provider>
    )
  }
}

export default AppContainer
