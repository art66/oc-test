const SECONDS_IN_DAY = 86400
const SECONDS_IN_HOUR = 3600
const SECONDS_IN_MINUTE = 60

export const parseTime = time => {
  const days = Math.floor(time / SECONDS_IN_DAY)
  const hours = Math.floor((time - days * SECONDS_IN_DAY) / SECONDS_IN_HOUR)
  const minutes = Math.floor((time - days * SECONDS_IN_DAY - hours * SECONDS_IN_HOUR) / SECONDS_IN_MINUTE)
  const seconds = Math.floor(time - days * SECONDS_IN_DAY - hours * SECONDS_IN_HOUR - minutes * SECONDS_IN_MINUTE)

  return {
    days,
    hours,
    minutes,
    seconds
  }
}

export const normalizeTimeValue = parsedTimeValue =>
  parsedTimeValue < 10 ? `0${parsedTimeValue}` : `${parsedTimeValue}`

export const normalizeTime = parsedTime => {
  const { days, hours, minutes, seconds } = parsedTime

  const normalizedDays = normalizeTimeValue(days)
  const normalizedHours = normalizeTimeValue(hours)
  const normalizedMinutes = normalizeTimeValue(minutes)
  const normalizedSeconds = normalizeTimeValue(seconds)

  return {
    days: normalizedDays,
    hours: normalizedHours,
    minutes: normalizedMinutes,
    seconds: normalizedSeconds
  }
}

export const getShortFullFormat = parsedTime => {
  const { days, hours, minutes, seconds } = parsedTime

  const shortFullFormat = `${days}:${hours}:${minutes}:${seconds}`

  return shortFullFormat
}

export const getShortFormat = parsedTime => {
  const { days, hours, minutes, seconds } = parsedTime

  const values = [days, hours, minutes, seconds]

  while (values.length > 2) {
    if (values[0] <= 0) {
      values.shift()
    } else {
      break
    }
  }

  // for (let i in  values) {
  //   if (values[i] <= 0) {
  //     values.shift()
  //   } else {
  //     break
  //   }
  // }

  const shortFormat = values.join(':')

  // console.log(shortFormat)

  return shortFormat
}

export const getTimeMaxValueFormat = parsedTime => {
  const { days, hours, minutes, seconds } = parsedTime

  if (days > 0) {
    return `${parseInt(days, 10)} day${days > 1 ? 's' : ''}`
  } else if (hours > 0) {
    return `${parseInt(hours, 10)} hour${hours > 1 ? 's' : ''}`
  } else if (minutes > 0) {
    return `${parseInt(minutes, 10)} minute${minutes > 1 ? 's' : ''}`
  } else if (seconds > 0) {
    return `${parseInt(seconds, 10)} second${seconds > 1 ? 's' : ''}`
  }

  return '0 seconds'
}

export const getTimeShortValueFormat = parsedTime => {
  const { days, hours, minutes, seconds } = parsedTime

  if (days > 0) {
    return `${parseInt(days, 10)} d`
  } else if (hours > 0) {
    return `${parseInt(hours, 10)} h`
  } else if (minutes > 0) {
    return `${parseInt(minutes, 10)} m`
  } else if (seconds > 0) {
    return `${parseInt(seconds, 10)} s`
  }

  return ''
}

export const getTimeDifference = (now, endDate) => {
  const timeDifference = Math.floor(Math.max(endDate - now, 0) / 1000)

  return timeDifference
}

export default {
  parseTime,
  normalizeTimeValue,
  normalizeTime,
  getShortFullFormat,
  getShortFormat,
  getTimeMaxValueFormat,
  getTimeShortValueFormat,
  getTimeDifference
}
