import { getCookie } from '@torn/shared/utils'
import { FACTION_DESCRIPTION_COOKIE_NAME, ITEMS_IN_ROW, MEDIATYPE_DESKTOP } from '../constants'
import getTimerValue from './getTimerValue'

const { numberFormat, $, Raphael, initialiseTCChart, queryStringToObj, getCurrentTimestamp } = window

const globalNumberFormat = (num, dec = 0) => {
  const n = Math.round(num * 10 ** dec) / 10 ** dec

  const res = numberFormat(n)

  const arr = res.split('.')

  if (arr.length === 1) {
    if (dec === 0) {
      return res
    }
    arr.push('')
  }
  const l = arr[1].length

  for (let i = l; i < dec; i++) {
    arr[1] += '0'
  }
  return arr.join('.')
}

const getScoreProgress = (score, maxPoints) => {
  return Math.round((score / maxPoints) * 100)
}

const getDescIndex = (mediaType, warBoxIndex, isDesktopContainerWidth) => {
  if (warBoxIndex === -1) {
    return -1
  } else if (mediaType !== MEDIATYPE_DESKTOP && !isDesktopContainerWidth) {
    return warBoxIndex + 1
  }
  return warBoxIndex - (warBoxIndex % ITEMS_IN_ROW) + ITEMS_IN_ROW
}

const addZero = n => {
  return n > 9 ? n.toString() : `0${n}`
}

/* BEGIN Chart functionality */
const showTooltip = (x, y, contents) => {
  $('<div id=\'white-tooltip\' class=\'white-tooltip\'></div>')
    .append(`<div class='ui-tooltip-content'>${contents}<div class='tooltip-arrow left bottom'></div></div>`)
    .appendTo('body')
    .fadeIn(200)

  const $tooltip = $('#white-tooltip')
  const tooltipWidth = $tooltip.width()
  const tooltipHeight = $tooltip.height()

  $tooltip.css({
    position: 'absolute',
    top: y - tooltipHeight - 20,
    left: x - tooltipWidth / 2
  })
}

const generalDataSet = {
  series: {
    lines: {
      show: true,
      fill: 0,
      lineWidth: 2
    },
    points: {
      show: false,
      radius: 1.5
    },
    shadowSize: 1
  },
  grid: {
    hoverable: true,
    clickable: true,
    color: '#bbb',
    backgroundColor: '#f2f2f2',
    show: true,
    aboveData: false,
    margin: { top: 10, right: 10, bottom: 10, left: 10 },
    borderWidth: 2,
    borderColor: '#ddd',
    minBorderMargin: 10
  },
  legend: {
    show: true,
    position: 'nw',
    margin: [5, 5]
  },
  selection: {
    mode: 'xy'
  }
}

const defaultAxisDataSet = {
    xaxis: {
      mode: 'number',
      minTickSize: 12
    }
  },
  monthAxisDataSet = {
    xaxis: {
      /* mode: "time",
    minTickSize: [1, "minute"],
    timeformat: "%H:%M", */
      mode: 'number',
      minTickSize: 0.05,
      axisLabel: 'Hours'
    },
    yaxes: [
      {
        axisLabel: 'Chain'
      },
      {
        position: 'right',
        axisLabel: 'Respect'
      }
    ]
  }

const updateChart = (placeholder, linesData, axisData) => {
  const axisDataInstance = axisData || defaultAxisDataSet
  const linesDataSet = linesData // || getLinesDataSet() FIXME: Use undefined function. Should it be?

  $.extend(generalDataSet, axisDataInstance)
  const plot = $.plot(placeholder, linesDataSet, generalDataSet)

  return plot
}

const initialiseRaidChart = (placeholderClass, linesDataSet, type) => {
  const placeholder = placeholderClass
  const $placeholder = $(placeholder)

  if (type === 'raid') {
    monthAxisDataSet.yaxes[0].axisLabel = undefined
  }

  $.extend(generalDataSet, monthAxisDataSet)
  const plot = $.plot(placeholder, linesDataSet, generalDataSet)

  let previousPoint = null
  // var tooltipDateType = $(this).attr('tooltip-date-type')

  $placeholder.bind('plothover', (event, pos, item /* eslint-disable-line no-unused-vars */) => {
    // FIXME: Lint should be enabled

    if (item) {
      if (!item.series.showTooltip) {
        return
      }

      if (previousPoint !== item.dataIndex) {
        previousPoint = item.dataIndex
        $('#white-tooltip').remove()

        // var dataIndex = item.dataIndex
        const point = item.series.data[item.dataIndex]

        const /* x = point[0], */
          y = point[1],
          z = point[2]

        if (item.series.respect) {
          showTooltip(item.pageX, item.pageY, `<p class="bold m-bottom3">Respect ${y}</p>`)
        } else {
          showTooltip(item.pageX, item.pageY, `<p class="bold m-bottom3">Chain #${y} (${z})</p>`)
        }
      }
    } else {
      $('#white-tooltip').remove()
      previousPoint = null
    }
  })

  $('.chart-wrap').resizable()

  return plot
}
/* END */

const initialiseChart = initialiseTCChart

const isManualDesktopMode = () => document.body.classList.contains('d') && !document.body.classList.contains('r')

const mergeWars = (arr1, arr2) => {
  const result = []
  let wars2 = [...arr2]

  arr1.map(war1 => {
    const war2 = wars2.find(item => item.key === war1.key)

    if (war2) {
      result.push(war2)
      wars2 = wars2.filter(item => item.key !== war1.key)
    } else {
      result.push(war1)
    }
    return null
  })

  wars2.map(war2 => result.push(war2))

  return result
}

const isActiveMainTab = () => !queryStringToObj(window.location.hash).tab

const isFactionDescriptionOpened = () => getCookie(FACTION_DESCRIPTION_COOKIE_NAME) === 'true'

export {
  globalNumberFormat,
  $,
  Raphael,
  getScoreProgress,
  getTimerValue,
  getDescIndex,
  updateChart,
  initialiseChart,
  initialiseRaidChart,
  queryStringToObj,
  getCurrentTimestamp,
  addZero,
  isManualDesktopMode,
  mergeWars,
  isActiveMainTab,
  isFactionDescriptionOpened
}
