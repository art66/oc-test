import formatTime from './formatTime'

export default function getTimerValue(now: Date, endDate: Date) {
  const timeDifference = formatTime.getTimeDifference(now, endDate)

  const parsedTime = formatTime.parseTime(timeDifference)

  const normalizedTime = formatTime.normalizeTime(parsedTime)

  const shortFullFormat = formatTime.getShortFullFormat(normalizedTime)
  const shortFormat = formatTime.getShortFormat(normalizedTime)
  const maxValueFormat = formatTime.getTimeMaxValueFormat(normalizedTime)
  const shortValueFormat = formatTime.getTimeShortValueFormat(normalizedTime)

  const timer = {
    ...normalizedTime,
    shortFullFormat,
    shortFormat,
    maxValueFormat,
    shortValueFormat
  }

  return timer
}
