export default interface IWindow extends Window {
  isDesktopMedia: () => boolean
}
