import IFaction from './IFaction'
import IItem from './IItem'

export type TWarNotificationType = 'rankedWarring' // in the future we will have more than one type of notifications
export type TWarResult = 'win' | 'lose'

export default interface IWarNotification {
  id: number
  type: TWarNotificationType
  currentFaction: IFaction
  opponentFaction: IFaction
  result: TWarResult
  reportLink: string
  reward: {
    received: boolean
    points?: number
    respect?: number
    items?: IItem[]
    message?: string
  }
}
