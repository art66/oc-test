import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'
import { IBrowser } from 'redux-responsive'
import { IProfileMembersListMember } from './IMember'

export default interface IStore {
  app: {
    factionID: number
    members?: IProfileMembersListMember[]
    factionInfo?: {
      tag: string
      tagImageUrl: string
      rank: TFactionRank
      capacity: number
      description: {
        opened: boolean
        text: string
      }
      size: number
    }
    currentUser?: {
      settings: {
        showImages: boolean
      }
    }
    [key: string]: any
  }
  browser: IBrowser
}
