import { TOnlineStatus } from '@torn/shared/components/UserInfo/interfaces'

export interface IMember {
  playername: string
  userID: number
  level: number
  icons: string
  status: {
    text: string
    okay: boolean
  }
}

export interface IWarMember extends IMember {
  printName: {
    small: string
    large: string
  }
  factionID: number
  onOffLineIcon: string
  tag: string
  score: number
}

export interface IProfileMembersListMember extends IMember {
  honor: string
  onlineStatus: {
    className: string
    status: TOnlineStatus
    updateAt: number | null
  }
  days: number
  position: string
}
