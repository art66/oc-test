export interface IFaction {
  id: number
  name: string
}

export interface IFactionInfo {
  factionID: number
  factionTag: string
  factionRank: string
  factionTagImageUrl: string
}
