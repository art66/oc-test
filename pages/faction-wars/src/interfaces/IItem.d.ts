export default interface IItem {
  id: number
  name: string
  img: string
  amount: number
}
