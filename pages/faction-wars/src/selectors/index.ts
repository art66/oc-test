import IStore from '../interfaces/IStore'

export const getFactionMembers = (state: IStore) => state.app.members
export const getFactionDescription = (state: IStore) => state.app.factionInfo.description
export const getFactionDescriptionState = (state: IStore) => state.app.factionInfo.description.opened
export const getFactionTag = (state: IStore) => state.app.factionInfo.tag
export const getFactionTagImgUrl = (state: IStore) => state.app.factionInfo.tagImageUrl
export const getFactionID = (state: IStore) => state.app.factionID
export const getShowImagesSetting = (state: IStore) => state.app.currentUser.settings.showImages
export const getFactionRank = (state: IStore) => state.app.factionInfo.rank
export const getFactionCapacity = (state: IStore) => state.app.factionInfo.capacity
export const getFactionSize = (state: IStore) => state.app.factionInfo.size
