import { fetchUrl } from '@torn/shared/utils'
import { BASE_URL, RANK_BOX_KEY, ANIMATION_DURATION, ANIMATION_INTERVAL } from '../constants'
import { queryStringToObj } from '../utils'
import * as a from '../actions'

const hrefParams = queryStringToObj(window.location.search)
const isProfile = hrefParams.step === 'profile'
const profileFactionID = isProfile && hrefParams.ID ? hrefParams.ID : 0 // 22295
const profileUserID = isProfile && hrefParams.userID ? hrefParams.userID : 0

export const getWarData = (activeWarID = -1, update) => {
  return dispatch => {
    return fetchUrl(
      `${BASE_URL}&step=getwardata`
        + `&factionID=${profileFactionID}&userID=${profileUserID}&wardescid=${activeWarID}&update=${update}`
    ).then(json => {
      if (json.redirect) {
        window.location.href = json.redirect
      }
      if (update) {
        dispatch(a.warDataUpdate(json))
      } else {
        dispatch(a.warDataLoaded(json))
      }
    })
  }
}

export const getWarDescription = key => {
  return dispatch => {
    return fetchUrl(
      `${BASE_URL}&step=getwarusers&factionID=${profileFactionID}&userID=${profileUserID}&warID=${key}`
    ).then(json => {
      dispatch(a.warDescriptionLoaded(key, json))
    })
  }
}

export const joinWar = (warID, slotIndex) => {
  return dispatch => {
    dispatch(a.joiningWar(warID, slotIndex))
    return fetchUrl(`${BASE_URL}&step=joinwar&factionID=${profileFactionID}`, { warID, index: slotIndex }).then(
      json => {
        dispatch(a.joinedWar(json))
      }
    )
  }
}

export const updateNow = now => {
  return dispatch => {
    dispatch(a.nowUpdated(now))
  }
}

export const closeMsg = () => {
  return dispatch => {
    dispatch(a.msgClosed())
  }
}

export const requestSurrender = raidID => {
  return dispatch => {
    dispatch(a.requestingSurrender(raidID))
    return fetchUrl(`${BASE_URL}&step=raidAction&action=requestSurrender`, { raidID }).then(json => {
      dispatch(a.requestedSurrender(json))
    })
  }
}

export const ceaseRaid = raidID => {
  return dispatch => {
    dispatch(a.ceasingRaid(raidID))
    return fetchUrl(`${BASE_URL}&step=raidAction&action=ceaseRaid`, { raidID }).then(json => {
      dispatch(a.ceasedRaid(json))
    })
  }
}

const animationIntervals = []

export const subscribeForWarChanges = factionID => (dispatch, getState) => {
  const warHandler = new WebsocketHandler('warBoxes', { channel: `faction-war-boxes-${factionID}` })

  warHandler.setActions({
    rankedWarStart: data => {
      dispatch(a.addRankedWar(data))
    },
    rankedWarTargetUpdate: data => {
      const ranks = getState().app.wars.filter(item => item.key === RANK_BOX_KEY)

      if (ranks.length === 0) {
        return
      }

      const { respectRequirement } = ranks[0]
      const targetDelta = (data.target - respectRequirement) * (ANIMATION_INTERVAL / ANIMATION_DURATION)
      let counter = respectRequirement

      dispatch(a.startRankedWarTargetAnimation())
      dispatch(a.updateRankedWarTarget(data))

      const intervalTarget = setInterval(() => {
        counter += targetDelta
        dispatch(a.rankedWarTargetAnimation(counter))
        if (Math.abs(counter - data.target) <= 0.1) {
          clearInterval(intervalTarget)
          dispatch(a.finishRankedWarTargetAnimation())
        }
      }, ANIMATION_INTERVAL)
    },
    rankedWarFinish: () => {
      dispatch(a.finishRankedWar())
    },
    attackFinish: data => {
      if (!data.isRankedWar) {
        return
      }
      const ranks = getState().app.wars.filter(war => war.key === RANK_BOX_KEY)

      if (ranks.length === 0) {
        return
      }

      while (animationIntervals.length > 0) {
        const animationInterval = animationIntervals.shift()

        clearInterval(animationInterval)
      }

      dispatch(a.startRankedWarAnimation())
      dispatch(a.updateRankedWarScore(data))

      let counter = 0
      const step = data.respect * (ANIMATION_INTERVAL / ANIMATION_DURATION)
      const intervalInc = setInterval(() => {
        counter += ANIMATION_INTERVAL

        dispatch(a.rankedWarScoreAnimationIncrease(data.attacker.factionId, step))
        if (counter >= ANIMATION_DURATION) {
          clearInterval(intervalInc)
          dispatch(a.finishRankedWarAnimation())
        }
      }, ANIMATION_INTERVAL)

      animationIntervals.push(intervalInc)
    }
  })
}

export const subscribeForFactionUserChanges = factionID => dispatch => {
  const warHandler = new WebsocketHandler('users', { channel: `faction-users-${factionID}` })

  warHandler.setActions({
    updateStatus: data => {
      dispatch(a.updateRankWarUserStatus(data))
    }
  })
}

export const poolMembers = userIds => dispatch => {
  return fetchUrl(
    `/page.php?sid=factionsRankedWarring&step=getProcessBarRefreshData&userIds[]=${userIds.join('&userIds[]=')}`
  ).then(json => {
    dispatch(a.updateRankedWarMembersStatus(json))
  })
}
