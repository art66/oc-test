import { createStore, applyMiddleware, compose } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
// import createLogger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import thunkMiddleware from 'redux-thunk'
import { fetchInterceptor } from './middleware/fetchInterceptor'
import rootSaga from '../sagas'
import makeRootReducer from '../reducers'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore(preloadedState) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  const store = createStore(
    makeRootReducer(),
    preloadedState,
    composeEnhancers(
      responsiveStoreEnhancer,
      applyMiddleware(
        thunkMiddleware,
        sagaMiddleware,
        fetchInterceptor
        // createLogger()
      )
    )
  )

  sagaMiddleware.run(rootSaga)
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      // eslint-disable-next-line global-require
      const reducers = require('../reducers').default

      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
