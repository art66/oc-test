export const fetchInterceptor = store => next => action => {
  const { payload = {} } = action
  if (payload.meta === 'ajax') {
    if (payload.json.debugInfo) {
      console.log(payload.json.debugInfo)
    } else if (payload.json.redirect !== undefined) {
      if (payload.json.redirect === true) {
        location.href = location.protocol + '//' + location.hostname + '/' + payload.json.url
      } else if (payload.json.redirect === false) {
        console.log(payload.json.content)
      }
    }
  }
  return next(action)
}
