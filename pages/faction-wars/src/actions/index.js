import { createAction } from 'redux-actions'
import * as a from './actionTypes'

export const warDataLoaded = createAction('data loaded', json => ({ json }))
export const warDataUpdate = createAction('war data update', json => ({ json }))
export const warDescriptionLoaded = createAction('war description loaded', (key, json) => ({ key, json }))
export const nowUpdated = createAction('now updated', now => ({ now }))
export const joiningWar = createAction('joining war', (warID, slotIndex) => ({ warID, slotIndex }))
export const joinedWar = createAction('joined war', json => ({ json }))
export const msgClosed = createAction('msg closed')
export const requestingSurrender = createAction('requesting surrender', raidID => ({ raidID }))
export const requestedSurrender = createAction('requested surrender', json => ({ json }))
export const ceasingRaid = createAction('ceaseing raid', raidID => ({ raidID }))
export const ceasedRaid = createAction('ceased raid', json => ({ json }))
export const setDesctopMode = createAction('set desktop mode', isDesktopMode => ({ isDesktopMode }))
export const fetchNotifications = createAction(a.FETCH_NOTIFICATIONS)
export const setNotifications = createAction(a.SET_NOTIFICATIONS, notifications => ({ notifications }))
export const closeNotification = createAction(a.CLOSE_NOTIFICATION, id => ({ id }))
export const removeNotification = createAction(a.REMOVE_NOTIFICATION, id => ({ id }))
export const fetchFactionProfileInfo = createAction(a.FETCH_FACTION_PROFILE_INFO)
export const setFactionProfileInfo = createAction(a.SET_FACTION_PROFILE_INFO, data => ({ data }))
export const toggleFactionDescription = createAction(a.TOGGLE_FACTION_DESCRIPTION)
export const setFactionDescriptionState = createAction(a.SET_FACTION_DESCRIPTION_STATE, opened => ({ opened }))
export const addRankedWar = createAction('add ranked war', rank => ({ rank }))
export const startRankedWarTargetAnimation = createAction('start ranked war target animation', () => ({}))
export const finishRankedWarTargetAnimation = createAction('finish ranked war target animation', () => ({}))
export const updateRankedWarTarget = createAction('update ranked war target', data => ({ data }))
export const rankedWarTargetAnimation = createAction('ranked war target animation', animationTarget => ({
  animationTarget
}))
export const finishRankedWar = createAction('finish ranked war', () => ({}))
export const updateRankedWarScore = createAction('update ranked war score', data => ({ data }))
export const rankedWarScoreAnimationIncrease = createAction(
  'ranked war score animation increase',
  (factionID, respectInc) => ({ factionID, respectInc })
)
export const rankedWarScoreAnimationDecrease = createAction('ranked war score animation decrease', respectDec => ({
  respectDec
}))
export const startRankedWarAnimation = createAction('start ranked war animation', () => ({}))
export const finishRankedWarAnimation = createAction('finish ranked war animation', () => ({}))
export const updateRankWarUserStatus = createAction('update ranked war user status', data => ({ data }))
export const updateRankedWarMembersStatus = createAction('update ranked war members status', data => ({ data }))
