import { all } from 'redux-saga/effects'
import warNotifications from './warNotifications'
import factionProfileInfo from './factionProfileInfo'

export default function* rootSaga() {
  yield all([warNotifications(), factionProfileInfo()])
}
