import { takeEvery, put, call } from 'redux-saga/effects'
import { fetchUrl, setCookie } from '@torn/shared/utils'
import { queryStringToObj, isFactionDescriptionOpened } from '../utils'
import { setFactionProfileInfo, setFactionDescriptionState } from '../actions'
import { FACTION_DESCRIPTION_COOKIE_NAME } from '../constants'
import * as a from '../actions/actionTypes'
import IWindow from '../interfaces/IWindow'

declare const window: IWindow
const urlParams = queryStringToObj(window.location.search)

function* fetchFactionProfileInfo() {
  try {
    const data = yield fetchUrl(
      `/page.php?sid=factionsProfile&step=getInfo&factionId=${urlParams.ID}&userId=${urlParams.userID}`
    )

    if (data.success) {
      yield put(setFactionProfileInfo(data))
    }
  } catch (err) {
    console.log(err)
  }
}

function* toggleFactionDescription() {
  const opened = yield call(isFactionDescriptionOpened)

  yield put(setFactionDescriptionState(!opened))
  yield call(setCookie, FACTION_DESCRIPTION_COOKIE_NAME, `${!opened}`, 365)
}

export default function* factionProfileInfo() {
  yield takeEvery(a.FETCH_FACTION_PROFILE_INFO, fetchFactionProfileInfo)
  yield takeEvery(a.TOGGLE_FACTION_DESCRIPTION, toggleFactionDescription)
}
