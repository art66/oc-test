import { takeEvery, put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { setNotifications, removeNotification } from '../actions'
import * as a from '../actions/actionTypes'

export function* fetchNotifications() {
  const data = yield fetchUrl('/page.php?sid=factionsSplashNotification', { step: 'init' })

  if (data.success) {
    yield put(setNotifications(data.splashNotifications))
  }
}

export function* closeNotification(action: { type: string; payload: { id: number } }) {
  const data = yield fetchUrl('/page.php?sid=factionsSplashNotification', { step: 'close', id: action.payload.id })

  if (data.success) {
    yield put(removeNotification(action.payload.id))
  }
}

export default function* warNotifications() {
  yield takeEvery(a.FETCH_NOTIFICATIONS, fetchNotifications)
  yield takeEvery(a.CLOSE_NOTIFICATION, closeNotification)
}
