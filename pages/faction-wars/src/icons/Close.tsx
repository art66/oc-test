import React from 'react'

/* eslint-disable max-len */
export const Close = () => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
      width='34'
      height='34'
      viewBox='0 0 34 34'
    >
      <defs>
        <linearGradient id='wars-close-icon-gradient' x1='0.5' x2='0.5' y2='1' gradientUnits='objectBoundingBox'>
          <stop offset='0' stopColor='#868686' />
          <stop offset='1' stopColor='#656565' />
        </linearGradient>
        <linearGradient id='wars-close-icon-gradient-hover' x1='0.5' x2='0.5' y2='1' gradientUnits='objectBoundingBox'>
          <stop offset='0' stopColor='#ccc' />
          <stop offset='1' stopColor='#999' />
        </linearGradient>
        <filter id='wars-close-icon-filter' x='7' y='7' width='20' height='20' filterUnits='userSpaceOnUse'>
          <feOffset in='SourceAlpha' />
          <feGaussianBlur stdDeviation='1' result='c' />
          <feFlood floodOpacity='0.651' />
          <feComposite operator='in' in2='c' />
          <feComposite in='SourceGraphic' />
        </filter>
        <filter id='wars-close-icon-filter-hover' x='7' y='7' width='20' height='20' filterUnits='userSpaceOnUse'>
          <feOffset in='SourceAlpha' />
          <feGaussianBlur stdDeviation='1.5' result='c' />
          <feFlood floodColor='#fff' floodOpacity='0.251' />
          <feComposite operator='in' in2='c' />
          <feComposite in='SourceGraphic' />
        </filter>
      </defs>
      <path
        d='M14,11.776,9.15,6.988l4.783-4.831L11.776,0,6.986,4.852,2.138.067,0,2.206,4.854,7.012.067,11.861,2.206,14l4.8-4.852,4.833,4.785Z'
        transform='translate(10 10)'
      />
    </svg>
  )
}

export default Close
