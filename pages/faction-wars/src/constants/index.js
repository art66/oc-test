export const BASE_URL = '/faction_wars.php?redirect=false'
export const MILLISECONDS_IN_DAY = 86400000
export const SECOND = 1000
export const ITEMS_IN_ROW = 3
export const GREEN_COLOUR = '#798c3d'
export const RED_COLOUR = '#bf7f6b'
export const MEDIATYPE_DESKTOP = 'desktop'
export const MEDIATYPE_MOBILE = 'mobile'
export const DESKTOP_CONTAINER_WIDTH = 784
export const MIN_CHAIN_REPORT_VAL = 10
export const CHAIN_COOLDOWN_MULT = 6

export const RANK_BOX_KEY = 'rank'
export const RANK_BOX_TYPE = 'rank'
export const RAID_BOX_TYPE = 'raid'
export const CHAIN_BOX_TYPE = 'chain'
export const TERRITORY_BOX_TYPE = 'territory'
export const STATUS_UPDATE_PERIOD = 10
export const ANIMATION_DURATION = 500
export const ANIMATION_INTERVAL = 20
export const ANIMATION_PAUSE = 500
export const CHAIN_REPORT_URL = '/war.php?step=chainreport&chainID='

export const FACTION_DESCRIPTION_COOKIE_NAME = 'openedFactionDescription'

export const ASC = 'asc'
export const DESC = 'desc'
export const DEFAULT_SORT_FIELD = 'level'

export const CEASE_ACTION = 'cease'
export const SURRENDER_ACTION = 'surrender'
export const GRAPH_ACTION = 'graph'

export const RAID_SORT_FIELDS = {
  members: 'playername',
  level: 'level',
  damage: 'damage',
  status: 'status'
}
