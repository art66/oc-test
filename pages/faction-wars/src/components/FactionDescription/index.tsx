import React from 'react'
import cn from 'classnames'
import { useDispatch } from 'react-redux'
import s from './index.cssmodule.scss'

interface IProps {
  description: {
    text: string
    opened: boolean
  }
  toggleFactionDescription: () => void
  mediaType: 'desktop' | 'tablet' | 'mobile'
}

const FactionDescription = (props: IProps) => {
  const dispatch = useDispatch()

  const handleTitleClick = () => {
    if (props.mediaType !== 'desktop') {
      dispatch(props.toggleFactionDescription())
    }
  }

  return (
    <>
      <hr className='delimiter-999 m-top10' />
      {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
      <div
        className={cn('title-black', 'm-top10', s.titleToggle, 'faction-title', {
          [s.active]: props.description.opened
        })}
        data-title='description'
        role='heading'
        aria-level={5}
        onClick={handleTitleClick}
      >
        <div className={s.titleArrow} />
        Faction Description
      </div>
      <div
        className={cn(
          'cont-gray10',
          'bottom-round',
          s.contToggle,
          'faction-description',
          'unreset',
          'text-a-center',
          'scrollbar-transparent',
          { [s.active]: props.description.opened }
        )}
        dangerouslySetInnerHTML={{ __html: props.description.text }}
      />
    </>
  )
}

export default React.memo(FactionDescription)
