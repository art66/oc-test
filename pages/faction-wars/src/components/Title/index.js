import React from 'react'
import PropTypes from 'prop-types'

const Title = props => {
  const { warMsg } = props
  return (
    <div className={'f-msg m-top10 ' + warMsg.color}>
      <div className="l" />
      <div className="r" />
      <div className="rank-icon left" />
      <div className="respect-icon right" />
      <span className="title"> {warMsg.title} </span>
      <div className="rank">TIER: N/A</div>
      <div className="respect">RESPECT: {warMsg.respect}</div>
      <div className="clear" />
    </div>
  )
}

Title.propTypes = {
  warMsg: PropTypes.object
}

export default Title
