import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { globalNumberFormat, getTimerValue, getScoreProgress } from '../../../../utils'

class WarScoreBlock extends Component {
  render() {
    const { leftFactionData, rightFactionData, war, now } = this.props

    const { maxPoints } = war
    const timer = getTimerValue(now, war.endDate)

    const isLeftAttack = leftFactionData.membersQuantity > rightFactionData.membersQuantity && war.score > 0
    const isRightAttack = leftFactionData.membersQuantity < rightFactionData.membersQuantity && war.score > 0
    const scoreColour = war.isMyAttack ? 'green' : 'red'
    let scoreProgress = getScoreProgress(war.score, war.maxPoints)

    scoreProgress = isLeftAttack || isRightAttack ? Math.max(scoreProgress, 0) : scoreProgress

    const absDiff = Math.abs(leftFactionData.membersQuantity - rightFactionData.membersQuantity)
    let speed = ''

    if (absDiff >= 5) {
      speed = 'fast'
    } else if (absDiff >= 3) {
      speed = 'medium'
    } else {
      speed = 'slow'
    }

    return (
      <div className='info clearfix'>
        <div className={`member-count ${leftFactionData.isMyFaction ? 'your' : 'enemy'}-count`}>
          <div className='count'>
            <i className='swords-icon' />
            {leftFactionData.membersQuantity}
          </div>
          <span className='tag' dangerouslySetInnerHTML={{ __html: leftFactionData.factionTag }} />
        </div>
        <div className='faction-progress-wrap left'>
          <span className={`score ${scoreColour}`}>
            {globalNumberFormat(war.score)} / {globalNumberFormat(maxPoints)}
          </span>

          <div className='advantage-bar'>
            <div
              className={leftFactionData.isMyFaction ? 'your' : 'enemy'}
              style={{
                // width: (scoreProgress === 0 ? 0.001 : scoreProgress) + '%'
                width: `${scoreProgress}%`
              }}
            >
              {!isLeftAttack || (
                <div className={`animation ${speed}`}>
                  <div className='arrows' />
                </div>
              )}
            </div>
            <div
              className={rightFactionData.isMyFaction ? 'your' : 'enemy'}
              style={{
                width: `${100 - scoreProgress}%`
              }}
            >
              {!isRightAttack || (
                <div className={`animation ${speed}`}>
                  <div className='arrows' />
                </div>
              )}
            </div>
          </div>
          <div className='timer'>
            {timer.shortFullFormat.split('').map((char, index) => (
              <span key={index}>{char}</span>
            ))}
          </div>
        </div>
        <div className={`member-count ${rightFactionData.isMyFaction ? 'your' : 'enemy'}-count`}>
          <div className='count'>
            <i className='shield-icon' />
            {rightFactionData.membersQuantity}
          </div>
          <span className='tag' dangerouslySetInnerHTML={{ __html: rightFactionData.factionTag }} />
        </div>
      </div>
    )
  }
}

WarScoreBlock.propTypes = {
  leftFactionData: PropTypes.object,
  rightFactionData: PropTypes.object,
  war: PropTypes.object,
  now: PropTypes.number
}

export default WarScoreBlock
