import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Raphael } from '../../../../utils'
import { GREEN_COLOUR, RED_COLOUR } from '../../../../constants'

const svgWrapSize = {
  width: 36,
  height: 30
}

const colours = {
  green: GREEN_COLOUR,
  red: RED_COLOUR
}

class SVGShape extends Component {
  // eslint-disable-next-line max-statements
  render() {
    const { path, scoreProgress, leftColour, rightColour } = this.props

    let pPath = Raphael.parsePathString(path)
    let bBox = Raphael.pathBBox(pPath)

    pPath[0][1] -= bBox.x
    pPath[0][2] -= bBox.y

    let c = {
      w: svgWrapSize.width / bBox.width,
      h: svgWrapSize.height / bBox.height
    }
    let scale = c.w < c.h ? c.w : c.h

    for (let i = 0; i < pPath.length; i++) {
      let fragment = pPath[i]

      for (let j = 1; j < fragment.length; j++) {
        fragment[j] = Math.round(fragment[j] * scale * 1000) / 1000
      }
    }

    let leftCoef = scoreProgress / 100
    let rightCoef = 1 - leftCoef

    let pPathLeft = pPath.toString()
    let nBBox = Raphael.pathBBox(pPathLeft)
    let svgSize = {
      width: nBBox.width,
      height: nBBox.height
    }

    let pPath01Temp = pPath[0][1]

    pPath[0][1] -= (scoreProgress / 100) * svgSize.width * leftCoef

    pPath[0][1] = pPath01Temp - Math.round(bBox.width * scale * (1 - rightCoef) * 10000) / 10000
    let pPathRight = pPath.toString()

    return (
      <span className='territory-wrap'>
        <div
          className='checking-class'
          style={{
            padding: Math.max(Math.floor((svgWrapSize.height - svgSize.height) / 2), 2) + 'px 2px'
          }}
        >
          <div
            className='left'
            style={{
              width: Math.round(svgWrapSize.width * leftCoef),
              height: '100%',
              position: 'relative'
            }}
          >
            <svg
              style={{
                width: Math.round(nBBox.width * leftCoef),
                height: Math.round(nBBox.height),
                position: 'absolute',
                right: 0,
                top: 0
              }}
            >
              <g>
                <path d={pPathLeft} fill={colours[leftColour]} stroke={colours[leftColour]} fillOpacity='0.5' />
              </g>
            </svg>
          </div>
          <div
            className='right'
            style={{
              width: svgWrapSize.width - Math.round(svgWrapSize.width * leftCoef),
              height: '100%',
              position: 'relative'
            }}
          >
            <svg
              style={{
                width: Math.floor(nBBox.width * rightCoef),
                height: Math.round(nBBox.height),
                position: 'absolute',
                left: 0,
                top: 0
              }}
            >
              <g>
                <path d={pPathRight} fill={colours[rightColour]} stroke={colours[rightColour]} fillOpacity='0.5' />
              </g>
            </svg>
          </div>
        </div>
      </span>
    )
  }
}

SVGShape.propTypes = {
  path: PropTypes.string,
  scoreProgress: PropTypes.number,
  leftColour: PropTypes.string,
  rightColour: PropTypes.string
}

export default SVGShape
