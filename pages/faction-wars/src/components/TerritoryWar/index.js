import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import { SVGShape, WarScoreBlock } from './components'
import WarListItem from '../WarList/WarListItem'
import { getScoreProgress } from '../../utils'

class TerritoryWar extends Component {
  // eslint-disable-next-line max-statements
  render() {
    const { war, myFactionInfo, now, activeWarBox, className, first, last, active } = this.props

    let leftFactionData
    let rightFactionData
    const myFactionData = { ...war.myFaction, ...myFactionInfo }

    if (war.isMyAttack) {
      leftFactionData = myFactionData
      leftFactionData.factionColour = 'green'
      leftFactionData.isMyFaction = true

      rightFactionData = war.enemyFaction
      rightFactionData.factionColour = 'red'
    } else {
      leftFactionData = war.enemyFaction
      leftFactionData.factionColour = 'red'

      rightFactionData = myFactionData
      rightFactionData.factionColour = 'green'
      rightFactionData.isMyFaction = true
    }

    /* eslint-disable react/no-danger */
    /* eslint-disable jsx-a11y/control-has-associated-label */
    return (
      <WarListItem
        first={first}
        last={last}
        green={war.isTerritorial && war.isMyAttack}
        red={war.isTerritorial && !war.isMyAttack}
        active={active}
        className={className}
        link={activeWarBox === war.key ? '#/' : `#/war/${war.key}`}
      >
        <div className='status-wrap'>
          <div className='name clearfix'>
            <span className='text t-overflow'>
              <span>{war.isMyAttack ? 'Assaulting' : 'Defending'} </span>
              <a href={`/city.php#terrName=${war.territoryName}`}> {war.territoryName}</a>
              <span> {war.isMyAttack ? 'held by' : 'from'} </span>
              <a
                href={`/factions.php?step=profile&ID=${war.enemyFaction.factionID}`}
                dangerouslySetInnerHTML={{
                  __html: war.enemyFaction.factionName
                }}
              />
            </span>
            <SVGShape
              path={war.path}
              scoreProgress={getScoreProgress(war.score, war.maxPoints)}
              leftColour={leftFactionData.factionColour}
              rightColour={rightFactionData.factionColour}
            />
          </div>
          <WarScoreBlock leftFactionData={leftFactionData} rightFactionData={rightFactionData} war={war} now={now} />
        </div>
      </WarListItem>
    )
  }
}

TerritoryWar.propTypes = {
  war: PropTypes.object,
  myFactionInfo: PropTypes.object,
  now: PropTypes.number,
  activeWarBox: PropTypes.string,
  className: PropTypes.string,
  first: PropTypes.bool,
  last: PropTypes.bool,
  active: PropTypes.bool
}

export default TerritoryWar
