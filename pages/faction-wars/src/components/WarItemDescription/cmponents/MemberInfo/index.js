import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { MEDIATYPE_DESKTOP } from '../../../../constants'

class MemberInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hovered: false
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { member } = this.props
    const nextMember = nextProps.member

    if (member && nextMember && member.userID && nextMember.userID) {
      if (
        member.tag == nextMember.tag &&
        member.iconsList.length == nextMember.iconsList.length &&
        member.onOffLineIcon.length == nextMember.onOffLineIcon.length &&
        member.printName.large == nextMember.printName.large &&
        member.level == nextMember.level
      ) {
        return false
      } else {
        // console.log('should update', member)
      }
    }

    return true
  }

  // eslint-disable-next-line complexity
  render() {
    const {
      member,
      isDesktop
    } = this.props

    return (
      <span>
        <div
          className='member icons left'
          dangerouslySetInnerHTML={{
            __html: member.onOffLineIcon + member.tag + (isDesktop ? member.printName.large : member.printName.small)
          }}
        />

        <div className='level left'>{member.level}</div>

        <div
          className='user-icons icons left'
          dangerouslySetInnerHTML={{
            __html: member.iconsList
          }}
        />
      </span>
    )
  }
}

MemberInfo.propTypes = {
  member: PropTypes.object,
  isDesktop: PropTypes.bool
}

export default MemberInfo
