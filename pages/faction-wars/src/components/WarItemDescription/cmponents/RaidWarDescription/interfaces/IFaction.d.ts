export default interface IFaction {
  factionID: number
  factionName: string
}
