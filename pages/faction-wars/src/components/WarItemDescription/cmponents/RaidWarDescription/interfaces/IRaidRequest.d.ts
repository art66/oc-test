export default interface IRaidRequest {
  cease?: boolean
  surrender?: boolean
  msg: string
  loading: boolean
}
