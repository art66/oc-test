import React from 'react'
import cn from 'classnames'
import { initialiseRaidChart } from '../../../../utils'
import s from './index.cssmodule.scss'

interface IProps {
  points: any
}

class Graph extends React.Component<IProps> {
  private graphDiv: React.RefObject<HTMLDivElement>

  constructor(props) {
    super(props)

    this.graphDiv = React.createRef()
  }

  componentDidMount() {
    initialiseRaidChart(this.graphDiv.current, this.props.points, 'raid')
  }

  render() {
    return (
      <div className={cn('faction-war-info', s.graphWp)}>
        <div className={s.graph} ref={this.graphDiv} />
      </div>
    )
  }
}

export default Graph
