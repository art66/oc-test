import React from 'react'
import cn from 'classnames'
import s from './index.cssmodule.scss'
import { ASC, DESC, RAID_SORT_FIELDS } from '../../../../constants'

interface IProps {
  changeSort: (sortField: string) => void
  field: string
  title: string
  sortField: string
  sortDirection: string
  customClass?: string
}

const Column = (props: IProps) => {
  const { sortField, sortDirection, changeSort, field, title, customClass } = props

  const handleSort = () => {
    changeSort(props.field)
  }

  const handleKeyPressOnCol = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'Enter') {
      handleSort()
    }
  }

  return (
    <div
      tabIndex={0}
      role='button'
      className={cn('left', 'c-pointer', field, customClass, s.column, {
        [s.members]: field === RAID_SORT_FIELDS.members
      })}
      onClick={handleSort}
      onKeyPress={handleKeyPressOnCol}
    >
      {title}
      <div
        className={cn(s.sortIcon, {
          [s.activeIcon]: sortField === props.field && sortDirection !== '',
          [s.desc]: sortDirection === DESC,
          [s.asc]: sortDirection === ASC
        })}
      />
    </div>
  )
}

Column.defaultProps = {
  customClass: ''
}

export default Column
