import React from 'react'
import Loading from '../../../Loading'
import FactionLink from './FactionLink'
import IFaction from './interfaces/IFaction'
import IRaidRequest from './interfaces/IRaidRequest'
import { CEASE_ACTION, SURRENDER_ACTION } from '../../../../constants'

interface IProps {
  action: string
  faction: IFaction
  raidRequest: IRaidRequest
}

const ActionConfirm = (props: IProps) => {
  const { raidRequest: { cease, surrender, loading, msg }, faction, action } = props

  if (!cease && !surrender) {
    if (action === CEASE_ACTION) {
      return (
        <div>
          <div>
            Are you sure you wish to cease the raid between your faction and
            <span> <FactionLink faction={faction} /></span>?
          </div>
        </div>
      )
    }

    if (action === SURRENDER_ACTION) {
      return (
        <div>
          <div>Do you wish to surrender to <FactionLink faction={faction} />?</div>
          <div>
            This will request an end to the raid, but does not guarantee your submission will be accepted.
          </div>
        </div>
      )
    }
  }

  if (loading) {
    return <Loading />
  }

  return <span>{msg}</span>
}

export default ActionConfirm
