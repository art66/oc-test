import React from 'react'
import cn from 'classnames'

import Loading from '../../../Loading'
import Graph from './Graph'
import FactionLink from './FactionLink'
import ActionConfirm from './ActionConfirm'

import MemberList from './MemberList'

import { IMember } from '../../../../interfaces/IMember'
import IFaction from './interfaces/IFaction'
import IRaidRequest from './interfaces/IRaidRequest'

import s from './index.cssmodule.scss'
import { CEASE_ACTION, SURRENDER_ACTION, GRAPH_ACTION } from '../../../../constants'

interface IProps {
  loadWarDescData: (key: string) => void
  requestSurrender: (ID: string) => void
  ceaseRaid: (ID: string) => void
  isDesktop: boolean
  isProfileMode: boolean
  warDescriptionLoaded: any
  warDesc: {
    members: IMember[]
    graph: {}
    defenderFaction: IFaction
    aggressorFaction: IFaction
    enemyFaction: IFaction
    surrenderCooldown: number
    isInactive: boolean
  }
  raid: {
    ID: string
    key: string
    isMyWar: boolean
    enemyScore: number
    myScore: number
    enemyMemberNums: {
      total: number
    }
    isLeader: boolean
    finishAvaliable: boolean
  }
  raidRequest: IRaidRequest
}

interface IState {
  isShown: {
    graph: boolean
    surrender: boolean
    cease: boolean
  }
}

class RaidWarDescription extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)

    this.onShowCease = this.onShowCease.bind(this)
    this.onShowCeaseKey = this.onShowCeaseKey.bind(this)
    this.onCease = this.onCease.bind(this)
    this.onCeaseKey = this.onCeaseKey.bind(this)

    this.onShowSurrender = this.onShowSurrender.bind(this)
    this.onShowSurrenderKey = this.onShowSurrenderKey.bind(this)
    this.onSurrender = this.onSurrender.bind(this)
    this.onSurrenderKey = this.onSurrenderKey.bind(this)

    this.onShowGraph = this.onShowGraph.bind(this)
    this.onShowGraphKey = this.onShowGraphKey.bind(this)

    this.state = {
      isShown: {
        graph: false,
        surrender: false,
        cease: false
      }
    }
  }

  componentDidMount() {
    this.props.loadWarDescData(this.props.raid.key)
  }

  onShow(type) {
    this.setState(prevState => {
      const isShown = { ...prevState.isShown }

      Object.keys(isShown).forEach(v => {
        isShown[v] = false
      })
      isShown[type] = !prevState.isShown[type]

      return {
        isShown
      }
    })
  }

  onCease() {
    const { raid, raidRequest, ceaseRaid } = this.props

    if (raidRequest.cease) {
      this.onShowCease()
    } else {
      ceaseRaid(raid.ID)
    }
  }

  onCeaseKey = e => {
    if (e.key === 'Enter') {
      this.onCease()
    }
  }

  onSurrender() {
    const { raid, raidRequest, requestSurrender } = this.props

    if (raidRequest.surrender) {
      this.onShowSurrender()
    } else {
      requestSurrender(raid.ID)
    }
  }

  onSurrenderKey = e => {
    if (e.key === 'Enter') {
      this.onSurrender()
    }
  }

  onShowCease = () => {
    this.onShow(CEASE_ACTION)
  }

  onShowCeaseKey = e => {
    if (e.key === 'Enter') {
      this.onShow(CEASE_ACTION)
    }
  }

  onShowSurrender = () => {
    this.onShow(SURRENDER_ACTION)
  }

  onShowSurrenderKey = e => {
    if (e.key === 'Enter') {
      this.onShow(SURRENDER_ACTION)
    }
  }

  onShowGraph = () => {
    this.onShow(GRAPH_ACTION)
  }

  onShowGraphKey = e => {
    if (e.key === 'Enter') {
      this.onShow(GRAPH_ACTION)
    }
  }

  renderTitleLinks() {
    const { raid, warDesc } = this.props
    const {
      isShown: { surrender, cease }
    } = this.state

    if (!raid.finishAvaliable || !raid.isLeader) {
      return null
    }

    if (raid.myScore >= raid.enemyScore || raid.enemyMemberNums.total < 10 || warDesc.isInactive) {
      return (
        <span
          className={cn('t-blue h left-icon-link c-pointer icon-link', { active: cease }, s.leftActionIconWp)}
          tabIndex={0}
          role='button'
          onKeyPress={this.onShowCeaseKey}
          onClick={this.onShowCease}
        >
          <i className='raid-cease-icon' />
          <span className='m-left10'>Cease raid</span>
        </span>
      )
    }

    if (raid.myScore < raid.enemyScore && !warDesc.surrenderCooldown) {
      return (
        <span
          className={cn('t-blue h left-icon-link left c-pointer icon-link', { surrender })}
          tabIndex={0}
          role='button'
          onKeyPress={this.onShowSurrenderKey}
          onClick={this.onShowSurrender}
        >
          <i className='raid-surrender-icon' />
          <span className='m-left10'>Request Surrender</span>
        </span>
      )
    }

    return null
  }

  renderCease() {
    const { warDesc, raidRequest } = this.props
    const {
      isShown: { cease }
    } = this.state

    if (!cease) {
      return null
    }

    return (
      <div className='faction-war-info'>
        <div>
          <div className='message-block m-left10'>
            <ActionConfirm raidRequest={raidRequest} action={CEASE_ACTION} faction={warDesc.enemyFaction} />
          </div>
          <div className='buttons-block m-left10'>
            <div
              className='btn-wrap silver c-pointer'
              tabIndex={0}
              role='button'
              onKeyPress={this.onCeaseKey}
              onClick={this.onCease}
            >
              <span className='btn'>
                <button className='torn-btn' type='button'>
                  OKAY
                </button>
              </span>
            </div>
            {raidRequest.cease || (
              <div>
                <span
                  className='t-blue h c-pointer m-left10'
                  tabIndex={0}
                  role='button'
                  onKeyPress={this.onShowCeaseKey}
                  onClick={this.onShowCease}
                >
                  Cancel
                </span>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }

  renderSurrender() {
    const { warDesc, raidRequest } = this.props
    const {
      isShown: { surrender }
    } = this.state

    if (!surrender) {
      return null
    }

    return (
      <div className='faction-war-info faction-war'>
        <div>
          <div className='message-block m-left10'>
            <ActionConfirm raidRequest={raidRequest} action={SURRENDER_ACTION} faction={warDesc.enemyFaction} />
          </div>
          <div className='buttons-block m-bottom5' style={{ verticalAlign: 'super' }}>
            <div
              className='btn-wrap silver c-pointer'
              tabIndex={0}
              role='button'
              onKeyPress={this.onSurrenderKey}
              onClick={this.onSurrender}
            >
              <span className='btn'>OKAY</span>
            </div>
            {raidRequest.surrender || (
              <div>
                <span
                  className='t-blue h c-pointer m-left10'
                  tabIndex={0}
                  role='button'
                  onKeyPress={this.onShowSurrenderKey}
                  onClick={this.onShowSurrender}
                >
                  Cancel
                </span>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { raid, warDescriptionLoaded, warDesc, isProfileMode, isDesktop } = this.props
    const {
      isShown: { graph }
    } = this.state

    if (!raid || !warDescriptionLoaded[raid.key]) {
      return (
        <div className={cn('desc-wrap', s.raidDescWp)}>
          <div className='faction-war-info bottom-round'>
            <Loading />
          </div>
        </div>
      )
    }

    return (
      <div className={cn('desc-wrap', 'raid-members-list', s.raidDescWp)}>
        <div className='faction-war-info'>
          <div className='text m-top10'>
            <span className='raid-desc-faction-name'>
              <span dangerouslySetInnerHTML={{ __html: warDesc.enemyFaction && warDesc.enemyFaction.factionName }} />
              <span> member list</span>
            </span>
          </div>
          <div className='raid-title text m-top5'>
            {this.renderTitleLinks()}

            <FactionLink faction={warDesc.aggressorFaction} />
            <span> raiding </span>
            <FactionLink faction={warDesc.defenderFaction} />

            <span
              className={cn('t-blue h right-icon-link m-left5 c-pointer icon-link', { active: graph }, s.viewGraphWp)}
              tabIndex={0}
              role='button'
              onKeyPress={this.onShowGraphKey}
              onClick={this.onShowGraph}
            >
              <span className='m-left10'>View graph</span>
              <i className='raid-graph-icon m-left10' />
            </span>
          </div>
        </div>

        {!graph || <Graph points={warDesc.graph} />}

        {this.renderCease()}

        {this.renderSurrender()}

        <MemberList
          members={warDesc.members}
          isProfileMode={isProfileMode}
          isDesktop={isDesktop}
          isMyWar={raid.isMyWar}
        />
      </div>
    )
  }
}

export default RaidWarDescription
