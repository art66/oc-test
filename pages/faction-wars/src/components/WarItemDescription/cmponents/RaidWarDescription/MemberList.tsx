import React from 'react'
import cn from 'classnames'
import UserInfo from '@torn/shared/components/UserInfo'
import Column from './Column'
import { globalNumberFormat as gnf } from '../../../../utils'
import { ASC, DESC, RAID_SORT_FIELDS } from '../../../../constants'
import s from './index.cssmodule.scss'

interface IProps {
  isDesktop: boolean
  isProfileMode: boolean
  isMyWar: boolean
  members: any
}

interface IState {
  sortField: string
  sortDirection: string
}

const renderUserInfo = (user, isDesktop) => {
  const config = {
    showImages: user.honor !== '',
    useProgressiveImage: false,
    status: {
      mode: user.onlineStatus.status.toLowerCase()
    },
    user: {
      ID: user.userID,
      name: user.playername,
      imageUrl: user.honor,
      isSmall: !isDesktop
    },
    faction: {
      ID: user.factionID,
      name: user.factiontag,
      rank: user.factionRank,
      imageUrl: user.factionTagUrl
    },
    customStyles: {
      status: s.customStatus,
      blockWrap: s.customBlockWrap
    }
  }

  return <UserInfo {...config} />
}

const renderAttack = (member) => {
  if (member.status.okay) {
    return (
      <a className='t-blue h c-pointer' href={`loader2.php?sid=getInAttack&user2ID=${member.userID}`}>Attack</a>
    )
  }

  return <span className='t-gray-9'>Attack</span>
}

class MemberList extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)

    this.handleSort = this.handleSort.bind(this)

    this.state = {
      sortField: RAID_SORT_FIELDS.level,
      sortDirection: DESC
    }
  }

  handleSort = (sortField) => {
    this.setState(prevState => ({
      sortField,
      sortDirection: prevState.sortDirection === DESC && prevState.sortField === sortField ? ASC : DESC
    }))
  }

  _getValue(member) {
    const { sortField } = this.state

    if (sortField === RAID_SORT_FIELDS.status) {
      return member[sortField].okay.toString() + member[sortField].text.toLowerCase()
    }

    if (typeof member[sortField] === 'string') {
      return member[sortField].toLowerCase()
    }

    return member[sortField]
  }

  render() {
    const { members, isDesktop, isProfileMode, isMyWar } = this.props
    const { sortField, sortDirection } = this.state
    const sortedMembers = [...members].sort((a, b) => {
      const valueA = this._getValue(a)
      const valueB = this._getValue(b)

      if (valueA === valueB) {
        return 0
      }
      if (sortDirection === ASC) {
        return valueA < valueB ? -1 : 1
      }
      if (sortDirection === DESC) {
        return valueA > valueB ? -1 : 1
      }
      return 0
    })

    return (
      <div className='faction-war'>
        <div className='tab-menu-cont cont-gray bottom-round'>
          <div className={cn('members-cont', { 'profile-mode': isProfileMode && !isMyWar })}>
            <div className='title white-grad clearfix'>
              <Column
                changeSort={this.handleSort}
                field={RAID_SORT_FIELDS.members}
                title='Members'
                customClass='members'
                sortField={sortField}
                sortDirection={sortDirection}
              />
              <Column
                changeSort={this.handleSort}
                field={RAID_SORT_FIELDS.level}
                title='Level'
                sortField={sortField}
                sortDirection={sortDirection}
              />
              <div className='user-icons icons left'>Icons</div>
              <Column
                changeSort={this.handleSort}
                field={RAID_SORT_FIELDS.damage}
                title='Damage'
                customClass='points'
                sortField={sortField}
                sortDirection={sortDirection}
              />
              <Column
                changeSort={this.handleSort}
                field={RAID_SORT_FIELDS.status}
                title='Status'
                sortField={sortField}
                sortDirection={sortDirection}
              />
              <div className='attack left'>Attack</div>
            </div>
            <ul className='members-list'>
              {sortedMembers.map((member, index) => {
                return (
                  <li key={member.userID} className={cn('enemy', { last: sortedMembers.length === index + 1 })}>
                    <div className='member icons left'>
                      {renderUserInfo(member, isDesktop)}
                    </div>

                    <div className='level left'>{member.level}</div>
                    <div
                      className='user-icons icons left'
                      dangerouslySetInnerHTML={{
                        __html: member.icons
                      }}
                    />
                    <div className='points left'>{gnf(member.damage, 2)}</div>

                    <div className={cn('status left', { ok: member.status.okay, 'not-ok': !member.status.okay })}>
                      {member.status.text}
                    </div>

                    <div className='attack left'>
                      {renderAttack(member)}
                    </div>
                    <div className='clear' />
                  </li>
                )
              })}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default MemberList
