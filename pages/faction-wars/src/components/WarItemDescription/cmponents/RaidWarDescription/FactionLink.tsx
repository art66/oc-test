import React from 'react'
import IFaction from './interfaces/IFaction'

interface IProps {
  faction: IFaction
}

const FactionLink = (props: IProps) => {
  return (
    <a
      className='t-blue h'
      href={`/factions.php?step=profile&ID=${props.faction.factionID}`}
    >
      <span dangerouslySetInnerHTML={{ __html: props.faction.factionName }} />
    </a>
  )
}

export default FactionLink
