import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { $, globalNumberFormat, updateChart, initialiseChart, addZero } from '../../../../utils'

const graphParams = {
  graphDisplayModes: [
    {
      label: 'Live',
      name: 'local'
    },
    {
      label: 'Current',
      name: 'current'
    },
    {
      label: 'Total',
      name: 'global'
    }
  ],
  scoreLine: {
    // label: 'War score',
    // color: '#bf7f6b',
    color: '#19b2e5'
  },
  successLine: {
    color: '#798c3d'
  },
  unsuccessLine: {
    color: '#bf7f6b'
  },
  graphMode: 'global',
  frameSize: 60,
  graphPointStoreSize: 100
}

class WarGraph extends Component {
  constructor(props) {
    super(props)
    this.state = {
      graphMode: 'global'
    }
  }

  componentDidMount() {
    this.localGraphData = []
    this.addLocalGraphPoint()

    // eslint-disable-next-line react/no-string-refs
    const $root = $(this.refs.rootNode)

    // $root.find('.radio input').prettyCheckable()
    const graphData = this._getWarGraphData()

    initialiseChart($root.find('.chart-placeholder'), graphData, 0, this._getWarGraphDataSet(graphData))
  }

  // eslint-disable-next-line react/sort-comp
  componentDidUpdate() {
    const graphData = this._getWarGraphData()

    // eslint-disable-next-line react/no-string-refs
    updateChart($(this.refs.rootNode).find('.chart-placeholder'), graphData, this._getWarGraphDataSet(graphData))
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { timestamp } = this.props
    const { graphMode } = this.state

    if (timestamp !== nextProps.timestamp) {
      this.addLocalGraphPoint()
    }

    return graphMode !== nextState.graphMode || timestamp !== nextProps.timestamp
  }

  onCheckMode(mode) {
    const { graphMode } = this.state

    if (graphMode !== mode) {
      this.setState({
        graphMode: mode
      })
    }
  }

  addLocalGraphPoint() {
    const { timestamp, score, isAJAXupdate } = this.props
    const { graphPointStoreSize } = graphParams

    const point = [timestamp, score]

    if (
      this.localGraphData.length > 0
      && (this.localGraphData[this.localGraphData.length - 1][0] === point[0] || isAJAXupdate === true)
    ) {
      this.localGraphData[this.localGraphData.length - 1] = point
    } else {
      this.localGraphData.push(point)
    }

    if (this.localGraphData.length > graphPointStoreSize) {
      this.localGraphData.shift()
    }

    return true
  }

  _getGraphBounds() {
    const { membersDiff, slots } = this.props

    const membersDiffAbs = Math.abs(membersDiff)
    const mArr = [1, 0.7, 0.3, 0]

    for (let i = 0; i < mArr.length - 1; i++) {
      if (membersDiffAbs >= mArr[i + 1] * slots) {
        return Math.round(mArr[i] * slots)
      }
    }
    return null
  }

  // eslint-disable-next-line max-statements, complexity
  _getWarGraphData() {
    const { props } = this
    const { graphMode } = this.state

    const graphData = []
    let i = 0
    let j = 0

    while (i < props.graphData.length || j < this.localGraphData.length) {
      const a = props.graphData[i]
      const b = this.localGraphData[j]

      if (a !== undefined) {
        if (b === undefined || a[0] < b[0]) {
          graphData.push(a)
          i++
        } else {
          graphData.push(b)
          j++
        }
      } else {
        graphData.push(b)
        j++
      }
    }

    const dataArr = []
    const { endDate } = props
    let localPeriod,
      startXVal,
      maxXVal,
      startScore

    const first = graphData[0]
    const second = graphData[1]

    let scoreIncLocal,
      endPeriodMaxScore,
      endPeriodMinScore

    switch (graphMode) {
      case 'local':
        localPeriod = graphParams.frameSize * 1000

        startXVal = endDate - (Math.floor((endDate - props.timestamp) / localPeriod) + 1) * localPeriod
        maxXVal = startXVal + localPeriod

        while (graphData[1] && graphData[0][0] < startXVal && graphData[1][0] < startXVal) {
          graphData.shift()
        }

        startScore = this._getFunctionValue(first, second, startXVal)
        // let maxScore = this._getFunctionValue(preLast, last, maxXVal)

        graphData[0] = [startXVal, startScore]

        dataArr.push({
          label: graphParams.scoreLine.label,
          color: graphParams.scoreLine.color,
          data: graphData
        })

        scoreIncLocal = Math.round(((maxXVal - startXVal) / 1000) * this._getGraphBounds())
        endPeriodMaxScore = Math.min(startScore + scoreIncLocal, props.maxPoints)
        endPeriodMinScore = Math.max(startScore - scoreIncLocal, 0)

        dataArr.push({
          data: [[maxXVal, endPeriodMinScore]]
        })

        dataArr.push({
          data: [[maxXVal, endPeriodMaxScore]]
        })

        break

      case 'current':
        dataArr.push({
          label: graphParams.scoreLine.label,
          color: graphParams.scoreLine.color,
          data: graphData
        })
        break

      case 'global':
        dataArr.push({
          label: graphParams.scoreLine.label,
          color: graphParams.scoreLine.color,
          data: graphData
        })
        dataArr.push({
          data: [graphData[0]]
        })
        dataArr.push({
          data: [[props.endDate, props.maxPoints]]
        })
        break

      default:
        break
    }

    const { scoreInc } = props

    if (dataArr[0].data.length > 1) {
      const startPoint = dataArr[0].data[0]
      const endPoint = dataArr[0].data[dataArr[0].data.length - 1]

      const endWarScoreValue = Math.round(((props.endDate - endPoint[0]) / 1000) * scoreInc + endPoint[1])
      const isSuccess = endWarScoreValue >= props.maxPoints
      let zeroEndPoint = new Array(2)

      if (graphMode === 'global') {
        const lastPoint = [props.endDate, Math.min(endWarScoreValue, props.maxPoints)]

        if (endWarScoreValue > props.maxPoints || scoreInc >= 0) {
          lastPoint[0] = Math.min(endPoint[0] + ((props.maxPoints - endPoint[1]) * 1000) / scoreInc, props.endDate)
        } else {
          lastPoint[0] = Math.min(endPoint[0] + ((0 - endPoint[1]) * 1000) / scoreInc, props.endDate)
          zeroEndPoint = [props.endDate, Math.max(lastPoint[1], 0)]
        }
        lastPoint[1] = Math.max(lastPoint[1], 0)

        dataArr.push({
          color: isSuccess === props.isMyAttack ? graphParams.successLine.color : graphParams.unsuccessLine.color,
          data: [endPoint, lastPoint, zeroEndPoint]
        })
      } else if (graphMode === 'current') {
        const lastTimestamp = Math.min(endPoint[0] + Math.round((endPoint[0] - startPoint[0]) * 0.2), props.endDate)
        const lastPoint = [lastTimestamp, endPoint[1] + ((lastTimestamp - endPoint[0]) / 1000) * scoreInc]

        lastPoint[1] = Math.max(lastPoint[1], 0)

        dataArr.push({
          color: isSuccess === props.isMyAttack ? graphParams.successLine.color : graphParams.unsuccessLine.color,
          data: [endPoint, lastPoint]
        })
      }
    }

    const isNumberFormat = ['global', 'current'].indexOf(graphMode) >= 0
    const divider = isNumberFormat ? 3600000 : 1
    const { startDate } = props

    for (i = 0; i < dataArr.length; i++) {
      for (j = 0; j < dataArr[i].data.length; j++) {
        dataArr[i].data[j] = [(dataArr[i].data[j][0] - startDate) / divider, dataArr[i].data[j][1]]
      }
    }

    return dataArr
  }

  _getWarGraphDataSet(graphData) {
    const { graphMode } = this.state
    const { props } = this
    let chartDataSet = {}
    let max = 0
    let min = Number.MAX_VALUE

    for (let i = 0; i < graphData.length; i++) {
      for (let j = 0; j < graphData[i].data.length; j++) {
        max = Math.max(max, graphData[i].data[j][1])
        min = Math.min(min, graphData[i].data[j][1])
      }
    }

    switch (graphMode) {
      case 'local':
        chartDataSet = {
          yaxis: {
            min,
            max
          },
          xaxis: {
            mode: 'time',
            minTickSize: [1, 'second'],
            timeformat: '%H:%M:%S'
          }
        }
        break

      case 'current':
        chartDataSet = {
          yaxis: {
            min: 0,
            max: Math.round(Math.min(max * 1.5, props.maxPoints))
          },
          xaxis: {
            mode: 'number',
            minTickSize: 1
          }
        }
        break

      case 'global':
        chartDataSet = {
          yaxis: {
            min: 0,
            max: props.maxPoints
          },
          xaxis: {
            mode: 'number',
            minTickSize: 12
          }
        }
        break

      default:
        break
    }
    chartDataSet.yaxis.minTickSize = 1
    chartDataSet.yaxis.tickFormatter = val => {
      return globalNumberFormat(Math.round(val))
    }
    return chartDataSet
  }

  _getFunctionValue(p1, p2, x) {
    if (!p1 || !p2) {
      // return x
      return this.props.score
    }
    const x1 = p1[0]
    const y1 = p1[1]
    const x2 = p2[0]
    const y2 = p2[1]

    return Math.round(((x - x1) / (x2 - x1)) * (y2 - y1) + y1)
  }

  _renderStartEndDates() {
    const { startDate, endDate } = this.props
    const start = new Date(startDate)
    const end = new Date(endDate)

    const sDay = addZero(start.getUTCDate())
    const sMonth = addZero(start.getUTCMonth() + 1)
    const sYear = start.getUTCFullYear()

    const eDay = addZero(end.getUTCDate())
    const eMonth = addZero(end.getUTCMonth() + 1)
    const eYear = end.getUTCFullYear()

    return `${sMonth}-${sDay}-${sYear} / ${eMonth}-${eDay}-${eYear}`
  }

  render() {
    const { graphMode } = this.state

    return (
      // eslint-disable-next-line react/no-string-refs
      <div ref='rootNode'>
        <div className='text'>
          <ul className='period-wrap'>
            {(() => {
              return graphParams.graphDisplayModes.map((item, index) => {
                return (
                  // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                  <li
                    className={'radio clearfix prettyradio labelright blue m-left' + (index === 0 ? '10' : '20')}
                    key={item.name}
                    style={{
                      float: 'left'
                    }}
                    onClick={() => {
                      this.onCheckMode(item.name)
                    }}
                  >
                    <input
                      className='radio-css'
                      type='radio'
                      name='mode'
                      readOnly={true}
                      checked={graphMode === item.name}
                    />
                    <label className='marker-css'>{item.label}</label>
                    <div className='t-delimiter white d-hide' />
                    <div className='b-delimiter white d-hide' />
                  </li>
                )
              })
            })()}
            <li className='right m-right10'>
              {this._renderStartEndDates()}
            </li>
            <li className='clear' />
          </ul>
        </div>
        <div className='chart-wrap'>
          <div
            className='chart-placeholder'
            style={{ height: 200, width: '100%' }}
            data-tooltip='Score '
            tooltip-date-type='time'
          />
        </div>
      </div>
    )
  }
}

WarGraph.propTypes = {
  graphData: PropTypes.array,
  endDate: PropTypes.number,
  startDate: PropTypes.number,
  maxPoints: PropTypes.number,
  slots: PropTypes.number,
  score: PropTypes.number,
  scoreInc: PropTypes.number,
  membersDiff: PropTypes.number,
  isMyAttack: PropTypes.bool,
  timestamp: PropTypes.number,
  isAJAXupdate: PropTypes.bool
}

export default WarGraph
