import React from 'react'
import cn from 'classnames'
import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'
import { IWarMember } from '../../../../interfaces/IMember'
import { globalNumberFormat as gnf } from '../../../../utils'
import s from './index.cssmodule.scss'

interface IFaction {
  id: number
  name: string
  rank: TFactionRank
}

interface IProps {
  members: IWarMember[]
  currentFaction: IFaction
  opponentFaction: IFaction
  scoreFieldName: string
  mediaType: string
  isProfileMode: boolean
}

interface IState {
  opponentActive: boolean
  sorting: {
    field: string
    direction: string
  }
}

const ASC = 'ask'
const DESC = 'desc'

class WarMemberList extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      opponentActive: true,
      sorting: {
        field: 'score',
        direction: DESC
      }
    }
  }

  getScore(factionID) {
    const sum = this.props.members
      .filter(member => member.factionID === factionID)
      .reduce((currentSum, member) => currentSum + member.score, 0)

    return Math.floor(sum)
  }

  /* eslint-disable no-nested-ternary */
  _handleTabClick(sortField) {
    this.setState(prevState => ({
      sorting: {
        field: sortField,
        direction: (
          prevState.sorting.direction === DESC && prevState.sorting.field === sortField ? ASC : DESC
        )
      }
    }))
  }

  _getSortValue(member) {
    const { sorting: { field } } = this.state

    if (field === 'status') {
      return member[field].okay.toString() + member[field].text.toLowerCase()
    }

    if (typeof member[field] === 'string') {
      return member[field].toLowerCase()
    }

    return member[field]
  }

  _renderMembers(factionID) {
    const { members, currentFaction, opponentFaction } = this.props
    const { sorting: { direction } } = this.state
    // const isDesktop = mediaType === 'desktop'
    const enemy = opponentFaction.id === factionID
    const your = currentFaction.id === factionID

    // console.log(field, direction)

    /* eslint-disable react/no-danger */
    return (
      <ul className={cn('members-list', s.membersCont)}>
        {[...members].sort((a, b) => {
          const valueA = this._getSortValue(a)
          const valueB = this._getSortValue(b)

          if (valueA === valueB) {
            return 0
          }
          if (direction === ASC) {
            return valueA < valueB ? -1 : 1
          }
          if (direction === DESC) {
            return valueA > valueB ? -1 : 1
          }
          return 0
        }).filter(member => (member.factionID === factionID)).map((member, index) => {
          const printName = member.printName.small

          return (
            <li
              key={member.userID}
              className={cn({
                enemy, your, last: members.length === index + 1, [s.enemy]: enemy, [s.your]: your
              })}
            >
              <div
                className={cn('member', 'icons', 'left', s.member)}
                dangerouslySetInnerHTML={{
                  __html: `${member.onOffLineIcon} ${member.tag} ${printName}`
                }}
              />
              <div className={cn('level', 'left', s.level)}>{member.level}</div>
              <div className={cn('points', 'left', s.points)}>{gnf(member.score, 2)}</div>
              <div
                className={cn('status', 'left', s.prevColumn, s.status, {
                  ok: member.status.okay,
                  'not-ok': !member.status.okay
                })}
              >
                { member.status.text}
              </div>
              <div className={cn('attack', 'left', s.attack)}>
                {member.status.okay ? (
                  <a
                    className={cn('t-blue', 'h', 'c-pointer')}
                    href={`loader2.php?sid=getInAttack&user2ID=${member.userID}`}
                  >
                    Attack
                  </a>
                ) : (
                  <span className='t-gray-9'> Attack </span>
                )}
              </div>
              <div className='clear' />
            </li>
          )
        })}
      </ul>
    )
  }

  _handleNameTabClick(isYour) {
    this.setState(prevState => ({
      opponentActive: !(isYour && prevState.opponentActive)
    }))
  }

  _renderFactionName(faction) {
    const { currentFaction } = this.props
    const { opponentActive } = this.state
    const isYour = faction.id === currentFaction.id

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div
        className={
          cn('name', s.name, {
            [s.current]: isYour,
            [s.opponent]: !isYour,
            [s.active]: isYour === !opponentActive,
            your: isYour,
            left: !isYour,
            enemy: !isYour,
            right: isYour
          })
        }
        onClick={() => this._handleNameTabClick(isYour)}
      >
        <div className={s.text} dangerouslySetInnerHTML={{ __html: faction.name }} />
        <div className={cn('score', s.score)}>
          {gnf(this.getScore(faction.id))}
        </div>
      </div>
    )
  }

  _renderMemberCont(faction) {
    const { isProfileMode, currentFaction } = this.props
    const { opponentActive, sorting: { field, direction } } = this.state
    const isYour = currentFaction.id === faction.id

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div className={
        cn('tab-menu-cont', 'cont-gray', 'bottom-round', s.tabMenuCont, {
          'your-faction': isYour,
          'enemy-faction': !isYour,
          'profile-mode': isProfileMode,
          left: !isYour,
          right: isYour,
          [s.active]: isYour === !opponentActive
        })
      }
      >
        <div className={cn('members-cont', s.membersCont, { [s.profileMode]: isProfileMode })}>
          <div className='white-grad c-pointer'>
            <div
              className={cn('member', 'left', s.member, s.tab)}
              onClick={() => this._handleTabClick('playername')}
            >
              Members
              <div className={cn(s.sortIcon, {
                [s.activeIcon]: field === 'playername' && direction !== '',
                [s.desc]: direction === DESC,
                [s.asc]: direction === ASC
              })}
              />
            </div>
            <div
              className={cn('level', 'left', s.level, s.tab)}
              onClick={() => this._handleTabClick('level')}
            >
              Level
              <div className={cn(s.sortIcon, {
                [s.activeIcon]: field === 'level' && direction !== '',
                [s.desc]: direction === DESC,
                [s.asc]: direction === ASC
              })}
              />
            </div>
            <div
              className={cn('points', 'left', s.points, s.tab)}
              onClick={() => this._handleTabClick('score')}
            >
              Score
              <div className={cn(s.sortIcon, {
                [s.activeIcon]: field === 'score' && direction !== '',
                [s.desc]: direction === DESC,
                [s.asc]: direction === ASC
              })}
              />
            </div>
            <div
              className={cn('status', 'left', s.status, s.tab)}
              onClick={() => this._handleTabClick('status')}
            >
              Status
              <div className={cn(s.sortIcon, {
                [s.activeIcon]: field === 'status' && direction !== '',
                [s.desc]: direction === DESC,
                [s.asc]: direction === ASC
              })}
              />
            </div>
            <div className={cn('attack', 'left', s.attack, s.tab)}>Attack</div>
            <div className='clear' />
          </div>
          {this._renderMembers(faction.id)}
        </div>
      </div>
    )
  }

  render() {
    const { currentFaction, opponentFaction } = this.props

    return (
      <div className={cn('faction-war', s.membersWrap)}>
        <div className='faction-names'>
          {this._renderFactionName(opponentFaction)}
          {this._renderFactionName(currentFaction)}
          <div className='clear' />
        </div>

        {this._renderMemberCont(opponentFaction)}
        {this._renderMemberCont(currentFaction)}

        <div className='clear' />
      </div>
    )
  }
}

export default WarMemberList
