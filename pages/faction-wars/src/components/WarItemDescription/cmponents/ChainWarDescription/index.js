import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import UserInfo from '@torn/shared/components/UserInfo'
import Loading from '../../../Loading'
import { getTimerValue, globalNumberFormat as gnf } from '../../../../utils'
import { MEDIATYPE_MOBILE, MIN_CHAIN_REPORT_VAL } from '../../../../constants'
import s from './index.cssmodule.scss'

const renderUserInfo = user => {
  const config = {
    showImages: user.honor !== '',
    useProgressiveImage: false,
    status: {
      mode: user.onlineStatus.status.toLowerCase()
    },
    user: {
      ID: user.userID,
      name: user.playername,
      imageUrl: user.honor,
      isSmall: true
    },
    faction: {
      ID: user.factionID,
      name: user.factiontag,
      rank: user.factionRank,
      imageUrl: user.tag
    },
    customStyles: {
      container: s.customContainer
    }
  }

  return <UserInfo {...config} />
}

const getIconBrightness = value => {
  switch (value) {
    case 3:
      return ''
    case 2:
      return 'faded'
    case 1:
      return 'transparent'
    case 0:
      return 'colourless'
    default:
      return 'colourless'
  }
}

class ChainWarDescription extends Component {
  componentDidMount() {
    const {
      loadWarDescData,
      box: { key }
    } = this.props

    loadWarDescData(key)
  }

  _renderReportLink() {
    const {
      box: {
        data: { chain }
      },
      userFactionID
    } = this.props

    if (chain.chain >= MIN_CHAIN_REPORT_VAL && userFactionID === chain.factionID) {
      return (
        <a href={`war.php?step=chainreport&chainID=${chain.ID}`} className='report-link'>
          View report
        </a>
      )
    }
    return null
  }

  render() {
    const { box, now, warDesc, warDescriptionLoaded, isDesktopContainerWidth, mediaType } = this.props
    const isRespect = isDesktopContainerWidth || mediaType !== MEDIATYPE_MOBILE

    // eslint-disable-next-line no-nested-ternary
    return warDescriptionLoaded[box.key] ? (
      warDesc.success !== false ? (
        <div className={cn(s.descWrap)}>
          {this._renderReportLink()}
          {warDesc.currentAttacks.length === 0 || (
            <div>
              <div className='chain-attacks-title'>Current outgoing attacks</div>
              <ul className='chain-attacks-list current-attacks'>
                {warDesc.currentAttacks.map(attack => {
                  const attacker = warDesc.users[attack.attackerID]
                  const defender = warDesc.users[attack.defenderID]

                  return (
                    // eslint-disable-next-line no-nested-ternary
                    <li key={attack.attackID} className={attack.myUserRow ? (attack.myAttack ? 'your' : 'enemy') : ''}>
                      <div className='attack-number'>--</div>
                      <div className='player left-player'>
                        <div className='member icons'>
                          {renderUserInfo(attacker)}
                          <div className='player-progress'>
                            <div className='player-progress-line' style={{ width: `${attacker.life}%` }} />
                          </div>
                        </div>
                      </div>
                      <div className='arrow'>
                        <i className='chain-arrow-icon' />
                      </div>
                      <div className='player right-player'>
                        <div className='member icons'>
                          {renderUserInfo(defender)}
                          <div className='player-progress'>
                            <div className='player-progress-line' style={{ width: `${defender.life}%` }} />
                          </div>
                        </div>
                      </div>
                      <div className='respect'>
                        {attack.attackersAmount} other attacker{attack.attackersAmount !== 1 ? 's' : ''}
                      </div>
                      <div className='bonuses'>
                        <i className='chain-bonus-icon fair-fight colourless' />
                        <i className='chain-bonus-icon war-hit colourless' />
                        <i className='chain-bonus-icon retaliation colourless' />
                        <i className='chain-bonus-icon overseas colourless' />
                        <i className='chain-bonus-icon group-attack colourless' />
                      </div>
                      <div className='time'>now</div>
                      <div className='link'>
                        <a href={`/loader2.php?sid=getInAttack&user2ID=${attack.defenderID}`}>Join</a>
                      </div>
                    </li>
                  )
                })}
              </ul>
            </div>
          )}

          <div
            className='chain-attacks-title'
            style={{ textAlign: warDesc.recentAttacks.length > 0 ? undefined : 'center' }}
          >
            {warDesc.recentAttacks.length > 0 ? 'Recent attacks' : 'No recent attacks recorded'}
          </div>
          {warDesc.recentAttacks.length === 0 || (
            <ul className='chain-attacks-list recent-attacks bottom-round'>
              {/* eslint-disable-next-line complexity */}
              {warDesc.recentAttacks.map(attack => {
                const timer = getTimerValue(attack.finishTimestamp, now)
                const first = warDesc.users[attack.firstUserID]
                const second = warDesc.users[attack.secondUserID]
                const enemyClass = attack.myAttack ? '' : 'enemy'

                return (
                  // eslint-disable-next-line no-nested-ternary
                  <li key={attack.attackID} className={attack.myUserRow ? (attack.myAttack ? 'your' : 'enemy') : ''}>
                    <div className='attack-number'>
                      {attack.respect > 0 && attack.chain > 0 ? `#${attack.chain}` : '--'}
                    </div>
                    <div className='player left-player'>
                      <div className='member icons'>{renderUserInfo(first)}</div>
                    </div>
                    <div className='arrow'>
                      {/* use class .enemy for red arrow icon */}
                      <i className={`chain-arrow-icon ${enemyClass}`} />
                    </div>
                    <div className='player right-player'>
                      {attack.stealthAttack === 2 && !attack.myAttack ? (
                        <div className='member icons' style={{ textAlign: 'center' }}>
                          Someone
                        </div>
                      ) : (
                        <div className='member icons'>{renderUserInfo(second)}</div>
                      )}
                    </div>
                    <div className='respect'>
                      {attack.raidID > 0 ? (
                        // eslint-disable-next-line no-nested-ternary
                        <span className={`value ${attack.respect > 0 ? 'green' : attack.respect < 0 ? 'red' : ''}`}>
                          {Math.abs(attack.respect) > 0 ?
                            `-${
                              attack.respect >= 0 ?
                                gnf(Math.abs(attack.respect), 2) :
                                gnf(Math.abs(attack.respect) / 0.25, 2)
                            }${isRespect ? ' respect' : ''}` :
                            attack.result}
                        </span>
                      ) : (
                        // eslint-disable-next-line no-nested-ternary
                        <span className={`value ${attack.respect > 0 ? 'green' : attack.respect < 0 ? 'red' : ''}`}>
                          {Math.abs(attack.respect) > 0 ?
                            (attack.respect >= 0 ? '+' : '-')
                              + gnf(Math.abs(attack.respect), 2)
                              + (isRespect ? ' respect' : '') :
                            attack.result}
                        </span>
                      )}
                    </div>
                    {/* use class .enemy for red colors.
                      Use classes .faded, .transparent, .colourless for brightness */}
                    {/* <div className="chain-bonus">10000</div> */}
                    {attack.chainBonus ? (
                      <div className='bonuses'>
                        <div className={`chain-bonus ${enemyClass}`} title='Incremental bonus'>
                          {gnf(attack.chain)}
                        </div>
                      </div>
                    ) : (
                      <div className='bonuses'>
                        <i
                          className={`chain-bonus-icon fair-fight ${enemyClass} ${getIconBrightness(attack.fairFight)}`}
                          title={
                            attack.bonusValues.fairFight > 1 ?
                              `Fair fight: x${attack.bonusValues.fairFight}` :
                              'Fair fight: None'
                          }
                        />
                        <i
                          className={`chain-bonus-icon war-hit ${enemyClass} ${getIconBrightness(attack.warBonus)}`}
                          title={
                            attack.bonusValues.warBonus > 1 ?
                              `War bonus: x${attack.bonusValues.warBonus}` :
                              'War bonus: None'
                          }
                        />
                        <i
                          className={`chain-bonus-icon retaliation ${enemyClass} ${getIconBrightness(attack.retal)}`}
                          title={
                            attack.bonusValues.retal > 1 ?
                              `Retaliation: x${attack.bonusValues.retal}` :
                              'Retaliation: None'
                          }
                        />
                        <i
                          className={`chain-bonus-icon overseas ${enemyClass} ${getIconBrightness(attack.overseas)}`}
                          title={
                            attack.bonusValues.overseas > 1 ?
                              `Overseas: x${attack.bonusValues.overseas}` :
                              'Overseas: None'
                          }
                        />
                        <i
                          className={`chain-bonus-icon group-attack ${enemyClass} ${getIconBrightness(
                            attack.groupAttack
                          )}`}
                          title={
                            attack.bonusValues.groupAttack > 1 ?
                              `Group attack: x${attack.bonusValues.groupAttack}` :
                              'Group attack: None'
                          }
                        />
                      </div>
                    )}
                    <div className='time'>{timer.shortValueFormat || '0 s'}</div>
                    <div className='link'>
                      <a href={`/loader.php?sid=attackLog&ID=${attack.publicID}`}>View</a>
                    </div>
                  </li>
                )
              })}
            </ul>
          )}
        </div>
      ) : (
        <div className='desc-wrap' style={{ display: 'block' }}>
          <div className='faction-war-info'>{warDesc.msg}</div>
        </div>
      )
    ) : (
      <div className='desc-wrap' style={{ display: 'block' }}>
        <div className='faction-war-info bottom-round'>
          <Loading pure />
        </div>
      </div>
    )
  }
}

ChainWarDescription.propTypes = {
  box: PropTypes.object.isRequired,
  now: PropTypes.number.isRequired,
  warDesc: PropTypes.object,
  warDescriptionLoaded: PropTypes.object.isRequired,
  loadWarDescData: PropTypes.func.isRequired,
  isDesktopContainerWidth: PropTypes.bool.isRequired,
  userFactionID: PropTypes.number.isRequired,
  mediaType: PropTypes.string.isRequired
}

export default ChainWarDescription
