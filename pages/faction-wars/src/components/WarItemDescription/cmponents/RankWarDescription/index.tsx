import React from 'react'
import cn from 'classnames'
import RankWarGraph from '@torn/shared/components/RankWarGraph'
import WarMemberList from '@torn/shared/components/WarMemberList'
import IMember from '@torn/shared/components/WarMemberList/interfaces/IMember'
import IFaction from '@torn/shared/components/WarMemberList/interfaces/IFaction'
import Loading from '../../../Loading'
import styles from './index.cssmodule.scss'
import { SECOND } from '../../../../constants'

const renderLoading = () => {
  return (
    <div className={cn('desc-wrap', styles.warDesc)}>
      <div className={cn('faction-war-info', 'bottom-round')}>
        <Loading />
      </div>
    </div>
  )
}

interface IProps {
  rank: {
    key: string
    currentFaction: IFaction
    completedAt: number
    timer: number
  }
  warDesc: {
    opponentFaction: IFaction
    members: IMember[]
    graph: {
      data: Array<Array<[number, number]>>
      respectRequirement: Array<Array<[number, number]>>
    }
    currentUser: {
      settings: {
        showImages: boolean
      }
    }
    success?: boolean
    message?: string
  }
  loadWarDescData: (key: string) => void
  mediaType: string
  now: number
  isProfileMode: boolean
  warDescriptionLoaded: {
    rank: boolean
  }
}

interface IState {
  isShowWarGraph: boolean
}

class RankWarDescription extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = {
      isShowWarGraph: false
    }
  }

  componentDidMount() {
    this.props.loadWarDescData(this.props.rank.key)
  }

  _handleOnClick = () => {
    this.setState(prevState => ({ isShowWarGraph: !prevState.isShowWarGraph }))
  }

  render() {
    if (!this.props.warDescriptionLoaded.rank) {
      return renderLoading()
    }

    if (this.props.warDesc.success === false) {
      return (
        <div className={cn('desc-wrap', 'raid-members-list', 'bottom-round', styles.warDesc)}>
          <div className={cn('faction-war-info', 'bottom-round')}>{this.props.warDesc.message}</div>
        </div>
      )
    }

    const {
      rank: { currentFaction, completedAt, timer },
      warDesc: {
        opponentFaction,
        members,
        graph: { data, respectRequirement },
        currentUser: {
          settings: { showImages }
        }
      },
      now,
      mediaType,
      isProfileMode
    } = this.props
    const { isShowWarGraph } = this.state
    const warNotStarted = timer * SECOND > now

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div className={cn('desc-wrap', styles.warDesc)}>
        <div className={cn('right', 'c-pointer', styles.graphIcon)}>
          {warNotStarted || (
            <div className='t-blue h icon-link' onClick={this._handleOnClick}>
              <span className={cn('m-right10', styles.text)}>View graph</span>
              <i className='raid-graph-icon' />
            </div>
          )}
        </div>
        <div className={cn('faction-war-info', styles.factionWarInfo, { [styles.hiddenGraph]: !isShowWarGraph })}>
          <span className={styles.descTitle}>
            <a className='t-blue h' href={`/factions.php?step=profile&ID=${opponentFaction.id}`}>
              <span dangerouslySetInnerHTML={{ __html: opponentFaction.name }} />
            </a>
            <span className='text'> vs </span>
            <a className='t-blue h' href={`/factions.php?step=profile&ID=${currentFaction.id}`}>
              <span dangerouslySetInnerHTML={{ __html: currentFaction.name }} />
            </a>
          </span>
          {!isShowWarGraph || (
            <div className='m-top20'>
              <RankWarGraph
                points={data}
                respectRequirement={respectRequirement}
                currentFactionName={currentFaction.name}
                opponentFactionName={opponentFaction.name}
                end={completedAt}
                mediaType={mediaType}
              />
            </div>
          )}
        </div>
        <WarMemberList
          members={members}
          currentFaction={currentFaction}
          opponentFaction={opponentFaction}
          isProfileMode={isProfileMode}
          scoreFieldName='score'
          mediaType={mediaType}
          showImages={showImages}
        />
      </div>
    )
  }
}

export default RankWarDescription
