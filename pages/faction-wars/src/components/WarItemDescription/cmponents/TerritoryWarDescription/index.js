import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Loading from '../../../Loading'
import Timer from '../Timer'
import WarGraph from '../WarGraph'
import Member from '../Member'

class TerritoryWarDescription extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowWarGraph: false
    }
  }

  componentDidMount() {
    this.props.loadWarDescData(this.props.war.key)
  }

  render() {
    const { myFactionInfo, war, warDescriptionLoaded, warDesc, now, isProfileMode } = this.props
    const { isShowWarGraph } = this.state
    const time = Math.round(now / 1000)

    return warDescriptionLoaded[war.key] ? (
      <div className='desc-wrap' style={{ display: 'block' }}>
        <div className='faction-war-info'>
          <div className='text raid-title' style={{ position: 'relative' }}>
            <a
              className='t-blue h'
              href={`/factions.php?step=profile&ID=${myFactionInfo.factionID}`}
              dangerouslySetInnerHTML={{ __html: myFactionInfo.factionName }}
            />
            <span> is {war.isMyAttack ? 'assaulting' : 'defending'} </span>
            <a className='t-blue h' href={`/city.php#terrName=${war.territoryName}`}>
              {' '}
              {war.territoryName}{' '}
            </a>
            <span className='t-block'>
              {' '}
              {war.isMyAttack ? 'which is currently held by ' : 'which is assaulted by '}
              <a
                className='t-blue h'
                href={`/factions.php?step=profile&ID=${war.enemyFaction.factionID}`}
                dangerouslySetInnerHTML={{
                  __html: war.enemyFaction.factionName
                }}
              />
            </span>
            <div
              className='right t-blue h right-icon-link right icon-link c-pointer'
              style={{ position: 'absolute', top: 0, right: 0 }}
              onClick={() => { this.setState({ isShowWarGraph: !isShowWarGraph }) }}
            >
              <span>View graph </span>
              <i className='raid-graph-icon m-left5' />
            </div>
          </div>
          <Timer timestamp={war.endDate} now={now} />
        </div>
        <div className='faction-war'>
          {!isShowWarGraph || (
            <div className='faction-war-info'>
              <WarGraph
                graphData={warDesc.warGraphData}
                startDate={war.startDate}
                endDate={war.endDate}
                maxPoints={war.maxPoints}
                slots={war.slots}
                score={war.score}
                membersDiff={
                  (war.myFaction.membersQuantity - war.enemyFaction.membersQuantity) * (war.isMyAttack ? 1 : -1)
                }
                scoreInc={
                  (war.myFaction.membersQuantity - war.enemyFaction.membersQuantity) * (war.isMyAttack ? 1 : -1)
                }
                isMyAttack={war.isMyAttack}
                timestamp={now}
              />
            </div>
          )}
          {war.isMyAttack ? (
            <div className='faction-names'>
              <span className='your ' dangerouslySetInnerHTML={{ __html: myFactionInfo.factionName }} />
              <span className='vs'> vs </span>
              <span className='enemy' dangerouslySetInnerHTML={{ __html: war.enemyFaction.factionName }} />
            </div>
          ) : (
            <div className='faction-names'>
              <span className='enemy' dangerouslySetInnerHTML={{ __html: war.enemyFaction.factionName }} />
              <span className='vs'> vs </span>
              <span className='your ' dangerouslySetInnerHTML={{ __html: myFactionInfo.factionName }} />
            </div>
          )}
          <ul className='factions-tabs' style={{ display: 'none' }}>
            <li className={`your ${war.isMyAttack ? 'left' : 'right'}`}>
              <a className='t-overflow' dangerouslySetInnerHTML={{ __html: myFactionInfo.factionName }} />
            </li>
            <li>VS</li>
            <li className={`enemy last ${war.isMyAttack ? 'right' : 'left'}`}>
              <a className='t-overflow' dangerouslySetInnerHTML={{ __html: war.enemyFaction.factionName }} />
            </li>
          </ul>

          <div className='tab-menu-cont cont-gray bottom-round'>
            <div className={`members-cont${isProfileMode && !war.isMyWar ? ' profile-mode' : ''}`}>
              <div className='title white-grad clearfix'>
                <div className='id left'>#</div>
                <div className='members left'>Members</div>
                <div className='level left'>Level</div>
                <div className='user-icons icons left'>Icons</div>
                <div className='points left'>Points</div>
                <div className='attack left'>Attack</div>
              </div>
              <ul className='members-list'>
                {warDesc.members.map((member, index) => {
                  return (
                    <Member
                      key={member.key}
                      member={member}
                      warID={war.warID}
                      isMyAttack={war.isMyAttack}
                      isMyWar={war.isMyWar}
                      index={index}
                      isLast={index === war.slots - 1}
                      isUserInList={warDesc.isUserInList}
                      time={time}
                      myFactionID={myFactionInfo.factionID}
                    />
                  )
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    ) : (
      <div className='desc-wrap' style={{ display: 'block' }}>
        <div className='faction-war-info bottom-round'>
          <Loading pure />
        </div>
      </div>
    )
  }
}

TerritoryWarDescription.propTypes = {
  myFactionInfo: PropTypes.object,
  war: PropTypes.object,
  warDesc: PropTypes.object,
  warDescriptionLoaded: PropTypes.object,
  loadWarDescData: PropTypes.func,
  now: PropTypes.number,
  isProfileMode: PropTypes.bool
}

export default TerritoryWarDescription
