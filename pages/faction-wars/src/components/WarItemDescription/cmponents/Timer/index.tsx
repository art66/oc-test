import React from 'react'
import { getTimerValue } from '../../../../utils'
import styles from './index.cssmodule.scss'

interface IProps {
  timestamp: number
  now: number
}

const Timer = ({ timestamp, now }: IProps) => {
  const { days, hours, minutes, seconds } = getTimerValue(new Date(now), new Date(timestamp))

  return (
    <span className={styles.timer}>
      <span>{days} day{days !== '01' ? 's' : ''}, </span>
      <span>{hours} hour{hours !== '01' ? 's' : ''}, </span>
      <span>{minutes} minute{minutes !== '01' ? 's' : ''} and </span>
      <span className={styles.fixWidth}>
        <span className={styles.num}>{seconds}</span>{' '}
        <span className={styles.secondsStr}>second{seconds !== '01' ? 's' : ''}</span>
      </span>
    </span>
  )
}

export default Timer
