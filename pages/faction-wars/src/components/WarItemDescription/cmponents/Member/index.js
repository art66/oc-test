import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Loading from '../../../Loading'
import { joinWar, closeMsg } from '../../../../modules'
import { globalNumberFormat } from '../../../../utils'
import { MEDIATYPE_DESKTOP } from '../../../../constants'
import MemberInfo from '../MemberInfo'

const renderTimeout = (timeout) => {
  if (timeout) {
    return (
      <>
        <i className='m-left10 timer-icon' />
        <span className='t-gray-9'> {timeout}</span>
      </>
    )
  }

  return null
}

class Member extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hovered: false
    }

    this.onHover = this.onHover.bind(this)
    this.onOver = this.onOver.bind(this)
    this.onJoinWar = this.onJoinWar.bind(this)
  }

  onHover() {
    const { isUserInList } = this.props

    this.setState({
      hovered: !isUserInList
    })
  }

  onOver() {
    this.setState({
      hovered: false
    })
  }

  onJoinWar() {
    const { warID, index, joinWar, isJoining } = this.props

    if (isJoining) {
      return false
    }
    joinWar(warID, index)
  }

  // eslint-disable-next-line complexity
  render() {
    const {
      isLast,
      member,
      activeSlotIndex,
      errorMsg,
      myFactionID,
      index,
      isUserInList,
      isFactionProfile,
      userFactionID,
      isMyWar,
      time,
      isDesktop,
      isJoining,
      closeMsg
    } = this.props
    const { hovered } = this.state
    const last = isLast ? ' last' : ''

    if (errorMsg && activeSlotIndex === index) {
      return (
        <li className={`msg-cont ${last}`}>
          <div className='t-red'>
            {errorMsg}
            {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
            <span className='t-blue m-left10 c-pointer' onClick={closeMsg}>
              Ok
            </span>
          </div>
        </li>
      )
    }

    const timeout = Math.max(member.waitTimestamp - time, 0)

    if (
      isJoining
      && activeSlotIndex === index
      && (member.isEmpty || (member.blocked && (timeout === 0 || member.isMyFactionBlock)))
    ) {
      return (
        <li className={`msg-cont  + ${last}`} onMouseEnter={this.onHover} onMouseLeave={this.onOver}>
          <Loading />
        </li>
      )
    }

    if (member.isEmpty || (member.blocked
      && (myFactionID === member.factionID && userFactionID === member.factionID || timeout === 0))
    ) {
      return (
        <li className='join' onMouseEnter={this.onHover} onMouseLeave={this.onOver}>
          <div className={`id left  + ${last}`}>#{index + 1}</div>
          <div className='text-center'>
            {/* eslint-disable-next-line react/destructuring-assignment */}
            {hovered
            && !isUserInList
            && !isJoining
            && (((!isFactionProfile || isMyWar) && !errorMsg) || (member.blocked === true && timeout === 0)) ? (
              // eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/anchor-is-valid
              <a className='join-link t-blue h c-pointer' onClick={this.onJoinWar}>
                JOIN {renderTimeout(timeout)}
              </a>
              ) : (
              // eslint-disable-next-line jsx-a11y/anchor-is-valid
              <a className='join-link t-blue h'>AVAILABLE {renderTimeout(timeout)}</a>
              )}
          </div>
        </li>
      )
    }

    if (member.blocked === true) {
      return (
        <li className='timer-wrap' onMouseEnter={this.onHover} onMouseLeave={this.onOver}>
          <span className={`id left ${last}`}>#{index + 1}</span>
          <div className='timer'>
            {renderTimeout(timeout)}
          </div>
        </li>
      )
    }

    return (
      <li className={(member.isMyFaction ? 'your ' : 'enemy ') + last}>
        <div
          className={
            `row-animation-new ${
              member.isAttackMember ? 'to-right ' : 'to-left '
            }${member.isMyFaction ? 'your ' : 'enemy '}`
          }
        />
        <div className='id left'>#{index + 1}</div>

        <MemberInfo member={member} isDesktop={isDesktop} />

        <div className='points left'>{globalNumberFormat(Math.max(time - member.joinTimestamp, 0))}</div>

        <div className={`attack left${member.factionProfile ? ' hide' : ''}`}>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a
            className={`t-blue h${member.isMyFaction ? '' : ' c-pointer'}`}
            style={{
              cursor: member.isMyFaction ? 'auto' : 'pointer'
            }}
            href={member.isMyFaction ? '' : `loader2.php?sid=getInAttack&user2ID=${member.userID}`}
          >
            {' '}
            Attack{' '}
          </a>
        </div>
        <div className='clear' />
      </li>
    )
  }
}

Member.propTypes = {
  member: PropTypes.object,
  warID: PropTypes.number,
  myFactionID: PropTypes.number,
  activeSlotIndex: PropTypes.number,
  isUserInList: PropTypes.bool,
  isLast: PropTypes.bool,
  isDesktop: PropTypes.bool,
  isSuccess: PropTypes.bool,
  joinWar: PropTypes.func,
  closeMsg: PropTypes.func,
  isJoining: PropTypes.bool,
  errorMsg: PropTypes.string,
  isFactionProfile: PropTypes.bool,
  userFactionID: PropTypes.number,
  isMyWar: PropTypes.bool,
  index: PropTypes.number,
  time: PropTypes.number
}

const mapStateToProps = state => ({
  errorMsg: state.app.msg,
  isSuccess: state.app.success,
  isDesktop: state.browser.mediaType === MEDIATYPE_DESKTOP,
  isJoining: state.app.isJoining,
  activeSlotIndex: state.app.activeSlotIndex,
  isFactionProfile: state.app.isProfileMode,
  userFactionID: state.app.userFactionID
})

const mapActionsToProps = {
  joinWar,
  closeMsg
}

export default connect(mapStateToProps, mapActionsToProps)(Member)
