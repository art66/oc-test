import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import TerritoryWarDescription from './cmponents/TerritoryWarDescription'
import ChainWarDescription from './cmponents/ChainWarDescription'
import RaidWarDescription from './cmponents/RaidWarDescription'
import RankWarDescription from './cmponents/RankWarDescription'
import { getWarDescription, requestSurrender, ceaseRaid, poolMembers } from '../../modules'
import { isManualDesktopMode } from '../../utils'
import { MEDIATYPE_DESKTOP, RANK_BOX_TYPE, TERRITORY_BOX_TYPE, CHAIN_BOX_TYPE, SECOND } from '../../constants'

class WarItemDescription extends Component {
  componentDidUpdate() {
    const { params, warDesc, warDescriptionLoaded, now } = this.props

    if (params.key === RANK_BOX_TYPE && warDescriptionLoaded[RANK_BOX_TYPE] && warDesc.members !== undefined) {
      const members = warDesc.members.filter(member => {
        const diff = now - member.status.updateAt * SECOND

        return member.status.updateAt && diff >= 0 && diff <= 2
      })

      if (members.length > 0) {
        this.props.poolMembers(members.map(member => member.userID))
      }
    }
  }

  render() {
    const {
      wars,
      myFactionInfo,
      warDesc,
      warDescriptionLoaded,
      raidRequest,
      now,
      isProfileMode,
      isDesktop,
      isDesktopContainerWidth,
      mediaType,
      userFactionID,
      params
    } = this.props

    const war = wars.filter(item => item.key === params.key)[0]

    if (war.type === TERRITORY_BOX_TYPE) {
      return (
        <TerritoryWarDescription
          myFactionInfo={myFactionInfo}
          war={war}
          warDesc={warDesc}
          warDescriptionLoaded={warDescriptionLoaded}
          loadWarDescData={this.props.getWarDescription}
          now={now}
          isProfileMode={isProfileMode}
        />
      )
    }

    if (war.type === CHAIN_BOX_TYPE) {
      return (
        <ChainWarDescription
          box={war}
          warDesc={warDesc}
          warDescriptionLoaded={warDescriptionLoaded}
          now={now}
          isDesktopContainerWidth={isDesktopContainerWidth}
          userFactionID={userFactionID}
          mediaType={mediaType}
          loadWarDescData={this.props.getWarDescription}
        />
      )
    }

    if (war.type === RANK_BOX_TYPE) {
      return (
        <RankWarDescription
          now={now}
          rank={war}
          warDesc={warDesc}
          mediaType={mediaType}
          isProfileMode={isProfileMode}
          warDescriptionLoaded={warDescriptionLoaded}
          loadWarDescData={this.props.getWarDescription}
        />
      )
    }

    return (
      <RaidWarDescription
        myFactionInfo={myFactionInfo}
        raid={war}
        warDesc={warDesc}
        warDescriptionLoaded={warDescriptionLoaded}
        now={now}
        isDesktop={isDesktop}
        loadWarDescData={this.props.getWarDescription}
        requestSurrender={this.props.requestSurrender}
        raidRequest={raidRequest}
        ceaseRaid={this.props.ceaseRaid}
      />
    )
  }
}

WarItemDescription.propTypes = {
  params: PropTypes.object.isRequired,
  myFactionInfo: PropTypes.object,
  wars: PropTypes.array.isRequired,
  warDescriptionLoaded: PropTypes.object.isRequired,
  warDesc: PropTypes.object,
  now: PropTypes.number.isRequired,
  isProfileMode: PropTypes.bool.isRequired,
  raidRequest: PropTypes.object.isRequired,
  isDesktop: PropTypes.bool.isRequired,
  isDesktopContainerWidth: PropTypes.bool.isRequired,
  getWarDescription: PropTypes.func.isRequired,
  requestSurrender: PropTypes.func.isRequired,
  ceaseRaid: PropTypes.func.isRequired,
  userFactionID: PropTypes.number.isRequired,
  mediaType: PropTypes.string.isRequired,
  poolMembers: PropTypes.func
}

const mapStateToProps = state => ({
  mediaType: isManualDesktopMode() ? 'desktop' : state.browser.mediaType,
  wars: state.app.wars,
  myFactionInfo: state.app.myFactionInfo,
  warDescriptionLoaded: state.app.warDescriptionLoaded,
  warDesc: state.app.warDesc,
  now: state.app.now,
  isProfileMode: state.app.isProfileMode,
  raidRequest: state.app.raidRequest,
  isDesktopContainerWidth: state.app.isDesktopContainerWidth,
  userFactionID: state.app.userFactionID,
  isDesktop: state.browser.mediaType === MEDIATYPE_DESKTOP
})

const mapActionsToProps = {
  getWarDescription,
  requestSurrender,
  ceaseRaid,
  poolMembers
}

export default connect(mapStateToProps, mapActionsToProps)(WarItemDescription)
