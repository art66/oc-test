import React, { PropsWithChildren, FC } from 'react'
import cn from 'classnames'
import { TWarResult } from '../../../interfaces/IWarNotification'
import commonStyles from '../index.cssmodule.scss'

interface IProps {
  id: number
  result: TWarResult
}

export const SVGTitle: FC<IProps> = (props: PropsWithChildren<IProps>) => {
  return (
    <svg className={cn(commonStyles.svgTitle)} xmlns='http://www.w3.org/2000/svg' version='1.1'>
      <defs>
        <linearGradient id={`ranked-war-notification-${props.id}-win`} x1='0%' y1='0%' x2='0%' y2='100%'>
          <stop offset='0%' stopColor='#D2FF4D' stopOpacity={1} />
          <stop offset='100%' stopColor='#698C00' stopOpacity={1} />
        </linearGradient>
        <linearGradient id={`ranked-war-notification-${props.id}-lose`} x1='0%' y1='0%' x2='0%' y2='100%'>
          <stop offset='0%' stopColor='#FF7A4D' stopOpacity={1} />
          <stop offset='100%' stopColor='#D93600' stopOpacity={1} />
        </linearGradient>
      </defs>
      <text
        className={cn(commonStyles.titleText, commonStyles[props.result])}
        fill={`url(#ranked-war-notification-${props.id}-${props.result})`}
        textAnchor='middle'
        dominantBaseline='middle'
        x='50%'
        y='50%'
      >
        {props.children}
      </text>
    </svg>
  )
}

export default SVGTitle
