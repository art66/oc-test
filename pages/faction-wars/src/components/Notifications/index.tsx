import React from 'react'
import IWarNotification from '../../interfaces/IWarNotification'
import RankedWarNotification from './RankedWarNotification'
import commonStyles from './index.cssmodule.scss'

interface IProps {
  notifications: IWarNotification[]
}

const Notifications = (props: IProps) => {
  return (
    <>
      <>{props.notifications?.length ? <hr className='page-head-delimiter m-bottom10 m-top10' /> : null}</>
      <div className={commonStyles.notificationsList}>
        {props.notifications.map(notification => {
          switch (notification.type) {
            case 'rankedWarring':
              return <RankedWarNotification key={notification.id} notification={notification} />
            default:
              return null
          }
        })}
      </div>
    </>
  )
}

export default Notifications
