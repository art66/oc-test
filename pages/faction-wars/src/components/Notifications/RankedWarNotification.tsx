import React from 'react'
import cn from 'classnames'
import { useDispatch } from 'react-redux'
import Tooltip from '@torn/shared/components/Tooltip'
import IWarNotification from '../../interfaces/IWarNotification'
import WarringTiers from '../../../../../shared/components/WarringTiers'
import Close from '../../icons/Close'
import SVGTitle from './common/SVGTitle'
import { closeNotification } from '../../actions'
import commonStyles from './index.cssmodule.scss'
import s from './RankedWarNotifications.cssmodule.scss'

interface IProps {
  notification: IWarNotification
}

const RankedWarNotification = (props: IProps) => {
  const dispatch = useDispatch()
  const {
    result,
    currentFaction,
    opponentFaction,
    id,
    reportLink,
    reward: { items, points, respect, received, message }
  } = props.notification

  const getWinner = () => (result === 'win' ? currentFaction : opponentFaction)
  const getLoser = () => (result === 'lose' ? currentFaction : opponentFaction)

  const getUrlToFactionProfile = ID => `/factions.php?step=profile&ID=${ID}`

  const close = () => dispatch(closeNotification(props.notification.id))

  return (
    <div className={commonStyles.notification}>
      <div className={cn(commonStyles.header, s.rankedWarHeader)}>
        <div className={commonStyles.title}>
          <SVGTitle id={id} result={result}>
            {result === 'win' ? 'RANKED WAR VICTORY' : 'RANKED WAR DEFEAT'}
          </SVGTitle>
        </div>
        <div className={cn(commonStyles.description)}>
          <a className={commonStyles.link} href={getUrlToFactionProfile(getWinner().id)}>
            {getWinner().name}
          </a>
          {' has defeated '}
          <a className={commonStyles.link} href={getUrlToFactionProfile(getLoser().id)}>
            {getLoser().name}
          </a>
        </div>
      </div>
      <div className={s.warringTiersWrapper}>
        <WarringTiers size='big' factionId={currentFaction.id} warringTiersID={`ranked-war-notification-${id}`} />
      </div>
      <div className={cn(commonStyles.result, s.rankedWarResult, { [s.noItems]: !items?.length })}>
        {received ? (
          <>
            <div className={s.itemsWrapper}>
              {items.map(item => {
                return (
                  <div key={item.id} id={`rank-notification-${item.id}`} className={s.item}>
                    <img src={item.img} alt={item.name} />
                    <div className={s.amount}>{item.amount}</div>
                    <Tooltip parent={`rank-notification-${item.id}`} postion='top' arrow='center'>
                      {item.name}
                    </Tooltip>
                  </div>
                )
              })}
            </div>
            <div className={s.pointsWrapper}>
              <div className={s.points} id='ranked-wars-notifications-points'>
                <i className={s.icon} />
                <span className={s.value}>{points.toLocaleString('en-US')}</span>
                <Tooltip parent='ranked-wars-notifications-points' postion='top' arrow='center'>
                  Points
                </Tooltip>
              </div>
              <div className={s.respect} id='ranked-wars-notifications-respect'>
                <i className={s.icon} />
                <span className={s.value}>{respect.toLocaleString('en-US')}</span>
                <Tooltip parent='ranked-wars-notifications-respect' postion='top' arrow='center'>
                  Respect
                </Tooltip>
              </div>
            </div>
          </>
        ) : (
          <div className={s.errorMessage}>{message}</div>
        )}

        <div className={cn(commonStyles.reportWrapper, s.rankedWarReportWrapper)}>
          <a className={commonStyles.link} href={reportLink}>
            View report
          </a>
        </div>
      </div>
      <button type='button' aria-label='Close' className={commonStyles.closeBtn} onClick={close}>
        <Close />
      </button>
    </div>
  )
}

export default RankedWarNotification
