import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Title from '../Title'
import WarList from '../WarList'
import Loading from '../Loading'
import Notifications from '../Notifications'
import FactionInfo from '../FactionInfo'
import { fetchNotifications, fetchFactionProfileInfo } from '../../actions'
import { getWarData, updateNow } from '../../modules'
import { getCurrentTimestamp, isActiveMainTab, isManualDesktopMode } from '../../utils'

class Main extends Component {
  constructor(props) {
    super(props)

    this.state = {
      counter: 0
    }
  }

  componentDidMount() {
    const {
      params: { key: activeWarID },
      isProfileMode
    } = this.props

    this.updateWarStateIntervalId = setInterval(this._startTimer, 1000)
    this.props.getWarData(activeWarID, false)

    if (isProfileMode) {
      this.props.fetchFactionProfileInfo()
    }

    if (!isProfileMode) {
      this.props.fetchNotifications()
    }
  }

  componentWillUnmount() {
    this._stopTimer()
  }

  _setStateCounter = (step = 'default') => {
    const isDefault = !step || step === 'default'

    this.setState(prevState => ({
      counter: isDefault ? 0 : prevState.counter + 1
    }))
  }

  _startTimer = () => {
    const {
      params: { key: activeWarID }
    } = this.props
    const { counter } = this.state

    try {
      this.props.updateNow(getCurrentTimestamp())
    } finally {
      this._setStateCounter('increment')

      if (counter >= 5 && isActiveMainTab()) {
        try {
          this.props.getWarData(activeWarID, true)
        } finally {
          this._setStateCounter('default')
        }
      }
    }
  }

  _stopTimer = () => clearInterval(this.updateWarStateIntervalId)

  render() {
    const {
      children,
      params,
      warDataLoaded,
      warMsg,
      isEmpty,
      notifications,
      isProfileMode,
      mediaType,
      factionMembers
    } = this.props

    if (!warDataLoaded) {
      return (
        <>
          <hr className='page-head-delimiter m-top10 m-bottom10' />
          <Loading />
        </>
      )
    }

    return (
      <div>
        <hr className='page-head-delimiter m-top10' />
        <Title warMsg={warMsg} />
        {!isEmpty && <WarList children={children} params={params} />}
        {!isProfileMode && notifications?.length ? <Notifications notifications={notifications} /> : null}
        {isProfileMode && factionMembers ? <FactionInfo mediaType={mediaType} /> : null}
      </div>
    )
  }
}

Main.propTypes = {
  children: PropTypes.element,
  params: PropTypes.object,
  getWarData: PropTypes.func,
  updateNow: PropTypes.func,
  warDataLoaded: PropTypes.bool,
  warMsg: PropTypes.object,
  isEmpty: PropTypes.bool,
  fetchNotifications: PropTypes.func,
  notifications: PropTypes.array,
  isProfileMode: PropTypes.bool,
  fetchFactionProfileInfo: PropTypes.func,
  mediaType: PropTypes.string,
  factionMembers: PropTypes.array
}

const mapStateToProps = state => ({
  warDataLoaded: state.app.warDataLoaded,
  warMsg: state.app.warMsg,
  isEmpty: state.app.isEmpty,
  notifications: state.app.notifications,
  isProfileMode: state.app.isProfileMode,
  mediaType: isManualDesktopMode() ? 'desktop' : state.browser.mediaType,
  factionMembers: state.app.members
})

const mapActionsToProps = {
  getWarData,
  updateNow,
  fetchNotifications,
  fetchFactionProfileInfo
}

export default connect(mapStateToProps, mapActionsToProps)(Main)
