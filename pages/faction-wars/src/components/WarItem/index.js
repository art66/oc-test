import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ITEMS_IN_ROW } from '../../constants'
import TerritoryWar from '../TerritoryWar'
import ChainWar from '../ChainWar'
import RaidWar from '../RaidWar'
import RankWar from '../RankWar'

class WarItem extends Component {
  render() {
    const {
      item,
      activeWarBox,
      myFactionInfo,
      mediaType,
      warBoxIndex,
      now,
      isProfileMode,
      rankBoxAnimation,
      rankBoxTargetAnimation
    } = this.props

    const indexInRow = warBoxIndex % ITEMS_IN_ROW

    const className = (typeof item.data === 'object' && item.data !== null && item.data.className) || null

    const sharedProps = {
      first: indexInRow === 0,
      last: (indexInRow + 1) % 3 === 0,
      active: activeWarBox === item.key
    }

    if (item.type === 'territory') {
      return (
        <TerritoryWar
          war={item}
          myFactionInfo={myFactionInfo}
          mediaType={mediaType}
          now={now}
          activeWarBox={activeWarBox}
          className={className}
          {...sharedProps}
        />
      )
    }

    if (item.type === 'chain') {
      return (
        <ChainWar
          chainBox={item}
          mediaType={mediaType}
          now={now}
          activeWarBox={activeWarBox}
          className={className}
          isProfileMode={isProfileMode}
          {...sharedProps}
        />
      )
    }

    if (item.type === 'rank') {
      return (
        <RankWar
          rank={item}
          now={now}
          activeWarBox={activeWarBox}
          className={className}
          isProfileMode={isProfileMode}
          mediaType={mediaType}
          rankBoxAnimation={rankBoxAnimation}
          rankBoxTargetAnimation={rankBoxTargetAnimation}
          {...sharedProps}
        />
      )
    }

    return (
      <RaidWar
        raid={item}
        activeWarBox={activeWarBox}
        className={className}
        isProfileMode={isProfileMode}
        {...sharedProps}
      />
    )
  }
}

WarItem.propTypes = {
  item: PropTypes.object,
  activeWarBox: PropTypes.string,
  myFactionInfo: PropTypes.object,
  mediaType: PropTypes.string,
  warBoxIndex: PropTypes.number,
  now: PropTypes.number,
  isProfileMode: PropTypes.bool,
  rankBoxAnimation: PropTypes.object,
  rankBoxTargetAnimation: PropTypes.object
}

export default WarItem
