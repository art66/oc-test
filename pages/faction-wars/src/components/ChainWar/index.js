import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { globalNumberFormat as gnf, getTimerValue } from '../../utils'
import { SECOND, CHAIN_REPORT_URL, CHAIN_COOLDOWN_MULT } from '../../constants'
import WarListItem from '../WarList/WarListItem'

const header = {
  'no-active': 'No active chain',
  blue: 'Chain in cooldown'
}

const getCooldownEnd = (chain, now) => {
  const cEnd = chain.end + CHAIN_COOLDOWN_MULT * SECOND * chain.chain

  if (chain.end > now) {
    return 0
  } else if (now > cEnd) {
    return -1
  }
  return cEnd
}

class ChainWar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentTap: ''
    }
  }

  getLinkPath() {
    const { chainBox, activeWarBox, isProfileMode, now } = this.props
    const { chain } = chainBox.data

    if (isProfileMode) {
      if (getCooldownEnd(chain, now) > now) {
        return `${CHAIN_REPORT_URL}${chain.ID}`
      }
      return ''
    }

    if (activeWarBox === chainBox.key) {
      return '#/'
    }

    return `#/war/${chainBox.key}`
  }

  _handleIconTap(event, tap) {
    const { currentTap } = this.state
    const { mediaType } = this.props

    if (currentTap !== tap && mediaType !== 'desktop') {
      event.preventDefault()
    }
    this.setState({ currentTap: currentTap === tap ? '' : tap })
  }

  render() {
    const { chainBox, now, first, active, className } = this.props
    const { chain, stats } = chainBox.data

    let chainClassName = chainBox.data.className
    const noActive = chainClassName === 'no-active'

    let chainVal = chain.chain
    const time = getTimerValue(now, chain.end)
    const cooldownEnd = getCooldownEnd(chain, now)
    const cooldownTime = getTimerValue(now, cooldownEnd)

    if (cooldownEnd === -1) {
      chainClassName = 'no-active'
      chainVal = 0
    }
    const extendedClassName = `${className} ${chainClassName}`

    return (
      <WarListItem
        first={first}
        green={chainBox.isTerritorial && chainBox.isMyAttack}
        red={chainBox.isTerritorial && !chainBox.isMyAttack}
        blue={cooldownEnd > 0}
        active={active}
        className={extendedClassName}
        link={this.getLinkPath()}
      >
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <div className='chain-box'>
          <div className='chain-box-title-block'>
            <span className='chain-box-title'>{header[chainClassName] || 'Chain active'}</span>
            <i className='chain-war-icon' />
          </div>
          <div className='chain-box-stats-block'>
            <div className='chain-box-general-info'>
              <span className='chain-box-top-stat'>{gnf(noActive ? 0 : chain.respect, 2)}</span>
              <span className='chain-box-center-stat'>{gnf(chainVal)}</span>
              <span className='chain-box-timeleft'>
                {cooldownEnd > 0 ?
                  cooldownTime.shortFormat :
                  `${(time.hours !== '00' ? `${time.hours}:` : '') + time.minutes}:${time.seconds}`}
              </span>
            </div>
            <ul className='chain-box-stats-list'>
              <li>
                <i
                  className='icon-chain-box-stat-members'
                  title='Members involved'
                  onClick={e => {
                    this._handleIconTap(e, 'members')
                  }}
                />
                {gnf(stats.members)}
              </li>
              <li>
                <i
                  className='icon-chain-box-stat-targets-hit'
                  title='Targets hit'
                  onClick={e => {
                    this._handleIconTap(e, 'targets hit')
                  }}
                />
                {gnf(stats.targets)}
              </li>
              <li>
                <i
                  className='icon-chain-box-stat-war-hits'
                  title='War hits'
                  onClick={e => {
                    this._handleIconTap(e, 'war hits')
                  }}
                />
                {gnf(stats.hits)}
              </li>
              <li>
                <i
                  className='icon-chain-box-stat-best-hit'
                  title='Best hit'
                  onClick={e => {
                    this._handleIconTap(e, 'best hit')
                  }}
                />
                {gnf(stats.bestHit)}
              </li>
            </ul>
            <ul className='chain-box-stats-list'>
              <li>
                <i
                  className='icon-chain-box-stat-assists'
                  title='Assists'
                  onClick={e => {
                    this._handleIconTap(e, 'assists')
                  }}
                />
                {gnf(stats.assists)}
              </li>
              <li>
                <i
                  className='icon-chain-box-stat-retaliations'
                  title='Retaliations'
                  onClick={e => {
                    this._handleIconTap(e, 'retaliations')
                  }}
                />
                {gnf(stats.retals)}
              </li>
              <li>
                <i
                  className='icon-chain-box-stat-overseas-hits'
                  title='Overseas hits'
                  onClick={e => {
                    this._handleIconTap(e, 'overseas hits')
                  }}
                />
                {gnf(stats.overseas)}
              </li>
              <li>
                <i
                  className='icon-chain-box-stat-losses'
                  title='Losses'
                  onClick={e => {
                    this._handleIconTap(e, 'losses')
                  }}
                />
                {gnf(stats.losses)}
              </li>
            </ul>
          </div>
        </div>
      </WarListItem>
    )
  }
}

ChainWar.propTypes = {
  chainBox: PropTypes.object.isRequired,
  now: PropTypes.number.isRequired,
  activeWarBox: PropTypes.string,
  className: PropTypes.string.isRequired,
  isProfileMode: PropTypes.bool.isRequired,
  first: PropTypes.bool.isRequired,
  active: PropTypes.bool.isRequired,
  mediaType: PropTypes.string.isRequired
}

export default ChainWar
