import React from 'react'
import Preloader from '@torn/shared/components/Preloader'
import styles from './index.cssmodule.scss'

const Loading = () => {
  return (
    <div className={styles.preloaderWrapper}>
      <Preloader />
    </div>
  )
}

export default Loading
