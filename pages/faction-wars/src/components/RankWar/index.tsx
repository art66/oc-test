import React from 'react'
import cn from 'classnames'
import { globalNumberFormat as nf } from '../../utils'
import getTimerValue from '../../utils/getTimerValue'
import WarListItem from '../WarList/WarListItem'
import styles from './index.cssmodule.scss'
import { SECOND } from '../../constants'

interface IFaction {
  id: number
  name: string
  score: number
  tagImageUrl: string
  okayMemberAmount: number
  totalMemberAmount: number
  factionRank?: string
}

interface IRank {
  opponentFaction: IFaction
  currentFaction: IFaction
  rank: string
  initRespectRequirement: number
  respectRequirement: number
  timer: number
  completedAt: number
  winner: number
  warId: number
  key: string
}

interface IProps {
  rank: IRank
  now: number
  first: boolean
  last: boolean
  active: boolean
  activeWarBox: string
  rankBoxAnimation: {
    isActive: boolean
    factionID?: number
    currentScore?: number
    opponentScore?: number
  }
  rankBoxTargetAnimation: {
    isActive: boolean
    animationTarget: number
  }
  isProfileMode: boolean
  className: string
  mediaType: string
}

const PROGRESS_WIDTH = {
  desktop: 83,
  tablet: 149,
  mobile: 116
}
const SLIDER_WIDTH = {
  desktop: 45,
  tablet: 77,
  mobile: 61
}
const SLIDER_OFFSET = 6

class RankWar extends React.Component<IProps> {
  getLinkPath() {
    const { rank, activeWarBox } = this.props

    if (activeWarBox === rank.key) {
      return '#/'
    }

    return '#/war/rank'
  }

  getRespectRequirement() {
    const {
      rank: { respectRequirement },
      rankBoxTargetAnimation: { isActive, animationTarget }
    } = this.props

    return isActive ? animationTarget || respectRequirement : respectRequirement
  }

  _renderProgressBar(sliderPosition, isCurrent) {
    const {
      rankBoxAnimation: { isActive, currentScore, opponentScore },
      mediaType,
      rank: { opponentFaction, currentFaction, respectRequirement }
    } = this.props

    const cScore = isActive ? currentScore : currentFaction.score
    const oScore = isActive ? opponentScore : opponentFaction.score

    if (isCurrent && cScore <= oScore) {
      return null
    }

    if (!isCurrent && oScore <= cScore) {
      return null
    }

    if (respectRequirement === 0) {
      return null
    }

    const score = Math.min(Math.abs(Math.floor(oScore) - Math.floor(cScore)), respectRequirement)
    const progressBarPosition =
      ((PROGRESS_WIDTH[mediaType] - sliderPosition) * score) / respectRequirement - SLIDER_OFFSET
    const barWidth = Math.max(progressBarPosition, 0)
    const positionStyle = isCurrent ? { left: progressBarPosition } : { right: progressBarPosition }

    return (
      <>
        <div
          className={cn(styles.progressBar, { [styles.green]: isCurrent, [styles.red]: !isCurrent })}
          style={{ width: barWidth }}
        />
        <div
          className={cn(styles.ball, { [styles.green]: isCurrent, [styles.red]: !isCurrent })}
          style={positionStyle}
        />
      </>
    )
  }

  _renderZeroProgressBar() {
    const {
      rank: { opponentFaction, currentFaction }
    } = this.props

    if (opponentFaction.score === 0 && currentFaction.score === 0) {
      return <div className={cn(styles.ball, styles.zero)} />
    }
    return null
  }

  _renderTimer() {
    const {
      now,
      rank: { timer, completedAt }
    } = this.props
    let timerValue

    if (completedAt > 0) {
      timerValue = getTimerValue(new Date(timer * SECOND), new Date(completedAt * SECOND))
    } else if (timer * SECOND < now) {
      timerValue = getTimerValue(new Date(timer * SECOND), new Date(now))
    } else {
      timerValue = getTimerValue(new Date(now), new Date(timer * SECOND))
    }

    /* eslint-disable react/no-array-index-key */
    return (
      <>
        {timerValue.shortFullFormat.split('').map((char, index) => (
          <span key={index}>{char}</span>
        ))}
      </>
    )
  }

  _renderOpponentTarget() {
    const {
      now,
      rank: { opponentFaction, currentFaction, completedAt, timer, winner }
    } = this.props

    if (completedAt > 0) {
      const text =
        (!winner && opponentFaction.score > currentFaction.score) || winner === opponentFaction.id ? 'WINNER' : 'LOSER'

      return <span className={styles.resultText}>{text}</span>
    }

    if (timer * SECOND > now) {
      return null
    }

    return (
      <>
        <i className={cn('raid-target-icon', 'm-right5', styles.targetIcon)} />
        {opponentFaction.okayMemberAmount} / {opponentFaction.totalMemberAmount}
      </>
    )
  }

  _renderCurrentTarget() {
    const {
      now,
      rank: { opponentFaction, currentFaction, completedAt, timer, winner }
    } = this.props

    if (completedAt > 0) {
      const text =
        (!winner && currentFaction.score > opponentFaction.score) || winner === currentFaction.id ? 'WINNER' : 'LOSER'

      return <span className={styles.resultText}>{text}</span>
    }

    if (timer * SECOND > now) {
      return null
    }

    return (
      <>
        {currentFaction.okayMemberAmount} / {currentFaction.totalMemberAmount}
        <i className={cn('raid-check-icon', 'm-left5', styles.targetIcon)} />
      </>
    )
  }

  render() {
    const {
      className,
      first,
      last,
      active,
      mediaType,
      rankBoxAnimation: { isActive, factionID, currentScore, opponentScore },
      rank,
      now,
      rank: { currentFaction, opponentFaction, initRespectRequirement, respectRequirement, timer }
    } = this.props

    const sliderPosition =
      (1 - respectRequirement / initRespectRequirement) * (PROGRESS_WIDTH[mediaType] - SLIDER_WIDTH[mediaType])
      + SLIDER_OFFSET
    const waiting = timer * SECOND > now
    const currentScoreDisplay = Math.floor(isActive ? currentScore : currentFaction.score)
    const opponentScoreDisplay = Math.floor(isActive ? opponentScore : opponentFaction.score)
    const green =
      !waiting && ((!rank.winner && currentFaction.score >= opponentFaction.score) || rank.winner === currentFaction.id)
    const red =
      !waiting && ((!rank.winner && currentFaction.score < opponentFaction.score) || rank.winner === opponentFaction.id)

    /* eslint-disable react/no-danger */
    /* eslint-disable jsx-a11y/control-has-associated-label */
    return (
      <WarListItem
        first={first}
        last={last}
        green={green}
        red={red}
        blue={false}
        active={active}
        className={className}
        link={this.getLinkPath()}
      >
        <div className={styles.rankBox} data-warid={rank.warId}>
          <div className={styles.titleBlock}>
            <div className={styles.nameWp}>
              <a
                className={cn(styles.linkColor, styles.opponentFactionName)}
                href={`/factions.php?step=profile&ID=${opponentFaction.id}`}
                dangerouslySetInnerHTML={{ __html: opponentFaction.name }}
              />
              <span> vs </span>
              <a
                className={cn(styles.linkColor, styles.currentFactionName)}
                href={`/factions.php?step=profile&ID=${currentFaction.id}`}
                dangerouslySetInnerHTML={{ __html: currentFaction.name }}
              />
            </div>
            <i className={cn(styles.rankWarIcon, styles[rank.rank])} />
          </div>
          <ul className={cn(styles.statsBox, { [styles.waiting]: waiting })}>
            <li className={styles.scoreBlock}>
              <span
                className={cn('left', styles.scoreText, styles.opponentFaction, {
                  [styles.animation]: isActive && opponentFaction.id === factionID
                })}
              >
                {nf(opponentScoreDisplay)}
              </span>
              <span className={cn(styles.target, { [styles.green]: green, [styles.red]: red })}>
                LEAD TARGET
                <br />
                {nf(
                  Math.min(Math.floor(Math.abs(currentScoreDisplay - opponentScoreDisplay)), respectRequirement)
                )} / {nf(this.getRespectRequirement())}
              </span>
              <span
                className={cn('right', styles.scoreText, styles.currentFaction, {
                  [styles.animation]: isActive && currentFaction.id === factionID
                })}
              >
                {nf(currentScoreDisplay)}
              </span>
            </li>
            <li className={styles.graphBlock}>
              <span className={cn('user faction', currentFaction.factionRank)}>
                <img className={cn('left', styles.tag)} src={opponentFaction.tagImageUrl} alt='' />
              </span>
              <div className={cn('left', styles.scoreBg)}>
                <div className={cn(styles.slider, styles.opponentSlider)} style={{ left: sliderPosition }}>
                  <div className={cn(styles.targetBall, styles.opponentBall)}>
                    <div className={cn(styles.ball, styles.grey)} />
                  </div>
                </div>
                {this._renderProgressBar(sliderPosition, false)}
              </div>

              <span className={cn('user faction', opponentFaction.factionRank)}>
                <img className={cn('right', styles.tag)} src={currentFaction.tagImageUrl} alt='' />
              </span>
              <div className={cn('right', styles.scoreBg)}>
                <div className={cn('right', styles.slider, styles.currentSlider)} style={{ right: sliderPosition }}>
                  <div className={cn('right', styles.targetBall, styles.currentBall)}>
                    <div className={cn(styles.ball, styles.grey)} />
                  </div>
                </div>
                {this._renderProgressBar(sliderPosition, true)}
              </div>

              {this._renderZeroProgressBar()}
            </li>
            <li className={styles.bottomBox}>
              <span className={cn('left', styles.infoBlock, styles.opponentBlock)}>{this._renderOpponentTarget()}</span>
              <span className={cn(styles.infoBlock, styles.timer)}>{this._renderTimer()}</span>
              <span className={cn('right', styles.infoBlock, styles.currentBlock)}>{this._renderCurrentTarget()}</span>
            </li>
          </ul>
        </div>
      </WarListItem>
    )
  }
}

export default RankWar
