import React from 'react'
import classNames from 'classnames'

import { IWarListItemProps } from './types'
import styles from './warListItem.cssmodule.scss'

export class WarListItem extends React.PureComponent<IWarListItemProps, any> {
  _getClassNames() {
    const { first, last, green, red, blue, active, className: classNameExtend, link } = this.props

    const className = classNames(
      styles.warListItem,
      {
        'first-in-row': first,
        green,
        red,
        blue,
        act: active,
        [styles.active]: active,
        [styles.firstInRow]: first,
        [styles.lastInRow]: last,
        [styles.green]: green,
        [styles.red]: red,
        [styles.blue]: blue,
        [styles.link]: link
      },
      classNameExtend
    )
    /*
      FIXME: Should remove the class name "first-in-row" and the property "first"
             when we remove parent class name dependency.
    */

    return className
  }

  _handleClick = event => {
    const { link } = this.props

    if (!(event.target instanceof HTMLAnchorElement)) {
      event.preventDefault()

      if (!link) {
        return
      }
      if (link[0] === '#') {
        window.location.hash = link
      } else {
        window.location.href = link
      }
    }
  }

  /* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
  /* eslint-disable jsx-a11y/click-events-have-key-events */
  render() {
    const { children } = this.props

    return (
      <li className={this._getClassNames()} onClick={this._handleClick}>
        {children}
      </li>
    )
  }
}

export default WarListItem
