export interface IWarListItemProps {
  first: boolean
  last: boolean
  green: boolean
  red: boolean
  blue: boolean
  active: boolean
  className: string
  link: string
  children: React.ReactNode
}
