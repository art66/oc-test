import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import WarItem from '../WarItem'
import EmptyBlock from '../EmptyBlock'
import {
  ITEMS_IN_ROW,
  DESKTOP_CONTAINER_WIDTH,
  RANK_BOX_TYPE,
  RANK_BOX_KEY,
  MILLISECONDS_IN_DAY,
  SECOND
} from '../../constants'
import { getDescIndex, $, isManualDesktopMode } from '../../utils'
import { setDesctopMode } from '../../actions'
import { subscribeForWarChanges, subscribeForFactionUserChanges } from '../../modules'

const getEmptyBlocks = wars => {
  const amount = wars.length % ITEMS_IN_ROW === 0 ? 0 : ITEMS_IN_ROW - (wars.length % ITEMS_IN_ROW)
  const blocks = []

  for (let i = 0; i < amount; i++) {
    blocks.push({
      index: i,
      isEmpty: true
    })
  }
  return blocks
}

class WarList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isDesktopContainerWidth: true
    }

    this.mainRef = React.createRef()
  }

  componentDidMount() {
    const $mainNode = $(this.mainRef.current)
    const { factionID, userFactionID, wars } = this.props

    this.props.subscribeForWarChanges(factionID || userFactionID)

    const rankWar = wars.find(war => war.key === RANK_BOX_KEY)

    if (rankWar !== undefined) {
      this.props.subscribeForFactionUserChanges(rankWar.currentFaction.id)
      this.props.subscribeForFactionUserChanges(rankWar.opponentFaction.id)
    }

    $(window)
      .resize(() => {
        const { mediaType } = this.props

        if (mediaType === 'desktop') {
          return
        }
        const newValue = $mainNode.width() >= DESKTOP_CONTAINER_WIDTH

        if (this.state.isDesktopContainerWidth === newValue) {
          return
        }
        this.setState({ isDesktopContainerWidth: newValue })
        this.props.setDesctopMode(newValue)
      })
      .resize()
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.isDesktopContainerWidth !== nextState.isDesktopContainerWidth
  }

  render() {
    const {
      children,
      params,
      wars,
      myFactionInfo,
      mediaType,
      now,
      isProfileMode,
      rankBoxAnimation,
      rankBoxTargetAnimation
    } = this.props
    const { isDesktopContainerWidth } = this.state
    // show rank war 24 hours after start
    const warsFilter = wars.filter(
      war =>
        !(war.type === RANK_BOX_TYPE && war.completedAt > 0 && war.completedAt * SECOND + MILLISECONDS_IN_DAY < now)
    )

    const emptyBlocks = getEmptyBlocks(warsFilter)
    let activeWarBoxIndex = -1

    for (let i = 0; i < warsFilter.length; i++) {
      if (warsFilter[i].key === params.key) {
        activeWarBoxIndex = i
        break
      }
    }

    const descIndex = getDescIndex(mediaType, activeWarBoxIndex, isDesktopContainerWidth)
    let warList = [...warsFilter, ...emptyBlocks, { isClear: true }]

    if (descIndex >= 0) {
      warList = [
        ...warList.slice(0, descIndex),
        {
          isDescription: true
        },
        ...warList.slice(descIndex, warList.length)
      ]
    }
    let index = -1

    return (
      <ul className='f-war-list war-new' ref={this.mainRef}>
        {warList.map(item => {
          if (item.isEmpty) {
            return <EmptyBlock key={item.index} />
          }
          if (item.isDescription) {
            return (
              <li className='descriptions' key={`description-${params.key}`}>
                {children}
              </li>
            )
          }
          if (item.isClear) {
            return <li className='clear' key={`clear-${index}`} />
          }
          index += 1
          return (
            <WarItem
              key={item.key}
              item={item}
              activeWarBox={params.key}
              myFactionInfo={myFactionInfo}
              mediaType={mediaType}
              warBoxIndex={index}
              now={now}
              isProfileMode={isProfileMode}
              rankBoxAnimation={rankBoxAnimation}
              rankBoxTargetAnimation={rankBoxTargetAnimation}
            />
          )
        })}
      </ul>
    )
  }
}

WarList.propTypes = {
  children: PropTypes.element,
  params: PropTypes.object,
  mediaType: PropTypes.string,
  wars: PropTypes.array,
  myFactionInfo: PropTypes.object,
  now: PropTypes.number,
  isProfileMode: PropTypes.bool,
  setDesctopMode: PropTypes.func,
  subscribeForWarChanges: PropTypes.func,
  subscribeForFactionUserChanges: PropTypes.func,
  factionID: PropTypes.number,
  userFactionID: PropTypes.number,
  rankBoxAnimation: PropTypes.object,
  rankBoxTargetAnimation: PropTypes.object
}

const mapStateToProps = state => ({
  mediaType: isManualDesktopMode() ? 'desktop' : state.browser.mediaType,
  wars: state.app.wars,
  myFactionInfo: state.app.myFactionInfo,
  now: state.app.now,
  factionID: state.app.factionID,
  userFactionID: state.app.userFactionID,
  isProfileMode: state.app.isProfileMode,
  rankBoxAnimation: state.app.rankBoxAnimation,
  rankBoxTargetAnimation: state.app.rankBoxTargetAnimation
})

const mapActionsToProps = {
  setDesctopMode,
  subscribeForWarChanges,
  subscribeForFactionUserChanges
}

export default connect(mapStateToProps, mapActionsToProps)(WarList)
