import React, { ChangeEvent, useState } from 'react'
import MemberList from './MemberList'
import MemberTableHeader from './MemberTableHeader'
import { IProfileMembersListMember } from '../../interfaces/IMember'
import { IFactionInfo } from '../../interfaces/IFaction'
import { ASC, DESC, DEFAULT_SORT_FIELD } from '../../constants'

interface IProps {
  factionMembers: IProfileMembersListMember[]
  factionCapacity: number
  factionSize: number
  mediaType: 'desktop' | 'tablet' | 'mobile'
  showImages: boolean
  faction: IFactionInfo
}

const FactionMembers = (props: IProps) => {
  const [searchValue, setSearchValue] = useState('')
  const [members, setMembers] = useState(props.factionMembers)
  const [sortField, setSortField] = useState(DEFAULT_SORT_FIELD)
  const [sortDirection, setSortDirection] = useState(DESC)

  const filterMembers = (searchInput: string) => {
    const filteredMembers = props.factionMembers.filter(member => {
      return `${member.playername} [${member.userID}]`.toLowerCase().indexOf(searchInput.toLowerCase()) > -1
    })

    setMembers(filteredMembers)
  }

  const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value)
    filterMembers(e.target.value)
  }

  const onSort = (newSortField: string) => {
    setSortDirection(sortDirection === DESC && newSortField === sortField ? ASC : DESC)
    setSortField(newSortField)
  }

  return (
    <>
      <hr className='delimiter-999 m-top10' />
      <div className='faction-info-wrap restyle another-faction'>
        <div className='f-war-list members-list m-top10'>
          <MemberTableHeader
            membersAmount={props.factionSize}
            factionCapacity={props.factionCapacity}
            filterMembersAmount={members.length}
            onSearch={onSearch}
            onSort={onSort}
            searchValue={searchValue}
            sortDirection={sortDirection}
            sortField={sortField}
          />
          <MemberList
            mediaType={props.mediaType}
            members={members}
            searchValue={searchValue}
            sortDirection={sortDirection}
            sortField={sortField}
            showImages={props.showImages}
            faction={props.faction}
          />
        </div>
      </div>
    </>
  )
}

export default React.memo(FactionMembers)
