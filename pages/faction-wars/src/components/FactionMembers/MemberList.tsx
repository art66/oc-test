import React from 'react'
import orderBy from 'lodash.orderby'
import MemberRow from './MemberRow'
import { IProfileMembersListMember } from '../../interfaces/IMember'
import { IFactionInfo } from '../../interfaces/IFaction'

interface IProps {
  members: IProfileMembersListMember[]
  searchValue: string
  mediaType: 'desktop' | 'tablet' | 'mobile'
  sortDirection: string
  sortField: string
  showImages: boolean
  faction: IFactionInfo
}

const getValue = (member, field) => {
  if (field === 'status') {
    return member.status.okay.toString() + member.status.text.toLowerCase()
  }

  if (typeof member[field] === 'string') {
    return member[field].toLowerCase()
  }

  return member[field]
}

const getFields = sortField => {
  if (sortField === 'position') {
    return ['permissionLevelImportance', member => getValue(member, sortField)]
  }

  return [member => getValue(member, sortField)]
}

const MemberList = (props: IProps) => {
  const fields = getFields(props.sortField)
  const members = orderBy(props.members, fields, Array(fields.length).fill(props.sortDirection))

  return members.length === 0 ? (
    // eslint-disable-next-line react/no-unescaped-entities
    <div className='table-search-empty'>No members matching "{props.searchValue}" were found</div>
  ) : (
    <ul className='table-body'>
      {members.map(member => (
        <MemberRow
          key={member.userID}
          member={member}
          mediaType={props.mediaType}
          showImages={props.showImages}
          faction={props.faction}
        />
      ))}
    </ul>
  )
}

export default MemberList
