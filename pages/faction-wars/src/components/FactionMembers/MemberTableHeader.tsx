import React, { ChangeEvent, Component } from 'react'
import cn from 'classnames'
import Search from '../../icons/Search'
import Column from './Column'
import s from './index.cssmodule.scss'

interface IProps {
  membersAmount: number
  factionCapacity: number
  filterMembersAmount: number
  onSearch: (e: ChangeEvent<HTMLInputElement>) => void
  onSort: (newSortField: string) => void
  searchValue: string
  sortField: string
  sortDirection: string
}

class MemberTableHeader extends Component<IProps> {
  state = {
    searchEnabled: false
  }

  _handleInputSearchClick = e => {
    e.stopPropagation()
  }

  _handleSearchButtonClick = e => {
    e.stopPropagation()
    this.setState({
      searchEnabled: true
    })
  }

  render() {
    const {
      sortField,
      sortDirection,
      filterMembersAmount,
      membersAmount,
      factionCapacity,
      searchValue,
      onSearch,
      onSort
    } = this.props
    const { searchEnabled } = this.state

    return (
      <ul className='table-header cont-gray'>
        <Column
          customClasses={cn('table-cell', 'member', 'c-pointer', s.membersCol)}
          sortField={sortField}
          sortDirection={sortDirection}
          field='playername'
          filterMembersAmount={filterMembersAmount}
          changeMembersSort={onSort}
        >
          <>
            <span className={cn(s.searchPlaceholder, { [s.hide]: searchEnabled })}>
              {membersAmount} / {factionCapacity} <span className='mobile-hide'>Faction</span> Members
            </span>
            <input
              autoFocus={true}
              className={cn(s.searchInput, { [s.hide]: !searchEnabled })}
              aria-label='Type here to filter members'
              type='text'
              value={searchValue}
              onChange={onSearch}
              onClick={this._handleInputSearchClick}
              maxLength={20}
              placeholder={`Search ${membersAmount} / ${factionCapacity} faction members`}
            />
            <button
              type='button'
              aria-label='Filter members'
              className={s.searchButton}
              onClick={this._handleSearchButtonClick}
            >
              <Search className={s.searchIcon} />
            </button>
          </>
        </Column>
        <Column
          customClasses={cn('table-cell', 'lvl', 'torn-divider', 'divider-vertical', 'c-pointer', s.lvlCol)}
          sortField={sortField}
          sortDirection={sortDirection}
          field='level'
          filterMembersAmount={filterMembersAmount}
          changeMembersSort={onSort}
        >
          Lvl
        </Column>
        <li className='table-cell member-icons torn-divider divider-vertical'>Icons</li>
        <Column
          customClasses={cn('table-cell', 'position', 'torn-divider', 'divider-vertical', 'c-pointer', s.positionCol)}
          sortField={sortField}
          sortDirection={sortDirection}
          field='position'
          filterMembersAmount={filterMembersAmount}
          changeMembersSort={onSort}
        >
          Position
        </Column>
        <Column
          customClasses='table-cell days torn-divider divider-vertical c-pointer'
          sortField={sortField}
          sortDirection={sortDirection}
          field='days'
          filterMembersAmount={filterMembersAmount}
          changeMembersSort={onSort}
        >
          Days
        </Column>
        <Column
          customClasses='table-cell status torn-divider divider-vertical c-pointer'
          sortField={sortField}
          sortDirection={sortDirection}
          field='status'
          filterMembersAmount={filterMembersAmount}
          changeMembersSort={onSort}
        >
          Status
        </Column>
      </ul>
    )
  }
}

export default MemberTableHeader
