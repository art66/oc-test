import React from 'react'
import cn from 'classnames'
import UserInfo from '@torn/shared/components/UserInfo'
import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'
import { IProfileMembersListMember } from '../../interfaces/IMember'
import { IFactionInfo } from '../../interfaces/IFaction'
import s from './index.cssmodule.scss'

interface IProps {
  mediaType: 'desktop' | 'tablet' | 'mobile'
  member: IProfileMembersListMember
  faction: IFactionInfo
  showImages: boolean
}

const MemberRow = (props: IProps) => {
  const userInfoConfig = {
    showImages: props.showImages,
    useProgressiveImage: false,
    status: {
      mode: props.member.onlineStatus.status
    },
    user: {
      ID: props.member.userID,
      name: props.member.playername,
      imageUrl: props.member.honor,
      isSmall: props.mediaType !== 'desktop'
    },
    faction: {
      ID: props.faction.factionID,
      name: props.faction.factionTag,
      rank: props.faction.factionRank as TFactionRank,
      imageUrl: props.faction.factionTagImageUrl
    }
  }

  return (
    <li className='table-row'>
      <div className={cn('table-cell', 'member', 'icons', s.membersCol)}>
        <UserInfo {...userInfoConfig} />
      </div>
      <div className={cn('table-cell', 'lvl', s.lvlCol)}>{props.member.level}</div>
      <div className='table-cell member-icons icons' dangerouslySetInnerHTML={{ __html: props.member.icons }} />
      <div className={cn('table-cell', 'position', s.positionCol)}>
        <span className='ellipsis'>{props.member.position}</span>
      </div>
      <div className='table-cell days'>{props.member.days}</div>
      <div className='table-cell status'>
        <span className={`ellipsis t-${props.member.status.okay ? 'green' : 'red'}`}>{props.member.status.text}</span>
      </div>
    </li>
  )
}

export default MemberRow
