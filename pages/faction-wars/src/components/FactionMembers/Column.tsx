import React from 'react'
import SortArrow from './SortArrow'

interface IProps {
  children: React.ReactChild
  changeMembersSort: (sortField: string) => void
  field: string
  sortField: string
  sortDirection: string
  customClasses: string
  filterMembersAmount: number
}

const Column = (props: IProps) => {
  const handleSortMembers = () => {
    props.changeMembersSort(props.field)
  }

  const handleKeyPressOnCol = (e: React.KeyboardEvent<HTMLLIElement>) => {
    if (e.key === 'Enter') {
      handleSortMembers()
    }
  }

  return (
    <li
      tabIndex={0}
      role={props.field === 'playername' ? 'heading' : undefined}
      aria-level={props.field === 'playername' ? 5 : undefined}
      className={props.customClasses}
      onClick={handleSortMembers}
      onKeyPress={handleKeyPressOnCol}
    >
      {props.children}
      {!props.filterMembersAmount || (
        <SortArrow sortField={props.sortField} sortDirection={props.sortDirection} field={props.field} />
      )}
    </li>
  )
}

export default Column
