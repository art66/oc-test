import React from 'react'
import cn from 'classnames'
import { ASC, DESC } from '../../constants'
import s from './index.cssmodule.scss'

interface IProps {
  field: string
  sortDirection: string
  sortField: string
}

const SortArrow = (props: IProps) => {
  return (
    <div
      className={cn(s.sortIcon, {
        [s.activeIcon]: props.sortField === props.field,
        [s.desc]: props.sortDirection === DESC,
        [s.asc]: props.sortDirection === ASC
      })}
    />
  )
}

export default SortArrow
