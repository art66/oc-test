import React from 'react'
import cn from 'classnames'
import { globalNumberFormat as gnf } from '../../utils'
import WarListItem from '../WarList/WarListItem'
import s from './index.cssmodule.scss'

const FONT_MAX = 28
const FONT_MIN = 10
const BOX_HEIGHT = 78
const INIT_LEFT_GREEN_POSITION = -82
const INIT_LEFT_RED_POSITION = -166

const getScoreFontSize = (score1, score2) => {
  const s1 = score1 + 1
  const s2 = score2 + 1

  return [
    FONT_MIN + Math.round((s1 * (FONT_MAX - FONT_MIN)) / (s1 + s2)),
    FONT_MIN + Math.round((s2 * (FONT_MAX - FONT_MIN)) / (s1 + s2))
  ]
}

interface IFaction {
  factionID: number
  name: string
}

interface IProps {
  className: string
  first: boolean
  last: boolean
  active: boolean
  activeWarBox: string
  raid: {
    key: string
    isMyRaid: boolean
    isMyAttack: boolean
    isTerritorial: boolean
    enemyScoreFraction: number
    myScoreFraction: number
    enemyScore: number
    enemyMemberNums: {
      okay: number
      total: number
    }
    myScore: number
    myMemberNums: {
      okay: number
      total: number
    }
    enemyFaction: IFaction
    myFaction: IFaction
  }
}

const renderScore = score => {
  return (
    <>
      {score < 1000 ? gnf(Math.floor(score)) : gnf(Math.round(score))}
      {score < 1000 ? `.${Math.round((score % 1) * 100) || '00'}` : ''}
    </>
  )
}

class RaidWar extends React.Component<IProps> {
  getLinkPath() {
    const { raid, activeWarBox } = this.props

    if (activeWarBox === raid.key) {
      return '#/'
    }

    return `#/war/${raid.key}`
  }

  getHeight() {
    const { raid } = this.props

    const rightTopHeight = BOX_HEIGHT - Math.floor(BOX_HEIGHT * raid.myScoreFraction)
    const rightBottomHeight = Math.floor(BOX_HEIGHT * raid.myScoreFraction)
    const leftTopHeight = BOX_HEIGHT - Math.floor(BOX_HEIGHT * raid.enemyScoreFraction)
    const leftBottomHeight = Math.floor(BOX_HEIGHT * raid.enemyScoreFraction)

    return {
      leftTopHeight,
      leftBottomHeight,
      rightTopHeight,
      rightBottomHeight
    }
  }

  // eslint-disable-next-line complexity
  render() {
    const { raid, className, first, last, active } = this.props

    const [myFontSize, enemyFontSize] = getScoreFontSize(raid.myScore, raid.enemyScore)
    const { leftTopHeight, leftBottomHeight, rightTopHeight, rightBottomHeight } = this.getHeight()
    const green = raid.myScore > raid.enemyScore
    const baseBackgroundPositionY = green ? INIT_LEFT_GREEN_POSITION : INIT_LEFT_RED_POSITION

    /* eslint-disable jsx-a11y/control-has-associated-label */
    return (
      <WarListItem
        first={first}
        last={last}
        green={green}
        red={!green}
        blue={false}
        active={active}
        className={className}
        link={this.getLinkPath()}
      >
        <div className={cn('raid-box', s.raidBoxWp)}>
          <div className='raid-box-title-block'>
            <span className='raid-box-title'>
              <a
                className={cn(s.linkColor, s.opponentFactionName)}
                href={`/factions.php?step=profile&ID=${raid.enemyFaction.factionID}`}
                dangerouslySetInnerHTML={{ __html: raid.enemyFaction.name }}
              />
              <span> vs </span>
              <a
                className={cn(s.linkColor, s.currentFactionName)}
                href={`/factions.php?step=profile&ID=${raid.myFaction.factionID}`}
                dangerouslySetInnerHTML={{ __html: raid.myFaction.name }}
              />
            </span>
            <i className={cn('raid-war-icon', { green, red: !green })} />
          </div>
          <div className={cn('raid-box-stats-block', { 'green-box': green, 'red-box': !green })}>
            <div className='half-box-wrapper'>
              <div className='raid-box-general-info progress-box left-progress-box' style={{ height: leftTopHeight }} />
              <div
                className='raid-box-general-info left-box'
                style={{
                  height: leftBottomHeight,
                  backgroundPositionY: baseBackgroundPositionY - leftTopHeight
                }}
              />

              <div className='score-wrapper-left'>
                <span className='raid-box-center-stat score score-red' style={{ fontSize: enemyFontSize }}>
                  {renderScore(raid.enemyScore)}
                </span>
                <span className='raid-box-timeleft' title='Available targets'>
                  <i className='raid-target-icon' />
                  <span className='m-left5'>
                    {raid.enemyMemberNums.okay || 0} / {raid.enemyMemberNums.total || 0}
                  </span>
                </span>
              </div>
            </div>

            <ul className='raid-box-stats-list' />

            <div className='half-box-wrapper'>
              <div
                className='raid-box-general-info progress-box right-progress-box'
                style={{ height: rightTopHeight }}
              />
              <div
                className='raid-box-general-info right-box'
                style={{
                  height: rightBottomHeight,
                  backgroundPositionY: baseBackgroundPositionY - rightTopHeight
                }}
              />

              <div className='score-wrapper-right'>
                <span className='raid-box-center-stat score score-green' style={{ fontSize: myFontSize }}>
                  {renderScore(raid.myScore)}
                </span>
                <span className='raid-box-timeleft' title='Available members'>
                  <i className='raid-check-icon' />
                  <span className='m-left5'>
                    {raid.myMemberNums.okay || 0} / {raid.myMemberNums.total || 0}
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
      </WarListItem>
    )
  }
}

export default RaidWar
