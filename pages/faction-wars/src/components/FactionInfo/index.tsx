import React from 'react'
import { useSelector } from 'react-redux'

import FactionMembers from '../FactionMembers'
import FactionDescription from '../FactionDescription'

import {
  getFactionMembers,
  getFactionDescription,
  getFactionCapacity,
  getFactionID,
  getFactionRank,
  getFactionTag,
  getFactionTagImgUrl,
  getShowImagesSetting,
  getFactionSize
} from '../../selectors'
import { toggleFactionDescription } from '../../actions'

interface IProps {
  mediaType: 'desktop' | 'tablet' | 'mobile'
}

const FactionInfo = (props: IProps) => {
  const factionMembers = useSelector(getFactionMembers)
  const factionDescription = useSelector(getFactionDescription)
  const factionCapacity = useSelector(getFactionCapacity)
  const factionSize = useSelector(getFactionSize)
  const showImages = useSelector(getShowImagesSetting)
  const faction = {
    factionID: useSelector(getFactionID),
    factionTag: useSelector(getFactionTag),
    factionRank: useSelector(getFactionRank),
    factionTagImageUrl: useSelector(getFactionTagImgUrl)
  }

  return (
    <>
      <FactionDescription
        description={factionDescription}
        toggleFactionDescription={toggleFactionDescription}
        mediaType={props.mediaType}
      />
      <FactionMembers
        factionMembers={factionMembers}
        factionCapacity={factionCapacity}
        factionSize={factionSize}
        mediaType={props.mediaType}
        showImages={showImages}
        faction={faction}
      />
    </>
  )
}

export default React.memo(FactionInfo)
