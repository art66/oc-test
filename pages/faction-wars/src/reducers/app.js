import { handleActions } from 'redux-actions'
import orderBy from 'lodash.orderby'
import { queryStringToObj, mergeWars, getCurrentTimestamp, isFactionDescriptionOpened } from '../utils'
import * as a from '../actions/actionTypes'
import { RANK_BOX_KEY, RANK_BOX_TYPE, TERRITORY_BOX_TYPE, DEFAULT_SORT_FIELD, DESC } from '../constants'

const initialState = {
  isProfileMode: queryStringToObj(window.location.href).step === 'profile',
  now: getCurrentTimestamp(),
  warDataLoaded: false,
  warDescriptionLoaded: {},
  raidRequest: {},
  isDesktopContainerWidth: true,
  wars: [],
  rankBoxAnimation: {
    isActive: false
  },
  rankBoxTargetAnimation: {
    isActive: false
  }
}

export default handleActions(
  {
    'data loaded': (state, action) => ({
      ...state,
      warDataLoaded: true,
      ...action.payload.json
    }),
    'war data update': (state, action) => ({
      ...state,
      ...action.payload.json,
      wars: mergeWars(state.wars, action.payload.json.wars)
    }),
    'add ranked war': (state, action) => ({
      ...state,
      wars: mergeWars(state.wars, [
        {
          ...action.payload.rank,
          type: RANK_BOX_KEY,
          key: RANK_BOX_KEY
        }
      ])
    }),
    'update ranked war target': (state, action) => {
      const newState = {
        ...state,
        wars: state.wars.map(war => {
          if (war.key === RANK_BOX_KEY) {
            return {
              ...war,
              respectRequirement: action.payload.data.target
            }
          }
          return war
        })
      }

      if (state.warDescriptionLoaded[RANK_BOX_KEY]) {
        newState.warDesc = {
          ...newState.warDesc,
          graph: {
            ...newState.warDesc.graph,
            respectRequirement: [
              ...newState.warDesc.graph.respectRequirement,
              [getCurrentTimestamp() / 1000, action.payload.data.target]
            ]
          }
        }
      }

      return newState
    },
    'ranked war target animation': (state, action) => ({
      ...state,
      rankBoxTargetAnimation: {
        ...state.rankBoxTargetAnimation,
        animationTarget: action.payload.animationTarget
      }
    }),
    'update ranked war score': (state, action) => {
      let currentScore
      let opponentScore
      let newScore
      const attack = action.payload.data
      const newState = {
        ...state,
        wars: state.wars.map(war => {
          if (war.key === RANK_BOX_KEY) {
            currentScore =
              war.currentFaction.score + (attack.attacker.factionId === war.currentFaction.id ? attack.respect : 0)
            opponentScore =
              war.opponentFaction.score + (attack.attacker.factionId === war.opponentFaction.id ? attack.respect : 0)

            newScore = currentScore - opponentScore

            return {
              ...war,
              currentFaction: {
                ...war.currentFaction,
                score: currentScore
              },
              opponentFaction: {
                ...war.opponentFaction,
                score: opponentScore
              }
            }
          }
          return war
        })
      }

      if (state.warDescriptionLoaded[RANK_BOX_KEY]) {
        newState.warDesc = {
          ...state.warDesc,
          graph: {
            ...state.warDesc.graph,
            data: [...state.warDesc.graph.data, [attack.currentTime, newScore]]
          },
          members: state.warDesc.members.map(member => {
            if (member.userID === attack.attacker.userId) {
              return {
                ...member,
                score: member.score + attack.respect
              }
            }
            return member
          })
        }
      }

      return newState
    },
    'update ranked war members status': (state, action) => ({
      ...state,
      warDesc: {
        ...state.warDesc,
        members: state.warDesc.members.map(member => ({
          ...member,
          status: action.payload.data.userStatuses[member.userID] || member.status
        }))
      }
    }),
    'update ranked war user status': (state, action) => {
      const newState = { ...state }

      if (state.warDesc && state.warDesc.members) {
        newState.warDesc = {
          ...state.warDesc,
          members: state.warDesc.members.map(member => {
            if (member.userID === action.payload.data.userId) {
              return {
                ...member,
                status: action.payload.data.status
              }
            }
            return member
          })
        }
      }
      return newState
    },
    'ranked war score animation increase': (state, action) => {
      const { factionID, respectInc } = action.payload
      const ranks = state.wars.filter(item => item.key === RANK_BOX_KEY)

      if (ranks.length === 0 && state.rankBoxAnimation.isActive === false) {
        return state
      }

      const war = ranks[0]
      let { currentScore, opponentScore } = state.rankBoxAnimation

      currentScore += factionID === war.currentFaction.id ? respectInc : 0
      opponentScore += factionID === war.opponentFaction.id ? respectInc : 0

      return {
        ...state,
        rankBoxAnimation: {
          isActive: true,
          factionID,
          currentScore,
          opponentScore
        }
      }
    },
    'ranked war score animation decrease': (state, action) => ({
      ...state,
      rankBoxAnimation: {
        ...state.rankBoxAnimation,
        currentScore: Math.max(state.rankBoxAnimation.currentScore - action.payload.respectDec, 0),
        opponentScore: Math.max(state.rankBoxAnimation.opponentScore - action.payload.respectDec, 0)
      }
    }),
    'start ranked war animation': state => {
      const ranks = state.wars.filter(item => item.key === RANK_BOX_KEY)

      if (ranks.length === 0) {
        return state
      }

      return {
        ...state,
        rankBoxAnimation: {
          isActive: true,
          currentScore: ranks[0].currentFaction.score,
          opponentScore: ranks[0].opponentFaction.score
        }
      }
    },
    'start ranked war target animation': state => {
      const ranks = state.wars.filter(item => item.key === RANK_BOX_KEY)

      if (ranks.length === 0) {
        return state
      }

      return {
        ...state,
        rankBoxTargetAnimation: {
          isActive: true
        }
      }
    },
    'finish ranked war animation': state => ({
      ...state,
      rankBoxAnimation: {
        ...state.rankBoxAnimation,
        isActive: false
      }
    }),
    'finish ranked war target animation': state => ({
      ...state,
      rankBoxtargetAnimation: {
        ...state.rankBoxtargetAnimation,
        isActive: false,
        animationTarget: 0
      }
    }),
    'war description loaded': (state, action) => ({
      ...state,
      warDescriptionLoaded: {
        ...state.warDescriptionLoaded,
        [action.payload.key]: true
      },
      ...action.payload.json
    }),
    'finish ranked war': state => ({
      ...state,
      wars: state.wars.map(war => {
        if (war.type === RANK_BOX_TYPE) {
          return {
            ...war,
            completedAt: getCurrentTimestamp() / 1000
          }
        }
        return war
      })
    }),
    'now updated': (state, action) => ({
      ...state,
      wars: state.wars.map(war => {
        if (war.type === TERRITORY_BOX_TYPE) {
          const diff = war.myFaction.membersQuantity - war.enemyFaction.membersQuantity
          const score = war.score + (war.isMyAttack ? diff : -diff)

          return {
            ...war,
            score: Math.min(Math.max(score, 0), war.maxPoints)
          }
        }

        return war
      }),
      now: action.payload.now
    }),
    'joining war': (state, action) => ({
      ...state,
      isJoining: true,
      activeSlotIndex: action.payload.slotIndex
    }),
    'joined war': (state, action) => ({
      ...state,
      isJoining: false,
      ...action.payload.json
    }),
    'msg closed': state => ({
      ...state,
      msg: '',
      activeSlotIndex: -1
    }),
    'requesting surrender': state => ({
      ...state,
      raidRequest: {
        surrender: true,
        loading: true
      }
    }),
    'requested surrender': (state, action) => ({
      ...state,
      raidRequest: {
        ...action.payload.json,
        loading: false
      }
    }),
    'ceasing raid': state => ({
      ...state,
      raidRequest: {
        cease: true,
        loading: true
      }
    }),
    'ceased raid': (state, action) => ({
      ...state,
      raidRequest: {
        ...action.payload.json,
        loading: false
      }
    }),
    'set desktop mode': (state, action) => ({
      ...state,
      isDesktopContainerWidth: action.payload.isDesktopMode
    }),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    '@@router/LOCATION_CHANGE': state => ({
      ...state,
      warDescriptionLoaded: {}
    }),
    [a.SET_NOTIFICATIONS]: (state, action) => ({
      ...state,
      notifications: action.payload.notifications
    }),
    [a.REMOVE_NOTIFICATION]: (state, action) => ({
      ...state,
      notifications: state.notifications.filter(notification => notification.id !== action.payload.id)
    }),
    [a.SET_FACTION_PROFILE_INFO]: (state, action) => {
      return {
        ...state,
        currentUser: action.payload.data.currentUser,
        factionInfo: {
          ...action.payload.data.faction,
          description: {
            text: action.payload.data.faction.description,
            opened: isFactionDescriptionOpened()
          }
        },
        members: orderBy(action.payload.data.members, [DEFAULT_SORT_FIELD], [DESC])
      }
    },
    [a.SET_FACTION_DESCRIPTION_STATE]: (state, action) => {
      return {
        ...state,
        factionInfo: {
          ...state.factionInfo,
          description: {
            ...state.factionInfo.description,
            opened: action.payload.opened
          }
        }
      }
    }
  },
  initialState
)
