import React from 'react'
import { render } from 'react-dom'
import { hashHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import configureStore from './store/configureStore'
import AppContainer from './containers/AppContainer'

const store = configureStore()
const history = syncHistoryWithStore(hashHistory, store)

if (__DEV__) {
  import('@torn/header')
}

render(<AppContainer store={store} history={history} />, document.getElementById('react-root'))
