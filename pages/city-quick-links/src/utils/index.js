import { INC_POPULARITY_URL } from '../constants'

const { $, queryStringToObj, tc } = window

export const incPopularityOnLinkClick = placeId => {
  fetch(INC_POPULARITY_URL, {
    method: 'POST',
    body: JSON.stringify({ places: placeId, option: 'popularity', is_qlink: true })
  })
}

export const isDesktopView = (mediaType) => {
  return mediaType === 'desktop' || !document.body.classList.contains('r')
}

export {
  $,
  tc,
  queryStringToObj
}
