import React from 'react'
import { render } from 'react-dom'
import configureStore from './store/configureStore'
import AppContainer from './containers/AppContainer'
import { tc } from './utils'

const store = configureStore()

tc.QuickLinks = {
  startRender: () => {
    render(<AppContainer store={store} />, document.getElementById('react-root'))
  }
}
