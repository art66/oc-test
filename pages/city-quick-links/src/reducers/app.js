import { handleActions } from 'redux-actions'

const initialState = {
  dataLoaded: false,
  quickLinks: {}
}

export default handleActions(
  {
    'data loaded'(state, action) {
      return {
        ...state,
        dataLoaded: true,
        quickLinks: action.payload.json
      }
    },

    'sort changed'(state, action) {}
  },

  initialState
)
