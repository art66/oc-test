import React from 'react'
import PropTypes from 'prop-types'

const Loading = props => {
  let className = 'ajax-placeholder m-top15 m-bottom10'

  const { pure } = props

  if (pure) {
    className = ''
  }
  return <img className={className} src='/images/v2/main/ajax-loader.gif' />
}

Loading.propTypes = {
  pure: PropTypes.bool
}

export default Loading
