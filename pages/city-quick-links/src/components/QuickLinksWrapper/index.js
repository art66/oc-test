import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Loading from '../Loading'
import QuickLinks from '../QuickLinks'
import { getQuickLinksData } from '../../actions'

class QuickLinksWrapper extends Component {
  constructor(props) {
    super(props)

    const { getQuickLinksDataProp } = this.props

    getQuickLinksDataProp()
  }

  shouldComponentUpdate() {
    return true
  }

  render() {
    const { dataLoaded, quickLinks, mediaType } = this.props

    if (!dataLoaded) {
      return <Loading />
    }

    return <QuickLinks
      quickLinks={quickLinks}
      mediaType={mediaType}
    />
  }
}

QuickLinksWrapper.propTypes = {
  dataLoaded: PropTypes.bool.isRequired,
  getQuickLinksDataProp: PropTypes.func.isRequired,
  quickLinks: PropTypes.object.isRequired,
  mediaType: PropTypes.string.isRequired
}

const mapStateToProps = state => ({
  dataLoaded: state.app.dataLoaded,
  quickLinks: state.app.quickLinks,
  mediaType: state.browser.mediaType
})

const mapActionsToProps = {
  getQuickLinksDataProp: getQuickLinksData
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(QuickLinksWrapper)
