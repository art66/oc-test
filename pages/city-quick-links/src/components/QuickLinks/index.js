import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { incPopularityOnLinkClick, isDesktopView } from '../../utils'
import { EXCEPTION_NAME } from '../../constants'

// to change order for sorting by types
const typeMap = [1, 2, 3, 0, 4, 5]
const orderChangeMap = (sortProp, sortVal) => {
  if (sortProp === 'type') {
    return typeMap[sortVal]
  }
  return sortVal
}

class QuickLinks extends Component {
  constructor(props) {
    super(props)

    this.state = {
      sorts: ['area', 'type', 'name', 'popularity'],
      sortProp: (window.localStorage && localStorage.getItem('sortProp')) || 'area'
    }
  }

  getResultLinksList(qLinks, qLinksSorts) {
    const { sortProp } = this.state
    const resultLinksList = []
    let currentType = -1

    for (let i = 0; i < Object.keys(qLinks).length; i++) {
      const item = qLinks[i]

      let title = ''

      item.title.split(' ').forEach(word => {
        if (word === EXCEPTION_NAME) {
          title += `${word} `
        } else {
          title += `${word.substr(0, 1) + word.substr(1).toLowerCase()} `
        }
      })
      item.title = title.trim()

      if (currentType !== item.sortProps[sortProp] && qLinksSorts[sortProp].titleRequired) {
        currentType = item.sortProps[sortProp]
        const titleItem = this._getItemState(sortProp, item.sortProps[sortProp])

        titleItem.isTitle = true
        resultLinksList.push(titleItem)
      }

      resultLinksList.push(item)
    }

    return resultLinksList
  }

  getRenderArray(resultLinksList, colNum, amountInColumn) {
    const { sortProp } = this.state
    const renderArray = []

    if (['name', 'popularity'].indexOf(sortProp) >= 0) {
      for (let i = 0; i < colNum; i++) {
        renderArray.push([])
      }
      for (let i = 0; i < resultLinksList.length; i++) {
        renderArray[i % colNum].push(resultLinksList[i])
      }
    } else {
      for (let i = 0; i < Object.keys(resultLinksList).length; i++) {
        if (i % amountInColumn === 0) {
          renderArray.push([])
        }
        renderArray[renderArray.length - 1].push(resultLinksList[i])
      }
    }

    for (let i = 0; i < colNum; i++) {
      while (renderArray[i].length < amountInColumn) {
        renderArray[i].push({ isEmpty: true, key: renderArray[i].length })
      }
    }

    return renderArray
  }

  handleChangeSort(sortProp) {
    if (window.localStorage) {
      localStorage.setItem('sortProp', sortProp)
    }
    this.setState({ sortProp })
  }

  _getItemState(prop, value) {
    const { quickLinks } = this.props
    const types = quickLinks.standardMapObjectsQLinksSorts[prop].sortData

    for (let i = 0; i < Object.keys(types).length; i++) {
      if (types[i][prop] === value) {
        return types[i]
      }
    }
    return false
  }

  sortedQLinks() {
    const { quickLinks } = this.props
    const { sortProp } = this.state
    const qLinks = quickLinks.standardMapObjects

    for (let i = 0; i < qLinks.length; i++) {
      for (let j = qLinks.length - 1; j > i; j--) {
        const curr = qLinks[j]
        const prev = qLinks[j - 1]

        if (
          orderChangeMap(sortProp, curr.sortProps[sortProp]) < orderChangeMap(sortProp, prev.sortProps[sortProp])
          || (curr.sortProps[sortProp] === prev.sortProps[sortProp] && curr.sortProps.name < prev.sortProps.name)
        ) {
          const tmp = curr

          qLinks[j] = prev
          qLinks[j - 1] = tmp
        }
      }
    }
    return qLinks
  }

  render() {
    const { quickLinks, mediaType } = this.props
    const { sorts, sortProp } = this.state
    const colNum = isDesktopView(mediaType) ? 5 : 2

    const qLinks = this.sortedQLinks()
    const qLinksSorts = quickLinks.standardMapObjectsQLinksSorts

    const resultLinksList = this.getResultLinksList(qLinks, qLinksSorts)

    const amountInColumn = Math.floor(resultLinksList.length / colNum) + (resultLinksList.length % colNum > 0 ? 1 : 0)
    const renderArray = this.getRenderArray(resultLinksList, colNum, amountInColumn)

    return (
      <div id='quick_links' className='quick-links-wrap'>
        <ul className='ql-at columns'>
          {renderArray.map((qLinksColumn, index) => {
            return (
              <li key={qLinksColumn[0].title} className={index === renderArray.length - 1 ? 'last' : ''}>
                <ul className='rows'>
                  {qLinksColumn.map((qLink, linkIndex) => {
                    if (qLink.isEmpty) {
                      return <li key={qLink.key} />
                    }
                    if (qLink.isTitle) {
                      return <li key={qLink.title} className='title'>{qLink.title}</li>
                    }
                    return (
                      <li key={qLink.title} className={linkIndex === amountInColumn - 1 ? '' : ''}>
                        <a
                          className={`font-num-${qLink.font_color}`}
                          href={`/${qLink.link}`}
                          onClick={() => {
                            incPopularityOnLinkClick(qLink.place_id)
                          }}
                        >
                          <i
                            className={`cql-${qLink.icon_class
                              || qLink.title
                                .toLowerCase()
                                .split(' ')
                                .join('-')
                                .split('\'')
                                .join('')}`}
                          />
                          <span>{qLink.title}</span>
                        </a>
                      </li>
                    )
                  })}
                </ul>
              </li>
            )
          })}
          <li className='clear' />
        </ul>
        <div className='sort-by choise-container' role='radiogroup' aria-labelledby='wai-sort-by'>
          <span id='wai-sort-by' className='t-hide'>
            Sort by:
          </span>

          {sorts.map(sort => (
            <span
              className='has-pretty-child'
              key={sort}
              onClick={() => { this.handleChangeSort(sort) }}
            >
              <div className='clearfix prettyradio labelright  blue'>
                <input id={sort} className='radio-css' type='radio' readOnly={true} checked={sort === sortProp} />
                <label htmlFor={sort} className='marker-css' aria-label={`Sort by ${sort.toUpperCase()}`}>
                  {sort.charAt(0).toUpperCase() + sort.slice(1)}
                </label>
              </div>
            </span>
          ))}
        </div>
      </div>
    )
  }
}

QuickLinks.propTypes = {
  quickLinks: PropTypes.object.isRequired,
  mediaType: PropTypes.string.isRequired
}

export default QuickLinks
