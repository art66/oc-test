import { createAction } from 'redux-actions'
import { fetchUrl } from '@torn/shared/utils'
import { BASE_URL } from '../constants'

export const dataLoaded = createAction('data loaded', json => ({ json }))

export const getQuickLinksData = () => {
  return dispatch => {
    return fetchUrl(`${BASE_URL}?step=quicklinksdata`).then(json => {
      dispatch(dataLoaded(json))
    })
  }
}

export const msgClosed = createAction('msg closed')

export const closeMsg = () => {
  return dispatch => {
    dispatch(msgClosed())
  }
}
