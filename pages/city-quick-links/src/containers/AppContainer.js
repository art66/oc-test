import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import QuickLinksWrapper from '../components/QuickLinksWrapper'

class AppContainer extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <div style={{ height: '100%' }}>
          <QuickLinksWrapper />
        </div>
      </Provider>
    )
  }
}

export default AppContainer
