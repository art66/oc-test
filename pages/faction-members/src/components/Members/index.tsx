import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import orderBy from 'lodash.orderby'
import tableStyles from '../../styles/tableStyles.cssmodule.scss'
import Row from './Row'
import { IMember, IMembersFilter, IMembersSort, IPositions, IReduxState } from '../../interfaces'

interface IProps {
  members: IMember[]
  positions: IPositions
  membersFilter: IMembersFilter
  membersSort: IMembersSort
}

class Members extends PureComponent<IProps> {
  renderTableRows = (): React.ReactNode | React.ReactNodeArray => {
    const {
      members,
      membersFilter: { value },
      membersSort: { sortField, sortDirection }
    } = this.props

    if (!members.length) {
      return (
        <div className={tableStyles.tableEmpty} role='alert'>
          {`No members matching "${value}" were found`}
        </div>
      )
    }

    const sortFieldArr =
      sortField === 'positionID' ?
        ['positionImportance', member => member.positionLabel.toLowerCase()] :
        [member => (typeof member[sortField] === 'string' ? member[sortField].toLowerCase() : member[sortField])]
    const sortDirectionArr = sortField === 'positionID' ? [sortDirection, sortDirection] : [sortDirection]

    const sortedMembers = orderBy(members, sortFieldArr, sortDirectionArr)

    return sortedMembers.map(member => <Row key={member.userID} member={member} />)
  }

  render() {
    return <div className={tableStyles.rowsWrapper}>{this.renderTableRows()}</div>
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    positions: state.data.positions,
    membersFilter: state.ui.membersFilter,
    membersSort: state.ui.membersSort
  }
}

export default connect(mapStateToProps)(Members)
