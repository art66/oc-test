import React, { PureComponent, RefObject } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import tableStyles from '../../../styles/tableStyles.cssmodule.scss'
import s from './index.cssmodule.scss'
import { ICurrentUser, IFaction, IReduxState, IMember, IPositions } from '../../../interfaces'
import {
  kickMember as kickMemberAction,
  assignPosition as assignPositionAction,
  checkKickMemberOpportunity,
  checkKickMemberOpportunitySuccess
} from '../../../actions'
import { Kick, Person, Position } from './Cells'
import { MAX_REASON_MESSAGE_LENGTH } from '../../../constants'

interface IProps {
  member: IMember
  positions: IPositions
  currentUser: ICurrentUser
  faction: IFaction
  error: {
    id: keyof IPositions
    message: string
  }
  mediaType: 'mobile' | 'tablet' | 'desktop'
  assignPosition: (data: { memberID: number; positionID: number }) => void
  kickMember: (userID: number, reason: string) => void
  checkKickMemberOpportunity: (id: number) => void
  checkKickMemberOpportunitySuccess: (id: number) => void
}

interface IState {
  kickConfirmActive: boolean
  changePositionConfirm: {
    active: boolean
    positionID: number
    confirmMessage: string
  }
  kickReason: string
}

class Row extends PureComponent<IProps, IState> {
  reasonInput: RefObject<HTMLInputElement>

  constructor(props: IProps) {
    super(props)

    this.state = {
      kickConfirmActive: false,
      changePositionConfirm: {
        active: false,
        positionID: null,
        confirmMessage: null
      },
      kickReason: ''
    }

    this.reasonInput = React.createRef()
  }

  componentDidUpdate(prevProps: Readonly<IProps>): void {
    const { currentUser, positions, member } = this.props
    const userWasKicked = !prevProps.member.state?.kicked && member.state?.kicked

    if (
      prevProps.positions[member.positionID].currentUserOperations.canChange
      && !positions[member.positionID].currentUserOperations.canChange
    ) {
      this._resetChangePositionConfirm()
    }

    if ((prevProps.currentUser.permissions.kick && !currentUser.permissions.kick) || userWasKicked) {
      this._resetKickConfirm()
    }
  }

  _handleMessageChange = e => {
    const msg = e.target.value

    this.setState({
      kickReason: msg.length > MAX_REASON_MESSAGE_LENGTH ? msg.slice(0, -(msg.length - MAX_REASON_MESSAGE_LENGTH)) : msg
    })
  }

  _resetKickConfirm = () => {
    this.setState({ kickConfirmActive: false })
  }

  _resetChangePositionConfirm = () => {
    this.setState({ changePositionConfirm: { active: false, positionID: null, confirmMessage: null } })
  }

  _handleKick = (): void => {
    if (!this.state.kickConfirmActive) {
      this.props.checkKickMemberOpportunity(this.props.member.userID)
    }
    if (this.state.kickConfirmActive) {
      this.props.checkKickMemberOpportunitySuccess(this.props.member.userID)
    }
    this.setState(prevState => ({
      kickConfirmActive: !prevState.kickConfirmActive,
      kickReason: ''
    }))
  }

  _handleKickConfirmClose = (): void => {
    this._resetKickConfirm()
    this.props.checkKickMemberOpportunitySuccess(this.props.member.userID)
  }

  _handleKickConfirm = () => {
    const {
      member: { userID },
      kickMember
    } = this.props
    const { kickReason } = this.state

    kickMember(userID, kickReason.trim())
  }

  _handlePositionChange = (memberID: number, positionID: number, confirmMessage: string = null): void => {
    const { currentUser, member, positions, assignPosition } = this.props

    if (confirmMessage) {
      this.setState({ changePositionConfirm: { active: true, positionID, confirmMessage } })
    } else if (currentUser.id === memberID) {
      const message = `Are you sure you want to step down as ${positions[member.positionID].label} of {faction}?`

      this.setState({ changePositionConfirm: { active: true, positionID, confirmMessage: message } })
    } else {
      assignPosition({ memberID, positionID })
    }
  }

  _handlePositionChangeDecline = (): void => {
    this._resetChangePositionConfirm()
  }

  _handlePositionChangeConfirm = () => {
    const {
      member: { userID },
      assignPosition
    } = this.props
    const {
      changePositionConfirm: { positionID }
    } = this.state

    assignPosition({ memberID: userID, positionID })
    this._resetChangePositionConfirm()
  }

  renderKickButton = (member: IMember, positionID: keyof IPositions): React.ReactElement => {
    const { positions } = this.props
    const { kickConfirmActive } = this.state

    return (
      <div className={cn(tableStyles.tableCell, tableStyles.kick)}>
        <Kick
          active={kickConfirmActive}
          disabled={!positions[positionID].currentUserOperations.canKick}
          userID={member.userID}
          userName={member.playername}
          positionID={positionID}
          onKick={this._handleKick}
        />
      </div>
    )
  }

  renderKickConfirm = (): React.ReactElement => {
    const {
      member: { playername, kickOpportunity },
      error
    } = this.props
    const { kickReason } = this.state

    if (error) {
      return (
        <div className={s.confirm}>
          <span role='alert' className={s.confirmError}>
            {error?.message}
          </span>
        </div>
      )
    }

    return (
      <div className={s.confirm}>
        <span className={s.confirmText} role='alert'>{`Are you sure you want to kick ${playername}?`}</span>
        {kickOpportunity?.error ? <div className={s.kickError}>{kickOpportunity?.message}</div> : null}
        <div className={s.reasonInputWrapper}>
          <input
            className={s.reasonInput}
            placeholder='Public reason (optional, up to 200 characters)'
            ref={this.reasonInput}
            onChange={this._handleMessageChange}
            value={kickReason}
            disabled={kickOpportunity?.error}
          />
        </div>
        {this.renderConfirmButtons(this._handleKickConfirm, this._handleKickConfirmClose)}
      </div>
    )
  }

  renderPositionChangeConfirm = (): React.ReactElement => {
    const {
      member: { playername },
      faction: { name }
    } = this.props
    const {
      changePositionConfirm: { confirmMessage }
    } = this.state
    const message = confirmMessage.replace('{user}', playername).replace('{faction}', name)

    return (
      <div className={s.confirm}>
        <span className='m-right10' role='alert'>
          {message}
        </span>
        {this.renderConfirmButtons(this._handlePositionChangeConfirm, this._handlePositionChangeDecline)}
      </div>
    )
  }

  renderConfirmButtons = (acceptAction: () => void, declineAction: () => void): React.ReactElement => {
    const {
      member: { kickOpportunity }
    } = this.props

    return (
      <>
        <button
          type='button'
          autoFocus={true}
          className={cn('t-blue b h', s.resetButton)}
          onClick={acceptAction}
          disabled={kickOpportunity?.error && this.state.kickConfirmActive}
        >
          Yes
        </button>
        <button type='button' className={cn('t-blue b h m-left10', s.resetButton)} onClick={declineAction}>
          No
        </button>
      </>
    )
  }

  renderRow = (): React.ReactElement => {
    return <div className={tableStyles.tableRow}>{this.renderRowData()}</div>
  }

  renderRowData = () => {
    const {
      member,
      currentUser: {
        permissions: { kick },
        settings: { showImages }
      },
      faction,
      mediaType
    } = this.props
    const { level, days, positionID, changePositionTimestamp, onlineStatus, ...userProps } = member

    if (member.state?.kicked) {
      return (
        <div className={s.successMessage}>
          <span className={s.successMessageText} dangerouslySetInnerHTML={{ __html: member.state?.message }} />
        </div>
      )
    }

    return (
      <>
        <div className={cn(tableStyles.tableCell, tableStyles.member)}>
          <Person
            user={userProps}
            status={onlineStatus}
            faction={faction}
            showImages={showImages}
            mediaType={mediaType}
          />
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.level)}>{String(level)}</div>
        <div className={cn(tableStyles.tableCell, tableStyles.days)}>{String(days)}</div>
        <div className={cn(tableStyles.tableCell, tableStyles.position, { [tableStyles.canKick]: kick })}>
          <Position
            changePositionTimestamp={changePositionTimestamp}
            positionID={positionID}
            memberID={userProps.userID}
            onPositionChange={this._handlePositionChange}
          />
        </div>
        {kick && this.renderKickButton(member, positionID)}
      </>
    )
  }

  render() {
    const { kickConfirmActive, changePositionConfirm } = this.state
    const active = kickConfirmActive || changePositionConfirm.active

    return (
      <div className={cn(s.rowWrapper, { [s.active]: active })}>
        {this.renderRow()}
        {kickConfirmActive && this.renderKickConfirm()}
        {changePositionConfirm.active && this.renderPositionChangeConfirm()}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState, ownProps: Pick<IProps, 'member'>) => {
  return {
    positions: state.data.positions,
    currentUser: state.data.currentUser,
    faction: state.data.faction,
    error: state.ui.errors.kick.find(err => err.id === ownProps.member.userID),
    mediaType: state.browser.mediaType
  }
}

const mapActionsToProps = {
  kickMember: kickMemberAction,
  assignPosition: assignPositionAction,
  checkKickMemberOpportunity,
  checkKickMemberOpportunitySuccess
}

export default connect(mapStateToProps, mapActionsToProps)(Row)
