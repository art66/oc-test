import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Tooltip from '@torn/shared/components/Tooltip'
import cn from 'classnames'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import Dropdown from '@torn/shared/components/Dropdown'
import { IReduxState, IPositions } from '../../../../interfaces'
import cellsStyles from './index.cssmodule.scss'
import globalStyles from '../../../../styles/global.cssmodule.scss'
import Recruit from './Recruit'

interface IProps {
  changePositionTimestamp: number
  positions: IPositions
  positionsOrder: number[]
  positionID: number
  memberID: number
  onPositionChange: (memberID: number, positionID: number, confirmMessage?: string) => void
}

class Position extends PureComponent<IProps> {
  getPositionsArray = (): { id: string; name: string; customClass: string }[] => {
    const { positions, positionsOrder, positionID: currentPositionID } = this.props

    return positionsOrder
      .filter(positionID => positions[positionID].currentUserOperations.canSelect && positionID !== currentPositionID)
      .map(positionID => {
        const { label, permissionsLevel } = positions[positionID]

        return {
          id: String(positionID),
          name: label,
          customClass: cellsStyles[`permissionsLevel${firstLetterUpper({ value: permissionsLevel })}`]
        }
      })
  }

  _handlePositionChange = (item): void => {
    const { memberID, onPositionChange, positions } = this.props

    onPositionChange(memberID, +item.id, positions[item.id].assignConfirmMessage)
  }

  renderPositionTooltip = (elID, label) => {
    return (
      <Tooltip parent={elID} position='top' arrow='center'>
        <span>{label}</span>
      </Tooltip>
    )
  }

  render() {
    const { positions, positionID, changePositionTimestamp, memberID } = this.props
    const {
      label,
      currentUserOperations: { canChange },
      permissionsLevel
    } = positions[positionID]
    const cellStyle: string = cn(
      cellsStyles[`permissionsLevel${firstLetterUpper({ value: permissionsLevel })}`],
      cellsStyles.cell
    )

    if (changePositionTimestamp) {
      return (
        <Recruit
          memberID={memberID}
          label={label}
          style={cellStyle}
          changePositionTimestamp={changePositionTimestamp}
        />
      )
    }

    const elID = `controls-member-${memberID}-position`

    return canChange ? (
      <div id={elID} style={{ width: '100%' }}>
        <Dropdown
          tabIndex={0}
          list={this.getPositionsArray()}
          selected={{ id: positionID, name: label }}
          controlled={true}
          className={cn(cellStyle, cellsStyles.positionDropdown)}
          onChange={this._handlePositionChange}
        />
        {this.renderPositionTooltip(elID, label)}
      </div>
    ) : (
      <>
        <span id={elID} className={cn(cellStyle, globalStyles.ellipsis)}>
          {label}
        </span>
        {this.renderPositionTooltip(elID, label)}
      </>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    positions: state.data.positions,
    positionsOrder: state.data.positionsOrder
  }
}

export default connect(mapStateToProps)(Position)
