import Position from './Position'
import Person from './Person'
import Kick from './Kick'

export {
  Position,
  Person,
  Kick
}
