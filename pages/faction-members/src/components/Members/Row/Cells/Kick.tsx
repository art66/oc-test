import React, { PureComponent } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import { IPositions } from '../../../../interfaces'
import tableStyles from '../../../../styles/tableStyles.cssmodule.scss'
import s from './index.cssmodule.scss'

declare function getCookie(key: string): string

interface IProps {
  active: boolean
  disabled: boolean
  positionID: keyof IPositions
  userID: number
  userName: string
  onKick: () => void
}

class Kick extends PureComponent<IProps> {
  _getDisabledState = (): boolean => {
    const { disabled, userID } = this.props

    return disabled || getCookie('uid') === String(userID)
  }

  _getIconPreset = (): { type: string; subtype: string } => {
    const { active } = this.props
    const disabled = this._getDisabledState()
    const checkActive = active ? 'REMOVE_ACTIVE' : 'REMOVE'

    return {
      type: 'faction',
      subtype: disabled ? 'REMOVE_DISABLED' : checkActive
    }
  }

  _handleKick = (): void => {
    const { onKick } = this.props

    onKick()
  }

  render() {
    const { userName } = this.props

    return (
      <button
        type='button'
        aria-label={`Kick ${userName} from the faction`}
        disabled={this._getDisabledState()}
        className={cn(s.kickButton)}
        onClick={this._handleKick}
      >
        <SVGIconGenerator
          iconName='Close'
          customClass={cn(tableStyles.icon, { [tableStyles.kickIconDisabled]: this._getDisabledState() })}
          iconsHolder={iconsHolder}
          preset={this._getIconPreset()}
          onHover={{ active: true, preset: { type: 'faction', subtype: 'REMOVE_ACTIVE' } }}
        />
      </button>
    )
  }
}

export default Kick
