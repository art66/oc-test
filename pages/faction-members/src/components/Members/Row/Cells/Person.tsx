import React, { PureComponent } from 'react'
import UserInfo from '@torn/shared/components/UserInfo'
import { IFaction } from '../../../../interfaces'
import cellsStyles from './index.cssmodule.scss'

interface IProps {
  user: {
    userID: number
    playername: string
    userImageUrl?: string | null
  }
  status: 'online' | 'idle' | 'offline'
  faction: IFaction
  mediaType: 'mobile' | 'tablet' | 'desktop'
  showImages: boolean
}

class Person extends PureComponent<IProps> {
  render() {
    const { user, status, faction, showImages, mediaType } = this.props
    const factionProps =
      mediaType === 'desktop' ?
        { ID: faction.id, name: faction.tag, imageUrl: faction.image, rank: faction.rank } :
        null

    return (
      <UserInfo
        status={{ mode: status, isActive: true, addTooltip: true }}
        user={{ ID: user.userID, name: user.playername, imageUrl: user.userImageUrl }}
        faction={factionProps}
        showImages={showImages}
        customStyles={{ blockWrap: cellsStyles.memberWrap, img: cellsStyles.memberImgWrap }}
      />
    )
  }
}

export default Person
