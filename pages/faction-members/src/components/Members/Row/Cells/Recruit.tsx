import React, { PureComponent } from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import { connect } from 'react-redux'
import { IReduxState } from '../../../../interfaces'
import { SECONDS_IN_HALF_HOUR } from '../../../../constants'

interface IProps {
  memberID: number
  label: string
  style: string
  changePositionTimestamp: number
  mediaType: 'mobile' | 'tablet' | 'desktop'
}

interface IState {
  timeNow: number
}

class Recruit extends PureComponent<IProps, IState> {
  private _timerID: any

  constructor(props) {
    super(props)

    this.state = {
      timeNow: Math.floor(Date.now() / 1000)
    }
  }

  componentDidMount(): void {
    this._timerID = setInterval(this.tick, 1000 * SECONDS_IN_HALF_HOUR)
  }

  componentWillUnmount(): void {
    clearInterval(this._timerID)
  }

  tick = (): void => {
    const { timeNow } = this.state
    const { changePositionTimestamp } = this.props

    if (timeNow >= changePositionTimestamp) {
      clearInterval(this._timerID)

      return
    }

    this.setState((prevState: IState) => ({
      timeNow: prevState.timeNow + SECONDS_IN_HALF_HOUR
    }))
  }

  render() {
    const { style, label, mediaType, changePositionTimestamp, memberID } = this.props
    const { timeNow } = this.state
    const timeLeft: number = changePositionTimestamp - timeNow
    const hoursLeft: number = Math.ceil(timeLeft / 60 / 60)
    const elID = `controls-member-${memberID}-${label}`

    return (
      <>
        <span id={elID} className={style}>
          {mediaType === 'desktop' ? `${label} (${hoursLeft}h left)` : label}
        </span>
        <Tooltip parent={elID} arrow='center' position='top'>
          <span>
            {label} ({hoursLeft}h left)
          </span>
        </Tooltip>
      </>
    )
  }
}

const mapStateToProps = (state: IReduxState) => ({
  mediaType: !document.body.classList.contains('r') ? 'desktop' : state.browser.mediaType
})

export default connect(mapStateToProps)(Recruit)
