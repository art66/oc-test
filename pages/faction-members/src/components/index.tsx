import React from 'react'
import { connect } from 'react-redux'
import MembersHeader from './MembersHeader'
import Members from './Members'
import { IMember, IMembersFilter, IMembersSort, IPositions, IReduxState } from '../interfaces'

interface IProps {
  members: IMember[]
  positions: IPositions
  membersFilter: IMembersFilter
  membersSort: IMembersSort
}

const MainWrapper = (props: IProps) => {
  const getFilteredMembers = (members: IMember[], value: string): IMember[] => {
    return members.filter(member => member.playername.toLowerCase().includes(value.toLowerCase()))
  }

  const getPreparedMembers = () => {
    const {
      members,
      membersFilter: { active, value },
      positions
    } = props
    const updateMembers = members.map(member => ({
      ...member,
      positionImportance: positions[member.positionID].permissionsLevelImportance,
      positionLabel: positions[member.positionID].label
    }))

    return active ? getFilteredMembers(updateMembers, value) : updateMembers
  }

  const preparedMembers = getPreparedMembers() || []

  return (
    <>
      <MembersHeader membersAmount={preparedMembers.length} />
      <Members members={preparedMembers} />
    </>
  )
}

const mapStateToProps = (state: IReduxState) => {
  return {
    members: state.data.members,
    positions: state.data.positions,
    membersFilter: state.ui.membersFilter,
    membersSort: state.ui.membersSort
  }
}

export default connect(mapStateToProps)(MainWrapper)
