import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import tableStyles from '../../styles/tableStyles.cssmodule.scss'
import { changeMembersSort } from '../../actions/ui'
import { IMembersSort, IReduxState, TSortField } from '../../interfaces'
import { ENTER_KEY_CODE } from '../../constants'

interface IProps {
  children: React.ReactChild
  changeMembersSort: (sortField: TSortField) => void
  field: TSortField
  membersSort: IMembersSort
  hasDirectionClass: boolean
  customClasses: string
}

const Column = (props: IProps) => {
  const handleSortMembers = () => {
    props.changeMembersSort(props.field)
  }

  const handleKeyPressOnCol = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === ENTER_KEY_CODE) {
      handleSortMembers()
    }
  }

  return (
    <div
      tabIndex={0}
      role='button'
      className={`${cn(tableStyles.tableCell, {
        [tableStyles[props.membersSort.sortDirection]]: props.hasDirectionClass
      })} ${props.customClasses}`}
      onClick={handleSortMembers}
      onKeyPress={handleKeyPressOnCol}
    >
      {props.children}
    </div>
  )
}

const mapStateToProps = (state: IReduxState) => {
  return {
    membersSort: state.ui.membersSort
  }
}

const mapActionsToProps = {
  changeMembersSort
}

export default connect(mapStateToProps, mapActionsToProps)(Column)
