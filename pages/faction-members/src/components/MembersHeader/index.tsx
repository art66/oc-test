import React, { ChangeEvent, PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import { IFaction, ICurrentUser, IMembersFilter, IReduxState, IMembersSort, TSortField } from '../../interfaces'
import { toggleMembersFilter, changeMembersFilterValue, changeMembersSort } from '../../actions/ui'
import Column from './Column'
import tableStyles from '../../styles/tableStyles.cssmodule.scss'
import s from './index.cssmodule.scss'

interface IProps {
  currentUser: ICurrentUser
  faction: IFaction
  memberFilter: IMembersFilter
  membersSort: IMembersSort
  mediaType: 'mobile' | 'tablet' | 'desktop'
  toggleFilter: () => void
  changeFilterValue: (value: string) => void
  changeMembersSort: (sortField: TSortField) => void
  membersAmount: number
}

class ApplicationList extends PureComponent<IProps> {
  handleFilterChange = (e: ChangeEvent<HTMLInputElement>): void => {
    const { changeFilterValue } = this.props

    changeFilterValue(e.target.value)
  }

  handleSearch = e => {
    this.stopPropagationOnElement(e)
    this.props.toggleFilter()
  }

  stopPropagationOnElement = e => e.stopPropagation()

  hasDirectionClass = (field: TSortField) => {
    const { membersSort, membersAmount } = this.props

    return membersSort.sortField === field && membersAmount > 0
  }

  renderSearchButton = (): React.ReactChild => {
    return (
      <button
        type='button'
        aria-label='Toggle members filter field'
        className={s.btnTransparent}
        onClick={this.handleSearch}
      >
        <SVGIconGenerator
          iconName='Search'
          customClass={tableStyles.icon}
          iconsHolder={iconsHolder}
          preset={{ type: 'faction', subtype: 'SEARCH' }}
          onHover={{ active: true, preset: { type: 'faction', subtype: 'SEARCH_HOVER' } }}
        />
      </button>
    )
  }

  renderMemberCellHeader = (): React.ReactChild => {
    const {
      faction: { membersAmount, maxMembersAmount },
      memberFilter,
      mediaType
    } = this.props
    const membersCount = `${membersAmount} / ${maxMembersAmount}`
    const isDesktop = mediaType === 'desktop'

    return memberFilter.active ? (
      <>
        <input
          type='text'
          aria-label='Type here to filter members'
          autoFocus={true}
          maxLength={30}
          placeholder={isDesktop ? `Search ${membersCount} faction members` : `${membersCount} members`}
          className={s.filterInput}
          onChange={this.handleFilterChange}
          onClick={this.stopPropagationOnElement}
          onKeyPress={this.stopPropagationOnElement}
        />
        {this.renderSearchButton()}
      </>
    ) : (
      <>
        <span className={s.membersCount}>{`${membersCount} ${isDesktop ? 'Faction ' : ''}Members`}</span>
        {this.renderSearchButton()}
      </>
    )
  }

  renderKickCellHeader = (): React.ReactChild => (
    <div className={cn(tableStyles.tableCell, tableStyles.kick)}>
      <SVGIconGenerator
        iconName='Close'
        customClass={cn(tableStyles.icon, tableStyles.kickIcon)}
        iconsHolder={iconsHolder}
        preset={{ type: 'faction', subtype: 'REMOVE_BOLD' }}
      />
    </div>
  )

  render() {
    const {
      currentUser: { permissions }
    } = this.props

    return (
      <div className={tableStyles.tableHeader}>
        <Column
          customClasses={cn(tableStyles.member, s.membersFilter)}
          hasDirectionClass={this.hasDirectionClass('playername')}
          field='playername'
        >
          {this.renderMemberCellHeader()}
        </Column>
        <Column customClasses={tableStyles.level} hasDirectionClass={this.hasDirectionClass('level')} field='level'>
          Lvl
        </Column>
        <Column customClasses={tableStyles.days} hasDirectionClass={this.hasDirectionClass('days')} field='days'>
          Days
        </Column>
        <Column
          customClasses={cn(tableStyles.position, { [tableStyles.canKick]: permissions.kick })}
          hasDirectionClass={this.hasDirectionClass('positionID')}
          field='positionID'
        >
          Position
        </Column>
        {permissions.kick && this.renderKickCellHeader()}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    currentUser: state.data.currentUser,
    faction: state.data.faction,
    members: state.data.members,
    memberFilter: state.ui.membersFilter,
    mediaType: state.browser.mediaType,
    membersSort: state.ui.membersSort
  }
}

const mapActionsToProps = {
  toggleFilter: toggleMembersFilter,
  changeFilterValue: changeMembersFilterValue,
  changeMembersSort
}

export default connect(mapStateToProps, mapActionsToProps)(ApplicationList)
