export const SECONDS_IN_HALF_HOUR = 1800
export const MAX_REASON_MESSAGE_LENGTH = 200
export const ASC = 'asc'
export const DESC = 'desc'
export const ENTER_KEY_CODE = 'Enter'
