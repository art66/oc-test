import { DESC } from '../constants'

export default {
  data: {
    currentUser: {
      permissions: {},
      settings: {}
    },
    positions: {},
    positionsOrder: [],
    faction: {},
    members: []
  },
  ui: {
    membersFilter: {
      active: false,
      value: ''
    },
    membersSort: {
      sortField: 'level',
      sortDirection: DESC
    },
    errors: {
      kick: []
    }
  }
}
