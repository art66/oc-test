import initialState from './initialState'
import * as a from '../constants/actionTypes'
import { IReduxState } from '../interfaces'
import { CHECK_KICK_MEMBER_OPPORTUNITY_SUCCESS } from '../constants/actionTypes'

const ACTION_HANDLERS = {
  [a.FETCH_MEMBERS]: (state: Readonly<IReduxState['data']>) => {
    return {
      ...state,
      loading: true
    }
  },
  [a.FETCH_MEMBERS_SUCCESS]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      ...payload,
      loading: false
    }
  },
  [a.JOIN_MEMBER]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      members: [...state.members, payload]
    }
  },
  [a.KICK_MEMBER_SUCCESS]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      members: state.members.filter(member => member.userID !== payload)
    }
  },
  [a.SET_KICK_MEMBER_MESSAGE]: (state: IReduxState['data'], { payload }) => {
    return {
      ...state,
      members: state.members.map(member => {
        return member.userID === payload.userID ?
          {
            ...member,
            state: {
              kicked: true,
              message: payload.message
            }
          } :
          member
      })
    }
  },
  [a.ASSIGN_POSITION_SUCCESS]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      members: state.members.map(member => {
        const changedMember = payload.find(changed => changed.userID === member.userID)

        return changedMember ?
          {
            ...member,
            positionID: changedMember.newPositionID,
            changePositionTimestamp: null
          } :
          member
      })
    }
  },
  [a.CREATE_POSITION]: (state: Readonly<IReduxState['data']>, { payload }) => {
    const { id, ...positionData } = payload

    return {
      ...state,
      positions: {
        ...state.positions,
        [id]: {
          ...positionData
        }
      },
      positionsOrder: [...state.positionsOrder, id]
    }
  },
  [a.UPDATE_POSITION]: (state: Readonly<IReduxState['data']>, { payload }) => {
    const { id, ...positionData } = payload

    return {
      ...state,
      positions: {
        ...state.positions,
        [id]: {
          ...state.positions[id],
          ...positionData
        }
      }
    }
  },
  [a.REMOVE_POSITION]: (state: Readonly<IReduxState['data']>, { payload }) => {
    const {
      members,
      positions,
      faction: { defaultPosition }
    } = state
    const { [payload]: _, ...restPositions } = positions

    return {
      ...state,
      positions: restPositions,
      positionsOrder: state.positionsOrder.filter(pos => pos !== payload),
      members: members.map(member => {
        return member.positionID === payload ?
          {
            ...member,
            positionID: defaultPosition
          } :
          member
      })
    }
  },
  [a.UPDATE_POSITIONS_INTERACTION]: (state: Readonly<IReduxState['data']>, { payload }) => {
    const { positions } = state

    return {
      ...state,
      positions: Object.keys(positions).reduce((acc, key) => {
        const position = positions[key]
        const interaction = payload[key]

        return {
          ...acc,
          [key]: {
            ...position,
            ...interaction
          }
        }
      }, {})
    }
  },
  [a.UPDATE_CURRENT_USER_PERMISSIONS]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      currentUser: {
        ...state.currentUser,
        permissions: {
          ...state.currentUser.permissions,
          ...payload
        }
      }
    }
  },
  [a.CHECK_KICK_MEMBER_OPPORTUNITY_FAILURE]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      members: state.members.map(member => {
        return payload.id === member.userID ? {
          ...member,
          kickOpportunity: {
            error: true,
            message: payload.message
          }
        } : member
      })
    }
  },
  [a.CHECK_KICK_MEMBER_OPPORTUNITY_SUCCESS]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      members: state.members.map(member => {
        return payload.id === member.userID ? {
          ...member,
          kickOpportunity: {
            error: false,
            message: ''
          }
        } : member
      })
    }
  }
}

const rootReducer = (state: any = initialState.data, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default rootReducer
