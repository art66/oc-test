import initialState from './initialState'
import {
  TOGGLE_MEMBERS_FILTER,
  CHANGE_MEMBERS_FILTER_VALUE,
  KICK_ERROR_FAILURE,
  CHANGE_MEMBERS_SORT
} from '../constants/actionTypes'
import { IReduxState } from '../interfaces'
import { ASC, DESC } from '../constants'

const ACTION_HANDLERS = {
  [TOGGLE_MEMBERS_FILTER]: (state: Readonly<IReduxState['ui']>) => {
    return {
      ...state,
      membersFilter: {
        ...state.membersFilter,
        active: !state.membersFilter.active
      }
    }
  },
  [CHANGE_MEMBERS_FILTER_VALUE]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      membersFilter: {
        ...state.membersFilter,
        value: payload
      }
    }
  },
  [CHANGE_MEMBERS_SORT]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      membersSort: {
        sortField: payload.sortField,
        sortDirection:
          payload.sortField === state.membersSort.sortField && state.membersSort.sortDirection === DESC ? ASC : DESC
      }
    }
  },
  [KICK_ERROR_FAILURE]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    const { id, message } = payload

    return {
      ...state,
      errors: {
        ...state.errors,
        kick: [...state.errors.kick, { id, message }]
      }
    }
  }
}

const uiReducer = (state: any = initialState.ui, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default uiReducer
