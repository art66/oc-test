import React, { Component } from 'react'
import { Store } from 'redux'
import { connect, Provider } from 'react-redux'
import { fetchMembers as fetchMembersAction } from '../actions'
import AppLayout from '../layout/AppLayout'

interface IProps {
  store: Store
  fetchMembers: () => void
}

class AppContainer extends Component<IProps> {
  componentDidMount() {
    const { fetchMembers } = this.props

    fetchMembers()
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <AppLayout />
      </Provider>
    )
  }
}

const mapActionToProps = {
  fetchMembers: fetchMembersAction
}

export default connect(null, mapActionToProps)(AppContainer)
