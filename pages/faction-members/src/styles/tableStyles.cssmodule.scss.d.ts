// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'asc': string;
  'canKick': string;
  'days': string;
  'desc': string;
  'globalSvgShadow': string;
  'icon': string;
  'kick': string;
  'kickIcon': string;
  'kickIconDisabled': string;
  'level': string;
  'member': string;
  'position': string;
  'rowsWrapper': string;
  'tableCell': string;
  'tableEmpty': string;
  'tableHeader': string;
  'tableRow': string;
}
export const cssExports: CssExports;
export default cssExports;
