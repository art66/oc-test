export type TAssignPositionPayload = {
  userID: number
  newPositionID: number
  oldPositionID: number
}[]


export type TUpdateCurrentUserPayload = {
  permissions: {
    changePermissions?: boolean
    managePositions?: boolean
    kick?: boolean
  }
}
