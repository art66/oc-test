import { createAction } from 'redux-actions'
import * as a from '../constants/actionTypes'

export const fetchMembers = createAction(a.FETCH_MEMBERS)
export const fetchMembersSuccess = createAction(a.FETCH_MEMBERS_SUCCESS, data => data)

export const joinMember = createAction(a.JOIN_MEMBER, newMember => newMember)
export const kickMember = createAction(a.KICK_MEMBER, (id, reason) => ({ id, reason }))
export const kickMemberSuccess = createAction(a.KICK_MEMBER_SUCCESS, id => id)
export const setKickMemberMessage = createAction(a.SET_KICK_MEMBER_MESSAGE, (userID, message) => ({ userID, message }))
export const checkKickMemberOpportunity = createAction(a.CHECK_KICK_MEMBER_OPPORTUNITY, (id) => ({ id }))
export const checkKickMemberOpportunityFailure = createAction(
  a.CHECK_KICK_MEMBER_OPPORTUNITY_FAILURE,
  (id, message) => ({ id, message })
)
export const checkKickMemberOpportunitySuccess = createAction(a.CHECK_KICK_MEMBER_OPPORTUNITY_SUCCESS, (id) => ({ id }))

export const assignPosition = createAction(a.ASSIGN_POSITION, data => data)
export const assignPositionSuccess = createAction(a.ASSIGN_POSITION_SUCCESS, data => data)

export const createPosition = createAction(a.CREATE_POSITION, newPosition => newPosition)
export const updatePosition = createAction(a.UPDATE_POSITION, newPosition => newPosition)
export const removePosition = createAction(a.REMOVE_POSITION, id => id)
export const updateCurrentUserPermissions = createAction(a.UPDATE_CURRENT_USER_PERMISSIONS, permissions => permissions)
export const updatePositionsInteraction = createAction(
  a.UPDATE_POSITIONS_INTERACTION,
  positionsInteraction => positionsInteraction
)
