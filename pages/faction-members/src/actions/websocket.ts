import {
  assignPositionSuccess,
  kickMemberSuccess,
  joinMember,
  createPosition,
  removePosition,
  updatePosition,
  updatePositionsInteraction,
  updateCurrentUserPermissions
} from './index'
import { IPosition, IPositionsMemberInteraction, IMember } from '../interfaces'
import * as types from './types'

declare const WebsocketHandler: any

const initWS = (dispatch: any): void => {
  WebsocketHandler.addEventListener('faction', 'kick', (payload: { user: { userID: number } }): void => {
    if (document.visibilityState === 'hidden') {
      dispatch(kickMemberSuccess(payload.user.userID))
    }
  })

  WebsocketHandler.addEventListener('faction', 'join', (payload: { user: IMember }): void => {
    dispatch(joinMember(payload.user))
  })

  WebsocketHandler.addEventListener(
    'factionPositions',
    'assignPositions',
    (data: types.TAssignPositionPayload): void => {
      dispatch(assignPositionSuccess(data))
    }
  )

  WebsocketHandler.addEventListener('factionPositions', 'createPosition', (newPosition: IPosition): void => {
    dispatch(createPosition(newPosition))
  })

  WebsocketHandler.addEventListener('factionPositions', 'removePosition', (position: { id: number }): void => {
    dispatch(removePosition(position.id))
  })

  WebsocketHandler.addEventListener('factionPositions', 'updatePosition', (newPosition: IPosition): void => {
    dispatch(updatePosition(newPosition))
  })

  WebsocketHandler.addEventListener(
    'factionPositions',
    'updateMemberPositions',
    (positionsInteraction: IPositionsMemberInteraction): void => {
      dispatch(updatePositionsInteraction(positionsInteraction))
    }
  )

  WebsocketHandler.addEventListener(
    'factionPositions',
    'updateCurrentUser',
    (payload: types.TUpdateCurrentUserPayload): void => {
      dispatch(updateCurrentUserPermissions(payload.permissions))
    }
  )
}

export default initWS
