import { createAction } from 'redux-actions'
import {
  TOGGLE_MEMBERS_FILTER,
  CHANGE_MEMBERS_FILTER_VALUE,
  KICK_ERROR_FAILURE,
  CHANGE_MEMBERS_SORT
} from '../constants/actionTypes'
import { TSortField } from '../interfaces'

export const toggleMembersFilter = createAction(TOGGLE_MEMBERS_FILTER)
export const changeMembersFilterValue = createAction(CHANGE_MEMBERS_FILTER_VALUE, value => value)
export const changeMembersSort = createAction(CHANGE_MEMBERS_SORT, (sortField: TSortField) => ({ sortField }))

export const kickErrorFailure = createAction(KICK_ERROR_FAILURE, (id: number, message: string) => ({ id, message }))
