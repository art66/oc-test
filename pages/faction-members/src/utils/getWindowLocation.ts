const currentLocation = () => {
  const getWindowLocation = window.location.hash.toString()

  return getWindowLocation.substr(2, getWindowLocation.length)
}

export default currentLocation()
