declare function addRFC(url: string): RequestInfo

const MAIN_URL = '/page.php?'
const fetchUrl = (url: string, data: object = {}) => {

  return fetch(addRFC(MAIN_URL + url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      try {
        const json = JSON.parse(text)

        if (!json.success) {
          throw json?.message?.text
        }

        return json
      } catch (e) {
        throw new Error(e)
      }
    })
  })
}

export default fetchUrl
