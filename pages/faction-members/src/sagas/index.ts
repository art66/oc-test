import { put, takeEvery } from 'redux-saga/effects'
import * as a from '../constants/actionTypes'
import fetchUrl from '../utils/fetchURL'
import fetchURLEncoded from '../utils/fetchURLEncoded'
import {
  fetchMembersSuccess,
  setKickMemberMessage,
  checkKickMemberOpportunityFailure,
  checkKickMemberOpportunitySuccess
} from '../actions'
import { kickErrorFailure } from '../actions/ui'

interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* fetchData() {
  try {
    const response = yield fetchUrl('sid=factionsControlMembers&step=init')

    yield put(fetchMembersSuccess(response))
  } catch (error) {
    console.log(error)
  }
}

function* checkKickMemberOpportunity(action: IAction<{ id: number }>) {
  const { id } = action.payload

  try {
    const data = yield fetchURLEncoded({ step: 'checkKickMember', user: id })

    if (data.success) {
      yield put(checkKickMemberOpportunitySuccess(id))
    }
  } catch (error) {
    yield put(checkKickMemberOpportunityFailure(id, error.message))
    console.error(error.message)
  }
}

function* kickMemberSaga(action: IAction<{ id: number; reason: string }>) {
  const { id, reason } = action.payload

  try {
    const data = yield fetchURLEncoded({ step: 'kickMember', user: id, force: 0, reason })

    if (data.success) {
      yield put(setKickMemberMessage(id, data.text))
    }
  } catch (error) {
    yield put(kickErrorFailure(id, error.message))
    console.error(error.message)
  }
}

function* assignPositionSaga(action: IAction<{ memberID: number; positionID: number }>) {
  try {
    yield fetchUrl('sid=factionsControlMembers&step=assignPosition', action.payload)
  } catch (error) {
    console.error(error)
  }
}

export default function* appSaga() {
  yield takeEvery(a.FETCH_MEMBERS, fetchData)
  yield takeEvery(a.KICK_MEMBER, kickMemberSaga)
  yield takeEvery(a.ASSIGN_POSITION, assignPositionSaga)
  yield takeEvery(a.CHECK_KICK_MEMBER_OPPORTUNITY, checkKickMemberOpportunity)
}
