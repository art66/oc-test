import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'

export type TPermissionsLevel = 'low' | 'mid' | 'high' | 'default'
export type TSortField = 'level' | 'days' | 'positionID' | 'playername'
export type TSortDirection = 'desc' | 'asc'

export interface ICurrentUser {
  permissions: {
    changePosition: boolean
    kick: boolean
  }
  settings: {
    showImages: boolean
  }
  id: number
}

export interface IPositionMemberInteraction {
  currentUserOperations: {
    canChange: boolean
    canSelect: boolean
    canKick: boolean
  }
  assignConfirmMessage: string
}

export interface IPosition extends IPositionMemberInteraction {
  id: number
  label: string
  permissionsLevel: TPermissionsLevel
  permissionsLevelImportance: number
}

export interface IPositionsMemberInteraction {
  [key: number]: IPositionMemberInteraction
}

export interface IPositions {
  [key: number]: IPosition
}

export interface IMember {
  userID: number
  onlineStatus: 'online' | 'idle' | 'offline'
  userImageUrl?: string | null
  playername: string
  level: number
  days: number
  positionID: keyof IPositions
  changePositionTimestamp: number
  state?: {
    kicked: boolean
    message: string
  },
  kickOpportunity?: {
    error: boolean
    message: string
  }
}

export interface IFaction {
  id: number
  name: string
  maxMembersAmount: number
  membersAmount: number
  defaultPosition: keyof IPositions
  image?: string
  tag?: string
  rank?: TFactionRank
}

export interface IMembersFilter {
  active: boolean
  value: string
}

export interface IMembersSort {
  sortField: TSortField
  sortDirection: TSortDirection
}

export interface IReduxState {
  browser: {
    mediaType: 'mobile' | 'tablet' | 'desktop'
  }
  data: {
    currentUser: ICurrentUser
    positions: IPositions
    positionsOrder: number[]
    faction: IFaction
    members: IMember[]
    loading: boolean
  }
  ui: {
    membersFilter: IMembersFilter
    membersSort: IMembersSort
    errors: {
      kick: {
        id: keyof IPositions
        message: string
      }[]
    }
  }
}
