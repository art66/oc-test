import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import dataReducer from '../reducers'
import uiReducer from '../reducers/ui'

export const makeRootReducer = (): any => {
  return combineReducers({
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    }),
    data: dataReducer,
    ui: uiReducer
  })
}

export default makeRootReducer
