# Models-Editor - Torn


## 1.1.4
 * Fixed items doubling in the left menu section.

## 1.1.3
 * Fixed hair/bread/moustaches selection.

## 1.1.2
 * Fixed background masks overlap.
 * Fixed wrap app top margin.

## 1.1.1
 * Fixed background positioning

## 1.1.0
 * Added background layout feature for particular items.

## 1.0.1
 * Added new slotsMapBackground inside Items extractItemFields sorting selector.

## 1.0.0
 * initial Release.
