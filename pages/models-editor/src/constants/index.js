export const BASE_URL = '/loader.php?sid=modelsEditor&type=data'
export const IMAGES_PATH = '/images/v2/items/player-model-f8b24382981b1011a229ca214024f2bb'
export const CHECK_DARK_MODE = 'CHECK_DARK_MODE'

export const URL_VAR = Math.random()

export const SLOTS = [
  {
    label: 'Head',
    prefix: 'H',
    slots: [1, 1, 1, 1, 1]
  },
  {
    label: 'Eyes',
    prefix: 'E',
    slots: [0, 0, 1, 0, 0]
  },
  {
    label: 'Nose',
    prefix: 'N',
    slots: [0, 0, 1, 0, 0]
  },
  {
    label: 'Mouth',
    prefix: 'M',
    slots: [0, 0, 1, 0, 0]
  },
  {
    label: 'Neck',
    prefix: 'P',
    slots: [0, 0, 1, 0, 0]
  },
  {
    label: 'Body',
    prefix: 'B',
    slots: [1, 1, 1, 1, 1]
  },
  {
    label: 'Wrist',
    prefix: 'W',
    slots: [0, 0, 1, 0, 0]
  },
  {
    label: 'Finger',
    prefix: 'R',
    slots: [0, 0, 1, 0, 0]
  },
  {
    label: 'Hands',
    prefix: 'D',
    slots: [1, 1, 1, 1, 1]
  },
  {
    label: 'Legs',
    prefix: 'L',
    slots: [1, 1, 1, 1, 1]
  },
  {
    label: 'Feet',
    prefix: 'F',
    slots: [1, 1, 1, 1, 1]
  }
]

export const SLOTS_ORDER = {
  H: 0, // head
  E: 1, // eyes
  N: 2, // nose
  M: 3, // mouth
  P: 4, // neck
  B: 5, // body
  W: 6, // wrist
  R: 7, // finger
  D: 8, // hands
  L: 9, // legs
  F: 10 // feet
}
