import { showInfoBox } from '../../actions'

export const fetchInterceptor = store => next => action => {
  const { payload = {} } = action

  const manageNavigation = (json = {}) => {
    if (json.debugInfo) {
      console.log(json.debugInfo)
    } else if (json.redirect !== undefined) {
      if (json.redirect === true) {
        location.href = `${location.protocol}//${location.hostname}/${json.url}`
      } else if (json.redirect === false) {
        store.dispatch(showInfoBox({ msg: json.content, color: 'red' }))
      }
    }
  }

  if (payload && payload.meta === 'ajax' && payload.json) {
    manageNavigation(payload.json)
  }

  return next(action)
}

export default fetchInterceptor
