import { createAction } from 'redux-actions'
import * as _ from 'lodash'
import { fetchUrl } from '@torn/shared/utils'
import { canUpdate, presetName as getPresetName } from '../selectors'
import { BASE_URL } from '../constants'

export function getInitialData() {
  return dispatch => {
    return fetchUrl(BASE_URL + '&step=getInitialData')
      .then(json => {
        dispatch(dataLoaded(json))
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const dataLoaded = createAction('data loaded', json => ({
  json,
  meta: 'ajax'
}))

export const selectItem = createAction('select item')

export function equipItem({ IDs }) {
  return (dispatch, getState) => {
    // const { app: { currentItem } } = getState()
    return fetchUrl(BASE_URL + '&step=equip&itemID', { itemID: IDs })
      .then(json => {
        dispatch(dataLoaded(json))
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export function unequipAll() {
  return (dispatch, getState) => {
    // const { app: { currentItem } } = getState()
    return fetchUrl(BASE_URL + '&step=unsetEquipped')
      .then(json => {
        dispatch(dataLoaded(json))
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export function saveSlot(slot) {
  return (dispatch, getState) => {
    const { app: { currentItem } } = getState()
    if (!currentItem) return
    const slots = (currentItem && currentItem.slotsMap && JSON.parse(currentItem.slotsMap)) || []
    const index = slots.indexOf(slot)
    if (index !== -1) {
      slots.splice(index, 1)
    } else {
      slots.push(slot)
    }
    return fetchUrl(BASE_URL + '&step=setSlots&itemID=' + currentItem.ID, {
      slots
    })
      .then(json => {
        dispatch(dataLoaded(json))
      })
      .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
  }
}

export const saveMask = maskID => (dispatch, getState) => {
  const { app: { currentItem } } = getState()
  if (!currentItem) return
  return fetchUrl(BASE_URL + '&step=setMask&itemID=' + currentItem.ID + '&maskID=' + maskID)
    .then(json => {
      dispatch(dataLoaded(json))
    })
    .catch(error => dispatch(showInfoBox({ msg: error.toString(), color: 'red' })))
}

export function loadPreset(presetID) {
  return (dispatch, getState) => {
    if (presetID) {
      return fetchUrl(BASE_URL + '&step=getPreset&presetID=' + presetID).then(json => {
        dispatch(dataLoaded(json))
        dispatch(setPreset({ presetID }))
      })
    } else {
      dispatch(unequipAll())
      dispatch(setPreset({ presetID: null }))
    }
  }
}

export const setPreset = createAction('set preset')

export function savePreset(presetName) {
  return (dispatch, getState) => {
    const { equippedItems } = getState().app
    if (equippedItems && equippedItems.length) {
      return fetchUrl(BASE_URL + '&step=savePreset&presetTitle=' + presetName).then(json => {
        dispatch(dataLoaded(json))
      })
    } else {
      console.log('nothing to save')
    }
  }
}

export function updatePreset(presetID) {
  return (dispatch, getState) => {
    const state = getState()
    const currentPreset = state.app.currentPreset
    const currentPresetName = getPresetName(state)
    if (currentPreset && canUpdate(state)) {
      return fetchUrl(BASE_URL + '&step=savePreset&presetID=' + currentPreset + '&title=' + currentPresetName).then(
        json => {
          dispatch(dataLoaded(json))
        }
      )
    } else {
      console.log('nothing to save')
    }
  }
}

export function removePreset() {
  return (dispatch, getState) => {
    const state = getState()
    const currentPreset = state.app.currentPreset
    if (currentPreset) {
      dispatch(setPreset({ presetID: null }))
      dispatch(unequipAll()).then(() => {
        return fetchUrl(BASE_URL + '&step=removePreset&&presetID=' + currentPreset).then(json => {
          dispatch(dataLoaded(json))
        })
      })
    } else {
      console.log('no preset selected')
    }
  }
}

export function random() {
  return (dispatch, getState) => {
    const { app: { items } } = getState()
    dispatch(unequipAll()).then(() => {
      // shuffle items
      // take first 20
      // return ID prop
      const IDs = _.map(_.take(_.shuffle(items), 20), _.property('ID'))
      dispatch(equipItem({ IDs }))
    })
  }
}

export const randomHair = createAction('RANDOM_HAIR')

export const switchGender = createAction('switch gender')

export const bringInfoBox = createAction('bring info box')

export const hideInfoBox = createAction('hide info box')

export const setCurrentHairPreset = createAction('SET_CURRENT_HAIR_PRESET')
export const setCurrentBeardPreset = createAction('SET_CURRENT_BEARD_PRESET')
export const setCurrentMoustachePreset = createAction('SET_CURRENT_MOUSTACHE_PRESET')

export function showInfoBox(argsObj) {
  return dispatch => {
    console.error(argsObj.msg)
    dispatch(bringInfoBox(argsObj))

    if (!argsObj.persistent) {
      setTimeout(() => dispatch(hideInfoBox()), 5000)
    }
  }
}
