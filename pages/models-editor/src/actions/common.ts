import { CHECK_DARK_MODE } from '../constants'
import { ICheckDarkMode } from '../interfaces/IController'

export const checkDarkMode = (status): ICheckDarkMode => ({
  status,
  type: CHECK_DARK_MODE
})
