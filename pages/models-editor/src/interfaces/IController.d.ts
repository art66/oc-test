export interface IType {
  type: string
}

export interface ICommon {
  isDarkModeEnabled: boolean
}

export interface ICheckDarkMode extends IType {
  status: boolean
}
