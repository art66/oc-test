import React, { Component } from 'react'
import { connect } from 'react-redux'
import { subscribeOnDarkMode, unsubscribeFromDarkMode } from '@torn/shared/utils/darkModeChecker'
import {
  equipItem,
  getInitialData,
  loadPreset,
  random,
  removePreset,
  saveMask,
  savePreset,
  saveSlot,
  selectItem,
  switchGender,
  unequipAll,
  updatePreset,
  setCurrentBeardPreset,
  setCurrentHairPreset,
  setCurrentMoustachePreset,
  randomHair
} from '../actions'
import {
  items, canUpdate, canReset, presets, presetName, availableMasks,
  getHairPresets, getCurrentHairPreset, getBeardPresets, getCurrentBeardPreset, getMoustachePresets,
  getCurrentMoustachePreset
} from '../selectors'
import { SlotMapper, ItemLibrary, Layout, Model, ModelButtons, PresetsManager, HairManager } from '../components'

import { checkDarkMode } from '../actions/common'

class App extends Component {
  componentDidMount() {
    const { getInitialData } = this.props

    getInitialData()
    this._checkDarkMode()
  }

  componentWillUnmount() {
    unsubscribeFromDarkMode()
  }

  _checkDarkMode = () => {
    const { runDarkModeChecker } = this.props

    subscribeOnDarkMode(payload => runDarkModeChecker(payload))
  }

  render() {
    const {
      availableMasks,
      canReset,
      canUpdate,
      currentItem,
      currentPreset,
      equipItem,
      equippedItems,
      gender,
      infoBox,
      items,
      loadPreset,
      presetName,
      presets,
      random,
      removePreset,
      saveMask,
      savePreset,
      saveSlot,
      selectItem,
      sortedItems,
      switchGender,
      unequipAll,
      updatePreset,
      hairPresets, currentHairPreset, beardPresets, currentBeardPreset, moustachePresets, currentMoustachePreset,
      setCurrentBeardPreset, setCurrentHairPreset, setCurrentMoustachePreset,
      randomHair
    } = this.props

    return (
      <Layout
        gender={gender}
        infoBox={infoBox}
        currentItem={currentItem}
        switchGender={switchGender}
        itemlibrary={
          <ItemLibrary
            equipItem={equipItem}
            equippedItems={equippedItems}
            items={items}
            currentItem={currentItem}
            selectItem={selectItem}
          />
        }
        presetsManager={
          <PresetsManager
            presets={presets}
            loadPreset={loadPreset}
            savePreset={savePreset}
            currentPreset={currentPreset}
          />
        }
        model={
          <Model
            gender={gender}
            sortedItems={sortedItems}
            currentHairPreset={currentHairPreset}
            currentBeardPreset={currentBeardPreset}
            currentMoustachePreset={currentMoustachePreset}
          />}
        hairManager={
          <HairManager
            loadPreset={loadPreset}
            savePreset={savePreset}
            hairPresets={hairPresets}
            currentHairPreset={currentHairPreset}
            beardPresets={beardPresets}
            currentBeardPreset={currentBeardPreset}
            moustachePresets={moustachePresets}
            currentMoustachePreset={currentMoustachePreset}
            setCurrentHairPreset={setCurrentHairPreset}
            setCurrentBeardPreset={setCurrentBeardPreset}
            setCurrentMoustachePreset={setCurrentMoustachePreset}
          />
        }
        modelbuttons={
          <ModelButtons
            canRemove={Boolean(currentPreset)}
            canReset={canReset}
            canUpdate={canUpdate}
            presetName={presetName}
            random={random}
            randomHair={randomHair}
            removePreset={removePreset}
            unequipAll={unequipAll}
            updatePreset={updatePreset}
          />
        }
        slotmapper={
          <SlotMapper
            availableMasks={availableMasks}
            currentItem={currentItem}
            saveMask={saveMask}
            saveSlot={saveSlot}
          />
        }
      />
    )
  }
}

const mapStateToProps = state => ({
  availableMasks: availableMasks(state),
  canReset: canReset(state),
  canUpdate: canUpdate(state),
  currentItem: state.app.currentItem || {},
  currentPreset: state.app.currentPreset,
  equippedItems: state.app.equippedItems,
  gender: state.app.gender,
  infoBox: state.app.infoBox,
  items: items(state),
  presetName: presetName(state),
  presets: presets(state),
  // sortedItems: null,
  hairPresets: getHairPresets(state),
  currentHairPreset: getCurrentHairPreset(state),
  beardPresets: getBeardPresets(state),
  currentBeardPreset: getCurrentBeardPreset(state),
  moustachePresets: getMoustachePresets(state),
  currentMoustachePreset: getCurrentMoustachePreset(state)
})

const mapActionsToProps = {
  equipItem,
  getInitialData,
  loadPreset,
  random,
  removePreset,
  saveMask,
  savePreset,
  saveSlot,
  selectItem,
  switchGender,
  unequipAll,
  updatePreset,
  setCurrentBeardPreset,
  setCurrentHairPreset,
  setCurrentMoustachePreset,
  randomHair,
  runDarkModeChecker: checkDarkMode
}

export default connect(mapStateToProps, mapActionsToProps)(App)
