import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import app from './app'
import common from './common'

const rootReducer = combineReducers({
  browser: createResponsiveStateReducer(
    {
      phone: 600,
      tablet: 1024
    },
    'desktop'
  ),
  app,
  common
})

export default rootReducer
