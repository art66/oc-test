import { handleActions } from 'redux-actions'
import * as _ from 'lodash'

const initialState = {
  gender: 'female',
  gotContent: false
}

export default handleActions(
  {
    'data loaded'(state, action) {
      const DB = _.get(action, 'payload.json.DB')
      const newState = {
        ...state,
        ...DB,
        gotContent: true
      }

      if (DB.currentItem && state.items) {
        const strigifiedItem = {}

        Object.keys(DB.currentItem).forEach(key => {
          strigifiedItem[key] = DB.currentItem[key] && DB.currentItem[key].toString() || DB.currentItem[key]
        })

        const index = state.items.findIndex(({ ID }) => Number(ID) === Number(DB.currentItem.ID))

        newState.items = [...state.items.slice(0, index), strigifiedItem, ...state.items.slice(index + 1)]
      }

      return newState
    },
    'bring info box'(state, action) {
      return {
        ...state,
        infoBox: {
          msg: action.payload.msg,
          color: action.payload.color
        }
      }
    },
    'hide info box'(state, action) {
      return {
        ...state,
        infoBox: null
      }
    },
    'select item'(state, action) {
      const currentItem = state.items.find(({ ID }) => ID === action.payload.ID)
      return {
        ...state,
        currentItem
      }
    },
    'switch gender'(state, action) {
      return {
        ...state,
        gender: action.payload
      }
    },
    'set preset'(state, action) {
      return {
        ...state,
        currentPreset: action.payload.presetID
      }
    },
    'SET_CURRENT_HAIR_PRESET'(state, action) {
      return {
        ...state,
        currentHairPreset: action.payload
      }
    },
    'SET_CURRENT_BEARD_PRESET'(state, action) {
      return {
        ...state,
        currentBeardPreset: action.payload
      }
    },
    'SET_CURRENT_MOUSTACHE_PRESET'(state, action) {
      return {
        ...state,
        currentMoustachePreset: action.payload
      }
    },
    'RANDOM_HAIR'(state, action) {
      const { hair, beard, moustache } = state
      const randHair = hair[Math.floor(Math.random() * hair.length)]
      const randBeard = beard[Math.floor(Math.random() * beard.length)]
      const randMoustache = moustache[Math.floor(Math.random() * moustache.length)]
      return {
        ...state,
        currentHairPreset: {
          value: hair.indexOf(randHair),
          label: randHair.title,
          path: randHair.path
        },
        currentBeardPreset: {
          value: beard.indexOf(randBeard),
          label: randBeard.title,
          path: randBeard.path
        },
        currentMoustachePreset: {
          value: moustache.indexOf(randMoustache),
          label: randMoustache.title,
          path: randMoustache.path
        }
      }
    },
    'CHECK_DARK_MODE'(state, action) {
      return ({
        ...state,
        isDarkModeEnabled: action.status
      })
    }
  },
  initialState
)
