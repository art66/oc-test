export const items = [
  {
    ID: '46',
    slotsMap: '["B1","B2"]',
    name: 'Tank Top'
  },
  {
    ID: '47',
    slotsMap: '["F3"]',
    name: 'Pair of Trainers'
  },
  {
    ID: '48',
    slotsMap: '["B4"]',
    name: 'Jacket'
  },
  {
    ID: '278',
    slotsMap: '["E3","N3","M3"]',
    name: 'Kabuki Mask'
  },
  {
    ID: '347',
    slotsMap: '["B1"]',
    name: 'Thong'
  },
  {
    ID: '404',
    slotsMap: '["N3","M3"]',
    name: 'Bandana'
  },
  {
    ID: '412',
    slotsMap: '["E3"]',
    name: 'Sports Shades'
  },
  {
    ID: '413',
    slotsMap: '["H3","H2"]',
    name: 'Mountie Hat',
    slotsMapMask: 'head-mask'
  },
  {
    ID: '414',
    slotsMap: '["E3"]',
    name: 'Proda Sunglasses'
  },
  {
    ID: '430',
    slotsMap: '["B1"]',
    name: 'Coconut Bra'
  },
  {
    ID: '562',
    slotsMap: '["B2"]',
    name: 'Sweater'
  },
  {
    ID: '598',
    slotsMap: '["H3","H4"]',
    name: "Witch's Hat"
  },
  {
    ID: '606',
    slotsMap: '["F3","F4"]',
    name: 'Santa Boots'
  },
  {
    ID: '607',
    slotsMap: '["D3"]',
    name: 'Santa Gloves'
  },
  {
    ID: '608',
    slotsMap: '["H4","H5","H3"]',
    name: 'Santa Hat'
  },
  {
    ID: '609',
    slotsMap: '["B3","B4"]',
    name: 'Santa Jacket'
  },
  {
    ID: '610',
    slotsMap: '["L3"]',
    name: 'Santa Trousers'
  },
  {
    ID: '621',
    slotsMap: '["E3","M3","N3"]',
    name: 'Snorkel'
  },
  {
    ID: '622',
    slotsMap: '["F3"]',
    name: 'Flippers'
  },
  {
    ID: '623',
    slotsMap: '["L1"]',
    name: 'Speedo'
  },
  {
    ID: '624',
    slotsMap: '["B1"]',
    name: 'Bikini'
  },
  {
    ID: '625',
    slotsMap: '["B1"]',
    name: 'Wetsuit'
  },
  {
    ID: '626',
    slotsMap: '["D3","D4"]',
    name: 'Diving Gloves'
  },
  {
    ID: '633',
    slotsMap: '["D3"]',
    name: 'Latex Gloves'
  },
  {
    ID: '635',
    slotsMap: '["B3"]',
    name: 'Straitjacket'
  },
  {
    ID: '703',
    slotsMap: '["F1"]',
    name: 'Festive Socks'
  },
  {
    ID: '704',
    slotsMap: '["B2"]',
    name: 'Respo Hoodie'
  },
  {
    ID: '726',
    slotsMap: '["H3"]',
    name: "Scrooge's Top Hat"
  },
  {
    ID: '727',
    slotsMap: '["B4"]',
    name: "Scrooge's Topcoat"
  },
  {
    ID: '728',
    slotsMap: '["L3"]',
    name: "Scrooge's Trousers"
  },
  {
    ID: '729',
    slotsMap: '["F3"]',
    name: "Scrooge's Boots"
  },
  {
    ID: '730',
    slotsMap: '["D3"]',
    name: "Scrooge's Gloves"
  },
  {
    ID: '743',
    slotsMap: '["B2"]',
    name: "Christmas Sweater '15"
  },
  {
    ID: '791',
    slotsMap: '["B1"]',
    name: 'Mediocre T-Shirt'
  },
  {
    ID: '803',
    slotsMap: '["N3","M3"]',
    name: "Duke's Gimp Mask"
  },
  {
    ID: '806',
    slotsMap: '["N3","M3"]',
    name: 'Old Lady Mask'
  },
  {
    ID: '807',
    slotsMap: '["N3","M3","H3"]',
    name: 'Exotic Gentleman Mask'
  },
  {
    ID: '808',
    slotsMap: '["N3","M3"]',
    name: 'Ginger Kid Mask'
  },
  {
    ID: '809',
    slotsMap: '["N3","M3"]',
    name: 'Young Lady Mask'
  },
  {
    ID: '810',
    slotsMap: '["N3","M3"]',
    name: 'Moustache Man Mask'
  },
  {
    ID: '811',
    slotsMap: '["N3","M3"]',
    name: 'Scarred Man Mask'
  },
  {
    ID: '812',
    slotsMap: '["N3","M3"]',
    name: 'Psycho Clown Mask'
  },
  {
    ID: '813',
    slotsMap: '["N3","M3"]',
    name: 'Nun Mask'
  },
  {
    ID: '828',
    slotsMap: '["N3","M3"]',
    name: "Donald Trump Mask '16"
  },
  {
    ID: '834',
    slotsMap: '["L3"]',
    name: 'Sweatpants'
  },
  {
    ID: '835',
    slotsMap: '["B2"]',
    name: 'String Vest'
  },
  {
    ID: '836',
    slotsMap: '["F3"]',
    name: 'Black Oxfords'
  },
  {
    ID: '841',
    slotsMap: '["H3","H4"]',
    name: 'Classic Fedora'
  },
  {
    ID: '842',
    slotsMap: '["L3"]',
    name: 'Pinstripe Suit Trousers'
  },
  {
    ID: '843',
    slotsMap: '["B4"]',
    name: 'Trenchcoat Duster'
  },
  {
    ID: '849',
    slotsMap: '["E3"]',
    name: 'Loupes'
  },
  {
    ID: '851',
    slotsMap: '["B1"]',
    name: 'Wifebeater'
  },
  {
    ID: '32',
    slotsMap: '["B3"]',
    name: 'Leather Vest'
  },
  {
    ID: '33',
    slotsMap: '["B3"]',
    name: 'Police Vest'
  },
  {
    ID: '34',
    slotsMap: '["B3"]',
    name: 'Bulletproof Vest'
  },
  {
    ID: '49',
    slotsMap: '["B3"]',
    name: 'Full Body Armor'
  },
  {
    ID: '50',
    slotsMap: '["B3"]',
    name: 'Outer Tactical Vest'
  },
  {
    ID: '101',
    slotsMap: '["B5","H5","D5","L5","F5","H4","H3"]',
    name: 'Bunny Fur'
  },
  {
    ID: '107',
    slotsMap: '["L4","B5"]',
    name: 'Kevlar Trench Coat'
  },
  {
    ID: '176',
    slotsMap: '[]',
    name: 'Chain Mail'
  },
  {
    ID: '178',
    slotsMap: '["B3"]',
    name: 'Flak Jacket'
  },
  {
    ID: '332',
    slotsMap: '["B3"]',
    name: 'Combat Vest'
  },
  {
    ID: '333',
    slotsMap: '["B3"]',
    name: 'Liquid Body Armor'
  },
  {
    ID: '334',
    slotsMap: '["B3"]',
    name: 'Flexible Body Armor'
  },
  {
    ID: '348',
    slotsMap: '["H5","B5","D5","L5","F5"]',
    name: 'Hazmat Suit'
  },
  {
    ID: '538',
    slotsMap: '["H3"]',
    name: 'Medieval Helmet'
  },
  {
    ID: '640',
    slotsMap: '["D3"]',
    name: 'Kevlar Gloves'
  },
  {
    ID: '641',
    slotsMap: '["H3"]',
    name: 'WWII Helmet'
  },
  {
    ID: '642',
    slotsMap: '["H3"]',
    name: 'Motorcycle Helmet'
  },
  {
    ID: '643',
    slotsMap: '["H3"]',
    name: 'Construction Helmet'
  },
  {
    ID: '644',
    slotsMap: '["H3","H2","H1","H4","H5"]',
    name: 'Welding Helmet'
  },
  {
    ID: '645',
    slotsMap: '["F3"]',
    name: 'Safety Boots'
  },
  {
    ID: '646',
    slotsMap: '["F3"]',
    name: 'Hiking Boots'
  },
  {
    ID: '647',
    slotsMap: '["H3"]',
    name: 'Leather Helmet'
  },
  {
    ID: '648',
    slotsMap: '["L3"]',
    name: 'Leather Pants'
  },
  {
    ID: '649',
    slotsMap: '["F3"]',
    name: 'Leather Boots'
  },
  {
    ID: '650',
    slotsMap: '["D3"]',
    name: 'Leather Gloves'
  },
  {
    ID: '651',
    slotsMap: '["H3"]',
    name: 'Combat Helmet'
  },
  {
    ID: '652',
    slotsMap: '["L3"]',
    name: 'Combat Pants'
  },
  {
    ID: '653',
    slotsMap: '["F3"]',
    name: 'Combat Boots'
  },
  {
    ID: '654',
    slotsMap: '["D3"]',
    name: 'Combat Gloves'
  },
  {
    ID: '655',
    slotsMap: '["H3"]',
    name: 'Riot Helmet'
  },
  {
    ID: '656',
    slotsMap: '["B3"]',
    name: 'Riot Body'
  },
  {
    ID: '657',
    slotsMap: '["L3"]',
    name: 'Riot Pants'
  },
  {
    ID: '658',
    slotsMap: '["F3"]',
    name: 'Riot Boots'
  },
  {
    ID: '659',
    slotsMap: '["D3"]',
    name: 'Riot Gloves'
  },
  {
    ID: '660',
    slotsMap: '["H3"]',
    name: 'Dune Helmet'
  },
  {
    ID: '661',
    slotsMap: '["B3"]',
    name: 'Dune Body'
  },
  {
    ID: '662',
    slotsMap: '["L3"]',
    name: 'Dune Pants'
  },
  {
    ID: '663',
    slotsMap: '["F3"]',
    name: 'Dune Boots'
  },
  {
    ID: '664',
    slotsMap: '["D3"]',
    name: 'Dune Gloves'
  },
  {
    ID: '665',
    slotsMap: '["H3"]',
    name: 'Assault Helmet'
  },
  {
    ID: '666',
    slotsMap: '["B3"]',
    name: 'Assault Body'
  },
  {
    ID: '667',
    slotsMap: '["L3"]',
    name: 'Assault Pants'
  },
  {
    ID: '668',
    slotsMap: '["F3"]',
    name: 'Assault Boots'
  },
  {
    ID: '669',
    slotsMap: '["D3"]',
    name: 'Assault Gloves'
  },
  {
    ID: '670',
    slotsMap: '["H3","N3","M3","E3"]',
    name: 'Delta Gas Mask'
  },
  {
    ID: '671',
    slotsMap: '["B3"]',
    name: 'Delta Body'
  },
  {
    ID: '672',
    slotsMap: '["L3"]',
    name: 'Delta Pants'
  },
  {
    ID: '673',
    slotsMap: '["F3"]',
    name: 'Delta Boots'
  },
  {
    ID: '674',
    slotsMap: '["D3"]',
    name: 'Delta Gloves'
  },
  {
    ID: '675',
    slotsMap: '["H3","E3","N3","M3"]',
    name: 'Marauder Face Mask'
  },
  {
    ID: '676',
    slotsMap: '["B3"]',
    name: 'Marauder Body'
  },
  {
    ID: '677',
    slotsMap: '["L3"]',
    name: 'Marauder Pants'
  },
  {
    ID: '678',
    slotsMap: '["F3"]',
    name: 'Marauder Boots'
  },
  {
    ID: '679',
    slotsMap: '["D3"]',
    name: 'Marauder Gloves'
  },
  {
    ID: '680',
    slotsMap: '["H4","H5","H3"]',
    name: 'EOD Helmet'
  },
  {
    ID: '681',
    slotsMap: '["B3","B4","B5"]',
    name: 'EOD Apron'
  },
  {
    ID: '682',
    slotsMap: '["L3","L4","L5"]',
    name: 'EOD Pants'
  },
  {
    ID: '683',
    slotsMap: '["F3","F4","F5"]',
    name: 'EOD Boots'
  },
  {
    ID: '684',
    slotsMap: '["D3","D4","D5"]',
    name: 'EOD Gloves'
  },
  {
    ID: '848',
    slotsMap: '["B3","B4"]',
    name: 'Kevlar Lab Coat'
  }
]
