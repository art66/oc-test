import { CHECK_DARK_MODE } from '../constants'
import { ICommon, IType, ICheckDarkMode } from '../interfaces/IController'

const ACTION_HANDLERS = {
  [CHECK_DARK_MODE]: (state: ICommon, action: ICheckDarkMode) => ({
    ...state,
    isDarkModeEnabled: action.status
  })
}

const reducer = (state = { isDarkModeEnabled: false }, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
