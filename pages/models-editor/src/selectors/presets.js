import { createSelector } from 'reselect'

const getPresets = state => state.app.presets || []

const presets = createSelector(getPresets, ps => ps.map(({ ID: value, title: label }) => ({ value, label })))

export default presets
