import { createSelector } from 'reselect'

const getEquippedItems = state => state.app.equippedItems || []

const canReset = createSelector(getEquippedItems, equippedItems => equippedItems.length !== 0)

export default canReset
