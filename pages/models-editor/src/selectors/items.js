import { createSelector } from 'reselect'

const getItems = state => state.app.items || []
const getEquippedItems = state => state.app.equippedItems || []

const items = createSelector(getItems, getEquippedItems, (items, equippedItems) =>
  items.concat().sort((a, b) => {
    return a.name.toLowerCase().localeCompare(b.name.toLowerCase())
  })
)

export default items
