import { createSelector } from 'reselect'

const getBeards = state => state.app.beard || []

const presets = createSelector(getBeards, ps => ps.map((p) => ({
  value: ps.indexOf(p), title: p.title, path: p.path, slotsMap: p.slotsMap, label: p.title, maskID: p.maskID
})))

export default presets
