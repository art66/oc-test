import { createSelector } from 'reselect'

const getMoustache = state => state.app.moustache || []

const presets = createSelector(getMoustache, ps => ps.map((p) => ({
  value: ps.indexOf(p), title: p.title, path: p.path, slotsMap: p.slotsMap, label: p.title, maskID: p.maskID
})))

export default presets
