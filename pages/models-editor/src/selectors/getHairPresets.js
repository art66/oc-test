import { createSelector } from 'reselect'

const getHair = state => state.app.hair || []

const presets = createSelector(getHair, ps => ps.map((p) => ({
  value: ps.indexOf(p), title: p.title, path: p.path, slotsMap: p.slotsMap, label: p.title, maskID: p.maskID
})))

export default presets
