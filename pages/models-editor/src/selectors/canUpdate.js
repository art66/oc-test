import { createSelector } from 'reselect'
import * as _ from 'lodash'

const getCurrentPresetID = state => state.app.currentPreset
const getAllPresets = state => state.app.presets
const getEquippedItems = state => state.app.equippedItems

// checks to see if selected preset was modified
const canUpdate = createSelector(
  getCurrentPresetID,
  getAllPresets,
  getEquippedItems,
  (currentPresetID, presets, equippedItems) => {
    if (currentPresetID) {
      const preset = _.find(presets, ({ ID }) => ID === currentPresetID).items
      const same = _.isEqual(preset, equippedItems)
      return !same
    }
    return false
  }
)

export default canUpdate
