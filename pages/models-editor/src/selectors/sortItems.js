// import { createSelector } from 'reselect'
// import isEmpty from '@torn/shared/utils/isEmpty'

// import { SLOTS_ORDER } from '../constants'
// import getCurrentHairPreset from './getCurrentHairPreset'
// import getCurrentBeardPreset from './getCurrentBeardPreset'
// import getCurrentMoustachePreset from './getCurrentMoustachePreset'

// // const compareSlots = (slotA, slotB) => {
// //   const aLetterIndex = SLOTS_ORDER[slotA.charAt(0)]
// //   const bLetterIndex = SLOTS_ORDER[slotB.charAt(0)]

// //   const aIndex = Number(slotA.slice(1))
// //   const bIndex = Number(slotB.slice(1))

// //   return bLetterIndex - aLetterIndex || aIndex - bIndex
// // }

// // const getTopmostSlot = slotsMap => {
// //   if (Array.isArray(slotsMap) && slotsMap.length) {
// //     return slotsMap.sort(compareSlots)[0]
// //   } else {
// //     return ''
// //   }
// // }

// const compareSlots = (slotA, slotB) => {
//   const aLetterIndex = SLOTS_ORDER[slotA.charAt(0)]
//   const bLetterIndex = SLOTS_ORDER[slotB.charAt(0)]

//   const aIndex = Number(slotA.slice(1))
//   const bIndex = Number(slotB.slice(1))

//   return aIndex - bIndex || bLetterIndex - aLetterIndex
// }

// const getTopmostSlot = slotsMap => {
//   if (Array.isArray(slotsMap) && slotsMap.length) {
//     return slotsMap.sort(compareSlots)[slotsMap.length - 1]
//   }

//   return ''
// }

// const getItems = state => state.app.items
// const getEquippedItems = state => state.app.equippedItems

// // exporting for testing purpose
// //
// // items have a property `slotsMap` that looks like this
// // '["B2"]'
// // '["H3","H4"]'
// // '["H5","B5","D5","L5","F5"]'
// // where letter is a body part, like: N = nose, M = mouth
// // and an index is equivalent to z-index
// // the index has higher priority
// export const sort = (items, equippedItems, currentHairPreset, currentBeardPreset, currentMoustachePreset) => {
//   // if (!equippedItems) return []

//   console.log(currentHairPreset, currentBeardPreset, currentMoustachePreset, 'currentHairPreset, currentBeardPreset, currentMoustachePreset')

//   const arr = []

//   if (currentHairPreset && currentHairPreset.path) {
//     arr.push(currentHairPreset)
//   }

//   if (currentBeardPreset && currentBeardPreset.path) {
//     arr.push(currentBeardPreset)
//   }

//   if (currentMoustachePreset && currentMoustachePreset.path) {
//     arr.push(currentMoustachePreset)
//   }

//   let allItems = null

//   if (equippedItems) {
//     allItems = !isEmpty(arr) ? equippedItems.concat(arr) : equippedItems
//   } else {
//     allItems = arr
//   }

//   const extractItemFields = item => {
//     const tempItem = item instanceof Object ? item : items.find(it => Number(it.ID) === Number(item) || it.ID === item)

//     if (!tempItem) {
//       console.error('Some error happen during props processing!')

//       return {}
//     }

//     return {
//       ID: tempItem.ID,
//       maskID: tempItem.slotsMapMask,
//       hairMaskID: tempItem.slotsMapMaskHair,
//       slotsMap: JSON.parse(tempItem.slotsMap || '[]'),
//       slotsMapBackground: tempItem.slotsMapBackground,
//       changedSlotsMap: JSON.parse(tempItem.changedSlotsMap || '[]'),
//       path: tempItem.path
//     }
//   }

//   // Additional sorting for "special" items
//   const preSortedItems = allItems
//     .map(extractItemFields)
//     .sort((a, b) => a.changedSlotsMap?.length - b.changedSlotsMap?.length)
//     .sort((a, b) => a.slotsMap?.length - b.slotsMap?.length)

//   return preSortedItems /* .map(extractItemFields(items)) */
//     .sort((a, b) => {
//       // const aTopmost = getTopmostSlot(a.slotsMap)
//       // const bTopmost = getTopmostSlot(b.slotsMap)

//       let aTopmost

//       let bTopmost

//       let result

//       if (a.changedSlotsMap?.length) {
//         aTopmost = getTopmostSlot(a.changedSlotsMap)
//       } else {
//         aTopmost = getTopmostSlot(a.slotsMap)
//       }

//       if (b.changedSlotsMap?.length) {
//         bTopmost = getTopmostSlot(b.changedSlotsMap)
//       } else {
//         bTopmost = getTopmostSlot(b.slotsMap)
//       }

//       try {
//         result = compareSlots(aTopmost, bTopmost)
//       } catch (e) {
//         console.log('problem with items', a, 'or', b, 'slotMaps:', aTopmost, bTopmost)
//       }

//       return result || 0
//     })
// }

// const sortedItems = createSelector(
//   getItems,
//   getEquippedItems,
//   getCurrentHairPreset,
//   getCurrentBeardPreset,
//   getCurrentMoustachePreset,
//   sort
// )

// export default sortedItems
