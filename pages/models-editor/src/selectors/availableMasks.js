import { createSelector } from 'reselect'
import { trimMaskName } from '../utils'

const getAvailableMasks = state => state.app.masks || []

const buildObject = item => ({ label: item, value: item })

const unique = (value, index, self) => self.indexOf(value) === index

const availableMasks = createSelector(getAvailableMasks, masks =>
  masks
    .map(trimMaskName)
    .filter(unique)
    .map(buildObject)
)

export default availableMasks
