import { createSelector } from 'reselect'
import presets from './presets'

const getCurrentPreset = state => state.app.currentPreset

const presetName = createSelector(presets, getCurrentPreset, (presets, currentPreset) => {
  if (currentPreset) {
    return presets.find(({ value }) => currentPreset === value).label
  }
})

export default presetName
