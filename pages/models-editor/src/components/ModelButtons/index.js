import PropTypes from 'prop-types'
import React, { Component } from 'react'
import s from './styles.cssmodule.scss'

class ModelButtons extends Component {
  render() {
    const { canReset, canUpdate, presetName, random, removePreset, unequipAll, updatePreset, canRemove, randomHair } = this.props
    return (
      <div className={s.buttonWrap}>
        {canReset && <button onClick={unequipAll}>Reset</button>}
        {canUpdate && <button onClick={updatePreset}>Save</button>}
        {<button onClick={random}>Random</button>}
        {<button onClick={randomHair}>Random Hair</button>}
        {canRemove && <button onClick={removePreset}>&#10008;</button>}
      </div>
    )
  }
}

ModelButtons.propTypes = {
  canRemove: PropTypes.bool,
  canReset: PropTypes.bool,
  canUpdate: PropTypes.bool,
  presetName: PropTypes.string,
  random: PropTypes.func,
  removePreset: PropTypes.func,
  unequipAll: PropTypes.func,
  updatePreset: PropTypes.func
}

export default ModelButtons
