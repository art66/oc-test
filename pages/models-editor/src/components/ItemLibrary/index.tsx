import React, { Component } from 'react'
import cx from 'classnames'

import s from './styles.cssmodule.scss'

interface IProps {
  equipItem: ({ IDs }: { IDs: string[] }) => void
  equippedItems: string[]
  items: {
    ID: string
    name: string
    info: string
    slotsMap: string
  }[]
  currentItem: {
    ID: string
  }
  selectItem: ({ ID }: { ID: number }) => void
}

class ItemLibrary extends Component<IProps> {
  _handleSelect = event => {
    const { dataset } = event.target

    const { selectItem } = this.props

    selectItem({ ID: dataset.id })
  }

  _handleEquip = event => {
    event.stopPropagation()

    const { dataset } = event.target

    const { equipItem } = this.props

    equipItem({ IDs: [dataset.id] })
  }

  _renderItems = () => {
    const { items, currentItem, equippedItems } = this.props
    const currentItemID = currentItem && currentItem.ID

    if (!items) {
      return <div>loading...</div>
    }

    return items.map(item => {
      const equipped = equippedItems && equippedItems.indexOf(item.ID) !== -1

      let gotSlotsMap = null

      try {
        gotSlotsMap = Array.isArray(JSON.parse(item.slotsMap))
      } catch (e) {
        console.log('Problem with item ', item.ID)
      }

      const c = cx({
        [s.item]: true,
        [s.equipped]: equipped,
        [s.red]: !gotSlotsMap,
        [s.selected]: currentItemID === item.ID
      })

      const haveSlots = (
        <span
          role='button'
          tabIndex={0}
          className={s.toggleEquip}
          data-id={item.ID}
          onClick={this._handleEquip}
          onKeyDown={undefined}
        >
          {equipped ? '\u2013' : '+'}
        </span>
      )

      const noSlots = (
        <span className={s.small}>no slots map</span>
      )

      return (
        <div
          key={item.ID}
          role='button'
          tabIndex={0}
          title={item.info}
          className={c}
          data-id={item.ID}
          onClick={this._handleSelect}
          onKeyDown={undefined}
        >
          <span>{item.name}</span>
          {gotSlotsMap ? haveSlots : noSlots}
        </div>
      )
    })
  }

  render() {
    return (
      <div className={s.itemLibrary}>{this._renderItems()}</div>
    )
  }
}

export default ItemLibrary
