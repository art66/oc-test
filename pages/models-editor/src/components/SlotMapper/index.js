import PropTypes from 'prop-types'
import React, { Component } from 'react'
import cx from 'classnames'
import Select from 'react-select'
import { SLOTS } from '../../constants'
import { trimMaskName } from '../../utils'
import s from './styles.cssmodule.scss'

class SlotMapper extends Component {
  onChange = item => {
    const { saveMask } = this.props
    const mask = trimMaskName(item ? item.label : '')
    saveMask(mask)
  }
  render() {
    const { currentItem, saveSlot, availableMasks, currentMask } = this.props
    const slotsMap = currentItem && currentItem.slotsMap
    return (
      <div className={s.itemLibrary}>
        <div className={s.itemsWrap}>
          <div className={s.item + ' ' + s.big}>
            {currentItem.ID && <img src={'images/items/' + currentItem.ID + '/large.png'} />}
          </div>
          <div className={s.item + ' ' + s.medium}>
            {currentItem.ID && <img src={'images/items/' + currentItem.ID + '/medium.png'} />}
          </div>
          <div className={s.item + ' ' + s.small}>
            {currentItem.ID && <img src={'images/items/' + currentItem.ID + '/small.png'} />}
          </div>
        </div>
        <Select
          options={availableMasks}
          clearable
          searchable
          onBlurResetsInput={false}
          disabled={!currentItem.ID}
          placeholder={currentItem.ID ? 'Select a mask' : 'Select an item first'}
          className={s.maskSelector}
          onChange={this.onChange}
          value={currentItem.slotsMapMaskHair || currentItem.slotsMapMask}
        />
        <div className={s.slots}>
          {SLOTS.map((row, i) => (
            <div key={i} className={s.slotsRow}>
              <div className={s.slotsLabel}>{row.label}</div>
              <div className={s.slotsWrap}>
                {row.slots.map((active, j) => {
                  const name = row.prefix + (j + 1)
                  const c = cx({
                    [s.slot]: true,
                    [s.enabled]: currentItem && active,
                    [s.active]: slotsMap && slotsMap.indexOf(name) !== -1
                  })
                  return (
                    <div key={j} className={c} onClick={() => currentItem && active && saveSlot(name)}>
                      {name}
                    </div>
                  )
                })}
              </div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}

SlotMapper.propTypes = {
  availableMasks: PropTypes.array,
  currentItem: PropTypes.object,
  saveSlot: PropTypes.func
}

export default SlotMapper
