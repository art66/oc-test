import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Select from 'react-select'
import 'react-select/dist/react-select.css'

class HairManager extends Component {
  static propTypes = {
    hairPresets: PropTypes.array,
    currentHairPreset: PropTypes.object,
    beardPresets: PropTypes.array,
    currentBeardPreset: PropTypes.object,
    moustachePresets: PropTypes.array,
    currentMoustachePreset: PropTypes.object,
    setCurrentHairPreset: PropTypes.func,
    setCurrentBeardPreset: PropTypes.func,
    setCurrentMoustachePreset: PropTypes.func,
    savePreset: PropTypes.func
  }

  static defaultProps = {
    hairPresets: [],
    currentHairPreset: null,
    beardPresets: [],
    currentBeardPreset: null,
    moustachePresets: [],
    currentMoustachePreset: null,
    setCurrentHairPreset: () => {},
    setCurrentBeardPreset: () => {},
    setCurrentMoustachePreset: () => {},
    savePreset: () => {}
  }

  _handleOnHairChange = item => {
    const { setCurrentHairPreset } = this.props

    setCurrentHairPreset(item)
  }

  _handleOnBeardChange = item => {
    const { setCurrentBeardPreset } = this.props

    setCurrentBeardPreset(item)
  }

  _handleOnMoustacheChange = item => {
    const { setCurrentMoustachePreset } = this.props

    setCurrentMoustachePreset(item)
  }

  _handleSave = event => {
    // const { savePreset } = this.props

    // savePreset(event.target.value)

    // console.log(event.target, 'event.target.value')
    if (event.keyCode === 13) {
      // const presetName = event.target.value
      // savePreset(presetName)
    }
  }

  render() {
    const {
      hairPresets,
      currentHairPreset,
      beardPresets,
      currentBeardPreset,
      moustachePresets,
      currentMoustachePreset
    } = this.props

    // console.log(hairPresets, currentHairPreset, 'HAIR')

    return (
      <div>
        <div>
          <Select
            clearable
            searchable
            onInputKeyDown={this._handleSave}
            onBlurResetsInput={false}
            noResultsText='Hit enter to save this preset'
            placeholder='Select hair'
            options={hairPresets}
            value={currentHairPreset}
            onChange={this._handleOnHairChange}
          />
        </div>
        <div>
          <Select
            clearable
            searchable
            onInputChange={this._handleSave}
            onBlurResetsInput={false}
            noResultsText='Hit enter to save this preset'
            placeholder='Select bread'
            options={beardPresets}
            value={currentBeardPreset}
            onChange={this._handleOnBeardChange}
          />
        </div>
        <div>
          <Select
            clearable
            searchable
            onInputChange={this._handleSave}
            onBlurResetsInput={false}
            noResultsText='Hit enter to save this preset'
            placeholder='Select moustaches'
            options={moustachePresets}
            value={currentMoustachePreset}
            onChange={this._handleOnMoustacheChange}
          />
        </div>
      </div>
    )
  }
}

export default HairManager
