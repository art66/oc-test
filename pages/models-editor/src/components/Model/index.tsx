import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import UserModel from '@torn/shared/components/UserModel'

import { IProps } from './interfaces'

class Model extends PureComponent<IProps> {
  render() {
    const { items, equippedItems, gender, presets, isDarkMode } = this.props

    return (
      <UserModel
        isDarkMode={isDarkMode}
        allItems={items}
        equippedIDs={equippedItems}
        gender={gender}
        presets={presets}
        withContainer={true}
        isCentered={true}
      />
    )
  }
}

const mapStateToProps = state => ({
  isDarkMode: state.common.isDarkModeEnabled,
  items: state.app.items,
  presets: {
    beardPreset: state.app.currentBeardPreset,
    hairPreset: state.app.currentHairPreset,
    moustachePreset: state.app.currentMoustachePreset
  },
  equippedItems: state.app.equippedItems
})

export default connect(mapStateToProps, null)(Model)
