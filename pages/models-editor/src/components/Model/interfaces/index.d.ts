import { IItem, IPresets } from '@torn/shared/components/UserModel/interfaces'
import { IGender } from '../../../../../attack/src/components/ModelLayers/interfaces'

export interface IProps extends IGender, IPresets {
  currentBeardPreset: object
  currentHairPreset: object
  currentMoustachePreset: object
  items: IItem[]
  equippedItems: number[] | string[]
  isDarkMode: boolean
}

export interface IDefaultProps {
  gender: 'male'
}
