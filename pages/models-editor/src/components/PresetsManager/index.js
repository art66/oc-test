import React, { Component } from 'react'
import Select from 'react-select'
import 'react-select/dist/react-select.css'

import './index.cssmodule.scss'

class PresetsManager extends Component {
  onChange = item => {
    const { loadPreset } = this.props
    const { value } = item || {}
    loadPreset(value)
  }
  save = event => {
    const { savePreset } = this.props
    if (event.keyCode === 13) {
      const presetName = event.target.value
      savePreset(presetName)
    }
  }
  render() {
    const { presets, currentPreset } = this.props
    return (
      <div>
        <Select
          clearable
          searchable
          onInputKeyDown={this.save}
          onBlurResetsInput={false}
          noResultsText={'Hit enter to save this preset'}
          placeholder="Select preset"
          options={presets}
          value={currentPreset}
          onChange={this.onChange}
        />
      </div>
    )
  }
}

export default PresetsManager
