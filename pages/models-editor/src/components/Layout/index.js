import PropTypes from 'prop-types'
import React, { Component } from 'react'
import InfoBox from '@torn/shared/components/InfoBox'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

class Layout extends Component {
  render() {
    const {
      currentItem,
      gender,
      infoBox,
      itemlibrary,
      model,
      modelbuttons,
      presetsManager,
      hairManager,
      slotmapper,
      switchGender
    } = this.props
    return (
      <div>
        {infoBox ? <InfoBox msg={infoBox.msg} color={infoBox.color} /> : null}

        <div className={s.wrap}>
          <div className={s.itemLibraryWrap}>
            <h3 className={s.header}>Item Library</h3>
            <div className={s.contentWrap}>
              <div className={s.presetsManager}>{presetsManager}</div>
              <div className={s.itemlibrary}>{itemlibrary}</div>
              <div>{hairManager}</div>
            </div>
          </div>
          <div className={s.modelEditorWindow}>
            <div className={s.tabs}>
              <h3
                className={s.tab + ' ' + (gender === 'female' ? s.active : '')}
                onClick={() => switchGender('female')}
              >
                Female
              </h3>
              <h3
                className={s.tab + ' ' + (gender === 'male' ? s.active : '')}
                onClick={() => switchGender('male')}
              >
                Male
              </h3>
            </div>
            <div className={cn(s.contentWrap, s.model, s.playerWindow)}>
              {model}
              {modelbuttons}
            </div>
          </div>
          <div className={s.slotsMapper}>
            <h3 className={s.header}>{`${currentItem.name} - ${currentItem.ID}`}</h3>
            <div className={s.contentWrap}>{slotmapper}</div>
          </div>
        </div>
      </div>
    )
  }
}

Layout.propTypes = {
  slotmapper: PropTypes.node,
  gender: PropTypes.string,
  infoBox: PropTypes.object,
  itemlibrary: PropTypes.node,
  model: PropTypes.node,
  modelbuttons: PropTypes.node,
  presetsManager: PropTypes.node,
  switchGender: PropTypes.func
}

export default Layout
