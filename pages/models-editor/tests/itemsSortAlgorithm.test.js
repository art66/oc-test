import { sort } from '../src/selectors/sortItems'
import { items } from '../src/reducers/_initialState'

const grabIDs = arr => arr.map(({ ID }) => ID)

describe('sort algorithm', () => {
  test('Nun Mask -> Sports Shades', () => {
    // 813 -> Nun Mask
    // 412 -> Sports Shades

    expect(grabIDs(sort(items, [412, 813]))).toEqual([813, 412])
    expect(grabIDs(sort(items, [813, 412]))).toEqual([813, 412])
  })
  test('Flexible Body Armor -> Hazmat Suit', () => {
    // 334 -> Flexible Body
    // 348 -> Hazmat Suit

    expect(grabIDs(sort(items, [334, 348]))).toEqual([334, 348])
    expect(grabIDs(sort(items, [348, 334]))).toEqual([334, 348])
  })
  test('Bunny Fur -> Ginger Kid Mask -> Sports Shades', () => {
    // 808 -> Ginger Kid Mask
    // 412 -> Sports Shades
    // 101 -> Bunny Fur

    expect(grabIDs(sort(items, [101, 808, 412]))).toEqual([808, 412, 101])
    expect(grabIDs(sort(items, [808, 101, 412]))).toEqual([808, 412, 101])
    expect(grabIDs(sort(items, [412, 808, 101]))).toEqual([808, 412, 101])
  })
  test('Scarred Man Mask -> Santa Hat', () => {
    // 811 -> Scarred Man Mask
    // 608 -> Santa Hat

    expect(grabIDs(sort(items, [811, 608]))).toEqual([811, 608])
    expect(grabIDs(sort(items, [608, 811]))).toEqual([811, 608])
  })
  test('Assault Pants -> Kevlar Lab Coat', () => {
    // 667 -> Assault Pants
    // 848 -> Kevlar Lab Coat

    expect(grabIDs(sort(items, [667, 848]))).toEqual([667, 848])
    expect(grabIDs(sort(items, [848, 667]))).toEqual([667, 848])
  })
  test('Combat Boots -> Marauder Pants', () => {
    // 653 -> Combat Boots
    // 677 -> Marauder Pants

    expect(grabIDs(sort(items, [653, 677]))).toEqual([653, 677])
    expect(grabIDs(sort(items, [677, 653]))).toEqual([653, 677])
  })
})
