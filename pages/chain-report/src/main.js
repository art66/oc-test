import React from 'react'
import { render } from 'react-dom'
import configureStore from './store/configureStore'
import AppContainer from './containers/AppContainer'

const store = configureStore()

if (__DEV__) {
  import('@torn/header')
}

render(<AppContainer store={store} />, document.getElementById('react-root'))
