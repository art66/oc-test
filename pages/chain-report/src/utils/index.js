import { formatTimeNumber } from '@torn/shared/utils/index'

const { numberFormat, $, Raphael, updateChart, initialiseChart, queryStringToObj } = window

const globalNumberFormat = (num, dec = 0) => {
  const n = Math.round(num * 10 ** dec) / 10 ** dec
  const res = numberFormat(n)

  const arr = res.split('.')

  if (arr.length === 1) {
    if (dec === 0) {
      return res
    }
    arr.push('')
  }
  const l = arr[1].length

  for (let i = l; i < dec; i++) {
    arr[1] += '0'
  }
  return arr.join('.')
}

const sortData = (data, sortField, sortDirection) => {
  return data.sort((a, b) => {
    if (a[sortField] < b[sortField]) {
      return sortDirection === 'asc' ? -1 : 1
    }

    if (a[sortField] > b[sortField]) {
      return sortDirection === 'asc' ? 1 : -1
    }

    return 0
  })
}

const isManualDesktopMode = () => document.body.classList.contains('d') && !document.body.classList.contains('r')

const getDate = tm => {
  const date = tm > 0 ? new Date(tm * 1000) : new Date()
  const year = date.getUTCFullYear().toString().substr(2)
  const month = formatTimeNumber(date.getUTCMonth() + 1)
  const day = formatTimeNumber(date.getUTCDate())
  const hour = formatTimeNumber(date.getUTCHours())
  const minutes = formatTimeNumber(date.getUTCMinutes())
  const seconds = formatTimeNumber(date.getUTCSeconds())

  return `${hour}:${minutes}:${seconds} - ${day}/${month}/${year}`
}

const getTicks = () => {
  const step = 6
  const duration = 72
  const ticks = []

  for (let i = 0; i <= duration; i += step) {
    ticks.push(i)
  }

  return ticks
}

const getGraphSettings = (isDarkMode = false, territoryGraph = false) => {
  const gridTick = isDarkMode ? '#000' : '#ccc'
  const bgColor = isDarkMode ? 'rgba(0, 0, 0, 0.5)' : 'rgba(223, 223, 223, 0.25)'

  return {
    xaxis: {
      tickColor: gridTick,
      axisLabel: 'Hours',
      ticks: territoryGraph ? getTicks() : undefined
    },
    yaxis: {
      tickColor: gridTick,
      tickFormatter: num => globalNumberFormat(num)
    },
    grid: {
      hoverable: true,
      clickable: true,
      color: gridTick,
      backgroundColor: bgColor,
      show: true,
      aboveData: false,
      margin: 10,
      borderWidth: 2,
      borderColor: gridTick,
      minBorderMargin: 10
    }
  }
}

export {
  globalNumberFormat,
  sortData,
  $,
  Raphael,
  updateChart,
  initialiseChart,
  queryStringToObj,
  isManualDesktopMode,
  getDate,
  getGraphSettings
}
