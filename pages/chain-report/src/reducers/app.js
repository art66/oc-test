import { handleActions } from 'redux-actions'
import { sortData } from '../utils'

const initialState = {
  dataLoaded: false,
  reportData: {},
  sortField: 'attacks',
  sortDirection: 'desc'
}

export default handleActions(
  {
    'data loaded': (state, action) => ({
      ...state,
      dataLoaded: true,
      reportData: {
        ...action.payload.json
      }
    }),
    'sort changed': (state, action) => {
      const { field } = action.payload

      let newSortField = field

      let newSortDirection = ''

      if (field !== state.sortField) {
        newSortDirection = 'desc'
      } else if (state.sortDirection === 'desc') {
        newSortDirection = 'asc'
      } else {
        newSortField = 'key'
        newSortDirection = 'desc'
      }

      return {
        ...state,
        dataLoaded: true,
        reportData: {
          ...state.reportData,
          members: sortData(state.reportData.members, newSortField, newSortDirection)
        },
        sortField: newSortField,
        sortDirection: newSortDirection
      }
    }
  },
  initialState
)
