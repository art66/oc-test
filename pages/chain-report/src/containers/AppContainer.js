import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import ReportWrapper from '../components/ReportWrapper'

class AppContainer extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <div style={{ height: '100%' }}>
          <ReportWrapper />
        </div>
      </Provider>
    )
  }
}

export default AppContainer
