import { createAction } from 'redux-actions'
import { fetchUrl } from '@torn/shared/utils'
import { BASE_URL } from '../constants'

export const dataLoaded = createAction('data loaded', json => ({ json }))

export const getChainReportData = ID => {
  return dispatch => {
    return fetchUrl(`${BASE_URL}?step=getChainReport&chainID=${ID}`).then(json => {
      dispatch(dataLoaded(json))
    })
  }
}

export const getRankReportData = ID => {
  return dispatch => {
    return fetchUrl(`/page.php?sid=factionsRankedWarringPublic&step=report&warId=${ID}`).then(json => {
      dispatch(dataLoaded(json))
    })
  }
}

export const getRaidReportData = ID => {
  return dispatch => {
    return fetchUrl(`${BASE_URL}?step=getRaidReport&raidID=${ID}`).then(json => {
      dispatch(dataLoaded(json))
    })
  }
}

export const getTerritoryReportData = ID => {
  return dispatch => {
    return fetchUrl(`${BASE_URL}?step=getTerritoryReport&warID=${ID}`).then(json => {
      dispatch(dataLoaded(json))
    })
  }
}

export const sortChanged = createAction('sort changed', field => ({ field }))

export const changeSort = field => {
  return dispatch => {
    dispatch(sortChanged(field))
  }
}

export const msgClosed = createAction('msg closed')

export const closeMsg = () => {
  return dispatch => {
    dispatch(msgClosed())
  }
}
