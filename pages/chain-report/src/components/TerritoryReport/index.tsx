import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import InfoBox from '@torn/shared/components/InfoBox'
import IFaction from '@torn/shared/components/WarMemberList/interfaces/IFaction'
import IMemberTerritory from './interfaces/IMemberTerritory'
import ITerritoryWar from './interfaces/ITerritoryWar'
import MemberList from './MemberList'
import Loading from '../Loading'
import { getTerritoryReportData } from '../../actions'
import { initialiseChart, isManualDesktopMode, getDate, getGraphSettings } from '../../utils'
import s from './index.cssmodule.scss'

interface IProps {
  getTerritoryReportData: (chainID: number) => void
  warID: number
  dataLoaded: boolean
  isDarkMode: boolean
  mediaType: string
  reportData: {
    msg?: string
    success: boolean
    members: IMemberTerritory[]
    war: ITerritoryWar
    faction1: IFaction
    faction2: IFaction
    showImages: boolean
    userID: number
    finishWarMessage: string
    chartData: Array<{
      showTooltip: number
      data: Array<[number, number]>
    }>
  }
}

class TerritoryReport extends React.Component<IProps> {
  private graph: React.RefObject<HTMLDivElement>

  constructor(props) {
    super(props)
    this.graph = React.createRef()
  }

  componentDidMount() {
    this.props.getTerritoryReportData(this.props.warID)
  }

  componentDidUpdate() {
    const { dataLoaded, reportData, isDarkMode } = this.props

    if (dataLoaded && reportData.success === true) {
      initialiseChart(this.graph.current, reportData.chartData, 'territory', getGraphSettings(isDarkMode, true))
    }
  }

  render() {
    const { dataLoaded, reportData, warID } = this.props

    if (!dataLoaded) {
      return <Loading />
    }

    const { success, msg } = reportData

    if (success === false) {
      return <InfoBox msg={msg} color='red' />
    }

    const { finishWarMessage, members, showImages, war, faction1, faction2, userID } = reportData
    const winner = war.winnerFactionID === faction1.id ? faction1 : faction2
    const loser = war.winnerFactionID === faction1.id ? faction2 : faction1

    return (
      <div className={cn('chain', 'chain-report-wrap', s.warReportWp)}>
        <div className='title-black m-top10 top-round'>War #{warID}</div>
        <div className={cn('cont-gray', s.contGray)}>
          <div className='report-title'>
            <span className={s.titleMsg} dangerouslySetInnerHTML={{ __html: finishWarMessage }} />
            <span className={cn('report-title-info', 'date', 'm-top10')}>
              {getDate(war.startDate)} until {getDate(war.endDate)}
            </span>
          </div>

          <div className='report-stats-chart'>
            <div className={cn('chart-placeholder', s.graph)} ref={this.graph} />
          </div>
        </div>
        <div className='f-war-list war-new'>
          <div className='desc-wrap'>
            <MemberList
              members={members}
              currentFaction={winner}
              opponentFaction={loser}
              showImages={showImages}
              userID={userID}
            />
          </div>
          <div className='clear' />
        </div>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    dataLoaded: state.app.dataLoaded,
    reportData: state.app.reportData,
    mediaType: isManualDesktopMode() ? 'desktop' : state.browser.mediaType
  }),
  {
    getTerritoryReportData
  }
)(TerritoryReport)
