import IMember from '@torn/shared/components/WarMemberList/interfaces/IMember'

interface IMemberTerritory extends IMember {
  points: number
  joins: number
  clears: number
}

export default IMemberTerritory
