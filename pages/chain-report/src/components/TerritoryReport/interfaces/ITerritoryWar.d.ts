export default interface ITerritoryWar {
  startDate: number
  endDate: number
  winnerFactionID: number
}
