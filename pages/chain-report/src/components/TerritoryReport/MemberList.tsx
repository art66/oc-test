import React from 'react'
import cn from 'classnames'
import UserInfo from '@torn/shared/components/UserInfo'
import IFaction from '@torn/shared/components/WarMemberList/interfaces/IFaction'
import IMemberTerritory from './interfaces/IMemberTerritory'
import { globalNumberFormat as gnf } from '../../utils'
import s from './index.cssmodule.scss'

const ASC = 'ask'
const DESC = 'desc'

const renderMember = member => {
  const config = {
    showImages: member.honor !== '',
    useProgressiveImage: false,
    status: {
      mode: member.onlineStatus.status.toLowerCase()
    },
    user: {
      ID: member.userID,
      name: member.playername,
      imageUrl: member.honor,
      isSmall: true
    },
    faction: {
      ID: member.factionId,
      name: member.factiontag,
      rank: member.factionRank,
      imageUrl: member.factionTagImageUrl
    },
    customStyles: {
      status: s.customStatus
    }
  }

  return (
    <div key={member.userID} className={cn('member', 'left')}>
      <UserInfo {...config} />
    </div>
  )
}

const getFieldValue = (member, field) => (field === 'playername' ? member[field].toLowerCase() : member[field])

interface IProps {
  members: IMemberTerritory[]
  userID: number
  currentFaction: IFaction
  opponentFaction: IFaction
  showImages: boolean
}

interface IState {
  opponentActive: boolean
  sorting: {
    field: string
    direction: string
  }
}

class MemberList extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      opponentActive: true,
      sorting: {
        field: 'points',
        direction: DESC
      }
    }
  }

  getScore(factionID) {
    const { members } = this.props
    const sum = members
      .filter(member => member.warFactionId === factionID)
      .reduce((currentSum, member) => currentSum + member.points, 0)

    return Math.floor(sum)
  }

  handleNameTabClick(isYour) {
    this.setState(prevState => ({
      opponentActive: !(isYour && prevState.opponentActive)
    }))
  }

  changeSort(sortField) {
    this.setState(prevState => ({
      sorting: {
        field: sortField,
        direction: prevState.sorting.direction === DESC && prevState.sorting.field === sortField ? ASC : DESC
      }
    }))
  }

  _renderFactionName(faction) {
    const { currentFaction } = this.props
    const { opponentActive } = this.state
    const isYour = faction.id === currentFaction.id

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div
        className={cn('name', s.name, {
          [s.current]: isYour,
          [s.opponent]: !isYour,
          [s.active]: isYour === !opponentActive,
          your: isYour,
          left: !isYour,
          enemy: !isYour,
          right: isYour
        })}
        onClick={() => this.handleNameTabClick(isYour)}
      >
        <div className={s.text} dangerouslySetInnerHTML={{ __html: faction.name }} />
        <div className={cn('score', s.score)}>{gnf(this.getScore(faction.id))}</div>
      </div>
    )
  }

  _renderMembers(factionID) {
    const { members, userID } = this.props
    const {
      sorting: { direction, field }
    } = this.state

    let totalPoints = 0
    let totalJoins = 0
    let totalClears = 0

    /* eslint-disable no-nested-ternary */
    return (
      <ul className={cn('members-list', s.membersCont, s.report)}>
        {[...members]
          .sort((a, b) => {
            const valueA = getFieldValue(a, field)
            const valueB = getFieldValue(b, field)

            if (valueA === valueB) {
              if (a.joins === b.joins) {
                return direction === ASC ? a.clears - b.clears : b.clears - a.clears
              }
              return direction === ASC ? a.joins - b.joins : b.joins - a.joins
            }
            if (direction === ASC) {
              return valueA < valueB ? -1 : 1
            }
            if (direction === DESC) {
              return valueA > valueB ? -1 : 1
            }
            return 0
          })
          .filter(member => member.warFactionId === factionID)
          .map(member => {
            totalPoints += member.points
            totalJoins += member.joins
            totalClears += member.clears
            return (
              <li
                key={member.userID}
                className={cn({
                  'bg-gray-c': !(member.points || member.joins || member.clears),
                  'bg-blue': member.userID === userID
                })}
              >
                {renderMember(member)}
                <div className='lvl left'> {member.level} </div>
                <div className='points left'> {gnf(member.points)} </div>
                <div className='joins left'> {gnf(member.joins)} </div>
                <div className='knock-off left'> {gnf(member.clears)}</div>
                <div className='clear' />
              </li>
            )
          })}

        <li key='total'>
          <div className={cn('member', 'icons', 'left', s.total)}>
            <span>Total</span>
          </div>
          <div className={cn('lvl', 'left')}>--</div>
          <div className={cn('points', 'left')}>{gnf(totalPoints)}</div>
          <div className={cn('joins', 'left', s.prevColumn, s.status)}>{gnf(totalJoins)}</div>
          <div className={cn('knock-off', 'left', s.prevColumn, s.status)}>{gnf(totalClears)}</div>
        </li>
      </ul>
    )
  }

  _renderMemberCont(faction) {
    const { currentFaction } = this.props
    const {
      opponentActive,
      sorting: { field, direction }
    } = this.state
    const isYour = currentFaction.id === faction.id

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div
        className={cn('tab-menu-cont', 'cont-gray', 'bottom-round', 'profile-mode', s.tabMenuCont, {
          'your-faction': isYour,
          'enemy-faction': !isYour,
          left: !isYour,
          right: isYour,
          [s.active]: isYour === !opponentActive
        })}
      >
        <div className={cn('members-cont', s.membersCont, s.profileMode)}>
          <div className='white-grad c-pointer'>
            <div className={cn('members', 'left', s.tab)} onClick={() => this.changeSort('playername')}>
              <span>Members</span>
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === 'playername' && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div className={cn('lvl', 'left', s.tab)} onClick={() => this.changeSort('level')}>
              Level
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === 'level' && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div className={cn('points', 'left', s.tab)} onClick={() => this.changeSort('points')}>
              Points
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === 'points' && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div className={cn('joins', 'left', s.tab)} onClick={() => this.changeSort('joins')}>
              Joins
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === 'joins' && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div className={cn('knock-off', 'left', s.tab)} onClick={() => this.changeSort('clears')}>
              Clears
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === 'clears' && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div className='clear' />
          </div>
          {this._renderMembers(faction.id)}
        </div>
      </div>
    )
  }

  render() {
    const { currentFaction, opponentFaction } = this.props

    return (
      <div className={cn('faction-war', s.membersWrap)}>
        <div className='faction-names'>
          {this._renderFactionName(opponentFaction)}
          {this._renderFactionName(currentFaction)}
          <div className='clear' />
        </div>

        {this._renderMemberCont(opponentFaction)}
        {this._renderMemberCont(currentFaction)}

        <div className='clear' />
      </div>
    )
  }
}

export default MemberList
