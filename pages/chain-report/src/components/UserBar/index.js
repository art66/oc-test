import React, { Component } from 'react'
import PropTypes from 'prop-types'

function UserBar(props) {
  const { member } = props

  return (
    <div className='member icons left'>
      <span
        className={`user-status ${member.onlineStatus.className} left m-top10`}
        title={`<b>${member.onlineStatus.status}</b`}
        href='#'
      />
      <a className='user faction' rel='nofollow' href={`/factions.php?step=profile&ID=${member.userFactionID}`}>
        {member.isTag ? (
          <img
            src={member.tag}
            alt={member.isTag ? member.factiontag : null}
            title={member.isTag ? member.factiontag : null}
            style={{ opacity: 0.6, filter: 'alpha(opacity=60)' }}
          />
        ) : (
          <span style={{ width: 25 }} dangerouslySetInnerHTML={{ __html: member.tag }} />
        )}
      </a>
      <a
        className='user name'
        data-placeholder={`${member.playername} [${member.userID}]`}
        href={`/profiles.php?XID=${member.userID}`}
      >
        {member.honor ? (
          <img
            style={{ width: 100 }}
            src={member.honor}
            alt={member.playername}
            title={`${member.playername} [${member.userID}]`}
          />
        ) : (
          <span className='t-overflow' style={{ width: 100 }} title={`${member.playername} [${member.userID}]`}>
            {member.playername}
          </span>
        )}
      </a>
    </div>
  )
}

UserBar.propTypes = {
  member: PropTypes.object
}

export default UserBar
