import React from 'react'
import { subscribeOnDarkMode, unsubscribeFromDarkMode } from '@torn/shared/utils/darkModeChecker/index'
import ChainReport from '../ChainReport'
import RankReport from '../RankReport'
import RaidReport from '../RaidReport'
import TerritoryReport from '../TerritoryReport'
import { queryStringToObj } from '../../utils'

interface IState {
  isDarkMode: boolean
}

class ReportWrapper extends React.Component<{}, IState> {
  constructor(props) {
    super(props)

    this.state = {
      isDarkMode: false
    }
  }

  componentDidMount() {
    subscribeOnDarkMode(isDarkMode => {
      this.setState({ isDarkMode })
    })
  }

  componentWillUnmount() {
    unsubscribeFromDarkMode()
  }

  render() {
    const { isDarkMode } = this.state
    const { step, chainID, rankID, raidID, warID } = queryStringToObj(window.location.href)

    switch (step) {
      case 'chainreport':
        // use /?step=chainreport&chainID=18057117 to check
        return <ChainReport chainID={parseInt(chainID, 10)} isDarkMode={isDarkMode} />
      case 'raidreport':
        // use /?step=raidreport&raidID=1973 to check
        return <RaidReport raidID={parseInt(raidID, 10)} isDarkMode={isDarkMode} />
      case 'rankreport':
        // use /?step=rankreport&rankID=50 to check
        return <RankReport rankID={parseInt(rankID, 10)} />
      case 'warreport':
        return <TerritoryReport warID={parseInt(warID, 10)} isDarkMode={isDarkMode} />
      default:
        return null
    }
  }
}

export default ReportWrapper
