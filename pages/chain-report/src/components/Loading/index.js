import React from 'react'

const Loading = () => {
  return <img alt='' className='ajax-placeholder m-top10' src='/images/v2/main/ajax-loader.gif' />
}

export default Loading
