import React from 'react'

interface IProps {
  bonuses: any
}

const Bonuses = (props: IProps) => {
  const { bonuses } = props

  return (
    <div className='cont-gray bottom-round m-top10 m-bottom20'>
      <div className='report-stats-titles white-grad'>
        <div className='m-left10 chain-bonus-title'>Bonuses</div>
      </div>
      <ul>
        <li className='members-stats-row'>
          <div className=''>
            {bonuses.map(bonus => (
              <div key={`${bonus.attacker.userID}-${bonus.chain}-${bonus.respect}`} className='members-bonus-row'>
                <a
                  className='user name bold'
                  data-placeholder={`${bonus.attacker.playername} [${bonus.attacker.userID}]`}
                  href={`/profiles.php?XID=${bonus.attacker.userID}`}
                >
                  {bonus.attacker.playername}
                </a>
                <span> defeated </span>
                <a
                  className='user name bold'
                  data-placeholder={`${bonus.defender.playername} [${bonus.defender.userID}]`}
                  href={`/profiles.php?XID=${bonus.defender.userID}`}
                >
                  {bonus.defender.playername}
                </a>
                <span>
                  {' '}
                  to achieve the <span className='bold'>{bonus.chain}</span> chain bonus and{' '}
                </span>
                <span className='bold'>{bonus.respect}</span> respect
              </div>
            ))}
          </div>
        </li>
      </ul>
    </div>
  )
}

export default Bonuses
