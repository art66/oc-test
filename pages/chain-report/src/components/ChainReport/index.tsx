import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import InfoBox from '@torn/shared/components/InfoBox'
import TopStats from './TopStats'
import MemberList from './MemberList'
import Bonuses from './Bonuses'
import Loading from '../Loading'
import IReportData from './interfaces/IReportData'
import { getChainReportData } from '../../actions'
import { $, initialiseChart, isManualDesktopMode, getGraphSettings } from '../../utils'
import s from './index.cssmodule.scss'

interface IProps {
  getChainReportData: (chainID: number) => void
  chainID: number
  dataLoaded: boolean
  reportData: IReportData
  isDarkMode: boolean
}

interface IState {
  sortField: string
  sortDirection: string
}

class ChainReport extends React.Component<IProps, IState> {
  private graph: React.RefObject<HTMLDivElement>

  constructor(props) {
    super(props)
    this.graph = React.createRef()
  }

  componentDidMount() {
    this.props.getChainReportData(this.props.chainID)
  }

  componentDidUpdate() {
    const { dataLoaded, reportData, isDarkMode } = this.props

    if (dataLoaded && reportData.success === true) {
      initialiseChart($(this.graph.current), reportData.graph, '', getGraphSettings(isDarkMode))
    }
  }

  render() {
    const { dataLoaded, reportData, chainID } = this.props

    if (!dataLoaded) {
      return <Loading />
    }

    const { success, msg, userID, quantity, duration, faction, members, stats, bonuses } = reportData

    if (success === false) {
      return <InfoBox msg={msg} color='red' />
    }

    return (
      <div className={cn('chain', 'chain-report-wrap', s.chainReportWp)}>
        <div className='title-black m-top10 top-round'>Chain #{chainID}</div>
        <div className='cont-gray'>
          <div className='report-title'>
            <a href={`factions.php?step=profile&ID=${faction.ID}`} style={{ textDecoration: 'none' }}>
              <span className='report-title-faction-name' dangerouslySetInnerHTML={{ __html: faction.name }} />
            </a>
            <span className='report-title-info'>
              Achieved a chain of {quantity} over the course of
              {duration.days ? ` ${duration.days} days, ` : ''}
              {duration.hours ? ` ${duration.hours} hours, ` : ''}
              {duration.minutes ? ` ${duration.minutes} minutes` : ''}
            </span>
            <span className='report-title-info date m-top5'>
              {duration.start} to {duration.end}
            </span>
          </div>

          <TopStats stats={stats} />

          <div className='report-stats-chart'>
            <div className={cn('chart-placeholder', s.graph)} ref={this.graph} />
          </div>

          <MemberList members={members} userID={userID} />
        </div>
        <Bonuses bonuses={bonuses} />
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    dataLoaded: state.app.dataLoaded,
    reportData: state.app.reportData,
    mediaType: isManualDesktopMode() ? 'desktop' : state.browser.mediaType
  }),
  {
    getChainReportData
  }
)(ChainReport)
