import React from 'react'
import cn from 'classnames'
import UserInfo from '@torn/shared/components/UserInfo'
import IMember from './interfaces/IMember'
import { globalNumberFormat as gnf } from '../../utils'
import s from './index.cssmodule.scss'

const statFields = [
  'respect',
  'avg',
  'attacks',
  'leave',
  'mug',
  'hosp',
  'war',
  'bonus',
  'assist',
  'retal',
  'overseas',
  'draw',
  'escape',
  'loss'
]

const ASC = 'ask'
const DESC = 'desc'

const renderMember = (member, userID) => {
  const config = {
    showImages: member.honor !== '',
    useProgressiveImage: false,
    status: {
      mode: member.onlineStatus.status.toLowerCase()
    },
    user: {
      ID: member.userID,
      name: member.playername,
      imageUrl: member.honor,
      isSmall: true
    },
    faction: {
      ID: member.factionID,
      name: member.factiontag,
      rank: member.factionRank,
      imageUrl: member.tag
    }
  }

  return (
    <li
      key={member.userID}
      className={cn({
        'bg-gray-c': member.attacks === 0 && member.assist === 0,
        'bg-blue': member.userID === userID
      })}
    >
      <UserInfo {...config} />
    </li>
  )
}

interface IProps {
  members: IMember[]
  userID: string
}

interface IState {
  sortField: string
  sortDirection: string
}

class MemberList extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      sortField: 'attacks',
      sortDirection: DESC
    }
  }

  changeSort(sortField) {
    this.setState(prevState => ({
      sortField,
      sortDirection: prevState.sortDirection === DESC && prevState.sortField === sortField ? ASC : DESC
    }))
  }

  render() {
    const { sortField, sortDirection } = this.state
    const { members, userID } = this.props

    const sortedMembers = [...members].sort((a, b) => {
      const valueA = a[sortField]
      const valueB = b[sortField]

      if (valueA === valueB) {
        return 0
      }
      if (sortDirection === ASC) {
        return valueA < valueB ? -1 : 1
      }
      if (sortDirection === DESC) {
        return valueA > valueB ? -1 : 1
      }
      return 0
    })

    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div className='cont-gray m-top10'>
        <div className='report-members-stats'>
          <div className='report-members-stats-content'>
            <div className='left-cont'>
              <ul className='report-stats-titles white-grad '>
                <li
                  className='members c-pointer'
                  onClick={() => {
                    this.changeSort('playername_sort')
                  }}
                >
                  Members
                  <div
                    className={cn(s.sortIcon, {
                      [s.activeIcon]: sortField === 'playername_sort' && sortDirection !== '',
                      [s.desc]: sortDirection === DESC,
                      [s.asc]: sortDirection === ASC
                    })}
                  />
                </li>
              </ul>
              <ul className='members-names-rows'>{sortedMembers.map(member => renderMember(member, userID))}</ul>
            </div>
            <div className='right-cont scroll-wrap'>
              <div className='scroll-cont'>
                <ul className='report-stats-titles white-grad '>
                  {statFields.map(stat => (
                    <li
                      key={stat}
                      className={`c-pointer ${stat}`}
                      onClick={() => {
                        this.changeSort(stat)
                      }}
                    >
                      {stat.charAt(0).toUpperCase() + stat.slice(1)}
                      <div
                        className={cn(s.sortIcon, {
                          [s.activeIcon]: sortField === stat && sortDirection !== '',
                          [s.desc]: sortDirection === DESC,
                          [s.asc]: sortDirection === ASC
                        })}
                      />
                    </li>
                  ))}
                </ul>

                <ul className='members-stats-rows'>
                  {sortedMembers.map(member => (
                    <li
                      className={cn('members-stats-row', {
                        'bg-gray-c': member.attacks === 0 && member.assist === 0,
                        'bg-blue': member.userID === userID
                      })}
                      key={member.userID}
                    >
                      <ul className='members-stats-cols'>
                        {statFields.map(stat => (
                          <li key={stat} className={`members-stats-col ${stat}`}>
                            {gnf(member[stat], stat === 'avg' ? 2 : 0)}
                          </li>
                        ))}
                      </ul>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MemberList
