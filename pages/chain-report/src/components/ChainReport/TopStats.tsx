import React from 'react'

interface IProps {
  stats: any
}

const TopStats = (props: IProps) => {
  const { stats } = props

  return (
    <div className='report-stats'>
      <ul className='report-stats-cols'>
        <li className='report-stats-col'>
          <ul className='report-stats-rows'>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-chain' title='Chain' />
              <span className='report-stat-name'>Chain</span>
              <span className='report-stat-value'>{stats.chain}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-respect' title='Respect' />
              <span className='report-stat-name'>Respect</span>
              <span className='report-stat-value'>{stats.respect}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-members' title='Members involved' />
              <span className='report-stat-name'>Members</span>
              <span className='report-stat-value'>{stats.members}</span>
            </li>
          </ul>
        </li>
        <li className='report-stats-col'>
          <ul className='report-stats-rows'>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-targets-hit' title='Targets hit' />
              <span className='report-stat-name'>Targets hit</span>
              <span className='report-stat-value'>{stats.targets}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-war-hits' title='War hits' />
              <span className='report-stat-name'>War hits</span>
              <span className='report-stat-value'>{stats.wars}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-best-hit' title='Best hit' />
              <span className='report-stat-name'>Best hit</span>
              <span className='report-stat-value'>{stats.best}</span>
            </li>
          </ul>
        </li>
        <li className='report-stats-col'>
          <ul className='report-stats-rows'>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-leave' title='Leave' />
              <span className='report-stat-name'>Leave</span>
              <span className='report-stat-value'>{stats.leave}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-mug' title='Mug' />
              <span className='report-stat-name'>Mug</span>
              <span className='report-stat-value'>{stats.mug}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-hospitalize' title='Hospitalize' />
              <span className='report-stat-name'>Hospitalize</span>
              <span className='report-stat-value'>{stats.hospitalize}</span>
            </li>
          </ul>
        </li>
        <li className='report-stats-col'>
          <ul className='report-stats-rows'>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-assists' title='Assists' />
              <span className='report-stat-name'>Assists</span>
              <span className='report-stat-value'>{stats.assists}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-retaliations' title='Retaliations' />
              <span className='report-stat-name'>Retaliations</span>
              <span className='report-stat-value'>{stats.retailiations}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-overseas-hits' title='Overseas hits' />
              <span className='report-stat-name'>Overseas hits</span>
              <span className='report-stat-value'>{stats.overseas}</span>
            </li>
          </ul>
        </li>
        <li className='report-stats-col'>
          <ul className='report-stats-rows'>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-draws' title='Draws' />
              <span className='report-stat-name'>Draws</span>
              <span className='report-stat-value'>{stats.draws}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-escapes' title='Escapes' />
              <span className='report-stat-name'>Escapes</span>
              <span className='report-stat-value'>{stats.escapes}</span>
            </li>
            <li className='report-stats-row'>
              <i className='icon-chain-stat-losses' title='Losses' />
              <span className='report-stat-name'>Losses</span>
              <span className='report-stat-value'>{stats.losses}</span>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  )
}

export default TopStats
