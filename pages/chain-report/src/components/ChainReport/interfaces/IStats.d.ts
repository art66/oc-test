export default interface IStats {
  assists: string
  best: string
  chain: string
  draws: string
  escapes: string
  hospitalize: string
  leave: string
  losses: string
  members: string
  mug: string
  overseas: string
  respect: string
  retailiations: string
  stalemates: string
  targets: string
  wars: string
}
