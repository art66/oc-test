export default interface IMember {
  playername: string
  playername_sort: string
  userID: string
  factionID: string
  level: number
  onlineStatus: {
    status: string
  }
  factiontag: string
  factionRank: string
  tag: string

  attacks: number
  assists: number
  respect: number
  avg: number
  attacks: number
  leave: number
  mug: number
  hosp: number
  war: number
  bonus: number
  assist: number
  retal: number
  overseas: number
  draw: number
  escape: number
  loss: number
}
