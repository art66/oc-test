import IMember from './IMember'
import IStats from './IStats'

interface IUser {
  userID: string
  playername: sstring
}

export default interface IReportData {
  members: IMember[]
  graph: any
  faction: {
    ID: string
    name: string
  }
  userID: string
  quantity: string
  success: boolean
  duration: {
    days: number
    end: string
    hours: number
    minutes: number
    start: string
  }
  bonuses: Array<{
    chain: number
    respect: number
    attacker: IUser
  }>
  stats: IStats
  msg?: string
}
