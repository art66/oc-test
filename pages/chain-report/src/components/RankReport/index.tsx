import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import InfoBox from '@torn/shared/components/InfoBox'
import RankWarGraph from '@torn/shared/components/RankWarGraph'
import WarMemberList from '@torn/shared/components/WarMemberList'
import IMember from '@torn/shared/components/WarMemberList/interfaces/IMember'
import Loading from '../Loading'
import { getRankReportData } from '../../actions'
import { isManualDesktopMode, getDate } from '../../utils'
import styles from './index.cssmodule.scss'

interface IFaction {
  id: number
  name: string
  rewardsMessage: string
}

interface IProps {
  dataLoaded: boolean
  rankID: number
  getRankReportDataProp: (rankID: number, step: string) => void
  reportData: {
    success?: boolean
    message?: string
    currentUser: {
      settings: {
        showImages: boolean
      }
    }
    members: Array<IMember>
    graph: {
      data: Array<Array<[number, number]>>
      respectRequirement: Array<Array<[number, number]>>
    }
    faction: Array<IFaction>
    war: {
      start: number
      end: number
      winner: number
      forfeit: boolean
    }
  }
  mediaType: string
}

class RankReport extends React.Component<IProps> {
  componentDidMount() {
    this.props.getRankReportDataProp(this.props.rankID, 'rankreport')
  }

  renderTitle(winner, loser) {
    const { rankID, reportData, mediaType } = this.props

    const { graph, war } = reportData
    const warFinished = war.end > 0

    return (
      <>
        <div className={cn('title-black', 'm-top10', 'top-round')}>Ranked War #{rankID}</div>
        <div className={cn('cont-gray', 'border-gray')}>
          <div className='report-title'>
            <span className='report-title-info'>
              {warFinished || <span className={styles.titleFontSize}>The Ranked War between </span>}
              <a
                className={cn('t-blue', 'h', styles.titleFontSize)}
                href={`/factions.php?step=profile&ID=${winner.id}`}
              >
                <span dangerouslySetInnerHTML={{ __html: winner.name }} />
              </a>
              <span className={styles.titleFontSize}> {warFinished ? 'has defeated' : 'and'} </span>
              <a className={cn('t-blue', 'h', styles.titleFontSize)} href={`/factions.php?step=profile&ID=${loser.id}`}>
                <span dangerouslySetInnerHTML={{ __html: loser.name }} />
              </a>
              <span className={styles.titleFontSize}> {warFinished ? 'in a ranked war' : 'is in progress'}</span>
              {!war.forfeit || <span> (forfeit)</span>}
            </span>
            <span className={cn('report-title-info', 'date', 'm-top10')}>
              {getDate(war.start)} until {getDate(war.end)}
            </span>
          </div>

          {this.renderRewards()}

          <div className={cn('chart-placeholder', styles.chartWrapper)}>
            <RankWarGraph
              points={graph.data}
              respectRequirement={graph.respectRequirement}
              currentFactionName={winner.name}
              opponentFactionName={loser.name}
              end={war.end}
              mediaType={mediaType}
            />
          </div>
        </div>
      </>
    )
  }

  renderRewards() {
    const {
      reportData: { faction }
    } = this.props
    const factionRewards = faction.filter(f => f.rewardsMessage)

    if (factionRewards.length === 0) {
      return null
    }

    return (
      <ul>
        {factionRewards.map(f => (
          <li className={cn('members-bonus-row', styles.bonusRow)} key={f.id}>
            <div className={cn('', styles.memberBonusRow)}>
              <div dangerouslySetInnerHTML={{ __html: f.rewardsMessage }} />
            </div>
          </li>
        ))}
      </ul>
    )
  }

  render() {
    const { dataLoaded, reportData, mediaType } = this.props

    if (!dataLoaded) {
      return <Loading />
    }

    const { success, message } = reportData

    if (success === false) {
      return <InfoBox msg={message} color='red' />
    }

    const {
      faction,
      war,
      members,
      currentUser: {
        settings: { showImages }
      }
    } = reportData
    const winner = faction.filter(f => f.id === war.winner)[0]
    const loser = faction.filter(f => f.id !== war.winner)[0]

    return (
      <div className={cn(styles.rankReportWrap, 'chain', 'chain-report-wrap', 'war-report-wrap')}>
        {this.renderTitle(winner, loser)}
        <div className='f-war-list war-new'>
          <div className='desc-wrap'>
            <WarMemberList
              members={members}
              currentFaction={winner}
              opponentFaction={loser}
              isProfileMode={true}
              scoreFieldName='score'
              mediaType={mediaType}
              isReport={true}
              showImages={showImages}
            />
          </div>
          <div className='clear' />
        </div>
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    dataLoaded: state.app.dataLoaded,
    reportData: state.app.reportData,
    mediaType: isManualDesktopMode() ? 'desktop' : state.browser.mediaType
  }),
  {
    getRankReportDataProp: getRankReportData
  }
)(RankReport)
