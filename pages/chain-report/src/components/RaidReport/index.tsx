import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import WarMemberList from '@torn/shared/components/WarMemberList'
import InfoBox from '@torn/shared/components/InfoBox'
import IMember from '@torn/shared/components/WarMemberList/interfaces/IMember'
import IFaction from '@torn/shared/components/WarMemberList/interfaces/IFaction'
import Loading from '../Loading'
import { getRaidReportData } from '../../actions'
import { initialiseChart, isManualDesktopMode, getGraphSettings, getDate } from '../../utils'
import s from './index.cssmodule.scss'

interface IProps {
  getRaidReportData: (raidID: number) => void
  isDarkMode: boolean
  raidID: number
  dataLoaded: boolean
  mediaType: string
  reportData: {
    msg?: string
    showImages: boolean
    success: boolean
    raid: {
      start: number
      end: number
    }
    defenderFaction: IFaction
    aggressorFaction: IFaction
    members: IMember[]
    graph: Array<{
      showTooltip: number
      data: Array<[number, number]>
    }>
  }
}

class RaidReport extends React.Component<IProps> {
  private graph: React.RefObject<HTMLDivElement>

  constructor(props) {
    super(props)
    this.graph = React.createRef()
  }

  componentDidMount() {
    this.props.getRaidReportData(this.props.raidID)
  }

  componentDidUpdate() {
    const { dataLoaded, isDarkMode, reportData } = this.props

    if (dataLoaded && reportData.success === true) {
      initialiseChart(this.graph.current, reportData.graph, 'raid', getGraphSettings(isDarkMode))
    }
  }

  render() {
    if (!this.props.dataLoaded) {
      return <Loading />
    }

    const {
      reportData: { success, msg },
      mediaType,
      raidID
    } = this.props

    if (success === false) {
      return <InfoBox msg={msg} color='red' />
    }

    const { raid, aggressorFaction, defenderFaction, members, showImages } = this.props.reportData

    return (
      <div className={cn('chain chain-report-wrap war-report-wrap', s.reportWrap)}>
        <div className='title-black m-top10 top-round'>Raid #{raidID}</div>
        <div className='cont-gray border-gray'>
          <div className='report-title'>
            <span className='report-title-info'>
              <a className='t-blue h' href={`/factions.php?step=profile&ID=${aggressorFaction.id}`}>
                <span dangerouslySetInnerHTML={{ __html: aggressorFaction.name }} />
              </a>
              <span>&apos;s raid on </span>
              <a className='t-blue h' href={`/factions.php?step=profile&ID=${defenderFaction.id}`}>
                <span dangerouslySetInnerHTML={{ __html: defenderFaction.name }} />
              </a>
            </span>
            <span className={cn('report-title-info', 'date', 'm-top10')}>
              {getDate(raid.start)} until {getDate(raid.end)}
            </span>
          </div>

          <div className='report-stats-chart'>
            <div className='chart-placeholder' ref={this.graph} style={{ height: 200 }} />
          </div>
        </div>
        <div className='f-war-list war-new'>
          <div className='desc-wrap'>
            <WarMemberList
              members={members}
              currentFaction={aggressorFaction}
              opponentFaction={defenderFaction}
              isProfileMode={true}
              scoreFieldName='damage'
              mediaType={mediaType}
              isReport={true}
              showImages={showImages}
            />
          </div>
          <div className='clear' />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  dataLoaded: state.app.dataLoaded,
  reportData: state.app.reportData,
  mediaType: isManualDesktopMode() ? 'desktop' : state.browser.mediaType
})

const mapActionsToProps = {
  getRaidReportData
}

export default connect(mapStateToProps, mapActionsToProps)(RaidReport)
