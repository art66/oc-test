import { createSelector } from 'reselect'
import get from 'lodash.get'

import isRival from '../../utils/isRival'
import { imageUrl, opponent } from './helpers'

const getOpponentsEquippedImageID = (state, { userType }) => {
  return get(state.attack.currentItems, !isRival(userType) ? 'user2EquipedItemImageID' : 'user1EquipedItemImageID')
}

const getOpponentsAttackResult = (state, { userType }) => {
  return get(state.attack, `attacking.${opponent(userType)}`) || {}
}

const getBulletsSelector = () => createSelector(
  [getOpponentsEquippedImageID, getOpponentsAttackResult],
  (itemID, attackResult) => {
    // take item id from items array,
    // if there is no itemID for that element, this is Kick of Fist
    // and we can grab it by name
    return {
      imageSrc: imageUrl(itemID, true),
      damageValue: attackResult.damageValue,
      result: attackResult.result
    }
  }
)

export default getBulletsSelector
