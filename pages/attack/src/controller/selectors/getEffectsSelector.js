import { createSelector } from 'reselect'
import get from 'lodash.get'

import getUser from './getUser'

const getEffects = state => get(state.attack, 'currentTemporaryEffects') || {}

const getEffectsSelector = () => createSelector(
  [getEffects, getUser],
  (effects, user) => effects[user.userID] || []
)

export default getEffectsSelector
