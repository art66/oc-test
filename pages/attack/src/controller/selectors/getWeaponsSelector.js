import { createSelector } from 'reselect'
import get from 'lodash.get'
import map from 'lodash.map'

import getUser from './getUser'
import getItems from './getItems'
import isRival from '../../utils/isRival'
import { MT } from '../../constants'
import { imageUrl } from './helpers'
const BOXES_SIZES = {
  Primary: 'full',
  Secondary: 'full',
  Melee: 'full',
  Temporary: 'full',
  Fists: 'small',
  Kick: 'small'
}

const getDesktopManualLayout = state => get(state.common, 'isDesktopManualLayout')

const getAttackStatus = state => get(state.attack, 'attackStatus')

const getAmmoStatuses = (state, { userType }) => get(state.attack, `${userType}AmmoStatus`)

const isItMe = (_state, { userType }) => !isRival(userType)

const getUser2EquipedItemID = state => {
  return get(state.attack.currentItems, 'user2EquipedItemID')
}

const getMediaType = state => get(state, 'browser.mediaType')

const getCurrentFightStatistics = state => get(state.attack, 'currentFightStatistics')

const hasAmmo = ammoStatus => !['No ammo', 'No clips'].includes(ammoStatus)

const getShowEnemyItems = state => get(state, 'attack.showEnemyItems', false)

const getDarkMode = state => get(state, 'common.isDarkModeEnabled')

const resolveAmmoStatus = (ammoStatus, me, title, showEnemyItems) => {
  switch (title) {
    case 'Primary':
    case 'Secondary':
      return me && ammoStatus || (hasAmmo(ammoStatus) && !showEnemyItems) && 'Unknown' || ammoStatus
    case 'Temporary':
      return ''
    case 'Melee':
    case 'Fists':
    case 'Kick':
      return 'infinity'
    default:
      return ''
  }
}

const weaponFields = (items, ammoStatuses, me, user2EquipedItemID, mediaType, attackStarted, showEnemyItems, desktopManualLayout, isDarkMode) => (
  title,
  equipSlot,
  optional
) => {
  // console.log(desktopManualLayout, 'weaponFields')
  const item = get(items, `[${equipSlot}].item.[0]`)

  if (!item && optional) return undefined
  const { accuracy, dmg, ID, name: weaponTitle, currentBonuses, currentUpgrades, rarity } = item || {}
  const ammoStatus = resolveAmmoStatus(get(ammoStatuses, equipSlot), me, title, showEnemyItems)
  const equipped = Boolean(ID)
  const active = equipped && me && hasAmmo(ammoStatus)
  const orange = !me && Number(equipSlot) === Number(user2EquipedItemID) && attackStarted
  const imgSrc = imageUrl(ID, equipped, title, isDarkMode)
  const type = mediaType === MT.PHONE && !desktopManualLayout ? 'small' : BOXES_SIZES[title]

  return {
    accuracy: accuracy,
    active,
    ammoStatus,
    boxTitle: title,
    currentBonuses: currentBonuses && map(currentBonuses, ({ icon, hoverover }) => ({ icon, hoverover })),
    currentUpgrades: currentUpgrades && map(currentUpgrades, ({ icon, hoverover }) => ({ icon, hoverover })),
    dmg,
    ID,
    imgSrc,
    orange,
    showItemsBonuses: title !== 'Temporary',
    type,
    equipSlot,
    weaponTitle,
    rarity
  }
}
const weapons = () => createSelector(
  [getItems, getAmmoStatuses, isItMe, getUser2EquipedItemID, getMediaType, getCurrentFightStatistics, getUser, getShowEnemyItems, getDesktopManualLayout, getAttackStatus, getDarkMode],
  (items, ammoStatuses, me, user2EquipedItemID, mediaType, currentFightStatistics, user, showEnemyItems, desktopManualLayout, attackStatus, isDarkMode) => {
    const attackStarted = attackStatus === 'started'

    const getWeaponFields = weaponFields(items, ammoStatuses, me, user2EquipedItemID, mediaType, attackStarted, showEnemyItems, desktopManualLayout, isDarkMode)

    return [
      getWeaponFields('Primary', 1),
      getWeaponFields('Secondary', 2),
      getWeaponFields('Melee', 3),
      getWeaponFields('Temporary', 5),
      getWeaponFields('Fists', 999),
      getWeaponFields('Kick', 1000, true)
    ].filter(Boolean)
  }
)

export default weapons
