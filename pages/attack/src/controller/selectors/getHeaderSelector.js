import { createSelector } from 'reselect'
import get from 'lodash.get'

import { opponent } from './helpers'
import getUser from './getUser'

const validateMitigatedValue = (mitigated, max) => {
  let tempMitigated = Number(mitigated || 0)
  const tempMax = Number(max || 0)

  if (tempMitigated < 0) {
    tempMitigated = 0
  }

  if (tempMitigated > tempMax) {
    tempMitigated = tempMax
  }

  return tempMitigated
}

const getStatistics = state => get(state.attack, 'currentFightStatistics') || {}

const getUserLife = (state, { userType }) => get(state.attack, `usersLife.${userType}`) || {}

const getDamage = (state, { userType }) => get(state.attack, `attacking.${opponent(userType)}`) || {}

const getHeaderSelector = () => (
  createSelector(
    [getStatistics, getUserLife, getUser, getDamage],
    (currentFightStatistics, usersLife, user, damage) => ({
      ...currentFightStatistics[user.userID],
      damageMitigated: validateMitigatedValue(damage.damageMitigated, usersLife.maxLife),
      health: Number(usersLife.currentLife || 0),
      healthMax: Number(usersLife.maxLife || 0),
      lifeBar: Number(usersLife.lifeBar || 0),
      playername: user.playername
    })
  )
)

export default getHeaderSelector
