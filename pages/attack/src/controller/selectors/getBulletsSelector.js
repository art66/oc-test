import { createSelector } from 'reselect'
import get from 'lodash.get'
import isObject from '@torn/shared/utils/isObject'

import getUser from './getUser'
import { opponent } from './helpers'

// all hits but last
const getHitsLog = state => get(state.attack, 'hitsLog')

// the last hit is here
const getHitInfo = (state, { userType }) => get(state.attack, `attacking.${opponent(userType)}.hitInfo`)

const round = val => Math.round(Number(val))

const getFieldsForABullet = ({ attackResult, x, y }) => ({
  type: attackResult === 'hit' ? 'hitOld' : 'missOld',
  left: round(x),
  top: round(y)
})

const getFieldsForLastHit = hit => ({
  type: hit.target === '0' ? 'miss' : 'hit',
  left: round(hit.x),
  top: round(hit.y)
})

const getBulletsSelector = () => (
  createSelector(
    [getHitsLog, getHitInfo, getUser],
    (hitsLog, hitInfo, user) => {
      const hitsLogTemp = (hitsLog && hitsLog[user.userID]) || []
      const hitInfoTemp = hitInfo ? [...isObject(hitInfo) ? [hitInfo] : hitInfo] : []

      return [...hitsLogTemp.map(getFieldsForABullet), ...hitInfoTemp.map(getFieldsForLastHit)]
    }
  )
)

export default getBulletsSelector
