import { createSelector } from 'reselect'
import get from 'lodash.get'

const getAttackStatus = state => get(state.attack, 'attackStatus')

const status = createSelector(getAttackStatus, attackStatus => {
  let tempAttackStatus = attackStatus

  if (tempAttackStatus && tempAttackStatus !== 'notStarted' && tempAttackStatus !== 'end') {
    tempAttackStatus = 'started'
  }

  return {
    attackStatus: tempAttackStatus
  }
})

export default status
