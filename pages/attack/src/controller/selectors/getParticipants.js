import { createSelector } from 'reselect'
import get from 'lodash.get'

const getCurrentDefends = state => get(state.attack, 'currentDefends')

const getCurrentFightStatistics = state => get(state.attack, 'currentFightStatistics')

const getParticipants = createSelector(
  [getCurrentDefends, getCurrentFightStatistics],
  (currentDefends, currentFightStatistics) => {
    if (currentDefends) {
      return currentDefends.map(user => ({
        ...user,
        ...(currentFightStatistics && currentFightStatistics[user.attackerID])
      }))
    }
    return []
  }
)

export default getParticipants
