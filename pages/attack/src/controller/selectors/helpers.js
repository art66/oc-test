import { ME, RIVAL } from '../../constants'
import isRival from '../../utils/isRival'

/**
  jsut get the opponent
*/
export const opponent = userType => (!isRival(userType) ? RIVAL : ME)

/**
 image src can have following values:

 - defaults, basicaly are there under disabling div
 /images/items/primary.png
 /images/items/secondary.png
 /images/items/melee.png
 /images/items/temporary.png

 - if ID equipped
 /images/items/<ID>/large.png -> for active, and
 /images/items/<ID>/blank.png -> for no ammo

- if no weapon, default value for slot 3 and 4
 /images/v2/attack/items/weapon/kick.png
 /images/v2/attack/items/weapon/kick_b.png
 /images/v2/attack/items/weapon/fists.png
 /images/v2/attack/items/weapon/fists_b.png
 CRAZY!
*/
export const imageUrl = (ID, active, title, isDarkMode) => {
  const DMSelector = isDarkMode ? '_dark' : ''

  if (ID === 999) {
    return `/images/v2/attack/items/weapon/fists${active ? '' : '_b'}.png`
  }
  if (ID === 1000) {
    return `/images/v2/attack/items/weapon/kick${active ? '' : '_b'}.png`
  }
  if (ID) {
    return `/images/items/${ID}/${active ? 'large' : 'blank'}${DMSelector}.png`
  }
  if (title) {
    return `/images/items/${title.toLowerCase()}.png`
  }
  return ''
}
