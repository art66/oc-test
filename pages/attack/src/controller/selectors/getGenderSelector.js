import { createSelector } from 'reselect'
import getUser from './getUser'

const getGenderSelector = () => createSelector(
  [getUser],
  user => (user.gender ? user.gender.toLowerCase() : '')
)

export default getGenderSelector
