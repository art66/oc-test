import get from 'lodash.get'

const getItems = (state, { userType }) => get(state.attack, `${userType}Items`) || {}

export default getItems
