import get from 'lodash.get'

const getUser = (state, { userType }) => get(state.attack, `${userType}User`) || {}

export default getUser
