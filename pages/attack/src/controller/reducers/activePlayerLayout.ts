import { ME, SET_ACTIVE_PLAYER_LAYOUT } from '../../constants'

export const initialState = {
  activePlayerLayout: ME.toString()
}

const ACTION_HANDLERS = {
  [SET_ACTIVE_PLAYER_LAYOUT]: (state, action) => ({
    ...state,
    activePlayerLayout: action.layoutModel.toString()
  })
}

const reducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
