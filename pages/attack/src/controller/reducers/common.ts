import { DEBUG_MESSAGE, CHECK_MANUAL_DESKTOP_MODE, CHECK_DARK_MODE } from '../../constants'
import { ICommon, IType, ICheckDesktopMode, IDebug, ICheckDarkMode } from '../../interfaces/IController'

const initialState: ICommon = {
  isDarkModeEnabled: false,
  isDesktopManualLayout: false,
  debugInfo: null
}

const ACTION_HANDLERS = {
  [DEBUG_MESSAGE]: (state: ICommon, action: IDebug) => ({
    ...state,
    debugInfo: action.debugMessage
  }),
  [CHECK_MANUAL_DESKTOP_MODE]: (state: ICommon, action: ICheckDesktopMode) => ({
    ...state,
    isDesktopManualLayout: action.status
  }),
  [CHECK_DARK_MODE]: (state: ICommon, action: ICheckDarkMode) => ({
    ...state,
    isDarkModeEnabled: action.status
  })
}

const reducer = (state: ICommon = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
