import isEmpty from '@torn/shared/utils/isEmpty'
import wsResponseAdapter from '../helpers/wsResponseAdapter'

import {
  INIT_START,
  INIT_DONE,
  UPDATE_START,
  UPDATE_DONE,
  ATTACK_START,
  ATTACK_DONE,
  FIGHT_FINISH_ATTEMPT,
  FIGHT_FINISH_DONE,
  FIGHT_START_ATTEMPT,
  FIGHT_START_DONE,
  POLL_DONE,
  POLL_ATTEMPT,
  ESCAPE_START,
  ESCAPE_DONE,
  SHOW_LOG_MODAL_ACTION,
  ERROR_MODAL,
  DEBUG_MESSAGE,
  AGGRESSION_UPDATED,
  HIT_UPDATE,
  SPECTATOR_UPDATE,
  ESCAPE_UPDATE
  // DESTROY_HEALTH_DAMAGE
} from '../../constants'
import {
  // IAttack,
  IType,
  IInit,
  IUpdate,
  IFightStart,
  IAttackAttempt,
  IAttackDone,
  IPoll,
  IFightEnd,
  IShowModalLog,
  IErrorModal,
  IAggressionUpdate,
  IWSPayload
  // IDestroyHealthDamage
} from '../../interfaces/IController'
import { IState } from '../../interfaces/IState'

import initialState from '../../store/initialState'

const dataParser = (state: IState, action: { payload: IState }, isSpectator?: boolean) => {
  const serverState = action.payload

  if (!serverState || Object.keys(serverState).length === 0) {
    return state
  }

  const checkAttackingItemsToStats = (incomingState, currentState) => {
    return {
      currentItems: {
        user1EquipedItemID: incomingState.user1EquipedItemID || currentState.user1EquipedItemID,
        user1EquipedItemImageID: incomingState.user1EquipedItemImageID || currentState.user1EquipedItemImageID,
        user2EquipedItemID: incomingState.user2EquipedItemID || currentState.user2EquipedItemID,
        user2EquipedItemImageID: incomingState.user2EquipedItemImageID || currentState.user2EquipedItemImageID
      }
    }
  }

  // hack for the case when attack is over and no data is available
  const usersLife = () => ({
    usersLife: {
      attacker: isSpectator ?
        {} :
        {
          ...((state.usersLife && state.usersLife.attacker) || {}),
          ...((serverState.usersLife && serverState.usersLife.attacker) || {})
        },
      defender: {
        ...((state.usersLife && state.usersLife.defender) || {}),
        ...((serverState.usersLife && serverState.usersLife.defender) || {})
      }
    }
  })

  return {
    ...state,
    ...serverState,
    ...checkAttackingItemsToStats(serverState, state),
    ...usersLife()
  }
}

const ACTION_HANDLERS = {
  [INIT_START]: (state: IState) => ({
    ...state,
    dataLoaded: false
  }),
  [INIT_DONE]: (state: IState, action: IInit) => ({
    ...dataParser(state, action),
    dataLoaded: true
  }),
  [UPDATE_START]: (state: IState) => ({
    ...state,
    inUpdateStage: true
  }),
  [UPDATE_DONE]: (state: IState, action: IUpdate) => ({
    ...dataParser(state, action),
    inUpdateStage: false
  }),
  [FIGHT_START_ATTEMPT]: (state: IState) => ({
    ...state,
    inProgress: true
  }),
  [FIGHT_START_DONE]: (state: IState, action: IFightStart) => ({
    ...dataParser(state, action),
    inProgress: false
  }),
  [ATTACK_START]: (state: IState, action: IAttackAttempt) => ({
    ...state,
    inProgress: true,
    preventWSUpdate: true,
    lockedWeapon: action.weaponID
  }),
  [ATTACK_DONE]: (state: IState, action: IAttackDone) => ({
    ...dataParser(state, action),
    inProgress: false,
    preventWSUpdate: false,
    lockedWeapon: null,
    isFightFinished: !!action.payload.endResult || false
  }),
  [POLL_ATTEMPT]: (state: IState) => ({
    ...state,
    inUpdateStage: true
  }),
  [POLL_DONE]: (state: IState, action: IPoll) => ({
    ...dataParser(state, action),
    inUpdateStage: false
  }),
  [FIGHT_FINISH_ATTEMPT]: (state: IState) => ({
    ...state,
    inProgress: true
  }),
  [FIGHT_FINISH_DONE]: (state: IState, action: IFightEnd) => ({
    ...dataParser(state, action),
    inProgress: false,
    isFightFinished: action.payload.attackStatus === 'end',
    escapeInProgress: false
  }),
  [ESCAPE_START]: (state: IState) => ({
    ...state,
    escapeInProgress: true
  }),
  [ESCAPE_DONE]: (state: IState, action: IFightEnd) => ({
    ...dataParser(state, action),
    escapeInProgress: false,
    isFightFinished: action.payload.attackStatus === 'end'
  }),
  [ERROR_MODAL]: (state: IState, action: IErrorModal) => ({
    ...state,
    error: action.error,
    isFightFinished: true,
    attackStatus: 'end',
    inUpdateStage: false,
    inProgress: false,
    usersLife: {
      ...state.usersLife,
      attacker: {
        ...state.usersLife.attacker,
        lifeBar: 0
      },
      defender: {
        ...state.usersLife.defender,
        lifeBar: 0
      }
    }
  }),
  [SHOW_LOG_MODAL_ACTION]: (state: IState, action: IShowModalLog) => ({
    ...state,
    isFightFinished: true,
    attackStatus: 'end',
    isModalError: action.error === 'You can\'t attack while in hospital',
    inUpdateStage: false,
    inProgress: false,
    usersLife: {
      ...state.usersLife,
      attacker: {
        ...state.usersLife.attacker
        // lifeBar: 0
      },
      defender: {
        ...state.usersLife.defender
        // lifeBar: 0
      }
    },
    endResult: {
      result: 'ok',
      info: 'The fight is over',
      outcome: 'pos',
      step: 'leave'
    }
  }),
  [DEBUG_MESSAGE]: (state: IState, action: any) => ({
    ...state,
    ...action.options
  }),
  [AGGRESSION_UPDATED]: (state: IState, action: IAggressionUpdate) => {
    return {
      ...state,
      distraction: action.distraction,
      defenderUser: {
        ...state.defenderUser,
        statsModifiers: !isEmpty(action.statsModifiers) ? action.statsModifiers : state.defenderUser.statsModifiers
      }
    }
  },
  [HIT_UPDATE]: (state: IState, action: { payload: IWSPayload } & IType) => ({
    ...state,
    ...dataParser(state, { payload: wsResponseAdapter(state, action.payload) })
  }),
  [SPECTATOR_UPDATE]: (state: IState, action: { payload: IWSPayload } & IType): IState => {
    const serverState: IState = wsResponseAdapter(state, action.payload)

    const mergedState: IState = {
      ...state,
      ...serverState
    }

    return {
      ...mergedState,
      escapeAvailable: state.escapeAvailable,
      attackStatus: state.attackStatus,
      // endResult: state.endResult,
      finishOptions: state.finishOptions,
      usersLife: {
        ...mergedState.usersLife,
        attacker: state.usersLife.attacker
      },
      attackerUser: state.attackerUser,
      attackerItems: state.attackerItems,
      attackerAmmo: state.attackerAmmo
    } as IState
  },
  [ESCAPE_UPDATE]: (state: IState, action: { payload: IWSPayload } & IType): IState => ({
    ...state,
    ...action.payload
  })
}

const reducer = (state: IState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
