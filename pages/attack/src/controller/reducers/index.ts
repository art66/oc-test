import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'

import attack from './attack'
import common from './common'
import activePlayerLayout from './activePlayerLayout'

const rootReducer = combineReducers({
  browser: createResponsiveStateReducer({ phone: 600, tablet: 1000 }, 'desktop'),
  attack,
  common,
  activePlayerLayout
})

export default rootReducer
