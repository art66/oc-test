import { put } from 'redux-saga/effects'
import { fetchUrl, getURLParameterByName } from '@torn/shared/utils'

import { BASE_URL } from '../../constants'
import { updateDone, showFightLogModal } from '../actions/attack'
import { showDebug } from '../actions/common'
import newAttackStartChecker from '../../utils/newAttackStartChecker'

import getState from './helpers/getState'

function* updateLoadSaga() {
  try {
    const { attack } = yield getState()

    if (attack.inProgress || attack.isFightFinished) {
      return
    }

    const json = yield fetchUrl(`${BASE_URL}&user2ID=${getURLParameterByName('user2ID')}`, null, false)

    const isNewAttackCanStart = newAttackStartChecker({
      prevStatus: attack.attackStatus,
      nextStatus: json.DB.attackStatus,
      endResult: json.DB.endResult
    })

    isNewAttackCanStart ? yield put(showFightLogModal({ error: json.DB.error })) : yield put(updateDone(json.DB))
  } catch (error) {
    yield put(
      showDebug({
        debugMessage: 'Oops! Something went wrong during data update request.',
        options: {
          inUpdateStage: false,
          inProgress: false
        }
      })
    )
    console.error('Error during update request:', error)
  }
}

export default updateLoadSaga
