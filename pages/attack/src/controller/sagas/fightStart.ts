import { put } from 'redux-saga/effects'
import { fetchUrl, getURLParameterByName } from '@torn/shared/utils'

import { BASE_URL, STEP_START } from '../../constants'
import { fightStartDone } from '../actions/attack'
import { showDebug } from '../actions/common'

function* fight() {
  try {
    const json = yield fetchUrl(
      BASE_URL,
      {
        step: STEP_START,
        user2ID: getURLParameterByName('user2ID')
      },
      false
    )

    yield put(fightStartDone(json.DB))
  } catch (error) {
    yield put(
      showDebug({
        debugMessage: 'Oops! Something went wrong during fight start request.',
        options: {
          inProgress: false
        }
      })
    )
    console.error('Error during fight start request:', error)
  }
}

export default fight
