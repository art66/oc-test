import { takeLatest } from 'redux-saga/effects'

import attack from './attack'
import fightStart from './fightStart'
import initLoad from './initLoad'
import updateLoad from './updateLoad'
import poll from './poll'
import fightEnd from './fightEnd'

import {
  ATTACK_START,
  INIT_START,
  UPDATE_START,
  POLL_ATTEMPT,
  FIGHT_START_ATTEMPT,
  FIGHT_FINISH_ATTEMPT,
  ESCAPE_START
} from '../../constants'

export default function* watchSagas() {
  yield takeLatest(ATTACK_START, attack)
  yield takeLatest(FIGHT_START_ATTEMPT, fightStart)
  yield takeLatest(INIT_START, initLoad)
  yield takeLatest(UPDATE_START, updateLoad)
  yield takeLatest(POLL_ATTEMPT, poll)
  yield takeLatest(FIGHT_FINISH_ATTEMPT, fightEnd)
  yield takeLatest(ESCAPE_START, fightEnd)
}
