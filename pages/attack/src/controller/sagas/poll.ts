import { put } from 'redux-saga/effects'
import { fetchUrl, getURLParameterByName } from '@torn/shared/utils'

import { BASE_URL } from '../../constants'
import { pollDone, showFightLogModal } from '../actions/attack'
import { showDebug } from '../actions/common'
import newAttackStartChecker from '../../utils/newAttackStartChecker'

import getState from './helpers/getState'

function* pollSaga() {
  try {
    const { attack } = yield getState()

    if (attack.inProgress || attack.escapeInProgress || attack.isFightFinished) {
      return
    }

    const json = yield fetchUrl(`${BASE_URL}&step=poll&user2ID=${getURLParameterByName('user2ID')}`, null, false)

    if (!json || !json.DB || json.DB.endResult) return

    const isNewAttackCanStart = newAttackStartChecker({
      prevStatus: attack.attackStatus,
      nextStatus: json.DB.attackStatus,
      endResult: json.DB.endResult
    })

    isNewAttackCanStart ? yield put(showFightLogModal({ error: json.DB.error })) : yield put(pollDone(json.DB))
  } catch (error) {
    yield put(
      showDebug({
        debugMessage: 'Oops! Something went wrong during background update request.',
        options: {
          inUpdateStage: false,
          inProgress: false
        }
      })
    )
    console.error('Error during poll request:', error)
  }
}

export default pollSaga
