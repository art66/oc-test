import { put } from 'redux-saga/effects'
import { fetchUrl, getURLParameterByName } from '@torn/shared/utils'

import { BASE_URL } from '../../constants'
import { initLoadDone } from '../actions/attack'
import { showDebug } from '../actions/common'

function* initLoadSaga() {
  try {
    const json = yield fetchUrl(`${BASE_URL}&user2ID=${getURLParameterByName('user2ID')}`, null, false)

    yield put(initLoadDone(json.DB))
  } catch (error) {
    yield put(
      showDebug({
        debugMessage: 'Oops! Something went wrong during initial load request.',
        options: {
          dataLoaded: false
        }
      })
    )
    console.error('Error during fight initial load request:', error)
  }
}

export default initLoadSaga
