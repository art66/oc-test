import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'

import { BASE_URL, STEP_END } from '../../constants'
import { fightFinishDone, escapeDone, showFightLogModal, endFightModal } from '../actions/attack'
import { showDebug } from '../actions/common'
import getState from './helpers/getState'
import newAttackStartChecker from '../../utils/newAttackStartChecker'

function* end({ step, fightResult, options, isEscape }: any) {
  try {
    const { attack } = yield getState()

    // if timer is started somehow after actual commit we should also handle it here
    if (attack.isFightFinished) {
      return
    }

    const json = yield fetchUrl(
      BASE_URL,
      {
        step: step || STEP_END,
        ...fightResult ? { fightResult } : {},
        ...options
      },
      false
    )

    const isNewAttackCanStart = newAttackStartChecker({
      prevStatus: attack.attackStatus,
      nextStatus: json.DB.attackStatus,
      endResult: json.DB.endResult
      // error: json.DB.error
    })

    if (isNewAttackCanStart) {
      yield put(showFightLogModal({ error: json.DB.error }))

      return
    }

    // and here as well
    if (json.DB.error) {
      yield put(endFightModal({ error: json.DB.error }))

      return
    }

    isEscape ? yield put(escapeDone(json.DB)) : yield put(fightFinishDone(json.DB))
  } catch (error) {
    yield put(
      showDebug({
        debugMessage: 'Oops! Something went wrong during fight end request.',
        options: {
          escapeInProgress: false,
          inProgress: false
        }
      })
    )
    console.error('Error during fight end request:', error)
  }
}

export default end
