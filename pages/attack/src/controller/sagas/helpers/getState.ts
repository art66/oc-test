import { select } from 'redux-saga/effects'

function* getState() {
  const state = yield select(store => store)

  return state
}

export default getState
