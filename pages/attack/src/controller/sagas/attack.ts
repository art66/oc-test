import { put } from 'redux-saga/effects'
import { fetchUrl, getURLParameterByName } from '@torn/shared/utils'

import { BASE_URL, STEP_ATTACK } from '../../constants'
import { attackDone } from '../actions/attack'
import { showDebug } from '../actions/common'

import attackIsOver from '../../utils/attackIsOver'
import getState from './helpers/getState'

function* attackSaga({ weaponID }: any) {
  const { attack } = yield getState()

  if (attackIsOver(attack.attackStatus)) {
    return
  }

  try {
    const json = yield fetchUrl(
      BASE_URL,
      {
        step: STEP_ATTACK,
        user2ID: getURLParameterByName('user2ID'),
        user1EquipedItemID: weaponID
      },
      false
    )

    yield put(attackDone(json.DB))
  } catch (error) {
    yield put(
      showDebug({
        debugMessage: 'Oops! Something went wrong during attack request.',
        options: {
          inProgress: false,
          lockedWeapon: null
        }
      })
    )
    console.error('Error during attack request:', error)
  }
}

export default attackSaga
