import { Store } from 'redux'

import initAttackerWS from './attacker'
import initSpectatorWS from './spectator'

const initWS = ({ getState, dispatch }: Store) => {
  initAttackerWS(getState, dispatch)
  initSpectatorWS(getState, dispatch)
}

export default initWS
