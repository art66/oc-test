import { spectatorUpdate } from '../actions/attack'
import { IWSPayload } from '../../interfaces/IController'

const NOT_STARTED = 'notStarted'

const initSpectatorWS = (getState, dispatch) => {
  const defenderID = window.location.href.replace(/.*=/i, '')

  // @ts-ignore global Websocket & Centrifuge handler!!!
  const handlerSpectator = new WebsocketHandler('attack', { channel: `attackSpectator_${defenderID}` })

  handlerSpectator.setActions({
    join: (payloadData: { data: IWSPayload }) => {
      if (getState().attack?.attackStatus !== NOT_STARTED) {
        return
      }

      dispatch(spectatorUpdate({ payload: payloadData.data }))
    },
    hit: (payloadData: { data: IWSPayload }) => {
      if (getState().attack?.attackStatus !== NOT_STARTED) {
        return
      }

      dispatch(spectatorUpdate({ payload: payloadData.data }))
    },
    escape: (payloadData: { data: IWSPayload }) => {
      if (getState().attack?.attackStatus !== NOT_STARTED) {
        return
      }

      dispatch(spectatorUpdate({ payload: payloadData.data }))
    }
  })
}

export default initSpectatorWS
