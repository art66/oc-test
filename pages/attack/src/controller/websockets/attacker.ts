import createDistractionDebounce from '../../utils/createDistractionDebounce'

import { updateAggression, hitUpdate, escapeUpdate } from '../actions/attack'
import { IAggressionPayload } from '../../interfaces/IWebsockets'
import { IWSPayload } from '../../interfaces/IController'

const initAttackerWS = (getState, dispatch) => {
    // @ts-ignore global Websocket & Centrifuge handler!!!
    const handler = new WebsocketHandler('attack')
    const runDemolitionDebounce = createDistractionDebounce(dispatch, updateAggression)

    handler.setActions({
      defenderAggressionUpdated: (payload: IAggressionPayload) => {
        // WS update force stop in case the attacker have ongoing commit request.
        if (getState().attack?.preventWSUpdate) {
          return
        }

        runDemolitionDebounce()

        const { distraction, statsModifiers } = payload

        // statsModifiers should have one action-channel for update!!!
        dispatch(updateAggression({ distraction, statsModifiers }))
      },
      hit: (payloadData: { data: IWSPayload }) => {
        dispatch(hitUpdate({ payload: payloadData.data }))
      },
      join: (payloadData: { data: IWSPayload }) => {
        dispatch(hitUpdate({ payload: payloadData.data }))
      },
      escape: (payloadData: { data: IWSPayload }) => {
        dispatch(escapeUpdate({ payload: payloadData.data }))
      }
    })
}

export default initAttackerWS
