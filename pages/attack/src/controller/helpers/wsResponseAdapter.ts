import { IWSPayload } from '../../interfaces/IController'
import { IState } from '../../interfaces/IState'

const stateAdapter = (prevState: IState, newStateWS: IWSPayload): IState => {
  return {
    startButtonTitle: newStateWS.startButtonTitle || prevState.startButtonTitle,
    addAttackWinButton: newStateWS.addAttackWinButton,
    inProgress: prevState.inProgress,
    dataLoaded: prevState.dataLoaded,
    preventWSUpdate: prevState.preventWSUpdate,
    attackStatus: newStateWS.attackStatus || prevState.attackStatus,
    escapeAvailable: newStateWS.escapeAvailable || prevState.escapeAvailable,
    enablePoll: prevState.enablePoll,
    endResult: newStateWS.endResult,
    finishOptions: newStateWS.finishOptions,
    hitsLog: newStateWS.hitsLog || prevState.hitsLog,
    usersLife: newStateWS.life,
    attacking: newStateWS.attacking,
    currentItems: newStateWS.currentItems || prevState.currentItems,
    currentDefends: newStateWS.defends,
    currentFightStatistics: newStateWS.statistics,
    currentTemporaryEffects: newStateWS.effects,
    currentFightHistory: newStateWS.currentFightHistory,
    attackerUser: newStateWS.attacker || prevState.attackerUser,
    defenderUser: newStateWS.defender || prevState.defenderUser,
    defenderItems: newStateWS.items.defenderItems || prevState.defenderItems,
    attackerItems: newStateWS.items.attackerItems || prevState.attackerItems,
    defenderAmmo: newStateWS.ammo.defenderAmmoStatus || prevState.defenderAmmo,
    attackerAmmo: newStateWS.ammo.attackerAmmoStatus || prevState.attackerAmmo
  }
}

export default stateAdapter
