import {
  ATTACK_START,
  ATTACK_DONE,
  INIT_START,
  INIT_DONE,
  FIGHT_START_ATTEMPT,
  FIGHT_START_DONE,
  POLL_ATTEMPT,
  POLL_DONE,
  UPDATE_START,
  UPDATE_DONE,
  SET_ACTIVE_PLAYER_LAYOUT,
  FIGHT_FINISH_ATTEMPT,
  FIGHT_FINISH_DONE,
  ESCAPE_START,
  ESCAPE_DONE,
  SHOW_LOG_MODAL_ACTION,
  ERROR_MODAL,
  AGGRESSION_UPDATED,
  HIT_UPDATE,
  SPECTATOR_UPDATE,
  ESCAPE_UPDATE
  // DESTROY_HEALTH_DAMAGE
} from '../../constants'

import {
  IType,
  IFightStart,
  IAttackAttempt,
  IAttackDone,
  IInit,
  IPoll,
  IUpdate,
  ILayoutSelect,
  IFightFinishAttempt,
  IFightEnd,
  IEscapeAttempt,
  IEscapeDone,
  IShowModalLog,
  IErrorModal,
  IAggressionUpdate,
  IWSPayload
  // IDestroyHealthDamage
} from '../../interfaces/IController'

export const fightStartAttempt = (): IType => ({
  type: FIGHT_START_ATTEMPT
})

export const fightStartDone = (payload): IFightStart => ({
  payload,
  type: FIGHT_START_DONE
})

export const attackAttempt = (weaponID): IAttackAttempt => ({
  type: ATTACK_START,
  weaponID
})

export const attackDone = (payload): IAttackDone => ({
  payload,
  type: ATTACK_DONE
})

export const initLoadAttempt = (): IType => ({
  type: INIT_START
})

export const initLoadDone = (payload): IInit => ({
  payload,
  type: INIT_DONE
})

export const pollAttempt = (): IType => ({
  type: POLL_ATTEMPT
})

export const pollDone = (payload): IPoll => ({
  payload,
  type: POLL_DONE
})

export const updateAttempt = (): IType => ({
  type: UPDATE_START
})

export const updateDone = (payload): IUpdate => ({
  payload,
  type: UPDATE_DONE
})

export const setActivePlayerLayout = (layoutModel): ILayoutSelect => ({
  layoutModel,
  type: SET_ACTIVE_PLAYER_LAYOUT
})

export const fightFinishAttempt = ({ step, fightResult, options }): IFightFinishAttempt => ({
  step,
  fightResult,
  options,
  type: FIGHT_FINISH_ATTEMPT
})

export const fightFinishDone = (payload): IFightEnd => ({
  payload,
  type: FIGHT_FINISH_DONE
})

export const escapeAttempt = ({ step }): IEscapeAttempt => ({
  step,
  type: ESCAPE_START
})

export const escapeDone = (payload): IEscapeDone => ({
  payload,
  type: ESCAPE_DONE
})

export const showFightLogModal = ({ error }): IErrorModal => ({
  error,
  type: SHOW_LOG_MODAL_ACTION
})

export const endFightModal = ({ error }): IShowModalLog => ({
  error,
  type: ERROR_MODAL
})

export const updateAggression = ({ distraction, statsModifiers }): IAggressionUpdate => ({
  distraction,
  statsModifiers,
  type: AGGRESSION_UPDATED
})

export const hitUpdate = ({ payload }): { payload: IWSPayload } & IType => ({
  payload,
  type: HIT_UPDATE
})

export const spectatorUpdate = ({ payload }): { payload: IWSPayload } & IType => ({
  payload,
  type: SPECTATOR_UPDATE
})

export const escapeUpdate = ({ payload }): { payload: IWSPayload } & IType => ({
  payload,
  type: ESCAPE_UPDATE
})

// export const destroyHealthDamage = ({ userType }): IDestroyHealthDamage => ({
//   userType,
//   type: DESTROY_HEALTH_DAMAGE
// })
