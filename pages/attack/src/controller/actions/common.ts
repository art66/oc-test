import { DEBUG_MESSAGE, CHECK_MANUAL_DESKTOP_MODE, CHECK_DARK_MODE } from '../../constants'
import { IType, IDebug, ICheckDesktopMode, ICheckDarkMode } from '../../interfaces/IController'

export const showDebug = ({ debugMessage, options }: IDebug): IDebug & IType => ({
  debugMessage,
  options,
  type: DEBUG_MESSAGE
})

export const checkManualDesktopMode = (status): ICheckDesktopMode => ({
  status,
  type: CHECK_MANUAL_DESKTOP_MODE
})

export const checkDarkMode = (status): ICheckDarkMode => ({
  status,
  type: CHECK_DARK_MODE
})
