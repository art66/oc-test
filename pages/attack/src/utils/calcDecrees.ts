const calcDecrees = (iteration, decreesValue) => {
  if (iteration > 0) {
    const nextValue = decreesValue / 2
    const nextIteration = iteration - 1

    return calcDecrees(nextIteration, nextValue)
  }

  return decreesValue
}

export default (value: number) => -(100 - calcDecrees(value, 100))
