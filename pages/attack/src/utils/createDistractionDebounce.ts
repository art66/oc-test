import { debounce } from '@torn/shared/utils/debounce'

const createDistractionDebounce = (dispatch, updateAggression) => {
  const clearDistraction = () => {
    const distractionNullMock = {
      ID: 302,
      aggressiveMoves: null,
      class: 'distraction',
      desc: 'Defender stats decreased',
      title: 'Distraction',
      type: 3
    }

    dispatch(updateAggression({ distraction: distractionNullMock, statsModifiers: null }))
  }

  return debounce({ callback: clearDistraction, delay: 30000 })
}

export default createDistractionDebounce
