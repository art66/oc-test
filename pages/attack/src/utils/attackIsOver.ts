const attackIsOver = (attackStatus: string): boolean => attackStatus === 'end'

export default attackIsOver
