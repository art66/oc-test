import isValue from '@torn/shared/utils/isValue'

interface IAggression {
  lastAggressiveID: number
  aggressiveMoves: {
    [x: number]: number
  }
}

const getAggressionValue = ({ lastAggressiveID, aggressiveMoves }: IAggression) => {
  let aggressiveValue = 0

  if (!aggressiveMoves || Object.keys(aggressiveMoves).length === 0 || !isValue(lastAggressiveID)) {
    return aggressiveValue
  }

  Object.keys(aggressiveMoves).forEach(key => {
    if (Number(key) <= lastAggressiveID && lastAggressiveID !== 0) {
      return
    }

    aggressiveValue += aggressiveMoves[key]
  })

  return aggressiveValue
}

export default getAggressionValue
