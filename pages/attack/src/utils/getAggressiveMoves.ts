/**
 * @deprecated
 */
// import timestampToSeconds from './timestampToSeconds'

// const TIME_INTERVAL = 10

// const getAggressiveMoves = ({ lastTimestamp, list }) => {
//   const timestamp = Date.now()
//   const timePast = timestampToSeconds(timestamp - (lastTimestamp * 1000))

//   if (!list) {
//     return null
//   }

//   if (timePast <= TIME_INTERVAL) {
//     // console.log('WARNING! Collecting value because of the action commit in the last 10 sec!!!')

//     return list.reduce((sum: number, item: any) => {
//       const timePastSinceEffect = timestampToSeconds(timestamp - (item.timestamp * 1000))
//       const isActionOverlapAllowedTime = timePast - timePastSinceEffect <= 0

//       // console.log(item.timestamp, 'item original timestamp')
//       // console.log(timePastSinceEffect, 'item time past since last commit')
//       // console.log(isActionOverlapAllowedTime, 'check is item time overlapping last timestamp')
//       // console.log(sum, 'sum reduce value after iteration')

//       return !isActionOverlapAllowedTime ? sum + item.total : sum + 0
//     }, 0)
//   }

//   return list.reduce((sum: number, item: any) => sum + item.total, 0)
// }

// export default getAggressiveMoves
