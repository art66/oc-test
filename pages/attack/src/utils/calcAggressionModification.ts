import isValue from '@torn/shared/utils/isValue'

import calcDecrees from './calcDecrees'

interface IAggression {
  value: number
  aggressiveRatio: number
  isSight?: boolean
  isPercentage?: boolean
}

const calcAggressionModification = ({ value, aggressiveRatio, isSight, isPercentage }: IAggression): number => {
  if (!isValue(aggressiveRatio)) {
    return value
  }

  // pre-processing configuration
  const decrees = calcDecrees(aggressiveRatio)

  const amplifyValue = value / 100 + 1
  const decreesValue = decrees / 100 + 1

  const result = (decreesValue * amplifyValue) * 100
  const postResult = result - 100

  const normalizedPercentageValue = isPercentage ? Number(postResult.toFixed(2)) : Number(postResult.toFixed(0))
  const normalizeSightValue = isSight ? normalizedPercentageValue : Math.abs(normalizedPercentageValue)

  return normalizeSightValue
}

export default calcAggressionModification
