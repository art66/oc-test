interface IProps {
  prevStatus: string,
  nextStatus: string,
  endResult: object,
  error?: string
}

const newAttackStartChecker = ({ prevStatus, nextStatus, endResult = {}, error }: IProps) => {
  const isPrevAttackStatusNotInProgress = prevStatus === 'end' || prevStatus === 'started'
  const isNextAttackStatusReadyToStart = nextStatus === 'notStarted'
  const isNotAlreadyModal = endResult && Object.keys(endResult).length === 0

  const isNewAttackCanStart = isPrevAttackStatusNotInProgress && isNextAttackStatusReadyToStart && isNotAlreadyModal && !error

  return isNewAttackCanStart
}

export default newAttackStartChecker
