import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'

import { IDistraction } from '../interfaces/IWebsockets'

interface IAggression {
  currentTemporaryEffects: object
  distraction: IDistraction
  defenderID: number
}

const aggressionUpdate = ({ currentTemporaryEffects, distraction, defenderID }: IAggression) => {
  const prevDefenderEffects = currentTemporaryEffects && currentTemporaryEffects[defenderID] || []

  const findAggressionAmongEffects = effect => (effect.class === distraction.title)
  const isAggressionEffectAlreadyExist = prevDefenderEffects.some(findAggressionAmongEffects)

  const updateAggressionAmongEffects = () => prevDefenderEffects.map(effect => (findAggressionAmongEffects(effect) ? {
    ...effect,
    effectsModifiers: {
      aggressiveMoves: distraction.aggressiveMoves
    }
  } : effect))

  const addAggressionToEffects = () => ([
    ...prevDefenderEffects,
    {
      ID: distraction.ID,
      effectID: distraction.ID,
      title: firstLetterUpper({ value: distraction.title }),
      class: distraction.title,
      type: distraction.type,
      desc: distraction.desc,
      effectsModifiers: {
        aggressiveMoves: distraction.aggressiveMoves
      }
    }
  ])

  return isAggressionEffectAlreadyExist ? updateAggressionAmongEffects() : addAggressionToEffects()
}

export default aggressionUpdate
