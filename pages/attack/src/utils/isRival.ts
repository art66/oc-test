import { RIVAL } from '../constants'

const isRival = (personType: string) => personType === RIVAL

export default isRival
