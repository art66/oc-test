const ATTACK_IMAGES_LIST = [
  '/images/v2/attack/effects_v3.png',
  '/images/v2/attack/missOld.svg',
  '/images/v2/attack/miss.svg',
  '/images/v2/attack/hit.svg',
  '/images/v2/attack/hitOld.svg',
  '/images/v2/attack/hits.svg',
  '/images/v2/attack/log_icons_sprite.svg',
  '/images/v2/attack/attack_sprite_bonus_and_attachment.svg',
  '/images/v2/attack/accuracy_damage_life_stealth.svg',
  '/images/v2/attack/attack_damage.svg',
  '/images/v2/attack/effect_icons/crippled.svg',
  '/images/v2/attack/effect_icons/gassed.svg',
  '/images/v2/attack/effect_icons/demoralized.svg',
  '/images/v2/attack/effect_icons/smoked.svg',
  '/images/v2/attack/effect_icons/stunned.svg',
  '/images/v2/attack/effect_icons/sleeping.svg',
  '/images/v2/attack/effect_icons/blinded.svg',
  '/images/v2/attack/effect_icons/distraction_1.svg',
  '/images/v2/attack/effect_icons/distraction_2.svg',
  '/images/v2/attack/effect_icons/distraction_3.svg',
  '/images/v2/attack/effect_icons/distraction_4.svg',
  '/images/v2/attack/effect_icons/distraction_5.svg',
  '/images/v2/attack/effect_icons/distraction_6.svg',
  '/images/v2/attack/effect_icons/distraction_7.svg',
  '/images/v2/attack/effect_icons/distraction_8.svg',
  '/images/v2/attack/effect_icons/distraction_10.svg',
  '/images/v2/attack/effect_icons/maced.svg',
  '/images/v2/attack/effect_icons/weakend.svg',
  '/images/v2/attack/effect_icons/chilled.svg',
  '/images/v2/attack/effect_icons/frozen.svg',
  '/images/v2/attack/effect_icons/hardened.svg',
  '/images/v2/attack/effect_icons/severe_burning.svg',
  '/images/v2/attack/effect_icons/eviscerated.svg',
  '/images/v2/attack/effect_icons/poisoned.svg',
  '/images/v2/attack/effect_icons/motivated.svg',
  '/images/v2/attack/effect_icons/hastened.svg',
  '/images/v2/attack/effect_icons/strengthened.svg',
  '/images/v2/attack/effect_icons/lacerated.svg',
  '/images/v2/attack/effect_icons/contaminated.svg',
  '/images/v2/attack/effect_icons/loot_1.svg',
  '/images/v2/attack/effect_icons/loot_2.svg',
  '/images/v2/attack/effect_icons/loot_3.svg',
  '/images/v2/attack/effect_icons/loot_4.svg',
  '/images/v2/attack/effect_icons/loot_5.svg',
  '/images/v2/attack/effect_icons/hazardous.svg',
  '/images/v2/attack/effect_icons/bleeding.svg',
  '/images/v2/attack/effect_icons/storage.svg',
  '/images/v2/attack/effect_icons/withered.svg',
  '/images/v2/attack/effect_icons/slowed.svg',
  '/images/v2/attack/effect_icons/paralyzed.svg',
  '/images/v2/attack/effect_icons/burning.svg',
  '/images/v2/attack/effect_icons/emasculated.svg',
  '/images/v2/attack/effect_icons/disoriented.svg',
  '/images/v2/attack/effect_icons/spray.svg',
  '/images/v2/attack/effect_icons/concussed.svg',
  '/images/v2/attack/effect_icons/sharpened.svg',
  '/images/v2/attack/effect_icons/supressed.svg',
  '/images/v2/attack/effect_icons/blindfire.svg'
]

const getBlobData = (formData) => {
  const requestData = {}

  try {
    formData.forEach((value, key) => {
      requestData[key] = value
    })
  } catch (e) {
    console.error('Something wrong with data: ', formData)
  }

  return requestData
}

const deleteOldCache = (caches, cache) => Promise.all(cache.map(cacheName => caches.delete(cacheName)))
const imagesListToLoad = () => ATTACK_IMAGES_LIST.map(imageID => imageID)

self.addEventListener('install', event => {
  self.skipWaiting()

  event.waitUntil(caches.keys().then(cacheNames => deleteOldCache(caches, cacheNames)))
})

self.addEventListener('activate', event => {
  event.waitUntil(
    caches.open('attack-images').then(cache => {
      try {
        cache.addAll(imagesListToLoad())
      } catch (e) {
        console.error(`Error during parsing image. Error: ${e}`)
      }
    })
  )
})

self.addEventListener('fetch', event => {
  const { request = {} } = event

  // some strange exceptions...
  const googleAllow = /(analytics)/.test(event.request.url)
  const bingAllow = /(bing)/.test(event.request.url)
  const doubleclickAllow = /(doubleclick)/.test(event.request.url)
  // console.log(request, request.destination

  const isNotAnAttackImage = request.destination !== 'image' || !/(images\/v2\/attack)/i.test(request.url)

  if (isNotAnAttackImage || googleAllow || bingAllow || doubleclickAllow) {
    return
  }

  // load real data if cache is missed
  const loadRealImagesData = () => {
    return fetch(event.request)
      .then(response => {
        const contentType = getBlobData(response.headers)['content-type']

        if (!/(img)/i.test(contentType)) {
          return response
        }

        return caches.open('attack-images').then(cache => {
          cache.put(event.request, response.clone())

          return response
        })
      })
      .catch(e => console.error(`Some error during fetch happen in WS: ${e}`))
  }

  event.respondWith(
    caches.match(event.request).then(resp => {
      return resp || loadRealImagesData()
    })
  )
})
