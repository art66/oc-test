import { connect } from 'react-redux'
import Distraction from '../components/EffectIcons/Distraction'

const mapStateToProps = ({ attack }) => ({
  lastAggressiveID: attack.attackerUser.lastAction,
  statsModifiersDefender: (attack.defenderUser && attack.defenderUser.statsModifiers) || {}
})

export default connect(mapStateToProps, null)(Distraction)
