// @ts-nocheck
import { connect } from 'react-redux'
import { attackAttempt } from '../controller/actions/attack'
import Weapons from '../components/Weapons'
import getWeaponsSelector from '../controller/selectors/getWeaponsSelector'

const makeMapStateToProps = () => {
  const weapons = getWeaponsSelector()

  return (state, props) => {
    return {
      weapons: weapons(state, props),
      lockedWeapon: state.attack.lockedWeapon,
      attackStatus: state.attack.attackStatus
    }
  }
}

const mapActionsToProps = {
  attack: attackAttempt
}

export default connect(
  makeMapStateToProps,
  mapActionsToProps
)(Weapons)
