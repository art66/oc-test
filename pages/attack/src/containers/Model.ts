// @ts-nocheck
import { connect } from 'react-redux'
import Model from '../components/Model'

import getBulletsSelector from '../controller/selectors/getBulletsSelector'
import getEffectsSelector from '../controller/selectors/getEffectsSelector'
import getGenderSelector from '../controller/selectors/getGenderSelector'
// import getArmourSelector from '../controller/selectors/getArmourSelector'
import getModelStatusSelector from '../controller/selectors/getModelStatusSelector'

const makeMapStateToProps = () => {
  const bullets = getBulletsSelector()
  const effects = getEffectsSelector()
  const gender = getGenderSelector()
  // const armour = getArmourSelector()
  const status = getModelStatusSelector()

  return (state, props) => {
    return {
      bullets: bullets(state, props),
      effects: effects(state, props),
      gender: gender(state, props),
      // armour: armour(state, props), // operated by shared module <UserModel />
      status: status(state, props)
    }
  }
}

export default connect(makeMapStateToProps)(Model)
