// @ts-nocheck
import { connect } from 'react-redux'
import PlayerBoxHeader from '../components/PlayerBoxHeader'
import getHeaderSelector from '../controller/selectors/getHeaderSelector'
import { setActivePlayerLayout } from '../controller/actions/attack'

import isRival from '../utils/isRival'

const makeMapStateToProps = () => {
  const header = getHeaderSelector()

  return (state, props) => {
    return {
      ...header(state, props),
      color: !isRival(props.userType) ? 'green' : 'rose',
      mediaType: state.browser.mediaType,
      userType: props.userType,
      activePlayerLayout: state.activePlayerLayout.activePlayerLayout,
      isDesktopManualLayout: state.common.isDesktopManualLayout
    }
  }
}

const mapActionsToProps = {
  setActivePlayerLayout
}

export default connect(
  makeMapStateToProps,
  mapActionsToProps
)(PlayerBoxHeader)
