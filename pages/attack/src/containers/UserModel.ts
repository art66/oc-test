import { connect } from 'react-redux'

import UserModel from '../components/ModelLayers/Model'

const mapStateToProps = state => ({
  isDarkMode: state.common.isDarkModeEnabled,
  attackerArmour: state.attack.attackerItems,
  defenderArmour: state.attack.defenderItems,
  presets: state.attack.presets
})

export default connect(mapStateToProps, null)(UserModel)
