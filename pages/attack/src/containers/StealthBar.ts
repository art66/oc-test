import { connect } from 'react-redux'
import StealthBar from '../components/StealthBar'

const mapStateToProps = state => ({
  level: Number(state.attack?.stealthLevel || 10),
  mediaType: state.browser.mediaType,
  isDesktopManualLayout: state.common.isDesktopManualLayout
})

export default connect(mapStateToProps)(StealthBar)
