import { connect } from 'react-redux'
import getParticipants from '../controller/selectors/getParticipants'
import Stats from '../components/Stats'

const mapStateToProps = state => ({
  participants: getParticipants(state)
})

export default connect(mapStateToProps)(Stats)
