import { connect } from 'react-redux'
import { fightFinishAttempt } from '../controller/actions/attack'
import status from '../controller/selectors/status'
import Dialog from '../components/Dialog'

const mapStateToProps = state => ({
  addAttackWinButton: state.attack.addAttackWinButton,
  endResult: state.attack.endResult,
  labels: state.attack.finishOptions,
  loading: state.attack.inProgress,
  logID: state.attack.logID,
  status: status(state),
  isError: state.attack.isModalError
})

const mapActionsToProps = {
  endFight: fightFinishAttempt
}

const goToLogButton = (logID: number) => ({
  label: 'CONTINUE',
  action: () => {
    window.location.href = '/loader.php?sid=attackLog&ID=' + logID
  }
})

// create exceptions for hosp -> {action: hosp, label: hospitalize}
// and loot -> {action: something complex, label: loot}
const getButtonsProps = (label: string, endFight: (config: { fightResult?: string, options?: any }) => void) => {
  let tempLabel = label

  let action = () => endFight({ fightResult: tempLabel })

  if (tempLabel === 'hosp') {
    tempLabel = 'hospitalize'
    action = () => endFight({ fightResult: 'hosp' })
  } else if (tempLabel === 'loot') {
    action = () => endFight({ fightResult: tempLabel, options: { fightResult: 'hosp', action: 17 } })
  }

  return { label, action }
}

const mergeProps = ({ addAttackWinButton, endResult, labels, logID, loading, isError }, { endFight }, { title }: { title?: string }) => {
  let red = false
  let tempTitle = title

  const buttons = []

  if (endResult) {
    red = endResult.outcome !== 'pos'
    tempTitle = endResult.info || endResult.groupEvent

    buttons.push(goToLogButton(logID))
  } else if (labels) {
    labels.forEach((label: string) => buttons.push(getButtonsProps(label, endFight)))

    // add aditional buttons like kiss
    if (addAttackWinButton) {
      buttons.push({
        label: addAttackWinButton.text,
        action: () => endFight({
          fightResult: 'leave',
          options: {
            action: addAttackWinButton.winFightAction
          }
        })
      })
    }
  } else if (logID) {
    buttons.push(goToLogButton(logID))
  }

  return { title: tempTitle, buttons, red, loading, isError }
}

export default connect(
  mapStateToProps,
  mapActionsToProps,
  mergeProps
)(Dialog)
