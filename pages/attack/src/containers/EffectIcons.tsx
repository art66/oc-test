import { connect } from 'react-redux'
import isEmpty from '@torn/shared/utils/isEmpty'

import EffectIcons from '../components/EffectIcons'
import isRival from '../utils/isRival'

const mapStateToProps = ({ attack }, ownProps) => ({
  ...ownProps,
  effects: isRival(ownProps.userType) && !isEmpty(attack.distraction) ?
    [
      ...ownProps.effects,
      attack.distraction
    ] :
    ownProps.effects
})

export default connect(mapStateToProps, null)(EffectIcons)
