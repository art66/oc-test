import { connect } from 'react-redux'
import Overlay from '../components/Overlay'
import status from '../controller/selectors/status'

const mapStateToProps = state => ({
  status: status(state),
  error: state.attack.error,
  mediaType: state.browser.mediaType,
  showEnemyItems: state.attack.showEnemyItems,
  isDesktopManualLayout: state.common.isDesktopManualLayout,
  isError: state.attack.isModalError
})

export default connect(mapStateToProps)(Overlay)
