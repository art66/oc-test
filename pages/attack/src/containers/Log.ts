import { connect } from 'react-redux'
import Log from '../components/Log'

const mapStateToProps = state => ({
  logs: state.attack.currentFightHistory || [],
  mediaType: state.browser.mediaType,
  isDesktopManualLayout: state.common.isDesktopManualLayout
})

export default connect(
  mapStateToProps,
  null
)(Log)
