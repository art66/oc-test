import { connect } from 'react-redux'
import IndicatorFacilities from '../components/IndicatorFacilities'

const mapStateToProps = ({ attack }) => ({
  statsModifiersAttacker: attack.attackerUser?.statsModifiers || {},
  statsModifiersDefender: attack.defenderUser?.statsModifiers || {},
  lastAggressiveID: attack.attackerUser?.lastAction,
  defenderUserID: attack.defenderUser?.userID,
  distraction: attack.distraction || {},
  showEnemyItems: attack.showEnemyItems,
  attackStatus: attack.attackStatus
})

export default connect(
  mapStateToProps,
  null
)(IndicatorFacilities)
