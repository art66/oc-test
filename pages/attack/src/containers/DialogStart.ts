import { connect } from 'react-redux'
import { fightStartAttempt } from '../controller/actions/attack'
import Dialog from '../components/Dialog'

const mapStateToProps = state => ({
  label: [state.attack.startButtonTitle || 'Start fight'],
  timerButton: state.attack.startButtonTimer,
  loading: state.attack.inProgress,
  isError: state.attack.isModalError
})

const mapActionsToProps = {
  startFight: fightStartAttempt
}

// mergeProps(stateProps, dispatchProps, ownProps): props
const mergeProps = ({ label, loading, timerButton }, { startFight: action }, ownProps) => {
  return { ...ownProps, loading, buttons: [{ label, action }], timerButton }
}

export default connect(
  mapStateToProps,
  mapActionsToProps,
  mergeProps
)(Dialog)
