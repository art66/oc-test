import { connect } from 'react-redux'
import { escapeAttempt } from '../controller/actions/attack'
import getUser from '../controller/selectors/getUser'
import status from '../controller/selectors/status'
import { RIVAL } from '../constants'
import ContentTitle from '../components/ContentTitle'

const mapStateToProps = state => {
  const { attack } = state

  return {
    currentMove: attack.currentMove,
    endResult: attack.endResult,
    escapeAvailable: attack.escapeAvailable && !attack.isFightFinished && !attack.error && !attack.finishOptions,
    loading: attack.escapeInProgress,
    status: status(state),
    timeLeft: attack.TimeChanged,
    userID: getUser(state, { userType: RIVAL }).userID
  }
}

const mapActionsToProps = {
  endFight: escapeAttempt
}

export default connect(mapStateToProps, mapActionsToProps)(ContentTitle)
