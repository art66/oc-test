import React, { Component } from 'react'
import { connect } from 'react-redux'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'
import { subscribeOnDarkMode, unsubscribeFromDarkMode } from '@torn/shared/utils/darkModeChecker'

import { initLoadAttempt, pollAttempt, updateAttempt, fightFinishAttempt } from '../controller/actions/attack'
import { checkManualDesktopMode, checkDarkMode } from '../controller/actions/common'
import newAttackStartChecker from '../utils/newAttackStartChecker'

import CoreLayout from '../components/CoreLayout'
import AttackLayout from '../components/AttackLayout'
import Tooltip from '../components/Tooltip'
import ContentTitle from './ContentTitle'
import PlayerBoxHeader from './PlayerBoxHeader'
import Weapons from './Weapons'
import Model from './Model'
import Overlay from './Overlay'
import Log from './Log'
import Stats from './Stats'
import StealthBar from './StealthBar'

import { ME, RIVAL, POLL_DELAY } from '../constants'
import { IContainer } from '../interfaces/IContaner'

const playerRenderingProps = userType => ({
  header: () => <PlayerBoxHeader userType={userType} />,
  overlay: () => <Overlay userType={userType} />,
  weapons: () => <Weapons userType={userType} />,
  model: () => <Model userType={userType} />,
  stealthbar: () => <StealthBar userType={userType} />
})

class App extends Component<IContainer, { isEndTimerInRun: boolean }> {
  private _pollTimerID: any
  private _endTimerID: any

  static defaultProps: IContainer = {
    endFight: () => {},
    startPoll: () => {},
    startInitLoad: () => {},
    startUpdateData: () => {},
    runCheckManualDesktopMode: () => {},
    runDarkModeChecker: () => {},
    mediaType: 'desktop',
    debugInfo: null,
    loading: false,
    activePlayerLayout: '',
    attackStatus: '',
    isDesktopManualLayout: false,
    isAttack: false,
    lootable: false,
    endResult: {
      outcome: ''
    },
    isFightFinished: false,
    endTimerDelay: 30,
    isBDError: false,
    error: false,
    enablePoll: true
  }

  constructor(props: IContainer) {
    super(props)

    this.state = {
      isEndTimerInRun: false
    }
  }

  componentDidMount() {
    const { startInitLoad } = this.props

    startInitLoad()
    this._runPollTimer()
    this._checkManualDesktopMode()
    this._checkDarkMode()

    window.addEventListener('focus', this._onPageFocus)
  }

  shouldComponentUpdate() {
    return true
  }

  // eslint-disable-next-line max-statements
  componentDidUpdate(prevProps: IContainer) {
    const { isAttack, endResult, attackStatus, isFightFinished, isBDError, error, enablePoll } = this.props

    const isNewAttackCanStart = newAttackStartChecker({
      prevStatus: prevProps.attackStatus,
      nextStatus: attackStatus,
      endResult
    })

    if (attackStatus === 'end') {
      this._runEndTimer()
      window.removeEventListener('focus', this._onPageFocus)
    }

    if (isFightFinished || isNewAttackCanStart || isBDError || error) {
      this._removePollTimer()
      this._removeEndTimer()
      window.removeEventListener('focus', this._onPageFocus)
    }

    // TODO: just for dev purposes
    if (!enablePoll) {
      window.removeEventListener('focus', this._onPageFocus)
      this._pollTimerID && this._removePollTimer()

      return
    }

    if ((isAttack !== prevProps.isAttack) && !isAttack) {
      this._removePollTimer()
      this._runPollTimer()
    }
  }

  componentWillUnmount() {
    this._removePollTimer()
    this._removeEndTimer()
    unsubscribeOnDesktopLayout()
    unsubscribeFromDarkMode()
    window.removeEventListener('focus', this._onPageFocus)
  }

  _checkManualDesktopMode = () => {
    const { runCheckManualDesktopMode } = this.props

    subscribeOnDesktopLayout(payload => runCheckManualDesktopMode(payload))
  }

  _checkDarkMode = () => {
    const { runDarkModeChecker } = this.props

    subscribeOnDarkMode(payload => runDarkModeChecker(payload))
  }

  _onPageFocus = () => {
    const { startUpdateData } = this.props

    startUpdateData()
  }

  _runPollTimer = () => {
    const { startPoll } = this.props

    this._pollTimerID = setInterval(() => {
      startPoll()
    }, POLL_DELAY)
  }

  // automatic user redirecting timer on congratulation screen
  // if no actions are done during last 30 sec after win
  _runEndTimer = () => {
    const { isEndTimerInRun } = this.state
    const { endFight, endResult, lootable, isFightFinished, endTimerDelay } = this.props

    if (isFightFinished || lootable || isEndTimerInRun || endResult && endResult.outcome !== 'pos') {
      return
    }

    this.setState({
      isEndTimerInRun: true
    })

    this._endTimerID = setTimeout(() => {
      endFight({ fightResult: 'leave' })
    }, endTimerDelay * 1000)
  }

  _removePollTimer = () => {
    clearInterval(this._pollTimerID)
  }

  _removeEndTimer = () => {
    clearTimeout(this._endTimerID)
  }

  _startDataLoad = () => {
    const { startInitLoad } = this.props

    startInitLoad()
  }

  // this a custom font preloader just for the halloween event. Remove it once event will done.
  _renderHalloweenFontPreloader = () => {
    return (
      <i className='halloween_font_preloader' />
    )
  }

  render() {
    const { mediaType, debugInfo, loading, activePlayerLayout, isDesktopManualLayout } = this.props

    return (
      <CoreLayout contentTitle={<ContentTitle />} debugInfo={debugInfo}>
        <Tooltip />
        <AttackLayout
          me={playerRenderingProps(ME)}
          rival={playerRenderingProps(RIVAL)}
          log={<Log />}
          stats={<Stats />}
          mediaType={mediaType}
          loading={loading}
          activePlayerLayout={activePlayerLayout}
          isDesktopManualLayout={isDesktopManualLayout}
        />
        {this._renderHalloweenFontPreloader()}
      </CoreLayout>
    )
  }
}

const mapStateToProps = state => ({
  isAttack: state.attack.inProgress,
  mediaType: state.browser.mediaType,
  debugInfo: state.common.debugInfo,
  loading: !state.attack.dataLoaded,
  activePlayerLayout: state.activePlayerLayout.activePlayerLayout,
  attackStatus: state.attack.attackStatus,
  isDesktopManualLayout: state.common.isDesktopManualLayout,
  endResult: state.attack.endResult,
  isFightFinished: state.attack.isFightFinished,
  endTimerDelay: state.attack.TimeChanged,
  isBDError: state.attack.isModalError,
  error: !!state.attack.error,
  enablePoll: state.attack.enablePoll
})

const mapActionsToProps = {
  startInitLoad: initLoadAttempt,
  startPoll: pollAttempt,
  startUpdateData: updateAttempt,
  endFight: fightFinishAttempt,
  runCheckManualDesktopMode: checkManualDesktopMode,
  runDarkModeChecker: checkDarkMode
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(App)
