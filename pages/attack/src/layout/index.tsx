import React from 'react'
import { Provider } from 'react-redux'

import App from '../containers/App'

import { IProps } from './interfaces'
import '../styles/global.cssmodule.scss'

export class AppContainer extends React.Component<IProps> {
  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <App />
      </Provider>
    )
  }
}

export default AppContainer
