import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import AppContainer from './layout'
import configureStore from './store/configureStore'

import swRegisterer from '@torn/shared/utils/swRegisterer'

const store = configureStore()

const MOUNT_NODE = document.getElementById('react-root')

// ========================================================
// ServiceWorkers Setup
// ========================================================
swRegisterer({ sw: __DEV__ ? './attack_sw.js' : '/js/serviceWorkers/attack_sw.js', scope: '/loader.php?sid=attack' })

// ========================================================
// Render Setup
// ========================================================
const render = () => {
  ReactDOM.render(<AppContainer store={store} />, MOUNT_NODE)
}

// ========================================================
// Developer Tools Setup
// ========================================================
if (__DEV__) {
  // importing real sidebar app bundle only for developing stage
  window.state = store.getState()
}

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderError = (error: any) => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    const devRender = () => {
      try {
        render()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./layout/index', devRender)
  }
}

// ========================================================
// Go!
// ========================================================
render()
