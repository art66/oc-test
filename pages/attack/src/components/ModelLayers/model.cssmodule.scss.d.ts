// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'allLayers': string;
  'armour': string;
  'armoursWrap': string;
  'background': string;
  'backgroundImage': string;
  'bulletsWrap': string;
  'center': string;
  'globalSvgShadow': string;
  'hairMask': string;
  'itemImg': string;
  'mask': string;
  'model': string;
  'modelLayers': string;
}
export const cssExports: CssExports;
export default cssExports;
