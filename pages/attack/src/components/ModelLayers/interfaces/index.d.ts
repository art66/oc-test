import { IArmourRaw } from '@torn/shared/components/UserModel/interfaces'

export interface IArmor {
  ID: string
  path?: string
  slotsMapBackground: string
  maskID: number
  hairMaskID: number
}

export interface IBullets {
  bullets: {
    left: number
    top: number
    type: number
  }[]
}

export interface IEffects {
  effects: {
    ID: number | string
    class: string
    type: number
  }[]
}

export interface IGender {
  gender: '' | 'male' | 'female'
}

export interface IProps extends IGender, IBullets, IEffects {
  userType: string
}

export interface IArmourProps extends IGender {
  armour: IArmor[]
}

export interface IReducer {
  index: number
  masks: number[]
}

export interface IModel extends IGender {
  attackerArmour: IArmourRaw
  defenderArmour: IArmourRaw
  userType: string
  isDarkMode: boolean
}
