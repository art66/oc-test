import React from 'react'

import Model from '../../containers/UserModel'
import Bullet from '../Bullet'
import Effects from './Effects'

import s from './model.cssmodule.scss'
import { IProps } from './interfaces'

// using modelLayers to center effects and bullets to the model
class ModelLayers extends React.Component<IProps> {
  _renderModel = () => {
    const { gender, userType } = this.props

    return <Model gender={gender} userType={userType} />
  }

  _renderBullets = () => {
    const { bullets = [] } = this.props

    const bulletsToRender = bullets.map((bullet, i) => {
      const { left, top, type } = bullet

      return <Bullet {...bullet} key={`${left}_${top}_${type}_${i}`} />
    })

    return <div className={s.bulletsWrap}>{bulletsToRender}</div>
  }

  _renderEffectBackgrounds = () => {
    const { effects = [] } = this.props

    return <Effects effects={effects} />
  }

  render() {
    return (
      <div className={s.allLayers}>
        <div className={s.modelLayers}>
          {this._renderModel()}
          {this._renderBullets()}
        </div>
        {this._renderEffectBackgrounds()}
      </div>
    )
  }
}

export default ModelLayers

// The fresh version for SVG clipPath icon cropping!!!
// Active it once all work on the clipPaths figures by Janes will be done and optimized!
// import React, { PureComponent } from 'react'
// import PropTypes from 'prop-types'
// import classnames from 'classnames'

// import Bullet from '../Bullet'
// import EffectBackgrounds from '../Effect'

// import s from './model.cssmodule.scss'

// // using modelLayers to center effects and bullets to the model
// class ModelLayers extends PureComponent {
//   static propTypes = {
//     armour: PropTypes.array,
//     bullets: PropTypes.array,
//     effects: PropTypes.array,
//     gender: PropTypes.oneOf(['male', 'female'])
//   }

//   _getHairMask = () => {
//     const { armour } = this.props

//     const hairMask = {}

//     const findHairMask = () => {
//       armour.forEach(
//         (item, index) => {
//           const { ID: itemID = '', hairMaskID = '' } = item

//           if (itemID.includes('hair')) {
//             hairMask.index = index
//           }

//           if (hairMaskID) {
//             hairMask.hairMaskID = hairMaskID
//           }
//         }
//       )
//     }

//     findHairMask()

//     if ([undefined, null].includes(hairMask.index || hairMask.hairMaskID)) {
//       return null
//     }

//     return hairMask
//   }

//   _hairMask = currentItemIndex => {
//     // this is a legacy on way to deal with hair masks
//     // (it's a particular case only for hair). Should be refactored from back-end first.
//     const { gender } = this.props
//     const { index, hairMaskID } = this._getHairMask()

//     if (index !== currentItemIndex) {
//       return null
//     }

//     return `${gender}_${hairMaskID}`
//   }

//   _armourMask = (armourMaskID) => {
//     const { gender } = this.props

//     if (!armourMaskID) {
//       return null
//     }

//     return `${gender}_${armourMaskID}`
//   }

//   _checkMask = (itemIndex, maskID) => {
//     const hairMask = this._hairMask(itemIndex)
//     const armourMask = this._armourMask(maskID)

//     return hairMask || armourMask
//   }

//   _renderModel = () => {
//     const { gender } = this.props

//     return (
//       <div className={s.model}>
//         <img src={`/images/v2/attack/models/${gender}_model.png`} alt={`${gender} model`} />
//       </div>
//     )
//   }

//   _renderArmour = () => {
//     const { armour = [], gender } = this.props

//     const getItem = (itemID, maskID, itemIndex) => {
//       if (!itemID) return null

//       const mask = this._checkMask(itemIndex, maskID)

//       return (
//         <div className={s.armour}>
//           <img
//              className={`${s.itemImg}`}
//              src={`/getImage.php?ID=${itemID}&gender=${gender.charAt(0)}`}
//              style={{ opacity: 1, clipPath: `url(#${mask})` }}
//           />
//         </div>
//       )
//     }

//     const armourToRender = armour.map(({ ID, maskID }, i) => (
//       <div className='ammour_container' key={i} style={{ zIndex: 10 + i }}>
//         {getItem(ID, maskID, i)}
//       </div>
//     ))

//     return <div className={s.armoursWrap}>{armourToRender}</div>
//   }

//   _renderBullets = () => {
//     const { bullets = [] } = this.props

//     const bulletsToRender = bullets.map((bullet, i) => <Bullet {...bullet} key={i} />)

//     return <div className={s.bulletsWrap}>{bulletsToRender}</div>
//   }

//   _renderEffectBackgrounds = () => {
//     const { effects = [] } = this.props

//     const effectToRender = effects.map((effect, i) => <EffectBackgrounds {...effect} key={i} />)

//     return effectToRender
//   }

//   render() {

//     return (
//       <div className={s.allLayers}>
//         <div className={s.modelLayers}>
//           {this._renderModel()}
//           {this._renderArmour()}
//           {this._renderBullets()}
//         </div>
//         {this._renderEffectBackgrounds()}
//       </div>
//     )
//   }
// }

// export default ModelLayers
