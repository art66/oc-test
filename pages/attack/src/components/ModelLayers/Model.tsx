import React, { PureComponent } from 'react'
import UserModel from '@torn/shared/components/UserModel'

import isRival from '../../utils/isRival'

import { IModel as IProps } from './interfaces'

class Model extends PureComponent<IProps> {
  render() {
    const { userType, gender, attackerArmour, defenderArmour, isDarkMode } = this.props

    return (
      <UserModel
        equippedArmour={isRival(userType) ? defenderArmour : attackerArmour}
        gender={gender}
        withContainer={true}
        isDarkMode={isDarkMode}
        isCentered={true}
      />
    )
  }
}

export default Model
