/* eslint-disable react/destructuring-assignment */
import React from 'react'
import isEmpty from '@torn/shared/utils/isEmpty'

import TimerHOC from '../TimerHOC'
import EffectBackgrounds from '../Effect'

import { IEffects } from './interfaces'

import styles from './effects.cssmodule.scss'
import { IEffect } from '../../interfaces/IState'

class Effects extends React.Component<IEffects> {
  // this hook helps us remove duplicated effects
  // backgrounds which eventually should appear as a single image
  // covered by their own opacity effect based on slices count
  // instead of separated placed effects with independent opacity on each
  _withEffectsSlicerHook = (effects: IEffect[]) => {
    const withoutStacks = []

    const isAdded = (effect: IEffect) => withoutStacks.some((effectPure) => {
      const checkingType = effect.isStackable ? 'class' : 'ID'

      return effectPure[checkingType] === effect[checkingType]
    })

    effects.forEach((effect: IEffect) => !isAdded(effect) && withoutStacks.push(effect))

    return withoutStacks
  }

  _getSortedEffects = () => {
    const { effects = [] } = this.props

    if (isEmpty(effects)) {
      return null
    }

    // preventing redux store from mutations
    const tempEffects = [...effects]

    // laying the perks under the effects ones
    const effectsToRender = tempEffects.sort((first, second) => second.type - first.type)

    return effectsToRender
  }

  _renderEffects = (effects = []) => {
    if (isEmpty(effects)) {
      return null
    }

    return this._withEffectsSlicerHook(effects).map((effect) => {
      return <EffectBackgrounds {...effect} key={`${effect.ID}_${effect.class}`} />
    })
  }

  render() {
    const { effects = [] } = this.props

    if (isEmpty(effects)) {
      return null
    }

    const sortedEffects = this._getSortedEffects()

    return <div className={styles.effectsWrap}>{this._renderEffects(sortedEffects)}</div>
  }
}

export default (props: IEffects) => (
  <TimerHOC isEffects={true} effects={props.effects}>
    <Effects {...props} />
  </TimerHOC>
)
