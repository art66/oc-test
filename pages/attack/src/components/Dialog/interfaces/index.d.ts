import { IStartButtonTimer } from "../../../interfaces/IState";

export interface IButton {
  action: () => {},
  label: string
  disabled?: boolean
  btnClass?: string
}

export interface IButtons {
  loading: boolean,
  buttons: IButton[]
}

export interface IProps {
  buttons: IButton[],
  loading: boolean,
  title: string,
  red: string | boolean,
  isError: boolean
  timerButton?: IStartButtonTimer
}

export interface IState {
  timeLeft: number
  updateType: string
}
