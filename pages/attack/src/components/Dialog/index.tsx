import React from 'react'
import cn from 'classnames'

import LoadingIndicator from '../LoadingIndicator'
import Buttons from './Buttons'

import s from './dialog.cssmodule.scss'

import { IProps, IState } from './interfaces'

class Dialog extends React.Component<IProps, IState> {
  private _timerID: any

  static defaultProps: IProps = {
    buttons: [],
    timerButton: null,
    loading: false,
    title: '',
    red: false,
    isError: false
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      timeLeft: null,
      updateType: 'props'
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    if (nextProps.timerButton && prevState.timeLeft === null) {
      return {
        timeLeft: nextProps.timerButton?.timeLeft
      }
    }

    return null
  }

  componentDidMount() {
    this._runButtonTimerUpdate()
  }

  // TODO: need to optimize once have I time
  shouldComponentUpdate(prevProps: IProps, prevState: IState) {
    const { timeLeft } = this.state
    const { buttons, loading, red, isError } = this.props

    const isTimerUpdated = timeLeft !== prevState.timeLeft
    const isButtonChanged = JSON.stringify(buttons) !== JSON.stringify(prevProps.buttons)
    const isLoadingChanged = loading !== prevProps.loading
    const isRedAlertHappen = red !== prevProps.red
    const isErrorHappen = isError !== prevProps.isError

    if (isTimerUpdated || isButtonChanged || isLoadingChanged || isRedAlertHappen || isErrorHappen) {
      return true
    }

    return false
  }

  componentWillUnmount() {
    clearInterval(this._timerID)
  }

  _classNameHolder = () => {
    const { buttons, red, isError } = this.props

    const classNamesWrap = cn(
      s.colored,
      {
        [s.red]: red || isError,
        [s.orange]: buttons && buttons[0] && buttons[0].label === 'Join Fight'
      }
    )

    return classNamesWrap
  }

  _runButtonTimerUpdate = () => {
    const { timerButton } = this.props

    if (!timerButton) {
      return
    }

    this._timerID = setInterval(() => {
      const { timeLeft } = this.state

      if (timeLeft === 0) {
        clearInterval(this._timerID)

        return
      }

      this.setState(prevState => ({
        timeLeft: prevState.timeLeft - 1,
        updateType: 'state'
      }))
    }, 1000)
  }

  _renderButtons = () => {
    const { buttons, loading } = this.props

    return <Buttons buttons={buttons} loading={loading} />
  }

  _renderTimerButton = () => {
    const { timeLeft } = this.state
    const { buttons } = this.props

    return (
      <Buttons
        buttons={[
          {
            label: `${buttons[0].label} (${timeLeft})`,
            action: null,
            disabled: true,
            btnClass: s.btnTimer
          }
        ]}
        loading={null}
      />
    )
  }

  render() {
    const { timeLeft } = this.state
    const { title, loading, timerButton } = this.props
    const classNamesWrap = this._classNameHolder()

    const titleToRender = title || timerButton?.text || null
    let contentToRender = <LoadingIndicator />

    if (timeLeft) {
      contentToRender = this._renderTimerButton()
    } else if (!loading) {
      contentToRender = this._renderButtons()
    }

    return (
      <div className={s.dialog}>
        <div className={classNamesWrap}>
          <div className={s.title} dangerouslySetInnerHTML={{ __html: titleToRender }} />
          {contentToRender}
        </div>
      </div>
    )
  }
}

export default Dialog
