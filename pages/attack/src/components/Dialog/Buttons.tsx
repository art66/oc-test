import Button from '@torn/shared/components/ResponsiveButton'
import React from 'react'
import s from './dialog.cssmodule.scss'
import { IButtons } from './interfaces'

class Buttons extends React.PureComponent<IButtons> {
  _renderButtons = () => {
    const { buttons } = this.props

    return buttons.map(({ action, label, disabled, btnClass }) => (
      <Button key={`${JSON.stringify(label)}`} action={action} className={`${s.btn} ${btnClass}`} disabled={disabled}>
        {label}
      </Button>
    ))
  }

  render() {
    return <div className={s.dialogButtons}>{this._renderButtons()}</div>
  }
}

export default Buttons
