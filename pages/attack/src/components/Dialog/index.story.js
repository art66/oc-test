import React from 'react'
import Dialog from '.'

export const Win = () => <Dialog title="you won" buttons={[]} />

Win.story = {
  name: 'win'
}

export const Lose = () => <Dialog title="you lost" buttons={[]} red={true} />

Lose.story = {
  name: 'lose'
}

export default {
  title: 'Attack/Dialog'
}
