import React from 'react'
import cn from 'classnames/bind'

import s from './modal.cssmodule.scss'
import { IProps } from './interfaces'

class Modal extends React.PureComponent<IProps> {
  render() {
    const { children, userType, transparentBg } = this.props

    return (
      <div className={cn(s.modal, s[userType], { [s.transBg]: transparentBg })}>{children}</div>
    )
  }
}

export default Modal
