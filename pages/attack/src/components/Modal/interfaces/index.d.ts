import { ReactPortal } from 'react'

export interface IProps {
  children?: JSX.Element | Node & ReactPortal | Element | Node | string,
  userType: string,
  transparentBg: boolean | number
}
