import React from 'react'

import Carousel from '../Carousel'

import { IBottomProps as IProps } from './interfaces'
import s from './layout.cssmodule.scss'

class Bottom extends React.Component<IProps> {
  _renderPanelsWithCarousel = () => {
    const { log, stats, mediaType } = this.props

    return (
      <Carousel mediaType={mediaType}>
        <div className={s.log}>{log}</div>
        <div className={s.stats}>{stats}</div>
      </Carousel>
    )
  }

  _renderPanels = () => {
    const { log, stats } = this.props

    return (
      <React.Fragment>
        <div className={s.log}>{log}</div>
        <div className={s.stats}>{stats}</div>
      </React.Fragment>
    )
  }

  render() {
    const { isManualDesktopMode, mediaType } = this.props

    const isMobile = !isManualDesktopMode && (mediaType === 'phone' || mediaType === 'tablet')
    const contentToRender = isMobile ? this._renderPanelsWithCarousel() : this._renderPanels()

    return (
      <div className={s.logStatsWrap}>
        {contentToRender}
      </div>
    )
  }
}

export default Bottom
