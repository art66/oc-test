import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IModel {
  header: () => JSX.Element
  overlay: () => JSX.Element
  weapons: () => JSX.Element
  model: () => JSX.Element
  stealthbar: () => JSX.Element
}

export interface IProps {
  me: IModel
  rival: IModel
  log: JSX.Element
  stats: JSX.Element
  mediaType: TMediaType | 'phone'
  loading: boolean
  activePlayerLayout: string
  isDesktopManualLayout: boolean
}

export interface IBottomProps {
  log: JSX.Element
  stats: JSX.Element
  isManualDesktopMode?: boolean
  mediaType?: TMediaType | 'phone'
}
