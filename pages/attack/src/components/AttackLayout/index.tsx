import React, { PureComponent } from 'react'

import LoadingIndicator from '../LoadingIndicator'
import Bottom from './Bottom'

import s from './layout.cssmodule.scss'
import { MT } from '../../constants'
import { IProps } from './interfaces'
import isRival from '../../utils/isRival'

class AttackLayout extends PureComponent<IProps> {
  _renderMe = () => {
    const { me, rival } = this.props

    return (
      <div className={s.playerArea}>
        {me.overlay()}
        {me.weapons()}
        <div className={s.playerWindow}>
          {rival.model()}
          {me.stealthbar()}
        </div>
      </div>
    )
  }

  _renderRival = () => {
    const { me, rival } = this.props

    return (
      <div className={s.playerArea}>
        {rival.overlay()}
        {rival.weapons()}
        <div className={s.playerWindow}>
          {me.model()}
          {rival.stealthbar()}
        </div>
      </div>
    )
  }

  _renderStatPanels = () => {
    const { log, stats, mediaType, isDesktopManualLayout } = this.props

    return (
      <Bottom
        log={log}
        stats={stats}
        mediaType={mediaType}
        isManualDesktopMode={isDesktopManualLayout}
      />
    )
  }

  _processDesktopLayout = () => {
    const { me, rival } = this.props

    return (
      <div className={s.playersModelWrap}>
        <div className={s.players}>
          <div id='attacker' className={s.player}>
            {me.header()}
            <div className={s.playerArea}>
              {me.overlay()}
              {me.weapons()}
              <div className={s.playerWindow}>
                {me.model()}
                {me.stealthbar()}
              </div>
            </div>
          </div>
          <div id='defender' className={s.player}>
            {rival.header()}
            <div className={s.playerArea}>
              {rival.overlay()}
              {rival.weapons()}
              <div className={s.playerWindow}>{rival.model()}</div>
            </div>
          </div>
        </div>
        {this._renderStatPanels()}
      </div>
    )
  }

  _processTouchscreenLayout = () => {
    const { me, rival, activePlayerLayout } = this.props

    const currentUserID = isRival(activePlayerLayout) ? 'defender' : 'attacker'
    const currentModelLayout = isRival(activePlayerLayout) ? this._renderRival() : this._renderMe()

    return (
      <div id={currentUserID} className={s.playersModelWrap}>
        {me.header()}
        {rival.header()}
        {currentModelLayout}
        {this._renderStatPanels()}
      </div>
    )
  }

  render() {
    const { loading, mediaType, isDesktopManualLayout } = this.props

    if (loading) {
      return <LoadingIndicator />
    }

    if (mediaType === MT.DESKTOP || isDesktopManualLayout) {
      return this._processDesktopLayout()
    }

    return this._processTouchscreenLayout()
  }
}

export default AttackLayout
