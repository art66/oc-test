// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'bounceDot': string;
  'dot': string;
  'dot1': string;
  'dot2': string;
  'dot3': string;
  'dot4': string;
  'dots': string;
  'globalSvgShadow': string;
  'preloader': string;
}
export const cssExports: CssExports;
export default cssExports;
