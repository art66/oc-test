import React from 'react'

import { IProps } from './interfaces'
import s from './styles.cssmodule.scss'

class LoadingIndicator extends React.PureComponent<IProps> {
  render() {
    const { className } = this.props

    return (
      <div className={`${s.preloader} ${className}`}>
        <div className={s.dots}>
          <span className={s.dot1} />
          <span className={s.dot2} />
          <span className={s.dot3} />
          <span className={s.dot4} />
        </div>
      </div>
    )
  }
}

export default LoadingIndicator
