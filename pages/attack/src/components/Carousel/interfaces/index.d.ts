import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IProps {
  mediaType: TMediaType | 'phone'
  children: JSX.Element | JSX.Element[] | Element | Element[]
}

export interface IState {
  coordsXStart: number
  coordsYStart: number
  coordsXFinish?: number
  moveStarted?: boolean
  moveDirection?: 'left' | 'right' | ''
  carouselShift?: number
}

export type TMoveDirections = '' | 'left' | 'right'
export type TCarouselNativeEvents = MouseEvent & TouchEvent
export type TCarouselReactEvents = React.MouseEvent<HTMLDivElement> & React.TouchEvent<HTMLDivElement>
export type TCarouselWholeEvents = TCarouselNativeEvents | TCarouselReactEvents
