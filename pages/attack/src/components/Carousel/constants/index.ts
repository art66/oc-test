export const DIRECTIONS = {
  left: 'left',
  right: 'right'
}

export const SHIFTS = {
  phone: {
    left: '-330',
    right: '0'
  },
  tablet: {
    left: '-598',
    right: '0'
  }
}

export const ANIM_TIME = 300
