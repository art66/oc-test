import React from 'react'

import { DIRECTIONS, SHIFTS, ANIM_TIME } from './constants'

import {
  IProps,
  IState,
  TCarouselReactEvents,
  TCarouselNativeEvents,
  TMoveDirections,
  TCarouselWholeEvents
} from './interfaces'

const FIX_LEFT = -40
const FIX_RIGHT = 40

import styles from './index.cssmodule.scss'

class Carousel extends React.Component<IProps, IState> {
  private _ref: React.RefObject<HTMLDivElement>
  private _transitionTimerID: any

  constructor(props) {
    super(props)

    this.state = {
      coordsXStart: null,
      coordsYStart: null,
      moveDirection: '',
      carouselShift: 0
    }

    this._ref = React.createRef()
  }

  componentDidUpdate(prevProps: IProps) {
    const { mediaType } = this.props

    if (prevProps.mediaType !== mediaType) {
      this._updateShiftOnEnd()
    }
  }

  componentWillUnmount() {
    this._clearRemoveTransition()
  }

  _handleContextMenu = (e: TCarouselReactEvents) => {
    e.preventDefault()
    e.stopPropagation()
  }

  _handleDown = (e: TCarouselReactEvents) => {
    this._hackPreventDocumentVerticalScroll(true)
    this._clearRemoveTransition()
    this._setStartCoords(e)
    this._addHandleMove()
  }

  _handleMove = (e: TCarouselNativeEvents) => {
    if (this._isMovingNotStarted(e)) {
      return
    }

    this._preventTextSelectionOnTouchDevices(true, e)
    this._setMoveDirection(e)

    const wrapNode = this._getWrapNode()
    const shift = this._getDynamicShift(e)

    if (!wrapNode || !shift) {
      return
    }

    wrapNode.style.transform = `translateX(${shift}px)`
  }

  _handleUp = () => {
    this._updateShiftOnEnd()
    this._removeHandleMove()
    this._preventTextSelectionOnTouchDevices(false, null)
    this._hackPreventDocumentVerticalScroll(false)
  }

  _addHandleMove = () => {
    const wrapNode = this._getWrapNode()

    wrapNode.addEventListener('mousemove', this._handleMove)
    wrapNode.addEventListener('touchmove', this._handleMove)
  }

  _removeHandleMove = () => {
    const wrapNode = this._getWrapNode()

    wrapNode.removeEventListener('mousemove', this._handleMove)
    wrapNode.removeEventListener('touchmove', this._handleMove)
  }

  _updateShiftOnEnd = () => {
    if (this._isCarouselNotAllowed()) {
      return
    }

    this._makeTransition()
    this._makeShift()
    this._runRemoveTransition()
  }

  _setStartCoords = (e: TCarouselReactEvents) => {
    this.setState({
      coordsXStart: this._getCurrentCoordsX(e),
      coordsYStart: this._getCurrentCoordsY(e)
    })
  }

  _setShiftValue = (shift: number | string) => {
    this.setState({
      carouselShift: Number(shift) || 0
    })
  }

  _setMoveDirection = (e: TCarouselNativeEvents) => {
    const { coordsXStart } = this.state

    const { left, right } = DIRECTIONS
    const currentCoordsX = this._getCurrentCoordsX(e)

    const isLeft = coordsXStart > currentCoordsX
    const isRight = coordsXStart < currentCoordsX

    const moveDirection = isLeft && left || isRight && right || ''

    this.setState({
      moveDirection: (moveDirection as TMoveDirections)
    })
  }

  _getCurrentCoordsX = (e: TCarouselWholeEvents) => {
    return e.clientX || e.touches[0]?.clientX
  }

  _getCurrentCoordsY = (e: TCarouselWholeEvents) => {
    return e.clientY || e.touches[0]?.clientY
  }

  _getWrapNode = () => {
    // e && e.persist && e.persist()
    // return  (e.target as HTMLDivElement).closest(`[class^="${CAROUSEL_ID}"]`) as HTMLDivElement

    return this._ref?.current
  }

  _getMoveDirection = () => {
    const { moveDirection } = this.state

    return moveDirection
  }

  _getCoordsFix = () => {
    const { left, right } = DIRECTIONS
    const { moveDirection } = this.state

    const coordsFix = moveDirection === right ? FIX_LEFT : moveDirection === left ? FIX_RIGHT : 0

    return coordsFix
  }

  _getDynamicShift = (e: TCarouselNativeEvents) => {
    const { left, right } = DIRECTIONS

    const { coordsXStart, carouselShift, moveDirection } = this.state

    const { maxShiftRight, maxShiftLeft } = this._getMaxShifts()
    const currentShift = this._getCurrentCoordsX(e) - (Math.abs(coordsXStart)) + this._getCoordsFix()
    const dynamicShift = carouselShift + currentShift

    const isLeftBorderOverlapped = moveDirection === right && (dynamicShift > maxShiftRight)
    const isRightBorderOverlapped = moveDirection === left && (dynamicShift < maxShiftLeft)

    let shift = dynamicShift

    if (isLeftBorderOverlapped) {
      shift = maxShiftRight
    } else if (isRightBorderOverlapped) {
      shift = maxShiftLeft
    }

    return shift
  }

  _getSupremeShift = () => {
    const { left, right } = DIRECTIONS
    const moveDirection = this._getMoveDirection()
    const { maxShiftRight, maxShiftLeft } = this._getMaxShifts()

    const isMovingLeft = moveDirection === right
    const isMovingRight = moveDirection === left

    let shiftValue = 0

    if (isMovingRight) {
      shiftValue = maxShiftLeft
    } else if (isMovingLeft) {
      shiftValue = maxShiftRight
    }

    return shiftValue
  }

  _getMaxShifts = () => {
    const { mediaType } = this.props
    const { right, left } = DIRECTIONS

    const maxShiftRight = Number(SHIFTS[mediaType][right])
    const maxShiftLeft = Number(SHIFTS[mediaType][left])

    return {
      maxShiftRight,
      maxShiftLeft
    }
  }

  _isMovingNotStarted = (e: TCarouselWholeEvents) => {
    if (!e) {
      return
    }

    const { coordsXStart, /* coordsYStart */ } = this.state

    const coordsX = this._getCurrentCoordsX(e)
    const shiftX = Math.floor(coordsXStart) - Math.floor(coordsX)
    const isShiftXHappen = shiftX < FIX_LEFT || shiftX > FIX_RIGHT

    // const coordsY = this._getCurrentCoordsY(e)
    // const shiftY = Math.floor(coordsYStart) - Math.floor(coordsY)
    // const isShiftYHappen = shiftY < -3 || shiftY > 3

    return !isShiftXHappen
  }

  _isCarouselNotAllowed = () => {
    const { carouselShift } = this.state
    const { left, right } = DIRECTIONS

    const { maxShiftLeft, maxShiftRight } = this._getMaxShifts()
    const movingDirection = this._getMoveDirection()

    const onTheStart = movingDirection === right && carouselShift === maxShiftRight
    const onTheEnd = movingDirection === left && carouselShift === maxShiftLeft

    return onTheStart || onTheEnd
  }

  _makeTransition = () => {
    const wrapNode = this._getWrapNode()

    wrapNode.style.transition = `transform ${ANIM_TIME}ms ease-out`
  }

  _makeShift = () => {
    const wrapNode = this._getWrapNode()
    const shiftValue = this._getSupremeShift()

    wrapNode.style.transform = `translateX(${shiftValue}px)`

    this._setShiftValue(shiftValue)
  }

  _runRemoveTransition = () => {
    this._transitionTimerID = setTimeout(() => {
      const wrapNode = this._getWrapNode()

      wrapNode.style.transition = 'none'
    }, ANIM_TIME)
  }

  _clearRemoveTransition = () => {
    clearTimeout(this._transitionTimerID)
  }

  _preventTextSelectionOnTouchDevices = (isPrevent = true, e: TCarouselNativeEvents) => {
    const wrapNode = this._getWrapNode()
    const isCarouselStarted = this._isMovingNotStarted(e)

    if (isPrevent && !isCarouselStarted) {
      wrapNode.classList.add(styles.noSelect)
    } else {
      wrapNode.classList.remove(styles.noSelect)
    }
  }

  // TODO: should be improved in future. Just temporary solution!
  _hackPreventDocumentVerticalScroll = (isHide = true) => {
    if (isHide) {
      document.documentElement.classList.add(styles.noGlobalSelectScroll)
    } else {
      document.documentElement.classList.remove(styles.noGlobalSelectScroll)
    }
  }

  render() {
    const { children } = this.props

    return (
      <div
        tabIndex={0}
        role='button'
        ref={this._ref}
        className={styles.carousel}
        onContextMenu={this._handleContextMenu}
        onMouseDown={this._handleDown}
        onMouseUp={this._handleUp}
        onMouseLeave={this._handleUp}
        onTouchStart={this._handleDown}
        onTouchEnd={this._handleUp}
        style={{ transform: 'translateX(0)', transition: 'none' }}
      >
        {children}
      </div>
    )
  }
}

export default Carousel
