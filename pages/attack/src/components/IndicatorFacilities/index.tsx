import React from 'react'
import isEmpty from '@torn/shared/utils/isEmpty'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import { IProps, IFacility, ISightTypes } from './interfaces'

import styles from './index.cssmodule.scss'

import calcAggressionModification from '../../utils/calcAggressionModification'
import isRival from '../../utils/isRival'
import getAggressionValue from '../../utils/getAggressionValue'

const POSITIVE_SIGHT = '+'
const NEGATIVE_SIGHT = '-'
const SIGHT_TYPES: ISightTypes = {
  positive: {
    sight: POSITIVE_SIGHT,
    highlightClass: styles.increase
  },
  negative: {
    sight: NEGATIVE_SIGHT,
    highlightClass: styles.decrease
  },
  neutral: {
    sight: '+',
    highlightClass: ''
  }
}

const TOOLTIPS_CUSTOM_PRESET = {
  overrideStyles: {
    container: styles.tooltipContainer,
    title: styles.tooltipTitle,
    arrowWrap: styles.tooltipArrowWrap,
    tooltipArrow: styles.tooltipArrow
  },
  position: { x: 'center', y: 'top' },
  addArrow: true,
  manualCoodsFix: {
    fixX: -4,
    fixY: 0
  },
  animProps: {
    className: 'expTooltip',
    timeExit: 300,
    timeIn: 150
  }
}

const STATS_TO_AGGRESSIVE_INFLUENCE = ['dexterity', 'defense']

class IndicatorFacilities extends React.PureComponent<IProps> {
  componentDidMount() {
    this._mountTooltips()
  }

  // shouldComponentUpdate(prevProps: IProps) {
  //   if (JSON.stringify(prevProps) === JSON.stringify(this.props)) {
  //     return false
  //   }

  //   return true
  // }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _getTooltipsData = () => {
    if (this._isDefender()) {
      return []
    }

    const { userType } = this.props
    const currentModifiers = this._getCurrentModificators()
    const tooltipsDate = []

    Object.keys(currentModifiers).forEach((facilityKey) => {
      const { legend = [] } = currentModifiers[facilityKey]

      legend
        && legend.length > 0
        && tooltipsDate.push({
          ID: `${userType}_${facilityKey}`,
          child: legend.join('\n'),
          customConfig: TOOLTIPS_CUSTOM_PRESET
        })
    })

    return tooltipsDate
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltipsData(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltipsData(), type: 'update' })

  _getDefenderAggressiveRatio = () => {
    const { distraction, lastAggressiveID } = this.props

    if (isEmpty(distraction)) {
      return null
    }

    return getAggressionValue({ aggressiveMoves: distraction.aggressiveMoves, lastAggressiveID })
  }

  _updatedModificationsWithAggression = (statsModifications: object) => {
    const newModifications = {}

    const aggressiveRatio = this._getDefenderAggressiveRatio()
    const isAggressionPresent = aggressiveRatio > 0
    const isEffectAllowedToAggression = (key: string) => STATS_TO_AGGRESSIVE_INFLUENCE.includes(key)

    Object.keys(statsModifications).forEach((key: string) => {
      const { value, ...rest } = statsModifications[key]

      if (isEffectAllowedToAggression(key) && isAggressionPresent) {
        newModifications[key] = {
          ...rest,
          value: calcAggressionModification({ value, aggressiveRatio, isSight: true })
        }

        return
      }

      newModifications[key] = statsModifications[key]
    })

    return newModifications
  }

  _getCurrentIndicatorSight = (currentStat: number) => {
    const { positive, negative, neutral } = SIGHT_TYPES

    return (currentStat > 0 && positive) || (currentStat < 0 && negative) || neutral
  }

  _getCurrentModificators = () => {
    const { statsModifiersAttacker, statsModifiersDefender } = this.props

    if (this._isDefender()) {
      return this._updatedModificationsWithAggression(statsModifiersDefender)
    }

    return statsModifiersAttacker
  }

  _renderIndicators = () => {
    const currentModifiers = this._getCurrentModificators()

    return Object.keys(currentModifiers).map((facilityKey) => {
      const { value = 0 } = currentModifiers[facilityKey]
      const currentSightData = this._getCurrentIndicatorSight(value)

      return this._createIndicator({ name: facilityKey, value, sightData: currentSightData })
    })
  }

  _isDefender = () => {
    const { userType } = this.props

    return isRival(userType)
  }

  _checkDefenderLayout = () => {
    const { showEnemyItems, attackStatus } = this.props
    const isIndicatorsShowSpecialCases = attackStatus !== 'started' && !showEnemyItems

    return this._isDefender() && isIndicatorsShowSpecialCases
  }

  _createIndicator = ({ name, value, sightData }: IFacility) => {
    const { userType } = this.props
    const { sight, highlightClass } = sightData

    const normalizedValue = value < 0 ? String(value).substr(1, String(value).length) : value

    return (
      <div
        id={`${userType}_${name}`} key={name}
        className={`${styles.indicatorWrap} ${highlightClass}`}
      >
        <span className={styles.arrow} />
        <span className={styles.value}>
          {sight} {normalizedValue}%
        </span>
        <span className={styles.title}>{name}</span>
      </div>
    )
  }

  render() {
    if (this._checkDefenderLayout()) {
      return null
    }

    return (
      <div id='test' className={styles.indicatorsContainer}>
        {this._renderIndicators()}
      </div>
    )
  }
}

export default IndicatorFacilities
