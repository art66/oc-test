export interface ISightData {
  sight: '+' | '-' | ''
  highlightClass: string
}

export interface IFacility {
  name: string
  value: number
  sightData: ISightData
}

export interface IStat {
  value: number
  title: string
}

export interface IStatsModifiers {
  strength: IStat
  speed: IStat
  defence: IStat
  dexterity: IStat
}

export interface IProps {
  lastAggressiveID: number
  distraction: {
    aggressiveMoves: {
      [x: number]: number
    }
  }
  defenderUserID: number
  userType: string
  showEnemyItems: boolean
  attackStatus: string
  statsModifiersAttacker: IStatsModifiers
  statsModifiersDefender: IStatsModifiers
}

export interface ISightTypes {
  positive: ISightData
  negative: ISightData
  neutral: ISightData
}

export interface IState extends IStatsModifiers {
  updateType: 'state' | 'props' | ''
}
