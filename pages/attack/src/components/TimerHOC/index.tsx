import React from 'react'

import { IProps, IState } from './interfaces'

class TimerHOC extends React.PureComponent<IProps, IState> {
  private _intervalID: any
  constructor(props: IProps) {
    super(props)

    this.state = {
      updateType: ''
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    if (nextProps.isEffects) {
      return {
        effects: nextProps.effects
      }
    }

    if (nextProps.isTimeDown) {
      return {
        timeLeft: nextProps.timeLeft
      }
    }

    if (nextProps.isTimeUp) {
      return {
        timePassed: nextProps.timePassed
      }
    }

    return null
  }

  componentDidMount() {
    this._runEffectsTimeLeftTicker()
  }

  componentDidUpdate() {
    this._clearTimeLeftTicker()

    this._runEffectsTimeLeftTicker()
  }

  componentWillUnmount() {
    this._clearTimeLeftTicker()
  }

  _runEffectsTimeLeftTicker = () => {
    const { isTimeUp, isTimeDown, isEffects } = this.props

    this._intervalID = setInterval(() => {
      isTimeUp && this._runTimeUp()
      isTimeDown && this._runTimeDown()
      isEffects && this._runEffects()
    }, 1000)
  }

  _clearTimeLeftTicker = () => clearInterval(this._intervalID)

  _runEffects = () => {
    const { effects } = this.state

    const actionsPostTimers = effects && effects.map((action: any) => {
      if (action.timeLeft === null || action.timeLeft === undefined) {
        return action
      }

      return {
        ...action,
        timeLeft: action.timeLeft - 1
      }
    }) || []

    this.setState({
      updateType: 'state',
      effects: actionsPostTimers
    })
  }

  _runTimeUp = () => {
    const { timePassed } = this.state

    if (timePassed === null || timePassed === undefined) {
      return
    }

    this.setState({
      updateType: 'state',
      timePassed: timePassed + 1
    })
  }

  _runTimeDown = () => {
    const { timeLeft } = this.state

    if (timeLeft === null || timeLeft === undefined) {
      return
    }

    this.setState({
      updateType: 'state',
      timeLeft: timeLeft - 1
    })
  }

  render() {
    const { effects, timeLeft, timePassed } = this.state
    const { isEffects, isTimeUp, isTimeDown, children } = this.props

    if (!children) {
      return null
    }

    const clonePropKey = isEffects && 'effects' || isTimeDown && 'timeLeft' || isTimeUp && 'timePassed'
    const clonePropValue = isEffects && effects || isTimeDown && timeLeft || isTimeUp && timePassed

    return React.cloneElement(children, { [clonePropKey]: clonePropValue })
  }
}

export default TimerHOC
