import { IEffects } from '../../ModelLayers/interfaces'

export interface IProps {
  children: JSX.Element
  effects?: any
  timeLeft?: number
  timePassed?: number
  isTimeDown?: boolean
  isTimeUp?: boolean
  isEffects?: boolean
}

export interface IState {
  updateType: 'state' | ''
  effects?: any
  timeLeft?: number
  timePassed?: number
}
