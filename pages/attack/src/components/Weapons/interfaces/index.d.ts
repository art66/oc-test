export interface IProps {
  weapons: {
    ID: number
    boxTitle: string
    type: string
    active: boolean
    equipSlot: number | boolean
  }[]
  attack: (slot: number | boolean) => void
  lockedWeapon: number | boolean
  userType: string
  attackStatus: string
}

export interface IExtraProps {
  active?: boolean
  loading?: boolean
  userType: string
  className: string
  attackStatus: string
  tabIndex?: number
  weaponType: string
  action?: () => void
}
