import React from 'react'

import WeaponBox from '../WeaponBox'
import isRival from '../../utils/isRival'

import { IProps, IExtraProps } from './interfaces'
import s from './weapons.cssmodule.scss'

class Weapons extends React.PureComponent<IProps> {
  _getWeaponType = (index: number) => {
    const IDsHolder = {
      0: 'main',
      1: 'second',
      2: 'melee',
      3: 'temp',
      4: 'fists',
      5: 'boots'
    }

    return IDsHolder[index]
  }

  _renderWeaponsList = () => {
    const { weapons, attack, lockedWeapon, userType, attackStatus } = this.props

    const weaponsList = weapons.map((weapon, i) => {
      const addedProps: IExtraProps = {
        userType,
        weaponType: this._getWeaponType(i),
        className: weapon.type === 'full' ? s.weaponWrapper : s.weaponWrapperSmall,
        attackStatus
      }

      // include attack action only if action is available and request is finished
      if (weapon && weapon.active && !lockedWeapon) {
        addedProps.action = () => attack(weapon.equipSlot)
        addedProps.tabIndex = i
      }

      // disable hover while request is in progress
      if (!isRival(userType) && lockedWeapon) {
        addedProps.active = false

        // include loading flags only for me, for the right weapon
        if (lockedWeapon === weapon.equipSlot) {
          addedProps.loading = true
        }
      }

      return <WeaponBox {...weapon} {...addedProps} key={`${weapon.ID}_${weapon.boxTitle}`} />
    })

    return weaponsList
  }

  render() {
    return <div className={s.weaponList}>{this._renderWeaponsList()}</div>
  }
}

export default Weapons
