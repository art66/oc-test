import React from 'react'
import { mount } from 'enzyme'
import initialState from './mocks'
import Tooltip from '..'

import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

describe('<Tooltip />', () => {
  it('should render basic Tooltip Component', () => {
    const outerNode = document.createElement('div')
    document.body.appendChild(outerNode)

    tooltipSubscriber.subscribe({ child: initialState.title, ID: 'expTooltip' })

    const Component = mount(
      <div id='expTooltip'>
        <Tooltip {...initialState} />,
      </div>,
      { attachTo: outerNode }
    )

    expect(Component.find('Tooltip').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
})
