import React from 'react'
import TooltipGlobal from '@torn/shared/components/TooltipNew'
// import tooltipSubscriber from '@torn/shared/components/TooltipNew/utils/tooltipSubscriber'

import styles from './index.cssmodule.scss'
import './anim.cssmodule.scss'

const ANIM_PROPS = {
  className: 'expTooltip',
  timeExit: 0,
  timeIn: 0
}

const STYLES = {
  container: styles.tooltipContainer,
  title: styles.tooltipTitle,
  tooltipArrow: styles.tooltipArrow
}

class Tooltip extends React.PureComponent {
  render() {
    return (
      <TooltipGlobal
        overrideStyles={STYLES}
        position={{ x: 'left', y: 'center' }}
        animProps={ANIM_PROPS}
      />
    )
  }
}

export default Tooltip
