import React from 'react'
import cn from 'classnames'

import { IProps } from './interfaces'
import s from './styles.cssmodule.scss'
import isRival from '../../utils/isRival'

class StatusBar extends React.PureComponent<IProps> {
  render() {
    const { userType, imageSrc, result, damageValue = 0 } = this.props

    const c = cn({
      [s.result]: true,
      [s.red]: damageValue > 0 && !isRival(userType),
      [s.green]: damageValue > 0 && isRival(userType)
    })

    return (
      <div className={s.statusBarWrap}>
        <div className={s.weaponIcon}>{imageSrc && <img src={imageSrc} alt='' />}</div>
        <div className={c}>{`${result || ''} ${damageValue || ''}`}</div>
      </div>
    )
  }
}

export default StatusBar
