export interface IProps {
  damageValue?: number,
  imageSrc?: string,
  result?: string,
  userType: string
}
