export interface IProps {
  // armour: {
  //   ID: string
  //   hairItemID: string
  //   hairMaskID: number
  //   slotsMapBackground: string
  //   maskID: number
  // }[]
  bullets: {
    left: number
    top: number
    type: number
  }[]
  effects: {
    ID: number | string
    class: string
    type: number
  }[]
  gender: '' | '' | ''
  status: object
  userType: string
}
