import React from 'react'

import ModelLayers from '../ModelLayers'
import EffectIcons from '../../containers/EffectIcons'
import IndicatorFacilities from '../../containers/IndicatorFacilities'
import StatusBar from '../StatusBar'

import { IProps } from './interfaces'

class Model extends React.Component<IProps> {
  render() {
    const { bullets, effects, gender, status, userType } = this.props

    return (
      <>
        <EffectIcons effects={effects} userType={userType} />
        <ModelLayers userType={userType} bullets={bullets} effects={effects} gender={gender} />
        <IndicatorFacilities userType={userType} />
        <StatusBar {...status} userType={userType} />
      </>
    )
  }
}

export default Model
