import React, { Component } from 'react'
import PropTypes from 'prop-types'

import ProgressBar from '.'

class Control extends Component {
  static propTypes = {
    mitigated: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      value: 100
    }
  }

  shouldComponentUpdate() {
    return true
  }

  update = (event) => {
    this.setState({ value: Number(event.target.value) })
  }

  render() {
    const { mitigated } = this.props
    const { value } = this.state

    return (
      <div>
        <div
          style={{
            padding: '8px 10px 0',
            background: 'linear-gradient(to bottom, #f2f8eb 0, #d2e8bc 100%)',
            borderRadius: '5px 5px 0 0',
            overflow: 'hidden',
            height: '34px',
            boxSizing: 'border-box',
            fontSize: '12px',
            fontWeight: 'bold',
            color: '#666666',
            textShadow: '0px 1px 0px rgba(255, 255, 255, 0.65)',
            borderBottom: '1px solid #ccc'
          }}
        >
          <ProgressBar value={value} mitigatedValue={mitigated} />
        </div>
        <br />
        <br />
        <br />
        <input type="range" onChange={this.update} list="list" style={{ width: '200px' }} value={value} />
        <datalist id="list">
          <option value="0" />
          <option value="1" />
          <option value="15" />
          <option value="30" />
          <option value="50" />
          <option value="70" />
          <option value="100" />
        </datalist>
      </div>
    )
  }
}

export const Mitigated10 = () => <Control mitigated={10} />

Mitigated10.story = {
  name: 'mitigated 10'
}

export const NoMitigatedIEUsualBar = () => <Control />

NoMitigatedIEUsualBar.story = {
  name: 'no mitigated, i.e. usual bar '
}

export default {
  title: 'Attack/ProgressBar'
}
