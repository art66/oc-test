export interface IProps {
  // destroyHealthDamage: ({ userType }: { userType: 'defender' | 'attacker' }) => void
  mitigatedValue: number
  value: number
  className: string
  ID: string
  // mediaType: string
  // userType: 'defender' | 'attacker'
}

export interface IState {
  updateType: '' | 'state' | 'props'
  healthLeft: number
  healthRefill: number
  currentDamage: number
  mitigatedReduce: number
  mitigatedReduceNext: number
  isIncrease: boolean
  isDecrease: boolean
  isMitigated: boolean
}
