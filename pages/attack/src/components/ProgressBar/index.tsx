import React from 'react'
import classnames from 'classnames'
import { debounce } from '@torn/shared/utils/debounce'

import { IProps, IState } from './interfaces'

import styles from './styles.cssmodule.scss'

class ProgressBar extends React.PureComponent<IProps, IState> {
  private _runBarUpdate: any
  private _supportTimerID: any

  static defaultProps: IProps = {
    mitigatedValue: null,
    className: '',
    ID: '',
    value: null
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      updateType: '',
      healthLeft: null,
      healthRefill: null,
      currentDamage: null,
      mitigatedReduce: null,
      mitigatedReduceNext: null,
      isIncrease: false,
      isDecrease: false,
      isMitigated: false
    }
  }

  static getDerivedStateFromProps(nextProps: IProps, prevState: IState) {
    if (prevState.updateType === 'state') {
      return {
        updateType: ''
      }
    }

    // increasing health configuration
    if (nextProps.value > prevState.healthLeft && prevState.healthLeft !== null) {
      return {
        updateType: 'props',
        healthLeft: nextProps.value,
        healthRefill: nextProps.value - prevState.healthLeft,
        currentDamage: 0,
        mitigatedReduce: 0,
        mitigatedReduceNext: 0,
        isIncrease: true,
        isDecrease: false,
        isMitigated: false
      }
    }

    // decreasing health configuration
    if ((nextProps.value !== prevState.healthLeft) || (nextProps.mitigatedValue !== prevState.mitigatedReduceNext)) {
      const newDamage = prevState.currentDamage + (nextProps.value - prevState.healthLeft)

      return {
        updateType: 'props',
        healthLeft: nextProps.value,
        healthRefill: 0,
        currentDamage: prevState.healthLeft ? newDamage : 0,
        mitigatedReduce: nextProps.mitigatedValue || 0,
        mitigatedReduceNext: nextProps.mitigatedValue || 0, // use here to prevent updates,
        isIncrease: false,
        isDecrease: !!newDamage,
        isMitigated: !!nextProps.mitigatedValue
      }
    }

    return null
  }

  componentDidMount() {
    this._createProgressBarOnDebounce()
  }

  componentDidUpdate() {
    const { updateType, currentDamage, healthRefill } = this.state

    if (updateType === 'props' && currentDamage !== 0 || healthRefill !== 0) {
      this._runBarUpdate()
    }
  }

  componentWillUnmount() {
    this._runBarUpdate({ clearTimer: true })
    this._clearDebounce()
    this._destroyBarUpdate()
  }

  _createProgressBarOnDebounce = () => {
    this._runBarUpdate = debounce({
      callback: this._updateProgressBarOnDebounce,
      delay: 1000
    })
  }

  _updateProgressBarOnDebounce = () => {
    this.setState({
      updateType: 'state',
      currentDamage: 0,
      healthRefill: 0,
      isMitigated: false // mitigated flag should finish here because it has an independent anim logic
    })

    this._finishBarUpdate()
  }

  _finishBarUpdate = () => {
    this._supportTimerID = setTimeout(() => {
      this.setState({
        isDecrease: false,
        isIncrease: false,
        mitigatedReduce: 0 // mitigated value should finish here because it has an independent anim logic
      })
    }, 500)
  }

  _destroyBarUpdate = () => clearTimeout(this._supportTimerID)

  _clearDebounce = () => clearTimeout(this._runBarUpdate)

  _classNamesHolder = () => {
    const { healthRefill, currentDamage, mitigatedReduce, isDecrease, isMitigated } = this.state
    const { className } = this.props

    const containerClass = classnames({
      [className]: true,
      [styles.wrap]: true
    })

    const fullBarClass = classnames({
      [styles.progress]: true,
      [styles.progressOut]: isDecrease
    })

    const increaseBarClass = classnames({
      [styles.increase]: true,
      [styles.increaseIn]: healthRefill,
      [styles.increaseOut]: !healthRefill
    })

    const decreaseBarClass = classnames({
      [styles.pure]: true,
      [styles.pureIn]: currentDamage,
      [styles.pureOut]: !currentDamage
    })

    const mitigatedBarClass = classnames({
      [styles.mitigated]: true,
      [styles.mitigatedIn]: isMitigated && mitigatedReduce,
      [styles.mitigatedOut]: !isMitigated && mitigatedReduce
    })

    return {
      containerClass,
      fullBarClass,
      increaseBarClass,
      decreaseBarClass,
      mitigatedBarClass
    }
  }

  _renderHealthBar = () => {
    const { healthLeft } = this.state

    const { fullBarClass } = this._classNamesHolder()

    return (
      <div
        aria-label={`Progress: ${healthLeft.toFixed(2)}%`}
        className={fullBarClass}
        style={{ width: `${healthLeft.toFixed(2)}%` }}
      />
    )
  }

  _renderSupportHighlightBar = () => {
    const { healthLeft, currentDamage, isIncrease, isDecrease, healthRefill } = this.state

    const { decreaseBarClass, increaseBarClass } = this._classNamesHolder()

    let config = null

    if (isIncrease) {
      config = {
        'aria-label': `Refill: ${healthRefill}%`,
        className: increaseBarClass,
        style: {
          width: `${healthRefill.toFixed(2)}%`,
          left: `${(healthLeft - healthRefill).toFixed(2)}%`
        }
      }
    } else if (isDecrease) {
      config = {
        'aria-label': `Damage: ${currentDamage}%`,
        className: decreaseBarClass,
        style: {
          width: `${Math.abs(currentDamage).toFixed(2)}%`,
          left: `${healthLeft.toFixed(2)}%`
        }
      }
    }

    return (
      <div {...config} />
    )
  }

  _renderMitigatedValue = () => {
    const { healthLeft, mitigatedReduce } = this.state
    const { mitigatedBarClass } = this._classNamesHolder()

    return (
      <div
        className={mitigatedBarClass}
        style={{ width: `${mitigatedReduce.toFixed(2)}%`, left: `${(healthLeft - mitigatedReduce).toFixed(2)}%` }}
      />
    )
  }

  _renderMotionBar = () => {
    const { containerClass } = this._classNamesHolder()

    return (
      <div className={containerClass}>
        {this._renderHealthBar()}
        {this._renderSupportHighlightBar()}
        {this._renderMitigatedValue()}
      </div>
    )
  }

  render() {
    return this._renderMotionBar()
  }
}

export default ProgressBar
