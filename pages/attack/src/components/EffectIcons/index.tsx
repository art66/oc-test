import React from 'react'
import isEmpty from '@torn/shared/utils/isEmpty'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import TimerHOC from '../TimerHOC'

import { ICONS_LAYOUT } from './constants'
import { EFFECTS_TYPE_TRANSPILER } from '../../constants'

import { IProps, IEffect, IGeneralState, TIconLabels, TIconTypes } from './interfaces'

import styles from './index.cssmodule.scss'

const MAX_VERTICAL_EFFECTS_COUNT = 8

class EffectIcons extends React.Component<IProps, IGeneralState> {
  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _initializeTooltip = () => {
    const { effects = [] } = this.props

    if (isEmpty(effects)) {
      return []
    }

    const child = (title: string, desc: string) => {
      return (
        <div className={styles.tooltipText}>
          <span className={styles.tooltipTitle}>{title}</span>
          <span className={styles.tooltipDesc}>{desc}</span>
        </div>
      )
    }

    const customConfig = (type: number) => {
      return [3, 4].includes(type) ? {
        position: { x: 'right', y: 'center' }
      } : null
    }

    const effectsTooltips = []

    effects.forEach((effect: IEffect) => {
      const { title, desc, type } = effect

      if (title === 'Distraction') { // should run independently
        return
      }

      effectsTooltips.push({
        ID: title,
        child: child(title, desc),
        customConfig: customConfig(type)
      })
    })

    return effectsTooltips
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initializeTooltip(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initializeTooltip(), type: 'update' })

  _getIconType = (iconKey: TIconTypes) => EFFECTS_TYPE_TRANSPILER[iconKey]

  _getIconClass = (iconName: string) => {
    const namesAdaptation = {
      severe_burning: 'severeBurning'
    }

    return namesAdaptation[iconName] || iconName
  }

  _iconNotAllowedToAppear = (iconName: string) => {
    // we should populate this array in case there will be more icons to hide
    const RESTRICTED_ICONS_LIST = ['confetti']

    return RESTRICTED_ICONS_LIST.some(icon => icon === iconName)
  }

  _getIconComponent = (iconType: TIconLabels) => ICONS_LAYOUT[iconType]

  _prepareIconsArrays = () => {
    const { effects } = this.props

    if (isEmpty(effects)) {
      return null
    }

    // 1, 2 are the Effects. 3 for Loot. 4 for Distraction
    const actions = effects.filter(icon => [1, 2].includes(icon.type))
    const fortunes = effects.filter(icon => [3, 4].includes(icon.type))

    return {
      actions,
      fortunes
    }
  }

  _getSortedListByType = (iconsList: any) => {
    const tempEffects = [...iconsList]

    return tempEffects.sort((a, b) => Number(b.type) - Number(a.type))
  }

  // _getSortedListByTimestamp = (iconsList: any) => {
  //   const tempEffects = [...iconsList]

  //   return tempEffects.sort((a, b) => Number(b.timestamp) - Number(a.timestamp))
  // }

  _renderIcons = (iconsList: any) => {
    const iconsToRender = iconsList
      .map(effect => {
        const iconType = this._getIconType(effect.type)
        const iconClass = effect.class
        const getNormalizedClass = this._getIconClass(effect.class)
        const componentToRender = this._getIconComponent(iconType as TIconLabels)

        if (!componentToRender || this._iconNotAllowedToAppear(iconClass)) {
          return null
        }

        const { component: IconComponent, icons } = componentToRender
        const { color = '', dimensions = {} } = icons[getNormalizedClass] || {}

        return (
          <IconComponent
            {...effect}
            class={iconClass}
            key={`${effect.ID}_${effect.class}`}
            iconType={iconType}
            colorScheme={color}
            dimensions={dimensions}
          />
        )
      })

    return iconsToRender
  }

  _renderLeftIconsSection = () => {
    const { fortunes } = this._prepareIconsArrays() || {}

    if (isEmpty(fortunes)) {
      return null
    }

    const iconsToRender = this._getSortedListByType(fortunes)

    return (
      <div className={`${styles.scrollIconsWrap} ${styles.scrollLeft}`}>
        <div className={`${styles.iconsWrap} ${iconsToRender?.length > MAX_VERTICAL_EFFECTS_COUNT ? styles.withoutScroll : ''}`}>{this._renderIcons(iconsToRender)}</div>
      </div>
    )
  }

  _renderRightIconsSection = () => {
    const { actions = [] } = this._prepareIconsArrays() || {}

    if (isEmpty(actions)) {
      return null
    }

    // const iconsToRender = this._getSortedListByTimestamp(actions)

    return (
      <div className={`${styles.scrollIconsWrap} ${styles.scrollRight}`}>
        <div className={`${styles.iconsWrap} ${actions?.length > MAX_VERTICAL_EFFECTS_COUNT ? styles.withoutScroll : ''}`}>{this._renderIcons(actions)}</div>
      </div>
    )
  }

  render() {
    const { effects } = this.props

    if (isEmpty(effects)) {
      return null
    }

    return (
      <div className={styles.iconsContainer}>
        {this._renderLeftIconsSection()}
        {this._renderRightIconsSection()}
      </div>
    )
  }
}

export default (props: IProps) => (
  <TimerHOC isEffects={true} effects={props.effects}>
    <EffectIcons {...props} />
  </TimerHOC>
)
