import React from 'react'

import Icon from './Icon'
import { IPerkProps } from './interfaces'
import styles from './index.cssmodule.scss'

const Perk = (props: IPerkProps) => {
  const { colorScheme, timestamp, title } = props

  const getTimeProgress = () => (title === 'Loot 5' ? 100 : timestamp)

  return (
    <div className={`${styles.iconWrap} ${styles[colorScheme]} ${styles.showup}`}>
      <Icon isLeft={true} {...props} />
      <span className={`${styles.timeLeft} ${styles.timeStamp}`}>{getTimeProgress()}%</span>
    </div>
  )
}

export default Perk
