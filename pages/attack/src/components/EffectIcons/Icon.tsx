import React from 'react'
import classnames from 'classnames'
// import isValue from '@torn/shared/utils/isValue'

import { IGeneralIconProps } from './interfaces'
import styles from './index.cssmodule.scss'

class Icon extends React.PureComponent<IGeneralIconProps> {
  // shouldComponentUpdate(nextProps: IGeneralIconProps) {
  //   // it's some kind of a hack should be replaced by cutting
  //   // the timeLeft from receiving by the icon component
  //   if (isValue(nextProps.timeLeft) && nextProps.timeLeft !== 0) {
  //     return false
  //   }

  //   return true
  // }

  _classNames = () => {
    const { iconType } = this.props

    const iconWrap = classnames({
      [styles.svgIconWrap]: true,
      [styles[iconType]]: iconType && styles[iconType]
    })

    return {
      iconWrap
    }
  }

  render() {
    const { title, class: iconClass, level: iconLVL } = this.props

    const { iconWrap } = this._classNames()

    const iconName = iconLVL ? `${iconClass}_${iconLVL}` : iconClass

    return (
      <div id={title} className={styles.iconContainer}>
        <div className={iconWrap}>
          <img className={styles.iconImg} src={`/images/v2/attack/effect_icons/${iconName}.svg`} />
        </div>
      </div>
    )
  }
}

export default Icon
