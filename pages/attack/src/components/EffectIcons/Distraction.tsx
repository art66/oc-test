import React from 'react'
import classnames from 'classnames'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import getAggressionValue from '../../utils/getAggressionValue'
import calcDecrees from '../../utils/calcDecrees'

import Icon from './Icon'

import { IDistractionProps, IDistractionState } from './interfaces'

import styles from './index.cssmodule.scss'

const ICON_MAX_TYPE = 10

class Distraction extends React.Component<IDistractionProps, IDistractionState> {
  private _timerID: any

  constructor(props: IDistractionProps) {
    super(props)

    this.state = {
      tempLastAggressiveID: null,
      tempAggressiveMoves: null,
      isDistractionAppear: true,
      shouldNotUnmount: true,
      updateType: ''
    }
  }

  static getDerivedStateFromProps(nextProps: IDistractionProps, prevState: IDistractionState) {
    const { aggressiveMoves, lastAggressiveID } = nextProps

    const { tempAggressiveMoves, updateType } = prevState

    if (updateType === 'state') {
      return {
        updateType: ''
      }
    }

    if (tempAggressiveMoves && !aggressiveMoves || !getAggressionValue({ lastAggressiveID, aggressiveMoves })) {
      return {
        // tempAggressiveMoves: tempAggressiveMoves,
        isDistractionAppear: false
      }
    }

    if (aggressiveMoves) {
      return {
        tempLastAggressiveID: lastAggressiveID,
        tempAggressiveMoves: aggressiveMoves,
        isDistractionAppear: true,
        shouldNotUnmount: false
      }
    }

    return null
  }

  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()

    const { isDistractionAppear } = this.state

    // we need to check if the user received new aggression
    // and we don't need to unmount the component now
    if (isDistractionAppear && this._timerID) {
      this._clearTimer()

      return
    }

    if (!isDistractionAppear) {
      this._startTimer()
    }
  }

  componentWillUnmount() {
    this._clearTimer()
  }

  _initializeTooltip = () => {
    const { title } = this.props

    return [{
      ID: title,
      child: (
        <div className={styles.tooltipText}>
          <span className={styles.tooltipTitle}>{title}</span>
          <span className={styles.tooltipDesc}>{this._getDescription()}</span>
        </div>
      ),
      customConfig: {
        position: { x: 'right', y: 'center' }
      }
    }]
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initializeTooltip(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initializeTooltip(), type: 'update' })

  _startTimer = () => {
    this._clearTimer()

    this._timerID = setTimeout(() => {
      const { shouldNotUnmount, isDistractionAppear: isAppearAllowed } = this.state

      shouldNotUnmount && !isAppearAllowed && this.setState({
        updateType: 'state',
        shouldNotUnmount: true
      })
    }, 750)
  }

  _clearTimer = () => clearTimeout(this._timerID)

  _classesHolder = () => {
    const { isDistractionAppear } = this.state
    const { colorScheme } = this.props

    const iconWrapClass = classnames({
      [styles.iconWrap]: true,
      [styles[colorScheme]]: colorScheme,
      [styles.showup]: isDistractionAppear,
      [styles.leave]: !isDistractionAppear
    })

    return {
      iconWrapClass
    }
  }

  _getAggressionValue = () => {
    const { tempLastAggressiveID: lastAggressiveID, tempAggressiveMoves: aggressiveMoves } = this.state

    const aggression = getAggressionValue({ lastAggressiveID, aggressiveMoves })

    return aggression
  }

  _getIconLVL = () => {
    const aggressionValue = this._getAggressionValue()

    return aggressionValue >= ICON_MAX_TYPE ? ICON_MAX_TYPE : aggressionValue
  }

  _getDescription = () => {
    const lvlValue = this._getAggressionValue()
    const percentDecrease = calcDecrees(lvlValue)
    const percentValue = percentDecrease === 100 ? 100 : String(percentDecrease).substr(0, 5)

    const firstLine = `Ability to respond reduced to 1 in ${lvlValue + 1}`
    const secondLine = `Defense & Dexterity reduced by ${percentValue}%`

    return `${firstLine} \n ${secondLine}`
  }

  render() {
    const { shouldNotUnmount } = this.state

    if (shouldNotUnmount) {
      return null
    }

    const iconLVL = this._getIconLVL()
    const aggressionValue = this._getAggressionValue()
    const { iconWrapClass } = this._classesHolder()

    return (
      <div className={iconWrapClass}>
        <Icon {...this.props} level={iconLVL} />
        <div className={styles.countWrap}>
          <span className={styles.count}>{aggressionValue}</span>
        </div>
      </div>
    )
  }
}

export default Distraction
