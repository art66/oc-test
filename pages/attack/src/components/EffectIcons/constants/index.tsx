import Effect from '../Effect'
import Perk from '../Perk'
import Distraction from '../../../containers/Distraction'

const ICONS_LAYOUT = {
  effect: {
    component: Effect,
    icons: {
      bleeding: {
        color: 'red',
        dimensions: {
          width: 28.01,
          height: 24.03,
          viewbox: '0 0 28.01 24.03'
        }
      },
      blast: {
        color: 'red',
        dimensions: {
          width: 29,
          height: 30,
          viewbox: '0 0 29 30'
        }
      },
      blinded: {
        color: 'red',
        dimensions: {
          width: 29,
          height: 30,
          viewbox: '0 0 29 30'
        }
      },
      blindfire: {
        color: 'green',
        dimensions: {
          width: 29,
          height: 18,
          viewbox: '0 0 29 18'
        }
      },
      supressed: {
        color: 'red',
        dimensions: {
          width: 29,
          height: 18,
          viewbox: '0 0 29 18'
        }
      },
      burning: {
        color: 'red',
        dimensions: {
          width: 24.01,
          height: 32,
          viewbox: '0 0 24.01 32'
        }
      },
      chilled: {
        color: 'red',
        dimensions: {
          width: 26,
          height: 24,
          viewbox: '0 0 26 24'
        }
      },
      contaminated: {
        color: 'red',
        dimensions: {
          width: 28,
          height: 26,
          viewbox: '0 0 28 26'
        }
      },
      crippled: {
        color: 'red',
        dimensions: {
          width: 28,
          height: 28,
          viewbox: '0 0 28 28'
        }
      },
      demoralized: {
        color: 'red',
        dimensions: {
          width: 28,
          height: 28,
          viewbox: '0 0 28 28'
        }
      },
      disoriented: {
        color: 'red',
        dimensions: {
          width: 24,
          height: 27,
          viewbox: '0 0 24 27'
        }
      },
      dimensiokinesis: {
        color: 'red',
        dimensions: {
          width: 24,
          height: 27,
          viewbox: '0 0 24 27'
        }
      },
      emasculated: {
        color: 'red',
        dimensions: {
          width: 27,
          height: 27,
          viewbox: '0 0 27 27'
        }
      },
      eviscerated: {
        color: 'red',
        dimensions: {
          width: 34,
          height: 34,
          viewbox: '0 0 34 34'
        }
      },
      essokinesis: {
        color: 'red',
        dimensions: {
          width: 34,
          height: 34,
          viewbox: '0 0 34 34'
        }
      },
      frozen: {
        color: 'red',
        dimensions: {
          width: 26,
          height: 28,
          viewbox: '0 0 26 28'
        }
      },
      gassed: {
        color: 'red',
        dimensions: {
          width: 28,
          height: 25,
          viewbox: '0 0 28 25'
        }
      },
      maced: {
        color: 'red',
        dimensions: {
          width: 26,
          height: 28,
          viewbox: '0 0 26 28'
        }
      },
      motivated: {
        color: 'green',
        dimensions: {
          width: 34,
          height: 27.56,
          viewbox: '0 0 34 27.56'
        }
      },
      paralyzed: {
        color: 'red',
        dimensions: {
          width: 35,
          height: 35,
          viewbox: '0 0 35 35'
        }
      },
      strengthened: {
        color: 'green',
        dimensions: {
          width: 21,
          height: 27,
          viewbox: '0 0 21 27'
        }
      },
      hastened: {
        color: 'green',
        dimensions: {
          width: 30.66,
          height: 26.38,
          viewbox: '0 0 30.66 26.38'
        }
      },
      hardened: {
        color: 'green',
        dimensions: {
          width: 22,
          height: 29.01,
          viewbox: '0 0 22 29.01'
        }
      },
      hazardous: {
        color: 'red',
        dimensions: {
          width: 29.66,
          height: 29.66,
          viewbox: '0 0 29.66 29.66'
        }
      },
      poisoned: {
        color: 'red',
        dimensions: {
          width: 19,
          height: 28.62,
          viewbox: '0 0 19 28.62'
        }
      },
      smoked: {
        color: 'red',
        dimensions: {
          width: 28,
          height: 25,
          viewbox: '0 0 28 25'
        }
      },
      spray: {
        color: 'green',
        dimensions: {
          width: 27,
          height: 27,
          viewbox: '0 0 27 27'
        }
      },
      storage: {
        color: 'green',
        dimensions: {
          width: 26,
          height: 28.55,
          viewbox: '0 0 26 28.55'
        }
      },
      oneirokinesis: {
        color: 'green',
        dimensions: {
          width: 26,
          height: 28.55,
          viewbox: '0 0 26 28.55'
        }
      },
      stunned: {
        color: 'red',
        dimensions: {
          width: 37,
          height: 28,
          viewbox: '0 0 37 28'
        }
      },
      suppressed: {
        color: 'red',
        dimensions: {
          width: 33.42,
          height: 25,
          viewbox: '0 0 33.42 25'
        }
      },
      sharpened: {
        color: 'green',
        dimensions: {
          width: 28,
          height: 28,
          viewbox: '0 0 28 28'
        }
      },
      severeBurning: {
        color: 'red',
        dimensions: {
          width: 35,
          height: 30,
          viewbox: '0 0 35 30'
        }
      },
      sleeping: {
        color: 'red',
        dimensions: {
          width: 28,
          height: 27,
          viewbox: '0 0 28 27'
        }
      },
      slowed: {
        color: 'red',
        dimensions: {
          width: 30.66,
          height: 26.38,
          viewbox: '0 0 30.66 26.38'
        }
      },
      weakened: {
        color: 'red',
        dimensions: {
          width: 22.5,
          height: 30,
          viewbox: '0 0 22.5 30'
        }
      },
      withered: {
        color: 'red',
        dimensions: {
          width: 21,
          height: 27,
          viewbox: '0 0 21 27'
        }
      },
      concussed: {
        color: 'red',
        dimensions: {
          width: 34,
          height: 34,
          viewbox: '0 0 34 34'
        }
      },
      lacerated: {
        color: 'red',
        dimensions: {
          width: 30,
          height: 34,
          viewbox: '0 0 30 34.2'
        }
      }
    },
    insanity: {
      color: 'red',
      dimensions: {
        width: 30,
        height: 34,
        viewbox: '0 0 30 34.2'
      }
    }
  },
  perk: {
    component: Perk,
    icons: {
      loot: {
        color: 'yellow',
        dimensions: {
          width: 34.72,
          height: 34.32,
          viewbox: '0 0 34.72 34.32'
        }
      }
    }
  },
  distraction: {
    component: Distraction,
    icons: {
      distraction: {
        color: 'blue',
        dimensions: {
          width: 38,
          height: 38,
          viewbox: '0 0 38 38'
        }
      }
    }
  }
}

const COLORS_HOLDER = {
  red: {
    'cls-1': {
      style: {
        fill: '#f1f1f1'
      }
    },
    'cls-2': {
      style: {
        fill: '#b22d00'
      }
    }
  },
  blue: {
    'cls-1': {
      style: {
        fill: '#f1f1f1'
      }
    },
    'cls-2': {
      style: {
        fill: '#2b8dad'
      }
    }
  },
  green: {
    'cls-1': {
      style: {
        fill: '#f1f1f1'
      }
    },
    'cls-2': {
      style: {
        fill: '#738e24'
      }
    }
  },
  yellow: {
    'cls-1': {
      style: {
        fill: '#f1f1f1'
      }
    },
    'cls-2': {
      style: {
        fill: '#daa520'
      }
    }
  }
}

const DIMENSIONS_HOLDER = {
  default: {
    width: 28,
    height: 25,
    viewbox: '0 0 28 25'
  }
}

const TEMP_EFFECTS_MOCK = [
  {
    ID: '12913499',
    additionalInfo: {
      itemname: 'Smoke grenade',
      time: 149,
      image: '/images/v2/attack/items/tempitems/226vs.fw.png'
    },
    basicDamage: '0',
    class: 'smoked',
    desc: 'Speed reduced to 1/3rd',
    effectID: '103',
    itemID: 226,
    level: null,
    new: false,
    timeLeft: 149,
    timestamp: '1587268441',
    title: 'Smoked',
    type: 2,
    userID: 1435333,
    uses: '0'
  },
  {
    ID: -2,
    type: 3,
    effectID: 302,
    title: 'Distraction',
    class: 'distraction',
    desc: 'Stats reduction',
    effectsModifiers: {
      aggressiveMoves: {
        0: 1
      }
    }
  },
  {
    ID: -22,
    type: 4,
    effectID: 300,
    level: 5,
    title: 'Loot 5',
    class: 'loot',
    desc: 'Loot description'
  }
]

export { ICONS_LAYOUT, COLORS_HOLDER, DIMENSIONS_HOLDER, TEMP_EFFECTS_MOCK }
