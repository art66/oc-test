import { IStatsModifiers } from '../../IndicatorFacilities/interfaces'

export interface IEffect {
  ID: number | string
  class: string
  type: number
  title: string
  desc: string
  isLeft: boolean
}

export interface IProps {
  userType: string
  effects: {
    ID: number | string
    class: string
    type: number
  }[]
}

export interface IState {
  isEffectHovered: boolean
  iconYPos: number
  iconXPos: number
  effectTitle: string
  effectDesc: string
}

export interface IGeneralIconProps {
  class: string
  colorScheme?: string
  level?: number
  dimensions: object
  iconType: string
  title: string
  timeLeft: number
  desc: string
  isLeft: boolean
}

export interface IGeneralState {
  ID: string
}

export interface IPerkProps extends IGeneralIconProps {
  timestamp: number
}

export interface IEffectProps extends IGeneralIconProps {
  timeLeft: number
}

export interface IDistractionProps extends IGeneralIconProps {
  statsModifiersDefender: IStatsModifiers
  lastAggressiveID?: number
  aggressiveMoves: {
    [x: number]: number
  }
}

export interface IDistractionState {
  tempLastAggressiveID: number
  tempAggressiveMoves: {
    [x: number]: number
  }
  isDistractionAppear: boolean
  shouldNotUnmount: boolean
  updateType: 'state' | 'props' | ''
}

export type TIconTypes = 1 | 2 | 3 | 4

export type TIconLabels = 'effect' | 'perk' | 'distraction'
