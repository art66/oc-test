import React, { memo } from 'react'
import classnames from 'classnames'

import Icon from './Icon'
import { IEffectProps } from './interfaces'
import styles from './index.cssmodule.scss'

const Effect = (props: IEffectProps) => {
  const { colorScheme, timeLeft } = props

  // it's a hack here!!!
  // Need to create redux channel and subscribe on update once effect became expired!
  if (timeLeft < 0) {
    return null
  }

  const iconAnim = {
    [styles.showup]: !!timeLeft,
    [styles.leave]: !timeLeft
  }

  const iconClasses = classnames(styles.iconWrap, styles[colorScheme], iconAnim)

  return (
    <div className={iconClasses}>
      <Icon {...props} />
      <div className={styles.timeLeft}>{timeLeft}</div>
    </div>
  )
}

export default memo(Effect)
