export interface IProps {
  contentTitle: JSX.Element | JSX.Element[] | Element | Element[] | string,
  children?: JSX.Element | JSX.Element[] | Element | Element[] | Node,
  debugInfo: string | object
}
