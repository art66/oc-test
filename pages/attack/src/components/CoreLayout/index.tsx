import React from 'react'
import DebugBox from '@torn/shared/components/DebugBox'

import Masks from '../ModelLayers/Masks'

import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

class CoreLayout extends React.PureComponent<IProps> {
  static defaultProps = {
    contentTitle: null,
    children: null,
    debugInfo: ''
  }

  render() {
    const { contentTitle, debugInfo, children } = this.props

    return (
      <div className={styles.coreWrap}>
        {contentTitle}
        {debugInfo && <DebugBox debugMessage={debugInfo} isBeatifyError={true} />}
        {/* {infoBox && <InfoBox {...infoBox} />} */}
        {children}
        <Masks />
      </div>
    )
  }
}

export default CoreLayout
