import React from 'react'
import cn from 'classnames'
import isValue from '@torn/shared/utils/isValue'

import isRival from '../../utils/isRival'

import s from './stealthBar.cssmodule.scss'
import i from '../../styles/icons.cssmodule.scss'

import { IProps } from './interfaces'

class StealthBar extends React.PureComponent<IProps> {
  render() {
    const { level, userType, mediaType, isDesktopManualLayout } = this.props

    const layoutOnTouchDevices = (mediaType !== 'desktop') && !isDesktopManualLayout

    if ((layoutOnTouchDevices && !isRival(userType))) {
      return null
    }

    const barHeight = isValue(level) ? level * 10 : 100

    return (
      <div className={s.stealthBarWrap}>
        <div className={s.level} style={{ height: `${barHeight}%` }} />
        <i className={cn(i.iconStealth, s.icon)} />
      </div>
    )
  }
}

export default StealthBar
