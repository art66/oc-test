export interface IProps {
  level: number
  userType: string
  mediaType: string
  isDesktopManualLayout: boolean
}
