import { TRarity } from '@torn/shared/utils/getItemGlowClassName/interfaces'

export interface IIconEnhancer {
  icon: string
  hoverover: string
}

export interface IProps {
  loading?: boolean
  accuracy: number | string
  action: () => void
  active: boolean
  ammoStatus: string
  boxTitle: string
  className: string
  currentBonuses: IIconEnhancer[]
  currentUpgrades: IIconEnhancer[]
  dmg: number | string
  imgSrc: string
  showItemsBonuses: boolean
  tabIndex: number
  type: string
  weaponTitle: string
  weaponType: string
  userType: string
  orange: boolean
  attackStatus: string
  rarity?: TRarity
}
