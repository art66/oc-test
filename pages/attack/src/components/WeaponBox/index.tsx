import React from 'react'
import cn from 'classnames'

import waify from '@torn/shared/utils/waify'
import getItemGlowClassName from '@torn/shared/utils/getItemGlowClassName'

import BonusIcon from './BonusIcon'
import LoadingIndicator from '../LoadingIndicator'

import { COLOR_PARAMS } from './constants'
import { IProps } from './interfaces'

import s from './weaponbox.cssmodule.scss'
import '../Weapons/weapons.cssmodule.scss'

class WeaponBox extends React.PureComponent<IProps> {
  private _ref: any

  static defaultProps: IProps = {
    loading: false,
    accuracy: '',
    action: () => {},
    active: false,
    ammoStatus: '',
    boxTitle: '',
    className: '',
    currentBonuses: [],
    currentUpgrades: [],
    dmg: null,
    imgSrc: '',
    showItemsBonuses: false,
    tabIndex: null,
    type: '',
    weaponTitle: '',
    weaponType: '',
    rarity: null,
    userType: '',
    orange: false,
    attackStatus: ''
  }

  constructor(props: IProps) {
    super(props)

    this._ref = React.createRef()
  }

  _handleClick = () => {
    const { action, attackStatus } = this.props

    if (!['process', 'started'].includes(attackStatus)) {
      return
    }

    action && action()
  }

  _toFixed = val => (isNaN(val) ? '' : Number(val).toFixed(2))

  _getColorParams = rarity => COLOR_PARAMS[rarity]

  _getWaify = (userType, boxTitle) => waify(`${userType}_${boxTitle}`)

  _getWeaponIconAndTitle = () => {
    const { currentUpgrades, currentBonuses } = this.props

    const { icon: iconUpZero, hoverover: hoveroverUpZero } = currentUpgrades[0] || ([] as any)
    const { icon: iconUpOne, hoverover: hoveroverUpOne } = currentUpgrades[1] || ([] as any)
    const { icon: iconBonZero, hoverover: hoveroverBonZero } = currentBonuses[0] || ([] as any)
    const { icon: iconBonOne, hoverover: hoveroverBonOne } = currentBonuses[1] || ([] as any)

    return {
      hoveroverUpZero,
      hoveroverUpOne,
      hoveroverBonZero,
      hoveroverBonOne,
      iconUpZero,
      iconUpOne,
      iconBonZero,
      iconBonOne
    }
  }

  _renderBonusSection() {
    const { userType, type, showItemsBonuses, boxTitle } = this.props
    const withTitle = this._getWaify(userType, boxTitle)

    const {
      hoveroverUpZero,
      hoveroverUpOne,
      hoveroverBonZero,
      hoveroverBonOne,
      iconUpZero,
      iconUpOne,
      iconBonZero,
      iconBonOne
    } = this._getWeaponIconAndTitle()

    if (type !== 'full') {
      return null
    }

    const bonuses = showItemsBonuses
      ? {
          left: (
            <div className={s.props}>
              <BonusIcon icon={iconUpOne} title={hoveroverUpOne} />
              <BonusIcon icon={iconUpZero} title={hoveroverUpZero} />
            </div>
          ),
          right: (
            <div className={s.props}>
              <BonusIcon icon={iconBonZero} title={hoveroverBonZero} />
              <BonusIcon icon={iconBonOne} title={hoveroverBonOne} />
            </div>
          )
        }
      : {}

    return (
      <div className={s.top}>
        {bonuses.left}
        <span className={s.topMarker} id={withTitle('label')}>
          {boxTitle}
        </span>
        {bonuses.right}
      </div>
    )
  }

  _renderFigure() {
    const { imgSrc, weaponTitle, rarity } = this.props
    const colorParams = this._getColorParams(rarity) || {}

    const imgClass = cn(
      {
        'rarity-item': true
      },
      getItemGlowClassName(rarity)
    )

    const imgToRender = (
      <img
        data-color={colorParams.color}
        data-opacity='1'
        data-overlay={colorParams.overlay}
        className={imgClass}
        alt={weaponTitle}
        src={imgSrc}
      />
    )

    return (
      <figure ref={this._ref} className={s.weaponImage}>
        {imgToRender}
      </figure>
    )
  }

  _renderAmmo() {
    const { dmg, showItemsBonuses, type, ammoStatus, userType, boxTitle, accuracy } = this.props
    const withTitle = this._getWaify(userType, boxTitle)

    if (type !== 'full' && ammoStatus === 'infinity') {
      return null
    }

    const renderTopSection =
      showItemsBonuses && type.toString() === 'full' && dmg ? (
        <div className={s.props}>
          <i className='bonus-attachment-item-damage-bonus' id={withTitle('damage')} aria-label='Damage' />
          <span id={withTitle('damage-value')}>{this._toFixed(dmg)}</span>
        </div>
      ) : null

    const renderMiddleSection = (
      <div className={s.bottomMarker}>
        {ammoStatus === 'infinity' ? <span className={s.eternity} /> : <span>{ammoStatus}</span>}
      </div>
    )

    const renderBottomSection =
      showItemsBonuses && type.toString() === 'full' && accuracy ? (
        <div className={s.props}>
          <i
            className={`bonus-attachment-item-accuracy-bonus ${s.bonusIcon} ${s.icon}`}
            aria-label='Accuracy'
            id={withTitle('accuracy')}
          />
          <span id={withTitle('accuracy')} className={s.iconText}>
            {this._toFixed(accuracy)}
          </span>
        </div>
      ) : null

    return (
      <div className={s.bottom}>
        {renderTopSection}
        {renderMiddleSection}
        {renderBottomSection}
      </div>
    )
  }

  render() {
    const { loading, active, boxTitle, className, userType, weaponTitle, weaponType, orange } = this.props
    const withTitle = this._getWaify(userType, boxTitle)

    return (
      <div
        id={`weapon_${weaponType}`}
        role='button'
        tabIndex={0}
        onKeyDown={undefined}
        aria-describedby={withTitle('label damage damage-value accuracy accuracy-value')}
        aria-label={weaponTitle ? `Attack with ${weaponTitle}` : 'This weapon is not available'}
        className={cn(className, s.weponBox, { [s.active]: active, [s.orange]: orange })}
        onClick={this._handleClick}
      >
        {this._renderBonusSection()}
        {loading ? <LoadingIndicator /> : this._renderFigure()}
        {this._renderAmmo()}
      </div>
    )
  }
}

export default WeaponBox
