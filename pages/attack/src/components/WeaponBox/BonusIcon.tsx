import React from 'react'
import s from './weaponbox.cssmodule.scss'

interface IProps {
  icon: string,
  title: string
}

class BonusIcon extends React.PureComponent<IProps> {
  static defaultProps: IProps = {
    icon: 'blank-bonus-25',
    title: ''
  }

  render() {
    const { icon = 'blank-bonus-25', title = '' } = this.props

    return (
      <div className={s.attachment} title={title}>
        <i className={`bonus-attachment-${icon}`} />
      </div>
    )
  }
}

export default BonusIcon
