export const COLOR_PARAMS = {
  1: {
    color: '#FFBF00',
    overlay: '#92886C'
  },
  2: {
    color: '#FF8000',
    overlay: '#927F6C'
  },
  3: {
    color: '#FF0000',
    overlay: '#926C6C'
  }
}
