// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'attachment': string;
  'bonusIcon': string;
  'bottom': string;
  'bottomMarker': string;
  'eternity': string;
  'globalSvgShadow': string;
  'icon': string;
  'iconText': string;
  'marker': string;
  'orange': string;
  'orangeBox': string;
  'props': string;
  'top': string;
  'topMarker': string;
  'weaponImage': string;
  'weponBox': string;
}
export const cssExports: CssExports;
export default cssExports;
