import React from 'react'

import Modal from '../Modal'
import Dialog from '../Dialog'
import DialogStart from '../../containers/DialogStart'
import DialogEnd from '../../containers/DialogEnd'

import { RIVAL, MT, ME } from '../../constants'
import { IProps } from './interfaces'

class Overlay extends React.PureComponent<IProps> {
  _renderRivalOverlay = () => {
    const { status, userType, error, showEnemyItems, isError } = this.props

    if (error) {
      return (
        <Modal transparentBg={showEnemyItems} userType={userType}>
          <Dialog title={error} red={true} isError={isError} />
        </Modal>
      )
    }

    if (status.attackStatus === 'notStarted') {
      return (
        <Modal transparentBg={showEnemyItems} userType={userType}>
          <DialogStart />
        </Modal>
      )
    }

    if (status.attackStatus === 'end') {
      return (
        <Modal transparentBg={false} userType={userType}>
          <DialogEnd />
        </Modal>
      )
    }

    return null
  }

  _renderRestrictOverlay = () => {
    const { userType } = this.props

    return <Modal transparentBg={true} userType={userType} />
  }

  render() {
    const { status, userType, error, mediaType, isDesktopManualLayout } = this.props

    const isDesktopMode = mediaType === MT.DESKTOP || isDesktopManualLayout
    const isRivalModel = isDesktopMode ? RIVAL : ME

    if (isRivalModel === userType) {
      return this._renderRivalOverlay()
    }

    if (['notStarted', 'end'].includes(status.attackStatus) || error) {
      return this._renderRestrictOverlay()
    }

    return null
  }
}

export default Overlay
