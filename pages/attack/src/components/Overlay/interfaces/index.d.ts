export interface IProps {
  status: {
    attackStatus: 'notStarted' | 'end'
  },
  userType: string,
  error: string,
  mediaType: string,
  showEnemyItems: boolean | number,
  isDesktopManualLayout: boolean
  isError: boolean
}
