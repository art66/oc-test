import React from 'react'

import s from './bullet.cssmodule.scss'
import {  IProps } from './interfaces'

class Bullet extends React.PureComponent<IProps> {
  render() {
    const { left, top, type } = this.props

    return (
      <div className={s.bullet} style={{ left, top, background: `url(/images/v2/attack/${type}.svg)` }} />
    )
  }
}

export default Bullet
