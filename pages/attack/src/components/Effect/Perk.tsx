import React, { PureComponent } from 'react'
import cn from 'classnames'

import { IPerk } from './interfaces'
import s from './effects.cssmodule.scss'

class Perk extends PureComponent<IPerk> {
  static defaultProps: IPerk = {
    class: '',
    level: null
  }

  _getEffecClass = () => {
    const { class: className, level } = this.props

    if (level !== null) {
      return `${className}_${level}`
    }

    return className
  }

  render() {
    const effectClass = this._getEffecClass()

    return (
      <div className={s.perkWrap}>
        <div className={cn(s[effectClass], s.bottomLayer, s.perkBcg)} />
      </div>
    )
  }
}

export default Perk
