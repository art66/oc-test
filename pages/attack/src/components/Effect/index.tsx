import React, { PureComponent } from 'react'

import Effect from './Effect'
import Perk from './Perk'

import { IProps } from './interfaces'
import { EFFECTS_TYPE_TRANSPILER } from '../../constants'

class EffectBackground extends PureComponent<IProps> {
  static defaultProps: any = {
    type: 1
  }

  _getEffectType = () => {
    const { type = 1 } = this.props
    const currentType = EFFECTS_TYPE_TRANSPILER[type]

    return currentType
  }

  _getCurrentLayout = () => {
    const currentType = this._getEffectType()

    const CurrentLayout = currentType === 'effect' ? Effect : Perk

    return CurrentLayout
  }

  render() {
    const { class: className, ID } = this.props

    const CurrentLayout = this._getCurrentLayout()

    // @ts-ignore
    return <CurrentLayout {...this.props} key={`${ID}_${className}`} />
  }
}

export default EffectBackground
