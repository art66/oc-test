import React, { PureComponent } from 'react'
import cn from 'classnames'

import { IProps } from './interfaces'
import s from './effects.cssmodule.scss'

class Effect extends PureComponent<IProps> {
  static defaultProps: IProps = {
    class: '',
    timeLeft: 0,
    level: null,
    ID: null,
    type: null
  }

  _getStackableOpacity = () => {
    const { stackCount, stackCountMax } = this.props

    return stackCount / stackCountMax || 1
  }

  _getOpacityTimeout = () => {
    const { timeLeft = 0 } = this.props
    const opacity = timeLeft / 5 > 1 ? 1 : timeLeft / 5

    return opacity
  }

  _getEffectType = () => {
    const { class: className, level } = this.props

    if (level !== null) {
      return `${className}_${level}`
    }

    return className
  }

  _getEffectClassNames = (layer: 'topLayer' | 'bottomLayer') => {
    const { isStackable } = this.props

    return cn({
      [s[this._getEffectType()]]: true,
      [s[layer]]: true,
      [s.noEnterAnim]: isStackable
    })
  }

  render() {
    const { class: className, ID, isStackable } = this.props

    const opacity = isStackable ? this._getStackableOpacity() : this._getOpacityTimeout()

    return (
      <div key={`${ID}_${className}`} className={s.effectWrap} style={{ opacity }}>
        <div className={this._getEffectClassNames('bottomLayer')} />
        <div className={this._getEffectClassNames('topLayer')} />
      </div>
    )
  }
}

export default Effect
