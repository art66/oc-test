import { IEffect } from '../../../interfaces/IState'

export interface IPerk {
  class: string
  level: number
}

export interface IProps extends IEffect {}
