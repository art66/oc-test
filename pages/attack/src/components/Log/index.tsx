// @ts-nocheck
import React from 'react'
import cn from 'classnames'
import isEmpty from '@torn/shared/utils/isEmpty'

import TimerHOC from '../TimerHOC'

import { MT } from '../../constants'
import s from './log.cssmodule.scss'
import misc from '../../styles/misc.cssmodule.scss'
import { IProps } from './interfaces'

class Log extends React.PureComponent<IProps> {
  _isDesktop = () => {
    const { mediaType, isDesktopManualLayout } = this.props

    return mediaType === MT.DESKTOP || isDesktopManualLayout
  }

  _calcEmptyRowsMax = () => {
    const emptyRowsLengthMax = this._isDesktop() ? 10 : 3

    return emptyRowsLengthMax
  }

  // this is the legacy optimization for dangerous markup received
  _getDangerousValue = (dangerValue: string) => {
    const fakeNode = document.createElement('div')

    fakeNode.innerHTML = dangerValue

    const { textContent: textFirst } = fakeNode.firstChild as HTMLElement
    const { textContent: textLast } = fakeNode.lastChild as HTMLElement

    const damageNode = fakeNode.querySelector('em')
    const damageValue = damageNode && damageNode.textContent
    const damageClass = damageNode && damageNode.className || {}

    const isLastText = textLast.nodeName !== 'EM' && textFirst !== textLast && damageValue !== textLast

    return (
      <React.Fragment>
        <span>{textFirst}</span>
        {damageValue && <em className={damageClass}>{damageValue}</em>}
        {isLastText && <span>{textLast}</span>}
      </React.Fragment>
    )
  }

  _getList = () => {
    const { logs = [], timePassed } = this.props

    if (isEmpty(logs)) {
      return null
    }

    const renderIcon = (icon: string) => (
      <span className={s.iconWrap}>
        <span className={icon} />
      </span>
    )

    const renderMessage = (text: string) => (
      <span className={s.message}>
        {this._getDangerousValue(text)}
      </span>
    )

    const renderTimeout = (timeAgo: number) => (
      <span className={s.col2}>{`${timeAgo + timePassed}s`}</span>
    )

    const listToRender = logs.map((props, i) => {
      const { ID, color, icon, text, timeAgo } = props

      return (
        <li className={s.row} key={`${ID}_${i}`}>
          <span className={cn(s.col1, s[color])}>
            {renderIcon(icon)}
            {renderMessage(text)}
          </span>
          {renderTimeout(timeAgo)}
        </li>
      )
    })

    return listToRender
  }

  _getListWrapper = (children) => {
    return (
      <ul className={s.list} aria-describedby='log-header'>
        {children}
      </ul>
    )
  }

  _renderEmptyRows = (rowsCount: number) => {
    if (!rowsCount || rowsCount === 0) {
      return null
    }

    const emptyRows = Array.from(Array(rowsCount).keys())

    return emptyRows.map((key: number) => (
      <li className={s.row} key={key}>
        <span className={s.col1} />
        <span className={s.col2} />
      </li>
    ))
  }

  // we need that to prevent gap once
  // there is less rows than actual height of container
  _renderEmptyRowsAdaptive = () => {
    const { logs } = this.props

    const logsLength = logs && logs.length
    const emptyRowsCount = this._calcEmptyRowsMax() - logsLength
    const emptyRowsToRender = emptyRowsCount > 0 && emptyRowsCount || 0

    return this._renderEmptyRows(emptyRowsToRender)
  }

  _renderPlaceholder = () => {
    const emptyRowsCount = this._calcEmptyRowsMax()

    const placeholder = this._renderEmptyRows(emptyRowsCount)

    return placeholder
  }

  _renderList = () => {
    const isListEmpty = !this._getList()

    const placeholder = this._renderPlaceholder()

    const list = (
      <React.Fragment>
        {this._getList()}
        {this._renderEmptyRowsAdaptive()}
      </React.Fragment>
    )

    const contentToRender = isListEmpty ? placeholder : list

    return this._getListWrapper(contentToRender)
  }

  _renderTitle = () => {
    return (
      <div className={cn(misc.boxTitle, s.cols)} aria-label='Log' id='log-header'>
        <span className={s.col1}>Action</span>
        <span className={s.col2}>Time</span>
      </div>
    )
  }

  render() {
    return (
      <div className={s.logWrap}>
        {this._renderTitle()}
        {this._renderList()}
      </div>
    )
  }
}

export default props => (
  <TimerHOC isTimeUp={true} timePassed={0}>
    <Log {...props} />
  </TimerHOC>
)
