export interface IProps {
  logs: {
    ID: string
    color: string
    icon: string
    text: string
    timeAgo: number
  }[]
  timerValue: number
  timePassed: number
  mediaType: string
  isDesktopManualLayout: boolean
}
