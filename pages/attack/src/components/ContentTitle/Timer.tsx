import React, { PureComponent } from 'react'
import { hhmmss } from '@torn/shared/utils'
import waify from '@torn/shared/utils/waify'

import TimerHOC from '../TimerHOC'

import { ITimer } from './interfaces'

class Timer extends PureComponent<ITimer, { uuid: number }> {
  static defaultProps = {
    timeLeft: null
  }

  constructor(props: ITimer) {
    super(props)

    this.state = {
      uuid: null
    }
  }

  componentDidMount() {
    this.setState({
      uuid: Math.random()
    })
  }

  _getID = () => {
    const { uuid } = this.state

    return waify('timeout-value', uuid && uuid.toString())
  }

  render() {
    const { timeLeft } = this.props
    const ID = this._getID()

    return (
      <span aria-describedby={ID.toString()}>
        <span id={ID.toString()}>{hhmmss(timeLeft >= 0 ? timeLeft : 0)}</span>
      </span>
    )
  }
}

export default props => (
  <TimerHOC isTimeDown={true} timeLeft={props.timeLeft}>
    <Timer {...props} />
  </TimerHOC>
)
