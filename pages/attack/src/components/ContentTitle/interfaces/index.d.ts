export interface IProps {
  currentMove: number
  timeLeft: number
  endFight: ({ step, isEscape }: { step: string; isEscape: boolean }) => void
  escapeAvailable: boolean
  status: {
    attackStatus: string
  }
  endResult: string | number
  userID: number | string
  loading: boolean
}

export interface IButton {
  loading: boolean
  escapeAvailable: boolean
  action: () => void
}

export interface ITimer {
  timeLeft: number
}
