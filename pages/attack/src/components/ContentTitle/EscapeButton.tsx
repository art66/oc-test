import Button from '@torn/shared/components/ResponsiveButton'
import React from 'react'
import LoadingIndicator from '../LoadingIndicator'
import styles from './contenttitle.cssmodule.scss'
import { IButton } from './interfaces'

class EscapeButton extends React.PureComponent<IButton> {
  render() {
    const { escapeAvailable, action, loading } = this.props

    if (loading) {
      return (
        <div className={styles.preloaderBtnEscapeWrap}>
          <LoadingIndicator />
        </div>
      )
    }

    return (
      <Button className={styles.escapeBtnFix} disabled={!escapeAvailable} action={action} responsive={false}>
        ESCAPE
      </Button>
    )
  }
}

export default EscapeButton
