import React from 'react'

import AppHeader from '@torn/shared/components/AppHeader'

import Timer from './Timer'
import EscapeButton from './EscapeButton'

import { STEP_ESCAPE } from '../../constants'
import styles from './contenttitle.cssmodule.scss'
import { IProps } from './interfaces'

class ContentTitle extends React.PureComponent<IProps> {
  static defaultProps: IProps = {
    currentMove: 0,
    endFight: () => {},
    escapeAvailable: false,
    status: {
      attackStatus: ''
    },
    endResult: '',
    userID: null,
    loading: false,
    timeLeft: null
  }

  _escape = () => {
    const { endFight, escapeAvailable } = this.props

    if (escapeAvailable) {
      endFight({ step: STEP_ESCAPE, isEscape: true })
    }
  }

  _checkAttackProgress = () => {
    const { status, endResult } = this.props

    const attackStarted = status.attackStatus === 'started'
    const attackNotEnd = status.attackStatus === 'end' && !endResult
    const attackInProgress = attackStarted || attackNotEnd

    return attackInProgress
  }

  _getAppHeaderProps = () => {
    const attackInProgress = this._checkAttackProgress()

    const { escapeAvailable, timeLeft, loading, currentMove, userID } = this.props

    const defaultAppHeaderProps = {
      titles: {
        default: {
          ID: 0,
          title: 'Attacking'
        }
      }
    }

    const clientPropsLinks = {
      links: {
        default: {
          ID: 0,
          items: [
            {
              title: 'Back to profile',
              href: `profiles.php?XID=${userID}`,
              label: 'back-to-profile',
              icon: 'Back'
            }
          ]
        }
      }
    }

    const clientPropsLabels = {
      labels: {
        default: {
          ID: 0,
          items: [
            {
              title: `${currentMove} / 25`,
              icon: 'AttackTurn'
            },
            {
              title: <Timer timeLeft={timeLeft} />,
              icon: 'Timeout'
            },
            {
              title: <EscapeButton action={this._escape} escapeAvailable={escapeAvailable} loading={loading} />,
              icon: null
            }
          ]
        }
      }
    }

    const attackInProgressLinks = {
      ...clientPropsLabels,
      links: null
    }

    const attackIdleLinks = {
      ...clientPropsLinks,
      labels: null
    }

    const clientProps = {
      ...defaultAppHeaderProps,
      ...(attackInProgress ? attackInProgressLinks : attackIdleLinks)
    }

    return clientProps
  }

  render() {
    const clientProps = this._getAppHeaderProps()

    return (
      <div className={styles.appHeaderAttackWrap}>
        <AppHeader appID='attack' pageID={0} clientProps={clientProps} />
      </div>
    )
  }
}

export default ContentTitle
