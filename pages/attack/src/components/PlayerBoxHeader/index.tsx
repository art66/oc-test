import React, { PureComponent } from 'react'
import cn from 'classnames'
import { toMoney } from '@torn/shared/utils'
import waify from '@torn/shared/utils/waify'

import ProgressBar from '../ProgressBar'

import { ME } from '../../constants'

import misc from '../../styles/misc.cssmodule.scss'
import s from './playerboxheader.cssmodule.scss'
import icons from '../../styles/icons.cssmodule.scss'
import wai from '../../styles/wai.cssmodule.scss'

import { IProps } from './interfaces'

class PlayerBoxHeader extends PureComponent<IProps> {
  static defaultProps: IProps = {
    damage: 0,
    health: 0,
    healthMax: 0,
    hits: 0,
    lifeBar: 0,
    playername: '',
    mediaType: 'desktop',
    activePlayerLayout: ME.toString(),
    setActivePlayerLayout: () => {},
    userType: undefined,
    color: '',
    damageMitigated: 0,
    isDesktopManualLayout: false
  }

  _handleClick = () => {
    const { userType, setActivePlayerLayout } = this.props
    const modelToShow = userType === 'attacker' ? 'defender' : 'attacker'

    setActivePlayerLayout(modelToShow)
  }

  _commonData = () => {
    const { color, userType, activePlayerLayout } = this.props

    const ariaPlayerHits = 'player-name player-hits player-hits-value'
    const ariaPlayerName = ' player-name player-hits player-hits-value player-damage player-damage-value'
    const ariaPlayerHealth = ' player-health player-health-value player-progress'

    return {
      ariaPlayerName: `${ariaPlayerHits} ${ariaPlayerName} ${ariaPlayerHealth}`,
      classes: cn(misc.boxTitle, s.header, s[color], { [s.activeHeader]: userType === activePlayerLayout })
    }
  }

  _getWaify = (playerName: string) => {
    return waify(playerName)
  }

  _renderUserName = () => {
    const { playername } = this.props

    return (
      <span className={`${s.userName} user-name left`} id={`playername_${playername}`}>
        {playername}
      </span>
    )
  }

  _renderEntry = (...params: any) => {
    const { playername } = this.props
    const [title, attackType, attackValue, attackDamage] = params

    const withPlayerName = this._getWaify(playername)

    return (
      <div key={attackValue} className={s.entry}>
        <i className={cn(icons[`icon${title}`], wai.hideText)} id={withPlayerName(attackType)}>
          {title}
        </i>
        <span id={withPlayerName(attackValue)}>{toMoney(attackDamage)}</span>
      </div>
    )
  }

  _renderEntries = () => {
    const { hits, damage, health, healthMax } = this.props

    const hitsBlock = this._renderEntry('Hits', 'player-hits', 'player-hits', hits)
    const damageBlock = this._renderEntry('Damage', 'player-damage', 'player-damage-value', damage)
    const healthBlock = this._renderEntry('Health', 'player-health', 'player-health-value', `${health} / ${healthMax}`)

    return (
      <div className={s.textEntries}>
        {hitsBlock}
        {damageBlock}
        {healthBlock}
      </div>
    )
  }

  _renderProgressBar = () => {
    const { lifeBar, damageMitigated, healthMax, playername } = this.props
    const withPlayerName = this._getWaify(playername)

    return (
      <ProgressBar
        value={lifeBar > 100 ? 100 : lifeBar}
        mitigatedValue={(damageMitigated / healthMax) * 100}
        className={s.pbWrap}
        ID={withPlayerName('player-progress')}
      />
    )
  }

  render() {
    const { playername } = this.props

    const { ariaPlayerName, classes } = this._commonData()
    const withPlayerName = this._getWaify(playername)

    return (
      <div
        className={classes}
        aria-describedby={withPlayerName(ariaPlayerName)}
        role='button'
        tabIndex={0}
        onKeyDown={undefined}
        onClick={this._handleClick}
      >
        <div className={s.topWrap}>
          {this._renderUserName()}
          {this._renderEntries()}
        </div>
        {this._renderProgressBar()}
      </div>
    )
  }
}

export default PlayerBoxHeader
