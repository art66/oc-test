export interface IProps {
  damage: number
  health: number
  healthMax: number
  hits: number
  lifeBar: number
  mediaType: string
  userType: 'defender' | 'attacker'
  playername: string
  setActivePlayerLayout: (modelToShow: 'defender' | 'attacker') => void
  activePlayerLayout: string
  color: string
  damageMitigated: number
  isDesktopManualLayout: boolean
}
