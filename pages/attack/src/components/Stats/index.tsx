import React from 'react'
import cn from 'classnames'
import { toMoney } from '@torn/shared/utils'

import ProgressBar from '../ProgressBar'

import s from './stats.cssmodule.scss'
import icons from '../../styles/icons.cssmodule.scss'
import misc from '../../styles/misc.cssmodule.scss'
import wai from '../../styles/wai.cssmodule.scss'

import { IProps } from './interfaces'

class Stats extends React.PureComponent<IProps> {
  static defaultProps: IProps = {
    participants: []
  }

  _getMaxHealth = (health: number, healthmax: number) => {
    return (health * 100) / healthmax
  }

  _getParticipantsCount = () => {
    const { participants = [] } = this.props

    const participantsCount = participants && participants.length || 0

    return participantsCount
  }

  _getParticipantsTitle = () => {
    const attackTypeTitle = this._getParticipantsCount() > 1 ? 'Group attack' : 'Solo attack'

    return attackTypeTitle
  }

  _getPlayerData = (health, healthmax, playername) => {
    return (
      <div className={s.playerWrap}>
        <span className={s.playername}>{playername}</span>
        <span className={s.playerDelimier}>-</span>
        <span className={s.playerHealth}>{`${health} / ${healthmax}`}</span>
      </div>
    )
  }

  _getPlayerDescription = (hits: number, damage: number) => {
    const hitsBlock = () => (
      <div className={s.entry}>
        <i className={cn(icons.iconHits, wai.hideText)} title='hits'>
          Hits:
        </i>
        <span>{hits}</span>
      </div>
    )

    const damageBlock = () => (
      <div className={s.entry}>
        <i className={cn(icons.iconDamage, wai.hideText)} title='damage'>
          Damage:
        </i>
        <span>{toMoney(damage)}</span>
      </div>
    )

    return (
      <div className={s.desc}>
        {hitsBlock()}
        {damageBlock()}
      </div>
    )
  }

  _getParticipants = () => {
    const { participants = [] } = this.props

    if (!participants) {
      return null
    }

    return participants.map(({ ID, playername, health, healthmax, hits, damage }) => {
      return (
        <li className={s.row} key={ID} role='gridcell'>
         <div className={s.rowDataWrap}>
            {this._getPlayerData(health, healthmax, playername)}
            {this._getPlayerDescription(hits, damage)}
          </div>
          <ProgressBar value={this._getMaxHealth(Number(health), Number(healthmax))} className={s.progressBar} />
        </li>
      )
    })
  }

  _renderTitle = () => {
    return (
      <div className={cn(s.title, misc.boxTitle)} id='stats-header'>
        <div className={s.titleText}>{this._getParticipantsTitle()}</div>
        <div className={s.titleNumber}>{this._getParticipantsCount()}</div>
      </div>
    )
  }

  _renderList = () => {
    return (
      <ul className={s.participants} role='grid' aria-describedby='stats-header'>
        {this._getParticipants()}
      </ul>
    )
  }

  render() {
    return (
      <div className={s.statsWrap}>
        {this._renderTitle()}
        {this._renderList()}
      </div>
    )
  }
}

export default Stats
