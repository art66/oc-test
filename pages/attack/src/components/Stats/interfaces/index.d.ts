export interface IProps {
  participants: {
    ID: string
    playername: string
    health: number
    healthmax: number
    hits: number
    damage: number
  }[]
}
