import { IState } from '../interfaces/IState'

const initialState: IState = {
  inProgress: false,
  dataLoaded: true,
  preventWSUpdate: false,
  enablePoll: true,
  addAttackWinButton: null,
  attackStatus: null,
  endResult: null,
  finishOptions: null,
  escapeAvailable: false,
  usersLife: {
    attacker: {
      currentLife: null,
      maxLife: null,
      lifeBar: null
    },
    defender: {
      currentLife: null,
      maxLife: null,
      lifeBar: null
    }
  },
  hitsLog: null,
  attacking: null,
  attackerUser: null,
  defenderUser: null,
  currentItems: null,
  currentDefends: null,
  currentTemporaryEffects: null,
  currentFightHistory: null,
  defenderItems: null,
  attackerItems: null,
  defenderAmmo: null,
  attackerAmmo: null,
  currentFightStatistics: null
}

export default initialState
