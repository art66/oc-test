import { createStore, applyMiddleware, compose } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import createSagaMiddleware from 'redux-saga'

import rootReducer from '../controller/reducers'
import rootSaga from '../controller/sagas'
import logger from './helpers/logger'
import initWS from '../controller/websockets'

const configureStore = (preloadedState: any = {}) => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  const sagaMiddleware = createSagaMiddleware()
  const middleware = [sagaMiddleware]

  if (__DEV__) {
    middleware.push(logger)
  }

  const store = createStore(
    rootReducer,
    preloadedState,
    composeEnhancers(responsiveStoreEnhancer, applyMiddleware(...middleware))
  )

  sagaMiddleware.run(rootSaga)
  initWS(store)

  return store
}

export default configureStore
