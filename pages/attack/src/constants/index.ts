export const BASE_URL = '/loader.php?sid=attackData&mode=json'

export const STEP_START = 'startFight'
export const STEP_ATTACK = 'attack'
export const STEP_END = 'finish'
export const STEP_ESCAPE = 'runAway'

export const ME = 'attacker'
export const RIVAL = 'defender'

export const POLL_DELAY = 4000

// media types
export const MT = {
  DESKTOP: 'desktop',
  TABLET: 'tablet',
  PHONE: 'phone'
}

export const EFFECTS_TYPE_TRANSPILER = {
  1: 'effect',
  2: 'effect',
  3: 'distraction',
  4: 'perk'
}

export const INIT_START = 'INIT_START'
export const INIT_DONE = 'INIT_DONE'
export const ATTACK_START = 'ATTACK_START'
export const ATTACK_DONE = 'ATTACK_DONE'
export const FIGHT_START_ATTEMPT = 'FIGHT_START_ATTEMPT'
export const FIGHT_START_DONE = 'FIGHT_START_DONE'
export const FIGHT_FINISH_ATTEMPT = 'FIGHT_FINISH_ATTEMPT'
export const FIGHT_FINISH_DONE = 'FIGHT_FINISH_DONE'
export const ESCAPE_START = 'ESCAPE_START'
export const ESCAPE_DONE = 'ESCAPE_DONE'
export const UPDATE_LOAD = 'UPDATE_LOAD'
export const POLL_ATTEMPT = 'POLL_ATTEMPT'
export const POLL_DONE = 'POLL_DONE'
export const UPDATE_START = 'UPDATE_START'
export const UPDATE_DONE = 'UPDATE_DONE'
export const SET_ACTIVE_PLAYER_LAYOUT = 'SET_ACTIVE_PLAYER_LAYOUT'
export const DEBUG_MESSAGE = 'DEBUG_MESSAGE'
export const CHECK_MANUAL_DESKTOP_MODE = 'CHECK_MANUAL_DESKTOP_MODE'
export const CHECK_DARK_MODE = 'CHECK_DARK_MODE'
export const SHOW_LOG_MODAL_ACTION = 'SHOW_LOG_MODAL_ACTION'
export const ERROR_MODAL = 'ERROR_MODAL'
export const AGGRESSION_UPDATED = 'AGGRESSION_UPDATED'
export const DESTROY_HEALTH_DAMAGE = 'DESTROY_HEALTH_DAMAGE'
export const HIT_UPDATE = 'HIT_UPDATE'
export const SPECTATOR_UPDATE = 'SPECTATOR_UPDATE'
export const ESCAPE_UPDATE = 'ESCAPE_UPDATE'
