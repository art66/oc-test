import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'

export interface IContainer {
  endFight: ({ fightResult }: { fightResult: 'leave' }) => void
  startPoll: () => void
  startInitLoad: () => void
  startUpdateData: () => void
  runCheckManualDesktopMode: (status: boolean) => void
  runDarkModeChecker: (state: boolean) => void
  mediaType: TMediaType | 'phone'
  debugInfo: object
  loading: boolean
  activePlayerLayout: string
  attackStatus: string
  isDesktopManualLayout: boolean
  isAttack: boolean
  lootable: boolean
  endResult: {
    outcome: string
  }
  isFightFinished: boolean
  endTimerDelay: number
  isBDError: boolean
  error: boolean
  enablePoll: boolean
}
