export interface IDistraction {
  ID: number
  title: string
  desc: string
  type: string
  aggressiveMoves: object
}
export interface IAggressionPayload {
  distraction: IDistraction
  statsModifiers?: object
  countdown?: number
}
