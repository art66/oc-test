import { IUserHit } from "./IController";

export interface ILife {
  attacker: {
    currentLife: string;
    lifeBar: number;
    maxLife: string;
  };
  defender: {
    currentLife: string;
    lifeBar: number;
    maxLife: string;
  };
}

export interface IAmmo {
  1: string;
  2: string;
}

export interface IUsersAmmo {
  attackerAmmoStatus: IAmmo;
  defenderAmmoStatus: IAmmo;
}

export interface IStat {
  legend: string[];
  value: number;
}

export interface IStatsModifiers {
  defense: IStat;
  dexterity: IStat;
  speed: IStat;
  strength: IStat;
  damage: IStat;
}

export interface IPlayer {
  lastAction: number;
  playername: string;
  statsModifiers: IStatsModifiers
}

export interface IAttacker extends IPlayer {}

export interface IDefender extends IPlayer {
  userID: string
}

export interface IFightHistory {
  color: string;
  icon: string;
  text: string;
  timeAgo: number;
}

export interface IEndResult {
  result: string,
  info: string,
  outcome: string,
  step: string
}

export interface IEffect  {
  ID: number | string
  timeLeft: number
  level: string
  class: string
  type: number
  isStackable?: boolean
  stackCount?: number
  stackCountMax?: number
}

export interface IFightStatistics {
  [userID: number]: {
    damage: number;
    hits: number;
    moves: number;
    playername: string;
    userID: string;
  }[];
}

export interface IItem {
  acc: number;
  accuracy: string;
  arm: string;
  armouryID: string;
  dmg: string;
  name: string;
  stealthLevel: number;
  upgrades: string;
  slotsMap: string
  slotsMapBackground: string
  slotsMapMask: string
  slotsMapMaskHair: string
}

export interface ILegacyItemWrap {
  item: IItem[];
}

export interface IUserItems {
  [x: number]: ILegacyItemWrap;
}

export interface IItems {
  attackerItems: IUserItems
  defenderItems: IUserItems
}

export interface IDamageDealed {
  attack: {
    itemDamage: number;
    specialEffect: string;
    value: number;
  };
  damageMitigated: number;
  damageMitigatedPure: number;
  damagePure: number;
  defence: number;
  roundsFired: number;
  specialEffect: string | "critical hit"; // TODO
  value: number;
}

export interface IAttacking {
  attackResult: "ok" | "started" | "finished" | string // TODO
  attacker: {
    damageMitigated: number;
    damageValue: number;
    hitInfo: {
      zone: "Head" | string // TODO
      x: number;
      y: number;
    };
    result: "CRITICAL" | string // TODO
  };
  currentMove: number;
  defender: {
    damageMitigated: number;
    damageValue: number;
    hitInfo: {
      target: "0" | string // TODO
      x: number;
      y: number;
    };
    result: "MISS" | string // TODO
  };
}

export interface IDefends {
  ID: string;
  attackerID: string;
  defenderID: string;
  health: string;
  healthmax: string;
  moves: string;
  playername: string;
  status: string;
}

export type TFinishOptions = 'leave' | 'mug' | 'hosp'

export interface IAddAttackWinButton {
  text: string
  winFightAction: string
}

export interface ICurrentItems {
  user1EquipedItemID: number
  user1EquipedItemImageID: number
  user2EquipedItemID: number
  user2EquipedItemImageID: number
}

export interface IStartButtonTimer {
  text: string
  timeLeft: number
}

export interface IState {
  inProgress: boolean
  dataLoaded: boolean
  preventWSUpdate: boolean
  enablePoll: boolean
  startButtonTitle?: string
  startButtonTimer?: IStartButtonTimer
  addAttackWinButton?: IAddAttackWinButton
  attackStatus: 'end' | string
  escapeAvailable: boolean
  endResult: IEndResult
  finishOptions: TFinishOptions[]
  usersLife: ILife
  hitsLog: IUserHit
  attacking: IAttacking
  currentDefends: IDefends[]
  currentItems: ICurrentItems
  currentTemporaryEffects: IEffect[]
  currentFightHistory: IFightHistory
  currentFightStatistics: IFightStatistics
  attackerUser: IAttacker
  defenderUser: IDefender
  defenderItems: IUserItems
  attackerItems: IUserItems
  defenderAmmo: IAmmo
  attackerAmmo: IAmmo
}
