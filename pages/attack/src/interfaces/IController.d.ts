import { IEndResult, IState, IAttacker, IDefender, IEffect, IFightHistory, ILife, IItems, IAttacking, IUsersAmmo, IFightStatistics, IDefends, TFinishOptions, IAddAttackWinButton, ICurrentItems } from './IState';
import { IAggressionPayload } from './IWebsockets'

export interface IType {
  type: string
}

export interface IPayload {
  payload: IState
}

export interface ICommon {
  debugInfo: string
  isDesktopManualLayout: boolean
  isDarkModeEnabled: boolean
}

export interface IDebug {
  debugMessage: string
  options?: any
}

export interface ICheckDesktopMode extends IType {
  status: boolean
}

export interface ICheckDarkMode extends IType {
  status: boolean
}

export interface IFightStart extends IType, IPayload {}

export interface IFightFinishAttempt extends IType {
  step: string
  fightResult: string
  options?: object
}

export interface IFightEnd extends IType, IPayload {}

export interface IEscapeAttempt extends IType {
  step: string
}

export interface IEscapeDone extends IType, IPayload {}

export interface IAttackAttempt extends IType {
  weaponID: number
}

export interface IAttackDone extends IType, IPayload {}

export interface IInit extends IType, IPayload {}

export interface IPoll extends IType, IPayload {}

export interface IUpdate extends IType, IPayload {}

export interface ILayoutSelect extends IType {
  layoutModel: string
}

export interface IShowModalLog extends IType {
  error: string
}

export interface IErrorModal extends IType {
  error: string
}

export interface IAggressionUpdate extends IAggressionPayload, IType {}

export interface IDestroyHealthDamage extends IType {
  userType: 'defender' | 'attacker'
}

export interface IHit {
  ID: string
  TimeCreated: string
  attackID: string
  attackResult: 'hit' | 'miss' | 'hitOld' | 'missOld'
  defenderID: string
  userID: string
  x: string
  y: string
}

export interface IUserHit {
  [x: number]: IHit
}

export interface IWSPayload {
  startButtonTitle?: string
  addAttackWinButton?: IAddAttackWinButton
  attackStatus?: 'end' | string
  escapeAvailable?: boolean
  endResult?: IEndResult;
  finishOptions: TFinishOptions[];
  defends: IDefends[];
  ammo: IUsersAmmo;
  items: IItems;
  hitsLog: IUserHit;
  life: ILife;
  effects: IEffect[];
  attacking: IAttacking;
  currentItems: ICurrentItems;
  currentFightHistory: IFightHistory;
  statistics: IFightStatistics;
  attacker: IAttacker;
  defender: IDefender;
}

export interface IActivePlayerLayout {
  activePlayerLayout: 'attacker' | 'defender'
}
