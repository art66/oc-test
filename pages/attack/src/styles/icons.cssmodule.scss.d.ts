// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'globalSvgShadow': string;
  'icon': string;
  'iconDamage': string;
  'iconHealth': string;
  'iconHits': string;
  'iconStealth': string;
}
export const cssExports: CssExports;
export default cssExports;
