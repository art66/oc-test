# Attack - Torn


## 4.6.0
 * Major refactor of core data structures.

## 4.5.0
 * Signicically improved perfomance in group attacks!

## 4.4.0
 * Added Carousel for Logs & Stats panels layout on touchdevices!
 * Fixed several style glitches.

## 4.3.0
 * Improved selectors core!

## 4.2.0
 * Added Websockets channel!
 * Added Distraction brand new icons layout along with its counter logic.

## 4.1.1
 * Fixed attacker bar zeroing in case while several tabs to play used at the same time!

## 4.1.0
 * Added Websocket API!

## 4.0.0
 * Major Attack update!
 * All the Components were moved to TS workaround.

## 3.0.0
 * Major Attack update!
 * Improved core stability and predicting.
 * Added typization and well knows OOP structure.
 * Fixed some bugs.

## 2.18.1
 * Fixed ProgressBar functionality.
 * wai.cssmodule is moved to wai.cssmodule.scss extension.
 * ProgressBar was moved to TS.

## 2.17.0
 * Added serviceWorkers for prefetching static data.
 * Added effects icons scrolling for a better UX.
 * Improved current tooltips system for effects.
 * Fixed missing tooltips for special icons around weapons.

## 2.16.2
 * Updated halloween layout.

## 2.16.1
 * Fixed broken modules in package.json.
 * Added global halloween text font style.

## 2.15.0
 * Added background layout for armory items.

## 2.14.2
 * Fixed broken styles in PlayersBox.

## 2.14.1
 * Fixed cycle dependency eslint error.

## 2.14.0
 * Updated React bundles up to v.16.9.0.
 * Added correct HMR setup.
 * Partially rewritten main core.

## 2.13.3
 * Fixed broken IE11 css rules.

## 2.13.3
 * Bump up react-redux version.

## 2.13.2
 * Fixed attack redirection after 30 sec in case of 3 buttons choose screen apeear.

## 2.13.1
 * Fixed attack redirection after 30 sec in case of 3 buttons choose screen apeear.

## 2.13.0
 * Fixed attack items dissapear once pooling return a server responce.

## 2.12.1
 * Update tooltips for bonuses & attachments in Attack.
 * Fixed uncontrolled button switching while rival is out of fight in Attack.

## 2.11.4
 * Added titles appear for WeaponBox items.

## 2.10.4
 * Temporary revert pooling once the fight is over.

## 2.10.3
 * Fixed reordering and flashing for effect icons layout.

## 2.10.2
 * Hot-fix for the Models Overlay while attack not started.

## 2.10.1
 * Modified Overlay Component appear in way to hide only model, without its/emeny items.
 * Fixed error in Weapon Component PropTypes.

## 2.9.1
 * Created and Implemented a fresh new img-to-canvas functionality for better image handling inside WeaponBox Component.
 * Removed legacy-based canvas injection in Weapon section.
 * Fixed css layout for mobile/tablet mode in Weapon section.

## 2.8.3
 * Reverted last AttackLayout update.
 * Switched onClick event result while click on PersonsHeader on: 1. my model + their weapons and 2. their model + my weapon.

## 2.8.2
 * Fixed Attack Layout after previous update.

## 2.8.1
 * Switched Attack Layout on: 1. my model + their weapons and 2. their model + my weapon.
 * Fixed layout for a WeaponBox Component.

## 2.8.0
 * Refactored WeaponBox Component.

## 2.7.1
 * Implemented new stuff for data refetching while fight is over without refreshing the page.
 * Fixed tests.

## 2.6.3
 * Tiny refactor for a couple of files in attack.
 * Fixed tests.

## 2.6.2
 * Fixed dataFetching while fight is over.

## 2.6.1
 * Added fix for mobile layout in AppHeader.

## 2.6.0
 * Implemented brand new header.

## 2.5.1
 * Laying the perks under the effects ones

## 2.5.0
 * Refactored ModelLayout Component.
 * Roll back z-index for Perk bcg appear.
 * Added code for handling svg-crop system.

## 2.4.2
 * Fixed masks dimensions in Layout.
 * Fixed Perk icon timestamp while on top level.

## 2.4.1
 * Fixed masks layout in ModeLayers.

## 2.4.0
 * Refactored model layer Component.
 * Created Masks Component for better handling masks on client without back-end legacy masks.

## 2.3.0
 * Effects backgound has been separated on Perk/Effect Component due to the itmer issue.
 * Fixed types in ContentTitle.

## 2.2.0
 * Added loot timestamp layout.

## 2.1.0
 * Added manual desktop layout mode checking.

## 2.0.1
 * Fixed .scss imports in .js files.

## 2.0.0
 * Refactored almoust all style sheets of the components.
 * Make app responsive layout activated only if manual desktop layout isn't set.

## 1.19.1
 * Added names adapter for IconEffects.

## 1.18.1
 * Added incapsulated timer for forcibly move user on congratulation screen from leave/mug/hosp one.
 * Refactored DialogEnd container.

## 1.8.2
 * Fixed Effects backgronds.

## 1.8.1
 * Fixed timeLeft text color.

## 1.8.0
 * Added 15 brand new effects backgrounds.

## 1.7.2
 * Fix for leave timer in loot mode.

## 1.7.1
 * Refactored Stats and PlayerBoxHeader Components.
 * Fixed playerName overdropping length.

## 1.6.0
 * All SVG Effect icons has been connected with the front-end part.

## 1.5.1
 * Added all Effects icons to the SVG lib.
 * Fixed title in Attack mode.

## 1.4.1
 * Refactored ContentTitle Component.
 * Minor fixes.

## 1.3.0
 * Added effects icons sorting feature.

## 1.2.1
 * Added new looting feature.
 * Added TS types for EffectIcons Components.
 * Refactored legacy EffecIcons components.
 * Refactored legacy EffecIcons style sheets.
 * Replaced legacy .png icons on .svg ones.
 * Minor fixes.

## 1.1.1
 * Improved fetch update requsts.
 * Fixed minor error.
 * Upgraded actions/reducers code structures.

## 1.0.2
 * Added fetch request on each this tab focus.

## 1.0.2
 * Fixed Timers in СontentTitle Component.

## 1.0.1
 * Fixed Timers in Log Component by providing Tiker Component with own action handler.
