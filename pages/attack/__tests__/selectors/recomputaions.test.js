// import getArmourSelector from '../../src/controller/selectors/getArmourSelector'
import getBulletsSelector from '../../src/controller/selectors/getBulletsSelector'
import getHeaderSelector from '../../src/controller/selectors/getHeaderSelector'
import getWeaponsSelector from '../../src/controller/selectors/getWeaponsSelector'
import status from '../../src/controller/selectors/status'
import initialState from '../../src/store/initialState'
import { ME, RIVAL } from '../../src/constants'

const state = {
  attack: initialState
}

describe('recomputations of selectors ', () => {
  test('header selector unit test', () => {
    const s1 = getHeaderSelector()
    const s2 = getHeaderSelector()

    s1(state, { userType: ME })
    s2(state, { userType: RIVAL })
    s1(state, { userType: ME })
    s2(state, { userType: RIVAL })
    expect(s1.recomputations()).toBe(2)
    expect(s2.recomputations()).toBe(2)
  })

  // Don't need it since we move Model to the shared folder
  // test('armour selector unit test', () => {
  //   const s1 = getArmourSelector()
  //   const s2 = getArmourSelector()

  //   s1(state, { userType: ME })
  //   s2(state, { userType: RIVAL })
  //   s1(state, { userType: ME })
  //   s2(state, { userType: RIVAL })
  //   expect(s1.recomputations()).toBe(1)
  //   expect(s2.recomputations()).toBe(1)
  // })

  test('weapons selector unit test', () => {
    const s1 = getWeaponsSelector()
    const s2 = getWeaponsSelector()

    s1(state, { userType: ME })
    s2(state, { userType: RIVAL })
    s1(state, { userType: ME })
    s2(state, { userType: RIVAL })
    expect(s1.recomputations()).toBe(2)
    expect(s2.recomputations()).toBe(2)
  })

  test('status selector unit test', () => {
    status(state)
    status(state)
    expect(status.recomputations()).toBe(1)
  })

  test('bullets selector unit test', () => {
    const s1 = getBulletsSelector()
    const s2 = getBulletsSelector()

    s1(state, { userType: ME })
    s2(state, { userType: RIVAL })
    s1(state, { userType: ME })
    s2(state, { userType: RIVAL })
    expect(s1.recomputations()).toBe(2)
    expect(s2.recomputations()).toBe(2)
  })
})
