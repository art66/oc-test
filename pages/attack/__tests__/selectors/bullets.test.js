import getBulletsSelector from '../../src/controller/selectors/getBulletsSelector'
import { ME, RIVAL } from '../../src/constants'
import initialState from '../../src/store/initialState'

const state = {
  attack: initialState
}

describe('getBulletsSelector selector unit test ', () => {
  test('my', () => {
    expect(getBulletsSelector()(state, { userType: ME })).toEqual([])
  })
  test('rivals', () => {
    expect(getBulletsSelector()(state, { userType: RIVAL })).toEqual([])
  })
})
