import getWeaponsSelector from '../../src/controller/selectors/getWeaponsSelector'
import { ME } from '../../src/constants'

import state from '../mocks/weaponsState'

describe('getWeaponsSelector selector unit test ', () => {
  const myWeapons = getWeaponsSelector()(state, { userType: ME })

  test('weapon length', () => {
    expect(myWeapons.length).toEqual(5)
  })
  test('weapon 0', () => {
    expect(Number(myWeapons[0].ID)).toEqual(28)
    expect(myWeapons[0].active).toEqual(true)
    expect(myWeapons[0].type).toEqual('full')
  })
  test('weapon 1', () => {
    expect(Number(myWeapons[1].ID)).toEqual(17)
    expect(myWeapons[1].active).toEqual(true)
    expect(myWeapons[1].type).toEqual('full')
  })
  test('weapon 2', () => {
    expect(Number(myWeapons[2].ID)).toEqual(1)
    expect(myWeapons[2].active).toEqual(true)
    expect(myWeapons[2].type).toEqual('full')
  })
  test('weapon 3 - temporary', () => {
    expect(myWeapons[3].boxTitle).toEqual('Temporary')
    expect(myWeapons[3].active).toEqual(false)
    expect(myWeapons[3].type).toEqual('full')
  })
  test('weapon 4 - fists', () => {
    expect(myWeapons[4].boxTitle).toEqual('Fists')
    expect(myWeapons[4].type).toEqual('small')
  })
})
