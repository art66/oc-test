import wsResponseAdapter from '../../src/controller/helpers/wsResponseAdapter'
import { attack as attackState } from '../mocks/initialState'
import { wsPayload as payloadState } from '../mocks/helpers'

describe('wsResponseAdapter reducer', () => {
  it('should return debug message', () => {
    const newState = {
      ...attackState,
      attackStatus: 'inProgress'
    }

    expect(wsResponseAdapter(attackState, payloadState)).toEqual(newState)
  })
})
