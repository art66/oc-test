import { IWSPayload } from '../../src/interfaces/IController'

export const wsPayload: IWSPayload = {
  addAttackWinButton: null,
  attackStatus: 'inProgress',
  escapeAvailable: false,
  endResult: null,
  finishOptions: null,
  defends: null,
  ammo: {
    defenderAmmoStatus: null,
    attackerAmmoStatus: null
  },
  items: {
    defenderItems: null,
    attackerItems: null
  },
  life: {
    attacker: {
      currentLife: null,
      lifeBar: null,
      maxLife: null
    },
    defender: {
      currentLife: null,
      lifeBar: null,
      maxLife: null
    }
  },
  effects: null,
  attacking: null,
  currentFightHistory: null,
  statistics: null,
  attacker: null,
  defender: null
}
