import { Browser } from 'redux-responsive'
import { ICommon, IActivePlayerLayout } from '../../src/interfaces/IController'
import { IState } from '../../src/interfaces/IState'

export const layout: IActivePlayerLayout = {
  activePlayerLayout: 'attacker'
}

export const attack: IState = {
  addAttackWinButton: null,
  attackStatus: null,
  attackerAmmo: null,
  attackerItems: null,
  attackerUser: null,
  attacking: null,
  currentDefends: null,
  currentFightStatistics: null,
  dataLoaded: true,
  defenderAmmo: null,
  defenderItems: null,
  escapeAvailable: false,
  finishOptions: null,
  inProgress: false,
  hitsLog: null,
  preventWSUpdate: false,
  currentTemporaryEffects: null,
  defenderUser: null,
  endResult: null,
  usersLife: {
    attacker: {
      currentLife: null,
      lifeBar: null,
      maxLife: null
    },
    defender: {
      currentLife: null,
      lifeBar: null,
      maxLife: null
    }
  },
  enablePoll: true,
  currentFightHistory: null
}

export const browser: Browser = {
  _responsiveState: true,
  breakpoints: {
    desktop: Infinity,
    phone: 600,
    tablet: 1000
  },
  greaterThan: {
    desktop: false,
    phone: false,
    tablet: false
  },
  height: undefined,
  lessThan: {
    desktop: false,
    phone: false,
    tablet: false
  },
  mediaType: 'desktop',
  orientation: null,
  width: undefined
}

export const common: ICommon = {
  debugInfo: null,
  isDesktopManualLayout: false
}

export default {
  activePlayerLayout: layout,
  attack,
  browser,
  common
}
