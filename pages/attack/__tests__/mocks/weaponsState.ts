import initialState from '../../src/store/initialState'

export default {
  attack: {
    ...initialState,
    attackerItems: {
      1: {
        item: [
          {
            armouryID: '2985822463',
            armoryID: '2985822463',
            userID: '1435333',
            itemID: '28',
            type: 'Weapon',
            equiped: '1',
            ammo: '12',
            type2: 'Primary',
            dmg: 151.14,
            ID: '28',
            arm: '0',
            accuracy: 126.1,
            equipSlot: '1',
            name: 'Benelli M4 Super',
            maxammo: 9,
            ammotype: '1',
            rofmin: '2',
            rofmax: '3',
            isDual: '0',
            isMask: '0',
            stealthLevel: '2.8',
            weptype: '5',
            upgrades: '',
            armourNotCover: '',
            currentUpgrades: [
              {
                title: '5mw Laser',
                upgradeID: '6',
                icon: '5mw-laser'
              },
              {
                title: 'High Capacity Mags',
                upgradeID: '13',
                icon: 'high-capacity-mags'
              }
            ]
          }
        ],
        overall: [
          {
            minCritHitChance: 4,
            maxCritHitChance: 4
          }
        ]
      },
      2: {
        item: [
          {
            armouryID: '2894016724',
            armoryID: '2894016724',
            userID: '1435333',
            itemID: '17',
            type: 'Weapon',
            equiped: '2',
            ammo: '14',
            type2: 'Secondary',
            dmg: 80.28,
            ID: '17',
            arm: '0',
            accuracy: 104.49,
            equipSlot: '2',
            name: 'Beretta 92FS',
            maxammo: '20',
            ammotype: '2',
            rofmin: '3',
            rofmax: '6',
            isDual: '0',
            isMask: '0',
            stealthLevel: '4.5',
            weptype: '3',
            upgrades: '',
            armourNotCover: '',
            currentUpgrades: [
              {
                title: 'Hair Trigger',
                upgradeID: '24',
                icon: 'hair-trigger'
              }
            ]
          }
        ],
        overall: ''
      },
      3: {
        item: [
          {
            armouryID: '2081379532',
            armoryID: '2081379532',
            userID: '1435333',
            itemID: '1',
            type: 'Weapon',
            equiped: '3',
            ammo: '0',
            type2: 'Melee',
            dmg: 2,
            ID: '1',
            arm: '0',
            accuracy: 121.3,
            equipSlot: '3',
            name: 'Hammer',
            maxammo: '0',
            ammotype: '0',
            rofmin: '0',
            rofmax: '0',
            isDual: '0',
            isMask: '0',
            stealthLevel: '7.6',
            weptype: '8',
            upgrades: '',
            armourNotCover: ''
          }
        ],
        overall: ''
      },
      4: {
        item: [
          {
            armouryID: '2894016687',
            armoryID: '2894016687',
            userID: '1435333',
            itemID: '33',
            type: 'Armour',
            equiped: '4',
            ammo: '0',
            type2: 'Defensive',
            dmg: '0',
            ID: '33',
            arm: 40.23,
            accuracy: '0',
            equipSlot: '4',
            name: 'Police Vest',
            maxammo: '0',
            ammotype: '0',
            rofmin: '0',
            rofmax: '0',
            isDual: '0',
            isMask: '0',
            stealthLevel: '0',
            weptype: '0',
            upgrades: null,
            armourNotCover: ''
          }
        ],
        overall: ''
      },
      8: {
        item: [
          {
            armouryID: '2894016731',
            armoryID: '2894016731',
            userID: '1435333',
            itemID: '649',
            type: 'Armour',
            equiped: '8',
            ammo: '0',
            type2: 'Defensive',
            dmg: '0',
            ID: '649',
            arm: 19.51,
            accuracy: '0',
            equipSlot: '8',
            name: 'Leather Boots',
            maxammo: '0',
            ammotype: '0',
            rofmin: '0',
            rofmax: '0',
            isDual: '0',
            isMask: '0',
            stealthLevel: '0',
            weptype: '0',
            upgrades: null,
            armourNotCover: ''
          }
        ],
        overall: ''
      },
      999: {
        item: [
          {
            type: 'Weapon',
            name: 'Fists',
            dmg: 1,
            accuracy: 100,
            ID: 999,
            stealthLevel: 3
          }
        ]
      }
    }
  }
}
