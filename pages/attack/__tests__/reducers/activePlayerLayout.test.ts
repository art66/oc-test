import reducer from '../../src/controller/reducers/activePlayerLayout'
import { layout as layoutState } from '../mocks/initialState'
import { SET_ACTIVE_PLAYER_LAYOUT } from '../../src/constants'

describe('activePlayerLayout reducer ', () => {
  const MODELS = {
    attacker: 'attacker',
    defender: 'defender'
  }

  it('should return current player layout - attacker', () => {
    const action = {
      type: SET_ACTIVE_PLAYER_LAYOUT,
      layoutModel: MODELS.attacker
    }

    const layoutStateNext = {
      ...layoutState,
      activePlayerLayout: MODELS.attacker
    }

    expect(reducer(layoutState, action)).toEqual(layoutStateNext)
  })
  it('should return new player layout - defender', () => {
    const action = {
      type: SET_ACTIVE_PLAYER_LAYOUT,
      layoutModel: MODELS.defender
    }

    const layoutStateNext = {
      ...layoutState,
      activePlayerLayout: MODELS.defender
    }

    expect(reducer(layoutState, action)).toEqual(layoutStateNext)
  })
})
