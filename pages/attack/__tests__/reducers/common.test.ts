import reducer from '../../src/controller/reducers/common'
import { common as commonState } from '../mocks/initialState'
import { DEBUG_MESSAGE, CHECK_MANUAL_DESKTOP_MODE } from '../../src/constants'

describe('common reducer', () => {
  it('should return debug message', () => {
    const action = {
      type: DEBUG_MESSAGE,
      debugMessage: 'test'
    }

    const commonStateNext = {
      ...commonState,
      debugInfo: 'test'
    }

    expect(reducer(commonState, action)).toEqual(commonStateNext)
  })
  it('should set manual desktop mode', () => {
    const action = {
      type: CHECK_MANUAL_DESKTOP_MODE,
      status: true
    }

    const commonStateNext = {
      ...commonState,
      isDesktopManualLayout: true
    }

    expect(reducer(commonState, action)).toEqual(commonStateNext)
  })
})
