import reducer from '../../src/controller/reducers/attack'
import { attack as attackState } from '../mocks/initialState'
import { INIT_DONE } from '../../src/constants'

describe('attack reducer ', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual(attackState)
  })
  it('should keep old maxLife value when usersLife is half baked', () => {
    const action = {
      type: INIT_DONE,
      payload: {
        usersLife: {
          attacker: {},
          defender: {
            currentLife: 1
          }
        }
      }
    }

    const attackStateNext = {
      ...attackState,
      usersLife: {
        ...attackState.usersLife,
        defender: {
          ...attackState.usersLife.defender,
          currentLife: 1
        }
      }
    }

    expect(reducer(attackState, action)).toEqual(attackStateNext)
  })
  it('should keep old maxLife value when usersLife is missing', () => {
    const action = {
      type: INIT_DONE,
      payload: {}
    }

    const attackStateNext = {
      ...attackState,
      usersLife: {
        ...attackState.usersLife,
        defender: {
          ...attackState.usersLife.defender
        }
      }
    }

    expect(reducer(attackState, action)).toEqual(attackStateNext)
  })
})
