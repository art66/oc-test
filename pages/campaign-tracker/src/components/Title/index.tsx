import React, { Component } from 'react'
import { connect } from 'react-redux'
import Checkbox from '@torn/shared/components/Checkbox'
import { setFilter, fetchCampaigns } from '../../actions'
import { Filters } from '../../models/Filters'
import s from './styles.cssmodule.scss'

interface IProps {
  filters: Filters
  loading: boolean
  setFilter: (filter: object) => void
  fetchCampaigns: () => void
}

class Title extends Component<IProps> {
  handleFilterChange(filter: string, checked: boolean) {
    this.props.setFilter({ [filter]: checked })
    this.props.fetchCampaigns()
  }

  render() {
    const { filters, loading } = this.props

    return (
      <div className={s.title}>
        Campaign Tracker {loading && (' (loading...)')}
        <Checkbox
          id='twoWeeksOnly'
          label='Last two weeks only'
          checked={filters.twoWeeks}
          containerClassName={s.checkbox}
          onChange={e => this.handleFilterChange('twoWeeks', e.target.checked)}
        />
        <Checkbox
          id='actsOnly'
          label='Acts only'
          checked={filters.actsOnly}
          containerClassName={s.checkbox}
          onChange={e => this.handleFilterChange('actsOnly', e.target.checked)}
        />
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    filters: state.campaigns.filters,
    loading: state.campaigns.loading
  }),
  {
    setFilter,
    fetchCampaigns
  }
)(Title)
