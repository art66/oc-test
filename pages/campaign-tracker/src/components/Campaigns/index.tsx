import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { orderBy } from 'lodash'
import { toMoney } from '@torn/shared/utils'
import { select, setSort, fetchCampaigns, fetchCountries } from '../../actions'
import { Campaign } from '../../models/Campaign'
import { Sort } from '../../models/Sort'
import Tooltip from './Tooltip'

import s from './styles.cssmodule.scss'
const ROW_HEIGHT = 34
const OFFSET = 500

type Props = {
  campaigns: Campaign[]
  selected: string
  sort: Sort
  select: (campaign: string) => void
  setSort: (fieldBy: string) => void
  fetchCampaigns: () => void
  fetchCountries: () => void
}

class Campaigns extends Component<Props> {
  constructor(props: Props) {
    super(props)
    this.state = { scrollY }
    props.fetchCampaigns()
  }

  componentDidMount() {
    window.addEventListener('scroll', this.forceRender)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.forceRender)
  }

  forceRender = () => {
    this.forceUpdate()
  }

  handleSelect(campaign: Campaign): void {
    if (campaign.campaign) {
      const selected = campaign.campaign !== this.props.selected ? campaign.campaign : ''

      this.props.select(selected)

      if (!campaign.countries) {
        this.props.fetchCountries()
      }
    }
  }

  order(fieldBy: string): void {
    this.props.setSort(fieldBy)
  }

  getTableHeader() {
    const { sort } = this.props
    const getClass = (field: string) =>
      cn(s.cell, s[field], { [s.selected]: sort.by === field, [s.asc]: sort.asc, [s.desc]: !sort.asc })

    return (
      <div className={cn(s.row, s.header)}>
        <div onClick={() => this.order('campaign')} className={getClass('campaign')}>Campaign</div>
        <div id='visits' onClick={() => this.order('visits')} className={getClass('visits')}>Visits</div>
        <div id='acts' onClick={() => this.order('acts')} className={getClass('acts')}>Acts</div>
        <div id='engaged' onClick={() => this.order('engaged')} className={getClass('engaged')}>Engaged</div>
        <div id='actives' onClick={() => this.order('actives')} className={getClass('actives')}>Actives</div>
        <div id='donators' onClick={() => this.order('donators')} className={getClass('donators')}>Donators</div>
        <div id='referrals' onClick={() => this.order('referrals')} className={getClass('referrals')}>Referrals</div>
        <div id='today' onClick={() => this.order('today')} className={getClass('today')}>Today</div>
        <div id='tier' onClick={() => this.order('tier')} className={getClass('tier')}>Tier</div>
        <div id='age' onClick={() => this.order('age')} className={getClass('age')}>Age</div>
        <div id='score' onClick={() => this.order('score')} className={getClass('score')}>Score</div>
        <Tooltip parent='visits'>
          These are <b>unique user visits</b> to the site, regardless of whether they signup. Aka clicks.
          Limited to <b>one per unique IP address</b>.
        </Tooltip>
        <Tooltip parent='acts'>
          These are <b>unique, validated signups</b> with confirmed email address. Limited to <b>one per unique IP</b>.
        </Tooltip>
        <Tooltip parent='engaged'>
          These are users who have shown some <b>interest in the site after signing up</b>. They have completed the
          tutorial missions and <b>logged in 24 hours after they signed up</b>. These are all accounts signed up under
          this campaign, <b>not limited by unique IP addresses</b>.
        </Tooltip>
        <Tooltip parent='actives'>
          These are users that have <b>spent at least 6 hours on the site</b>, their future looks bright. This metric
          can take up to a week become reliable. These are all accounts signed up under this campaign, <b>not limited by
            unique IP addresses
                                                                                                       </b>.
        </Tooltip>
        <Tooltip parent='donators'>
          These are <b>users who have donator status</b>. Regardless of whether they paid for it, bought it with TC cash
          or earned it by referring other people. This metric can take anywhere from 2-8 weeks to become reliable. These
          are all accounts signed up under this campaign, <b>not limited by unique IP addresses</b>.
        </Tooltip>
        <Tooltip parent='referrals'>
          This increases by 1 whenever <b>one of the users from this campaign invites someone else to the game</b>. Upon
          friends signup & validation of email. These are <b>total referrals for all accounts signed up under this
            campaign, not limited by unique IP addresses
                                                          </b>.
        </Tooltip>
        <Tooltip parent='today'>
          These are <b>users who signed up at least 48 hours ago, and are active today</b>. This metric isn't great for
          comparing with other campaigns which differ in start date. But interesting nonetheless. These are <b>all
            accounts signed up under this campaign, not limited by unique IP addresses
                                                                                                            </b>.
        </Tooltip>
        <Tooltip parent='score'>
          This number is <b>generated by comparing acts, returns and actives</b> to determine the success of the
          campaign.
        </Tooltip>
      </div>
    )
  }

  getId = (c: Campaign, field: string) => field + c.campaign.replace(/[^\w-]/g, '') + (c.country || '')

  getClass = (field: string, rate?: string) => cn(s.cell, s[field], s[rate])

  getTooltip(c: Campaign, field: string) {
    if (c[field] && c[field].tooltip) {
      return (
        <Tooltip parent={this.getId(c, field)} arrow='center' position='top'>
          <div dangerouslySetInnerHTML={{ __html: c[field].tooltip }} />
        </Tooltip>
      )
    }
  }

  getCells = (c: Campaign, fields: string[]) => fields.map(field => {
    const key = this.getId(c, field)
    const className = this.getClass(field, c[field] && c[field].rate)
    const tooltip = this.getTooltip(c, field)
    let value = typeof c[field] === 'object' ? c[field].value : c[field]

    value = value === -1 ? '--' : value
    value = typeof value === 'number' ? toMoney(value.toString()) : value

    return (
      <div key={key} className={className} id={key}>
        {value}
        {tooltip}
      </div>
    )
  })

  getCampaigns(campaigns: Campaign[] = this.props.campaigns) {
    const { selected, sort } = this.props
    let sortedCampaigns = [...campaigns]

    if (!sortedCampaigns || !sortedCampaigns.length) {
      return <div />
    }

    if (sort.by) {
      const campaign = campaigns[0]
      const sortBy = campaign[sort.by].value ? [`${sort.by}.value`] : [sort.by]

      sortedCampaigns = orderBy(sortedCampaigns, sortBy, [sort.asc ? 'asc' : 'desc'])
    }

    const selectedI = sortedCampaigns.findIndex(c => c.campaign === selected)

    return sortedCampaigns.map((c, i) => {
      const isSelected = selected === c.campaign
      let pos = i * ROW_HEIGHT

      if ((selectedI > 0 && selectedI < i && sortedCampaigns[selectedI] && sortedCampaigns[selectedI].countries)) {
        pos += sortedCampaigns[selectedI].countries.length * ROW_HEIGHT
      }

      const visible = (pos + OFFSET > window.scrollY) && (pos < window.scrollY + OFFSET + window.outerHeight + OFFSET)

      if (!visible && !isSelected && !c.country) {
        return (
          <div
            key={c.campaign}
            className={cn(s.row, s.empty)}
          />
        )
      }

      const fields = ['visits', 'acts', 'engaged', 'actives', 'donators', 'referrals', 'today', 'tier', 'age', 'score']
      const cells = this.getCells(c, fields)

      return (
        <div
          key={c.campaign + (c.country || '')}
          className={cn(s.row, { [s.selected]: c.campaign === selected })}
          onClick={() => this.handleSelect(c)}
        >
          <div className={cn(s.cell, s.name)}>{c.country || c.campaign}</div>
          {cells}
          {isSelected && c.countries && this.getCampaigns(c.countries)}
        </div>
      )
    })
  }

  render() {
    const header = this.getTableHeader()
    const campaigns = this.getCampaigns()

    return (
      <div className={s.campaigns}>
        {header}
        {campaigns}
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    campaigns: state.campaigns.list,
    selected: state.campaigns.selected,
    sort: state.campaigns.sort
  }),
  {
    fetchCampaigns,
    fetchCountries,
    select,
    setSort
  }
)(Campaigns)
