import React, { Component } from 'react'
import PortalTooltip from '@torn/shared/components/Tooltip'

interface IProps {
  arrow?: string
  parent: string
  position?: string
  style?: any
}

class Tooltip extends Component<IProps> {
  static defaultProps = {
    position: 'bottom'
  }

  render() {
    return (
      <PortalTooltip {...this.props} style={{ style: { maxWidth: 250, lineHeight: 1.25 } }} />
    )
  }
}

export default Tooltip
