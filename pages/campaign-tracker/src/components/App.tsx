import React from 'react'
import Title from './Title'
import Campaigns from './Campaigns'
import s from './main.cssmodule.scss'

const App = () => (
  <div className={s.campaignTracker}>
    <div className={s.container}>
      <Title />
      <Campaigns />
    </div>
  </div>
)

export default App
