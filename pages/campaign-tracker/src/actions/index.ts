import { createAction } from 'redux-actions'
import * as actionTypes from './actionTypes'

export const setCampaigns = createAction(actionTypes.SET_CAMPAIGNS)
export const setCountries = createAction(actionTypes.SET_COUNTRIES, (campaign, countries) => ({ campaign, countries }))
export const fetchCampaigns = createAction(actionTypes.FETCH_CAMPAIGNS)
export const fetchCountries = createAction(actionTypes.FETCH_COUNTRIES)
export const select = createAction(actionTypes.SELECT, (name = '') => name)
export const setSort = createAction(actionTypes.SET_SORT, by => ({ by }))
export const setFilter = createAction(actionTypes.SET_FILTER, (filter: object) => filter)
export const loading = createAction(actionTypes.LOADING)
