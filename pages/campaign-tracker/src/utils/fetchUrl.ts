const fetchUrl = (url, data) => {
  const password = new URL(location.href).searchParams.get('password')

  return fetch(url + `?password=${password}`, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then(response =>
      response.text()
      .then(text => {
        let json

        try {
          json = JSON.parse(text)
        } catch (e) {
          throw new Error('Malformed response')
        }
        if (json && json.error) {
          throw json.message || json.error
        }
        if (json.content) {
          throw json.content
        }
        return json
      })
    )
    .catch(e => {
      throw e
    })
}

export default fetchUrl
