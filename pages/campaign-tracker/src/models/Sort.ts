export interface Sort {
  by: string
  asc: boolean
}
