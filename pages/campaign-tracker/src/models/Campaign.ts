interface Field {
  value: number
  rate: string
  tooltip: string
}

export interface Campaign {
  actives: Field
  acts: Field
  age: number
  campaign?: string
  country?: string
  donators: Field
  engaged: Field
  lastVisit: string
  referrals: Field
  returns: Field
  score: Field
  tier: string
  today: Field
  visits: number
  countries?: Campaign[]
}
