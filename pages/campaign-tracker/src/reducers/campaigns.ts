import { handleActions } from 'redux-actions'
import * as actions from '../actions/actionTypes'
import { Campaign } from '../models/Campaign'
import { Filters } from '../models/Filters'
import { Sort } from '../models/Sort'

type State = {
  filters: Filters;
  selected: string;
  sort: Sort;
  list: Campaign[];
  loading: boolean;
}

type Action<Payload = object> = {
  type: string;
  payload: Payload;
}

type SetCountriesPayload = {
  campaign: string;
  countries: Campaign[];
}

const initialState = {
  list: [],
  sort: { by: '', up: false },
  filters: { actsOnly: true, twoWeeks: true },
  loading: true
}

export default handleActions(
  {
    [actions.SET_CAMPAIGNS](state: State, action: Action) {
      return {
        ...state,
        list: action.payload
      }
    },
    [actions.SET_COUNTRIES](state: State, action: Action<SetCountriesPayload>) {
      return {
        ...state,
        list: state.list.map(campaign => ({
          ...campaign,
          countries: campaign.campaign === action.payload.campaign ? action.payload.countries : null
        }))
      }
    },
    [actions.SELECT](state: State, action: Action) {
      return {
        ...state,
        selected: action.payload
      }
    },
    [actions.SET_SORT](state: State, action: Action<Sort>) {
      return {
        ...state,
        sort: {
          by: action.payload.by,
          asc: (state.sort.by === action.payload.by) ? !state.sort.asc : state.sort.asc
        }
      }
    },
    [actions.SET_FILTER](state: State, action: Action) {
      return {
        ...state,
        filters: {
          ...state.filters,
          ...action.payload
        }
      }
    },
    [actions.LOADING](state: State, action: Action) {
      return {
        ...state,
        loading: action.payload
      }
    }
  },
  initialState
)
