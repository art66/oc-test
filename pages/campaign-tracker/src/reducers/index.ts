import { createResponsiveStateReducer } from 'redux-responsive'
import { combineReducers } from 'redux'
import campaigns from './campaigns'

export default combineReducers({
  campaigns,
  browser: createResponsiveStateReducer({
    small: 600,
    medium: 1000,
    large: 2000
  })
})
