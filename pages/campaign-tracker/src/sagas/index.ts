import { all } from 'redux-saga/effects'
import campaigns from './campaigns'

export default function* rootSaga() {
  yield all([campaigns()])
}
