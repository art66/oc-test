import { put, takeLatest, select } from 'redux-saga/effects'
import fetchUrl from '../utils/fetchUrl'
import { setCampaigns, setCountries, loading } from '../actions/'
import * as actions from '../actions/actionTypes'
import { getFilters, getSelectedCampaign } from '../selectors'

function* fetchCampaigns() {
  try {
    const filters = yield select(getFilters)
    yield put(loading(true))
    const data = yield fetchUrl('/campaignTracker.php', { filters, step: 'getCampaignsListByFilters' })
    yield put(loading(false))

    if (data && data.campaigns) {
      yield put(setCampaigns(data.campaigns))
    }
  } catch (error) {
    alert(error)
  }
}

function* fetchCountries() {
  try {
    const campaign = yield select(getSelectedCampaign)

    if (!campaign) {
      return
    }

    yield put(loading(true))
    const data = yield fetchUrl('/campaignTracker.php', { step: 'getCampaignListByID', id: campaign.campaign })
    yield put(loading(false))

    if (data && data.campaigns) {
      yield put(setCountries(campaign.campaign, data.campaigns))
    }
  } catch (error) {
    alert(error)
  }
}

export default function* stats() {
  yield takeLatest(actions.FETCH_CAMPAIGNS, fetchCampaigns)
  yield takeLatest(actions.FETCH_COUNTRIES, fetchCountries)
}
