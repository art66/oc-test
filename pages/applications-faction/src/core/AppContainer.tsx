import React, { Component } from 'react'
import { Provider, connect } from 'react-redux'
import AppLayout from '../layout/AppLayout'
import { fetchApplicationData } from '../actions'
import initWS from '../actions/websocket'

interface IProps {
  store: any
  fetchData: () => void
}

class AppContainer extends Component<IProps> {
  componentDidMount() {
    const { store, fetchData } = this.props

    initWS(store.dispatch)
    fetchData()
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <AppLayout />
      </Provider>
    )
  }
}

const mapActionsToProps = {
  fetchData: fetchApplicationData
}

export default connect(null, mapActionsToProps)(AppContainer)
