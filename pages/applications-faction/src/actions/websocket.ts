import {
  addApplication,
  changeApplicationState,
  removeApplication,
  updateApplication,
  updateFactionSettingsSuccess,
  updateUserPermissions
} from './index'
import { APPLICATION_STATES } from '../constants'
import { IApplication, IUpdateSettings } from '../interfaces'
import * as types from './types'

declare function WebsocketHandler(chanel: string): void

const initWS = (dispatch: any) => {
  const handler = new WebsocketHandler('factionApplication')

  handler.setActions({
    updateSettings: (newSettings: IUpdateSettings) => {
      dispatch(updateFactionSettingsSuccess(newSettings))
    },
    create: (application: IApplication) => {
      dispatch(addApplication(application))
    },
    update: (data: types.TUpdateCurrentUserSocket) => {
      dispatch(updateApplication(data))
    },
    withdraw: (data: types.TWithdrawApplicationSocket) => {
      dispatch(changeApplicationState(data.id, APPLICATION_STATES.WITHDRAWN))
    },
    decline: (data: types.TDeclineApplicationSocket) => {
      dispatch(changeApplicationState(data.id, APPLICATION_STATES.DECLINED))
    },
    accept: (data: types.TAcceptApplicationSocket) => {
      dispatch(changeApplicationState(data.id, APPLICATION_STATES.ACCEPTED))
    },
    updateCurrentUser: (newPermissions: types.TUpdateCurrentUserSocket) => {
      dispatch(updateUserPermissions(newPermissions))
    },
    remove: (data: types.TRemoveApplicationSocket) => {
      dispatch(removeApplication(data.id))
    }
  })
}

export default initWS
