import { createAction } from 'redux-actions'
import * as a from '../constants/actionTypes'

export const fetchApplicationData = createAction(a.FETCH_APPLICATIONS_DATA)
export const fetchApplicationDataSuccess = createAction(a.FETCH_APPLICATIONS_DATA_SUCCESS, data => data)

export const updateFactionSettings = createAction(a.UPDATE_FACTION_SETTINGS, settings => settings)
export const updateFactionSettingsSuccess = createAction(a.UPDATE_FACTION_SETTINGS_SUCCESS, settings => settings)

export const acceptApplication = createAction(a.ACCEPT_APPLICATION, id => id)
export const declineApplication = createAction(a.DECLINE_APPLICATION, id => id)
export const changeApplicationState = createAction(
  a.CHANGE_APPLICATION_STATE,
  (id, state, message = '') => ({ id, state, message })
)
export const addApplication = createAction(a.ADD_APPLICATION, application => application)
export const updateApplication = createAction(a.UPDATE_APPLICATION, application => application)
export const removeApplication = createAction(a.REMOVE_APPLICATION, id => id)

export const updateUserPermissions = createAction(a.UPDATE_USER_PERMISSIONS, permissions => permissions)
