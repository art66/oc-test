export const TABLE_COLUMNS = ['person', 'lvl', 'application', 'expires', 'view']
export const APPLICATION_STATES = {
  DECLINED: 'declined',
  ACCEPTED: 'accepted',
  WITHDRAWN: 'withdrawn'
}
export const SECONDS_IN_HOUR = 3600
export const TIME_NOT_ASSIGNED = 'N/A'
