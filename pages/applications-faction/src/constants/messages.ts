export const LOOKING_FOR_MEMBERS_LABEL = 'Looking for members'
export const APPLICATIONS_ALLOWED_LABEL = 'Allow applications'
export const MEMBERS_FULL = 'This faction does not have the capacity for any more members'
export const APPLICATIONS_EMPTY = 'There are no applications at this time.'
