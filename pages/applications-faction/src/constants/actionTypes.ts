export const FETCH_APPLICATIONS_DATA = 'FETCH_APPLICATIONS_DATA'
export const FETCH_APPLICATIONS_DATA_SUCCESS = 'FETCH_APPLICATIONS_DATA_SUCCESS'
export const UPDATE_FACTION_SETTINGS = 'UPDATE_FACTION_SETTINGS'
export const UPDATE_FACTION_SETTINGS_SUCCESS = 'UPDATE_FACTION_SETTINGS_SUCCESS'
export const ACCEPT_APPLICATION = 'ACCEPT_APPLICATION'
export const DECLINE_APPLICATION = 'DECLINE_APPLICATION'
export const CHANGE_APPLICATION_STATE = 'CHANGE_APPLICATION_STATE'
export const ADD_APPLICATION = 'ADD_APPLICATION'
export const UPDATE_APPLICATION = 'UPDATE_APPLICATION'
export const REMOVE_APPLICATION = 'REMOVE_APPLICATION'
export const UPDATE_USER_PERMISSIONS = 'UPDATE_USER_PERMISSIONS'
