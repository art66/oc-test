import React, { Component, ReactNode } from 'react'
import cn from 'classnames'
import s from './index.cssmodule.scss'

interface IProps {
  id: string
  label: ReactNode
  checked: boolean
  disabled?: boolean
  customStyles?: {
    wrapper?: string
    label?: string
    radioButton?: string
  }
  onChange: () => void
}

interface IState {
  checked: boolean
}

class RadioButton extends Component<IProps, IState> {
  static defaultProps = {
    disabled: false,
    customStyles: {
      wrapper: undefined,
      label: undefined,
      radioButton: undefined
    }
  }

  _handleChange = () => {
    const { disabled, onChange } = this.props

    !disabled && onChange()
  }

  render() {
    const { id, label, checked, disabled, customStyles } = this.props

    return (
      <div className={cn(s.radioWrapper, checked && s.checked, disabled && s.disabled, customStyles.wrapper)}>
        <label className={cn(s.radioLabel, customStyles.label)} htmlFor={id}>{label}</label>
        <div className={cn(s.radioBtnWrapper, customStyles.radioButton)}>
          <div className={s.labelOn}>ON</div>
          <div className={s.labelBtn} />
          <div className={s.labelOff}>OFF</div>
          <input
            id={id}
            type='checkbox'
            checked={checked}
            className={s.radioInput}
            onChange={this._handleChange}
          />
        </div>
      </div>
    )
  }
}

export default RadioButton
