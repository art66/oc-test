import React, { Component } from 'react'
import { connect } from 'react-redux'
import s from './index.cssmodule.scss'
import { LOOKING_FOR_MEMBERS_LABEL, APPLICATIONS_ALLOWED_LABEL } from '../../constants/messages'
import { IUpdateSettings, IReduxState } from '../../interfaces'
import { updateFactionSettings as updateFactionSettingsAction } from '../../actions'
import RadioButton from '../RadioButton'

interface IProps {
  applicationAllowed: boolean
  lookingForMembers: boolean
  userPermissions: { manageApplications: boolean }
  updateFactionSettings: (settings: IUpdateSettings) => void
}

class Switches extends Component<IProps> {
  _handleAllowApplicationChange = () => {
    const { applicationAllowed, updateFactionSettings } = this.props

    updateFactionSettings({ applicationAllowed: !applicationAllowed })
  }

  _handleLookingForMembersChange = () => {
    const { lookingForMembers, updateFactionSettings } = this.props

    updateFactionSettings({ lookingForMembers: !lookingForMembers })
  }

  render() {
    const { applicationAllowed, lookingForMembers, userPermissions } = this.props

    return (
      <div className={s.switches}>
        <div className={s.switchWrapper}>
          <RadioButton
            id='applications-state'
            label={APPLICATIONS_ALLOWED_LABEL}
            checked={applicationAllowed}
            disabled={!userPermissions.manageApplications}
            onChange={this._handleAllowApplicationChange}
          />
        </div>
        <div className={s.switchWrapper}>
          <RadioButton
            id='looking-for-members'
            label={LOOKING_FOR_MEMBERS_LABEL}
            checked={lookingForMembers}
            disabled={!userPermissions.manageApplications}
            onChange={this._handleLookingForMembersChange}
          />
        </div>

      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    applicationAllowed: state.data.faction.applicationAllowed,
    lookingForMembers: state.data.faction.lookingForMembers,
    userPermissions: state.data.currentUser.permissions
  }
}

const mapActionsToProps = {
  updateFactionSettings: updateFactionSettingsAction
}

export default connect(mapStateToProps, mapActionsToProps)(Switches)
