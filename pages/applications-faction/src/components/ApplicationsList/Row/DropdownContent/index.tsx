import React, { PureComponent } from 'react'
import cn from 'classnames'
import { IApplication, ICurrentUser, ICharacteristic, TApplicationStatus } from '../../../../interfaces'
import formatCharacteristicsValue from '../../../../utils/formatCharacteristicsValue'
import s from './index.cssmodule.scss'

interface IProps {
  application: IApplication
  allowedUsersAmount: number
  userPermissions: ICurrentUser['permissions']
  onAccept: () => void
  onDecline: () => void
  applicationState: TApplicationStatus
}

interface IState {
  requestSent: boolean
}

class DropdownContent extends PureComponent<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      requestSent: false
    }
  }

  getActionsAvailability = () => {
    const { userPermissions, allowedUsersAmount, applicationState } = this.props
    const { requestSent } = this.state

    return {
      accept: userPermissions.manageApplications && allowedUsersAmount > 0 && !requestSent && !applicationState,
      decline: userPermissions.manageApplications && !requestSent && !applicationState
    }
  }

  getCharacteristicsList = (characteristics: ICharacteristic[]) => {
    return characteristics.map(item => (
      <li key={item.title} className={s.characteristicsItem}>
        <span className={s.characteristicsItemText}>{`${item.title}: ${formatCharacteristicsValue(item.value)}`}</span>
      </li>
    ))
  }

  _handleAccept = () => {
    const { onAccept } = this.props

    this.setState({ requestSent: true })
    onAccept()
  }

  _handleDecline = () => {
    const { onDecline } = this.props

    this.setState({ requestSent: true })
    onDecline()
  }

  renderCharacteristics = (characteristics: ICharacteristic[]) => {
    return <ul className={s.characteristics}>{this.getCharacteristicsList(characteristics)}</ul>
  }

  renderInfoFooter = () => {
    const { accept, decline } = this.getActionsAvailability()

    return (
      <>
        <button
          type='button'
          disabled={!accept}
          className={cn('torn-btn', s.buttonAccept, { disabled: !accept })}
          onClick={this._handleAccept}
        >
          Accept
        </button>
        <button
          type='button'
          disabled={!decline}
          className={cn('torn-btn', { disabled: !decline })}
          onClick={this._handleDecline}
        >
          Decline
        </button>
      </>
    )
  }

  render() {
    const {
      application: { user, message, state }
    } = this.props

    return (
      <div className={s.infoWrapper}>
        {user.characteristics.length > 0 && this.renderCharacteristics(user.characteristics)}
        <div className={s.message}>
          {message && <div className={s.text}>{message}</div>}
          {state?.error && state?.message ? <div className={s.errText}>{state.message}</div> : null}
          <div className={s.infoFooter}>{this.renderInfoFooter()}</div>
        </div>
      </div>
    )
  }
}

export default DropdownContent
