import React, { PureComponent } from 'react'
import cn from 'classnames'
import s from './index.cssmodule.scss'
import { SECONDS_IN_HOUR, TIME_NOT_ASSIGNED } from '../../../../constants'
import getFormattedTime from '../../../../utils/getFormattedTime'

interface IProps {
  value: number
  showTime?: boolean
  onExpire?: () => void
}

interface IState {
  timeNow: number
}

class Countdown extends PureComponent<IProps, IState> {
  private _timerID: any

  static defaultProps = {
    showTime: true,
    onExpire: () => {}
  }

  constructor(props) {
    super(props)

    this.state = {
      timeNow: Math.floor(Date.now() / 1000)
    }
  }

  componentDidMount() {
    this._timerID = setInterval(this.tick, 1000)
  }

  componentDidUpdate(prevProps: Readonly<IProps>) {
    const { showTime } = this.props

    if (prevProps.showTime && !showTime) {
      clearInterval(this._timerID)
    }
  }

  componentWillUnmount() {
    clearInterval(this._timerID)
  }

  handleExpire = () => {
    const { onExpire } = this.props

    onExpire()
    clearInterval(this._timerID)
  }

  tick = () => {
    const { timeNow } = this.state
    const { value } = this.props

    if (timeNow === value) {
      this.handleExpire()

      return false
    }

    this.setState((prevState: IState) => ({
      timeNow: prevState.timeNow + 1
    }))
  }

  render() {
    const { timeNow } = this.state
    const { value, showTime } = this.props
    const timeLeft = value - timeNow

    return (
      <span className={cn({ [s.warning]: timeLeft < SECONDS_IN_HOUR, [s.timeNA]: !showTime })}>
        {showTime ? getFormattedTime(timeLeft) : TIME_NOT_ASSIGNED}
      </span>
    )
  }
}

export default Countdown
