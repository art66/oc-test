import React, { PureComponent } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import s from './index.cssmodule.scss'

interface IProps {
  onClick: () => void
}

class View extends PureComponent<IProps> {
  _handleClick = () => {
    const { onClick } = this.props

    onClick()
  }

  render() {
    return (
      <button
        type='button'
        className={s.viewButton}
        onClick={this._handleClick}
      >
        <SVGIconGenerator
          iconName='View'
          iconsHolder={iconsHolder}
          customClass={s.viewIcon}
          preset={{ type: 'faction', subtype: 'VIEW_APPLICATION' }}
          onHover={{ active: true, preset: { type: 'faction', subtype: 'VIEW_APPLICATION_HOVER' } }}
        />
      </button>
    )
  }
}

export default View
