import React, { PureComponent } from 'react'
import UserInfo from '@torn/shared/components/UserInfo'
import { IUserFaction, TOnlineStatus } from '../../../../interfaces'
import s from './index.cssmodule.scss'

interface IProps {
  userID: number
  playername: string
  userImageUrl: string
  faction?: IUserFaction
  onlineStatus: TOnlineStatus
  showImages: boolean
  showFaction?: boolean
}

class Person extends PureComponent<IProps> {
  static defaultProps = {
    showFaction: true
  }

  render() {
    const { onlineStatus, userID, playername, userImageUrl, showImages, faction, showFaction } = this.props
    const factionProps = faction ?
      { ID: faction.id, name: faction.tag.shortName, imageUrl: faction.tag.imageUrl, rank: faction.rank } :
      { ID: null, name: null, imageUrl: null }

    return (
      <UserInfo
        status={{ mode: onlineStatus, isActive: true }}
        user={{ ID: userID, name: playername, imageUrl: userImageUrl }}
        faction={showFaction ? factionProps : null}
        showImages={showImages}
        customStyles={{ status: s.userStatus, text: s.text, blockWrap: s.userImgWrap, img: s.customImgWrap }}
      />
    )
  }
}

export default Person
