import React, { PureComponent } from 'react'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import { TApplicationState } from '../../../../interfaces'

interface IProps {
  message: string
  applicationState: TApplicationState
}

class Message extends PureComponent<IProps> {
  getApplicationMessage = () => {
    const { applicationState, message } = this.props

    return (applicationState?.message && applicationState?.message)
      || (applicationState?.state && firstLetterUpper({ value: applicationState?.state }))
      || message
  }

  render() {
    return (
      <span>{this.getApplicationMessage()}</span>
    )
  }
}

export default Message
