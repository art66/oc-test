import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import {
  IApplication,
  ICurrentUser,
  TApplicationsStateObject,
  IReduxState,
  TApplicationStatus
} from '../../../interfaces'
import { Countdown, Message, Person, View } from './Cells'
import {
  acceptApplication as acceptApplicationAction,
  declineApplication as declineApplicationAction,
  removeApplication as removeApplicationAction,
  updateApplication
} from '../../../actions'
import DropdownContent from './DropdownContent'
import tableStyles from '../index.cssmodule.scss'

interface IProps {
  application: IApplication
  applicationsState: TApplicationsStateObject
  allowedUsersAmount: number
  currentUser: ICurrentUser
  acceptApplication: (id: number) => void
  declineApplication: (id: number) => void
  removeApplication: (id: number) => void
  updateApplication: (application: { [key: string]: any }) => void
}

interface IState {
  expanded: boolean
}

class Row extends PureComponent<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      expanded: false
    }
  }

  componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>): void {
    if (!prevProps.applicationsState[prevProps.application.id] && this.getApplicationState() && prevState.expanded) {
      this.closeDropdown()
    }
  }

  getApplicationState = () => {
    const {
      application: { id },
      applicationsState
    } = this.props

    return applicationsState[id]
  }

  closeDropdown = () => {
    this.setState({ expanded: false })
  }

  _handleAccept = () => {
    const {
      application: { id },
      acceptApplication
    } = this.props

    acceptApplication(id)
  }

  _handleDecline = () => {
    const {
      application: { id },
      declineApplication
    } = this.props

    declineApplication(id)
  }

  _handleViewClick = () => {
    const {
      application: { id, state }
    } = this.props

    if (this.state.expanded && state?.error) {
      this.props.updateApplication({ id, state: null })
    }

    this.setState((prevState: IState) => {
      return {
        expanded: !prevState.expanded
      }
    })
  }

  _handleApplicationExpire = () => {
    const {
      application: { id },
      removeApplication
    } = this.props

    removeApplication(id)
  }

  renderRowPreview = () => {
    const {
      application,
      currentUser: { settings }
    } = this.props
    const {
      id,
      user: { userID, level, ...userProps },
      expires,
      message
    } = application
    const personProps = { ...userProps, ...settings, userID, showFaction: false }
    const state = this.getApplicationState()

    return (
      <div key={id} className={tableStyles.tableRow}>
        <div className={cn(tableStyles.tableCell, tableStyles.person)}>
          <Person {...personProps} />
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.lvl)}>
          <span>{String(level)}</span>
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.application)}>
          <Message message={String(message)} applicationState={state} />
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.expires)}>
          <Countdown value={expires} onExpire={this._handleApplicationExpire} showTime={!state} />
        </div>
        <div className={cn(tableStyles.tableCell, tableStyles.view)}>
          <View onClick={this._handleViewClick} />
        </div>
      </div>
    )
  }

  renderInfo = (applicationState: TApplicationStatus) => {
    const {
      application,
      allowedUsersAmount,
      currentUser: { permissions }
    } = this.props
    const { expanded } = this.state

    return (
      expanded && (
        <DropdownContent
          application={application}
          allowedUsersAmount={allowedUsersAmount}
          userPermissions={permissions}
          onAccept={this._handleAccept}
          onDecline={this._handleDecline}
          applicationState={applicationState}
        />
      )
    )
  }

  render() {
    const { expanded } = this.state
    const applicationState = this.getApplicationState()?.state
    const rowWrapperStyles = cn({
      [tableStyles.rowWrapper]: true,
      [tableStyles[applicationState]]: applicationState,
      [tableStyles.expanded]: expanded
    })

    return (
      <div className={rowWrapperStyles}>
        {this.renderRowPreview()}
        {this.renderInfo(applicationState)}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    applicationsState: state.data.applicationsState,
    currentUser: state.data.currentUser
  }
}

const mapActionsToProps = {
  acceptApplication: acceptApplicationAction,
  declineApplication: declineApplicationAction,
  removeApplication: removeApplicationAction,
  updateApplication
}

export default connect(mapStateToProps, mapActionsToProps)(Row)
