import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import s from './index.cssmodule.scss'
import { IFaction, IApplication, TApplicationsStateObject, IReduxState } from '../../interfaces'
import { MEMBERS_FULL, APPLICATIONS_EMPTY } from '../../constants/messages'
import { TABLE_COLUMNS } from '../../constants'
import Row from './Row'

interface IProps {
  faction: IFaction
  applications: IApplication[]
  applicationsState: TApplicationsStateObject
}

class ApplicationList extends PureComponent<IProps> {
  getAllowedUsersAmount = () => {
    const {
      faction: { amountOfMembers, maxAmountOfMembers }
    } = this.props

    return maxAmountOfMembers - amountOfMembers
  }

  renderAllowedMembersCount = () => {
    const allowedMembers = this.getAllowedUsersAmount()

    return allowedMembers > 0 ? (
      <div className={s.applicationListHeader}>
        {`Your faction can accept ${allowedMembers} more member${allowedMembers !== 1 ? 's' : ''}`}
      </div>
    ) : (
      <div className={cn(s.applicationListHeader, s.warning)}>{MEMBERS_FULL}</div>
    )
  }

  renderTableHeaderCells = () => {
    return TABLE_COLUMNS.map(label => (
      <span key={label} className={cn(s.tableCell, s[label])}>
        {firstLetterUpper({ value: label })}
      </span>
    ))
  }

  renderTableRows = () => {
    const { applications } = this.props

    return applications.map(application => (
      <Row key={application.user.userID} allowedUsersAmount={this.getAllowedUsersAmount()} application={application} />
    ))
  }

  render() {
    const { applications } = this.props

    if (!applications.length) {
      return <div className={s.applicationsEmpty}>{APPLICATIONS_EMPTY}</div>
    }

    return (
      <div className={s.applicationListWrapper}>
        {this.renderAllowedMembersCount()}
        <div className={s.tableHeader}>{this.renderTableHeaderCells()}</div>
        <div className={s.rowsWrapper}>{this.renderTableRows()}</div>
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    faction: state.data.faction,
    applications: state.data.applications,
    applicationsState: state.data.applicationsState
  }
}

export default connect(mapStateToProps)(ApplicationList)
