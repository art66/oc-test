import initialState from './initialState'
import { APPLICATION_STATES } from '../constants'
import * as a from '../constants/actionTypes'

const ACTION_HANDLERS = {
  [a.FETCH_APPLICATIONS_DATA]: state => {
    return {
      ...state,
      loading: true
    }
  },
  [a.FETCH_APPLICATIONS_DATA_SUCCESS]: (state, { payload }) => {
    return {
      ...state,
      ...payload,
      loading: false
    }
  },
  [a.UPDATE_FACTION_SETTINGS_SUCCESS]: (state, { payload }) => {
    return {
      ...state,
      faction: {
        ...state.faction,
        ...payload
      }
    }
  },
  [a.CHANGE_APPLICATION_STATE]: (state, { payload }) => {
    return {
      ...state,
      applicationsState: {
        ...state.applicationsState,
        [payload.id]: {
          state: payload.state,
          message: payload.message
        }
      },
      faction: {
        ...state.faction,
        amountOfMembers: payload.state === APPLICATION_STATES.ACCEPTED ?
          state.faction.amountOfMembers + 1 :
          state.faction.amountOfMembers
      }
    }
  },
  [a.ADD_APPLICATION]: (state, { payload }) => {
    if (state.applications.find(application => application.user.userID === payload.user.userID)) {
      return {
        ...state,
        applications: state.applications.map(application => {
          return application.user.userID === payload.user.userID ? payload : application
        })
      }
    }

    return {
      ...state,
      applications: [
        payload,
        ...state.applications
      ]
    }
  },
  [a.UPDATE_APPLICATION]: (state, { payload }) => {
    const { id, user: newUserValues = {}, ...newValues } = payload

    return {
      ...state,
      applications: state.applications.map(item => {
        return item.id === id ?
          {
            ...item,
            ...newValues,
            user: {
              ...item.user,
              ...newUserValues
            }
          } : item
      })
    }
  },
  [a.REMOVE_APPLICATION]: (state, { payload }) => {
    return {
      ...state,
      applications: state.applications.filter(item => item.id !== payload)
    }
  },
  [a.UPDATE_USER_PERMISSIONS]: (state, { payload }) => {
    return {
      ...state,
      currentUser: {
        ...state.currentUser,
        permissions: {
          ...state.currentUser.permissions,
          ...payload
        }
      }
    }
  }
}

const rootReducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default rootReducer
