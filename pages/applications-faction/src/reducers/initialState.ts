export default {
  loading: false,
  currentUser: {
    permissions: {},
    settings: {}
  },
  faction: {},
  applications: [],
  applicationsState: {}
}
