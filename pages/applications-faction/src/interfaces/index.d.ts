import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'

export type TOnlineStatus = 'online' | 'offline' | 'idle'
export type TApplicationStatus = 'accepted' | 'declined' | 'withdrawn'

interface IUserSettings {
  showImages: boolean
}

export interface ICharacteristic {
  title: string
  value: number
}

export interface IFactionTag {
  imageUrl?: string
  shortName?: string
}

export interface IUserFaction {
  id: number
  tag: IFactionTag
  rank?: TFactionRank
}

interface IUser {
  userID: number
  onlineStatus: TOnlineStatus
  faction?: IUserFaction
  userImageUrl: string
  playername: string
  level: number
  characteristics: ICharacteristic[]
}

export interface ICurrentUser {
  permissions: {
    manageApplications: boolean
  }
  settings: IUserSettings
}

export interface IFaction {
  amountOfMembers: number
  maxAmountOfMembers: number
  applicationAllowed: boolean
  lookingForMembers: boolean
}

export interface IApplication {
  id: number
  user: IUser
  message: string
  expires: number
  state?: {
    error: boolean
    message: string
  }
}

export interface IUpdateSettings {
  applicationAllowed?: boolean
  lookingForMembers?: boolean
}

export type TApplicationState = {
  state: TApplicationStatus
  message?: string
}

export type TApplicationsStateObject = {
  [K in IApplication['id']]: TApplicationState
}

export interface IReduxState {
  browser: {
    mediaType: string
  }
  data: {
    loading: boolean
    currentUser: ICurrentUser
    faction: IFaction
    applications: IApplication[]
    applicationsState: TApplicationsStateObject
  }
}
