const getFormattedTime = (timeInSeconds: number) => {
  const pad = (num, size) => `000${num}`.slice(size * -1)
  const hours = Math.floor(timeInSeconds / 60 / 60)
  const minutes = Math.floor(timeInSeconds / 60) % 60
  const seconds = Math.floor(timeInSeconds - minutes * 60)

  return `${pad(hours, 2)}:${pad(minutes, 2)}:${pad(seconds, 2)}`
}

export default getFormattedTime
