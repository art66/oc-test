declare function addRFC(url: string): RequestInfo

const defaultSid = 'applicationFactionControls'
const MAIN_URL = '/page.php?'

const fetchUrl = (step: string, data: object = {}, sid: string = defaultSid) => {
  return fetch(addRFC(`${MAIN_URL}sid=${sid}&step=${step}`), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      try {
        const json = JSON.parse(text)

        if (!json.success) {
          throw json.message.text
        }

        return json
      } catch (e) {
        throw new Error(e)
      }
    })
  })
}

export default fetchUrl
