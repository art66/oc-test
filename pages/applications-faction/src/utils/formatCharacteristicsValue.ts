const ONE_MILLION = 1000000
const ONE_BILLION = ONE_MILLION * 1000
const ONE_TRILLION = ONE_BILLION * 1000
const formatCharacteristicsValue = number => {
  if (number >= ONE_TRILLION) {
    return `${(number / ONE_TRILLION).toFixed(2)}t`
  }

  if (number >= ONE_BILLION) {
    return `${(number / ONE_BILLION).toFixed(2)}b`
  }

  if (number >= ONE_MILLION) {
    return `${(number / ONE_MILLION).toFixed(2)}m`
  }

  return String(number)
    .split('')
    .reverse()
    .map((char, i) => (i + 1) % 3 === 0 && i !== String(number).length - 1 ? `,${char}` : char)
    .reverse()
    .join('')
}

export default formatCharacteristicsValue
