import { put, takeEvery } from 'redux-saga/effects'
import * as actionTypes from '../constants/actionTypes'
import { APPLICATION_STATES } from '../constants'
import { IApplication, IUpdateSettings } from '../interfaces'
import fetchUrl from '../utils/fetchURL'
import * as actions from '../actions'

interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* fetchData() {
  try {
    const data = yield fetchUrl('init')

    const sortApplications = (a: IApplication, b: IApplication) => {
      const { applicationsState } = data

      if (!applicationsState[a.id] && applicationsState[b.id]) return -1

      if (applicationsState[a.id] && !applicationsState[b.id]) return 1

      return b.expires - a.expires
    }

    yield put(
      actions.fetchApplicationDataSuccess({
        ...data,
        applications: data.applications.sort(sortApplications)
      })
    )
  } catch (error) {
    console.error(error)
  }
}

function* updateFactionSettings(action: IAction<{ payload: IUpdateSettings }>) {
  try {
    const { payload } = action

    // response coming from socket, don't handle response here to avoid double action dispatch
    yield fetchUrl('updateSettings', payload, 'applicationFactionSettings')
  } catch (error) {
    console.error(error)
  }
}

function* acceptApplication(action: IAction<{ payload: number }>) {
  try {
    const { payload } = action

    // response coming from socket, don't handle response here to avoid double action dispatch
    yield fetchUrl('accept', { application: { id: payload } })
  } catch (error) {
    yield put(actions.updateApplication({ id: action.payload, state: { error: true, message: error.message } }))
  }
}

function* declineApplication(action: IAction<{ payload: number }>) {
  try {
    const { payload } = action

    // response coming from socket, don't handle response here to avoid double action dispatch
    yield fetchUrl('decline', { application: { id: payload } })
  } catch (error) {
    yield put(actions.changeApplicationState(action.payload, APPLICATION_STATES.DECLINED, error.message))
  }
}

export default function* appSaga() {
  yield takeEvery(actionTypes.FETCH_APPLICATIONS_DATA, fetchData)
  yield takeEvery(actionTypes.UPDATE_FACTION_SETTINGS, updateFactionSettings)
  yield takeEvery(actionTypes.ACCEPT_APPLICATION, acceptApplication)
  yield takeEvery(actionTypes.DECLINE_APPLICATION, declineApplication)
}
