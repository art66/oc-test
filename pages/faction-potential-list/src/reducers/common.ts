import { ICommonState, IDebugShow, IInfoShow, IType } from '../interfaces/common'
import { SHOW_DEBUG_BOX, HIDE_DEBUG_BOX, SHOW_INFO_BOX, HIDE_INFO_BOX } from '../constants/actionTypes'

const initialState = {
  debug: null,
  info: null
}

const ACTION_HANDLERS = {
  [SHOW_DEBUG_BOX]: (state: ICommonState, action: IDebugShow) => ({
    ...state,
    debug: action.msg
  }),
  [HIDE_DEBUG_BOX]: (state: ICommonState) => ({
    ...state,
    debug: null
  }),
  [SHOW_INFO_BOX]: (state: ICommonState, action: IInfoShow) => ({
    ...state,
    info: action.msg
  }),
  [HIDE_INFO_BOX]: (state: ICommonState) => ({
    ...state,
    info: null
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: ICommonState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
