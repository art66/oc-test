import { APPLIED_STATUS_DECLINED } from '../constants'
import initialState from './initialState'
import * as a from '../constants/actionTypes'

const ACTION_HANDLERS = {
  [a.FETCH_FACTION]: state => {
    return {
      ...state,
      loading: true
    }
  },
  [a.FETCH_FACTION_SUCCESS]: (state, { payload }) => {
    return {
      ...state,
      ...payload,
      loading: false
    }
  },
  [a.CREATE_FACTION_SUCCESS]: state => {
    return {
      ...state,
      newFaction: {
        ...state.faction,
        created: true
      }
    }
  },
  [a.CHANGE_APPLICATION_STATE]: (state, { payload }) => {
    const { id, factionId, state: applicationState } = payload
    const declined = applicationState === APPLIED_STATUS_DECLINED

    return {
      ...state,
      factions: state.factions.map(item => {
        const compareWith = declined ? item.application.id : item.id
        const compareTo = declined ? id : factionId

        return compareWith === compareTo ? {
          ...item,
          application: {
            id,
            appliedStatus: applicationState
          }
        } : item
      })
    }
  }
}

const rootReducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default rootReducer
