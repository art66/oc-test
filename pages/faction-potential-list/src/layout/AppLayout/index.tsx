import React from 'react'
import { connect } from 'react-redux'
import DebugBox from '@torn/shared/components/DebugBox'
import InfoBox from '@torn/shared/components/InfoBox'
import { ICommonState } from '../../interfaces/common'
import { infoShow, debugHide } from '../../actions/common'
import CreateFaction from '../../components/CreateFaction'
import FactionsList from '../../components/FactionsList'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'

export interface IProps extends ICommonState {
  infoMessage: string
  infoBoxShow: (msg: string) => void
}

class AppLayout extends React.PureComponent<IProps> {
  renderDebugBox = () => {
    const { debug, debugCloseAction } = this.props

    return (
      <>
        <DebugBox debugMessage={debug} close={debugCloseAction} isBeatifyError={true} />
        <hr className='page-head-delimiter m-top10 m-bottom10' />
      </>
    )
  }

  renderInfoDebugAreas = () => {
    const { debug, info } = this.props

    return (
      <React.Fragment>
        {debug && this.renderDebugBox()}
        {info && <InfoBox msg={info} />}
      </React.Fragment>
    )
  }

  render() {
    const { infoMessage } = this.props

    return (
      <>
        {this.renderInfoDebugAreas()}
        <CreateFaction />
        <hr className='delimiter-999 m-top10 m-bottom10' />
        {infoMessage && <InfoBox msg={infoMessage} color='blue' />}
        <FactionsList />
      </>
    )
  }
}

const mapStateToProps = (state: any) => ({
  debug: state.common.debug,
  info: state.common.info,
  infoMessage: state.data.message.text
})

const mapDispatchToState = dispatch => ({
  debugCloseAction: () => dispatch(debugHide()),
  infoBoxShow: (msg: string) => dispatch(infoShow(msg))
})

export default connect(mapStateToProps, mapDispatchToState)(AppLayout)
