import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Preloader from '@torn/shared/components/Preloader'
import s from './index.cssmodule.scss'
import globalStyles from '../../styles/global.cssmodule.scss'
import { INewFaction, IReduxState } from '../../interfaces'
import { createFaction as createFactionAction } from '../../actions'
import formatNumberWithCommas from '../../utils/formatNumberWithCommas'
import ContentWrapper from '../ContentWrapper'

interface IProps {
  newFaction: INewFaction
  loading: boolean
  createFaction: (name: string) => void
}

interface IState {
  factionName: string
}

class CreateFaction extends React.PureComponent<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      factionName: ''
    }
  }

  _handleFactionNameChange = (e) => {
    this.setState({ factionName: e.target.value })
  }

  _handleFactionCreate = () => {
    const { createFaction } = this.props
    const { factionName } = this.state

    createFaction(factionName)
  }

  renderContent = (loading: boolean) => {
    const {
      newFaction: { cost, created }
    } = this.props
    const { factionName } = this.state
    const costAmount = `$${formatNumberWithCommas(cost)}`

    if (loading) {
      return <Preloader />
    }

    return created ? (
      <div className={s.factionCreated}>
        Congratulations! You have successfully created faction&nbsp;
        <strong>{factionName}</strong>
      </div>
    ) : (
      <>
        <div className={s.factionCost}>
          <span>
            It will cost <strong>{costAmount}</strong> to make a Faction.{' '}
          </span>
          <span>Please enter faction name below:</span>
        </div>
        <div className={s.actionWrapper}>
          <input
            className={s.createFactionInputName}
            name='factionname'
            autoComplete='off'
            maxLength={25}
            value={factionName}
            onChange={this._handleFactionNameChange}
          />
          <button type='button' className='torn-btn' onClick={this._handleFactionCreate}>
            Create
          </button>
        </div>
      </>
    )
  }

  render() {
    const { loading } = this.props

    return (
      <ContentWrapper
        title='Create a Faction'
        contentClass={cn(s.createFactionWrapper, loading && globalStyles.loading)}
      >
        {this.renderContent(loading)}
      </ContentWrapper>
    )
  }
}

const mapStateToProps = ({ data }: IReduxState) => ({
  newFaction: data.newFaction,
  loading: data.loading
})

const mapDispatchToState = {
  createFaction: createFactionAction
}

export default connect(mapStateToProps, mapDispatchToState)(CreateFaction)
