import React, { Component, ReactNode } from 'react'

interface IProps {
  title: ReactNode
  children: ReactNode
  titleClass?: string
  contentClass?: string
}

class ContentWrapper extends Component<IProps> {
  render() {
    const { title, children, titleClass, contentClass } = this.props

    return (
      <>
        <div className={`title-black top-round ${titleClass || ''}`}>{title}</div>
        <div className={`cont bottom-round ${contentClass || ''}`}>{children}</div>
      </>
    )
  }
}
export default ContentWrapper
