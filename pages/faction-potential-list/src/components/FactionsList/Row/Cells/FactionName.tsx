import React from 'react'
import cn from 'classnames'
import s from './index.cssmodule.scss'
import globalStyles from '../../../../styles/global.cssmodule.scss'
import { IFactionTag } from '../../../../interfaces'

interface IProps {
  factionProps: {
    id: number | string
    profileImageUrl?: string
    title: string
    tag: IFactionTag
  }
}

const FactionName = (props: IProps) => {
  const { factionProps: { id, profileImageUrl, title, tag } } = props

  const getFactionImage = () => {
    return !profileImageUrl || profileImageUrl.includes('defaultuser') ? (
      <span className={s.noImage}>N/A</span>
    ) : (
      <img
        className={globalStyles.factionImage}
        src={profileImageUrl}
        alt='Faction'
      />
    )
  }

  const getFactionTagImage = () => {
    return !tag.imageUrl ? (
      <span className={cn(s.tag, s.noImage)}>N/A</span>
    ) : (
      <img
        className={s.tag}
        src={tag.imageUrl}
        alt='Tag'
      />
    )
  }

  return (
    <a className={s.factionLink} href={`/factions.php?step=profile&ID=${id}`}>
      <div className={s.imgWrapper}>
        {getFactionImage()}
      </div>
      <div className={s.tagWrapper}>
        {getFactionTagImage()}
        <span className={s.name}>{title}</span>
      </div>
    </a>
  )
}

export default FactionName
