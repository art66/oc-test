import React from 'react'
import formatNumberWithCommas from '../../../../utils/formatNumberWithCommas'

interface IProps {
  respect: number
}

const Respect = (props: IProps) => {
  const { respect } = props

  return (
    <span>{formatNumberWithCommas(respect)}</span>
  )
}

export default Respect
