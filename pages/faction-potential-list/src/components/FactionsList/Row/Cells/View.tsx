import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import s from './index.cssmodule.scss'

interface IProps {
  factionID: number
}

const View = (props: IProps) => {
  const { factionID } = props

  return (
    <a className={s.factionViewLink} href={`/factions.php?step=profile&ID=${factionID}`}>
      <SVGIconGenerator
        iconName='View'
        iconsHolder={iconsHolder}
        preset={{ type: 'faction', subtype: 'VIEW_APPLICATION' }}
        onHover={{ active: true, preset: { type: 'faction', subtype: 'VIEW_APPLICATION_HOVER' } }}
      />
    </a>
  )
}

export default View
