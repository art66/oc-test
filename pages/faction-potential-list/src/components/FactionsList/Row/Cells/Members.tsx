import React from 'react'

interface IProps {
  members: {
    amountOfMembers: number
    maxAmountOfMembers: number
  }
}

const Members = (props: IProps) => {
  const { members: { amountOfMembers, maxAmountOfMembers } } = props

  return (
    <span>{`${amountOfMembers} / ${maxAmountOfMembers}`}</span>
  )
}

export default Members
