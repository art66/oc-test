import React from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import factionIcons from '@torn/shared/SVG/helpers/iconsHolder/faction-potential-list'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { IApplication } from '../../../../interfaces'
import { APPLICATION_STATUS_MESSAGES, APPLIED_STATUS_NONE } from '../../../../constants'

interface IProps {
  application: IApplication
  factionID: number
}

const Application = (props: IProps) => {
  const { application, factionID } = props
  const applicationState = application.appliedStatus || APPLIED_STATUS_NONE
  const iconPresetSubtype = `APPLICATION_STATE_${applicationState.toUpperCase()}`

  return (
    <span id={`faction-${factionID}-application`}>
      <Tooltip
        position='top'
        arrow='center'
        parent={`faction-${factionID}-application`}
      >
        {APPLICATION_STATUS_MESSAGES[applicationState]}
      </Tooltip>
      <SVGIconGenerator
        iconName='Application'
        iconsHolder={factionIcons}
        preset={{ type: 'factionPotentialList', subtype: iconPresetSubtype }}
      />
    </span>
  )
}

export default Application
