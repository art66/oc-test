import React from 'react'
import cn from 'classnames'
import tableStyles from '../index.cssmodule.scss'
import { IFaction } from '../../../interfaces'
import { Application, FactionName, Members, Respect, View } from './Cells'

interface IProps {
  faction: IFaction
}

const Row = (props: IProps) => {
  const { faction: { id, respect, amountOfMembers, maxAmountOfMembers, application, ...factionNameProps } } = props

  return (
    <div className={tableStyles.tableRow}>
      <div className={cn(tableStyles.tableCell, tableStyles.factionName)}>
        <FactionName factionProps={{ id, ...factionNameProps }} />
      </div>
      <div className={cn(tableStyles.tableCell, tableStyles.respect)}>
        <Respect respect={respect} />
      </div>
      <div className={cn(tableStyles.tableCell, tableStyles.members)}>
        <Members members={{ amountOfMembers, maxAmountOfMembers }} />
      </div>
      <div className={cn(tableStyles.tableCell, tableStyles.applied)}>
        <Application application={application} factionID={id} />
      </div>
      <div className={cn(tableStyles.tableCell, tableStyles.view)}>
        <View factionID={id} />
      </div>
    </div>
  )
}

export default Row
