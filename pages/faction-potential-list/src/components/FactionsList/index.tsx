import React from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Preloader from '@torn/shared/components/Preloader'
import s from './index.cssmodule.scss'
import globalStyles from '../../styles/global.cssmodule.scss'
import { IFaction, IReduxState } from '../../interfaces'
import { NORMALIZED_TABLE_LABELS } from '../../constants'
import ContentWrapper from '../ContentWrapper'
import TableRow from './Row'

interface IProps {
  factions: IFaction[]
  loading: boolean
}

class CreateFaction extends React.PureComponent<IProps> {
  renderTableHeaderCells = () => {
    return Object.keys(NORMALIZED_TABLE_LABELS).map(key => (
      <div key={key} className={cn(s.tableCell, s[key])}>{NORMALIZED_TABLE_LABELS[key]}</div>
    ))
  }

  renderTableRows = () => {
    const { factions } = this.props

    return factions.map(faction => (
      <TableRow key={faction.id} faction={faction} />
    ))
  }

  renderContent = (loading: boolean) => {
    return loading ? (
      <Preloader />
    ) : (
      <>
        <div className={s.tableHeader}>
          {this.renderTableHeaderCells()}
        </div>
        <div className={s.rowsWrapper}>
          {this.renderTableRows()}
        </div>
      </>
    )
  }

  render() {
    const { loading } = this.props

    return (
      <ContentWrapper
        title='Factions looking for members'
        contentClass={cn(s.factionsTableWrapper, loading && globalStyles.loading)}
      >
        {this.renderContent(loading)}
      </ContentWrapper>
    )
  }
}

const mapStateToProps = (state: IReduxState) => ({
  factions: state.data.factions,
  loading: state.data.loading
})

export default connect(mapStateToProps)(CreateFaction)
