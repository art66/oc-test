import { put, takeEvery } from 'redux-saga/effects'
import * as a from '../constants/actionTypes'
import fetchUrl from '../utils/fetchUrl'
import fetchUrlEncoded from '../utils/fetchUrlEncoded'
import { debugShow } from '../actions/common'
import { fetchFactionSuccess, createFactionSuccess } from '../actions'

interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* fetchFaction() {
  try {
    const data = yield fetchUrl('init')

    yield put(fetchFactionSuccess(data))
  } catch (error) {
    console.error(error)
    yield put(debugShow(error.message))
  }
}

function* createFaction(action: IAction<{ payload: string }>) {
  try {
    const { payload } = action
    const requestData = {
      step: 'actionCreateFaction',
      name: payload
    }

    yield fetchUrlEncoded(requestData)
    yield put(createFactionSuccess())
  } catch (error) {
    console.error(error)
    yield put(debugShow(error.message))
  }
}


export default function* appSaga() {
  yield takeEvery(a.FETCH_FACTION, fetchFaction)
  yield takeEvery(a.CREATE_FACTION, createFaction)
}
