export const NORMALIZED_TABLE_LABELS = {
  factionName: 'Faction Name',
  respect: 'Respect',
  members: 'Members',
  applied: 'Applied',
  view: 'View'
}

export const APPLIED_STATUS_NONE = 'none'
export const APPLIED_STATUS_PENDING = 'pending'
export const APPLIED_STATUS_DECLINED = 'declined'

export const APPLICATION_STATUS_MESSAGES = {
  [APPLIED_STATUS_NONE]: 'You\'ve not applied to this faction yet',
  [APPLIED_STATUS_PENDING]: 'You have a pending application with this faction',
  [APPLIED_STATUS_DECLINED]: 'Your application was declined by this faction'
}
