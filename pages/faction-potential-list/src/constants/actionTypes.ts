// --------------------------
// COMMON
// --------------------------
export const SHOW_DEBUG_BOX = 'SHOW_DEBUG_BOX'
export const HIDE_DEBUG_BOX = 'HIDE_DEBUG_BOX'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_INFO_BOX = 'HIDE_INFO_BOX'

// --------------------------
// APP
// --------------------------
export const FETCH_FACTION = 'FETCH_FACTION'
export const FETCH_FACTION_SUCCESS = 'FETCH_FACTION_SUCCESS'
export const CREATE_FACTION = 'CREATE_FACTION'
export const CREATE_FACTION_SUCCESS = 'CREATE_FACTION_SUCCESS'
export const CHANGE_APPLICATION_STATE = 'CHANGE_APPLICATION_STATE'
