import React, { Component } from 'react'
import { Provider, connect } from 'react-redux'
import AppLayout from '../layout/AppLayout'
import { fetchFaction as fetchFactionAction } from '../actions'
import initWS from '../actions/websocket'


interface IProps {
  store: any
  fetchFaction: () => void
}

class AppContainer extends Component<IProps> {
  componentDidMount() {
    const { store, fetchFaction } = this.props

    initWS(store.dispatch)
    fetchFaction()
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <AppLayout />
      </Provider>
    )
  }
}

const mapActionsToProps = {
  fetchFaction: fetchFactionAction
}

export default connect(null, mapActionsToProps)(AppContainer)
