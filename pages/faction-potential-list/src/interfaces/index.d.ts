import { ICommonState } from './common'

export type TAppliedStatus = 'declined' | 'pending' | 'none'

export interface IFactionTag {
  imageUrl?: string | null
  shortName?: string | null
}

export interface INewFaction {
  cost: number
  created?: boolean
}

export interface IApplication {
  id?: number
  appliedStatus?: TAppliedStatus
}

export interface IFaction {
  id: number
  profileImageUrl: string
  tag: IFactionTag
  title: string
  respect: number
  amountOfMembers: number
  maxAmountOfMembers: number
  application: IApplication
}

export interface IMessage {
  type: string
  text: string
}

export interface IReduxState {
  browser: {
    mediaType: string
  }
  common: ICommonState
  data: {
    loading: boolean
    newFaction: INewFaction
    factions: IFaction[]
    message: IMessage
    settings: {
      showImages: boolean
    }
  }
}
