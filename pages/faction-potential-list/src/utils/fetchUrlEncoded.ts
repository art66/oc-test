declare function addRFC(url: string): RequestInfo

const MAIN_URL = '/factions.php?'
const fetchUrlEncoded = (data: object = {}) => {
  const formData = new URLSearchParams()

  Object.keys(data).forEach(key => {
    formData.append(key, data[key])
  })

  return fetch(addRFC(MAIN_URL), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    body: formData
  }).then(response => {
    return response.text().then(text => {
      try {
        const json = JSON.parse(text)

        if (!json.success) {
          throw json.text
        }

        return json
      } catch (e) {
        throw new Error(e)
      }
    })
  })
}

export default fetchUrlEncoded
