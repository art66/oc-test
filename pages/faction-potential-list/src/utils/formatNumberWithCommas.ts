const formatNumberWithCommas = number => {
  return String(number)
    .split('')
    .reverse()
    .map((char, i) => (i + 1) % 3 === 0 && i !== String(number).length - 1 ? `,${char}` : char)
    .reverse()
    .join('')
}

export default formatNumberWithCommas
