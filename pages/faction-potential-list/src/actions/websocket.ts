import { APPLIED_STATUS_PENDING, APPLIED_STATUS_DECLINED } from '../constants'
import { changeApplicationState } from './index'

const initWS = (dispatch: any) => {
  // @ts-ignore
  const handler = new WebsocketHandler('factionApplication')

  handler.setActions({
    decline: (application: { id: number }) => {
      dispatch(changeApplicationState(application.id, null, APPLIED_STATUS_DECLINED))
    },
    create: (newApplication: { id: number; factionId: number }) => {
      dispatch(changeApplicationState(newApplication.id, newApplication.factionId, APPLIED_STATUS_PENDING))
    }
  })
}

export default initWS
