import { createAction } from 'redux-actions'
import * as a from '../constants/actionTypes'

export const fetchFaction = createAction(a.FETCH_FACTION)
export const fetchFactionSuccess = createAction(a.FETCH_FACTION_SUCCESS, data => data)
export const createFaction = createAction(a.CREATE_FACTION, name => name)
export const createFactionSuccess = createAction(a.CREATE_FACTION_SUCCESS)
export const changeApplicationState = createAction(
  a.CHANGE_APPLICATION_STATE,
  (id, factionId, state) => ({ id, factionId, state })
)
