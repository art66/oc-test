import { SHOW_DEBUG_BOX, HIDE_DEBUG_BOX, SHOW_INFO_BOX, HIDE_INFO_BOX } from '../constants/actionTypes'
import { IDebugShow, IType, IInfoShow, TDebugMessage } from '../interfaces/common'

export const debugShow = (error: TDebugMessage): IDebugShow => ({
  msg: error,
  type: SHOW_DEBUG_BOX
})

export const debugHide = (): IType => ({
  type: HIDE_DEBUG_BOX
})

export const infoShow = (info: TDebugMessage): IInfoShow => ({
  msg: info,
  type: SHOW_INFO_BOX
})

export const infoHide = (): IType => ({
  type: HIDE_INFO_BOX
})
