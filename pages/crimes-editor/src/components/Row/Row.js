import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import { CSSTransition } from 'react-transition-group'
import { toMoney } from '@torn/shared/utils'
import { tooltipSubscriber } from '@torn/shared/components/TooltipNew/utils'
import styles from './index.cssmodule.scss'
import g from '../../styles/global.cssmodule.scss'
import s from '../Stats/styles.cssmodule.scss'

class Row extends Component {
  componentDidMount() {
    const { ID, realChance } = this.props

    tooltipSubscriber.subscribe({ ID: `${ID}_chance_cell`, child: `${0 || realChance.toFixed(2)}%` })
  }

  _toClassName = str => {
    return str.toLowerCase().replace(/\s/g, '')
  }

  _getChanceValueLable = () => {
    const { chanceLabel } = this.props
    const chanceLabelValue = !chanceLabel ? '-' : chanceLabel

    return chanceLabelValue
  }

  _getChanceColumn = () => {
    const { ID, realChance = 0 } = this.props
    const chanceLabelValue = this._getChanceValueLable()

    return (
      <div id={`${ID}_chance_cell`} className={`${s['trc']} ${s[this._toClassName(chanceLabelValue)]} ${s['chance']}`}>
        {`${(100 / realChance).toFixed(0)}`}
      </div>
    )
  }

  render() {
    const {
      // creator = '-',
      creatorName = '-',
      crimeID,
      editOutcome,
      ID,
      index,
      minCrimeLevel,
      maxCrimeLevel,
      // money = 0,
      name = '-',
      result = '-',
      resultValue = '',
      stageLabel,
      trigger,
      value,
      cooldown,
      special,
      children
    } = this.props

    const triggerValue = !trigger ? '-' : trigger
    const chanceLabelValue = this._getChanceValueLable()
    const customBcg = classnames({
      [styles.cooldownActive]: cooldown && cooldown >= 3600,
      [styles.special]: special >= 1
    })

    return (
      <div
        tabIndex={0}
        role='editOutcome'
        className={`${s['tr']} ${styles.columnView} ${customBcg}`}
      >
        <section className={styles.rowHeaderSection} onClick={() => editOutcome(index, crimeID)}>
          <div className={s['trc']}>#{ID}</div>
          <div className={s['trc']}>{`${minCrimeLevel} - ${maxCrimeLevel}`}</div>
          <div className={s['trc']} dangerouslySetInnerHTML={{ __html: name }} />
          <div className={s['trc']}>
            <span>{result}</span>
            <span className={s['result-value']} dangerouslySetInnerHTML={{ __html: resultValue }} />
          </div>
          <div className={s['trc']}>{value ? '$' + toMoney(value) : '-'}</div>
          <div className={s['trc']}>{triggerValue}</div>
          <div className={s['trc']}>{creatorName}</div>
          <div className={s['trc'] + ' ' + s[this._toClassName(chanceLabelValue)]}>{chanceLabelValue}</div>
          {this._getChanceColumn()}
          <div className={s['trc'] + ' ' + s['center']}>
            <div className={g['stage'] + ' ' + g[stageLabel]}>
              <i />
            </div>
          </div>
        </section>
        <CSSTransition in={!!children} classNames='rowSlide' timeout={{ enter: 500, exit: 200 }} unmountOnExit>
          <section className={styles.rowEditorWrapper} key={children && children.index}>
            {children}
          </section>
        </CSSTransition>
      </div>
    )
  }
}

Row.propTypes = {
  children: PropTypes.node,
  stageLabel: PropTypes.string,
  chanceLabel: PropTypes.string,
  crimeID: PropTypes.number,
  creatorName: PropTypes.string,
  editOutcome: PropTypes.func,
  ID: PropTypes.number,
  index: PropTypes.number,
  minCrimeLevel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  maxCrimeLevel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  realChance: PropTypes.number,
  result: PropTypes.string,
  resultValue: PropTypes.string,
  trigger: PropTypes.string,
  value: PropTypes.number
}

Row.defaultProps = {
  children: null,
  stageLabel: '-',
  chanceLabel: '-',
  crimeID: null,
  creatorName: '-',
  editOutcome: () => {},
  ID: null,
  index: null,
  minCrimeLevel: '?',
  maxCrimeLevel: '?',
  name: '-',
  realChance: 0,
  result: '',
  resultValue: '-',
  trigger: '-',
  value: null
}

export default Row
