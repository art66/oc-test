import PropTypes from 'prop-types'
import React from 'react'
import Row from './Row'
import styles from './index.cssmodule.scss'

const Rows = ({ rows }) => {
  const renderRows = () => {
    return rows.map(row => {
      return (
        <Row {...row} key={row.outcomeID} />
      )
    })
  }

  return (
    <div className={styles.rowsContainer}>
      {renderRows()}
    </div>
  )
}

Rows.propTypes = {
  rows: PropTypes.array
}

Rows.defaultProps = {
  rows: []
}

export default Rows
