import React from 'react'
import PropTypes from 'prop-types'
import styles from './outcome.cssmodule.scss'

class OutcomeContainer extends React.PureComponent {
  static propTypes = {
    status: PropTypes.string,
    mediaType: PropTypes.string,
    outcome: PropTypes.shape({
      ID: PropTypes.number,
      outcomeDesc: PropTypes.string,
      result: PropTypes.string,
      image: PropTypes.string,
      rewardsGive: PropTypes.shape({
        userRewards: PropTypes.shape({
          money: PropTypes.number
        }),
        itemsRewards: PropTypes.objectOf(
          PropTypes.shape({
            count: PropTypes.number,
            label: PropTypes.string
          })
        )
      }),
      story: PropTypes.array
    })
  }

  static defaultProps = {
    status: '',
    mediaType: '',
    outcome: {
      ID: null,
      outcomeDesc: '',
      result: '',
      image: '',
      rewardsGive: {
        userRewards: {
          money: 0
        },
        itemsRewards: {
          1: {
            count: 0,
            label: ''
          }
        }
      },
      story: []
    }
  }

  _storyText = () => {
    const {
      outcome: { story = [] },
      mediaType
    } = this.props

    const getTextLayout = () => {
      if (!story.length || story.length <= 0) {
        return ''
      }

      const desktopLayout = story.map((stories, i) => <p key={i} dangerouslySetInnerHTML={{ __html: stories }} />)
      const mobileLayout = story.map((stories, i) => (
        <span key={i} dangerouslySetInnerHTML={{ __html: stories + ' ' }} />
      ))

      if (mediaType !== 'desktop') {
        return mobileLayout
      }

      return desktopLayout
    }

    return getTextLayout()
  }

  _getRewards = () => {
    const {
      outcome: { rewardsGive = {} }
    } = this.props
    const { itemsRewards, userRewards } = rewardsGive || {}

    if (!itemsRewards) return null

    const itemReward = () => {
      const isRewardsObject = typeof itemsRewards === 'object'

      if (isRewardsObject) {
        return Object.keys(itemsRewards).map(key => {
          const { label, count } = itemsRewards[key]

          return (
            <div key={label} className={styles.itemsRewards}>
              <div className={styles.imageWrap} title={label}>
                <img alt='outcome_rewards' src={`/images/items/${key}/medium.png`} />
                <div className={styles.count}>
                  <span>x{count}</span>
                </div>
              </div>
            </div>
          )
        })
      }

      return <span>Rewads given: {itemsRewards}</span>
    }

    const moneyReward = () => {
      if (!userRewards) return null

      return (
        <div className={styles.moneyRewards}>
          <span>Money reward: {userRewards.money}</span>
        </div>
      )
    }

    return (
      <div className={styles.rewardsWrap}>
        {itemReward()}
        {moneyReward()}
      </div>
    )
  }

  _getMainText = () => {
    const {
      outcome: { result }
    } = this.props

    return <span className={styles.mainText}>{result}</span>
  }

  _getDescription = () => {
    const {
      outcome: { result, outcomeDesc, customDesc },
      mediaType
    } = this.props

    if (!outcomeDesc) return null

    const tooLongDescForMobileLayout = result.length > 8 && outcomeDesc.length > 8
    const cutTextFont = tooLongDescForMobileLayout && mediaType === 'mobile' ? styles.infoTextCutted : ''
    const specTextFont = !outcomeDesc.match(/\$/gi) ? styles.specResultFont : ''

    if (customDesc) {
      return (
        <span
          className={`${styles.infoText} ${cutTextFont} ${specTextFont}`}
          dangerouslySetInnerHTML={{ __html: outcomeDesc }}
        />
      )
    }

    return <span className={`${styles.infoText} ${cutTextFont} ${specTextFont}`}>{outcomeDesc}</span>
  }

  // DEV container! Should display only on dev-www.torn.com!
  _getOutcomeID = () => {
    const {
      outcome: { ID }
    } = this.props

    // this hack is made just for dev purposes and would not affect on Prod Server
    const isDevServer = window.location.href.match(/https?:\/\/(www.)?torn\.com/g)

    if (!ID || isDevServer) return null

    return <span className={styles.outcomeID}>{`Outcome #${ID}`}</span>
  }

  render() {
    const { status } = this.props

    return (
      <div className={`${styles.outcomeWrap} ${status}`}>
        <div className={styles.story}>{this._storyText()}</div>
        <div className={styles.result}>
          {this._getMainText()}
          {this._getDescription()}
          {this._getRewards()}
          {this._getOutcomeID()}
        </div>
      </div>
    )
  }
}

export default OutcomeContainer
