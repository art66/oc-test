const onEntering = props => {
  const { calcHeight, outcomeRef } = props

  calcHeight(outcomeRef.current)
}

const onEntered = props => {
  const { activeButton, getCurrentOutcome, outcomeHeight, outcomeRef } = props
  const { current } = outcomeRef

  activeButton()
  if (current !== null) {
    const outcomeOffestTop = current.firstChild.getBoundingClientRect().top + pageYOffset

    getCurrentOutcome(outcomeHeight, outcomeOffestTop)
  }
}

export { onEntering, onEntered }
