import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import classnames from 'classnames'
import { onEntering, onEntered } from './OutcomeSlider'
import OutcomeContainer from './OutcomeContainer'
import styles from './outcome.cssmodule.scss'
import './anim.scss'

class Outcome extends Component {
  static propTypes = {
    state: PropTypes.string,
    isCurrentRow: PropTypes.bool,
    counter: PropTypes.number,
    activeButton: PropTypes.func,
    getCurrentOutcome: PropTypes.func,
    isRowNotInProgress: PropTypes.bool,
    outcome: PropTypes.object,
    showOutcome: PropTypes.bool,
    mediaType: PropTypes.string
  }

  static defaultProps = {
    state: '',
    isCurrentRow: false,
    counter: 0,
    activeButton: () => {},
    getCurrentOutcome: () => {},
    isRowNotInProgress: false,
    outcome: {},
    showOutcome: false,
    mediaType: ''
  }

  constructor(props) {
    super(props)

    this.ref = React.createRef()

    this.state = {
      height: null
    }
  }

  // Disabled due to developer/admin manipulation purposes.
  // shouldComponentUpdate(nextProps) {
  //   const { outcome, mediaType } = this.props
  //   const outcomeChanged = JSON.stringify(outcome) !== JSON.stringify(nextProps.outcome)

  //   // update Outcome if viewport is changed or if outcome object is changed and allow to update
  //   if (mediaType !== nextProps.mediaType || outcomeChanged) {
  //     return true
  //   }

  //   // check if crime is currently not in fetch request stage
  //   if (!nextProps.isRowNotInProgress) {
  //     return false
  //   }

  //   return true
  // }

  componentDidUpdate(nextProps, nextState) {
    const { outcome } = this.props
    const { mediaType = '' } = outcome || {}

    const { outcome: nextOutcome } = nextProps || {}
    const { mediaType: nextMediaType = '' } = nextOutcome

    const newRef = (this.ref && this.ref.current) || document.body.querySelector('[class*="outcomeContainer"]') // get node throw native method because react does recorgonize dom changes under the CSSTransition Animation logic.
    const isViewportChanged = mediaType !== nextMediaType

    if (newRef && isViewportChanged) {
      this._calcHeight(newRef)
    }
  }

  _calcHeight = el => {
    if (el === null) return null

    const newHeight = el.firstChild.scrollHeight

    this.setState(prevState => ({
      ...prevState,
      height: newHeight
    }))
  }

  _zeroHeight = () => {
    this.setState(prevState => ({
      ...prevState,
      height: 0
    }))
  }

  _getClassStatus = () => {
    const { state } = this.props

    let status = ''

    if (state === 'success') {
      status = 'success'
    } else if (state === 'failed') {
      status = 'failure'
    } else {
      status = 'jailed'
    }

    return styles[status]
  }

  _isOutcomeToShow = () => {
    const { outcome, showOutcome } = this.props
    const isOutcomeShow = outcome && outcome.story && showOutcome
    // console.log(isOutcomeShow, 'isOutcomeShow')

    return isOutcomeShow
  }

  _renderOutcome = outcomeHeight => {
    const { activeButton, counter, outcome, isCurrentRow, getCurrentOutcome, mediaType } = this.props

    if (!this._isOutcomeToShow()) return null

    const status = this._getClassStatus()

    const animProps = {
      counter,
      outcomeHeight,
      isCurrentRow,
      activeButton,
      getCurrentOutcome,
      calcHeight: this._calcHeight,
      outcomeRef: this.ref
    }

    // console.log(this.props, CSSTransition, 'isCurrentRow')
    return (
      <CSSTransition
        classNames={counter > 1 ? 'currentRowOutcome' : 'newRowOutcome'}
        key={counter}
        timeout={{ enter: 350, exit: 1000 }}
        onEntering={() => onEntering(animProps)}
        onEntered={() => onEntered(animProps)}
        onExit={() => this._zeroHeight()}
        unmountOnExit
      >
        <div ref={this.ref} style={{ zIndex: counter }} className={`${styles.outcomeContainer} ${status}`}>
          <OutcomeContainer outcome={outcome} mediaType={mediaType} />
        </div>
      </CSSTransition>
    )
  }

  render() {
    const { height } = this.state
    const { isCurrentRow, outcome } = this.props
    const { mediaType: outcomeMediaType = '' } = outcome || {}

    // console.log(this.ref, outcomeHeight, itemId, crimeID, 'itemId === crimeID && isRowActive')

    const classes = classnames({
      [styles.outcome]: true,
      [styles[outcomeMediaType]]: outcomeMediaType,
      [styles.currentRow]: isCurrentRow
    })

    return (
      <TransitionGroup className={classes} data-set={isCurrentRow} style={{ height: `${height}px` }} component='div'>
        {this._renderOutcome(height)}
      </TransitionGroup>
    )
  }
}

export default Outcome
