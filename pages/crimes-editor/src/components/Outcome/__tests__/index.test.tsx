import React from 'react'
import { shallow } from 'enzyme'
import Outcome from '../index'
import initialState, { newOutcome, currentOutcome, successOutcome, failedOutcome, jailedOutcome } from './mocks/index'

describe('<Outcome Global />', () => {
  it('should render basic Outcome Component', () => {
    const Component = shallow(<Outcome {...initialState} />)

    expect(Component.find('CSSTransition')).toBeTruthy()
    expect(Component.find('OutcomeContainer')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
  it('should render new Outcome of the row', () => {
    const Component = shallow(<Outcome {...newOutcome} />)

    expect(Component.find('CSSTransition')).toBeTruthy()
    expect(Component.find('CSSTransition').prop('classNames')).toBe('newRowOutcome')
    expect(Component.find('.outcomeContainer')).toBeTruthy()
    expect(Component.find('OutcomeContainer')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
  it('should render current Outcome of the row', () => {
    const Component = shallow(<Outcome {...currentOutcome} />)

    expect(Component.find('CSSTransition')).toBeTruthy()
    expect(Component.find('CSSTransition').prop('classNames')).toBe('currentRowOutcome')
    expect(Component.find('.outcomeContainer')).toBeTruthy()
    expect(Component.find('OutcomeContainer')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
  it('should render success Outcome', () => {
    const Component = shallow(<Outcome {...successOutcome} />)

    expect(Component.find('CSSTransition')).toBeTruthy()
    expect(Component.find('.outcomeContainer').prop('className')).toContain('success')
    expect(Component.find('OutcomeContainer')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
  it('should render failed Outcome', () => {
    const Component = shallow(<Outcome {...failedOutcome} />)

    expect(Component.find('CSSTransition')).toBeTruthy()
    expect(Component.find('.outcomeContainer').prop('className')).toContain('failure')
    expect(Component.find('OutcomeContainer')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
  it('should render jailed Outcome', () => {
    const Component = shallow(<Outcome {...jailedOutcome} />)

    expect(Component.find('CSSTransition')).toBeTruthy()
    expect(Component.find('.outcomeContainer').prop('className')).toContain('jailed')
    expect(Component.find('OutcomeContainer')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
})
