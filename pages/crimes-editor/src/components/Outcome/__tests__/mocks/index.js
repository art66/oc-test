const initialState = {
  outcome: {
    result: 'success',
    desc: 'Test Description',
    story: ['Test Story']
  },
  counter: 0,
  isCurrentRow: false,
  mediaType: 'desktop',
  isRowNotInProgress: false,
  state: 'success',
  showOutcome: true,
  getCurrentOutcome: () => {},
  activeButton: () => {}
}

export const newOutcome = {
  ...initialState,
  counter: 1,
  isCurrentRow: false
}

export const currentOutcome = {
  ...initialState,
  counter: 2,
  isCurrentRow: true
}

export const successOutcome = {
  ...initialState,
  state: 'success'
}

export const failedOutcome = {
  ...initialState,
  state: 'failed'
}

export const jailedOutcome = {
  ...initialState,
  state: 'jailed'
}

export default initialState
