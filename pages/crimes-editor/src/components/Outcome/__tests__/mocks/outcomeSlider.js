import React from 'react'

const onEnteringState = {
  calcHeight: () => {},
  outcomeRef: {
    current: <div>Test Node</div>
  }
}

const onEnteredState = {
  activeButton: () => {},
  getCurrentOutcome: () => {},
  outcomeHeight: () => {},
  outcomeRef: {
    current: null
  }
}

export { onEnteringState, onEnteredState }
