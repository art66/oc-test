import { onEnteredState, onEnteringState } from './mocks/outcomeSlider'
import { onEntered, onEntering } from '../OutcomeSlider'

describe('Slider Outcome onEntered / onEntering callbacks', () => {
  it('should responce OutcomeSlider', () => {
    expect(onEntering(onEnteringState)).toBeUndefined()
    expect(onEntered(onEnteredState)).toBeUndefined()
  })
})
