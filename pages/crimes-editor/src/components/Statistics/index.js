import PropTypes from 'prop-types'
import React, { PureComponent, Fragment } from 'react'
import './styles.scss'

class Statistics extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      inProgress: false,
      value: ''
    }
  }

  _action = () => {
    const { iteratedResults } = this.props

    this.setState({
      inProgress: true
    })

    iteratedResults()
  }

  _handleChange = e => {
    const { getInputValue } = this.props

    this.setState({
      value: e.target.value
    })

    getInputValue(e.target.value)
  }

  _makeStatsList = chartStats => {
    if (!chartStats) return

    const statsList = Object.keys(chartStats).map((stat, i) => {
      if (typeof chartStats[stat] === 'object') {
        return (
          <div className="chart-stat" key={stat}>
            <div className="chart-stat-index">
              <span>{i}. </span>
            </div>
            <div className="chart-stat-lable">
              <span>{chartStats[stat].label}</span>
            </div>
            <div className="chart-stat-amount">
              <span>{chartStats[stat].amount}</span>
            </div>
          </div>
        )
      }
    })

    return statsList
  }

  _statsPlaceholder = () => {
    return (
      <div className="iterated-table-placeholder">
        <span className="placeholder-text">
          Empty Data! Please, enter iteration count above and click 'Get Statistics' button
        </span>
      </div>
    )
  }

  _errorPlaceholder = errorMessage => {
    return (
      <div className="iterated-table-error">
        <span className="error-text">RESPONSE ERROR: "{errorMessage}"!</span>
      </div>
    )
  }

  _statsHolder = chartStats => {
    if (chartStats.error) {
      return this._errorPlaceholder(chartStats.error)
    }

    return (
      <div className="iterated-table-data">
        <div className="iterated-table-legend">
          <span className="legend-index">№</span>
          <span className="legend-lable">Label</span>
          <span className="legend-amount">Count</span>
        </div>
        {this._makeStatsList(chartStats)}
      </div>
    )
  }

  _fetchButton = () => {
    const { chartStatsFetch } = this.props
    const { value } = this.state
    const ajaxAnim = '/images/v2/main/ajax-loader.gif'
    const btnClass = value ? 'btn-fetch torn-btn gold' : 'btn-fetch torn-btn gold btn-disabled'

    return (
      <Fragment>
        {chartStatsFetch ? (
          <button type='button' className="torn-btn gold btn-gold-fetch">
            <div className="btn-img-container">
              <img className="btn-img-anim" src={ajaxAnim} alt="loads snake" />
            </div>
          </button>
        ) : (
          <button className={btnClass} onClick={this._action}>
            Get statistics
          </button>
        )}
      </Fragment>
    )
  }

  render() {
    const { chartStats = {} } = this.props
    const isChartStatsEmply = !Object.keys(chartStats).length

    return (
      <div className="iterated-results-container">
        <div className="iterated-input-holder">
          <span>Enter iteration count:</span>
          <input
            className="iterated-input"
            type="number"
            placeholder="Count of iteration"
            value={this.state.value}
            onChange={this._handleChange}
          />
          {this._fetchButton()}
        </div>
        <div className="iterated-results-table">
          <div className="iterated-table-title">
            <span>Statistic data results:</span>
          </div>
          {isChartStatsEmply ? this._statsPlaceholder() : this._statsHolder(chartStats)}
        </div>
      </div>
    )
  }
}

Statistics.propTypes = {
  chartStats: PropTypes.object,
  getInputValue: PropTypes.func,
  iteratedResults: PropTypes.func,
  chartStatsFetch: PropTypes.bool
}

export default Statistics
