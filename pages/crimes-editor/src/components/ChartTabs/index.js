import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'

class ChartTabs extends PureComponent {
  render() {
    const {
      buttonChartFilter,
      classBtnsNames: { mainBtnClass, sellBtnClass, marketBtnClass, iteratedBtnClass }
    } = this.props

    return (
      <div className="chart-button-container">
        <div className="button-chart-title-holder">
          <span>Choose Charts Layout:</span>
        </div>
        <button data-set="Sell value" className={mainBtnClass + ' ' + sellBtnClass} onClick={e => buttonChartFilter(e)}>
          Sell value
        </button>
        <button
          data-set="Market value"
          className={mainBtnClass + ' ' + marketBtnClass}
          onClick={e => buttonChartFilter(e)}
        >
          Market value
        </button>
        <button
          data-set="Iterated results"
          className={mainBtnClass + ' ' + iteratedBtnClass}
          onClick={e => buttonChartFilter(e)}
        >
          Iterated results
        </button>
      </div>
    )
  }
}

ChartTabs.propTypes = {
  classBtnsNames: PropTypes.object,
  buttonChartFilter: PropTypes.func
}

export default ChartTabs
