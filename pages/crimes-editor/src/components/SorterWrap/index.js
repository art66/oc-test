import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import classnames from 'classnames'
import g from '../../styles/global.cssmodule.scss'
import s from './styles.cssmodule.scss'

const SORT_ITEMS_LIST = [
  {
    value: 'timestamp',
    label: 'id/date'
  },
  {
    value: 'minCrimeLevel',
    label: 'level'
  },
  {
    value: 'value',
    label: 'reward'
  },
  {
    value: 'triggers',
    label: 'trigger'
  },
  {
    value: 'rarity',
    label: 'rarity'
  },
  {
    value: 'chance',
    label: 'chance'
  },
  {
    value: 'stage',
    label: 'stage'
  }
]

class SorterWrap extends Component {
  static propTypes = {
    sortOrder: PropTypes.string,
    sortOrderEmit: PropTypes.func,
    createNewOutcome: PropTypes.func,
    editedItem: PropTypes.number,
    filterOutcomes: PropTypes.func,
    outcomesFilter: PropTypes.string,
    selectedStatsTab: PropTypes.string,
    sortBy: PropTypes.func
  }

  static defaultProps = {
    sortOrder: '',
    sortOrderEmit: () => {},
    createNewOutcome: () => {},
    filterOutcomes: () => {},
    editedItem: null,
    outcomesFilter: '',
    selectedStatsTab: '',
    sortBy: () => {}
  }

  _btnEnabledCheck = () => {
    const { editedItem, selectedStatsTab } = this.props

    const isSomeItemEditable = isNaN(editedItem) || editedItem === null
    const btnEnabled = isSomeItemEditable && selectedStatsTab

    return btnEnabled
  }

  _classnameHolder = () => {
    const { selectedStatsTab } = this.props

    const btnEnabled = this._btnEnabledCheck()

    const c1 = classnames({
      [g['white-box']]: true,
      [s['sorter']]: true,
      [s['disabled']]: !selectedStatsTab
    })

    const c2 = classnames({
      [g['white-box']]: true,
      [s['new-outcome-button']]: true,
      [s['disabled']]: !btnEnabled
    })

    const buttonCreateOutcome = classnames({
      [s.buttonCreateOutcome]: true,
      [s.transform]: true,
      [s.font]: true
    })

    return {
      c1,
      c2,
      buttonCreateOutcome
    }
  }

  _handleFilterOutcomes = event => {
    const { filterOutcomes } = this.props
    const { value } = event.target

    if (!filterOutcomes) return

    filterOutcomes(value)
  }

  _handleReoder = orderStatus => {
    const { sortOrderEmit } = this.props

    if (!sortOrderEmit || !orderStatus) return

    sortOrderEmit(orderStatus)
  }

  _handleCreateOutcome = () => {
    const { createNewOutcome } = this.props
    const btnEnabled = this._btnEnabledCheck()

    if (!btnEnabled || !createNewOutcome) return

    createNewOutcome()
  }

  _handleSort = sortTipe => {
    const { selectedStatsTab, sortBy } = this.props

    if (!selectedStatsTab || !sortBy || !sortTipe) return

    sortBy(sortTipe)
  }

  _sortItem = item => {
    const { value = '', label = '' } = item

    return (
      <label key={value} htmlFor={value}>
        <input
          id={value}
          onClick={() => this._handleSort(value)}
          type='radio'
          name='filter'
          value={value}
        />
        <span>{label}</span>
      </label>
    )
  }

  _renderSortItemsList = () => {
    const listToRender = SORT_ITEMS_LIST.map(sortItem => {
      return this._sortItem(sortItem)
    })

    return listToRender
  }

  _renderSortOrder = () => {
    const { sortOrder } = this.props

    return (
      <Fragment>
        <label htmlFor='up' className={s.sortOrderInput}>
          <input
            id='up'
            className={`${s.sortTriangle} ${s.sortTriagleUp}`}
            onChange={() => this._handleReoder('asc')}
            type='radio'
            name='sort'
            value='up'
            checked={sortOrder === 'asc'}
          />
        </label>
        <label htmlFor='up' className={s.sortOrderInput}>
          <input
            id='up'
            className={`${s.sortTriangle} ${s.sortTriangleDown}`}
            onChange={() => this._handleReoder('desc')}
            type='radio'
            name='sort'
            value='down'
            checked={sortOrder !== 'asc'}
          />
        </label>
      </Fragment>
    )
  }

  _renderSearchInput = () => {
    const { outcomesFilter } = this.props

    return (
      <div className={s['search']}>
        <input
          className={s['search-input']}
          onChange={this._handleFilterOutcomes}
          type='text'
          value={outcomesFilter}
          placeholder='filter by ID/Title/Story...'
        />
      </div>
    )
  }

  render() {
    const { c1, c2, buttonCreateOutcome } = this._classnameHolder()

    return (
      <div>
        <hr className='page-head-delimiter m-top10 m-bottom10' />
        <div className={s['wrap']}>
          <div className={c1}>
            <div className={s.sortOrderContainer}>
              {this._renderSortOrder()}
            </div>
            <span>Sort by:</span>
            {this._renderSortItemsList()}
            {this._renderSearchInput()}
          </div>
          <div className={c2}>
            <button
              type='button'
              onClick={this._handleCreateOutcome}
              className={buttonCreateOutcome}
            >
              Create New Outcome
            </button>
          </div>
        </div>
      </div>
    )
  }
}

export default SorterWrap
