import PropTypes from 'prop-types'
import React, { Component } from 'react'
import * as _ from 'lodash'

export class LimitedInput extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.value
    }
  }
  _checkCharsLeft = event => {
    const { value: oldValue } = this.state
    let { value, selectionStart, selectionEnd, scrollLeft } = event.target
    let limit = false
    event.target.scrollLeft = 9999
    event.target.selectionStart = selectionStart
    event.target.selectionEnd = selectionEnd
    // if (scrollLeft !== 0 && event.target.value.length >= oldValue.length) {
    if (scrollLeft !== 0) {
      limit = true
    }
    value = limit ? oldValue : value
    this.setState({ limit, value })
    return value
  }
  onChange = _.flow(this._checkCharsLeft, this.props.onChange)
  render() {
    const { width, className } = this.props
    const { value, limit } = this.state
    const style = { width, ...(limit && { borderColor: '#e2805f' }) }
    return <input type="text" style={style} value={value} onChange={this.onChange} className={className} />
  }
}

LimitedInput.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  width: PropTypes.number.isRequired
}

export default LimitedInput
