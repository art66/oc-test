import React, { PureComponent } from 'react'
import Tooltip from '@torn/shared/components/TooltipNew'

import { IProps } from './types/index'
import styles from './index.cssmodule.scss'

const ANIM_PROPS = {
  className: 'crimesEditorTooltip',
  timeExit: 0,
  timeIn: 100
}

const STYLE_PROPS = {
  container: styles.container,
  title: styles.title,
  tooltipArrow: styles.tooltipArrow
}

class ToolTip extends PureComponent<IProps> {
  render() {
    return <Tooltip animProps={ANIM_PROPS} overrideStyles={STYLE_PROPS} />
  }
}

export default ToolTip
