/* eslint-disable max-statements */
import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import classnames from 'classnames'
import { get } from 'lodash'
import { toMoney } from '@torn/shared/utils'
import { RESULTS } from '../OutcomeEditor/constants'
import OutcomeEditor from '../OutcomeEditor'
import Chart from '../Chart'
import Rows from '../Row'
import g from '../../styles/global.cssmodule.scss'
import s from './styles.cssmodule.scss'

const STAGES = ['released', 'beta', 'alpha']

const MISSING_KEY_PROPS = [{ rarity: 'chance' }, { chanceLabel: 'chance' }, { trigger: 'triggers' }]

const MULTI_REWARDS_LABEL = 'Multi Rewards'
const MULTI_ITEMS_LABEL = 'Multi Items'

class Stats extends Component {
  _header = () => {
    const { selectedStatsTab } = this.props

    return (
      <div className={s['th'] + ' ' + s[selectedStatsTab]}>
        <div className={s['thc']}>ID</div>
        <div className={s['thc']}>Level</div>
        <div className={s['thc']}>Name</div>
        <div className={s['thc']}>Result</div>
        <div className={s['thc']}>Value</div>
        <div className={s['thc']}>Trigger</div>
        <div className={s['thc']}>Creator</div>
        <div className={s['thc']}>Rarity</div>
        <div className={s['thc'] + ' ' + s['chance']}>Chance</div>
        <div className={s['thc'] + ' ' + s['center']}>Stage</div>
      </div>
    )
  }

  _table = () => {
    const {
      chartData,
      crimeID,
      drops,
      editedItem,
      editOutcome,
      loading,
      outcomes,
      outcomesFilter,
      selectedStatsTab,
      sortByField,
      sortOrder,
      submit,
      thirdLine,
      typeConditions
    } = this.props
    const rows = []

    const sortType = sortByField || 'timestamp'
    const sortOrderBy = sortOrder === 'asc' ? 'minToMax' : 'maxToMin'
    const posNegInfinity = sortOrderBy === 'maxToMin' ? -Infinity : Infinity

    if (!outcomes[selectedStatsTab] || outcomes[selectedStatsTab].length === 0) {
      return (
        <div className={s['tr']}>
          <div className={s['no-records']}>No records in this category</div>
        </div>
      )
    }

    // this function is responce for extra props checking and adding
    // in case if they need to be placed as an sort triggers
    const findMissingProps = (object, keyProps) => {
      if (!keyProps || keyProps.length === 0) return null

      const propsObj = {}

      keyProps.forEach(prop => {
        const propKey = Object.values(prop).toString()
        const propValue = Object.keys(prop).toString()
        const missingItem = drops[propKey].find(item => Number(object[propKey]) === Number(item.value))

        if (!missingItem) return

        const { ID = null, label = null } = missingItem

        if (propValue === 'rarity') {
          propsObj[propValue] = ID

          return
        }

        propsObj[propValue] = label
      })

      return propsObj
    }

    const findStage = object => {
      object.meta === 'new' && console.log(object)
      const perm = (object.perm || 1) - 1
      const stageLabel = STAGES[perm]

      return {
        stage: object.perm,
        stageLabel
      }
    }

    const sortedOutcomesList = outcomes[selectedStatsTab]
      .map((item, i) => ({
        // adds the rarity/chanceLabel/trigger and stage props for each objects because of missing
        outcomeID: i,
        ...item,
        ...findMissingProps(item, MISSING_KEY_PROPS),
        ...findStage(item)
      }))
      .filter(outcome => {
        // sort Outcomes by ID/title/text before layout
        if (!outcome.desc || !outcomesFilter) {
          return true
        }

        const searchableProps = [outcome.ID, outcome.name, outcome.desc]

        // check if some of the searchable props if matched, that return such object
        const isObjectMatchFound = searchableProps.some(prop => {
          const propToMatch = new RegExp(outcomesFilter, 'gi')
          const matchFound = propToMatch && prop && prop.toString().match(propToMatch) || false

          return matchFound
        })

        return isObjectMatchFound
      })
      .sort((a, b) => {
        // check if the user is started a new Outcome row creation,
        // then place such row on the top of the list
        if (a.meta === 'new' || b.meta === 'new') {
          return
        }

        // check if there no value in object to sort,
        // then equaling this value to the max Infiniti value and place such object to the end of the list
        const prevValue = Number(a[sortType]) || posNegInfinity
        const nextValue = Number(b[sortType]) || posNegInfinity

        if (sortOrderBy === 'maxToMin') {
          return nextValue - prevValue
        }

        return prevValue - nextValue
      })

    sortedOutcomesList.forEach((row, i) => {
      const { result, resultValue } = this._getRewardValue(JSON.parse(row.rewards || '{}'))

      let children = null

      if (editedItem === i) {
        children = (
          <Fragment>
            <Chart
              key={'chart' + i}
              className={g['inline-chart']}
              data={thirdLine ? [...chartData, thirdLine] : [...chartData]}
            />
            <OutcomeEditor
              key={'editor' + i}
              {...row}
              crimeID={crimeID}
              loading={loading}
              submit={submit}
              typeConditions={typeConditions}
            />
          </Fragment>
        )
      }

      const rowWithProps = {
        ...row,
        crimeID,
        editOutcome,
        index: i,
        key: i + resultValue,
        result,
        resultValue,
        children
      }

      rows.push(rowWithProps)
    })

    return <Rows rows={rows} />
  }

  _checkMultiRewards = rewards => {
    if (!rewards) {
      return false
    }

    const immutableRewards = {
      ...rewards
    }

    delete immutableRewards.manualMode

    return immutableRewards.length > 1
  }

  _getRewardValue = (rewards = []) => {
    const result = rewards && Object.keys(rewards)[0] || null
    const isMultiRewards = result && result.length > 1

    if (this._checkMultiRewards(rewards)) {
      return MULTI_REWARDS_LABEL
    }

    const result2 = RESULTS.find(r => r.value === result)
    const result3 = result2 ? result2.label : result

    let value

    if (result === 'money') {
      value = '$' + toMoney(rewards[result]['min']) + ' - $' + toMoney(rewards[result]['max'])
    } else if (['item', 'items'].includes(result)) {
      const rewardsKeys = Object.keys(rewards[result])
      const firstItemsRewardID = rewardsKeys[0]

      const isMultiple = rewardsKeys.length > 1
      const isSingleItemMissed = !['undefined'].includes(firstItemsRewardID)

      const multipleItemsID = rewards[result][firstItemsRewardID]?.itemID

      const itemID = isSingleItemMissed ? firstItemsRewardID : multipleItemsID

      value = isMultiple ? MULTI_ITEMS_LABEL : '<img src=/images/items/' + itemID + '/small.png />'
    } else if (result === 'points') {
      value = rewards[result]['min'] + ' - ' + rewards[result]['max']
    } else if (result === 'jailTime') {
      value = rewards[result]['min'] + 'min'
    } else if (result === 'hospTime') {
      value = get(rewards[result], 'looseLife.min') + '% - ' + get(rewards[result], 'hospTime.min') + 'min'
    } else if (result === 'costMoney') {
      value = '$' + rewards[result]['max']
    }
    return { result: result3, resultValue: value }
  }

  render() {
    const { outcomes, selectedStatsTab, switchStatsTab } = this.props

    const c = classnames({
      [g['white-box']]: true,
      [g['conected']]: selectedStatsTab,
      'm-top10': true,
      [s['tabs']]: true
    })

    return (
      <div className={s['wrap']}>
        <div className={c}>
          <div
            className={s['tab'] + ' ' + s['success'] + ' ' + (selectedStatsTab === 'success' ? s['selected'] : '')}
            onClick={() => switchStatsTab('success')}
          >
            Success ({(outcomes.success || []).length})
          </div>
          <div className={g['divider']} />
          <div
            className={s['tab'] + ' ' + s['failure'] + ' ' + (selectedStatsTab === 'fail' ? s['selected'] : '')}
            onClick={() => switchStatsTab('fail')}
          >
            Failure ({(outcomes.fail || []).length})
          </div>
          <div className={g['divider']} />
          <div
            className={s['tab'] + ' ' + s['critical'] + ' ' + (selectedStatsTab === 'jail' ? s['selected'] : '')}
            onClick={() => switchStatsTab('jail')}
          >
            Critical failure ({(outcomes.jail || []).length})
          </div>
        </div>
        {selectedStatsTab && (
          <div className={g['bottom-round']}>
            {this._header()}
            {this._table()}
          </div>
        )}
      </div>
    )
  }
}

Stats.propTypes = {
  chartData: PropTypes.array,
  crimeID: PropTypes.number,
  drops: PropTypes.object,
  editedItem: PropTypes.number,
  editOutcome: PropTypes.func,
  loading: PropTypes.bool,
  outcomes: PropTypes.object,
  outcomesFilter: PropTypes.string,
  selectedStatsTab: PropTypes.string,
  sortByField: PropTypes.string,
  sortOrder: PropTypes.string,
  submit: PropTypes.func,
  switchStatsTab: PropTypes.func,
  thirdLine: PropTypes.array
}

export default Stats
