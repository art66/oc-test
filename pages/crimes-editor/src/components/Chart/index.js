/* global $ */
import PropTypes from 'prop-types'
import classname from 'classnames'
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import ChartTabs from '../ChartTabs'
import Statistics from '../Statistics'
import { loadChartStats } from '../../modules'
import 'flot/jquery.flot.js'
import 'flot/jquery.flot.time.js'
// import 'flot/jquery.flot.resize.js'
import 'flot/jquery.flot.selection.js'
import './styles.scss'

const CHART_OPTIONS = {
  series: {
    lines: {
      show: true,
      fill: 0,
      lineWidth: 1
    },
    points: {
      show: false,
      radius: 1.5
    },
    shadowSize: 1
  },
  grid: {
    hoverable: true,
    clickable: true,
    color: '#bbb',
    // backgroundColor: "#f2f2f2",
    backgroundColor: '#fff',
    show: true,
    aboveData: false,
    margin: { top: 0, right: 10, bottom: 10, left: 25 },
    borderWidth: 2,
    borderColor: '#ddd',
    minBorderMargin: 10
  },
  legend: {
    show: true,
    // noColumns: 0,
    backgroundOpacity: 0,
    container: '.legend-container span'
  },
  selection: {
    mode: 'xy'
  },
  yaxis: {
    min: 0,
    tickFormatter: function(value) {
      return '$' + value
    }
  },
  xaxis: {
    mode: 'number',
    min: 0.999,
    max: 100,
    ticks: [
      ...Array(21)
        .fill(0)
        .map((z, i) => (i === 0 ? 1 : i * 5))
    ]
    // tickSize: 1
  }
}

const COLOR_SETTINGS = [
  {
    label: 'Recommended',
    color: 'rgba(205,205,205,1)'
  },
  {
    label: 'Current',
    color: 'rgba(165,198,66,1)'
  },
  {
    label: 'Market',
    color: 'rgba(220,0,0,.7)'
  }
]

class Chart extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: [],
      value: '',
      sellValueBtn: true,
      marketValueBtn: false,
      iteratedResultsBtn: false
    }
  }

  _prepareData(data) {
    const datas = data.map((dataSet, i) => ({
      ...COLOR_SETTINGS[i],
      data: [
        ...Array(100)
          .fill(0)
          .map((z, j) => [j + 1, dataSet[j]])
      ]
    }))

    return datas
  }

  showTooltip(x, y, contents) {
    $("<div id='white-tooltip' class='white-tooltip'></div>")
      .append("<div class='ui-tooltip-content'>" + contents + "<div class='tooltip-arrow left bottom'></div></div>")
      .appendTo('body')
      .fadeIn(200)

    let $tooltip = $('#white-tooltip')
    let tooltipWidth = $tooltip.width()
    let tooltipHeight = $tooltip.height()
    $tooltip.css({
      position: 'absolute',
      top: y - tooltipHeight - 20,
      left: x - tooltipWidth / 2
    })
  }

  _buttonChartFilter = e => {
    const targetName = e.target.dataset.set
    const { sellValueBtn, marketValueBtn, iteratedResultsBtn } = this.state

    if (targetName === 'Sell value') {
      this.setState({
        sellValueBtn: !sellValueBtn,
        iteratedResultsBtn: false
      })
    } else if (targetName === 'Market value') {
      this.setState({
        marketValueBtn: !marketValueBtn,
        iteratedResultsBtn: false
      })
    } else if (targetName === 'Iterated results') {
      this.setState({
        iteratedResultsBtn: !iteratedResultsBtn,
        marketValueBtn: false,
        sellValueBtn: false
      })
    }
  }

  _iteratedResults = () => {
    const { loadChartStats, crimeIndex } = this.props
    const { value } = this.state

    loadChartStats(crimeIndex, value)
  }

  _getInputValue = value => {
    this.setState({
      value: value
    })
  }

  componentDidMount() {
    this.setState({
      data: this._prepareData(this.props.data)
    })
    const { data } = this.state
    const $chart = $(this.refs.chart)
    $.plot(this.refs.chart, data, CHART_OPTIONS)
    let previousPoint = null
    $chart.bind(
      'plothover',
      function(event, pos, item) {
        if (item) {
          if (previousPoint !== item.dataIndex) {
            previousPoint = item.dataIndex
            $('#white-tooltip').remove()
            this.showTooltip(item.pageX, item.pageY, item.datapoint[1])
          }
        } else {
          $('#white-tooltip').remove()
          previousPoint = null
        }
      }.bind(this)
    )
  }

  componentDidUpdate() {
    const { data, sellValueBtn, marketValueBtn } = this.state
    const newChartData =
      marketValueBtn && sellValueBtn
        ? data
        : sellValueBtn
          ? [data[0], data[1]]
          : marketValueBtn
            ? [data[0], data[2]]
            : [data[0]]
    const $chart = $(this.refs.chart)
    $.plot($chart, newChartData, CHART_OPTIONS)
  }

  render() {
    const { sellValueBtn, marketValueBtn, iteratedResultsBtn } = this.state
    const { className, chartStatsFetch, crimes = [], crimeIndex } = this.props
    const currentCrime = crimes.filter(crime => crime.ID === crimeIndex) || []
    const chartStats = !!currentCrime.length && currentCrime[0].chartStats || {}
    const chartsHideOrShow = iteratedResultsBtn ? 'none' : 'block'

    const classBtnsNames = {
      mainBtnClass: classname('button-white-box', 'new-chart-button'),
      sellBtnClass: sellValueBtn ? 'new-chart-button--active' : '',
      marketBtnClass: marketValueBtn ? 'new-chart-button--active' : '',
      iteratedBtnClass: iteratedResultsBtn ? 'new-chart-button--active' : ''
    }

    return (
      <Fragment>
        <ChartTabs classBtnsNames={classBtnsNames} buttonChartFilter={this._buttonChartFilter} />
        <div className={className}>
          <div className="legend-container" style={{ display: chartsHideOrShow }}>
            <span />
          </div>
          <div className="label-vertical" style={{ display: chartsHideOrShow }}>
            Value per nerve
          </div>
          {iteratedResultsBtn && (
            <Statistics
              getInputValue={this._getInputValue}
              chartStatsFetch={chartStatsFetch}
              chartStats={chartStats}
              iteratedResults={this._iteratedResults}
            />
          )}
          <div
            className="chart-container left"
            ref="chart"
            style={{ width: 976, height: 218, display: chartsHideOrShow }}
          />
          <div className="label-horizontal" style={{ display: chartsHideOrShow }}>
            <span>Skill level</span>
          </div>
        </div>
      </Fragment>
    )
  }
}

Chart.propTypes = {
  data: PropTypes.array,
  crimes: PropTypes.array,
  crimeIndex: PropTypes.number,
  className: PropTypes.string,
  loadChartStats: PropTypes.func,
  chartStatsFetch: PropTypes.bool
}

const mapStateToProps = state => ({
  crimes: state.app.crimes,
  crimeIndex: state.app.outcomeCrimeID,
  chartStatsFetch: state.app.chartStatsFetch
})

const mapActionsToProps = {
  loadChartStats
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Chart)
