import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dropdown from 'react-select'
import { setSelectValue, loadCrime, editCrime } from '../../modules'
import classnames from 'classnames'
import striptags from 'striptags'
import s from './styles.cssmodule.scss'
import g from '../../styles/global.cssmodule.scss'
import 'react-select/dist/react-select.css'

class CrimeSelect extends Component {
  _handleChange = (selectName, item) => {
    const { setSelectValue, loadCrime } = this.props
    setSelectValue({
      selectName,
      value: item ? item.value : null
    })
    if (selectName === 'subcrime') {
      loadCrime(item.value)
    }
  }
  render() {
    const { dropDownState = {}, crimes, crimesTypes, connected, editCrime } = this.props
    const crimeDD1Oprions = crimesTypes.map(crime => ({ value: crime.ID, label: crime.title }))
    const crimeDD2Oprions = crimes
      .filter(crime => crime.typeID === dropDownState.crime)
      .map(crime => ({ value: crime.ID, label: striptags(crime.title) }))
    const c = classnames({
      [g['white-box']]: true,
      [g['conected']]: connected
    })

    return (
      <div className={c}>
        <Dropdown
          className={g['dropdown'] + ' ' + s['dropdown']}
          placeholder="Select crime"
          options={crimeDD1Oprions}
          value={dropDownState.crime}
          onChange={item => {
            this._handleChange('crime', item)
          }}
        />
        <div className={g['divider']} />
        <Dropdown
          className={g['dropdown'] + ' ' + s['dropdown']}
          placeholder="Select subcrime"
          options={crimeDD2Oprions}
          value={dropDownState.subcrime}
          onChange={item => {
            this._handleChange('subcrime', item)
          }}
        />
        {(dropDownState.crime) && <div className={s['settings-icon']} onClick={editCrime} />}
      </div>
    )
  }
}

CrimeSelect.propTypes = {
  connected: PropTypes.bool,
  crimes: PropTypes.array,
  crimesTypes: PropTypes.array,
  dropDownState: PropTypes.object,
  editCrime: PropTypes.func,
  loadCrime: PropTypes.func,
  setSelectValue: PropTypes.func
}

const mapStateToProps = state => ({
  dropDownState: state.app.dropDownState,
  crimes: state.app.crimes,
  crimesTypes: state.app.crimesTypes
})

const mapActionsToProps = {
  editCrime,
  loadCrime,
  setSelectValue
}

export default connect(mapStateToProps, mapActionsToProps)(CrimeSelect)
