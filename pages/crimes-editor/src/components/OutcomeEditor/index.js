/* eslint-disable max-statements */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Dropdown from 'react-select'
import curry from 'lodash.curry'
import debounce from 'lodash.debounce'
import get from 'lodash.get'
import cn from 'classnames'
import 'react-select/dist/react-select.css'

import isValue from '@torn/shared/utils/isValue'
import isArray from '@torn/shared/utils/isArray'
import Outcome from '@torn/crimes/src/components/Outcome'
import isEmpty from '@torn/shared/utils/isEmpty'
import isObject from '@torn/shared/utils/isObject'

import Loading from '@torn/shared/components/AnimationLoad'

import { toMoney } from '@torn/shared/utils'
import {
  hideEditor,
  invalidateOutcome,
  requestChartUpdate,
  submit,
  requestOutcome,
  changeOutcomeLayout
} from '../../modules'

import LimitedInput from '../LimitedInput'

import {
  COOLDOWN_VALUES,
  RESULTS,
  NO_OUTCOME_PLACEHOLDER,
  STAGE_ACTIONS,
  SETTINGS_LABELS,
  MEDIAQUERY_LAYOUTS,
  LIST_KEYS,
  REQUEST_ADAPTER_KEYS,
  STAGE_IDS
} from './constants'

import g from '../../styles/global.cssmodule.scss'
import s from './styles.cssmodule.scss'

class OutcomeEditor extends Component {
  constructor(props) {
    super(props)

    this.state = {
      formData: this._mapFormToState(props),
      realChance: props.realChance,
      manualMode: false,
      counter: 0,
      special: 0,
      isSaveRequest: false,
      isDeleteRequest: false,
      isUnreleaseRequest: false,
      isStageRequest: false,
      isOutcomeRequest: false
    }

    const { requestChartUpdate: localRequestChartUpdate } = this.props

    this.requestChartUpdate = debounce(localRequestChartUpdate, 1000, {
      leading: false
    })
    this.filteredResults = RESULTS.filter(({ avaliable }) => avaliable.indexOf(props.selectedStatsTab) !== -1)
  }

  shouldComponentUpdate() {
    return true
  }

  componentDidUpdate(prevProps) {
    const { loading } = this.props
    const isEmitRequestFinish = prevProps.loading !== loading && !loading

    isEmitRequestFinish && this._clearCurrentActionStatus()
  }

  _getConditionData = (typeCondition = [], typeConditions = {}) => {
    if (!typeConditions) {
      return []
    }

    const conditionKeys = Object.keys(typeConditions)
    const currentConditions = []

    conditionKeys.forEach(key => {
      typeConditions[key].forEach(condition => {
        if (isArray(typeCondition) && typeCondition.some(currentCondition => currentCondition === condition.value)) {
          currentConditions.push(condition)
        }
      })
    })

    return currentConditions
  }

  _mapFormToState = props => {
    return {
      chance: Number(props.chance) || 10000,
      cooldown: props.cooldown,
      special: props.special,
      crimeID: props.crimeID,
      itemsNeeded: props.itemsNeeded,
      minLevel: props.minCrimeLevel || 1,
      maxLevel: props.maxCrimeLevel || 100,
      name: props.name,
      new: !props.creatorID,
      outcomeID: props.outcomeID,
      outcomeType: props.selectedStatsTab,
      realChance: props.realChance,
      rewards: this._flatternRewards(JSON.parse(props.rewards || '{}')),
      story: Array.isArray(props.desc) ? props.desc : [props.desc],
      triggers: props.triggers,
      typeConditions: props.typeConditions || {},
      typeCondition: {
        currentType: null,
        lastUsed: null,
        list: this._getConditionData(props.typeCondition, props.typeConditions)
      }
    }
  }

  // eslint-disable-next-line max-statements
  _flatternRewards = rewards => {
    const immutableRewards = {
      ...rewards
    }

    if (isValue(immutableRewards.manualMode)) {
      delete immutableRewards.manualMode
    }

    // when we receive the old-style object with single prop
    // reward we shall rewrite it in the object-list style
    // eslint-disable-next-line complexity
    // eslint-disable-next-line max-statements
    const normalizeRawData = (reward, rewardType) => {
      const commonData = {
        result: rewardType,
        ...reward
      }

      const manualMode = isValue(rewards.manualMode) ? rewards.manualMode : commonData.manualMode

      if (['item', 'items'].includes(rewardType)) {
        const items = rewards.item || rewards.items
        const newItems = {}

        Object.keys(items).forEach(itemKey => {
          newItems[itemKey] = {
            itemID: itemKey || Object.keys(itemKey)[0],
            ...commonData[itemKey],
            chance: 100, // constant just for Pavel request,
            result: 'item',
            manualMode
          }
        })

        return newItems
      } else if (rewardType === 'money') {
        const newMoneyData = {
          ...commonData,
          result: 'money',
          manualMode
        }

        return newMoneyData
      } else if (rewardType === 'ammo') {
        return {
          ...commonData,
          qnt: '1',
          result: 'ammo',
          manualMode
        }
      } else if (rewardType === 'cost') {
        return {
          ...commonData,
          result: 'cost',
          manualMode
        }
      } else if (rewardType === 'points') {
        return {
          ...commonData,
          result: 'points',
          manualMode
        }
      } else if (rewardType === 'hospTime') {
        return {
          ...commonData,
          result: 'hospTime',
          manualMode
        }
      } else if (rewardType === 'jailTime') {
        return {
          ...commonData,
          result: 'jailTime',
          manualMode
        }
      }

      return null
    }

    const firstElem = immutableRewards[Object.keys(immutableRewards)[0]]
    const toNormalize = !isObject(firstElem) || !Object.prototype.hasOwnProperty.call(firstElem, 'result')

    const createNormalizedRewards = () => {
      const newRewards = {}

      Object.keys(immutableRewards).forEach(rewardKey => {
        newRewards[rewardKey] = normalizeRawData(immutableRewards[rewardKey], rewardKey)
      })

      return newRewards
    }

    const newData = toNormalize ? createNormalizedRewards() : immutableRewards

    return newData
  }

  _handleInputChange = formDataToProcess => {
    const { formData = {} } = this.state
    const { invalidateOutcome: localInvalidateOutcome, outcomes, selectedStatsTab, outcomeID } = this.props

    const newFormData = {
      ...formDataToProcess,
      currentReward: {
        ...formData.currentReward,
        ...formDataToProcess.currentReward
      }
    }

    this.setState(
      () => {
        if (Object.prototype.hasOwnProperty.call(newFormData, 'chance')) {
          const chanceSum = outcomes[selectedStatsTab].reduce(
            (sum, outcome, index) => (outcomeID === index ? sum : sum + Number(outcome.chance)),
            0
          )
          const newChanceSum = chanceSum + newFormData.chance
          const realChance = (newFormData.chance / newChanceSum) * 100

          return {
            realChance,
            formData: {
              ...formData,
              ...newFormData
            }
          }
        }

        return {
          formData: {
            ...formData,
            ...newFormData
          }
        }
      },
      () => {
        localInvalidateOutcome()
        if (
          Object.prototype.hasOwnProperty.call(newFormData, 'minLevel')
          || Object.prototype.hasOwnProperty.call(newFormData, 'rewards')
          || Object.prototype.hasOwnProperty.call(newFormData, 'chance')
        ) {
          this.requestChartUpdate(this._formatformData(formData))
        }
      }
    )
  }

  // eslint-disable-next-line
  _handleStoryChange = curry((index, result) => {
    if (result !== false) {
      const { formData } = this.state
      const newFormData = {
        story: [...formData.story.slice(0, index), result, ...formData.story.slice(index + 1)]
      }

      this._handleInputChange(newFormData)
    }
  })

  _formatformData = formData => {
    const formDataCopy = {
      ...formData,
      typeCondition: formData.typeCondition.list
    }

    const checkStory = () => {
      if (formDataCopy.story && formDataCopy.story.length > 1) {
        for (let i = 1; i < formDataCopy.story.length; i++) {
          if (formDataCopy.story[i].length === 0) {
            formDataCopy.story = [...formDataCopy.story.slice(0, i), ...formDataCopy.story.slice(i + 1)]
          }
        }
      }
    }

    checkStory()

    return formDataCopy
  }

  _handleInputChecked = () => {
    this.setState(prevState => ({
      ...prevState,
      manualMode: !prevState.manualMode
    }))
  }

  _getManualModeCheckbox = () => {
    const {
      formData: { manualMode }
    } = this.state

    return (
      <div className={s.manualModeContainer}>
        <hr className={s.checkboxDivider} />
        <span>Manual mode: </span>
        <input
          type='checkbox'
          className={s.checkboxInput}
          onClick={this._handleInputChecked}
          defaultChecked={manualMode}
        />
      </div>
    )
  }

  _getResultFields = rewardType => {
    const { formData } = this.state
    const { items, nerve } = this.props
    const resultObject = RESULTS.find(item => item.value === rewardType)

    if (!resultObject) {
      return ''
    }

    const getFields = () => {
      const currentFields = resultObject.fields

      const itemDropDown = (i, field) => (
        <Dropdown
          key={i}
          className={s['items-dd']}
          onChange={item => this._handleInputChange({ currentReward: { [field.name]: get(item, 'value') } })}
          options={items}
          placeholder='Type to search...'
          value={formData.currentReward[field.name] || ''}
        />
      )

      const imageDropDown = (i, itemID, finalItemQnt) => (
        <div className={s['items-list']} key={i}>
          <img alt='' src={`/images/items/${itemID}/medium.png`} />
          <span>{finalItemQnt ? ` x ${finalItemQnt}` : ''}</span>
        </div>
      )

      const inputDropDown = (i, field) => {
        const c = cn({
          [s['input']]: true,
          [s['short']]: field.name !== 'reason',
          [s['reason']]: field.name === 'reason'
        })

        return (
          <input
            key={i}
            title={field.isHidden ? 'Read only, default filed!' : field.placeholder}
            data-hidden={field.isHidden}
            readOnly={field.readOnly}
            value={formData.currentReward[field.name] || field.defaultValue || ''}
            onChange={event => this._handleInputChange({ currentReward: { [field.name]: event.target.value } })}
            className={c}
            type='text'
            placeholder={formData.currentReward.qnt || field.placeholder}
          />
        )
      }

      const preProcessedFields = currentFields.map((field, i) => {
        if (['item'].includes(field.type)) {
          return itemDropDown(i, field)
        }

        if (field.type === 'images' && formData?.rewards?.itemID) {
          const { itemID, qnt } = formData.rewards
          const finalItemQnt = qnt

          return imageDropDown(i, itemID, finalItemQnt)
        }

        return inputDropDown(i, field)
      })

      return preProcessedFields
    }

    const fields = getFields()

    let value
    let medianprice

    const getDataToProcess = () => {
      switch (rewardType) {
        case 'money':
          value = (Number(formData.currentReward.min) + Number(formData.currentReward.max)) / 2
          break
        case 'item':
          const { itemID, qnt } = formData.currentReward
          const itemObj = itemID && items.find(({ value }) => itemID === value)

          value = itemObj && itemObj.cost
          medianprice = itemObj && itemObj.medianprice
          qnt && (value *= qnt)
          qnt && (medianprice *= qnt)
          break
        case 'points':
          const { pointCost } = this.props

          value = ((Number(formData.currentReward.min) + Number(formData.currentReward.max)) / 2) * pointCost
          break
        default:
          break
      }
    }

    getDataToProcess()

    const priceHolder = () => (
      <div className={s['value']} key='v'>
        {`Value per nerve: $${toMoney(Math.floor(value / nerve))}${
          medianprice ? ` ($${toMoney(medianprice)} mvpn)` : ''
        }`}
      </div>
    )

    value && fields.push(priceHolder())

    return (
      <div className={s['result-fields']}>
        {fields}
        {this._getManualModeCheckbox()}
        {this._addItemButton()}
      </div>
    )
  }

  _addItemButton = () => {
    return (
      <button type='button' onClick={this._handleAddReward} className={`torn-btn gold ${s.addItemBtn}`}>
        Add
      </button>
    )
  }

  _handleRemoveReward = ({ target }) => {
    const {
      dataset: { result, itemid: itemID }
    } = target.parentNode

    const calcFilteredRewards = rewards => {
      const immutableRewards = {
        ...rewards
      }

      const normalizedResult =
        (['item', 'items'].includes(result) && `${result}s`) || (result === 'costMoney' && 'cost') || result
      const isItemsKey = ['item', 'items'].includes(normalizedResult)
      const entities = isItemsKey ?
        immutableRewards['item'] || immutableRewards['items'] :
        immutableRewards[normalizedResult]

      const isItemsCollection = Object.values(entities).some(entity => {
        return isObject(entity) && !(entities && Object.prototype.hasOwnProperty.call(entities, 'hospTime'))
      })

      const isSingleEntity = Object.keys(entities).length === 1

      if (isItemsCollection && !isSingleEntity) {
        if (immutableRewards['item']?.itemID === itemID) {
          delete immutableRewards['item'][itemID]
        } else {
          delete immutableRewards['items'][itemID]
        }
      } else if (isItemsCollection) {
        if (immutableRewards['item']) {
          delete immutableRewards['item']
        } else {
          delete immutableRewards['items']
        }
      } else {
        delete immutableRewards[normalizedResult]
      }

      return {
        ...immutableRewards
      }
    }

    this.setState(prevState => ({
      ...prevState,
      formData: {
        ...prevState.formData,
        rewards: calcFilteredRewards(prevState.formData.rewards)
      }
    }))
  }

  _handleAddReward = () => {
    const { manualMode } = this.state

    // eslint-disable-next-line complexity
    const calcResultState = ({ currentReward, rewards }) => {
      const reward = currentReward || rewards
      const { result: rewardType } = currentReward

      let rewardPayload = null

      switch (rewardType) {
        case 'item':
          if (!reward.itemID) {
            break
          }

          rewardPayload = {
            items: {
              ...rewards?.items,
              [reward.itemID]: {
                ...reward,
                qnt: reward.qnt || 1,
                manualMode,
                chance: 100 // constant just for Pavel request
              }
            }
          }
          break
        case 'money':
          rewardPayload = {
            money: {
              ...reward,
              min: reward.min || 1,
              max: reward.max || 1,
              dnm: reward.dnm || 0,
              manualMode
            }
          }
          break
        case 'ammo':
          rewardPayload = {
            ammo: {
              ...reward,
              qnt: 1, // this is a particular optimization based on Pavel request,
              manualMode
            }
          }
          break
        case 'points':
          rewardPayload = {
            points: {
              ...reward,
              min: reward.min || 1,
              max: reward.max || 1,
              manualMode
            }
          }
          break
        case 'costMoney':
          rewardPayload = {
            cost: {
              ...reward,
              max: reward.max || 1,
              manualMode
            }
          }
          break
        case 'jailTime':
          rewardPayload = {
            jailTime: {
              ...reward,
              max: reward.min,
              manualMode
            }
          }
          break
        case 'hospTime':
          rewardPayload = {
            hospTime: {
              result: reward.result,
              reason: reward.reason,
              looseLife: reward.lifeMin,
              hospTime: reward.hospTimeMin,
              manualMode
            }
          }
          break
        default:
          break
      }

      return rewardPayload
    }

    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        rewards: {
          ...prevState.formData.rewards,
          ...calcResultState(prevState.formData)
        },
        currentReward: {
          result: prevState.formData.currentReward.result
          // manualMode: false
        }
      }
    }))
  }

  _stageUpgrade = () => {
    const { submit: localSubmit, stageLabel, crimeID, outcomeID, selectedStatsTab, stage } = this.props
    const {
      formData: { triggers, typeCondition, cooldown, minLevel, maxLevel }
    } = this.state

    let newStage

    if (stageLabel === 'alpha') {
      newStage = STAGE_IDS['beta']
    } else if (stageLabel === 'beta') {
      newStage = STAGE_IDS['release']
    }

    if (stageLabel === 'released') {
      return
    }

    isValue(newStage)
      && localSubmit('editOutcome', {
        crimeID,
        outcomeID,
        outcomeType: selectedStatsTab,
        stage: newStage,
        triggers,
        cooldown,
        typeCondition: typeCondition.list,
        minLevel,
        maxLevel
      })

    this.setState({
      [REQUEST_ADAPTER_KEYS['stageRequest']]: true
    })
  }

  _submit = step => {
    const { formData } = this.state
    const { hideEditor: localHideEditor, submit: localSubmit } = this.props

    const _step = step
    const isUnreleased = _step === 'unrelease'

    const formDataNormalized = () => {
      const data = {
        ...this._formatformData(formData)
      }
      const { rewards } = data

      delete data.currentReward

      const requestPayload = {
        ...data,
        ...(isUnreleased ?
          {
            stage: STAGE_IDS['beta']
          } :
          {}),
        rewards: {
          ...rewards,
          ...(rewards.hospTime ?
            {
              hospTime: {
                reason: rewards.hospTime?.reason,
                manualMode: rewards.hospTime?.manualMode,
                hospTime: {
                  min: rewards.hospTime?.hospTime || 0,
                  max: rewards.hospTime?.hospTime || 0
                },
                looseLife: {
                  min: rewards.hospTime?.looseLife || 0,
                  max: rewards.hospTime?.looseLife || 0
                }
              }
            } :
            {})
        }
      }

      delete requestPayload.rewards.looseLife

      return requestPayload
    }

    this.setState({
      [REQUEST_ADAPTER_KEYS[_step]]: true
    })

    const submitType = isUnreleased ? 'editOutcome' : _step

    localSubmit(submitType, formDataNormalized())
    if (_step === 'saveOutcome') {
      localHideEditor()
    }
  }

  _addStory = event => {
    event.preventDefault()
    const { formData } = this.state

    this.setState({
      formData: {
        ...formData,
        story: [...formData.story, '']
      }
    })
  }

  _classNamesHolder = () => {
    const { loading, outcome } = this.props
    const { mediaType = '' } = outcome || {}

    const wrapClass = cn({
      [s['wrap']]: true,
      [s['loading']]: loading
    })

    const outcomeWrap = cn({
      [s.outcomeWrap]: true,
      [s[mediaType]]: mediaType
    })

    const outcomeResize = button =>
      cn({
        [s.clicked]: mediaType === button,
        [s.disabled]: !outcome
      })

    return {
      wrapClass,
      outcomeWrap,
      outcomeResize
    }
  }

  _getOutcome = () => {
    const { counter } = this.state
    const { outcome } = this.props
    const { outcomeWrap } = this._classNamesHolder()

    if (!outcome) {
      return <div className={`${s.outcomeWrap} ${s.oucomePlaceholder}`}>{NO_OUTCOME_PLACEHOLDER}</div>
    }

    const outcomeConfig = {
      counter,
      outcome,
      state: outcome && outcome.result,
      isCurrentRow: true,
      isRowNotInProgress: true,
      showOutcome: true,
      mediaType: outcome.mediaType
    }

    // rolling up animation on outcome regeneration won't work
    // because we always receive the same Outcome data!
    return (
      <div className={outcomeWrap}>
        <Outcome {...outcomeConfig} />
      </div>
    )
  }

  _generateOutcome = () => {
    const { requestOutcome: localRequestOutcome, ID, crimeID } = this.props

    if (!requestOutcome) return

    this.setState(prevState => ({
      counter: prevState.counter + 1,
      [REQUEST_ADAPTER_KEYS['outcomeRequest']]: true
    }))

    localRequestOutcome(crimeID, ID)
  }

  _renderLabels = () => {
    const { formData } = this.state

    const labelsToRender = SETTINGS_LABELS.map(label => {
      if (!(formData.typeConditions && Object.keys(formData.typeConditions).length !== 0) && label === 'Condition') {
        // TODO: hack for Condition label appear. Need to refactor this one.
        return null
      }

      const customLabelClass = label === 'Result' ? 'resultLabel' : ''

      return (
        <div key={label} className={`${s.thr} ${s[customLabelClass]}`}>
          {label}:
        </div>
      )
    })

    return <div className={s['th']}>{labelsToRender}</div>
  }

  _handleConditionChange = item => {
    const {
      formData: { typeCondition }
    } = this.state

    const config = {
      typeCondition: {
        lastUsed: null,
        currentType: null,
        list: typeCondition.list
      }
    }

    if (item) {
      config.typeCondition.currentType = item.value

      if (typeCondition) {
        config.typeCondition.list = typeCondition.list
      }
    }

    this._handleInputChange(config)
  }

  _handleConditionEnhancerChange = item => {
    const {
      formData: { typeCondition }
    } = this.state

    const isItemNotInState = !typeCondition.list.find(listItem => listItem && listItem.label === item.label)
    const config = {
      typeCondition: {
        ...typeCondition,
        lastUsed: item,
        list: [...typeCondition.list]
      }
    }

    if (item && isItemNotInState) {
      config.typeCondition.list.push(item)
    }

    this._handleInputChange(config)
  }

  _handleConditionRemove = e => {
    const itemToRemove = e.target.parentNode

    const findItemToRemove = listItem => listItem && listItem.label !== itemToRemove.dataset.label

    this.setState(prevState => ({
      ...prevState,
      formData: {
        ...prevState.formData,
        typeCondition: {
          ...prevState.formData['typeCondition'],
          list: prevState.formData['typeCondition'].list.filter(findItemToRemove)
        }
      }
    }))
  }

  _getSelectedList = (listToRender = [], onClick = undefined, styles = {}, withEmpty = false) => {
    if (!listToRender || listToRender.length === 0) {
      if (withEmpty) {
        return (
          <div className={s.selectedList} style={styles}>
            <span className={s.selectedPlaceholder}>No reward selected yet.</span>
          </div>
        )
      }

      return null
    }

    const itemsList = listToRender.map(listItem => {
      if (!listItem) {
        return null
      }

      return (
        <div
          key={listItem.label}
          className={`${s.selectedItem} ${listItem.manualMode ? s.manual : ''}`}
          data-label={listItem.label}
          data-result={listItem.result}
          data-itemID={listItem.itemID}
        >
          {listItem.label && <span className={s.itemLabel}>{listItem.label}</span>}
          {listItem.itemID && <img alt='' src={`/images/items/${listItem.itemID}/medium.png`} />}
          {listItem.qnt && listItem.result !== 'ammo' && <span className={s.itemLabel}>x {listItem.qnt}</span>}
          {listItem.result === 'ammo' && <span className={s.itemLabel}>Ammo x1</span>}
          {listItem.result === 'money' && (
            <span className={s.itemLabel}>
              Money: {listItem.min} - {listItem.max} | {listItem.dnm || 0}
            </span>
          )}
          {listItem.result === 'points' && (
            <span className={s.itemLabel}>
              Points: {listItem.min} - {listItem.max}
            </span>
          )}
          {listItem.result === 'costMoney' && <span className={s.itemLabel}>Cost: {listItem.max}</span>}
          {listItem.result === 'jailTime' && (
            <span className={s.itemLabel}>
              Jail: time {listItem.min} - {listItem.max} | reason - {listItem.reason}
            </span>
          )}
          {listItem.result === 'hospTime' && (
            <span className={s.itemLabel}>
              Hosp: life{' '}
              {isValue(listItem.looseLife?.min) ? listItem.looseLife?.min : JSON.stringify(listItem.looseLife)} | time{' '}
              {isValue(listItem.hospTime?.min) ? listItem.hospTime?.min : JSON.stringify(listItem.hospTime)} | reason -{' '}
              {listItem.reason}
            </span>
          )}
          <button className={s.selectedItemButton} type='button' onClick={onClick}>
            x
          </button>
        </div>
      )
    })

    return (
      <div className={s.selectedList} style={styles}>
        {itemsList}
      </div>
    )
  }

  _getConditionFields = () => {
    const {
      formData: { typeCondition, typeConditions }
    } = this.state

    if (!typeConditions || (typeConditions && Object.keys(typeConditions).length === 0)) {
      return null
    }

    return (
      <div className={s.conditionsEnhencerWrap}>
        <Dropdown
          className={`${s['items-dd']} ${s.conditionsEnhencerInputWidth}`}
          onChange={this._handleConditionEnhancerChange}
          options={typeConditions[typeCondition && typeCondition.currentType]}
          placeholder='Add Condition'
          value={typeCondition.lastUsed || ''}
        />
        {this._getSelectedList(typeCondition.list, this._handleConditionRemove)}
      </div>
    )
  }

  _normalizeName = name => name.substr(0, 1).toUpperCase() + name.substr(1, name.length - 1)

  _isRawRewardObject = rewardsList =>
    !Object.keys(rewardsList).some(key => LIST_KEYS.some(keyInner => key === keyInner))

  _getFlattenRewards = (rewardsList = {}) => {
    if (isEmpty(rewardsList)) {
      return null
    }

    const rewards = Object.values(rewardsList).map(reward => {
      const isItems = Object.keys(reward).some(innerRewardKey => isObject(reward[innerRewardKey]))

      let rewardList = null

      // this is a hack because of unoptimized hospTime object structure!!
      if (Object.prototype.hasOwnProperty.call(reward, 'hospTime')) {
        rewardList = {
          result: reward.result || 'hospTime',
          reason: reward.reason,
          hospTime: reward.hospTime?.min || reward.hospTime || 0,
          looseLife: reward.looseLife?.min || reward.looseLife || 0,
          manualMode: reward.manualMode || false
        }
      } else if (isItems) {
        rewardList = Object.values(reward)
      } else {
        rewardList = [reward]
      }

      return rewardList
    })

    const flattenRewards = rewards.flat()

    return flattenRewards
  }

  _getCurrentActionStatus = () => {
    const { isSaveRequest, isDeleteRequest, isUnreleaseRequest, isStageRequest, isOutcomeRequest } = this.state
    const { loading } = this.props

    const isSaveAwait = loading && isSaveRequest
    const isDeleteAwait = loading && isDeleteRequest
    const isUnreleaseAwait = loading && isUnreleaseRequest
    const isStageAwait = loading && isStageRequest
    const isOutcomeAwait = loading && isOutcomeRequest

    return {
      isSaveAwait,
      isDeleteAwait,
      isUnreleaseAwait,
      isStageAwait,
      isOutcomeAwait
    }
  }

  _clearCurrentActionStatus = () => {
    this.setState({
      isSaveRequest: false,
      isDeleteRequest: false,
      isUnreleaseRequest: false,
      isStageRequest: false,
      isOutcomeRequest: false
    })
  }

  _renderSpinner = () => {
    return (
      <span className={s.fetchActionSpinner}>
        <Loading dotsCount={3} />
      </span>
    )
  }

  _renderDropDowns = () => {
    const { drops = [], items } = this.props
    const { chance = {}, specialTypes = [] } = drops
    const { formData, realChance } = this.state
    const rewardType = get(formData, 'currentReward.result')
    const realChanceNormalize = (realChance && realChance.toString()) || ''
    const realChanceNumber = Number(realChanceNormalize.substr(0, realChanceNormalize.length - 6)).toFixed(2)
    const conditionsList = Object.keys(formData.typeConditions)
    const conditionFields = conditionsList.map(key => ({ value: key, label: this._normalizeName(key) }))

    return (
      <Fragment>
        <div className={s['tbr']}>
          <Dropdown
            clearable={false}
            className={s['first-line']}
            placeholder={chance[0].label || 'Rarity'}
            options={chance}
            value={formData['chance']}
            onChange={item => this._handleInputChange({ chance: item.value })}
          />
          <span>{`${realChanceNumber} %`}</span>
        </div>
        <div className={s['tbr']}>
          <Dropdown
            clearable={false}
            className={s['first-line']}
            placeholder={specialTypes[formData['special']].label || 'Common'}
            options={specialTypes}
            value={specialTypes[formData['special']]}
            onChange={item => this._handleInputChange({ special: item.value })}
          />
        </div>
        <div className={s['tbr']} style={{ flexWrap: 'wrap', height: 'auto', marginTop: '2px' }}>
          <Dropdown
            clearable={false}
            className={s['first-line']}
            placeholder='Result'
            options={this.filteredResults}
            value={rewardType}
            onChange={item => this._handleInputChange({ currentReward: { result: item.value } })}
          />

          {this._getResultFields(rewardType)}
          {this._getSelectedList(
            this._getFlattenRewards(formData.rewards),
            this._handleRemoveReward,
            { width: '782px', height: '82px', marginBottom: '7px', marginTop: '7px' },
            true
          )}
        </div>
        <div className={s['tbr']}>
          <Dropdown
            className={s['first-line']}
            placeholder='None'
            options={drops.triggers}
            value={formData['triggers']}
            onChange={item => this._handleInputChange({ triggers: item ? item.value : null })}
          />
        </div>
        <div className={s['tbr']}>
          <Dropdown
            className={s['first-line']}
            placeholder='None'
            options={COOLDOWN_VALUES}
            value={formData['cooldown']}
            onChange={item => this._handleInputChange({ cooldown: item ? item.value : null })}
          />
        </div>
        <div className={s['tbr']}>
          <Dropdown
            className={s['items-dd']}
            onChange={item => this._handleInputChange({ itemsNeeded: get(item, 'value') })}
            options={items}
            placeholder='Search item'
            value={formData.itemsNeeded}
          />
        </div>
        <div className={`${s['tbr']} ${s.conditionsWrap}`}>
          {formData.typeConditions && Object.keys(formData.typeConditions).length !== 0 && (
            <Dropdown
              className={s['items-dd']}
              onChange={this._handleConditionChange}
              options={conditionFields}
              placeholder='Select Condition'
              value={formData.typeCondition && formData.typeCondition.currentType}
            />
          )}
          {this._getConditionFields()}
        </div>
      </Fragment>
    )
  }

  _resizeOutcome = e => {
    const { changeOutcomeLayout: localChangeOutcomeLayout, outcome } = this.props
    const {
      dataset: { mediaquery }
    } = e.target

    if (!outcome || !localChangeOutcomeLayout || !mediaquery) return

    localChangeOutcomeLayout(mediaquery)
  }

  _renderOutcome = () => {
    const { isOutcomeRequest } = this.state
    const { wrapClass, outcomeResize } = this._classNamesHolder()

    const resizeButtons = MEDIAQUERY_LAYOUTS.map(button => (
      <div
        tabIndex={0}
        role='button'
        onKeyDown={undefined}
        key={button}
        data-mediaquery={button}
        className={`${s.resizeButton} ${s[button]} ${outcomeResize(button)}`}
        onClick={this._resizeOutcome}
      />
    ))

    return (
      <div className={wrapClass}>
        <div className={s['th']}>
          <div className={s['thr']}>Outcome Layout:</div>
        </div>
        <div className={s['tb']}>
          <div className={`${s['tbr']} ${s.fullHeight} ${s.outcomeTest}`}>
            {this._getOutcome()}
            <div className={`${s.actionBtns} ${s.btsColumn}`}>
              <div className={s.actionOutcomeBtn}>
                <button
                  type='button'
                  className={`${s['btn']} ${s.generateBtn} torn-btn gold ${s.generate}`}
                  onClick={this._generateOutcome}
                >
                  {!isOutcomeRequest ? 'Generate' : this._renderSpinner()}
                </button>
              </div>
              <div className={s.resizeBtns}>{resizeButtons}</div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  _renderButtons = () => {
    const { creatorID, stageLabel, otherstatus } = this.props
    const saveAction = creatorID ? 'editOutcome' : 'saveOutcome'
    const { wrapClass } = this._classNamesHolder()
    const { isSaveAwait, isDeleteAwait, isUnreleaseAwait, isStageAwait } = this._getCurrentActionStatus()

    return (
      <div className={`${wrapClass} ${s.flexEnd}`}>
        <div className={`${s['tbr']} ${s.buttonsEnd}`}>
          <div className={s.actionBtns}>
            <button
              type='button'
              onClick={() => this._submit(saveAction)}
              className={`${s['btn']} torn-btn gold ${s.save}`}
            >
              {!isSaveAwait ? 'Save' : this._renderSpinner()}
            </button>
            {stageLabel !== 'released' ? (
              <button
                type='button'
                onClick={() => this._submit('deleteOutcome')}
                className={`${s['btn']} torn-btn gold ${s.delete}`}
              >
                {!isDeleteAwait ? 'Delete' : this._renderSpinner()}
              </button>
            ) : (
              otherstatus === 4 && (
                <button
                  type='button'
                  onClick={() => this._submit('unrelease')}
                  className={`${s['btn']} torn-btn gold ${s.unrelease}`}
                >
                  {!isUnreleaseAwait ? 'Unrelease' : this._renderSpinner()}
                </button>
              )
            )}
            {STAGE_ACTIONS[stageLabel] && !(stageLabel === 'beta' && otherstatus !== 4) && (
              <button
                type='button'
                onClick={this._stageUpgrade}
                className={`${s['btn']} torn-btn gold ${s[STAGE_ACTIONS[stageLabel]]}`}
              >
                {!isStageAwait ? STAGE_ACTIONS[stageLabel] : this._renderSpinner()}
              </button>
            )}
          </div>
        </div>
      </div>
    )
  }

  _renderSkillLevel = () => {
    const { stageLabel } = this.props
    const { formData } = this.state

    return (
      <div className={s['tbr']}>
        <input
          value={formData['minLevel'] === 0 ? 1 : formData['minLevel']}
          onChange={event => this._handleInputChange({ minLevel: event.target.value })}
          className={`${s['input']} ${s['short']}`}
          type='text'
        />
        <span style={{ marginRight: '10px' }}>—</span>
        <input
          value={formData['maxLevel']}
          onChange={event => this._handleInputChange({ maxLevel: event.target.value })}
          className={`${s['input']} ${s['short']}`}
          type='text'
        />
        {Number(formData['maxLevel']) < Number(formData['minLevel']) ? (
          <span className={s.minmaxLevelError}>WARNING! maxLevel cannot be lower than minLevel!</span>
        ) : (
          ''
        )}
        {Number(formData['maxLevel']) > 100 ? (
          <span className={s.minmaxLevelError}>WARNING! maxLevel cannot be higher than 100!</span>
        ) : (
          ''
        )}
        <div className={g['fb-right']}>
          <div className={`${g['stage']} ${g[stageLabel]}`}>
            <i />
            <em />
          </div>
        </div>
      </div>
    )
  }

  _renderName = () => {
    const { formData } = this.state

    return (
      <div className={s['tbr']}>
        <input
          value={formData['name']}
          onChange={event => this._handleInputChange({ name: event.target.value })}
          className={`${s['input']} ${s['first-line']}`}
          type='text'
        />
      </div>
    )
  }

  _renderStory = () => {
    const { formData } = this.state

    return (
      <Fragment>
        <div className={`${s['tbr']} ${s['story']}`}>
          {formData['story'].map((story, i) => (
            <LimitedInput
              width={778}
              key={i}
              value={story}
              onChange={this._handleStoryChange(i)}
              className={cn(s.input, s.long)}
            />
          ))}
        </div>
        <div className={s['tbr']}>
          <div className={s.addLink}>
            <button
              type='button'
              style={{
                border: 'none',
                margin: 0,
                padding: 0,
                font: 'inherit',
                cursor: 'pointer',
                background: 'transparent',
                width: 'auto',
                height: '20px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-start'
              }}
              href='#'
              onClick={this._addStory}
            >
              <i style={{ marginTop: '-2px' }}>+</i>Add another line
            </button>
          </div>
        </div>
      </Fragment>
    )
  }

  _renderSettings = () => {
    return (
      <div className={s['tb']}>
        {this._renderName()}
        {this._renderSkillLevel()}
        {this._renderDropDowns()}
        {this._renderStory()}
      </div>
    )
  }

  _renderMainBody = () => {
    const { wrapClass } = this._classNamesHolder()

    return (
      <div className={wrapClass}>
        {this._renderLabels()}
        {this._renderSettings()}
      </div>
    )
  }

  render() {
    return (
      <Fragment>
        {this._renderMainBody()}
        {this._renderOutcome()}
        {this._renderButtons()}
      </Fragment>
    )
  }
}

OutcomeEditor.propTypes = {
  ID: PropTypes.number,
  chance: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  creatorID: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  crimeID: PropTypes.number,
  desc: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  drops: PropTypes.object,
  hideEditor: PropTypes.func,
  invalidateOutcome: PropTypes.func,
  items: PropTypes.array,
  // ammo: PropTypes.array,
  loading: PropTypes.bool,
  typeConditions: PropTypes.object,
  typeCondition: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  minCrimeLevel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  nerve: PropTypes.number,
  stage: PropTypes.string,
  otherstatus: PropTypes.number,
  outcomeID: PropTypes.number,
  manualMode: PropTypes.bool,
  outcome: PropTypes.object,
  outcomes: PropTypes.object,
  pointCost: PropTypes.number,
  realChance: PropTypes.number,
  requestChartUpdate: PropTypes.func,
  rewards: PropTypes.string,
  special: PropTypes.number,
  selectedStatsTab: PropTypes.string,
  stageLabel: PropTypes.string,
  submit: PropTypes.func,
  triggers: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  requestOutcome: PropTypes.func,
  changeOutcomeLayout: PropTypes.func
}

OutcomeEditor.defaultProps = {
  ID: null,
  chance: '',
  creatorID: '',
  crimeID: null,
  desc: [],
  drops: {},
  items: [],
  ammo: [],
  stage: null,
  loading: false,
  typeConditions: {},
  typeCondition: [],
  minCrimeLevel: null,
  nerve: null,
  otherstatus: null,
  outcomeID: null,
  outcome: {},
  outcomes: {},
  pointCost: null,
  realChance: null,
  manualMode: false,
  rewards: '',
  special: 0,
  selectedStatsTab: '',
  stageLabel: '',
  triggers: null,
  submit: () => {},
  hideEditor: () => {},
  requestOutcome: () => {},
  invalidateOutcome: () => {},
  requestChartUpdate: () => {},
  changeOutcomeLayout: () => {}
}

const mapStateToProps = state => ({
  drops: {
    chance: state.app.chance,
    triggers: state.app.triggers,
    specialTypes: state.app.specialTypes
  },
  items: state.app.items,
  ammo: state.app.ammo,
  nerve: state.app.currentCrime.nerve,
  otherstatus: Number(state.app.otherstatus),
  outcome: state.app.currentCrime.outcome,
  outcomes: state.app.currentCrime.outcomes,
  pointCost: Number(state.app.pointCost),
  selectedStatsTab: state.app.selectedStatsTab
})

const mapDispatchToProps = {
  hideEditor,
  invalidateOutcome,
  requestChartUpdate,
  submit,
  requestOutcome,
  changeOutcomeLayout
}

export default connect(mapStateToProps, mapDispatchToProps)(OutcomeEditor)
