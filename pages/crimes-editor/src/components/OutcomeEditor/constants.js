export const STAGE_IDS = {
  alpha: 3,
  beta: 2,
  release: 1
}

export const STAGE_ACTIONS = {
  alpha: 'confirm',
  beta: 'release',
  released: 'released'
}

export const LIST_KEYS = ['ammo', 'points', 'cost', 'money', 'item']

export const REQUEST_ADAPTER_KEYS = {
  saveOutcome: 'isSaveRequest',
  editOutcome: 'isSaveRequest',
  deleteOutcome: 'isDeleteRequest',
  unrelease: 'isUnreleaseRequest',
  stageRequest: 'isStageRequest',
  outcomeRequest: 'isOutcomeRequest'
}

export const COOLDOWN_VALUES = [
  {
    value: 0,
    label: 'None'
  },
  {
    value: 3600,
    label: 'Hourly'
  },
  {
    value: 86400,
    label: 'Daily'
  },
  {
    value: 604800,
    label: 'Weekly'
  },
  {
    value: 2592000,
    label: 'Monthly'
  },
  {
    value: 31536000,
    label: 'Yearly'
  },
  {
    value: -1,
    label: 'Permanent'
  }
]
export const RESULTS = [
  {
    value: 'money',
    label: 'Receive money',
    avaliable: ['success'],
    fields: [
      { name: 'min', placeholder: 'min' },
      { name: 'max', placeholder: 'max' },
      { name: 'dnm', placeholder: 'denominator' }
    ]
  },
  {
    value: 'item',
    label: 'Receive Item',
    avaliable: ['success'],
    fields: [
      { type: 'item', name: 'itemID', placeholder: 'Item ID' },
      { name: 'qnt', placeholder: 'Quantity' },
      { type: 'images', isHidden: true }
    ]
  },
  {
    value: 'ammo',
    label: 'Receive Ammo',
    avaliable: ['success'],
    fields: [
      { type: 'ammo', name: 'ammoID', placeholder: 'Ammo ID', isHidden: true },
      { name: 'qnt', placeholder: '1', defaultValue: '1', readOnly: true }
    ]
  },
  {
    value: 'points',
    label: 'Receive Points',
    avaliable: ['success'],
    fields: [{ name: 'min', placeholder: 'min' }, { name: 'max', placeholder: 'max' }]
  },
  {
    value: 'jailTime',
    label: 'Receive jail time',
    avaliable: ['jail'],
    fields: [
      { name: 'min', placeholder: 'Jail Time' },
      // { name: 'max', placeholder: 'Jail Time Max (minutes)' },
      { name: 'reason', placeholder: 'Reason' }
    ]
  },
  {
    value: 'hospTime',
    label: 'Lose life / hospital time',
    avaliable: ['jail'],
    fields: [
      { name: 'lifeMin', placeholder: 'Life Percent' },
      { name: 'hospTimeMin', placeholder: 'Hosp Time' },
      { name: 'reason', placeholder: 'Reason' }
    ]
  },
  {
    value: 'costMoney',
    label: 'Cost money',
    avaliable: ['success', 'jail'],
    fields: [{ name: 'max', placeholder: 'Cost' }]
  }
]

export const NO_OUTCOME_PLACEHOLDER = 'No Outcome currently. Please, click the "Generate" button to produce one here.'
export const SETTINGS_LABELS = ['Name', 'Skill Level', 'Rarity', 'Special', 'Result', 'Trigger', 'Cooldown', 'Required Item', 'Condition', 'Story']
export const MEDIAQUERY_LAYOUTS = ['desktop', 'tablet', 'mobile']
