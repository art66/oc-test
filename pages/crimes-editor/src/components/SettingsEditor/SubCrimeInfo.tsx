import React, { useMemo } from 'react'
import { connect } from 'react-redux'
import isValue from '@torn/shared/utils/isValue'

import styles from './index.cssmodule.scss'

export interface IProps {
  crimes: any[]
  currentCrime: number
  currentSubCrime: number
}

const USER_AVERAGE_DAILY_NERVE = 192
const USER_MAXIMUM_DAILY_NERVE = 507

const SubCrimeInfo = (props: IProps) => {
  const getCurrentCrimeData = useMemo(() => crime => {
    return ((crime.typeID === props.currentCrime) && (crime.ID === props.currentSubCrime))
  }, [props.currentCrime, props.currentSubCrime])

  if (!isValue(props.currentSubCrime)) {
    return null
  }

  const currentSubCrimeData = props.crimes.find(getCurrentCrimeData)
  const { skillGainMultiplier, expRequired, successChance, jailChance } = currentSubCrimeData || {}

  return (
    <div className={styles.infoContainer}>
      <span>
        When the user reaches
        <strong>
          {Number(expRequired) / 4}
        </strong> crime experience, their success rate will begin
        increasing from the minimum success chance
        <strong>
          {successChance.min}%
        </strong> up to the base success chance{' '}
        <strong>
          {successChance.lowmax}%
        </strong> when they reach
        <strong>
          {expRequired}
        </strong> crime experience. Once the base
        success chance has been achieved, this can then increase further up to the maximum success chance{' '}
        <strong>{successChance.highmax}%</strong> as their crime skill level scales from 1 to 100.
      </span>
      <br />
      <br />
      <span>
        Whenever the user is set to receive a failure, there is a
        <strong>
          {jailChance.min}%
        </strong> chance that this failure
        will be a critical failure.
      </span>
      <br />
      <br />
      <span>
        For every successful outcome, the user will receive an average of
        <strong>
          {skillGainMultiplier}
        </strong> x nerveUsed crime skill EXP.
        As 1,000,000 is required for level 100, this will take between
        <strong> {Math.round((1000000 / Number(skillGainMultiplier)) / USER_AVERAGE_DAILY_NERVE) || 0}</strong>
        -
        <strong>{Math.round((1000000 / Number(skillGainMultiplier)) / USER_MAXIMUM_DAILY_NERVE) || 0} </strong>
        days to achieve.
      </span>
    </div>
  )
}

const mapStateToProps = ({ app: state }) => ({
  crimes: state.crimes,
  currentCrime: state.dropDownState.crime,
  currentSubCrime: state.dropDownState.subcrime,
  skillGainMultiplier: state.skillGainMultiplier,
  expRequired: state.expRequired,
  min: state.min,
  lowmax: state.lowmax,
  highmax: state.highmax,
  jail: state.jail
})

export default connect(mapStateToProps, null)(SubCrimeInfo)
