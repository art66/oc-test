import React from 'react'
import { connect } from 'react-redux'

import AnimationLoad from '@torn/shared/components/AnimationLoad'
import keyDownHandler from '@torn/shared/utils/keyDownHandler'

import { cancelEditCrime, changeCrimeSettings, saveCrimeSettings } from '../../modules'

import styles from './index.cssmodule.scss'
import SubCrimeInfo from './SubCrimeInfo'

export interface IProps {
  loading: boolean
  crimesData: any[]
  crimeID: number | string
  saveCrimeSettingsRun: () => void
  cancelEditCrimeRun: () => void
  changeCrimeSettingsRun: (value: string, key: string, rowID: string) => void
}

const TITLES = [null, 'Skill multiplier', 'Optimal EXP', 'Min chance', 'Base chance', 'Max chance', 'Critical Chance']

class CrimeSettingEditor extends React.PureComponent<IProps, { isSaveRequest: boolean }> {
  constructor(props) {
    super(props)

    this.state = {
      isSaveRequest: false
    }
  }

  componentDidUpdate(prevProps: IProps) {
    const { loading } = this.props

    const isEmitRequestFinish = (prevProps.loading !== loading) && !loading

    isEmitRequestFinish && this._clearCurrentActionStatus()
  }

  _handleKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'esc',
          onEvent: this._handleClickCancel
        },
        {
          type: 'enter',
          onEvent: this._handleClickSave
        }
      ]
    })
  }

  _handleChange = ({ target }) => {
    const { value, dataset } = target
    const { dataset: { rowId } } = target.parentNode.parentNode
    const { changeCrimeSettingsRun } = this.props

    const commaIndex = value.indexOf('.')
    const normalizedValue = value.replace(/[^0-9.]/gi, '')

    if ((commaIndex >= 0) && (normalizedValue.substr(commaIndex + 1).length > 2)) {
      return
    }

    changeCrimeSettingsRun(normalizedValue, dataset.id, rowId)
  }

  _handleClickSave = () => {
    const { saveCrimeSettingsRun, loading } = this.props

    if (loading) {
      return
    }

    this.setState({
      isSaveRequest: true
    })

    saveCrimeSettingsRun()
  }

  _handleClickCancel = () => {
    const { cancelEditCrimeRun } = this.props

    cancelEditCrimeRun()
  }

  _clearCurrentActionStatus = () => {
    this.setState({
      isSaveRequest: false
    })
  }

  _keysNormalizer = (row: any) => {
    const {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      _id,
      title,
      skillGainMultiplier,
      expRequired,
      jailChance,
      successChance
    } = row

    return {
      rowID: _id.$oid,
      subCrimeLabel: title,
      skillMultiplier: skillGainMultiplier,
      optimalExp: expRequired,
      minChance: successChance.min,
      baseChance: successChance.lowmax,
      maxChance: successChance.highmax,
      criticalChance: jailChance.min
    }
  }

  _getCurrentCrimeSubcrimes = () => {
    const { crimesData, crimeID } = this.props

    const parse = crimesData.filter(crime => crime.typeID === crimeID)

    return parse
  }

  _createCells = (valuesArr) => (value: string | number, index) => {
    const cellKey = Object.keys(valuesArr)[index]

    return this._renderCell(cellKey, value)
  }

  _renderSubCrimeInfo = () => {
    return <SubCrimeInfo />
  }

  _renderColumnTitles = () => {
    const titlesToRender = TITLES.map(title => {
      return (
        <div key={title} className={styles.titleContainer}>
          <span className={styles.title}>{title || ''}</span>
        </div>
      )
    })

    return titlesToRender
  }

  _renderCell = (key: string, value: number | string) => {
    return (
      <div className={styles.cellWrap}>
        <input
          data-id={key}
          type='text'
          className={styles.inputCell}
          value={value}
        />
        {!['optimalExp', 'skillMultiplier'].includes(key) ? '%' : ''}
      </div>
    )
  }

  _renderRows = () => {
    const rowsDataToRender = this._getCurrentCrimeSubcrimes()

    const rowsToRender = rowsDataToRender.map(row => {
      const { rowID, subCrimeLabel, ...restValues } = this._keysNormalizer(row)

      return (
        <div key={subCrimeLabel} data-row-id={rowID} className={styles.rowContainer}>
          <span className={styles.rowLabel} dangerouslySetInnerHTML={{ __html: subCrimeLabel }} />
          {Object.values(restValues).map(this._createCells(restValues))}
        </div>
      )
    })

    return rowsToRender
  }

  _renderActionButtons = () => {
    const { isSaveRequest } = this.state

    return (
      <div className={styles.buttonsWrap}>
        <button
          type='button'
          className={`${styles.btn} torn-btn gold`}
          onClick={this._handleClickSave}
          disabled={isSaveRequest}
        >
          {!isSaveRequest ? 'SAVE' : <AnimationLoad dotsCount={3} isAdaptive={true} />}
        </button>
        <button
          type='button'
          className={`${styles.btn} torn-btn gold`}
          onClick={this._handleClickCancel}
        >
          CANCEL
        </button>
      </div>
    )
  }

  render() {
    return (
      <div
        role='button'
        aria-label='Settings Panel'
        tabIndex={0}
        className={styles.settingsWrap}
        onChange={this._handleChange}
        onKeyDown={this._handleKeydown}
      >
        {this._renderSubCrimeInfo()}
        <div className={styles.columnsWrap}>
          {this._renderColumnTitles()}
        </div>
        <div className={styles.rowsWrap}>
          {this._renderRows()}
        </div>
        {this._renderActionButtons()}
      </div>
    )
  }
}

const mapStateToProps = ({ app }) => ({
  loading: app.loading,
  crimesData: app.crimes,
  crimeID: app.dropDownState.crime
})

const mapDispatchToProps = {
  cancelEditCrimeRun: cancelEditCrime,
  saveCrimeSettingsRun: saveCrimeSettings,
  changeCrimeSettingsRun: changeCrimeSettings
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrimeSettingEditor)
