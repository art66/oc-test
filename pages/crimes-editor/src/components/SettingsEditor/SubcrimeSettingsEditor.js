// Replaced by CrimesSettingsEditor component as a personal request from Joe

// import PropTypes from 'prop-types'
// import React, { Component, Fragment } from 'react'
// import { connect } from 'react-redux'
// import classnames from 'classnames'
// import { cancelEditSubcrime, saveSubcrimeSettings } from '../../modules'

// import formalizeFormData from './utils/formalizeFormData'

// import s from './styles.cssmodule.scss'

// class SubcrimeSettingsEditor extends Component {
//   constructor(props) {
//     super(props)

//     this.state = {
//       updateType: 'props',
//       formData: {
//         ...props.formData
//       }
//     }
//   }

//   static getDerivedStateFromProps(nextProps, prevState) {
//     if (!prevState.updateType || prevState.updateType === 'props') {
//       return {
//         formData: nextProps.formData
//       }
//     }

//     if (prevState.updateType === 'state') {
//       return {
//         updateType: ''
//       }
//     }

//     return null
//   }

//   shouldComponentUpdate() {
//     return true
//   }

//   _submit = () => {
//     const { saveSubcrimeSettingsRun, crimeID } = this.props
//     const { formData } = this.state

//     saveSubcrimeSettingsRun('saveSubcrime', { ...formData, crimeID })
//   }

//   _toggleAdvanced = () => {
//     const { advanced } = this.state

//     this.setState({
//       updateType: 'state',
//       advanced: !advanced
//     })
//   }

//   // deprecated method since React v.16.5!
//   // componentWillReceiveProps(props) {
//   //   this.setState({
//   //     formData: props.formData
//   //   })
//   // }

//   _handleChange = event => {
//     const { name, value } = event.target
//     const { formData } = this.state

//     this.setState({
//       updateType: 'state',
//       formData: {
//         ...formData,
//         [name]: value
//       }
//     })
//   }

//   render() {
//     const { loading, cancelEditSubcrimeRun, skillGainMultiplier: skillGainMultiplierProps } = this.props
//     const {
//       advanced,
//       formData: { expRequired, highmax, lowmax, min, jail, skillGainMultiplier }
//     } = this.state
//     const wrapClass = classnames({
//       [s['wrap']]: true,
//       [s['loading']]: loading
//     })

//     // console.log(this.props, skillGainMultiplierProps, 'this.props')

//     return (
//       <Fragment>
//         <div className={s.tooltipContainer}>
//           <span>
//             When the user reaches <strong>{expRequired / 4}</strong> crime experience, their success rate will begin
//             increasing from the minimum success chance <strong>{min}%</strong> up to the base success chance{' '}
//             <strong>{lowmax}%</strong> when they reach <strong>{expRequired}</strong> crime experience. Once the base
//             success chance has been achieved, this can then increase further up to the maximum success chance{' '}
//             <strong>{highmax}%</strong> as their crime skill level scales from 1 to 100.
//           </span>
//           <br />
//           <br />
//           <span>
//             Whenever the user is set to receive a failure, there is a <strong>{jail}%</strong> chance that this failure
//             will be a critical failure.
//           </span>
//           <br />
//           <br />
//           <span>
//             For every successful outcome, the user will receive an average of <strong>{skillGainMultiplier}</strong> x nerveUsed crime skill EXP.
//             As 1,000,000 is required for level 100, this will take between
//             <strong> {Math.round((1000000 / skillGainMultiplier) / 192) || 0}</strong>
//             -
//             <strong>{Math.round((1000000 / skillGainMultiplier) / 507) || 0} </strong>
//             days to achieve.
//           </span>
//         </div>
//         <div className={wrapClass}>
//           <div className={s['th']}>
//             <div className={s['thr']}>Optimal EXP:</div>
//             <div className={s['thr']}>Minimum success chance:</div>
//             <div className={s['thr']}>Base success chance:</div>
//             <div className={s['thr']}>Maximum success chance:</div>
//             <div className={s['thr']}>Critical Failure chance:</div>
//             <div className={s['thr']}>Skill Gain Multiplier:</div>
//             {advanced && <div className={s['thr']}>More</div>}
//           </div>
//           <div className={s['tb']}>
//             <div className={s['tbr']}>
//               <input
//                 name='expRequired'
//                 onChange={this._handleChange}
//                 value={expRequired}
//                 className={s['input']}
//                 type='text'
//               />
//             </div>
//             <div className={s['tbr']}>
//               <input name='min' onChange={this._handleChange} value={min} className={s['input']} type='text' />
//               <i>%</i>
//             </div>
//             <div className={s['tbr']}>
//               <input name='lowmax' onChange={this._handleChange} value={lowmax} className={s['input']} type='text' />
//               <i>%</i>
//             </div>
//             <div className={s['tbr']}>
//               <input name='highmax' onChange={this._handleChange} value={highmax} className={s['input']} type='text' />
//               <i>%</i>
//             </div>
//             <div className={s['tbr']}>
//               <input name='jail' onChange={this._handleChange} value={jail} className={s['input']} type='text' />
//               <i>%</i>
//             </div>
//             <div className={s['tbr']}>
//               <input name='skillGainMultiplier' onChange={this._handleChange} value={skillGainMultiplier} className={s['input']} type='text' />
//             </div>
//             {advanced && (
//               <div className={s['tbr']}>
//                 <input name='more' onChange={this._handleChange} value={0} className={s['input']} type='text' />
//               </div>
//             )}
//             <div className={s['tbr']}>
//               {/* <div className={s['add-link']}>
//                 <a onClick={this._toggleAdvanced} href='#'>
//                   {advanced ? 'less settings' : 'more settings'}
//                 </a>
//               </div> */}
//               <div className={s['action-btns']}>
//                 <button type='button' onClick={this._submit} className={s['btn'] + ' ' + 'torn-btn gold'}>
//                   Save
//                 </button>
//                 <button type='button' onClick={cancelEditSubcrimeRun} className={s['btn'] + ' ' + 'torn-btn gold'}>
//                   Cancel
//                 </button>
//               </div>
//             </div>
//           </div>
//         </div>
//       </Fragment>
//     )
//   }
// }

// SubcrimeSettingsEditor.propTypes = {
//   cancelEditSubcrimeRun: PropTypes.func,
//   crimeID: PropTypes.number,
//   formData: PropTypes.object,
//   loading: PropTypes.bool,
//   saveSubcrimeSettingsRun: PropTypes.func,
//   skillGainMultiplier: PropTypes.number
// }

// SubcrimeSettingsEditor.defaultProps = {
//   cancelEditSubcrimeRun: () => {},
//   crimeID: null,
//   formData: {},
//   loading: false,
//   saveSubcrimeSettingsRun: () => {},
//   skillGainMultiplier: null
// }

// const mapStateToProps = state => {
//   const formData = {
//     expRequired: state.app.currentCrime.expRequired,
//     highmax: state.app.currentCrime.successChance.highmax,
//     jail: state.app.currentCrime.jailChance.min,
//     lowmax: state.app.currentCrime.successChance.lowmax,
//     min: state.app.currentCrime.successChance.min,
//     skillGainMultiplier: state.app.currentCrime.skillGainMultiplier
//   }

//   return {
//     formData: formalizeFormData(formData)
//   }
// }

// const mapDispatchToProps = {
//   cancelEditSubcrimeRun: cancelEditSubcrime,
//   saveSubcrimeSettingsRun: saveSubcrimeSettings
// }

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(SubcrimeSettingsEditor)
