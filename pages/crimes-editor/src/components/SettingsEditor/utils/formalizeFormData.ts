import isDecimal from '../../../utils/isDecimal'

const formalizeFormData = (dataToNormalize: object) => {
  const tempData = {
    ...dataToNormalize
  }

  const setIntData = (keyData) => {
    if (keyData) {
      return String(keyData)?.replace(/\.|,\d$/i, '')
    }

    return keyData
  }

  const setDecimalData = (keyData) => {
    if (!keyData || keyData === '0' || /\.$/i.test(keyData)) {
      return keyData
    }

    return `${keyData}.0`
  }

  Object.keys(tempData).forEach((key: string) => {
    const keyData = tempData[key]

    const isInStrictList = ['expRequired', 'skillMultiplier', 'optimalExp', 'skillGainMultiplier'].includes(key)
    const alreadyDecimalValue = isDecimal({ value: keyData, withZeroTail: true })

    tempData[key] = alreadyDecimalValue || isInStrictList ? setIntData(keyData) : setDecimalData(keyData)
  })

  return tempData
}

export default formalizeFormData
