// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'btn': string;
  'buttonsWrap': string;
  'cellWrap': string;
  'columnsWrap': string;
  'container': string;
  'flexCenter': string;
  'flexCenterJustEnd': string;
  'flexCenterJustStart': string;
  'globalSvgShadow': string;
  'infoContainer': string;
  'inputCell': string;
  'label': string;
  'rowContainer': string;
  'rowLabel': string;
  'rowsWrap': string;
  'settingsWrap': string;
  'title': string;
  'titleContainer': string;
}
export const cssExports: CssExports;
export default cssExports;
