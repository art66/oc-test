import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import './styles.scss'
import { hideDebugInfo } from '../../modules'

class DebugInfo extends Component {
  render() {
    let { msg, close } = this.props
    console.error(msg)
    if (typeof msg === 'object') msg = JSON.stringify(msg)
    return (
      <div className="debug-info">
        <span dangerouslySetInnerHTML={{ __html: msg }} />
        <span className="debug-info--close" onClick={close}>
          x
        </span>
      </div>
    )
  }
}

DebugInfo.propTypes = {
  msg: PropTypes.string.isRequired,
  close: PropTypes.func
}

const mapDispatchToProps = {
  close: hideDebugInfo
}

const mapStateToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(DebugInfo)
