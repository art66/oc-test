import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loadEditorData, loadItemsData, filterOutcomes, setHashHandler } from '../modules'
import { CrimeSelect, Stats, SorterWrap } from '../components'
import { CrimeSettingsEditor } from '../components/SettingsEditor'
import CoreLayout from '../layouts/CoreLayout'
import { switchStatsTab, sortBy, sortOrderEmit, createNewOutcome, editOutcome, submit } from '../modules'

/* global $ */
class EditorContainer extends Component {
  constructor(props) {
    super(props)
    props.setHashHandler()
    props.loadEditorData()
    props.loadItemsData()
    // props.loadAmmoData()
    // disable global tooltips for content-wrapper
    const i = setInterval(() => {
      if ($('.content-wrapper').data('tooltip')) {
        $('.content-wrapper').tooltip('destroy')
        clearInterval(i)
      }
    }, 1000)
  }

  render() {
    const {
      chartData,
      createNewOutcome,
      crimeDataLoaded,
      crimesListLoaded,
      currentCrime,
      drops,
      editedItem,
      editOutcome,
      editCrimeMode,
      filterOutcomes,
      loading,
      outcomesFilter,
      selectedStatsTab,
      sortBy,
      sortOrderEmit,
      sortByField,
      sortOrder,
      submit,
      switchStatsTab,
      thirdLine,
      typeConditions,
      dropDownState
    } = this.props

    return (
      <CoreLayout>
        {crimesListLoaded && <CrimeSelect connected={editCrimeMode} />}
          <div>
            {editCrimeMode && <CrimeSettingsEditor />}
            {crimeDataLoaded && (
              <>
                <SorterWrap
                  createNewOutcome={createNewOutcome}
                  editedItem={editedItem}
                  filterOutcomes={filterOutcomes}
                  outcomesFilter={outcomesFilter}
                  selectedStatsTab={selectedStatsTab}
                  sortBy={sortBy}
                  sortOrder={sortOrder}
                  sortOrderEmit={sortOrderEmit}
                  sortByField={sortByField}
                />
                <Stats
                  chartData={chartData}
                  crimeID={currentCrime.ID}
                  drops={drops}
                  editedItem={editedItem}
                  editOutcome={editOutcome}
                  loading={loading}
                  outcomes={currentCrime.outcomes || {}}
                  outcomesFilter={outcomesFilter}
                  selectedStatsTab={selectedStatsTab}
                  sortByField={sortByField}
                  sortOrder={sortOrder}
                  submit={submit}
                  switchStatsTab={switchStatsTab}
                  thirdLine={thirdLine}
                  typeConditions={typeConditions}
                />
              </>
            )}
          </div>
      </CoreLayout>
    )
  }
}

EditorContainer.propTypes = {
  chartData: PropTypes.array,
  createNewOutcome: PropTypes.func,
  crimeDataLoaded: PropTypes.bool,
  crimesListLoaded: PropTypes.bool,
  currentCrime: PropTypes.object,
  drops: PropTypes.object,
  editedItem: PropTypes.number,
  editOutcome: PropTypes.func,
  editCrimeMode: PropTypes.bool,
  filterOutcomes: PropTypes.func,
  loadEditorData: PropTypes.func,
  loading: PropTypes.bool,
  loadItemsData: PropTypes.func,
  // loadAmmoData: PropTypes.func,
  outcomesFilter: PropTypes.string,
  selectedStatsTab: PropTypes.string,
  setHashHandler: PropTypes.func,
  sortBy: PropTypes.func,
  sortByField: PropTypes.string,
  sortOrder: PropTypes.string,
  submit: PropTypes.func,
  switchStatsTab: PropTypes.func,
  thirdLine: PropTypes.array,
  typeConditions: PropTypes.object
}

const mapDispatchToProps = {
  createNewOutcome,
  editOutcome,
  filterOutcomes,
  loadEditorData,
  loadItemsData,
  // loadAmmoData,
  setHashHandler,
  sortBy,
  sortOrderEmit,
  submit,
  switchStatsTab
}

const mapStateToProps = state => ({
  chartData: state.app.chartData,
  crimeDataLoaded: state.app.crimeDataLoaded,
  crimesListLoaded: state.app.crimesListLoaded,
  currentCrime: state.app.currentCrime,
  drops: {
    chance: state.app.chance,
    triggers: state.app.triggers,
    specialTypes: state.app.specialTypes
  },
  special: state.app.special,
  editedItem: state.app.editedItem,
  editCrimeMode: state.app.editCrimeMode,
  loading: state.app.loading,
  outcomesFilter: state.app.outcomesFilter,
  selectedStatsTab: state.app.selectedStatsTab,
  sortByField: state.app.sortByField,
  sortOrder: state.app.sortOrder,
  thirdLine: state.app.thirdLine,
  typeConditions: state.app.typeConditions,
  dropDownState: state.app.dropDownState
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditorContainer)
