import PropTypes from 'prop-types'
import React from 'react'
import { Provider } from 'react-redux'
import EditorContainer from './EditorContainer'

export const AppContainer = ({ store }) => (
  <Provider store={store}>
    <EditorContainer />
  </Provider>
)

AppContainer.propTypes = {
  store: PropTypes.object.isRequired
}

export default AppContainer
