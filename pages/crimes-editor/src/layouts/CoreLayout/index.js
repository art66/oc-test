import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import InfoBox from '@torn/shared/components/InfoBox'
// import PictureGenerator from '@torn/shared/components/PictureGenerator'

import { DebugInfo } from '../../components'
import Tooltip from '../../components/Tooltip/index'

import s from './styles.cssmodule.scss'

// TODO: remove this once team will understand how to use the PictureGenerator package!!!
// const Picture = () => {
//   const config = {
//     classes: {
//       picture: 'testPicture',
//       img: 'testImage'
//     },
//     alt: 'Progressive Torn\'s Images',
//     type: 'png',
//     path: 'images/v2/retina/torn_logo/',
//     name: 'logo',
//     extraResolutions: {
//       desktop: 'min-width: 601px',
//       mobile: 'max-width: 600px'
//     }
//   }

//   return <PictureGenerator {...config} />
// }

const CoreLayout = ({ children, app: { dbg, infoBox } }) => {
  return (
    <div className={s['wrap']}>
      {/* <Picture /> */}
      {dbg && <DebugInfo msg={dbg} />}
      {infoBox && <InfoBox {...infoBox} />}
      <Tooltip />
      {children}
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  )
}

CoreLayout.propTypes = {
  children: PropTypes.node.isRequired,
  app: PropTypes.object
}

const mapStateToProps = state => ({
  app: state.app
})

export default connect(mapStateToProps)(CoreLayout)
