const isDecimal = ({ value, withZeroTail }: { value: string | number; withZeroTail: boolean }) => {
  const isZeroTail = /\.0$/i.test(String(value))
  const isPureDecimal = (Number(value) % 1) !== 0

  return isPureDecimal || withZeroTail && isZeroTail
}

export default isDecimal
