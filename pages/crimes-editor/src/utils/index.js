import { ROOT_URL } from '../constants'

export function fetchUrl(url, data) {
  const config = {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
  }
  if (data) {
    config.method = 'post'
    config.body = JSON.stringify(data)
  }
  return fetch(ROOT_URL + url, config).then(response =>
    response.text().then(text => {
      var error
      try {
        var json = JSON.parse(text)
      } catch (e) {
        error = 'Server responded with: ' + text
      }
      if (error) {
        throw new Error(error)
      } else {
        return json
      }
    })
  )
}
