import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import app from '../modules'

export const makeRootReducer = asyncReducers => {
  return combineReducers({
    ...asyncReducers,
    app,
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    })
  })
}

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
