import { showDebugInfo, showInfoBox } from '../../modules'

export const fetchInterceptor = store => next => action => {
  if (action.meta === 'ajax') {
    if (action.json.debugInfo) {
      store.dispatch(showDebugInfo(action.json.debugInfo))
    } else if (action.json.redirect !== undefined) {
      if (action.json.redirect === true) {
        location.href = location.protocol + '//' + location.hostname + '/' + action.json.url
      } else if (action.json.redirect === false) {
        store.dispatch(showInfoBox({ msg: action.json.content, color: 'red' }))
      }
    }
  }
  return next(action)
}
