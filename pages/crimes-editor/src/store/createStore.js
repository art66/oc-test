import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'
import { fetchInterceptor } from './middleware/fetchInterceptor'
import logger from './middleware/reduxLogger'
import makeRootReducer from './reducers'

export default (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [thunk, fetchInterceptor]

  if (__DEV__) {
    middleware.push(logger)
  }

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []
  if (__DEV__) {
    const devToolsExtension = window.devToolsExtension
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer, applyMiddleware(...middleware), ...enhancers)
  )
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('../modules', () => {
      const reducers = require('../modules').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
