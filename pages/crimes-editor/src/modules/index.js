import { fetchUrl as fetchUrlFull } from '@torn/shared/utils'
import { fetchUrl } from '../utils'

// ------------------------------------
// Constants
// ------------------------------------
export const CRIMES_LIST_LOADED = 'CRIMES_LIST_LOADED'
export const EMIT_REQUEST = 'EMIT_REQUEST'
export const CRIME_DATA_LOADED = 'CRIME_DATA_LOADED'
export const SHOW_DEBUG_INFO = 'SHOW_DEBUG_INFO'
export const HIDE_DEBUG_INFO = 'HIDE_DEBUG_INFO'
export const SHOW_INFOBOX = 'SHOW_INFOBOX'
export const DROPDOWN_SELECT = 'DROPDOWN_SELECT'
export const SWITCH_STATS_TAB = 'SWITCH_STATS_TAB'
export const SORT_STATS = 'SORT_STATS'
export const NEW_OUTCOME = 'NEW_OUTCOME'
export const EDIT_OUTCOME = 'EDIT_OUTCOME'
export const SUBMIT = 'SUBMIT'
export const INVALIDATE_OUTCOME = 'INVALIDATE_OUTCOME'
export const UPDATE_CHART = 'UPDATE_CHART'
export const EDIT_CRIME = 'EDIT_CRIME'
// export const EDIT_SUBCRIME = 'EDIT_SUBCRIME'
// export const CANCEL_EDIT_SUBCRIME = 'CANCEL_EDIT_SUBCRIME'
export const CANCEL_EDIT_CRIME = 'CANCEL_EDIT_CRIME'
export const CHANGE_CRIME_SETTINGS = 'CHANGE_CRIME_SETTINGS'
export const SAVE_SUBCRIME_SETTINGS = 'SAVE_SUBCRIME_SETTINGS'
export const SAVE_CRIME_SETTINGS = 'SAVE_CRIME_SETTINGS'
export const CRIME_SETTINGS_UPDATED = 'CRIME_SETTINGS_UPDATED'
export const CRIME_SETTINGS_ATTEMPT = 'CRIME_SETTINGS_ATTEMPT'
export const SAVE_SETTINGS_HANDLER = 'SAVE_SETTINGS_HANDLER'
export const HIDE_EDITOR = 'HIDE_EDITOR'
export const ITEMS_LOADED = 'ITEMS_LOADED'
export const FILTER_OUTCOMES = 'FILTER_OUTCOMES'
export const CHART_STATISTIC = 'CHART_STATISTIC'
export const CHART_STATISTIC_FETCHED = 'CHART_STATISTIC_FETCHED'
export const SORT_ORDER = 'SORT_ORDER'
export const OUTCOME_LOADED = 'OUTCOME_LOADED'
export const OUTCOME_LAYOUT_CHANGED = 'OUTCOME_LAYOUT_CHANGED'
// export const AMMO_LOADED = 'AMMO_LOADED'

// ------------------------------------
// Actions
// ------------------------------------

export function setSelectValue({ selectName, value }) {
  return {
    type: DROPDOWN_SELECT,
    selectName,
    value
  }
}

export const loadEditorData = () => dispatch => fetchUrl('')
    .then(json => dispatch(crimesListLoaded(json)))
    .then(json => dispatch(handleNewHash()))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))

export const loadCrime = crimeID => dispatch => {
  dispatch(emitRequest())
  fetchUrl('&step=view&crimeID=' + crimeID)
    .then(json => dispatch(crimeDataLoaded(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const loadChartStats = (crimeID, iterationCount) => dispatch => {
  dispatch(startChartFetch(crimeID))
  fetchUrl('&step=testRun&iterations=' + iterationCount + '&ID=' + crimeID)
    .then(json => dispatch(getChartStats(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const loadOutcome = ID => (dispatch, getState) => {
  dispatch(emitRequest())
  fetchUrl('&step=view&ID=' + ID)
    .then(json => dispatch(crimeDataLoaded(json)))
    .then(json => {
      const state = getState().app
      const { outcomes } = state.currentCrime
      let selectedStatsTab
      let outcomeIndex

      Object.keys(outcomes).map(tab => {
        let index = outcomes[tab].findIndex(outcome => outcome.ID === Number(ID))

        if (index !== -1) {
          selectedStatsTab = tab
          outcomeIndex = index
        }
      })
      selectedStatsTab && dispatch(switchStatsTab(selectedStatsTab, true))
      !isNaN(outcomeIndex) && dispatch(editOutcome(outcomeIndex))
    })
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const crimesListLoaded = json => ({
  type: CRIMES_LIST_LOADED,
  json,
  meta: 'ajax'
})

export function emitRequest() {
  return {
    type: EMIT_REQUEST
  }
}

export const crimeDataLoaded = json => ({
  type: CRIME_DATA_LOADED,
  json,
  meta: 'ajax'
})

export const settingDataAttempt = () => ({
  type: CRIME_SETTINGS_ATTEMPT
})

export const settingsDataLoaded = json => ({
  type: CRIME_SETTINGS_UPDATED,
  json,
  meta: 'ajax'
})

export const outcomeFetched = json => ({
  type: OUTCOME_LOADED,
  json,
  meta: 'ajax'
})

export function showInfoBox(data) {
  return {
    type: SHOW_INFOBOX,
    payload: data
  }
}

export const showDebugInfo = ({ msg }) => ({
  type: SHOW_DEBUG_INFO,
  loading: false,
  msg
})

export const hideDebugInfo = () => ({
  type: HIDE_DEBUG_INFO
})

export const switchStatsTab = (selectedStatsTab, anyway) => ({
  type: SWITCH_STATS_TAB,
  selectedStatsTab,
  anyway
})

export const sortBy = sortByField => ({
  type: SORT_STATS,
  sortByField
})

export const sortOrderEmit = sortOrder => ({
  type: SORT_ORDER,
  sortOrder
})

export const createNewOutcome = () => ({
  type: NEW_OUTCOME
})

export const editOutcome = (index, ID) => ({
  type: EDIT_OUTCOME,
  index,
  ID
})

export const submit = (step, data) => dispatch => {
  dispatch(emitRequest())
  fetchUrl('&step=' + step, data)
    .then(json => dispatch(crimeDataLoaded(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const requestOutcome = (crimeID, outcomeID) => dispatch => {
  const config = {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
  }

  dispatch(emitRequest())
  fetchUrlFull(`loader2.php?sid=manageCrimesData&step=testOutcome&ID=${crimeID}&definedOutcomeID=${outcomeID}`, config)
    .then(json => dispatch(outcomeFetched(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const invalidateOutcome = () => ({
  type: INVALIDATE_OUTCOME
})

export const requestChartUpdate = input => dispatch => {
  const { crimeID, ...data } = input

  fetchUrl('&style=api&crimeID=' + crimeID, data)
    .then(json => dispatch(updateChart(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const updateChart = json => ({
  type: UPDATE_CHART,
  json
})

export const editCrime = () => ({
  type: EDIT_CRIME
})

// export const editSubcrime = () => ({
//   type: EDIT_SUBCRIME
// })

export const cancelEditCrime = () => ({
  type: CANCEL_EDIT_CRIME
})

// export const cancelEditSubcrime = () => ({
//   type: CANCEL_EDIT_SUBCRIME
// })

export const changeCrimeSettings = (value, key, rowID) => {
  return {
    value,
    key,
    rowID,
    type: CHANGE_CRIME_SETTINGS
  }
}

export const saveCrimeSettings = () => (dispatch, getState) => {
  dispatch(settingDataAttempt())

  const { app } = getState()

  const { crimes, dropDownState } = app

  const crimesEditorData = crimes.filter(crime => crime.typeID === dropDownState.crime)

  fetchUrl('&step=saveSubcrimes', { data: crimesEditorData })
    .then(json => dispatch(settingsDataLoaded(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const saveSubcrimeSettings = (step, data) => dispatch => {
  // dispatch(emitRequest())
  fetchUrl('&step=' + step, data)
    .then(json => dispatch(crimeDataLoaded(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))
}

export const saveSettingsHandler = () => ({
  type: SAVE_SETTINGS_HANDLER
})

export const hideEditor = () => ({
  type: HIDE_EDITOR
})

export const loadItemsData = () => dispatch => fetchUrl('&step=getItems')
    .then(json => dispatch(itemsLoaded(json)))
    .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))

// export const loadAmmoData = () => dispatch => fetchUrl('&step=getAmmo')
//     .then(json => dispatch(ammoLoaded(json)))
//     .catch(error => dispatch(showDebugInfo({ msg: error.toString() })))

export const itemsLoaded = json => ({
  type: ITEMS_LOADED,
  json
})

// export const ammoLoaded = json => ({
//   type: AMMO_LOADED,
//   json
// })

export const filterOutcomes = filter => ({
  type: FILTER_OUTCOMES,
  filter
})

export const setHashHandler = () => dispatch => {
  window.addEventListener('hashchange', () => dispatch(handleNewHash()), false)
}

export const handleNewHash = () => dispatch => {
  var outcomeID = window.location.hash.replace(/^#\/?|\/$/g, '').split('/')[0]

  if (outcomeID && !isNaN(outcomeID)) {
    console.log('loading: ' + outcomeID)
    dispatch(loadOutcome(outcomeID))
  }
}

export function startChartFetch(crimeID) {
  return {
    type: CHART_STATISTIC,
    crimeID
  }
}

export const getChartStats = json => ({
  type: CHART_STATISTIC_FETCHED,
  json
})

export const changeOutcomeLayout = meqiaQuery => ({
  type: OUTCOME_LAYOUT_CHANGED,
  meqiaQuery
})

export const actions = {
  createNewOutcome,
  crimeDataLoaded,
  crimesListLoaded,
  editOutcome,
  filterOutcomes,
  handleNewHash,
  hideDebugInfo,
  hideEditor,
  invalidateOutcome,
  itemsLoaded,
  loadCrime,
  loadItemsData,
  loadOutcome,
  requestChartUpdate,
  saveSettingsHandler,
  saveSubcrimeSettings,
  setHashHandler,
  setSelectValue,
  showDebugInfo,
  showInfoBox,
  sortBy,
  sortOrderEmit,
  submit,
  switchStatsTab,
  updateChart,
  loadChartStats,
  getChartStats
}

// ------------------------------------
// Helpers
// ------------------------------------
const rearangeChartData = (outSucGraph, index) => {
  const chartData = [[], [], []]

  Object.keys(outSucGraph).map(i => {
    // if (i === 1 || i % 5 === 0) {
    if (index !== undefined) {
      chartData[0].push(outSucGraph[i][index] || 0)
    } else {
      chartData[0].push(outSucGraph[i].rec || 0)
      chartData[1].push(outSucGraph[i].money || 0)
      chartData[2].push(outSucGraph[i].moneymedian || 0)
    }
    // }
  })
  return index ? chartData[0] : chartData
}

const crimesSettingsAdapter = (rowData, key, value) => {
  if (key === 'optimalExp') {
    rowData.expRequired = value
  } else if (key === 'skillMultiplier') {
    rowData.skillGainMultiplier = value
  } else if (key === 'criticalChance') {
    rowData.jailChance = {
      ...rowData.jailChance,
      min: value
    }
  } else if (key === 'baseChance') {
    rowData.successChance = {
      ...rowData.successChance,
      lowmax: value
    }
  } else if (key === 'minChance') {
    rowData.successChance = {
      ...rowData.successChance,
      min: value
    }
  } else if (key === 'maxChance') {
    rowData.successChance = {
      ...rowData.successChance,
      highmax: value
    }
  } else {
    rowData[key] = value
  }

  return rowData
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [CRIMES_LIST_LOADED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    crimesListLoaded: true
  }),
  [CRIME_SETTINGS_ATTEMPT]: state => ({
    ...state,
    loading: true
  }),
  [CRIME_SETTINGS_UPDATED]: (state, action) => ({
    ...state,
    ...action.json.DB,
    loading: false
  }),
  [CRIME_DATA_LOADED]: (state, action) => {
    const [filteredCrime] = action.json.DB.crimesTypes.filter(
      crime => crime.crimeRoute.toLowerCase() === action.json.DB.currentCrime.classNameType.toLowerCase()
    )
    const typeConditions = (filteredCrime && filteredCrime.typeCondition) || null

    return {
      ...state,
      ...action.json.DB,
      chartData: action.json.DB.outSucGraph ? rearangeChartData(action.json.DB.outSucGraph) : state.chartData,
      // thirdLine: action.json.DB.outSucGraph && rearangeChartData(action.json.DB.outSucGraph, 'money'),
      dropDownState: {
        crime: action.json.DB.currentCrime.typeID,
        subcrime: action.json.DB.currentCrime.ID
      },
      thirdLine: undefined,
      sortOrder: 'asc',
      crimeDataLoaded: true,
      loading: false,
      typeConditions
    }
  },
  [EMIT_REQUEST]: state => ({
    ...state,
    loading: true
  }),
  [SHOW_DEBUG_INFO]: (state, action) => ({
    ...state,
    dbg: action.msg
  }),
  [HIDE_DEBUG_INFO]: state => ({
    ...state,
    dbg: null
  }),
  [SHOW_INFOBOX]: (state, action) => ({
    ...state,
    infoBox: action.payload
  }),
  [DROPDOWN_SELECT]: (state, action) => ({
    ...state,
    outcomesFilter: '',
    editCrimeMode: action.selectName === 'crime' && !action.value ? false : state.editCrimeMode,
    editedItem: undefined,
    crimeDataLoaded: false,
    dropDownState: {
      ...state.dropDownState,
      [action.selectName]: action.value
    }
  }),
  [SWITCH_STATS_TAB]: (state, action) => {
    let { selectedStatsTab, anyway } = action

    if (!anyway && state.selectedStatsTab === selectedStatsTab) {
      selectedStatsTab = null
    }
    return {
      ...state,
      selectedStatsTab,
      editedItem: undefined
    }
  },
  [SORT_STATS]: (state, action) => ({
    ...state,
    sortByField: action.sortByField,
    editedItem: undefined
    // sortOrder: state.sortOrder === 'asc' ? 'desc' : 'asc'
  }),
  [SORT_ORDER]: (state, action) => ({
    ...state,
    sortOrder: action.sortOrder
  }),
  [NEW_OUTCOME]: state => {
    const selectedOutcomes = state.currentCrime.outcomes[state.selectedStatsTab] || []

    return {
      ...state,
      editedItem: 0,
      currentCrime: {
        ...state.currentCrime,
        outcomes: {
          ...state.currentCrime.outcomes,
          [state.selectedStatsTab]: [
            {
              meta: 'new',
              perm: 3,
              timestamp: 0
            },
            ...selectedOutcomes
          ]
        }
      }
    }
  },
  [EDIT_OUTCOME]: (state, action) => ({
    ...state,
    editedItem: state.editedItem === action.index ? undefined : action.index,
    outcomeCrimeID: action.ID,
    thirdLine: undefined,
    currentCrime: {
      ...state.currentCrime,
      outcome: null
    }
  }),
  [INVALIDATE_OUTCOME]: (state, action) => ({
    ...state,
    outcomeAltered: true
  }),
  [UPDATE_CHART]: (state, action) => ({
    ...state,
    thirdLine: action.json.DB.outSucGraph && rearangeChartData(action.json.DB.outSucGraph, 'money')
  }),
  [EDIT_CRIME]: state => ({
    ...state,
    editCrimeMode: !state.editCrimeMode
  }),
  // [EDIT_SUBCRIME]: state => ({
  //   ...state,
  //   editSubcrimeMode: !state.editSubcrimeMode
  // }),
  [CANCEL_EDIT_CRIME]: state => ({
    ...state,
    editCrimeMode: false
  }),
  // [CANCEL_EDIT_SUBCRIME]: state => ({
  //   ...state,
  //   editSubcrimeMode: false
  // }),
  [CHANGE_CRIME_SETTINGS]: (app, { value, key, rowID }) => {
    return {
      ...app,
      crimes: app.crimes.map(row => {
        if (String(row._id.$oid) === String(rowID)) {
          const tempRow = {
            ...row
          }

          return crimesSettingsAdapter(tempRow, key, value)
        }

        return row
      })
    }
  },
  [SAVE_SETTINGS_HANDLER]: (state, action) => ({
    ...state,
    loading: false
  }),
  [HIDE_EDITOR]: state => ({
    ...state,
    editedItem: undefined
  }),
  [ITEMS_LOADED]: (state, action) => ({
    ...state,
    items: action.json.DB.map(({ ID, name, cost, medianprice }) => ({
      value: ID,
      label: `${name} (${ID})`,
      cost,
      medianprice
    }))
  }),
  // [AMMO_LOADED]: (state, action) => (console.log(action), {
  //   ...state,
  //   ammo: action && action.json && action.json && action.json.DB && action.json.DB.map(({ ID, name, cost, medianprice }) => ({
  //     value: ID,
  //     label: `${name} (${ID})`,
  //     cost,
  //     medianprice
  //   })) || []
  // }),
  [FILTER_OUTCOMES]: (state, action) => ({
    ...state,
    outcomesFilter: action.filter
  }),
  [CHART_STATISTIC]: (state, action) => ({
    ...state,
    chartStatsFetch: true,
    lastCrimeID: action.crimeID
  }),
  [CHART_STATISTIC_FETCHED]: (state, action) => ({
    ...state,
    crimes: state.crimes.map(
      crime => crime.ID === state.outcomeCrimeID ?
          {
              ...crime,
              chartStats: action.json
            } :
          crime
    ),
    chartStats: action.json,
    chartStatsFetch: false
  }),
  [OUTCOME_LOADED]: (state, action) => ({
    ...state,
    loading: false,
    currentCrime: {
      ...state.currentCrime,
      outcome: {
        ...action.json,
        mediaType: (state.currentCrime.outcome && state.currentCrime.outcome.mediaType) || 'desktop'
      }
    }
  }),
  [OUTCOME_LAYOUT_CHANGED]: (state, action) => ({
    ...state,
    currentCrime: {
      ...state.currentCrime,
      outcome: {
        ...state.currentCrime.outcome,
        mediaType: action.meqiaQuery
      }
    }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  dropDownState: {},
  outcomesFilter: '',
  selectedStatsTab: 'success',
  special: 0,
  specialTypes: [
    { value: 0, label: 'Normal' },
    { value: 1, label: 'Special' }
  ]
}

export default function(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  // console.log(state, action)

  return handler ? handler(state, action) : state
}
