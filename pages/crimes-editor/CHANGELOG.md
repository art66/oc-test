# Crimes-Editor - Torn

## 2.11.0
 * Fixed Action Buttons in favor of global torn migration.
 * Outcome import was replaced in favor of crimes global one.

## 2.10.0
 * Set Special field in OutcomeEditor.
 * Added Cooldown Row highlighting.

## 2.9.6
 * Fixed settings handlers in case of updates React bundles up to v.16.11.

## 2.9.5
 * Fixed default value in rewardGiven.

## 2.9.4
 * Fixed TC Clothing searching.

## 2.9.3
 * Fixed Outcomes creation.

## 2.9.2
 * Fixed Conditions appear inside EditorOutcome component.

## 2.9.1
 * Refactored EditorOutcome component.

## 2.9.0
 * Added DropDown menu selection for Conditions sections inside OutcomeEditor.

## 2.8.1
 * Fixed DropDown menu overflow while over Outcome section.

## 2.8.0
 * Removed server configuration in a whole.

## 2.7.0
 * Added reduxLogger.

## 2.6.0
 * Fixed lvl unsaving.

## 2.5.0
 * Added special text section in the settings.

## 2.4.2
 * Fixed default minLvl value on submit in the outcome config.

## 2.4.1
 * MInor fix for level layout in the row section.

## 2.4.0
 * Added skillGainMultiplier input inside the SettingEditor panel.

## 2.3.0
 * Added new LVL layout in the row lvl section.

## 2.2.0
 * Injected Outcome Component inside OutcomeEditor Component.
 * Updated its styles.

## 2.1.0
 * Refactored ToolTip Component due to the original TooltipNew API changes.
 * Added Action and Reducers for Generating live Outcomes results.

## 2.0.1
 * Added Outcome layout for live debugging via Admin panel.
 * Updated React-transition-group to ver. 3.0.0.
 * Refactored Components.

## 1.8.1
 * Refactored Tooltips due to the original Tooltip Component API changes.
 * Improved story row font readability.

## 1.7.1
 * Added react-based Tooltips for Chance Column in Row Component.
 * Minor fixes.

## 1.6.1
 * Fixed unrelease Button reset logic.
 * Fixed release button logic about non-adding cooldown prop on POST requeet.

## 1.6.0
 * Fixed cooldown UI Layout.
 * Updated Chance Column layout in the Outcomes List Component.

## 1.5.0
 * Added typeCondiotion props inside POST request on each commit in OutcomeEditor.

## 1.4.0
 * Added new input field (typeConditions) inside OutcomeEditor Component.

## 1.3.1
 * Added search sorting for outcomes dropdown menu by ID/title/text.
 * Fixes crete new Outcome button.
 * Refactored Rows/Rows/Stat Components.
 * Improved render speed by separationg components, changing sorting methods in Stat Component and intuitive use of CSSTransition group for component that are really need to be rerender.

## 1.2.1
 * Added extra sorting by rarity, stage and trigger.
 * Fixes previos sorting with non-correct layout for date, evel, reward.
 * Tiny rewritten previous version of sorting, made it more safely and predictable.

## 1.1.1
 * Added checkbox for Outcome Rewards editor field.

## 1.1.1
 * Fixed OutcomeEditor Rarity value.
 * Added extra field(maxLevel) for LVL settings.
 * Added validation for them with warning message.

## 1.0.2
 * Added prop checking and default value for drops.chance prop in Outcome component

## 1.0.1
 * check and fix the problem "when I select bootlegging, choose a subcrime and hit the new crime    button, it comes up with an empty page"

## 1.0.0
 * Added chart tabs and iteration stats functionals in Outcome Component.
