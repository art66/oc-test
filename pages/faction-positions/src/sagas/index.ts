import { put, takeEvery } from 'redux-saga/effects'
import { ITogglePermission } from '../interfaces'
import * as a from '../constants/actionTypes'
import fetchUrl from '../utils/fetchURL'
import { fetchPositionsSuccess, createPositionFailure, editPositionFailure, removePositionFailure } from '../actions'

interface IAction<TPayload> {
  type: string
  payload: TPayload
}

function* fetchData() {
  try {
    const response = yield fetchUrl('sid=factionsControlPositions&step=init')

    yield put(fetchPositionsSuccess(response))
  } catch (error) {
    console.error(error)
  }
}

function* createPosition({ payload }: IAction<string>) {
  try {
    const requestData = {
      position: {
        label: payload
      }
    }

    yield fetchUrl('sid=factionsControlPositions&step=createPosition', requestData)
  } catch (error) {
    yield put(createPositionFailure(payload, error.message))
    console.error(error)
  }
}

function* editPosition({ payload }: IAction<{ id: number; label: string }>) {
  try {
    yield fetchUrl('sid=factionsControlPositions&step=updatePosition', { position: payload })
  } catch (error) {
    yield put(editPositionFailure(payload, error.message))
    console.error(error)
  }
}

function* removePosition({ payload }: IAction<number>) {
  try {
    yield fetchUrl('sid=factionsControlPositions&step=removePosition', { id: payload })
  } catch (error) {
    yield put(removePositionFailure(payload))
    console.error(error)
  }
}

function* togglePermission({ payload }: IAction<ITogglePermission>) {
  try {
    yield fetchUrl('sid=factionsControlPositions&step=togglePermission', payload)
  } catch (error) {
    console.error(error)
  }
}

export default function* appSaga() {
  yield takeEvery(a.FETCH_POSITIONS, fetchData)
  yield takeEvery(a.CREATE_POSITION, createPosition)
  yield takeEvery(a.EDIT_POSITION, editPosition)
  yield takeEvery(a.REMOVE_POSITION, removePosition)
  yield takeEvery(a.TOGGLE_PERMISSION, togglePermission)
}
