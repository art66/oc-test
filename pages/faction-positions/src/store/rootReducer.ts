import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import reducer from '../reducers'
import reducerUI from '../reducers/ui'

export const makeRootReducer = (): any => {
  return combineReducers({
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    }),
    data: reducer,
    ui: reducerUI
  })
}

export default makeRootReducer
