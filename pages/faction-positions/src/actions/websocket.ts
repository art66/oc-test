import {
  togglePermissionSuccess,
  createPositionSuccess,
  editPositionSuccess,
  removePositionSuccess,
  assignPosition,
  kickMember,
  updateCurrentUserPermissions
} from './index'
import { IPosition } from '../interfaces'
import * as types from '../interfaces/actions'

declare const WebsocketHandler: any

const initWS = (dispatch: any): void => {
  WebsocketHandler.addEventListener('faction', 'kick', (payload: { user: { positionID: number } }): void => {
    dispatch(kickMember(payload.user.positionID))
  })

  WebsocketHandler.addEventListener('factionPositions', 'togglePermission', (data: types.TTogglePermissionPayload): void => {
    dispatch(togglePermissionSuccess(data))
  })

  WebsocketHandler.addEventListener('factionPositions', 'createPosition', (newPosition: IPosition): void => {
    dispatch(createPositionSuccess(newPosition))
  })

  WebsocketHandler.addEventListener('factionPositions', 'removePosition', (position: { id: number }): void => {
    dispatch(removePositionSuccess(position.id))
  })

  WebsocketHandler.addEventListener('factionPositions', 'updatePosition', (newPosition: IPosition): void => {
    dispatch(editPositionSuccess(newPosition))
  })

  WebsocketHandler.addEventListener('factionPositions', 'assignPositions', (data: types.TAssignPositionPayload): void => {
    dispatch(assignPosition(data))
  })

  WebsocketHandler.addEventListener('factionPositions', 'updateCurrentUser', (payload: types.TUpdateCurrentUserPayload): void => {
    dispatch(updateCurrentUserPermissions(payload.permissions))
  })
}

export default initWS
