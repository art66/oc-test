import { createAction } from 'redux-actions'
import { ITogglePermission } from '../interfaces'
import * as a from '../constants/actionTypes'

export const fetchPositions = createAction(a.FETCH_POSITIONS)
export const fetchPositionsSuccess = createAction(a.FETCH_POSITIONS_SUCCESS, data => data)

export const createPosition = createAction(a.CREATE_POSITION, label => label)
export const createPositionSuccess = createAction(a.CREATE_POSITION_SUCCESS, position => position)
export const createPositionFailure = createAction(a.CREATE_POSITION_FAILURE, (position, error) => ({ position, error }))
export const editPosition = createAction(a.EDIT_POSITION, (id, label) => ({ id, label }))
export const editPositionSuccess = createAction(a.EDIT_POSITION_SUCCESS, position => position)
export const editPositionFailure = createAction(a.EDIT_POSITION_FAILURE, (position, error) => ({ position, error }))
export const removePosition = createAction(a.REMOVE_POSITION, id => id)
export const removePositionSuccess = createAction(a.REMOVE_POSITION_SUCCESS, id => id)
export const removePositionFailure = createAction(a.REMOVE_POSITION_FAILURE, id => id)
export const assignPosition = createAction(a.ASSIGN_POSITION, data => data)

export const kickMember = createAction(a.KICK_MEMBER, positionID => positionID)

export const togglePermission = createAction(a.TOGGLE_PERMISSION, (data: ITogglePermission) => data)
export const togglePermissionSuccess = createAction(a.TOGGLE_PERMISSION_SUCCESS, (data: ITogglePermission) => data)

export const updateCurrentUserPermissions = createAction(a.UPDATE_CURRENT_USER_PERMISSIONS, permissions => permissions)
