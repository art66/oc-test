import { createAction } from 'redux-actions'
import { SET_SCROLLABLE_STATE, REMOVE_VALIDATION_ERROR } from '../constants/actionTypes'

export const setScrollableState = createAction(SET_SCROLLABLE_STATE, (value: boolean) => value)
export const removeValidationError = createAction(REMOVE_VALIDATION_ERROR, (id: string) => id)
