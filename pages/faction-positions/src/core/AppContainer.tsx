import React, { Component } from 'react'
import { Store } from 'redux'
import {connect, Provider} from 'react-redux'
import { fetchPositions as fetchPositionsAction } from '../actions'
import AppLayout from '../layout/AppLayout'

interface IProps {
  store: Store
  fetchPositions: () => void
}

class AppContainer extends Component<IProps> {
  componentDidMount() {
    const { fetchPositions } = this.props

    fetchPositions()
  }

  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <AppLayout />
      </Provider>
    )
  }
}

const mapActionToProps = {
  fetchPositions: fetchPositionsAction
}

export default connect(null, mapActionToProps)(AppContainer)
