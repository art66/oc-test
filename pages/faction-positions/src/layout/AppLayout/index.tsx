import React from 'react'
import { connect } from 'react-redux'
import s from './index.cssmodule.scss'
import { IReduxState } from '../../interfaces'
import Permissions from '../../components/Permissions'
import PositionsList from '../../components/PositionsList'
import '../../styles/vars.scss'
import '../../styles/DM_vars.scss'

interface IProps {
  loading: boolean
}

class AppLayout extends React.PureComponent<IProps> {
  render() {
    const { loading } = this.props

    // use legacy preloader here to sync with tabs preloader on faction controls page
    if (loading) {
      return <img src='/images/v2/main/ajax-loader.gif' className='ajax-placeholder m-top10 m-bottom10' />
    }

    return (
      <div className={s.permissionsContentWrapper}>
        <PositionsList />
        <Permissions />
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => ({
  loading: state.data.loading
})

export default connect(mapStateToProps)(AppLayout)
