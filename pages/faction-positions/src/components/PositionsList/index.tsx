import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import s from './index.cssmodule.scss'
import { IReduxState, ICurrentUser, IPositions } from '../../interfaces'
import { removePosition as removePositionAction } from '../../actions'
import NewPosition from './NewPosition'
import Position from './Position'

interface IProps {
  currentUser: ICurrentUser
  positions: IPositions
  positionsOrder: number[]
  defaultPosition: keyof IPositions
  loading: number[]
  removePosition: (id: number) => void
}

interface IState {
  removeDialog: {
    active: boolean
    positionID: keyof IPositions
  }
}

class ApplicationList extends PureComponent<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      removeDialog: {
        active: false,
        positionID: null
      }
    }
  }

  _handleRemoveClick = (positionID: number): void => {
    this.setState({ removeDialog: { active: true, positionID } })
  }

  _handleRemoveConfirm = (): void => {
    const { removePosition } = this.props
    const { removeDialog: { positionID } } = this.state

    removePosition(positionID)
    this.setState({ removeDialog: { active: false, positionID: null } })
  }

  _handleRemoveDecline = (): void => {
    this.setState({ removeDialog: { active: false, positionID: null } })
  }

  renderRemoveWarning = (label: string, membersAmount: number) => {
    const plural = membersAmount !== 1 ? 's' : ''

    return (
      <span>
        {`${membersAmount} user${plural} with `}
        <strong>{label}</strong> position will become regular <strong>{`Member${plural}`}</strong>.
      </span>
    )
  }

  renderRemoveDialog = (): React.ReactNode => {
    const { positions } = this.props
    const { removeDialog: { positionID } } = this.state
    const { label, membersAmount } = positions[positionID]
    const membersExist = membersAmount > 0

    return (
      <div className={cn(s.removePositionDialog, { [s.withWarning]: membersExist })}>
        <div className={s.removePositionMessage} role='alert'>
          <span>{`Are you sure you would like to delete the '${label}' position?`}</span>
          {membersExist && this.renderRemoveWarning(label, membersAmount)}
        </div>
        <span className={s.optionButtonsWrapper}>
          <button
            type='button'
            autoFocus={true}
            className={s.optionButtons}
            onClick={this._handleRemoveConfirm}
          >
            Yes
          </button>
          <button
            type='button'
            className={s.optionButtons}
            onClick={this._handleRemoveDecline}
          >
            No
          </button>
        </span>
      </div>
    )
  }

  renderPositions = (): React.ReactNodeArray => {
    const { positions, positionsOrder, defaultPosition, loading } = this.props
    const { removeDialog } = this.state

    return positionsOrder.map(positionID => (
      <Position
        key={positionID}
        positionID={positionID}
        position={positions[positionID]}
        isDefault={positionID === defaultPosition}
        isLoading={loading.includes(positionID)}
        isDeleting={positionID === removeDialog.positionID}
        onRemoveClick={this._handleRemoveClick}
      />
    ))
  }

  render() {
    const { currentUser: { permissions } } = this.props
    const { removeDialog } = this.state

    return permissions.managePositions ? (
      <div className={s.positionsListWrapper}>
        <NewPosition />
        {this.renderPositions()}
        {removeDialog.active && this.renderRemoveDialog()}
      </div>
    ) : null
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    currentUser: state.data.currentUser,
    positions: state.data.positions,
    positionsOrder: state.data.positionsOrder,
    loading: state.ui.loading.positions,
    defaultPosition: state.data.defaultPosition
  }
}

const mapActionsToProps = {
  removePosition: removePositionAction
}

export default connect(mapStateToProps, mapActionsToProps)(ApplicationList)
