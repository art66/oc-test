import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import s from './index.cssmodule.scss'
import positionsStyles from '../../../styles/positionsStyles.cssmodule.scss'
import { IReduxState, IValidationError } from '../../../interfaces'
import { KEY_CODE_ENTER } from '../../../constants'
import { createPosition as createPositionAction } from '../../../actions'
import CircularSpinner from '../../CircularSpinner'

interface IProps {
  errors: IValidationError[]
  isLoading: boolean
  newPositionNameBuffer: string
  validation: { positionNameMaxLength: number }
  createPosition: (label: string) => void
}

interface IState {
  label: string
}

class ApplicationList extends PureComponent<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = {
      label: ''
    }
  }

  componentDidUpdate(prevProps: Readonly<IProps>) {
    const { newPositionNameBuffer } = this.props

    if (prevProps.newPositionNameBuffer && !newPositionNameBuffer) {
      this._clearLabel()
    }
  }

  _clearLabel = (): void => {
    this.setState({ label: '' })
  }

  _handleEnterClick = (e: React.KeyboardEvent<HTMLInputElement>): void => {
    if (e.keyCode === KEY_CODE_ENTER) {
      this._handlePositionCreate()
    }
  }

  _handleLabelChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ label: e.target.value })
  }

  _handlePositionCreate = (): void => {
    const { createPosition, isLoading } = this.props
    const { label } = this.state

    !isLoading && createPosition(label)
  }

  _getError = () => this.props.errors.find(err => err.type === 'new')

  _renderError = (): React.ReactElement => {
    const error = this._getError()

    return error && (
      <div role='alert' className={positionsStyles.errorTooltip}>{error?.message}</div>
    )
  }

  _renderIconOrLoader = (): React.ReactElement => {
    const { isLoading } = this.props

    return isLoading ? (
      <CircularSpinner />
    ) : (
      <SVGIconGenerator
        iconName='PlusWide'
        iconsHolder={iconsHolder}
        preset={{ type: 'faction', subtype: 'FACTION_POSITIONS_LIST_ADD' }}
      />
    )
  }

  render() {
    const { validation: { positionNameMaxLength }, isLoading } = this.props
    const { label } = this.state

    return (
      <div
        className={
          cn(positionsStyles.positionWrapper, s.newPositionWrapper, { [positionsStyles.error]: this._getError() })
        }
      >
        {this._renderError()}
        <button
          type='button'
          aria-label='Create position'
          className={cn(positionsStyles.iconWrapper, { [positionsStyles.loading]: isLoading })}
          onClick={this._handlePositionCreate}
        >
          {this._renderIconOrLoader()}
        </button>
        <input
          type='text'
          placeholder='Create position to assign permissions'
          aria-label='Type new position name here'
          maxLength={positionNameMaxLength}
          className={positionsStyles.inputField}
          value={label}
          onChange={this._handleLabelChange}
          onKeyDown={this._handleEnterClick}
          data-lpignore='true'
        />
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    errors: state.ui.validationErrors,
    newPositionNameBuffer: state.ui.newPositionNameBuffer,
    validation: state.data.validation,
    isLoading: state.ui.loading.newPosition
  }
}

const mapActionsToProps = {
  createPosition: createPositionAction
}

export default connect(mapStateToProps, mapActionsToProps)(ApplicationList)
