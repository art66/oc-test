// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'globalSvgShadow': string;
  'hide': string;
  'newPositionWrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
