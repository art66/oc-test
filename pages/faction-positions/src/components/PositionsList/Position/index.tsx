import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import positionsStyles from '../../../styles/positionsStyles.cssmodule.scss'
import s from './index.cssmodule.scss'
import { IPosition, IReduxState, IValidationError } from '../../../interfaces'
import { KEY_CODE_ENTER } from '../../../constants'
import { editPosition as editPositionAction } from '../../../actions'
import { removeValidationError } from '../../../actions/ui'
import CircularSpinner from '../../CircularSpinner'

interface IProps {
  errors: IValidationError[]
  isLoading: boolean
  position: IPosition
  positionID: number
  isDefault: boolean
  isDeleting: boolean
  validation: { positionNameMaxLength: number }
  editPosition: (positionID: number, label: string) => void
  onRemoveClick: (positionID: number) => void
  hideError: (id: string) => void
}

interface IState {
  isEditing: boolean
  labelInputValue: string
}

class Position extends PureComponent<IProps, IState> {
  constructor(props) {
    super(props)

    this.state = {
      isEditing: false,
      labelInputValue: ''
    }
  }

  componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>): void {
    const { position, positionID, errors } = this.props
    const hadError = errors.find(err => +err.type === positionID)

    if (position.label !== prevProps.position.label && !hadError && prevState.isEditing) {
      this._resetInput()
    }
  }

  _getIconPreset = (): { type: string; subtype: string } => {
    const { isDeleting, isDefault } = this.props
    const { isEditing } = this.state
    const checkEdit = isEditing ? 'DEFAULT' : 'REMOVE'
    const checkDeletion = isDeleting ? 'REMOVE_ACTIVE' : checkEdit
    const disabled = isDefault && !isEditing

    return {
      type: 'faction',
      subtype: disabled ? 'REMOVE_DISABLED' : checkDeletion
    }
  }

  _resetInput = (): void => {
    setTimeout(() => {
      this.setState({ isEditing: false, labelInputValue: '' })
    }, 0)
  }

  _handleEnterClick = (e: React.KeyboardEvent<HTMLInputElement>): void => {
    if (e.keyCode === KEY_CODE_ENTER) {
      this._handleEditClick()
    }
  }

  _handleLabelClick = (): void => {
    const {
      position: { label }
    } = this.props

    this.setState({ isEditing: true, labelInputValue: label })
  }

  _handleLabelInputChange = ({ target }: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ labelInputValue: target.value })
  }

  _handleRemoveClick = (): void => {
    const { positionID, onRemoveClick, isLoading } = this.props

    !isLoading && onRemoveClick(positionID)
  }

  _handleEditClick = (): void => {
    const { positionID, position, editPosition, hideError, isLoading } = this.props
    const { labelInputValue } = this.state
    const inputValue = labelInputValue.replace(/\s{2,}/g, ' ').trim()

    if (isLoading) return

    if (position.label === inputValue) {
      hideError(String(positionID))
      this._resetInput()
    } else {
      editPosition(positionID, inputValue)
    }
  }

  renderIconOrLoader = (): React.ReactElement => {
    const { isLoading } = this.props
    const { isEditing } = this.state

    return isLoading ? (
      <CircularSpinner />
    ) : (
      <SVGIconGenerator
        iconName={isEditing ? 'Checked' : 'Close'}
        iconsHolder={iconsHolder}
        preset={this._getIconPreset()}
      />
    )
  }

  renderButton = (): React.ReactNode => {
    const {
      isDefault,
      isLoading,
      position: { label }
    } = this.props
    const { isEditing, labelInputValue } = this.state
    const styles = cn({
      [positionsStyles.iconWrapper]: true,
      [positionsStyles.disabled]: isDefault && !isEditing,
      [positionsStyles.loading]: isLoading
    })

    return (
      <button
        type='button'
        aria-label={isEditing ? `Confirm editing ${label} to ${labelInputValue}` : `Remove ${label} position`}
        className={styles}
        disabled={isDefault && !isEditing}
        onClick={isEditing ? this._handleEditClick : this._handleRemoveClick}
      >
        {this.renderIconOrLoader()}
      </button>
    )
  }

  renderError = (): React.ReactNode => {
    const { errors, positionID } = this.props
    const { isEditing } = this.state
    const error = errors.find(err => +err.type === positionID)

    return (
      isEditing
      && error && (
        <div role='alert' className={positionsStyles.errorTooltip}>
          {error?.message}
        </div>
      )
    )
  }

  renderLabel = (): React.ReactNode => {
    const {
      position: { label, membersAmount },
      validation: { positionNameMaxLength }
    } = this.props
    const { isEditing, labelInputValue } = this.state

    return isEditing ? (
      <>
        {this.renderError()}
        <input
          className={cn(positionsStyles.inputField, s.positionInput)}
          maxLength={positionNameMaxLength}
          value={labelInputValue}
          onChange={this._handleLabelInputChange}
          onKeyDown={this._handleEnterClick}
        />
      </>
    ) : (
      <button
        type='button'
        aria-label={`${label} position. Click to edit`}
        className={s.positionLabel}
        onClick={this._handleLabelClick}
      >
        <span className={s.textBold}>{label}</span>
        {membersAmount > 0 && ` (${membersAmount})`}
      </button>
    )
  }

  render() {
    return (
      <div className={positionsStyles.positionWrapper}>
        {this.renderButton()}
        {this.renderLabel()}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState, ownProps: Partial<IProps>) => {
  return {
    errors: state.ui.validationErrors,
    validation: state.data.validation,
    isLoading: state.ui.loading.positions.includes(ownProps.positionID)
  }
}

const mapActionsToProps = {
  editPosition: editPositionAction,
  hideError: removeValidationError
}

export default connect(mapStateToProps, mapActionsToProps)(Position)
