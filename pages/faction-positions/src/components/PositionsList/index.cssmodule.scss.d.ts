// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'globalSvgShadow': string;
  'optionButtons': string;
  'optionButtonsWrapper': string;
  'positionsListWrapper': string;
  'removePositionDialog': string;
  'removePositionMessage': string;
  'withWarning': string;
}
export const cssExports: CssExports;
export default cssExports;
