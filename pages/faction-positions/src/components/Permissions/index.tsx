import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import s from './index.cssmodule.scss'
import { IPosition, IReduxState } from '../../interfaces'
import PermissionsColumn from './PermissionsColumn'
import PermissionsPositions from './PermissionsPositions'

interface IProps {
  defaultPosition: IPosition
  tableScrollable: boolean
}

class Permissions extends PureComponent<IProps> {
  render() {
    const { defaultPosition, tableScrollable } = this.props

    return (
      <div className={s.permissionsWrapper}>
        <div className={s.recruitsMessage}>
          <strong>Recruits</strong> will receive the <strong>{defaultPosition?.label}</strong> position
          <strong> 72 hours</strong> after joining the faction
        </div>
        <div className={s.permissionsTableWrapper}>
          {tableScrollable && <div className={s.positionsTableBackground} />}
          <PermissionsColumn />
          <PermissionsPositions />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    tableScrollable: state.ui.tableScrollable,
    defaultPosition: state.data.positions[state.data.defaultPosition]
  }
}

export default connect(mapStateToProps)(Permissions)
