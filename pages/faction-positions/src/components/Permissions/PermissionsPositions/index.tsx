import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import globalStyles from '../../../styles/global.cssmodule.scss'
import s from './index.cssmodule.scss'
import { POSITIONS_TO_TOGGLE_MAX_WIDTH, MAX_WIDTH_FULL, MAX_WIDTH_TRUNCATED } from '../../../constants'
import { IReduxState, IPermissions, IPositions, ICurrentUser } from '../../../interfaces'
import { setScrollableState as setScrollableStateAction } from '../../../actions/ui'
import Cell from './Cell'

interface IProps {
  currentUser: ICurrentUser
  permissions: IPermissions
  positions: IPositions
  positionsOrder: number[]
  scrollable: boolean
  mediaType: string
  loading: string[]
  setScrollableState: (value: boolean) => void
}

interface IState {
  isScrolling: boolean
}

class Permissions extends PureComponent<IProps, IState> {
  private _tableRef: React.RefObject<HTMLDivElement>
  private _startX: number
  private _scrollLeft: number

  constructor(props) {
    super(props)

    this._tableRef = React.createRef()
    this._startX = 0
    this._scrollLeft = 0

    this.state = {
      isScrolling: false
    }
  }

  componentDidMount() {
    this.checkScrollableState(this.props)
  }

  componentDidUpdate(prevProps: Readonly<IProps>): void {
    this.checkScrollableState(prevProps, true)
  }

  checkScrollableState = (props: IProps, checkMedia = false): void => {
    const { scrollable, mediaType } = props
    const { mediaType: newMediaType, setScrollableState } = this.props
    const newScrollable: boolean = this._tableRef.current.scrollWidth > this._tableRef.current.offsetWidth
    const isChanged: boolean = newScrollable !== scrollable

    if ((!checkMedia && isChanged) || (checkMedia && mediaType !== newMediaType || isChanged)) {
      setScrollableState(newScrollable)
    }
  }

  setScrollingState = (value: boolean): void => {
    this.setState({ isScrolling: value })
  }

  _handleScrollStart = e => {
    const pageX = e.pageX || e.touches[0].pageX

    this.setScrollingState(true)
    this._startX = pageX - this._tableRef.current.offsetLeft
    this._scrollLeft = this._tableRef.current.scrollLeft
  }

  _handleScrollEnd = (): void => {
    this.setScrollingState(false)
  }

  _handleScroll = e => {
    const { isScrolling } = this.state

    if (!isScrolling) return

    !e.touches && e.preventDefault()

    const pageX = e.pageX || e.touches[0].pageX
    const x = pageX - this._tableRef.current.offsetLeft
    const walk = x - this._startX

    this._tableRef.current.scrollLeft = this._scrollLeft - walk
  }

  renderPositionsPermissionsCells = (positionID: number): React.ReactNodeArray => {
    const { currentUser, permissions, positions, loading } = this.props

    return Object.keys(permissions).map(permissionID => (
      <Cell
        key={permissionID}
        canChange={currentUser.permissions.changePermissions}
        isLoading={loading.includes(`${positionID}_${permissionID}`)}
        positionID={positionID}
        positionLabel={positions[positionID].label}
        permissionID={permissionID}
        permission={permissions[permissionID]}
        value={permissions[permissionID].positions[positionID]}
      />
    ))
  }

  renderPositionsPermissionsColumns = (): React.ReactNodeArray => {
    const { positionsOrder, positions } = this.props
    const columnMaxWidth = positionsOrder.length > POSITIONS_TO_TOGGLE_MAX_WIDTH ? MAX_WIDTH_TRUNCATED : MAX_WIDTH_FULL

    return positionsOrder.map(positionID => {
      const position = positions[positionID]
      const { label } = position
      const positionDomId = `position-${positionID}`

      return (
        <div
          key={positionID}
          style={{ maxWidth: columnMaxWidth }}
          className={s.tableColumn}
        >
          <Tooltip
            delay={0}
            parent={positionDomId}
          >
            {positions[positionID].label}
          </Tooltip>
          <div id={positionDomId} className={cn(globalStyles.positionsCell, s.headerCell)}>
            <span className={s.headerCellLabel}>{label}</span>
          </div>
          {this.renderPositionsPermissionsCells(positionID)}
        </div>
      )
    })
  }

  render() {
    const { scrollable } = this.props
    const { isScrolling } = this.state

    return (
      <div
        className={cn(s.positionsTable, isScrolling && s.scrolling, scrollable && s.scrollable)}
        ref={this._tableRef}
        onMouseDown={this._handleScrollStart}
        onMouseLeave={this._handleScrollEnd}
        onMouseMove={this._handleScroll}
        onMouseUp={this._handleScrollEnd}
        onTouchStart={this._handleScrollStart}
        onTouchMove={this._handleScroll}
        onTouchEnd={this._handleScrollEnd}
      >
        {this.renderPositionsPermissionsColumns()}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    currentUser: state.data.currentUser,
    permissions: state.data.permissions,
    mediaType: state.browser.mediaType,
    positions: state.data.positions,
    positionsOrder: state.data.positionsOrder,
    scrollable: state.ui.tableScrollable,
    loading: state.ui.loading.permissions
  }
}

const mapActionsToProps = {
  setScrollableState: setScrollableStateAction
}

export default connect(mapStateToProps, mapActionsToProps)(Permissions)
