import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import globalStyles from '../../../../styles/global.cssmodule.scss'
import s from './index.cssmodule.scss'
import { IPermission, ITogglePermission } from '../../../../interfaces'
import { togglePermission as togglePermissionAction } from '../../../../actions'
import CircularSpinner from '../../../CircularSpinner'

interface IProps {
  canChange: boolean
  isLoading: boolean
  permissionID: string
  positionID: number
  positionLabel: string
  permission: IPermission
  value: boolean
  togglePermission: (data: ITogglePermission) => void
}

class Cell extends PureComponent<IProps> {
  _handlePermissionToggle = (): void => {
    const { canChange, permissionID, positionID, value, togglePermission } = this.props

    if (!canChange) return

    const actionData = {
      permissionID: Number(permissionID),
      positionID: positionID,
      isAssigned: !value
    }

    togglePermission(actionData)
  }

  render() {
    const { canChange, isLoading, value, positionLabel, permission: { label, level } } = this.props

    if (isLoading) {
      return (
        <div className={globalStyles.positionsCell}>
          <CircularSpinner />
        </div>
      )
    }

    const preset = value ?
      `PERMISSION_CHECKBOX_LEVEL_${level.toUpperCase()}` :
      'PERMISSION_CHECKBOX_INACTIVE'
    const hoverPreset = value ?
      `PERMISSION_CHECKBOX_LEVEL_${level.toUpperCase()}_HOVER` :
      'PERMISSION_CHECKBOX_INACTIVE_HOVER'
    const ariaLabel = `${value ? 'Reject' : 'Apply'} permission ${label} for ${positionLabel} position`

    return (
      <div className={globalStyles.positionsCell}>
        <button
          type='button'
          aria-label={ariaLabel}
          disabled={!canChange}
          className={s.iconWrapper}
          onClick={this._handlePermissionToggle}
        >
          <SVGIconGenerator
            iconName={value ? 'Checked' : 'Close'}
            iconsHolder={iconsHolder}
            preset={{ type: 'faction', subtype: preset }}
            onHover={{ active: true, preset: { type: 'faction', subtype: hoverPreset } }}
          />
        </button>
      </div>
    )
  }
}

const mapActionsToProps = {
  togglePermission: togglePermissionAction
}

export default connect(null, mapActionsToProps)(Cell)
