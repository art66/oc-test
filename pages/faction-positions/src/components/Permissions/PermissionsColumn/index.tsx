import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Tooltip from '@torn/shared/components/Tooltip'
import s from './index.cssmodule.scss'
import { IReduxState, IPermissions } from '../../../interfaces'

interface IProps {
  permissions: IPermissions
}

class PermissionsColumn extends PureComponent<IProps> {
  renderPermissionsColumnCells = () => {
    const { permissions } = this.props

    return Object.keys(permissions).map(permissionID => {
      const { label, description, level } = permissions[permissionID]
      const id = `permission-${permissionID}`
      const tooltipStyles = {
        padding: '10px 15px',
        maxWidth: '200px',
        zIndex: 101,
        lineHeight: '14px',
        textAlign: 'center',
        color: 'var(--default-gray-6-color)'
      }

      return (
        <div
          key={permissionID}
          className={cn(s.permissionsColumnCell, s[`${level}PermissionLevel`])}
        >
          <Tooltip
            style={{ style: tooltipStyles }}
            parent={id}
          >
            {description}
          </Tooltip>
          <span id={id} className={s.permissionsColumnCellText}>{label}</span>
        </div>
      )
    })
  }

  render() {
    return (
      <div className={s.permissionsColumn}>
        <div className={s.permissionsColumnHeader}>Permissions</div>
        {this.renderPermissionsColumnCells()}
      </div>
    )
  }
}

const mapStateToProps = (state: IReduxState) => {
  return {
    permissions: state.data.permissions
  }
}

const mapActionsToProps = {

}

export default connect(mapStateToProps, mapActionsToProps)(PermissionsColumn)
