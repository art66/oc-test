import React from 'react'
import s from './index.cssmodule.scss'

const CircularSpinner: React.FC = () => {
  return (
    <div className={s.circularSpinner}>
      <div /><div /><div /><div /><div /><div /><div /><div /><div /><div /><div /><div />
    </div>
  )
}

export default CircularSpinner
