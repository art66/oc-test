import * as a from '../constants/actionTypes'
import { TAction } from '../interfaces/actions'
import { IReduxState, IPosition } from '../interfaces'

const initialState = {
  tableScrollable: false,
  validationErrors: [],
  newPositionNameBuffer: null,
  loading: {
    permissions: [],
    positions: [],
    newPosition: false
  }
}

const ACTION_HANDLERS = {
  [a.SET_SCROLLABLE_STATE]: (state: Readonly<IReduxState['ui']>, { payload }: TAction<boolean>) => {
    return {
      ...state,
      tableScrollable: payload
    }
  },
  [a.REMOVE_VALIDATION_ERROR]: (state: Readonly<IReduxState['ui']>, { payload }: TAction<string>) => {
    return {
      ...state,
      validationErrors: state.validationErrors.filter(err => err.type !== payload)
    }
  },
  [a.CREATE_POSITION]: (state: Readonly<IReduxState['ui']>, { payload }: TAction<string>) => {
    return {
      ...state,
      newPositionNameBuffer: payload,
      loading: { ...state.loading, newPosition: true }
    }
  },
  [a.CREATE_POSITION_SUCCESS]: (state: Readonly<IReduxState['ui']>, { payload }: TAction<IPosition>) => {
    return {
      ...state,
      validationErrors: state.validationErrors.filter(err => err.type !== 'new'),
      newPositionNameBuffer: payload.label === state.newPositionNameBuffer ? null : state.newPositionNameBuffer,
      loading: { ...state.loading, newPosition: false }
    }
  },
  [a.CREATE_POSITION_FAILURE]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      loading: { ...state.loading, newPosition: false },
      validationErrors: [{ type: 'new', message: payload.error }, ...state.validationErrors]
    }
  },
  [a.EDIT_POSITION]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      loading: {
        ...state.loading,
        positions: [...state.loading.positions, payload.id]
      }
    }
  },
  [a.EDIT_POSITION_SUCCESS]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      validationErrors: state.validationErrors.filter(err => +err.type !== payload.id),
      loading: {
        ...state.loading,
        positions: state.loading.positions.filter(el => el !== payload.id)
      }
    }
  },
  [a.EDIT_POSITION_FAILURE]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    const { position, error } = payload

    return {
      ...state,
      loading: {
        ...state.loading,
        positions: state.loading.positions.filter(el => el !== position.id)
      },
      validationErrors: [{ type: position.id, message: error }, ...state.validationErrors]
    }
  },
  [a.REMOVE_POSITION]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      loading: {
        ...state.loading,
        positions: [...state.loading.positions, payload]
      }
    }
  },
  [a.REMOVE_POSITION_SUCCESS]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      loading: {
        ...state.loading,
        positions: state.loading.positions.filter(el => el !== payload)
      }
    }
  },
  [a.REMOVE_POSITION_FAILURE]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    return {
      ...state,
      loading: {
        ...state.loading,
        positions: state.loading.positions.filter(el => el !== payload)
      }
    }
  },
  [a.TOGGLE_PERMISSION]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    const { positionID, permissionID } = payload

    return {
      ...state,
      loading: {
        ...state.loading,
        permissions: [...state.loading.permissions, `${positionID}_${permissionID}`]
      }
    }
  },
  [a.TOGGLE_PERMISSION_SUCCESS]: (state: Readonly<IReduxState['ui']>, { payload }) => {
    const { positionID, permissionID } = payload

    return {
      ...state,
      loading: {
        ...state.loading,
        permissions: state.loading.permissions.filter(el => el !== `${positionID}_${permissionID}`)
      }
    }
  }
}

const uiReducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default uiReducer
