import initialState from './initialState'
import { IReduxState } from '../interfaces'
import * as a from '../constants/actionTypes'

const ACTION_HANDLERS = {
  [a.FETCH_POSITIONS]: (state: Readonly<IReduxState['data']>) => {
    return {
      ...state,
      loading: true
    }
  },
  [a.FETCH_POSITIONS_SUCCESS]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      ...payload,
      loading: false
    }
  },
  [a.CREATE_POSITION_SUCCESS]: (state, { payload }) => {
    const { permissions, positions } = state
    const { id, ...positionData } = payload

    return {
      ...state,
      positions: {
        ...positions,
        [id]: {
          ...positionData,
          membersAmount: 0
        }
      },
      permissions: Object.keys(permissions).reduce((acc, key) => {
        const permission = permissions[key]

        return {
          ...acc,
          [key]: {
            ...permission,
            positions: {
              ...permission.positions,
              [id]: false
            }
          }
        }
      }, {}),
      positionsOrder: [...state.positionsOrder, id]
    }
  },
  [a.EDIT_POSITION_SUCCESS]: (state, { payload }) => {
    const { id, ...positionData } = payload

    return {
      ...state,
      positions: {
        ...state.positions,
        [id]: {
          ...state.positions[id],
          ...positionData
        }
      }
    }
  },
  [a.REMOVE_POSITION_SUCCESS]: (state, { payload }) => {
    const { permissions, positions, defaultPosition } = state
    const { [payload]: _, ...restPositions } = positions

    return {
      ...state,
      positions: {
        ...restPositions,
        [defaultPosition]: {
          ...positions[defaultPosition],
          membersAmount: positions[defaultPosition].membersAmount + positions[payload].membersAmount
        }
      },
      permissions: Object.keys(permissions).reduce((acc, key) => {
        const permission = permissions[key]
        const { [payload]: __, ...restPermissionPositions } = permission.positions

        return {
          ...acc,
          [key]: {
            ...permission,
            positions: restPermissionPositions
          }
        }
      }, {}),
      positionsOrder: state.positionsOrder.filter(pos => pos !== payload)
    }
  },
  [a.ASSIGN_POSITION]: (state, { payload }) => {
    const { positions } = state
    const increment = []
    const decrement = []

    payload.forEach(member => {
      increment.push(member.newPositionID)
      decrement.push(member.oldPositionID)
    })

    return {
      ...state,
      positions: Object.keys(positions).reduce((acc, key) => {
        let position = positions[key]

        if (increment.includes(+key)) {
          position = {
            ...position,
            membersAmount: position.membersAmount += 1
          }
        }

        if (decrement.includes(+key)) {
          position = {
            ...position,
            membersAmount: position.membersAmount -= 1
          }
        }

        return {
          ...acc,
          [key]: position
        }
      }, {})
    }
  },
  [a.KICK_MEMBER]: (state, { payload }) => {
    const { positions } = state

    return {
      ...state,
      positions: Object.keys(positions).reduce((acc, key) => {
        let position = positions[key]

        if (+key === payload) {
          position = {
            ...position,
            membersAmount: position.membersAmount -= 1
          }
        }

        return {
          ...acc,
          [key]: position
        }
      }, {})
    }
  },
  [a.TOGGLE_PERMISSION_SUCCESS]: (state, { payload }) => {
    const { positionID, permissionID, isAssigned } = payload

    return {
      ...state,
      permissions: {
        ...state.permissions,
        [permissionID]: {
          ...state.permissions[permissionID],
          positions: {
            ...state.permissions[permissionID].positions,
            [positionID]: isAssigned
          }
        }
      }
    }
  },
  [a.UPDATE_CURRENT_USER_PERMISSIONS]: (state: Readonly<IReduxState['data']>, { payload }) => {
    return {
      ...state,
      currentUser: {
        ...state.currentUser,
        permissions: {
          ...state.currentUser.permissions,
          ...payload
        }
      }
    }
  }
}

const rootReducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default rootReducer
