export type TPermissionsLevel = 'low' | 'mid' | 'high' | 'default'

export interface ICurrentUser {
  permissions: {
    changePermissions: boolean
    managePositions: boolean
  }
  settings: {
    showImages: boolean
  }
}

export interface IPosition {
  label: string
  membersAmount: number
}

export interface IPositions {
  [key: number]: IPosition
}

export type TPermissionPositions = {
  [K in keyof IPermissions]: boolean
}

export interface IPermission {
  label: TPermissionsLevel
  description: string
  level: TPermissionsLevel
  positions: TPermissionPositions
}

export interface IPermissions {
  [key: number]: IPermission
}

export interface ITogglePermission {
  positionID: number
  permissionID: number
  isAssigned: boolean
}

export interface IValidationError {
  type: string
  message: string
}

export interface IReduxState {
  data: {
    currentUser: ICurrentUser
    positions: IPositions
    positionsOrder: number[]
    defaultPosition: keyof IPositions
    permissions: IPermissions
    loading: boolean
    validation: {
      positionNameMaxLength: number
    }
  }
  ui: {
    tableScrollable: boolean
    validationErrors: IValidationError[]
    loading: {
      permissions: string[]
      positions: number[]
      newPosition: boolean
    }
    newPositionNameBuffer: string
  }
  browser: {
    mediaType: string
  }
}
