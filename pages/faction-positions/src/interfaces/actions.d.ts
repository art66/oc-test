export type TAction<T> = {
  type: string
  payload: T
}

export type TTogglePermissionPayload = {
  positionID: number
  permissionID: number
  isAssigned: boolean
}

export type TAssignPositionPayload = {
  userID: number
  newPositionID: number
  oldPositionID: number
}[]

export type TUpdateCurrentUserPayload = {
  permissions: {
    changePermissions?: boolean
    managePositions?: boolean
    kick?: boolean
  }
}
