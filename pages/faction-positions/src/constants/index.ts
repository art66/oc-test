export const MAX_LABEL_COUNT = 6
export const POSITIONS_TO_TOGGLE_MAX_WIDTH = 4
export const MAX_WIDTH_FULL = '100%'
export const MAX_WIDTH_TRUNCATED = '100px'
export const KEY_CODE_ENTER = 13
