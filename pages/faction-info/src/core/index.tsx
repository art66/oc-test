import React, { Component } from 'react'
import { Provider } from 'react-redux'

import AppLayout from '../layout'

import { IProps } from './interfaces'

// Our main container for app handling.
// We're using react-router-dom and connected-react-router package for SPA react-redux routing
class AppContainer extends Component<IProps> {
  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <AppLayout />
      </Provider>
    )
  }
}

export default AppContainer
