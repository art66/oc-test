import { getCookie } from '@torn/shared/utils'
import { FACTION_DESCRIPTION_COOKIE_NAME } from '../constants'

export const isFactionDescriptionOpened = () => getCookie(FACTION_DESCRIPTION_COOKIE_NAME) === 'true'
