import React, { Component } from 'react'

import FactionMembers from '@torn/faction-wars/src/components/FactionMembers'
import FactionDescription from '@torn/faction-wars/src/components/FactionDescription'

import { toggleFactionDescription } from '../../controller/actions/common'

import { IProps } from './interfaces'

class FactionInfo extends Component<IProps> {
  render() {
    const { mediaType, factionInfo } = this.props

    if (!factionInfo) {
      return null
    }

    const { members, faction, currentUser } = factionInfo

    return (
      <>
        <FactionDescription
          mediaType={mediaType}
          description={factionInfo.description}
          toggleFactionDescription={toggleFactionDescription}
        />
        <FactionMembers
          mediaType={mediaType}
          factionMembers={members}
          factionCapacity={faction.capacity}
          factionSize={faction.size}
          showImages={currentUser.settings.showImages}
          faction={{
            factionID: faction.id,
            factionTag: faction.tag,
            factionRank: faction.rank,
            factionTagImageUrl: faction.tagImageUrl
          }}
        />
      </>
    )
  }
}

export default FactionInfo
