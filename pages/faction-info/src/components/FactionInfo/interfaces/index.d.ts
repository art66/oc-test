import { IFactionInfo } from '../../../interfaces/IFactionInfo'

export interface IProps {
  mediaType: 'desktop' | 'tablet' | 'mobile'
  factionInfo: IFactionInfo
}
