import { IInitialState } from '../interfaces/IStore'

const initialState: IInitialState = {
  isDesktopManualLayout: false,
  factionInfo: null
}

export default initialState
