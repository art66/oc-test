import { all } from 'redux-saga/effects'

import fetchFactionProfileInfo from '../../controller/sagas'

export default function* rootSaga() {
  yield all([fetchFactionProfileInfo()])
}
