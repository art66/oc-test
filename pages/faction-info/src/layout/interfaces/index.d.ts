import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import { IInitialState } from '../../interfaces/IStore'

export interface IProps extends IInitialState {
  checkManualMode: (status: boolean) => void
  fetchFactionProfileInfo: () => void
  mediaType: TMediaType
}

export interface IContainerStore {
  app: IInitialState
  browser: any
}
