import React from 'react'
import { Dispatch, Store } from 'redux'
import { connect } from 'react-redux'

import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'

import FactionInfo from '../components/FactionInfo'

import { checkManualDesktopMode, fetchFactionProfileInfo } from '../controller/actions/common'
import { IProps, IContainerStore } from './interfaces'

import styles from './index.cssmodule.scss'
import '../styles/global.cssmodule.scss'

class AppLayout extends React.PureComponent<IProps> {
  componentDidMount() {
    this._checkManualDesktopMode()

    this.props.fetchFactionProfileInfo()
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  _checkManualDesktopMode = () => {
    const { checkManualMode } = this.props

    subscribeOnDesktopLayout((payload: boolean) => checkManualMode(payload))
  }

  _renderBody = () => {
    const { mediaType, factionInfo } = this.props

    return (
      <div className={styles.bodyWrap}>
        <FactionInfo mediaType={mediaType} factionInfo={factionInfo} />
      </div>
    )
  }

  render() {
    return <div className={styles.appWrap}>{this._renderBody()}</div>
  }
}

const mapStateToProps = (state: IContainerStore & Store<any>) => ({
  factionInfo: state.app.factionInfo,
  mediaType: state.browser.mediaType
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  checkManualMode: (status: boolean) => dispatch(checkManualDesktopMode({ status })),
  fetchFactionProfileInfo: () => dispatch(fetchFactionProfileInfo())
})

export default connect(mapStateToProps, mapDispatchToState)(AppLayout)
