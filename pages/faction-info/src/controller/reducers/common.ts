import initialState from '../../store/initialState'

import { isFactionDescriptionOpened } from '../../utils'

import { IType } from '../../interfaces/IController'
import { IInitialState } from '../../interfaces/IStore'

import * as a from '../../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [a.SET_FACTION_PROFILE_INFO]: (state: IInitialState, action) => {
    return {
      ...state,
      factionInfo: {
        ...action.factionInfo,
        description: {
          text: action.factionInfo.faction.description,
          opened: isFactionDescriptionOpened()
        }
      }
    }
  },
  [a.SET_FACTION_DESCRIPTION_STATE]: (state: IInitialState, action) => {
    return {
      ...state,
      factionInfo: {
        ...state.factionInfo,
        description: {
          ...state.factionInfo.description,
          opened: action.opened
        }
      }
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
