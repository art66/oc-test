import * as a from '../../constants'
import { IManualDesktopStatus } from '../../interfaces/IController'

export const checkManualDesktopMode = ({ status }: { status: boolean }): IManualDesktopStatus => ({
  type: a.MANUAL_DESKTOP_MODE,
  status
})

export const fetchFactionProfileInfo = () => ({
  type: a.FETCH_FACTION_PROFILE_INFO
})

export const setFactionProfileInfo = factionInfo => ({
  type: a.SET_FACTION_PROFILE_INFO,
  factionInfo
})

export const toggleFactionDescription = () => ({
  type: a.TOGGLE_FACTION_DESCRIPTION
})

export const setFactionDescriptionState = opened => ({
  type: a.SET_FACTION_DESCRIPTION_STATE,
  opened
})
