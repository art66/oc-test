// --------------------------
// REDUX NAMESPACES
// --------------------------
export const MEDIA_SCREEN_CHANGED = 'redux-responsive/CALCULATE_RESPONSIVE_STATE'
export const ROUTE_HEADER_SET = 'ROUTE_HEADER_SET'
export const MANUAL_DESKTOP_MODE = 'MANUAL_DESKTOP_MODE'
export const FETCH_FACTION_PROFILE_INFO = 'FETCH_FACTION_PROFILE_INFO'
export const SET_FACTION_PROFILE_INFO = 'SET_FACTION_PROFILE_INFO'
export const TOGGLE_FACTION_DESCRIPTION = 'TOGGLE_FACTION_DESCRIPTION'
export const SET_FACTION_DESCRIPTION_STATE = 'SET_FACTION_DESCRIPTION_STATE'

// --------------------------
// APP HEADER PATHS IDs
// --------------------------
export const PATHS_ID = {
  '': 1,
  someSubRoute: 2
}

export const FACTION_DESCRIPTION_COOKIE_NAME = 'openedFactionDescription'
