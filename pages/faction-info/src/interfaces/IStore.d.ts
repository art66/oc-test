import IFactionInfo from './IFactionInfo'

export interface IInitialState {
  isDesktopManualLayout?: any
  factionInfo: IFactionInfo
}
