export interface IFaction {
  id: number
  capacity: number
  description: string
  rank: string
  tag: string
  tagImageUrl: string
}
