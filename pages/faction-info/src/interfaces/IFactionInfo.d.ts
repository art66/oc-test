import IFaction from './IFaction'
import { IProfileMembersListMember } from '@torn/faction-wars/src/interfaces/IMember'

export interface IFactionInfo {
  members: IProfileMembersListMember[]
  faction: IFaction
  description: {
    text: string
    opened: boolean
  }
  currentUser: {
    settings: {
      showImages: boolean
    }
  }
}
