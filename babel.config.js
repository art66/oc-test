module.exports = {
  presets: ['@babel/preset-react', '@babel/preset-env', '@babel/preset-typescript'],
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }], // storybook requirement
    ['@babel/plugin-proposal-private-methods', { loose: true }], // storybook requirement
    ['@babel/plugin-proposal-private-property-in-object', { loose: true }], // storybook requirement
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-transform-async-to-generator',
    '@babel/plugin-syntax-optional-catch-binding',
    '@babel/plugin-transform-modules-commonjs',
    '@babel/plugin-transform-object-assign',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-runtime'
  ],
  env: {
    test: {
      plugins: ['@babel/plugin-transform-runtime', 'dynamic-import-node']
    }
  }
}
