// global variables for webpack and app hot-reload. Makes work more easy.

global.__TEST__ = process.env.NODE_ENV === 'test'
global.__DEV__ = process.env.NODE_ENV === 'development'
global.__PROD__ = process.env.NODE_ENV === 'production'
global.__NODE_ENV__ = process.env.NODE_ENV
global.__PORT__ = process.env.PORT
global.__CT_CHUNK_FIX__ = process.env.WEBPACK_CHUNKS_CYCLE_FIX

module.exports = {
  __TEST__: global.__TEST__,
  __DEV__: global.__DEV__,
  __PROD__: global.__PROD__,
  __NODE_ENV__: global.__NODE_ENV__,
  __PORT__: global.__PORT__,
  __CT_CHUNK_FIX__: global.__CT_CHUNK_FIX__
}
