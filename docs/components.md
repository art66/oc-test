# Components

## Ticker() - higher order component

`import { Ticker } from '@torn/shared/hoc'`   

Can be used as a cowntdown (or up), it basically updates one chosen property by a specific rule.

**Examples:**

`export default Ticker(ContentTitle, { iteratedProperty: 'timeLeft', end: 0 })`

Here `ContentTitle` is a component whose `timeLeft` property will be updated once per second from initial value till 0.

___

`export default Ticker(Log, { operation: x => x + 1, iteratedProperty: 'timePassed', start: 0 })`

`Log`'s `timePassed` property will be increased, cause the `operation` is provided `x + 1`, from 0 infinitely
___



## LoadingIndicator() - higher order component

`import { LoadingIndicator } from '@torn/shared/hoc'`   

Can be used as a to show "loading dots" (or potentially any other loading animation).   
To trigger loading state pass `loading` prop to component.   
Accepts Wrapped component and options as arguments.   
Options include `className` and `inheritClass` arguments.   
Calculated `className` will be passed to loading indicator component

**Example:**

`export default LoadingIndicator(WeaponBox, { inheritClass: true, className: s.weponBox })`


___
