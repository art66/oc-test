account_recovery:
 url: `https://dev-www.torn.com/account_recovery.php#/`
 status: `OK!`

account_closed:
 url: `GLOBAL_URL_ONCE_BANNED`
 status: `OK!`

ammo:
 url: `https://dev-www.torn.com/page.php?sid=ammo`
 status: `OK!`

attack:
 url: `https://dev-www.torn.com/loader.php?sid=attack&user2ID=1414133`
 status: `OK!`

bazzar:
 url: `https://dev-www.torn.com/bazaar.php`
 status: `OK!`

betapi:
 url: `https://dev-www.torn.com/bookies.php#/baseball`
 status: `OK!`

calendar:
 url: https://dev-www.torn.com/calendar.php
 status: `OK!`

campaign-tracker:
 url: https://dev-www.torn.com/campaignTracker.php?password=trn928nf
 status: `OK!`

chain-report:
 url: `https://dev-www.torn.com/report.php`
 status: `OK!`

christmastown:
 url: `https://dev-www.torn.com/christmas_town.php`
 status: `OK!`

christmastown_mymaps:
 url: `https://dev-www.torn.com/christmas_town.php#/mymaps`
 status: `OK!`

christmastown_allmaps:
 url: `https://dev-www.torn.com/christmas_town.php#/allmaps`
 status: `OK!`

christmastown_params:
 url: `https://dev-www.torn.com/christmas_town.php#/parametereditor`
 status: `OK!`

christmastown_editor:
 url: `https://dev-www.torn.com/christmas_town.php#/mapeditor`
 status: `OK!`

christmastown_npc:
 url: `https://dev-www.torn.com/christmas_town.php#/npceditor`
 status: `OK!`

chronicles_hub:
 url: `https://dev-www.torn.com/chronicles.php`
 status: `OK!`

contract:
 url: `??????`
 status: `OK!`

crimes:
 url: `https://dev-www.torn.com/loader.php?sid=crimes#/searchforcash`
 status: `OK`

crimes-editor:
 url: `https://dev-www.torn.com/loader.php?sid=manageCrimes`
 status: `OK!`

crimes-face-editor:
 url: `https://dev-www.torn.com/loader.php?sid=manageCrimesTargets#/`
 status: `OK`

faction-wars:
 url: `https://dev-www.torn.com/factions.php?step=your`
 status: `OK`

gym:
 url: `https://dev-www.torn.com/gym.php`
 status: `OK`

holdem:
 url: `https://dev-www.torn.com/loader.php?sid=holdem`
 status: `OK`

hud-tutorial:
 url: `https://dev-www.torn.com/hudtutorial.php#`
 status: 'NULL!'

ics:
 url: `https://dev-www.torn.com/ics.php`
 status: 'OK!'

items:
 url: `https://dev-www.torn.com/item.php`
 status: 'OK!'

models-editor:
 url: `https://dev-www.torn.com/loader.php?sid=modelsEditor`
 status: 'OK!'

newspaper:
 url: `https://dev-www.torn.com/newspaper.php`
 status: 'OK!'

personal-stats:
 url: `https://dev-www.torn.com/personalstats.php?ID=2072301&stat=useractivity&from=1%20month`
 status: 'OK!'

preferences:
 url: `https://dev-www.torn.com/preferences.php`
 status: `OK!`

profile:
 url: `https://dev-www.torn.com/profiles.php?XID=2114355#/`
 status: `OK!`

profile-mini:
 url: `GLOBAL`
 status: `OK!`

puzzledevice:
 url: `https://dev-www.torn.com/loader.php?sid=missionsPuzzleDevice`
 status: `OK!`

questionnaire:
 url: `https://dev-www.torn.com/loader.php?sid=missionsQuestionnaire`
 error: `OK`

reporting:
 url: `https://dev-www.torn.com/playerreport.php?step=add&userID=1046304`
 status: `OK!`

sidebar:
 url: `GLOBAL_URL!`
 status: `OK!`

status-monitor:
 url: `https://dev-www.torn.com/status_monitor.php?password=trn928nf`
 status: `OK!`

token-shop:
 url: `https://dev-www.torn.com/token_shop.php`
 status: `OK!`
