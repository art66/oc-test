#How to connect to the Dev Remote Workspace (/torncity folder) on Mac

This guide is aimed to help torn-devs who have some problems with standart 
connection to the Remove Torn-dev Worspace as I have.

So, if you cannot connect on your Mac to `/torcity` folder on Dev Workspace using `ssh torn` command in terminal 
with standart Torn-dev connection configuration (`.conf`) file in `~/.ssh` folder:

    Host torn
      HostName 89.145.73.200
      IdentityFile ~/.ssh/id_rsa
      User USER_NAME
      LocalForward 1111 admin.torn.com:80
      LocalForward 2222 dev:22
      LocalForward 3333 10.99.99.151:27017

In that case you need to make an additional connection to the dev server 
in another terminal (the previous one with established connection must be still opened) 
with the next command: `ssh -p 2222 USER_NAME@127.0.0.1` and then put `cd /torncity` there. 
And whoala! You are already connected to the `/torncity` folder on dev.

Enjoy!

