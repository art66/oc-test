## Unit test in Torns JS|JS|TS|TSX files

 * On torn platrorm we uses Jest + Enzyme test coupling bomb, which provide us with maximum  flexability and perfomance in JS|JS|TS|TSX test suits.

 * Links:
  JEST - https://jestjs.io/docs/en/
  ENZYME - http://airbnb.io/enzyme/docs/

 # How to run a specific test for some file?
  - To do this, you can simply run `jest [you_file_name]` from root directory or specific app folder

 # How to run tests for specific dir?
  - To make it you need to use regular expression in the root dir of react-apps in next way: `yarn jest pages/[your_app_dir]/**/*.test.js`

 # How to run a whole tests in Torns platform?
  - If you really want to do this you need to go in root folder and then run the command `yarn testall`. It will take a while, but it will run all tests in torns platform.

 # How to write the tests?
  - First of all you need to create the right structure in maximum clously place in range of target file to test. You check the example in `pages/crimes/src/routes/Bootlegging/components/__test__` folder. So, the basic structure is to create the folder `__test__` with two including folders `mocks` and `snapshots` where will be placed the mocks and snapshots files correspondently.
  - The basic example of how to write the test you can see in the same folder above in any of  the `*.test.js` file.

 # Which types of files we can put throw the Jest tests?
  - Anyone in a range of JS|JS|TS|TSX file types.

