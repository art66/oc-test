# React workflow

## Setup for Development

1. Start by cloning this repo

2. Install lerna globally `npm i lerna -g` and bootstrap your repo by running `lerna bootstrap` command from the root directory.

3. Create a file `config/boxConfig.js` and store your personal server url and remote home folder in following format
```javascript
module.exports = {
  boxURL: 'your_personal_server_url',
  proxyURL: 'your_personal_proxy_url',
  userFolderName: 'remote_home_folder'
}
```

<details>
<summary>
Sample
</summary>

```javascript
module.exports = {
  boxURL: 'https://harabara-dev.torn.com',
  proxyURL: 'https://harabara-front-dev.torn.com',
  userFolderName: 'harabara'
}
```
</details>

4. You can start your project from its folder by running `yarn start` you will get on the main page at `https://localhost:3000` or on your personal dev box (if proxy is configured and docker container is run according to [Proxy setup](#proxy-setup)).
   By default all requests are proxied to the server specified in your `boxConfig.js`. In case it is required to point to another personal development server you can run start command with the `boxURL` argument like this `yarn start --boxURL=https://catherinei-dev.torn.com`

**Notes:**
   - On Windows systems you should run the commands in the bash.
   - If you want to start project from the root directory, you can run this command `yarn start <app_name>` (`app_name` is a name of app defined in the package.json without `@torn/`), for example: `yarn start header`.
   - If you have issues with starting apps locally because of CF, you can start it using a tunnel. To create a tunnel run this command `ssh -i ~/.ssh/id_ed25519 -L 8080:127.0.0.1:80 -J torn-dev@89.145.73.200,YOUR_USER_NAME@dev -N root@localhost -p PORT`. To find out port assigned to your container run `docker port $(whoami) | cut -d : -f2`, for username run `whoami` on dev. After that you should update `boxURL` field in your `config/boxConfig.js` for proxying requests to your box directly. For example:
      ```javascript
      module.exports = {
        boxURL: 'http://localhost:8080',
        ...
      }
      ```

<h2 id="proxy-setup">Proxy setup</h2>

This step will allow you to develop applications in your personal box environment. Meaning that the application started locally will be available on your box `https://<your_box_url>/<path_to_the_page_in_development>`. For example, if you run `yarn start calendar`, you can see it on the `https://<your_box_url>/calendar.php`. Localhost still will be available.
The main document regarding proxy setup: https://github.com/torncity/torn/wiki/Proxying-local-webpack-dev-server-to-the-dev-box. After setting up docker and running container on your local machine it's required to run `yarn set-proxy <app_name>` and start application `yarn start <app_name>`.

## Upload

Make sure you have ssh access to dev server.

   1. Set up ssh tunnel
   - Create your own `config` file in the `~/.ssh/` directory;
   - Fill new file with the following configuration:
```
Host torn-*
  IdentityFile  <path_to_personal_key>

Host torn-proxy
   HostName      89.145.73.200
   User          torn-dev
   Port          22
   LocalForward  2222 dev:22

Host torn
   HostName 127.0.0.1
   User <remote_user_name>
   Port 2222
```

   2. Create `config/credentials.js` with:
```
    const credentials = {
        host: '127.0.0.1',
        username: '<remote_user_name>',
        passphrase: '<your_phrase>',
        port: 2222,
        privateKey: '<path_to_personal_key>'
     }

     module.exports = credentials
```
   3. If it's a new project, create and fill the `.env` and `template.ejs` files with the data similar to other projects.
   4. Open connection `ssh torn-proxy`
   5. Run `yarn deploy` in the app folder.

      **Note:** Make sure you have created boxConfig.js file like mentioned in **Setup for Development.3**
## Installing New Packages
Since lerna is configured to use **yarn workspaces** it is as simple as running `yarn add your_package -D`. Please don't use npm, it will generate a `package-lock.json` files but we only need only `yarn.lock`

## GIT rules
Please follow this [git cheat sheet](/projects/site/repositories/react-apps/blob/master/docs/git.md)
