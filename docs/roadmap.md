# Fixes roadmap

### # Date and time format

Currently, dates and times are formatted separately in each of the applications. It will be great to use one shared utility for this.

### # Sidebar 'setTimeout' handler took ~500ms

A sidebar handler in a setTimeout is processed very slowly, which stops the application.
