# Scrollbars

### Styles

Scrollbar styles located in `static/css/style/scrollbar.css`.

**Currently described scrolbar types:**
![Scrollbar types](https://i.imgur.com/9xEdelC.png)

You can also test it yourself using sandbox [here](https://codepen.io/ruslanv-dv/pen/PoZjodV).

### Usage

To make block scrollable:
- Add height limit
- Add `scroll-area` class (or add `overflow-x/y: auto` in CSS)
- Add one of scrollbar type classes

HTML:
```html
<div className='custom-content-wrapper scroll-area scrollbar-thin'>
    <!--some content here-->
</div>
```
CSS:
```css
.custom-content-wrapper {
    max-height: 200px;
}
```

### Dark-mode

Scrollbars that already have dark-mode support:
- scrollbar-bright
