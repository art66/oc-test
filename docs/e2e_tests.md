
# E2E testing on Torn over Cypress

### Fast, easy and reliable testing for anything that runs in a browser at Torn.com. The link on origin resource: https://www.cypress.io/

## How to start
  - Simply run `yarn cypress:open` from root directory.

## How it works
 - Use native Doc link: https://www.cypress.io/how-it-works.

## Run particular test
  - Once started you will be navigated onto native Cypress window. It's simple to figure out how it works right there. Click on the test among the list (or/and choose the browser to run with; or/and navigate through apps in dropdown first).

## Write the tests
  - There is a strict, flexible architecture we have in use to write scalable E2E testing over any page on the project. Investigate the doc first in `../cypress/README.md`. There you can find all the descriptions about how to write tests.


## FAQ
 1. I'm getting errors while running `yarn cypress:open`.
 - The first reason is missing `node_modules` packages after cloning the repo. Use `yarn lerna:bootstrap` or `yarn install` as a pre-starting rule.
 - On the other hand it could be so because of internal Cypress reasons. Anyway, once faced, you should try to install/reinstall Cypress globally by `yarn add cypress -g` or `npm install cypress -g`. Then, run `cypress install` right after installation. Navigate back to the root react-apps folder and try to run `yarn cypress:open` one more time.
