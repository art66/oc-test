# First Time Running Guide

This guide should help in understanding how exactly the apps (pages/modules) should run by the developer. There are several scenarios to manage app running (`yarn <%running_scenario%> <%your_app_name%>`) with:

 - "start" -> start and serve app running over `Hot Module Replacement`.
 - "test" -> invoke `Jest + Enzyme` testing managers to check the app on Unit tests.
 - "lint" -> run `Prettier + ESLint + StyleLint`.
 - "compile" -> build app.
 - "upload" -> upload compiled chunks to personal dev box.
 - "deploy" -> compile and upload app to your personal dev box. Also, it sets 'manual' to VERSION.MD file.
 - "set-manual" -> set 'manual' to VERSION.MD file to disable sync application on your personal dev box with prod.
 - "unset-manual" -> remove 'manual' from VERSION.MD file to enable sync application on your personal dev box with prod.
 - "set-proxy" -> update app template on your personal dev box for listening the app that is started locally on your machine and set 'manual'.

## Installing the app

Let's say you got a task about new "Player Statistic" page creation. It would require you to initialize future app from scratch. Luckily for us we have a template starter kit `./templates`. Which could install the app in couple ways. Based on which model you need to created (routing or non-routing based one):

 - **Basic App**. Navigate to `./templates/basic-app` and clone all the entire folder inside `./pages` and rename it afterward.
 - **Routing App**. Navigate to `./templates/routing-app` and clone all the entire folder inside `./pages` and rename it afterward.

 So typically you just need take the template you need to rollout and clone all the entire folder inside `./pages`.

## Setting up the app

All right! Once the app template is installed it's time to set up several core scripts in brand new `./pages/<%your_app_name%>` app. First of all it's important to fill `package.json` and `.env` files in a right way. These two are responsible for app running and deploying on the DEV.

Besides, looking ahead it's a whole list of files you shall configure to:

 1. `./package.json`
 2. `./.env`
 3. `./src/store/initialState.ts`

### Configure package.json file

 It's almost automated and don't require fine tuning so fill just several fields inside:

  1. `"name": "@torn/<%your_app_name%>"`
  2. `"description": "I will show user's stats."`
  3. `"author": "<%my_github_username%>"`

### Configure .env file

 This is where you should store all unique info about the app. Such as `name` or path `remoteStaticPath` info and so on. Basically this file responsible for entire app running (for all scenarios, described on the very beginning).

 Fields required to fill on first run:

  1. `localPath=<%your_app_name%>/` - this is your current folder name among `./pages`
  2. `remoteStaticPath=builds/<%your_app_name%>/` - app static folder on `DEV`, where compiled chunks will be pushed over `deploy` command run and served afterward.
  3. `ENTITY_TYPE=<%your_app_type%>` - it can be "page" or "module" based on which model of the app you're working.

TIP: you can start your app locally even with `localPath` filled only (in case back-end is not ready yet).

## Running the app

Run `yarn start` from the app folder or `yarn start <app_name>` from the repo root.

## FAQ

 1. I need to install some particular packages, like ramda. Where should I do that?
 - In that case you feel free to add them directly into your own app via  `./<%your_app_name%>/package.json` file. To do that just navigate inside it `cd ./<%your_app_name%>` and install the package your like to: `yarn add <%package_name%>` with/without `-D` flag based on which kind of the package you prefer to add (for app running or just your own developer-like purposes - `redux-logger` for example).

 2. I need to add my personal environment variables for some reason.
 - Okay, it's not a common scenario but in case you need it try to add any of them inside your `./.env` file directly. This script will hoist them right before app running into the global scope right before app compiling stage.

-----

## Thanks for you time and let's build a better Torn together! :)
