# Git cheatsheet

## Recommened git config
`git config --global branch.autosetuprebase always`   
`git config --global pull.rebase preserve`   

This config will let you run `git pull` instead of two separated commands `git fetch` and `git rebase origin -p`

## Start work

clone the repo   
`git clone git@codebasehq.com:torncity/site/react-apps.git`

create your working branch
`git checkout -b <feature>`

Work in this branch, add files to staging area by  
`git add <filename>`  

Or all files at once  
`git add .`  

Including removed/renamed  
`git add -a`  

commit  
`git commit -m "commit message"`

<details>
<summary>Ammend commit name</summary>   

---
Something went wrong with commit, and you need to modify it’s message or files
Make staging area look like you want the commit to look by adding files, then  
`git commit --amend`  
You’ll be able to modify commit message

---
</details>


## Merge your work in master branch


Pulling updates  
`git fetch origin`  
Will take all the code from origin but will NOT merge or rebase anything

Rebase your branch against master (basicaly this means taht you will put all the work done in master ahead of yours)  
`git rebase origin/master`  

<details><summary>Resolve conflicts</summary>
 
---
If a conflict arise, you need to resolve them manually, by editing the files, the list of them is available by
`git status`

Edit files, and add files to staging area  
`git add filename`

When all the files are there, continue the rebase process  
`git rebase --continue`

The process of rebasing is per commit, so you might need to repeat it a few times

---
</details>

Checkout master branch   
`git checkout master`

Get updated in master   
`git pull`

And finaly merge  

`git merge <feature> --no-ff`  
`--no-ff` flag makes sure that history is preserved in a branch, otherwise history will look linear, as if all commits were made on master branch

![git branches](https://i.imgur.com/qDLlu75.png)


The branch should be deleted now, if not it it will require a rebase later, so it’s a good practice to get rid of it (the history will stay there anyway)  

`git branch -d <feature>`  


Push your work to remote repo
`git push`
