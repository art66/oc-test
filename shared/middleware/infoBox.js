import { createAction } from 'redux-actions'

export const show = createAction('show')

export const hideInfoBox = createAction('hide')

export const showInfoBox = params => dispatch => {
  dispatch(show(params))
  if (!params.persistent) {
    setTimeout(() => dispatch(hideInfoBox()), 5000)
  }
}

const ACTION_HANDLERS = {
  show(state, action) {
    return {
      ...state,
      msg: action.payload.msg,
      color: action.payload.color
    }
  },
  hide(state, action) {
    return {}
  }
}

const initialState = {}

export default function sidebarReducer(state = initialState, action) {
  let handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
