import { useEffect } from 'react'
import { isString } from 'util'

export default function PreloadFontsHook(font: string | string[]) {
  useEffect(() => {
    function preloadFont(fontFamily) {
      const element = document.createElement('span')

      element.style.fontFamily = fontFamily
      element.style.width = '0px'
      element.style.height = '0px'
      element.style.overflow = 'hidden'

      document.body.appendChild(element)

      setTimeout(() => document.body.removeChild(element))
    }

    if (isString(font)) {
      preloadFont(font)
    } else {
      const fonts = font

      fonts.forEach(preloadFont)
    }
  }, [])
}
