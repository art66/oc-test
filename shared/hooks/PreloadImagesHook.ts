import { useEffect } from 'react'

export default function PreloadImagesHook(imagesSrc: string[]) {
  useEffect(() => {
    imagesSrc.forEach(imageSrc => {
      new Image().src = imageSrc
    })
  }, [])
}
