# Shared - Torn


## 3.3.0
 * Created isDev util.

## 3.3.0
 * Created numberToArray util.

## 3.2.0
 * Improved getUserID util.

## 3.19.1
 * Improved getUserID util.

## 3.19.0
 * Added isEmpty util.

## 3.18.0
 * Added several "is" testing utils.

## 3.17.0
 * Added brand new keyDownManager and localStorageManager utils.

## 3.16.1
 * Fixed icons reinstalling in async mode.

## 3.16.0
 * Added brand new UserInfo Component.

## 3.15.1
 * Fixed unsubscribing on debounce & throttler packages.

## 3.15.0
 * Added brand new Carousel Component.

## 3.14.0
 * Added brand new ProgressiveImage Component.

## 3.13.1
 * Fixed toMoney type definitions.

## 3.13.0
 * Added lazyLoadScrollChecker util.

## 3.12.0
 * Added firstLetterUpper util.

## 3.11.0
 * Added regexp string checker util securedRegExpString.

## 3.10.1
 * Created manageAppWrapLayout util for managing react-app layout for both sidebar/non-sidebar apps in one time SPA app.

## 3.10.0
 * All the newest utils were refactored and covered with tests/interfaces.

## 3.9.3
 * Routing dependencies were moved to root.

## 3.9.2
 * Bumped up redux-router version.

## 3.9.1
 * Improved stringChecker util.

## 3.9.0
 * Added getUserID util.

## 3.8.0
 * Added mediaTypeChange util.

## 3.7.0
 * Added stringChecker util.

## 3.6.1
 * Fixed fetchURL util on errors checking.

## 3.6.0
 * Added debounce util.
 * Improved typing of throttler util.

## 3.5.0
 * Added getCurrentPageURL util for returning current page url string.

## 3.4.0
 * Added sidebarHeightSPAToggle class that allows set sidebar/content-wrapper height for SPA-like apps easily.
 * Added subscribeOnDOMContentLoaded util that allows subscribe on DOMContentLoaded events correctly.

## 3.3.0
 * Added getKeyByValue util that allows easily get key from some object by its value provided.

## 3.2.0
 * Added normalizeFetchParams util that allows make GET queries easily as never before.

## 3.1.0
 * Added isValue util for checking on real/unreal value income, instead of joggling with undefined/null checks.

## 3.0.2
 * Fixed react-redux package to be hoisted from root node_modules folder.

## 3.0.1
 * Fixed shared package.json packages.

## 3.0.0
 * Added fully documented JSDoc data to all of the utils.

## 2.2.0
 * ProgressiveImagesSetGenerator has been renamed by PictureGenerator and moved to the Components folder.
 * Separated webpChecker and created its own folder inside utils.

## 2.1.0
 * Added ProgressiveImagesSetGenerator util.

## 2.0.0
 * Major React v.16.8.6 Update!

## 1.4.2
 * Added few pacakges dependencies inside package.json!

## 1.4.1
 * Update mediaTypeChange util (shold be improved, currently deprecated).

## 1.4.0
 * Ported legacy-based convertImageToCanvas function into the React Apps.

## 1.3.0
 * Added node script for svg icons parser run from cli.

## 1.2.0
 * Added mediaTypeChange util.

## 1.1.0
 * Added throttler util for better browser events handling.
 * Added tooltipModifier util for legacy based tooltip modification inside React Apps.

## 1.0.0
 * Refactored desktopLayoutChecker util.

## 0.0.1
 * Added manual desktop layout mode checking util.
 * Added observable util.
