
# Global ToolTip Component

How to use:
  - Quick Start:

  so, first of all you need to initialize the <Tooltip /> component inside the main app module, like:

      import TooltipNew from '@shared/component/TooltipsNew'

      // ...
      <App> // your very first app entry in the whole app
        ... // the rest part of the components list
        <ToolTip />
      </App>

  but it's not enaught to start using them. To be able to show any of the tooltips you need to throw some data into `{ tooltipsObservable }` helper, imported from `@torn/share/components/TooltipsNew` just on the component where tooltip is need. The next, you need to initialize `tooltipsObservable.render({ child, ID })` in the `componentDidMount()` of its component. For example:

      class ButtonWithToltip extends React.Component {  // component where you need to add the tooltip
        ... //
        componentDidMount() {
          tooltipsObservable.render({
            ID: 'SOME_NODE_ID_TO_BIND',
            child: 'TITLE_TO_SHOW'
          })
        }
        ... //
      }

  That is all! :)

  - Advanced Configuration:

  You can customize toltips layout as you wish: starting from animations and finishing with custom styling:

      <ToolTip
          addArrow: true, // gives an opportunity to cut the arrow from Tooltip
          customCoods: {  // gives an opportunity to set the Tooltip position manually
          position: {  // set the Tooltip position by sides, e.g. ("left top" or "center bottom" and so on)
            x: '',
            y: ''
          },
          manualCoordsFix: { // can be used to extra manual positioning based on calculated own tooltip coords.
            fixX: 0,
            fixY: 0
          }
          closeButton: { // can render the button to close Tooltip explicitly by onClick event.
            active: false,
            buttonChildren: '' // text of the button,
            actionOnClose: () => {} // some action for the close click button
          },
          animProps: { // tunу animation configuration by youselef, make you own transitions
            className: 'tooltip',
            timeExit: 1000,
            timeIn: 30
          },
          overrideStyles: { // customizing all Tooltip DOM elements by custom styles providing.
            button: '',
            wrap: '',
            container: '',
            arrowWrap: '',
            tooltipArrow: '',
            title: ''
          },
          actionOnEntered: () => {}, // callback, fires on onEntered phase during Animation mode active
          actionOnExit: () => {} // the same one callback, that fires just only on the onExited Animation phase
      />

  - PRO Configuration:

    Sometimes you'll need to update Tooltips inner data dynamicly basically on parent component updates. To deal with that you need to reinstall Tooltip component inside parent `componentsDidUpdate` method. For this time you just need to add flag `type: 'update` provided for the `render` method. For example:

   ```
      class ButtonWithToltip extends React.Component {  // component where you need to add the tooltip
        ... //
        componentDidUpdate() {
          tooltipsObservable.render({
            ID: 'SOME_NODE_ID_TO_BIND',
            child: 'TITLE_TO_SHOW'
          }, type: 'update')
        }
        ... //
      }
   ```

  So, as you can see - that's very easy to use. The rest part is need just on your own. Have fun! :)
