
# Tooltip - Torn


## 4.1.2
 * Fixed right side arrow appear.

## 4.1.1
 * Fixed classes ordering for overrideStyles config.

## 4.1.0
 * Improved tooltips update logic.

## 4.0.2
 * Improved independent props throw.

## 4.0.1
 * Added tooltip hiding by touchmove event for touchscreen devices.

## 4.0.0
 * Added ability to set individual style props by each of tooltips.

## 3.7.2
 * Own Throttler util were replaced by global one.

## 3.7.1
 * Added ability to throw even JSX children as a tooltip layout.

## 3.7.0
 * Update Tooltips storybook.
 * Improve README.md config.

## 3.6.0
 * Improved interface by adding render method to the tooltipsSubscriber util.
 * Added tooltipsSubscriber util as exported from index.tsx main file.
 * Updated tests.

## 3.5.0
 * Update yarn.lock in TooltipNew Component.

## 3.4.0
 * Refactored some code.
 * Improved stability.

## 3.3.0
 * Added ability to throw custom margins (coordsFixes) for manual slim positioning.

## 3.2.0
 * Updated All tests for Tooltip Component.

## 3.1.1
 * Updated StoryBook config.
 * Fixed broken positioning on right/left appear.
 * Minor bug fixes.

## 3.0.3
 * Removed unused code from index.tsx.

## 3.0.2
 * Fixed phantom bugs in Tooltip positioning while mounting.
 * Minor bugs fixed.
 * Improved stability.

## 3.0.1
 * Updated App achitecture.
 * Added throttler util for better resize events handling.

## 3.0.0
 * Major TooltipNew Library Update to v.3.0.0!
 * Rewritten mouseEvents logic responded for showing/hiding tooltip.
 * Switched single-using pattern with explicit injection Tooltip inside some component to global document.body delegation.
 * Add observable environment to do the stuffs above.
 * Optimized and refactored some code inside.
 * Updated README.md file.

## 2.4.0
 * Updated README.md file.

## 2.3.1
 * Updated Storybook config.
 * Updated Tests suits.
 * Minor fixes.

## 2.2.1
 * Added the right close click hindling in manual Tooltip close mode.
 * Removed unuseful common layout mode.
 * Minor fixes.

## 2.1.2
 * Minor style fixes.

## 2.1.1
 * Adde ability to swap position around parent element (if pageYOffset more than topest point of Tooltip element and vice versa).
 * Minor Bug Fixes in Tooltip Component.

## 2.0.0
 * Added API for comfortable manipulating Tooltip coords position under parent Node.
 * Tooltip now can be placed inside root body node, istead of local in-component using.
 * Improved source code stability.
 * Updated Types.
 * Minor Fixes.

## 1.1.0
 * Added Feature for handling Events inside Tooltip component.
 * Added ability to throw custom coords props.
 * Updated Types.
 * Minor Fixes.

## 1.0.0
 * Written Readme.md file.
 * First stabel release.

## 0.5.0
 * Written tests for All Component inside Tooltip.

## 0.4.0
 * Created more separated Tooltip structure.

## 0.3.1
 * Updated StoryBook config file.
 * Minor fixes.

## 0.2.1
 * Added StoryBook config file.
 * Minor fixes.

## 0.1.0
 * Tooltip has been created.
