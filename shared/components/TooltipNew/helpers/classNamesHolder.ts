import classnames from 'classnames'
import styles from '../styles/index.cssmodule.scss'

const classNamesHolder = overrideStyles => {
  const wrapStyles = classnames({
    [styles.tooltipWrap]: true,
    [overrideStyles.wrap]: overrideStyles.wrap
  })

  const containerStyles = classnames({
    [styles.tooltipContainer]: true,
    [overrideStyles.container]: overrideStyles.container
  })

  const titleStyles = classnames({
    [styles.title]: true,
    [overrideStyles.title]: overrideStyles.title
  })

  const buttonStyles = classnames({
    [styles.button]: true,
    [styles.closeButton]: true,
    [overrideStyles.button]: overrideStyles.button
  })

  const arrowWrapStyles = classnames({
    [styles.arrowWrap]: true,
    [overrideStyles.arrowWrap]: overrideStyles.arrowWrap
  })

  const arrowStyles = classnames({
    [styles.tooltipArrow]: true,
    [overrideStyles.tooltipArrow]: overrideStyles.tooltipArrow
  })

  return {
    wrapStyles,
    containerStyles,
    titleStyles,
    buttonStyles,
    arrowWrapStyles,
    arrowStyles
  }
}

export default classNamesHolder
