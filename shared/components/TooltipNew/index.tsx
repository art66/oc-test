import React from 'react'
import ReactDOM from 'react-dom'
import { CSSTransition } from 'react-transition-group'
import { ThrottlerDOM } from '@torn/shared/utils/throttler'

import { tooltipSubscriber } from './utils'
import { getCoordsShift, getTransformShift, classNamesHolder, defaultProps } from './helpers'

import { Arrow, CloseButton } from './components'

import { IProps, IState, ITooltips } from './types'
import {
  ROOT_NODE,
  ALLOW_EVENTS,
  DISABLE_EVENTS,
  TOP,
  LEFT,
  BOTTOM,
  RIGHT,
  CENTER,
  DEFAULT_ANIM_PROPS
} from './constants'

import './styles/anim.scss'

class ToolTip extends React.Component<IProps, IState> {
  private _throttler: any
  private _ref: React.RefObject<HTMLInputElement>

  static defaultProps: IProps = defaultProps

  constructor(props: IProps) {
    super(props)

    this.state = {
      showTooltip: false,
      tooltipNode: null,
      parentNode: null,
      tooltip: {
        ID: null,
        child: ''
      }
    }

    this._ref = React.createRef()
  }

  componentDidMount() {
    document.body.addEventListener('mouseover', this._tooltipsObserver)
    document.body.addEventListener('touchmove', this._hideTooltip)

    this._throttler = new ThrottlerDOM()
    this._throttler.run({ event: 'resize', callback: this._hideTooltip })

    tooltipSubscriber.addAction(this._updateActiveTooltip)
  }

  componentWillUnmount() {
    document.body.removeEventListener('mouseover', this._tooltipsObserver)
    document.body.removeEventListener('touchmove', this._hideTooltip)
    window.removeEventListener('resize', this._hideTooltip)
    tooltipSubscriber.destroyAll()
    this._throttler.stop()
  }

  _tooltipNotPermitted = (nodeID) => ((nodeID === null || !nodeID) && !this._isButtonThrowed()) || false

  _findTargetNode = (e) => {
    const nodeRef = e.target
    const nodeID = nodeRef.getAttribute('id')

    if (this._tooltipNotPermitted(nodeID)) {
      this._hideTooltip()

      return null
    }

    return {
      nodeRef,
      nodeID
    }
  }

  _tooltipsObserver = (e) => {
    const tooltips: ITooltips[] = tooltipSubscriber.getTargets()
    const { nodeRef = null, nodeID = null } = this._findTargetNode(e) || {}
    const tooltip = tooltips.find((tooltipNode) => nodeID === tooltipNode.ID)

    // console.log(tooltip, nodeRef, 'tooltips')

    if (!tooltip) return

    this._showTooltip(nodeRef, tooltip)
    this._checkTooltipUnderCell()
  }

  _updateActiveTooltip = (nextTooltips) => {
    const { tooltip: currentTooltip } = this.state

    // watching new data updates once tooltip is showed
    const newTooltip = nextTooltips.find((incomingTooltip: any) => {
      const targetMatch = incomingTooltip.ID === currentTooltip.ID
      const childsNotEqual = incomingTooltip.child !== currentTooltip.child

      if (targetMatch && childsNotEqual) {
        return incomingTooltip
      }

      return null
    })

    this.setState((prevState) => ({
      ...prevState,
      tooltip: newTooltip || prevState.tooltip
    }))
  }

  _getTooltipNode = () => ReactDOM.findDOMNode(this)

  _setTooltipeNode = () => {
    this.setState({
      tooltipNode: this._getTooltipNode()
    })
  }

  _showTooltip = (nodeRef, tooltip) => {
    this.setState({
      tooltip,
      showTooltip: true,
      parentNode: nodeRef,
      tooltipNode: this._getTooltipNode()
    })
  }

  _hideTooltip = () => this.setState({ showTooltip: false })

  _getClientNodesRects = () => {
    const { tooltipNode, parentNode } = this.state

    const parentRects = (parentNode && parentNode.getBoundingClientRect && parentNode.getBoundingClientRect()) || {}
    const tooltipRects = (tooltipNode && tooltipNode.getBoundingClientRect && tooltipNode.getBoundingClientRect()) || {}

    return {
      parentRects,
      tooltipRects
    }
  }

  _getCustomTooltipProps = () => {
    const { tooltip } = this.state

    if (!tooltip.customConfig || Object.keys(tooltip.customConfig).length === 0) {
      return {} as IProps
    }

    const { customConfig } = tooltip
    const {
      position,
      manualCoodsFix,
      closeButton,
      overrideStyles,
      animProps,
      customCoords,
      addArrow,
      actionOnEntered,
      actionOnExited
    } = this.props

    return {
      ...this.props,
      position: customConfig.position || position,
      manualCoodsFix: customConfig.manualCoodsFix || manualCoodsFix,
      closeButton: customConfig.closeButton || closeButton,
      overrideStyles: customConfig.overrideStyles || overrideStyles,
      animProps: customConfig.animProps || animProps,
      customCoords: customConfig.customCoords || customCoords,
      addArrow: customConfig.addArrow || addArrow,
      actionOnEntered: customConfig.actionOnEntered || actionOnEntered,
      actionOnExited: customConfig.actionOnExited || actionOnExited
    } as IProps
  }

  _checkTooltipUnderCell = () => {
    const { tooltipNode } = this.state
    const {
      position: { y: coordY }
    } = this.props

    const overrideProps = this._getCustomTooltipProps()

    const positionY = (overrideProps.position && overrideProps.position.y) || coordY

    const { parentRects, tooltipRects } = this._getClientNodesRects()

    const { height = 0, top = 0, bottom = 0 } = tooltipRects
    const { height: parentHeight = 0 } = parentRects

    const tooltipSettedFromTopToBottom = positionY === BOTTOM && top < parentHeight + height
    const tooltipSettedFromBottomToTop = positionY === TOP && innerHeight - bottom + height < parentHeight + height
    const shiftAlreadySetted = tooltipSettedFromTopToBottom || tooltipSettedFromBottomToTop

    const topYOffset = height > top
    const bottomYOffset = positionY === BOTTOM && bottom > innerHeight

    if (!tooltipNode) return

    this.setState((prevState) => ({
      ...prevState,
      coordY: (shiftAlreadySetted && positionY) || (topYOffset && BOTTOM) || (bottomYOffset && TOP) || positionY
    }))
  }

  _calculatePositions = () => {
    const {
      position: { x: coordX, y: coordY }
    } = this.props

    const overrideProps = this._getCustomTooltipProps()

    const positionX = (overrideProps.position && overrideProps.position.x) || coordX
    const positionY = (overrideProps.position && overrideProps.position.y) || coordY

    const { parentRects, tooltipRects } = this._getClientNodesRects()

    const { top = 0, bottom = 0, left = 0, right = 0, height = 0, width = 0 } = parentRects || {}
    const { height: tooltipHeight = 0, width: tooltipWidth = 0 } = tooltipRects || {}

    const restProps = () => {
      const propsX = {
        left: {
          parentLeft: left,
          tooltipWidth
        },
        center: {
          parentLeft: left,
          parentWidth: width
        },
        right: {
          parentRight: right
        }
      }

      const propsY = {
        top: {
          parentTop: top,
          tooltipHeight
        },
        center: {
          parentTop: top,
          parentHeight: height,
          tooltipHeight
        },
        bottom: {
          parentBottom: bottom
        }
      }

      return {
        x: propsX[positionX],
        y: propsY[positionY]
      }
    }

    return {
      positionX,
      positionY,
      restProps: restProps()
    }
  }

  _getCoordsFix = () => {
    const { manualCoodsFix } = this.props
    const overrideProps = this._getCustomTooltipProps()

    const { fixX = 0, fixY = 0 } = (overrideProps && overrideProps.manualCoodsFix) || manualCoodsFix || {}

    return {
      fixX,
      fixY
    }
  }

  _calculateCoords = () => {
    const {
      position: { x: coordX, y: coordY }
    } = this.props

    const overrideProps = this._getCustomTooltipProps()

    const positionX = (overrideProps.position && overrideProps.position.x) || coordX
    const positionY = (overrideProps.position && overrideProps.position.y) || coordY

    const { restProps } = this._calculatePositions()
    const { trasformX, trasformY } = getTransformShift(positionX, positionY)
    const { left: leftCoord, top: topCoord } = getCoordsShift(positionX, positionY, restProps)
    const { fixX, fixY } = this._getCoordsFix()

    return {
      top: topCoord + fixX,
      left: leftCoord + fixY,
      transform: `translate(${trasformX}, ${trasformY})`
    }
  }

  _getCoords = () => {
    const {
      customCoords: { active, top, left }
    } = this.props

    const overrideProps = this._getCustomTooltipProps()

    const isActive = (overrideProps.customCoords && overrideProps.customCoords.active) || active
    const customTop = (overrideProps.customCoords && overrideProps.customCoords.top) || top
    const customLeft = (overrideProps.customCoords && overrideProps.customCoords.left) || left

    if (!isActive) {
      return this._calculateCoords()
    }

    return {
      top: customTop,
      left: customLeft
    }
  }

  _isButtonThrowed = () => {
    const { closeButton = { active: false } } = this.props
    const overrideProps = this._getCustomTooltipProps()

    const isActiveButton = (overrideProps.closeButton && overrideProps.closeButton.active) || closeButton.active

    if (!isActiveButton) {
      return false
    }

    return true
  }

  _animEnter = () => this._setTooltipeNode()

  _animEntered = () => {
    const { actionOnEntered } = this.props

    const emitActionOnEnter = this._getCustomTooltipProps().actionOnEntered || actionOnEntered

    emitActionOnEnter && emitActionOnEnter()
  }

  _animFinished = () => {
    const { actionOnExited } = this.props

    const emitActionOnExit = this._getCustomTooltipProps().actionOnExited || actionOnExited

    emitActionOnExit && emitActionOnExit()
  }

  _getPointerEvents = (isButtonThrowed) => (isButtonThrowed ? ALLOW_EVENTS : DISABLE_EVENTS)

  _getWrapperStyles = () => {
    const {
      position: { x: coordX }
    } = this.props

    const overrideProps = this._getCustomTooltipProps()

    const positionX = (overrideProps.position && overrideProps.position.x) || coordX
    const isButonThrowed = this._isButtonThrowed()
    const customCoords = this._getCoords()
    const customOrientation = positionX === CENTER ? 'column' : 'row'

    return {
      pointerEvents: this._getPointerEvents(isButonThrowed),
      flexDirection: customOrientation,
      ...customCoords
    }
  }

  _getBodyElementQueie = (body, arrow) => {
    const {
      position: { x: coordX, y: coordY },
      addArrow = false
    } = this.props

    const isAddArrow = this._getCustomTooltipProps().addArrow || addArrow

    if (!isAddArrow) {
      return body()
    }

    const overrideProps = this._getCustomTooltipProps()

    const positionX = (overrideProps.position && overrideProps.position.x) || coordX
    const positionY = (overrideProps.position && overrideProps.position.y) || coordY

    const arrowFirst = positionX === RIGHT || (positionX !== LEFT && positionY === BOTTOM)
    const tooltipBody = arrowFirst ? [arrow(), body()] : [body(), arrow()]

    return tooltipBody
  }

  _hideByClick = () => {
    const {
      closeButton: { actionOnClose }
    } = this.props

    const overrideProps = this._getCustomTooltipProps()
    const isActionOnClose = (overrideProps.closeButton && overrideProps.closeButton.actionOnClose) || actionOnClose

    if (isActionOnClose) {
      actionOnClose()
    }

    this._hideTooltip()
  }

  _renderToolTip() {
    const {
      tooltip: { child, ID }
    } = this.state
    const {
      overrideStyles,
      closeButton,
      addArrow,
      position: { x: coordX, y: coordY }
    } = this.props

    const overrideProps = this._getCustomTooltipProps()

    const stylesToOverride = overrideProps.overrideStyles || overrideStyles
    const buttonToClose = overrideProps.closeButton || closeButton
    const isArrow = overrideProps.addArrow || addArrow
    const positionX = (overrideProps.position && overrideProps.position.x) || coordX
    const positionY = (overrideProps.position && overrideProps.position.y) || coordY

    const { wrapStyles, containerStyles, titleStyles, arrowWrapStyles, arrowStyles, buttonStyles } = classNamesHolder(
      stylesToOverride
    )

    const body = () => (
      <div key={1} className={containerStyles}>
        <CloseButton
          active={buttonToClose.active}
          buttonStyles={buttonStyles}
          buttonChildren={buttonToClose.buttonChildren}
          clickAction={this._hideByClick}
        />
        <span className={titleStyles}>{child}</span>
      </div>
    )

    const arrow = () => (
      <Arrow
        key={2}
        addArrow={isArrow}
        arrowWrapStyles={arrowWrapStyles}
        arrowStyles={arrowStyles}
        arrowRotate={{ coordX: positionX, coordY: positionY }}
      />
    )

    const tooltipBody = this._getBodyElementQueie(body, arrow)

    return (
      <div
        id={`${ROOT_NODE}${ID}`}
        ref={this._ref}
        className={wrapStyles}
        // @ts-ignore: strange error in styles processing
        style={this._getWrapperStyles()}
      >
        {tooltipBody}
      </div>
    )
  }

  _renderAnimatedTooltip = () => {
    const { showTooltip } = this.state
    const { animProps } = this.props
    const overrideAnimProps = this._getCustomTooltipProps()

    const { className = 'tooltip', timeExit = 1000, timeIn = 60 } =
      (overrideAnimProps && overrideAnimProps.animProps) || animProps || DEFAULT_ANIM_PROPS

    return (
      <CSSTransition
        in={showTooltip}
        classNames={className}
        timeout={{ enter: timeIn, exit: timeExit }}
        onEnter={this._animEnter}
        onEntered={this._animEntered}
        onExited={this._animFinished}
        unmountOnExit={true}
      >
        {this._renderToolTip()}
      </CSSTransition>
    )
  }

  render() {
    const tooltipDOM = this._renderAnimatedTooltip()

    return ReactDOM.createPortal(tooltipDOM, document.body)
  }
}

export { tooltipSubscriber as tooltipsSubscriber }

export default ToolTip
