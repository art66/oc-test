import React from 'react'
import PropTypes from 'prop-types'
import { boolean } from '@storybook/addon-knobs'
import { State, Store } from '@sambego/storybook-state'

import stylesExtended from './mocks/extended.cssmodule.scss'

import Tooltip from '../index'
import { tooltipSubscriber } from '../utils'
import { ARROW, TITLE } from './mocks'

const STORY_GROUPS = {
  common: 'Common',
  coords: 'Coords',
  closeButton: 'Close Button',
  addArrow: 'Arrow',
  animProps: 'Animation',
  overrideStyles: 'Styles'
}

const STYLES_WRAPPER = {
  width: '100px',
  height: '100px',
  background: 'black',
  color: 'white',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}

const store = new Store({
  showTooltip: false
})

const CONTAINER_TITLE = 'Hover Me'
const PARENT = {
  ID: 'storybook'
}

const storeButton = {
  checkIsButtonActive: false
}

/* -------------------------------- */
// STORYBBOk PROPS UPDATE LOGIC START
/* -------------------------------- */

const _setFalse = () => store.set({ showTooltip: false })
const _getActions = (parent) => {
  parent && parent.addEventListener('mouseenter', () => store.set({ showTooltip: true }))

  // hack for practicing handler by clicking on the close button
  storeButton.checkIsButtonActive &&
    parent &&
    parent.addEventListener('mouseleave', () => store.set({ showTooltip: true }))
  !storeButton.checkIsButtonActive && parent && parent.addEventListener('mouseleave', () => _setFalse())

  // parent && parent.addEventListener('mouseleave', () => store.set({ showTooltip: true })
  // parent && parent.addEventListener('mouseleave', () => store.set({ showTooltip: false })
}

const _getParent = () => document.getElementById('storybook')

document.addEventListener('DOMContentLoaded', () => _getActions(_getParent()))
document.body.addEventListener('DOMSubtreeModified', () => _getActions(_getParent()))

class WrapperExpanded extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
  }

  componentDidMount() {
    const { children, title } = this.props

    if (children.props.closeButton.active === true) {
      storeButton.checkIsButtonActive = true
    }

    tooltipSubscriber.subscribe({
      child: title || TITLE,
      ID: PARENT.ID,
      customConfig: {
        overrideStyles: {
          container: stylesExtended.tooltipContainer,
          title: stylesExtended.tooltipTitle,
          arrowWrap: stylesExtended.tooltipArrowWrap,
          tooltipArrow: stylesExtended.tooltipArrow
        },
        position: { x: 'center', y: 'bottom' },
        addArrow: true,
        manualCoodsFix: {
          fixX: 4,
          fixY: 0
        },
        animProps: {
          className: 'expTooltip',
          timeExit: 300,
          timeIn: 150
        }
      }
    })
  }

  render() {
    const { children } = this.props

    return (
      <div id="storybook" style={STYLES_WRAPPER}>
        {CONTAINER_TITLE}
        <State store={store}>{children}</State>
      </div>
    )
  }
}

export const Defaults = () => {
  const config = {
    addArrow: boolean('Add Arrow:', ARROW, STORY_GROUPS.common)
  }

  return (
    <WrapperExpanded>
      <Tooltip {...config} />
    </WrapperExpanded>
  )
}

Defaults.story = {
  name: 'defaults'
}

export default {
  title: 'Shared/Tooltip/Expanded'
}
