import React from 'react'
import PropTypes from 'prop-types'
import { text, boolean, number } from '@storybook/addon-knobs'
import { State, Store } from '@sambego/storybook-state'

import styles from './mocks/index.cssmodule.scss'

import Tooltip from '../index'
import { tooltipSubscriber } from '../utils'
import { CLOSE_BUTTON, ARROW, TITLE, ANIM_PROPS } from './mocks'

const STORY_GROUPS = {
  common: 'Common',
  coords: 'Coords',
  closeButton: 'Close Button',
  addArrow: 'Arrow',
  animProps: 'Animation',
  overrideStyles: 'Styles'
}

const STYLES_WRAPPER = {
  width: '100px',
  height: '100px',
  background: 'black',
  color: 'white',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}

const store = new Store({
  showTooltip: false
})

const CONTAINER_TITLE = 'Hover Me'
const PARENT = {
  ID: 'storybook'
}

const storeButton = {
  checkIsButtonActive: false
}

/* -------------------------------- */
// STORYBBOk PROPS UPDATE LOGIC START
/* -------------------------------- */
// eslint-disable-next-line
const _setFalse = () => store.set({ showTooltip: false })
// eslint-disable-next-line
const _getActions = (parent) => {
  parent && parent.addEventListener('mouseenter', () => store.set({ showTooltip: true }))

  // hack for practicing handler by clicking on the close button
  storeButton.checkIsButtonActive
    && parent
    && parent.addEventListener('mouseleave', () => store.set({ showTooltip: true }))
  !storeButton.checkIsButtonActive && parent && parent.addEventListener('mouseleave', () => _setFalse())

  // parent && parent.addEventListener('mouseleave', () => store.set({ showTooltip: true })
  // parent && parent.addEventListener('mouseleave', () => store.set({ showTooltip: false })
}

// eslint-disable-next-line
const _getParent = () => document.getElementById('storybook')

document.addEventListener('DOMContentLoaded', () => _getActions(_getParent()))
document.body.addEventListener('DOMSubtreeModified', () => _getActions(_getParent()))

class WrapperBasic extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
  }

  componentDidMount() {
    const { children, title } = this.props

    if (children.props.closeButton.active === true) {
      storeButton.checkIsButtonActive = true
    }

    tooltipSubscriber.subscribe({ child: title || TITLE, ID: PARENT.ID })
  }

  render() {
    const { children } = this.props

    return (
      <div id='storybook' style={STYLES_WRAPPER}>
        {CONTAINER_TITLE}
        <State store={store}>{children}</State>
      </div>
    )
  }
}

export default {
  title: 'Shared/Tooltip/Basic'
}

export const Defaults = () => {
  const config = {
    addArrow: boolean('Add Arrow:', ARROW, STORY_GROUPS.common),
    position: {
      x: text('layoutX:', 'left', STORY_GROUPS.coords),
      y: text('layoutY:', 'top', STORY_GROUPS.coords)
    },
    customCoords: {
      active: boolean('Active customCoords:', false, STORY_GROUPS.common),
      top: number('positionX:', 0, STORY_GROUPS.coords),
      left: number('positionY:', 0, STORY_GROUPS.coords)
    },
    closeButton: {
      active: boolean('Active Button:', CLOSE_BUTTON.active, STORY_GROUPS.closeButton),
      buttonChildren: text('Button Children:', CLOSE_BUTTON.buttonChildren, STORY_GROUPS.closeButton)
    },
    animProps: {
      className: text('Animation name:', ANIM_PROPS.className, STORY_GROUPS.animProps),
      timeExit: text('Time Enter:', ANIM_PROPS.timeIn, STORY_GROUPS.animProps),
      timeIn: text('Time Exit:', ANIM_PROPS.timeExit, STORY_GROUPS.animProps)
    },
    overrideStyles: {},
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

Defaults.story = {
  name: 'defaults'
}

export const CloseByButtonMode = () => {
  const config = {
    showTooltip: boolean('Show Tooltip:', true),
    addArrow: boolean('Add Arrow:', true),
    animProps: {
      className: text('Animation name:', ANIM_PROPS.className, STORY_GROUPS.animProps),
      timeExit: text('Time Enter:', ANIM_PROPS.timeIn, STORY_GROUPS.animProps),
      timeIn: text('Time Exit:', ANIM_PROPS.timeExit, STORY_GROUPS.animProps)
    },
    closeButton: {
      active: boolean('Active Button:', true),
      buttonChildren: text('Button Children:', ''),
      actionOnClose: () => store.set({ showTooltip: false })
    },
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

CloseByButtonMode.story = {
  name: 'close by button mode'
}

export const WithoutArrowMode = () => {
  const config = {
    showTooltip: boolean('Show Tooltip:', true),
    addArrow: false,
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

WithoutArrowMode.story = {
  name: 'without arrow mode'
}

export const OverrideStylesMode = () => {
  const config = {
    showTooltip: boolean('Show Tooltip:', true),
    addArrow: boolean('Add Arrow:', true),
    overrideStyles: { ...styles },
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

OverrideStylesMode.story = {
  name: 'override styles mode'
}

export const TopLayout = () => {
  const config = {
    showTooltip: boolean('Show Tooltip:', true),
    addArrow: boolean('Add Arrow:', true),
    position: { x: 'center', y: 'top' },
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

TopLayout.story = {
  name: 'top layout'
}

export const RightLayout = () => {
  const config = {
    showTooltip: boolean('Show Tooltip:', true),
    addArrow: boolean('Add Arrow:', true),
    position: { x: 'right', y: 'center' },
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

RightLayout.story = {
  name: 'right layout'
}

export const BottomLayout = () => {
  const config = {
    showTooltip: boolean('Show Tooltip:', true),
    addArrow: boolean('Add Arrow:', true),
    position: { x: 'center', y: 'bottom' },
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

BottomLayout.story = {
  name: 'bottom layout'
}

export const LeftLayout = () => {
  const config = {
    showTooltip: boolean('Show Tooltip:', true),
    addArrow: boolean('Add Arrow:', true),
    position: { x: 'left', y: 'center' },
    actionOnEntered: () => {},
    actionOnExit: () => {}
  }

  return (
    <WrapperBasic>
      <Tooltip {...config} />
    </WrapperBasic>
  )
}

LeftLayout.story = {
  name: 'left layout'
}
