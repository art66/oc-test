import tooltipSubscriber from './tooltipSubscriber'
import { throttlerDOM } from '@torn/shared/utils/throttler'

/**
* @deprecated don't use them!
*/

export { tooltipSubscriber, throttlerDOM }
