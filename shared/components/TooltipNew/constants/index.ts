export const ROOT_NODE = '#react-tooltip-'
export const ALLOW_EVENTS = 'all'
export const DISABLE_EVENTS = 'none'
export const TOP = 'top'
export const LEFT = 'left'
export const BOTTOM = 'bottom'
export const RIGHT = 'right'
export const CENTER = 'center'
export const DEFAULT_ANIM_PROPS = {
  className: 'tooltip',
  timeIn: 1000,
  timeExit: 300
}
export const DEFAULT_CONFIG = {
  position: {
    x: null,
    y: null
  },
  closeButton: {
    active: false,
    actionOnClose: undefined,
    buttonChildren: null
  },
  overrideStyles: {
    button: '',
    wrap: '',
    container: '',
    arrowWrap: '',
    tooltipArrow: '',
    title: ''
  },
  customCoords: {
    active: false,
    left: null,
    top: null
  },
  manualCoodsFix: {
    fixX: null,
    fixY: null
  }
}
