import React, { PureComponent } from 'react'

import { IProps } from './types'
import styles from './styles.cssmodule.scss'

export class DebugInfo extends PureComponent<IProps> {
  _serverRenderedDOM = () => {
    const { debugMessage = {} } = this.props

    const isServerRenderedDOM = debugMessage.match && debugMessage.match(/<div[\s\S]*.*<\/div>/gim)

    return isServerRenderedDOM ? true : false
  }

  _getMessage = () => {
    const { debugMessage } = this.props

    const nonMarkupMessage = typeof debugMessage === 'object'

    return nonMarkupMessage ? JSON.stringify(debugMessage) : debugMessage
  }

  _renderMessage = () => {
    const { close } = this.props

    return (
      <div className={styles.wrapper}>
        <span dangerouslySetInnerHTML={{ __html: this._getMessage() }} />
        <button type='button' className={styles.close} onClick={close}>
          x
        </button>
      </div>
    )
  }

  _renderBeatifyError = () => {
    return (
      <div className='info-msg-cont border-round m-top10 red' style={{ marginBottom: '10px' }}>
        <div className={`info-msg border-round ${styles.messageWrap}`}>
          <i className='info-icon' />
          <div className='delimiter'>
            <div className={`msg right-round message_beauty`}>
              <span className='message_container'>{this._getMessage()}</span>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { debugMessage, isBeatifyError = false } = this.props

    console.error('Debug Message: ', debugMessage)

    if (this._serverRenderedDOM()) {
      return <div dangerouslySetInnerHTML={{ __html: debugMessage }} />
    }

    if (isBeatifyError) {
      return this._renderBeatifyError()
    }

    return this._renderMessage()
  }
}

export default DebugInfo
