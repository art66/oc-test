# DebugBox - Torn


## 1.2.2
 * Fixed layout in case of SPA Sidebar include in the App.

## 1.2.1
 * Added value checking in case of missing data.

## 1.2.0
 * Added max-height for beatified layout to prevent so huge container height appear.

## 1.1.3
 * Added margin-bottom: 10px for beatified layout version.

## 1.1.2
 * Added tablet width resolution for DebugBox.

## 1.1.1
 * Added width restriction to being used with the sidebar on the same screen.

## 1.1.0
 * Added beatified version of error layout.

## 1.0.0
 * First stabel release.
 * Added ability to render server DOM messages with markup.
 * Added tests.

## 0.0.1
 * DebugBox created.
