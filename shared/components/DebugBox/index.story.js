import React from 'react'
import { DEBUG_MESSAGE } from './mocks'
import DebugInfo from './index'

export const Defaults = () => <DebugInfo debugMessage={DEBUG_MESSAGE} />

Defaults.story = {
  name: 'defaults'
}

export default {
  title: 'Shared/DebugInfo'
}
