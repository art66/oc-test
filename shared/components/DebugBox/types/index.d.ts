export interface IProps {
  isBeatifyError?: boolean
  debugMessage: any
  close?: () => void
}
