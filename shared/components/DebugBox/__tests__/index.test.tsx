import React from 'react'
import { shallow } from 'enzyme'
import initialState, { objectMessage, stringMessage, domMessage } from './mocks'
import { DebugInfo } from '..'

describe('<DedugInfo />', () => {
  it('should render DebugInfo component with string message', () => {
    const Component = shallow(<DebugInfo {...initialState} />)

    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('span').length).toBe(1)
    expect(Component.find('span').html()).toBe('<span>testError</span>')
    expect(Component).toMatchSnapshot()
  })
  it('should render DebugInfo component with object message', () => {
    const Component = shallow(<DebugInfo {...objectMessage} />)

    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('span').length).toBe(1)
    expect(Component.find('span').html()).toEqual(`<span>${JSON.stringify(objectMessage.debugMessage)}</span>`)
    expect(Component).toMatchSnapshot()
  })
  it('should render DebugInfo component with string message', () => {
    const Component = shallow(<DebugInfo {...stringMessage} />)

    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('span').length).toBe(1)
    expect(Component.find('span').html()).toEqual(`<span>server string message</span>`)
    expect(Component).toMatchSnapshot()
  })
  it('should render DebugInfo component with beautified layout', () => {
    const Component = shallow(<DebugInfo {...stringMessage} isBeatifyError={true} />)

    expect(Component.find('div.info-msg-cont.border-round.m-top10.red').length).toBe(1)
    expect(Component.find('div.info-msg.border-round').length).toBe(1)
    expect(Component.find('i.info-icon').length).toBe(1)
    expect(Component.find('div.delimiter').length).toBe(1)
    expect(Component.find('div.msg.right-round.message_beauty').length).toBe(1)
    expect(Component.find('span.message_container').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render DebugInfo component with DOM message', () => {
    const Component = shallow(<DebugInfo {...domMessage} />)

    expect(Component.find('div').length).toBe(1)
    expect(Component.find('div').html()).toEqual(
      `<div><div class=\"msg\">server <div>hah</div>object message</div></div>`
    )
    expect(Component).toMatchSnapshot()
  })
})
