const initiailState = {
  close: () => {},
  debugMessage: 'testError'
}

export const objectMessage = {
  close: () => {},
  debugMessage: {
    error: 'server object message'
  }
}

export const stringMessage = {
  close: () => {},
  debugMessage: 'server string message'
}

export const domMessage = {
  close: () => {},
  debugMessage: '<div class="msg">server <div>hah</div>object message</div>'
}

export default initiailState
