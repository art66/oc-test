export const SLOTS_ORDER = {
  H: 0, // head
  E: 1, // eyes
  N: 2, // nose
  M: 3, // mouth
  P: 4, // neck
  B: 5, // body
  W: 6, // wrist
  R: 7, // finger
  D: 8, // hands
  L: 9, // legs
  F: 10 // feet
}

export const EQUIP_SLOTS_IDS = [1, 2, 3, 5]
export const WEAPON_IDS = [999, 1000]
