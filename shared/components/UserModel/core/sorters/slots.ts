import isArray from '@torn/shared/utils/isArray'

import compareSlots from '../helpers/compareSlots'
import { TSlots } from '../../interfaces'

const getTopmostSlot = (slotsMap: TSlots[]) => {
  if (!isArray(slotsMap) || !slotsMap.length) {
    return '' as TSlots
  }

  const getSlots = slotsMap.sort(compareSlots)

  return getSlots[slotsMap.length - 1]
}

export default getTopmostSlot
