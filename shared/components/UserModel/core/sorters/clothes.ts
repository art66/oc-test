import getTopmostSlot from './slots'
import addPresets from '../utils/addPresets'
import getItemsToRender from '../utils/getItemsToRender'

import compareSlots from '../helpers/compareSlots'
import extractItemFields from '../helpers/extractItemFields'
import { ISorter, TSlots } from '../../interfaces'

// items have a property `slotsMap` that looks like this
// '["B2"]'
// '["H3","H4"]'
// '["H5","B5","D5","L5","F5"]'
// where letter is a body part, like: N = nose, M = mouth
// and an index is equivalent to z-index
// the index has higher priority
export const clothesSorter = (props: ISorter) => {
  const { items, hairPreset, beardPreset, moustachePreset } = props

  // in case we use some presets config (for example in models-editor app)
  const presets = addPresets({ hairPreset, beardPreset, moustachePreset })
  const allItems = getItemsToRender(items, presets)
  const normalizedItems = allItems.map(extractItemFields)

  // Additional sorting for "special" items
  return normalizedItems
    .sort((a, b) => a.changedSlotsMap?.length - b.changedSlotsMap?.length)
    .sort((a, b) => a.slotsMap?.length - b.slotsMap?.length)
    .sort((a, b) => {
      // FIXME: figure out what is wrong here in future releases
      const prevSlot = a.changedSlotsMap?.length ? a.changedSlotsMap as unknown as TSlots[] : a.slotsMap as unknown as TSlots[]
      const nextSlot = b.changedSlotsMap?.length ? b.changedSlotsMap as unknown as TSlots[] : b.slotsMap as unknown as TSlots[]

      const aTopmost = getTopmostSlot(prevSlot)
      const bTopmost = getTopmostSlot(nextSlot)

      return compareSlots(aTopmost, bTopmost) || 0
    })
}

export default clothesSorter
