import { createSelector } from 'reselect'
import flow from 'lodash/flow'
import identity from 'lodash/identity'
import getEquippedArmour from './utils/getEquippedArmour'
import normalizeEquippedArmour from './utils/normalizeEquippedArmour'
import objectsToArray from './utils/objectsToArray'
import filterArmour from './helpers/filterArmour'
import sortArmour from './sorters/clothes'
import { IArmourSelectorProps, IItemToRender } from '../interfaces'

const selectArmour = (allItems, equippedIDs, equippedArmour) =>
  (allItems ? getEquippedArmour(allItems, equippedIDs) : normalizeEquippedArmour(equippedArmour))

const transformArmour = flow(selectArmour, filterArmour, objectsToArray)

export default createSelector<IArmourSelectorProps, IArmourSelectorProps, IItemToRender[]>(identity, props => {
  const { allItems, equippedIDs, equippedArmour, presets } = props
  const { beardPreset, hairPreset, moustachePreset } = presets || {}

  const items = transformArmour(allItems, equippedIDs, equippedArmour)

  return sortArmour({
    items,
    beardPreset,
    hairPreset,
    moustachePreset
  })
})
