import { IItem } from '../../interfaces'

const getEquippedArmour = (allItems: IItem[], equippedItemsIDs: number[] | string[]) => {
  const armourToRender = {}

  if (!allItems || !equippedItemsIDs) {
    return armourToRender
  }

  equippedItemsIDs.forEach((itemID: number | string) => {
    const armour = allItems.find((item: IItem) => Number(item.ID) === Number(itemID) || String(item.ID) === String(itemID))

    if (!armour) {
      return
    }

    armourToRender[itemID] = {
      item: [armour]
    }
  })

  return armourToRender
}

export default getEquippedArmour
