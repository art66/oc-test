import flattenArray from '@torn/shared/utils/arrayFlatter'

import { IArmourRaw, IItem } from '../../interfaces'

const objectsToArray = (itemsObject: IArmourRaw): IItem[] => {
  if (!itemsObject) {
    return []
  }

  const arrayWithinArray = Object.values(itemsObject).map(item => item.item)
  const arrayNormalized: IItem[] = arrayWithinArray?.flat ? arrayWithinArray.flat() : flattenArray(arrayWithinArray)

  return arrayNormalized
}

export default objectsToArray
