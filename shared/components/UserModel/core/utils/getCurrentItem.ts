import isObject from '@torn/shared/utils/isObject'

import { IItem } from '../../interfaces'

const getCurrentItem = (items: IItem[], item: IItem | string | number): IItem => {
  // in case of single item, legacy disoptimizartion
  if (isObject(item)) {
    return item as IItem
  }

  return items.find((itemInternal: IItem) => {
    return Number(itemInternal.ID) === Number(item) || String(itemInternal.ID) === String(item)
  })
}

export default getCurrentItem
