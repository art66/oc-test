import isEmpty from '../../../../utils/isEmpty'
import { IItem } from '../../interfaces'

const getItemsToRender = (equippedItems: IItem[], presetItems: IItem[]) => {
  if (!equippedItems) {
    return []
  }

  return !isEmpty(presetItems) ? equippedItems.concat(presetItems) : equippedItems
}

export default getItemsToRender
