import { IAddPreset } from '../../interfaces'

const addPresets = ({ hairPreset, beardPreset, moustachePreset }: IAddPreset) => {
  const presetArr = []

  if (hairPreset && hairPreset.path) {
    presetArr.push(hairPreset)
  }

  if (beardPreset && beardPreset.path) {
    presetArr.push(beardPreset)
  }

  if (moustachePreset && moustachePreset.path) {
    presetArr.push(moustachePreset)
  }

  return presetArr
}

export default addPresets
