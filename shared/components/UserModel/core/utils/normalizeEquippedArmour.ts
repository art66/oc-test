import pickBy from 'lodash/pickBy'
import mapKeys from 'lodash/mapKeys'
import { IArmourRaw } from '../../interfaces'

export default function normalizeEquippedArmour(equippedArmour: IArmourRaw): IArmourRaw {
  const filteredArmour = pickBy(equippedArmour, armour => armour && armour.item)

  return mapKeys(filteredArmour, armour => armour.item[0].ID)
}
