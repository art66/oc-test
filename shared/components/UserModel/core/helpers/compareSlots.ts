import { SLOTS_ORDER } from '../../constants'
import { TSlots } from '../../interfaces'

const compareSlots = (slotA: TSlots, slotB: TSlots) => {
  const aNumberIndex = Number(slotA.slice(1))
  const bNumberIndex = Number(slotB.slice(1))

  const aLetterIndex = SLOTS_ORDER[slotA.charAt(0)]
  const bLetterIndex = SLOTS_ORDER[slotB.charAt(0)]

  return aNumberIndex - bNumberIndex || bLetterIndex - aLetterIndex
}

export default compareSlots
