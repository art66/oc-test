import { EQUIP_SLOTS_IDS, WEAPON_IDS } from '../../constants'
import { IArmourRaw } from '../../interfaces'

const filterArmour = (items: IArmourRaw): IArmourRaw => {
  const filteredItems: IArmourRaw = {}

  if (!items) {
    return filteredItems
  }

  Object.keys(items).forEach((key: string | number) => {
    const armourItem = items[key]

    if (!armourItem || !armourItem.item) {
      return
    }

    const equipSlot = Number(armourItem && armourItem.item && armourItem.item[0].equipSlot)
    const ID = Number(armourItem && armourItem.item && armourItem.item[0].ID)

    if (EQUIP_SLOTS_IDS.includes(equipSlot) || WEAPON_IDS.includes(ID)) {
      return
    }

    filteredItems[key] = armourItem
  })

  return filteredItems
}

export default filterArmour
