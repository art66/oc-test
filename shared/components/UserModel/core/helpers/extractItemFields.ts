import { IItem, IItemToRender } from '../../interfaces'

const areaMap = {
  4: 'chest',
  6: 'helmet',
  7: 'pants',
  8: 'boots',
  9: 'gloves'
}

const extractItemFields = (item: IItem): IItemToRender => {
  if (!item) {
    console.error('Item or Armour doesn\'t found!: ', item)

    return {} as IItemToRender
  }

  return {
    ID: item.ID || null,
    maskID: item.slotsMapMask || null,
    hairMaskID: item.slotsMapMaskHair || null,
    slotsMap: JSON.parse(((item.slotsMap as unknown) as string) || '[]'),
    slotsMapBackground: item.slotsMapBackground || null,
    changedSlotsMap: JSON.parse(((item.changedSlotsMap as unknown) as string) || '[]'),
    path: item.path || null,
    area: areaMap[item.equipSlot],
    name: item.name
  }
}

export default extractItemFields
