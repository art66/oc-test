import React from 'react'

import { select, boolean } from '@storybook/addon-knobs'

import { ARMOUR, GENDER, EQUIPPED_ARMOUR_IDS, PRESETS } from './mocks/storybook'

import UserModel from '.'
import objectsToArray from './core/utils/objectsToArray'

export const Default = () => {
  return (
    <UserModel
      gender={select('Select Gender:', [GENDER.male, GENDER.female], GENDER.male)}
      withContainer={true}
      isDarkMode={boolean('Set Dark Mode:', false)}
    />
  )
}

Default.story = {
  name: 'default'
}

export const Joe = () => {
  return (
    <UserModel
      gender={select('Select Gender:', [GENDER.male, GENDER.female], GENDER.male)}
      equippedArmour={ARMOUR.joe}
      presets={null}
      withContainer={true}
      isDarkMode={boolean('Set Dark Mode:', false)}
    />
  )
}

Joe.story = {
  name: 'joe'
}

export const Pavel = () => {
  return (
    <UserModel
      gender={select('Select Gender:', [GENDER.male, GENDER.female], GENDER.male)}
      equippedArmour={ARMOUR.pavel}
      presets={null}
      withContainer={true}
      isDarkMode={boolean('Set Dark Mode:', false)}
    />
  )
}

Pavel.story = {
  name: 'pavel'
}

export const Sviat = () => {
  return (
    <UserModel
      gender={select('Select Gender:', [GENDER.male, GENDER.female], GENDER.male)}
      equippedArmour={ARMOUR.sviat}
      presets={null}
      withContainer={true}
      isDarkMode={boolean('Set Dark Mode:', false)}
    />
  )
}

Sviat.story = {
  name: 'sviat'
}

export const WithPresets = () => {
  return (
    <UserModel
      gender={select('Select Gender:', [GENDER.male, GENDER.female], GENDER.male)}
      equippedArmour={ARMOUR.sviat}
      presets={PRESETS}
      withContainer={true}
      isDarkMode={boolean('Set Dark Mode:', false)}
    />
  )
}

WithPresets.story = {
  name: 'with presets'
}

export const WithAllItems = () => {
  const allItems = objectsToArray(ARMOUR.pavel)

  return (
    <UserModel
      gender={select('Select Gender:', [GENDER.male, GENDER.female], GENDER.male)}
      allItems={allItems}
      equippedIDs={EQUIPPED_ARMOUR_IDS}
      withContainer={true}
      isDarkMode={boolean('Set Dark Mode:', false)}
    />
  )
}

export const WithDarkMode = () => {
  return (
    <UserModel
      gender={select('Select Gender:', [GENDER.male, GENDER.female], GENDER.male)}
      equippedArmour={ARMOUR.joe}
      presets={null}
      withContainer={true}
      isDarkMode={true}
    />
  )
}

WithDarkMode.story = {
  name: 'with dark mode'
}

export default {
  title: 'Shared/UserModel'
}
