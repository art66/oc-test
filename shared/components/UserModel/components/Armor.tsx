import React from 'react'
import isArray from '@torn/shared/utils/isArray'

import { IArmourToRender, IModelProps, IItemToRender } from '../interfaces'

import styles from '../styles/index.cssmodule.scss'

class Armor extends React.PureComponent<IArmourToRender & IModelProps> {
  _getHairMask = () => {
    const { armour } = this.props

    const hairMasks = armour.reduce(
      (accum: any, curr: any, index: number) => {
        const { ID: hairItemID = '', hairMaskID = '' } = curr

        if ((hairItemID as string)?.includes('hair')) {
          accum.index = index
        }

        if (hairMaskID) {
          accum.masks = [...accum.masks, hairMaskID]
        }

        return accum
      },
      { index: -1, masks: [] }
    )

    return hairMasks
  }

  _renderHairMask = (currentItemIndex: number) => {
    // this is a legacy on way to deal with hair masks
    // (it's a particular case only for hair). Should be refactored from back-end first.
    const { gender, isDarkMode } = this.props
    const { index, masks } = this._getHairMask()

    if (index !== currentItemIndex) {
      return null
    }

    const masksToRender = masks.map((hairMask: string) => {
      return (
        <div className={styles.hairMask} key={`${gender}-${hairMask}`}>
          <img
            alt=''
            className={styles.itemImg}
            src={`/images/v2/items/masks/${gender}-${hairMask}${isDarkMode ? '-d' : ''}.png`}
          />
        </div>
      )
    })

    return masksToRender
  }

  _renderArmour = () => {
    const { armour = [], gender, isDarkMode } = this.props

    const renderBackground = (slotsMapBackground: string) =>
      slotsMapBackground ? (
        <div className={styles.background}>
          <img
            alt=''
            className={`${styles.itemImg} ${styles.backgroundImage}`}
            src={`/images/v2/items/backgrounds/${gender}-${slotsMapBackground}.png`}
          />
        </div>
      ) : null

    const renderMask = (maskID: string | number) =>
      maskID ? (
        <div className={styles.mask}>
          <img
            alt=''
            className={styles.itemImg}
            src={`/images/v2/items/masks/${gender}-${maskID}${isDarkMode ? '-d' : ''}.png`}
          />
        </div>
      ) : null

    const renderItem = ({ itemID, itemPath }: { itemID: number | string; itemPath: string }) => {
      if (!itemID && !itemPath) {
        return null
      }

      // because in development we don't need user itemID to get the image
      // eventually we can achieve this by raw image link instead
      const pathUrl = `${itemPath}_${gender.charAt(0)}.png`
      const idUrl = `/getImage.php?ID=${itemID}&gender=${gender.charAt(0)}`

      return (
        <div className={styles.armour}>
          <img alt='' className={styles.itemImg} src={itemPath ? pathUrl : idUrl} />
        </div>
      )
    }

    // `${ID}_${path}` instead of pure "ID", because of the same reason above
    const armourToRender = armour.map(({ ID, path, slotsMapBackground, maskID }: IItemToRender, i) => (
      <div key={`${ID}_${path}`} className={styles.armourContainer} style={{ zIndex: 10 + i }}>
        {renderBackground(slotsMapBackground)}
        {renderMask(maskID)}
        {renderItem({ itemID: ID, itemPath: path })}
        {this._renderHairMask(i)}
      </div>
    ))

    return <div className={styles.armoursWrap}>{armourToRender}</div>
  }

  render() {
    const { armour, gender } = this.props

    if (!isArray(armour) || !gender) {
      console.error('Some error among armour: ', armour, 'or gender:', gender)

      return null
    }

    return this._renderArmour()
  }
}

export default Armor
