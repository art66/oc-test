/* eslint-disable max-len */
import React from 'react'
import { TArmourTitles } from '../interfaces'

type TAreaProps = {
  title: string
}

const AreaMap = {
  helmet: ({ title }: TAreaProps) => (
    <area alt='Helmet' title={title} coords='118,77,104,67,99,52,104,36,118,26,132,32,136,51,133,69' shape='poly' />
  ),
  chest: ({ title }: TAreaProps) => (
    <area
      alt='Chest'
      title={title}
      coords='119,79,99,73,80,96,62,131,54,150,52,167,62,169,79,138,91,118,99,142,95,159,143,161,144,143,148,118,162,141,174,166,187,165,176,129,162,95,140,75'
      shape='poly'
    />
  ),
  pants: ({ title }: TAreaProps) => (
    <area
      alt='Pants'
      title={title}
      coords='94,162,145,162,157,204,154,239,150,261,156,275,150,301,136,303,131,283,121,209,109,284,105,300,89,299,85,276,87,257,84,236,85,201'
      shape='poly'
    />
  ),
  gloves: ({ title }: TAreaProps) => (
    <>
      <area alt='Gloves' title={title} coords='48,203,55,192,62,195,67,192,61,172,50,169,44,183,40,203' shape='poly' />
      <area
        alt='Gloves'
        title={title}
        coords='175,171,189,170,196,185,198,200,191,202,184,191,177,196,176,180'
        shape='poly'
      />
    </>
  ),
  boots: ({ title }: TAreaProps) => (
    <>
      <area
        alt='Boots'
        title={title}
        coords='87,300,89,322,86,336,78,349,88,354,99,354,104,340,106,325,105,302'
        shape='poly'
      />
      <area
        alt='Boots'
        title={title}
        coords='136,304,153,300,151,318,153,330,160,343,153,352,138,353,132,330'
        shape='poly'
      />
    </>
  )
}

type TProps = {
  armourTitles: TArmourTitles
  associatedImage: string
}

export const TooltipsMap = ({ armourTitles, associatedImage }: TProps) => (
  <map name={associatedImage}>
    {armourTitles.map(({ name, title }) => {
      const Area = AreaMap[name]

      return Area ? <Area title={title} key={title} /> : null
    })}
  </map>
)
