import cn from 'classnames'
import React from 'react'
import { IProps } from '../interfaces'
import styles from '../styles/index.cssmodule.scss'
import Armor from './Armor'
import Model from './Model'

// using modelLayers to center effects and bullets to the model
class ModelLayers extends React.PureComponent<IProps> {
  getArmourTitles = () => this.props.armour.map(({ name, area }) => ({ name: area, title: name }))

  _renderModel = () => {
    const { gender } = this.props

    return <Model gender={gender} armourTitles={this.getArmourTitles()} />
  }

  _renderArmour = () => {
    const { armour, gender, isDarkMode } = this.props

    return <Armor armour={armour} gender={gender} isDarkMode={isDarkMode} />
  }

  _renderLayers = () => {
    return (
      <React.Fragment>
        {this._renderModel()}
        {this._renderArmour()}
      </React.Fragment>
    )
  }

  render() {
    const { classNameWrap, withContainer, isCentered } = this.props

    if (!withContainer) {
      return this._renderLayers()
    }

    return (
      <div className={cn(styles.modelWrap, classNameWrap, { [styles.centered]: isCentered })}>
        <div className={styles.modelLayers}>{this._renderLayers()}</div>
      </div>
    )
  }
}

export default ModelLayers
