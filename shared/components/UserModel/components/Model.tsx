import uniqueId from 'lodash/uniqueId'
import React from 'react'
import { IModelProps } from '../interfaces'
import styles from '../styles/index.cssmodule.scss'
import { TooltipsMap } from './TooltipsMap'

class Model extends React.PureComponent<IModelProps> {
  private uniqueId = uniqueId('map_')

  componentDidMount() {
    try {
      // @ts-ignore
      initializeTooltip(`.${styles.model}`, 'white-tooltip', { track: true })
    } catch (error) {
      console.error(error)
    }
  }

  render() {
    const { gender = 'male', armourTitles } = this.props

    return (
      <div className={styles.model}>
        <img alt='' useMap={`#${this.uniqueId}`} src={`/images/v2/attack/models/${gender}_model.png`} />
        {Boolean(armourTitles?.length) && <TooltipsMap armourTitles={armourTitles} associatedImage={this.uniqueId} />}
      </div>
    )
  }
}

export default Model
