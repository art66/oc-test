# UserModel - Torn


## 1.0.3
 * Added dark-mode integration.

## 1.0.2
 * Removed alt label because of the broken image.

## 1.0.1
 * Improved performance and stability.

## 1.0.0
 * First stable release.
