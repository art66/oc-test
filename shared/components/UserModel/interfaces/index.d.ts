export type TSlots = 'L1' | 'L2' | 'L3' | 'L4' | 'H1' | 'H2' | 'H3' | 'H4' | 'H5' | 'B1' | 'B2' | 'B3' | 'B4' | 'B5'
export type TGender = '' | 'male' | 'female'

export type TArmourTitles = {
  name: string
  title: string
}[]

export interface IModelProps {
  gender: TGender
  armourTitles?: TArmourTitles
}

export interface IPreset {
  value: string | number
  label: string
  path: string
}

export interface IAddPreset {
  hairPreset?: IPreset
  beardPreset?: IPreset
  moustachePreset?: IPreset
}

export interface IPresets {
  presets?: IAddPreset
}

export interface IArmour {
  armour: IItem[]
}

export interface IArmourToRender {
  armour: IItemToRender[]
  isDarkMode: boolean
}

export interface ISelectors extends IAddPreset {
  items: IItem[]
  equippedItems: IItem[]
}

export interface IItem {
  maskID?: number
  hairItemID?: string
  hairMaskID?: string
  armouryID: string
  armoryID: string
  userID: string
  itemID: string
  type: string
  equiped: string | number
  ammo: string
  type2: string
  dmg: number | string
  ID: string | number
  arm: string | number
  accuracy: string | number
  acc: number | string
  equipSlot: string
  name: string
  maxammo: string
  ammotype: string
  rofmin: string
  rofmax: string
  scary: string
  isDual: string
  isMask: string
  stealthLevel: string | number
  slotsMap: string
  changedSlotsMap: string
  slotsMapMask: string
  slotsMapMaskHair: string
  weptype: string
  upgrades: string
  armourNotCover: string
  slotsMapBackground: string
  roffixed: number
  finalAccuracy?: number
  maxmugs?: string | number
  rarity?: string
  quality?: string | number
  path?: string
  currentBonuses?: {
    [x: number]: {
      title: string
      hoverover: string
      icon: string
    }
  }
  currentUpgrades?: {
    title: string
    hoverover: string
    upgradeID: string
    icon: string
  }[]
}

type TArea = 'chest' | 'helmet' | 'pants' | 'boots' | 'gloves'

export interface IItemToRender {
  ID: string | number
  maskID: string | number
  hairMaskID: string | number
  slotsMap: TSlots[]
  slotsMapBackground: string
  changedSlotsMap: TSlots[]
  path: string
  area: TArea
  name: string
}

export interface IWeapon {
  type: string
  name: string
  dmg: number
  accuracy: number
  acc: number
  ID: number
  stealthLevel: number
}

export interface IHair {
  itemID: string
  ID: string
  equiped: number
  type: string
  type2: string
  slotsMap: string
}

export type TArmourRawItem = {
  item: IItem[] | IWeapon[] | IHair[]
}

export interface IArmourRaw {
  [x: number]: TArmourRawItem
}

export interface ISorter extends IAddPreset {
  items: IItem[]
  // equippedItems: IItem[]
}

export interface IProps extends IModelProps {
  classNameWrap?: string
  withContainer?: boolean
  armour: IItemToRender[]
  isDarkMode: boolean
  isCentered: boolean
}

export interface IModel extends IModelProps, IPresets {
  classNameWrap?: string
  withContainer?: boolean
  equippedArmour?: IArmourRaw
  allItems?: IItem[]
  equippedIDs?: number[] | string[]
  isDarkMode?: boolean
  isCentered?: boolean
}

export interface IArmourSelectorProps {
  allItems: IItem[]
  equippedIDs: number[] | string[]
  equippedArmour: IArmourRaw
  presets: IAddPreset
}
