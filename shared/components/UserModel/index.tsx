import React, { memo } from 'react'

import UserModel from './components'
import getArmourSelector from './core'

import { IModel } from './interfaces'

export default memo(
  ({
    classNameWrap,
    allItems,
    equippedIDs,
    equippedArmour,
    presets,
    gender,
    withContainer,
    isDarkMode,
    isCentered
  }: IModel) => {
    const armour = getArmourSelector({ allItems, equippedIDs, equippedArmour, presets })

    return (
      <UserModel
        classNameWrap={classNameWrap}
        armour={armour}
        gender={gender}
        withContainer={withContainer}
        isDarkMode={isDarkMode}
        isCentered={isCentered}
      />
    )
  }
)

export { UserModel }
