# Global UserModel Component

### Render the whole model male/female with all items/clothes on them.

  ** For the deeper look on schema or definitions, please, go into ./interfaces folder.

### Schema:

```
  <UserModel
    allItems={allItems} // optional, requires ${equippedIDs} to be able to work
    equippedIDs={equippedIDs} // optional, requires ${allItems} to be able to work
    equippedArmour={armour} // optional, can't be process if ${allItems} has been provided
    gender={gender} // required, ${'male' | 'female'} options to render corresponding model
    withContainer={false} // optional, set a rule about layout of the model
  />
```
