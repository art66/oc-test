export default interface IMember {
  playername: string
  userID: number
  factionId: number
  warFactionId: number
  level: number
  onlineStatus: {
    className: string
    status: string
  }
  factionTag: string
  factionTagImageUrl: string
  score: number
  attacks?: number
  status?: {
    text: string
    okay: boolean
  }
}
