declare global {
  interface Window {
    numberFormat: (num: number) => string
  }
}

const { numberFormat } = window

export const globalNumberFormat = (num, dec = 0) => {
  const n = Math.round(num * 10 ** dec) / 10 ** dec

  const res = numberFormat(n)

  const arr = res.split('.')

  if (arr.length === 1) {
    if (dec === 0) {
      return res
    }
    arr.push('')
  }
  const l = arr[1].length

  for (let i = l; i < dec; i++) {
    arr[1] += '0'
  }
  return arr.join('.')
}

export const capitalizeFirstLetter = (str) => (
  str.charAt(0).toUpperCase() + str.slice(1)
)
