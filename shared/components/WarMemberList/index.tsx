import React from 'react'
import cn from 'classnames'
import UserInfo from '@torn/shared/components/UserInfo'
import { globalNumberFormat as gnf, capitalizeFirstLetter as cfl } from './utils'
import IFaction from './interfaces/IFaction'
import IMember from './interfaces/IMember'
import s from './index.cssmodule.scss'

interface IProps {
  members: IMember[]
  currentFaction: IFaction
  opponentFaction: IFaction
  scoreFieldName: string
  mediaType: string
  isProfileMode: boolean
  isReport: boolean
  showImages: boolean
}

interface IState {
  opponentActive: boolean
  sorting: {
    field: string
    direction: string
  }
}

const ASC = 'ask'
const DESC = 'desc'

class WarMemberList extends React.Component<IProps, IState> {
  static defaultProps = {
    isReport: false,
    scoreFieldName: 'score'
  }

  constructor(props) {
    super(props)

    this.state = {
      opponentActive: true,
      sorting: {
        field: props.scoreFieldName,
        direction: DESC
      }
    }
  }

  getScore(factionID) {
    const { scoreFieldName, members } = this.props
    const sum = members
      .filter(member => member.warFactionId === factionID)
      .reduce((currentSum, member) => currentSum + member[scoreFieldName], 0)

    return Math.floor(sum)
  }

  _handleTabClick(sortField) {
    this.setState(prevState => ({
      sorting: {
        field: sortField,
        direction: prevState.sorting.direction === DESC && prevState.sorting.field === sortField ? ASC : DESC
      }
    }))
  }

  _getSortValue(member) {
    const {
      sorting: { field }
    } = this.state

    if (field === 'status') {
      return member[field].okay.toString() + member[field].text.toLowerCase()
    }

    if (typeof member[field] === 'string') {
      return member[field].toLowerCase()
    }

    return member[field]
  }

  _renderUserInfo(user) {
    const { showImages } = this.props
    const config = {
      showImages,
      useProgressiveImage: false,
      status: {
        mode: user.onlineStatus.status.toLowerCase()
      },
      user: {
        ID: user.userID,
        name: user.playername,
        imageUrl: user.honor,
        isSmall: true
      },
      faction: {
        ID: user.factionId,
        name: user.factionTag,
        rank: user.factionRank,
        imageUrl: user.factionTagImageUrl
      },
      customStyles: {
        status: s.customStatus,
        blockWrap: s.customBlockWrap
      }
    }

    return <UserInfo {...config} />
  }

  _renderMembers(factionID) {
    const { members, currentFaction, opponentFaction, scoreFieldName, isReport } = this.props
    const {
      sorting: { direction }
    } = this.state
    const enemy = opponentFaction.id === factionID
    const your = currentFaction.id === factionID

    let field2Sum = 0
    let field3Sum = 0

    /* eslint-disable no-nested-ternary */
    return (
      <ul className={cn('members-list', s.membersCont, { [s.report]: isReport })}>
        {[...members]
          .sort((a, b) => {
            const valueA = this._getSortValue(a)
            const valueB = this._getSortValue(b)

            if (valueA === valueB) {
              return 0
            }
            if (direction === ASC) {
              return valueA < valueB ? -1 : 1
            }
            if (direction === DESC) {
              return valueA > valueB ? -1 : 1
            }
            return 0
          })
          .filter(member => member.warFactionId === factionID)
          .map((member, index) => {
            field2Sum += isReport ? member.attacks : member[scoreFieldName]
            field3Sum += isReport ? member[scoreFieldName] : 0
            return (
              <li
                key={member.userID}
                className={cn({
                  enemy,
                  your,
                  last: !isReport && members.length === index + 1,
                  [s.enemy]: enemy,
                  [s.your]: your
                })}
              >
                <div className={cn('member', 'icons', 'left', s.member)}>{this._renderUserInfo(member)}</div>
                <div className={cn('level', 'left', s.level)}>{member.level}</div>
                <div className={cn('points', 'left', s.points)}>
                  {isReport ? member.attacks : gnf(member[scoreFieldName], 2)}
                </div>
                <div
                  className={cn('status', 'left', s.prevColumn, s.status, {
                    ok: member.status && member.status.okay,
                    'not-ok': member.status && !member.status.okay
                  })}
                >
                  {isReport ? gnf(member[scoreFieldName], 2) : member.status ? member.status.text : ''}
                </div>
                <div className={cn('attack', 'left', s.attack)}>
                  {member.status && member.status.okay ? (
                    <a
                      className={cn('t-blue', 'h', 'c-pointer')}
                      href={`loader2.php?sid=getInAttack&user2ID=${member.userID}`}
                    >
                      Attack
                    </a>
                  ) : (
                    <span className='t-gray-9'> Attack </span>
                  )}
                </div>
                <div className='clear' />
              </li>
            )
          })}

        {!isReport || (
          <li key='total'>
            <div className={cn('member', 'icons', 'left', s.member, s.total)}>
              <span>Total</span>
            </div>
            <div className={cn('level', 'left', s.level)}>--</div>
            <div className={cn('points', 'left', s.points)}>{gnf(field2Sum)}</div>
            <div className={cn('status', 'left', s.prevColumn, s.status)}>{gnf(field3Sum, 2)}</div>
          </li>
        )}
      </ul>
    )
  }

  _handleNameTabClick(isYour) {
    this.setState(prevState => ({
      opponentActive: !(isYour && prevState.opponentActive)
    }))
  }

  _renderFactionName(faction) {
    const { currentFaction } = this.props
    const { opponentActive } = this.state
    const isYour = faction.id === currentFaction.id

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div
        className={cn('name', s.name, {
          [s.current]: isYour,
          [s.opponent]: !isYour,
          [s.active]: isYour === !opponentActive,
          your: isYour,
          left: !isYour,
          enemy: !isYour,
          right: isYour
        })}
        onClick={() => this._handleNameTabClick(isYour)}
      >
        <div className={s.text} dangerouslySetInnerHTML={{ __html: faction.name }} />
        <div className={cn('score', s.score)}>{gnf(this.getScore(faction.id))}</div>
      </div>
    )
  }

  _renderMemberCont(faction) {
    const { isProfileMode, currentFaction, scoreFieldName, isReport } = this.props
    const {
      opponentActive,
      sorting: { field, direction }
    } = this.state
    const isYour = currentFaction.id === faction.id
    const dataColNames = isReport ? ['level', 'attacks', scoreFieldName] : ['level', scoreFieldName, 'status']

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    return (
      <div
        className={cn('tab-menu-cont', 'cont-gray', 'bottom-round', s.tabMenuCont, {
          'your-faction': isYour,
          'enemy-faction': !isYour,
          'profile-mode': isProfileMode,
          left: !isYour,
          right: isYour,
          [s.active]: isYour === !opponentActive
        })}
      >
        <div className={cn('members-cont', s.membersCont, { [s.profileMode]: isProfileMode })}>
          <div className='white-grad c-pointer'>
            <div className={cn('member', 'left', s.member, s.tab)} onClick={() => this._handleTabClick('playername')}>
              <span>Members</span>
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === 'playername' && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div className={cn('level', 'left', s.level, s.tab)} onClick={() => this._handleTabClick(dataColNames[0])}>
              {cfl(dataColNames[0])}
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === dataColNames[0] && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div
              className={cn('points', 'left', s.points, s.tab)}
              onClick={() => this._handleTabClick(dataColNames[1])}
            >
              {cfl(dataColNames[1])}
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === dataColNames[1] && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div
              className={cn('status', 'left', s.status, s.tab)}
              onClick={() => this._handleTabClick(dataColNames[2])}
            >
              {cfl(dataColNames[2])}
              <div
                className={cn(s.sortIcon, {
                  [s.activeIcon]: field === dataColNames[2] && direction !== '',
                  [s.desc]: direction === DESC,
                  [s.asc]: direction === ASC
                })}
              />
            </div>
            <div className={cn('attack', 'left', s.attack, s.tab)}>Attack</div>
            <div className='clear' />
          </div>
          {this._renderMembers(faction.id)}
        </div>
      </div>
    )
  }

  render() {
    const { currentFaction, opponentFaction } = this.props

    return (
      <div className={cn('faction-war', s.membersWrap)}>
        <div className='faction-names'>
          {this._renderFactionName(opponentFaction)}
          {this._renderFactionName(currentFaction)}
          <div className='clear' />
        </div>

        {this._renderMemberCont(opponentFaction)}
        {this._renderMemberCont(currentFaction)}

        <div className='clear' />
      </div>
    )
  }
}

export default WarMemberList
