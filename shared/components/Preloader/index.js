import React, { Component } from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

class Preloader extends Component {
  render() {
    const dots = [1, 2, 3, 4]

    return (
      <div className={cn(s.preloader, 'tornPreloader')}>
        <div className={s['dots']}>
          {dots.map((dot, i) => {
            return <span key={i} className={`${s.dot} ${s[`dot-${i + 1}`]}`} />
          })}
        </div>
      </div>
    )
  }
}

export default Preloader
