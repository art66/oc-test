import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ToolTip from 'react-portal-tooltip'
import { withTheme } from '../../hoc/withTheme/withTheme'
import './styles.scss'

class Tooltip extends Component {
  static defaultProps = {
    active: true,
    delay: 0,
    arrow: 'center',
    position: 'top',
    style: {
      style: {},
      arrowStyle: {}
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      isTooltipActive: false
    }
  }

  componentDidMount() {
    this.parent = document.getElementById(this.props.parent)
    if (!this.parent) {
      return
    }

    this.parent.addEventListener('blur', this.hideTooltip)
    this.parent.addEventListener('focus', this.showTooltip)
    this.parent.addEventListener('touchstart', this.showTooltip)
    this.parent.addEventListener('mouseleave', this.hideTooltip)
    this.parent.addEventListener('mouseenter', this.showTooltip)
  }

  componentWillUnmount() {
    if (!this.parent) {
      return
    }

    this.parent.removeEventListener('blur', this.hideTooltip)
    this.parent.removeEventListener('focus', this.showTooltip)
    this.parent.removeEventListener('touchstart', this.showTooltip)
    this.parent.removeEventListener('mouseleave', this.hideTooltip)
    this.parent.removeEventListener('mouseenter', this.showTooltip)
  }

  showTooltip = () => {
    this.setState({ isTooltipActive: true })
  }

  hideTooltip = () => {
    this.setState({ isTooltipActive: false })
  }

  render() {
    const { active, delay, children, position, arrow, parent, style, theme } = this.props
    const { isTooltipActive } = this.state
    const darkModeEnabled = theme === 'dark'
    const tooltipShadow = darkModeEnabled ? '0px 0px 2px rgba(0, 0, 0, 0.65)' : '0px 0px 8px rgba(0, 0, 0, 0.3)'
    const tooltipBg = darkModeEnabled ? '#444' : '#f2f2f2'
    const arrowBorder = darkModeEnabled ? 'rgba(0, 0, 0, 0.2)' : '#dadada'
    const tooltipStyle = {
      style: {
        background: tooltipBg,
        padding: 8,
        boxShadow: tooltipShadow,
        ...style.style
      },
      arrowStyle: {
        color: tooltipBg,
        borderColor: arrowBorder,
        transition: 'none',
        ...style.arrowStyle
      }
    }

    return (
      <ToolTip
        active={active && isTooltipActive}
        tooltipTimeout={delay}
        position={position}
        arrow={arrow}
        parent={`#${parent}`}
        style={tooltipStyle}
      >
        <div>{children}</div>
      </ToolTip>
    )
  }
}

Tooltip.propTypes = {
  active: PropTypes.bool,
  delay: PropTypes.number,
  children: PropTypes.node,
  position: PropTypes.string,
  arrow: PropTypes.string,
  parent: PropTypes.string, // id
  style: PropTypes.object,
  theme: PropTypes.string
}

/*
 * @param Compoennt Name - React-Based Tooltip
 *
 * Legacy Tooltip for Torn's React Apps.
 *
 * This Component is deprecated and will be removed soon. Make sure you don't use it in your App anymore.
 *
 * @see /@torn/shared/components/TooltipNew for a fresh one React-Based Tooltip.
 * @type protocol
 * @deprecated
 *
 */

export default withTheme(Tooltip)
