import React from 'react'
import { text } from '@storybook/addon-knobs'

import PictureGenerator from './index'

export const Defaults = () => {
  const config = {
    alt: text('alt text', 'Picture Set'),
    classes: {
      picture: text('container class', 'picture')
    },
    path: text('Path to Image', 'public/assets/img/retina/torn_logo/regular/'),
    name: text('Image Name', 'logo'),
    type: text('Image Type', 'png')
  }

  return <PictureGenerator {...config} />
}

Defaults.story = {
  name: 'defaults'
}

export const OnlyTablet = () => {
  const config = {
    path: text('Path to Image', 'public/assets/img/retina/torn_logo/regular/'),
    name: text('Image Name', 'logo'),
    type: text('Image Type', 'png'),
    extraResolutions: {
      tablet: text('extra resolution 1:', 'min-width: 600px')
    }
  }

  return <PictureGenerator {...config} />
}

OnlyTablet.story = {
  name: 'only tablet'
}

export const OnlyMobile = () => {
  const config = {
    path: text('Path to Image', 'public/assets/img/retina/torn_logo/regular/'),
    name: text('Image Name', 'logo'),
    type: text('Image Type', 'png'),
    extraResolutions: {
      mobile: text('extra resolution 1:', 'min-width: 600px')
    }
  }

  return <PictureGenerator {...config} />
}

OnlyMobile.story = {
  name: 'only mobile'
}

export const OnlyDesktop = () => {
  const config = {
    path: text('Path to Image', 'public/assets/img/retina/torn_logo/regular/'),
    name: text('Image Name', 'logo'),
    type: text('Image Type', 'png'),
    extraResolutions: {
      desktop: text('extra resolution 1:', 'min-width: 600px')
    }
  }

  return <PictureGenerator {...config} />
}

OnlyDesktop.story = {
  name: 'only desktop'
}

export const OnlyTabletAndMobile = () => {
  const config = {
    path: text('Path to Image', 'public/assets/img/retina/torn_logo/regular/'),
    name: text('Image Name', 'logo'),
    type: text('Image Type', 'png'),
    extraResolutions: {
      tablet: text('extra resolution 1:', 'min-width: 601px'),
      mobile: text('extra resolution 2:', 'max-width: 600px')
    }
  }

  return <PictureGenerator {...config} />
}

OnlyTabletAndMobile.story = {
  name: 'only tablet and mobile'
}

export const OnlyDesktopAndMobile = () => {
  const config = {
    path: text('Path to Image', 'public/assets/img/retina/torn_logo/regular/'),
    name: text('Image Name', 'logo'),
    type: text('Image Type', 'png'),
    extraResolutions: {
      desktop: text('extra resolution 1:', 'min-width: 601px'),
      mobile: text('extra resolution 2:', 'max-width: 600px')
    }
  }

  return <PictureGenerator {...config} />
}

OnlyDesktopAndMobile.story = {
  name: 'only desktop and mobile'
}

export default {
  title: 'Shared/PictureGenerator'
}
