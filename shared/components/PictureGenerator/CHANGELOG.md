# PictureGenerator - Torn


## 2.1.1
 * Updated storybook config.
 * Fixed memory leak.

## 2.0.0
 * Written tests suits.
 * Moved to the shared/Components.
 * Added PureComponent extending way.

## 1.0.0
 * First stable release.
