# PictureGenerator Util

How to use:
  - Quick Start:
  `{path}, {name}, {type}` are only required props to start image generation. You can don't keep in mind the pixels and resolutions to render, by defoult it will always render images in whole 1-4x pixel resolutions for all mediaBreakpoints (desktop, tablet, mobile).

```
  const config = {
    path: {path_to_image_on_dev},
    name: {name_of_the_image},
    type: {image_type}
  }

  <ProgressiveImagesSetGenerator {...config} />
```

  - Advanced:
  In case when you need to set up a better custom config you can throw props as:
   - `{classes: { picture, img }}` object for `<picture />` and `<img />` tags.
   - `{alt}` for an alt img tag.
   - `{extraResolutions: { desktop, tablet, mobile }}` for the cusom resolutions breakpoints (see JSDoc of the index.tsx for info).
```
  const config = {
    path: {path_to_image_on_dev},
    name: {name_of_the_image},
    type: {image_type}
  }

  <ProgressiveImagesSetGenerator {...config} />
```
