const initialState = {
  value: 1,
  srcsetArr: ['setOne 1x', 'setTwo 2x', 'setThree 3x']
}

export default initialState
