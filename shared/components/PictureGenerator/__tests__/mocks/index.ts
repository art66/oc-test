const initialState = {
  type: 'png',
  path: 'images/v2/retina/',
  name: 'image',
  alt: '',
  classes: {
    pictrure: '',
    img: ''
  }
}

export const onlyImage = {
  ...initialState,
  extraResolutions: {}
}

export const onlyTablet = {
  ...initialState,
  extraResolutions: {
    tablet: 'min-width: 1000px'
  }
}

export const onlyMobile = {
  ...initialState,
  extraResolutions: {
    mobile: 'min-width: 600px'
  }
}

export const onlyDesktopMobile= {
  ...initialState,
  extraResolutions: {
    desktop: 'min-width: 601px',
    mobile: 'max-width: 600px'
  }
}

export const fullResolutions = {
  desktop: 'min-width: 1001px',
  tablet: 'max-width: 1000px',
  mobile: 'max-width: 600px'
}

export default initialState
