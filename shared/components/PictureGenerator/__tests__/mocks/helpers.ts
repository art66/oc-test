
import { ISourceGenerator, ISrcIterator, ISrcCreator } from '../../types'

const initialState = {
  srcCreator: {
    name: 'logo',
    path: 'images/v2/torn_logo/',
    pixelDensity: 1,
    srcWithPixelsPostfic: true,
    layout: 'tablet',
    type: 'png'
  },
  srcIterator: {
    name: 'logo',
    path: 'images/v2/torn_logo/',
    layout: 'tablet',
    type: 'png',
    isMain: false
  },
  sourcesGenerator: {
    name: 'logo',
    path: 'images/v2/torn_logo/',
    mediaBreakpoint: 'min-width: 1001px',
    layout: 'tablet',
    type: 'png'
  }
}

export const srcCreatorWithoutPostfics: ISrcCreator = {
  ...initialState.srcCreator,
  srcWithPixelsPostfic: false
}

export const srcCreatorDefsults: ISrcCreator = {
  ...initialState.srcCreator,
  name: 'logo',
  path: 'images/v2/torn_logo/'
}

export const srcIteratorFull: ISrcIterator = {
  ...initialState.srcIterator
}

export const srcIteratorMain: ISrcIterator = {
  ...initialState.srcIterator,
  isMain: true
}

export const sourcesGeneratorMain: ISourceGenerator = {
  ...initialState.sourcesGenerator
}

export const sourcesGeneratorWithoutBreakpoint: ISourceGenerator = {
  ...initialState.sourcesGenerator,
  mediaBreakpoint: ''
}

export default initialState
