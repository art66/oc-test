import initialState from './mocks/utils'

import { normalizedDensity, stringifySrcSet } from '../utils'

describe('PictureGenerator > utils', () => {
  it('should return normalized array value for pixel destinity', () => {
    expect(normalizedDensity(initialState.value)).toBe(2)
  })
  it('should return stringified srcset consisted of array\'s values', () => {
    expect(stringifySrcSet(initialState.srcsetArr)).toBe('setOne 1x, setTwo 2x, setThree 3x')
  })
})
