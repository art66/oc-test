import React from 'react'
import { mount } from 'enzyme'

import PictureGenerator from '../index'

import initialState, { onlyMobile, onlyTablet, onlyImage, onlyDesktopMobile, fullResolutions } from './mocks'
import {
  DESKTOP_SOURCE_SRCSET,
  TABLET_SOURCE_SRCSET,
  MOBILE_SOURCE_SRCSET,
  MAIN_IMG_SRCSET,
  MAIN_MG_SRC
} from './constants'

describe('PictureGenerator > index', () => {
  it('srcCreator should return regular set of <picture /> component with .png images and sources', () => {
    const { name, path, type, alt, classes } = initialState

    const ComponentPicture = mount(
      <PictureGenerator
        name={name}
        path={path}
        type={type}
        alt={alt}
        classes={classes}
      />
    )

    expect(ComponentPicture.props()).toEqual({ name, path, type, alt, classes, extraResolutions: fullResolutions })

    expect(ComponentPicture.find('picture').length).toBe(1)
    expect(ComponentPicture.find('img').length).toBe(1)
    expect(ComponentPicture.find('source').length).toBe(3)

    expect(ComponentPicture.find('source').at(0).prop('media')).toBe('(min-width: 1001px)')
    expect(ComponentPicture.find('source').at(1).prop('media')).toBe('(max-width: 1000px)')
    expect(ComponentPicture.find('source').at(2).prop('media')).toBe('(max-width: 600px)')

    expect(ComponentPicture.find('source').at(0).prop('srcSet')).toBe(DESKTOP_SOURCE_SRCSET)
    expect(ComponentPicture.find('source').at(1).prop('srcSet')).toBe(TABLET_SOURCE_SRCSET)
    expect(ComponentPicture.find('source').at(2).prop('srcSet')).toBe(MOBILE_SOURCE_SRCSET)

    expect(ComponentPicture.find('img').prop('alt')).toBe('')
    expect(ComponentPicture.find('img').prop('className')).toBe('')
    expect(ComponentPicture.find('img').prop('src')).toBe(MAIN_MG_SRC)
    expect(ComponentPicture.find('img').prop('srcSet')).toBe(MAIN_IMG_SRCSET)

    expect(ComponentPicture).toMatchSnapshot()
  })
  it('srcCreator should <picture /> only with <img /> tag included', () => {
    const { name, path, type, extraResolutions, alt, classes } = onlyImage

    const ComponentPicture = mount(
      <PictureGenerator
        name={name}
        path={path}
        type={type}
        alt={alt}
        classes={classes}
        extraResolutions={extraResolutions}
      />
    )

    expect(ComponentPicture.props()).toEqual({ name, path, type, alt, classes, extraResolutions })

    expect(ComponentPicture.find('picture').length).toBe(1)
    expect(ComponentPicture.find('img').length).toBe(1)
    expect(ComponentPicture.find('source').length).toBe(0)

    expect(ComponentPicture.find('img').prop('alt')).toBe('')
    expect(ComponentPicture.find('img').prop('className')).toBe('')
    expect(ComponentPicture.find('img').prop('src')).toBe(MAIN_MG_SRC)
    expect(ComponentPicture.find('img').prop('srcSet')).toBe(MAIN_IMG_SRCSET)

    expect(ComponentPicture).toMatchSnapshot()
  })
  it('srcCreator should <picture /> with only tablet source', () => {
    const { name, path, type, extraResolutions, alt, classes } = onlyTablet

    const ComponentPicture = mount(
      <PictureGenerator
        name={name}
        path={path}
        type={type}
        alt={alt}
        classes={classes}
        extraResolutions={extraResolutions}
      />
    )

    expect(ComponentPicture.props()).toEqual({ name, path, type, alt, classes, extraResolutions })

    expect(ComponentPicture.find('picture').length).toBe(1)
    expect(ComponentPicture.find('img').length).toBe(1)
    expect(ComponentPicture.find('source').length).toBe(1)

    expect(ComponentPicture.find('source').at(0).prop('media')).toBe('(min-width: 1000px)')
    expect(ComponentPicture.find('source').at(0).prop('srcSet')).toBe(TABLET_SOURCE_SRCSET)

    expect(ComponentPicture.find('img').prop('alt')).toBe('')
    expect(ComponentPicture.find('img').prop('className')).toBe('')
    expect(ComponentPicture.find('img').prop('src')).toBe(MAIN_MG_SRC)
    expect(ComponentPicture.find('img').prop('srcSet')).toBe(MAIN_IMG_SRCSET)

    expect(ComponentPicture).toMatchSnapshot()
  })
  it('srcCreator should <picture /> with only mobile source', () => {
    const { name, path, type, extraResolutions, alt, classes } = onlyMobile

    const ComponentPicture = mount(
      <PictureGenerator
        name={name}
        path={path}
        type={type}
        alt={alt}
        classes={classes}
        extraResolutions={extraResolutions}
      />
    )

    expect(ComponentPicture.props()).toEqual({ name, path, type, alt, classes, extraResolutions })

    expect(ComponentPicture.find('picture').length).toBe(1)
    expect(ComponentPicture.find('img').length).toBe(1)
    expect(ComponentPicture.find('source').length).toBe(1)

    expect(ComponentPicture.find('source').at(0).prop('media')).toBe('(min-width: 600px)')
    expect(ComponentPicture.find('source').at(0).prop('srcSet')).toBe(MOBILE_SOURCE_SRCSET)

    expect(ComponentPicture.find('img').prop('alt')).toBe('')
    expect(ComponentPicture.find('img').prop('className')).toBe('')
    expect(ComponentPicture.find('img').prop('src')).toBe(MAIN_MG_SRC)
    expect(ComponentPicture.find('img').prop('srcSet')).toBe(MAIN_IMG_SRCSET)

    expect(ComponentPicture).toMatchSnapshot()
  })
  it('srcCreator should <picture /> with only desktop and mobile sources', () => {
    const { name, path, type, extraResolutions, alt, classes } = onlyDesktopMobile

    const ComponentPicture = mount(
      <PictureGenerator
        name={name}
        path={path}
        type={type}
        alt={alt}
        classes={classes}
        extraResolutions={extraResolutions}
      />
    )

    expect(ComponentPicture.props()).toEqual({ name, path, type, alt, classes, extraResolutions })

    expect(ComponentPicture.find('picture').length).toBe(1)
    expect(ComponentPicture.find('img').length).toBe(1)
    expect(ComponentPicture.find('source').length).toBe(2)

    expect(ComponentPicture.find('source').at(0).prop('media')).toBe('(min-width: 601px)')
    expect(ComponentPicture.find('source').at(1).prop('media')).toBe('(max-width: 600px)')

    expect(ComponentPicture.find('source').at(0).prop('srcSet')).toBe(DESKTOP_SOURCE_SRCSET)
    expect(ComponentPicture.find('source').at(1).prop('srcSet')).toBe(MOBILE_SOURCE_SRCSET)

    expect(ComponentPicture.find('img').prop('alt')).toBe('')
    expect(ComponentPicture.find('img').prop('className')).toBe('')
    expect(ComponentPicture.find('img').prop('src')).toBe(MAIN_MG_SRC)
    expect(ComponentPicture.find('img').prop('srcSet')).toBe(MAIN_IMG_SRCSET)

    expect(ComponentPicture).toMatchSnapshot()
  })
  it('srcCreator should <picture /> only with <img /> tag included in case of manual desktop mode activated', () => {
    const { name, path, type, alt, classes } = initialState

    const ComponentPicture = mount(
      <PictureGenerator
        name={name}
        path={path}
        type={type}
        alt={alt}
        classes={classes}
      />
    )

    ComponentPicture.setState({ isManualDesktopMode: true })
    expect(ComponentPicture.state('isManualDesktopMode')).toBeTruthy()
    expect(ComponentPicture.props()).toEqual({ alt, classes, name, path, type, extraResolutions: fullResolutions })

    expect(ComponentPicture.find('picture').length).toBe(1)
    expect(ComponentPicture.find('img').length).toBe(1)
    expect(ComponentPicture.find('source').length).toBe(1)

    expect(ComponentPicture.find('source').at(0).prop('srcSet')).toBe(DESKTOP_SOURCE_SRCSET)

    expect(ComponentPicture.find('img').prop('alt')).toBe('')
    expect(ComponentPicture.find('img').prop('className')).toBe('')
    expect(ComponentPicture.find('img').prop('src')).toBe(MAIN_MG_SRC)
    expect(ComponentPicture.find('img').prop('srcSet')).toBe(MAIN_IMG_SRCSET)

    expect(ComponentPicture).toMatchSnapshot()
  })
})
