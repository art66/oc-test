import React from 'react'
import { shallow } from 'enzyme'
import initialState, {
  srcCreatorDefsults,
  srcCreatorWithoutPostfics,
  srcIteratorMain,
  srcIteratorFull,
  sourcesGeneratorMain,
  sourcesGeneratorWithoutBreakpoint
} from './mocks/helpers'

import {
  srcCreator,
  srcIterator,
  sourcesGenerator as SourcesGenerator
} from '../helpers'

import {
  SRC_CREATOR_RESULT,
  SRC_ITERATOR_RESULT,
  SRC_ITERATOR_RESULT_FULL,
  SOURCE_GENERETOR_RESULT,
  SOURCE_GENERETOR_WITHOUT_BREAKPOINTS_RESULT
} from './constants/helpers'

describe('PictureGenerator > helpers', () => {
  it('srcCreator should return basic generater srcset version', () => {
    expect(srcCreator(initialState.srcCreator)).toBe(`${SRC_CREATOR_RESULT} 1x`)
  })
  it('srcCreator should return srcset by reqiured props throw only', () => {
    expect(srcCreator(srcCreatorDefsults)).toBe(`${SRC_CREATOR_RESULT} 1x`)
  })
  it('srcCreator should return srcset verion without postfics pixels', () => {
    expect(srcCreator(srcCreatorWithoutPostfics)).toBe(SRC_CREATOR_RESULT)
  })
  it('srcIterator should return regular full 4 verions srcset', () => {
    expect(srcIterator(srcIteratorFull)).toEqual(SRC_ITERATOR_RESULT_FULL)
  })
  it('srcCreator should return regular srcset without main 1x verion for the img srcset only', () => {
    expect(srcIterator(srcIteratorMain)).toEqual(SRC_ITERATOR_RESULT)
  })
  it('sourcesGenerator should a <source> tag with srcset including', () => {
    const { name, path, mediaBreakpoint, layout, type } = sourcesGeneratorMain

    const Component = shallow(
      <SourcesGenerator
        name={name}
        path={path}
        mediaBreakpoint={mediaBreakpoint}
        layout={layout}
        type={type}
      />
    )

    expect(Component.debug()).toBe(SOURCE_GENERETOR_RESULT)
    expect(Component).toMatchSnapshot()
  })
  it('sourcesGenerator should a <source> tag with srcset including by the way without media break rule', () => {
    const { name, path, mediaBreakpoint, layout, type } = sourcesGeneratorWithoutBreakpoint

    const Component = shallow(
      <SourcesGenerator
        name={name}
        path={path}
        mediaBreakpoint={mediaBreakpoint}
        layout={layout}
        type={type}
      />
    )

    expect(Component.debug()).toBe(SOURCE_GENERETOR_WITHOUT_BREAKPOINTS_RESULT)
    expect(Component).toMatchSnapshot()
  })
})
