export const SRC_CREATOR_RESULT = 'images/v2/torn_logo/tablet/logo@1x.png'
export const SRC_ITERATOR_RESULT = [
  'images/v2/torn_logo/tablet/logo@2x.png 2x',
  'images/v2/torn_logo/tablet/logo@3x.png 3x',
  'images/v2/torn_logo/tablet/logo@4x.png 4x'
]
export const SRC_ITERATOR_RESULT_FULL = ['images/v2/torn_logo/tablet/logo@1x.png 1x', ...SRC_ITERATOR_RESULT]
export const SOURCE_GENERETOR_RESULT = '<source type="image/png" media="(min-width: 1001px)" srcSet="images/v2/torn_logo/tablet/logo@1x.png 1x, images/v2/torn_logo/tablet/logo@2x.png 2x, images/v2/torn_logo/tablet/logo@3x.png 3x, images/v2/torn_logo/tablet/logo@4x.png 4x" />'
export const SOURCE_GENERETOR_WITHOUT_BREAKPOINTS_RESULT = '<source type="image/png" media="" srcSet="images/v2/torn_logo/tablet/logo@1x.png 1x, images/v2/torn_logo/tablet/logo@2x.png 2x, images/v2/torn_logo/tablet/logo@3x.png 3x, images/v2/torn_logo/tablet/logo@4x.png 4x" />'
