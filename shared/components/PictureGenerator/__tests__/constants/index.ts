export const DESKTOP_SOURCE_SRCSET = 'images/v2/retina/desktop/image@1x.png 1x, images/v2/retina/desktop/image@2x.png 2x, images/v2/retina/desktop/image@3x.png 3x, images/v2/retina/desktop/image@4x.png 4x'
export const TABLET_SOURCE_SRCSET = 'images/v2/retina/tablet/image@1x.png 1x, images/v2/retina/tablet/image@2x.png 2x, images/v2/retina/tablet/image@3x.png 3x, images/v2/retina/tablet/image@4x.png 4x'
export const MOBILE_SOURCE_SRCSET = 'images/v2/retina/mobile/image@1x.png 1x, images/v2/retina/mobile/image@2x.png 2x, images/v2/retina/mobile/image@3x.png 3x, images/v2/retina/mobile/image@4x.png 4x'

export const MAIN_IMG_SRCSET = 'images/v2/retina/desktop/image@2x.png 2x, images/v2/retina/desktop/image@3x.png 3x, images/v2/retina/desktop/image@4x.png 4x'
export const MAIN_MG_SRC = 'images/v2/retina/desktop/image@1x.png'
