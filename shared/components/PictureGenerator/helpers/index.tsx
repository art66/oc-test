import React from 'react'

import {
  normalizedDensity,
  stringifySrcSet
} from '../utils'

import {
  WEBP_FORMAT,
  WEBP_SUPPORT,
  DESKTOP_LAYOUT,
  DESKTOP_PIXEL_DESTINY,
  PIXELS_DENSITY_COUNT
} from '../constants'

import {
  ISrcCreator,
  ISrcIterator,
  ISourceGenerator
} from '../types'

const srcCreator = (srcProps: ISrcCreator) => {
  const {
    name,
    path,
    pixelDensity = DESKTOP_PIXEL_DESTINY,
    srcWithPixelsPostfic = true,
    layout = DESKTOP_LAYOUT,
    type = WEBP_FORMAT
  } = srcProps

  const src = `${path}${layout}/${name}@${pixelDensity}x.${type}`
  const srcWithPostfics = `${src} ${pixelDensity}x`

  return srcWithPixelsPostfic ? srcWithPostfics : src
}

const srcIterator = (srcProps: ISrcIterator) => {
  const { name, path, layout, type, isMain = false } = srcProps

  const srcList = []
  const iterationArray = Array.from(Array(PIXELS_DENSITY_COUNT).keys())

  iterationArray.forEach(pixelDensity => {
    const mainRegularPixelDestiny = isMain && pixelDensity === 0

    if (mainRegularPixelDestiny) {
      return
    }

    srcList.push(srcCreator({ name, path, pixelDensity: normalizedDensity(pixelDensity), layout, type }))
  })

  return srcList
}

const sourcesGenerator = (sourceProps: ISourceGenerator) => {
  const { name, path, mediaBreakpoint, layout, type } = sourceProps

  const imageFormat = !WEBP_SUPPORT && type ? type : WEBP_FORMAT
  const layoutBreakpoint = mediaBreakpoint ? `(${mediaBreakpoint})` : ''
  const srcset = srcIterator({ name, path, layout, type: imageFormat })

  return (
    <source
      key={layoutBreakpoint}
      type={`image/${imageFormat}`}
      media={layoutBreakpoint}
      srcSet={stringifySrcSet(srcset)}
    />
  )
}

export {
  srcCreator,
  srcIterator,
  sourcesGenerator
}
