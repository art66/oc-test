interface IProps {
  alt?: string,
  type: string,
  path: string,
  name: string,
  extraResolutions?: {
    desktop?: string,
    tablet?: string,
    mobile?: string
  },
  classes?: {
    picture?: string,
    img?: string
  }
}

interface ISrcProps {
  name: string,
  path: string,
  layout: string,
  type: string
}

interface ISrcCreator {
  name: ISrcProps['name'],
  path: ISrcProps['path'],
  type: ISrcProps['type'],
  layout?: ISrcProps['layout'],
  pixelDensity?: number,
  srcWithPixelsPostfic?: boolean
}

interface ISrcIterator extends ISrcProps {
  isMain?: boolean
}

interface ISourceGenerator extends ISrcProps {
  mediaBreakpoint?: string
}

export {
  IProps,
  ISrcCreator,
  ISrcIterator,
  ISourceGenerator
}
