/**
*  @name ProgressiveImagesSetGEnerator
*  @author 3p-sviat
*  @version 1.0.0
*  @description creates <picture /> set with smart logic based on manual layout and browser webP images format support
*  @params className = { picture: {string}, img: {string} } // non-required
*  @params alt = {string} // non-required
*  @params path = {string}
*  @params name = {string}
*  @params type = {string}
*  @params extraResolutions = { {string}: {string}, {string}: {string}, {string}: {string} } // non-required,
*          key - mediaType, value - mediaValue
*  @returns valid React-based {html} markup <picture /> tag
*
*  @copyright Copyright (c) Torn, LTD.
*/

import React from 'react'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'

import { stringifySrcSet } from './utils'
import { srcCreator, srcIterator, sourcesGenerator } from './helpers'

import { DESKTOP_LAYOUT } from './constants'

import { IProps } from './types'

class PictureGenerator extends React.PureComponent<IProps, any> {
  static defaultProps: IProps = {
    classes: {
      picture: '',
      img: ''
    },
    alt: '',
    type: 'png',
    path: 'images/v2/retina/',
    name: 'image',
    extraResolutions: {
      desktop: 'min-width: 1001px',
      tablet: 'max-width: 1000px',
      mobile: 'max-width: 600px'
    }
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      isManualDesktopMode: false
    }
  }

  componentDidMount() {
    subscribeOnDesktopLayout(manualStatus => this._checkManualMode(manualStatus))
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  _checkManualMode = (status: string) => {
    this.setState({
      isManualDesktopMode: status
    })
  }

  _sources = () => {
    const { isManualDesktopMode } = this.state
    const { name, path, type, extraResolutions } = this.props

    const sourcesArr = []
    const responsiveSourcesArray = extraResolutions && Object.keys(extraResolutions) || []
    const configGenerator = (key: string) => ({
      name,
      path,
      mediaBreakpoint: extraResolutions[key],
      layout: key,
      type
    })

    if (!isManualDesktopMode && responsiveSourcesArray) {
      responsiveSourcesArray.forEach(key => {
        sourcesArr.push(sourcesGenerator(configGenerator(key)))
      })
    } else {
      const firstAvailableResolution = configGenerator(responsiveSourcesArray[0])

      sourcesArr.push(sourcesGenerator(firstAvailableResolution))
    }

    return sourcesArr
  }

  _img = () => {
    const { classes, alt, name, path, type } = this.props

    const mainSrcSet = () => srcIterator({ name, path, layout: DESKTOP_LAYOUT, type, isMain: true })
    const mainSrc = () => srcCreator({ name, path, type, srcWithPixelsPostfic: false })

    return (
      <img
        alt={alt}
        className={classes.img}
        src={mainSrc()}
        srcSet={stringifySrcSet(mainSrcSet())}
      />
    )
  }

  render() {
    const { classes } = this.props

    if (!this.props || Object.keys(this.props).length === 0) {
      console.error('PROGRESIVE_IMAGES_SET_GENERATOR - YOUR MUST PROVIDE A CONFIG FIRST!')
      return null
    }

    return (
      <picture className={classes.picture}>
        {this._sources()}
        {this._img()}
      </picture>
    )
  }
}

export default PictureGenerator
