import { IOwnBoughtProps } from './IBoughtInfo'
import { IOwnConfirmationProps } from './IBuyConfirmation'
import { IOwnErrorProps } from './IError'
import { ICommon, IEventHandlers } from './ICommon'

export interface IDefaultProps {
  tradeLabel: string
}

export interface IProps extends ICommon, IEventHandlers {
  isDealComplete: boolean
  isFetchActive: boolean
  isError: boolean
  tradeLabel?: string
  buyConfirm?: IOwnConfirmationProps
  boughtInfo: IOwnBoughtProps
  error?: IOwnErrorProps
  classes?: {
    wrapCustomClass?: string
  }
}


// export interface IState {
//   isFetchActive: boolean
//   isDealComplete: boolean
// }
