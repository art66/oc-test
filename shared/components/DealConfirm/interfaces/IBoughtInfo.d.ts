import { ICommon } from './ICommon'

export interface IControlFlags {
  isOwnerDisabled?: boolean
  isTradingPlaceDisabled?: boolean
  isSuffixLabelDisabled?: boolean
  isCloseButtonDisabled?: boolean
}

export interface ICustomClasses {
  classes?: {
    customWrapClass?: string
    customInfoClass?: string
    customTextClass?: string
    customCloseClass?: string
  }
}

export interface IOwnBoughtProps extends IControlFlags, ICustomClasses {
  ownerName?: string
  suffixLabel?: string
  tradePlace?: string
}

export interface IProps extends IOwnBoughtProps, ICommon {
  tradeLabel: string
  onClose: () => void
}
