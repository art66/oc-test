import { ICommon } from './ICommon'

export interface IFlags {
  isDisabledAverage?: boolean
}

export interface ICustomClasses {
  classes?: {
    confirmWrapClass?: string
    summaryPriceClass?: string
    averagePriceClass?: string
    textTitlesClass?: string
    textPriceClass?: string
    confirmControlClass?: string
    confirmYesButtonClass?: string
    confirmNoButtonClass?: string
  }
}

export interface IOwnConfirmationProps extends IFlags, ICustomClasses {
  averagePrice?: number
}

export interface IProps extends IOwnConfirmationProps, ICommon {
  tradeLabel: string
  isFetchActive: boolean
  onSave: () => void
  onCancel: () => void
}
