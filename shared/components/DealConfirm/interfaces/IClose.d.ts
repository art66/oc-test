export interface IProps {
  customCloseClass: string
  isCloseButtonDisabled: boolean
  onClose: () => void
}
