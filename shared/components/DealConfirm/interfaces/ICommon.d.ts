export interface ICommon {
  price: number
  itemTitle: string
  itemTitleShort?: string
  boughtAmount: number
}

export interface IEventHandlers {
  onSave: () => void
  onCancel: () => void
  onClose: () => void
}

// export interface IFetchData {
//   type?: string
//   userID?: number
//   itemID: number
//   endpoint: string
//   step?: string
//   ammoID?: number
// }
