export interface IFlags {
  isCloseButtonDisabled?: boolean
}

export interface ICustomClasses {
  classes?: {
    customWrapClass?: string
    customErrorMessage?: string
    customCloseClass?: string
  }
}

export interface IOwnErrorProps extends IFlags, ICustomClasses {
  errorMessage?: string
}

export interface IProps extends IOwnErrorProps {
  onClose: () => void
}
