import React from 'react'

import { text, number, boolean, select } from '@storybook/addon-knobs'
import { appendRequiredSVGStyles } from '../../../.storybook/utils'

import DealConfirm from '.'

import initialState from './mocks/storybook'
import { DEFS_HTML } from './mocks/svg'
import styles from './mocks/index.cssmodule.scss'

export const Default = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...initialState,
    isDealComplete: boolean('Complete Confirmation: ', false),
    classes: {
      wrapCustomClass: styles.customWrap
    }
  }

  return <DealConfirm {...config} />
}

Default.story = {
  name: 'default'
}

export const BuyConfirmation = () => {
  const config = {
    ...initialState,
    isFetchActive: boolean('Activate Fetch: ', false),
    price: number('Set Price: ', 109),
    boughtAmount: number('Set bought Amount: ', 1212),
    itemTitle: text('Set Item Name: ', 'Pistols 12mm'),
    itemTitleShort: select('Set Item Short Name: ', ['12mm', ''], ''),
    tradeLabel: select('Set Trade Label: ', ['buy', 'sell'], 'buy'),
    buyConfirm: {
      isDisabledAverage: boolean('Disable Average: ', false),
      averagePrice: number('Average Price: ', 1),
      classes: initialState.buyConfirm.classes
    },
    classes: {
      wrapCustomClass: styles.customWrap
    }
  }

  return <DealConfirm {...config} />
}

BuyConfirmation.story = {
  name: 'buy confirmation'
}

export const BoughtInformation = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...initialState,
    isDealComplete: true,
    price: number('Set Price: ', 109),
    boughtAmount: number('Set bought Amount: ', 1212),
    itemTitle: text('Set Item Name: ', 'Pistols 12mm'),
    itemTitleShort: select('Set Item Short Name: ', ['12mm', ''], ''),
    tradeLabel: select('Set Trade Label: ', ['buy', 'sell'], 'buy'),
    boughtInfo: {
      ownerName: text('Set Owner Name: ', '[OwnerName]'),
      suffixLabel: text('Set Suffix Label: ', ''),
      isOwnerDisabled: boolean('Disabled Owner', false),
      isTradingPlaceDisabled: boolean('Disable Trading Place: ', false),
      isSuffixLabelDisabled: boolean('Disable Suffix Label: ', false),
      isCloseButtonDisabled: boolean('Disable Close Button: ', false),
      classes: initialState.boughtInfo.classes
    },
    classes: {
      wrapCustomClass: styles.customWrap
    }
  }

  return <DealConfirm {...config} />
}

BoughtInformation.story = {
  name: 'bought information'
}

export const Error = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...initialState,
    isError: boolean('Disable Error: ', true),
    error: {
      errorMessage: text('Set Error Text: ', ''),
      isCloseButtonDisabled: boolean('Disabled Close Button: ', false),
      classes: {
        customWrapClass: 'customWrapClass',
        customErrorMessage: 'customErrorMessage',
        customCloseClass: 'customCloseClass'
      }
    },
    classes: {
      wrapCustomClass: styles.customWrap
    }
  }

  return <DealConfirm {...config} />
}

Error.story = {
  name: 'error'
}

export default {
  title: 'Shared/DealConfirm'
}
