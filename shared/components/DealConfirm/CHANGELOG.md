# DealConfirm - Torn

## 1.5.1
 * Fixed long float numbers.

## 1.5.0
 * Improved accessability.

## 1.4.0
 * Added ability to render short item names.
 * Increased gap between confirmation buttons.

## 1.3.0
 * Added keyBoard listeners.

## 1.2.0
 * Added sell/buy label interface along with unit tests.

## 1.1.0
 * Minor improvements.

## 1.0.0
 * First stable release.
 * Added Storybook.
 * Added unit tests.
 * Added Readme.md.
