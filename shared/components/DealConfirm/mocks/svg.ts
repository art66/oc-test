export const DEFS_HTML = `
  <filter id="bazaar_view_shadow">
    <fedropshadow dx="0" dy="1" stddeviation="0.1" flood-color="#fff"></fedropshadow>
  </filter>
  <lineargradient id="bazaar_cross_gradient" gradientunits="objectBoundingBox" gradienttransform="rotate(90)">
    <stop offset="0" stop-color="#999"></stop>
    <stop offset="1" stop-color="#ccc"></stop>
  </lineargradient>
  <lineargradient id="bazaar_cross_hover_gradient" gradientunits="objectBoundingBox" gradienttransform="rotate(90)">
    <stop offset="0" stop-color="#666"></stop>
    <stop offset="1" stop-color="#999"></stop>
  </lineargradient>
`
