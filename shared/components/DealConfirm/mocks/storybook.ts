import { IProps as IDealConfirm } from '../interfaces/IDealConfirm'
import styles from './index.cssmodule.scss'

const initialState: IDealConfirm = {
  isFetchActive: false,
  isDealComplete: false,
  isError: false,
  price: 109,
  boughtAmount: 1212,
  itemTitleShort: '',
  tradeLabel: 'buy',
  itemTitle: 'Pistols 12mm',
  onSave: () => {},
  onCancel: () => {},
  onClose: () => {},
  buyConfirm: {
    isDisabledAverage: false,
    averagePrice: 15000,
    classes: {
      confirmWrapClass: styles.buyConfirmation,
      summaryPriceClass: 'summaryPriceClass',
      averagePriceClass: 'averagePriceClass',
      textTitlesClass: 'textTitlesClass',
      confirmControlClass: 'confirmControlClass',
      confirmYesButtonClass: 'confirmYesButtonClass',
      confirmNoButtonClass: 'confirmNoButtonClass'
    }
  },
  boughtInfo: {
    ownerName: '[OwnerName]',
    tradePlace: 'bazaar',
    suffixLabel: '',
    isOwnerDisabled: false,
    isTradingPlaceDisabled: false,
    isSuffixLabelDisabled: false,
    isCloseButtonDisabled: false,
    classes: {
      customWrapClass: 'testWrap',
      customInfoClass: 'testInfo',
      customTextClass: 'tetText'
    }
  },
  error: {
    errorMessage: '',
    isCloseButtonDisabled: false,
    classes: {
      customWrapClass: 'customWrapClass',
      customErrorMessage: 'customErrorMessage',
      customCloseClass: 'customCloseClass'
    }
  },
  classes: {
    wrapCustomClass: 'wrapCustomClass'
  }
}

export default initialState

// fetchData: {
//   endpoint: 'page.php',
//   type: 'bazaar',
//   step: 'buy',
//   itemID: 112,
//   userID: 1212
// },
