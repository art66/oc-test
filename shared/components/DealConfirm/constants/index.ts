import { BAZAAR_CROSS_ICON_PRESET } from '@torn/shared/SVG/presets/icons/bazaar'

// eslint-disable-next-line import/prefer-default-export
export const CLOSE_CONFIG = {
  ...BAZAAR_CROSS_ICON_PRESET,
  gradient: {
    ...BAZAAR_CROSS_ICON_PRESET.gradient,
    ID: 'bazaar_cross_gradient'
  },
  fill: {
    ...BAZAAR_CROSS_ICON_PRESET.fill,
    color: 'url(#bazaar_cross_gradient)'
  },
  onHover: {
    ...BAZAAR_CROSS_ICON_PRESET.onHover,
    gradient: {
      ...BAZAAR_CROSS_ICON_PRESET.onHover.gradient,
      ID: 'bazaar_cross_hover_gradient'
    },
    fill: {
      ...BAZAAR_CROSS_ICON_PRESET.onHover.fill,
      color: 'url(#bazaar_cross_hover_gradient)'
    }
  }
}
