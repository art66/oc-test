import React from 'react'
import classnames from 'classnames'

import BuyConfirmation from './components/BuyConfirmation'
import BoughtInfo from './components/BoughtInfo'
import Error from './components/Error'

import styles from './styles/wrap.cssmodule.scss'

import { IProps, IDefaultProps } from './interfaces/IDealConfirm'

class DealConfirm extends React.Component<IProps> {
  static defaultProps: IDefaultProps = {
    tradeLabel: 'buy'
  }

  _classNameHolder = () => {
    const { classes } = this.props
    const { wrapCustomClass } = classes || {}

    const wrapClasses = classnames({
      [styles.dealConfirmWrap]: true,
      [wrapCustomClass]: wrapCustomClass
    })

    return {
      wrapClasses
    }
  }

  _handleSave = () => {
    const { onSave } = this.props

    if (!onSave) {
      return
    }

    onSave()
  }

  _handleCancel = () => {
    const { onCancel } = this.props

    if (!onCancel) {
      return
    }

    onCancel()
  }

  _handleClose = () => {
    const { onClose } = this.props

    if (!onClose) {
      return
    }

    onClose()
  }

  _renderBoughtInfo = () => {
    const { price, itemTitle, boughtAmount, boughtInfo, tradeLabel, itemTitleShort } = this.props

    return (
      <BoughtInfo
        {...boughtInfo}
        price={price}
        tradeLabel={tradeLabel}
        itemTitle={itemTitle}
        itemTitleShort={itemTitleShort}
        boughtAmount={boughtAmount}
        onClose={this._handleClose}
      />
    )
  }

  _renderBuyConfirmation = () => {
    const { tradeLabel, isFetchActive, buyConfirm, price, boughtAmount, itemTitle, itemTitleShort } = this.props

    return (
      <BuyConfirmation
        {...buyConfirm}
        price={price}
        tradeLabel={tradeLabel}
        itemTitle={itemTitle}
        itemTitleShort={itemTitleShort}
        boughtAmount={boughtAmount}
        isFetchActive={isFetchActive}
        onSave={this._handleSave}
        onCancel={this._handleCancel}
      />
    )
  }

  _renderError = () => {
    const { error } = this.props

    return (
      <Error
        {...error}
        onClose={this._handleClose}
      />
    )
  }

  _renderContent = () => {
    const { isError, isDealComplete } = this.props

    if (isError) {
      return this._renderError()
    }

    return isDealComplete ? this._renderBoughtInfo() : this._renderBuyConfirmation()
  }

  render() {
    const { wrapClasses } = this._classNameHolder()

    return (
      <div className={wrapClasses}>
        {this._renderContent()}
      </div>
    )
  }
}

export default DealConfirm

// TODO: perhaps in future we can make this component like a micro-service
// constructor(props: IProps) {
//   super(props)

//   this.state = {
//     isFetchActive: false,
//     isDealComplete: false
//   }
// }

// _setFetchStatus = (status: boolean) => {
//   this.setState({
//     isFetchActive: status
//   })
// }

// _setDealStatus = (status: boolean) => {
//   this.setState({
//     isDealComplete: status
//   })
// }

// _getFetchData = () => {
//   const { price, boughtAmount, fetchData } = this.props
//   const { type, userID, itemID, step } = fetchData

//   const postData: IFetchData = {
//     amount: boughtAmount
//   }

//   if (type === 'bazaar') {
//     fetchData.userID = userID
//     fetchData.id = itemID
//     fetchData.price = price
//     fetchData.beforeval = price * boughtAmount
//   }

//   if (type === 'ammo') {
//     fetchData.ammoID: itemID,
//     fetchData.step = step
//   }

//   return postData
// }

// _fetchDeal = async () => {
//   const { fetchEndpoint } = this.props

//   let data = null

//   this._setFetchStatus(true)

//   try {
//     const payload = await fetchUrl(fetchEndpoint, this._getFetchData())

//     data = await payload.json()

//     this._setDealStatus(true)
//   } catch (e) {
//     throw new Error(`Some error happen during payload receiving: ${e}`)
//   }

//   this._setFetchStatus(false)

//   return data
// }
