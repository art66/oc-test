import React, { PureComponent } from 'react'
import classnames from 'classnames'
import { toMoney } from '@torn/shared/utils'

import Close from '../Close'

import { IProps } from '../../interfaces/IBoughtInfo'

import styles from './index.cssmodule.scss'
import globalStyles from '../../styles/index.cssmodule.scss'

export class BoughtInfo extends PureComponent<IProps> {
  _classNameHolder = () => {
    const { classes } = this.props
    const { customWrapClass, customInfoClass, customTextClass } = classes || {}

    const wrapClass = classnames({
      [styles.boughtContainer]: true,
      [customWrapClass]: customWrapClass
    })

    const infoContainer = classnames({
      [styles.infoWrap]: true,
      [customInfoClass]: customInfoClass
    })

    const textClass = classnames({
      [styles.text]: true,
      [customTextClass]: customTextClass
    })

    const textPriceClass = classnames({
      [textClass]: true,
      [globalStyles.priceText]: true
    })

    return {
      wrapClass,
      infoContainer,
      textClass,
      textPriceClass
    }
  }

  _tradeLabelNormalizer = () => {
    const { tradeLabel } = this.props

    const pastPerfect = {
      sell: 'sold',
      buy: 'bought'
    }

    return pastPerfect[tradeLabel]
  }

  _prefixAmountLabel = () => {
    const { boughtAmount } = this.props

    const { textPriceClass, textClass } = this._classNameHolder()

    return (
      <React.Fragment>
        <span className={textClass}>You {this._tradeLabelNormalizer()}</span>
        <span className={textPriceClass}>{toMoney(boughtAmount || 0)}</span>
        <span className={textClass}>x</span>
      </React.Fragment>
    )
  }

  _itemTitle = () => {
    const { itemTitle, itemTitleShort } = this.props

    const { textClass } = this._classNameHolder()

    return <span className={textClass}>{itemTitleShort || itemTitle}</span>
  }

  _ownerText = () => {
    const { ownerName, isOwnerDisabled } = this.props

    if (isOwnerDisabled) {
      return null
    }

    const { textClass } = this._classNameHolder()

    return (
      <React.Fragment>
        <span className={textClass}>from</span>
        <span className={textClass}>{ownerName}&#39;s</span>
      </React.Fragment>
    )
  }

  _tradingPlaceLabel = () => {
    const { isTradingPlaceDisabled, tradePlace } = this.props

    if (isTradingPlaceDisabled) {
      return null
    }

    const { textClass } = this._classNameHolder()

    return <span className={textClass}>{tradePlace || 'bazaar'}</span>
  }

  _suffixLabel = () => {
    const { isSuffixLabelDisabled, suffixLabel } = this.props

    if (isSuffixLabelDisabled) {
      return null
    }

    const { textClass } = this._classNameHolder()

    return <span className={textClass}>{suffixLabel || 'for a total of'}</span>
  }

  _totalSum = () => {
    const { boughtAmount, price } = this.props

    const { textPriceClass } = this._classNameHolder()

    const preNormalizedValue = Number((boughtAmount * price).toFixed(2))

    return <span className={textPriceClass}>${toMoney(preNormalizedValue)}</span>
  }

  _renderInfoBlock = () => {
    const { infoContainer } = this._classNameHolder()

    return (
      <div className={infoContainer}>
        {this._prefixAmountLabel()}
        {this._itemTitle()}
        {this._ownerText()}
        {this._tradingPlaceLabel()}
        {this._suffixLabel()}
        {this._totalSum()}
      </div>
    )
  }

  _renderCloseButton = () => {
    const { isCloseButtonDisabled, classes, onClose } = this.props

    return (
      <Close
        customCloseClass={classes?.customCloseClass}
        isCloseButtonDisabled={isCloseButtonDisabled}
        onClose={onClose}
      />
    )
  }

  render() {
    const { wrapClass } = this._classNameHolder()

    return (
      <div className={wrapClass}>
        {this._renderInfoBlock()}
        {this._renderCloseButton()}
      </div>
    )
  }
}

export default BoughtInfo
