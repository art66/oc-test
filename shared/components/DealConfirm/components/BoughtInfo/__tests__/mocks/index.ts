import { IProps } from '../../../../interfaces/IBoughtInfo'

const initialState: IProps = {
  tradeLabel: 'buy',
  ownerName: 'love_intent',
  itemTitleShort: '',
  itemTitle: 'Test name',
  boughtAmount: 1324,
  price: 1573460,
  onClose: () => {}
}

export const withoutBazaarOwner = {
  ...initialState,
  isOwnerDisabled: true,
  isTradingPlaceDisabled: true
}

export const withoutClose = {
  ...initialState,
  isCloseButtonDisabled: true
}

export const soldLabel: IProps = {
  ...initialState,
  tradeLabel: 'sell'
}

export const shortTitle: IProps = {
  ...initialState,
  itemTitleShort: 'Test'
}

export default initialState
