import React from 'react'
import { shallow } from 'enzyme'

import BoughtInfo from '..'
import initialState, { withoutBazaarOwner, withoutClose, soldLabel, shortTitle } from './mocks'

describe('<BoughtInfo />', () => {
  it('should render basic component', () => {
    const Component = shallow(<BoughtInfo {...initialState} />)

    expect(Component.find('.boughtContainer').length).toBe(1)
    expect(Component.find('.infoWrap').length).toBe(1)
    expect(
      Component.find('.text.priceText')
        .at(0)
        .text()
    ).toBe('1,324')
    expect(
      Component.find('.text.priceText')
        .at(1)
        .text()
    ).toBe('$2,083,261,040')
    expect(
      Component.find('.text')
        .at(3)
        .text()
    ).toBe('Test name')
    expect(
      Component.find('.text')
        .at(5)
        .text()
    ).toBe('love_intent\'s')
    expect(
      Component.find('.text')
        .at(6)
        .text()
    ).toBe('bazaar')
    expect(Component.find('Close').length).toBe(1)
    expect(Component.find('Close').prop('isCloseButtonDisabled')).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })
  it('should render without bazaar and user titles', () => {
    const Component = shallow(<BoughtInfo {...withoutBazaarOwner} />)

    expect(Component.find('.boughtContainer').length).toBe(1)
    expect(Component.find('.infoWrap').length).toBe(1)
    expect(
      Component.find('.text.priceText')
        .at(0)
        .text()
    ).toBe('1,324')
    expect(
      Component.find('.text.priceText')
        .at(1)
        .text()
    ).toBe('$2,083,261,040')
    expect(
      Component.find('.text')
        .at(3)
        .text()
    ).toBe('Test name')
    expect(
      Component.find('.text')
        .at(5)
        .text()
    ).toBe('$2,083,261,040')
    expect(Component.find('Close').length).toBe(1)
    expect(Component.find('Close').prop('isCloseButtonDisabled')).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })
  it('should render without close button', () => {
    const Component = shallow(<BoughtInfo {...withoutClose} />)

    expect(Component.find('.boughtContainer').length).toBe(1)
    expect(Component.find('.infoWrap').length).toBe(1)
    expect(
      Component.find('.text.priceText')
        .at(0)
        .text()
    ).toBe('1,324')
    expect(
      Component.find('.text.priceText')
        .at(1)
        .text()
    ).toBe('$2,083,261,040')
    expect(
      Component.find('.text')
        .at(3)
        .text()
    ).toBe('Test name')
    expect(
      Component.find('.text')
        .at(5)
        .text()
    ).toBe('love_intent\'s')
    expect(
      Component.find('.text')
        .at(6)
        .text()
    ).toBe('bazaar')
    expect(Component.find('Close').length).toBe(1)
    expect(Component.find('Close').prop('isCloseButtonDisabled')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
  it('should render without sold trade label', () => {
    const Component = shallow(<BoughtInfo {...soldLabel} />)

    expect(Component.find('.boughtContainer').length).toBe(1)
    expect(Component.find('.infoWrap').length).toBe(1)
    expect(
      Component.find('.text.priceText')
        .at(0)
        .text()
    ).toBe('1,324')
    expect(
      Component.find('.text.priceText')
        .at(1)
        .text()
    ).toBe('$2,083,261,040')
    expect(
      Component.find('.text')
        .at(0)
        .text()
    ).toBe('You sold')
    expect(
      Component.find('.text')
        .at(3)
        .text()
    ).toBe('Test name')
    expect(
      Component.find('.text')
        .at(5)
        .text()
    ).toBe('love_intent\'s')
    expect(
      Component.find('.text')
        .at(6)
        .text()
    ).toBe('bazaar')
    expect(Component.find('Close').length).toBe(1)
    expect(Component.find('Close').prop('isCloseButtonDisabled')).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })
  it('should render short title', () => {
    const Component = shallow(<BoughtInfo {...shortTitle} />)

    expect(Component.find('.boughtContainer').length).toBe(1)
    expect(Component.find('.text').at(3).text()).toBe('Test')
  })
})
