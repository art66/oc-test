import React from 'react'
import { shallow } from 'enzyme'

import Close from '..'

import initialState, { disabledButton } from './mocks'

describe('<Close />', () => {
  it('should render basic component', () => {
    const Component = shallow(<Close {...initialState} />)

    expect(Component.find('.close').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should not render component', () => {
    const Component = shallow(<Close {...disabledButton} />)

    expect(Component.find('.close').length).toBe(0)
    expect(Component.find('SVGIconGenerator').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
