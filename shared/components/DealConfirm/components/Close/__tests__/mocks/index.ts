import { IProps } from '../../../../interfaces/IClose'

const initialState: IProps = {
  customCloseClass: 'customCloseClass',
  isCloseButtonDisabled: false,
  onClose: () => {}
}

const disabledButton: IProps = {
  ...initialState,
  isCloseButtonDisabled: true
}

export default initialState
export { disabledButton }
