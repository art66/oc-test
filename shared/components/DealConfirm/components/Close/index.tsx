import React from 'react'
import classnames from 'classnames'
import bazaarIcons from '@torn/shared/SVG/helpers/iconsHolder/bazaar'
import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import { KEY_TYPES } from '@torn/shared/utils/keyDownHandler/constants'

import { SVGIconGenerator } from '@torn/shared/SVG'

import { CLOSE_CONFIG } from '../../constants'
import { IProps } from '../../interfaces/IClose'

import styles from './index.cssmodule.scss'

class Close extends React.PureComponent<IProps> {
  private _closeRef: React.RefObject<HTMLButtonElement>

  constructor(props: IProps) {
    super(props)

    this._closeRef = React.createRef()
  }

  componentDidMount() {
    this._closeRef && this._closeRef.current && this._closeRef.current.focus()
  }

  _classNameHolder = () => {
    const { customCloseClass } = this.props

    const closeButtonClass = classnames({
      [styles.close]: true,
      [customCloseClass]: customCloseClass
    })

    return {
      closeButtonClass
    }
  }

  _handleClose = () => {
    const { onClose } = this.props

    if (!onClose) {
      return
    }

    onClose()
  }

  _handleKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          onEvent: this._handleClose,
          isPreventBubbling: true,
          type: KEY_TYPES.esc
        },
        {
          onEvent: this._handleClose,
          type: KEY_TYPES.enter
        },
        {
          onEvent: this._handleClose,
          type: KEY_TYPES.space
        }
      ]
    })
  }

  _renderCloseButton = () => {
    const { isCloseButtonDisabled } = this.props

    if (isCloseButtonDisabled) {
      return null
    }

    const { closeButtonClass } = this._classNameHolder()

    return (
      <button
        ref={this._closeRef}
        type='button'
        className={closeButtonClass}
        onClick={this._handleClose}
        onKeyDown={this._handleKeyDown}
      >
        <SVGIconGenerator iconsHolder={bazaarIcons} {...CLOSE_CONFIG} />
      </button>
    )
  }

  render() {
    return this._renderCloseButton()
  }
}

export default Close
