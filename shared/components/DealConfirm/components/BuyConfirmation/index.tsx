import React, { PureComponent } from 'react'
import classnames from 'classnames'
import { toMoney } from '@torn/shared/utils'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import AnimationLoad from '@torn/shared/components/AnimationLoad'
import keyDownHandler from '@torn/shared/utils/keyDownHandler'
import { KEY_TYPES } from '@torn/shared/utils/keyDownHandler/constants'

import { IProps } from '../../interfaces/IBuyConfirmation'

import styles from './index.cssmodule.scss'
import globalStyles from '../../styles/index.cssmodule.scss'

export class BuyConfirmation extends PureComponent<IProps> {
  private _confirmRef: React.RefObject<HTMLButtonElement>

  constructor(props: IProps) {
    super(props)

    this._confirmRef = React.createRef()
  }

  componentDidMount() {
    this._confirmRef && this._confirmRef.current && this._confirmRef.current.focus()
  }

  _classNameHolder = () => {
    const { classes, isFetchActive } = this.props

    const {
      confirmWrapClass,
      summaryPriceClass,
      averagePriceClass,
      textTitlesClass,
      textPriceClass,
      confirmControlClass,
      confirmYesButtonClass,
      confirmNoButtonClass
    } = classes || {}

    const confirmWrap = classnames({
      [styles.buyConfirmation]: true,
      [styles.preloader]: isFetchActive,
      [confirmWrapClass]: confirmWrapClass
    })

    const summaryClass = classnames({
      [styles.summary]: true,
      [summaryPriceClass]: summaryPriceClass
    })

    const averageClass = classnames({
      [styles.average]: true,
      [averagePriceClass]: averagePriceClass
    })

    const textClass = classnames({
      [styles.text]: true,
      [textTitlesClass]: textTitlesClass
    })

    const priceClass = classnames({
      [textClass]: true,
      // [styles.textBold]: true,
      [globalStyles.priceText]: true,
      [textPriceClass]: textPriceClass
    })

    const confirmButtonsWrap = classnames({
      [styles.control]: true,
      [confirmControlClass]: confirmControlClass
    })

    const confirmYesClass = classnames({
      [styles.button]: true,
      [confirmYesButtonClass]: confirmYesButtonClass
    })

    const confirmNoClass = classnames({
      [styles.button]: true,
      // [styles.gray]: true,
      [confirmNoButtonClass]: confirmNoButtonClass
    })

    return {
      confirmWrap,
      summaryClass,
      averageClass,
      textClass,
      priceClass,
      confirmButtonsWrap,
      confirmYesClass,
      confirmNoClass
    }
  }

  _handleSave = () => {
    const { onSave, isFetchActive } = this.props

    if (!onSave || isFetchActive) {
      return
    }

    onSave()
  }

  _handleCancel = () => {
    const { onCancel } = this.props

    if (!onCancel) {
      return
    }

    onCancel()
  }

  _handleKeyDownSave = (event: React.KeyboardEvent<HTMLButtonElement>) => keyDownHandler({
    event,
    keysConfig: [
      {
        onEvent: this._handleCancel,
        isPreventBubbling: true,
        type: KEY_TYPES.esc
      },
      {
        onEvent: this._handleSave,
        type: KEY_TYPES.enter
      },
      {
        onEvent: this._handleSave,
        type: KEY_TYPES.space,
        isPreventBubbling: true
      }
    ]
  })

  _handleKeyDownCancel = (event: React.KeyboardEvent<HTMLButtonElement>) => keyDownHandler({
    event,
    keysConfig: [
      {
        onEvent: this._handleCancel,
        isPreventBubbling: true,
        type: KEY_TYPES.esc
      },
      {
        onEvent: this._handleCancel,
        type: KEY_TYPES.enter
      },
      {
        onEvent: this._handleCancel,
        type: KEY_TYPES.space,
        isPreventBubbling: true
      }
    ]
  })

  _getTotalPrice = () => {
    const { boughtAmount, price } = this.props

    if (!boughtAmount && !price) {
      return 0
    }

    return toMoney(boughtAmount * price)
  }

  _getAveragePercent = () => {
    const { price, averagePrice } = this.props

    if (!averagePrice && !price) {
      return 0
    }

    const averageTotal = Math.round((100 / averagePrice) * price)

    return averageTotal
  }

  _renderInformBlock = () => {
    const { tradeLabel, itemTitle, itemTitleShort, boughtAmount } = this.props

    const { summaryClass, textClass, priceClass } = this._classNameHolder()
    const labelTitle = firstLetterUpper({ value: tradeLabel, isDisabledMultiUpper: true }) || 'Buy'

    return (
      <div className={summaryClass}>
        <span id='actionTitle' className={textClass}>{labelTitle}</span>
        <span id='itemAmount' className={priceClass}>{toMoney(boughtAmount)}</span>
        <span className={textClass}>x</span>
        <span id='itemTitle' className={textClass}>{itemTitleShort || itemTitle}</span>
        <span id='actionPrefix' className={textClass}>for</span>
        <span id='itemPrice' className={priceClass}>${this._getTotalPrice()}</span>
        <span className={textClass}>?</span>
      </div>
    )
  }

  _renderAverageCosts = () => {
    const { isDisabledAverage, price, averagePrice = 0 } = this.props

    const isHustlePrice = price >= averagePrice * 3

    if (!isHustlePrice || !averagePrice || isDisabledAverage) {
      return null
    }

    const { averageClass, textClass, priceClass } = this._classNameHolder()

    return (
      <div className={averageClass}>
        <span className={textClass}>This is</span>
        <span className={priceClass}>{this._getAveragePercent()}</span>
        <span className={textClass}>% above market value</span>
      </div>
    )
  }

  _renderConfirmationButtons = () => {
    const { confirmButtonsWrap, confirmYesClass, confirmNoClass } = this._classNameHolder()

    return (
      <div
        className={confirmButtonsWrap}
      >
        <button
          aria-labelledby='actionTitle itemAmount itemTitle actionPrefix itemPrice'
          type='button'
          ref={this._confirmRef}
          className={confirmYesClass}
          onClick={this._handleSave}
          onKeyDown={this._handleKeyDownSave}
        >
          Yes
        </button>
        <button
          type='button'
          aria-label='Discard trading process'
          className={confirmNoClass}
          onClick={this._handleCancel}
          onKeyDown={this._handleKeyDownCancel}
        >
          No
        </button>
      </div>
    )
  }

  _renderBlock = () => {
    return (
      <React.Fragment>
        {this._renderInformBlock()}
        {this._renderAverageCosts()}
        {this._renderConfirmationButtons()}
      </React.Fragment>
    )
  }

  _renderPreloader = () => {
    return <AnimationLoad dotsCount={5} dotsColor='black' isAdaptive={true} />
  }

  render() {
    const { isFetchActive } = this.props
    const { confirmWrap } = this._classNameHolder()

    const dataToRender = isFetchActive ? this._renderPreloader() : this._renderBlock()

    return <div className={confirmWrap}>{dataToRender}</div>
  }
}

export default BuyConfirmation
