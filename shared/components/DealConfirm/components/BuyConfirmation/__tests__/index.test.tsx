import React from 'react'
import { shallow } from 'enzyme'

import BuyConfirmation from '..'

import initialState, { hustleDeal, disabledHustle, sellLabel, shortTitle } from './mocks'

describe('<BuyConfirmation />', () => {
  it('should render basic component', () => {
    const Component = shallow(<BuyConfirmation {...initialState} />)

    expect(Component.find('.buyConfirmation').length).toBe(1)
    expect(Component.find('.text.priceText').at(0).text()).toBe('8,000')
    expect(Component.find('.text.priceText').at(1).text()).toBe('$16,000,000')
    expect(Component.find('.text').at(3).text()).toBe('Lead Pipe')

    expect(Component).toMatchSnapshot()
  })
  it('should render with hustle deal block', () => {
    const Component = shallow(<BuyConfirmation {...hustleDeal} />)

    expect(Component.find('.buyConfirmation').length).toBe(1)
    expect(Component.find('.text.priceText').at(0).text()).toBe('8,000')
    expect(Component.find('.text.priceText').at(1).text()).toBe('$16,000,000')
    expect(Component.find('.text.priceText').at(2).text()).toBe('200000')

    expect(Component).toMatchSnapshot()
  })
  it('should not render hustle deal block', () => {
    const Component = shallow(<BuyConfirmation {...disabledHustle} />)

    expect(Component.find('.buyConfirmation').length).toBe(1)
    expect(Component.find('.text.priceText').at(0).text()).toBe('8,000')
    expect(Component.find('.text.priceText').at(1).text()).toBe('$16,000,000')

    expect(Component).toMatchSnapshot()
  })
  it('should not render with sell label', () => {
    const Component = shallow(<BuyConfirmation {...sellLabel} />)

    expect(Component.find('.buyConfirmation').length).toBe(1)
    expect(Component.find('.text').at(0).text()).toBe('Sell')
    expect(Component.find('.text.priceText').at(0).text()).toBe('8,000')
    expect(Component.find('.text.priceText').at(1).text()).toBe('$16,000,000')

    expect(Component).toMatchSnapshot()
  })
  it('should render short title', () => {
    const Component = shallow(<BuyConfirmation {...shortTitle} />)

    expect(Component.find('.buyConfirmation').length).toBe(1)
    expect(Component.find('.text').at(0).text()).toBe('Buy')
    expect(Component.find('.text.priceText').at(0).text()).toBe('8,000')
    expect(Component.find('.text.priceText').at(1).text()).toBe('$16,000,000')
    expect(Component.find('.text').at(3).text()).toBe('Lead')

    expect(Component).toMatchSnapshot()
  })
})
