import { IProps } from '../../../../interfaces/IBuyConfirmation'

const initialState: IProps = {
  tradeLabel: 'buy',
  itemTitleShort: '',
  isFetchActive: false,
  itemTitle: 'Lead Pipe',
  price: 2000,
  boughtAmount: 8000,
  averagePrice: 50000,
  onSave: () => {},
  onCancel: () => {}
}

export const hustleDeal = {
  ...initialState,
  averagePrice: 1
}

export const disabledHustle = {
  ...hustleDeal,
  isDisabledAverage: true
}

export const sellLabel = {
  ...hustleDeal,
  tradeLabel: 'sell'
}

export const shortTitle = {
  ...hustleDeal,
  itemTitleShort: 'Lead'
}

export default initialState
