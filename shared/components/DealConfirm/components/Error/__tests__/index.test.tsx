import React from 'react'
import { shallow } from 'enzyme'

import Error from '..'

import initialState, { customError, withoutClose } from './mocks'

const DEFAULT_ERROR = 'Some error happen during trading. Please, try a bit later.'

describe('<BuyConfirmation />', () => {
  it('should render basic component', () => {
    const Component = shallow(<Error {...initialState} />)

    expect(Component.find('.errorWrap').length).toBe(1)
    expect(Component.find('.messageContainer').length).toBe(1)
    expect(Component.find('.messageContainer').text()).toBe(DEFAULT_ERROR)
    expect(Component.find('Close').length).toBe(1)
    expect(Component.find('Close').prop('isCloseButtonDisabled')).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })
  it('should render with custom error', () => {
    const Component = shallow(<Error {...customError} />)

    expect(Component.find('.errorWrap').length).toBe(1)
    expect(Component.find('.messageContainer').length).toBe(1)
    expect(Component.find('.messageContainer').text()).toBe('error oops!')
    expect(Component.find('Close').length).toBe(1)
    expect(Component.find('Close').prop('isCloseButtonDisabled')).toBeFalsy()

    expect(Component).toMatchSnapshot()
  })
  it('should render without close button', () => {
    const Component = shallow(<Error {...withoutClose} />)

    expect(Component.find('.errorWrap').length).toBe(1)
    expect(Component.find('.messageContainer').length).toBe(1)
    expect(Component.find('Close').length).toBe(1)
    expect(Component.find('Close').prop('isCloseButtonDisabled')).toBeTruthy()

    expect(Component).toMatchSnapshot()
  })
})
