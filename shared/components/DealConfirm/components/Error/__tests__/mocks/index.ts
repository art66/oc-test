import { IProps } from '../../../../interfaces/IError'

const initialState: IProps = {
  errorMessage: '',
  isCloseButtonDisabled: false,
  classes: {
    customWrapClass: 'customWrapClass',
    customErrorMessage: 'customErrorMessage',
    customCloseClass: 'customCloseClass'
  },
  onClose: () => {}
}

const customError: IProps = {
  ...initialState,
  errorMessage: 'error oops!'
}

const withoutClose: IProps = {
  ...initialState,
  isCloseButtonDisabled: true
}

export default initialState
export { customError, withoutClose }
