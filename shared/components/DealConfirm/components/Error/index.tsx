import React, { PureComponent } from 'react'
import classnames from 'classnames'

import Close from '../Close'

import { IProps } from '../../interfaces/IError'

import styles from './index.cssmodule.scss'

export class Error extends PureComponent<IProps> {
  _classNameHolder = () => {
    const { classes } = this.props
    const { customWrapClass, customErrorMessage } = classes || {}

    const wrapClass = classnames({
      [styles.errorWrap]: true,
      [customWrapClass]: customWrapClass
    })

    const errorContainer = classnames({
      [styles.messageContainer]: true,
      [customErrorMessage]: customErrorMessage
    })

    return {
      wrapClass,
      errorContainer
    }
  }

  _renderErrorBlock = () => {
    const { errorMessage } = this.props
    const { errorContainer } = this._classNameHolder()

    return (
      <span className={errorContainer}>
        {errorMessage || 'Some error happen during trading. Please, try a bit later.'}
      </span>
    )
  }

  _renderCloseButton = () => {
    const { isCloseButtonDisabled, classes, onClose } = this.props

    return (
      <Close
        customCloseClass={classes?.customCloseClass}
        isCloseButtonDisabled={isCloseButtonDisabled}
        onClose={onClose}
      />
    )
  }

  render() {
    const { wrapClass } = this._classNameHolder()

    return (
      <div className={wrapClass}>
        {this._renderErrorBlock()}
        {this._renderCloseButton()}
      </div>
    )
  }
}

export default Error
