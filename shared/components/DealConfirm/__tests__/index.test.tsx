import React from 'react'
import { mount } from 'enzyme'

import DealConfirm from '..'

import initialState, { error } from './mocks'

describe('<DealConfirm />', () => {
  it('should render regular confirmation layout', () => {
    const Component = mount(<DealConfirm {...initialState} />)

    expect(Component.find('DealConfirm').length).toBe(1)
    expect(Component.find('BuyConfirmation').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render regular confirmation layout, then loading preloader and then success dialog', () => {
    const Component = mount(<DealConfirm {...initialState} />)

    expect(Component.find('DealConfirm').length).toBe(1)
    expect(Component.find('BuyConfirmation').length).toBe(1)
    expect(Component.find('AnimationLoad').length).toBe(0)

    Component.setProps({
      isFetchActive: true
    })

    expect(Component.find('BuyConfirmation').length).toBe(1)
    expect(Component.find('AnimationLoad').length).toBe(1)

    Component.setProps({
      isFetchActive: false,
      isDealComplete: true
    })

    expect(Component.find('BoughtInfo').length).toBe(1)
    expect(Component.find('AnimationLoad').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render error layout', () => {
    const Component = mount(<DealConfirm {...error} />)

    expect(Component.find('DealConfirm').length).toBe(1)
    expect(Component.find('Error').length).toBe(1)
    expect(Component.find('BuyConfirmation').length).toBe(0)
    expect(Component.find('BoughtInfo').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
