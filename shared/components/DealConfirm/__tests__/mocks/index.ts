import defaultState from '../../mocks/storybook'

const initialState = {
  ...defaultState
}

const error = {
  ...initialState,
  isError: true
}

export default initialState

export { error }
