# DealConfirm - reusable dumb component for deal confirmations.

### The Component for confirming 2-step deals.

How to use:
  - Quick Start:
    `{isDealComplete}` - flag for managing layout: confirmation or information.
    `{isFetchActive}` - flag for attempting fetch request and as a result showing loading anim.
    `{price}` - real item price to buy.
    `{itemTitle}` - real item name.
    `{boughtAmount}` - a total amount of the items to buy.
    `{onSave}` - a trigger callback for committing fetch request.

    Example:
      ```
        <DealConfirm
          isDealComplete={false}
          isFetchActive={false}
          price={1000}
          itemTitle='Awesome Item'
          boughtAmount={100}
          onSave={callback}
        />
      ```
  - Advanced:
    For advanced usage you can dive into the whole API described inside mocks > storybook.ts state.
