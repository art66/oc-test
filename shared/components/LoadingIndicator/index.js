import React from 'react'
import PropTypes from 'prop-types'
import s from './styles.cssmodule.scss'

const LoadingIndicator = ({ className }) => (
  <div className={s.preloader + ' ' + className}>
    <div className={s.dots}>
      <span className={s.dot1} />
      <span className={s.dot2} />
      <span className={s.dot3} />
      <span className={s.dot4} />
    </div>
  </div>
)

LoadingIndicator.propTypes = {
  className: PropTypes.string
}

export default LoadingIndicator
