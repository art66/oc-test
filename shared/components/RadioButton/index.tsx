import React, { Component, ChangeEvent } from 'react';
import '../Checkbox/styles.scss';

type Props = {
  id: string;
  name?: string;
  value?: string;
  checked: boolean;
  label?: string;
  containerClassName?: string;
  onChange(event: ChangeEvent<HTMLInputElement>): any;
};

class RadioButton extends Component<Props> {
  static defaultProps = {
    containerClassName: 'choice-container',
  };

  render() {
    const { id, name, value, checked, label, containerClassName, onChange } = this.props;

    return(
      <div className={containerClassName}>
        <input
          id={id}
          className="radio-css"
          type="radio"
          name={name}
          value={value}
          checked={checked}
          onChange={onChange}
        />
        <label className="marker-css" htmlFor={id}>{label}</label>
      </div>
    );
  }
}

export default RadioButton;
