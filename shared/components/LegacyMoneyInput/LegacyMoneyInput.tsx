import React, { useEffect, useRef } from 'react'

type TProps = {
  value: number | string
  maxValue: number
  onChange: (data: { value: string; error: boolean }) => void
  disabled?: boolean
  symbol?: string
}

export const LegacyMoneyInput = (componentProps: TProps) => {
  const ref = useRef()

  useEffect(() => {
    $(ref.current)
      .attr('data-money', componentProps.maxValue)
      // @ts-ignore
      .tornInputMoney({ onAfterChange: componentProps.onChange, symbol: componentProps.symbol })
  }, [componentProps.maxValue, componentProps.onChange, componentProps.symbol, ref])

  useEffect(() => {
    $(ref.current)
      .val(componentProps.value)
      // @ts-ignore
      .tornInputMoney('format')
  }, [componentProps.value, ref])

  useEffect(() => {
    $(ref.current).prop('disabled', Boolean(componentProps.disabled))
  }, [componentProps.disabled, ref])

  return <input ref={ref} />
}
