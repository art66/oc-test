# UserInfo - Torn

## 1.5.8
 * Changed tooltip style to correspond global one.

## 1.5.7
 * Added integrated tooltips invoke.

## 1.5.6
 * Improved error checking.

## 1.5.5
 * Fixed small image transformation.

## 1.5.4
 * Created fallback for faction image/text.

## 1.5.3
 * Add iconHolder for OnlineStatus icon.

## 1.5.2
 * Fixed text width while honorbars are off.

## 1.5.1
 * Fixed labels layout while only images should be on.

## 1.5.0
 * Improved Component Structure.
 * Added ability to render small images.

## 1.4.0
 * Added opacity for Faction image.

## 1.3.3
 * Fixed colors on text wraps.

## 1.3.2
 * Updated test suits.

## 1.3.1
 * Fixed status icon orientation.

## 1.3.0
 * Removed unnecessary styles.

## 1.2.1
 * Added status icon tooltips on request.
 * Fixed some classes collision.
 * Fixed customStyles interfaces.

## 1.2.0
 * Added JSDoc.
 * Added README.md.
 * Added Unit tests.

## 1.1.1
 * Improved Storybook Config.

## 1.1.0
 * Added basic storybook config.
 * Improved Tooltips updating logic.

## 1.0.1
 * Minor structural improvements.

## 1.0.0
 * First stable release.
