import React from 'react'
import classnames from 'classnames'

import ProgressiveImage from '@torn/shared/components/ProgressiveImage'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import OnlineStatus from '@torn/shared/SVG/icons/global/OnlineStatus'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import isValue from '@torn/shared/utils/isValue'

import { DEFAULT_FACTION_IMG, SVG_ICON, USER_LABEL, FACTION_LABEL, LINK_TYPES } from './constants'
import { IProps, IRenderImageFactory, ILinkDecorator, IClassNames, TImageTypes, TFactionRank } from './interfaces'

import styles from './index.cssmodule.scss'
import Tooltip from './components/Tooltips'

class UserInfo extends React.Component<IProps> {
  static defaultProps: IProps = {
    showImages: true,
    isDecodeImg: false,
    isActiveTooltips: true,
    useProgressiveImage: true,
    status: {
      mode: 'online',
      isActive: true
    },
    user: {
      isActive: true,
      ID: null,
      name: '',
      imageUrl: ''
    },
    faction: {
      isActive: true,
      ID: null,
      name: '',
      imageUrl: '',
      rank: '',
      isOpacityDisabled: false
    },
    tooltips: {
      isActive: false,
      position: null,
      animation: null,
      overrideStyles: null,
      manualFix: null
    },
    customStyles: {
      container: '',
      blockWrap: '',
      status: '',
      img: '',
      text: '',
      link: ''
    }
  }

  componentDidMount() {
    this._mountTooltips()
  }

  shouldComponentUpdate(nextProps: IProps) {
    const { status, user, faction } = this.props

    const isStatusChanged = JSON.stringify(status) !== JSON.stringify(nextProps.status)
    const isUserChanged = JSON.stringify(user) !== JSON.stringify(nextProps.user)
    const isFactionChanged = JSON.stringify(faction) !== JSON.stringify(nextProps.faction)

    return isStatusChanged || isUserChanged || isFactionChanged || this._isFlagsChanged(this.props, nextProps)
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _isFlagsChanged = (prevProps: IProps, nextProps: IProps) => {
    const { showImages, isDecodeImg, isActiveTooltips, useProgressiveImage } = prevProps

    const isShowImageChanged = showImages !== nextProps.showImages
    const isDecodeImgChanged = isDecodeImg !== nextProps.isDecodeImg
    const isActiveTooltipsChanged = isActiveTooltips !== nextProps.isActiveTooltips
    const IsUseProgressiveImageChanged = useProgressiveImage !== nextProps.useProgressiveImage

    return isShowImageChanged || isDecodeImgChanged || isActiveTooltipsChanged || IsUseProgressiveImageChanged
  }

  _getTooltips = () => {
    const { isActiveTooltips, status, user, faction } = this.props

    if (!isActiveTooltips) {
      return []
    }

    const tooltipsList = []

    status
      && status.addTooltip
      && tooltipsList.push({
        child: firstLetterUpper({ value: status.mode }),
        ID: this._getTooltipsID().status
      })

    user
      && tooltipsList.push({
        child: `${user.name} [${user.ID}]`,
        ID: this._getTooltipsID().user
      })

    faction
      && tooltipsList.push({
        child: faction.name,
        ID: this._getTooltipsID().faction
      })

    return tooltipsList
  }

  _getTooltipsID = () => {
    const { status, user, faction } = this.props

    return {
      status: status ? `${user?.ID}_${status?.mode}-${USER_LABEL}` : null,
      user: user ? `${user?.ID}-${USER_LABEL}` : null,
      faction: faction ? `${user?.ID}_${faction?.ID}-${FACTION_LABEL}` : null
    }
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltips(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._getTooltips(), type: 'update' })

  _classNamesHolder = (type?: 'user' | 'faction', isOpacityDisabled?: boolean, isSmall?: boolean, rank?: TFactionRank) => {
    const { customStyles } = this.props

    const { container: containerCls, blockWrap, img: imgCls, text: textCls, link: linkCls, status: statusCls } =
      customStyles || {}

    const classNames = {
      container: classnames({
        [styles.userInfoBox]: true,
        [containerCls]: containerCls
      }),
      wrap: classnames({
        [styles[`${type}Wrap`]]: type,
        [styles[`${type}WrapSmall`]]: isSmall,
        [styles.textWrap]: !this._showImages(),
        [blockWrap]: blockWrap
      }),
      status: classnames({
        [styles.userStatusWrap]: true,
        [statusCls]: statusCls
      }),
      link: classnames({
        [styles.linkWrap]: true,
        [linkCls]: linkCls
      }),
      img: classnames({
        [styles[`${type}Image`]]: type,
        [styles.cutOpacity]: !isOpacityDisabled,
        [imgCls]: imgCls,
        [styles[rank]]: rank
      }),
      text: classnames({
        [styles[`${type}Name`]]: type,
        [textCls]: textCls
      }),
      search: classnames({
        [styles.searchWrap]: true
      }),
      searchText: classnames({
        [styles.searchText]: true,
        [styles.serachTextHide]: !(this._showImages() && type === USER_LABEL)
      })
    }

    return classNames
  }

  _isFaction = (type: TImageTypes) => {
    const isNotFactionImg = type === 'faction'

    return isNotFactionImg
  }

  _showImages = () => {
    const { showImages } = this.props

    return showImages
  }

  _getAdaptiveImg = (imgSrc: string, isSmall: boolean) => {
    if (isSmall && /(large)/i.test(imgSrc)) {
      const smallImg = imgSrc.replace(/large/i, 'small')

      return smallImg
    }

    return imgSrc
  }

  _normalizeImgSrc = (imgSrc: string, isSmall: boolean) => {
    const { isDecodeImg } = this.props

    const optimizedImg = this._getAdaptiveImg(imgSrc, isSmall)

    return isDecodeImg ? decodeURIComponent(optimizedImg) : optimizedImg
  }

  _linkDecorator = ({ children, urlID, type, text }: ILinkDecorator) => {
    if (!children || !urlID || !type) {
      return null
    }

    const href = `${LINK_TYPES[`${type}Link`]}${urlID}`
    const tooltipsLinkID = this._showImages() ? this._getTooltipsID()[type] : ''
    const classNames = this._classNamesHolder(type)
    const childrenWrap = this._showImages() && type === USER_LABEL ?
      (
        <div className={classNames.search}>
          {children}
          <span className={classNames.searchText}>{text}</span>
        </div>
      ) : children

    const link = (
      <a
        rel='noopener noreferrer'
        target='_blank'
        id={tooltipsLinkID}
        className={classNames.link}
        href={href}
      >
        {childrenWrap}
      </a>
    )

    return link
  }

  _generateImage = (imgSrc: string, classNames: IClassNames, isSmall: boolean, type?: TImageTypes) => {
    const { useProgressiveImage } = this.props

    const isFactionImg = this._isFaction(type)

    if (!imgSrc && !isFactionImg) {
      return null
    }

    const normalizedImgSrc = !isFactionImg ? this._normalizeImgSrc(imgSrc, isSmall) : imgSrc
    const postProcessedImg = normalizedImgSrc || DEFAULT_FACTION_IMG

    if (useProgressiveImage) {
      return (
        <ProgressiveImage
          image={{ name: postProcessedImg, imgClass: classNames.img }}
          preloader={{ prlClass: styles.preloader }}
        />
      )
    }

    return <img alt='' className={classNames.img} src={postProcessedImg} />
  }

  _generateText = (text: string, classNames: IClassNames) => {
    return <span className={classNames.text}>{text || 'N/A'}</span>
  }

  _generateImageBlock = (props: IRenderImageFactory) => {
    const {
      active = true,
      urlID = null,
      type = 'user',
      imgSrc = '',
      text = '',
      isOpacityDisabled = false,
      isSmall,
      rank = ''
    } = props

    if (!active) {
      return null
    }

    if (!urlID && (imgSrc || text)) {
      console.error('Error: no-image link was provided. Please, check your config: ', this.props)
    }

    const classNames = this._classNamesHolder(type, isOpacityDisabled, isSmall, rank)

    const children = this._showImages() ?
      this._generateImage(imgSrc, classNames, isSmall, type) :
      this._generateText(text, classNames)

    return <div className={classNames.wrap}>{this._linkDecorator({ children, urlID, type, text })}</div>
  }

  _isBlockShouldExist = (blockData: object) => {
    return blockData && Object.keys(blockData).length >= 0
  }

  _renderTooltips = () => {
    const { tooltips } = this.props
    const { isActive, ...tooltipsProps } = tooltips || {}

    if (!isActive) {
      return null
    }

    return <Tooltip {...tooltipsProps} />
  }

  _renderUserStatus = () => {
    const {
      status: { mode = 'online', isActive = true }
    } = this.props
    const { iconName, types, fills, filter, gradients, dimensions } = SVG_ICON

    if (!mode || (isValue(isActive) && !isActive)) {
      return null
    }

    return (
      <div id={this._getTooltipsID().status} className={this._classNamesHolder().status}>
        <SVGIconGenerator
          iconName={iconName}
          iconsHolder={{ OnlineStatus }}
          type={types[mode]}
          fill={fills[mode]}
          gradient={gradients[mode]}
          dimensions={dimensions}
          filter={filter}
        />
      </div>
    )
  }

  _renderUserBlocks = () => {
    const { faction, user } = this.props

    const factionImage = this._isBlockShouldExist(faction) ?
      this._generateImageBlock({
        active: faction.isActive,
        type: FACTION_LABEL,
        urlID: faction.ID,
        imgSrc: faction.imageUrl,
        text: faction.name,
        rank: faction.rank,
        isSmall: false,
        isOpacityDisabled: faction.isOpacityDisabled
      }) :
      null

    const userImage = this._isBlockShouldExist(user) ?
      this._generateImageBlock({
        active: user.isActive,
        type: USER_LABEL,
        urlID: user.ID,
        imgSrc: user.imageUrl,
        text: user.name,
        isSmall: user.isSmall,
        isOpacityDisabled: true
      }) :
      null

    return (
      <React.Fragment>
        {factionImage}
        {userImage}
      </React.Fragment>
    )
  }

  render() {
    return (
      <div className={this._classNamesHolder().container}>
        {this._renderTooltips()}
        {this._renderUserStatus()}
        {this._renderUserBlocks()}
      </div>
    )
  }
}

export default UserInfo
