import { IProps as ITooltips } from '../components/Tooltips'

export type TOnlineStatus = 'online' | 'offline' | 'idle'
export type TImageTypes = 'user' | 'faction'
export type TUrlID = number
export type TFactionRank = 'platinum' | 'diamond' | 'gold' | ''

export interface IStatus {
  mode: TOnlineStatus
  addTooltip?: boolean
  isActive?: boolean
}

export interface IUnit {
  isActive?: boolean
  ID: number
  name: string
  imageUrl?: string
}

export interface IUser extends IUnit {
  isSmall?: boolean
}

export interface IFaction extends IUnit {
  rank?: TFactionRank
  isOpacityDisabled?: boolean
}

export interface ICustomStyles {
  container?: string
  blockWrap?: string
  status?: string
  img?: string
  text?: string
  link?: string
}

export interface IRenderImageFactory {
  active: boolean
  type: TImageTypes
  urlID: TUrlID
  imgSrc?: string
  rank?: TFactionRank
  isOpacityDisabled: boolean
  isSmall: boolean
  text: string
}

export interface ILinkDecorator {
  children: JSX.Element
  urlID: TUrlID
  type: TImageTypes
  text: string
}

export interface IOwnProps {
  showImages?: boolean
  isDecodeImg?: boolean
  isActiveTooltips?: boolean
  useProgressiveImage?: boolean
}

export interface IProps extends IOwnProps {
  status?: IStatus
  user?: IUser
  faction?: IFaction
  customStyles?: ICustomStyles
  tooltips?: ITooltips
}

export interface IClassNames {
  wrap: string
  img: string
  text: string
}
