import { IProps } from '../interfaces'

export const DEFAULT_CONFIG: IProps = {
  showImages: true,
  isDecodeImg: false,
  useProgressiveImage: true,
  isActiveTooltips: true,
  status: {
    mode: 'online'
  },
  user: {
    ID: 2114355,
    name: 'svyat770UserLongNameHere',
    imageUrl: 'https://awardimages.torn.com/1036235-32804-large.png',
    isActive: true,
    isSmall: false
  },
  faction: {
    ID: 124124,
    name: 'CR',
    imageUrl: 'https://factiontags.torn.com/20514-75433.png',
    isActive: true,
    isOpacityDisabled: false
  },
  customStyles: {}
}

export const STORY_GROUPS = {
  flags: 'Flags',
  status: 'Status',
  user: 'User',
  faction: 'Faction',
  customClasses: 'Classes'
}

export const DEFS_HTML = `
  <linearGradient id="svg_status_online" gradientTransform="translate(4003.99 5792.17) scale(11.88)" x1="-243.94" y1="-487.37" x2="-243.94" y2="-486.36" gradientUnits="userSpaceOnUse">
    <stop offset="0" stop-color="#a3d900"></stop>
    <stop offset="1" stop-color="#4c6600"></stop>
  </linearGradient>
  <linearGradient id="svg_status_idle" gradientTransform="matrix(11.88, 0, 0, -11.88, 11469.59, 5991.42)" x1="-964.96" y1="504.33" x2="-964.96" y2="503.31" gradientUnits="userSpaceOnUse">
    <stop offset="0" stop-color="#ffbf00"></stop>
    <stop offset="1" stop-color="#b25900"></stop>
  </linearGradient>
  <linearGradient id="svg_status_offline" gradientTransform="matrix(11.88, 0, 0, -11.88, 10388.51, 5992.42)" x1="-873.96" y1="504.42" x2="-873.96" y2="503.31" gradientUnits="userSpaceOnUse">
    <stop offset="0" stop-color="#ccc"></stop>
    <stop offset="1" stop-color="#666"></stop>
  </linearGradient>
`
