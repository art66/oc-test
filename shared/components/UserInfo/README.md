# UserInfo - reusable user information component.

How to use:
  - Quick Start:
  `{user}` config is only one required for the quick start. Of course you would need to render `{faction}` config as well. They're 100% indentical just their data will be different.

```
  const config = {
    ID: 1212,
    name: 'Test',
    imageUrl: 'https://torn.com/test.png'
  }

  <UserInfo status={...} user={...config} />
```

  - Advanced:
  In case you need to make a custom set up, you can config all the properties based on the interface of the component on `./interfaces/index.ts` and configure your progressive image in way like:

```
  <UserInfo
    user={...}
    faction={...}
    customStyles={...}
    status={...}
    tooltips={...}
    showImages={true}
    isDecodeImg={true}
    isActiveTooltips={true}
    useProgressiveImage={true}
  />
```
