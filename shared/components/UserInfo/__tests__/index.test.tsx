import React from 'react'
import { mount } from 'enzyme'

import UserInfo from '../index'

import initialState, {
  onlyText,
  withoutStatus,
  withoutFaction,
  withoutUser,
  withoutFactionOpacity,
  withSmallUserImage,
  withPlaceholderImgFaction,
  withPlaceholderTextFaction
} from './mocks'

describe('UserInfo', () => {
  it('should return UserInfo with images', async done => {
    const Component = mount(<UserInfo {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const checkLayout = () => {
      expect(Component.find('UserInfo').length).toBe(1)
      expect(Component.find('.userStatusWrap').length).toBe(1)
      expect(Component.find('.userInfoBox').length).toBe(1)
      expect(Component.find('.userWrap').length).toBe(1)
      expect(Component.find('.factionWrap').length).toBe(1)
      expect(Component.find('.linkWrap').length).toBe(2)
      expect(Component.find('svg').length).toBe(1)
      expect(Component.find('img').length).toBe(2)
    }

    const checkValueOnContainers = () => {
      expect(Component.find('svg').prop('fill')).toBe('url("#svg_status_online")')
      expect(Component.find('img').at(0).prop('src')).toBe('https://torn.com/faction/localStorage.png')
      expect(Component.find('img').at(1).prop('src')).toBe('https://torn.com/user/localStorage.png')
      expect(Component.find('C').length).toBe(0)
    }

    checkLayout()
    checkValueOnContainers()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return UserInfo with texts', async done => {
    const Component = mount(<UserInfo {...onlyText} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const checkLayout = () => {
      expect(Component.find('UserInfo').length).toBe(1)
      expect(Component.find('.userStatusWrap').length).toBe(1)
      expect(Component.find('.userInfoBox').length).toBe(1)
      expect(Component.find('.userWrap').length).toBe(1)
      expect(Component.find('.factionWrap').length).toBe(1)
      expect(Component.find('.linkWrap').length).toBe(2)
      expect(Component.find('svg').length).toBe(1)
      expect(Component.find('img').length).toBe(0)
      expect(Component.find('span').length).toBe(2)
    }

    const checkValueOnContainers = () => {
      expect(Component.find('svg').prop('fill')).toBe('url("#svg_status_online")')
      expect(Component.find('span').at(0).text()).toBe('Test_Faction')
      expect(Component.find('span').at(1).text()).toBe('Test_User')
    }

    checkLayout()
    checkValueOnContainers()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should not render icon status block', async done => {
    const Component = mount(<UserInfo {...withoutStatus} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const checkLayout = () => {
      expect(Component.find('UserInfo').length).toBe(1)
      expect(Component.find('.userStatusWrap').length).toBe(0)
      expect(Component.find('.userInfoBox').length).toBe(1)
      expect(Component.find('.userWrap').length).toBe(1)
      expect(Component.find('.factionWrap').length).toBe(1)
      expect(Component.find('.linkWrap').length).toBe(2)
      expect(Component.find('svg').length).toBe(0)
      expect(Component.find('img').length).toBe(2)
    }

    const checkValueOnContainers = () => {
      expect(Component.find('img').at(0).prop('src')).toBe('https://torn.com/faction/localStorage.png')
      expect(Component.find('img').at(1).prop('src')).toBe('https://torn.com/user/localStorage.png')
    }

    checkLayout()
    checkValueOnContainers()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should not render faction block', async done => {
    const Component = mount(<UserInfo {...withoutFaction} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const checkLayout = () => {
      expect(Component.find('UserInfo').length).toBe(1)
      expect(Component.find('.userStatusWrap').length).toBe(1)
      expect(Component.find('.userInfoBox').length).toBe(1)
      expect(Component.find('.userWrap').length).toBe(1)
      expect(Component.find('.factionWrap').length).toBe(0)
      expect(Component.find('.linkWrap').length).toBe(1)
      expect(Component.find('svg').length).toBe(1)
      expect(Component.find('img').length).toBe(1)
      expect(Component.find('span.factionName').length).toBe(0)
    }

    const checkValueOnContainers = () => {
      expect(Component.find('svg').prop('fill')).toBe('url("#svg_status_online")')
      expect(Component.find('img').at(0).prop('src')).toBe('https://torn.com/user/localStorage.png')
    }

    checkLayout()
    checkValueOnContainers()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should not render user block', async done => {
    const Component = mount(<UserInfo {...withoutUser} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const checkLayout = () => {
      expect(Component.find('UserInfo').length).toBe(1)
      expect(Component.find('.userStatusWrap').length).toBe(1)
      expect(Component.find('.userInfoBox').length).toBe(1)
      expect(Component.find('.userWrap').length).toBe(1)
      expect(Component.find('.factionWrap').length).toBe(1)
      expect(Component.find('.linkWrap').length).toBe(1)
      expect(Component.find('svg').length).toBe(1)
      expect(Component.find('img').length).toBe(1)
      expect(Component.find('span').length).toBe(0)
    }

    const checkValueOnContainers = () => {
      expect(Component.find('svg').prop('fill')).toBe('url("#svg_status_online")')
      expect(Component.find('img').at(0).prop('src')).toBe('https://torn.com/faction/localStorage.png')
    }

    checkLayout()
    checkValueOnContainers()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render faction image without opacity', async done => {
    const Component = mount(<UserInfo {...withoutFactionOpacity} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const checkLayout = () => {
      expect(Component.find('UserInfo').length).toBe(1)
      expect(Component.find('.userStatusWrap').length).toBe(1)
      expect(Component.find('.userInfoBox').length).toBe(1)
      expect(Component.find('.userWrap').length).toBe(1)
      expect(Component.find('.factionWrap').length).toBe(1)
      expect(Component.find('.linkWrap').length).toBe(2)
      expect(Component.find('svg').length).toBe(1)
      expect(Component.find('img').length).toBe(2)
    }

    const checkValueOnContainers = () => {
      expect(Component.find('svg').prop('fill')).toBe('url("#svg_status_online")')
      expect(Component.find('img').at(0).prop('src')).toBe('https://torn.com/faction/localStorage.png')
      expect(Component.find('img').at(1).prop('src')).toBe('https://torn.com/user/localStorage.png')
      expect(Component.find('img.factionImage.cutOpacity').length).toBe(0)
      expect(Component.find('img.userImage.cutOpacity').length).toBe(0)
      expect(Component.find('C').length).toBe(0)
    }

    checkLayout()
    checkValueOnContainers()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render small user image', async done => {
    const Component = mount(<UserInfo {...withSmallUserImage} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const checkLayout = () => {
      expect(Component.find('UserInfo').length).toBe(1)
      expect(Component.find('.userStatusWrap').length).toBe(1)
      expect(Component.find('.userInfoBox').length).toBe(1)
      expect(Component.find('.userWrap').length).toBe(1)
      expect(Component.find('.factionWrap').length).toBe(1)
      expect(Component.find('.linkWrap').length).toBe(2)
      expect(Component.find('svg').length).toBe(1)
      expect(Component.find('img').length).toBe(2)
    }

    const checkValueOnContainers = () => {
      expect(Component.find('img').at(0).prop('src')).toBe('https://torn.com/faction/localStorage.png')
      expect(Component.find('img').at(1).prop('src')).toBe('https://torn.com/user/localStorage-small.png')
      expect(Component.find('div').at(3).prop('className')).toBe('userWrap userWrapSmall')
    }

    checkLayout()
    checkValueOnContainers()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render placeholder image faction', async done => {
    const Component = mount(<UserInfo {...withPlaceholderImgFaction} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('img').at(0).prop('src')).toBe('https://factiontags.torn.com/0-0.png')
    expect(Component.find('img').at(1).prop('src')).toBe('https://torn.com/user/localStorage.png')
    expect(Component.find('div').at(3).prop('className')).toBe('userWrap')

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render placeholder text faction', async done => {
    const Component = mount(<UserInfo {...withPlaceholderTextFaction} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('svg').prop('fill')).toBe('url("#svg_status_online")')
    expect(Component.find('span').at(0).text()).toBe('N/A')
    expect(Component.find('span').at(1).text()).toBe('Test_User')

    expect(Component).toMatchSnapshot()

    done()
  })
})
