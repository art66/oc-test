import { IProps } from '../../interfaces'

const initialState: IProps = {
  status: {
    mode: 'online'
  },
  useProgressiveImage: false,
  user: {
    imageUrl: 'https://torn.com/user/localStorage.png',
    name: 'Test_User',
    ID: 121
  },
  faction: {
    imageUrl: 'https://torn.com/faction/localStorage.png',
    name: 'Test_Faction',
    ID: 2121
  }
}

const onlyText: IProps = {
  ...initialState,
  showImages: false
}

const withoutStatus: IProps = {
  ...initialState,
  useProgressiveImage: true,
  status: {
    ...initialState.status,
    isActive: false
  }
}

const withoutFaction: IProps = {
  ...initialState,
  useProgressiveImage: true,
  faction: null
}

const withoutUser: IProps = {
  ...initialState,
  useProgressiveImage: true,
  user: {
    ...initialState.user,
    name: null,
    ID: null
  }
}

const withoutFactionOpacity: IProps = {
  ...initialState,
  faction: {
    ...initialState.faction,
    isOpacityDisabled: true
  }
}

const withSmallUserImage: IProps = {
  ...initialState,
  user: {
    ...initialState.user,
    imageUrl: 'https://torn.com/user/localStorage-large.png',
    isSmall: true
  }
}

const withPlaceholderImgFaction: IProps = {
  ...initialState,
  showImages: true,
  faction: {
    ...initialState.faction,
    imageUrl: ''
  }
}

const withPlaceholderTextFaction: IProps = {
  ...initialState,
  showImages: false,
  faction: {
    ...initialState.faction,
    name: ''
  }
}


export default initialState
export {
  onlyText,
  withoutStatus,
  withoutFaction,
  withoutUser,
  withoutFactionOpacity,
  withSmallUserImage,
  withPlaceholderImgFaction,
  withPlaceholderTextFaction
}
