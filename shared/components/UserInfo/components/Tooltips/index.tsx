import React from 'react'
import TooltipGlobal from '@torn/shared/components/TooltipNew'
import { IPosition } from '@torn/shared/components/TooltipNew/types'
import isEmpty from '@torn/shared/utils/isEmpty'

import styles from './index.cssmodule.scss'
// import './anim.cssmodule.scss'

// const ANIM_PROPS = {
//   className: 'expTooltip',
//   timeExit: 50,
//   timeIn: 300
// }

export interface IProps {
  isActive?: boolean
  position?: IPosition
  overrideStyles?: {
    tooltipContainer?: string
    tooltipTitle?: string
    tooltipArrow?: string
  }
  animation?: {
    className?: string
    timeExit?: number
    timeIn?: number
  }
  manualFix?: {
    fixX?: number
    fixY?: number
  }
}

class Tooltip extends React.PureComponent<IProps> {
  render() {
    const {
      position = { x: 'center', y: 'top' },
      overrideStyles = styles,
      animation,
      manualFix = { fixX: -7 }
    } = this.props

    const stylesToThrow = isEmpty(overrideStyles) ? styles : overrideStyles

    const STYLES = {
      container: stylesToThrow.tooltipContainer,
      title: stylesToThrow.tooltipTitle,
      tooltipArrow: stylesToThrow.tooltipArrow
    }

    return (
      <TooltipGlobal
        overrideStyles={STYLES}
        manualCoodsFix={manualFix}
        position={position}
        animProps={animation}
      />
    )
  }
}

export default Tooltip
