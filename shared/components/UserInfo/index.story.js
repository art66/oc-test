import React from 'react'

import { select, text, number, boolean } from '@storybook/addon-knobs'

import { TooltipWrapper } from '../../../.storybook/helpers'
import { appendRequiredSVGStyles } from '../../../.storybook/utils'

import UserInfo from '.'

import { DEFAULT_CONFIG, STORY_GROUPS, DEFS_HTML } from './mocks/storybook'
import styles from './mocks/index.cssmodule.scss'

export const Default = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    showImages: boolean('Show Images:', true, STORY_GROUPS.flags),
    useProgressiveImage: boolean('Use Progressive Images:', true, STORY_GROUPS.flags),
    isActiveTooltips: boolean('Activate Tooltips:', true, STORY_GROUPS.flags),
    isDecodeImg: boolean('Decode Images:', false, STORY_GROUPS.flags),
    status: {
      mode: select('User Status:', ['online', 'idle', 'offline'], 'online', STORY_GROUPS.status),
      isActive: boolean('Status Config Active:', true, STORY_GROUPS.status)
    },
    faction: {
      ID: number('Faction ID:', 124124, 0, STORY_GROUPS.faction),
      name: text('Faction Name', 'DXP', STORY_GROUPS.faction),
      imageUrl: text('Faction Image URL', 'https://factiontags.torn.com/20514-75433.png', STORY_GROUPS.faction),
      isActive: boolean('Faction Config Active:', true, STORY_GROUPS.faction),
      isOpacityDisabled: boolean('Disable Image Opacity Cut:', false, STORY_GROUPS.faction)
    },
    user: {
      ID: number('User ID:', 2114355, 0, STORY_GROUPS.user),
      name: text('User Name:', 'svyat770', STORY_GROUPS.user),
      imageUrl: text('User Image URL:', 'https://awardimages.torn.com/1036235-32804-large.png', STORY_GROUPS.user),
      isSmall: boolean('Use Small User Image:', false, STORY_GROUPS.user),
      isActive: boolean('User Config Active:', true, STORY_GROUPS.user)
    },
    customStyles: {
      container: text('Container class:', '', STORY_GROUPS.customClasses),
      blockWrap: text('Block class:', '', STORY_GROUPS.customClasses),
      status: text('Status class:', '', STORY_GROUPS.customClasses),
      img: text('Image class:', '', STORY_GROUPS.customClasses),
      text: text('Text class:', '', STORY_GROUPS.customClasses),
      link: text('Link class:', '', STORY_GROUPS.customClasses)
    }
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

Default.story = {
  name: 'default'
}

export const OnlyTexts = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    showImages: false
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

OnlyTexts.story = {
  name: 'only texts'
}

export const OnlyUserImage = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    faction: {}
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

OnlyUserImage.story = {
  name: 'only user image'
}

export const OnlyUserImageWhileTagImageIsMissed = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    faction: {
      ...DEFAULT_CONFIG.faction,
      imageUrl: ''
    }
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

OnlyUserImageWhileTagImageIsMissed.story = {
  name: 'only user image while tag image is missed'
}

export const OnlyFactionImage = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    user: {}
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

OnlyFactionImage.story = {
  name: 'only faction image'
}

export const OnlyTagImageWhileUserImageIsMissed = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    user: {
      ...DEFAULT_CONFIG.user,
      imageUrl: ''
    }
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

OnlyTagImageWhileUserImageIsMissed.story = {
  name: 'only tag image while user image is missed'
}

export const WithoutTooltips = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    isActiveTooltips: false
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

WithoutTooltips.story = {
  name: 'without tooltips'
}

export const WithoutProgressiveImage = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    useProgressiveImage: false
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

WithoutProgressiveImage.story = {
  name: 'without progressive image'
}

export const WithDecodingImage = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    isDecodeImg: true
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

WithDecodingImage.story = {
  name: 'with decoding image'
}

export const WithStatusTooltip = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    status: {
      ...DEFAULT_CONFIG.status,
      addTooltip: true
    }
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

WithStatusTooltip.story = {
  name: 'with status tooltip'
}

export const WithCustomStyles = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    customStyles: {
      container: styles.container,
      blockWrap: styles.blockWrap,
      status: styles.status,
      img: styles.img,
      text: styles.text,
      link: styles.link
    }
  }

  return (
    <TooltipWrapper>
      <UserInfo {...config} />
    </TooltipWrapper>
  )
}

WithCustomStyles.story = {
  name: 'with custom styles'
}

export const WithIntegratedTooltips = () => {
  appendRequiredSVGStyles({ defsHTML: DEFS_HTML })

  const config = {
    ...DEFAULT_CONFIG,
    tooltips: {
      isActive: true
    },
    faction: {}
  }

  return <UserInfo {...config} />
}

WithIntegratedTooltips.story = {
  name: 'with integrated tooltips'
}

export default {
  title: 'Shared/UserInfo'
}
