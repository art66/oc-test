import PropTypes from 'prop-types'
import React, { Component } from 'react'
import cn from 'classnames'
import s from './styles.cssmodule.scss'

export class ContentTitle extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showTutorial: false
    }
  }

  onTutorialToggle = () => {
    this.setState(state => ({ showTutorial: !state.showTutorial }))
  }

  render() {
    const { showTutorial } = this.state
    const { title, links, tutorial, children, bottomHr = true } = this.props

    return (
      <div className={cn('content-title m-bottom10', s.contentTitleWrap)}>
        {title && <h4 className='left'>{title}</h4>}
        {children}
        {links.length !== 0 && (
          <div className={cn('links-top-wrap', s.links)}>
            <div
              id='top-page-links-button'
              className='links-top-arrow'
              role='button'
              aria-owns='top-page-links-list'
              aria-haspopup='true'
              aria-label='Top page links'
            />
            <div
              id='top-page-links-list'
              className='content-title-links'
              role='list'
              aria-labelledby='top-page-links-button'
            >
              <div className='left-side' />
              <div className='right-side' />
              <div className='links-header'>
                <div className='l' />
              </div>
              {links.map(({ icon, title, action, link, linkClassName }, i) => (
                <a
                  key={i}
                  role='button'
                  aria-labelledby='back'
                  className={`back t-clear h c-pointer line-h24 m-left10 ${linkClassName} ${s.link}`}
                  href={link}
                  onClick={action}
                  {...(link && { target: '_blank', rel: 'noopener noreferrer' })}
                >
                  <span className='icon-wrap'>
                    {typeof icon === 'string' ? <i className={'top-page-icon ' + icon} /> : icon}
                  </span>{' '}
                  <span id='back'>{title}</span>
                </a>
              ))}
              <div className='links-footer'>
                <div className='l' />
                <div className='r' />
              </div>
            </div>
          </div>
        )}
        {tutorial && (
          <div
            role="button"
            aria-label="show tutorial"
            className={cn(s.tutorial, 'tutorial-switcher c-pointer line-h24')}
            id={tutorial.id}
            onClick={this.onTutorialToggle}
          >
            <span className='tutorial-switcher-icon tutorial-show' />
            <span>Tutorial</span>
          </div>
        )}
        {bottomHr && <hr className={cn('page-head-delimiter', s.hr)} />}
        {showTutorial && tutorial}
      </div>
    )
  }
}

ContentTitle.propTypes = {
  bottomHr: PropTypes.bool,
  links: PropTypes.array,
  title: PropTypes.string,
  tutorial: PropTypes.object
}

ContentTitle.defaultProps = {
  links: []
}

export default ContentTitle
