import { IItemsList, TCurrentTab, TFetchTypes } from './ICommon'
import { ILasts } from '../components/Lasts/interfaces'
import { IContext } from './IContext'

export type TFetchRoot = string

export interface IFetchData {
  isInit?: boolean
  isSilent?: boolean
  isDisabled?: boolean
  fetchStatus: TFetchTypes
}

export interface ICachedRequest {
  value: string
  tab: string
}

export interface ILocalStorage {
  isDisabled?: boolean
  storeKey?: string
}

export interface IWebsockets {
  isDisabled?: boolean
  channelID?: string
}

export interface IProps extends IContext {
  isDisabled?: boolean
  stylePreset?: 'grey'
  useGlobalTooltips?: boolean
  fetchRoot?: TFetchRoot
  localStorage?: ILocalStorage
  websockets?: IWebsockets
  fullLayout?: boolean
  saveOnClick?: (payload: object) => void
  customStyles?: {
    wrap?: string
    topContainer?: string
    bottomContainer?: string
  }
}

export interface IState {
  itemsList: IItemsList
  cachedRequests: ICachedRequest[]
  alreadySelectedList: number[]
  lastsSearches: ILasts[]
  isFirstSearchDone: boolean
  isFetchActive: boolean
  isHistoryAppear: boolean
  isDropDownAppear: boolean
  isLiveSearch: boolean
  isFirstSearch: boolean
  isForceSearch: boolean
  isManualSearch: boolean
  isFetchError: boolean
  inputValue: string
  activeRole: string
  isBadSymbol: boolean
  isMaxLength: boolean
  currentTab: TCurrentTab
}
