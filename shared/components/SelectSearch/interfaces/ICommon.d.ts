// eslint-disable-next-line
export type TFetchTypes = '' | 'isAllItemsTab' | 'isFirstSearch' | 'isActiveSearch' | 'isForceSearch' | 'isLiveSearch' | 'isManualSearch'
export type TCurrentTab = 'all' | 'faction' | 'company' | 'friends' | '' | string
export type TTabs = string[]

export interface ICategory {
  ID?: number
  name?: string
  category?: string
  online?: string
}

export interface IItemsList {
  all?: ICategory[]
  faction?: ICategory[]
  company?: ICategory[]
  friends?: ICategory[]
}

export interface IItemData {
  name: string
  ID: number
  role?: string
}

export type TTimestamp = string
export type TItemName = string
export type TItemID = string

export type TUsersCategories = '' | 'all' | 'faction' | 'friends' | 'company'
export type TOnlineStatus = 'online' | 'offline' | 'idle'

export interface IUserRaw {
  0: TUsersCategories
  1: TItemID
  2: TItemName
  3: TTimestamp
  type: TUsersCategories
  id: TItemID
  name: TItemName
  online: TOnlineStatus
}

export interface IUserRawWebsocket extends IUserRaw {
  isDelete?: boolean
}

export interface IFetch {
  isInit?: boolean
  isSilent?: boolean
  isDisabled?: boolean
  fetchStatus: TFetchTypes
}

export interface IMatch {
  itemMatchValue: string
}

export interface ISystemData {
  activateTooltips?: boolean
  stylePreset?: 'grey'
}
