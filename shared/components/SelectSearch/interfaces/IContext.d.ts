import { IInputConfiguration } from '../components/Input/interfaces'
import { IButtonsConfiguration } from '../components/Buttons/interfaces'
import { IEnhancersConfiguration } from '../components/Enhancers/interfaces'
import { ITooltipCustomConfig } from '../components/Tooltips/interfaces'
import { ITabsConfig } from '../components/Tabs/interfaces'
import { ILastsConfiguration } from '../components/Lasts/interfaces'
import { IDropDownConfig } from '../components/DropDown/interfaces'
import { IConfigPlaceholder } from '../components/Placeholder/interfaces'
import { ISystemData } from './ICommon'

export interface IContext {
  systemData?: ISystemData
  input?: IInputConfiguration
  tabs?: ITabsConfig
  lasts?: ILastsConfiguration
  dropdown?: IDropDownConfig
  placeholders?: IConfigPlaceholder
  buttons?: IButtonsConfiguration
  tooltips?: ITooltipCustomConfig
  enhancers?: IEnhancersConfiguration
}
