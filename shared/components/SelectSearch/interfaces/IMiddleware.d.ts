import { IUserRaw } from './ICommon'

export type TCallback = (value: IUserRaw[]) => void

export interface IPayload {
  itemsData: IUserRaw[]
}

export interface IWebsockets {
  callback: TCallback
  actionID?: string
  channelID: string
}
