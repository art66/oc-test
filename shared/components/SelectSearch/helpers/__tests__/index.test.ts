import getContext from '../getContext'

import initialState, { buttonsState, noState } from './mocks'

describe('getContext()', () => {
  it('should return input context', () => {
    expect(getContext(initialState)).toEqual({ isDisabled: false })
  })
  it('should return nested buttons context', () => {
    expect(getContext(buttonsState)).toEqual({ isDisabled: false })
  })
  it('should return null in case no context were found', () => {
    expect(getContext(noState)).toEqual(null)
  })
})
