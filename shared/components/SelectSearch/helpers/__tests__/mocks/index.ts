const initialState = {
  context: {
    input: {
      isDisabled: false
    }
  },
  name: 'input',
  type: ''
}

const buttonsState = {
  ...initialState,
  context: {
    ...initialState.context,
    buttons: {
      commitButton: {
        isDisabled: true
      }
    }
  },
  type: 'commitButton'
}

const noState = {
  ...initialState,
  name: 'test'
}

export default initialState
export { buttonsState, noState }
