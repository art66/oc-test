import { IContext } from '../interfaces/IContext'

const getContext = ({ context, name, type }: { context: IContext; name: string; type?: string }) => {
  if (!context || !name || !context[name]) {
    console.error('Some error happen during Context processing: ', context, name, type)

    return null
  }

  const configuration = context[name]

  if (configuration && configuration[type]) {
    return configuration[type]
  }

  return configuration
}

export default getContext
