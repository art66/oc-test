import React from 'react'

import { select, boolean, text, number } from '@storybook/addon-knobs'

import { TooltipWrapper } from '../../../.storybook/helpers'

import UsersSearch from '.'
import { DEFAULT_CONFIG } from './mocks/storybook'

export const Default = () => {
  return (
    <TooltipWrapper>
      <UsersSearch {...DEFAULT_CONFIG} />
    </TooltipWrapper>
  )
}

Default.story = {
  name: 'default'
}

export const NoLocalStorage = () => {
  const config = {
    ...DEFAULT_CONFIG,
    localStorage: {
      isDisabled: boolean('Disable LocalStorage: ', true)
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

NoLocalStorage.story = {
  name: 'noLocalStorage'
}

export const Common = () => {
  const config = {
    ...DEFAULT_CONFIG,
    isDisabled: boolean('Disable Module from render', false),
    fullLayout: boolean('Set Full layout: ', true),
    fetchRoot: select('Set Root Fetch URL', ['page.php', ''], ''),
    stylePreset: select('Set Preset Type: ', ['grey', 'blue'], 'grey'),
    useGlobalTooltips: boolean('Set Tooltips Controller to being used from Global Context: ', true),
    systemData: {
      activateTooltips: boolean('Set Whole Tooltips to Appear: ', true)
    },
    localStorage: {
      isDisabled: boolean('Disable LocalStorage: ', false),
      storeKey: select('Set LocalStorage Key', ['test_store', ''], 'test_store')
    },
    websockets: {
      isDisabled: boolean('Disable Websockets: ', false),
      channelID: select('Set Websockets ID', ['user', ''], 'user')
    },
    customStyles: {
      wrap: text('Set Wrap Class', 'wrap'),
      topContainer: text('Set Top Section Class', 'topContainer'),
      bottomContainer: text('Set Bottom Section Class', 'bottomContainer')
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Common.story = {
  name: 'common'
}

export const Presets = () => {
  const preset = select('Select preset: ', ['blue', 'grey'], 'blue')

  return (
    <TooltipWrapper>
      <UsersSearch {...DEFAULT_CONFIG} systemData={{ stylePreset: preset }} />
    </TooltipWrapper>
  )
}

Presets.story = {
  name: 'presets'
}

export const Tooltips = () => {
  const config = {
    ...DEFAULT_CONFIG,
    tooltips: {
      isDisabled: boolean('Disable Tooltips: ', false),
      texts: {
        badText: text('Bad Tooltip Text: ', 'Some ERROR HERE!'),
        maxText: text('Max Length Tooltip Text: ', 'MAX LENGTH!!')
      },
      animation: {
        timeIn: number('Set Timein Animation: ', 2000),
        timeOut: number('Set Timeout Animation: ', 2000)
      },
      customStyles: {
        tooltipWrap: 'tooltipWrap',
        animClass: 'animClass'
      }
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Tooltips.story = {
  name: 'tooltips'
}

export const Input = () => {
  const config = {
    ...DEFAULT_CONFIG,
    input: {
      isDisabled: boolean('Disable Input: ', false),
      readOnly: boolean('ReadOnly Input: ', false),
      placeholder: text('Set Input Placeholder: ', 'Type something...'),
      debounceDelay: number('Set Debounce Delay: ', 1000),
      disableBackdrop: boolean('Disabled Backdrop: ', false),
      clearInputOnClick: boolean('Activate input clear on save: ', false),
      inputChecker: {
        maxLength: number('Maximum Input Length: ', 34),
        allowedSymbols: '^[a-zA-Z0-9-_]+$',
        removeSpaces: boolean('Remove Space input: ', true)
      },
      customStyles: {
        inputWrap: text('Wrapper Class: ', 'inputWrap'),
        search: text('Search Class: ', 'search'),
        activeDropDown: text('Active DropDown Class: ', 'activeDropDown'),
        warning: text('Warning Class: ', 'warning'),
        disabled: text('Disabled Class: ', 'disabled'),
        backdrop: text('Backdrop Class: ', 'backdrop')
      }
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Input.story = {
  name: 'input'
}

export const Enhancers = () => {
  const GROUPS = {
    addSave: 'AddSave',
    addRole: 'AddRole'
  }

  const config = {
    ...DEFAULT_CONFIG,
    enhancers: {
      isActive: boolean('Active: ', true),
      list: [
        {
          type: 'addSave',
          isDisabled: boolean('Disable AddSave: ', false, GROUPS.addSave),
          iconName: select('Icon AddSave: ', ['Plus', 'Minus'], 'Plus', GROUPS.addSave),
          iconProps: {},
          customChild: select('Set custom child: ', [null, '+'], null, GROUPS.addSave),
          tooltips: {
            isActive: boolean('Add Tooltips AddSave: ', true, GROUPS.addSave),
            text: text('Tooltips AddSave Text: ', 'Add', GROUPS.addSave)
          },
          customStyles: {
            container: text('Container Class AddSave: ', 'testContainer', GROUPS.addSave),
            containerDisabled: text('Container Disabled Class AddSave: ', 'testContainerDisabled', GROUPS.addSave)
          }
        },
        {
          type: 'addRole',
          isDisabled: boolean('Disable AddRole: ', false, GROUPS.addRole),
          activeRole: select('Set Active Role: ', ['tester', 'editor'], 'tester', GROUPS.addRole),
          roles: ['tester', 'editor'],
          disabledRoles: [select('Disabled Role: ', ['tester', 'editor'], 'tester', GROUPS.addRole)],
          iconNames: {
            editor: select('Icon Editor AddRole: ', ['Plus', 'Cubes'], 'Cubes', GROUPS.addRole),
            tester: select('Icon Tester AddRole: ', ['Minus', 'Testing'], 'Testing', GROUPS.addRole)
          },
          iconProps: {},
          tooltips: {
            isActive: boolean('Add Tooltips AddRole: ', true, GROUPS.addRole)
          },
          customStyles: {
            container: text('Container Class AddRole: ', 'testContainer', GROUPS.addRole),
            containerDisabled: text('Container Disabled Class AddRole: ', 'testContainerDisabled', GROUPS.addRole)
          }
        }
      ]
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Enhancers.story = {
  name: 'enhancers'
}

export const Buttons = () => {
  const GROUPS = {
    global: 'Global',
    commit: 'Commit',
    history: 'History',
    clear: 'Clear'
  }

  const config = {
    ...DEFAULT_CONFIG,
    buttons: {
      isDisabled: boolean('Disable Buttons: ', false, GROUPS.global),
      activateTooltips: boolean('Activate Buttons Tooltips: ', false, GROUPS.global),
      commonActiveColor: select('Icons Active Color: ', ['#efe', '#ece', ''], '#ece', GROUPS.global),
      commonDisabledColor: select('Icons Disable Color: ', ['#fdfefe', '#92b6c7', ''], '#92b6c7', GROUPS.global),
      commonIconsProps: {},
      clearButton: {
        isDisabled: boolean('Disable Clear Button: ', false, GROUPS.clear),
        iconName: select('Icon Clear Name: ', ['Close', 'History'], 'Close', GROUPS.clear),
        iconActiveColor: select('Icon Clear Active Color: ', ['#ccc', '#111'], '#ccc', GROUPS.clear),
        iconDisabledColor: select('Icon Clear Disabled Color: ', ['#fafa', '#caca'], '#fafa', GROUPS.clear),
        iconProps: {},
        tooltipText: text('Set Clear Tooltip Text: ', 'Clear', GROUPS.clear),
        customStyles: {
          buttonClass: text('Set Clear Class: ', 'clearButton', GROUPS.clear)
        }
      },
      historyButton: {
        isDisabled: boolean('Disable History Button: ', false, GROUPS.history),
        iconName: select('Icon History Name: ', ['Plus', 'History'], 'History', GROUPS.history),
        iconActiveColor: select('Icon History Active Color: ', ['#ccc', '#111'], '#ccc', GROUPS.history),
        iconDisabledColor: select('Icon History Disabled Color: ', ['#fafa', '#caca'], '#fafa', GROUPS.history),
        iconProps: {},
        tooltipText: text('Set History Tooltip Text: ', 'History', GROUPS.history),
        customStyles: {
          buttonClass: text('Set History Class: ', 'historyButton', GROUPS.history)
        }
      },
      commitButton: {
        isDisabled: boolean('Disable Commit Button: ', false, GROUPS.commit),
        iconName: select('Icon Commit Name: ', ['Plus', 'Search'], 'Search', GROUPS.commit),
        iconActiveColor: select('Icon Commit Active Color: ', ['#ccc', '#111'], '#ccc', GROUPS.history),
        iconDisabledColor: select('Icon Commit Disabled Color: ', ['#fafa', '#caca'], '#fafa', GROUPS.history),
        iconProps: {},
        tooltipText: text('Set Commit Tooltip Text: ', 'Commit', GROUPS.history),
        customStyles: {
          buttonClass: text('Set Commit Class: ', 'commitButton', GROUPS.commit),
          buttonDisabled: text('Set Commit Disabled Class: ', 'disabledCommitButton', GROUPS.commit),
          animation: text('Set Commit Animation Class: ', 'animCommitButton', GROUPS.commit)
        }
      }
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Buttons.story = {
  name: 'buttons'
}

export const Tabs = () => {
  const config = {
    ...DEFAULT_CONFIG,
    tabs: {
      isDisabled: boolean('Disable Tabs: ', false),
      activeTab: select('Set Active Tab: ', ['friends', 'faction', 'company', 'all'], 'company'),
      categories: select(
        'Set Categories: ',
        [['friends', 'faction', 'company', 'all'], ['friends', 'faction'], null],
        ['friends', 'faction', 'company', 'all']
      ),
      customStyles: {
        tabsWrap: 'tabsWrap',
        tabButton: 'tabButton',
        tabButtonClicked: 'tabButtonClicked'
      }
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Tabs.story = {
  name: 'tabs'
}

export const Lasts = () => {
  const config = {
    ...DEFAULT_CONFIG,
    lasts: {
      isDisabled: boolean('Disable History: ', false),
      storeLastsKey: text('Set localStorage key for History: ', 'test'),
      iconCloseName: select('Icon Close Name: ', ['Close', 'History'], 'Close'),
      iconHistoryName: select('Icon History Name: ', ['Close', 'History'], 'History'),
      iconPropsHistory: {},
      iconPropsRemove: {},
      activateTooltips: boolean('Activate Tooltips: ', false),
      tooltipText: text('Set Tooltip Text: ', 'Delete From History!'),
      customStyles: {
        wrap: text('Set Wrap Class: ', 'wrap'),
        lastContainer: text('Set lastContainer Class: ', 'lastContainer'),
        iconSvg: text('Set iconSvg Class: ', 'iconSvg'),
        buttonRemove: text('Set buttonRemove Class: ', 'buttonRemove'),
        label: text('Set label Class: ', 'label')
      }
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Lasts.story = {
  name: 'lasts'
}

export const Dropdown = () => {
  const config = {
    ...DEFAULT_CONFIG,
    dropdown: {
      isDisabled: boolean('Disable DropDown: ', false),
      type: select('users', ['users'], 'users'),
      hideByClick: boolean('Hide Items List By Click', false),
      alreadySelected: {
        isDisabled: boolean('Disable Already Selected Label from Appear: ', false),
        customAlreadySelected: select('Set Custom Already Selected Child: ', [null, 'x'], null),
        alreadySelectedText: text('Set Custom Already Selected Text: ', 'test')
      },
      customConfig: {
        users: {
          status: {
            isDisabled: boolean('Disable Users Status from Appear: ', false),
            isSVGLayout: boolean('Use SVG Statuses: ', false),
            SVGStatusData: null
          },
          ID: {
            isDisabled: boolean('Disable Users ID from Appear: ', false)
          },
          name: {
            isDisabled: boolean('Disable Users Name from Appear: ', false)
          }
        }
      },
      customStyles: {
        users: {
          dropDownWrap: text('Set DropDown Class: ', 'dropDownWrap'),
          onlineStatus: text('Set Online Class: ', 'onlineStatus'),
          onlineStatusIdle: text('Set Offline Class: ', 'onlineStatusIdle'),
          onlineStatusActive: text('Set Online Class: ', 'onlineStatusActive'),
          unitContainer: text('Set User Container Class: ', 'unitContainer'),
          unitContainerSelected: text('Set User Container Selected: ', 'unitContainerSelected'),
          name: text('Set User Name Class: ', 'name'),
          id: text('Set User ID Class: ', 'id'),
          alreadyInLabel: text('Set Already In Label: ', 'alreadyInLabel')
        }
      }
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Dropdown.story = {
  name: 'dropdown'
}

export const Placeholders = () => {
  const config = {
    ...DEFAULT_CONFIG,
    placeholders: {
      isDisabled: boolean('Disable Placeholders: ', false),
      spinner: {
        isDisabled: boolean('Disable Spinner Anim: ', false),
        customSpinner: boolean('Disable Spinner Anim: ', 'x')
      },
      texts: {
        fallback: text('Set Fallback text', 'FALL BACK TEXT HERE'),
        init: text('Set Init text', 'INIT LOAD TEXT HERE'),
        live: text('Set Live text', 'LIVE SEARCH TEXT HERE'),
        fail: text('Set Fail text', 'FAIL SEARCH TEXT HERE')
      },
      customStyles: {
        loadingAnim: text('Set Load Anim Class', 'loadingAnim'),
        textPlaceholder: text('Set Text Class', 'textPlaceholder')
      }
    }
  }

  return (
    <TooltipWrapper>
      <UsersSearch {...config} />
    </TooltipWrapper>
  )
}

Placeholders.story = {
  name: 'placeholders'
}

export default {
  title: 'Shared/SelectSearch'
}
