import React from 'react'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import keyDownHandler, { KEY_TYPES } from '@torn/shared/utils/keyDownHandler'
import isEmpty from '@torn/shared/utils/isEmpty'

import { IProps, ILasts, ILastsConfiguration } from './interfaces'
import styles from './index.cssmodule.scss'
import NestedContext from '../../context'
import getContext from '../../helpers/getContext'
import { tooltipsSubscriber } from '../../../TooltipNew'

const ICON_NAME_DEFAULT = 'History'
const DEFAULT_TOOLTIP = 'Delete'
const ICON_HISTORY_PROPS = {
  fill: {
    color: '#d0cdcd', strokeWidth: 0
  },
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '-1 -2 28 28'
  },
  onHover: {
    active: true,
    fill: { color: '#908989', strokeWidth: 0 }
  }
}
const ICON_REMOVE_PROPS = {
  fill: {
    color: '#d0cdcd',
    strokeWidth: 0
  },
  dimensions: {
    width: 10,
    height: 10,
    viewbox: '0 -4 28 28'
  },
  onHover: {
    active: true,
    fill: {
      color: '#908989',
      strokeWidth: 0
    }
  }
}

class Lasts extends React.Component<IProps> {
  // static contextType = NestedContext

  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _initTooltips = () => {
    const { tooltipText } = this._getContext()
    const lastSearchList = this._getLatsList()

    if (!this._isTooltipsAllowed()) {
      return []
    }

    return lastSearchList.map((item: ILasts) => ({
      child: tooltipText || DEFAULT_TOOLTIP,
      ID: `${item.name}_${DEFAULT_TOOLTIP.toLowerCase()}`
    }))
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'update' })

  _isTooltipsAllowed = () => {
    const { context } = this.props
    const { systemData: { activateTooltips: isActiveTooltips } } = context
    const { activateTooltips } = this._getContext()

    return isActiveTooltips || activateTooltips
  }

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'lasts' })

    return contextData as ILastsConfiguration || {}
  }

  _classNamesHolder = () => {
    const { customStyles } = this._getContext()
    const { wrap, lastContainer, iconSvg, buttonRemove, label } = customStyles || {}

    const containerClass = classnames({
      [styles.lastsWrap]: true,
      [wrap]: wrap
    })

    const buttonClass = classnames({
      [styles.lastContainer]: true,
      [lastContainer]: lastContainer
    })

    const labelClass = classnames({
      [styles.label]: true,
      [label]: label
    })

    const iconClass = classnames({
      [styles.iconSvg]: true,
      [iconSvg]: iconSvg
    })

    const removeClass = classnames({
      [styles.buttonRemove]: true,
      [buttonRemove]: buttonRemove
    })

    return {
      containerClass,
      buttonClass,
      labelClass,
      iconClass,
      removeClass
    }
  }

  _handleClick = (event: React.MouseEvent<HTMLDivElement> & React.KeyboardEvent<HTMLButtonElement>) => {
    event.preventDefault()

    const { callbackClick } = this._getContext()
    const { onClick } = this.props

    if (!onClick || !event) {
      return
    }

    const { dataset } = event.target as HTMLButtonElement
    const { name } = dataset

    callbackClick && callbackClick(name)
    onClick(name)
  }

  _handleDeleteLast = (event: React.MouseEvent<HTMLButtonElement> & React.KeyboardEvent<HTMLButtonElement>) => {
    event.preventDefault()
    event.stopPropagation()

    const { callbackDelete } = this._getContext()
    const { onDelete } = this.props

    const { dataset } = event.target && (event.target as any).parentNode
    const { name } = dataset

    if (!onDelete || !event.target) {
      return
    }

    callbackDelete && callbackDelete(name)
    onDelete(name)
  }

  _handleMouseDown = (event: React.MouseEvent<HTMLButtonElement> | React.MouseEvent<HTMLDivElement>) => {
    event.preventDefault()
  }

  _handleKeyDownDelete = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: this._handleDeleteLast
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleDeleteLast
        }
      ]
    })
  }

  _handleKeyDownClick = (event: React.KeyboardEvent<HTMLDivElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: this._handleClick
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleClick
        }
      ]
    })
  }

  _getLatsList = () => {
    const { lastsSearches } = this.props

    return lastsSearches
  }

  _getIcon = () => {
    const { iconHistoryName, iconPropsHistory } = this._getContext()
    const { iconClass } = this._classNamesHolder()

    return (
      <span className={iconClass}>
        <SVGIconGenerator
          isDeepParent={true}
          iconName={iconHistoryName || ICON_NAME_DEFAULT}
          {...ICON_HISTORY_PROPS}
          {...iconPropsHistory}
        />
      </span>
    )
  }

  _getLabel = (label: string) => {
    const { labelClass } = this._classNamesHolder()

    return (
      <span className={labelClass}>{label}</span>
    )
  }

  _getRemoveButton = (name: string) => {
    const { iconCloseName, iconPropsRemove } = this._getContext()
    const { removeClass } = this._classNamesHolder()

    return (
      <button
        id={this._isTooltipsAllowed() ? `${name}_${DEFAULT_TOOLTIP.toLowerCase()}` : ''}
        type='button'
        className={removeClass}
        onClick={this._handleDeleteLast}
        onKeyDown={this._handleKeyDownDelete}
        onMouseDown={this._handleMouseDown}
      >
        <SVGIconGenerator
          iconName={iconCloseName || 'Close'}
          {...ICON_REMOVE_PROPS}
          {...iconPropsRemove}
        />
      </button>
    )
  }

  _renderButton = (name: string) => {
    return (
      <React.Fragment>
        {this._getIcon()}
        {this._getLabel(name)}
        {this._getRemoveButton(name)}
      </React.Fragment>
    )
  }

  _renderList = (list: ILasts[]) => {
    const { buttonClass } = this._classNamesHolder()

    return list.map(({ name }: ILasts) => {
      return (
        <div
          key={name}
          role='button'
          tabIndex={0}
          data-name={name}
          className={buttonClass}
          onClick={this._handleClick}
          onMouseDown={this._handleMouseDown}
          onKeyDown={this._handleKeyDownClick}
        >
          {this._renderButton(name)}
        </div>
      )
    })
  }

  render() {
    const { isDisabled } = this._getContext()
    const { isHistoryAppear } = this.props
    const lastSearchList = this._getLatsList()

    if (isEmpty(lastSearchList) || isDisabled || !isHistoryAppear) {
      return null
    }

    const { containerClass } = this._classNamesHolder()

    return (
      <div className={containerClass}>
        {this._renderList(lastSearchList)}
      </div>
    )
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <Lasts context={context} {...props} />}
  </NestedContext.Consumer>
)
