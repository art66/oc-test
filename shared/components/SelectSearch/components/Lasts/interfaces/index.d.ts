export interface ILastsConfiguration {
  isDisabled?: boolean
  storeLastsKey?: string
  iconHistoryName?: string
  iconCloseName?: string
  iconPropsHistory?: object
  iconPropsRemove?: object
  activateTooltips?: boolean
  tooltipText?: string
  callbackClick?: (payload: string) => void
  callbackDelete?: (payload: string) => void
  customStyles?: {
    wrap?: string
    lastContainer?: string
    iconSvg?: string
    buttonRemove?: string
    label?: string
  }
}

export interface ILasts {
  name: string
}

export interface IProps {
  lastsSearches: ILasts[]
  isHistoryAppear: boolean
  storeLastsKey: string
  onClick: (name: string) => void
  onDelete: (name: string) => void
  context?: any // temporary!
}
