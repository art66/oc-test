import context from './context'

const initialState = {
  lastsSearches: [{ name: 'test' }],
  isHistoryAppear: true,
  storeLastsKey: '',
  onClick: (name: string) => name,
  onDelete: (name: string) => name,
  context
}

const noList = {
  ...initialState,
  lastsSearches: []
}

const historyHidden = {
  ...initialState,
  isHistoryAppear: false
}

export default initialState
export { noList, historyHidden }
