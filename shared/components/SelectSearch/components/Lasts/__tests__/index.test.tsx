import React from 'react'
import { mount } from 'enzyme'

import Lasts from '..'
import initialState, { noList, historyHidden } from './mocks'

describe('<Lasts />', () => {
  it('should render regular lasts layout', async done => {
    const Component = mount(<Lasts {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Lasts').length).toBe(1)
    expect(Component.find('div.lastsWrap').length).toBe(1)
    expect(Component.find('div.lastContainer').length).toBe(1)
    expect(Component.find('span.iconSvg').length).toBe(1)
    expect(Component.find('span.iconSvg SVGIconGenerator').prop('iconName')).toBe('History')
    expect(Component.find('span.label').length).toBe(1)
    expect(Component.find('span.label').text()).toBe('test')
    expect(Component.find('button.buttonRemove').length).toBe(1)
    expect(Component.find('button.buttonRemove SVGIconGenerator').prop('iconName')).toBe('Close')

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should not render Lasts in case it restricted by empty list provided', () => {
    const Component = mount(<Lasts {...noList} />)

    expect(Component.find('Lasts').length).toBe(1)
    expect(Component.find('div.lastsWrap').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should not render Lasts in case it restricted by false isHistoryAppear flag', () => {
    const Component = mount(<Lasts {...historyHidden} />)

    expect(Component.find('Lasts').length).toBe(1)
    expect(Component.find('div.lastsWrap').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
