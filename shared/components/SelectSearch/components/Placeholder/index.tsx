import React from 'react'
import classnames from 'classnames'

import { IProps, TPlaceholderTypes, IConfigPlaceholder } from './interfaces'
import spinnerStyles from '../../styles/animation.cssmodule.scss'
import styles from './index.cssmodule.scss'
import NestedContext from '../../context'
import getContext from '../../helpers/getContext'

class Placeholder extends React.PureComponent<IProps> {
  // static contextType = NestedContext

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'placeholders' })

    return contextData as IConfigPlaceholder || {}
  }

  _classNameHolder = () => {
    const { customStyles } = this._getContext()
    const { inProgress } = this.props
    const { wrap, loadingAnim, textPlaceholder } = customStyles || {}

    const placeholderWrap = classnames({
      [styles.dropDownListPlaceholder]: true,
      [wrap]: wrap
    })

    const placeholderSpinner = classnames({
      [spinnerStyles.loadingSpinner]: inProgress,
      [styles.loadingAnim]: inProgress,
      [loadingAnim]: loadingAnim && inProgress
    })

    const placeholderTexts = classnames({
      [styles.dropDownListPlaceholderText]: true,
      [textPlaceholder]: textPlaceholder
    })

    return {
      placeholderWrap,
      placeholderSpinner,
      placeholderTexts
    }
  }

  _renderSpinner = () => {
    const { spinner } = this._getContext()
    const { isDisabled, customSpinner } = spinner || {}

    const { placeholderSpinner } = this._classNameHolder()

    if (isDisabled) {
      return null
    }

    if (customSpinner) {
      return customSpinner
    }

    return <i className={placeholderSpinner} />
  }

  _renderPlaceholder = (placeholderText: string) => {
    const { placeholderWrap, placeholderTexts } = this._classNameHolder()

    return (
      <div className={placeholderWrap}>
        {this._renderSpinner()}
        <span className={placeholderTexts}>
          {placeholderText}
        </span>
      </div>
    )
  }

  _renderCurrentPlaceholder = (typeToRender: TPlaceholderTypes) => {
    const { texts } = this._getContext()
    const { fallback, init, live, fail } = texts || {}

    const placeholderHolder = {
      default: this._renderPlaceholder(fallback || 'You don\'t have anything here yet.'),
      init: this._renderPlaceholder(init || 'Fetching your lists...'),
      live: this._renderPlaceholder(live || 'Searching, doing my best...'),
      fail: this._renderPlaceholder(fail || 'Nothing were found, sorry.')
    }

    return placeholderHolder[typeToRender]
  }

  render() {
    const { isDisabled } = this._getContext()
    const { type } = this.props

    if (!type) {
      return null
    }

    if (isDisabled) {
      return null
    }

    return this._renderCurrentPlaceholder(type)
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <Placeholder context={context} {...props} />}
  </NestedContext.Consumer>
)
