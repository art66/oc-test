export interface IConfigPlaceholder {
  isDisabled?: boolean
  spinner?: {
    isDisabled?: boolean
    customSpinner?: JSX.Element
  }
  texts?: {
    fallback?: string
    init?: string
    live?: string
    fail?: string
  }
  customStyles?: {
    wrap?: string
    loadingAnim?: string
    textPlaceholder?: string
  }
}

export interface IProps {
  type: TPlaceholderTypes
  inProgress: boolean
  context?: any // temporary!
}

export type TPlaceholderTypes = 'default' | 'init' | 'live' | 'fail'
