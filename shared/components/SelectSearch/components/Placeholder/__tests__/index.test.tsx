import React from 'react'
import { mount } from 'enzyme'

import Placeholder from '..'
import initialState, { defaultSearch, liveSearch, failSearch } from './mocks'

describe('<Placeholder />', () => {
  it('should render init placeholder layout', () => {
    const Component = mount(<Placeholder {...initialState} />)

    expect(Component.find('Placeholder').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholder').length).toBe(1)
    expect(Component.find('.loadingSpinner').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholderText').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholderText').text()).toBe('Fetching your lists...')

    expect(Component).toMatchSnapshot()
  })
  it('should render default placeholder layout', () => {
    const Component = mount(<Placeholder {...defaultSearch} />)

    expect(Component.find('Placeholder').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholder').length).toBe(1)
    expect(Component.find('.loadingSpinner').length).toBe(0)
    expect(Component.find('.dropDownListPlaceholderText').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholderText').text()).toBe('You don\'t have anything here yet.')

    expect(Component).toMatchSnapshot()
  })
  it('should render live placeholder layout', () => {
    const Component = mount(<Placeholder {...liveSearch} />)

    expect(Component.find('Placeholder').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholder').length).toBe(1)
    expect(Component.find('.loadingSpinner').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholderText').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholderText').text()).toBe('Searching, doing my best...')

    expect(Component).toMatchSnapshot()
  })
  it('should render fail placeholder layout', () => {
    const Component = mount(<Placeholder {...failSearch} />)

    expect(Component.find('Placeholder').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholder').length).toBe(1)
    expect(Component.find('.loadingSpinner').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholderText').length).toBe(1)
    expect(Component.find('.dropDownListPlaceholderText').text()).toBe('Nothing were found, sorry.')

    expect(Component).toMatchSnapshot()
  })
})
