import context from './context'

const initialState = {
  type: 'init',
  inProgress: true,
  context
}

const defaultSearch = {
  ...initialState,
  type: 'default',
  inProgress: false
}

const liveSearch = {
  ...initialState,
  type: 'live'
}

const failSearch = {
  ...initialState,
  type: 'fail'
}

export default initialState
export { defaultSearch, liveSearch, failSearch }
