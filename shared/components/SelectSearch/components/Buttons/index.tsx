import Clear from './Clear'
import Commit from './Commit'
import History from './History'

export default { Clear, Commit, History }
