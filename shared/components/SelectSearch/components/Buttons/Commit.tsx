import React from 'react'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import keyDownHandler, { KEY_TYPES } from '@torn/shared/utils/keyDownHandler'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import iconStyler from '../../utils/iconStyler'
import getContext from '../../helpers/getContext'
import NestedContext from '../../context'

import { SEARCH_LOOP_DIMENSIONS } from '../../constants'
import { IProps, ICommitButtonConfiguration } from './interfaces/ICommit'

import styles from './index.cssmodule.scss'
import globalStyles from '../../styles/common.cssmodule.scss'

const DEFAULT_TOOLTIP = 'Search'

class Commit extends React.PureComponent<IProps> {
  // static contextType = NestedContext

  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  // shouldComponentUpdate(prevProps: IProps) {
  //   const { inputValue, isFetchActive } = this.props

  //   return prevProps.inputValue !== inputValue || prevProps.isFetchActive !== isFetchActive
  // }

  _initTooltips = () => {
    const { tooltipText } = this._getContext()

    if (this._isTooltipAllowed()) {
      return []
    }

    return [
      {
        child: tooltipText || DEFAULT_TOOLTIP,
        ID: DEFAULT_TOOLTIP.toLowerCase()
      }
    ]
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'update' })

  _isTooltipAllowed = () => {
    const { context } = this.props
    const {
      systemData: { activateTooltips: isActiveTooltips },
      buttons: { activateTooltips }
    } = context

    return isActiveTooltips || activateTooltips
  }

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context: context, name: 'buttons', type: 'commitButton' })

    return (contextData as ICommitButtonConfiguration) || {}
  }

  _classNameHolder = () => {
    const { customStyles } = this._getContext()
    const { animation, buttonClass, buttonDisabled } = customStyles || {}

    const { inputValue } = this.props

    const buttonClasses = classnames({
      [styles.buttonCommit]: true,
      [globalStyles.buttonDisabled]: !inputValue,
      [buttonClass]: buttonClass,
      [buttonDisabled]: buttonDisabled && !inputValue
    })

    const animClass = classnames({
      [styles.searchTypingLoader]: true,
      [animation]: animation
    })

    return {
      buttonClasses,
      animClass
    }
  }

  _handleClick = (event: React.MouseEvent<HTMLButtonElement> & React.KeyboardEvent<HTMLButtonElement>) => {
    event.preventDefault()

    const { onClick } = this.props

    onClick(event)
  }

  _handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  }

  _handleKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: this._handleClick
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleClick
        }
      ]
    })
  }

  _getIconStyles = () => {
    const { inputValue, context } = this.props
    const { buttons, systemData } = context
    const { stylePreset } = systemData
    const { commonIconsProps = {}, commonDisabledColor, commonActiveColor } = buttons
    const { iconDisabledColor, iconActiveColor, iconProps = {} } = this._getContext()

    const iconPostprocessingStyles = iconStyler({
      preset: stylePreset,
      isHover: false,
      isStroke: true,
      isDynamicColor: !!inputValue,
      commonActiveColor,
      commonDisabledColor,
      commonIconsProps,
      iconActiveColor,
      iconDisabledColor,
      iconProps
    })

    return iconPostprocessingStyles
  }

  _renderInputSearchButton = () => {
    const { isFetchActive, context } = this.props
    const { buttons } = context
    const { commonIconsProps = {} } = buttons

    const { iconName, iconProps = {} } = this._getContext()

    const { buttonClasses, animClass } = this._classNameHolder()

    if (isFetchActive) {
      return <span className={animClass} />
    }

    return (
      <button
        id={(this._isTooltipAllowed() && DEFAULT_TOOLTIP.toLowerCase()) || ''}
        type="button"
        className={buttonClasses}
        onClick={this._handleClick}
        onMouseDown={this._handleMouseDown}
        onKeyDown={this._handleKeyDown}
      >
        <SVGIconGenerator
          iconName={iconName || 'Search'}
          dimensions={SEARCH_LOOP_DIMENSIONS}
          {...commonIconsProps}
          {...iconProps}
          {...this._getIconStyles()}
        />
      </button>
    )
  }

  render() {
    const { isDisabled } = this._getContext()

    if (isDisabled) {
      return null
    }

    return this._renderInputSearchButton()
  }
}

export default props => (
  <NestedContext.Consumer>{context => <Commit context={context} {...props} />}</NestedContext.Consumer>
)
