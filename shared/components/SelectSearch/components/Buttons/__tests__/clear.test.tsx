import React from 'react'
import { mount, ReactWrapper } from 'enzyme'

import Clear from '../Clear'
import initialState from './mocks/clear'

describe('<Clear />', () => {
  const checkBasicButtonLayout = (component: ReactWrapper) => {
    const Component = component

    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('fill')).toBe('#d4dfe5')
  }

  it('should return basic button in case inputValue provided', async done => {
    const Component = mount(<Clear {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    checkBasicButtonLayout(Component)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return empty button in case no inputValue was provided', async done => {
    const config = {
      ...initialState,
      inputValue: ''
    }

    const Component = mount(<Clear {...config} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('button').length).toBe(0)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return basic button with active onHover configuration', async done => {
    const Component = mount(<Clear {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    checkBasicButtonLayout(Component)

    const {
      active: isHoverActive
    } = Component.find('SVGIconGenerator').prop('onHover')

    expect(isHoverActive).toBeTruthy()

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should appear then disappear and appear again', async done => {
    const Component = mount(<Clear {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component).toMatchSnapshot()
    checkBasicButtonLayout(Component)

    Component.setProps({
      ...initialState,
      inputValue: ''
    })

    expect(Component).toMatchSnapshot()
    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('button').length).toBe(0)

    Component.setProps({
      ...initialState,
      inputValue: 'world'
    })

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component).toMatchSnapshot()
    checkBasicButtonLayout(Component)

    done()
  })
})
