import React from 'react'
import { mount } from 'enzyme'

import Commit from '../Commit'
import initialState from './mocks/commit'

describe('<Commit />', () => {
  it('should return basic button', async done => {
    const config = {
      ...initialState,
      inputValue: ''
    }

    const Component = mount(<Commit {...config} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Commit').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('fill')).toBe('#d4dfe5')

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return clickable button in case input is filled', async done => {
    const Component = mount(<Commit {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Commit').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('fill')).toBe('#7fa1b2')

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return spinner once button is clicked and spinner should appear', async done => {
    const config = {
      ...initialState,
      isFetchActive: true
    }

    const Component = mount(<Commit {...config} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Commit').length).toBe(1)
    expect(Component.find('button').length).toBe(0)
    expect(Component.find('span').length).toBe(1)

    expect(Component).toMatchSnapshot()

    done()
  })
})
