import context from './context'

const initialState = {
  isShowButton: true,
  context
}

export default initialState
