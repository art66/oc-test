import context from './context'

const initialState = {
  context,
  inputValue: 'test',
  isFetchActive: false
}

export default initialState
