import context from './context'

const initialState = {
  inputValue: 'hello!',
  context
}

export default initialState
