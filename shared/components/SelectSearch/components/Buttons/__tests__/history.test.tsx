import React from 'react'
import { mount } from 'enzyme'

import History from '../History'
import initialState from './mocks/history'

describe('<History />', () => {
  it('should return basic button if input clicked and dropdown active', async done => {
    const Component = mount(<History {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('History').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('fill')).toBe('#d4dfe5')

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return empty button in case input is not clicked', async done => {
    const config = {
      ...initialState,
      isShowButton: false
    }

    const Component = mount(<History {...config} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('History').length).toBe(1)
    expect(Component.find('button').length).toBe(0)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should return clicked button once isHistoryAppear: true && lastsSearches.length > 0', async done => {
    const config = {
      ...initialState,
      isHistoryAppear: true,
      lastsSearches: [{ name: 'test' }]
    }

    const Component = mount(<History {...config} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('History').length).toBe(1)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('fill')).toBe('#7fa1b2')

    expect(Component).toMatchSnapshot()

    done()
  })
})
