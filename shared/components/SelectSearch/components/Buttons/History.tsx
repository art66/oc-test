import React from 'react'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import keyDownHandler, { KEY_TYPES } from '@torn/shared/utils/keyDownHandler'

import NestedContext from '../../context'
import iconStyler from '../../utils/iconStyler'
import getContext from '../../helpers/getContext'

import { IProps, IHistoryButtonConfiguration } from './interfaces/IHistory'
import { IContext } from '../../interfaces/IContext'

import styles from './index.cssmodule.scss'
import globalStyles from '../../styles/common.cssmodule.scss'

const DEFAULT_TOOLTIP = 'History'

class History extends React.PureComponent<IProps> {
  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  // shouldComponentUpdate(prevProps: IProps) {
  //   const { lastsSearches, isShowButton, isHistoryAppear } = this.props

  //   return prevProps.isHistoryAppear !== isHistoryAppear || prevProps.isShowButton !== isShowButton || prevProps.lastsSearches !== lastsSearches
  // }

  _initTooltips = () => {
    const { context } = this.props

    const {
      systemData: { activateTooltips: isActiveTooltips },
      buttons: { activateTooltips }
    } = context
    const { tooltipText } = this._getContext()

    if (!isActiveTooltips && !activateTooltips) {
      return []
    }

    return [{
      child: tooltipText || DEFAULT_TOOLTIP,
      ID: DEFAULT_TOOLTIP.toLowerCase()
    }]
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'update' })

  _isTooltipAllowed = () => {
    const { context } = this.props
    const {
      systemData: { activateTooltips: isActiveTooltips },
      buttons: { activateTooltips }
    } = context

    return isActiveTooltips || activateTooltips
  }

  _getContext = () => {
    const { context } = this.props
    const contextData = getContext({ context, name: 'buttons', type: 'historyButton' })

    return contextData as IHistoryButtonConfiguration || {}
  }

  _classNameHolder = () => {
    const { customStyles } = this._getContext()
    const { buttonClass, buttonDisabled } = customStyles || {}

    const buttonClasses = classnames({
      [styles.historyButton]: true,
      [globalStyles.buttonDisabled]: !this._isButtonAllowed(),
      [buttonClass]: buttonClass,
      [buttonDisabled]: buttonDisabled && !this._isButtonAllowed()
    })

    return {
      buttonClasses
    }
  }

  _handleClick = (event: React.KeyboardEvent<HTMLButtonElement> & React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()

    const { onClick } = this.props

    onClick()
  }

  _handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  }

  _handleKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: this._handleClick
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleClick
        }
      ]
    })
  }

  _isButtonAllowed = () => {
    const { lastsSearches } = this.props

    return lastsSearches && lastsSearches.length > 0
  }

  _getIconStyles = () => {
    const { isHistoryAppear, context } = this.props
    const { buttons, systemData } = context
    const { stylePreset } = systemData
    const { commonIconsProps = {}, commonDisabledColor, commonActiveColor } = buttons
    const { iconDisabledColor, iconActiveColor, iconProps = {} } = this._getContext()

    const iconPostprocessingStyles = iconStyler({
      preset: stylePreset,
      isHover: this._isButtonAllowed(),
      isStroke: true,
      isDynamicColor: this._isButtonAllowed() && isHistoryAppear,
      commonActiveColor,
      commonDisabledColor,
      commonIconsProps,
      iconActiveColor,
      iconDisabledColor,
      iconProps
    })

    return iconPostprocessingStyles
  }

  _renderHistoryButton = () => {
    const { context } = this.props
    const { buttons: { commonIconsProps = {} } } = context as IContext
    const { iconName, iconProps = {} } = this._getContext()

    const { buttonClasses } = this._classNameHolder()

    return (
      <button
        id={this._isTooltipAllowed() && DEFAULT_TOOLTIP.toLowerCase() || ''}
        type='button'
        className={buttonClasses}
        onClick={this._handleClick}
        onMouseDown={this._handleMouseDown}
        onKeyDown={this._handleKeyDown}
      >
        <SVGIconGenerator
          iconName={iconName || 'History'}
          dimensions={{ width: 14, height: 14, viewbox: '-2 -3 28 28' }}
          {...commonIconsProps}
          {...iconProps}
          {...this._getIconStyles()}
        />
      </button>
    )
  }

  render() {
    const { isDisabled } = this._getContext()
    const { isShowButton } = this.props

    if (!isShowButton || isDisabled) {
      return null
    }

    return this._renderHistoryButton()
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <History context={context} {...props} />}
  </NestedContext.Consumer>
)
