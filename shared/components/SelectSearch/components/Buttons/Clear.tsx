import React from 'react'
import classnames from 'classnames'

import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { SVGIconGenerator } from '@torn/shared/SVG'
import keyDownHandler, { KEY_TYPES } from '@torn/shared/utils/keyDownHandler'

import NestedContext from '../../context'
import getContext from '../../helpers/getContext'
import iconStyler from '../../utils/iconStyler'

import { CLOSE_DIMENSIONS } from '../../constants'
import { IProps, IClearButtonConfiguration } from './interfaces/IClear'
import styles from './index.cssmodule.scss'

const DEFAULT_TOOLTIP = 'Clear'

class Clear extends React.PureComponent<IProps> {
  // static contextType = NestedContext

  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _initTooltips = () => {
    const { tooltipText } = this._getContext()

    if (!this._isTooltipAllowed()) {
      return []
    }

    return [{
      child: tooltipText || DEFAULT_TOOLTIP,
      ID: DEFAULT_TOOLTIP.toLowerCase()
    }]
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'update' })

  _isTooltipAllowed = () => {
    const { context } = this.props
    const {
      systemData: { activateTooltips: isActiveTooltips },
      buttons: { activateTooltips }
    } = context

    return isActiveTooltips || activateTooltips
  }

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'buttons', type: 'clearButton' })

    return contextData as IClearButtonConfiguration || {}
  }

  _classNamesHolder = () => {
    const { customStyles } = this._getContext()
    const { buttonClass } = customStyles || {}

    const buttonClasses = classnames({
      [styles.clearButton]: true,
      [buttonClass]: buttonClass
    })

    return {
      buttonClasses
    }
  }

  _handleClick = (event: React.MouseEvent<HTMLButtonElement> & React.KeyboardEvent<HTMLButtonElement>) => {
    event.preventDefault()

    const { callback } = this._getContext()
    const { onClick } = this.props

    callback && callback(event)
    onClick(event)
  }

  _handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  }

  _handleKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: this._handleClick
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleClick
        }
      ]
    })
  }

  _getIconStyles = () => {
    const { context } = this.props
    const { buttons, systemData } = context
    const { stylePreset } = systemData
    const { commonIconsProps = {}, commonDisabledColor, commonActiveColor } = buttons
    const { iconDisabledColor, iconActiveColor, iconProps = {} } = this._getContext()

    const iconPostprocessingStyles = iconStyler({
      preset: stylePreset,
      isHover: true,
      isStroke: true,
      isDynamicColor: false,
      commonActiveColor,
      commonDisabledColor,
      commonIconsProps,
      iconActiveColor,
      iconDisabledColor,
      iconProps
    })

    return iconPostprocessingStyles
  }

  _renderClearSearchButton = () => {
    const { inputValue, context } = this.props
    const { buttons: { commonIconsProps = {} } } = context
    const { iconName, iconProps = {} } = this._getContext()

    if (!inputValue) {
      return null
    }

    const { buttonClasses } = this._classNamesHolder()

    return (
      <button
        id={this._isTooltipAllowed() && DEFAULT_TOOLTIP.toLowerCase() || ''}
        type='button'
        className={buttonClasses}
        onMouseDown={this._handleMouseDown}
        onKeyDown={this._handleKeyDown}
        onClick={this._handleClick}
      >
        <SVGIconGenerator
          iconName={iconName || 'Close'}
          dimensions={CLOSE_DIMENSIONS}
          {...commonIconsProps}
          {...iconProps}
          {...this._getIconStyles()}
        />
      </button>
    )
  }

  render() {
    const { isDisabled } = this._getContext()

    if (isDisabled) {
      return null
    }

    return this._renderClearSearchButton()
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <Clear context={context} {...props} />}
  </NestedContext.Consumer>
)
