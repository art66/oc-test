import { IProps as ITooltipProps } from '@torn/shared/components/TooltipsNew/types/index'

import { ICommitButtonConfiguration } from './ICommit'
import { IClearButtonConfiguration } from './IClear'
import { IHistoryButtonConfiguration } from './IHistory'

export interface IButtonsConfiguration {
  isDisabled?: boolean
  activateTooltips?: boolean
  commonIconsProps?: ITooltipProps
  commonActiveColor?: string
  commonDisabledColor?: string
  commitButton?: ICommitButtonConfiguration
  historyButton?: IHistoryButtonConfiguration
  clearButton?: IClearButtonConfiguration
}
