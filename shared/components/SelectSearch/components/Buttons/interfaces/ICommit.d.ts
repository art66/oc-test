import { IButtonConfiguration } from './IButton'

export interface ICommitButtonConfiguration extends IButtonConfiguration {
  customStyles?: {
    buttonClass?: string
    buttonDisabled?: string
    animation?: string
  }
}

export interface IProps {
  inputValue: string
  isFetchActive: boolean
  onClick: (event: React.KeyboardEvent<HTMLElement> | React.MouseEvent<HTMLElement>) => void
  context?: any // temporary!
}
