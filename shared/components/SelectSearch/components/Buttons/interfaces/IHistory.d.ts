import { IButtonConfiguration } from './IButton'
import { ILasts } from '../../Lasts/interfaces'

export interface IHistoryButtonConfiguration extends IButtonConfiguration {
  customStyles?: {
    buttonClass?: string
    buttonDisabled?: string
  }
}

export interface IProps {
  isShowButton: boolean
  isHistoryAppear: boolean
  lastsSearches: ILasts[]
  onClick: () => void
  context?: any // temporary!
}
