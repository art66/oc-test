import { IProps as ITooltipProps } from '@torn/shared/components/TooltipsNew/types/index'

export interface IButtonConfiguration {
  isDisabled?: boolean
  callback?: (payload: object) => void
  iconName?: string
  iconDisabledColor?: string
  iconActiveColor?: string
  iconProps?: ITooltipProps
  tooltipText?: string
}
