import { IButtonConfiguration } from './IButton'

export interface IClearButtonConfiguration extends IButtonConfiguration {
  customStyles?: {
    buttonClass?: string
  }
}

export interface IProps {
  inputValue: string
  onClick: (event: React.KeyboardEvent<HTMLElement> | React.MouseEvent<HTMLElement>) => void
  context?: any // temporary!
}
