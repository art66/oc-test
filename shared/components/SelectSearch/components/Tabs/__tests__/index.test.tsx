import React from 'react'
import { mount } from 'enzyme'

import Tabs from '..'
import initialState, { customTabs, noTabs } from './mocks'

describe('<Tabs />', () => {
  it('should render basic tabs layout with active All tab', () => {
    const Component = mount(<Tabs {...initialState} />)

    expect(Component.find('Tabs').length).toBe(1)
    expect(Component.find('.tabsList').length).toBe(1)
    expect(Component.find('.tabButton').length).toBe(4)
    expect(Component.find('.tabButton.tabButtonClicked').length).toBe(1)
    expect(Component.find('.tabButton').at(3).text()).toBe('All')
    expect(Component.find('.tabButton').at(2).text()).toBe('Company')
    expect(Component.find('.tabButton').at(1).text()).toBe('Faction')
    expect(Component.find('.tabButton').at(0).text()).toBe('Friends')

    expect(Component).toMatchSnapshot()
  })
  it('should render only Criteria and Parents tabs', () => {
    const Component = mount(<Tabs {...customTabs} />)

    expect(Component.find('Tabs').length).toBe(1)
    expect(Component.find('.tabsList').length).toBe(1)
    expect(Component.find('.tabButton').length).toBe(2)
    expect(Component.find('.tabButton.tabButtonClicked').length).toBe(1)
    expect(Component.find('.tabButton').at(1).text()).toBe('Parents')
    expect(Component.find('.tabButton').at(0).text()).toBe('Criteria')

    expect(Component).toMatchSnapshot()
  })
  it('should render no tabs', () => {
    const Component = mount(<Tabs {...noTabs} />)

    expect(Component.find('Tabs').length).toBe(1)
    expect(Component.find('.tabsList').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render tabs and change active tab by click', () => {
    const Component = mount(<Tabs {...initialState} />)

    expect(Component.find('Tabs').length).toBe(1)
    expect(Component.find('.tabsList').length).toBe(1)
    expect(Component.find('.tabButton').at(3).prop('className')).toBe('tabButton tabButtonClicked')

    Component.setProps({
      currentTab: 'faction'
    })

    expect(Component.find('.tabButton').at(1).prop('className')).toBe('tabButton tabButtonClicked')

    expect(Component).toMatchSnapshot()
  })
})
