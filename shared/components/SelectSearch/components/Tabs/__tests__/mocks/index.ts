import context from './context'

const initialState = {
  currentTab: 'all',
  onChooseTab: (clickedTab: string) => clickedTab,
  context
}

const customTabs = {
  ...initialState,
  currentTab: 'criteria',
  context: {
    ...initialState.context,
    tabs: {
      ...initialState.context.tabs,
      categories: ['criteria', 'parents']
    }
  }
}

const noTabs = {
  ...initialState,
  context: {
    ...initialState.context,
    tabs: {
      ...initialState.context.tabs,
      categories: []
    }
  }
}

export default initialState
export { customTabs, noTabs }
