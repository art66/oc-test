import React from 'react'
import classnames from 'classnames'
import upperLeadingLetter from '@torn/shared/utils/firstLetterUpper'
import keyDownHandler, { KEY_TYPES, TKeyTypes } from '@torn/shared/utils/keyDownHandler'
import isEmpty from '@torn/shared/utils/isEmpty'

import styles from './index.cssmodule.scss'
import { IProps, ITabsConfig } from './interfaces'
import { TCurrentTab } from '../../interfaces/ICommon'
import NestedContext from '../../context'
import getContext from '../../helpers/getContext'

class Tabs extends React.PureComponent<IProps> {
  // static contextType = NestedContext

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'tabs' })

    return contextData as ITabsConfig || {}
  }

  _tabButtonClassNameHolder = (tabID?: TCurrentTab) => {
    const { customStyles } = this._getContext()
    const { tabsWrap, tabButton, tabButtonClicked } = customStyles || {}

    const { currentTab } = this.props

    const wrapClass = classnames({
      [styles.tabsList]: true,
      [tabsWrap]: tabsWrap
    })

    const buttonClass = classnames({
      [styles.tabButton]: true,
      [styles.tabButtonClicked]: currentTab === tabID,
      [tabButton]: tabButton,
      [tabButtonClicked]: tabButtonClicked && currentTab === tabID
    })

    return {
      wrapClass,
      buttonClass
    }
  }

  _handleTabClick = (event: React.MouseEvent<HTMLButtonElement> & React.KeyboardEvent<HTMLButtonElement>) => {
    if (this._isCurrentTab(event)) {
      return
    }

    const { callback } = this._getContext()
    const { onChooseTab } = this.props
    const clickedTab = this._getClickedTab(event)

    callback && callback(clickedTab)
    onChooseTab(clickedTab)
  }

  _handleKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: this._handleTabClick,
          isDisabled: this._isCurrentTab(event)
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleTabClick,
          isDisabled: this._isCurrentTab(event)
        },
        {
          type: KEY_TYPES.up,
          onEvent: this._moveToSearch
        },
        {
          type: KEY_TYPES.left,
          onEvent: this._navigateTabs(KEY_TYPES.left)
        },
        {
          type: KEY_TYPES.right,
          onEvent: this._navigateTabs(KEY_TYPES.right)
        },
        {
          type: KEY_TYPES.down,
          onEvent: this._moveToItemsList
        }
      ]
    })
  }

  _moveToSearch = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    const tabsContainer = event.target && (event.target as HTMLElement).parentNode
    const bottomContainer = tabsContainer && tabsContainer.parentNode
    const topContainer = bottomContainer && bottomContainer.previousSibling
    const inputNode = topContainer && (topContainer as HTMLElement).querySelector('input')

    if (!inputNode) {
      return
    }

    inputNode.focus()
  }

  _navigateTabs = (type: TKeyTypes) => (event: React.KeyboardEvent<HTMLButtonElement>) => {
    event.preventDefault()

    const { previousSibling, nextSibling } = event.target as HTMLButtonElement
    const customEvent = (target: ChildNode) => ({ ...event, target: target })

    if (previousSibling && type === KEY_TYPES.left) {
      this._handleTabClick(customEvent(previousSibling) as any);

      (previousSibling as HTMLButtonElement).focus()
    } else if (nextSibling && type === KEY_TYPES.right) {
      this._handleTabClick(customEvent(nextSibling) as any);

      (nextSibling as HTMLButtonElement).focus()
    }
  }

  _moveToItemsList = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    const tabsContainer = event.target && (event.target as HTMLButtonElement).parentNode
    const itemsContainer = tabsContainer && tabsContainer.nextSibling
    const firstItemButton = itemsContainer && itemsContainer.firstChild as HTMLButtonElement

    if (!firstItemButton || !this._isCurrentTab(event)) {
      return
    }

    firstItemButton.focus()
    event.preventDefault()
  }

  // _saveTabDecorator = (event: React.KeyboardEvent<HTMLButtonElement>) => {
  //   const { onChooseTab, configuration: { callback } } = this.props

  //   const clickedTab = this._getClickedTab(event)

  //   callback && callback(clickedTab)
  //   onChooseTab(clickedTab)
  // }

  _getClickedTab = (event: React.MouseEvent<HTMLButtonElement> | React.KeyboardEvent<HTMLButtonElement>) => {
    const {
      dataset: { id: clickedTab }
    } = event.target as HTMLElement

    return clickedTab
  }

  _isCurrentTab = (event: React.MouseEvent<HTMLButtonElement> | React.KeyboardEvent<HTMLButtonElement>) => {
    const { currentTab } = this.props

    return currentTab === this._getClickedTab(event)
  }

  _renderTabSections = () => {
    const { categories } = this._getContext()

    return categories.map(tabName => {
      const { buttonClass } = this._tabButtonClassNameHolder(tabName)
      const tabLabel = upperLeadingLetter({ value: tabName })

      return (
        <button
          type='button'
          key={tabName}
          className={buttonClass}
          data-id={tabName}
          onKeyDown={this._handleKeyDown}
          onClick={this._handleTabClick}
        >
          {tabLabel}
        </button>
      )
    })
  }

  render() {
    const { isDisabled, categories } = this._getContext()

    if (isEmpty(categories) || isDisabled) {
      return null
    }

    const { wrapClass } = this._tabButtonClassNameHolder()

    return (
      <div className={wrapClass}>{this._renderTabSections()}</div>
    )
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <Tabs context={context} {...props} />}
  </NestedContext.Consumer>
)
