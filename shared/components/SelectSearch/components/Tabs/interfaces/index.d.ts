import { TCurrentTab, TTabs } from '../../../interfaces/ICommon'

export interface ITabsConfig {
  isDisabled?: boolean
  activeTab?: TCurrentTab
  categories?: TTabs
  callback?: (clickedTab: string) => void
  customStyles?: {
    tabsWrap: string
    tabButton: string
    tabButtonClicked: string
  }
}

export interface IProps {
  currentTab: TCurrentTab
  onChooseTab: (clickedTab: string) => void
  context?: any // temporary!
}
