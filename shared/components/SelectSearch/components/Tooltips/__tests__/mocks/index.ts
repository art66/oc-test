import context from './context'

const initialState = {
  isBadSymbol: false,
  isMaxLength: false,
  isFetchError: false,
  context
}

const activeTooltipBad = {
  ...initialState,
  isBadSymbol: true
}

export default initialState
export { activeTooltipBad }
