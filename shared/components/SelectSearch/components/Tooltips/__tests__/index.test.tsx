import React from 'react'
import { mount } from 'enzyme'

import Tooltips from '..'
import initialState, { activeTooltipBad } from './mocks'

describe('<Tabs />', () => {
  it('should render basic Tooltips layout without tooltip appear', () => {
    const Component = mount(<Tooltips {...initialState} />)

    expect(Component.find('Tooltips').length).toBe(1)
    expect(Component.find('TransitionGroup').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render basic Tooltips with bad symbol text appeared', () => {
    const Component = mount(<Tooltips {...activeTooltipBad} />)

    expect(Component.find('Tooltips').length).toBe(1)
    expect(Component.find('TransitionGroup').length).toBe(1)
    expect(Component.find('CSSTransition').length).toBe(1)
    expect(Component.find('.warningTooltip').length).toBe(1)
    expect(Component.find('.warningTooltip').text()).toBe('Only letters, numbers, dashes, underscores allowed.')

    expect(Component).toMatchSnapshot()
  })
})
