import React from 'react'
import classnames from 'classnames'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import { IProps, IText, ITooltipCustomConfig } from './interfaces'
import styles from './index.cssmodule.scss'
import './anim.cssmodule.scss'
import isValue from '../../../../utils/isValue'
import NestedContext from '../../context'
import getContext from '../../helpers/getContext'
import { MAX_INPUT_LENGTH } from '../../constants'

const ALERTS = {
  default: () => '',
  badSymbol: () => 'Only letters, numbers, dashes, underscores allowed.',
  maxLength: ({ value }: IText) => `Yoo can't enter more than ${value || 0} characters.`,
  fetchError: () => 'Server response error. Try again later.'
}

const ANIM_PROPS = {
  classNameDefault: 'selectSearchTooltip',
  timeInDefault: 1000,
  timeExitDefault: 300
}

class Tooltips extends React.PureComponent<IProps> {
  // static contextType = NestedContext

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'tooltips' })

    return contextData as ITooltipCustomConfig || {}
  }

  _classNamesHolder = () => {
    const { customStyles } = this._getContext()
    const { tooltipWrap, animClass } = customStyles || {}
    const { classNameDefault } = ANIM_PROPS

    const animClasses = classnames({
      [classNameDefault]: true,
      [animClass]: animClass
    })

    const wrapClasses = classnames({
      [styles.warningTooltip]: true,
      [tooltipWrap]: tooltipWrap
    })

    return {
      animClasses,
      wrapClasses
    }
  }

  _isTooltipNotAllowed = () => {
    const { isDisabled } = this._getContext()
    const { isBadSymbol, isMaxLength, isFetchError } = this.props

    return isDisabled || !isBadSymbol && !isMaxLength && !isFetchError
  }

  _tooltipTexts = () => {
    const { context: { input }, isFetchError, isBadSymbol, isMaxLength } = this.props
    const { inputChecker } = input || {}

    const { texts } = this._getContext()
    const { badText, maxText } = texts || {}

    let text = null

    if (isBadSymbol) {
      text = badText || ALERTS.badSymbol
    } else if (isMaxLength) {
      text = maxText || ALERTS.maxLength
    } else if (isFetchError) {
      text = ALERTS.fetchError
    }

    return typeof text === 'string' ? text : text({ value: isMaxLength ? inputChecker && inputChecker.maxLength || MAX_INPUT_LENGTH : '' })
  }

  _getAnimKey = () => {
    const { isBadSymbol, isMaxLength, isFetchError } = this.props

    return isBadSymbol && 1 || isMaxLength && 2 || isFetchError && 3
  }

  _inputTooltip = () => {
    const { animation } = this._getContext()

    if (this._isTooltipNotAllowed()) {
      return null
    }

    const { timeIn, timeOut } = animation || {}
    const { animClasses, wrapClasses } = this._classNamesHolder()
    const { timeExitDefault, timeInDefault } = ANIM_PROPS

    const animTime = {
      enter: isValue(timeIn) && timeIn || timeInDefault,
      exit: isValue(timeOut) && timeOut || timeExitDefault
    }

    return (
      <CSSTransition
        in={!!this._getAnimKey()}
        appear={true}
        key={this._getAnimKey()}
        classNames={animClasses}
        timeout={animTime}
        unmountOnExit={true}
      >
        <div className={wrapClasses}>
          {this._tooltipTexts()}
        </div>
      </CSSTransition>
    )
  }

  render() {
    return (
      <TransitionGroup className={styles.tooltipsBlock}>
        {this._inputTooltip()}
      </TransitionGroup>
    )
  }
}


export default props => (
  <NestedContext.Consumer>
    {context => <Tooltips context={context} {...props} />}
  </NestedContext.Consumer>
)
