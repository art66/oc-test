import { IRestrictFlags } from '../../Input/interfaces'

export interface IProps extends IRestrictFlags {
  context?: any // temporary!
}

export interface IText {
  value?: string
}

export interface ITooltipCustomConfig {
  isDisabled?: boolean
  texts?: {
    badText?: string
    maxText?: string
  }
  animation?: {
    timeIn?: number
    timeOut?: number
  }
  customStyles?: {
    tooltipWrap?: string
    animClass?: string
  }
}
