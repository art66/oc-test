import React from 'react'
import { mount } from 'enzyme'

import User from '../Users/User'
import initialStore, { selectedUser, disabledStatus, disabledName, disabledID } from './mocks/user'

describe('<User />', () => {
  it('should render with default props', () => {
    const Component = mount(<User {...initialStore} />)

    expect(Component.find('button').length).toBe(1)
    expect(Component.find('i.onlineStatus').length).toBe(1)
    expect(Component.find('span.userName').length).toBe(1)
    expect(Component.find('span.userID').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render clicked user', () => {
    const Component = mount(<User {...selectedUser} />)

    expect(Component.find('button.alreadySelected').length).toBe(1)
    expect(Component.find('i.onlineStatus').length).toBe(1)
    expect(Component.find('span.userName').length).toBe(1)
    expect(Component.find('span.userID').length).toBe(1)
    expect(Component.find('span').at(2).text()).toBe('12')

    expect(Component).toMatchSnapshot()
  })
  it('should render without status', () => {
    const Component = mount(<User {...disabledStatus} />)

    expect(Component.find('button').length).toBe(1)
    expect(Component.find('i.onlineStatus').length).toBe(0)
    expect(Component.find('span.userName').length).toBe(1)
    expect(Component.find('span.userID').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render without name', () => {
    const Component = mount(<User {...disabledName} />)

    expect(Component.find('button').length).toBe(1)
    expect(Component.find('i.onlineStatus').length).toBe(1)
    expect(Component.find('span.userName').length).toBe(0)
    expect(Component.find('span.userID').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render without ID', () => {
    const Component = mount(<User {...disabledID} />)

    expect(Component.find('button').length).toBe(1)
    expect(Component.find('i.onlineStatus').length).toBe(1)
    expect(Component.find('span.userName').length).toBe(1)
    expect(Component.find('span.userID').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
