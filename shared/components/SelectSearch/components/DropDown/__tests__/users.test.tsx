import React from 'react'
import { mount } from 'enzyme'

import Users from '../Users'
import initialStore, { disabled } from './mocks/users'

describe('<Users />', () => {
  it('should render basic list', () => {
    const Component = mount(<Users {...initialStore} />)

    expect(Component.find('button').length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
  it('should don\'t render whole list', () => {
    const Component = mount(<Users {...disabled} />)

    expect(Component.find('button').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
