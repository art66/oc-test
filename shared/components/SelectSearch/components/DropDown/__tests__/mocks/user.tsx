import { enhancers } from './users'

const initialStore = {
  ID: 12,
  name: 'test',
  online: 'online',
  isSelected: false,
  customConfig: {
    status: {
      isDisabled: false,
      isSVGLayout: false,
      SVGStatusData: {}
    },
    ID: {
      isDisabled: false
    },
    name: {
      isDisabled: false
    }
  },
  customStyles: {},
  ...enhancers
}

const selectedUser = {
  ...initialStore,
  isSelected: true
}

const disabledStatus = {
  ...initialStore,
  customConfig: {
    ...initialStore.customConfig,
    status: {
      ...initialStore.customConfig.status,
      isDisabled: true
    }
  }
}

const disabledName = {
  ...initialStore,
  customConfig: {
    ...initialStore.customConfig,
    name: {
      ...initialStore.customConfig.name,
      isDisabled: true
    }
  }
}

const disabledID = {
  ...initialStore,
  customConfig: {
    ...initialStore.customConfig,
    ID: {
      ...initialStore.customConfig.ID,
      isDisabled: true
    }
  }
}

export default initialStore

export {
  disabledStatus,
  selectedUser,
  disabledID,
  disabledName
}
