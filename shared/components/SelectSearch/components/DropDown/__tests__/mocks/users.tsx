import React from 'react'

const enhancers = {
  onKeyDown: () => {},
  onClick: () => {},
  alreadySelectedBlock: (ID: number) => <span>{ID}</span>
}

const initialState = {
  itemsList: [
    {
      ID: 12,
      name: 'test',
      category: 'test_cat',
      online: 'online'
    },
    {
      ID: 13,
      name: 'test_2',
      category: 'test_cat_2',
      online: 'online'
    }
  ],
  customConfig: {},
  customStyles: {},
  isSelected: (ID: number) => !!ID,
  ...enhancers
}

const disabled = {
  ...initialState,
  itemsList: []
}

export default initialState
export { enhancers, disabled }
