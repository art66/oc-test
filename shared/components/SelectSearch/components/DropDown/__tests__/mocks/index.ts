import context from './context'

const initialStore = {
  type: 'Users',
  inviteItemsList: [
    {
      ID: 1
    },
    {
      ID: 2
    },
    {
      ID: 3
    },
    {
      ID: 4
    },
    {
      ID: 5
    },
    {
      ID: 6
    },
    {
      ID: 7
    },
    {
      ID: 8
    },
    {
      ID: 9
    },
    {
      ID: 10
    },
    {
      ID: 11
    },
    {
      ID: 12
    }],
  membersList: [],
  saveMember: (member: object) => member,
  isAllItemsTab: false,
  isActiveSearch: false,
  isInitLoad: false,
  isForceSearch: false,
  isLiveSearch: false,
  isManualSearch: false,
  context
}

const noScroll = {
  ...initialStore,
  inviteItemsList: [{ ID: 1 }]
}

const placeholder = {
  ...initialStore,
  isInitLoad: true
}

export default initialStore
export { placeholder, noScroll }
