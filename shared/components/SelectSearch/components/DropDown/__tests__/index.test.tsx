import React from 'react'
import { mount } from 'enzyme'

import DropDown from '..'
import initialStore, { noScroll, placeholder } from './mocks'

describe('<DropDown />', () => {
  it('should render with default props', () => {
    const Component = mount(<DropDown {...initialStore} />)

    expect(Component.find('DropDownList').length).toBe(1)
    expect(Component.find('div').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render without scroll bar if items less than capacity block', () => {
    const Component = mount(<DropDown {...noScroll} />)

    expect(Component.find('DropDownList').length).toBe(1)
    expect(Component.find('div.noScroll').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render placeholder', () => {
    const Component = mount(<DropDown {...placeholder} />)

    expect(Component.find('DropDownList').length).toBe(1)
    expect(Component.find('Placeholder').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
