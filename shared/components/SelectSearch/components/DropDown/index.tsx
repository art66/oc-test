import React from 'react'
import classnames from 'classnames'

import keyDownHandler, { KEY_TYPES, TKeyTypes } from '@torn/shared/utils/keyDownHandler'

import placeholderRequired from '../../utils/placeholderRequired'
import dropDownLists from './utils/dropDownLists'
import getTargetData from '../../utils/getTargetData'
import alreadySelectedItem from '../../utils/alreadySelectedItem'

import { IProps, IState, IDefaultProps, IDropDownConfig } from './interfaces'
import { TPlaceholderTypes } from '../Placeholder/interfaces'
import { MINIMAL_CONTAINER_CAPACITY } from '../../constants'

import Placeholder from '../Placeholder'
import styles from './styles/index.cssmodule.scss'
import globalStyles from '../../styles/common.cssmodule.scss'
import NestedContext from '../../context'
import getContext from '../../helpers/getContext'

class DropDownList extends React.Component<IProps, IState> {
  // static contextType = NestedContext

  static defaultProps: IDefaultProps = {
    type: 'Users'
  }

  shouldComponentUpdate(prevProps: IProps) {
    const {
      isAllItemsTab,
      isActiveSearch,
      isInitLoad,
      isForceSearch,
      isLiveSearch,
      isManualSearch,
      membersList,
      inviteItemsList,
      context
    } = this.props

    const isFlagsChanged = prevProps.isAllItemsTab !== isAllItemsTab || prevProps.isActiveSearch !== isActiveSearch || prevProps.isInitLoad! !== isInitLoad || prevProps.isForceSearch !== isForceSearch || prevProps.isLiveSearch !== isLiveSearch || prevProps.isManualSearch !== isManualSearch
    const isListChanged = JSON.stringify(prevProps.inviteItemsList) !== JSON.stringify(inviteItemsList)
    const isMembersChanged = JSON.stringify(prevProps.membersList) !== JSON.stringify(membersList)
    const isContextChanged = JSON.stringify(prevProps.context.dropdown) !== JSON.stringify(context.dropdown)

    return isFlagsChanged || isListChanged || isMembersChanged || isContextChanged
  }

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'dropdown' })

    return contextData as IDropDownConfig || {}
  }

  _classNameHolder = () => {
    const { inviteItemsList } = this.props
    const { dropDownWrap, alreadyInLabel } = this._getCurrentStyles()

    const inProgress = this._isSearchInProgress()
    const isLessCapacity = !inviteItemsList || inviteItemsList.length <= MINIMAL_CONTAINER_CAPACITY

    const containerClass = classnames({
      [styles.itemsList]: true,
      [globalStyles.noScroll]: inProgress || isLessCapacity,
      [dropDownWrap]: dropDownWrap
    })

    const alreadyInClass = classnames({
      [styles.alreadyInLabel]: true,
      [alreadyInLabel]: alreadyInLabel
    })

    return {
      containerClass,
      alreadyInClass
    }
  }

  _getType = () => {
    const { type } = this.props

    return type || 'Users'
  }

  _navigateList = (event: React.KeyboardEvent<HTMLButtonElement>, type: TKeyTypes) => {
    const { nextSibling, previousSibling } = event.target as HTMLButtonElement

    const jumpToSearchInput = () => {
      const buttonsListParent = event.target && (event.target as HTMLElement).parentNode
      const bottomContainer = buttonsListParent && buttonsListParent.parentNode
      const topContainer = bottomContainer && bottomContainer.previousSibling

      const searchInput = topContainer && (topContainer as HTMLElement).querySelector('input') as HTMLButtonElement

      if (!searchInput) {
        return
      }

      searchInput.focus()
    }

    const runGoingUp = () => {
      if (previousSibling) {
        (previousSibling as HTMLElement).focus()
        event.preventDefault()

        return
      }

      jumpToSearchInput()
    }

    const runGoingDown = () => {
      if (!nextSibling) {
        return
      }

      (nextSibling as HTMLElement).focus()
      event.preventDefault()
    }

    if (type === KEY_TYPES.up) {
      runGoingUp()
    } else if (type === KEY_TYPES.down) {
      runGoingDown()
    }
  }

  _navigateListDown = (event: React.KeyboardEvent<HTMLButtonElement>) => this._navigateList(event, KEY_TYPES.down)
  _navigateListUp = (event: React.KeyboardEvent<HTMLButtonElement>) => this._navigateList(event, KEY_TYPES.up)

  _handleKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    const { ID } = getTargetData(event.target)
    const isDisabled = this._isAlreadySelected(ID)

    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.space,
          onEvent: this._handleItemClick,
          isDisabled
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleItemClick,
          isDisabled
        },
        {
          type: KEY_TYPES.down,
          onEvent: this._navigateListDown
        },
        {
          type: KEY_TYPES.up,
          onEvent: this._navigateListUp
        }
      ]
    })
  }

  _handleItemClick = (event: React.MouseEvent<HTMLButtonElement> & React.KeyboardEvent<HTMLButtonElement>) => {
    event.preventDefault()
    const { saveMember } = this.props

    saveMember(event.target) // should be removed from once went to production
  }

  _isSearchInProgress = () => {
    const { isInitLoad, isManualSearch, isForceSearch, isLiveSearch } = this.props
    const inProgress = !isManualSearch && (isInitLoad || isLiveSearch || isForceSearch)

    return inProgress
  }

  _isAlreadySelected = (ID: number | string) => {
    const { membersList = [] } = this.props
    const { disabledList } = this._getContext()

    return alreadySelectedItem({ membersList, disabledList, ID })
  }

  _getCurrentStyles = () => {
    const { customStyles = {} } = this._getContext()

    return customStyles[this._getType()] || customStyles.users || {}
  }

  _getCurrentConfig = () => {
    const { customConfig = {} } = this._getContext()

    return customConfig[this._getType()] || customConfig.users || {}
  }

  _isPlaceholderRequired = () => {
    const {
      isActiveSearch,
      isInitLoad,
      isForceSearch,
      isLiveSearch
    } = this.props

    return isActiveSearch || isInitLoad || isForceSearch || isLiveSearch
  }

  _renderPlaceholder = (placeholderType: TPlaceholderTypes) => {
    return (
      <Placeholder
        type={placeholderType}
        inProgress={this._isSearchInProgress()}
      />
    )
  }

  // TODO: should be moved to its own detached component!
  _renderAlreadySelectedBlock = (ID: number) => {
    const { alreadySelected } = this._getContext()
    const { isDisabled, customAlreadySelected, alreadySelectedText = '(already in)' } = alreadySelected || {}

    const isAlreadySelected = this._isAlreadySelected(ID)

    if (!isAlreadySelected || isDisabled) {
      return null
    }

    if (customAlreadySelected) {
      return customAlreadySelected
    }

    const { alreadyInClass } = this._classNameHolder()

    return <span className={alreadyInClass}>{alreadySelectedText}</span>
  }

  _renderList = () => {
    const { isDisabled } = this._getContext()
    const { inviteItemsList } = this.props

    const ListToRender = dropDownLists[this._getType()]

    if (!ListToRender || isDisabled) {
      console.error('No List Wrapper were found! Please, check your configs: ', this._getType(), dropDownLists)

      return null
    }

    return (
      <ListToRender
        customStyles={this._getCurrentStyles()}
        alreadySelectedBlock={this._renderAlreadySelectedBlock}
        itemsList={inviteItemsList}
        isSelected={this._isAlreadySelected}
        onClick={this._handleItemClick}
        onKeyDown={this._handleKeyDown}
        customConfig={this._getCurrentConfig()}
      />
    )
  }

  render() {
    const placeholderType = placeholderRequired(this.props)

    if (placeholderType) {
      return this._renderPlaceholder(placeholderType)
    }

    return (
      <div className={this._classNameHolder().containerClass}>
        {this._renderList()}
      </div>
    )
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <DropDownList context={context} {...props} />}
  </NestedContext.Consumer>
)
