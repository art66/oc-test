import { ICategory, IItemsList, TFetchTypes } from '../../../interfaces/ICommon'
import { IUserCustomStyles, IUserConfig } from './IUsers'

export interface ISetItems {
  isInit?: boolean
  isDisabled?: boolean
  fetchStatus?: TFetchTypes
  itemsList: IItemsList
}

export interface ICoreDropDown {
  isAllItemsTab: boolean
  isActiveSearch: boolean
  isInitLoad: boolean
  isForceSearch: boolean
  isLiveSearch: boolean
  isManualSearch: boolean
  inviteItemsList: ICategory[]
}

export interface IDropDownConfig {
  isDisabled?: boolean
  type?: TLayoutTypes
  hideByClick?: boolean
  disabledList?: number[]
  alreadySelected?: {
    isDisabled?: boolean
    customAlreadySelected?: JSX.Element
    alreadySelectedText?: string
  }
  customConfig?: {
    users?: IUserConfig // TODO: should be added right config if it would be needed to enhance the Users API,
    items?: any// TODO: should be added right config if it would be needed to enhance the Items API,
  }
  customStyles?: {
    users?: IUserCustomStyles
    items?: any // TODO: add them once someone will began work on the items dropdown
  }
}

export interface IMember {
  name: string
  ID: number
}

export type TLayoutTypes = 'Users' | 'Items'

export interface IDefaultProps {
  type?: TLayoutTypes
}

export interface IProps extends ICoreDropDown, IDefaultProps {
  membersList: number[]
  saveMember: (member: EventTarget) => void
  context?: any // temporary!
}

export interface IState {}
