import { ICategory } from '../../../interfaces/ICommon'

export interface IDefaultProps {
  customConfig?: IUserConfig
}

export interface IProps extends IUserCommon {
  itemsList: ICategory[]
  isSelected: (ID: number) => boolean
}

export interface IUserConfig {
  status?: {
    isDisabled?: boolean
    isSVGLayout?: boolean
    SVGStatusData?: object
  }
  ID?: {
    isDisabled?: boolean
  }
  name?: {
    isDisabled?: boolean
    preventFirstListLetterUpper?: boolean
  }
}

export interface IUserCustomStyles {
  dropDownWrap?: string
  onlineStatus?: string
  onlineStatusIdle?: string
  onlineStatusActive?: string
  unitContainer?: string
  unitContainerSelected?: string
  name?: string
  id?: string
  alreadyInLabel?: string
}

export interface IUser extends IUserCommon {
  ID: number
  name: string
  online: string
  isSelected: boolean
}

export interface IUserCommon {
  customConfig: IUserConfig
  customStyles: IUserCustomStyles
  onKeyDown: (event: React.KeyboardEvent<HTMLButtonElement>) => void
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void
  alreadySelectedBlock: (ID: number) => JSX.Element | Element
}

export interface IDefaultPropsUser {
  customConfig?: IUserConfig
  customStyles?: IUserCustomStyles
}
