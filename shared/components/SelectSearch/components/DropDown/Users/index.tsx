import React from 'react'

import User from './User'

import { ICategory } from '../../../interfaces/ICommon'
import { IProps } from '../interfaces/IUsers'

class Users extends React.Component<IProps> {
  _renderUsers = () => {
    const { itemsList, onKeyDown, onClick, isSelected, alreadySelectedBlock, customConfig, customStyles } = this.props

    if (!itemsList || itemsList.length === 0) {
      console.error('Users list were missed!', this.props)

      return null
    }

    return itemsList.map(({ ID, name, online }: ICategory) => {
      return (
        <User
          key={`${ID}_${name}`}
          name={name}
          ID={ID}
          online={online}
          onClick={onClick}
          onKeyDown={onKeyDown}
          isSelected={isSelected(ID)}
          customConfig={customConfig}
          customStyles={customStyles}
          alreadySelectedBlock={alreadySelectedBlock}
        />
      )
    })
  }

  render() {
    return this._renderUsers()
  }
}

export default Users
