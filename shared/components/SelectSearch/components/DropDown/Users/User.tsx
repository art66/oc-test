import React from 'react'
import classnames from 'classnames'
import upperLeadingLetter from '@torn/shared/utils/firstLetterUpper'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { IUser, IDefaultPropsUser } from '../interfaces/IUsers'
import styles from '../styles/users.cssmodule.scss'
import dropDownStyles from '../styles/index.cssmodule.scss'

class Users extends React.PureComponent<IUser> {
  static defaultProps: IDefaultPropsUser = {
    customConfig: {
      name: {
        preventFirstListLetterUpper: false
      }
    },
    customStyles: {}
  }

  _classNameHolder = ({ status, isSelected }: { status?: string; isSelected?: boolean } = {}) => {
    const { customStyles } = this.props
    const {
      name,
      id,
      onlineStatus,
      onlineStatusIdle,
      onlineStatusActive,
      unitContainer,
      unitContainerSelected
    } = customStyles

    const nameClass = classnames({
      [styles.userText]: true,
      [styles.userName]: true,
      [name]: name
    })

    const idClass = classnames({
      [styles.userText]: true,
      [styles.userID]: true,
      [id]: id
    })

    const onlineClass = classnames({
      [styles.onlineStatus]: true,
      [onlineStatus]: onlineStatus,
      [styles.userOnline]: status === 'online',
      [onlineStatusIdle]: onlineStatusIdle && status === 'idle',
      [onlineStatusActive]: onlineStatusActive && status === 'online'
    })

    const containerClass = classnames({
      [styles.userButton]: true,
      [dropDownStyles.alreadySelected]: isSelected,
      [unitContainer]: unitContainer,
      [unitContainerSelected]: unitContainerSelected && isSelected
    })

    return {
      onlineClass,
      nameClass,
      idClass,
      containerClass
    }
  }

  _renderStatus = () => {
    const {
      online,
      customConfig: { status }
    } = this.props
    const { isDisabled, isSVGLayout, SVGStatusData } = status || {}

    if (isDisabled) {
      return null
    }

    if (isSVGLayout && SVGStatusData) {
      return <SVGIconGenerator iconName={upperLeadingLetter({ value: online })} {...SVGStatusData} />
    }

    const { onlineClass } = this._classNameHolder({ status: online })

    return <i className={onlineClass} />
  }

  _renderName = () => {
    const { name } = this.props

    const {
      customConfig: { name: nameConfig }
    } = this.props
    const { isDisabled, preventFirstListLetterUpper } = nameConfig || {}

    if (isDisabled) {
      return null
    }

    const { nameClass } = this._classNameHolder()
    const formattedUserName = preventFirstListLetterUpper ?
      name :
      `${upperLeadingLetter({ value: name, isDisabledMultiUpper: true })}`
    const userName = formattedUserName || ''

    return <span className={nameClass}>{userName}</span>
  }

  _renderID = () => {
    const {
      ID,
      customConfig: { ID: IDConfig }
    } = this.props
    const { isDisabled } = IDConfig || {}

    const { idClass } = this._classNameHolder()

    if (isDisabled) {
      return null
    }

    return <span className={idClass}>{`[${ID}]`}</span>
  }

  _renderUser = () => {
    const { ID, name, online, isSelected, onKeyDown, onClick, alreadySelectedBlock } = this.props

    const { containerClass } = this._classNameHolder({ status: online, isSelected })

    return (
      <button
        key={`${ID}_${name}`}
        type='button'
        className={containerClass}
        data-name={name}
        data-id={ID}
        onClick={onClick}
        onKeyDown={onKeyDown}
      >
        {this._renderStatus()}
        {this._renderName()}
        {this._renderID()}
        {alreadySelectedBlock(ID)}
      </button>
    )
  }

  render() {
    return this._renderUser()
  }
}

export default Users
