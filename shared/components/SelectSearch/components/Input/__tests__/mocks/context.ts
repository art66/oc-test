import buttonsContext from '../../../Buttons/__tests__/mocks/context'
import enhancersContext from '../../../Enhancers/__tests__/mocks/context'

export default {
  systemData: {},
  enhancers: enhancersContext.enhancers,
  buttons: buttonsContext.buttons
}
