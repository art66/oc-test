import context from './context'

const initialState = {
  value: 'test',
  inviteItemsList: [{ ID: 1 }],
  membersList: [],
  lastsSearches: [],
  isFetchActive: false,
  isFetchError: false,
  isBadSymbol: false,
  isMaxLength: false,
  isDropDownActive: true,
  isHistoryAppear: false,
  onClick: (event) => event,
  onClear: (event) => event,
  onSearch: (event) => event,
  onSave: (member) => member,
  onToggle: (newRole) => newRole,
  onChange: (event) => event,
  onDebounce: () => {},
  onHistory: () => {},
  onCheck: (error) => error,
  context
}

const withoutEnhancers = {
  ...initialState,
  context: {
    ...context,
    enhancers: {}
  }
}

const withoutButtonsAndEnhancers = {
  ...initialState,
  context: {
    ...context,
    enhancers: {
      isActive: false
    },
    buttons: {
      ...context.buttons,
      historyButton: {
        isDisabled: true
      },
      clearButton: {
        isDisabled: true
      },
      commitButton: {
        isDisabled: true
      }
    }
  },
  value: '',
  isDropDownActive: false
}

const badSymbolTyped = {
  ...initialState,
  isBadSymbol: true
}

export default initialState
export { withoutButtonsAndEnhancers, withoutEnhancers, badSymbolTyped }
