/* eslint-disable max-statements */
import React, { useState } from 'react'
import { mount } from 'enzyme'

import NestedContext from '../../../context'

import Input from '..'
import initialState, { withoutEnhancers, withoutButtonsAndEnhancers, badSymbolTyped } from './mocks'

describe('<Input />', () => {
  it('should render the whole input layout along with its all descendent', () => {
    const { context, ...props } = initialState

    const Component = mount(
      <NestedContext.Provider value={context}>
        <Input {...props} />
      </NestedContext.Provider>
    )

    expect(Component.find('Input').length).toBe(1)
    expect(Component.find('AddRole').length).toBe(1)
    expect(Component.find('AddSave').length).toBe(1)
    expect(Component.find('History').length).toBe(1)
    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('Commit').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render input layout without enhancers block', () => {
    const { context, ...props } = withoutEnhancers

    const Component = mount(
      <NestedContext.Provider value={context}>
        <Input {...props} />
      </NestedContext.Provider>
    )

    expect(Component.find('Input').length).toBe(1)
    expect(Component.find('AddRole').length).toBe(0)
    expect(Component.find('AddSave').length).toBe(0)
    expect(Component.find('History').length).toBe(1)
    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('Commit').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render input layout without buttons and enhancers blocks', () => {
    const { context, ...props } = withoutButtonsAndEnhancers

    const Component = mount(
      <NestedContext.Provider value={context}>
        <Input {...props} />
      </NestedContext.Provider>
    )

    expect(Component).toMatchSnapshot()

    expect(Component.find('Input').length).toBe(1)
    expect(Component.find('AddRole').length).toBe(0)
    expect(Component.find('AddSave').length).toBe(0)
    expect(Component.find('History').length).toBe(1)
    expect(Component.find('button.historyButton').length).toBe(0)
    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('button.clearButton').length).toBe(0)
    expect(Component.find('Commit').length).toBe(1)
    expect(Component.find('button.buttonCommit').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render input with warning wrapper in case badSymbol were typed', () => {
    const { context, ...props } = badSymbolTyped

    const Component = mount(
      <NestedContext.Provider value={context}>
        <Input {...props} />
      </NestedContext.Provider>
    )

    expect(Component).toMatchSnapshot()

    expect(Component.find('Input').length).toBe(1)
    expect(Component.find('.inputWrap.warningInput').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should prevent input in case bad symbol were typed, alert with warning and then allow input again', () => {
    const onChangeEvent = (value: string) => ({
      preventDefault: () => {},
      target: { value }
    })

    const { context, ...restProps } = initialState

    const Proxy = ({ props }: any) => {
      const [inputValue, onChange] = useState('test')
      const [isBadSymbol, onCheck] = useState(false)

      return (
        <NestedContext.Provider value={context}>
          <Input
            {...props}
            value={inputValue}
            isBadSymbol={isBadSymbol}
            onCheck={onCheck}
            onChange={onChange}
          />
        </NestedContext.Provider>
      )
    }

    const Component = mount(<Proxy props={restProps} />)

    expect(Component).toMatchSnapshot()

    expect(Component.find('input').length).toBe(1)
    expect(Component.find('input').prop('value')).toBe('test')
    expect(Component.find('.inputWrap.warningInput').length).toBe(0)

    Component.find('input').simulate('change', onChangeEvent('theValue'))

    expect(Component.find('input').prop('value')).toBe('theValue')
    expect(Component.find('.inputWrap.warningInput').length).toBe(0)

    Component.find('input').simulate('change', onChangeEvent('theValue=;'))

    expect(Component.find('input').prop('value')).toBe('theValue')
    expect(Component.find('.inputWrap.warningInput').length).toBe(1)

    Component.find('input').simulate('change', onChangeEvent('theValueGood'))

    expect(Component.find('input').prop('value')).toBe('theValueGood')
    expect(Component.find('.inputWrap.warningInput').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
