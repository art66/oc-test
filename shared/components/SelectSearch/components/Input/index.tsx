import React from 'react'
import classnames from 'classnames'

import { DebounceDOM } from '@torn/shared/utils/debounce'
import InputStringChecker from '@torn/shared/utils/stringChecker'
import keyDownHandler, { KEY_TYPES } from '@torn/shared/utils/keyDownHandler'

import Enhancers from '../Enhancers'
import Buttons from '../Buttons'

import { SEARCH_PLACEHOLDER, MAX_INPUT_LENGTH } from '../../constants'
import { IProps, IState, IInputConfiguration } from './interfaces'
import styles from './index.cssmodule.scss'
import NestedContext from '../../context'
import getContext from '../../helpers/getContext'
import isValue from '../../../../utils/isValue'

const DEBOUNCE_DELAY = 300
const CHECK_STRING = {
  maxLength: MAX_INPUT_LENGTH,
  allowedSymbols: '^[a-zA-Z0-9-_]+$',
  removeSpaces: true
}

class Input extends React.Component<IProps, IState> {
  private _refInput: React.RefObject<HTMLInputElement>
  private _debounce: any
  private _inputChecker: any

  // static contextType = NestedContext

  constructor(props: IProps) {
    super(props)

    this.state = {
      isAutocompleteSave: false
    }

    this._refInput = React.createRef()
  }

  componentDidMount() {
    this._initializeDebounceSearch()
    this._runInputChecker()
  }

  shouldComponentUpdate(prevProps: IProps, prevState: IState) {
    const { isAutocompleteSave } = this.state
    const { value, membersList, inviteItemsList, lastsSearches, isFetchActive, isDropDownActive, isHistoryAppear, isFetchError, isBadSymbol, isMaxLength, context } = this.props

    const isValueChanged = prevProps.value !== value
    const isDropDownChanged = prevProps.isDropDownActive !== isDropDownActive
    const isHistoryChanged = prevProps.isHistoryAppear !== isHistoryAppear
    const isFetchChanged = prevProps.isFetchActive !== isFetchActive
    const isLastSearches = JSON.stringify(prevProps.lastsSearches) !== JSON.stringify(lastsSearches)
    const isMembersList = JSON.stringify(prevProps.membersList) !== JSON.stringify(membersList)
    const isInvitedList = JSON.stringify(prevProps.inviteItemsList) !== JSON.stringify(inviteItemsList)
    const isContextChanged = JSON.stringify(prevProps.context.input) !== JSON.stringify(context.input)
    const isAutocompleteChange = prevState.isAutocompleteSave !== isAutocompleteSave
    const isFlagsChanged = prevProps.isBadSymbol !== isBadSymbol || prevProps.isMaxLength !== isMaxLength || prevProps.isFetchError !== isFetchError

    return isValueChanged || isDropDownChanged || isHistoryChanged || isFetchChanged || isFlagsChanged || isLastSearches || isInvitedList || isMembersList || isAutocompleteChange || isContextChanged
  }

  componentWillUnmount() {
    this._stopDebounceSearch()
  }

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'input' })

    return contextData as IInputConfiguration || {}
  }

  _classNameHolder = () => {
    const { readOnly, customStyles } = this._getContext()
    const { isFetchError, isBadSymbol, isMaxLength, isDropDownActive } = this.props
    const { inputWrap, search, activeDropDown, warning, disabled, backdrop } = customStyles || {}

    const containerClass = classnames({
      [styles.inputWrap]: true,
      [styles.warningInput]: isBadSymbol || isMaxLength || isFetchError,
      [styles.activeDropDown]: isDropDownActive,
      [styles.readOnlyInputWrap]: readOnly,
      [inputWrap]: inputWrap,
      [activeDropDown]: activeDropDown && isDropDownActive,
      [warning]: warning && (isBadSymbol || isMaxLength || isFetchError)
    })

    const inputClass = classnames({
      [styles.searchItem]: true,
      [styles.readOnlyInput]: readOnly,
      [search]: search,
      [disabled]: disabled && readOnly
    })

    const backdropClass = classnames({
      [styles.backDropInput]: true,
      [backdrop]: backdrop
    })

    return {
      containerClass,
      inputClass,
      backdropClass
    }
  }

  _initializeDebounceSearch = () => {
    const { debounceDelay } = this._getContext()

    this._debounce = new DebounceDOM()

    this._debounce.run({
      event: 'input',
      target: this._refInput.current,
      callback: this._handleDebounce,
      delay: debounceDelay || DEBOUNCE_DELAY
    })
  }

  _stopDebounceSearch = () => this._debounce.stop()

  _runInputChecker = () => {
    const { inputChecker } = this._getContext()
    const { maxLength, allowedSymbols, removeSpaces } = inputChecker || {}

    const config = {
      maxLength: maxLength || CHECK_STRING.maxLength,
      allowedSymbols: allowedSymbols || CHECK_STRING.allowedSymbols,
      removeSpaces: isValue(removeSpaces) ? removeSpaces : CHECK_STRING.removeSpaces
    }

    this._inputChecker = new InputStringChecker(config)
  }

  _resetFlags = () => {
    this.setState({
      isAutocompleteSave: false
    })
  }

  _handleDebounce = () => {
    const { onDebounce } = this.props

    onDebounce()
  }

  _handleAutocompleteSave = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const { disableBackdrop, callbackChange } = this._getContext()
    const { isAutocompleteSave } = this.state
    const { onChange } = this.props

    if (disableBackdrop) {
      return
    }

    const autocompleteValue = this._getBackdropValue()

    if (!autocompleteValue) {
      return
    }

    !isAutocompleteSave && event.preventDefault() // allow tab navigation once two clicks in the row

    this.setState({
      isAutocompleteSave: true
    })

    callbackChange && callbackChange(autocompleteValue as any) // temporary
    onChange(autocompleteValue)
  }

  _handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { callbackChange } = this._getContext()
    const { onChange, onCheck } = this.props
    const { value: inputValue } = event.target as HTMLInputElement

    const {
      value,
      isSymbolAllowed,
      isSpaceInputted,
      isMaxLengthExceeded
    } = this._inputChecker.checkString({ text: inputValue })

    if (value && !isSymbolAllowed || isSpaceInputted) {
      onCheck({
        errorType: 'isBadSymbol'
      })

      return
    }

    if (isMaxLengthExceeded) {
      onCheck({
        errorType: 'isMaxLength'
      })

      return
    }

    onCheck()
    callbackChange && callbackChange(value)
    onChange(value)
  }

  _handleClick = (event: React.MouseEvent<HTMLInputElement>) => {
    const { callbackClick } = this._getContext()
    const { onClick } = this.props

    callbackClick && callbackClick(event)
    onClick(event)
  }

  _handleBlur = () => {
    const { onCheck } = this.props

    onCheck()
    this._resetFlags()
  }

  _handleSearch = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const { callbackSearch } = this._getContext()
    const { onSearch } = this.props

    callbackSearch && callbackSearch(event)
    onSearch(event)
  }

  _handleClear = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const { callbackCancel } = this._getContext()
    const { onClear, onCheck } = this.props

    onCheck()
    this._resetFlags()
    callbackCancel && callbackCancel(event)
    onClear(event)
  }

  _handleSave = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const { onSave } = this.props
    const firstListButton = this._getFirstListButton(event.target)

    onSave(firstListButton)
  }

  _handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: KEY_TYPES.tab,
          onEvent: this._handleAutocompleteSave
        },
        {
          type: KEY_TYPES.enter,
          onEvent: this._handleSave
        },
        {
          type: KEY_TYPES.esc,
          onEvent: this._handleClear
        },
        {
          type: KEY_TYPES.down,
          onEvent: this._moveToItemsList
        }
      ]
    })
  }

  _enhancerHandlers = () => {
    const { onToggle } = this.props

    return {
      addRole: onToggle,
      addSave: this._handleSave
    }
  }

  _getBackdropValue = () => {
    const { disableBackdrop } = this._getContext()
    const { value, inviteItemsList } = this.props

    if (disableBackdrop || !inviteItemsList || inviteItemsList.length === 0) {
      return null
    }

    const { ID, name } = inviteItemsList[0]
    const isNameBackdrop = this._isBackdropAllowed({ toCheck: name, value })
    const isIDBackdrop = this._isBackdropAllowed({ toCheck: ID.toString(), value })

    if (!value || !name || !isNameBackdrop && !isIDBackdrop) {
      return null
    }

    const backdropValue = isNameBackdrop && name || isIDBackdrop && ID.toString()

    return backdropValue
  }

  _getFirstListButton = (target?: EventTarget) => {
    const inputBlock = target && (target as HTMLElement).parentNode
    const inputContainer = inputBlock && inputBlock.parentNode
    const topContainer = inputContainer && inputContainer.parentNode
    const bottomContainer = topContainer && topContainer.nextSibling

    const listContainer = bottomContainer as HTMLButtonElement

    if (!listContainer) {
      return null
    }

    return listContainer.querySelector('button[class^="itemButton"]') || listContainer.querySelector('button[class^="userButton"]')
  }

  _moveToItemsList = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const firstListButton = this._getFirstListButton(event.target)

    if (!firstListButton) {
      return
    }

    (firstListButton as HTMLButtonElement).focus()
    event.preventDefault()
  }

  _isBackdropAllowed = ({ toCheck, value } :{ toCheck: string; value: string}) => {
    const preProceededValue = `^${value}`
    const match = new RegExp(preProceededValue, 'i')
    const isMatched = match.test(toCheck)

    return isMatched
  }

  _renderClearSearchButton = () => {
    const { value } = this.props

    return <Buttons.Clear onClick={this._handleClear} inputValue={value} />
  }

  _renderHistoryButton = () => {
    const { lastsSearches, onHistory, isDropDownActive, isHistoryAppear } = this.props

    return (
      <Buttons.History
        onClick={onHistory}
        lastsSearches={lastsSearches}
        isShowButton={isDropDownActive}
        isHistoryAppear={isHistoryAppear}
      />
    )
  }

  _renderInputSearchButton = () => {
    const { value, isFetchActive } = this.props

    return (
      <Buttons.Commit
        onClick={this._handleSearch}
        isFetchActive={isFetchActive}
        inputValue={value}
      />
    )
  }

  _renderEnhancers = () => {
    const { context, isDropDownActive, inviteItemsList, membersList } = this.props

    const { enhancers: { list, isActive } } = context

    if (!list || list.length === 0 || !isActive) {
      return null
    }

    const enhancersToRender = list.map(enhancer => {
      const { type } = enhancer

      if (!type) {
        return null
      }

      const Enhancer = Enhancers[type]
      const enhancerHandler = this._enhancerHandlers()[type]

      return (
        <Enhancer
          key={type}
          isDropDown={isDropDownActive}
          membersList={membersList}
          inviteItemsList={inviteItemsList}
          onClick={enhancerHandler}
        />
      )
    })

    return (
      <div className={styles.enhancersBlock}>
        {enhancersToRender}
      </div>
    )
  }

  _renderInputPure = () => {
    const { placeholder, readOnly } = this._getContext()
    const { value } = this.props
    const { inputClass } = this._classNameHolder()

    return (
      <input
        type='text'
        autoComplete='off'
        readOnly={readOnly}
        ref={this._refInput}
        className={inputClass}
        placeholder={placeholder || SEARCH_PLACEHOLDER}
        value={value}
        onClick={this._handleClick}
        onKeyDown={this._handleKeyDown}
        onChange={this._handleChange}
        onBlur={this._handleBlur}
      />
    )
  }

  _renderInputBackdrop = () => {
    const { disableBackdrop } = this._getContext()
    const { value } = this.props

    if (disableBackdrop) {
      return null
    }

    const backdropValue = this._getBackdropValue()
    const { backdropClass } = this._classNameHolder()

    if (!backdropValue || !value) {
      return null
    }

    const backdropValueList = backdropValue.split('')
    const valueList = value.split('')

    const backdropValueNormalized = backdropValueList.map((letter, index) => {
      const letterToReplace = valueList[index]

      return letterToReplace || letter
    })

    return (
      <span className={backdropClass}>{backdropValueNormalized}</span>
    )
  }

  _renderInput = () => {
    return (
      <div className={styles.inputBlock}>
        {this._renderInputPure()}
        {this._renderInputBackdrop()}
      </div>
    )
  }

  _renderButtons = () => {
    return (
      <div className={styles.buttonsBlock}>
        {this._renderClearSearchButton()}
        {this._renderHistoryButton()}
        {this._renderInputSearchButton()}
      </div>
    )
  }

  render() {
    const { isDisabled } = this._getContext()

    if (isDisabled) {
      return null
    }

    const { containerClass } = this._classNameHolder()

    return (
      <div className={containerClass}>
        {this._renderEnhancers()}
        {this._renderInput()}
        {this._renderButtons()}
      </div>
    )
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <Input context={context} {...props} />}
  </NestedContext.Consumer>
)
