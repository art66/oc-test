import { ICategory } from '../../../interfaces/ICommon'
import { ILasts } from '../../Lasts/interfaces'

export interface IInputConfiguration {
  isDisabled?: boolean
  readOnly?: boolean
  placeholder?: string
  debounceDelay?: number
  disableBackdrop?: boolean
  clearInputOnClick?: boolean
  inputChecker?: {
    maxLength: number
    allowedSymbols: string
    removeSpaces: boolean
  }
  callbackClick?: (payload: object) => void
  callbackSearch?: (payload: object) => void
  callbackCancel?: (payload: object) => void
  callbackChange?: (payload: object) => void
  customStyles?: {
    inputWrap?: string
    search?: string
    activeDropDown?: string
    warning?: string
    disabled?: string
    backdrop?: string
  }
}

export interface IProps {
  value: string
  inviteItemsList: ICategory[]
  membersList: number[]
  lastsSearches: ILasts[]
  isFetchActive: boolean
  isFetchError: boolean
  isBadSymbol: boolean
  isMaxLength: boolean
  isDropDownActive: boolean
  isHistoryAppear: boolean
  onClick: (event: React.MouseEvent<HTMLElement>) => void
  onClear: (event: React.KeyboardEvent<HTMLElement> | React.MouseEvent<HTMLElement>) => void
  onSearch: (event: React.KeyboardEvent<HTMLElement> | React.MouseEvent<HTMLElement>) => void
  onSave: (member: EventTarget) => void
  onToggle: (newRole: string) => void
  onChange: (event: string) => void
  onDebounce: () => void
  onHistory: () => void
  onCheck: (error?: { errorType: 'isBadSymbol' | 'isMaxLength' }) => void
  context?: any // temporary!
}

export interface IState {
  isAutocompleteSave: boolean
}

export interface IRestrictFlags {
  isBadSymbol: boolean
  isMaxLength: boolean
  isFetchError?: boolean
}
