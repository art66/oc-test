import React from 'react'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import isEmpty from '@torn/shared/utils/isEmpty'

import getContext from '../../helpers/getContext'
import iconStyler from '../../utils/iconStyler'
import NestedContext from '../../context'
import { tooltipsSubscriber } from '../../../TooltipNew'

import { IProps, IAddSaveConfiguration } from './interfaces/IAddSave'
import styles from './styles/addsave.cssmodule.scss'

const DEFAULT_ADD_ICON = 'Plus'
const DEFAULT_ICON_PROPS = {
  fill: {
    color: '#999',
    strokeWidth: 0
  },
  filter: {
    isActive: false
  },
  dimensions: {
    width: 10,
    height: 10
  }
}

class AddSave extends React.PureComponent<IProps> {
  // static contextType = NestedContext

  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()
  }

  _initTooltips = () => {
    const { tooltips } = this._getContext()
    const { text } = tooltips || {}

    if (!this._isTooltipsAllowed()) {
      return []
    }

    return [
      {
        child: text || 'Add',
        ID: DEFAULT_ADD_ICON.toLowerCase()
      }
    ]
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'update' })

  _isTooltipsAllowed = () => {
    const { context } = this.props
    const {
      systemData: { activateTooltips: isActiveTooltips }
    } = context
    const { tooltips } = this._getContext()
    const { isActive } = tooltips || {}

    return isActiveTooltips || isActive
  }

  _getContext = () => {
    const { context } = this.props

    const contextData = getContext({ context, name: 'enhancers' })

    return contextData.list.find((enhancer: IAddSaveConfiguration) => enhancer.type === 'addSave')
  }

  _classNameHolder = () => {
    const { customStyles } = this._getContext()
    const { container, containerDisabled } = customStyles || {}

    const containerClass = classnames({
      [styles.container]: true,
      [styles.containerDisabled]: this._isPreventClick(),
      [container]: container,
      [containerDisabled]: containerDisabled && this._isPreventClick()
    })

    return {
      containerClass
    }
  }

  _isPreventClick = () => {
    const { context, membersList = [], inviteItemsList = [], isDropDown } = this.props
    const { disabledList = [] } = context?.dropdown || {}

    const isAlreadyAddedInternal = inviteItemsList && membersList?.some(item => item === inviteItemsList[0]?.ID)
    const isAlreadyAddedExternal = inviteItemsList && disabledList?.some(item => item === inviteItemsList[0]?.ID)

    return isEmpty(inviteItemsList) || isAlreadyAddedInternal || isAlreadyAddedExternal || !isDropDown
  }

  _handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    const { callback } = this._getContext()
    const { onClick } = this.props

    if (!onClick || this._isPreventClick()) {
      return
    }

    callback && callback(event)
    onClick(event)
  }

  _handleMouseDown = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
  }

  _getIconStyles = () => {
    const { context } = this.props
    const { systemData } = context
    const { stylePreset } = systemData
    const { iconProps = {} } = this._getContext()

    const iconPostprocessingStyles = iconStyler({
      preset: stylePreset,
      isHover: false,
      isStroke: true,
      isDynamicColor: true,
      iconProps,
      customDefault: '#fff'
    })

    return iconPostprocessingStyles
  }

  _renderChild = () => {
    const { iconName, iconProps, customChild } = this._getContext()

    if (customChild) {
      return customChild
    }

    return (
      <SVGIconGenerator
        iconName={iconName || DEFAULT_ADD_ICON}
        {...DEFAULT_ICON_PROPS}
        {...iconProps}
        {...this._getIconStyles()}
      />
    )
  }

  render() {
    const { isDisabled } = this._getContext()

    if (isDisabled) {
      return null
    }

    const { containerClass } = this._classNameHolder()

    return (
      <button
        id={(this._isTooltipsAllowed() && DEFAULT_ADD_ICON.toLowerCase()) || ''}
        type="button"
        className={containerClass}
        onClick={this._handleClick}
        onMouseDown={this._handleMouseDown}
      >
        {this._renderChild()}
      </button>
    )
  }
}

export default props => (
  <NestedContext.Consumer>{context => <AddSave context={context} {...props} />}</NestedContext.Consumer>
)
