import React from 'react'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import firstLetterUpper from '@torn/shared/utils/firstLetterUpper'
import isEmpty from '@torn/shared/utils/isEmpty'

import { IProps, IState, IAddRoleConfiguration } from './interfaces/IAddRole'
import styles from './styles/addrole.cssmodule.scss'

import NestedContext from '../../context'
import getContext from '../../helpers/getContext'
import { tooltipsSubscriber } from '../../../TooltipNew'
import iconStyler from '../../utils/iconStyler'

const DEFAULT_TOGGLE_ICON = 'Testing'
const DEFAULT_TOGGLE_ID = 'toggleRole'
const DEFAULT_ROLES = ['tester', 'editor']
const DEFAULT_ACTIVE_ROLE = 'tester'
const DEFAULT_ICON_NAMED = {
  tester: 'Testing',
  editor: 'Cubes'
}

class AddRole extends React.PureComponent<IProps, IState> {
  // static contextType = NestedContext

  constructor(props: IProps) {
    super(props)

    this.state = {
      activeRole: DEFAULT_ACTIVE_ROLE,
      updateType: ''
    }
  }

  static getDerivedStateFromProps(nextProps: IProps, prevState: IState) {
    if (prevState.updateType === 'state') {
      return {}
    }

    const enhancer = nextProps.context.enhancers.list.find(
      (_enhancer: IAddRoleConfiguration) => _enhancer.type === 'addRole'
    ) || {}

    return {
      activeRole: enhancer.activeRole
    }
  }

  componentDidMount() {
    this._mountTooltips()
  }

  componentDidUpdate() {
    this._updateTooltips()
    this._checkForActiveRole()
  }

  _initTooltips = () => {
    const { activeRole } = this.state

    if (!this._isTooltipsAllowed()) {
      return []
    }

    return [{
      child: activeRole,
      ID: DEFAULT_TOGGLE_ID
    }]
  }

  _isTooltipsAllowed = () => {
    const { context } = this.props
    const { systemData: { activateTooltips: isActiveTooltips } } = context
    const { tooltips } = this._getContext()
    const { isActive } = tooltips || {}

    return isActiveTooltips || isActive
  }

  _mountTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'mount' })

  _updateTooltips = () => tooltipsSubscriber.render({ tooltipsList: this._initTooltips(), type: 'update' })

  _getContext = () => {
    const { context } = this.props
    const contextData = getContext({ context, name: 'enhancers' })

    return contextData.list.find((enhancer: IAddRoleConfiguration) => enhancer.type === 'addRole')
  }

  _classNameHolder = () => {
    const { customStyles } = this._getContext()
    const { container, containerDisabled } = customStyles || {}

    const rolesToToggle = this._getAllowedRole()

    const containerClass = classnames({
      [styles.container]: true,
      [styles.containerDisabled]: isEmpty(rolesToToggle),
      [container]: container,
      [containerDisabled]: containerDisabled && isEmpty(rolesToToggle)
    })

    return {
      containerClass
    }
  }

  _handleClick = () => {
    const { callback } = this._getContext()
    const { activeRole } = this.state
    const { onClick } = this.props

    const rolesToToggle = this._getAllowedRole()

    if (!onClick || isEmpty(rolesToToggle)) {
      return
    }

    const [newRole] = rolesToToggle.filter((role: string) => role !== activeRole) || []

    callback && callback(newRole)
    onClick(newRole)

    if (rolesToToggle.length > 1) {
      this.setState({
        activeRole: newRole,
        updateType: 'state'
      })
    }
  }

  _handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  }

  _getAllowedRole = () => {
    const { disabledRoles, roles = DEFAULT_ROLES } = this._getContext()

    if (isEmpty(disabledRoles)) {
      return roles
    }

    const allowedRoles = []

    if (isEmpty(roles)) {
      return allowedRoles
    }

    roles.forEach((role: string) => {
      const isRoleShouldBeDisabled = disabledRoles.some((disabledRole: string) => disabledRole === role)
      const isRoleAlreadyAmongAllowed = allowedRoles.some((addedRole: string) => addedRole === role)

      if (isRoleShouldBeDisabled || isRoleAlreadyAmongAllowed) {
        return
      }

      allowedRoles.push(role)
    })

    return allowedRoles
  }

  _checkForActiveRole = () => {
    const { activeRole } = this.state

    const allowedRoles = this._getAllowedRole()

    if (isEmpty(allowedRoles)) {
      return
    }

    const isCurrentRoleNotAllowed = !allowedRoles.some((role: string) => role === activeRole)

    if (isCurrentRoleNotAllowed) {
      this.setState({
        activeRole: allowedRoles[0],
        updateType: 'state'
      })
    }
  }

  _getIconName = () => {
    const { iconNames = DEFAULT_ICON_NAMED } = this._getContext()
    const { activeRole } = this.state

    const iterableName = iconNames && iconNames[activeRole] || null

    return iterableName || DEFAULT_TOGGLE_ICON
  }

  _getIconStyles = () => {
    const { context } = this.props
    const { systemData } = context
    const { stylePreset } = systemData
    const { iconProps = {} } = this._getContext()

    const iconPostprocessingStyles = iconStyler({
      preset: stylePreset,
      isHover: false,
      isStroke: true,
      isDynamicColor: false,
      iconProps,
      customDefault: '#fff'
    })

    return iconPostprocessingStyles
  }

  _renderChild = () => {
    const { iconProps } = this._getContext()

    const iconName = this._getIconName()
    const nameToRender = firstLetterUpper({ value: iconName, isDisabledMultiUpper: true })

    return <SVGIconGenerator iconName={nameToRender} {...iconProps} {...this._getIconStyles()} />
  }

  render() {
    const { isDisabled } = this._getContext()

    if (isDisabled) {
      return null
    }

    const { containerClass } = this._classNameHolder()

    return (
      <button
        id={this._isTooltipsAllowed() && DEFAULT_TOGGLE_ID || ''}
        type='button'
        className={containerClass}
        onClick={this._handleClick}
        onMouseDown={this._handleMouseDown}
      >
        {this._renderChild()}
      </button>
    )
  }
}

export default props => (
  <NestedContext.Consumer>
    {context => <AddRole context={context} {...props} />}
  </NestedContext.Consumer>
)
