/* eslint-disable max-statements */
import React from 'react'
import { mount, ReactWrapper } from 'enzyme'

import AddRole from '../AddRole'
import initialState, { disabledRole, disabledRoleSelection } from './mocks/addRole'

describe('<AddRole />', () => {
  const testActiveButton = (component: ReactWrapper) => {
    const Component = component

    expect(Component.find('AddRole').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('button.containerDisabled').length).toBe(0)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Testing')
  }

  const testDisabledButton = (component: ReactWrapper) => {
    const Component = component

    expect(Component.find('AddRole').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('button.containerDisabled').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Testing')
  }

  const testChangeButtonRole = (component: ReactWrapper) => {
    const Component = component

    expect(Component.find('AddRole').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Cubes')
  }

  const testDisabledButtonAfterChange = (component: ReactWrapper) => {
    const Component = component

    expect(Component.find('AddRole').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('button.containerDisabled').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Cubes')
  }

  it('should render with default props and active toggle button', async done => {
    const Component = mount(<AddRole {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    testActiveButton(Component)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render with disabled toggle button', async done => {
    const Component = mount(<AddRole {...disabledRoleSelection} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    testDisabledButton(Component)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should change active role based on disabled roles provided', async done => {
    const Component = mount(<AddRole {...initialState} />)

    // check roles on load
    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    testActiveButton(Component)
    expect(Component).toMatchSnapshot()

    // check roles once one of them is reach the limit
    await new Promise(res => setTimeout(() => res(), 1000))
    Component.setProps(disabledRole)
    Component.update()

    testChangeButtonRole(Component)
    expect(Component).toMatchSnapshot()

    // check that all the roles should be unable to select, then button should be disabled
    await new Promise(res => setTimeout(() => res(), 1000))
    Component.setProps(disabledRoleSelection)
    Component.update()

    testDisabledButtonAfterChange(Component)
    expect(Component).toMatchSnapshot()

    done()
  })
})
