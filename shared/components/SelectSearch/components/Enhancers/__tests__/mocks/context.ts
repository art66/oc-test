export default {
  systemData: {
    activateTooltips: false
  },
  enhancers: {
    isActive: true,
    list: [
      {
        type: 'addSave',
        isDisabled: false,
        iconName: '',
        iconProps: {},
        customChild: null,
        callback: payload => console.log('callback ENHANCER CLICK payload: ', payload),
        tooltips: {
          isActive: true,
          text: 'Add'
        },
        customStyles: {
          container: '',
          containerDisabled: ''
        }
      },
      {
        type: 'addRole',
        isDisabled: false,
        activeRole: 'tester',
        roles: ['tester', 'editor'],
        disabledRoles: [],
        iconNames: {
          editor: 'Cubes',
          tester: 'Testing'
        },
        iconProps: {},
        tooltips: {
          isActive: true
        },
        callback: payload => console.log('callback ENHANCER TOGGLE payload: ', payload),
        customStyles: {
          container: '',
          containerDisabled: ''
        }
      }
    ]
  }
}
