import context from './context'

const initialState = {
  context
}

const disabledRoleSelection = {
  ...initialState,
  context: {
    ...initialState.context,
    enhancers: {
      ...initialState.context.enhancers,
      list: [
        initialState.context.enhancers.list[0],
        {
          ...initialState.context.enhancers.list[1],
          disabledRoles: ['tester', 'editor']
        }
      ]
    }
  }
}

const disabledRole = {
  ...initialState,
  context: {
    ...initialState.context,
    enhancers: {
      ...initialState.context.enhancers,
      list: [
        initialState.context.enhancers.list[0],
        {
          ...initialState.context.enhancers.list[1],
          disabledRoles: ['tester']
        }
      ]
    }
  }
}


export default initialState
export { disabledRoleSelection, disabledRole }
