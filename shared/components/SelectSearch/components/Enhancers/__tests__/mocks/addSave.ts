import context from './context'

const initialState = {
  membersList: [],
  inviteItemsList: [{ ID: 1 }],
  isDropDown: true,
  context
}

const emptyList = {
  ...initialState,
  inviteItemsList: []
}

const dropDownHidden = {
  ...initialState,
  isDropDown: false
}

const alreadyAdded = {
  ...initialState,
  membersList: [1]
}

export default initialState
export { emptyList, dropDownHidden, alreadyAdded }
