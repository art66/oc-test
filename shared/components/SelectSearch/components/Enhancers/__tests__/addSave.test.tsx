import React from 'react'
import { mount } from 'enzyme'

import AddSave from '../AddSave'
import initialState, { emptyList, dropDownHidden, alreadyAdded } from './mocks/addSave'

describe('<AddSave />', () => {
  it('should render with default active button', async done => {
    const Component = mount(<AddSave {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('AddSave').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('button.containerDisabled').length).toBe(0)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Plus')
    expect(Component.find('svg').length).toBe(1)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render disabled button if list empty', async done => {
    const Component = mount(<AddSave {...emptyList} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('AddSave').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('button.containerDisabled').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Plus')
    expect(Component.find('svg').length).toBe(1)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render disabled button if dropDown not appear', async done => {
    const Component = mount(<AddSave {...dropDownHidden} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('AddSave').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('button.containerDisabled').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Plus')
    expect(Component.find('svg').length).toBe(1)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render disabled button if item already added', async done => {
    const Component = mount(<AddSave {...alreadyAdded} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('AddSave').length).toBe(1)
    expect(Component.find('button.container').length).toBe(1)
    expect(Component.find('button.containerDisabled').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('SVGIconGenerator').prop('iconName')).toBe('Plus')
    expect(Component.find('svg').length).toBe(1)

    expect(Component).toMatchSnapshot()

    done()
  })
})
