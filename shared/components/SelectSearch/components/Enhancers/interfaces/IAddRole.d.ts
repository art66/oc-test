export interface IAddRoleConfiguration {
  type?: 'addRole'
  isDisabled?: boolean
  iconNames?: object
  iconProps?: object
  activeRole?: string
  roles?: string[]
  disabledRoles?: string[]
  callback?: (newRole: string) => void
  tooltips?: {
    isActive?: boolean
  }
  customStyles?: {
    container?: string
    containerDisabled?: string
  }
}

export interface IProps {
  onClick: (newRole: string) => void
  context?: any // temporary!
}

export interface IState {
  activeRole: string
  updateType: 'state' | 'props' | ''
}
