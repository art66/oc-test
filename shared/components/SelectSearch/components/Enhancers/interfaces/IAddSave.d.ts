import { ICategory } from '../../../interfaces/ICommon'

export interface IAddSaveConfiguration {
  type: 'addSave'
  isDisabled?: boolean
  iconName?: string
  iconProps?: object
  customChild?: JSX.Element
  callback?: (event: React.MouseEvent<HTMLButtonElement>) => void
  tooltips?: {
    isActive?: boolean
    text?: string
  }
  customStyles?: {
    container?: ''
    containerDisabled?: ''
  }
}

export interface IDefaultProps {
  configuration?: IAddSaveConfiguration
}

export interface IProps extends IAddSaveConfiguration {
  configuration?: IAddSaveConfiguration
  isDropDown?: boolean
  membersList?: number[]
  inviteItemsList?: ICategory[]
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void
  context?: any // temporary!
}
