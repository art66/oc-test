import { IAddSaveConfiguration } from './IAddSave'
import { IAddRoleConfiguration } from './IAddRole'

export interface IEnhancersConfiguration {
  type?: string
  isActive?: boolean
  list?: Array<IAddSaveConfiguration | IAddRoleConfiguration>
}
