import AddSave from './AddSave'
import AddRole from './AddRole'

export default {
  addSave: AddSave,
  addRole: AddRole
}
