# SelectSearch - Torn


## 14.2.0
 * Added ability to clear input on select save.

## 14.1.3
 * Fixed broken AddSave enhancer onClick event.
 * Updated tests.

## 14.1.2
 * Added minor fixes.

## 14.1.1
 * Added readme description.

## 14.1.0
 * SelectSearch has been finally covered by unit tests.

## 14.0.0
 * SelectSearch has been finally covered by storybook.

## 13.3.0
 * Added Buttons storybook.

## 13.2.0
 * Improved Enhanced object, fixed some urgent bugs inside API.
 * Improved Buttons logic to be controlled from thrown preset config.

## 13.1.0
 * Improved storybook config.
 * Fixed buttons styler util.
 * Fixed interface of the User.

## 13.0.0
 * Created easy configurable presets logic for instantly package usage.

## 12.0.1
 * Fixed adaptive styles.

## 12.0.0
 * Added re-fetching logic once we fail the attempt on the initial load.
 * Improved deeps objects nesting access, now app won't fail while the parents are not provided.
 * Improved interfaces for icons params managing.
 * Fixed a lot of errors while using in production.
 * External list of already clicked users were added in the API.

## 11.4.0
 * Removed duplication.

## 11.3.0
 * Lifted up tooltips state & component for a better rendering.

## 11.2.0
 * Added tooltips for interactive icons.

## 11.1.0
 * Created helpers for context navigation.

## 11.0.0
 * Created an independent Context through whole App.

## 10.3.0
 * Added Search History functional.

## 10.2.0
 * Improved Top Section Layout.

## 10.1.0
 * Added enhancer AddRole Component.

## 10.0.0
 * Added enhancer Components and their logic along with API.

## 9.5.1
 * Improved autocomplete backdrop logic.
 * Fixed Tabs navigation once autocomplete is done.

## 9.4.0
 * Added backdrop feature for real-live looks typing.

## 9.3.0
 * Added broken connection handling.
 * Added ability to show dropDown list instantly without first click on input field to load.

## 9.2.0
 * Added LocalStorage and Websockets API.

## 9.1.0
 * Added API for user saving outside of the Component on callback.

## 9.0.0
 * Created fully customizable, developer-friendly interface for managing UserSearch work.

## 8.4.0
 * Added fully covered Tabs & Tooltips interfaces with ability to set custom DropDown layouts: Users, Items, etc.

## 8.3.0
 * Added fully covered DropDown interface with ability to set custom DropDown layouts: Users, Items, etc.

## 8.2.0
 * Created DropDown decomposition.

## 8.1.0
 * Significancy improved typings.

## 8.0.0
 * Added warning tooltips component.
 * Added string sanitization for user search input.

## 7.0.0
 * Integrated websockets channel for live users list updates.

## 6.0.0
 * Integrated localStorage data caching.

## 5.3.0
 * Added tabs navigation by keys.

## 5.2.0
 * Added save string decoration in CSS.
 * Added partially dropdown API.

## 5.1.0
 * Added fetch request caching.

## 5.0.1
 * Fixed advanced navigation for tabs accessability.

## 5.0.0
 * Added fully covered users list navigation.

## 4.0.0
 * Added WAI ARIA support by clicking fucusable elements.
 * Prevented memory leak in case if component unmounted but AJAX request is still in progress.
 * Other minor fixes.

## 3.1.0
 * Improved app architecture.

## 3.0.0
 * Created deconstructed component architecture for a better support and further development.

## 2.1.0
 * Improved Typings.
 * Added keyDown events.
 * Fixed some bugs.

## 2.0.0
 * Significantly improved stability and searching logic.

## 1.1.0
 * Fully implemented live-search system.

## 1.0.0
 * First stable release.
