export const ROOT_FETCH_URL = '/autocompleteUserAjaxAction.php'
export const NEW_SEARCH_POSTFIX = 'option=ac-ct-all'

export const BLUE_HOVERED = '#7fa1b2'
export const BLUE_DIMMED = '#d4dfe5'

export const SEARCH_TOGGLE_ICON_NAMES = {
  editor: 'Cubes',
  tester: 'Testing'
}
export const SEARCH_TOGGLE_ICON_DIMENSIONS = {
  Testing: {
    width: 15,
    height: 15,
    viewbox: '0 0 22 24'
  },
  Cubes: {
    width: 17,
    height: 15,
    viewbox: '0 0 22 24'
  }
}

export const SEARCH_PLACEHOLDER = 'Type user\'s name or ID...'
export const SEARCH_LOOP_DIMENSIONS = {
  width: 14,
  height: 14,
  viewbox: '0 0 14 14'
}
export const CLOSE_DIMENSIONS = {
  width: 12,
  height: 12,
  viewbox: '-2 -3 28 28'
}
export const CLOSE_CONFIG = {
  onHover: {
    active: true,
    fill: {
      color: BLUE_HOVERED
    }
  }
}
export const PRESET_ICONS = {
  grey: {
    color: '#ccc',
    disabledColor: '#ccc',
    activeColor: '#999',
    hoverColor: '#999'
  }
}


export const ALL_LISTS_TAB_LABEL = 'all'
export const MINIMAL_CONTAINER_CAPACITY = 5

export const LOCAL_STORE_KEY = 'user_fellows'
export const LASTS_STORE_KEY = 'lasts'
export const DEFAULT_TABS = ['friends', 'faction', 'company', 'all']
export const MAX_INPUT_LENGTH = 25
