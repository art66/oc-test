import { IPayload, IWebsockets } from '../interfaces/IMiddleware'

const websockets = ({ callback, actionID, channelID }: IWebsockets) => {
  // global Websocket & Centrifuge handler!!!
  // @ts-ignore
  const handler = channelID ? new WebsocketHandler('channelID') : new WebsocketHandler()
  const wsActionID = actionID || 'userFellows'

  handler.setActions({
    [wsActionID]: (payload: IPayload) => callback(payload.itemsData)
  })
}

export default websockets
