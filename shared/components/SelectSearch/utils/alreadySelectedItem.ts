import isEmpty from '@torn/shared/utils/isEmpty'

export interface IAlreadySelected {
  membersList: number[]
  disabledList?: number[]
  ID: number | string
}

const alreadySelectedItem = ({ membersList, disabledList, ID } :IAlreadySelected) => {
  const listToCheck = !isEmpty(disabledList) ? disabledList : membersList

  if (!membersList && !disabledList || !ID) {
    return false
  }

  return listToCheck.some(memberID => memberID === Number(ID))
}

export default alreadySelectedItem
