import isEmpty from '@torn/shared/utils/isEmpty'

import { IItemsList } from '../interfaces/ICommon'

const reversItems = (itemsList: IItemsList) => {
  const reversedItems = {}

  if (isEmpty(itemsList)) {
    return itemsList
  }

  Object.keys(itemsList).forEach(list => {
    reversedItems[list] = itemsList[list]
  })

  return reversedItems
}

export default reversItems
