import isEmpty from '@torn/shared/utils/isEmpty'

import { ICoreDropDown } from '../components/DropDown/interfaces'

const placeholderRequired = (props: ICoreDropDown) => {
  const {
    isAllItemsTab,
    isActiveSearch,
    isInitLoad,
    isForceSearch,
    isLiveSearch,
    isManualSearch,
    inviteItemsList
  } = props

  const isNoItemsFounded = isEmpty(inviteItemsList)

  const manualSearch = () => isManualSearch && !isNoItemsFounded
  const fallback = () => isNoItemsFounded && 'default'
  const initLoad = () => isInitLoad && 'init'
  const liveSearch = () => (isForceSearch && isNoItemsFounded || isLiveSearch) && 'live'
  const failedSearch = () => {
    const noItemsFoundInManualSearch = isManualSearch && !isNoItemsFounded
    const noItemsFoundAtAll = isAllItemsTab && isNoItemsFounded
    const noItemFoundWhileSearching = isActiveSearch && isNoItemsFounded

    return (noItemsFoundInManualSearch || noItemsFoundAtAll || noItemFoundWhileSearching) && 'fail'
  }

  return !manualSearch() && (initLoad() || liveSearch() || failedSearch() || fallback())
}

export default placeholderRequired
