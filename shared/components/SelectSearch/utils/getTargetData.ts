const getTargetData = (target: EventTarget) => {
  if (!target || !(target as HTMLButtonElement).dataset) {
    return null
  }

  const { dataset } = target as HTMLButtonElement
  const { name, id: ID } = dataset

  return {
    name,
    ID: Number(ID)
  }
}

export default getTargetData
