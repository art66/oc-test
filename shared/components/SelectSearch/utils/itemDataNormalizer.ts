import isEmpty from '@torn/shared/utils/isEmpty'

import { IUserRaw } from '../interfaces/ICommon'

const normalizedItem = (itemData: IUserRaw) => {
  if (isEmpty(itemData)) {
    return null
  }

  return ({
    name: itemData.name,
    category: itemData.type,
    online: itemData.online,
    ID: Number(itemData.id)
  })
}

export default normalizedItem
