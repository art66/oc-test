import { ALL_LISTS_TAB_LABEL } from '../constants'
import { TCurrentTab, TTabs } from '../interfaces/ICommon'

const isAllItemsRender = (categoriesTabs?: TTabs, tabLabel?: TCurrentTab) => {
  if (!categoriesTabs || !tabLabel) {
    return true
  }

  const commonLabel = tabLabel === ALL_LISTS_TAB_LABEL
  const tabLabelNotPresent = !categoriesTabs.some(label => label === tabLabel)

  return commonLabel || tabLabelNotPresent
}

export default isAllItemsRender
