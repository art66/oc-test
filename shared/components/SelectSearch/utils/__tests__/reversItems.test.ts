import reversItems from '../reversItems'

import { initialState, reversedInitialState } from './mocks/reversItems'

describe('reversItems()', () => {
  it('should reverse items lists in the object', () => {
    expect(reversItems(initialState)).toEqual(reversedInitialState)
  })
  it('should return null if no data provided', () => {
    expect(reversItems({})).toEqual({})
  })
})
