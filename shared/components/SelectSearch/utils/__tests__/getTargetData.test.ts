import getTargetData from '../getTargetData'

const target = null

describe('getTargetData()', () => {
  it('should return null if no target provided', () => {
    expect(getTargetData(target)).toBe(null)
  })
})
