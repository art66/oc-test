import isAllItemsRender from '../isAllItemsRender'

describe('isAllItemsRender()', () => {
  it('should return true if categoriesTabs includes tabLabel', () => {
    expect(isAllItemsRender(['all', 'categories'], 'all')).toBeTruthy()
  })
  it('should return true if nothing were thrown', () => {
    expect(isAllItemsRender(null)).toBeTruthy()
  })
  it('should return true if tabLabel === ALL_LISTS_TAB_LABEL', () => {
    expect(isAllItemsRender(null, 'all')).toBeTruthy()
  })
  it('should return false if tabLabel !== "all" among tabs list', () => {
    expect(isAllItemsRender(['all', 'categories'], 'categories')).toBeFalsy()
  })
})
