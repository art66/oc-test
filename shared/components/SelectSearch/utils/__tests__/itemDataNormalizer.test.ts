import itemDataNormalizer from '../itemDataNormalizer'

import { configBefore, configAfter } from './mocks/itemDataNormalizer'

describe('itemDataNormalizer()', () => {
  it('should return normalized itemConfig', () => {


    expect(itemDataNormalizer(configBefore as any)).toEqual(configAfter)
  })
  it('should return null if no data provided', () => {
    // @ts-ignore
    expect(itemDataNormalizer({})).toBe(null)
  })
})
