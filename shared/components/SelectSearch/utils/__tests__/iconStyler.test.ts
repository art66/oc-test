import iconStyler from '../iconStyler'

import {
  initialState,
  receivedInitial,
  dynamicColorBefore,
  dynamicColorAfter,
  withStrokeBefore,
  withStrokeAfter,
  iconPersonalColorBefore,
  iconPersonalColorAfter,
  withPresetBefore,
  withPresetAfter
} from './mocks/iconStyler'

describe('iconStyler()', () => {
  it('should return svg icon styles with deactivated onHover and without dynamic color changing', () => {
    const result = iconStyler(initialState)

    expect(result).toEqual(receivedInitial)
    expect(result).toMatchSnapshot()
  })
  it('should return svg icon styles with deactivated onHover and active dynamic color', () => {
    const result = iconStyler(dynamicColorBefore)

    expect(result).toEqual(dynamicColorAfter)
    expect(result).toMatchSnapshot()
  })
  it('should return svg icon styles with stroke', () => {
    const result = iconStyler(withStrokeBefore)

    expect(result).toEqual(withStrokeAfter)
    expect(result).toMatchSnapshot()
  })
  it('should return svg icon with particular icon color instead of global one', () => {
    const result = iconStyler(iconPersonalColorBefore)

    expect(result).toEqual(iconPersonalColorAfter)
    expect(result).toMatchSnapshot()
  })
  it('should return svg icon with preset styles configuration', () => {
    const result = iconStyler(withPresetBefore)

    expect(result).toEqual(withPresetAfter)
    expect(result).toMatchSnapshot()
  })
})
