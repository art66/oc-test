export const initialState = {
  preset: 'grey' as 'grey',
  isHover: false,
  isStroke: false,
  isDynamicColor: false,
  commonActiveColor: '#fff',
  commonDisabledColor: '#111',
  commonIconsProps: {},
  iconActiveColor: '',
  iconDisabledColor: '',
  iconProps: {},
  customDefault: ''
}

export const receivedInitial = {
  fill: {
    color: '#ccc',
    stroke: '#ccc',
    strokeWidth: 0
  },
  onHover: {
    active: false,
    fill: {
      color: '#999',
      stroke: '#999',
      strokeWidth: 0
    }
  }
}

export const dynamicColorBefore = {
  ...initialState,
  isDynamicColor: true
}

export const dynamicColorAfter = {
  ...receivedInitial,
  fill: {
    ...receivedInitial.fill,
    color: '#999',
    stroke: '#999'
  }
}

export const withStrokeBefore = {
  ...initialState,
  isStroke: true
}

export const withStrokeAfter = {
  ...receivedInitial,
  fill: {
    ...receivedInitial.fill,
    strokeWidth: 0.1
  },
  onHover: {
    ...receivedInitial.onHover,
    fill: {
      ...receivedInitial.onHover.fill,
      strokeWidth: 0.1
    }
  }
}

export const iconPersonalColorBefore = {
  ...initialState,
  iconActiveColor: '#ccc',
  iconDisabledColor: '#c4c'
}

export const iconPersonalColorAfter = {
  ...receivedInitial,
  fill: {
    ...receivedInitial.fill,
    color: '#ccc',
    stroke: '#ccc'
  },
  onHover: {
    ...receivedInitial.onHover,
    fill: {
      ...receivedInitial.onHover.fill,
      color: '#999',
      stroke: '#999'
    }
  }
}

export const withPresetBefore = {
  ...initialState,
  preset: 'grey' as 'grey'
}

export const withPresetAfter = {
  ...receivedInitial,
  fill: {
    ...receivedInitial.fill,
    color: '#ccc',
    stroke: '#ccc'
  },
  onHover: {
    ...receivedInitial.onHover,
    fill: {
      ...receivedInitial.onHover.fill,
      color: '#999',
      stroke: '#999'
    }
  }
}
