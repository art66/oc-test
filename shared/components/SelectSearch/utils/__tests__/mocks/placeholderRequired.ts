export const initialState = {
  isAllItemsTab: false,
  isActiveSearch: false,
  isInitLoad: false,
  isForceSearch: false,
  isLiveSearch: false,
  isManualSearch: false,
  inviteItemsList: []
}

export const manualSearch = {
  ...initialState,
  isInitLoad: true,
  isManualSearch: true
}

export const initLoad = {
  ...initialState,
  isInitLoad: true
}

export const livePlaceholder = {
  ...initialState,
  isLiveSearch: true
}

export const fallbackPlaceholder = {
  ...initialState
}

export const failPlaceholder = {
  ...initialState,
  isAllItemsTab: true
}
