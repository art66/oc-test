export const configBefore = {
  name: 'test',
  type: 'test_cat',
  online: 'online',
  id: 1,
  boris: 'false'
}

export const configAfter = {
  name: 'test',
  category: 'test_cat',
  online: 'online',
  ID: 1
}
