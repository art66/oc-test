export const initialState = {
  membersList: [1],
  disabledList: [],
  ID: 1
}

export const foundBtwDisabledList = {
  ...initialState,
  membersList: [],
  disabledList: [1]
}

export const noSelectedFound = {
  ...initialState,
  membersList: [2, 3],
  ID: 1
}
