export const initialState = {
  friends: [],
  faction: [],
  company: [],
  all: []
}

export const reversedInitialState = {
  all: [],
  company: [],
  faction: [],
  friends: []
}
