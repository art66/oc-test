import placeholderRequired from '../placeholderRequired'

import {
  manualSearch,
  initLoad,
  livePlaceholder,
  fallbackPlaceholder,
  failPlaceholder
} from './mocks/placeholderRequired'

describe('placeholderRequired()', () => {
  it('should return manual placeholder', () => {
    expect(placeholderRequired(manualSearch)).toBeTruthy()
  })
  it('should return init load placeholder', () => {
    expect(placeholderRequired(initLoad)).toBe('init')
  })
  it('should return live load placeholder', () => {
    expect(placeholderRequired(livePlaceholder)).toBe('live')
  })
  it('should return fallback load placeholder', () => {
    expect(placeholderRequired(fallbackPlaceholder)).toBe('default')
  })
  it('should return failed load placeholder', () => {
    expect(placeholderRequired(failPlaceholder)).toBe('fail')
  })
})
