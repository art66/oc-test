import alreadySelectedItem from '../alreadySelectedItem'

import { initialState, foundBtwDisabledList, noSelectedFound } from './mocks/alreadySelectedItem'

describe('alreadySelectedItem()', () => {
  it('should return true because some item already selected', () => {
    expect(alreadySelectedItem(initialState)).toBeTruthy()
  })
  it('should return true because some item thrown from outer component', () => {
    expect(alreadySelectedItem(foundBtwDisabledList)).toBeTruthy()
  })
  it('should return false because no items selected were founded', () => {
    expect(alreadySelectedItem(noSelectedFound)).toBeFalsy()
  })
})
