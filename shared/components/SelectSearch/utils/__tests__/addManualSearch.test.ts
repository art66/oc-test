import addManualSearch from '../addManualSearch'

describe('addManualSearch()', () => {
  it('should return actual query parameter', () => {
    expect(addManualSearch('manualInput')).toBe('&q=manualInput')
  })
  it('should not return query parameter', () => {
    expect(addManualSearch('')).toBe('')
  })
})
