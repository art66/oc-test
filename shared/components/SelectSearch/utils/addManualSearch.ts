const addManualSearch = (manualInput: string) => manualInput && `&q=${manualInput}` || ''


export default addManualSearch
