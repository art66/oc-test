import { IIConStyleProps as ISVGProps, IEvents } from '@torn/shared/SVG/types/icons'

import { BLUE_HOVERED, BLUE_DIMMED, CLOSE_CONFIG, PRESET_ICONS } from '../constants'

interface IIconStyler {
  preset?: 'grey' | 'blue'
  isHover?: boolean
  isStroke?: boolean
  isDynamicColor?: boolean
  commonActiveColor?: string
  commonDisabledColor?: string
  commonIconsProps?: ISVGProps & IEvents
  iconActiveColor?: string
  iconDisabledColor?: string
  iconProps?: ISVGProps & IEvents
  customDefault?: string
}

const iconStyler = (props: IIconStyler) => {
  const {
    preset,
    isHover,
    isStroke,
    isDynamicColor,
    commonActiveColor,
    commonDisabledColor,
    commonIconsProps = {},
    iconActiveColor,
    iconDisabledColor,
    iconProps = {},
    customDefault
  } = props

  const { activeColor, disabledColor, hoverColor } = preset && PRESET_ICONS[preset] || {}

  const iconHovered = activeColor || iconActiveColor || commonActiveColor || BLUE_HOVERED
  const iconDimmed = disabledColor || iconDisabledColor || commonDisabledColor || customDefault || BLUE_DIMMED

  const generateFill = () => {
    const fill = {
      ...commonIconsProps && commonIconsProps.fill,
      ...iconProps && iconProps.fill,
      strokeWidth: isStroke ? 0.1 : 0,
      stroke: isDynamicColor ? iconHovered : iconDimmed,
      color: isDynamicColor ? iconHovered : iconDimmed
    }

    return fill
  }

  const generateOnHover = () => {
    const onHover = {
      ...CLOSE_CONFIG.onHover,
      ...commonIconsProps && commonIconsProps.onHover,
      ...iconProps && iconProps.onHover,
      active: isHover,
      fill: {
        ...commonIconsProps && commonIconsProps.onHover && commonIconsProps.onHover.fill,
        ...iconProps && iconProps.onHover && iconProps.onHover.fill,
        color: hoverColor || iconHovered,
        strokeWidth: isStroke ? 0.1 : 0,
        stroke: hoverColor || iconHovered
      }
    }

    return onHover
  }

  return {
    fill: generateFill(),
    onHover: generateOnHover()
  }
}

export default iconStyler
