# SelectSearch - reusable lists items search.

### The Component for search items among lists with live-typing ability and intelligence caching.

Advantages:
 - fully reusable and agile to represent any kind of the data requested in the list, not only the users itself.
 - be secured and covered by user-friendly navigation (WAI and keyboard).
 - able to stop punishing server-side with redundant requests.
 - use websockets and localStorage under the hood.

How to use:
  - Quick Start:
    So for typical use (for **users search** in this example) you will find that only layout theme `{stylePreset: 'grey' | 'blue'}` and saveOnClick func `{callback: (item: object) => object}` will be enough.

    Example:
      ```
        <SelectSearch
          systemData={{ stylePreset: 'grey' }}
          saveOnClick={callbackOnSave}
        />
      ```
  - Advanced:
    For advanced usage you can dive into the whole API described inside mocks > storybook.ts state.
