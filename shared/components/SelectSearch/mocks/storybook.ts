
import { IProps } from '../interfaces/IController'

const DEFAULT_CONFIG: IProps = {
  isDisabled: false,
  fetchRoot: '',
  fullLayout: false,
  // @ts-ignore just for mock testing
  stylePreset: '',
  useGlobalTooltips: true,
  saveOnClick: payload => console.log('callback SAVE payload: ', payload),
  customStyles: {
    wrap: '',
    topContainer: '',
    bottomContainer: ''
  },
  systemData: {
    activateTooltips: false
  },
  localStorage: {
    isDisabled: false,
    storeKey: ''
  },
  websockets: {
    isDisabled: false,
    channelID: ''
  },
  input: {
    isDisabled: false,
    readOnly: false,
    placeholder: '',
    debounceDelay: null,
    disableBackdrop: false,
    clearInputOnClick: false,
    inputChecker: {
      maxLength: null,
      allowedSymbols: '',
      removeSpaces: true
    },
    callbackClick: payload => console.log('callback INPUT CLICK payload: ', payload),
    callbackSearch: payload => console.log('callback INPUT KEYDOWN SEARCH payload: ', payload),
    callbackCancel: payload => console.log('callback INPUT CANCEL payload: ', payload),
    callbackChange: payload => console.log('callback INPUT CHANGE payload: ', payload),
    customStyles: {
      inputWrap: '',
      search: '',
      activeDropDown: '',
      warning: '',
      disabled: '',
      backdrop: ''
    }
  },
  tabs: {
    isDisabled: false,
    activeTab: 'all',
    categories: ['friends', 'faction', 'company', 'all'],
    callback: payload => console.log('callback TABS payload: ', payload),
    customStyles: {
      tabsWrap: '',
      tabButton: '',
      tabButtonClicked: ''
    }
  },
  lasts: {
    isDisabled: false,
    storeLastsKey: '',
    iconHistoryName: '',
    iconCloseName: '',
    iconPropsHistory: {},
    iconPropsRemove: {},
    activateTooltips: false,
    tooltipText: '',
    callbackClick: payload => console.log('callback LASTS CLICK payload: ', payload),
    callbackDelete: payload => console.log('callback LASTS DELETE payload: ', payload),
    customStyles: {
      wrap: '',
      lastContainer: '',
      iconSvg: '',
      buttonRemove: '',
      label: ''
    }
  },
  dropdown: {
    disabledList: [],
    isDisabled: false,
    // @ts-ignore just for default mock props
    type: '', // we should be able to handle different dropDown layout: users/items/etc ['users'|'items']
    hideByClick: false,
    alreadySelected: {
      isDisabled: false,
      customAlreadySelected: null,
      alreadySelectedText: undefined
    },
    customConfig: {
      users: {
        status: {
          isDisabled: false,
          isSVGLayout: false,
          SVGStatusData: null
        },
        ID: {
          isDisabled: false
        },
        name: {
          isDisabled: false
        }
      }
    },
    customStyles: {
      users: {
        dropDownWrap: '',
        onlineStatus: '',
        onlineStatusIdle: '',
        onlineStatusActive: '',
        unitContainer: '',
        unitContainerSelected: '',
        name: '',
        id: '',
        alreadyInLabel: ''
      }
    }
  },
  placeholders: {
    isDisabled: false,
    spinner: {
      isDisabled: false,
      customSpinner: null
    },
    texts: {
      fallback: '',
      init: '',
      live: '',
      fail: ''
    },
    customStyles: {
      loadingAnim: '',
      textPlaceholder: ''
    }
  },
  tooltips: {
    isDisabled: false,
    texts: {
      badText: '',
      maxText: ''
    },
    animation: {
      timeIn: null,
      timeOut: null
    },
    customStyles: {
      tooltipWrap: '',
      animClass: ''
    }
  },
  buttons: {
    isDisabled: false,
    activateTooltips: false,
    commonActiveColor: '',
    commonDisabledColor: '',
    commonIconsProps: {},
    clearButton: {
      isDisabled: false,
      callback: payload => console.log('callback BUTTON CLEAR CLICK payload: ', payload),
      iconName: 'Close',
      iconActiveColor: '',
      iconDisabledColor: '',
      iconProps: null,
      tooltipText: '',
      customStyles: {
        buttonClass: ''
      }
    },
    historyButton: {
      isDisabled: false,
      callback: payload => console.log('callback BUTTON HISTORY CLICK payload: ', payload),
      iconName: 'History',
      iconActiveColor: '',
      iconDisabledColor: '',
      iconProps: null,
      tooltipText: '',
      customStyles: {
        buttonClass: ''
      }
    },
    commitButton: {
      isDisabled: false,
      callback: payload => console.log('callback BUTTON COMMIT CLICK payload: ', payload),
      iconName: 'Search',
      tooltipText: '',
      iconActiveColor: '',
      iconDisabledColor: '',
      customStyles: {
        buttonClass: '',
        buttonDisabled: '',
        animation: ''
      }
    }
  },
  enhancers: {
    isActive: false,
    list: [
      {
        type: 'addSave',
        isDisabled: false,
        iconName: '',
        iconProps: {},
        customChild: null,
        callback: payload => console.log('callback ENHANCER CLICK payload: ', payload),
        tooltips: {
          isActive: true,
          text: 'Add'
        },
        customStyles: {
          container: '',
          containerDisabled: ''
        }
      },
      {
        type: 'addRole',
        isDisabled: false,
        activeRole: 'tester',
        roles: ['tester', 'editor'],
        disabledRoles: [],
        iconNames: {
          editor: 'Cubes',
          tester: 'Testing'
        },
        iconProps: {},
        tooltips: {
          isActive: true
        },
        callback: payload => console.log('callback ENHANCER TOGGLE payload: ', payload),
        customStyles: {
          container: '',
          containerDisabled: ''
        }
      }
    ]
  }
}

export {
  DEFAULT_CONFIG
}
