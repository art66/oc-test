import React from 'react'
import uniqBy from 'lodash.uniqby'
import classnames from 'classnames'

import TooltipsGlobal from '@torn/shared/components/TooltipNew'
import getUserID from '@torn/shared/utils/getUserID'
import securedRegExpString from '@torn/shared/utils/secureRegExpString'
import { addStore, getStore } from '@torn/shared/utils/localStorageManager'

import NestedContext from '../context'

import normalizedItem from '../utils/itemDataNormalizer'
import isAllItemsRender from '../utils/isAllItemsRender'
import reversItems from '../utils/reversItems'
import addManualSearch from '../utils/addManualSearch'
import alreadySelectedItem from '../utils/alreadySelectedItem'
import getTargetData from '../utils/getTargetData'

import initWS from '../middleware/websockets'

import { ROOT_FETCH_URL, NEW_SEARCH_POSTFIX, ALL_LISTS_TAB_LABEL, LOCAL_STORE_KEY, LASTS_STORE_KEY } from '../constants'
import { IProps, IState, IFetchData, ICachedRequest } from '../interfaces/IController'
import {
  ICategory,
  IItemsList,
  IUserRaw,
  IUserRawWebsocket,
  TCurrentTab,
  IFetch,
  IMatch,
  IItemData
} from '../interfaces/ICommon'
import { ISetItems } from '../components/DropDown/interfaces'

import Tooltips from '../components/Tooltips'
import Input from '../components/Input'
import Tabs from '../components/Tabs'
import Lasts from '../components/Lasts'
import DropDown from '../components/DropDown'

import { ILasts } from '../components/Lasts/interfaces'
import { IContext } from '../interfaces/IContext'

import styles from '../styles/index.cssmodule.scss'
import '../styles/DM_vars.scss'
import '../styles/vars.scss'

class SelectSearch extends React.Component<IProps, IState> {
  private _isMounted: boolean
  private _initTimerFetchID: any

  static defaultProps: IProps = {
    isDisabled: false,
    systemData: {
      activateTooltips: false
    },
    localStorage: {
      isDisabled: false,
      storeKey: 'item_fellows'
    },
    websockets: {
      isDisabled: true // TODO: active once back-end will be ready for this one
    },
    input: {
      placeholder: 'Type item\'s name or ID...'
    },
    tabs: {
      activeTab: 'all',
      categories: ['friends', 'faction', 'company', 'all']
    },
    dropdown: {
      hideByClick: false,
      disabledList: []
    }
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      isFirstSearchDone: false,
      isFirstSearch: false,
      isFetchActive: false,
      isLiveSearch: false,
      isForceSearch: false,
      isManualSearch: false,
      isFetchError: false,
      isDropDownAppear: false,
      isHistoryAppear: false,
      inputValue: '',
      currentTab: 'all',
      itemsList: null,
      activeRole: '',
      isBadSymbol: false,
      isMaxLength: false,
      cachedRequests: [],
      alreadySelectedList: [],
      lastsSearches: []
    }
  }

  componentDidMount() {
    this._setMountFlag(true)
    this._setInitData()
    this._loadLastSearches()
    this._runWebsockets()
    this._initDropDownOnMount() // in case we need to appear dropDown on the mount

    document.body.addEventListener('click', this._handleDropDownCancel)
  }

  componentWillUnmount() {
    this._setMountFlag(false)

    document.body.removeEventListener('click', this._handleDropDownCancel)
  }

  _runWebsockets = () => {
    const { websockets } = this.props
    const { isDisabled, channelID } = websockets || {}

    if (isDisabled) {
      return
    }

    initWS({ callback: this._updateItems, channelID })
  }

  _setMountFlag = (status: boolean) => {
    this._isMounted = status
  }

  _setInitData = () => {
    const {
      tabs: { categories, activeTab }
    } = this._getContext()

    this.setState({
      currentTab: activeTab || (categories && categories[categories.length - 1]) || ALL_LISTS_TAB_LABEL
    })
  }

  _setItemsToLocalStorage = (itemsData: IItemsList) => {
    const { localStorage } = this.props
    const { isDisabled } = localStorage || {}

    if (isDisabled) {
      return
    }

    addStore({ storeKey: this._getLocalStoreKey(), storeData: itemsData })
  }

  _setItemListsFromLocalStorage = () => {
    const { localStorage } = this.props
    const { isDisabled } = localStorage || {}

    if (isDisabled) {
      return
    }

    this.setState({
      isFirstSearchDone: true,
      itemsList: this._getItemsFromLocalStorage()
    })
  }

  _setItems = ({ isInit, fetchStatus, itemsList }: ISetItems) => {
    // preventing memory leak if component unmounted before async request will finish
    if (!this._getUnmountedFlag()) {
      return
    }

    this.setState(
      prevState =>
        ({
          itemsList,
          isFirstSearchDone: isInit ? true : prevState.isFirstSearchDone,
          isFetchError: false,
          isFetchActive: false,
          [fetchStatus]: false
        } as any)
    ) // fix for easily state updating buy its particular key
  }

  _setShowDropDown = (status: boolean) => {
    const { fullLayout } = this.props

    this.setState({
      isDropDownAppear: fullLayout || status
    })
  }

  _setCacheRequest = () => {
    const { currentTab, inputValue } = this.state

    const isCacheInstalled = (cachedRequests: ICachedRequest[]) => cachedRequests.some(({ tab }) => tab === currentTab)

    const initCache = (currentCaches: ICachedRequest[]) => [
      ...currentCaches,
      {
        tab: currentTab,
        value: inputValue
      }
    ]

    const updateCache = (cachedRequests: ICachedRequest[]) =>
      cachedRequests.map((cache: ICachedRequest) =>
        cache.tab === currentTab
          ? {
              ...cache,
              value: inputValue
            }
          : cache
      )

    this.setState(({ cachedRequests }: IState) => ({
      cachedRequests: !isCacheInstalled(cachedRequests) ? initCache(cachedRequests) : updateCache(cachedRequests)
    }))
  }

  _setLastSearches = () => {
    const { inputValue, lastsSearches } = this.state

    if (!inputValue) {
      return
    }

    const localStoreKey = this._getLastsSearchesStoreKey()

    const prevSearches = (localStoreKey && getStore({ storeKey: localStoreKey })) || lastsSearches || []
    const isAlreadyIn = prevSearches.some((last: ILasts) => last.name === inputValue)
    const newLastSearches = isAlreadyIn ? prevSearches : [{ name: inputValue }, ...prevSearches]

    this.setState({
      lastsSearches: newLastSearches
    })

    localStoreKey && addStore({ storeKey: localStoreKey, storeData: newLastSearches })
  }

  // ========================= FETCH ======================== //

  _fetchItems = async ({ isInit, isSilent, isDisabled, fetchStatus }: IFetchData) => {
    // preventing memory leak if component unmounted before async request will finish
    if (!this._getUnmountedFlag()) {
      return
    }

    try {
      this._activeFetchStatus({ fetchStatus, isDisabled, isSilent })

      const payload = await fetch(this._getFetchParams({ useValue: !isInit }))
      const json = await payload.json()
      const normalizedItemsList = this._normalizeItemsData(json)

      this._setItemsToLocalStorage(normalizedItemsList)
      this._setItems({ fetchStatus, itemsList: normalizedItemsList, isInit })
      this._setCacheRequest()
      this._setLastSearches()
    } catch (error) {
      this._alertFetchError({ isSilent, fetchStatus, isInit })

      const repeatFetch = () => {
        const { isFirstSearchDone } = this.state

        if (isInit) {
          this._initTimerFetchID = setTimeout(() => {
            if (isFirstSearchDone) {
              clearInterval(this._initTimerFetchID)
              return
            }

            this._fetchItems({ isInit, isDisabled, fetchStatus, isSilent: true })
          }, 1000)
        }
      }

      repeatFetch()

      throw new Error(`Some error happen during items data response: ${error}`)
    }
  }

  // ========================= FETCH ======================== //

  _initialItemsLoad = () => {
    const { isFirstSearchDone } = this.state
    const {
      localStorage: { isDisabled: isLocalStorageDisabled }
    } = this.props

    if (isFirstSearchDone) {
      return
    }

    this._setItemListsFromLocalStorage()

    const itemsListLocalStorage = this._getItemsFromLocalStorage()
    const isDisabled = !isLocalStorageDisabled && itemsListLocalStorage && Object.keys(itemsListLocalStorage).length > 0

    this._fetchItems({ fetchStatus: 'isFirstSearch', isDisabled, isInit: true })
  }

  _forceItemsSearch = (newTabClicked: TCurrentTab) => {
    const isRequestCached = this._getCacheRequest()

    // if we're in search and no items founded being on some category diverse of 'All',
    // we need to repeat looking once we get on the 'All' tab now
    if (this._isAllTab(newTabClicked) && this._activeSearch() && !this._findItemInCategory() && !isRequestCached) {
      this._fetchItems({ fetchStatus: 'isForceSearch' })
    }
  }

  _liveItemSearch = () => {
    const { inputValue } = this.state
    const itemsToRender = this._getItemsToRender()

    const isItemFoundedInCache = itemsToRender && itemsToRender.length > 0
    const isNoDataToSearch = !inputValue || inputValue.length === 0
    const isFetchActive = this._isFetchActive()
    const isNotAllTab = !this._isAllTab() && this._isWholeListNeed()

    if (isFetchActive || isNoDataToSearch || isNotAllTab) {
      return
    }

    this._fetchItems({ fetchStatus: isItemFoundedInCache ? 'isManualSearch' : 'isLiveSearch' })
  }

  _manualItemSearch = async () => {
    await this._fetchItems({ fetchStatus: 'isManualSearch' })

    if (!this._isAllTab() && this._isWholeListNeed()) {
      await this.setState({
        currentTab: ALL_LISTS_TAB_LABEL
      })
    }
  }

  _activeSearch = () => {
    const { inputValue } = this.state

    const isActiveSearch = !!inputValue && !this._isFetchActive()

    return isActiveSearch
  }

  _activeFetchStatus = ({ fetchStatus, isSilent, isDisabled }: IFetch) => {
    if (isSilent || isDisabled) {
      return
    }

    this.setState({
      isFetchActive: true,
      [fetchStatus]: true
    } as any) // fix for easily state updating buy its particular key
  }

  _alertFetchError = ({ isSilent, isInit, fetchStatus }: IFetch) => {
    if (isSilent) {
      return
    }

    this.setState(
      prevState =>
        ({
          isFetchError: true,
          isFirstSearchDone: isInit ? false : prevState.isFirstSearchDone,
          isFetchActive: false,
          [fetchStatus]: false
        } as any)
    ) // fix for easily state updating buy its particular key

    setTimeout(() => {
      this.setState({
        isFetchError: false
      })
    }, 1500)
  }

  _updateItems = (newItems: IUserRaw[]) => {
    const normalizedItems = this._normalizeItemsData(newItems)

    this.setState({
      itemsList: normalizedItems
    })

    this._setItemsToLocalStorage(normalizedItems)
  }

  _getFetchParams = ({ useValue }: { useValue?: boolean }) => {
    const { inputValue } = this.state
    const { fetchRoot } = this.props

    const rootParam = fetchRoot || `${ROOT_FETCH_URL}?${NEW_SEARCH_POSTFIX}`

    return `${rootParam}${useValue ? addManualSearch(inputValue) : ''}`
  }

  _getUnmountedFlag = () => this._isMounted

  _getLocalStoreKey = () => {
    const { localStorage } = this.props
    const { storeKey } = localStorage || {}

    return `${storeKey || LOCAL_STORE_KEY}_${getUserID()}`
  }

  _getItemsFromLocalStorage = () => {
    const { localStorage } = this.props
    const { isDisabled } = localStorage || {}

    if (isDisabled) {
      return null
    }

    return getStore({ storeKey: this._getLocalStoreKey() })
  }

  _getAllItems = () => {
    const { itemsList } = this.state

    if (!itemsList) {
      return null
    }

    // we need reverse items lists here to put all new items
    // income to be appeared on top of the present lists.
    const newLists = Object.values(reversItems(itemsList))

    // @ts-ignore
    return uniqBy(newLists.flat(), 'ID')
  }

  _getCategoriesTabs = () => {
    const { currentTab } = this.state
    const {
      tabs: { categories }
    } = this._getContext()

    if (!categories || categories.length === 0) {
      return [currentTab]
    }

    return categories
  }

  _getItemLists = () => {
    const { itemsList } = this.state
    const tabsToSort = this._getCategoriesTabs()

    const itemLists = {}

    tabsToSort.forEach(tabType => {
      const prevListState = (itemsList && itemsList[tabType]) || []

      itemLists[tabType] = [...prevListState]
    })

    return itemLists
  }

  _getCurrentItemsList = () => {
    const {
      tabs: { categories }
    } = this._getContext()
    const { itemsList, currentTab } = this.state

    if (!currentTab) {
      console.error('No tab label was found. Please check your store data: ', this.state)

      return null
    }

    if (!itemsList || Object.keys(itemsList).length === 0) {
      return null
    }

    if (isAllItemsRender(categories, currentTab)) {
      return this._getAllItems()
    }

    return itemsList[currentTab]
  }

  _getItemsToRender = () => {
    const { inputValue } = this.state

    const itemsListToRender = this._getCurrentItemsList()

    if (!itemsListToRender || itemsListToRender.length === 0) {
      return null
    }

    if (itemsListToRender && inputValue) {
      return this._filterInviteItems(itemsListToRender, inputValue)
    }

    return itemsListToRender
  }

  _getMember = (target: EventTarget) => {
    const { activeRole } = this.state
    const data: IItemData = getTargetData(target)

    if (activeRole) {
      data.role = activeRole
    }

    return data
  }

  _getContext = () => {
    const { systemData, input, placeholders, dropdown, tooltips, tabs, buttons, enhancers, lasts } = this.props

    const contextConfig: IContext = {
      systemData: systemData || {},
      input: input || {},
      tabs: tabs || {},
      lasts: lasts || {},
      dropdown: dropdown || {},
      placeholders: placeholders || {},
      tooltips: tooltips || {},
      buttons: buttons || {},
      enhancers: enhancers || {}
    }

    return contextConfig
  }

  _getLastsSearchesStoreKey = () => {
    const { localStorage: { isDisabled, storeKey } = {}, lasts: { storeLastsKey } = {} } = this.props

    if (isDisabled) {
      return undefined
    }

    return `${storeKey || LOCAL_STORE_KEY}_${storeLastsKey || LASTS_STORE_KEY}_${getUserID()}`
  }

  _getCacheRequest = () => {
    const { cachedRequests, currentTab, inputValue } = this.state

    return cachedRequests.find(({ tab, value }: ICachedRequest) => tab === currentTab && value === inputValue)
  }

  _classNamesHolder = () => {
    const { customStyles, systemData } = this.props
    const { wrap, topContainer, bottomContainer } = customStyles || {}
    const { stylePreset } = systemData || {}

    const containerClass = classnames({
      [styles.selectSearchWrap]: true,
      [wrap]: wrap,
      [`${stylePreset}SelectSearchWrap`]: stylePreset
    })

    const topSectionClass = classnames({
      [styles.topContainer]: true,
      [topContainer]: topContainer
    })

    const bottomSectionClass = classnames({
      [styles.bottomContainer]: true,
      [bottomContainer]: bottomContainer
    })

    return {
      containerClass,
      topSectionClass,
      bottomSectionClass
    }
  }

  _findItemInCategory = () => {
    const itemsToRender = this._getItemsToRender()
    const isItemFoundedInCache = itemsToRender && itemsToRender.length > 0

    return isItemFoundedInCache
  }

  _filterInviteItems = (itemsListToRender: ICategory[], inputValue: string) => {
    const secureRegExpInputValue = securedRegExpString(inputValue)
    const matchTrigger = new RegExp(secureRegExpInputValue, 'ig')

    const inviteItemsListPreFiltered = itemsListToRender.filter(item => {
      const { ID, name } = item

      return ID.toString().match(matchTrigger) || name.match(matchTrigger)
    })

    return inviteItemsListPreFiltered
  }

  _checkIsItemAlreadyAdded = ({ itemMatchValue }: IMatch) => {
    const { itemsList, isFirstSearch } = this.state

    if (isFirstSearch || !itemsList || Object.keys(itemsList).length === 0) {
      return false
    }

    const isItemAlreadyAdded = this._getAllItems().some((itemFromStore: ICategory) => {
      return itemFromStore.ID === Number(itemMatchValue) || itemFromStore.name === itemMatchValue
    })

    return isItemAlreadyAdded
  }

  _normalizeItemsData = (itemList: IUserRaw[] | IUserRawWebsocket[]) => {
    const normalizedItemList: IItemsList = this._getItemLists()

    if (!itemList) {
      return normalizedItemList
    }

    const removeItemFromTheList = (item: IUserRawWebsocket) => {
      normalizedItemList[item.type] = [
        ...normalizedItemList[item.type].filter((itemFromList: ICategory) => itemFromList.ID === Number(item.id))
      ]
    }

    const addItemToList = (itemData: ICategory) => {
      const itemHasOwnCategory = !!normalizedItemList[itemData.category]

      if (itemHasOwnCategory) {
        normalizedItemList[itemData.category].push(itemData)

        return
      }

      // if we don't need to render all the lists together
      if (!normalizedItemList.all) {
        return
      }

      normalizedItemList.all.push(itemData)
    }

    itemList.forEach((item: IUserRawWebsocket & IUserRaw) => {
      const preprocessedItem = normalizedItem(item)

      if (!item.type) {
        console.error('Some item was missed during itemsList normalization: ', item)

        return
      }

      if (item.isDelete) {
        removeItemFromTheList(item)

        return
      }

      if (this._checkIsItemAlreadyAdded({ itemMatchValue: item.id })) {
        return
      }

      addItemToList(preprocessedItem)
    })

    return normalizedItemList
  }

  _initDropDownOnMount = () => {
    const { fullLayout } = this.props

    if (!fullLayout) {
      return
    }

    this._handleDropDownStart()
    // this._initialItemsLoad()
  }

  _isAllTab = (selectedTab: TCurrentTab = undefined) => {
    const { currentTab } = this.state
    const tabLabel = selectedTab || currentTab

    return tabLabel === ALL_LISTS_TAB_LABEL
  }

  _isWholeListNeed = () => {
    const {
      tabs: { categories }
    } = this._getContext()

    return categories.some(tab => tab === ALL_LISTS_TAB_LABEL)
  }

  _isFetchActive = () => {
    const { isFetchError, isFirstSearch, isForceSearch, isManualSearch, isLiveSearch } = this.state

    return !isFetchError && (isFirstSearch || isForceSearch || isManualSearch || isLiveSearch)
  }

  _loadLastSearches = () => {
    const storeKey = this._getLastsSearchesStoreKey()

    if (!storeKey) {
      return
    }

    const lastsSearches = getStore({ storeKey }) || []

    this.setState({
      lastsSearches
    })
  }

  _handleChange = (value: string) => {
    const { inputValue } = this.state

    if (value === inputValue) {
      return
    }

    this.setState({
      inputValue: value
    })
  }

  _handleCheck = ({ errorType }: { errorType?: 'isBadSymbol' | 'isMaxLength' } = {}) => {
    if (errorType) {
      this.setState({
        [errorType]: true
      } as any)

      return
    }

    this.setState({
      isMaxLength: false,
      isBadSymbol: false
    })
  }

  _handleStart = (event: React.KeyboardEvent<HTMLInputElement>) => {
    event.preventDefault()

    const { inputValue } = this.state

    if (!inputValue || this._isFetchActive()) {
      return
    }

    this._manualItemSearch()
  }

  _handleCancel = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    event.preventDefault()

    this.setState({
      inputValue: ''
    })
  }

  _handleHistory = () => {
    this.setState(
      (prevState: IState) =>
        ({
          isHistoryAppear: !prevState.isHistoryAppear
        } as any)
    )
  }

  _handleDeleteHistory = (name: string) => {
    const { lastsSearches } = this.state
    const storeKey = this._getLastsSearchesStoreKey()

    const prevSearches = (storeKey && getStore({ storeKey })) || lastsSearches || []
    const newSearchLists = [...prevSearches.filter((item: ILasts) => item.name !== name)]

    this.setState({
      lastsSearches: newSearchLists
    })

    storeKey && addStore({ storeKey, storeData: newSearchLists })
  }

  _handleDropDownStart = () => {
    const { isDropDownAppear } = this.state

    if (isDropDownAppear) {
      return
    }

    this._setShowDropDown(true)
    this._initialItemsLoad()
  }

  _handleDropDownCancel = (event: Event) => {
    const { target } = event

    const isItemOutsideItemsSearch = !(target as HTMLElement).closest('div[class^="selectSearchWrap"]')

    if (isItemOutsideItemsSearch) {
      this._setShowDropDown(false)
    }
  }

  _handleRoleToggle = (activeRole: string) => {
    this.setState({
      activeRole
    })
  }

  _handleSave = (target: EventTarget) => {
    const { alreadySelectedList } = this.state
    const { saveOnClick } = this.props
    const {
      dropdown: { hideByClick, disabledList },
      input: { clearInputOnClick }
    } = this._getContext()

    if (!target) {
      return
    }

    const member = this._getMember(target)
    const isAlreadySelected = alreadySelectedItem({ membersList: alreadySelectedList, disabledList, ID: member.ID })

    if (isAlreadySelected) {
      return
    }

    this.setState(prevState => ({
      alreadySelectedList: [...prevState.alreadySelectedList, member.ID],
      inputValue: clearInputOnClick ? '' : prevState.inputValue
    }))

    saveOnClick && saveOnClick(member)
    !hideByClick && this._setShowDropDown(false)
  }

  _handleChooseMembersTab = async (newTabClicked: TCurrentTab) => {
    await this.setState({
      currentTab: newTabClicked
    })

    await this._forceItemsSearch(newTabClicked)
  }

  _renderTooltips = () => {
    const { isFetchError, isMaxLength, isBadSymbol } = this.state
    const { useGlobalTooltips } = this.props

    return (
      <React.Fragment>
        {!useGlobalTooltips && <TooltipsGlobal />}
        <Tooltips isBadSymbol={isBadSymbol} isMaxLength={isMaxLength} isFetchError={isFetchError} />
      </React.Fragment>
    )
  }

  _renderInput = () => {
    const {
      lastsSearches,
      alreadySelectedList,
      inputValue,
      isLiveSearch,
      isManualSearch,
      isDropDownAppear,
      isHistoryAppear,
      isFetchError,
      isMaxLength,
      isBadSymbol
    } = this.state
    const { fullLayout } = this.props

    return (
      <Input
        value={inputValue}
        membersList={alreadySelectedList}
        inviteItemsList={this._getItemsToRender()}
        isFetchActive={isLiveSearch || isManualSearch}
        isFetchError={isFetchError}
        isBadSymbol={isBadSymbol}
        isMaxLength={isMaxLength}
        isDropDownActive={isDropDownAppear || fullLayout}
        isHistoryAppear={isHistoryAppear}
        lastsSearches={lastsSearches}
        onClick={this._handleDropDownStart}
        onClear={this._handleCancel}
        onSearch={this._handleStart}
        onChange={this._handleChange}
        onCheck={this._handleCheck}
        onSave={this._handleSave}
        onToggle={this._handleRoleToggle}
        onDebounce={this._liveItemSearch}
        onHistory={this._handleHistory}
      />
    )
  }

  _renderTabs = () => {
    const { currentTab } = this.state

    return <Tabs currentTab={currentTab} onChooseTab={this._handleChooseMembersTab} />
  }

  _renderLasts = () => {
    const { isHistoryAppear, lastsSearches } = this.state

    return (
      <Lasts
        lastsSearches={lastsSearches}
        isHistoryAppear={isHistoryAppear}
        storeLastsKey={this._getLastsSearchesStoreKey()}
        onClick={this._handleChange}
        onDelete={this._handleDeleteHistory}
      />
    )
  }

  _renderDropDown = () => {
    const { alreadySelectedList, isFirstSearch, isForceSearch, isManualSearch, isLiveSearch } = this.state

    return (
      <DropDown
        isForceSearch={isForceSearch}
        isInitLoad={isFirstSearch}
        isLiveSearch={isLiveSearch}
        isManualSearch={isManualSearch}
        isActiveSearch={this._activeSearch()}
        isAllItemsTab={this._isAllTab()}
        inviteItemsList={this._getItemsToRender()}
        membersList={alreadySelectedList}
        saveMember={this._handleSave}
      />
    )
  }

  _renderInputBlock = () => {
    const { topSectionClass } = this._classNamesHolder()

    return (
      <div className={topSectionClass}>
        {this._renderTooltips()}
        {this._renderInput()}
      </div>
    )
  }

  _renderDropDownBlock = () => {
    const { isDropDownAppear } = this.state
    const { fullLayout } = this.props
    const { bottomSectionClass } = this._classNamesHolder()

    if (!isDropDownAppear && !fullLayout) {
      return null
    }

    return (
      <div className={bottomSectionClass}>
        {this._renderTabs()}
        {this._renderLasts()}
        {this._renderDropDown()}
      </div>
    )
  }

  render() {
    const { isDisabled } = this.props
    const { containerClass } = this._classNamesHolder()

    if (isDisabled) {
      return null
    }

    return (
      <NestedContext.Provider value={this._getContext()}>
        <div className={containerClass}>
          {this._renderInputBlock()}
          {this._renderDropDownBlock()}
        </div>
      </NestedContext.Provider>
    )
  }
}

export default SelectSearch
