import React from 'react'
import { mount } from 'enzyme'

import SelectSearch from '..'

import initialState, { usersState, fullLayout } from './mocks'

describe('Controller', () => {
  it('should return basic controller (only input block) layout', () => {
    const Component = mount(<SelectSearch {...initialState} />)

    expect(Component.find('SelectSearch').length).toBe(1)
    expect(Component.find('Tooltips').length).toBe(1)
    expect(Component.find('Input').length).toBe(1)
    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('History').length).toBe(1)
    expect(Component.find('Commit').length).toBe(1)
    expect(Component.find('Tabs').length).toBe(0)
    expect(Component.find('Placeholder').length).toBe(0)
    expect(Component.find('DropDownList').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should return full controller layout with placeholder', () => {
    const Component = mount(<SelectSearch {...fullLayout} />)

    expect(Component.find('SelectSearch').length).toBe(1)
    expect(Component.find('Tooltips').length).toBe(1)
    expect(Component.find('Input').length).toBe(1)
    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('History').length).toBe(1)
    expect(Component.find('Commit').length).toBe(1)
    expect(Component.find('Tabs').length).toBe(1)
    expect(Component.find('Placeholder').length).toBe(1)
    expect(Component.find('DropDownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should return full controller layout with dropDown', () => {
    Object.defineProperty(window.document, 'cookie', {
      writable: true,
      value: 'uid=2114355'
    })

    localStorage.setItem('user_fellows_2114355', JSON.stringify(usersState))

    const Component = mount(<SelectSearch {...fullLayout} />)

    expect(Component.find('SelectSearch').length).toBe(1)
    expect(Component.find('Tooltips').length).toBe(1)
    expect(Component.find('Input').length).toBe(1)
    expect(Component.find('Clear').length).toBe(1)
    expect(Component.find('History').length).toBe(1)
    expect(Component.find('Commit').length).toBe(1)
    expect(Component.find('Tabs').length).toBe(1)
    expect(Component.find('Placeholder').length).toBe(0)
    expect(Component.find('DropDownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  // TODO: add more tests one websockets and localStorage features will be standardized on the backend.
})
