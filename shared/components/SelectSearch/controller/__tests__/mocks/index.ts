import { DEFAULT_CONFIG } from '../../../mocks/storybook'

const initialState = {
  ...DEFAULT_CONFIG
}

const fullLayout = {
  ...initialState,
  fullLayout: true
}

const usersState = {
  all: [],
  faction: [{ ID: 1, category: 'faction', status: 'online', name: 'Test_User' }],
  friends: [],
  company: []
}

export default initialState
export { fullLayout, usersState }
