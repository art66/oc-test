import React from 'react'
import { text, number } from '@storybook/addon-knobs'

import AccuracyBonus from './index'
import { DEFAULT_STATE } from './mocks'

export const Defaults = () => {
  const config = {
    ...DEFAULT_STATE,
    value: text('Value', '41.90')
  }

  return <AccuracyBonus {...config} />
}

Defaults.story = {
  name: 'defaults'
}

export const ValueAsNumber = () => {
  const config = {
    ...DEFAULT_STATE,
    value: number('Value', 41.9)
  }

  return <AccuracyBonus {...config} />
}

ValueAsNumber.story = {
  name: 'value as number'
}

export const WithoutValue = () => {
  const config = {
    ...DEFAULT_STATE
  }

  return <AccuracyBonus {...config} />
}

WithoutValue.story = {
  name: 'without value'
}

export default {
  title: 'Shared/Item/AccuracyBonus'
}
