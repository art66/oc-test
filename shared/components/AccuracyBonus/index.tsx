import React, { useMemo } from 'react'
import cn from 'classnames'
import { TProps } from './types'
import styles from './index.cssmodule.scss'

export const AccuracyBonus: React.SFC<TProps> = props => {
  const { className: extraClassName, iconClassName: extraIconClassName, value } = props

  const className = useMemo(() => cn(styles.accuracyBonus, extraClassName), [extraClassName])
  const iconClassName = useMemo(() => cn(styles.accuracyBonusIcon, extraIconClassName), [extraIconClassName])
  const normalizedValue = useMemo(() => value || 'N/A', [value])

  return (
    <span className={className}>
      <i className={iconClassName} />
      {normalizedValue}
    </span>
  )
}

export default AccuracyBonus
