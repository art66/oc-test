# Global AccuracyBonus Component

### The Component for render the accuracy bonus value with it icon.

## Schema:

### Props: Manual props injection for Component work:

- **value**. Accepts one of a "string" or a "number" parameter::

  --- `value: "40.95" or 40.94`. Added the class name to the root element.

  _Example_:
  `const Value = '40.94'`

- **className**. Accepts only a "string" parameter:

  --- `className: "accuracyBonusClassName"`. Added the class name to the root element.

  _Example_:
  `const CLASS_NAME = 'accuracyBonusClassName'`

- **iconClassName**. Accepts only a "string" parameter:

  --- `iconClassName: "accuracyBonusIconClassName"`. Added the class name to the icon element in the component.

  _Example_:
  `const ICON_CLASS_NAME = 'accuracyBonusIconClassName'`

## How to use in App:

### So, the final Schema (_including all discussed above_) will have the next look:

```
  <AccuracyBonus
    value={VALUE}
    className={CLASS_NAME}
    iconClassName={ICON_CLASS_NAME}
  />
```
