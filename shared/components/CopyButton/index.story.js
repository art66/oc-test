import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'
import { withCustomStyles } from '../../../.storybook/addons'
import CopyButton, { CopyLinkButton } from './'
import { TOOLTIP_CONFIG, ICON_CONFIG } from './mocks'

export const Default = () => {
  return (
    <CopyButton
      value='Test text'
      tooltip={TOOLTIP_CONFIG}
      title='Copy test'
      titleAfterAction='Text copied'
      icon={ICON_CONFIG}
    />
  )
}

Default.story = {
  name: 'default'
}

export const CopyLink = () => {
  return (
    <CopyLinkButton
      path='/test/path'
      tooltip={TOOLTIP_CONFIG}
      title='Copy link'
      titleAfterAction='Link copied'
      icon={ICON_CONFIG}
    />
  )
}

CopyLink.story = {
  name: 'copy link'
}

export default {
  title: 'Shared/CopyButton',
  decorators: [withCustomStyles('max-width: 1200px; padding: 0 15px;'), withKnobs]
}
