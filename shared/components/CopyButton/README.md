
# Global CopyButton Component

### This component created for copying links.



## Schema:

### Props: Manual props injection for Component work:

#### <CopyButton /> component:

* **value**: `string` - value to copy.

  *Example*:

  ```value="Test text"``` - will copy "Test text" string.

* **id**: `string` = `path` - tooltip id.

  *Example*:

  ```id={`${match.alias}-${match.ID}`}``` - unique id for the tooltip component.

* **title**?: `string` = "Copy link" - aria label text.

  *Example*:

  ```title={`Copy ${match.name} match  link`}``` - set up specified aria label.

* **titleAfterAction**?: `string` = "Link copied to clipboard" -  aria label text after action.

  *Example*:

  ```titleAfterAction={`${match.name} match link copied to clipboard`}``` - set up specified aria label after action.

* **tabIndex**?: `number` = 1 -  tab index.

  *Example*:

  ```tabIndex={1}``` - set up specified tab index.

* **styles**?:

    ```
      {
        container?: string
        input?: string
        button?: string
        icon?: string
      }
    ```
   -  addition class names with special styles for each element.

  *Example*:

  ```styles={{ container: styles.copyLinkButton }}``` - set up an additional styles for the container element.

* **icon**?: `ISVGIconGeneratorProps` -  SVG icon generator component props.

  *Example*:

  ```icon={{ iconName: 'Copy' }}``` - extends the default icon setting.

* **tooltip**?: `ITooltipProps` -  tooltip component props.

  *Example*:

  ```tooltip={{ manualCoodsFix: { fixX: -15, fixY: -20 } }}``` - extends the default tooltip setting.


#### <CopyLinkButton /> component:

* **path**?: `string` - link path.

  *Example*:

  ```path={`#/${match.alias}/${match.ID}`}``` - It will create `**host**/#/football/3118159` string.

* **url**?: `string` - a full link.

  *Example*:

  ```url="**host**/#/football/3118159"``` - the full link string to copy.

It will take **path** or **url** as the string to copy.




## How to use in App:

### So, the final Schema (*including all discussed above*) will have the next look:

  *Example*:

  ```
    <CopyButton
      id='578907'
      value='Test text'
      title={`Copy ${value} text`}
      titleAfterAction={`${value} text copied to clipboard`}
      tabIndex={1}
      styles={{
        container: styles.copyButtonContainer,
        input: styles.copyButtonInput,
        button: styles.copyButtonButton,
        icon: styles.copyButtonIcon
      }}
      tooltip={
        position: { x: 'left', y: 'center' },
        manualCoodsFix: { fixX: -15, fixY: -20 }
      }
      icon={{ iconName: 'CopyIcon' }}
    />

    <CopyLinkButton
      id={match.ID}
      path={`#/${match.alias}/${match.ID}`}
      title={`Copy ${match.name} match link`}
      titleAfterAction={`${match.name} match link copied to clipboard`}
      tabIndex={1}
      styles={{
        container: styles.copyButtonContainer,
        input: styles.copyButtonInput,
        button: styles.copyButtonButton,
        icon: styles.copyButtonIcon
      }}
      tooltip={
        position: { x: 'left', y: 'center' },
        manualCoodsFix: { fixX: -15, fixY: -20 }
      }
      icon={{ iconName: 'CopyIcon' }}
    />
  ```
