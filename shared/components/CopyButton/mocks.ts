export const TOOLTIP_CONFIG = {
  position: { x: 'left', y: 'center' },
  manualCoodsFix: { fixX: -15, fixY: -20 }
}

export const ICON_CONFIG = {
  fill: {
    color: '#666',
    strokeWidth: '0'
  },
  onHover: {
    active: true,
    fill: {
      color: '#999',
      strokeWidth: '0'
    }
  }
}
