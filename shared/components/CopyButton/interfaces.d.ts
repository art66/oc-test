import { IProps as ISVGIconGeneratorProps } from '@torn/shared/SVG/types/icons'
import { IProps as ITooltipProps } from '@torn/shared/components/TooltipNew/types'

export interface IProps {
  value: string
  id: string
  title?: string
  titleAfterAction?: string
  tabIndex?: number
  styles?: {
    container?: string
    input?: string
    button?: string
    icon?: string
  }
  icon?: ISVGIconGeneratorProps
  tooltip?: ITooltipProps
}

export interface ICopyLinkProps
  extends Pick<IProps, 'id' | 'title' | 'titleAfterAction' | 'tabIndex' | 'styles' | 'icon' | 'tooltip'> {
  url?: string
  path?: string
}

export interface TTooltipHookProps extends ITooltipProps {
  elementId: string
  copied: boolean
  title?: IProps['title']
  titleAfterAction?: IProps['titleAfterAction']
  setCopied: (copied: boolean) => void
}
