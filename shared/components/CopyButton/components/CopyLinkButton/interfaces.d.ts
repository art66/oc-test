import { IProps as ICopyButtonProps } from '../CopyButton/interfaces'

export interface IProps
  extends Pick<ICopyButtonProps, 'id' | 'title' | 'titleAfterAction' | 'tabIndex' | 'styles' | 'icon' | 'tooltip'> {
  url?: string
  path?: string
}
