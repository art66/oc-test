import React from 'react'
import getCurrentPageURL from '@torn/shared/utils/getCurrentPageURL'
import { LINK_ICON } from '@torn/shared/SVG/presets/icons/global'
import { TOOLTIP_COPY_TITLE, TOOLTIP_COPIED_TITLE } from '../../constants'
import CopyButton from '../CopyButton'
import { ICopyLinkProps } from '../../interfaces'

export default function CopyLinkButton(props: ICopyLinkProps) {
  const { url: urlProp, path, title, titleAfterAction, icon, ...extendProps } = props

  const url = urlProp || `${getCurrentPageURL({ isOriginOnly: true })}/${path}`

  return (
    <CopyButton
      value={url}
      title={title || TOOLTIP_COPY_TITLE}
      titleAfterAction={titleAfterAction || TOOLTIP_COPIED_TITLE}
      icon={{ ...LINK_ICON, ...icon }}
      {...extendProps}
    />
  )
}
