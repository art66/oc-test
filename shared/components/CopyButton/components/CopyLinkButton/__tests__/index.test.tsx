import React from 'react'
import { render } from '@testing-library/react'
import CopyLinkButton from '..'
import { defaultProps, withPathProps, withUrlProps } from './mocks'

describe('<CopyLinkButton />', () => {
  it('should render button and match snapshot', () => {
    const { container } = render(<CopyLinkButton {...defaultProps} />)

    expect(container.firstChild).toMatchSnapshot()
  })

  it('should render button with path and match snapshot', () => {
    const { container } = render(<CopyLinkButton {...withPathProps} />)

    expect(container.firstChild).toMatchSnapshot()
  })

  it('should render button with url and match snapshot', () => {
    const { container } = render(<CopyLinkButton {...withUrlProps} />)

    expect(container.firstChild).toMatchSnapshot()
  })
})
