import { ICopyLinkProps } from '../../../interfaces'

export const defaultProps: ICopyLinkProps = {
  id: '234234',
  path: '#/footbal/234234',
  title: 'Copy "Chojniczanka Chojnice v Polkowice" match link',
  titleAfterAction: '"Chojniczanka Chojnice v Polkowice" match link copied to clipboard',
  tabIndex: 1,
  styles: {
    container: '.container',
    input: '.input',
    button: '.button',
    icon: '.icon'
  },
  icon: { iconName: 'Link' },
  tooltip: {
    position: { x: 'left', y: 'center' },
    manualCoodsFix: { fixX: -15, fixY: -20 }
  }
}

export const withPathProps: ICopyLinkProps = {
  id: '234234',
  path: '#/footbal/90676'
}

export const withUrlProps: ICopyLinkProps = {
  id: '234234',
  url: 'https://localhost/#/footbal/90676'
}
