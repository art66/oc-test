import { IProps as ICopyButtonProps } from '../CopyButton/interfaces'
import { IProps as ITooltipProps } from '@torn/shared/components/TooltipNew/types'

export interface IProps extends ITooltipProps {
  elementId: string
  copied: boolean
  title?: ICopyButtonProps['title']
  titleAfterAction?: ICopyButtonProps['titleAfterAction']
  setCopied: (copied: boolean) => void
}
