import React, { useEffect, useMemo } from 'react'
import cn from 'classnames'
import ToolTip, { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { IProps } from './interfaces'
import { TOOLTIP_CONFIG, TOOLTIP_COPY_TITLE, TOOLTIP_COPIED_TITLE } from '../../constants'
import styles from './index.cssmodule.scss'

export default function Tooltip(props: IProps) {
  const { elementId, copied, setCopied, title, titleAfterAction, ...tooltipProps } = props

  const config = useMemo(() => {
    return {
      ...TOOLTIP_CONFIG,
      overrideStyles: {
        ...TOOLTIP_CONFIG.overrideStyles,
        title: cn(styles.title, { [styles.afterAction]: copied }),
        ...tooltipProps.overrideStyles
      },
      actionOnExited() {
        if (copied) {
          setCopied(false)
        }
      },
      ...tooltipProps
    }
  }, [copied, tooltipProps])

  useEffect(() => {
    tooltipsSubscriber.subscribe({
      ID: elementId,
      child: (
        <>
          <p>{titleAfterAction || TOOLTIP_COPIED_TITLE}</p>
          <p>{title || TOOLTIP_COPY_TITLE}</p>
        </>
      )
    })

    return () => {
      tooltipsSubscriber.unsubscribe({
        ID: elementId,
        child: null
      })
    }
  })

  return <ToolTip {...config} />
}
