import React, { useCallback, useRef, useState } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { COPY_ICON } from '@torn/shared/SVG/presets/icons/global'
import { AriaLabelHook } from './hooks'
import Tooltip from '../Tooltip'
import styles from './index.cssmodule.scss'
import { IProps } from '../../interfaces'

export default function CopyButton(props: IProps) {
  const {
    id,
    value,
    styles: stylesProps,
    title: titleProp,
    titleAfterAction,
    tabIndex = 1,
    icon,
    tooltip: tooltipProps
  } = props
  const ref = useRef()

  const elementId = `copyButtonTooltip${id ? `-${id}` : ''}`

  const [copied, setCopied] = useState(false)

  const handlerCopy = useCallback(() => {
    const copyText = ref.current as React.RefObject<HTMLInputElement> & HTMLInputElement

    copyText.select()
    copyText.setSelectionRange(0, 99999)
    document.execCommand('copy')

    setCopied(true)
  }, [ref, setCopied])

  const ariaLabel = AriaLabelHook({ copied, title: titleProp, titleAfterAction })

  return (
    <div className={cn(styles.container, stylesProps?.container)}>
      <input ref={ref} type='text' className={cn(styles.input, stylesProps?.input)} value={value} readOnly={true} />
      <button
        id={elementId}
        className={cn(styles.button, stylesProps?.button)}
        onClick={handlerCopy}
        type='button'
        aria-label={ariaLabel}
        tabIndex={tabIndex}
      >
        <SVGIconGenerator customClass={stylesProps?.icon} {...COPY_ICON} {...icon} />
      </button>
      <Tooltip
        {...tooltipProps}
        elementId={elementId}
        copied={copied}
        setCopied={setCopied}
        title={titleProp}
        titleAfterAction={titleAfterAction}
      />
    </div>
  )
}
