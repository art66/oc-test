import { IProps as ISVGIconGeneratorProps } from '@torn/shared/SVG/types/icons'
import { IProps as ITooltipProps } from '@torn/shared/components/TooltipNew/types'

export interface IProps {
  value: string
  id: string
  title?: string
  titleAfterAction?: string
  tabIndex?: number
  styles?: {
    container?: string
    input?: string
    button?: string
    icon?: string
  }
  icon?: ISVGIconGeneratorProps
  tooltip?: ITooltipProps
}
