import { TOOLTIP_COPY_TITLE, TOOLTIP_COPIED_TITLE } from '../../constants'

export function AriaLabelHook(props) {
  const { copied, title, titleAfterAction } = props

  if (copied) {
    return titleAfterAction || TOOLTIP_COPIED_TITLE
  }

  return title || TOOLTIP_COPY_TITLE
}
