import { IProps } from '../../../interfaces'

export const defaultProps: IProps = {
  id: '234234',
  value: 'Test text',
  title: 'Copy "Test text"',
  titleAfterAction: '"Test text" copied to clipboard',
  tabIndex: 1,
  styles: {
    container: '.container',
    input: '.input',
    button: '.button',
    icon: '.icon'
  },
  icon: { iconName: 'Copy' },
  tooltip: {
    position: { x: 'left', y: 'center' },
    manualCoodsFix: { fixX: -15, fixY: -20 }
  }
}

export const minProps: IProps = {
  id: '234234',
  value: 'T-est extr__a text #)%:@#@!$'
}
