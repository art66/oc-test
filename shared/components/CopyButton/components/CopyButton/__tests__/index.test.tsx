import React from 'react'
import { render } from '@testing-library/react'
import CopyButton from '..'
import { defaultProps, minProps } from './mocks'

describe('<CopyButton />', () => {
  it('should render button and match snapshot', () => {
    const { container } = render(<CopyButton {...defaultProps} />)

    expect(container.firstChild).toMatchSnapshot()
  })

  it('should render button with minimal props and match snapshot', () => {
    const { container } = render(<CopyButton {...minProps} />)

    expect(container.firstChild).toMatchSnapshot()
  })
})
