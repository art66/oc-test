import tooltipStyles from './components/Tooltip/index.cssmodule.scss'

export const TOOLTIP_COPY_TITLE = 'Copy'
export const TOOLTIP_COPIED_TITLE = 'Copied to clipboard'
export const TOOLTIP_COPY_LINK_TITLE = 'Copy link'
export const TOOLTIP_COPIED_LINK_TITLE = 'Link copied to clipboard'

export const TOOLTIP_CONFIG = {
  showTooltip: true,
  addArrow: true,
  position: { x: 'left', y: 'center' },
  manualCoodsFix: { fixX: -1, fixY: -20 },
  overrideStyles: { ...tooltipStyles }
}
