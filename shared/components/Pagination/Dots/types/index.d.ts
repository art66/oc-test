export interface IProps {
  layoutLength: number
  type: string
  slicedArray: number[]
  pageCount: number
}
