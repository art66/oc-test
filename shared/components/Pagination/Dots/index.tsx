import React, { PureComponent } from 'react'

import { LAST_TWO_PAGES, ARRAY_FIRST_ITEM, SECOND_PAGE_ID, ARRAY_LENGTH_FIXER } from '../constants'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

class Dots extends PureComponent<IProps> {
  _dotsMustBeHide = () => {
    const { layoutLength, pageCount } = this.props

    const isDotsMustBeHide = pageCount <= layoutLength

    return isDotsMustBeHide
  }

  _renderDot = () => {
    return (
      <div className={styles.dotsContainer}>
        <span className={styles.dots}>...</span>
      </div>
    )
  }

  _getFirstDot = () => {
    const { slicedArray } = this.props
    const arrayFirstPageIDLessFirstThreePagesID = slicedArray[ARRAY_FIRST_ITEM] <= SECOND_PAGE_ID

    if (arrayFirstPageIDLessFirstThreePagesID) {
      return null
    }

    return this._renderDot()
  }

  _getLastDot = () => {
    const { slicedArray, pageCount } = this.props

    const lastThreePages = pageCount - LAST_TWO_PAGES
    const arrayLastPageIDLessLastThreePagesID = slicedArray[slicedArray.length - ARRAY_LENGTH_FIXER] >= lastThreePages

    if (arrayLastPageIDLessLastThreePagesID) {
      return null
    }

    return this._renderDot()
  }

  _dotsHolder = () => {
    return {
      first: this._getFirstDot(),
      last: this._getLastDot()
    }
  }

  render() {
    if (this._dotsMustBeHide()) {
      return null
    }

    const { type } = this.props

    const dotToRender = this._dotsHolder()[type]

    if (!dotToRender) {
      return null
    }

    return dotToRender
  }
}

export default Dots
