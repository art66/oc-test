import React from 'react'
import { mount } from 'enzyme'
import { firstDotActive, firstDotDisabled, secondDotActive, secondDotDisabled, disabledDots, noDotType } from './mocks'
import Dots from '..'

describe('<Dots test />', () => {
  it('should render first Dot as active', () => {
    const Component = mount(<Dots {...firstDotActive} />)

    expect(Component.prop('type')).toBe('first')
    expect(Component.find('.dotsContainer').length).toBe(1)
    expect(Component.find('.dots').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should not render the first Dot in case of one of the first three pages entered', () => {
    const Component = mount(<Dots {...firstDotDisabled} />)

    expect(Component.prop('type')).toBe('first')
    expect(Component.find('.dotsContainer').length).toBe(0)
    expect(Component.find('.dots').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render last Dot as active', () => {
    const Component = mount(<Dots {...secondDotActive} />)

    expect(Component.prop('type')).toBe('last')
    expect(Component.find('.dotsContainer').length).toBe(1)
    expect(Component.find('.dots').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should not render the last Dot in case of one of the last three pages entered', () => {
    const Component = mount(<Dots {...secondDotDisabled} />)

    expect(Component.prop('type')).toBe('last')
    expect(Component.find('.dotsContainer').length).toBe(0)
    expect(Component.find('.dots').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render all Dots as disabled if pages length is less or equal layout length', () => {
    const Component = mount(<Dots {...disabledDots} />)

    expect(Component.find('.dotsContainer').length).toBe(0)
    expect(Component.find('.dots').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should not render all Dots if type is not filled', () => {
    const Component = mount(<Dots {...noDotType} />)

    expect(Component.find('.dotsContainer').length).toBe(0)
    expect(Component.find('.dots').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
