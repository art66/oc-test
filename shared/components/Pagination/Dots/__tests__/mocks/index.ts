const firstDotActive = {
  layoutLength: 14,
  type: 'first',
  slicedArray: [4, 5, 6, 7, 8, 9, 10, 11, 12],
  pageCount: 15
}

const firstDotDisabled = {
  ...firstDotActive,
  slicedArray: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  layoutLength: 3,
  pageCount: 3
}

const secondDotActive = {
  layoutLength: 14,
  type: 'last',
  slicedArray: [4, 5, 6, 7, 8, 9, 10, 11, 12],
  pageCount: 15
}

const secondDotDisabled = {
  ...secondDotActive,
  layoutLength: 3,
  pageCount: 3
}

const disabledDots = {
  layoutLength: 14,
  type: 'first',
  slicedArray: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  pageCount: 14
}

const noDotType = {
  layoutLength: 14,
  type: '',
  slicedArray: [4, 5, 6, 7, 8, 9, 10, 11, 12],
  pageCount: 14
}

export { firstDotActive, firstDotDisabled, secondDotActive, secondDotDisabled, disabledDots, noDotType }
