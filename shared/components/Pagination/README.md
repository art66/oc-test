
# Pagination
### This Component helps to solve the problem with pagination in Torn's Apps.

 * Schema:
  - pageCount: number - the current count of the page to render.
  - currentPage: number - the current page on which user are now.
  - layoutLength: number - custumizable count of the displayd pagination length.
  - tabsCount: number - custumizable count of the displayd tabs length.
  - setPageNumberAction: (ID: number) => void - action for control page change by clicking tabs
  - changePageNumberAction: (nextPageID: number) => void - action for control page change by clicking arrow

  P.S.
    All props are not reqired at all, so you can custumize the pagination on your own,
