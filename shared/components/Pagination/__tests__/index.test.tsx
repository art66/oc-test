import React, { cloneElement } from 'react'
import { mount } from 'enzyme'
import { HashRouter } from 'react-router-dom'
import {
  emptyPagintaion,
  onePageLayout,
  twoPagesLayout,
  regularLayoutFirstClicked,
  regularLayoutTenClicked,
  regularLayoutNineClicked,
  sevenTabsLayoutAfterArrowClickOnBorderButtonRight,
  tabletLayoutFourteenClicked
} from './mocks'
import Pagination from '..'

const prePreLastTestDesc = `should render Pagination with eight Buttons, with first button clicked.
With Arrows. With left Dots. With first and last buttons`

const preLastTestDesc = `should render Pagination with eight Buttons, with ten button clicked.
With Arrows. With right Dots. With first and last buttons`

const lastTestDesc = `should render Pagination with eight Buttons, with nine button clicked.
With Arrows. With right Dots. With first and last buttons.
And after right Arrwo click should render Pagination with seven Buttons, with ten button clicked.
With Arrows. With two Dots. With first and last buttons`

describe('<Pagination test />', () => {
  it('should render basic empty Pagination', () => {
    const Component = mount(
      <HashRouter>
        <Pagination {...emptyPagintaion} />
      </HashRouter>
    )

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(0)
    expect(Component.find('.dotsContainer').length).toBe(0)
    expect(Component.find('.arrowContainer').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render Pagination with one, clicked Button. Without Arrows and Dots', () => {
    const Component = mount(
      <HashRouter>
        <Pagination {...onePageLayout} />
      </HashRouter>
    )

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(1)
    expect(Component.find('.navButtonActive').length).toBe(1)
    expect(Component.find('.dotsContainer').length).toBe(0)
    expect(Component.find('.arrowContainer').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render Pagination with two Buttons, where one of them is clicked. With Arrows. Without Dots', () => {
    const Component = mount(
      <HashRouter>
        <Pagination {...twoPagesLayout} />
      </HashRouter>
    )

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(2)
    expect(Component.find('.navButtonActive').length).toBe(1)
    expect(Component.find('.dotsContainer').length).toBe(0)
    expect(Component.find('.arrowContainer').length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
  it(prePreLastTestDesc, () => {
    const Component = mount(
      <HashRouter>
        <Pagination {...regularLayoutFirstClicked} />
      </HashRouter>
    )

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(10)
    expect(Component.find('.navButtonActive').prop('data-id')).toBe(1)
    expect(Component.find('.navButtonActive').length).toBe(1)
    expect(Component.find('.dotsContainer').length).toBe(1)
    expect(Component.find('.arrowContainer').length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
  it(preLastTestDesc, () => {
    const Component = mount(
      <HashRouter>
        <Pagination {...regularLayoutTenClicked} />
      </HashRouter>
    )

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(10)
    expect(Component.find('.navButtonActive').prop('data-id')).toBe(10)
    expect(Component.find('.navButtonActive').length).toBe(1)
    expect(Component.find('.dotsContainer').length).toBe(1)
    expect(Component.find('.arrowContainer').length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
  it(lastTestDesc, () => {
    const Component = mount(
      <HashRouter>
        <Pagination {...regularLayoutNineClicked} />
      </HashRouter>
    )

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(10)
    expect(Component.find('.navButtonActive').prop('data-id')).toBe(9)
    expect(Component.find('.navButtonActive').length).toBe(1)
    expect(Component.find('.dotsContainer').length).toBe(1)
    expect(Component.find('.arrowContainer').length).toBe(2)

    expect(Component).toMatchSnapshot()

    Component.find('.arrowButton')
      .at(2)
      .simulate('click')
    Component.find('Pagination')
      .instance()
      .setState(sevenTabsLayoutAfterArrowClickOnBorderButtonRight)
    Component.setProps({
      children: cloneElement(Component.props().children, { currentPage: 10 })
    })
    Component.update()

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(9)
    expect(Component.find('.navButtonActive').prop('data-id')).toBe(10)
    expect(Component.find('.navButtonActive').length).toBe(1)
    expect(Component.find('.dotsContainer').length).toBe(2)
    expect(Component.find('.arrowContainer').length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
  it('should render Pagination with pageLength = 22 and currentPage = 14 with 5 tabs included', () => {
    const Component = mount(
      <HashRouter>
        <Pagination {...tabletLayoutFourteenClicked} />
      </HashRouter>
    )

    expect(Component.find('.paginationContainer').length).toBe(1)
    expect(Component.find('.navContainer').length).toBe(7)
    expect(Component.find('.navButtonActive').prop('data-id')).toBe(14)
    expect(Component.find('.navButtonActive').length).toBe(1)
    expect(Component.find('.dotsContainer').length).toBe(2)
    expect(Component.find('.arrowContainer').length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
})
