const initialState = {
  setPageNumberAction: () => {},
  changePageNumberAction: () => {}
}

const emptyPagintaion = {
  ...initialState,
  pageCount: 0,
  currentPage: 0,
  layoutLength: 0,
  tabsCount: 0
}

const onePageLayout = {
  ...initialState,
  pageCount: 1,
  currentPage: 1,
  layoutLength: 1,
  tabsCount: 1
}

const twoPagesLayout = {
  ...initialState,
  pageCount: 2,
  currentPage: 1,
  layoutLength: 2,
  tabsCount: 0
}

const regularLayoutFirstClicked = {
  ...initialState,
  pageCount: 13,
  currentPage: 1
}

const regularLayoutTenClicked = {
  ...initialState,
  pageCount: 13,
  currentPage: 10
}

const regularLayoutNineClicked = {
  ...initialState,
  pageCount: 13,
  currentPage: 9,
  layoutLength: 11,
  tabsCount: 7
}

const sevenTabsLayoutAfterArrowClickOnBorderButtonRight = {
  currentClick: 'ArrowRight',
  tempArr: [2, 3, 4, 5, 6, 7, 8]
}

const tabletLayoutFourteenClicked = {
  ...initialState,
  pageCount: 22,
  currentPage: 14,
  layoutLength: 9,
  tabsCount: 5
}

export default initialState
export {
  emptyPagintaion,
  onePageLayout,
  twoPagesLayout,
  regularLayoutFirstClicked,
  regularLayoutTenClicked,
  regularLayoutNineClicked,
  sevenTabsLayoutAfterArrowClickOnBorderButtonRight,
  tabletLayoutFourteenClicked
}
