# Pagination - Torn


## 1.5.2
 * Updated tests due to SVGIconGenerator update.

## 1.5.1
 * Add correct StoryBook config.
 * Fixed right layout in mobile and tablet modes.
 * minor improvements.

## 1.4.1
 * Added common Pagination Component tests.
 * Fixed dots layout in count of tabs 1-3.

## 1.3.0
 * Added Arrow Component tests.
 * Improved Button Component tests.

## 1.2.0
 * Added Dots Component tests.

## 1.1.0
 * Added Button Component tests.

## 1.0.0
 * Pagination was separated on independent UI Components: Arrow, Button, Dots, instead of put them all inside the bussiness logic of Pagination Component.
 * Added feature for cusumization count of layout tabs and pagination.
 * Fixed minor bugs.

## 0.0.1
 * Pagination Component was created.
