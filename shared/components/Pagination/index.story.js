import React from 'react'
import { number } from '@storybook/addon-knobs'
import { HashRouter } from 'react-router-dom'
import { State, Store } from '@sambego/storybook-state'

import Pagination from './index'

const store = new Store({
  currentPage: 1
})

export const Defaults = () => {
  const config = {
    layoutLength: number('layoutLength', 11),
    tabsCount: number('tabsCount', 7),
    pageCount: number('pageCount', 20)
  }

  return (
    <HashRouter>
      <State store={store}>
        <Pagination
          {...config}
          setPageNumberAction={(pageID) => store.set({ currentPage: pageID })}
          changePageNumberAction={(pageID) => store.set({ currentPage: pageID })}
        />
      </State>
    </HashRouter>
  )
}

Defaults.story = {
  name: 'defaults'
}

export const Tablet = () => {
  const config = {
    layoutLength: number('layoutLength', 9),
    tabsCount: number('tabsCount', 5),
    pageCount: number('pageCount', 20)
  }

  return (
    <HashRouter>
      <State store={store}>
        <Pagination
          {...config}
          setPageNumberAction={(pageID) => store.set({ currentPage: pageID })}
          changePageNumberAction={(pageID) => store.set({ currentPage: pageID })}
        />
      </State>
    </HashRouter>
  )
}

Tablet.story = {
  name: 'tablet'
}

export const Mobile = () => {
  const config = {
    layoutLength: number('layoutLength', 7),
    tabsCount: number('tabsCount', 3),
    pageCount: number('pageCount', 20)
  }

  return (
    <HashRouter>
      <State store={store}>
        <Pagination
          {...config}
          setPageNumberAction={(pageID) => store.set({ currentPage: pageID })}
          changePageNumberAction={(pageID) => store.set({ currentPage: pageID })}
        />
      </State>
    </HashRouter>
  )
}

Mobile.story = {
  name: 'mobile'
}

export default {
  title: 'Shared/Pagination'
}
