import React, { PureComponent } from 'react'

import Arrow from './Arrow'
import Button from './Button'
import Dots from './Dots'

import { IProps, IState } from './types'
import styles from './index.cssmodule.scss'

import {
  PAGE_ID_FIXER,
  ARRAY_LENGTH_FIXER,
  ARRAY_NO_LENGTH,
  ARRAY_FIRST_ITEM,
  FIRST_DYNAMIC_ITEM_ID,
  SECOND_DYNAMIC_ITEM_ID,
  SECOND_PAGE_ID,
  PRE_LAST_PAGE_ID,
  LAST_TWO_PAGES,
  FROM_SECOND_PAGE,
  FIRST_PAGE,
  POST_FIRST_PAGE,
  POST_POST_FIRST_PAGE,
  PRE_PRE_LAST_PAGE,
  PRE_LAST_PAGE,
  ONE_EXTRA_PAGE,
  TWO_EXTRA_PAGES,
  LAST_BUTTON,
  FIRST_BUTTON,
  FIRST_BUTTON_ID,
  BUTTON_NAV_CLICKED,
  BUTTON_HOVERED,
  ARROW_LEFT,
  ARROW_RIGHT
} from './constants'

class Pagination extends PureComponent<IProps, IState> {
  static defaultProps: IProps = {
    layoutLength: 11,
    tabsCount: 7,
    setPageNumberAction: () => {},
    changePageNumberAction: () => {}
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      currentClick: '',
      tempArr: []
    }
  }

  _setClick = (click, tempArr) => {
    this.setState(prevState => ({
      currentClick: click,
      tempArr: tempArr || prevState.tempArr
    }))
  }

  _arraySettedByClickCheck = () => {
    const { tempArr } = this.state
    const pageCount = this._getPageCount()

    const arrayNotSetted = () => {
      return this.setState(prevState => ({
        ...prevState,
        currentClick: BUTTON_NAV_CLICKED
      }))
    }

    const arrayAlreadySetted = () => {
      const postFirstButtonClicked = tempArr[SECOND_DYNAMIC_ITEM_ID] === SECOND_PAGE_ID
      const preLastButtonClicked = tempArr[tempArr.length - ARRAY_LENGTH_FIXER] === pageCount - PRE_LAST_PAGE_ID

      if (postFirstButtonClicked) {
        return this.setState(prevState => ({
          ...prevState,
          currentClick: BUTTON_NAV_CLICKED,
          tempArr: tempArr.slice(SECOND_DYNAMIC_ITEM_ID, tempArr.length)
        }))
      }

      if (preLastButtonClicked) {
        return this.setState(prevState => ({
          ...prevState,
          currentClick: BUTTON_NAV_CLICKED,
          tempArr: tempArr.slice(FIRST_DYNAMIC_ITEM_ID, tempArr.length - ARRAY_LENGTH_FIXER)
        }))
      }

      return arrayNotSetted()
    }

    return {
      arrayNotSetted,
      arrayAlreadySetted
    }
  }

  _handleClickButton = ({ target }) => {
    const { tempArr } = this.state
    const { setPageNumberAction, currentPage } = this.props

    const isArraySetted = tempArr.length > ARRAY_NO_LENGTH

    const {
      dataset: { id: ID }
    } = target

    if (currentPage !== Number(ID)) {
      setPageNumberAction(Number(ID))
    }

    const { arrayNotSetted, arrayAlreadySetted } = this._arraySettedByClickCheck()

    if (isArraySetted) {
      return arrayAlreadySetted()
    }

    return arrayNotSetted()
  }

  _handleChangePage = nextPageID => {
    const { changePageNumberAction } = this.props

    changePageNumberAction(nextPageID)
  }

  _handleHover = () => {
    const tempArr = this._getCurrentPageSlice()

    this.setState(prevState => ({
      ...prevState,
      currentClick: BUTTON_HOVERED,
      tempArr
    }))
  }

  _getPageCount = () => {
    const { pageCount } = this.props

    return pageCount
  }

  _getCurrentPageSlice = () => {
    const { currentClick, tempArr } = this.state
    const { currentPage, layoutLength: MAX_PAGINATION_LAYOUT_LENGTH, tabsCount: MAX_PAGINATION_TABS_COUNT } = this.props

    const pageCount = this._getPageCount()
    const originalPages = Array.from(Array(pageCount).keys())

    const maxLayoutPageCount = MAX_PAGINATION_TABS_COUNT + LAST_TWO_PAGES
    const maxLayoutPageCountFromEnd = pageCount - maxLayoutPageCount
    const preLastLayoutPage = pageCount - PRE_LAST_PAGE

    if (pageCount <= MAX_PAGINATION_LAYOUT_LENGTH) {
      return originalPages.slice(FROM_SECOND_PAGE, pageCount - PRE_LAST_PAGE)
    }

    const arrowRightClick = () => {
      const pageListStartFromEnd = currentPage - MAX_PAGINATION_TABS_COUNT
      const prePreLastPage = pageCount - PRE_PRE_LAST_PAGE
      const preLastPage = pageCount - PRE_LAST_PAGE
      const notOnLastPage = currentPage !== pageCount
      const lastDynamicArrayItem = tempArr[tempArr.length - ARRAY_LENGTH_FIXER] + TWO_EXTRA_PAGES
      const hitLastDynamicArrayItem = currentPage === lastDynamicArrayItem

      // on pre pre last page we need to show dynamic array
      // with extra next page button instead of dots and empty space
      if (currentPage === prePreLastPage) {
        return originalPages.slice(pageListStartFromEnd, currentPage + ONE_EXTRA_PAGE)
      }

      // on pre last page we need to show dynamic array with
      // extra first dynamic array page button instead of empty space
      if (currentPage === preLastPage) {
        return originalPages.slice(pageListStartFromEnd - ONE_EXTRA_PAGE, currentPage)
      }

      // dynamically change array if we hit the last array button by one
      if (hitLastDynamicArrayItem && notOnLastPage) {
        return originalPages.slice(pageListStartFromEnd, currentPage)
      }

      return tempArr
    }

    const arrowLeftClick = () => {
      const maxPageListOnStart = currentPage + MAX_PAGINATION_TABS_COUNT
      const forwardFirstClickedPage = currentPage - ONE_EXTRA_PAGE
      const maxTabsLayoutLengthFromEnd = forwardFirstClickedPage + MAX_PAGINATION_TABS_COUNT

      // on post first page we need to show original array without
      // extra last dynamic array page button
      if (currentPage === POST_FIRST_PAGE) {
        return originalPages.slice(FIRST_PAGE, maxPageListOnStart)
      }

      // on post post first page we need to show original array
      // without extra prev page button instead of dots and empty space
      // and extra last dynamic array page button
      if (currentPage === POST_POST_FIRST_PAGE) {
        return originalPages.slice(FIRST_PAGE, maxPageListOnStart - ONE_EXTRA_PAGE)
      }

      // dynamically change array if we hit the first array button by one
      if (currentPage === tempArr[ARRAY_FIRST_ITEM] && currentPage !== FIRST_PAGE) {
        return originalPages.slice(forwardFirstClickedPage, maxTabsLayoutLengthFromEnd)
      }

      return tempArr
    }

    const pageButtonClicked = () => {
      // clicked page is in the range of beginnings layout count
      if (currentPage <= maxLayoutPageCount) {
        return originalPages.slice(FROM_SECOND_PAGE, maxLayoutPageCount)
      }

      // clicked page is in the range of ends layout count
      if (currentPage > maxLayoutPageCountFromEnd) {
        return originalPages.slice(maxLayoutPageCountFromEnd, preLastLayoutPage)
      }

      return tempArr
    }

    const pageActiveWithoutClick = () => {
      const currentPageOnTheBeginning = currentPage < MAX_PAGINATION_TABS_COUNT + TWO_EXTRA_PAGES

      // entered page is in the range of beginnings layout count
      if (currentPageOnTheBeginning) {
        return originalPages.slice(FROM_SECOND_PAGE, maxLayoutPageCount)
      }

      const currentPageOnTheEnd = currentPage > pageCount - MAX_PAGINATION_TABS_COUNT - TWO_EXTRA_PAGES

      // entered page is in the range of ends layout count
      if (currentPageOnTheEnd) {
        return originalPages.slice(maxLayoutPageCountFromEnd, preLastLayoutPage)
      }

      const withThreePagesBeforeCurrentPage = currentPage - MAX_PAGINATION_TABS_COUNT / 2
      const withThreePagesAfterCurrentPage = currentPage + MAX_PAGINATION_TABS_COUNT / 2

      return originalPages.slice(withThreePagesBeforeCurrentPage, withThreePagesAfterCurrentPage)
    }

    const arraySetted = () => {
      return tempArr
    }

    // user clicked on right arrow
    if (currentClick === ARROW_RIGHT) {
      return arrowRightClick()
    }

    // user clicked on left arrow
    if (currentClick === ARROW_LEFT) {
      return arrowLeftClick()
    }

    // user clicked on tab button arrow
    if (currentClick === BUTTON_NAV_CLICKED) {
      return pageButtonClicked()
    }

    // there already set tempArr pagination array in memory by somehow
    if (tempArr.length > ARRAY_NO_LENGTH) {
      return arraySetted()
    }

    return pageActiveWithoutClick()
  }

  _getPaginationList = () => {
    const { currentClick } = this.state
    const { currentPage } = this.props
    const slicedPageCount = this._getCurrentPageSlice()

    return slicedPageCount.map(itemNumber => {
      const pageID = itemNumber + PAGE_ID_FIXER

      return (
        <Button
          key={itemNumber}
          pageID={pageID}
          dataId={pageID}
          itemNumber={itemNumber}
          currentPage={currentPage}
          handleHover={currentClick === BUTTON_HOVERED ? undefined : this._handleHover}
          handleClickButton={this._handleClickButton}
        />
      )
    })
  }

  _renderBorderButton = type => {
    const { currentClick } = this.state
    const { currentPage } = this.props
    const pageCount = this._getPageCount()

    const lastButtonMustBeDisabled = type === LAST_BUTTON && pageCount <= ARRAY_NO_LENGTH
    const currentButtonID = type === FIRST_BUTTON ? FIRST_BUTTON_ID : pageCount

    if (lastButtonMustBeDisabled) return

    return (
      <Button
        currentPage={currentPage}
        pageID={currentButtonID}
        dataId={currentButtonID}
        handleHover={currentClick === BUTTON_HOVERED ? undefined : this._handleHover}
        handleClickButton={this._handleClickButton}
      />
    )
  }

  _renderPageDots = type => {
    const { layoutLength: MAX_PAGINATION_LAYOUT_LENGTH } = this.props

    const slicedArray = this._getCurrentPageSlice()
    const pageCount = this._getPageCount()

    return (
      <Dots type={type} slicedArray={slicedArray} pageCount={pageCount} layoutLength={MAX_PAGINATION_LAYOUT_LENGTH} />
    )
  }

  _renderArrow = arrowType => {
    const { currentPage } = this.props
    const slicedArray = this._getCurrentPageSlice()
    const pageCount = this._getPageCount()

    return (
      <Arrow
        setClick={this._setClick}
        arrowType={arrowType}
        changePage={this._handleChangePage}
        pageNumber={currentPage}
        pageCount={pageCount}
        tempArr={slicedArray}
      />
    )
  }

  render() {
    const pageCount = this._getPageCount()

    if (!pageCount || pageCount === 0) {
      return <div className={styles.paginationContainer} />
    }

    return (
      <div className={styles.paginationContainer}>
        {this._renderArrow('ArrowLeft')}
        {this._renderBorderButton('first')}
        {this._renderPageDots('first')}
        {this._getPaginationList()}
        {this._renderPageDots('last')}
        {this._renderBorderButton('last')}
        {this._renderArrow('ArrowRight')}
      </div>
    )
  }
}

export default Pagination
