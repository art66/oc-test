import React, { PureComponent } from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const BUTTON_CLICKED = true
const BUTTON_NOT_CLICKED = false

class Button extends PureComponent<IProps> {
  _checkIsButtonClicked = () => {
    const { currentPage, pageID } = this.props

    if (currentPage === pageID) {
      return BUTTON_CLICKED
    }

    return BUTTON_NOT_CLICKED
  }

  _classNameButtonHolder = () => {
    const buttonClass = classnames({
      [styles.navButton]: true,
      [styles.navButtonActive]: this._checkIsButtonClicked()
    })

    return buttonClass
  }

  render() {
    const { pageID, dataId, itemNumber, handleHover, handleClickButton } = this.props
    const buttonClass = this._classNameButtonHolder()

    const CustomTag = this._checkIsButtonClicked() ? 'button' : Link

    return (
      <div key={itemNumber} className={styles.navContainer}>
        <CustomTag
          to={`/?page=${pageID}`}
          type='button'
          className={buttonClass}
          data-id={dataId}
          onMouseEnter={handleHover}
          onClick={handleClickButton}
        >
          {pageID}
        </CustomTag>
      </div>
    )
  }
}

export default Button
