export interface IProps {
  dataId?: number
  currentPage?: number
  itemNumber?: number
  pageID?: number
  handleHover?: () => void
  handleClickButton?: ({ target }: { target: any }) => void
}
