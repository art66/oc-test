const initialState = {
  dataId: 1,
  currentPage: 1,
  itemNumber: 1,
  pageID: 1,
  handleHover: () => {},
  handleClickButton: () => {}
}

const theSamePage = {
  ...initialState,
  currentPage: 1
}

const notTheSamePage = {
  ...initialState,
  currentPage: 2
}

export default initialState
export { theSamePage, notTheSamePage }
