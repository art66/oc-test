import React, { cloneElement } from 'react'
import { mount } from 'enzyme'
import { HashRouter } from 'react-router-dom'
import initialState, { notTheSamePage, theSamePage } from './mocks'
import Button from '..'

describe('<Buttons test />', () => {
  it('should render Link and after click on it make it as Button', async done => {
    const Component = mount(
      <HashRouter>
        <Button {...notTheSamePage} />
      </HashRouter>
    )

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component).toMatchSnapshot()

    const findLink = () => {
      expect(Component.find('Link').length).toBe(1)
      expect(Component.find('a').length).toBe(1)
      expect(Component.find('button').length).toBe(0)
      expect(Component.find('.navButton').length).toBe(3)
      expect(Component).toMatchSnapshot()
    }

    const findButton = () => {
      expect(Component.find('Link').length).toBe(0)
      expect(Component.find('a').length).toBe(0)
      expect(Component.find('button').length).toBe(1)
      expect(Component.find('.navButton').length).toBe(1)
      expect(Component).toMatchSnapshot()
    }

    findLink()

    Component.find('a').simulate('click')
    Component.setProps({
      children: cloneElement(Component.props().children, { currentPage: 1 })
    })
    Component.update()


    findButton()

    done()
  })
  it('should render basic Button', () => {
    const Component = mount(<Button {...initialState} />)

    expect(Component.find('button').length).toBe(1)
    expect(Component.find('.navButton').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render button tag in case when the current pageID is equal to the clickedPage', () => {
    const Component = mount(<Button {...theSamePage} />)

    expect(Component.find('a').length).toBe(0)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('.navButton').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render button tag clicked with active class added', () => {
    const Component = mount(<Button {...theSamePage} />)

    expect(Component.find('a').length).toBe(0)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('.navButton').length).toBe(1)
    expect(Component.find('.navButtonActive').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should render link tag in case when the current pageID is not equal to the clickedPage', () => {
    const Component = mount(
      <HashRouter>
        <Button {...notTheSamePage} />
      </HashRouter>
    )

    expect(Component.find('Link').length).toBe(1)
    expect(Component.find('a').length).toBe(1)
    expect(Component.find('button').length).toBe(0)
    expect(Component.find('.navButton').length).toBe(3)

    expect(Component).toMatchSnapshot()
  })
})
