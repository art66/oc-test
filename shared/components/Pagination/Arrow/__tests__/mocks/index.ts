const commonArrowProps = {
  tempArr: [1, 2, 3, 4],
  pageCount: 13,
  pageNumber: 10,
  arrowType: '',
  changePage: () => {},
  setClick: () => {}
}

const leftArrowActive = {
  ...commonArrowProps,
  arrowType: 'ArrowLeft'
}

const leftArrowDisabled = {
  ...commonArrowProps,
  pageNumber: 1,
  arrowType: 'ArrowLeft'
}

const rightArrowActive = {
  ...commonArrowProps,
  pageNumber: 10,
  arrowType: 'ArrowRight'
}

const rightArrowDisabled = {
  ...commonArrowProps,
  pageNumber: 13,
  arrowType: 'ArrowRight'
}

const noArrow = {
  ...commonArrowProps,
  pageCount: 1
}

export { leftArrowActive, leftArrowDisabled, rightArrowActive, rightArrowDisabled, noArrow }
