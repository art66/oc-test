import React from 'react'
import { mount } from 'enzyme'
import { HashRouter } from 'react-router-dom'
import { leftArrowActive, leftArrowDisabled, rightArrowActive, rightArrowDisabled, noArrow } from './mocks'
import Arrow from '..'

describe('<Arrow test />', () => {
  const findArrowLink = ({ Component, arrowClass = '.customArrowLeft' }) => {
    expect(Component.find('Arrow').length).toBe(1)
    expect(Component.find('a').length).toBe(1)
    expect(Component.find('button').length).toBe(0)
    expect(Component.find(arrowClass).length).toBe(1)
    expect(Component).toMatchSnapshot()
  }

  const findButtonLink = ({ Component, arrowClass = '.customArrowLeft' }) => {
    expect(Component.find('Arrow').length).toBe(1)
    expect(Component.find('a').length).toBe(0)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find(arrowClass).length).toBe(1)
    expect(Component).toMatchSnapshot()
  }

  it('should render left Arrow as active Link and after click on it make it disabled by change on Button', async done => {
    const Component = mount(
      <HashRouter>
        <Arrow {...leftArrowActive} />
      </HashRouter>
    )

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    const makeClick = () => {
      Component.find('a').simulate('click')
      Component.setProps(leftArrowDisabled)
    }

    findArrowLink({ Component })
    makeClick()
    findArrowLink({ Component })

    done()
  })
  it('should render left Arrow as active Link if it doesn\'t on the first page', async done => {
    const Component = mount(
      <HashRouter>
        <Arrow {...leftArrowActive} />
      </HashRouter>
    )

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    findArrowLink({ Component })

    done()
  })
  it('should render left Arrow as disabled Button if it on the first page', async done => {
    const Component = mount(<Arrow {...leftArrowDisabled} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Arrow').length).toBe(1)
    expect(Component.find('a').length).toBe(0)
    expect(Component.find('button').length).toBe(1)
    expect(Component.find('.customArrowLeft').length).toBe(1)

    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render right Arrow as active Link and after click on it make it disabled by change on Button', async done => {
    const Component = mount(
      <HashRouter>
        <Arrow {...rightArrowActive} />
      </HashRouter>
    )

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    findArrowLink({ Component, arrowClass: '.customArrowRight' })

    Component.find('a').simulate('click')
    Component.setProps(rightArrowDisabled)

    findArrowLink({ Component, arrowClass: '.customArrowRight' })

    done()
  })
  it('should render right Arrow as active Link if it doesn\'t on the last page', async done => {
    const Component = mount(
      <HashRouter>
        <Arrow {...rightArrowActive} />
      </HashRouter>
    )

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component).toMatchSnapshot()

    findArrowLink({ Component, arrowClass: '.customArrowRight' })

    done()
  })
  it('should render right Arrow as disabled Button if it on the last page', async done => {
    const Component = mount(<Arrow {...rightArrowDisabled} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component).toMatchSnapshot()

    findButtonLink({ Component, arrowClass: '.customArrowRight' })

    done()
  })
  it('should not render Arrow if pagination length is equal 1', async done => {
    const Component = mount(<Arrow {...noArrow} />)

    await new Promise(res => setTimeout(() => res(), 1000))
    Component.update()

    expect(Component.find('Arrow').length).toBe(1)
    expect(Component.find('button').length).toBe(0)
    expect(Component.find('.customArrowRight').length).toBe(0)

    expect(Component).toMatchSnapshot()

    done()
  })
})
