export interface IProps {
  tempArr: any
  pageCount: number
  pageNumber: number
  arrowType: string
  changePage: (type: number) => void
  setClick: (click: string, slicedPageCount: number[]) => void
}
