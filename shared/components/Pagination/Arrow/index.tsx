import React, { PureComponent } from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const FIRST_PAGE_ID = 1
const ARROW_CLICKED_COLOR = '#79796a'
const ARROW_INACTIVE_COLOR = '#cecebf'
const CHANGE_PAGE_COUNTER_BY_ONE = 1
const SVG_FILTER = null
const SVG_FILL_COLOR = {
  stroke: '#f9f9f1',
  strokeWidth: 3
}
const SVG_DIMENSIONS = {
  width: 25,
  height: 25
}

class Arrow extends PureComponent<IProps, any> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      fillColor: ARROW_CLICKED_COLOR,
      disableClick: false
    }
  }

  static getDerivedStateFromProps(nextProps: IProps) {
    const isRightArrow = nextProps.arrowType === 'ArrowRight'
    const isLeftArrow = nextProps.arrowType === 'ArrowLeft'
    const isLastPageClicked = nextProps.pageNumber === nextProps.pageCount
    const isFirstPageClicked = nextProps.pageNumber === FIRST_PAGE_ID

    const rightArrow = isRightArrow && isLastPageClicked
    const leftArrow = isLeftArrow && isFirstPageClicked

    if (rightArrow || leftArrow) {
      return {
        fillColor: ARROW_INACTIVE_COLOR,
        disableClick: true
      }
    }

    return {
      fillColor: ARROW_CLICKED_COLOR,
      disableClick: false
    }
  }

  _classNameHolder = () => {
    const { disableClick } = this.state

    const buttonClass = classnames({
      [styles.arrowButton]: true,
      [styles.arrowButtonDisalbed]: disableClick
    })

    return buttonClass
  }

  _getNewPagePerChange = () => {
    const { arrowType, pageNumber, pageCount } = this.props
    const isRightArrow = arrowType === 'ArrowRight'
    const isLeftArrow = arrowType === 'ArrowLeft'
    const isNotFirstPageClicked = pageNumber !== FIRST_PAGE_ID
    const isNotLAstPageClicked = pageCount !== pageNumber

    if (isLeftArrow && isNotFirstPageClicked) {
      return pageNumber - CHANGE_PAGE_COUNTER_BY_ONE
    }

    if (isRightArrow && isNotLAstPageClicked) {
      return pageNumber + CHANGE_PAGE_COUNTER_BY_ONE
    }

    return pageNumber
  }

  _handlerClick = () => {
    const { disableClick } = this.state

    if (disableClick) return

    const { changePage, setClick, arrowType, tempArr } = this.props

    const pageNumber = this._getNewPagePerChange()

    changePage(pageNumber)
    setClick(arrowType, tempArr)
  }

  _handlerBlur = () => {
    const { setClick } = this.props

    setClick('', undefined)
  }

  render() {
    const { fillColor } = this.state
    const { arrowType, pageNumber, pageCount } = this.props

    if (!pageCount || pageCount === FIRST_PAGE_ID) {
      return null
    }

    const buttonClass = this._classNameHolder()
    const newPageNumber = this._getNewPagePerChange()

    const CustomTag = pageNumber === FIRST_PAGE_ID || pageNumber === pageCount ? 'button' : Link

    return (
      <div className={styles.arrowContainer}>
        <CustomTag
          to={`/?page=${newPageNumber}`}
          className={buttonClass}
          onClick={this._handlerClick}
          onBlur={this._handlerBlur}
        >
          <SVGIconGenerator
            iconsHolder={import(/* webpackChunkName: "globalSVGIcons" */ '@torn/shared/SVG/helpers/iconsHolder/global')}
            iconName={arrowType}
            customClass={styles[`custom${arrowType}`]}
            filter={{ ID: SVG_FILTER }}
            fill={{ color: fillColor, ...SVG_FILL_COLOR }}
            dimensions={SVG_DIMENSIONS}
          />
        </CustomTag>
      </div>
    )
  }
}

export default Arrow
