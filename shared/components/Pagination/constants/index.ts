// main pagination export constants
export const PAGE_ID_FIXER = 1

// common array export constants
export const ARRAY_LENGTH_FIXER = 1
export const ARRAY_NO_LENGTH = 1
export const ARRAY_FIRST_ITEM = 0

// export constants for dynamicly generated array
export const FIRST_DYNAMIC_ITEM_ID = 0
export const SECOND_DYNAMIC_ITEM_ID = 1

// pages export constants id
export const SECOND_PAGE_ID = 2
export const PRE_LAST_PAGE_ID = 2
export const LAST_TWO_PAGES = 2
export const FROM_SECOND_PAGE = 1
export const FIRST_PAGE = 1
export const POST_FIRST_PAGE = 2
export const POST_POST_FIRST_PAGE = 3
export const PRE_PRE_LAST_PAGE = 2
export const PRE_LAST_PAGE = 1
export const ONE_EXTRA_PAGE = 1
export const TWO_EXTRA_PAGES = 2
export const THREE_EXTRA_PAGES = 3
export const FOUR_EXTRA_PAGES = 4

// buttons export constants lables and id
export const LAST_BUTTON = 'last'
export const FIRST_BUTTON = 'first'
export const FIRST_BUTTON_ID = 1

// click export constansts lables
export const BUTTON_NAV_CLICKED = 'pageButtonClicked'
export const BUTTON_HOVERED = 'hoverButton'
export const ARROW_LEFT = 'ArrowLeft'
export const ARROW_RIGHT = 'ArrowRight'
