export interface IProps {
  pageCount?: number
  currentPage?: number
  layoutLength?: number
  tabsCount?: number
  setPageNumberAction?: (ID: number) => void
  changePageNumberAction?: (type: number) => void
}

export interface IState {
  currentClick: string
  tempArr: number[]
}
