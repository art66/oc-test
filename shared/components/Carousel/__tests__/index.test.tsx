import React from 'react'
import { mount } from 'enzyme'

import Carousel from '../index'
import makeChildren from '../helpers'
import styles from '../mocks/index.cssmodule.scss'

describe('Carousel', () => {
  beforeAll(() => {
    // settings carousel Window aka real-browser dimensions for synthetic testing
    // @ts-ignore
    HTMLDivElement.prototype.getBoundingClientRect = jest.fn(() => {
      return {
        width: 744,
        height: 50,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
      }
    })

    // @ts-ignore
    HTMLImageElement.prototype.getBoundingClientRect = jest.fn(() => {
      return {
        width: 55,
        height: 24,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
      }
    })
  })

  it('should return basic carousel with scrolling', () => {
    const itemsCount = 34

    const Component = mount(
      <Carousel mediaType='desktop'>
        {makeChildren({ clsName: styles.carouselItem, count: itemsCount })}
      </Carousel>
    )

    expect(Component.find('Memo(Carousel)').length).toBe(1)
    expect(Component.find('Arrow').length).toBe(2)
    expect(Component.find('div.scrollWrap').length).toBe(1)
    expect(Component.find('div.carouselContainer').length).toBe(1)
    expect(Component.find('img').length).toBe(itemsCount)

    expect(Component).toMatchSnapshot()
  })
  it('should return carousel without scrolling', () => {
    const itemsCount = 10

    const Component = mount(
      <Carousel>
        {makeChildren({ clsName: styles.carouselItem, count: itemsCount })}
      </Carousel>
    )

    expect(Component.find('Memo(Carousel)').length).toBe(1)
    expect(Component.find('Arrow').length).toBe(2)
    expect(Component.find('div.scrollWrap').length).toBe(0)
    expect(Component.find('div.carouselContainer').length).toBe(1)
    expect(Component.find('img').length).toBe(itemsCount)

    expect(Component).toMatchSnapshot()
  })
  it('should return scrolling carousel without arrows', () => {
    const itemsCount = 34

    const Component = mount(
      <Carousel isArrows={false}>
        {makeChildren({ clsName: styles.carouselItem, count: itemsCount })}
      </Carousel>
    )

    expect(Component.find('Memo(Carousel)').length).toBe(1)
    expect(Component.find('Arrow').length).toBe(0)
    expect(Component.find('div.scrollWrap').length).toBe(1)
    expect(Component.find('div.carouselContainer').length).toBe(1)
    expect(Component.find('img').length).toBe(itemsCount)

    expect(Component).toMatchSnapshot()
  })
  it('should return scrolling carousel with wrap over carousel', () => {
    const itemsCount = 34

    const Component = mount(
      <Carousel addWrap={true}>
        {makeChildren({ clsName: styles.carouselItem, count: itemsCount })}
      </Carousel>
    )

    expect(Component.find('Memo(Carousel)').length).toBe(1)
    expect(Component.find('Arrow').length).toBe(2)
    expect(Component.find('div.wrap').length).toBe(1)
    expect(Component.find('div.scrollWrap').length).toBe(1)
    expect(Component.find('div.carouselContainer').length).toBe(1)
    expect(Component.find('img').length).toBe(itemsCount)

    expect(Component).toMatchSnapshot()
  })
  it('should return scrolling carousel with custom classes', () => {
    const itemsCount = 34
    const STYLES_OVERRIDE = {
      wrap: styles.wrap,
      scrollWrap: styles.scroll,
      container: styles.container
    }

    const Component = mount(
      <Carousel addWrap={true} stylesOverride={STYLES_OVERRIDE}>
        {makeChildren({ clsName: styles.carouselItem, count: itemsCount })}
      </Carousel>
    )

    expect(Component.find('Memo(Carousel)').length).toBe(1)
    expect(Component.find('Arrow').length).toBe(2)
    expect(Component.find('div.wrap.wrap').length).toBe(1)
    expect(Component.find('div.scrollWrap.scroll').length).toBe(1)
    expect(Component.find('div.carouselContainer.container').length).toBe(1)
    expect(Component.find('img').length).toBe(itemsCount)

    expect(Component).toMatchSnapshot()
  })
  it('should scroll carousel by arrows Clicks', () => {
    const itemsCount = 34

    const Component = mount(
      <Carousel addWrap={true}>
        {makeChildren({ clsName: styles.carouselItem, count: itemsCount })}
      </Carousel>
    )

    expect(Component.find('Memo(Carousel)').length).toBe(1)
    expect(Component.find('Arrow').length).toBe(2)
    expect(Component.find('div.wrap').length).toBe(1)
    expect(Component.find('div.scrollWrap').length).toBe(1)
    expect(Component.find('div.carouselContainer').length).toBe(1)
    expect(Component.find('img').length).toBe(itemsCount)

    // TODO: it's a hack below! Need to be fixed once carousel became much complex and strong!
    expect(Component.find('div.carouselContainer').prop('style')).toEqual({ transform: 'translateX(0px)' })
    Component.find('button.arrowWrap').at(1).simulate('click')
    expect(Component.find('div.carouselContainer').prop('style')).toEqual({ transform: 'translateX(0px)' })

    expect(Component).toMatchSnapshot()
  })
})
