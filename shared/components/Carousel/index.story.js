import React, { useEffect, useState } from 'react'
import { select, boolean, text, number } from '@storybook/addon-knobs'

import getMediaType from '../../../.storybook/utils/getMediaType'
import styles from './mocks/index.cssmodule.scss'
import Carousel from './index'

const STYLES_OVERRIDE = {
  wrap: styles.wrap,
  scrollWrap: styles.scroll,
  container: styles.container
}

const CHILDREN = Array.from(Array(34).keys()).map((item) => (
  <img alt="" key={item} className={styles.carouselItem} src={`/images/items/${++item}/large.png`} />
))

// const mediaTypeForceUpdate = mediaType => {
//   setTimeout(() => {
//     mediaType = 'tablet'

//     setTimeout(() => {
//       mediaType = 'desktop'
//     }, 25)
//   }, 25)
// }

const SupportCarouselStorybookWrap = () => {
  const [mediaType, setMediaType] = useState('desktop')

  const mediaUpdate = () => getMediaType(setMediaType)

  useEffect(() => {
    window.addEventListener('resize', mediaUpdate)

    return () => window.removeEventListener('resize', mediaUpdate)
  }, [])

  return (
    <Carousel addWrap={true} mediaType={mediaType} stylesOverride={STYLES_OVERRIDE}>
      {CHILDREN}
    </Carousel>
  )
}

export const Defaults = () => <SupportCarouselStorybookWrap />

Defaults.story = {
  name: 'defaults'
}

export const SelectAdaptiveMode = () => {
  const mediaType = select('MediaType Layout', ['desktop', 'tablet', 'mobile'])

  const updatedStyles = {
    ...STYLES_OVERRIDE,
    wrap:
      (mediaType === 'tablet' && styles.wrap_tablet) ||
      (mediaType === 'mobile' && styles.wrap_mobile) ||
      styles.wrap_desktop
  }

  return (
    <Carousel addWrap={true} mediaType={mediaType} stylesOverride={updatedStyles}>
      {CHILDREN}
    </Carousel>
  )
}

SelectAdaptiveMode.story = {
  name: 'select adaptive mode'
}

export const WithoutWrap = () => {
  let mediaType = 'desktop'

  // it's a hack for forcing carousel layout update
  setTimeout(() => {
    mediaType = 'tablet'

    setTimeout(() => {
      mediaType = 'desktop'
    }, 25)
  }, 25)

  return (
    <Carousel addWrap={boolean('Add wrap', false)} mediaType={mediaType} stylesOverride={STYLES_OVERRIDE}>
      {CHILDREN}
    </Carousel>
  )
}

WithoutWrap.story = {
  name: 'without wrap'
}

export const SelectScrollType = () => {
  return (
    <Carousel
      addWrap={true}
      mediaType="dekstop"
      scrollType={select('Animation Type:', ['manual', 'auto'])}
      stylesOverride={STYLES_OVERRIDE}
    >
      {CHILDREN}
    </Carousel>
  )
}

SelectScrollType.story = {
  name: 'select scroll type'
}

export const AddArrows = () => {
  return (
    <Carousel
      addWrap={true}
      mediaType="dekstop"
      isArrows={boolean('Add arrows', false)}
      stylesOverride={STYLES_OVERRIDE}
    >
      {CHILDREN}
    </Carousel>
  )
}

AddArrows.story = {
  name: 'add arrows'
}

export const StylesOverride = () => {
  const stylesConfig = {
    wrap: text('Set wrap Class: ', styles.wrapTest),
    scrollWrap: text('Set scroll Class: ', styles.scrollTest),
    container: text('Set container Class: ', styles.containerTest)
  }

  return (
    <Carousel addWrap={true} mediaType="desktop" stylesOverride={stylesConfig}>
      {CHILDREN}
    </Carousel>
  )
}

StylesOverride.story = {
  name: 'styles override'
}

export const SetResistingShift = () => {
  return (
    <Carousel
      addWrap={true}
      mediaType="desktop"
      resistanceMin={number('Set min resistance: ', 2)}
      resistanceMax={number('Set max resistance: ', 8)}
      stylesOverride={STYLES_OVERRIDE}
    >
      {CHILDREN}
    </Carousel>
  )
}

SetResistingShift.story = {
  name: 'set resisting shift'
}

export const DeactivatedCarousel = () => {
  const childCount = number('Set children count: ', 10)

  const children = Array.from(Array(childCount).keys()).map((item) => (
    <img alt="" key={item} className={styles.carouselItem} src={`/images/items/${++item}/large.png`} />
  ))

  return (
    <Carousel addWrap={true} mediaType="desktop" stylesOverride={STYLES_OVERRIDE}>
      {children}
    </Carousel>
  )
}

DeactivatedCarousel.story = {
  name: 'deactivated carousel'
}

export default {
  title: 'Shared/Carousel',
  parameters: {
    viewport: { defaultViewport: 'Tablet' }
  }
}
