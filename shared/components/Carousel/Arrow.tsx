import React from 'react'
import classnames from 'classnames'

import { SVGIconGenerator } from '@torn/shared/SVG'
import ArrowCrimes from '@torn/shared/SVG/icons/crimes/Arrow'

import styles from './arrow.cssmodule.scss'

const FILL_DISABLED = {
  color: '#ccc',
  strokeWidth: 1,
  stroke: '#ccc'
}

const FILL_ACTIVE = {
  color: '#9c9a9a',
  strokeWidth: 1,
  stroke: '#9c9a9a'
}

const DIMENSIONS = {
  width: '100%',
  height: '100%',
  viewbox: '0 -5 20 55'
}

const ON_HOVER = {
  active: true,
  fill: {
    strokeWidth: 1,
    color: '#111',
    stroke: '#111'
  }
}

const FILTER = {
  active: false
}

interface IProps {
  isDisabled?: boolean
  onClick?: () => void
  arrowType: string
}

class Arrow extends React.Component<IProps> {
  shouldComponentUpdate(prevProps: IProps) {
    const { isDisabled } = this.props

    if (prevProps.isDisabled !== isDisabled) {
      return true
    }

    return false
  }

  render() {
    const { arrowType = 'left', onClick, isDisabled } = this.props

    const classNamesHolder = classnames({
      [styles.arrowWrap]: true,
      [styles[arrowType]]: true,
      [styles.disableHover]: isDisabled
    })

    return (
      <button
        type='button'
        className={classNamesHolder}
        onClick={isDisabled ? undefined : onClick}
      >
        <SVGIconGenerator
          iconsHolder={{ Arrow: { ...ArrowCrimes } }}
          iconName='Arrow'
          type={arrowType}
          fill={isDisabled ? FILL_DISABLED : FILL_ACTIVE}
          onHover={isDisabled ? {} : ON_HOVER}
          onClick={isDisabled ? {} : ON_HOVER}
          dimensions={DIMENSIONS}
          filter={FILTER}
        />
      </button>
    )
  }
}

export default Arrow
