import React from 'react'

const makeChildren = ({ clsName, count = 34 }: { clsName: string; count: number }) => {
  return Array.from(Array(count).keys()).map(item => (
    <img
      alt=''
      key={item}
      className={clsName}
      src={`/images/items/${item + 1}/large.png`}
    />
  ))
}

export default makeChildren
