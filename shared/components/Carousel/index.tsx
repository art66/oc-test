import React, { useRef, memo, useState, useEffect, useLayoutEffect } from 'react'
import classnames from 'classnames'
import isValue from '@torn/shared/utils/isValue'

import Arrow from './Arrow'

import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'
import { MOVE_FIXER, MIN_SHIFT, MAX_SHIFT } from './constants'

// We must use here several "outscoped" state values because React state management is asynchronous.
// It don't allow to receive the actual data on the mousemove event because of the event-loop queue,
// that block useState value update on the fly.
// let originalShift = 0
let isMoveStarted = false
let isMoveRunning = false
let alreadyShifted = 0
let transitionOnStart = 0
let translateStartShift = 0
// let translateEndShift = 0
let moveDirection = 'none'
let animRequestFrameID = null

const Carousel = ({
  addWrap = false,
  stylesOverride = {},
  children,
  mediaType = 'desktop',
  scrollType = 'manual',
  isArrows = true,
  resistanceMin = MIN_SHIFT,
  resistanceMax = MAX_SHIFT
}: IProps) => {
  const carouselWrap = useRef(null)
  const [currentCarouselShift, updateCarouselShift] = useState(0)
  const [carouselParams, setCarouselWidth] = useState({ maxWidth: 0, capacity: 0 })

  // once layout is changed we need to be ensure
  // carouse is on the right coords by set default value
  useLayoutEffect(() => {
    setTranslateShift(0)
  }, [mediaType])

  useEffect(() => {
    return () => window.cancelAnimationFrame(animRequestFrameID)
  }, [mediaType])

  useLayoutEffect(() => {
    const wrap = getCarouselNode()
    const { width: carouselClientWidth } = wrap && wrap.getBoundingClientRect()

    const itemInWrap = wrap.firstChild
    const { width: itemClientWidth } = itemInWrap.getBoundingClientRect()

    const maxWidth = carouselClientWidth >= itemClientWidth ? carouselClientWidth : itemClientWidth

    setCarouselWidth({
      maxWidth: maxWidth || 0,
      capacity: Math.floor(carouselClientWidth / itemClientWidth) || 0
    })
  }, [mediaType])

  const getCarouselNode = () => {
    return carouselWrap && carouselWrap.current || document.body.querySelector('[class*="carouselContainer"]') || null
  }

  const getImagesWrapNode = () => {
    const carouselNode = getCarouselNode()

    return carouselNode && carouselNode.parentNode || null
  }

  const getCarouselWidth = () => {
    const carouselNode = getCarouselNode()

    return carouselNode && carouselNode.scrollWidth || 0
  }

  const getCarouselParams = () => carouselParams

  const getTranslateShift = () => {
    const carouselNode = getCarouselNode()
    const translateValue = carouselNode && carouselNode.style.transform.match(/-?\d+(\.\d+)?/i) || [0]

    return Number(translateValue[0])
  }

  // const setOriginalShift = (shift = 0) => {
  //   originalShift = shift
  // }

  const isAutoScroll = () => scrollType === 'auto'

  const checkIsCarouselMoving = () => isMoveStarted || isMoveRunning

  const checkAnimDirection = () => {
    return {
      isGoingBack: moveDirection === 'back',
      isGoingForward: moveDirection === 'forward',
      isIdle: moveDirection === 'none',
      currentDirection: moveDirection
    }
  }

  const checkAnimOverlap = () => {
    const currentTranslateShift = getTranslateShift()
    const cCarouselWidth = getCarouselWidth()
    const { maxWidth: maxCarouselWidth } = getCarouselParams()
    const { isGoingBack, isGoingForward } = checkAnimDirection()

    const shiftStep = currentTranslateShift
    const maxShiftGap = maxCarouselWidth - cCarouselWidth

    const isShouldGoToBack = shiftStep <= maxShiftGap && isGoingBack
    const isShouldGoToStart = shiftStep >= 0 && isGoingForward

    return {
      isShouldGoToStart,
      isShouldGoToBack
    }
  }

  const setTranslateShift = (shift = 0) => {
    const carouselNode = getCarouselNode()

    if (!carouselNode) {
      return
    }

    carouselNode.style.transform = `translateX(${shift}px)`
  }

  const setStartTransitionShift = startMoveValue => {
    translateStartShift = startMoveValue
    transitionOnStart = getTranslateShift()
  }

  // const setFinalTransitionShift = shiftValue => {
  //   // translateEndShift = addShiftFix(shiftValue)
  // }

  const setMoveDirection = (shift = 0) => {
    moveDirection = translateStartShift > shift && 'back' || translateStartShift < shift && 'forward' || 'none'
  }

  const setMovingFlags = ({ isStarted, isMoving }: { isStarted?: boolean; isMoving?: boolean }) => {
    isMoveStarted = isValue(isStarted) ? isStarted : isMoveStarted
    isMoveRunning = isValue(isMoving) ? isMoving : isMoveRunning
  }

  const updatePreviousTransitionShift = () => {
    alreadyShifted = getTranslateShift()
  }

  const manageTransitionPhase = () => {
    const wrap = getCarouselNode()

    wrap.style.transition = 'all 0.35s ease-out'

    setTimeout(() => {
      wrap.style.transition = 'none'
    }, 350)
  }

  const preventImagesEvents = (status = false) => {
    const wrap = getCarouselNode()

    if (status) {
      wrap.classList.add(styles.restrictPointerEventsOnScroll)

      return
    }

    wrap.classList.remove(styles.restrictPointerEventsOnScroll)
  }

  const preventGlobalScroll = (status = false) => {
    if (status) {
      document.documentElement.style.overflow = 'hidden'

      return
    }

    document.documentElement.style.overflow = 'unset'
  }

  const addEventListeners = () => {
    const wrap = getImagesWrapNode()

    // we need to add these listeners through the pure JS, because
    // React can't handle their removing in case of restarted animation
    wrap.addEventListener('mousemove', handlerMouseMove)
    wrap.addEventListener('touchmove', handlerMouseMove)
    wrap.addEventListener('mouseup', handlerUpClick)
    wrap.addEventListener('touchend', handlerUpClick)
    wrap.addEventListener('mouseleave', handlerMouseLeave)
    wrap.addEventListener('contextmenu', handleContextMenu)
  }

  const removeEventListeners = () => {
    const wrap = getImagesWrapNode()

    wrap.removeEventListener('mouseup', handlerUpClick)
    wrap.removeEventListener('touchend', handlerUpClick)
    wrap.removeEventListener('mousemove', handlerMouseMove)
    wrap.removeEventListener('touchmove', handlerMouseMove)
    wrap.removeEventListener('mouseleave', handlerMouseLeave)
    wrap.removeEventListener('contextmenu', handleContextMenu)
  }

  const addShiftFix = (shiftOrigin) => {
    const { isGoingForward } = checkAnimDirection()

    return isGoingForward ? shiftOrigin - MOVE_FIXER : shiftOrigin
  }

  const isFakeMove = value => {
    return -MOVE_FIXER < value - translateStartShift && value - translateStartShift < MOVE_FIXER
  }

  const isMoveFromStartOrEnd = () => {
    const { maxWidth } = getCarouselParams()
    const carouselWidth = getCarouselWidth()
    const { isGoingBack, isGoingForward } = checkAnimDirection()

    const maxShift = carouselWidth - maxWidth

    return alreadyShifted === 0 && isGoingForward || -maxShift === alreadyShifted && isGoingBack
  }

  const startAnimatedShift = () => {
    const translateValue = getTranslateShift()
    let startTransitionValue = 0
    let stepTemp = translateValue
    let stepTempExtra = 0

    return (() => {
      const { maxWidth } = getCarouselParams()
      const carouselWidth = getCarouselWidth()

      const maxShift = carouselWidth - maxWidth

      const checkForAnimationRemove = () => {
        const { isGoingBack, isGoingForward } = checkAnimDirection()

        const isScrollExpired = stepTemp === stepTempExtra
        const isOnTheStart = Math.round(stepTemp) <= 0 && isGoingForward
        const isOnTheEnd = Math.round(stepTemp) >= -maxShift && isGoingBack

        if (isScrollExpired) {
          window.cancelAnimationFrame(animRequestFrameID)

          if (isOnTheEnd && checkIsCarouselMoving()) {
            setTranslateShift(-maxShift)
            updateCarouselShift(-maxShift)
          } else if (isOnTheStart && checkIsCarouselMoving()) {
            setTranslateShift(0)
            updateCarouselShift(0)
          }

          setMovingFlags({ isStarted: false, isMoving: false })

          return
        }

        animRequestFrameID = window.requestAnimationFrame(handleShift)
      }

      const handleShift = () => {
        // if we restart moving by click down, then we need to restart the animation as well
        if (checkIsCarouselMoving()) {
          window.cancelAnimationFrame(animRequestFrameID)

          return
        }

        const translateValueDynamic = getTranslateShift()
        const { isShouldGoToStart, isShouldGoToBack } = checkAnimOverlap()
        const { isGoingBack, isGoingForward } = checkAnimDirection()

        // console.log(isShouldGoToStart, isShouldGoToBack, 'isShouldGoToStart, isShouldGoToBack')
        stepTempExtra = stepTemp

        const preventBordersOverlappedSmooth = () => {
          if (isGoingForward) {
            const animationShift = translateValueDynamic / resistanceMax

            stepTemp = animationShift <= 0.01 ? 0 : stepTemp - animationShift
          } else if (isGoingBack) {
            const animationShift = Math.abs(translateValueDynamic + maxShift) / resistanceMax

            stepTemp = translateValueDynamic // it's a hack for initShit reset!!!
            stepTemp = animationShift <= 0.01 ? -maxShift : stepTemp + animationShift
          }

          setTranslateShift(stepTemp)
          updateCarouselShift(stepTemp)
          checkForAnimationRemove()
        }

        const addAutoScroll = () => {
          if (isGoingForward) {
            const transitionBack = alreadyShifted - translateValueDynamic - startTransitionValue
            const transitionShift = transitionBack > 10 ? resistanceMin : resistanceMax
            const animationShift = Math.abs(transitionBack) / transitionShift
            const step = translateValueDynamic === stepTemp ? Math.abs(alreadyShifted - stepTemp) : stepTemp

            stepTemp = step <= 0.1 ? 0 : step - animationShift
            startTransitionValue = alreadyShifted - translateValueDynamic
          } else if (isGoingBack) {
            const transitionForward = translateValueDynamic - alreadyShifted - startTransitionValue
            const transitionShift = transitionForward > 10 ? resistanceMin : resistanceMax
            const animationShift = transitionForward / transitionShift
            const step = translateValueDynamic === stepTemp ? stepTemp - alreadyShifted : stepTemp

            stepTemp = step >= -0.1 ? 0 : step - animationShift
            startTransitionValue = translateValueDynamic - alreadyShifted
          }
        }

        const addScrollSmooth = () => {
          if (isShouldGoToStart || isShouldGoToBack) {
            preventBordersOverlappedSmooth()

            return
          }

          if (isAutoScroll()) {
            return
          }

          addAutoScroll()

          setTranslateShift(translateValueDynamic + stepTemp)
          updateCarouselShift(translateValueDynamic + stepTemp)
          checkForAnimationRemove()
        }

        addScrollSmooth()
      }

      animRequestFrameID = window.requestAnimationFrame(handleShift)
    })()
  }

  const shiftAutoCarousel = () => {
    const { maxWidth } = getCarouselParams()
    const { isGoingBack, isGoingForward } = checkAnimDirection()
    const carouselWidth = getCarouselWidth()
    const maxSHIFT = carouselWidth - maxWidth

    const goBack = () => {
      const isCapacityLess = maxSHIFT + transitionOnStart < maxWidth
      const onEndScroll = transitionOnStart + -(maxSHIFT + transitionOnStart)
      const regularScroll = getTranslateShift() + (transitionOnStart - getTranslateShift() - maxWidth)

      const transitionValue = isCapacityLess ? onEndScroll : regularScroll

      manageTransitionPhase()
      setTranslateShift(transitionValue)
    }

    const goForward = () => {
      const isCapacityLess = maxWidth + transitionOnStart > 0
      const onEndScroll = 0
      const regularScroll = getTranslateShift() + (transitionOnStart - getTranslateShift() + maxWidth)

      const transitionValue = isCapacityLess ? onEndScroll : regularScroll

      manageTransitionPhase()
      setTranslateShift(transitionValue)
    }

    if (isGoingBack) {
      goBack()
    } else if (isGoingForward) {
      goForward()
    }
  }

  const handleContextMenu = e => {
    e.preventDefault()
  }

  const handlerMouseMove = e => {
    const value = e.pageX || e.touches[0].pageX

    if (isFakeMove(value)) {
      return
    }

    // if we starting from the start or end of carousel need to change the resistance value
    const getShiftResistance = () => (isMoveFromStartOrEnd() && resistanceMax || isAutoScroll() && 1 || resistanceMin)
    const shiftValue = value - translateStartShift + MOVE_FIXER
    const shift = (addShiftFix(shiftValue) / getShiftResistance()) + alreadyShifted

    setTranslateShift(shift)
    // setFinalTransitionShift(shift)
    setMoveDirection(value)

    preventImagesEvents(true)
    preventGlobalScroll(true)
    setMovingFlags({ isMoving: true })

    // setOriginalShift(shift - alreadyShifted)
    // console.log(originalShift, shift, 'shift!!')
  }

  const handlerDownClick = e => {
    const startMoveValue = e.pageX || e.touches[0].pageX

    addEventListeners()

    setStartTransitionShift(startMoveValue)
    setMovingFlags({ isStarted: true })
    updatePreviousTransitionShift()
  }

  const handlerUpClick = () => {
    isAutoScroll() && shiftAutoCarousel()
    removeEventListeners()
    startAnimatedShift()

    updateCarouselShift(getTranslateShift())

    setMovingFlags({ isStarted: false, isMoving: false })
    preventImagesEvents(false)
    preventGlobalScroll(false)
  }

  const handlerMouseLeave = () => {
    if (!isMoveRunning) {
      return
    }

    // in case we lose the focus around the target we
    // need also remove all the controller logic
    removeEventListeners()
    startAnimatedShift()

    setMovingFlags({ isStarted: false, isMoving: false })
    preventImagesEvents(false)
    preventGlobalScroll(false)
  }

  const handlerClick = (isPrev = false) => {
    const currentShift = getTranslateShift()
    const currentCarouselWidth = getCarouselWidth()
    const { maxWidth } = getCarouselParams()

    const maxShift = maxWidth - currentCarouselWidth
    let shift = 0

    if (isPrev) {
      shift = -(currentShift) <= maxWidth || currentShift >= 0 ? 0 : currentShift + maxWidth
    } else {
      shift = maxShift === currentShift || -(maxShift - currentShift) <= maxWidth ? maxShift : currentShift - maxWidth
    }

    // console.log(shift, currentShift, maxWidth, currentShift - maxWidth, isPrev, 'shift')

    setTranslateShift(shift)
    updateCarouselShift(shift)
    manageTransitionPhase()
  }

  const handlerClickPrev = () => {
    handlerClick(true)
  }

  const handlerClickNext = () => {
    handlerClick(false)
  }

  if (!children || children.length === 0) {
    return null
  }

  const { capacity, maxWidth: defaultMaxWidth } = getCarouselParams()
  const carouselMaxWidth = getCarouselWidth()
  const isLessToCarousel = children && (capacity >= children.length)

  const wrapClassNameHolder = addWrap ? {
    className: classnames({
      [styles.wrap]: true,
      [stylesOverride.wrap]: stylesOverride.wrap
    })
  } : {}

  const carouselClassNameHolder = classnames({
    [styles.carouselContainer]: true,
    [styles.noArrowsMargin]: !isArrows,
    [styles.carouselContainerBasic]: isLessToCarousel,
    [stylesOverride.container]: stylesOverride.container
  })

  const carouselController = isLessToCarousel ? {} : {
    className: classnames({
      [styles.scrollWrap]: true,
      [styles.noScrollMask]: !isArrows,
      [stylesOverride.scrollWrap]: stylesOverride.scrollWrap
    }),
    onMouseDown: handlerDownClick,
    onTouchStart: handlerDownClick
  }

  const leftArrow = !isArrows ? null : (
    <Arrow
      arrowType='left'
      onClick={handlerClickPrev}
      isDisabled={isLessToCarousel || Number(currentCarouselShift) === 0}
    />
  )

  const rightArrow = !isArrows ? null : (
    <Arrow
      arrowType='right'
      onClick={handlerClickNext}
      isDisabled={isLessToCarousel || defaultMaxWidth - carouselMaxWidth === Number(currentCarouselShift)}
    />
  )

  const CustomTagName = addWrap ? 'div' : React.Fragment

  return (
    <CustomTagName {...wrapClassNameHolder}>
      {leftArrow}
      <div {...carouselController}>
        <div
          ref={carouselWrap}
          className={carouselClassNameHolder}
          style={{ transform: `translateX(${0}px)` }}
        >
          {children}
        </div>
      </div>
      {rightArrow}
    </CustomTagName>
  )
}

export default memo(Carousel)
