# Carousel - Torn


## 1.1.2
 * Revert of animationScroll preventing.

## 1.1.1
 * Improved tests and stability.

## 1.1.0
 * Added unit tests.

## 1.0.0
 * First stable release.
