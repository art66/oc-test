# Carousel - a Torn based carousel for react-apps usage.

How to use:
  - Quick Start:
  `{children}` are only required props to start. All the further works Carousel will take on itself.

```
  <Carousel>
    <span id={1}/>
    <span id={2}/>
    <span id={3}/>
    <span id={4}/>
    <span id={5}/>
    <span id={6}/>
    <span id={7}/>
    <span id={8}/>
    <span id={9}/>
  </Carousel>
```

  - Advanced:
  In case you need a custom Carousel, then throw an additional bunch of props in way you need it for:
  ---`{scrollType?: manual | auto}` - set a way in which animation will work. `{manual}` - scroll carousel on the actual user's scrolled width. `{auto}` - no matter how much user scroll, the scroll will always be scrolled to the next **viewPort** of the visible container over the Carousel.
  Example: we have: `{scrollWidth: 1000px}` and `{wrapWidth: 250px}`, then `1st viewPort: 0-250, 2nd viewPort: 250-500` and so on.
  ---`{mediaType?: 'desktop | tablet | mobile'}` - selector that says Carousel which layout should it take on. Based on the `{carouselParams}` or **default one** config.
  ---`{isArrows}` - allows to show/hide the arrows from layout.
  ---`{addWrapper}` - if not set, then Carousel will be returned like a list of elements, without any wrapper.
  ---`{minShift?}` - set a maximum coefficient by which actual scroll width will be decreased on the go (aka smooth scroll).
  ---`{maxShift?}` - set a minimum coefficient by which actual scroll width will be decreased on the go (aka smooth scroll).
  ---`{stylesOverride?}` - once thrown, can take over control about the carousel css layout

```
  const config = {
    addWrapper: false,
    isArrows: true,
    mediaType: 'desktop',
    scrollType: manual | auto,
    minShift: 2,
    maxShift: 8,
    stylesOverride: {
      wrap: 'wrapStyle',
      scroll: 'scrollStyle',
      container: 'containerStyles'
    }
  }

  const children = Array.from(Array(10).keys()).map(item => <span key={item}/>)

  <Carousel {...config}>
    {children}
  </Carousel>
```

  For more info you can visit an interface of Carousel on `./interfaces/index`. Thanks!
