export interface IProps {
  stylesOverride?: {
    wrap?: string
    scrollWrap?: string
    container?: string
  }
  addWrap?: boolean
  isArrows?: boolean
  resistanceMin?: number
  resistanceMax?: number
  scrollType?: 'auto' | 'manual'
  mediaType?: 'desktop' | 'tablet' | 'mobile'
  children: JSX.Element[] | Element[]
}
