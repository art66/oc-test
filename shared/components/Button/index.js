import PropTypes from 'prop-types'
import React, { Component } from 'react'
import cn from 'classnames'
import './button.scss'

class TornBtn extends Component {
  static defaultProps = {
    className: '',
    color: 'silver'
  }

  handleClick = (e) => {
    if (!this.props.disabled) {
      this.props.onClick && this.props.onClick(e)
    } else {
      e.preventDefault()
    }
  }

  render() {
    const { value, children, type, disabled, className, color, loading, onClick } = this.props

    return (
      <button
        type={type || 'submit'}
        className={cn(
          'torn-btn',
          className,
          color,
          {
            loading,
            disabled
          }
        )}
        onClick={e => this.handleClick(e)}
        ref={el => typeof(this.props.refFunc) === 'function' && this.props.refFunc(el)}
      >
        {value || children}
        <div className="preloader-wrap">
          <div className="dzsulb-preloader preloader-fountain">
            <div id="fountainG_1" className="fountainG" />
            <div id="fountainG_2" className="fountainG" />
            <div id="fountainG_3" className="fountainG" />
            <div id="fountainG_4" className="fountainG" />
          </div>
        </div>
      </button>
    )
  }
}

TornBtn.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  loading: PropTypes.bool,
  value: PropTypes.string,
  children: PropTypes.any,
  disabled: PropTypes.bool,
  color: PropTypes.oneOf(['silver', 'orange']),
  refFunc: PropTypes.func,
  onClick: PropTypes.func
}

export default TornBtn
