import React from 'react'
import { boolean, text } from '@storybook/addon-knobs'

import Button from '.'

export const ButtonTest = () => (
  <div className={'d'}>
    <Button value={text('Text', 'Hello')} loading={boolean('loading', true)} disabled={boolean('disabled', false)} />
  </div>
)

ButtonTest.story = {
  name: 'Button test'
}

export default {
  title: 'Shared/Button'
}
