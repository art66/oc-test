import React from 'react'
import { GoogleCharts } from 'google-charts'
import cn from 'classnames'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import { subscribeOnDarkMode, unsubscribeFromDarkMode } from '@torn/shared/utils/darkModeChecker/index'
import {
  addGradientAndText,
  mergeDataTables,
  getGraphOptions,
  pointsPreprocessing,
  runOnGraphReady,
  isEarlyFinish
} from './helpers'
import {
  CURRENT_COLOR,
  OPPONENT_COLOR,
  CURRENT_GRADIENT_URL,
  OPPONENT_GRADIENT_URL,
  MAX_GRAPH_VALUE_MULT
} from './constants'
import styles from './index.cssmodule.scss'

interface IProps {
  points: Array<Array<[number, number]>>
  respectRequirement: Array<Array<[number, number]>>
  currentFactionName: string
  opponentFactionName: string
  end: number
  mediaType: string
}

interface IState {
  isDarkMode: boolean
  isGraphDrawn: boolean
}

class RankWarGraph extends React.Component<IProps, IState> {
  private graphRef: React.RefObject<HTMLInputElement>
  private graphBottomRef: React.RefObject<HTMLInputElement>
  private chartLoaded: boolean

  constructor(props) {
    super(props)

    this.graphRef = React.createRef()
    this.graphBottomRef = React.createRef()
    this.chartLoaded = false

    this.state = {
      isDarkMode: false,
      isGraphDrawn: false
    }
  }

  componentDidMount() {
    this.loadChartAndDraw()
  }

  shouldComponentUpdate(prevProps, prevState) {
    const { points, respectRequirement, mediaType, currentFactionName, opponentFactionName } = this.props

    return !(
      points.length === prevProps.points.length
      && respectRequirement.length === prevProps.respectRequirement.length
      && mediaType === prevProps.mediaType
      && currentFactionName === prevProps.currentFactionName
      && opponentFactionName === prevProps.opponentFactionName
      && prevState.isDarkMode === this.state.isDarkMode
      && prevState.isGraphDrawn === this.state.isGraphDrawn
    )
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.mediaType !== this.props.mediaType
      || prevState.isDarkMode !== this.state.isDarkMode
      || prevState.isGraphDrawn !== this.state.isGraphDrawn
      || prevProps.respectRequirement.length !== this.props.respectRequirement.length
      || prevProps.points.length !== this.props.points.length
    ) {
      this.drawChart()
    }
  }

  componentWillUnmount() {
    unsubscribeFromDarkMode()
  }

  loadChartAndDraw() {
    try {
      if (this.chartLoaded) {
        this.drawChart()
      } else {
        this.chartLoaded = true
        GoogleCharts.load(() => {
          this.drawChart()
          subscribeOnDarkMode(isDarkMode => {
            this.setState({ isDarkMode })
          })
        })
      }
    } catch (e) {
      console.error(e)
    }
  }

  drawChart() {
    const { respectRequirement, currentFactionName, opponentFactionName, points, end, mediaType } = this.props
    const { currentLine, opponentLine, targetLine, zeroLine, maxLine } = pointsPreprocessing(
      points,
      respectRequirement,
      end
    )
    const maxGraphValue = Math.round(Number(respectRequirement[0][1]) * MAX_GRAPH_VALUE_MULT)
    const { defaultChartOptions, bottomChartOptions } = getGraphOptions(
      maxGraphValue,
      mediaType,
      this.state.isDarkMode,
      isEarlyFinish(points, end)
    )

    const currentDt = mergeDataTables([targetLine, currentLine, zeroLine, maxLine])
    const opponentDt = mergeDataTables([targetLine, opponentLine, zeroLine, maxLine])

    const graphTop = new GoogleCharts.api.visualization.ComboChart(this.graphRef.current)
    const graphBottom = new GoogleCharts.api.visualization.ComboChart(this.graphBottomRef.current)

    addGradientAndText({
      graph: graphTop,
      container: this.graphRef.current,
      checkColor: CURRENT_COLOR,
      gradientUrl: CURRENT_GRADIENT_URL,
      factionName: currentFactionName,
      textOffsetY: 0,
      mediaType
    })
    graphTop.draw(currentDt, defaultChartOptions)

    addGradientAndText({
      graph: graphBottom,
      container: this.graphBottomRef.current,
      checkColor: OPPONENT_COLOR,
      gradientUrl: OPPONENT_GRADIENT_URL,
      factionName: opponentFactionName,
      textOffsetY: -5,
      mediaType
    })
    graphBottom.draw(opponentDt, bottomChartOptions)

    runOnGraphReady(graphBottom, () => {
      this.setState({ isGraphDrawn: true })
    })
  }

  renderModeGradient() {
    if (this.state.isDarkMode) {
      return (
        <svg className={styles.svgHiden} aria-hidden='true' focusable='false'>
          <linearGradient id={CURRENT_GRADIENT_URL} x2='0' y2='1'>
            <stop offset='0%' stopColor='#6CA236' fillOpacity='0.1' />
            <stop offset='100%' stopColor='#F2F2F2' fillOpacity='0' />
          </linearGradient>
          <linearGradient id={OPPONENT_GRADIENT_URL} x2='0' y2='1'>
            <stop offset='0%' stopColor='#F2F2F2' fillOpacity='0' />
            <stop offset='100%' stopColor='#E54C19' fillOpacity='0.15' />
          </linearGradient>
        </svg>
      )
    }
    return (
      <svg className={styles.svgHiden} aria-hidden='true' focusable='false'>
        <linearGradient id={CURRENT_GRADIENT_URL} x2='0' y2='1'>
          <stop offset='0%' stopColor='#c8d999' />
          <stop offset='50%' stopColor='#eaede2' />
          <stop offset='100%' stopColor='#fff' />
        </linearGradient>
        <linearGradient id={OPPONENT_GRADIENT_URL} x2='0' y2='1'>
          <stop offset='0%' stopColor='#fff' />
          <stop offset='50%' stopColor='#f0eae6' />
          <stop offset='100%' stopColor='#f3c5b3' />
        </linearGradient>
      </svg>
    )
  }

  render() {
    const { mediaType } = this.props
    const { isGraphDrawn } = this.state

    return (
      <>
        {isGraphDrawn || (
          <div className={styles.loaderWrapper}>
            <LoadingIndicator />
          </div>
        )}
        <div className={cn(styles.graphWrapper, mediaType)}>
          {this.renderModeGradient()}
          {!isGraphDrawn || <span className={styles.scoreTitle}>Score</span>}
          <div ref={this.graphRef} />
          <div className={styles.red} ref={this.graphBottomRef} />
        </div>
      </>
    )
  }
}

export default RankWarGraph
