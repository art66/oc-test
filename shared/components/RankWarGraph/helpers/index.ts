import { GoogleCharts } from 'google-charts'
import styles from '../index.cssmodule.scss'
import {
  CURRENT_COLOR,
  OPPONENT_COLOR,
  FONT_NAME,
  CHART_AREA_WIDTH,
  LINE_BREAK_POINT_VALUE,
  LINE_ZERO_POINT_VALUE,
  MAX_GRAPH_VALUE_MULT,
  FACTION_NAME_OFFSET,
  CHART_AREA_WIDTH_PX,
  INIT_FACTIONAME_FONT_SIZE,
  SECOND,
  HOUR
} from '../constants'

const { numberFormat } = window
const htmlDecode = value => {
  const div = document.createElement('div')

  div.innerHTML = value
  return div.innerText
}
const getTextWidth = (text, fontSize) => {
  const canvas = document.createElement('canvas')
  const context = canvas.getContext('2d')

  context.font = `${fontSize}px Fjalla One`

  return context.measureText(text).width
}
const setTextAttr = (text, options) => {
  const { checkColor, factionName, mediaType } = options
  // adjust text position for different graphics
  const textY = 45 + options.textOffsetY

  text.textContent = htmlDecode(factionName)
  text.setAttribute('class', styles.factionName)
  text.setAttribute('x', `${FACTION_NAME_OFFSET[mediaType]}%`)
  text.setAttribute('y', `${textY}%`)
  text.setAttribute('fill', checkColor)
  text.setAttribute('opacity', '0.8')
  text.setAttribute('dominant-baseline', 'middle')
  text.setAttribute('text-anchor', 'middle')

  let fontSize = INIT_FACTIONAME_FONT_SIZE

  while (getTextWidth(factionName, fontSize) >= CHART_AREA_WIDTH_PX[mediaType]) {
    fontSize -= 2
  }

  if (fontSize !== INIT_FACTIONAME_FONT_SIZE) {
    text.setAttribute('style', `font-size: ${fontSize}px;`)
  }
}

export const addGradientAndText = options => {
  const { graph, container, checkColor, gradientUrl } = options
  const text = document.createElementNS('http://www.w3.org/2000/svg', 'text')

  setTextAttr(text, options)

  const listener = GoogleCharts.api.visualization.events.addListener(graph, 'ready', () => {
    const observer = new MutationObserver(() => {
      container.getElementsByTagName('svg')[0].setAttribute('xmlns', 'http://www.w3.org/2000/svg')
      Array.prototype.forEach.call(container.getElementsByTagName('path'), path => {
        if (path.getAttribute('fill').toLowerCase() === checkColor.toLowerCase()) {
          path.setAttribute('fill', `url(#${gradientUrl})`)
          path.parentElement.parentElement.prepend(text)
        }
      })
      Array.prototype.forEach.call(container.getElementsByTagName('rect'), rect => {
        if (['#c2c2c2', '#999999', '#ebebeb'].includes(rect.getAttribute('fill'))) {
          rect.remove()
        }
      })
    })

    observer.observe(container, {
      childList: true,
      subtree: true
    })

    GoogleCharts.api.visualization.events.removeListener(listener)
  })
}

export const runOnGraphReady = (graph, callback) => {
  const listener = GoogleCharts.api.visualization.events.addListener(graph, 'ready', () => {
    callback.call()
    GoogleCharts.api.visualization.events.removeListener(listener)
  })
}

const addZeroPoints = points => {
  if (points.length === 0) {
    return []
  }
  const newArray = [points[0]]

  for (let index = 1; index < points.length; index++) {
    if ((points[index - 1][1] > 0 && points[index][1] < 0) || (points[index - 1][1] < 0 && points[index][1] > 0)) {
      newArray.push([points[index][0], LINE_ZERO_POINT_VALUE])
    }
    newArray.push(points[index])
  }
  return newArray
}

const preparePoints = (points, end) => {
  const newPoints = []
  const start = points[0][0]
  const hours = HOUR
  const now = end === 0 ? Date.now() / SECOND : end

  for (let index = 0; index < points.length; index++) {
    const newPoint = [...points[index]]

    newPoint[0] = Math.round(((newPoint[0] - start) / hours) * 100) / 100

    if (index > 0) {
      newPoints.push([newPoint[0], points[index - 1][1]])
    }
    newPoints.push(newPoint)
  }
  newPoints.push([(now - start) / hours, newPoints[newPoints.length - 1][1]])

  if (isEarlyFinish(points, end)) {
    for (let j = 0; j < newPoints.length; j++) {
      newPoints[j][0] = newPoints[j][0] < 0 ? 0.001 : newPoints[j][0]
    }
  }

  return newPoints
}

const getViewColumns = dataTable => {
  const viewColumns = []

  for (let col = 0; col < dataTable.getNumberOfColumns(); col++) {
    viewColumns.push(col)

    // add tooltip column
    if (col > 0) {
      viewColumns.push({
        type: 'string',
        role: 'tooltip',
        properties: { html: true },
        calc: (dt, row) =>
          '<div class=\'ggl-tooltip\'>'
          + `<div><span=class'field'>${dt.getColumnLabel(0)}:</span> <span class='value'>
          ${Math.floor(dt.getValue(row, 0))}
          </span></div>`
          + `<div><span=class'field'>${dt.getColumnLabel(col)}:</span> <span class='value'>
          ${numberFormat(Math.floor(dt.getValue(row, col) || 0))}
          </span></div></div>`
      })
    }
  }

  return viewColumns
}

const fillNull = dataTable => {
  const numberOfColumns = dataTable.getNumberOfColumns()

  for (let i = 1; i < dataTable.getNumberOfRows(); i++) {
    for (let j = 1; j < numberOfColumns; j++) {
      if (dataTable.getValue(i, j) === null) {
        dataTable.setValue(i, j, dataTable.getValue(i - 1, j))
      }
    }
  }

  return dataTable
}

export const mergeDataTables = tables => {
  const target = GoogleCharts.api.visualization.arrayToDataTable(tables[0])
  const score = GoogleCharts.api.visualization.arrayToDataTable(tables[1])
  const zero = GoogleCharts.api.visualization.arrayToDataTable(tables[2])
  const topArea = GoogleCharts.api.visualization.arrayToDataTable(tables[3])
  let dataTable = GoogleCharts.api.visualization.data.join(target, zero, 'full', [[0, 0]], [1], [1])

  dataTable = GoogleCharts.api.visualization.data.join(dataTable, score, 'full', [[0, 0]], [1, 2], [1])

  dataTable = GoogleCharts.api.visualization.data.join(dataTable, topArea, 'full', [[0, 0]], [1, 2, 3], [1])

  dataTable = fillNull(dataTable)

  const viewColumns = getViewColumns(dataTable)
  const dataView = new GoogleCharts.api.visualization.DataView(dataTable)

  dataView.setColumns(viewColumns)

  return dataView.toDataTable()
}

export const getGraphOptions = (maxGraphValue, mediaType, isDarkMode, earlyFinish) => {
  const gridColor = isDarkMode ? '#000' : '#ccc'
  const bgFill = isDarkMode ? '#000' : '#dfdfdf'
  const zeroLineColor = isDarkMode ? '#555' : '#fff'

  const defaultChartOptions = {
    tooltip: { isHtml: true },
    isStacked: true,
    hAxis: {
      baselineColor: gridColor,
      gridlineColor: gridColor,
      gridlines: {
        color: gridColor,
        count: earlyFinish ? 0 : 8
      },
      textPosition: 'none',
      viewWindow: {
        min: 0
      }
    },
    vAxis: {
      textStyle: {
        color: '#888',
        fontName: FONT_NAME
      },
      baselineColor: gridColor,
      gridlines: {
        color: gridColor,
        count: 4
      },
      viewWindow: {
        max: maxGraphValue,
        min: 0
      }
    },
    colors: [CURRENT_COLOR, zeroLineColor, CURRENT_COLOR, '#85b201'],
    style: {
      color: '#000000'
    },
    crosshair: {
      color: '#000',
      trigger: 'selection'
    },
    backgroundColor: 'transparent',
    legend: { position: 'none' },
    chartArea: {
      top: 5,
      right: 5,
      width: `${CHART_AREA_WIDTH[mediaType]}%`,
      height: 93,
      backgroundColor: {
        fillOpacity: 0.5,
        fill: bgFill,
        stroke: gridColor,
        strokeWidth: 2
      }
    },
    height: 107,
    seriesType: 'line',
    series: {
      0: { type: 'area', lineWidth: 1 },
      1: { lineWidth: 2, tooltip: false, enableInteractivity: false },
      2: { lineWidth: 2 },
      3: { type: 'area', lineWidth: 0, tooltip: false, enableInteractivity: false, areaOpacity: 0.5 }
    }
  }

  const bottomChartOptions = {
    ...defaultChartOptions,
    vAxis: {
      textStyle: {
        color: '#888',
        fontName: FONT_NAME
      },
      gridlines: {
        color: gridColor,
        count: 4
      },
      viewWindow: {
        max: maxGraphValue,
        min: 0
      },
      baselineColor: gridColor,
      direction: -1
    },
    hAxis: {
      textStyle: {
        color: '#888',
        fontName: FONT_NAME
      },
      baselineColor: gridColor,
      gridlineColor: gridColor,
      gridlines: {
        color: gridColor,
        count: 8
      },
      textPosition: 'out',
      title: 'Hours',
      titleTextStyle: {
        color: '#888',
        fontName: 'Fjalla One',
        fontSize: 12,
        bold: false,
        italic: false
      },
      format: '#.##',
      viewWindow: {
        min: 0
      }
    },
    colors: [OPPONENT_COLOR, zeroLineColor, OPPONENT_COLOR, '#ff794b'],
    chartArea: {
      top: 0,
      right: 5,
      width: `${CHART_AREA_WIDTH[mediaType]}%`,
      height: 94,
      backgroundColor: {
        fillOpacity: 0.5,
        fill: bgFill,
        stroke: gridColor,
        strokeWidth: 2
      }
    },
    height: 126
  }

  return { defaultChartOptions, bottomChartOptions }
}

const addLineBreak = point => [point[0], point[1] === 0 ? LINE_BREAK_POINT_VALUE : point[1]]

/* eslint-disable max-statements */
export const pointsPreprocessing = (points, respectRequirement, end) => {
  const currentLine = []
  const opponentLine = []
  const newPoints = preparePoints(addZeroPoints(points), end)
  const newRr = preparePoints(respectRequirement, end)
  const last = Math.max(newPoints[newPoints.length - 1][0], newRr[newRr.length - 1][0])

  for (let index = 0; index < newPoints.length; index++) {
    const time = newPoints[index][0]
    const score = newPoints[index][1]
    const point = [time, score]

    if (Math.abs(score) < 1) {
      currentLine.push(addLineBreak(point))
      opponentLine.push(addLineBreak(point))
    } else if (score > 0) {
      currentLine.push(point)
      opponentLine.push([point[0], LINE_BREAK_POINT_VALUE])
    } else {
      point[1] = Math.abs(point[1])
      opponentLine.push(point)
      currentLine.push([point[0], LINE_BREAK_POINT_VALUE])
    }
  }

  const maxGraphValue = respectRequirement[0][1] * MAX_GRAPH_VALUE_MULT

  return {
    currentLine: [['Hour', 'Score'], ...currentLine],
    opponentLine: [['Hour', 'Score'], ...opponentLine],
    targetLine: [['Hour', 'Target'], ...newRr],
    zeroLine: [
      ['Hour', 'zero'],
      [0, 0],
      [last, 0]
    ],
    maxLine: [
      ['Hour', 'max'],
      [0, maxGraphValue],
      [last, maxGraphValue]
    ]
  }
}

export const isEarlyFinish = (points, end) => {
  if (points.length > 0 && end > 0 && points[0][0] > end) {
    return true
  }
  return false
}
