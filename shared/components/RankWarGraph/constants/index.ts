export const CURRENT_COLOR = '#85b200'
export const OPPONENT_COLOR = '#ff794c'

export const FONT_NAME = 'Fjalla One'
export const CHART_AREA_WIDTH = {
  desktop: 92,
  tablet: 86,
  mobile: 84
}

export const CHART_AREA_WIDTH_PX = {
  desktop: 700,
  tablet: 300,
  mobile: 246
}
export const INIT_FACTIONAME_FONT_SIZE = 25

export const FACTION_NAME_OFFSET = {
  desktop: 52.5,
  tablet: 55.5,
  mobile: 56.5
}

export const CURRENT_GRADIENT_URL = 'current-gradient'
export const OPPONENT_GRADIENT_URL = 'opponent-gradient'
export const LINE_BREAK_POINT_VALUE = -1000
export const LINE_ZERO_POINT_VALUE = 0.001
export const MAX_GRAPH_VALUE_MULT = 1.2

export const SECOND = 1000
export const HOUR = 3600
