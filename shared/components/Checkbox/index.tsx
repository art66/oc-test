import React, { Component, ChangeEvent } from 'react';
import './styles.scss';

type Props = {
  id: string;
  name?: string;
  checked: boolean;
  label?: string;
  containerClassName?: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => any;
};

class Checkbox extends Component<Props> {
  static defaultProps = {
    containerClassName: 'choice-container',
  };

  render() {
    const { id, name, checked, label, containerClassName, onChange } = this.props;

    return(
      <div className={containerClassName}>
        <input
          id={id}
          className="checkbox-css"
          type="checkbox"
          name={name}
          checked={checked}
          onChange={onChange}
        />
        <label className="marker-css" htmlFor={id}>{label}</label>
      </div>
    );
  }
}

export default Checkbox;
