import React from 'react'
import s from '../index.cssmodule.scss'

interface IProps {
  state: 'new' | 'active' | 'inactive'
}

const Star = (props: IProps) => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
      width='9'
      height='9'
      viewBox='0 0 9 9'
      className={s[props.state]}
    >
      <g>
        <g>
          <path
            d='M4.5,0A6.76,6.76,0,0,1,0,4.5,6.8,6.8,0,0,1,4.5,9,6.8,6.8,0,0,1,9,4.5,6.8,6.8,0,0,1,4.5,0Z'
            fill={`url(#tiers-star-gradient--${props.state})`}
          />
        </g>
      </g>
    </svg>
  )
}

export default Star
