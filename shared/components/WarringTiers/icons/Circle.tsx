import React from 'react'
import s from '../index.cssmodule.scss'

interface IProps {
  state: 'new' | 'active' | 'inactive'
}

const Circle = (props: IProps) => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
      width='7'
      height='7'
      viewBox='0 0 7 7'
      className={s[props.state]}
    >
      <g>
        <g>
          <circle cx='3.5' cy='3.5' r='3.5' fill={`url(#tiers-circle-gradient--${props.state})`} />
        </g>
      </g>
    </svg>
  )
}

export default Circle
