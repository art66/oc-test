import React from 'react'

export const SvgDefs = () => {
  return (
    <svg
      className='warring_tiers_svg_defs'
      xmlns='http://www.w3.org/2000/svg'
      width={0}
      height={0}
      style={{ position: 'absolute' }}
    >
      <defs>
        <linearGradient id='tiers-circle-gradient--active' x1='3.5' x2='3.5' y2='7.35' gradientUnits='userSpaceOnUse'>
          <stop offset='0' stopColor='#eee' />
          <stop offset='1' stopColor='#9b9b8c' />
        </linearGradient>
        <linearGradient id='tiers-circle-gradient--inactive' x1='3.5' x2='3.5' y2='7.35' gradientUnits='userSpaceOnUse'>
          <stop offset='0' stopColor='#585858' />
          <stop offset='1' stopColor='#484845' />
        </linearGradient>
        <linearGradient id='tiers-circle-gradient--new' x1='3.5' x2='3.5' y2='7.35' gradientUnits='userSpaceOnUse'>
          <stop offset='0' stopColor='#bfff00' />
          <stop offset='1' stopColor='#336600' />
        </linearGradient>
        <linearGradient
          id='tiers-star-gradient--active'
          x1='-76.45'
          y1='2039.73'
          x2='-76.45'
          y2='2038.87'
          gradientTransform='matrix(11, 0, 0, -11, 845.4, 22437.01)'
          gradientUnits='userSpaceOnUse'
        >
          <stop offset='0' stopColor='#eee' />
          <stop offset='1' stopColor='#9b9b8c' />
        </linearGradient>
        <linearGradient
          id='tiers-star-gradient--inactive'
          x1='-76.45'
          y1='2039.73'
          x2='-76.45'
          y2='2038.87'
          gradientTransform='matrix(11, 0, 0, -11, 845.4, 22437.01)'
          gradientUnits='userSpaceOnUse'
        >
          <stop offset='0' stopColor='#585858' />
          <stop offset='1' stopColor='#484845' />
        </linearGradient>
        <linearGradient
          id='tiers-star-gradient--new'
          x1='-76.45'
          y1='2039.73'
          x2='-76.45'
          y2='2038.87'
          gradientTransform='matrix(11, 0, 0, -11, 845.4, 22437.01)'
          gradientUnits='userSpaceOnUse'
        >
          <stop offset='0' stopColor='#bfff00' />
          <stop offset='1' stopColor='#336600' />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default SvgDefs
