export type TSize = 'big' | 'medium'
export type TRank = 'metal' | 'bronze' | 'silver' | 'gold' | 'platinum' | 'diamond'
