import React, { useEffect, useState } from 'react'
import cn from 'classnames'
import ReactDOM from 'react-dom'
import Tooltip from '@torn/shared/components/Tooltip'
import Circle from './icons/Circle'
import Star from './icons/Star'
import SvgDefs from './icons/defs'
import { getWarringTiersData, updateWarringTiersDataViaWS } from './modules'
import { TSize } from './types'
import s from './index.cssmodule.scss'

interface IProps {
  size: TSize
  factionId: number
  warringTiersID: string
}

const WarringTiers = (props: IProps) => {
  const [diamondRating, setDiamondRating] = useState(null)
  const [divisions, setDivisions] = useState([])
  const [rank, setRank] = useState('')
  const [tooltipTitle, setTooltipTitle] = useState('')
  const [wins, setWinsAmount] = useState(0)
  const tipStyles = {
    style: {
      background: '#444',
      boxShadow: '0px 0px 2px rgba(0, 0, 0, 0.65)',
      color: '#fff',
      textAlign: 'center',
      lineHeight: '16px'
    },
    arrowStyle: {
      color: '#444',
      borderColor: 'rgba(0, 0, 0, 0.2)'
    }
  }

  const updateWarringTiersData = data => {
    setDiamondRating(data?.diamondRating)
    setDivisions(data?.divisions)
    setRank(data?.rank)
    setTooltipTitle(data?.title)
    setWinsAmount(data?.wins)
  }

  useEffect(() => {
    getWarringTiersData(props.factionId)
      .then(data => updateWarringTiersData(data?.rankedTier))
      .catch(err => console.error(err))
  }, [props.factionId])

  useEffect(() => {
    updateWarringTiersDataViaWS(props.factionId, updateWarringTiersData)
  }, [props.factionId])

  const renderDividers = () => {
    const divs = []

    for (let i = 0; i < divisions.length; i++) {
      divs.push(
        <div key={i} className={cn(s.divisionWrapper, s[divisions[i]])}>
          <div className={s.divider} />
          {rank === 'metal' ? <Circle state={divisions[i]} /> : <Star state={divisions[i]} />}
        </div>
      )
    }

    return divs
  }

  const renderSVGDefs = () => {
    const container =
      document.getElementsByClassName('svgs_defs_container')
      && document.getElementsByClassName('svgs_defs_container')[0]

    return container ?
      ReactDOM.createPortal(<SvgDefs />, document.getElementsByClassName('svgs_defs_container')[0]) :
      null
  }

  if (!rank) return null

  return (
    <>
      <div id={props.warringTiersID} className={cn(s.warringTiers, s[props.size], s[rank])}>
        {renderSVGDefs()}
        <img
          className={s.img}
          src={`/images/v2/faction/rank/warring_tiers/${props.size}/${rank}.png`}
          alt={`${rank} rank`}
        />
        <div>{renderDividers()}</div>
        {diamondRating && diamondRating !== 0 ? (
          <div
            className={cn(s.diamondRating, {
              [s.digits2]: diamondRating > 9 && diamondRating < 100,
              [s.digits3]: diamondRating >= 100
            })}
          >
            {diamondRating}
          </div>
        ) : null}
      </div>
      <Tooltip style={tipStyles} parent={props.warringTiersID} postion='top' arrow='center'>
        <b>{tooltipTitle}</b>
        <br />
        {wins} {wins === 1 ? 'win' : 'wins'}
      </Tooltip>
    </>
  )
}

export default WarringTiers
