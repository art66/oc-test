import { fetchUrl } from '@torn/shared/utils'

declare function WebsocketHandler(namespace: string, options?: { channel: string }): void

export const getWarringTiersData = factionId =>
  fetchUrl('/page.php?sid=factionsRankedWarring', { step: 'getRankedTier', factionId })

export const updateWarringTiersDataViaWS = (factionId, callback) => {
  const handler = new WebsocketHandler('factionRankedWars', { channel: `faction-${factionId}` })

  handler.setActions({
    updateRankedTier: data => {
      callback(data)
      const event = new CustomEvent('onWarringTierUpdate', {
        detail: { rank: data.rank, title: data.title, wins: data.wins }
      })

      window.dispatchEvent(event)
    }
  })
}
