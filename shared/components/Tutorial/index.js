import PropTypes from 'prop-types'
import React, { Component } from 'react'
import cn from 'classnames'
import s from './style.cssmodule.scss'

class Tutorial extends Component {
  static propTypes = {
    header: PropTypes.string,
    content: PropTypes.string
  }
  static defaultProps = {}

  render() {
    const { header, content } = this.props
    return (
      <div className={cn(s.tutorialContainer)}>
        <span>{header}</span>
        <p>{content}</p>
      </div>
    )
  }
}

export default Tutorial
