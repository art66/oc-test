import React from 'react'
import { text } from '@storybook/addon-knobs'

import Tutorial from '.'

const styles = {
  container: {
    width: '600px',
    height: '80px'
  }
}

export const TutorialTest = () => (
  <div className="d r">
    <div style={styles.container}>
      <Tutorial header={text('Header', 'Tutorial')} content={text('Content', 'Content')} />
    </div>
  </div>
)

TutorialTest.story = {
  name: 'Tutorial test'
}

export default {
  title: 'Shared/Tutorial'
}
