import React from 'react'
import { mount } from 'enzyme'

import ProgressiveImage from '../index'

describe('ProgressiveImage', () => {
  it('should return common preloader', () => {
    const Component = mount(
      <ProgressiveImage
        image={{ name: '960/localStorage.png', path: 'images/items' }}
      />
    )

    expect(Component.find('div').at(0).prop('className')).toBe('progressiveImagePreloader ')
    expect(Component.find('AnimationLoad').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
  it('should return custom preloader', () => {
    const customPreloader = (
      <span>Hello World</span>
    )

    const Component = mount(
      <ProgressiveImage
        image={{ name: '960/localStorage.png', path: 'images/items' }}
        preloader={{ isPrlActive: true, prlComponent: customPreloader }}
      />
    )

    expect(Component).toMatchSnapshot()

    expect(Component.find('span').at(0).text()).toBe('Hello World')

    expect(Component).toMatchSnapshot()
  })
  it('should return preloader with image by ID and tooltip', () => {
    const Component = mount(
      <ProgressiveImage
        image={{ name: '960/localStorage.png', path: 'images/items', ID: '960/localStorage.png' }}
        tooltip={{ ID: '960/localStorage.png', child: 'Hello World' }}
      />
    )

    expect(Component.find('img').prop('id')).toBe('960/localStorage.png')

    expect(Component).toMatchSnapshot()
  })
})
