# PictureGenerator - Torn


## 2.1.5
 * Changed span on div container for semantic purposes.

## 2.1.4
 * Fixed default class for image render.

## 2.1.3
 * Improved default props.

## 2.1.2
 * Improved placeholder layout logic.

## 2.1.3
 * Improved storybook tests.

## 2.1.2
 * Updated readme.

## 2.1.1
 * Updated storybook config.
 * Fixed memory leak.

## 2.0.0
 * Written tests suits.
 * Moved to the shared/Components.
 * Added PureComponent extending way.

## 1.0.0
 * First stable release.
