import React from 'react'
import { text, number } from '@storybook/addon-knobs'

import Tooltips from '@torn/shared/components/TooltipNew'

import ProgressiveImage from './index'
import styles from './mocks/index.cssmodule.scss'

export const Defaults = () => {
  const imageName = '16/large.png'

  const config = {
    image: {
      path: '/images/items/',
      name: imageName,
      ID: imageName
    }
  }

  return <ProgressiveImage {...config} />
}

Defaults.story = {
  name: 'defaults'
}

export const WithCustomClasses = () => {
  const imageName = '16/large.png'

  const config = {
    image: {
      path: text('Image Path', '/images/items/'),
      name: text('Image Name', imageName),
      ID: text('Image ID', imageName),
      imgClass: styles.imageWeapon
    },
    preloader: {
      prlClass: styles.imageWeaponPreloader
    }
  }

  return <ProgressiveImage {...config} />
}

WithCustomClasses.story = {
  name: 'with custom classes'
}

export const WithTooltipOnHover = () => {
  const imageName = '16/large.png'

  const config = {
    image: {
      path: '/images/items/',
      name: imageName,
      ID: imageName,
      imgClass: styles.imageWeapon
    },
    preloader: {
      prlClass: styles.imageWeaponPreloader
    },
    tooltip: {
      child: text('Tooltip Child', 'MTR'),
      ID: text('Tooltip ID', imageName),
      customConfig: {
        manualCoodsFix: {
          fixX: number('Tooltips Coords Fix', -5)
        }
      }
    }
  }

  const TooltipWrapper = ({ children }) => {
    return children
  }

  return (
    <TooltipWrapper>
      <Tooltips />
      <ProgressiveImage {...config} />
    </TooltipWrapper>
  )
}

WithTooltipOnHover.story = {
  name: 'with tooltip (on hover)'
}

export default {
  title: 'Shared/ProgressiveImage'
}
