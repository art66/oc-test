# Progressive Image - powerful component for handling image preloading stage in various ways.

How to use:
  - Quick Start:
  `{path}, {name}` are only required props to start image preloading. You can don't keep in mind the pixels and resolutions to render, by defoult it will always render images in whole 1-4x pixel resolutions for all mediaBreakpoints (desktop, tablet, mobile).

```
  const config = {
    path: '/images/items/',
    name: '89_large.png'
  }

  <ProgressiveImage image={...config} />
```

  - Advanced:
  In case you need to make a custom set up, you can config all the properties based on the interface of the component on `./interfaces/index.ts` and configure your progressive image in way like:

```
  <ProgressiveImagesSetGenerator image={...} preloader={...} tooltip={...} />
```
