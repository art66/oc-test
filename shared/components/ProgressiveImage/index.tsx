import React, { memo, useState, useCallback, useEffect } from 'react'
import AnimationLoad from '@torn/shared/components/AnimationLoad'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'

import styles from './index.cssmodule.scss'
import { IProps } from './interfaces'

/**
 *  @name ProgressiveImage
 *  @author 3p-sviat
 *  @version 1.0.0
 *  @description Powerful component for handling image preloading stage in various ways
 *
 *  @typedef {Object} IProps
 *
 *  @params {object} config - with image, preloader and tooltip params
 *  @params {object} config.image - image params config
 *  @params {object} config.image.name - real image name, including its extension
 *  @params {object} config.image.path [path=''] - real image path on dev
 *  @params {object} config.image.ID [ID=''] - can set an id for tooltip on image
 *  @params {object} config.image.imgClass [imgClass=''] - custom class for an image layout
 *  @params {object} config.preloader [preloader={}] - preloader params config
 *  @params {object} config.preloader.isPrlActive [isPrlActive=true] - main flag for active/deactivate preloader from being shown
 *  @params {object} config.preloader.dotsCount [dotsCount=3] - dots count for default preloader
 *  @params {object} config.preloader.dotsColor [dotsColor='black'] - dots color for default preloader
 *  @params {object} config.preloader.prlComponent [prlComponent=null] - ability to thrown your own preloader component
 *  @params {object} config.preloader.prlClass [prlClass=''] - custom class for preloader component
 *  @params {object} config.tooltip - a tooltip config for our TooltipsNew component for animation showing.

 *  @return {JSX.Element} react node - a real img or span tag based on the current onload img stage
 *
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const ProgressiveImage = (props: IProps) => {
  const { image, preloader, tooltip } = props
  const { path = '', name, ID = '', imgClass = '' } = image
  const { isPrlActive = true, dotsCount = 3, dotsColor = 'black', prlComponent = null, prlClass = '' } = preloader || {}

  const [imageLoadStatus, setImageLoadStatus] = useState('')

  useEffect(() => {
    if (!ID || !tooltip || ID !== tooltip.ID || Object.keys(tooltip).length === 0) {
      return
    }

    tooltipsSubscriber.subscribe(tooltip)
  }, [tooltip])

  const handlerLoaded = useCallback(() => setImageLoadStatus('loaded'), [])

  const imageToRender = (
    <img
      alt=''
      id={ID && String(ID) || ''}
      src={`${path}${name}`}
      className={`${styles.progressiveImage} ${imgClass}`}
      onLoad={handlerLoaded}
      style={!imageLoadStatus ? { display: 'none' } : {}}
    />
  )

  const spinner = prlComponent || (
    <AnimationLoad
      dotsCount={dotsCount}
      dotsColor={dotsColor}
      isAdaptive={true}
    />
  )

  if (!imageLoadStatus && isPrlActive && spinner) {
    return (
      <div className={`${styles.progressiveImagePreloader} ${prlClass}`}>
        {imageToRender}
        {spinner}
      </div>
    )
  }

  return imageToRender
}

export default memo(ProgressiveImage)
