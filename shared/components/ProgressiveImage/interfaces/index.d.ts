import { ITooltips } from '@torn/shared/components/TooltipNew/types/index'

export interface IProps {
  image: {
    name: string
    path?: string
    ID?: number | string
    imgClass?: string
  }
  preloader?: {
    isPrlActive?: boolean
    prlComponent?: JSX.Element
    dotsCount?: number
    dotsColor?: 'black' | 'white'
    prlClass?: string
  }
  tooltip?: ITooltips
}
