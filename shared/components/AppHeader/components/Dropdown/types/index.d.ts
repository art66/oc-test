export interface IProps {
  children: React.ReactChild
  className?: string
}

export interface IStore {
  active: boolean
}
