import React, { PureComponent } from 'react'
import cn from 'classnames'

import List from './List'
import ListItem from './ListItem'

import { IProps, IStore } from './types'
import styles from './index.cssmodule.scss'

export class Dropdown extends PureComponent<IProps, IStore> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      active: false
    }
  }

  _getActivatorLabel = () => {
    const { active } = this.state

    return `${active ? 'Hide' : 'Show'} links`
  }

  _toggleActivation = () => {
    const { active } = this.state

    this.setState({ active: !active })
  }

  render() {
    const { children, className } = this.props
    const { active } = this.state

    return (
      <div className={cn(styles.dropdown, className)}>
        <button
          className={cn(styles.activator, { [styles.active]: active })}
          onClick={this._toggleActivation}
          aria-label={this._getActivatorLabel()}
        >
          <div className={styles.activatorIcon} />
        </button>
        {active && children}
      </div>
    )
  }
}

export default Dropdown

export { List, ListItem }
