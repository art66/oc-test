import React, { PureComponent } from 'react'

import { IProps } from './types/list'
import styles from './list.cssmodule.scss'

class List extends PureComponent<IProps> {
  render() {
    const { children } = this.props

    return <div className={styles.list}>{children}</div>
  }
}

export default List
