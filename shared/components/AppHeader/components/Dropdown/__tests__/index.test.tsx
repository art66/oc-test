import React from 'react'
import { mount } from 'enzyme'
import Dropdown from '../index'

describe('<Dropdown />', () => {
  it('should render not active', () => {
    const Children = () => <div className='unique' />
    const Component = mount(
      <Dropdown>
        <Children />
      </Dropdown>
    )

    expect(Component.find('.dropdown').length).toBe(1)
    expect(Component.find('.activator').length).toBe(1)

    expect(Component.find('.activator.active').length).toBe(0)
    expect(Component.find('.content').length).toBe(0)
    expect(Component.find('.content').find(Children).length).toBe(0)

    expect(Component).toMatchSnapshot()
  })

  it('should render content when active', () => {
    const Children = () => <div className='unique' />
    const Component = mount(
      <Dropdown>
        <Children />
      </Dropdown>
    )
    Component.setState({ active: true })

    expect(Component.find('.dropdown').length).toBe(1)
    expect(Component.find('.activator').length).toBe(1)
    expect(Component.find('.activator.active').length).toBe(1)
    expect(Component.find(Children).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should toggle on activator button click', () => {
    const Children = () => <div className='unique' />
    const Component = mount(
      <Dropdown>
        <Children />
      </Dropdown>
    )

    expect(Component.find('.activator').length).toBe(1)

    Component.find('.activator').simulate('click')

    expect(Component.state('active')).toBe(true)

    Component.find('.activator').simulate('click')

    expect(Component.state('active')).toBe(false)

    expect(Component).toMatchSnapshot()
  })
})
