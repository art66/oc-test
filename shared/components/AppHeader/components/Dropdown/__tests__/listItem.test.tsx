import React from 'react'
import { mount } from 'enzyme'
import ListItem from '../ListItem'

describe('<List />', () => {
  it('should render list items', () => {
    const Children = () => <div className='unique' />
    const Component = mount(
      <ListItem>
        <Children />
      </ListItem>
    )

    expect(Component.find('.listItem').length).toBe(1)
    expect(Component.find(Children).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })
})
