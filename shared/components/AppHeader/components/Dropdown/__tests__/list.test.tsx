import React from 'react'
import { mount } from 'enzyme'
import List from '../List'
import ListItem from '../ListItem'

describe('<List />', () => {
  it('should render list items', () => {
    const Children = () => <div className='unique' />
    const Component = mount(
      <List>
        <ListItem>
          <Children />
        </ListItem>
        <ListItem>
          <Children />
        </ListItem>
      </List>
    )

    expect(Component.find('.list').length).toBe(1)
    expect(Component.find(ListItem).length).toBe(2)

    expect(Component).toMatchSnapshot()
  })
})
