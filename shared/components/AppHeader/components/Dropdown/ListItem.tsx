import React, { PureComponent } from 'react'

import { IProps } from './types/listItem'
import styles from './listItem.cssmodule.scss'

class ListItem extends PureComponent<IProps> {
  render() {
    const { children } = this.props

    return <div className={styles.listItem}>{children}</div>
  }
}

export default ListItem
