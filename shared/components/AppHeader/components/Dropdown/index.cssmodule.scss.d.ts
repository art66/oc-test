// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'activator': string;
  'activatorIcon': string;
  'active': string;
  'dropdown': string;
  'globalSvgShadow': string;
}
export const cssExports: CssExports;
export default cssExports;
