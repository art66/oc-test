import React from 'react'
import cn from 'classnames'
import { Link as SPALink } from 'react-router-dom'
import { SVGIconGenerator } from '@torn/shared/SVG'

import getSVGIconConfig from '../../utils/getSVGIconConfig'
import { IProps, IState } from './types/link'
import styles from './link.cssmodule.scss'
import globalStyles from '../../styles/index.cssmodule.scss'

class Link extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    title: '',
    label: '',
    href: '',
    icon: ''
  }

  _onClick = event => {
    const { id, onClick, callback } = this.props

    if (onClick) {
      onClick(id, event)
    }

    if (callback) {
      callback(id, event)
    }
  }

  _generateClassNames = () => {
    const { icon, isDropDownLayout } = this.props

    const wrapClasses = cn(
      styles.linkContainer,
      !isDropDownLayout ? styles.inRow : styles.inColumn,
      {
        [globalStyles.greyLineV]: !isDropDownLayout
      },
      `link-container-${icon}`
    )

    return wrapClasses
  }

  render() {
    const { title, label, href, icon, isSPALink, isDropDownLayout } = this.props

    if (!title && !icon) return null

    const CustomTag = isSPALink ? SPALink : 'a'
    const className = cn(
      {
        [globalStyles.isDropDownLayout]: isDropDownLayout
      },
      styles.svgIcon
    )

    return (
      <CustomTag
        to={href}
        role='button'
        aria-labelledby={label}
        href={href}
        onClick={this._onClick}
        className={this._generateClassNames()}
      >
        <span className={cn(globalStyles.iconContainer, styles.linkIconContainer)}>
          <SVGIconGenerator {...getSVGIconConfig(icon, className)} />
        </span>
        <span className={styles.linkTitle}>{title}</span>
      </CustomTag>
    )
  }
}

export default Link
