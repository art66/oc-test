import React from 'react'
import { shallow } from 'enzyme'
import initialState, { empty, isSPALink } from './mocks/link'
import Link from '../Link'

describe('<Link />', () => {
  it('should render Link component', () => {
    const Component = shallow<Link>(<Link {...initialState} />)
    const component = Component.instance()

    const LinkComponent = Component.find('.linkContainer')
    expect(LinkComponent.length).toBe(1)
    expect(LinkComponent.prop('onClick')).toBe(component._onClick)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })

  it('should render SPA Link component in case of isSPALink flag provided', () => {
    const Component = shallow<Link>(<Link {...isSPALink} />)
    const component = Component.instance()

    const LinkComponent = Component.find('.linkContainer')
    expect(LinkComponent.length).toBe(1)
    expect(LinkComponent.prop('onClick')).toBe(component._onClick)
    expect(Component.find('Link').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })

  it('should not render Link component in case of empty data', () => {
    const Component = shallow<Link>(<Link {...empty} />)

    expect(Component.find('.linkContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })

  it('should handle onClick and callback listeners on click', () => {
    const onClick = jest.fn()
    const callback = jest.fn()
    const state = { ...initialState, onClick, callback }
    const Component = shallow<Link>(<Link {...state} />)
    const component = Component.instance()

    const LinkComponent = Component.find('.linkContainer')
    expect(LinkComponent.length).toBe(1)
    expect(LinkComponent.prop('onClick')).toBe(component._onClick)

    LinkComponent.simulate('click', '8968')

    expect(onClick).toHaveBeenCalledTimes(1)
    expect(onClick.mock.calls[0][0]).toBe('5')
    expect(onClick.mock.calls[0][1]).toBe('8968')

    expect(callback).toHaveBeenCalledTimes(1)
    expect(callback.mock.calls[0][0]).toBe('5')
    expect(callback.mock.calls[0][1]).toBe('8968')

    expect(onClick).toMatchSnapshot()
    expect(callback).toMatchSnapshot()
  })

  it('should handle onClick listener and avoid callback on click', () => {
    const onClick = jest.fn()
    const state = { ...initialState, onClick }
    const Component = shallow<Link>(<Link {...state} />)
    const component = Component.instance()

    const LinkComponent = Component.find('.linkContainer')
    expect(LinkComponent.length).toBe(1)
    expect(LinkComponent.prop('onClick')).toBe(component._onClick)

    LinkComponent.simulate('click', '8968')

    expect(onClick).toHaveBeenCalledTimes(1)
    expect(onClick.mock.calls[0][0]).toBe('5')
    expect(onClick.mock.calls[0][1]).toBe('8968')

    expect(onClick).toMatchSnapshot()
  })

  it('should handle callback listener and avoid onClick on click', () => {
    const callback = jest.fn()
    const state = { ...initialState, callback }
    const Component = shallow<Link>(<Link {...state} />)
    const component = Component.instance()

    const LinkComponent = Component.find('.linkContainer')
    expect(LinkComponent.length).toBe(1)
    expect(LinkComponent.prop('onClick')).toBe(component._onClick)

    LinkComponent.simulate('click', '8968')

    expect(callback).toHaveBeenCalledTimes(1)
    expect(callback.mock.calls[0][0]).toBe('5')
    expect(callback.mock.calls[0][1]).toBe('8968')

    expect(callback).toMatchSnapshot()
  })

  it('should avoid callback and onClick listeners on click', () => {
    const callback = jest.fn()
    const state = { ...initialState, callback }
    const Component = shallow<Link>(<Link {...state} />)
    const component = Component.instance()

    const LinkComponent = Component.find('.linkContainer')
    expect(LinkComponent.length).toBe(1)
    expect(LinkComponent.prop('onClick')).toBe(component._onClick)

    LinkComponent.simulate('click', '8968')

    expect(callback).toHaveBeenCalledTimes(1)
    expect(callback.mock.calls[0][0]).toBe('5')
    expect(callback.mock.calls[0][1]).toBe('8968')

    expect(callback).toMatchSnapshot()
  })
})
