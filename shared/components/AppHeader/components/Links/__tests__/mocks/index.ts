import { IProps } from '../../types'

const initialState: IProps = {
  pageID: 1,
  callback: () => {},
  links: {
    active: true,
    default: {
      ID: 0,
      items: [
        {
          id: '0',
          title: 'Last Games',
          href: 'loader.php?sid=kenoLastGames',
          label: 'last-spins',
          icon: 'LastRolls'
        },
        {
          id: '1',
          title: 'Company Profile',
          href: 'loader.php?sid=kenoLastGames',
          label: 'last-spins',
          icon: 'CompanyProfile'
        }
      ]
    },
    list: [
      {
        ID: 1,
        items: [
          {
            id: '0',
            title: 'Last Games',
            href: 'loader.php?sid=kenoLastGames',
            label: 'last-spins',
            icon: 'LastRolls'
          },
          {
            id: '1',
            title: 'Company Profile',
            href: 'loader.php?sid=kenoLastGames',
            label: 'last-spins',
            icon: 'CompanyProfile'
          }
        ]
      }
    ]
  }
}

export const empty: IProps = {
  ...initialState,
  links: {}
}

export const dropDownLayout: IProps = {
  ...initialState,
  links: {
    ...initialState.links,
    list: [
      ...initialState.links.list.map(item =>
        item.ID === 1
          ? {
              ...item,
              isDropDownLayout: true
            }
          : item
      )
    ]
  }
}

export default initialState
