const initialState = {
  id: '5',
  title: 'Last Games',
  href: 'loader.php?sid=kenoLastGames',
  label: 'last-spins',
  icon: 'last-spins-icon'
}

export const empty = {
  ...initialState,
  title: '',
  icon: ''
}

export const isSPALink = {
  ...initialState,
  isSPALink: true
}

export default initialState
