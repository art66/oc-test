import React from 'react'
import { shallow } from 'enzyme'
import initialState, { empty, dropDownLayout } from './mocks/index'
import Links from '../index'
import Link from '../Link'
import Dropdown, { List, ListItem } from '../../Dropdown'

describe('<Links />', () => {
  it('should render Links component with two nested Link Components', () => {
    const Component = shallow(<Links {...initialState} />)

    expect(Component.prop('className')).toBe('linksContainer')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    ComponentChildren.forEach((LinkComponent, index) => {
      expect(LinkComponent.is(Link)).toBeTruthy()
      expect(LinkComponent.key()).toBe(index.toString())
      expect(LinkComponent.prop('id')).toBe(initialState.links.list[0].items[index].id)
      expect(LinkComponent.prop('title')).toBe(initialState.links.list[0].items[index].title)
      expect(LinkComponent.prop('href')).toBe(initialState.links.list[0].items[index].href)
      expect(LinkComponent.prop('label')).toBe(initialState.links.list[0].items[index].label)
      expect(LinkComponent.prop('icon')).toBe(initialState.links.list[0].items[index].icon)
      expect(LinkComponent.prop('title')).toBe(initialState.links.list[0].items[index].title)
      expect(LinkComponent.prop('callback')).toBe(initialState.callback)
    })

    expect(Component).toMatchSnapshot()
  })

  it('should not render Links component in case of empty data', () => {
    const Component = shallow(<Links {...empty} />)

    expect(Component.find('.linksContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })

  it('should render Links component in Dropdown mode', () => {
    const Component = shallow(<Links {...dropDownLayout} />)

    expect(Component.is(Dropdown)).toBeTruthy()
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(1)
    const ListComponent = ComponentChildren.at(0)
    expect(ListComponent.is(List)).toBeTruthy()
    const ListChildren = ListComponent.children()
    expect(ListChildren.length).toBe(2)
    ListChildren.forEach((ListItemComponent, index) => {
      expect(ListItemComponent.is(ListItem)).toBeTruthy()
      expect(ListItemComponent.key()).toBe(index.toString())
      const ListItemChildren = ListItemComponent.children()
      expect(ListItemChildren.length).toBe(1)
      const LinkComponent = ListItemChildren.at(0)
      expect(LinkComponent.is(Link)).toBeTruthy()
      expect(LinkComponent.prop('id')).toBe(initialState.links.list[0].items[index].id)
      expect(LinkComponent.prop('title')).toBe(initialState.links.list[0].items[index].title)
      expect(LinkComponent.prop('href')).toBe(initialState.links.list[0].items[index].href)
      expect(LinkComponent.prop('label')).toBe(initialState.links.list[0].items[index].label)
      expect(LinkComponent.prop('icon')).toBe(initialState.links.list[0].items[index].icon)
      expect(LinkComponent.prop('title')).toBe(initialState.links.list[0].items[index].title)
      expect(LinkComponent.prop('callback')).toBe(initialState.callback)
    })

    expect(Component).toMatchSnapshot()
  })
})
