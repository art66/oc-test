import React, { PureComponent } from 'react'
import Link from './Link'
import Dropdown, { List, ListItem } from '../Dropdown'
import { IProps } from './types'
import payloadFilter from '../../utils/payloadFilter'
import styles from './index.cssmodule.scss'

class Links extends PureComponent<IProps> {
  static defaultProps: IProps = {
    pageID: 0
  }

  _getPageLinks() {
    const { pageID, links } = this.props

    if (links === null || Object.keys(links).length === 0) {
      return null
    }

    const pageLinks = payloadFilter(links, pageID)

    return pageLinks
  }

  _renderLinksRow() {
    const { callback } = this.props
    const pageLinks = this._getPageLinks()

    if (!pageLinks || !pageLinks.items) {
      return null
    }

    if (pageLinks.isDropDownLayout) {
      return pageLinks.items.map((link, i) => (
        <ListItem key={i}>
          <Link {...link} callback={callback} isDropDownLayout={pageLinks.isDropDownLayout} />
        </ListItem>
      ))
    }

    return pageLinks.items.map((link, i) => <Link key={i} {...link} callback={callback} />)
  }

  render() {
    const pageLinks = this._getPageLinks()

    if (!pageLinks || !pageLinks.items) {
      return null
    }

    const { isDropDownLayout = false } = pageLinks

    if (isDropDownLayout) {
      return (
        <Dropdown className={styles.linksDropdown}>
          <List>{this._renderLinksRow()}</List>
        </Dropdown>
      )
    }

    return <div className={styles.linksContainer}>{this._renderLinksRow()}</div>
  }
}

export default Links
