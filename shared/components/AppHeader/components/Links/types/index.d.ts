export type TLinksListItem = {
  ID: number
  items: {
    id?: string
    title: string
    label: string
    href: string
    icon: string
    onClick?: (id?: string, event?: React.MouseEvent) => void
  }[]
}

export interface ILinks {
  active?: boolean
  default?: TLinksListItem
  list?: TLinksListItem[]
}

export interface IProps {
  pageID?: number
  links?: ILinks
  callback?: (id?: string, event?: React.MouseEvent) => void
}
