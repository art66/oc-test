export interface IProps {
  id?: string
  title?: string
  label?: string
  href?: string
  icon?: string
  isSPALink?: boolean
  isDropDownLayout?: boolean
  onClick?: (id?: string, event?: React.MouseEvent) => void
  callback?: (id?: string, event?: React.MouseEvent) => void
}

export interface IState {
  onHover: boolean
}
