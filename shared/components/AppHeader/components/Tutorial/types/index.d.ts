export interface ITutorials {
  list?: {
    ID?: number
    items?: {
      title?: string
      text?: string
    }[]
  }[]
  active?: boolean
  default?: {
    ID?: number
    items: {
      title: string
      text: string
    }[]
  }
  activateOnLoad?: boolean
  hideByDefault?: boolean
}

export interface IProps {
  pageID?: number
  showTutorial?: boolean
  tutorials?: ITutorials
}

export interface IMethods {
  getCurrentTutorial: () => any
}
