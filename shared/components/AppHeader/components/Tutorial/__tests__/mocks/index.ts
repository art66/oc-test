const initialState = {
  pageID: 1,
  showTutorial: true,
  checkTutorial: () => {},
  tutorials: {
    list: [
      {
        ID: 1,
        items: [
          {
            title: 'Tutorial totorial',
            text: `Lorem ipsum dolor sit amet, ea diam consul consulatu est.
            No nam velit malorum minimum, est eu esse laoreet impedit, ei eirmod corrumpit
            ullamcorper nam. An iuvaret alienum efficiantur vix`
          }
        ]
      }
    ],
    active: false,
    default: {
      ID: 0,
      items: [
        {
          title: 'Tutorial title',
          text: 'Tutorial text'
        }
      ]
    },
    activateOnLoad: false,
    hideByDefault: false
  }
}

export const disabled = {
  ...initialState,
  showTutorial: false
}

export const empty = {
  ...initialState,
  showTutorial: true,
  tutorials: {
    ...initialState.tutorials,
    list: [
      {
        ID: 1,
        items: [
          {
            title: '',
            text: ''
          }
        ]
      }
    ]
  }
}

export default initialState
