import React from 'react'
import { shallow } from 'enzyme'
import initialState, { disabled, empty } from './mocks'
import Tutorial from '../index'

describe('<Tutorial />', () => {
  it('should render Tutorial component', () => {
    const Component = shallow(<Tutorial {...initialState} />)

    expect(Component.find('.tContainer').length).toBe(1)
    expect(Component.find('.tHeader').length).toBe(1)
    expect(Component.find('.tHeader').text()).toBe('Tutorial totorial')
    expect(Component.find('.tText').length).toBe(1)
    expect(Component.find('.tText').text()).toBe(initialState.tutorials.list[0].items[0].text)
    expect(Component).toMatchSnapshot()
  })
  it('should not render Tutorial component in case of disabled', () => {
    const Component = shallow(<Tutorial {...disabled} />)

    expect(Component.find('.tContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render Tutorial component in case of empty data', () => {
    const Component = shallow(<Tutorial {...empty} />)

    expect(Component.find('.tContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
