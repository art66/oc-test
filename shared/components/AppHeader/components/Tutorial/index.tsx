import { SVGIconGenerator } from '@torn/shared/SVG'
import React, { PureComponent } from 'react'
import payloadFilter from '../../utils/payloadFilter'
import styles from './index.cssmodule.scss'
import { IMethods, IProps } from './types'

const MOCK_TITLE = 'No Title for this page!'
const MOCK_TEXT = 'No Tutorial for this page!'

class Tutorial extends PureComponent<IProps> implements IMethods {
  static defaultProps: IProps = {
    pageID: 0,
    showTutorial: false,
    tutorials: {
      list: [
        {
          ID: 0,
          items: [
            {
              title: 'string',
              text: 'string'
            }
          ]
        }
      ],
      active: false,
      default: {
        ID: 0,
        items: [
          {
            title: 'string',
            text: 'string'
          }
        ]
      },
      activateOnLoad: false,
      hideByDefault: false
    }
  }

  getCurrentTutorial = () => {
    const { tutorials, pageID } = this.props

    const tutorial = payloadFilter(tutorials, pageID)

    return tutorial
  }

  render() {
    const { showTutorial } = this.props
    const tutorial = this.getCurrentTutorial()

    if (!tutorial) {
      return null
    }

    const { items } = tutorial
    const noItemsProvided = !items || (!items[0].title && !items[0].text)

    if (!showTutorial || noItemsProvided) {
      return null
    }

    const { title, text } = items[0]

    return (
      <div className={styles.tContainer}>
        <div className={styles.topSection}>
          <span className={styles.iconContainer}>
            <SVGIconGenerator
              iconName='Tutorial'
              type='show'
              fill={{ color: 'rgb(207, 207, 207)', strokeWidth: 0 }}
              dimensions={{ width: 11, height: 16, viewbox: '0 1 11 17' }}
              filter={{ active: false }}
            />
          </span>
          <span className={styles.tHeader}>{title || MOCK_TITLE}</span>
        </div>
        <div className={styles.bottomSection}>
          <p className={styles.tText}>{text || MOCK_TEXT}</p>
        </div>
        <hr className={styles.delimiter} />
      </div>
    )
  }
}

export default Tutorial
