const initialState = {
  pageID: 1,
  titles: {
    default: {
      ID: 0,
      title: 'Tutorials'
    },
    list: []
  },
  tutorialSwitcher: () => {},
  switchIcon: false,
  showSwitcher: true
}

export const withSubtitle = {
  ...initialState,
  titles: {
    ...initialState.titles,
    list: [
      {
        ID: 1,
        subTitle: 'SubTitle'
      }
    ]
  }
}

export const empty = {
  ...initialState,
  titles: {
    ...initialState.titles,
    default: {
      ID: 0,
      title: ''
    }
  }
}

export default initialState
