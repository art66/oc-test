import React from 'react'
import { shallow } from 'enzyme'
import initialState, { withSubtitle, empty } from './mocks'
import Title from '../index'

describe('<Title />', () => {
  it('should render Title component', () => {
    const Component = shallow(<Title {...initialState} />)

    expect(Component.find('.titleContainer').length).toBe(1)
    expect(Component.find('.title').length).toBe(1)
    expect(Component.find('.title').text()).toBe('Tutorials')
    expect(Component).toMatchSnapshot()
  })
  // it('should render Title componen with title and subTitle', () => {
  //   const Component = shallow(<Title {...withSubtitle} />)

  //   expect(Component.find('.titleContainer').length).toBe(1)
  //   expect(Component.find('.title').length).toBe(1)
  //   expect(Component.find('.title').text()).toBe('Tutorials - SubTitle')
  //   expect(Component).toMatchSnapshot()
  // })
  it('should render Title component without title, but subTitle only', () => {
    const Component = shallow(<Title {...withSubtitle} />)

    expect(Component.find('.titleContainer').length).toBe(1)
    expect(Component.find('.title').length).toBe(1)
    expect(Component.find('.title').text()).toBe('SubTitle')
    expect(Component).toMatchSnapshot()
  })
  it('should not render Title component in case of empty data', () => {
    const Component = shallow(<Title {...empty} />)

    expect(Component.find('.titleContainer').length).toBe(1)
    expect(Component.find('.title').length).toBe(1)
    expect(Component.find('.title').text()).toBe('')
    expect(Component).toMatchSnapshot()
  })
})
