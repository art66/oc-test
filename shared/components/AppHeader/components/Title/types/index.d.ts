export interface IProps {
  pageID?: number
  titles?: {
    default: {
      ID: number
      title: string
    }
    list: {
      ID: number
      subTitle: string
    }[]
  }
}
