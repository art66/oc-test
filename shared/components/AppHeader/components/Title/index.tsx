import React, { PureComponent } from 'react'
import styles from './index.cssmodule.scss'
import { IProps } from './types'

type TTitle = {
  subTitle?: string
  ID?: number
}

class Title extends PureComponent<IProps> {
  static defaultProps = {
    titles: []
  }

  _getTitle = () => {
    const { titles } = this.props
    const { title = '' } = titles.default || {}

    return title
  }

  _getSubTitle() {
    const {
      pageID,
      titles: { list = [] }
    } = this.props

    if (!list) {
      return ''
    }

    const title = list.find((pageTitle: TTitle) => pageTitle.ID === pageID)

    if (!title || !title.subTitle) {
      return ''
    }

    return title.subTitle
  }

  render() {
    const { titles } = this.props

    if (!titles) {
      console.error('NO TITLES PROVIDED! PLEASE, CHECK THE TITLE COMPONENT.')

      return null
    }

    const title = this._getTitle()
    const subTitle = this._getSubTitle()

    return (
      <div className={styles.titleContainer}>
        <h4 className={styles.title}>
          {subTitle ? subTitle : title}
        </h4>
      </div>
    )
  }
}

export default Title
