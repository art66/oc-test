const initialState = {
  title: '0 / 25',
  icon: 'Mods'
}

export const empty = {
  ...initialState,
  title: '',
  icon: ''
}

export default initialState
