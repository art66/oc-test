const initialState = {
  pageID: 1,
  labels: {
    active: true,
    default: {
      ID: 0,
      items: [
        {
          title: 'Test',
          icon: 'Test'
        }
      ]
    },
    list: [
      {
        ID: 1,
        items: [
          {
            title: 'Time',
            icon: 'Send'
          },
          {
            title: 'Mods',
            icon: 'Mods'
          }
        ]
      }
    ]
  }
}

export const empty = {
  ...initialState,
  labels: null
}

export default initialState
