import React from 'react'
import { shallow } from 'enzyme'
import initialState, { empty } from './mocks/label'
import Label from '../Label'

describe('<Label />', () => {
  it('should render Label component', () => {
    const Component = shallow(<Label {...initialState} />)

    expect(Component.find('.labelContainer').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should not render Label component in case of empty data', () => {
    const Component = shallow(<Label {...empty} />)

    expect(Component.find('.labelContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
