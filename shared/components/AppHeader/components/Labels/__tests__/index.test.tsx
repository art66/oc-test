import React from 'react'
import { mount } from 'enzyme'
import initialState, { empty } from './mocks/index'
import Labels from '../index'

describe('<Labels />', () => {
  it('should render lables component with two nested Label Components', () => {
    const Component = mount(<Labels {...initialState} />)

    expect(Component.find('.labelsContainer').length).toBe(1)
    expect(Component.find('.labelContainer').length).toBe(2)
    expect(Component.find('.iconContainer').length).toBe(2)
    expect(Component.find('SVGIconGenerator').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should not render Labels component in case of empty data', () => {
    const Component = mount(<Labels {...empty} />)

    expect(Component.find('.labelsContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
