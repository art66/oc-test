import React, { PureComponent } from 'react'
import Label from './Label'
import { IProps } from './types'
import payloadFilter from '../../utils/payloadFilter'
import styles from './index.cssmodule.scss'

class Labels extends PureComponent<IProps> {
  static defaultProps: IProps = {
    pageID: 0
  }

  _bulidLabelsList() {
    const { pageID, labels } = this.props

    const pageLinks = payloadFilter(labels, pageID)

    if (!pageLinks) {
      return null
    }

    return pageLinks.items && pageLinks.items.map((link, i) => <Label key={i} {...link} />)
  }

  render() {
    const { labels } = this.props

    if (labels === null || Object.keys(labels).length === 0) {
      return null
    }

    return <div className={styles.labelsContainer}>{this._bulidLabelsList()}</div>
  }
}

export default Labels
