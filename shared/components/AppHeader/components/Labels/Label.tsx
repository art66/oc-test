import React from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'

import { IProps } from './types/label'
import styles from './label.cssmodule.scss'
import globalStyles from '../../styles/index.cssmodule.scss'
import getSVGIconConfig from '../../utils/getSVGIconConfig'

class Label extends React.PureComponent<IProps> {
  static defaultProps: IProps = {
    title: '',
    icon: ''
  }

  _renderIcon = () => {
    const { icon } = this.props

    if (!icon || icon === undefined) return null

    return (
      <span className={`${globalStyles.iconContainer} ${styles.labelIconContainer} icon-container-${icon}`}>
        <SVGIconGenerator {...getSVGIconConfig(icon, globalStyles.svgIcon)} />
      </span>
    )
  }

  render() {
    const { title, icon } = this.props

    if (!title && !icon) return null

    return (
      <div className={`${styles.labelContainer} ${globalStyles.greyLineV} label-container-${icon}`}>
        {this._renderIcon()}
        <span className={styles.labelTitle}>{title}</span>
      </div>
    )
  }
}

export default Label
