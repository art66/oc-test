// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'globalSvgShadow': string;
  'labelContainer': string;
  'labelIconContainer': string;
  'labelTitle': string;
}
export const cssExports: CssExports;
export default cssExports;
