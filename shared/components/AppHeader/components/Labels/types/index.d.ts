export interface ILabels {
  active?: boolean
  default?: {
    ID: number
    items: {
      title: string
      icon: string
    }[]
  }
  list?: {
    ID: number
    items: {
      title: string
      icon: string
    }[]
  }[]
}

export interface IProps {
  pageID?: number
  labels?: ILabels
}
