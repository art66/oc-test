import React, { PureComponent } from 'react'
import Button from './Button'
import payloadFilter from '../../utils/payloadFilter'
import { IProps, IState, IMethods } from './types'
import styles from './index.cssmodule.scss'

const ACTIVE_BUTTON = true

class Buttons extends PureComponent<IProps, IState> implements IMethods {
  static defaultProps: IProps = {
    callback: () => {},
    buttons: {}
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      activeButtonIndex: 0
    }
  }

  _handleClick = (index: number, id?: string, event?: React.MouseEvent) => {
    const { callback } = this.props

    // invoke buttons callback in case if it provided by developer
    if (callback && typeof callback === 'function') {
      callback(index, id, event)
    }

    this.setState((prevState: IState) => ({
      ...prevState,
      activeButtonIndex: index
    }))
  }

  _bulidButtonsList = () => {
    const { pageID, buttons } = this.props
    const { activeButtonIndex } = this.state

    const pageButtons = payloadFilter(buttons, pageID)

    if (!pageButtons) {
      return null
    }

    return (
      pageButtons.items &&
      pageButtons.items.map((button, i) => {
        let isButtonActive = false

        if (activeButtonIndex === i) {
          isButtonActive = ACTIVE_BUTTON
        }

        return <Button {...button} key={i} index={i} isButtonActive={isButtonActive} click={this._handleClick} />
      })
    )
  }

  render() {
    const { buttons } = this.props

    if (!buttons.list || buttons.list.length === 0) {
      return null
    }

    return <div className={styles.buttonsContainer}>{this._bulidButtonsList()}</div>
  }
}

export default Buttons
