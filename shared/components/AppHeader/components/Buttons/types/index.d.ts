export interface IButtons {
  list?: {
    ID?: number
    items?: {
      id?: string
      label?: string
      icon?: string
    }[]
  }[]
}

export interface IProps {
  pageID?: number
  callback?: any
  buttons?: IButtons
}

export interface IState {
  activeButtonIndex: number
}

export interface IMethods {
  _handleClick: (index: number, id?: string, event?: React.MouseEvent) => void
  _bulidButtonsList: (butons: object[]) => JSX.Element[]
}
