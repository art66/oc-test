interface ICallbackType {
  callbackData: any
}

export interface IProps {
  id?: string
  label?: string
  icon?: string
  index?: number
  isButtonActive?: boolean
  click?: (index: number, id?: string, event?: React.MouseEvent) => void
  callback?: {
    buttonCallback: (payload: ICallbackType, id?: string, event?: React.MouseEvent) => void
    callbackData: ICallbackType['callbackData']
  }
}

export type TMethods = (target: any) => void
