import { IProps } from '../../types/button'

const initialState: IProps = {
  id: 'items',
  label: 'items',
  icon: 'iconGrid',
  index: 0,
  isButtonActive: false
}

export const empty: IProps = {
  ...initialState,
  label: '',
  icon: ''
}

export const activated: IProps = {
  ...initialState,
  isButtonActive: true
}

export const disabled: IProps = {
  ...initialState,
  isButtonActive: false
}

export default initialState
