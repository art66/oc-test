const initialState = {
  pageID: 1,
  callback: () => {},
  buttons: {
    list: [
      {
        ID: 1,
        items: [
          {
            id: 'VIEW_TOGGLE_LIST',
            label: 'items',
            icon: 'ViewToggleList'
          },
          {
            id: 'VIEW_TOGGLE_THUMBNAILS',
            label: 'items',
            icon: 'ViewToggleThumbnails'
          }
        ]
      }
    ]
  }
}

export const empty = {
  active: true,
  buttons: {
    list: []
  },
  callback: null
}

export default initialState
