import React from 'react'
import { shallow } from 'enzyme'
import initialState, { activated, disabled, empty } from './mocks/button'
import Button from '../Button'

describe('<Button />', () => {
  it('should render Button component', () => {
    const Component = shallow(<Button {...initialState} />)

    expect(Component.find('.buttonContainer').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })

  it('should not render Button component in case of empty data', () => {
    const Component = shallow(<Button {...empty} />)

    expect(Component.find('.buttonContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })

  it('should render Button component Activated', () => {
    const Component = shallow(<Button {...activated} />)

    expect(Component.find('.buttonContainer').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should not render Button component notActivated', () => {
    const Component = shallow(<Button {...disabled} />)

    expect(Component.find('.buttonContainer').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should be clickabled and return some value from global callback', done => {
    const callback = jest.fn(ID => {
      expect(ID).toEqual(0)
      done()
    })

    const Component = shallow(<Button {...initialState} click={callback} />)

    expect(Component.find('.buttonContainer').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)

    Component.find('button').simulate('click', { target: { dataset: { buttonId: 0 } } })

    expect(Component).toMatchSnapshot()
  })

  it('should be clickabled and return some value from its own callback', done => {
    const callback = jest.fn(ID => {
      expect(ID).toEqual(0)
      done()
    })

    const Component = shallow(<Button {...disabled} callback={{ buttonCallback: callback, callbackData: 0 }} />)

    expect(Component.find('.buttonContainer').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)

    Component.find('button').simulate('click', { target: { dataset: { buttonId: 0 } } })

    expect(Component).toMatchSnapshot()
  })

  it('should handle global listener on click', () => {
    const click = jest.fn()
    const state = { ...initialState, click }
    const Component = shallow<Button>(<Button {...state} />)

    Component.simulate('click', { target: { dataset: true } })

    expect(click).toHaveBeenCalledTimes(1)
    expect(click.mock.calls[0][0]).toBe(0)
    expect(click.mock.calls[0][1]).toBe('items')
    expect(click.mock.calls[0][2]).toEqual({ target: { dataset: true } })

    expect(click).toMatchSnapshot()
  })
})
