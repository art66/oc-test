import React from 'react'
import { mount } from 'enzyme'
import initialState, { empty } from './mocks/index'
import Buttons from '../index'

describe('<Buttons />', () => {
  it('should render Buttons component with two nested Link Components', () => {
    const Component = mount(<Buttons {...initialState} />)

    expect(Component).toMatchSnapshot()
    expect(Component.find('.buttonsContainer').length).toBe(1)
    expect(Component.find('.buttonContainer').length).toBe(2)
    expect(Component.find('.iconContainer').length).toBe(2)
    expect(Component.find('SVGIconGenerator').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should not render Buttons component in case of empty data', () => {
    const Component = mount(<Buttons {...empty} />)

    expect(Component.find('.buttonsContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
