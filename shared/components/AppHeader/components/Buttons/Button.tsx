import React from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'

import getSVGIconConfig from '../../utils/getSVGIconConfig'
import { IProps, TMethods } from './types/button'
import styles from './button.cssmodule.scss'
import globalStyles from '../../styles/index.cssmodule.scss'

class Button extends React.PureComponent<IProps> {
  static defaultProps = {
    id: null,
    label: '',
    icon: '',
    index: 0,
    isButtonActive: false,
    click: () => {},
    callback: {
      buttonCallback: () => {},
      callbackData: ''
    }
  }

  _handleClick: TMethods = event => {
    const { target } = event
    const { click, index, id, callback, callback: { buttonCallback = () => {}, callbackData = null } = {} } = this.props

    if (!target || !target.dataset) return

    if (click) {
      click(index, id, event)
    }

    if ((callback && buttonCallback && callbackData) !== undefined) {
      buttonCallback(callbackData, id, event)
    }
  }

  render() {
    const { label, icon, index, isButtonActive } = this.props

    if (!icon && !label) return null

    const className = cn(globalStyles.svgIcon, { [globalStyles.isHovering]: isButtonActive })

    return (
      <button
        data-button-id={index}
        type='button'
        aria-labelledby={label}
        className={`${styles.buttonContainer} ${globalStyles.greyLineV}`}
        onClick={this._handleClick}
      >
        <span className={globalStyles.iconContainer}>
          <SVGIconGenerator {...getSVGIconConfig(icon, className)} />
        </span>
      </button>
    )
  }
}

export default Button
