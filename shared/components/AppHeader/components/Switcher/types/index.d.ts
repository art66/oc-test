export interface IProps {
  tutorialSwitchHandler: (payload: any) => void
  appID?: string
  showTutorial?: boolean
  iconSwitch?: boolean
  showSwitcher?: boolean
  isClientPropsThrowed?: boolean
}

export interface ICheckMethods {
  _requestTutorialUpdate(): void
  _handleTutorialSwitch(): void
}

export interface IState {
  isHovered: boolean
}
