import React, { PureComponent } from 'react'
import cn from 'classnames'
import { IProps, ICheckMethods, IState } from './types'
import { fetchUrl } from '../../../../utils'
import { SVGIconGenerator } from '../../../../SVG'

import { ROOT_PAYLOAD_URL, UPDATE_TUTORIAL_URL } from '../../constants'

import styles from './index.cssmodule.scss'
import globalStyles from '../../styles/index.cssmodule.scss'

const ACTIVE_SWITCHER = 1
const DISABLE_SWITCHER = 0
const DIMENSIONS_SHOW = {
  width: 11,
  height: 15,
  viewbox: '.5 1 11 17'
}
const DIMENSIONS_HIDE = {
  width: 19,
  height: 16,
  viewbox: '-.5 2 20 18'
}

class Switcher extends PureComponent<IProps, IState> implements ICheckMethods {
  static defaultProps: IProps = {
    tutorialSwitchHandler: () => {},
    appID: '',
    iconSwitch: false,
    showSwitcher: false
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      isHovered: false
    }
  }

  _handleOver = () => {
    this.setState({
      isHovered: true
    })
  }

  _handleLeave = () => {
    this.setState({
      isHovered: false
    })
  }

  _requestTutorialUpdate = async () => {
    const { iconSwitch, appID } = this.props

    const tutorialShowState = iconSwitch ? DISABLE_SWITCHER : ACTIVE_SWITCHER
    // @ts-ignore
    const payload = await fetchUrl(`${ROOT_PAYLOAD_URL}${appID}${UPDATE_TUTORIAL_URL}${tutorialShowState}`)
    const { tutorials: { activateOnLoad = false } = {} } = payload || {}

    return activateOnLoad
  }

  _handleTutorialSwitch = () => {
    const { tutorialSwitchHandler, iconSwitch, isClientPropsThrowed } = this.props

    if (isClientPropsThrowed) {
      tutorialSwitchHandler(!iconSwitch)

      return
    }

    this._requestTutorialUpdate().then(payload => tutorialSwitchHandler(payload))
  }

  render() {
    const { isHovered } = this.state
    const { iconSwitch, showSwitcher } = this.props

    if (!showSwitcher) return null

    return (
      <div
        onMouseOver={this._handleOver}
        onMouseLeave={this._handleLeave}
        className={`${styles.switcherContainer} ${globalStyles.greyLineV}`}
      >
        <button type='button' className={styles.tutorialSwitcher} onClick={this._handleTutorialSwitch} />
        <span className={`${globalStyles.iconContainer} ${styles.switchIconContainer}`}>
          <SVGIconGenerator
            iconsHolder={
              import(/* webpackChunkName: "globalSVGIcons" */ '@torn/shared/SVG/helpers/iconsHolder/topPageLinks')
            }
            iconName='Tutorial'
            type={!iconSwitch ? 'show' : 'hide'}
            customClass={cn(styles.svgIconSwitch, { [styles.isHovering]: isHovered })}
            fill={{ strokeWidth: 0 }}
            filter={{ active: true, ID: 'top_svg_icon' }}
            dimensions={!iconSwitch ? DIMENSIONS_SHOW : DIMENSIONS_HIDE}
          />
        </span>
        <span className={styles.switcherTitle}>Tutorial</span>
      </div>
    )
  }
}

export default Switcher
