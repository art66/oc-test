// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'appHeaderWrapper': string;
  'bottomSection': string;
  'delimiter': string;
  'disableButtonsRightMargin': string;
  'disableLinksRightMargin': string;
  'globalSvgShadow': string;
  'greyLineV': string;
  'iconContainer': string;
  'isDropDownLayout': string;
  'isHovering': string;
  'svgIcon': string;
  'svgIconSwitch': string;
  'switchIconContainer': string;
  'switcherContainer': string;
  'switcherIcon': string;
  'switcherTitle': string;
  'topSection': string;
  'tutorialContainer': string;
  'tutorialHide': string;
  'tutorialShow': string;
  'tutorialSwitcher': string;
}
export const cssExports: CssExports;
export default cssExports;
