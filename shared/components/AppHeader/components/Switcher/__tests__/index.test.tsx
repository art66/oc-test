import React from 'react'
import { shallow, mount } from 'enzyme'
import initialState, { active, deactive, disabled, hovered } from './mocks/index'
import Switcher from '../index'

const ACTIVE_ICON_SHAPE =
  'M7.92,13.72c0-3.49,3.78-4.6,3.49-8.07C11.24,3.46,9.72,1,5.71,1S.19,3.46,0,5.65c-.29,3.47,3.49,4.58,3.49,8.07Zm-6.17-8c.16-2.08,1.61-3.13,4-3.13s3.8,1,4,3.13c.1,1.14-.57,1.9-1.47,3.15a10,10,0,0,0-1.82,3.4H5a10,10,0,0,0-1.82-3.4C2.32,7.67,1.65,6.91,1.75,5.77ZM3.42,16.33A4,4,0,0,0,5.71,17,4,4,0,0,0,8,16.33V14.68H3.42Z'
const DEACTIVATED_ICON_SHAPE =
  'M6.79,17.34A3.93,3.93,0,0,0,9.07,18a3.9,3.9,0,0,0,2.27-.66V15.7H6.79Zm5.82-6c1-1.36,2.27-2.58,2.11-4.61A5.07,5.07,0,0,0,13.8,4.2L17,1H15.87l-2.6,2.59a5.8,5.8,0,0,0-4.2-1.46A5.83,5.83,0,0,0,4.86,3.59L2.27,1H1.13L4.34,4.2a5.09,5.09,0,0,0-.93,2.54c-.16,2,1.07,3.25,2.11,4.61L0,16.87H1.13L6,12a4.82,4.82,0,0,1,.89,2.74h4.37A4.82,4.82,0,0,1,12.14,12L17,16.87h1.13ZM9.07,3.76a4.16,4.16,0,0,1,3,1l-3,3-3-3A4.17,4.17,0,0,1,9.07,3.76ZM6.71,10.15,6.59,10C5.7,8.75,5,8,5.13,6.87a3.57,3.57,0,0,1,.42-1.45l3,2.95Zm3,3.21H8.4a8.21,8.21,0,0,0-1.22-2.54L9.07,8.93,11,10.82A8,8,0,0,0,9.74,13.36ZM11.54,10l-.12.17L9.63,8.37l2.95-2.95A3.42,3.42,0,0,1,13,6.87C13.09,8,12.43,8.75,11.54,10Z'

describe('<Switcher />', () => {
  it('should render Switcher component', async done => {
    const Component = shallow(<Switcher {...initialState} />)

    await new Promise(res => setTimeout(() => res(), 1000))

    expect(Component.find('.switcherContainer').length).toBe(1)
    expect(Component.find('.tutorialSwitcher').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('.switcherTitle').length).toBe(1)
    expect(Component.find('.switcherTitle').text()).toBe('Tutorial')
    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render Switcher component in active phase', async done => {
    const Component = mount(<Switcher {...active} />)

    await new Promise(res => setTimeout(() => res(), 1000))

    Component.setProps({ iconSwitch: true })

    expect(Component.find('.switcherContainer').length).toBe(1)
    expect(Component.find('.tutorialSwitcher').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('path').prop('d')).toBe(DEACTIVATED_ICON_SHAPE)
    expect(Component.find('.switcherTitle').length).toBe(1)
    expect(Component.find('.switcherTitle').text()).toBe('Tutorial')
    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render Switcher component in deactive phase', async done => {
    const Component = mount(<Switcher {...deactive} />)

    await new Promise(res => setTimeout(() => res(), 1000))

    Component.setProps({ iconSwitch: false })

    expect(Component.find('.switcherContainer').length).toBe(1)
    expect(Component.find('.tutorialSwitcher').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('path').prop('d')).toBe(ACTIVE_ICON_SHAPE)
    expect(Component.find('.switcherTitle').length).toBe(1)
    expect(Component.find('.switcherTitle').text()).toBe('Tutorial')
    expect(Component).toMatchSnapshot()

    done()
  })
  it('should render Switcher component in Hovered state', async done => {
    const Component = mount(<Switcher {...hovered} />)

    await new Promise(res => setTimeout(() => res(), 1000))

    Component.setState({ isHovered: true })

    expect(Component.find('.switcherContainer').length).toBe(1)
    expect(Component.find('.tutorialSwitcher').length).toBe(1)
    expect(Component.find('.iconContainer').length).toBe(1)
    expect(Component.find('SVGIconGenerator').length).toBe(1)
    expect(Component.find('svg').prop('fill')).toBe('#fff')
    expect(Component.find('.switcherTitle').length).toBe(1)
    expect(Component.find('.switcherTitle').text()).toBe('Tutorial')
    expect(Component).toMatchSnapshot()

    done()
  })
  it('should not render Switcher component in disabled phase', () => {
    const Component = shallow(<Switcher {...disabled} />)

    expect(Component.find('.switcherContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
})
