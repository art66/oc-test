const initialState = {
  tutorialSwitchHandler: () => {},
  iconSwitch: false,
  showSwitcher: true,
  ID: 1
}

export const active = {
  ...initialState,
  iconSwitch: true
}

export const deactive = {
  ...initialState,
  iconSwitch: false
}

export const disabled = {
  ...initialState,
  showSwitcher: false
}

export const hovered = {
  ...initialState
}

export default initialState
