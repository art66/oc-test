import Title from './Title'
import Links from './Links'
import Buttons from './Buttons'
import Labels from './Labels'
import Switcher from './Switcher'
import Tutorial from './Tutorial'

export { Title, Links, Labels, Buttons, Switcher, Tutorial }
