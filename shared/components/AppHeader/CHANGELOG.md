# AppHeader - Torn


## 4.12.2
 * Move svg icon to be loaded fully asynchronously.

## 4.12.2
 * Improved SVIcons loading.
 * Updated test suits.

## 4.12.1
 * Added button id.

## 4.12.0
 * Added link on click handler.

## 4.11.2
 * Fixed Tutorial layout glitches.

## 4.11.1
 * Fixed layout position glitches in desktop/tablet/mobile modes.

## 4.11.0
 * Added new logic for Tutorial icon switch.

## 4.10.0
 * Updated tutorial SVG icon.
 * Fixed color of SVG icons.

## 4.9.0
 * Removed title layout for subpages (obly subtitle leaved).

## 4.8.0
 * Added ability to deal with SPA redirects (without reload).

## 4.7.0
 * Added ability to throw custom callback functions on particular buttons.

## 4.6.4
 * Fixed broken containers width.
 * Reverted styeles.cssmodule.scss.
 * Update storybook config.

## 4.6.4
 * Fixed Links processing in payloadFilter, added few more checks.
 * Update Test Suits.

## 4.6.3
 * Added storybook for links Dropdown Component.

## 4.6.2
 * Transformed image styles to css.

## 4.6.1
 * Added storybook for Labels Component.

## 4.5.1
 * Fixes added to Lables Component.

## 4.5.0
 * Added Labels Component. Tests included.
 * Update Tests Suits.

## 4.4.2
 * Fixed clientProps throwing while parent component updates own props for rerender.
 * Fixed height for AppHeader wrapper.
 * Update Unit Tests.

## 4.4.1
 * Fixed error in Tutorils Switch once clientProps is throwed.

## 4.4.0
 * Moved Links and Buttons icons to SVG using.

## 4.3.0
 * Update Storybook Config.

## 4.2.3
 * Added Links Dropdown Component. Written Tests.

## 4.2.2
 * Fixed links icons dimensions container.

# 4.2.6
 * Fixed icon borders on tablet/mobile.

# 4.2.5
 * Fixed topSection lightbulb position.

## 4.2.4
 * Fixed correct height in topSections for consitency with legacy-one.

## 4.2.3
 * Fixed links & buttons right margin in case of non-providing tutorials/buttons (for the first one).

## 4.2.2
 * Fixed links icons dimensions container.

## 4.2.1
 * Fixed topSection margin bottom.

## 4.2.0
 * Added ability to set AppHeader props just from the client side, without fetching backend.
 * Upodated tests.

## 4.2.0
 * Added extra value checking to prevent the app fall once no one is received.

## 4.1.0
 * Updated README.md file.

## 4.0.1
 * Fixed hr tag css styles.
 * Fixed delimier border top color.
 * Fixed global width to make it adoptive to the parent container.

## 4.0.0
 * Updated Title logic from manual throwing to back-end receiving.
 * Fixed legacy code issue in Tutorials component.

## 3.2.0
 * Updated few types.
 * Fixed typo in payloaderFilter util.

## 3.2.0
 * Updated Unit tests.

## 3.1.1
 * Fixed explicitly setted desktop layout mode.

## 3.1.0
 * Changed Fetch URL corresponding to the back-end improvements.

## 3.0.0
 * Replaced logic of incoming config from manual providing on self-processing based on the own back-end responce.
 * Tests suits are updated.
 * Removed some legacy code.

## 2.3.1
 * Replaced legacy lightbulb icon with SVG one.
 * Fied some minor issues.
 * Updated tests suits for AppHeader Component.

## 2.2.1
 * Fixed tutorials show layout for rare bug case.

## 2.2.0
 * Added global flag for tutorial show switcher.

## 2.1.0
 * Added fetch request on Switch Tutorial Button for updating tutorial status to show (on/off).

## 2.0.0
 * Tutorial logic has been moved from app importing to independed state managment with fetch requests on back-end to update themself.
 * Rewritten tests suits for Tutorial and AppContainer components.
 * Updated Types.

## 1.0.0
 * Added tuttorial update fetch post request.

## 0.9.0
 * Added active state for tutorial and switcher by default.

## 0.8.4
 * Minor style fix for wrapper.

## 0.8.3
 * Minor style fix for divider.

## 0.8.2
 * Minor style fix for wrapper.

## 0.8.1
 * Improved data schema for app.
 * Minor style fixes.

## 0.7.1
 * Writeen tests for all Components.
 * Improved whole Components logic and structure.
 * Fixed some TypeScipt typizations.

## 0.6.1
 * Created Buttons Component (like in Items app).
 * Created Links Component (like in Any other app).
 * Created Adoptive layout.
 * Fixed minor bugs.

## 0.5.0
 * App rewritten on TypeScript and start to write in future on it.

## 0.4.0
 * Created Title and Switcher Components.

## 0.3.0
 * Rewriteen basic Tutorials files on React.
 * Rewriteen styles from css to cssmodules.

## 0.2.0
 * Created basic App structure.
 * Added React-Redux environment. Bootstrapped App.
 * Created basic store configuration.
 * Added middleware and actions/reducers.

## 0.1.0
 * App folder created. Pre-work stage.
