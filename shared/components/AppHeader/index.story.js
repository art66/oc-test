import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'
import { withCustomStyles } from '../../../.storybook/addons'
import {
  LABELS_CONFIG,
  LINKS_CONFIG,
  BUTTONS_CONFIG,
  DEFAULT_CONFIG,
  DEFAULT_LINKS_DROPDOWN_CONFIG,
  LINKS_DROPDOWN_CONFIG,
  LINKS_AND_BUTTONS_CONFIG
} from './mocks'
import AppHeader from './index'

export const Defaults = () => {
  return <AppHeader {...DEFAULT_CONFIG} />
}

Defaults.story = {
  name: 'defaults'
}

export const LinksDropdown = () => {
  return <AppHeader {...DEFAULT_LINKS_DROPDOWN_CONFIG} />
}

LinksDropdown.story = {
  name: 'links dropdown'
}

export const OnlyLinksAndButtons = () => {
  return <AppHeader {...LINKS_AND_BUTTONS_CONFIG} />
}

OnlyLinksAndButtons.story = {
  name: 'only links and buttons'
}

export const OnlyButtons = () => {
  return <AppHeader {...BUTTONS_CONFIG} />
}

OnlyButtons.story = {
  name: 'only buttons'
}

export const OnlyLinks = () => {
  return <AppHeader {...LINKS_CONFIG} />
}

OnlyLinks.story = {
  name: 'only links'
}

export const OnlyLinksDropdown = () => {
  return <AppHeader {...LINKS_DROPDOWN_CONFIG} />
}

OnlyLinksDropdown.story = {
  name: 'only links dropdown'
}

export const OnlyLabels = () => {
  return <AppHeader {...LABELS_CONFIG} />
}

OnlyLabels.story = {
  name: 'only labels'
}

export default {
  title: 'Shared/AppHeader',
  decorators: [withCustomStyles('max-width: 1200px; padding: 0 15px;'), withKnobs]
}
