import { text, number, boolean } from '@storybook/addon-knobs'

export const STORY_GROUPS = {
  links: 'Links',
  common: 'Common',
  buttons: 'Buttons',
  labels: 'Labels',
  titles: 'Titles',
  tutorials: 'Tutorials'
}

const { common, links, buttons, labels, titles, tutorials } = STORY_GROUPS

export const DEFAULT_CONFIG = {
  appID: text('App ID:', 'crimes', common),
  pageID: number('App Page ID:', 0, {}, common),
  linkCallback: (id, event) => {
    console.log('linkCallback: ', id, event)
  },
  buttonCallback: (index, id, event) => {
    console.log('buttonCallback: ', index, id, event)
  },
  clientProps: {
    titles: {
      default: {
        ID: number('Title ID:', 0, {}, titles),
        title: text('Title:', 'Crimes', titles)
      },
      list: [
        {
          ID: number('Subtitle ID 1:', 1, {}, titles),
          subTitle: text('Subtitle to Show 1:', 'Search for сash', titles)
        },
        {
          ID: number('Subtitle ID 2:', 2, {}, titles),
          subTitle: text('Subtitle to Show 2:', 'Bootlegging', titles)
        }
      ]
    },
    links: {
      active: boolean('Links active:', true, links),
      list: [
        {
          ID: number('links Page to Show:', 0, {}, links),
          items: [
            {
              title: text('Links Items Title 1', 'Ammo', links),
              href: text('Links Items href 1', 'loader.php?sid=kenoLastGames', links),
              label: text('Links Items label 1', 'last-spins', links),
              icon: text('Links Items icon 1', 'AmmoLocker', links)
            },
            {
              id: text('Links Items Id 3', '2', links),
              title: text('Links Items Title 3', 'Clickable', links),
              href: text('Links Items href 3', null, links),
              label: text('Links Items label 3', 'last-spins', links),
              icon: text('Links Items icon 3', 'Trades', links),
              onClick: (id, event) => {
                console.log('onClick: ', id, event)
              }
            }
          ]
        }
      ]
    },
    labels: {
      pageID: number('Labes Page to Show:', 0, {}, labels),
      active: boolean('Activate Labels:', true),
      default: {
        ID: number('Default Page Labels:', 0, {}, labels),
        items: [
          {
            title: text('Default Title:', labels),
            icon: text('Default Icon:', 'Test', labels)
          }
        ]
      },
      list: [
        {
          ID: number('main Page Labels:', 0, {}, labels),
          items: [
            {
              title: text('Title labels 1:', 'Time', labels),
              icon: text('Text labels 1:', 'Send', labels)
            },
            {
              title: text('Title labels 2:', 'Mods', labels),
              icon: text('Text labels 1:', 'Mods', labels)
            }
          ]
        }
      ]
    },
    buttons: {
      active: boolean('Buttons active:', true, buttons),
      callback: () => {
        console.log('callback')
      },
      list: [
        {
          ID: number('Buttons Page to Show:', 0, {}, buttons),
          items: [
            {
              id: text('Buttons Items Id 1', '0', buttons),
              label: text('Buttons Items Title 1', 'items', buttons),
              icon: text('Buttons Items icon 1', 'ViewToggleList', buttons),
              onClick: (index, id, event) => {
                console.log('onClick: ', index, id, event)
              }
            },
            {
              id: text('Buttons Items Id 2', '1', buttons),
              label: text('Buttons Items Title 2', 'items', buttons),
              icon: text('Buttons Items icon 2', 'ViewToggleThumbnails', buttons),
              onClick: (index, id, event) => {
                console.log('onClick: ', index, id, event)
              }
            }
          ]
        }
      ]
    },
    tutorials: {
      active: boolean('Tutorials active:', true, tutorials),
      activateOnLoad: boolean('Tutorials active on load:', false, tutorials),
      hideByDefault: boolean('Tutorials hide by default:', false, tutorials),
      default: {
        ID: number('Default Tutorial:', 0, {}, tutorials),
        items: [
          {
            title: text('Default Tutorial Title:', 'Tutorial title', tutorials),
            text: text('Default Tutorial Text:', 'Tutorial text', tutorials)
          }
        ]
      },
      list: [
        {
          ID: number('Tutorial ID 1:', 1, {}, tutorials),
          items: [
            {
              title: text('Tutorial Title:', 'Tutorial totorial', tutorials),
              text: text(
                'Tutorial Text:',
                `Lorem ipsum dolor sit amet, ea diam consul consulatu est.
              No nam velit malorum minimum, est eu esse laoreet impedit, ei eirmod corrumpit
              ullamcorper nam. An iuvaret alienum efficiantur vix`,
                tutorials
              )
            }
          ]
        }
      ]
    }
  }
}

export const LINKS_DROPDOWN = {
  ...DEFAULT_CONFIG.clientProps.links,
  list: [
    {
      ...DEFAULT_CONFIG.clientProps.links.list[0],
      isDropDownLayout: boolean('Is Dropdown layout:', true),
      items: [
        {
          href: text('Items href 1', 'imarket.php'),
          label: text('Items label 1', 'item-market'),
          icon: text('Items icon 1', 'ItemMarket'),
          title: text('Items title 1', 'Item Market')
        },
        {
          href: text('Items href 2', 'item.php'),
          label: text('Items label 2', 'items'),
          icon: text('Items icon 2', 'Items'),
          title: text('Items title 2', 'Your Items')
        },
        {
          href: text('Items href 3', '#'),
          label: text('Items label 3', 'bazaar-state'),
          icon: text('Items icon 3', 'Items'),
          title: text('Items title 3', 'Closed')
        },
        {
          href: text('Items href 4', '#'),
          label: text('Items label 4', 'bazaar-state'),
          icon: text('Items icon 4', 'Items'),
          title: text('Items title 4', 'Open')
        },
        {
          href: text('Items href 5', '#/add'),
          label: text('Items label 5', 'add-items'),
          icon: text('Items icon 5', 'ItemsAdd'),
          title: text('Items title 5', 'Add items')
        },
        {
          href: text('Items href 6', '#/manage'),
          label: text('Items label 6', 'manage-items'),
          icon: text('Items icon 6', 'ManageItems'),
          title: text('Items title 6', 'Manage items')
        },
        {
          href: text('Items href 7', '#/personalize'),
          label: text('Items label 7', 'personalize'),
          icon: text('Items icon 7', 'Personalize'),
          title: text('Items title 7', 'Personalize')
        },
        {
          href: text('Items href 8', '#/profile'),
          label: text('Items label 8', 'back'),
          icon: text('Items icon 8', 'Back'),
          title: text('Items title 8', 'Your Profile')
        }
      ]
    }
  ]
}

export const DEFAULT_LINKS_DROPDOWN_CONFIG = {
  ...DEFAULT_CONFIG,
  clientProps: { ...DEFAULT_CONFIG.clientProps, links: LINKS_DROPDOWN }
}

export const BUTTONS_CONFIG = {
  ...DEFAULT_CONFIG,
  clientProps: {
    titles: DEFAULT_CONFIG.clientProps.titles,
    buttons: DEFAULT_CONFIG.clientProps.buttons
  }
}

export const LINKS_CONFIG = {
  ...DEFAULT_CONFIG,
  clientProps: {
    titles: DEFAULT_CONFIG.clientProps.titles,
    links: DEFAULT_CONFIG.clientProps.links
  }
}

export const LINKS_AND_BUTTONS_CONFIG = {
  ...DEFAULT_CONFIG,
  clientProps: {
    titles: DEFAULT_CONFIG.clientProps.titles,
    buttons: DEFAULT_CONFIG.clientProps.buttons,
    links: DEFAULT_CONFIG.clientProps.links
  }
}

export const LINKS_DROPDOWN_CONFIG = {
  ...DEFAULT_CONFIG,
  clientProps: {
    titles: DEFAULT_CONFIG.clientProps.titles,
    links: DEFAULT_LINKS_DROPDOWN_CONFIG.clientProps.links
  }
}

export const LABELS_CONFIG = {
  ...DEFAULT_CONFIG,
  clientProps: {
    titles: DEFAULT_CONFIG.clientProps.titles,
    labels: DEFAULT_CONFIG.clientProps.labels
  }
}
