import { IProps as ITitle } from '../../components/Title/types'
import { ITutorials } from '../../components/Tutorial/types'
import { IButtons } from '../../components/Buttons/types'
import { ILabels } from '../../components/Labels/types'
import { ILinks } from '../../components/Links/types'

export interface IClientProps {
  titles?: object
  tutorials?: object
  buttons?: object
  links?: ILinks
}

export interface IProps {
  appID?: string
  pageID?: number
  clientProps?: IClientProps
  buttonCallback?: (index: number, id?: string, event?: React.MouseEvent) => void
  linkCallback?: (id?: string, event?: React.MouseEvent) => void
}

export interface IStore extends ITitle {
  isClientPropsThrowed?: boolean
  clientProps?: IClientProps
  pageID?: number
  showTutorial?: boolean
  tutorialFinded?: boolean
  tutorials?: ITutorials
  buttons?: IButtons
  labels?: ILabels
  links?: ILinks
}

export interface IMethods {
  _handleTutorialSwitch: (tutorialSwitch: boolean) => void
  _requestPayload: () => void
  _setInitialState: (payload: object) => void
}
