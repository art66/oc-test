/* eslint-disable max-statements */
import React from 'react'
import { mount } from 'enzyme'
import initialState, {
  initialProps,
  tutorialState,
  onlyTitle,
  clientProps,
  onlyLinksTitle,
  onlyButtons
} from './mocks/index'
import AppHeader from '../AppHeader'

const marginLinksTestTitle = `should render AppHeader component
with disabled margin-right for links if not buttons and tutorials are provided`

describe('<AppHeader />', () => {
  it('should render whole AppHeader component', async () => {
    const requestPayload = jest.fn()

    requestPayload.mockReturnValueOnce(Promise.resolve(tutorialState))
    const Component = mount(<AppHeader {...initialProps} />)

    Component.setState(initialState)
    await Promise.resolve()

    expect(Component).toMatchSnapshot()
    expect(Component.find('.appHeaderWrapper').length).toBe(1)
    expect(Component.find('.appHeaderWrapper > .topSection').length).toBe(1)
    expect(Component.find('.titleContainer').length).toBe(1)
    expect(Component.find('.tContainer > .topSection').length).toBe(1)
    expect(Component.find('.tContainer > .bottomSection').length).toBe(1)
    expect(Component.find('.switcherContainer').length).toBe(1)
    expect(Component.find('.linksContainer').length).toBe(1)
    expect(Component.find('.labelsContainer').length).toBe(1)
    expect(Component.find('.buttonsContainer').length).toBe(1)
    expect(Component.find('.delimiter').length).toBe(2)
    expect(Component.find('.appHeaderWrapper > .bottomSection').length).toBe(1)
    expect(Component.find('.tContainer').length).toBe(1)
    expect(Component).toMatchSnapshot()
  })
  it('should render AppHeader component only with Title component and Switcher', () => {
    const Component = mount(<AppHeader {...onlyTitle} />)

    expect(Component.find('.appHeaderWrapper').length).toBe(1)
    expect(Component.find('.topSection').length).toBe(1)
    expect(Component.find('.titleContainer').length).toBe(1)
    expect(Component.find('.switcherContainer').length).toBe(0)
    expect(Component.find('.linksContainer').length).toBe(0)
    expect(Component.find('.labelsContainer').length).toBe(0)
    expect(Component.find('.buttonsContainer').length).toBe(0)
    expect(Component.find('.delimiter').length).toBe(1)
    expect(Component.find('.bottomSection').length).toBe(1)
    expect(Component.find('.tContainer').length).toBe(0)
    expect(Component).toMatchSnapshot()
  })
  it('should render AppHeader component with clientProps only', () => {
    const Component = mount(<AppHeader {...clientProps} />)

    expect(Component.find('.appHeaderWrapper').length).toBe(1)
    expect(Component.find('.title').length).toBe(1)
    expect(Component.find('.title').text()).toBe('Main title')
    expect(Component).toMatchSnapshot()
  })
  it(marginLinksTestTitle, () => {
    const Component = mount(<AppHeader {...onlyLinksTitle} />)

    Component.setState(onlyLinksTitle)

    expect(Component.find('.appHeaderWrapper.disableLinksRightMargin').length).toBe(1)
    expect(Component.find('.title').length).toBe(1)
    expect(Component.find('a').length).toBe(2)
    expect(Component.find('button').length).toBe(0)
    expect(Component.find('.switcherContainer').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
  it('should render AppHeader component with disabled margin-right for buttons if tutorials are provided', () => {
    const Component = mount(<AppHeader {...onlyButtons} />)

    Component.setState(onlyButtons)

    expect(Component.find('.appHeaderWrapper.disableButtonsRightMargin').length).toBe(1)
    expect(Component.find('.title').length).toBe(1)
    expect(Component.find('a').length).toBe(0)
    expect(Component.find('button').length).toBe(2)
    expect(Component.find('.switcherContainer').length).toBe(0)

    expect(Component).toMatchSnapshot()
  })
})
