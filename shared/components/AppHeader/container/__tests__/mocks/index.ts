import buttons from '../../../components/Buttons/__tests__/mocks/index'
import labels from '../../../components/Labels/__tests__/mocks/index'
import links from '../../../components/Links/__tests__/mocks/index'
import tutorials from '../../../components/Tutorial/__tests__/mocks/index'

const initialState = {
  ...links,
  ...labels,
  ...buttons,
  ...tutorials,
  title: 'Test title',
  appID: 'Test',
  pageID: 1,
  tutorialFinded: true,
  showTutorial: true,
  iconSwitch: true,
  showSwitcher: true
}

export const initialProps = {
  title: 'Test title',
  appID: 'Test',
  pageID: 1
}

export const tutorialState = {
  tutorials,
  showTutorial: true
}

export const onlyTitle = {
  title: 'Test title',
  appID: 'Test',
  pageID: 0,
  links: {},
  labels: {},
  buttons: {},
  tutorial: {}
}

export const onlyLinksTitle = {
  title: 'Test title',
  appID: 'Test',
  pageID: 0,
  ...links,
  labels: {},
  buttons: {},
  tutorial: {}
}

export const onlyButtons = {
  title: 'Test title',
  appID: 'Test',
  pageID: 0,
  links: {},
  ...buttons,
  tutorial: {}
}

export const clientProps = {
  appID: 'Test',
  pageID: 0,
  clientProps: {
    titles: {
      default: {
        ID: 0,
        title: 'Main title'
      }
    }
  }
}

export default initialState
