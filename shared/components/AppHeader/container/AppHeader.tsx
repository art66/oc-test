import React, { Component } from 'react'
import classnames from 'classnames'
import { fetchUrl } from '../../../utils'
import payloadFilter from '../utils/payloadFilter'

import { Title, Links, Labels, Buttons, Switcher, Tutorial } from '../components'

import { IProps, IStore, IMethods } from './types'
import { ROOT_PAYLOAD_URL } from '../constants'
import styles from '../styles/index.cssmodule.scss'

class AppHeader extends Component<IProps, IStore> implements IMethods {
  static defaultProps: IProps = {
    appID: '',
    pageID: 0,
    buttonCallback: () => {}
  }

  constructor(props: IProps) {
    super(props)

    this.state = {
      pageID: null,
      showTutorial: false,
      tutorials: {},
      buttons: {},
      labels: {},
      links: {},
      titles: {
        default: {
          ID: null,
          title: ''
        },
        list: [
          {
            ID: null,
            subTitle: ''
          }
        ]
      }
    }
  }

  static getDerivedStateFromProps(nextProps: IProps, prevState: IStore) {
    const newPageID = Number(nextProps.pageID)
    const showSwitcher = payloadFilter(prevState.tutorials, newPageID)

    // hack for client props throwing
    if (nextProps.clientProps) {
      return {
        ...prevState,
        ...nextProps.clientProps,
        pageID: newPageID,
        isClientPropsThrowed: true,
        tutorialFinded: !!showSwitcher
      }
    }

    if (newPageID !== prevState.pageID) {
      return {
        ...prevState,
        pageID: newPageID,
        showTutorial: false, // hide tutorial on each page changes
        tutorialFinded: !!showSwitcher
      }
    }

    return prevState
  }

  componentDidMount() {
    if (this.state.isClientPropsThrowed) {
      this._checkTutorial()
      return
    }

    this._requestPayload().then(payload => this._setInitialState(payload))
  }

  _classesHolder = () => {
    const { tutorials, buttons } = this.state

    const appContainerClasses = classnames({
      [styles.appHeaderWrapper]: true,
      [styles.disableLinksRightMargin]: !tutorials.default && !buttons.list,
      [styles.disableButtonsRightMargin]: !tutorials.default && buttons.list
    })

    return appContainerClasses
  }

  _requestPayload = async () => {
    const { appID } = this.props

    if (appID === null) return

    // @ts-ignore
    const payload = await fetchUrl(`${ROOT_PAYLOAD_URL}${appID}`)

    return payload
  }

  _setInitialState = payload => {
    this.setState((prevState: IStore) => ({
      ...prevState,
      ...payload
    }))
  }

  _checkTutorial = () => {
    const showSwitcher = payloadFilter(this.state.tutorials, this.props.pageID)

    this.setState({
      tutorialFinded: !!showSwitcher
    })
  }

  _handleTutorialSwitch = tutorialSwitch => {
    this.setState(prevState => ({
      ...prevState,
      showTutorial: tutorialSwitch
    }))
  }

  render() {
    const { tutorialFinded, showTutorial, titles, buttons, links, labels, tutorials, isClientPropsThrowed } = this.state
    const { pageID, appID, linkCallback, buttonCallback } = this.props

    const appContainerClasses = this._classesHolder()

    if (!appID) {
      console.error('You must provide an appID for AppHeder first!')
      return null
    }

    // TODO: move multiply pageID throwing to Context logic once we done with updating to react v.16.8
    return (
      <div className={appContainerClasses}>
        <div className={styles.topSection}>
          <Title pageID={pageID} titles={titles} />
          <Labels pageID={pageID} labels={labels} />
          <Links pageID={pageID} links={links} callback={linkCallback} />
          <Buttons pageID={pageID} buttons={buttons} callback={buttonCallback} />
          <Switcher
            appID={appID}
            iconSwitch={showTutorial}
            showSwitcher={tutorialFinded}
            tutorialSwitchHandler={this._handleTutorialSwitch}
            isClientPropsThrowed={isClientPropsThrowed}
          />
        </div>
        <hr className={styles.delimiter} />
        <div className={styles.bottomSection}>
          <Tutorial pageID={pageID} tutorials={tutorials} showTutorial={showTutorial} />
        </div>
      </div>
    )
  }
}

export default AppHeader
