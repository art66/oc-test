
# Global AppHeader Component
### This Component helps to solve the problem with app Header in react-apps. He can process the various props: `title, buttons, links, tutorials`. So, with that mono-component you does not need to inject in your app a several components for header normal work. Also it reduces a regular boilerplate.



## Schema:

### Props: Manual props injection for Component work:
* **appID**. Accepts only one `string` parament:

   --- `appID: string'`. Need to request the payload on back-end about current page.

  *Example*:
  `const APP_ID = 'crimes'`

* **pageID**. Accepts only one `number` parament:

   --- `pageID: number'`. Need to filter the data for particular user interaction page.

  *Example*:
  `const PAGE_ID = 0`

* **clientProps**. Accepts `object` paraments like as received from back-end to manual operating them without server:

  *Example*:
  `const CLIENT_PROPS = {
    titles: {
      default: {
        ID: 0,
        title: 'Main title'
      }
    }
  }`

* **buttonCallback**. Accepts only one `function` parament:

   --- `buttonCallback: (index: number, id?: string, event?: React.MouseEvent) => void'`. Function will handle on button click.

  *Example*:
  `const buttonCallback = (index, id, event) => console.log(id)`

* **linkCallback**. Accepts only one `function` parament:

   --- `linkCallback: (id?: string, event?: React.MouseEvent) => void'`. Function will handle on link click.

  *Example*:
  `const linkCallback = (id, event) => console.log(id)`


### State: Automatical App logic responded for fetching and processing AppHeader data:
 * **Titles**. Receives from back-end an `object` named as `titles` with next fields:

   --- `default: object` the default props with common title for the App. Always required.
   --- `list: object[]` the list of subTitles for particular pages with yheir own props.

   *Example*:
   ```
    const TITLES = {
      default: {
        ID: 0,
        title: 'crimes'
      },
      list: [
        {
          ID: 1,
          subTitle: 'searchforcash'
        },
        {
          ID: 2,
          subTitle: 'bootlegging'
        }
      ]
    }
  ```

 * **Buttons**. Receives from back-end an `object` named as `buttons` with next fields:

    --- `buttons: object[]` list with objects of buttons to render: `active: boolean`, `default: object`(fallback props), `list: array`(props list per page). Also `list` (and `default`) objects must include descendence objects with the next props: `ID: number`, `items: array` with objects defined as: `id: string`(identifier), `label: string`(aria label), `icon: string`(glob className):

    --- `callback: any` function, provided by developer, that can be invoked on any button click with returned back clicked button `ID: number` info.

   *Example*:
   ```
    const BUTTONS = {
      active: true,
      callback: () => {},
      default: {
        ID: 1,
        items: [
          {
            id: 'grid',
            label: 'items',
            icon: 'iconGrid',
            callback: {
              buttonCallback: () => {},
              callbackData: 'some_payload'
            }
          }
        ]
      },
      list: [
        {
          ID: 1,
          items: [
            {
              id: 'grid',
              label: 'items',
              icon: 'iconGrid',
              callback: {
                buttonCallback: () => {},
                callbackData: 'some_payload'
              }
            },
            {
              label: 'tumb',
              label: 'items',
              icon: 'iconTumb',
              callback: {
                buttonCallback: () => {},
                callbackData: 'some_payload'
              }
            }
          ]
        }
      ]
    }
   ```

* **Links**. Receives from back-end an `object` with:

  - `links: object[]` list with objects of links to render:

    - `default: object` (fallback props)

    - `list: object`(props per page to render). Also `list` (and `default`) objects must included objects with the next props:

      - `ID: number`

      - `isDropDownLayout: boolean` - links in dropdown menu if `true`

      - `items: array` with object defined as:

        - `id: string`

        - `title: string`

        - `label: string`(aria label)

        - `href: string`

        - `icon: string`

        - `onClick: (id: string, event: React.MouseEvent) => void`

  *Example*:

   ```
   const LINKS = {
     default: {
        ID: 1,
        items: [
          {
            id: '0',
            title: 'Last Games',
            href: 'loader.php?sid=kenoLastGames',
            label: 'last-spins',
            icon: 'last-spins-icon',
            onClick: (id, event) => console.log(id)
          }
        ]
     }
     list: [
       {
          ID: 1,
          items: [
            {
              id: '0',
              title: 'Last Games',
              href: 'loader.php?sid=kenoLastGames',
              label: 'last-spins',
              icon: 'last-spins-icon',
              onClick: (id, event) => console.log(id)
            },
            {
              id: '1',
              title: 'Last Games',
              href: 'loader.php?sid=kenoLastGames',
              label: 'last-spins',
              icon: 'last-spins-icon',
              onClick: (id, event) => console.log(id)
            }
          ]
       }

     ]
   }
   ```

* **Labels**. Receives an `object` with:

  - `labels: object[]` list with objects of labels to render:

    - `default: object` (fallback props)

    - `list: object`(props per page to render). Also `list` (and `default`) objects must included objects with the next props:

      - `ID: number`

      - `items: array` with object defined as:

        - `title: string`

        - `icon: string`

  *Example*:

   ```
   const LABELS = {
     default: {
        ID: 0,
        items: [
          {
            title: 'Text',
            icon: 'TextIcon'
          }
        ]
     }
     list: [
       {
          ID: 1,
          items: [
            {
              title: 'Text',
              icon: 'TextIcon'
            },
            {
              title: 'Text',
              icon: 'TextIcon'
            }
          ]
       }

     ]
   }
   ```

* **Tutorial**. The only one object with self-regulating logic. It accepts  an `object` with:

  ---  `active: boolean` state,

  ---  `default: object` a fallback props confing,

  ---  `list: object` an objects with regular pages props,

  ---  `ID: number` identificator for page to use with corresponding props,

  ---  `items: object` an objects to use for particular page,

  ---  `title: string` title of the tutorial,

  ---  `text: string` text of the tutorial.

  ---  `showTutorial: boolean` (calculates by system based on `activateOnLoad` and location change state) state make a decision to start showing tutorial onLoad page stage or leave it deactivated until user will click it,

  ---  `activateOnLoad: boolean` responded for show up / hide tutorial during initial load without explicitly clicking,

  ---  `hideByDefault: boolean` main flag, regulate show/hide tutorial on page change,


  *Example*:
   ```
   const TUTORIAL = {
      active: true,
      showTutorial: false,
      activateOnLoad: true,
      hideByDefault: false,
      default: {
        ID: 1,
        items: [
          {
            title: 'Lorem ipsum',
            text: `Lorem ipsum dolor sit amet, ea diam consul consulatu est.
            No nam velit malorum minimum, est eu esse laoreet impedit, ei eirmod corrumpit
            ullamcorper nam. An iuvaret alienum efficiantur vix`
          }
        ]
      }
      list: [
        {
          ID: 1,
          items: [
            {
              title: 'Lorem ipsum',
              text: `Lorem ipsum dolor sit amet, ea diam consul consulatu est.
                No nam velit malorum minimum, est eu esse laoreet impedit, ei eirmod corrumpit
                ullamcorper nam. An iuvaret alienum efficiantur vix`
            }
          ]
        }
      ]

   }
   ```


## How to use in App:
 ### So, the final Schema (*including all discussed above*) will have the next look:

   ```
    <AppHeader
       appID={APP_ID}
       pageID={PAGE_ID}
    />
   ```
