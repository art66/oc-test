import initialState, {
  processedObject,
  emptyList,
  defaultObject,
  noItemsToRender,
  nullObject,
  noPageIDProvided,
  noPropsProvided
} from './mocks'
import payloadFilter from '../payloadFilter'

describe('payloadFilter work logic test', () => {
  it('should return a processed object data with items included', () => {
    const { ID, props } = initialState
    expect(payloadFilter(props, ID)).toEqual(processedObject)
  })
  it('should return a default object in case of not provided data', () => {
    const { ID, props } = emptyList
    expect(payloadFilter(props, ID)).toEqual(defaultObject)
  })
  it('should return null in case of empty items list inside object provided', () => {
    const { ID, props } = noItemsToRender
    expect(payloadFilter(props, ID)).toEqual(nullObject)
  })
  it('should return null in case of not provided pageID for render', () => {
    const { ID, props } = noPageIDProvided
    expect(payloadFilter(props, ID)).toEqual(nullObject)
  })
  it('should return null in case of empty props throwed', () => {
    const { ID, props } = noPropsProvided
    expect(payloadFilter(props, ID)).toEqual(nullObject)
  })
})
