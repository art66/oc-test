const initialState = {
  ID: 1,
  props: {
    default: {
      ID: 0,
      items: [
        {
          icon: 'icon_name',
          text: 'icon_text'
        }
      ]
    },
    list: [
      {
        ID: 1,
        items: [
          {
            icon: 'icon_name',
            text: 'icon_text'
          }
        ]
      }
    ]
  }
}

export const emptyList = {
  ...initialState,
  props: {
    ...initialState.props,
    list: []
  }
}

export const processedObject = {
  ID: 1,
  items: [
    {
      icon: 'icon_name',
      text: 'icon_text'
    }
  ]
}

export const defaultObject = {
  ID: 0,
  items: [
    {
      icon: 'icon_name',
      text: 'icon_text'
    }
  ]
}

export const noItemsToRender = {
  ...initialState,
  props: {
    ...initialState.props,
    list: [
      {
        ID: 1,
        items: []
      }
    ]
  }
}

export const noPageIDProvided = {
  ...initialState,
  ID: null
}

export const noPropsProvided = {
  ...initialState,
  props: null,
  ID: null
}

export const nullObject = null

export default initialState
