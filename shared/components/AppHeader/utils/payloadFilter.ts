// Provides props filtering for page that must be rendered with:
const payloadFilter = (props, pageID: number) => {
  if (!props) return null

  const [currentProp = undefined] = props.list ? props.list.filter(prop => Number(prop.ID) === Number(pageID)) : []
  const pageIsLoaded = pageID !== null
  const propListEmpty = currentProp && currentProp.items && currentProp.items.length === 0

  // show user nothing if the empty props list received
  if (!pageIsLoaded || propListEmpty) {
    return null
  }

  // show user default props if nothing received
  if (!currentProp) {
    return props.default || null
  }

  return currentProp
}

export default payloadFilter
