// import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/topPageLinks'

const APP_HEADER_SVG_FOLDER = 'topPageLinks'

function getSVGIconConfig(iconName: string, customClass: string) {
  const svgIconConfig = {
    iconName,
    customClass,
    preset: {
      type: APP_HEADER_SVG_FOLDER,
      subtype: iconName
    }
  }

  return svgIconConfig
}

export default getSVGIconConfig
