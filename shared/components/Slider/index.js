import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Range from 'rc-slider'
import s from './range-input.cssmodule.scss'

class Slider extends Component {
  render() {
    return <Range {...this.props} prefixCls={s.inputRange} />
  }
}

export default Slider
