import React, { Component } from 'react'
import { fetchUrl } from '@torn/shared/utils'
import cn from 'classnames'
import Autocomplete from '../Autocomplete'
import './styles.scss'

type Category = {
  title: string
  requestParam: string
}

type Props = {
  autoFocus?: boolean
  inputClassName?: string
  placeholder?: string
  withCategories?: boolean // friends, faction, company, all
  onSelect?: (selected: any) => void
  onClear?: () => void
  onResponse?: (users?: { name: string; id: string }[]) => void
  onRef?: (component?: UsersAutocomplete) => void
  initSearchValue?: string
}

type State = {
  searchValue: string
  autocompleteUsers: object[]
  fetchingAutocomplete: boolean
  selectedCategory: Category
}

class UsersAutocomplete extends Component<Props, State> {
  autocompleteComponent: Autocomplete

  static defaultProps = {
    onSelect: selected => selected,
    onClear: () => {},
    onResponse: users => users,
    onRef: () => {},
    inputClassName: 'input-text-autocomplete',
    withCategories: false,
    autoFocus: false
  }

  categories: Category[] = [
    { title: 'Friends', requestParam: 'ac-friends' },
    { title: 'Faction', requestParam: 'ac-factions' },
    { title: 'Company', requestParam: 'ac-company' },
    { title: 'All', requestParam: 'ac-ct-all' }
  ]

  constructor(props: Props) {
    super(props)
    this.state = {
      searchValue: this.props.initSearchValue || '',
      autocompleteUsers: [],
      fetchingAutocomplete: false,
      selectedCategory: this.categories[3]
    }
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    this.props.onRef()
  }

  clear = () => {
    this.setState({ searchValue: '', autocompleteUsers: [] })
    this.autocompleteComponent.hideDropdown()
  }

  handleSubmit = e => {
    e.preventDefault()
  }

  loadAutocompleteData = (query, category: Category = this.state.selectedCategory) => {
    const { protocol, port, hostname } = window.location
    const option = category.requestParam
    const urlPort = port ? `:${port}` : ''
    const action = `${protocol}//${hostname}${urlPort}/autocompleteUserAjaxAction.php?q=${query}&option=${option}`

    this.setState({
      fetchingAutocomplete: true
    })

    fetchUrl(action)
      .then(data => {
        if (!data) {
          return
        }

        this.setState({
          autocompleteUsers: data,
          fetchingAutocomplete: false
        })
        this.props.onResponse(data)

        if (query !== this.state.searchValue) {
          this.loadAutocompleteData(this.state.searchValue)
        }
      })
      .catch(() => {
        this.setState({
          autocompleteUsers: [],
          fetchingAutocomplete: false
        })
      })
  }

  handleAutocompleteInputChange = e => {
    if (!this.state.fetchingAutocomplete) {
      this.loadAutocompleteData(e.target.value)
    }

    if (e.target.value === '') {
      this.props.onClear()
    }

    this.setState({ searchValue: e.target.value })
  }

  handleAutocompleteInputClick = () => {
    const { selectedCategory, searchValue } = this.state
    const { withCategories } = this.props

    withCategories && this.loadAutocompleteData(searchValue, selectedCategory)
  }

  handleAutocompleteSelect = selected => {
    this.setState({ searchValue: `${selected.name} [${selected.id}]` })
    this.props.onSelect(selected)
    this.autocompleteComponent.closeDropdown()
  }

  selectCategory = (e, category: Category) => {
    e.stopPropagation()
    this.setState({ selectedCategory: category })
    this.loadAutocompleteData(this.state.searchValue, category)
  }

  getCategories = () => {
    if (!this.props.withCategories) {
      return
    }

    const { selectedCategory } = this.state
    const categories = this.categories.map(category => (
      <li key={category.title}>
        <button
          onClick={(e) => this.selectCategory(e, category)}
          className={cn({ selected: selectedCategory.title === category.title })}
        >
          {category.title}
        </button>
      </li>
    ))

    return (
      <ul className='autocomplete-categories'>
        {categories}
      </ul>
    )
  }

  render() {
    const categories = this.getCategories()

    return (
      <Autocomplete
        name='searchAccount'
        value={this.state.searchValue}
        className={cn({ 'with-categories': this.props.withCategories })}
        placeholder={this.props.placeholder}
        inputClassName={this.props.inputClassName}
        autoFocus={this.props.autoFocus}
        list={this.state.autocompleteUsers}
        targets='users'
        onInput={this.handleAutocompleteInputChange}
        onInputClick={this.handleAutocompleteInputClick}
        onSelect={this.handleAutocompleteSelect}
        onRef={component => this.autocompleteComponent = component}
      >
        {categories}
      </Autocomplete>
    )
  }
}

export default UsersAutocomplete
