import PropTypes from 'prop-types'
import React, { Component } from 'react'
import cn from 'classnames'
import s from './button.cssmodule.scss'

class ResponsiveButton extends Component {
  static propTypes = {
    action: PropTypes.func,
    children: PropTypes.any,
    className: PropTypes.string,
    color: PropTypes.string,
    disabled: PropTypes.bool,
    responsive: PropTypes.bool
  }

  static defaultProps = {
    color: ''
  }

  render() {
    const { children, disabled, className, action, color, responsive = true } = this.props
    const c = cn(s.btn, 'torn-btn', className, { [s.disabled]: disabled }, s[color], { [s.notResponsive]: !responsive })

    return (
      <button type='button' className={c} onClick={action} aria-disabled={disabled} disabled={disabled}>
        {children}
      </button>
    )
  }
}

export default ResponsiveButton
