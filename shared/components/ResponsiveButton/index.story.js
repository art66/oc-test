import React from 'react'
import { boolean, text, select } from '@storybook/addon-knobs'
import cn from 'classnames'

import Button from '.'

const colors = ['orange', 'black', 'silver', 'steel']

export const ButtonTest = () => (
  <div className={'d r'}>
    <table>
      <tbody>
        <tr>
          <td width="100px">new</td>
          <td width="100px">old</td>
        </tr>
        <tr>
          <td>
            <Button
              disabled={boolean('disabled', false)}
              color={select('Color', colors, 'silver')}
              responsive={boolean('responsive', true)}
            >
              {text('Text', 'HELLO')}
            </Button>
          </td>

          <td>
            <div className={cn('btn-wrap', select('Color', colors, 'silver'), { disable: boolean('disabled', false) })}>
              <div className="btn">{text('Text', 'HELLO')}</div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
)

ButtonTest.story = {
  name: 'Button test'
}

export default {
  title: 'Shared/ResponsiveButton'
}
