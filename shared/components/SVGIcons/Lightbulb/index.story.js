import React from 'react'
import { ICON_SWITCH, CUSTOM_COLOR, CUSTOM_CLASS, ACTIVE_SHADOW } from './mocks'
import LightBulbIcon from './index'

export const LightBulbIconTest = () => (
  <LightBulbIcon
    iconSwitch={ICON_SWITCH}
    customClass={CUSTOM_CLASS}
    color={CUSTOM_COLOR}
    activeShadow={ACTIVE_SHADOW}
  />
)

LightBulbIconTest.story = {
  name: 'LightBulbIcon test'
}

export default {
  title: 'Shared/LightBulbIcon'
}
