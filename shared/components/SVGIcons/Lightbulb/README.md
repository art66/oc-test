
# Lightbulb - SVGIcons - Torn

# !!!!DEPRECATED!!!

### This iconchas been created just for Tutoril Component purposes, but it can be also used in other Torn's React-Apps components throw cusumizeble SVGIcons Interface. It can assept: custom color, disable/active layout states, and include any custom class provided.


## Schema:

* **iconSwitch**. Accepts only one `boolean` parament:

     ---  `iconSwitch: boolean'`. Active/Disable layouts switching.

  *Example*:
  `const ICON_SWITCH = true`

* **customClass**. Accepts only one `string` parament:

     ---  `customClass: string'`. Can be used for cusomizing icon shape (width, height, etc.).

  *Example*:
  `const CUSTOM_CLASS = 'iconCustomClass__fsffs'`

* **color**. Accepts only one `object` parament:

     ---  `color: object'`. Can be used for cusomizing icon color scheme (shape and background).

  *Example*:
  ```
  const CUSTOM_CLASS = {
    stroke: 'green',
    fill: 'transparent'
  }
  ```
* **activeShadow**. Accepts only one `boolean` parament:

     ---  `activeShadow: boolean'`. Can active/disable Icon shadow effect.

  *Example*:
  `const ACTIVE_SHADOW = true`


## How to use in App:
 ### So, the final Schema (*including all discussed above*) will have the next look:

   ```
    <Lightbulb
      iconSwitch={ICON_SWITCH}, // optional
      customClass={CUSTOM_CLASS}, // optional
      color={CUSTOM_COLOR}, // optional
      activeShadow={ACTIVE_SHADOW}, // optional
    />
   ```
