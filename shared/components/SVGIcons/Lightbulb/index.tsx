import React, { PureComponent, Fragment } from 'react'
import { IProps } from './types'
import styles from './styles/index.cssmodule.scss'

const GREY_TORN_COLOR = '#7f7f7f'
const BCKG_COLOR = 'transparent'

class LightBulbIcon extends PureComponent<IProps> {
  static defaultProps: IProps = {
    iconSwitch: false,
    customClass: '',
    color: {
      stroke: '#7f7f7f',
      fill: 'transparent'
    },
    activeShadow: false
  }

  _disableFirstLine = () => {
    return (
      <Fragment key={1}>
        <path
          fill='white'
          stroke='rgba(255, 255, 255, 0.45)'
          strokeWidth='3.5'
          d='M 1 64
            L 59 1'
        />
        <path
          fill='black'
          strokeWidth='3.5'
          d='M 0 63
            L 58 0'
        />
      </Fragment>
    )
  }

  _disableSecondLine = () => {
    return (
      <Fragment key={2}>
        <path
          fill='white'
          stroke='rgba(255, 255, 255, 0.45)'
          strokeWidth='3.5'
          d='M 0 2
            L 60 62'
        />
        <path
          fill='black'
          strokeWidth='3.5'
          d='M 1 1
            L 61 61'
        />
      </Fragment>
    )
  }

  _activeBackgroundShadow = () => {
    const { activeShadow } = this.props

    if (!activeShadow) return null

    return (
      <Fragment>
        <path
          stroke='rgba(255, 255, 255, 0.45)'
          d='M 11,32
            C 5 3 55 3 49 32'
        />
        <path
          stroke='rgba(255, 255, 255, 0.45)'
          strokeWidth='5'
          d='M 22.5 67
            C 23 76.5 40 73 37.5 67'
        />
      </Fragment>
    )
  }

  _activeDisabledIcon = () => {
    const { iconSwitch } = this.props

    if (!iconSwitch) return null

    return [this._disableFirstLine(), this._disableSecondLine()]
  }

  render() {
    const { color, customClass } = this.props
    const activeDisabledIcon = this._activeDisabledIcon()
    const activeShadow = this._activeBackgroundShadow()

    return (
      <svg
        className={`${styles['svgIcon']} ${customClass}`}
        stroke={color.stroke || GREY_TORN_COLOR}
        fill={color.fill || BCKG_COLOR}
        strokeWidth='8'
        viewBox='0 0 60 75'
      >
        {activeShadow}
         {' '}
        <path
          d='M 10,30
            C 0 0 60 0 50 30
            Q 42 38 37 49
            L 37 53
            L 23 53
            L 23 50
            Q 20 42 9.7 29'
        />
        <path
          fill='black'
          strokeWidth='5'
          d='M 20 65
            L 40 65
            C 38 65 32 75 22 66'
        />
        {activeDisabledIcon}
      </svg>
    )
  }
}

export default LightBulbIcon
