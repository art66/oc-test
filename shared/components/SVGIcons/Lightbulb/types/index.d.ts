export interface IProps {
  iconSwitch?: boolean
  customClass?: string
  color?: {
    stroke?: string
    fill?: string
  }
  activeShadow?: boolean
}
