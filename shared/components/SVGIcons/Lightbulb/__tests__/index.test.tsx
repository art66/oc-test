import React from 'react'
import { shallow } from 'enzyme'
import initialState, { withoutShadow, iconDisabled, iconDisabledWithoutShadow, customColor, customClass } from './mocks'
import LightBulbIcon from '..'

describe('<LightBulbIcon />', () => {
  it('should render LightBulbIcon icon with shadow', () => {
    const Component = shallow(<LightBulbIcon {...initialState} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('path').length).toBe(4)
    expect(Component).toMatchSnapshot()
  })
  it('should render LightBulbIcon icon without shadow', () => {
    const Component = shallow(<LightBulbIcon {...withoutShadow} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('path').length).toBe(2)
    expect(Component).toMatchSnapshot()
  })
  it('should render LightBulbIcon icon in disabled mode with shadow', () => {
    const Component = shallow(<LightBulbIcon {...iconDisabled} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('path').length).toBe(8)
    expect(Component).toMatchSnapshot()
  })
  it('should render LightBulbIcon icon in disabled mode without shadow', () => {
    const Component = shallow(<LightBulbIcon {...iconDisabledWithoutShadow} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('path').length).toBe(6)
    expect(Component).toMatchSnapshot()
  })
  it('should render LightBulbIcon icon with custom color', () => {
    const Component = shallow(<LightBulbIcon {...customColor} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('stroke')).toBe('green')
    expect(Component).toMatchSnapshot()
  })
  it('should render LightBulbIcon icon with custom className', () => {
    const Component = shallow(<LightBulbIcon {...customClass} />)

    expect(Component.find('svg').length).toBe(1)
    expect(Component.find('svg').prop('className')).toBe('svgIcon test_class')
    expect(Component).toMatchSnapshot()
  })
})
