const initialState = {
  iconSwitch: false,
  customClass: 'customClass',
  color: {
    stroke: 'black',
    fill: 'transparent'
  },
  activeShadow: true
}

export const withoutShadow = {
  ...initialState,
  activeShadow: false
}

export const iconDisabled = {
  ...initialState,
  iconSwitch: true
}

export const iconDisabledWithoutShadow = {
  ...iconDisabled,
  activeShadow: false
}

export const customColor = {
  ...initialState,
  color: {
    ...initialState.color,
    stroke: 'green'
  }
}

export const customClass = {
  ...initialState,
  customClass: 'test_class'
}

export default initialState
