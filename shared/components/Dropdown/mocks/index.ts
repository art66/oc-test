import { IProps } from '../container/types'

export const DEFAULT_STATE: IProps = {
  className: 'react-dropdown-default',
  list: [
    { id: 'one', name: 'one' },
    { id: 'two', name: 'two' },
    { id: 'tree', name: 'tree' },
    { id: 'fore', name: 'fore' },
    { id: 'five', name: 'five' },
    { id: 'six', name: 'six' },
    { id: 'seven', name: 'seven' },
    { id: 'eight', name: 'eight' },
    { id: 'ten', name: 'ten' }
  ],
  placeholder: 'Select'
}

export const DISABLED_OPTIONS = [false, true]

export const SEARCH_OPTIONS = [false, true]
