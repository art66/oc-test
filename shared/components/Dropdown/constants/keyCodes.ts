export const KEY_TAB = 9
export const KEY_ARROW_UP = 38
export const KEY_ARROW_DOWN = 40
export const KEY_ENTER = 13
export const KEY_ESC = 27
export const KEY_SPACE = 32
export const KEY_HOME = 36
export const KEY_END = 35
