import { IListItem } from '../components/Item/types'

interface ICustomClasses {
  mainWrapper?: string
  toggle?: string
  toggleDisabled?: string
  contentWrapper?: string
  inputWrapper?: string
  input?: string
  list?: string
  listItem?: string
  scrollbar?: string
}

export interface IProps {
  id?: number | string
  /** @deprecated */
  tabIndex?: number | string
  list?: IListItem[]
  name?: string
  selected?: IListItem | IListItem[]
  placeholder?: string
  /** @deprecated use classes instead */
  className?: string
  classes?: ICustomClasses
  search?: boolean
  disabled?: boolean
  controlled?: boolean
  multiselect?: boolean
  limit?: number
  onChange?: (item: IListItem | IListItem[], props: IProps) => void
  onInit?: (item: IListItem | IListItem[], props: IProps) => void
  onClose?: () => void
  selectedId?: boolean
}

export interface IState {
  opened: boolean
  searchValue: string
  selected?: any
  isShowTooltip: boolean
  dropdownPosition: 'down' | 'up'
}

export interface IMethods {
  dropdownWrapper?: React.RefObject<HTMLInputElement>
  scrollArea?: React.RefObject<HTMLInputElement>
}
