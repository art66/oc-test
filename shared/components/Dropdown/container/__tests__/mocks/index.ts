import { IProps } from '../../types'

export const initialState: IProps = {
  id: null,
  className: null,
  tabIndex: 3,
  name: 'testDropdownName',
  list: [
    { id: '0', name: 'first' },
    { id: '1', name: 'second' },
    { id: '2', name: 'third' },
    { id: '3', name: 'fourth' }
  ],
  placeholder: 'Test placeholder',
  search: false,
  onChange: () => {},
  onInit: () => {}
}

export const classifiedState: IProps = {
  ...initialState,
  className: 'testClassName'
}

export const identifiedState: IProps = {
  ...initialState,
  id: '5'
}

export const selectedState: IProps = {
  ...initialState,
  selected: { id: '1', name: 'second' }
}

export const disabledState: IProps = {
  ...initialState,
  disabled: true
}
