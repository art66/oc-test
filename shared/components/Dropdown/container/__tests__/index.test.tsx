import React from 'react'
import { mount } from 'enzyme'
import Dropdown from '../index'

import { initialState, classifiedState, selectedState, identifiedState, disabledState } from './mocks'

import { IProps, IState } from '../types'

describe('<Dropdown />', () => {
  it('should render default', () => {
    const Component = mount(<Dropdown />)

    expect(Component.find('.react-dropdown-default').length).toBe(1)
    const RootComponent = Component.find('.react-dropdown-default')
    expect(RootComponent.prop('className')).toBe('react-dropdown-default')
    expect(RootComponent.prop('className')).not.toBe('dropdown-opened')

    expect(RootComponent.find('input').length).toBe(1)
    const InputComponent = RootComponent.find('input')
    expect(InputComponent.prop('type')).toBe('hidden')
    expect(InputComponent.prop('name')).toBe(null)
    expect(InputComponent.prop('value')).toBe('')
    expect(InputComponent.prop('disabled')).toBe(false)

    expect(RootComponent.find('.toggler').length).toBe(1)
    const ActivatorComponent = RootComponent.find('.toggler')
    expect(ActivatorComponent.text()).toBe('')

    expect(RootComponent.find('.dropdownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render state', () => {
    const Component = mount(<Dropdown {...initialState} />)

    expect(Component.find('.react-dropdown-default').length).toBe(1)
    const RootComponent = Component.find('.react-dropdown-default')
    expect(RootComponent.prop('className')).toBe('react-dropdown-default')
    expect(RootComponent.prop('className')).not.toBe('dropdown-opened')
    expect(RootComponent.find('input').length).toBe(1)
    const InputComponent = RootComponent.find('input')
    expect(InputComponent.prop('type')).toBe('hidden')
    expect(InputComponent.prop('name')).toBe('testDropdownName')
    expect(InputComponent.prop('value')).toBe('')
    expect(InputComponent.prop('disabled')).toBe(false)

    expect(RootComponent.find('.toggler').length).toBe(1)

    const ActivatorComponent = RootComponent.find('.toggler')

    expect(ActivatorComponent.text()).toBe('Test placeholder')

    expect(RootComponent.find('.dropdownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render classified', () => {
    const Component = mount(<Dropdown {...classifiedState} />)

    expect(Component.find('.react-dropdown-default.testClassName').length).toBe(1)
    expect(Component.find('.testClassName').hostNodes().length).toBe(1)
    const RootComponent = Component.find('.testClassName').hostNodes()
    expect(RootComponent.prop('className')).toContain('testClassName')

    expect(RootComponent.find('input').length).toBe(1)
    const InputComponent = RootComponent.find('input')
    expect(InputComponent.prop('type')).toBe('hidden')
    expect(InputComponent.prop('name')).toBe('testDropdownName')
    expect(InputComponent.prop('value')).toBe('')
    expect(InputComponent.prop('disabled')).toBe(false)

    expect(RootComponent.find('.toggler').length).toBe(1)
    const ActivatorComponent = RootComponent.find('.toggler')
    expect(ActivatorComponent.text()).toBe('Test placeholder')

    expect(RootComponent.find('.dropdownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render selected', () => {
    const Component = mount(<Dropdown {...selectedState} />)

    expect(Component.find('.react-dropdown-default').length).toBe(1)
    const RootComponent = Component.find('.react-dropdown-default')
    expect(RootComponent.prop('className')).toBe('react-dropdown-default')
    expect(RootComponent.prop('className')).not.toBe('dropdown-opened')

    expect(RootComponent.find('input').length).toBe(1)
    const InputComponent = RootComponent.find('input')
    expect(InputComponent.prop('type')).toBe('hidden')
    expect(InputComponent.prop('name')).toBe('testDropdownName')
    expect(InputComponent.prop('value')).toBe('1')
    expect(InputComponent.prop('disabled')).toBe(false)

    expect(RootComponent.find('.toggler').length).toBe(1)
    const ActivatorComponent = RootComponent.find('.toggler')
    expect(ActivatorComponent.text()).toBe('second')

    expect(RootComponent.find('.dropdownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should change selected value', () => {
    const Component = mount(<Dropdown {...selectedState} />)

    Component.setState({
      selected: { id: '2', name: 'third' }
    })
    expect(Component.find('input').prop('value')).toBe('2')
    expect(Component.find('.toggler').text()).toBe('third')

    expect(Component).toMatchSnapshot()
  })

  it('should render identified', () => {
    const Component = mount(<Dropdown {...identifiedState} />)

    expect(Component.find('.react-dropdown-default').length).toBe(1)
    const RootComponent = Component.find('.react-dropdown-default')
    expect(RootComponent.prop('className')).toBe('react-dropdown-default')
    expect(RootComponent.prop('className')).not.toBe('dropdown-opened')
    expect(RootComponent.find('input').length).toBe(1)
    const InputComponent = RootComponent.find('input')
    expect(InputComponent.prop('type')).toBe('hidden')
    expect(InputComponent.prop('name')).toBe('testDropdownName')
    expect(InputComponent.prop('value')).toBe('')
    expect(InputComponent.prop('disabled')).toBe(false)

    expect(RootComponent.find('.toggler').length).toBe(1)
    const ActivatorComponent = RootComponent.find('.toggler')
    expect(ActivatorComponent.text()).toBe('Test placeholder')

    expect(RootComponent.find('.dropdownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render disabled', () => {
    const Component = mount(<Dropdown {...disabledState} />)

    expect(Component.find('.react-dropdown-default').length).toBe(1)
    const RootComponent = Component.find('.react-dropdown-default')
    expect(RootComponent.prop('className')).toBe('react-dropdown-default disabled')

    expect(RootComponent.find('input').length).toBe(1)
    const InputComponent = RootComponent.find('input')
    expect(InputComponent.prop('type')).toBe('hidden')
    expect(InputComponent.prop('name')).toBe('testDropdownName')
    expect(InputComponent.prop('value')).toBe('')
    expect(InputComponent.prop('disabled')).toBe(true)

    expect(RootComponent.find('.toggler').length).toBe(1)
    const ActivatorComponent = RootComponent.find('.toggler')
    expect(ActivatorComponent.text()).toBe('Test placeholder')

    expect(RootComponent.find('.dropdownList').length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should toggle dropdown', () => {
    const Component = mount<IProps, IState>(<Dropdown {...initialState} />)

    expect(Component.state().opened).toBe(false)

    Component.find('.toggler').simulate('click')

    expect(Component.state().opened).toBe(true)

    expect(Component).toMatchSnapshot()
  })
})
