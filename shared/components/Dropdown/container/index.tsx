import React, { ChangeEvent } from 'react'
import cn from 'classnames'
import TooltipGlobal, { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { isTouchDevice } from '@torn/shared/utils/isTouchDevice'
import { IProps, IState, IMethods } from './types'
import { IListItem } from '../components/Item/types'
import * as keyCodes from '../constants/keyCodes'
import Item from '../components/Item'
import { getSpaceBelowElement } from '../utils/getSpaceBelowElement'
import styles from '../styles/multiselectTooltip.cssmodule.scss'
import '../styles/DM_vars.scss'
import '../styles/vars.scss'

const initialCharSearch = {
  timeoutID: null,
  input: '',
  matchedItems: [],
  cursor: 0
}

const LIMIT_TOOLTIP = 9

const POSITION = {
  x: 'center',
  y: 'top'
}

const STYLES = {
  container: styles.tooltipContainer,
  title: styles.tooltipTitle,
  tooltipArrow: styles.tooltipArrow
}

class Dropdown extends React.PureComponent<IProps, IState> implements IMethods {
  dropdownWrapper = null

  _focusedItemIndex: number

  _charSearch: {
    timeoutID: number
    input: string
    matchedItems: number[]
    cursor: number
  }

  _refs: {
    wrapper: React.RefObject<HTMLDivElement>
    toggle: React.RefObject<HTMLButtonElement>
    dropdown: React.RefObject<HTMLDivElement>
    input: React.RefObject<HTMLInputElement>
    list: React.RefObject<HTMLUListElement>
  }

  static defaultProps = {
    id: null,
    tabIndex: 1,
    list: [],
    name: null,
    onChange: () => {},
    onInit: () => {},
    limit: 10,
    multiselect: false,
    placeholder: null,
    className: null,
    search: false,
    disabled: false,
    controlled: false,
    classes: {
      scrollbar: 'scrollbar-bright'
    },
    selectedId: false
  }

  constructor(props: IProps, context: object) {
    super(props, context)

    const { selected } = this.props

    this._focusedItemIndex = 0
    this._refs = {
      wrapper: React.createRef(),
      toggle: React.createRef(),
      dropdown: React.createRef(),
      input: React.createRef(),
      list: React.createRef()
    }
    this._charSearch = {
      ...initialCharSearch
    }
    this.state = {
      opened: false,
      isShowTooltip: true,
      selected,
      searchValue: '',
      dropdownPosition: 'down'
    }
  }

  componentDidMount() {
    const { selected, onInit, id, multiselect } = this.props

    window.addEventListener('click', this._handleClickOutside, false)

    if (onInit && selected) {
      onInit(selected, this.props)
    }

    multiselect && tooltipsSubscriber.subscribe({ child: this._getMultiselectTooltipText(), ID: `${id}` })
  }

  componentDidUpdate(prevProps: IProps, prevState: IState) {
    const { disabled, selected, id, onClose, multiselect } = this.props
    const { opened } = this.state

    if (!prevProps.disabled && disabled) {
      this._handleClose()
    }

    if (JSON.stringify(prevProps.selected) !== JSON.stringify(selected)) {
      this._updateSelected(selected)
    }

    if (prevState.opened && !opened) {
      onClose && onClose()
      this._updateIsShowTooltip(true)
    }

    if (prevState.opened !== opened) {
      this.updateDropdownPosition()
    }

    multiselect && tooltipsSubscriber.update({ child: this._getMultiselectTooltipText(), ID: `${id}` })
  }

  componentWillUnmount() {
    clearTimeout(this._charSearch.timeoutID)
    window.removeEventListener('click', this._handleClickOutside, false)
    tooltipsSubscriber.unsubscribe({ child: this._getMultiselectTooltipText(), ID: `${this.props.id}` })
  }

  _getSelectedValue() {
    const { selected } = this.state

    return (selected && (selected.id ?? selected.ID)) || ''
  }

  _getMultiselectSelectedValues() {
    const { selected } = this.state

    return (selected && selected.map(item => item.id ?? item.ID).join(', ')) || ''
  }

  _getPlaceholder() {
    const { placeholder, selectedId } = this.props
    const { selected } = this.state

    return (selected && (selectedId ? selected.id : selected.name)) || placeholder
  }

  _getMultiselectPlaceholder() {
    const { placeholder, selectedId } = this.props
    const { selected } = this.state

    return (selected && selected.map(item => (selectedId ? item.id : item.name)).join(', ')) || placeholder
  }

  _getMultiselectTooltipText() {
    const { selected } = this.state
    const { multiselect } = this.props

    if (multiselect) {
      const elements = selected && selected.map((item, index) => {
        const andMore = selected.length - 10 !== 0 ? `and ${selected.length - 10} more` : null

        if (index <= LIMIT_TOOLTIP) {
          return index === LIMIT_TOOLTIP ?
            <div key={item.name}>{item.name} {andMore}</div> :
            <div key={item.name}>{item.name}</div>
        }
      })

      return <div>{elements}</div>
    }

    return null
  }

  _renderMultiselectTooltip() {
    const { multiselect } = this.props

    return multiselect && this.state.isShowTooltip && <TooltipGlobal position={POSITION} overrideStyles={STYLES} />
  }

  _getFilteredList = list => {
    const { searchValue } = this.state
    const strictMatch = []
    const match = []

    if (!searchValue) return list

    list && list.forEach(item => {
      const isMatchStrict = searchValue.toLowerCase() === item.name.slice(0, searchValue.length).toLowerCase()
      const isMatch = item.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1 && !isMatchStrict

      searchValue && isMatchStrict && strictMatch.push(item)
      searchValue && isMatch && match.push(item)
    })

    return strictMatch.concat(match)
  }

  _getMatchedItems = () => {
    const { list } = this.props

    return this._getFilteredList(list).reduce((acc, el, i) => {
      if (this._charSearch.input.toLowerCase() === el.name.toLowerCase().slice(0, this._charSearch.input.length)) {
        return [...acc, i]
      }

      return acc
    }, [])
  }

  _setFocusedItemByIndex = (index: number) => {
    const { list } = this.props

    this._focusedItemIndex = Math.min(list.length - 1, Math.max(index, 0))
    this._setFocusOnItemByIndex(this._focusedItemIndex)
  }

  _setFocusOnItemByIndex = (index: number) => {
    const nextFocused = this._refs.list.current.children[index] as HTMLElement

    nextFocused && nextFocused.focus()
  }

  _setFocusOnSelectedItem = () => {
    const { selected, list } = this.props
    const selectedIndex = list.findIndex(li =>
      (li?.id ?? li?.ID) === ((selected as IListItem)?.id ??
      (selected as IListItem)?.ID) && li?.name === (selected as IListItem)?.name)

    this._setFocusedItemByIndex(selectedIndex)
  }

  _setFocusOnSelectedItems = () => {
    const { selected, list } = this.props
    const selectedIndex = list.findIndex(li =>
      (li?.id ?? li?.ID) === (selected[0]?.id ?? selected[0]?.ID) && li?.name === selected[0]?.name)

    this._setFocusedItemByIndex(selectedIndex)
  }

  _setFocusOnToggle = () => {
    this._refs.toggle.current.focus()
    this._handleClose()
  }

  _moveFocusFromItemToList = () => {
    // focus is braking functionality on touch devices
    // so we disabled it
    if (isTouchDevice()) {
      return
    }

    const focused = this._refs.list.current.children[this._focusedItemIndex] as HTMLElement

    focused && focused.blur()
    this._refs.list.current.focus()
  }

  _updateSelected = (selected: IListItem | IListItem[]) => {
    this.setState({ selected })
  }

  _updateIsShowTooltip = (isShowTooltip: boolean) => {
    this.setState({ isShowTooltip })
  }

  _handleToggle = () => {
    const { disabled } = this.props
    const { opened } = this.state

    if (disabled) return false

    opened ? this._handleClose() : this._handleOpen()
  }

  _handleOpen = () => {
    const { multiselect } = this.props

    this.setState({ opened: true }, () => {
      if (this._refs.input.current) {
        this._refs.input.current.focus()

        if (this._refs.list.current) {
          multiselect ? this._setFocusOnSelectedItems() : this._setFocusOnSelectedItem()
        }
      }
    })
  }

  _handleClickOutside = (e: any) => {
    if (this._refs.wrapper && !this._refs.wrapper.current.contains(e.target)) {
      this._handleClose()
    }
  }

  _handleClose = () => {
    this._focusedItemIndex = 0
    this._charSearch = { ...initialCharSearch }
    this.setState({ opened: false, searchValue: '' })
  }

  _handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchValue: e.target.value })
  }

  _handleSelect = (item: IListItem) => {
    const { onChange, controlled } = this.props
    const { selected } = this.state

    this.setState({
      selected: controlled ? selected : item,
      opened: false
    })

    if (onChange) {
      onChange(item, this.props)
    }
  }

  _handleMultiselect = (item: IListItem, e: any) => {
    const { onChange, controlled, multiselect, limit } = this.props
    const { selected } = this.state
    const isClickedCheckbox = e.target.classList.contains('checkbox')

    if (this.state.isShowTooltip) {
      this._updateIsShowTooltip(false)
    }

    const itemList = selected.filter(el => (el.id ?? el.ID) !== -1)
    const isSelected = itemList.find(el => (el.id ?? el.ID) === (item.id ?? item.ID) && el.name === item.name)
    let newSelected = isSelected ?
      itemList.filter(el => (el.id ?? el.ID) !== (item.id ?? item.ID) && el.name !== item.name) :
      [...itemList, item]

    if ((item.id ?? item.ID) === -1) {
      newSelected.length = 0
      newSelected.push(item)
    }

    if ([...itemList, item].length > limit) {
      if (isSelected) {
        newSelected = itemList.filter(el => (el.id ?? el.ID) !== (item.id ?? item.ID) && el.name !== item.name)
      } else {
        return
      }
    }

    this.setState({
      selected: controlled && multiselect && isClickedCheckbox ? newSelected : [item],
      opened: !!isClickedCheckbox
    })

    if (onChange) {
      onChange(isClickedCheckbox ? newSelected : [item], this.props)
    }
  }

  _handleCharSearch = (key: string) => {
    clearTimeout(this._charSearch.timeoutID)

    if (this._charSearch.input !== key) {
      this._charSearch.input += key
    }

    this._charSearch.matchedItems = this._getMatchedItems()

    if (this._charSearch.matchedItems.length) {
      if (this._charSearch.input.length <= 1) {
        this._charSearch.cursor = this._charSearch.matchedItems.findIndex(i => i > this._focusedItemIndex)
      }

      if (this._charSearch.cursor < 0 || this._charSearch.cursor > this._charSearch.matchedItems.length) {
        this._charSearch.cursor = 0
      }

      this._setFocusedItemByIndex(this._charSearch.matchedItems[this._charSearch.cursor])
    }

    // @ts-ignore
    this._charSearch.timeoutID = setTimeout(() => {
      this._charSearch = {
        ...this._charSearch,
        input: ''
      }
    }, 750)
  }

  _handleListKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
    const { list } = this.props
    const keyCode = e.which

    e.preventDefault()

    if (e.key.length === 1) {
      this._handleCharSearch(e.key.toLowerCase())
      return
    }

    switch (keyCode) {
      case keyCodes.KEY_ARROW_DOWN: {
        this._setFocusedItemByIndex(this._focusedItemIndex + 1)
        break
      }
      case keyCodes.KEY_ARROW_UP: {
        this._setFocusedItemByIndex(this._focusedItemIndex - 1)
        break
      }
      case keyCodes.KEY_TAB: {
        if (e.shiftKey && this._refs.input.current) {
          this._refs.input.current.focus()
          break
        }

        this._setFocusOnToggle()
        break
      }
      case keyCodes.KEY_HOME: {
        this._setFocusedItemByIndex(0)
        break
      }
      case keyCodes.KEY_END: {
        this._setFocusedItemByIndex(list.length)
        break
      }
      default: break
    }
  }

  _handleToggleKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
    if (e.which === keyCodes.KEY_ARROW_UP || e.which === keyCodes.KEY_ARROW_DOWN) {
      e.preventDefault()
      this._handleOpen()
      this._setFocusOnSelectedItem()
    }
  }

  _handleDropdownKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
    if (e.which === keyCodes.KEY_ESC) {
      e.preventDefault()
      this._setFocusOnToggle()
    }
  }

  _handleInputKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
    if (e.which === keyCodes.KEY_TAB && (e.shiftKey || !this._refs.list.current)) {
      e.preventDefault()
      this._setFocusOnToggle()
    }

    if (e.which === keyCodes.KEY_TAB && !e.shiftKey) {
      e.preventDefault()
      this._refs.list.current ? this._setFocusedItemByIndex(0) : this._setFocusOnToggle()
    }
  }

  _renderSearch = () => {
    const { search, classes } = this.props
    const { searchValue } = this.state

    return search && (
      <div className={cn('dropdown-search', classes.inputWrapper)}>
        <input
          ref={this._refs.input}
          maxLength={50}
          placeholder='Filter list'
          className={cn(classes.input)}
          value={searchValue}
          onKeyDown={this._handleInputKeyDown}
          onChange={this._handleInputChange}
        />
      </div>
    )
  }

  getDropdownPosition = () => {
    if (!this._refs.dropdown) {
      return 'down'
    }

    return getSpaceBelowElement(this._refs.dropdown.current) <= 0 ? 'up' : 'down'
  }

  updateDropdownPosition = () => {
    this.setState({
      dropdownPosition: this.getDropdownPosition()
    })
  }

  _renderDropdownList = () => {
    const { classes } = this.props
    const { dropdownPosition } = this.state

    return (
      <div
        className={cn('dropdownList', classes.contentWrapper, dropdownPosition)}
        ref={this._refs.dropdown}
        onKeyDown={this._handleDropdownKeyDown}
      >
        {this._renderSearch()}
        {this._renderList()}
      </div>
    )
  }

  _renderList = () => {
    const { classes, list, multiselect } = this.props
    const { searchValue, dropdownPosition } = this.state
    const filteredList = this._getFilteredList(list)

    if (!filteredList.length && searchValue) {
      return (
        <div role='alert' className='scroll-area listEmpty'>
          {`No items matched "${searchValue}" were found`}
        </div>
      )
    }

    return (
      <ul
        onKeyDown={this._handleListKeyDown}
        onMouseEnter={this._moveFocusFromItemToList}
        role='listbox'
        tabIndex={0}
        className={cn(
          'scrollarea scroll-area dropdown-content',
          classes.list,
          classes.scrollbar,
          { multiselect },
          dropdownPosition
        )}
        ref={this._refs.list}
      >
        {this._renderListItems(filteredList)}
      </ul>
    )
  }

  _renderListItems = (filteredList: IListItem[]) => {
    const { classes, selected, multiselect } = this.props

    return filteredList.map(item => {
      const foundItem = multiselect && selected && (selected as IListItem[]).find(el =>
        (el.id ?? el.ID) === (item.id ?? item.ID) && el.name === item.name)
      const isSelected = multiselect && foundItem ? true : (selected as IListItem)?.name === item.name

      return (
        <Item
          key={`option-${item.name}`}
          itemClass={classes.listItem}
          item={item}
          isSelected={isSelected}
          multiselect={multiselect}
          onClickSelect={multiselect ? this._handleMultiselect : this._handleSelect}
        />
      )
    })
  }

  render() {
    const { className, name, disabled, classes, multiselect, id } = this.props
    const { opened, dropdownPosition } = this.state
    const wrapperClasses = cn(
      'react-dropdown-default',
      className,
      classes.mainWrapper,
      { disabled, 'dropdown-opened': opened }
    )
    const tooglerClasses = cn(
      'toggler',
      classes.toggle,
      { [classes.toggleDisabled]: disabled, multiselect: multiselect },
      dropdownPosition
    )

    return (
      <div ref={this._refs.wrapper} className={wrapperClasses}>
        <input
          type='hidden'
          name={name}
          disabled={disabled}
          value={multiselect ? this._getMultiselectSelectedValues() : this._getSelectedValue()}
        />
        <button
          id={id ? `${id}` : null}
          type='button'
          className={tooglerClasses}
          aria-expanded={opened}
          aria-haspopup='listbox'
          ref={this._refs.toggle}
          onClick={this._handleToggle}
          onKeyDown={this._handleToggleKeyDown}
        >
          {multiselect ? this._getMultiselectPlaceholder() : this._getPlaceholder()}
        </button>
        {this._renderMultiselectTooltip()}
        {this._renderDropdownList()}
      </div>
    )
  }
}

export default Dropdown
