import { getScrollHeight } from '@torn/shared/utils/getScrollHeight'

export const getSpaceBelowElement = (element: HTMLElement): number => {
  const dropdownListHeight = element && element.offsetHeight
  const topOffset = element && element.getBoundingClientRect().top + window.scrollY
  const chatHeight = document.getElementById('chatRoot')?.children[0]?.clientHeight || 0

  return getScrollHeight() - (topOffset + dropdownListHeight + chatHeight)
}
