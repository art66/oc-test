import React from 'react'
import { select } from '@storybook/addon-knobs'
import { State, Store } from '@sambego/storybook-state'

import Dropdown from '.'

import { DEFAULT_STATE, DISABLED_OPTIONS, SEARCH_OPTIONS } from './mocks'

export const DropdownTest = () => {
  const config = {
    ...DEFAULT_STATE,
    disabled: select('Disabled:', DISABLED_OPTIONS, DISABLED_OPTIONS[0], 'options'),
    search: select('With search:', SEARCH_OPTIONS, SEARCH_OPTIONS[0], 'options')
  }

  const STORE_CONFIG = {
    onChange: (value) => {
      store.set({
        ...STORE_CONFIG,
        selected: value
      })
    },
    selected: null
  }

  const store = new Store(STORE_CONFIG)

  return (
    <State store={store}>
      <Dropdown {...config} />
    </State>
  )
}

DropdownTest.story = {
  name: 'Dropdown test'
}

export default {
  title: 'Shared/Dropbox'
}
