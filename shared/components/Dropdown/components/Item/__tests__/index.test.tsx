import React from 'react'
import { mount } from 'enzyme'
import Item from '../index'

import { initialState } from './mocks'

import { IProps } from '../types'

describe('<Item />', () => {
  it('should render default', () => {
    const Component = mount<IProps>(<Item {...initialState} />)

    expect(Component.find('.item').length).toBe(1)
    expect(Component.find('.item').text()).toBe('fourth')

    expect(Component).toMatchSnapshot()
  })

  it('should handle on click', () => {
    const onClickSelect = jest.fn()
    const state = { ...initialState, onClickSelect }
    const Component = mount<IProps>(<Item {...state} />)

    expect(Component.find('.item').length).toBe(1)

    Component.find('.item').simulate('click')

    expect(onClickSelect).toHaveBeenCalledTimes(1)
    expect(onClickSelect.mock.calls[0][0]).toEqual({ id: '3', name: 'fourth' })

    expect(onClickSelect).toMatchSnapshot()
  })
})
