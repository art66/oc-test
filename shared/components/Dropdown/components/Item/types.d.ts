export interface IListItem {
  id?: number | string
  ID?: number | string
  name: string
  customClass?: string
}

export interface IProps {
  itemClass?: string
  item: IListItem
  isSelected: boolean
  onClickSelect: (item: IListItem | IListItem[], e?: any) => void
  multiselect: boolean
}
