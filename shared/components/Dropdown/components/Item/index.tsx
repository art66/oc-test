import React from 'react'
import cn from 'classnames'

import { IProps } from './types'
import { KEY_ENTER } from '../../constants/keyCodes'

export class Item extends React.PureComponent<IProps> {
  _handleClick = e => {
    e.preventDefault()
    const { item, onClickSelect, multiselect } = this.props

    multiselect ? onClickSelect(item, e) : onClickSelect(item)
  }

  _handleKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
    const { item, onClickSelect, multiselect } = this.props

    if (e.which === KEY_ENTER) {
      multiselect ? onClickSelect(item, e) : onClickSelect(item)
    }
  }

  renderMultiselectCheckbox = () => {
    const { multiselect, isSelected } = this.props

    return multiselect && (
      <label className='checkbox checkbox-button'>
        <input
          type='checkbox'
          className='checkbox checkbox-button__input'
          checked={isSelected}
          onChange={this._handleClick}
        />
        <span className='checkbox checkbox-button__control' />
      </label>
    )
  }

  render() {
    const {
      item: { name, customClass },
      itemClass,
      isSelected
    } = this.props

    return (
      <li
        tabIndex={0}
        role='option'
        aria-selected={isSelected}
        id={`option-${name}`.replace(/\s+/g, '-')}
        className={cn('item', itemClass, customClass, { selected: isSelected })}
        onClick={this._handleClick}
        onKeyDown={this._handleKeyDown}
      >
        {name}
        {this.renderMultiselectCheckbox()}
      </li>
    )
  }
}

export default Item
