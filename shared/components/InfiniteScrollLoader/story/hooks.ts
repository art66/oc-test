import { useState, useCallback } from 'react'
import { ITEMS } from './mocks'
import { ITEMS_PER_LOADING_COUNT } from './constants'

export function MockRequestHook() {
  const [isLoading, setIsLoading] = useState(false)
  const [items, setItems] = useState(ITEMS.slice(0, ITEMS_PER_LOADING_COUNT))

  const onLoading = useCallback(() => {
    setIsLoading(true)

    setTimeout(() => {
      const nextItems = ITEMS.slice(0, items.length + ITEMS_PER_LOADING_COUNT)

      setItems(nextItems)

      setIsLoading(false)
    }, 1000)
  }, [items.length])

  return {
    isLoading,
    onLoading,
    items
  }
}
