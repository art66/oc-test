# Global InfiniteScrollLoader Component

### Schema:

```
  <InfiniteScrollLoader
    isLoading={boolean} // required, show the status of loading
    onLoading={() => { loadMore() }} // required, a callback to load more data
  />
```
