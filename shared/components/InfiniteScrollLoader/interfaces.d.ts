export interface IProps {
  isLoading: boolean
  onLoading: () => any
  children: JSX.Element
}
