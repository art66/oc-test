import React, { useCallback, useEffect, useRef, memo } from 'react'
import debounce from 'lodash.debounce'
import { IProps } from './interfaces'

export default memo((props: IProps) => {
  const { isLoading, onLoading, children } = props

  const ref = useRef(null)

  const onWindowScroll = useCallback(
    debounce(() => {
      const element = ref.current

      if (isLoading || !element) return

      const scrollY = document.documentElement.scrollTop
      const extraScrollY = scrollY + window.innerHeight - (element.offsetTop + element.clientHeight)

      if (extraScrollY >= 0) {
        onLoading()
      }
    }, 30),
    [ref, isLoading]
  )

  useEffect(() => {
    window.addEventListener('scroll', onWindowScroll)

    onWindowScroll()

    return () => {
      window.removeEventListener('scroll', onWindowScroll)
    }
  }, [onWindowScroll])

  return <>{React.cloneElement(children, { ref })}</>
})
