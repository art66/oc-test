import React, { useEffect } from 'react'
import InfiniteScrollLoader from '.'
import { MockRequestHook } from './story/hooks'
import LoadingIndicator from '../LoadingIndicator'
import styles from './story/index.cssmodule.scss'

export const Default = () => {
  const { isLoading, onLoading, items } = MockRequestHook()

  useEffect(() => {
    const containerEl = document.querySelector('#root').children[0] as HTMLDivElement

    containerEl.style.position = 'relative'

    return () => {
      containerEl.style.position = 'fixed'
    }
  }, [])

  return (
    <div className={styles.container}>
      <InfiniteScrollLoader isLoading={isLoading} onLoading={onLoading}>
        <ul>
          {items.map(item => {
            return (
              <li key={item.id} className={styles.item}>
                {item.id}
              </li>
            )
          })}
        </ul>
      </InfiniteScrollLoader>
      {isLoading && <LoadingIndicator />}
    </div>
  )
}

Default.story = {
  name: 'default'
}

export default {
  title: 'Shared/InfiniteScrollLoader'
}
