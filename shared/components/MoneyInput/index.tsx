/* eslint-disable react/no-unused-state */
import React from 'react'
import classnames from 'classnames'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { toMoney } from '@torn/shared/utils'
import moneyShorter from '@torn/shared/utils/moneyShorter'
import StringChecker from '@torn/shared/utils/stringChecker'
import keyDownHandler from '@torn/shared/utils/keyDownHandler'

import { TOOLTIP_PRE, TOOLTIP_POS, INPUT_CHECKER_CONFIG, ANIM_PROPS, SIGN, INPUT_ID } from './constants'
import { IProps, IState } from './interfaces'

import styles from './index.cssmodule.scss'

class MoneyInput extends React.Component<IProps, IState> {
  private _stringChecker: any
  private _ref: React.RefObject<HTMLInputElement>

  constructor(props: IProps) {
    super(props)

    this.state = {
      inputValue: '',
      isSpaceInputted: false,
      isSymbolAllowed: true,
      isMaxAmount: false
    }

    this._ref = React.createRef()
  }

  componentDidMount() {
    const { disableOnMountInputFocus } = this.props

    this._initializeStringChecker()
    this._mountButtonTooltip()
    !disableOnMountInputFocus && this._forceInputFocus()
  }

  _initializeStringChecker = () => {
    this._stringChecker = new StringChecker(INPUT_CHECKER_CONFIG)
  }

  _mountButtonTooltip = () => {
    const { disableButtonTooltip, tooltipText } = this.props

    if (disableButtonTooltip) {
      return
    }

    tooltipsSubscriber.render({
      tooltipsList: [{ ID: INPUT_ID, child: tooltipText || `${TOOLTIP_PRE} ${TOOLTIP_POS}` }],
      type: 'mount'
    })
  }

  _replaceComma = (value: string) => value.replace(/,/gi, '')

  _setRestrictionsOnInput = (isSpaceInputted, isSymbolAllowed, isMaxAmount) => {
    this.setState({ isSpaceInputted, isSymbolAllowed, isMaxAmount })
  }

  _checkString = ({ targetValue }) => {
    const { checkString: sanitizeString } = this._stringChecker
    const { userMoney } = this.props

    const normalizedInputValue = this._replaceComma(targetValue)
    const inputValueWithShortCommands = moneyShorter({ value: normalizedInputValue, userMoney })

    const { value, isSpaceInputted, isSymbolAllowed } = sanitizeString({ text: inputValueWithShortCommands })

    const isMaxAmount = Number(value) > Number(userMoney)

    this._setRestrictionsOnInput(isSpaceInputted, isSymbolAllowed, isMaxAmount)

    return {
      value,
      isMaxAmount,
      isSpaceInputted,
      isSymbolAllowed
    }
  }

  _forceInputFocus = () => {
    this._ref && this._ref.current && this._ref.current.focus()
  }

  _handleButtonClick = () => {
    const { onChange, userMoney } = this.props

    if (!userMoney || !onChange) {
      return
    }

    this.setState({
      inputValue: String(userMoney)
    })

    onChange(String(userMoney))

    this._forceInputFocus()
  }

  _handleInput = (event: React.KeyboardEvent<HTMLInputElement> & React.ChangeEvent<HTMLInputElement>) => {
    const { onChange, userMoney } = this.props
    const { value: targetValue } = event.target as HTMLInputElement

    const { value: normalizedMoneyValue, isSpaceInputted, isSymbolAllowed, isMaxAmount } = this._checkString({
      targetValue
    })

    if (isSpaceInputted || !isSymbolAllowed) {
      return
    }

    const value = isMaxAmount ? String(userMoney) : String(normalizedMoneyValue)

    this.setState({
      inputValue: value
    })

    onChange(value)
  }

  _handleInputBlur = event => {
    const { onBlur } = this.props

    this.setState({
      isSpaceInputted: false,
      isSymbolAllowed: true,
      isMaxAmount: false
    })

    if (onBlur) {
      onBlur(event)
    }
  }

  _handleInputClick = (e: any) => {
    const { inputValue } = this.state
    const { value, onClick } = this.props

    if (!Number(value) && !Number(inputValue)) {
      return
    }

    onClick(e)
  }

  _handleInputCancel = () => {
    const { onChange } = this.props

    this.setState({
      inputValue: ''
    })

    onChange('')
  }

  _handleButtonKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleButtonClick
        }
        // {
        //   type: 'space',
        //   onEvent: this._handleButtonClick
        // }
      ]
    })
  }

  _handleInputKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    keyDownHandler({
      event,
      keysConfig: [
        {
          type: 'enter',
          onEvent: this._handleInputClick
        },
        {
          type: 'esc',
          onEvent: this._handleInputCancel
        }
      ]
    })
  }

  _classNamesHolder = () => {
    const { isSpaceInputted, isSymbolAllowed } = this.state
    const { customStyles, disableSignButton } = this.props

    const inputWrapClass = classnames({
      'input-money-group': true,
      [styles.inputBlock]: true,
      [styles.inputError]: isSpaceInputted || !isSymbolAllowed,
      [customStyles?.inputBlock]: customStyles?.inputBlock,
      [customStyles?.inputError]: customStyles?.inputError
    })

    const inputClass = classnames({
      'input-money': true,
      [styles.input]: true,
      [styles.rounded]: disableSignButton,
      [customStyles?.input]: customStyles?.input,
      [styles.noSignButtonInput]: disableSignButton
    })

    const buttonClass = classnames({
      [styles.signButton]: true,
      'input-money-symbol': true,
      [customStyles?.signButton]: customStyles?.signButton
    })

    const tooltipClass = classnames({
      [styles.warningTooltip]: true,
      [customStyles?.tooltipWarning]: customStyles?.tooltipWarning
    })

    return {
      inputWrapClass,
      inputClass,
      buttonClass,
      tooltipClass
    }
  }

  _renderTooltip = () => {
    const { isSpaceInputted, isSymbolAllowed } = this.state
    const { disableWarningTooltip } = this.props
    const { className, timeExit, timeIn } = ANIM_PROPS

    if (disableWarningTooltip) {
      return null
    }

    const tooltipText =
      (isSpaceInputted && 'Space not allowed') || (!isSymbolAllowed && 'Only valid numbers or shortcuts allowed')
    // (isMaxAmount && `You can\'t input more than you have: ${userMoney}`)

    if (!isSpaceInputted && isSymbolAllowed) {
      return null
    }

    const { tooltipClass } = this._classNamesHolder()

    return (
      <TransitionGroup>
        <CSSTransition
          appear={true}
          key={(isSpaceInputted && 1) || (!isSymbolAllowed && 2)}
          classNames={className}
          timeout={{ enter: timeIn, exit: timeExit }}
          unmountOnExit={true}
        >
          <div className={tooltipClass}>{tooltipText}</div>
        </CSSTransition>
      </TransitionGroup>
    )
  }

  _renderSignButton = () => {
    const { userMoney, readOnly, disableSignButton } = this.props

    if (disableSignButton) {
      return null
    }

    const { buttonClass } = this._classNamesHolder()

    return (
      <button
        id={INPUT_ID}
        type='button'
        aria-label={`Set maximum bet amount equal to: ${userMoney}`}
        className={buttonClass}
        disabled={readOnly}
        onClick={this._handleButtonClick}
        onKeyDown={this._handleButtonKeydown}
      >
        {SIGN}
      </button>
    )
  }

  _renderInput = () => {
    const { inputValue } = this.state
    const { readOnly, disableReadOnly, readOnlyText, userMoney, value } = this.props

    const { inputClass } = this._classNamesHolder()
    const moneyToRender = value ?? inputValue
    const isReadOnly = readOnly || ((!userMoney || userMoney === 0) && !disableReadOnly)

    return (
      <input
        ref={this._ref}
        type='text'
        aria-label={`Type bet amount: ${moneyToRender} or ${TOOLTIP_POS}`}
        className={inputClass}
        readOnly={isReadOnly}
        placeholder={isReadOnly ? readOnlyText || 'no money...' : ''}
        value={!moneyToRender.includes('.') ? toMoney(moneyToRender) : moneyToRender}
        onChange={this._handleInput}
        onBlur={this._handleInputBlur}
        onKeyDown={this._handleInputKeydown}
        data-lpignore='true'
      />
    )
  }

  render() {
    const { inputWrapClass } = this._classNamesHolder()

    return (
      <div className={inputWrapClass}>
        {this._renderTooltip()}
        {this._renderSignButton()}
        {this._renderInput()}
      </div>
    )
  }
}

export default MoneyInput
