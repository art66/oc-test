import React from 'react'
import { number, boolean, text } from '@storybook/addon-knobs'

import MoneyInput from './index'

const mockFn = () => {}

export const Defaults = () => {
  return (
    <MoneyInput
      onClick={mockFn}
      onChange={mockFn}
      userMoney={number('Set User Money:', 1232456)}
      readOnly={boolean('Set ReadOnly mode: ', false)}
      disableButtonTooltip={boolean('Disable Button Tooltip: ', false)}
      disableWarningTooltip={boolean('Disable Warn Tooltip: ', false)}
      disableSignButton={boolean('Disable Sign Button: ', false)}
      tooltipText={text('Set Custom Tooltip Text: ', '')}
    />
  )
}

Defaults.story = {
  name: 'defaults'
}

export const ReadOnly = () => {
  return <MoneyInput onClick={mockFn} onChange={mockFn} userMoney={1232456} readOnly={true} />
}

ReadOnly.story = {
  name: 'read only'
}

export const DisabledButton = () => {
  return <MoneyInput onClick={mockFn} onChange={mockFn} userMoney={1232456} disableSignButton={true} />
}

DisabledButton.story = {
  name: 'disabled button'
}

export const NoUserMoney = () => {
  return <MoneyInput onClick={mockFn} onChange={mockFn} userMoney={null} />
}

NoUserMoney.story = {
  name: 'no user money'
}

export default {
  title: 'Shared/MoneyInput'
}
