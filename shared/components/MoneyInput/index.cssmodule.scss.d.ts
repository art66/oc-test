// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'button': string;
  'disabledButton': string;
  'globalSvgShadow': string;
  'input': string;
  'inputBlock': string;
  'inputError': string;
  'noSignButtonInput': string;
  'rounded': string;
  'signButton': string;
  'signText': string;
  'warningTooltip': string;
}
export const cssExports: CssExports;
export default cssExports;
