export const INPUT_ID = 'moneyInput'
export const SIGN = '$'

export const TOOLTIP_PRE = 'Click here to add the maximum amount, or'
export const TOOLTIP_POS = 'use shortcuts like 5k, 1.5m, max, half, quarter, 1/2, 1/3, 1/4, 25%'

export const ANIM_PROPS = {
  className: 'inputTooltip',
  timeIn: 1000,
  timeExit: 300
}

// input sanitization rules
const number = '(^[1-9]+([0-9]+)?$)'
const floatNumber = '(^[1-9]+\\.?([0-9]+)?$)'
const half = '^h$|^(ha)$|^(hal)$|^(half)$'
const quarter = '^q$|^(qu)$|^(qua)$|^(quar)$|^(quart)$|^(quarte)$|^(quarter)$'
const max = '^m$|^(ma)$|^(max)$'
const fractions = '^(1/?(2|3|4|5|6|7|8|9)?)$'
const percents = '^([1-9][0-9]?%?)$|^(25%)$|^(50%)$|^(100%)$'

export const INPUT_CHECKER_CONFIG = {
  maxLength: 1000,
  allowedSymbols: `${number}|${floatNumber}|${half}|${max}|${quarter}|${fractions}|${percents}`,
  allowedFlags: 'i',
  allowDecimal: true,
  removeSpaces: true
}
