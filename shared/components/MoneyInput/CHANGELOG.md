# MoneyInput - Torn

## 1.5.9
 * Fixed money zero input with percentages
## 1.5.8
 * Improved money shortcuts logic.

## 1.5.7
 * Fixed the sign button align.

## 1.5.6
 * Added ability for float number input.

## 1.5.5
 * Added more control around readOnly state.

## 1.5.4
 * Fixed max amount value.

## 1.5.3
 * Improved max value input according to the global module.

## 1.5.2
 * Fixed input width while sign button is disabled.

## 1.5.1
 * Fixed tooltips position on touchscreen devices.

## 1.5.0
 * Added aria labels.

## 1.4.1
 * Improved focusing and keydown events.

## 1.4.0
 * Added focusing and keydown events.

## 1.3.0
 * Added ability to control input value from outside.

## 1.2.0
 * Restricted input more than user actually have on hand.

## 1.1.0
 * Improved Tooltip anim.
 * Added Readme.

## 1.0.0
 * First stable release.
