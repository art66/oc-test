export interface IProps {
  value?: string
  disableButtonTooltip?: boolean
  disableWarningTooltip?: boolean
  disableSignButton?: boolean
  disableOnMountInputFocus?: boolean
  disableReadOnly?: boolean
  tooltipText?: string
  readOnly?: boolean
  readOnlyText?: string
  userMoney: number
  onChange: (value: string) => void
  onClick?: (e: any) => void
  onBlur?: (e: any) => void
  customStyles?: {
    inputBlock?: string
    inputError?: string
    input?: string
    signButton?: string
    tooltipWarning?: string
  }
}

export interface IState {
  inputValue: string
  isMaxAmount: boolean
  isSpaceInputted: boolean
  isSymbolAllowed: boolean
}
