/* eslint-disable max-statements */
import React from 'react'
import { shallow } from 'enzyme'

import initialState, { maxLimit, outerValue, floatValue } from './mocks'

import MoneyInput from '../index'

describe('<MoneyInput />', () => {
  it('should render MoneyInput component and input basic values', () => {
    const Component = shallow(<MoneyInput {...initialState} />)

    expect(Component.find('.inputBlock').length).toBe(1)
    expect(Component.find('.signButton').length).toBe(1)
    expect(Component.find('.input').length).toBe(1)

    Component.find('.input').simulate('change', { target: { value: '1' } })
    expect(Component.find('.input').prop('value')).toBe('1')

    Component.find('.input').simulate('change', { target: { value: '10' } })
    expect(Component.find('.input').prop('value')).toBe('10')

    Component.find('.input').simulate('change', { target: { value: '100' } })
    expect(Component.find('.input').prop('value')).toBe('100')

    Component.find('.input').simulate('change', { target: { value: '1000' } })
    expect(Component.find('.input').prop('value')).toBe('1,000')

    Component.find('.input').simulate('change', { target: { value: '10000' } })
    expect(Component.find('.input').prop('value')).toBe('10,000')

    Component.find('.input').simulate('change', { target: { value: '100000' } })
    expect(Component.find('.input').prop('value')).toBe('100,000')

    Component.find('.input').simulate('change', { target: { value: '1000000' } })
    expect(Component.find('.input').prop('value')).toBe('1,000,000')

    Component.find('.input').simulate('change', { target: { value: '100000' } })
    expect(Component.find('.input').prop('value')).toBe('100,000')

    Component.find('.input').simulate('change', { target: { value: '10000' } })
    expect(Component.find('.input').prop('value')).toBe('10,000')

    Component.find('.input').simulate('change', { target: { value: '1000' } })
    expect(Component.find('.input').prop('value')).toBe('1,000')

    Component.find('.input').simulate('change', { target: { value: '100' } })
    expect(Component.find('.input').prop('value')).toBe('100')

    Component.find('.input').simulate('change', { target: { value: '10' } })
    expect(Component.find('.input').prop('value')).toBe('10')

    Component.find('.input').simulate('change', { target: { value: '1' } })
    expect(Component.find('.input').prop('value')).toBe('1')

    Component.find('.input').simulate('change', { target: { value: '' } })
    expect(Component.find('.input').prop('value')).toBe('')

    expect(Component).toMatchSnapshot()
  })
  it('should render MoneyInput component and input fraction values', () => {
    const Component = shallow(<MoneyInput {...initialState} />)

    expect(Component.find('.inputBlock').length).toBe(1)
    expect(Component.find('.signButton').length).toBe(1)
    expect(Component.find('.input').length).toBe(1)

    Component.find('.input').simulate('change', { target: { value: '1/2' } })
    expect(Component.find('.input').prop('value')).toBe('500,000')

    Component.find('.input').simulate('change', { target: { value: '1/3' } })
    expect(Component.find('.input').prop('value')).toBe('333,333')

    Component.find('.input').simulate('change', { target: { value: '1/4' } })
    expect(Component.find('.input').prop('value')).toBe('250,000')

    Component.find('.input').simulate('change', { target: { value: '1/5' } })
    expect(Component.find('.input').prop('value')).toBe('200,000')

    Component.find('.input').simulate('change', { target: { value: '1/6' } })
    expect(Component.find('.input').prop('value')).toBe('166,667')

    Component.find('.input').simulate('change', { target: { value: '1/7' } })
    expect(Component.find('.input').prop('value')).toBe('142,857')

    Component.find('.input').simulate('change', { target: { value: '1/8' } })
    expect(Component.find('.input').prop('value')).toBe('125,000')

    Component.find('.input').simulate('change', { target: { value: '1/9' } })
    expect(Component.find('.input').prop('value')).toBe('111,111')

    expect(Component).toMatchSnapshot()
  })
  it('should render MoneyInput component and input words values', () => {
    const Component = shallow(<MoneyInput {...initialState} />)

    expect(Component.find('.inputBlock').length).toBe(1)
    expect(Component.find('.signButton').length).toBe(1)
    expect(Component.find('.input').length).toBe(1)

    Component.find('.input').simulate('change', { target: { value: 'quarter' } })
    expect(Component.find('.input').prop('value')).toBe('250,000')

    Component.find('.input').simulate('change', { target: { value: 'half' } })
    expect(Component.find('.input').prop('value')).toBe('500,000')

    Component.find('.input').simulate('change', { target: { value: 'max' } })
    expect(Component.find('.input').prop('value')).toBe('1,000,000')

    // reverse testing
    Component.find('.input').simulate('change', { target: { value: '' } })
    expect(Component.find('.input').prop('value')).toBe('')

    // reverse testing
    Component.find('.input').simulate('change', { target: { value: 'quar' } })
    Component.find('.input').simulate('change', { target: { value: 'quars' } })
    expect(Component.find('.input').prop('value')).toBe('quar')

    // reverse testing
    Component.find('.input').simulate('change', { target: { value: '' } })
    expect(Component.find('.input').prop('value')).toBe('')

    // reverse testing
    Component.find('.input').simulate('change', { target: { value: 'ma' } })
    Component.find('.input').simulate('change', { target: { value: 'mar' } })
    expect(Component.find('.input').prop('value')).toBe('ma')

    // reverse testing
    Component.find('.input').simulate('change', { target: { value: '' } })
    expect(Component.find('.input').prop('value')).toBe('')

    // reverse testing
    Component.find('.input').simulate('change', { target: { value: 'h' } })
    Component.find('.input').simulate('change', { target: { value: 'hs' } })
    expect(Component.find('.input').prop('value')).toBe('h')

    expect(Component).toMatchSnapshot()
  })
  it('should render MoneyInput component and input percentage values', () => {
    const Component = shallow(<MoneyInput {...initialState} />)

    expect(Component.find('.inputBlock').length).toBe(1)
    expect(Component.find('.signButton').length).toBe(1)
    expect(Component.find('.input').length).toBe(1)

    Component.find('.input').simulate('change', { target: { value: '5%' } })
    expect(Component.find('.input').prop('value')).toBe('50,000')

    Component.find('.input').simulate('change', { target: { value: '17%' } })
    expect(Component.find('.input').prop('value')).toBe('170,000')

    Component.find('.input').simulate('change', { target: { value: '25%' } })
    expect(Component.find('.input').prop('value')).toBe('250,000')

    Component.find('.input').simulate('change', { target: { value: '50%' } })
    expect(Component.find('.input').prop('value')).toBe('500,000')

    Component.find('.input').simulate('change', { target: { value: '73%' } })
    expect(Component.find('.input').prop('value')).toBe('730,000')

    Component.find('.input').simulate('change', { target: { value: '100%' } })
    expect(Component.find('.input').prop('value')).toBe('1,000,000')

    expect(Component).toMatchSnapshot()
  })
  it('should render MoneyInput component and only max value', () => {
    const Component = shallow(<MoneyInput {...maxLimit} />)

    expect(Component.find('.inputBlock').length).toBe(1)
    expect(Component.find('.signButton').length).toBe(1)
    expect(Component.find('.input').length).toBe(1)

    Component.find('.input').simulate('change', { target: { value: '100' } })
    expect(Component.find('.input').prop('value')).toBe('100')

    Component.find('.input').simulate('change', { target: { value: '1000' } })
    expect(Component.find('.input').prop('value')).toBe('1,000')

    // reverse testing
    Component.find('.input').simulate('change', { target: { value: '10000' } })
    expect(Component.find('.input').prop('value')).toBe('1,000')

    expect(Component).toMatchSnapshot()
  })
  it('should render MoneyInput component and outer value', () => {
    const Component = shallow(<MoneyInput {...outerValue} />)

    expect(Component.find('.inputBlock').length).toBe(1)
    expect(Component.find('.signButton').length).toBe(1)
    expect(Component.find('.input').length).toBe(1)

    expect(Component.find('.input').prop('value')).toBe('100,000')

    expect(Component).toMatchSnapshot()
  })
  it('should render with float value', () => {
    const Component = shallow(<MoneyInput {...floatValue} />)

    expect(Component.find('.inputBlock').length).toBe(1)
    expect(Component.find('.signButton').length).toBe(1)
    expect(Component.find('.input').length).toBe(1)

    Component.find('.input').simulate('change', { target: { value: '12.2' } })
    expect(Component.find('.input').prop('value')).toBe('12.2')

    Component.find('.input').simulate('change', { target: { value: '12.2k' } })
    expect(Component.find('.input').prop('value')).toBe('12,200')

    expect(Component).toMatchSnapshot()
  })
})
