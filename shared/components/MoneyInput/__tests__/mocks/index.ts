import { IProps } from '../../interfaces'

const initialState: IProps = {
  onChange: () => {},
  userMoney: 1000000
}

const maxLimit: IProps = {
  ...initialState,
  userMoney: 1000
}

const outerValue: IProps = {
  ...initialState,
  value: '100000'
}

const floatValue: IProps = {
  ...initialState
}

export { maxLimit, outerValue, floatValue }
export default initialState
