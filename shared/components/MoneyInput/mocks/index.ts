export const INFO_MESSAGE = 'All changes are saved!'

export const MESSAGE_HTML = '<button>Click on me</button>'

export const COLOR_OPRIONS = ['gray', 'green', 'red']

export const LOG = [
  'You put Glock 17 in 1st position on your bazaar.',
  'You put Glock 17 in 2nd position on your bazaar.',
  'You put Glock 17 in 3rd position on your bazaar.',
  'You put Glock 17 in 4th position on your bazaar.',
  'You put Glock 17 in 5th position on your bazaar.',
  'You put Glock 17 in 6th position on your bazaar.',
  'You put Glock 17 in 7th position on your bazaar.',
  'You put Glock 17 in 8th position on your bazaar.',
  'You put Glock 17 in 9th position on your bazaar.',
  'You put Glock 17 in 10th position on your bazaar.',
  'You put Glock 17 in 11th position on your bazaar.',
  'You put Glock 17 in 12th position on your bazaar.',
  'You put Glock 17 in 13th position on your bazaar.',
  'You put Glock 17 in 14th position on your bazaar.',
  'You put Glock 17 in 15th position on your bazaar.',
  'You put Glock 17 in 16th position on your bazaar.',
  'You put Glock 17 in 17th position on your bazaar.',
  'You put Glock 17 in 18th position on your bazaar.',
  'You put Empty Blood Bag in 19th position on your bazaar.'
]

export const DEFAULT_CONFIG = {
  msg: INFO_MESSAGE,
  color: COLOR_OPRIONS[0]
}

export const LOG_CONFIG = {
  msg: INFO_MESSAGE,
  color: COLOR_OPRIONS[0],
  log: LOG
}

export const HTML_CONFIG = {
  msg: MESSAGE_HTML,
  color: COLOR_OPRIONS[0],
  showDangerously: true
}

export const HTML_AND_LOG_CONFIG = {
  msg: MESSAGE_HTML,
  color: COLOR_OPRIONS[0],
  log: LOG,
  showDangerously: true
}

export const LOADING_CONFIG = {
  loading: true
}
