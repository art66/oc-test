# MoneyInput - Global Component

### Basic configuration:
      ```
        <MoneyInput
          userMoney={12323}
          onChange={callback}
        />
      ```
  - Advanced:
    For advanced usage you can dive into the whole API described inside mocks > storybook.ts state.
