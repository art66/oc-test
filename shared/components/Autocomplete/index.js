import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ScrollArea from 'react-scrollbar'
import AutocompleteItem from './AutocompleteItem'
import './Autocomplete.scss'

const KEY_CODE_TAB = 9
const KEY_CODE_ENTER = 13
const KEY_CODE_ESCAPE = 27
const KEY_CODE_ARROW_UP = 38
const KEY_CODE_ARROW_DOWN = 40

class Autocomplete extends Component {
  static defaultProps = {
    hideDropdown: false,
    noItemsFoundMessage: 'No person could be found',
    onInputClick: () => {},
    onRef: () => {}
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      dropdownIsActive: false,
      dropdownIsVisible: false,
      selected: this.props.selected,
      value: this.props.value
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.hideDropdownIfInactive, false)
    window.addEventListener('keydown', this._handleKeyPress, false)
    this.props.onRef(this)
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.hideDropdownIfInactive, false)
    window.removeEventListener('keydown', this._handleKeyPress, false)
    this.props.onRef(null)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { value } = prevState

    if (nextProps.value !== value) {
      return {
        value: nextProps.value
      }
    }

    return null
  }

  // deprecated since React v.16.5!
  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.value !== this.props.value) {
  //     this.setState({
  //       value: nextProps.value
  //     })
  //   }
  // }

  componentDidUpdate() {
    if (this.props.value !== this.state.value) {
      this.setState({ value: this.props.value })
    }
  }

  _handleKeyPress = e => {
    if (document.activeElement !== this.input) {
      return
    }

    if (e.keyCode === KEY_CODE_ARROW_UP) {
      e.preventDefault()
      this._selectPrev()
    } else if (e.keyCode === KEY_CODE_ARROW_DOWN) {
      e.preventDefault()
      this._selectNext()
    } else if (e.keyCode === KEY_CODE_ENTER && this.state.selected) {
      this._handleSelect(this.state.selected)
    } else if (e.keyCode === KEY_CODE_ESCAPE) {
      this.hideDropdown()
    } else if (e.keyCode === KEY_CODE_TAB) {
      this.closeDropdown()
    }
  }

  hideDropdown = () => {
    this.setState({ dropdownIsVisible: false })
  }

  hideDropdownIfInactive = () => {
    const { dropdownIsActive } = this.state

    if (!dropdownIsActive) {
      this.setState({ dropdownIsVisible: false })
    }
  }

  closeDropdown = () => {
    this.setState({ dropdownIsVisible: false, dropdownIsActive: false })
  }

  openDropdown = () => {
    this.setState({ dropdownIsActive: true, dropdownIsVisible: true })
  }

  _showDropdown = () => {
    const { dropdownIsActive } = this.state

    if (dropdownIsActive) {
      this.setState({ dropdownIsVisible: true })
    }
  }

  _handleInputClick = () => {
    const { dropdownIsActive } = this.state
    const { onInputClick } = this.props

    onInputClick()

    if (dropdownIsActive) {
      this.setState({ dropdownIsVisible: true })
    }
  }

  _handleInputFocus = () => {
    this.openDropdown()
    this.props.onFocus && this.props.onFocus()
  }

  _handleFocus = () => {
    this.openDropdown()
  }

  _handleBlur = () => {
    const { onBlur } = this.props

    if (onBlur) {
      onBlur()
    }

    this.setState({ dropdownIsActive: false })
  }

  _handleInputChange = e => {
    this.setState({ value: e.target.value })
    this._showDropdown()
    this.props.onInput && this.props.onInput(e)
  }

  _handleSelect = selected => {
    this.setState({
      value: selected.name,
      dropdownIsActive: false,
      selected
    })
    this.hideDropdownIfInactive()
    this.props.onSelect && this.props.onSelect(selected)
    this.closeDropdown()
  }

  _selectNext = () => {
    const selectedIndex = this._getSelectedIndex()
    const selected = this.props.list[selectedIndex + 1] || this.state.selected

    this.setState({ selected })
  }

  _selectPrev = () => {
    const selectedIndex = this._getSelectedIndex()
    const selected = this.props.list[selectedIndex - 1] || this.state.selected

    this.setState({ selected })
  }

  _setSelected = item => {
    this.setState({
      selected: item
    })
  }

  _getSelectedIndex = () => {
    return this.props.list.findIndex(item => item === this.state.selected) || 0
  }

  _renderListItems = list => {
    if (!list.length) {
      return <div className='autocomplete-no-items'>{this.props.noItemsFoundMessage}</div>
    }

    return list.map((item, i) => (
      <AutocompleteItem
        key={i}
        item={item}
        selected={this.state.selected}
        targets={this.props.targets}
        onClick={this._handleSelect}
        onMouseEnter={this._setSelected}
      />
    ))
  }

  _renderAutocomplete = () => {
    const { dropdownIsVisible, value } = this.state
    const { list, name, inputClassName, placeholder, autoFocus, children, hideDropdown, ariaLabel } = this.props
    const show = !hideDropdown && dropdownIsVisible && list

    return (
      <div className={`autocomplete-wrapper ${show ? 'active' : ''}`}>
        <input
          type='text'
          name={name}
          value={value || ''}
          className={inputClassName || ''}
          placeholder={placeholder || ''}
          autoFocus={autoFocus || false}
          ref={input => {
            this.input = input
          }}
          onChange={this._handleInputChange}
          onClick={this._handleInputClick}
          onFocus={this._handleInputFocus}
          onBlur={this._handleBlur}
          autoComplete='off'
          aria-label={ariaLabel}
        />
        {show && (
          <>
            {children}
            <div
              className='scrollarea scroll-area scrollbar-bright'
              tabIndex='1'
              onFocus={this._handleFocus}
              onBlur={this._handleBlur}
            >
              <div className='scrollarea-content content dropdown-content'>{this._renderListItems(list)}</div>
            </div>
          </>
        )}
      </div>
    )
  }

  render() {
    return <div className={`torn-react-autocomplete ${this.props.className || ''}`}>{this._renderAutocomplete()}</div>
  }
}

Autocomplete.propTypes = {
  id: PropTypes.string,
  tabIndex: PropTypes.string,
  list: PropTypes.array.isRequired,
  hideDropdown: PropTypes.bool,
  name: PropTypes.string,
  inputClassName: PropTypes.string,
  autoFocus: PropTypes.bool,
  selected: PropTypes.object,
  onChange: PropTypes.func,
  onInit: PropTypes.func,
  onInput: PropTypes.func,
  onSelect: PropTypes.func,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  targets: PropTypes.string,
  children: PropTypes.any,
  noItemsFoundMessage: PropTypes.string,
  onRef: PropTypes.func,
  onInputClick: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  ariaLabel: PropTypes.string
}

export default Autocomplete
