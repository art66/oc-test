import React from 'react'
import { State, Store } from '@sambego/storybook-state'

import SearchBar from './index'
import { DEFAULT_STATE } from './mocks'

export const Defaults = () => {
  const config = {
    ...DEFAULT_STATE
  }

  const STORE_CONFIG = {
    value: 'test',
    onInput: (event) => {
      console.log(`onInput: ${event.target.value}`)

      store.set({ ...STORE_CONFIG, value: event.target.value })
    },
    onSelect: (item) => {
      console.log('onSelect:', item)

      store.set({ ...STORE_CONFIG, value: item.name })
    }
  }

  const store = new Store(STORE_CONFIG)

  return (
    <State store={store}>
      <SearchBar {...config} />
    </State>
  )
}

Defaults.story = {
  name: 'defaults'
}

export const DropdownHidden = () => {
  const config = {
    ...DEFAULT_STATE,
    hideDropdown: true
  }

  const STORE_CONFIG = {
    value: 'test',
    onInput: (event) => {
      console.log(`onInput: ${event.target.value}`)

      store.set({ ...STORE_CONFIG, value: event.target.value })
    },
    onSelect: (item) => {
      console.log('onSelect:', item)

      store.set({ ...STORE_CONFIG, value: item.name })
    }
  }

  const store = new Store(STORE_CONFIG)

  return (
    <State store={store}>
      <SearchBar {...config} />
    </State>
  )
}

DropdownHidden.story = {
  name: 'dropdown hidden'
}

export default {
  title: 'Shared/Autocomplete'
}
