import PropTypes from 'prop-types'
import React from 'react'

export const AutocompleteItem = props => (
  <div
    className={'item ' + (JSON.stringify(props.item) === JSON.stringify(props.selected) ? 'selected' : '')}
    onClick={() => props.onClick(props.item)}
    onMouseEnter={() => props.onMouseEnter(props.item)}
  >
    {props.targets === 'users' ? `${props.item.name} [${props.item.id}]` : props.item.name}
  </div>
)

AutocompleteItem.propTypes = {
  item: PropTypes.object.isRequired,
  selected: PropTypes.object,
  targets: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  onMouseEnter: PropTypes.func
}

export default AutocompleteItem
