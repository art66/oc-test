export type TProps = {
  className?: string
  iconClassName?: string
  value?: string | number
}
