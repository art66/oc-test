import React from 'react'
import DamageBonus from '..'
import { initialState, withotValueState, withNumberValueState, withClassNamesState } from './mocks'
import { shallow } from 'enzyme'

describe('Shared <DamageBonus />', () => {
  const testSuitHandler = (
    Component,
    { value = '45.67', className = 'damageBonus', iconClassName = 'damageBonusIcon' } = {}
  ) => {
    expect(Component.is('span')).toBeTruthy()
    expect(Component.prop('className')).toBe(className)
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const IconComponent = ComponentChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe(iconClassName)
    const TextComponent = ComponentChildren.at(1)
    expect(TextComponent.text()).toBe(value)
  }

  it('should render', () => {
    const Component = shallow(<DamageBonus {...initialState} />)

    testSuitHandler(Component)

    expect(Component).toMatchSnapshot()
  })

  it('should render without value', () => {
    const Component = shallow(<DamageBonus {...withotValueState} />)

    testSuitHandler(Component, { value: 'N/A' })

    expect(Component).toMatchSnapshot()
  })

  it('should render with number value', () => {
    const Component = shallow(<DamageBonus {...withNumberValueState} />)

    testSuitHandler(Component, { value: '67.09' })

    expect(Component).toMatchSnapshot()
  })

  it('should render with class names', () => {
    const Component = shallow(<DamageBonus {...withClassNamesState} />)

    testSuitHandler(Component, {
      className: 'damageBonus testClassName',
      iconClassName: 'damageBonusIcon testIconClassName'
    })

    expect(Component).toMatchSnapshot()
  })
})
