import { TProps } from '../../types'

export const initialState: TProps = {
  value: '45.67'
}

export const withotValueState: TProps = {}

export const withNumberValueState: TProps = {
  value: 67.09
}

export const withClassNamesState: TProps = {
  ...initialState,
  className: 'testClassName',
  iconClassName: 'testIconClassName'
}
