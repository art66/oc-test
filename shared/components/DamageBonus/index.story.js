import React from 'react'
import { text, number } from '@storybook/addon-knobs'

import DamageBonus from './index'
import { DEFAULT_STATE } from './mocks'

export const Defaults = () => {
  const config = {
    ...DEFAULT_STATE,
    value: text('Value', '41.90')
  }

  return <DamageBonus {...config} />
}

Defaults.story = {
  name: 'defaults'
}

export const ValueAsNumber = () => {
  const config = {
    ...DEFAULT_STATE,
    value: number('Value', 41.9)
  }

  return <DamageBonus {...config} />
}

ValueAsNumber.story = {
  name: 'value as number'
}

export const WithoutValue = () => {
  const config = {
    ...DEFAULT_STATE
  }

  return <DamageBonus {...config} />
}

WithoutValue.story = {
  name: 'without value'
}

export default {
  title: 'Shared/Item/DamageBonus'
}
