export interface IProps {
  dotsCount: number
  dotsColor?: string
  isAdaptive?: boolean
  animationDuaration?: string
}
