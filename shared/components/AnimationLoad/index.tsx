import React, { PureComponent } from 'react'
import classnames from 'classnames'

import { IProps } from './types'
import styles from './index.cssmodule.scss'

const BTN_CLASS = 'loaderPoint_'
const MAX_DOTS_COUNT = 7

class AnimationLoad extends PureComponent<IProps> {
  static defaultProps: IProps = {
    dotsCount: MAX_DOTS_COUNT,
    dotsColor: 'black',
    animationDuaration: null
  }

  _classNamesHolder = (pointClassName?: string) => {
    const { isAdaptive, dotsCount, dotsColor } = this.props

    const loaderWrapClass = classnames({
      [styles.loader]: true,
      [styles[`loader_${dotsCount}`]]: true,
      [styles.adoptiveLoader]: isAdaptive
    })
    const loaderDotsClass = classnames({
      [styles.loaderPoint]: true,
      [styles[pointClassName]]: true,
      [styles[dotsColor]]: true
    })

    return {
      loaderWrapClass,
      loaderDotsClass
    }
  }

  _renderAnimationList = () => {
    const { dotsCount, dotsColor, animationDuaration } = this.props

    const pointsList = Array.from(Array(dotsCount).keys())

    return pointsList.map(i => {
      const { loaderDotsClass } = this._classNamesHolder(BTN_CLASS + i)

      if (i >= MAX_DOTS_COUNT) {
        return null
      }

      return (
        <div
          key={i}
          className={loaderDotsClass}
          style={{ animationName: styles[dotsColor], animationDuration: animationDuaration }}
        />
      )
    })
  }

  render() {
    const { loaderWrapClass } = this._classNamesHolder()

    return <div className={loaderWrapClass}>{this._renderAnimationList()}</div>
  }
}

export default AnimationLoad
