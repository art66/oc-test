# AnimationLoad - Torn

## 0.5.3
 * Fixed height in adoptive mode.

## 0.5.2
 * Added red layout dots.

## 0.5.1
 * Fixed layout in FF browser.

## 0.5.0
 * Added width layout by dots count provided.

## 0.4.3
 * Improved adaptive mode layout.

## 0.4.2
 * Temporary disabled translateY css prop in desktop mode.

## 0.4.1
 * Added isAdaptive prop to allow ability set custom dots container width by they parent one.

## 0.4.0
 * Added a couple of new colors: blue and green.

## 0.4.0
 * Added new dots color - orange.

## 0.3.0
 * Added feature to set color dots (black/white).
 * Updated tests for Component.

## 0.2.0
 * Written tests for Component.

## 0.1.0
 * Added set animDuaration feature.

## 0.0.1
 * AnimationLoad Component was created.
