import React from 'react'
import { number, text } from '@storybook/addon-knobs'

import { DOTS_COUNT } from './mocks'
import AnimationLoad from './index'

export const Default = () => {
  const config = {
    dotsCount: number('Dots Count:', DOTS_COUNT),
    dotsColor: text('Color Scheme:', ''),
    animationDuaration: text('Animation Duaration:', '')
  }

  return <AnimationLoad {...config} />
}

Default.story = {
  name: 'default'
}

export default {
  title: 'Shared/AnimationLoad'
}
