
# Loading Buttons Animation
### This Component helps to solve the problem with beatiful animation for waiting time on fetch responce.

 * Schema:
  `dotsCount` - can be setted by dev. Count of the dots to render (in range from 1 to 7).
  'bcgColor' - color for dots. Can be black or white: by 'black' or 'white' label provided.
  `isAdoptive` - allows to set custom width of the dots' container by parent one.
  `animationDuaration` - custom speed for dots animation transition.
