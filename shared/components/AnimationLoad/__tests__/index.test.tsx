import React from 'react'
import { mount } from 'enzyme'
import { noneDots, fourDots, eightDots, withAnimSpeed, whiteColor } from './mocks/index'
import AnimationLoad from '../index'

describe('<AnimationLoad />', () => {
  it('should render AnimationLoad component with 7 dots in case of none counts provided', () => {
    const Component = mount(<AnimationLoad {...noneDots} />)

    expect(Component).toMatchSnapshot()
    expect(Component.find('.loader').length).toBe(1)
    expect(Component.find('.loaderPoint').length).toBe(7)
    expect(Component).toMatchSnapshot()
  })
  it('should render AnimationLoad component with 4 dots', () => {
    const Component = mount(<AnimationLoad {...fourDots} />)

    expect(Component.find('.loader').length).toBe(1)
    expect(Component.find('.loaderPoint').length).toBe(4)
    expect(Component).toMatchSnapshot()
  })
  it('should render AnimationLoad component with 7 dots only if more that its processed count provided', () => {
    const Component = mount(<AnimationLoad {...eightDots} />)

    expect(Component.find('.loader').length).toBe(1)
    expect(Component.find('.loaderPoint').length).toBe(7)
    expect(Component).toMatchSnapshot()
  })
  it('should render AnimationLoad component with 7 dots and setted animation speed', () => {
    const Component = mount(<AnimationLoad {...withAnimSpeed} />)

    expect(Component.find('.loader').length).toBe(1)
    expect(Component.find('.loaderPoint').length).toBe(7)
    expect(
      Component.find('.loaderPoint')
        .first()
        .prop('style').animationDuration
    ).toBe('0.25s')
    expect(Component).toMatchSnapshot()
  })
  it('should render AnimationLoad component with 7 dots and white color dots', () => {
    const Component = mount(<AnimationLoad {...whiteColor} />)

    expect(Component.find('.loader').length).toBe(1)
    expect(Component.find('.loaderPoint').length).toBe(7)
    expect(
      Component.find('.loaderPoint')
        .first()
        .prop('style').animationName
    ).toBe('white')
    expect(Component).toMatchSnapshot()
  })
  it('should render AnimationLoad with custom width', () => {
    const Component = mount(<AnimationLoad {...fourDots} isAdaptive={true} />)

    expect(Component.find('.loader').length).toBe(1)
    expect(Component.find('.adoptiveLoader').length).toBe(1)
    expect(Component.find('.loaderPoint').length).toBe(4)
    expect(Component).toMatchSnapshot()
  })
})
