const initialState = {
  dotsCount: 7,
  animationDuaration: null
}

export const withAnimSpeed = {
  ...initialState,
  animationDuaration: '0.25s'
}

export const noneDots = {}

export const fourDots = {
  ...initialState,
  dotsCount: 4
}

export const eightDots = {
  ...initialState,
  dotsCount: 8
}

export const whiteColor = {
  ...initialState,
  dotsColor: 'white'
}

export default initialState
