# InfoMessage - Global Component

## Schema:

### Props: Manual props injection for Component work:

- **msg**. Accepts only one `any` parameter:

  --- `msg: any`. Ability to show info messages.

  _Example_:

  ```javascript
  const MESSAGE = 'All changes are saved!'
  ```

- **color**. Accepts only one `string` parameter:

  --- `color: string`. Ability to set layout color. Can be `gray`, `red`, `green`...

  _Example_:

  ```javascript
  const COLOR = 'red'
  ```

* **log**. Accepts only one `string[]` optional parameter:

  --- `log: string[]`. Ability to show log of messages.

  _Example_:

  ```javascript
  const LOG = [
    'You put Glock 17 in 1st position on your bazaar.',
    'You put Glock 17 in 2nd position on your bazaar.',
    'You put Glock 17 in 3rd position on your bazaar.',
    'You put Glock 17 in 4th position on your bazaar.',
    'You put Glock 17 in 5th position on your bazaar.'
  ]
  ```
