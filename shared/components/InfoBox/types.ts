export type TLog = TLogMessage[]

export type TMessage = any

export type TLogMessage = string

export interface IProps {
  msg?: TMessage
  color?: string
  log?: TLog
  showDangerously?: boolean
  loading?: boolean
  customClass?: string
}

export interface IState {
  showLog: boolean
}

export interface IRender {
  isServerMessage?: boolean
}

export type TRenderMessage = () => JSX.Element

export type TRenderLogMessageParams = {
  showDangerously: boolean
  message: TMessage
}

export type TRenderLogMessage = (params: TRenderLogMessageParams) => JSX.Element

export type TRenderLogList = () => JSX.Element

export type TRenderLogToggler = () => JSX.Element

export type TOnToggleLog = () => void

export type TWithLog = () => boolean

export type TWithActiveLog = () => boolean
