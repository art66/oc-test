import React from 'react'
import cn from 'classnames'
import {
  IProps,
  IRender,
  TRenderLogToggler,
  IState,
  TOnToggleLog,
  TRenderMessage,
  TRenderLogMessage,
  TRenderLogList,
  TWithLog,
  TWithActiveLog
} from './types'
import styles from './index.cssmodule.scss'
import LoadingIndicator from '@torn/shared/components/LoadingIndicator'

class InfoBox extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)

    this.state = {
      showLog: false
    }
  }

  _withLog: TWithLog = () => {
    const { log } = this.props

    return log && log.length > 0
  }

  _withActiveLog: TWithActiveLog = () => {
    const { showLog } = this.state

    return this._withLog() && showLog
  }

  _onToggleLog: TOnToggleLog = () => {
    const { showLog } = this.state

    this.setState({ showLog: !showLog })
  }

  _serverRenderedDOM = () => {
    const { msg } = this.props

    const isServerRenderedDOM = msg.match && msg.match(/<div[\s\S]*.*<\/div>/gim)

    return isServerRenderedDOM || false
  }

  _getMessage = () => {
    const { msg } = this.props

    return typeof msg === 'object' ? JSON.stringify(msg) : msg
  }

  _renderLogToggler: TRenderLogToggler = () => {
    const { showLog } = this.state

    const togglerText = showLog ? 'Hide log' : 'Show log'

    return (
      <a className={styles.logToggler} onClick={this._onToggleLog}>
        {togglerText}
      </a>
    )
  }

  _renderSSRMessage = () => {
    const infoMessage = this._getMessage()

    return <div dangerouslySetInnerHTML={{ __html: infoMessage }} />
  }

  _renderLogMessage: TRenderLogMessage = ({ showDangerously, message }) => {
    if (showDangerously) {
      return <div dangerouslySetInnerHTML={{ __html: message }} />
    }

    return <p>{message}</p>
  }

  _renderLogList: TRenderLogList = () => {
    const { log, showDangerously } = this.props
    const { showLog } = this.state

    if (!showLog) {
      return null
    }

    const logList = log.map((text, index) =>
      showDangerously ? <li key={index} dangerouslySetInnerHTML={{ __html: text }} /> : <li key={index}>{text}</li>
    )

    return <ul className='t-gray-9 p10 log-list'>{logList}</ul>
  }

  _renderMessage: TRenderMessage = () => {
    const { loading, showDangerously } = this.props

    const message = this._getMessage()

    if (loading) {
      return (
        <div className={cn('msg right-round', styles.messageContent)}>
          <LoadingIndicator className={styles.loadingIndicator} />
          {/* TODO: Loader should be with 10 dots. */}
        </div>
      )
    }

    if (this._withLog()) {
      return (
        <div className={cn('msg right-round', styles.messageContent)}>
          <div className='change-logger'>
            {this._renderLogMessage({ showDangerously, message })}
            {this._renderLogList()}
            {this._renderLogToggler()}
          </div>
        </div>
      )
    }

    if (showDangerously) {
      return (
        <div className={cn('msg right-round', styles.messageContent)} dangerouslySetInnerHTML={{ __html: message }} />
      )
    }

    return <div className={cn('msg right-round', styles.messageContent)}>{message}</div>
  }

  _renderRegularMessage = () => {
    const { color, customClass } = this.props

    return (
      <div className={cn('info-msg-cont border-round m-top10', color, customClass)}>
        <div className={cn('info-msg border-round', styles.messageWrap, { [styles.activeLog]: this._withActiveLog() })}>
          <i className={styles.infoIcon} />
          <div className='delimiter'>{this._renderMessage()}</div>
        </div>
      </div>
    )
  }

  _renderContainer = (config?: IRender) => {
    const { isServerMessage = false } = config || {}
    const children = isServerMessage ? this._renderSSRMessage() : this._renderRegularMessage()

    return (
      <div className='wrapper'>
        {children}
        <hr className='page-head-delimiter m-top10 m-bottom10' />
      </div>
    )
  }

  render() {
    const { msg, loading } = this.props

    if (loading) {
      return this._renderContainer()
    }

    if (!msg) {
      return null
    }

    // legacy
    if (msg instanceof Error) {
      console.error(msg)

      return this._renderContainer()
    }

    if (this._serverRenderedDOM()) {
      return this._renderContainer({ isServerMessage: true })
    }

    return this._renderContainer()
  }
}

export default InfoBox
