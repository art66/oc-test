import React from 'react'
import { select } from '@storybook/addon-knobs'
import InfoBox from './index'
import { DEFAULT_CONFIG, COLOR_OPRIONS, LOG_CONFIG, HTML_CONFIG, HTML_AND_LOG_CONFIG, LOADING_CONFIG } from './mocks'

export const Defaults = () => {
  const config = {
    ...DEFAULT_CONFIG,
    color: select('Color:', COLOR_OPRIONS, COLOR_OPRIONS[0], 'options')
  }

  return <InfoBox {...config} />
}

Defaults.story = {
  name: 'defaults'
}

export const WithHtml = () => {
  const config = {
    ...HTML_CONFIG,
    color: select('Color:', COLOR_OPRIONS, COLOR_OPRIONS[0], 'options')
  }

  return <InfoBox {...config} />
}

WithHtml.story = {
  name: 'with html'
}

export const WithLog = () => {
  const config = {
    ...LOG_CONFIG,
    color: select('Color:', COLOR_OPRIONS, COLOR_OPRIONS[0], 'options')
  }

  return <InfoBox {...config} />
}

WithLog.story = {
  name: 'with log'
}

export const WithHtmlAndLog = () => {
  const config = {
    ...HTML_AND_LOG_CONFIG,
    color: select('Color:', COLOR_OPRIONS, COLOR_OPRIONS[0], 'options')
  }

  return <InfoBox {...config} />
}

WithHtmlAndLog.story = {
  name: 'with html and log'
}

export const Loading = () => {
  const config = {
    ...LOADING_CONFIG,
    color: select('Color:', COLOR_OPRIONS, COLOR_OPRIONS[0], 'options')
  }

  return <InfoBox {...config} />
}

Loading.story = {
  name: 'loading'
}

export default {
  title: 'Shared/InfoBox'
}
