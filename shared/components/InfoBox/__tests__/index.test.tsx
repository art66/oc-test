import React from 'react'
import { shallow } from 'enzyme'
import initialState, {
  objectMessage,
  serverStringMessage,
  domMessage,
  textMessageWithLogState,
  htmlMessageWithLogState,
  loadingState,
  filledLoadingState,
  filledHtmlLoadingState
} from './mocks'
import InfoBox from '../index'

describe('<InfoBox />', () => {
  it('should render InfoBox component with string message', () => {
    const Component = shallow(<InfoBox {...initialState} />)

    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('.msg.right-round').length).toBe(1)
    expect(Component.find('.msg.right-round').html()).toBe(
      '<div class="msg right-round messageContent">testError</div>'
    )
    expect(Component).toMatchSnapshot()
  })

  it('should render InfoBox component with object message', () => {
    const Component = shallow(<InfoBox {...objectMessage} />)

    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('.msg.right-round').length).toBe(1)
    expect(Component.find('.msg.right-round').html()).toEqual(
      `<div class=\"msg right-round messageContent\">{&quot;message&quot;:&quot;server object message&quot;}</div>`
    )
    expect(Component).toMatchSnapshot()
  })

  it('should render InfoBox component with server string message', () => {
    const Component = shallow(<InfoBox {...serverStringMessage} />)

    expect(Component.find('.wrapper').length).toBe(1)
    expect(Component.find('.msg.right-round').length).toBe(1)
    expect(Component.find('.msg.right-round').html()).toEqual(
      `<div class=\"msg right-round messageContent\">server string message</div>`
    )
    expect(Component).toMatchSnapshot()
  })

  it('should render InfoBox component with DOM message', () => {
    const Component = shallow(<InfoBox {...domMessage} />)

    expect(Component.find('div').length).toBe(2)
    expect(
      Component.find('div')
        .at(0)
        .html()
    ).toEqual(
      `<div class=\"wrapper\"><div><div class=\"msg\">server <div>hah</div>object message</div></div><hr class=\"page-head-delimiter m-top10 m-bottom10\"/></div>`
    )
    expect(Component).toMatchSnapshot()
  })

  it('should render with string message and log', () => {
    const Component = shallow<InfoBox>(<InfoBox {...textMessageWithLogState} />)
    const component = Component.instance()

    Component.setState({ showLog: true })

    expect(Component.prop('className')).toBe('wrapper')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const ContainerComponent = ComponentChildren.at(0)
    expect(ContainerComponent.is('div')).toBeTruthy()
    expect(ContainerComponent.prop('className')).toBe('info-msg-cont border-round m-top10')
    const ContainerChildren = ComponentChildren.children()
    expect(ContainerChildren.length).toBe(1)
    const InfoMessageComponent = ContainerChildren.at(0)
    expect(InfoMessageComponent.is('div')).toBeTruthy()
    expect(InfoMessageComponent.prop('className')).toBe('info-msg border-round messageWrap activeLog')
    const InfoMessageChildren = InfoMessageComponent.children()
    expect(InfoMessageChildren.length).toBe(2)
    const IconComponent = InfoMessageChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe('infoIcon')
    const DelimiterComponent = InfoMessageChildren.at(1)
    expect(DelimiterComponent.prop('className')).toBe('delimiter')
    const DelimiterChildren = DelimiterComponent.children()
    expect(DelimiterChildren.length).toBe(1)
    const LoggerContainerComponent = DelimiterChildren.at(0)
    expect(LoggerContainerComponent.prop('className')).toBe('msg right-round messageContent')
    const LoggerContainerChildren = LoggerContainerComponent.children()
    expect(LoggerContainerChildren.length).toBe(1)
    const LoggerComponent = LoggerContainerChildren.at(0)
    expect(LoggerComponent.prop('className')).toBe('change-logger')
    const LoggerChildren = LoggerComponent.children()
    expect(LoggerChildren.length).toBe(3)
    const MessageComponent = LoggerChildren.at(0)
    expect(MessageComponent.is('p')).toBeTruthy()
    expect(MessageComponent.text()).toBe('Test message')
    const LogListComponent = LoggerChildren.at(1)
    expect(LogListComponent.is('ul')).toBeTruthy()
    expect(LogListComponent.prop('className')).toBe('t-gray-9 p10 log-list')
    const LogListChildren = LogListComponent.children()
    expect(LogListChildren.length).toBe(5)
    ;[
      'You put Glock 17 in 1st position on your bazaar.',
      'You put Glock 17 in 2nd position on your bazaar.',
      'You put Glock 17 in 3rd position on your bazaar.',
      'You put Glock 17 in 4th position on your bazaar.',
      'You put Glock 17 in 5th position on your bazaar.'
    ].forEach((message, index) => {
      expect(LogListChildren.at(index).is('li')).toBeTruthy()
      expect(LogListChildren.at(index).key()).toBe(index.toString())
      expect(LogListChildren.at(index).text()).toBe(message)
    })
    const LogTogglerComponent = LoggerChildren.at(2)
    expect(LogTogglerComponent.is('a')).toBeTruthy()
    expect(LogTogglerComponent.prop('className')).toBe('logToggler')
    expect(LogTogglerComponent.prop('onClick')).toBe(component._onToggleLog)
    const HeadDelimiterComponent = ComponentChildren.at(1)
    expect(HeadDelimiterComponent.is('hr')).toBeTruthy()
    expect(HeadDelimiterComponent.prop('className')).toBe('page-head-delimiter m-top10 m-bottom10')

    expect(Component).toMatchSnapshot()
  })

  it('should render with html message and log', () => {
    const Component = shallow<InfoBox>(<InfoBox {...htmlMessageWithLogState} />)
    const component = Component.instance()

    Component.setState({ showLog: true })

    expect(Component.prop('className')).toBe('wrapper')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const ContainerComponent = ComponentChildren.at(0)
    expect(ContainerComponent.is('div')).toBeTruthy()
    expect(ContainerComponent.prop('className')).toBe('info-msg-cont border-round m-top10')
    const ContainerChildren = ComponentChildren.children()
    expect(ContainerChildren.length).toBe(1)
    const InfoMessageComponent = ContainerChildren.at(0)
    expect(InfoMessageComponent.is('div')).toBeTruthy()
    expect(InfoMessageComponent.prop('className')).toBe('info-msg border-round messageWrap activeLog')
    const InfoMessageChildren = InfoMessageComponent.children()
    expect(InfoMessageChildren.length).toBe(2)
    const IconComponent = InfoMessageChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe('infoIcon')
    const DelimiterComponent = InfoMessageChildren.at(1)
    expect(DelimiterComponent.prop('className')).toBe('delimiter')
    const DelimiterChildren = DelimiterComponent.children()
    expect(DelimiterChildren.length).toBe(1)
    const LoggerContainerComponent = DelimiterChildren.at(0)
    expect(LoggerContainerComponent.prop('className')).toBe('msg right-round messageContent')
    const LoggerContainerChildren = LoggerContainerComponent.children()
    expect(LoggerContainerChildren.length).toBe(1)
    const LoggerComponent = LoggerContainerChildren.at(0)
    expect(LoggerComponent.prop('className')).toBe('change-logger')
    const LoggerChildren = LoggerComponent.children()
    expect(LoggerChildren.length).toBe(3)
    const MessageComponent = LoggerChildren.at(0)
    expect(MessageComponent.is('div')).toBeTruthy()
    expect(MessageComponent.html()).toBe('<div><button>Click on me</button></div>')
    const LogListComponent = LoggerChildren.at(1)
    expect(LogListComponent.is('ul')).toBeTruthy()
    expect(LogListComponent.prop('className')).toBe('t-gray-9 p10 log-list')
    const LogListChildren = LogListComponent.children()
    expect(LogListChildren.length).toBe(5)
    ;[
      'You put Glock 17 in 1st position on your bazaar.',
      'You put Glock 17 in 2nd position on your bazaar.',
      'You put Glock 17 in 3rd position on your bazaar.',
      'You put Glock 17 in 4th position on your bazaar.',
      'You put Glock 17 in 5th position on your bazaar.'
    ].forEach((message, index) => {
      expect(LogListChildren.at(index).is('li')).toBeTruthy()
      expect(LogListChildren.at(index).key()).toBe(index.toString())
      expect(LogListChildren.at(index).html()).toBe(`<li>${message}</li>`)
    })
    const LogTogglerComponent = LoggerChildren.at(2)
    expect(LogTogglerComponent.is('a')).toBeTruthy()
    expect(LogTogglerComponent.prop('className')).toBe('logToggler')
    expect(LogTogglerComponent.prop('onClick')).toBe(component._onToggleLog)
    const HeadDelimiterComponent = ComponentChildren.at(1)
    expect(HeadDelimiterComponent.is('hr')).toBeTruthy()
    expect(HeadDelimiterComponent.prop('className')).toBe('page-head-delimiter m-top10 m-bottom10')

    expect(Component).toMatchSnapshot()
  })

  it('should render with string message and deactivated log', () => {
    const Component = shallow<InfoBox>(<InfoBox {...textMessageWithLogState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('wrapper')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const ContainerComponent = ComponentChildren.at(0)
    expect(ContainerComponent.is('div')).toBeTruthy()
    expect(ContainerComponent.prop('className')).toBe('info-msg-cont border-round m-top10')
    const ContainerChildren = ComponentChildren.children()
    expect(ContainerChildren.length).toBe(1)
    const InfoMessageComponent = ContainerChildren.at(0)
    expect(InfoMessageComponent.is('div')).toBeTruthy()
    expect(InfoMessageComponent.prop('className')).toBe('info-msg border-round messageWrap')
    const InfoMessageChildren = InfoMessageComponent.children()
    expect(InfoMessageChildren.length).toBe(2)
    const IconComponent = InfoMessageChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe('infoIcon')
    const DelimiterComponent = InfoMessageChildren.at(1)
    expect(DelimiterComponent.prop('className')).toBe('delimiter')
    const DelimiterChildren = DelimiterComponent.children()
    expect(DelimiterChildren.length).toBe(1)
    const LoggerContainerComponent = DelimiterChildren.at(0)
    expect(LoggerContainerComponent.prop('className')).toBe('msg right-round messageContent')
    const LoggerContainerChildren = LoggerContainerComponent.children()
    expect(LoggerContainerChildren.length).toBe(1)
    const LoggerComponent = LoggerContainerChildren.at(0)
    expect(LoggerComponent.prop('className')).toBe('change-logger')
    const LoggerChildren = LoggerComponent.children()
    expect(LoggerChildren.length).toBe(2)
    const MessageComponent = LoggerChildren.at(0)
    expect(MessageComponent.is('p')).toBeTruthy()
    expect(MessageComponent.text()).toBe('Test message')
    const LogTogglerComponent = LoggerChildren.at(1)
    expect(LogTogglerComponent.is('a')).toBeTruthy()
    expect(LogTogglerComponent.prop('className')).toBe('logToggler')
    expect(LogTogglerComponent.prop('onClick')).toBe(component._onToggleLog)
    const HeadDelimiterComponent = ComponentChildren.at(1)
    expect(HeadDelimiterComponent.is('hr')).toBeTruthy()
    expect(HeadDelimiterComponent.prop('className')).toBe('page-head-delimiter m-top10 m-bottom10')

    expect(Component).toMatchSnapshot()
  })

  it('should render with html message and deactivated log', () => {
    const Component = shallow<InfoBox>(<InfoBox {...textMessageWithLogState} />)
    const component = Component.instance()

    expect(Component.prop('className')).toBe('wrapper')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const ContainerComponent = ComponentChildren.at(0)
    expect(ContainerComponent.is('div')).toBeTruthy()
    expect(ContainerComponent.prop('className')).toBe('info-msg-cont border-round m-top10')
    const ContainerChildren = ComponentChildren.children()
    expect(ContainerChildren.length).toBe(1)
    const InfoMessageComponent = ContainerChildren.at(0)
    expect(InfoMessageComponent.is('div')).toBeTruthy()
    expect(InfoMessageComponent.prop('className')).toBe('info-msg border-round messageWrap')
    const InfoMessageChildren = InfoMessageComponent.children()
    expect(InfoMessageChildren.length).toBe(2)
    const IconComponent = InfoMessageChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe('infoIcon')
    const DelimiterComponent = InfoMessageChildren.at(1)
    expect(DelimiterComponent.prop('className')).toBe('delimiter')
    const DelimiterChildren = DelimiterComponent.children()
    expect(DelimiterChildren.length).toBe(1)
    const LoggerContainerComponent = DelimiterChildren.at(0)
    expect(LoggerContainerComponent.prop('className')).toBe('msg right-round messageContent')
    const LoggerContainerChildren = LoggerContainerComponent.children()
    expect(LoggerContainerChildren.length).toBe(1)
    const LoggerComponent = LoggerContainerChildren.at(0)
    expect(LoggerComponent.prop('className')).toBe('change-logger')
    const LoggerChildren = LoggerComponent.children()
    expect(LoggerChildren.length).toBe(2)
    const LogTogglerComponent = LoggerChildren.at(1)
    expect(LogTogglerComponent.is('a')).toBeTruthy()
    expect(LogTogglerComponent.prop('className')).toBe('logToggler')
    expect(LogTogglerComponent.prop('onClick')).toBe(component._onToggleLog)
    const HeadDelimiterComponent = ComponentChildren.at(1)
    expect(HeadDelimiterComponent.is('hr')).toBeTruthy()
    expect(HeadDelimiterComponent.prop('className')).toBe('page-head-delimiter m-top10 m-bottom10')

    expect(Component).toMatchSnapshot()
  })

  it('should render loading state', () => {
    const Component = shallow<InfoBox>(<InfoBox {...loadingState} />)

    expect(Component.prop('className')).toBe('wrapper')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const ContainerComponent = ComponentChildren.at(0)
    expect(ContainerComponent.is('div')).toBeTruthy()
    expect(ContainerComponent.prop('className')).toBe('info-msg-cont border-round m-top10')
    const ContainerChildren = ComponentChildren.children()
    expect(ContainerChildren.length).toBe(1)
    const InfoMessageComponent = ContainerChildren.at(0)
    expect(InfoMessageComponent.is('div')).toBeTruthy()
    expect(InfoMessageComponent.prop('className')).toBe('info-msg border-round messageWrap')
    const InfoMessageChildren = InfoMessageComponent.children()
    expect(InfoMessageChildren.length).toBe(2)
    const IconComponent = InfoMessageChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe('infoIcon')
    const DelimiterComponent = InfoMessageChildren.at(1)
    expect(DelimiterComponent.prop('className')).toBe('delimiter')
    const DelimiterChildren = DelimiterComponent.children()
    expect(DelimiterChildren.length).toBe(1)
    const LoadingContainerComponent = DelimiterChildren.at(0)
    expect(LoadingContainerComponent.prop('className')).toBe('msg right-round messageContent')
    const LoadingContainerChildren = LoadingContainerComponent.children()
    expect(LoadingContainerChildren.length).toBe(1)
    const LoadingComponent = LoadingContainerChildren.at(0)
    expect(LoadingComponent.prop('className')).toBe('loadingIndicator')
    const HeadDelimiterComponent = ComponentChildren.at(1)
    expect(HeadDelimiterComponent.is('hr')).toBeTruthy()
    expect(HeadDelimiterComponent.prop('className')).toBe('page-head-delimiter m-top10 m-bottom10')

    expect(Component).toMatchSnapshot()
  })

  it('should render loading filled state', () => {
    const Component = shallow<InfoBox>(<InfoBox {...filledLoadingState} />)

    expect(Component.prop('className')).toBe('wrapper')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const ContainerComponent = ComponentChildren.at(0)
    expect(ContainerComponent.is('div')).toBeTruthy()
    expect(ContainerComponent.prop('className')).toBe('info-msg-cont border-round m-top10')
    const ContainerChildren = ComponentChildren.children()
    expect(ContainerChildren.length).toBe(1)
    const InfoMessageComponent = ContainerChildren.at(0)
    expect(InfoMessageComponent.is('div')).toBeTruthy()
    expect(InfoMessageComponent.prop('className')).toBe('info-msg border-round messageWrap')
    const InfoMessageChildren = InfoMessageComponent.children()
    expect(InfoMessageChildren.length).toBe(2)
    const IconComponent = InfoMessageChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe('infoIcon')
    const DelimiterComponent = InfoMessageChildren.at(1)
    expect(DelimiterComponent.prop('className')).toBe('delimiter')
    const DelimiterChildren = DelimiterComponent.children()
    expect(DelimiterChildren.length).toBe(1)
    const LoadingContainerComponent = DelimiterChildren.at(0)
    expect(LoadingContainerComponent.prop('className')).toBe('msg right-round messageContent')
    const LoadingContainerChildren = LoadingContainerComponent.children()
    expect(LoadingContainerChildren.length).toBe(1)
    const LoadingComponent = LoadingContainerChildren.at(0)
    expect(LoadingComponent.prop('className')).toBe('loadingIndicator')
    const HeadDelimiterComponent = ComponentChildren.at(1)
    expect(HeadDelimiterComponent.is('hr')).toBeTruthy()
    expect(HeadDelimiterComponent.prop('className')).toBe('page-head-delimiter m-top10 m-bottom10')

    expect(Component).toMatchSnapshot()
  })

  it('should render loading filled with html state', () => {
    const Component = shallow<InfoBox>(<InfoBox {...filledHtmlLoadingState} />)

    expect(Component.prop('className')).toBe('wrapper')
    const ComponentChildren = Component.children()
    expect(ComponentChildren.length).toBe(2)
    const ContainerComponent = ComponentChildren.at(0)
    expect(ContainerComponent.is('div')).toBeTruthy()
    expect(ContainerComponent.prop('className')).toBe('info-msg-cont border-round m-top10')
    const ContainerChildren = ComponentChildren.children()
    expect(ContainerChildren.length).toBe(1)
    const InfoMessageComponent = ContainerChildren.at(0)
    expect(InfoMessageComponent.is('div')).toBeTruthy()
    expect(InfoMessageComponent.prop('className')).toBe('info-msg border-round messageWrap')
    const InfoMessageChildren = InfoMessageComponent.children()
    expect(InfoMessageChildren.length).toBe(2)
    const IconComponent = InfoMessageChildren.at(0)
    expect(IconComponent.is('i')).toBeTruthy()
    expect(IconComponent.prop('className')).toBe('infoIcon')
    const DelimiterComponent = InfoMessageChildren.at(1)
    expect(DelimiterComponent.prop('className')).toBe('delimiter')
    const DelimiterChildren = DelimiterComponent.children()
    expect(DelimiterChildren.length).toBe(1)
    const LoadingContainerComponent = DelimiterChildren.at(0)
    expect(LoadingContainerComponent.prop('className')).toBe('msg right-round messageContent')
    const LoadingContainerChildren = LoadingContainerComponent.children()
    expect(LoadingContainerChildren.length).toBe(1)
    const LoadingComponent = LoadingContainerChildren.at(0)
    expect(LoadingComponent.prop('className')).toBe('loadingIndicator')
    const HeadDelimiterComponent = ComponentChildren.at(1)
    expect(HeadDelimiterComponent.is('hr')).toBeTruthy()
    expect(HeadDelimiterComponent.prop('className')).toBe('page-head-delimiter m-top10 m-bottom10')

    expect(Component).toMatchSnapshot()
  })

  it('should handle log toggler', () => {
    const Component = shallow<InfoBox>(<InfoBox {...textMessageWithLogState} />)
    const component = Component.instance()

    expect(Component.state('showLog')).toBe(false)

    component._onToggleLog()

    expect(Component.state('showLog')).toBe(true)

    expect(Component).toMatchSnapshot()
  })
})
