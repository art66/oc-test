import { IProps } from '../../types'

const initialState = {
  msg: 'testError'
}

export const objectMessage = {
  msg: {
    message: 'server object message'
  }
}

export const serverStringMessage: IProps = {
  msg: 'server string message'
}

export const domMessage: IProps = {
  msg: '<div class="msg">server <div>hah</div>object message</div>'
}

export const textMessageWithLogState: IProps = {
  msg: 'Test message',
  log: [
    'You put Glock 17 in 1st position on your bazaar.',
    'You put Glock 17 in 2nd position on your bazaar.',
    'You put Glock 17 in 3rd position on your bazaar.',
    'You put Glock 17 in 4th position on your bazaar.',
    'You put Glock 17 in 5th position on your bazaar.'
  ]
}

export const htmlMessageWithLogState: IProps = {
  msg: '<button>Click on me</button>',
  log: [
    'You put Glock 17 in 1st position on your bazaar.',
    'You put Glock 17 in 2nd position on your bazaar.',
    'You put Glock 17 in 3rd position on your bazaar.',
    'You put Glock 17 in 4th position on your bazaar.',
    'You put Glock 17 in 5th position on your bazaar.'
  ],
  showDangerously: true
}

export const loadingState: IProps = {
  loading: true
}

export const filledLoadingState: IProps = {
  loading: true,
  msg: 'Test message',
  log: [
    'You put Glock 17 in 1st position on your bazaar.',
    'You put Glock 17 in 2nd position on your bazaar.',
    'You put Glock 17 in 3rd position on your bazaar.',
    'You put Glock 17 in 4th position on your bazaar.',
    'You put Glock 17 in 5th position on your bazaar.'
  ]
}

export const filledHtmlLoadingState: IProps = {
  loading: true,
  msg: '<button>Click on me</button>',
  log: [
    'You put Glock 17 in 1st position on your bazaar.',
    'You put Glock 17 in 2nd position on your bazaar.',
    'You put Glock 17 in 3rd position on your bazaar.',
    'You put Glock 17 in 4th position on your bazaar.',
    'You put Glock 17 in 5th position on your bazaar.'
  ],
  showDangerously: true
}

export default initialState
