# InfoBox - Torn

## 1.1.0

- Added icon centering inside the container.

## 1.0.0

- First stable release.
