import { IProps } from '../container/types'

export const SORT_BY_OPTIONS = [
  { id: 'default', name: 'Default' },
  { id: 'name', name: 'Name' },
  { id: 'category', name: 'Category' },
  { id: 'cost', name: 'Cost' },
  { id: 'value', name: 'Value' },
  { id: 'rarity', name: 'Rarity' }
]

export const ORDER_BY_OPTIONS = ['asc', 'desc']

export const SEARCH_AUTOCOMPLETE_LIST = [
  {
    id: '0',
    name: 'Weapon'
  },
  {
    id: '1',
    name: 'Guns'
  },
  {
    id: '2',
    name: 'Pistols'
  },
  {
    id: '3',
    name: 'Hacker tools'
  }
]

export const MEDIA_TYPE_OPTIONS = ['', 'desktop', 'tablet', 'mobile']

export const ATTACHED_OPTIONS = ['', 'top']

export const DEFAULT_STATE: IProps = {
  sortBy: {
    value: '',
    list: SORT_BY_OPTIONS,
    onChange() {}
  },
  orderBy: {
    value: '',
    list: ORDER_BY_OPTIONS,
    onChange() {}
  },
  search: {
    value: '',
    list: SEARCH_AUTOCOMPLETE_LIST,
    onChange() {}
  }
}
