import { IProps } from '../../types'

export const initialState: IProps = {
  className: 'testClassName',
  sortBy: {
    value: 'category',
    list: [{ id: 'name', name: 'Name' }, { id: 'category', name: 'Category' }, { id: 'rarity', name: 'Rarity' }],
    onChange: () => {}
  },
  search: {
    value: 'test search string',
    list: [{ id: '0', name: 'test1' }, { id: '1', name: 'test2' }, { id: '2', name: 'test3' }],
    onChange: () => {}
  },
  orderBy: {
    value: 'asc',
    list: ['asc', 'desc'],
    onChange() {}
  },
  onChange: () => {},
  mediaType: null,
  attached: null
}

export const attachedTopState: IProps = {
  ...initialState,
  attached: 'top'
}

export const tabletState: IProps = {
  ...initialState,
  mediaType: 'tablet'
}

export const mobileState: IProps = {
  ...initialState,
  mediaType: 'mobile'
}

export const descOrderState: IProps = {
  ...initialState,
  orderBy: {
    ...initialState.orderBy,
    value: 'desc'
  }
}

export const tabletDescState: IProps = {
  ...descOrderState,
  mediaType: 'tablet'
}
