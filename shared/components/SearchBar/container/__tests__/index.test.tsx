import React from 'react'
import { mount } from 'enzyme'

import Dropdown from '@torn/shared/components/Dropdown'
import SearchBar from '../'
import { Item, Search } from '../../components'
import { initialState, tabletState, mobileState, attachedTopState, descOrderState, tabletDescState } from './mocks'

describe('<SearchBar />', () => {
  it('should render SearchBar component default props', () => {
    const Component = mount<SearchBar>(<SearchBar />)
    const componentInstance = Component.instance()

    expect(Component.find('.searchBar').length).toBe(1)
    expect(Component.find('.searchBar').find(Item).length).toBe(1)
    expect(
      Component.find(Item)
        .last()
        .prop('insensitive')
    ).toBe(true)
    expect(Component.find(Item).find(Search).length).toBe(1)
    expect(
      Component.find(Item)
        .last()
        .find(Search).length
    ).toBe(1)
    expect(Component.find(Search).prop('list')).toEqual([])
    expect(Component.find(Search).prop('value')).toEqual('')
    expect(Component.find(Search).prop('onChange')).toEqual(componentInstance._onSearchChange)

    expect(Component.find('.searchBar').prop('className')).toBe('searchBar')
    expect(Component.find(Item).prop('mediaType')).toBeNull()
    expect(
      Component.find(Item)
        .find(Search)
        .prop('mediaType')
    ).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render SearchBar component', () => {
    const Component = mount<SearchBar>(<SearchBar {...initialState} />)
    const componentInstance = Component.instance()

    expect(Component.find('.searchBar').length).toBe(1)
    expect(Component.find('.searchBar').prop('className')).toBe('searchBar testClassName')
    expect(Component.find('.searchBar').find(Item).length).toBe(4)
    expect(
      Component.find('.searchBar')
        .find(Item)
        .at(1)
        .prop('active')
    ).toBe(true)
    expect(
      Component.find(Item)
        .first()
        .prop('insensitive')
    ).toBe(false)
    expect(
      Component.find(Item)
        .last()
        .prop('insensitive')
    ).toBe(true)
    expect(Component.find(Item).find(Search).length).toBe(1)
    expect(
      Component.find(Item)
        .last()
        .find(Search).length
    ).toBe(1)
    expect(Component.find(Search).prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    expect(Component.find(Search).prop('value')).toEqual('test search string')
    expect(Component.find(Search).prop('onChange')).toEqual(componentInstance._onSearchChange)

    Component.find(Item).map(CollectionComponent => expect(CollectionComponent.prop('mediaType')).toBeNull())
    expect(
      Component.find(Item)
        .find(Search)
        .prop('mediaType')
    ).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render SearchBar component layout options', () => {
    const Component = mount<SearchBar>(<SearchBar {...attachedTopState} />)
    const componentInstance = Component.instance()

    expect(Component.find('.searchBar').length).toBe(1)
    expect(Component.find('.searchBar').prop('className')).toBe('searchBar attachedTop testClassName')
    expect(Component.find('.searchBar').find(Item).length).toBe(4)
    expect(
      Component.find('.searchBar')
        .find(Item)
        .at(1)
        .prop('active')
    ).toBe(true)
    expect(
      Component.find(Item)
        .first()
        .prop('insensitive')
    ).toBe(false)
    expect(
      Component.find(Item)
        .last()
        .prop('insensitive')
    ).toBe(true)
    expect(Component.find(Item).find(Search).length).toBe(1)
    expect(
      Component.find(Item)
        .last()
        .find(Search).length
    ).toBe(1)
    expect(Component.find(Search).prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    expect(Component.find(Search).prop('value')).toEqual('test search string')
    expect(Component.find(Search).prop('onChange')).toEqual(componentInstance._onSearchChange)

    Component.find(Item).map(CollectionComponent => expect(CollectionComponent.prop('mediaType')).toBeNull())
    expect(
      Component.find(Item)
        .find(Search)
        .prop('mediaType')
    ).toBeNull()

    expect(Component).toMatchSnapshot()
  })

  it('should render SearchBar component tablet media type', () => {
    const Component = mount<SearchBar>(<SearchBar {...tabletState} />)
    const componentInstance = Component.instance()

    expect(Component.find('.searchBar').length).toBe(1)
    expect(Component.find('.searchBar').prop('className')).toBe('searchBar tablet testClassName')
    const SortByDropdownItemComponent = Component.find(Item).first()
    expect(SortByDropdownItemComponent.prop('insensitive')).toBe(true)
    expect(SortByDropdownItemComponent.prop('mediaType')).toBe('tablet')
    expect(SortByDropdownItemComponent.find(Dropdown).length).toBe(1)
    expect(SortByDropdownItemComponent.find(Dropdown).prop('className')).toBe('react-dropdown-default dropdown tablet')
    const SortByDropdownComponent = SortByDropdownItemComponent.find(Dropdown)
    expect(SortByDropdownComponent.prop('className')).toBe('react-dropdown-default dropdown tablet')
    expect(SortByDropdownComponent.prop('list')).toEqual([
      { id: 'name', name: 'Name' },
      { id: 'category', name: 'Category' },
      { id: 'rarity', name: 'Rarity' }
    ])
    expect(SortByDropdownComponent.prop('placeholder')).toBe('Sort by')
    const SearchItemComponent = Component.find(Item).last()
    expect(SearchItemComponent.prop('insensitive')).toBe(true)
    expect(SearchItemComponent.prop('mediaType')).toBe('tablet')
    expect(SearchItemComponent.find(Search).length).toBe(1)
    expect(SearchItemComponent.find(Search).prop('mediaType')).toBe('tablet')
    expect(SearchItemComponent.find(Search).prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    expect(SearchItemComponent.find(Search).prop('value')).toEqual('test search string')
    expect(SearchItemComponent.find(Search).prop('onChange')).toEqual(componentInstance._onSearchChange)

    expect(Component).toMatchSnapshot()
  })

  it('should render SearchBar component mobile media type', () => {
    const Component = mount<SearchBar>(<SearchBar {...mobileState} />)
    const componentInstance = Component.instance()

    expect(Component.find('.searchBar').length).toBe(1)
    expect(Component.find('.searchBar').prop('className')).toBe('searchBar mobile testClassName')
    const SortByDropdownItemComponent = Component.find(Item).first()
    expect(SortByDropdownItemComponent.prop('insensitive')).toBe(true)
    expect(SortByDropdownItemComponent.prop('mediaType')).toBe('mobile')
    expect(SortByDropdownItemComponent.find(Dropdown).length).toBe(1)
    expect(SortByDropdownItemComponent.find(Dropdown).prop('className')).toBe('react-dropdown-default dropdown mobile')
    const SortByDropdownComponent = SortByDropdownItemComponent.find(Dropdown)
    expect(SortByDropdownComponent.prop('className')).toBe('react-dropdown-default dropdown mobile')
    expect(SortByDropdownComponent.prop('list')).toEqual([
      { id: 'name', name: 'Name' },
      { id: 'category', name: 'Category' },
      { id: 'rarity', name: 'Rarity' }
    ])
    expect(SortByDropdownComponent.prop('placeholder')).toBe('Sort by')
    const SearchItemComponent = Component.find(Item).last()
    expect(SearchItemComponent.prop('insensitive')).toBe(true)
    expect(SearchItemComponent.prop('mediaType')).toBe('mobile')
    expect(SearchItemComponent.find(Search).length).toBe(1)
    expect(SearchItemComponent.find(Search).prop('mediaType')).toBe('mobile')
    expect(SearchItemComponent.find(Search).prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    expect(SearchItemComponent.find(Search).prop('value')).toEqual('test search string')
    expect(SearchItemComponent.find(Search).prop('onChange')).toEqual(componentInstance._onSearchChange)

    expect(Component).toMatchSnapshot()
  })

  it('should c default value, emit onSortByChange and onChange on sortBy change', () => {
    const onSortByChange = jest.fn()
    const onOrderByChange = jest.fn()
    const onChange = jest.fn()
    const state = {
      ...initialState,
      sortBy: { ...initialState.sortBy, onChange: onSortByChange },
      orderBy: { ...initialState.orderBy, onChange: onOrderByChange },
      onChange
    }
    const Component = mount<SearchBar>(<SearchBar {...state} />)
    const componentInstance = Component.instance()

    expect(
      Component.find(Item)
        .first()
        .prop('onChange')
    ).toEqual(componentInstance._onSortByChange)

    componentInstance._onSortByChange('rarity')

    expect(onSortByChange).toHaveBeenCalledTimes(1)
    expect(onSortByChange.mock.calls[0][0]).toBe('rarity')

    expect(onOrderByChange).toHaveBeenCalledTimes(0)

    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toEqual({ sortBy: 'rarity', orderBy: 'asc', search: 'test search string' })

    expect(onSortByChange).toMatchSnapshot()
    expect(onOrderByChange).toMatchSnapshot()
    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should change orderBy emit orderByChange and onChange on the same sortBy button twice', () => {
    const onSortByChange = jest.fn()
    const onOrderByChange = jest.fn()
    const onChange = jest.fn()
    const state = {
      ...initialState,
      sortBy: { ...initialState.sortBy, onChange: onSortByChange },
      orderBy: { ...initialState.orderBy, onChange: onOrderByChange },
      onChange
    }
    const Component = mount<SearchBar>(<SearchBar {...state} />)
    const componentInstance = Component.instance()

    expect(
      Component.find(Item)
        .at(1)
        .prop('onChange')
    ).toEqual(componentInstance._onSortByChange)

    componentInstance._onSortByChange('category')

    expect(onSortByChange).toHaveBeenCalledTimes(0)

    expect(onOrderByChange).toHaveBeenCalledTimes(1)
    expect(onOrderByChange.mock.calls[0][0]).toBe('desc')

    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toEqual({ sortBy: 'category', orderBy: 'desc', search: 'test search string' })

    expect(onSortByChange).toMatchSnapshot()
    expect(onOrderByChange).toMatchSnapshot()
    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should emit orderByChange and onChange and set the first order by value if now is last value', () => {
    const onSortByChange = jest.fn()
    const onOrderByChange = jest.fn()
    const onChange = jest.fn()
    const state = {
      ...descOrderState,
      sortBy: { ...descOrderState.sortBy, onChange: onSortByChange },
      orderBy: { ...descOrderState.orderBy, onChange: onOrderByChange },
      onChange
    }
    const Component = mount<SearchBar>(<SearchBar {...state} />)
    const componentInstance = Component.instance()

    expect(
      Component.find(Item)
        .at(1)
        .prop('onChange')
    ).toEqual(componentInstance._onSortByChange)

    componentInstance._onSortByChange('category')

    expect(onSortByChange).toHaveBeenCalledTimes(0)

    expect(onOrderByChange).toHaveBeenCalledTimes(1)
    expect(onOrderByChange.mock.calls[0][0]).toBe('asc')

    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toEqual({ sortBy: 'category', orderBy: 'asc', search: 'test search string' })

    expect(onSortByChange).toMatchSnapshot()
    expect(onOrderByChange).toMatchSnapshot()
    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it(
    'should change sortBy, set orderBy default value, emit onSortByChange and onChange on dropdown sortBy' + ' changed',
    () => {
      const onSortByChange = jest.fn()
      const onOrderByChange = jest.fn()
      const onChange = jest.fn()
      const data = { id: 'name', name: 'Name' }
      const state = {
        ...tabletState,
        sortBy: { ...tabletState.sortBy, onChange: onSortByChange },
        orderBy: { ...tabletState.orderBy, onChange: onOrderByChange },
        onChange
      }
      const Component = mount<SearchBar>(<SearchBar {...state} />)
      const componentInstance = Component.instance()

      expect(
        Component.find(Item)
          .find(Dropdown)
          .first()
          .prop('onChange')
      ).toEqual(componentInstance._onSortByDropdownChange)

      componentInstance._onSortByDropdownChange(data)

      expect(onSortByChange).toHaveBeenCalledTimes(1)
      expect(onSortByChange.mock.calls[0][0]).toBe('name')

      expect(onOrderByChange).toHaveBeenCalledTimes(0)

      expect(onChange).toHaveBeenCalledTimes(1)
      expect(onChange.mock.calls[0][0]).toEqual({ sortBy: 'name', orderBy: 'asc', search: 'test search string' })

      expect(onSortByChange).toMatchSnapshot()
      expect(onOrderByChange).toMatchSnapshot()
      expect(onChange).toMatchSnapshot()
      expect(Component).toMatchSnapshot()
    }
  )

  it('should change orderBy, emit onOrderByChange and onChange on dropdown sortBy the same', () => {
    const onSortByChange = jest.fn()
    const onOrderByChange = jest.fn()
    const onChange = jest.fn()
    const data = { id: 'category', name: 'Category' }
    const state = {
      ...tabletState,
      sortBy: { ...tabletState.sortBy, onChange: onSortByChange },
      orderBy: { ...tabletState.orderBy, onChange: onOrderByChange },
      onChange
    }
    const Component = mount<SearchBar>(<SearchBar {...state} />)
    const componentInstance = Component.instance()

    expect(
      Component.find(Item)
        .find(Dropdown)
        .first()
        .prop('onChange')
    ).toEqual(componentInstance._onSortByDropdownChange)

    componentInstance._onSortByDropdownChange(data)

    expect(onSortByChange).toHaveBeenCalledTimes(0)

    expect(onOrderByChange).toHaveBeenCalledTimes(1)
    expect(onOrderByChange.mock.calls[0][0]).toBe('desc')

    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toEqual({ sortBy: 'category', orderBy: 'desc', search: 'test search string' })

    expect(onSortByChange).toMatchSnapshot()
    expect(onOrderByChange).toMatchSnapshot()
    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should change sortBy, set orderBy default value, emit onSortByChange and onChange on dropdown and set the first order by value if now is the last value', () => {
    const onSortByChange = jest.fn()
    const onOrderByChange = jest.fn()
    const onChange = jest.fn()
    const data = { id: 'category', name: 'Category' }
    const state = {
      ...tabletDescState,
      sortBy: { ...tabletDescState.sortBy, onChange: onSortByChange },
      orderBy: { ...tabletDescState.orderBy, onChange: onOrderByChange },
      onChange
    }
    const Component = mount<SearchBar>(<SearchBar {...state} />)
    const componentInstance = Component.instance()

    expect(
      Component.find(Item)
        .find(Dropdown)
        .first()
        .prop('onChange')
    ).toEqual(componentInstance._onSortByDropdownChange)

    componentInstance._onSortByDropdownChange(data)

    expect(onSortByChange).toHaveBeenCalledTimes(0)

    expect(onOrderByChange).toHaveBeenCalledTimes(1)
    expect(onOrderByChange.mock.calls[0][0]).toBe('asc')

    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toEqual({ sortBy: 'category', orderBy: 'asc', search: 'test search string' })

    expect(onSortByChange).toMatchSnapshot()
    expect(onOrderByChange).toMatchSnapshot()
    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })

  it('should emit onSearchChange and onChange on search change', () => {
    const onSearchChange = jest.fn()
    const onChange = jest.fn()
    const state = { ...initialState, search: { ...initialState.search, onChange: onSearchChange }, onChange }
    const Component = mount<SearchBar>(<SearchBar {...state} />)
    const componentInstance = Component.instance()

    expect(
      Component.find(Item)
        .last()
        .find(Search)
        .prop('onChange')
    ).toEqual(componentInstance._onSearchChange)

    componentInstance._onSearchChange('test1')

    expect(onSearchChange).toHaveBeenCalledTimes(1)
    expect(onSearchChange.mock.calls[0][0]).toBe('test1')

    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toEqual({ orderBy: 'asc', sortBy: 'category', search: 'test1' })

    expect(Component.find('.searchBar.attachedTop').length).toBe(0)

    expect(onSearchChange).toMatchSnapshot()
    expect(onChange).toMatchSnapshot()
    expect(Component).toMatchSnapshot()
  })
})
