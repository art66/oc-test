import { IProps as ISearchProps } from '../components/Search/types'

interface ISortByItem {
  id: string
  name: string
}

export interface IOutput {
  sortBy: string
  search: string
}

interface ISortBy {
  value?: string
  list?: ISortByItem[]
  onChange?: (value?: string) => void
  id?: string
  tabIndex?: number
  name?: string
  selected?: ISortByItem
  onInit?: (item?: ISortByItem) => void
  placeholder?: string
  className?: string
  search?: boolean
}

interface IOrderBy {
  value?: string
  list?: string[]
  onChange?: (value?: string) => void
}

type TOnChangeValues = { sortBy: string; orderBy: string; search: string }

type TOnChange = (values: TOnChangeValues) => any

export interface IProps {
  className?: string
  sortBy?: ISortBy
  orderBy?: IOrderBy
  search?: ISearchProps
  onChange?: TOnChange
  attached?: 'top'
  mediaType?: 'desktop' | 'tablet' | 'mobile'
}

export interface IStore {}

export interface IMethods {}

type TMethodOnChangeValues = { sortBy?: string; orderBy?: string; search?: string }

export type TMethodOnChange = (values: TMethodOnChangeValues) => any
