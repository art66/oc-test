import React, { PureComponent } from 'react'
import cn from 'classnames'

import Dropdown from '@torn/shared/components/Dropdown'
import { Item, Search } from '../components'
import styles from './index.cssmodule.scss'

import { IProps, IStore, IMethods, TMethodOnChange } from './types'

class SearchBar extends PureComponent<IProps, IStore> implements IMethods {
  static defaultProps: IProps = {
    className: '',
    sortBy: {
      value: '',
      list: [],
      onChange() {}
    },
    orderBy: {
      value: '',
      list: [],
      onChange() {}
    },
    search: {
      value: '',
      list: [],
      onChange() {}
    },
    onChange() {},
    mediaType: null,
    attached: null
  }

  _getIsDeviceMediaType() {
    const { mediaType } = this.props

    return mediaType === 'tablet' || mediaType === 'mobile'
  }

  _getMediaTypeClassName() {
    const { mediaType } = this.props

    return cn({
      [styles.tablet]: mediaType === 'tablet',
      [styles.mobile]: mediaType === 'mobile'
    })
  }

  _getAttachedClassName() {
    const { attached } = this.props

    return cn({ [styles.attachedTop]: attached === 'top' })
  }

  _getPlaceholder() {
    const {
      sortBy: { placeholder }
    } = this.props

    return placeholder || 'Sort by'
  }

  _getDropdownSettings() {
    const {
      sortBy: { id, tabIndex, list, name, selected, onInit, className, search }
    } = this.props

    return {
      className: cn('react-dropdown-default', styles.dropdown, this._getMediaTypeClassName(), className),
      list,
      onChange: this._onSortByDropdownChange,
      placeholder: this._getPlaceholder(),
      id,
      tabIndex,
      name,
      selected,
      onInit,
      search
    }
  }

  _onChange: TMethodOnChange = values => {
    const { sortBy, orderBy, search, onChange } = this.props

    const nextValues = {
      sortBy: typeof values.sortBy !== 'undefined' ? values.sortBy : sortBy.value,
      orderBy: typeof values.orderBy !== 'undefined' ? values.orderBy : orderBy.value,
      search: typeof values.search !== 'undefined' ? values.search : search.value
    }

    onChange(nextValues)
  }

  _onSortByChange = value => {
    const { sortBy, orderBy } = this.props

    const nextValues = { sortBy: undefined, orderBy: undefined }
    if (sortBy.value !== value) {
      nextValues.sortBy = value
      sortBy.onChange(nextValues.sortBy)

      const [nextOrderByValue] = orderBy.list
      if (orderBy.value !== nextOrderByValue) {
        nextValues.orderBy = nextOrderByValue

        orderBy.onChange(nextValues.orderBy)
      }
    } else {
      const currentIndex = orderBy.list.indexOf(orderBy.value)
      nextValues.orderBy = currentIndex !== orderBy.list.length - 1 ? orderBy.list[currentIndex + 1] : orderBy.list[0]

      orderBy.onChange(nextValues.orderBy)
    }

    this._onChange(nextValues)
  }

  _onSearchChange = value => {
    const { search } = this.props

    search.onChange(value)

    this._onChange({ search: value })
  }

  _onSortByDropdownChange = data => {
    const { id: value } = data

    this._onSortByChange(value)
  }

  _renderSortBy = () => {
    const { sortBy, mediaType } = this.props

    if (this._getIsDeviceMediaType()) {
      return (
        <Item mediaType={mediaType} insensitive={true}>
          <Dropdown {...this._getDropdownSettings()} />
        </Item>
      )
    }

    return sortBy.list.map(({ id, name }) => (
      <Item
        key={id}
        value={id}
        onChange={this._onSortByChange}
        active={sortBy.value === id}
        mediaType={mediaType}
        ariaLabel={`Search bar button: ${name}`}
      >
        {name}
      </Item>
    ))
  }

  render() {
    const { className, search, mediaType } = this.props

    return (
      <div className={cn(styles.searchBar, this._getMediaTypeClassName(), this._getAttachedClassName(), className)}>
        {this._renderSortBy()}
        <Item insensitive={true} mediaType={mediaType}>
          <Search {...search} onChange={this._onSearchChange} mediaType={mediaType} />
        </Item>
      </div>
    )
  }
}

export default SearchBar
