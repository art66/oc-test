# Global SearchBar Component

### The Component for sort and search.

## Schema:

### Props: Manual props injection for Component work:

- **className**. Accepts only one `string` parameter:

  --- `className: string'`. Ability to extend root component element by a css class styles.

  _Example_:
  `const CLASS_NAME = 'bazaarSearchBar'`

- **sortBy**. Accepts `object` parameter with next fields:

  --- `value: string` the current `sortBy` value;
  --- `list: object[]` an `array` of `object` with `name` and `value` properties of possible sorting by values;
  --- `onChange: function(value)'` emit a listener on `sortBy` value change with the next parameters:
  ---- `value` - changed search `value`.

  _Example_:
  `export const SORT_BY = { value: '', [ { name: 'Default', value: 'default' }, { name: 'Name', value: 'name' }, { name: 'Category', value: 'category' } ], onChange: (value) => {} }`

- **orderBy**. Accepts `object` parameter with next fields:

  --- `value: string` the current `orderBy` value;
  --- `list: string[]` an `array` of possible ordering by values;
  --- `onChange: function(value)'` emit a listener on `orderBy` value change with the next parameters:
  ---- `value` - changed search `value`.

  _Example_:
  `export const ORDER_BY = { value: 'asc', ['asc', 'desc'], onChange: (value) => {} }`

- **search**. Accepts `object` parameter with next fields:

  --- `value: string` the current `sortBy` value;
  --- `list: object[]` an `array` of `object` with `id` and `name` properties for `Autocomplete` component list of possible autocomplete values.
  --- `onChange: function(value)'` emit a listener on `search` value change with the next parameters:
  ---- `value` - changed search `value`.

  And additional props for shared `Autocomplete` component(see it specs).

  _Example_:
  `export const SEARCH = { value: 'machine gun', list: [ { id: '0', name: 'Primary weapon' }, { id: '1', name: 'Secondary weapon' }, { id: '2', name: 'Hacking tools', onChange: (value) => {} }] }`

* **onChange**. Accepts only one `function` that receives `values` parameter:

  --- `onChange: function(values)'`. Emit a listener after `onSortByChange` or `onSearchChange` emitted:

  ---- `values` - an `object` what contains actual `sortBy: string` and `search: string` values;

  _Example_:
  `const onChange = (values) => { const { sortBy, search } = values }`

* **attached**. Accepts only one `"top"` parameter:

  --- `attached: "top"`. Change component styles to be attached as a header.

  _Example_:
  `const ATTACHED = 'top'`

* **mediaType**. Accepts only one of `"tablet"` or `"mobile"` parameter:

  --- `mediaType: "tablet" or "mobile"`. Changes component styles to adapt on header.

  _Example_:
  `const MEDIA_TYPE = 'tablet'`

## How to use in App:

### So, the final Schema (_including all discussed above_) will have the next look:

```
  <SearchBar
    className={CLASS_NAME}
    sortBy={SORT_BY}
    search={SEARCH}
    onSortByChange={onSortByChange}
    onSearchChange={onSearchChange}
    onChange={onChange}
    attached={ATTACHED}
    mediaType={MEDIA_TYPE}
  />
```
