# SearchBar - Torn

## 5.0.2

- Added "hideIcon" option for hidding icon.

## 5.0.1

- Added "orderBy" logic.

## 5.0.1

- Fixed "mediaType" property.

## 5.0.0

- Changed sortBy props interface.
- Added the ability to pass all properties to the used Dropdown component.

## 4.0.0

- Changed search props interface.
- Added the ability to pass all properties to the used Autocomplete component.

## 3.0.0

- Added 'mediaType' property for layout customization.

## 2.2.1

- Added 'attached' property for layout customization.

## 2.1.1

- Added a label for Search input.

## 2.1.0

- Moved Search logic in the separate component.

## 2.0.0

- Renamed to SearchBar.

## 1.0.0

- Added dropdown layout for sortBy.
- Changed sortBy and search properties architecture.
- Removed argument event on event listeners.

## 0.3.3

- Added ability to setup a className for the container.

## 0.3.2

- Added active value.

## 0.3.1

- Created search icon presets.

## 0.3.0

- Added onSearchChange and extends onChange handlers.
- Added insensitive mode for Item component.
- Added Autocomplete component for search.

## 0.2.0

- Added onChange and onGroupChange handlers.
- Created Item component to select a group in search.
- Added default Storybook case.

## 0.1.0

- Init application.
