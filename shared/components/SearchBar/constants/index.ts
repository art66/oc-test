export const searchIconSettings = {
  iconName: 'Search',
  preset: {
    type: 'searchBar',
    subtype: 'Search'
  }
}
