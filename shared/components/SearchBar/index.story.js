import React from 'react'
import { select } from '@storybook/addon-knobs'
import { State, Store } from '@sambego/storybook-state'

import SearchBar from './index'
import { DEFAULT_STATE, MEDIA_TYPE_OPTIONS, ATTACHED_OPTIONS } from './mocks'

export const Defaults = () => {
  const config = {
    ...DEFAULT_STATE,
    mediaType: select('Media type:', MEDIA_TYPE_OPTIONS, MEDIA_TYPE_OPTIONS[0], 'options'),
    attached: select('Attached to:', ATTACHED_OPTIONS, ATTACHED_OPTIONS[0], 'options')
  }

  const STORE_CONFIG = {
    sortBy: {
      ...DEFAULT_STATE.sortBy,
      value: 'default',
      onChange: (value) => {
        console.log(`sortBy change: ${value}`)

        store.set({
          sortBy: { ...STORE_CONFIG.sortBy, value }
        })
      }
    },
    orderBy: {
      ...DEFAULT_STATE.orderBy,
      value: 'asc',
      onChange: (value) => {
        console.log(`orderBy change: ${value}`)

        store.set({
          orderBy: { ...STORE_CONFIG.orderBy, value }
        })
      }
    },
    search: {
      ...DEFAULT_STATE.search,
      value: '',
      onChange: (value) => {
        console.log(`search change : ${value}`)

        store.set({
          search: { ...STORE_CONFIG.search, value }
        })
      }
    },
    onChange: (values) => {
      console.log('change:', values)

      store.set({
        sortBy: { ...STORE_CONFIG.sortBy, value: values.sortBy },
        orderBy: { ...STORE_CONFIG.orderBy, value: values.orderBy },
        search: { ...STORE_CONFIG.search, value: values.search }
      })
    }
  }

  const store = new Store(STORE_CONFIG)

  return (
    <State store={store}>
      <SearchBar {...config} />
    </State>
  )
}

Defaults.story = {
  name: 'defaults'
}

export const WithotIcon = () => {
  const config = {
    ...DEFAULT_STATE,
    mediaType: select('Media type:', MEDIA_TYPE_OPTIONS, MEDIA_TYPE_OPTIONS[0], 'options'),
    attached: select('Attached to:', ATTACHED_OPTIONS, ATTACHED_OPTIONS[0], 'options')
  }

  const STORE_CONFIG = {
    sortBy: {
      ...DEFAULT_STATE.sortBy,
      value: 'default',
      onChange: (value) => {
        console.log(`sortBy change: ${value}`)

        store.set({
          sortBy: { ...STORE_CONFIG.sortBy, value }
        })
      }
    },
    orderBy: {
      ...DEFAULT_STATE.orderBy,
      value: 'asc',
      onChange: (value) => {
        console.log(`orderBy change: ${value}`)

        store.set({
          orderBy: { ...STORE_CONFIG.orderBy, value }
        })
      }
    },
    search: {
      ...DEFAULT_STATE.search,
      value: '',
      onChange: (value) => {
        console.log(`search change : ${value}`)

        store.set({
          search: { ...STORE_CONFIG.search, value }
        })
      },
      hideIcon: true
    },
    onChange: (values) => {
      console.log('change:', values)

      store.set({
        sortBy: { ...STORE_CONFIG.sortBy, value: values.sortBy },
        orderBy: { ...STORE_CONFIG.orderBy, value: values.orderBy },
        search: { ...STORE_CONFIG.search, value: values.search }
      })
    }
  }

  const store = new Store(STORE_CONFIG)

  return (
    <State store={store}>
      <SearchBar {...config} />
    </State>
  )
}

WithotIcon.story = {
  name: 'withot icon'
}

export default {
  title: 'Shared/SearchBar'
}
