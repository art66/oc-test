import React from 'react'
import { mount } from 'enzyme'

import Autocomplete from '@torn/shared/components/Autocomplete'
import { SVGIconGenerator } from '@torn/shared/SVG'
import Search from '../'
import {
  initialState,
  tabletState,
  mobileState,
  idState,
  extraClassNamesState,
  placeholderState,
  extraClassNamesMobileState,
  hiddenIconState
} from './mocks'

describe('<Search />', () => {
  it('should render with default props', () => {
    const Component = mount<Search>(<Search />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input')
    expect(AutocompleteComponent.prop('list')).toEqual([])
    expect(AutocompleteComponent.prop('value')).toEqual('')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer')
    expect(LabelComponent.prop('htmlFor')).toBe('searchBarSearchInput')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render', () => {
    const Component = mount<Search>(<Search {...initialState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer')
    expect(LabelComponent.prop('htmlFor')).toBe('searchBarSearchInput')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render tablet media type', () => {
    const Component = mount<Search>(<Search {...tabletState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete tablet')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input tablet')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer tablet')
    expect(LabelComponent.prop('htmlFor')).toBe('searchBarSearchInput')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render mobile media type', () => {
    const Component = mount<Search>(<Search {...mobileState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete mobile')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input mobile')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer mobile')
    expect(LabelComponent.prop('htmlFor')).toBe('searchBarSearchInput')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render with user defined id', () => {
    const Component = mount<Search>(<Search {...idState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('testId')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer')
    expect(LabelComponent.prop('htmlFor')).toBe('testId')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render with extra class names', () => {
    const Component = mount<Search>(<Search {...extraClassNamesState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete testClassName')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input testInputClassName')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer')
    expect(LabelComponent.prop('htmlFor')).toBe('searchBarSearchInput')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render with extra class names in mobile media type', () => {
    const Component = mount<Search>(<Search {...extraClassNamesMobileState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete mobile testClassName')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input mobile testInputClassName')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer mobile')
    expect(LabelComponent.prop('htmlFor')).toBe('searchBarSearchInput')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render with user defined placeholder', () => {
    const Component = mount<Search>(<Search {...placeholderState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('Test placeholder...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(1)
    expect(LabelComponent.prop('className')).toBe('iconContainer')
    expect(LabelComponent.prop('htmlFor')).toBe('searchBarSearchInput')
    expect(LabelComponent.find(SVGIconGenerator).length).toBe(1)

    expect(Component).toMatchSnapshot()
  })

  it('should render without icon', () => {
    const Component = mount<Search>(<Search {...hiddenIconState} />)

    expect(Component.find('.search').length).toBe(1)
    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)
    expect(AutocompleteComponent.prop('id')).toBe('searchBarSearchInput')
    expect(AutocompleteComponent.prop('className')).toBe('autocomplete hiddenIcon')
    expect(AutocompleteComponent.prop('inputClassName')).toBe('input hiddenIcon')
    expect(AutocompleteComponent.prop('value')).toEqual('test search string')
    expect(AutocompleteComponent.prop('placeholder')).toBe('search...')
    expect(AutocompleteComponent.prop('tabIndex')).toBe('5')
    expect(AutocompleteComponent.prop('name')).toBe('test name')
    expect(AutocompleteComponent.prop('autoFocus')).toBe(true)
    expect(AutocompleteComponent.prop('selected')).toEqual({ id: '0', name: 'test1' })
    expect(AutocompleteComponent.prop('targets')).toBe('test targets')
    expect(AutocompleteComponent.prop('children')).toBe('test children')
    expect(AutocompleteComponent.prop('noItemsFoundMessage')).toBe('test no items message')
    expect(AutocompleteComponent.prop('onRef')).toBe(initialState.onRef)
    expect(AutocompleteComponent.prop('onInit')).toBe(initialState.onInit)
    expect(AutocompleteComponent.prop('onFocus')).toBe(initialState.onFocus)
    expect(AutocompleteComponent.prop('onBlur')).toBe(initialState.onBlur)
    expect(AutocompleteComponent.prop('list')).toEqual([
      { id: '0', name: 'test1' },
      { id: '1', name: 'test2' },
      { id: '2', name: 'test3' }
    ])
    const LabelComponent = Component.find('.search').find('label')
    expect(LabelComponent.length).toBe(0)

    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener on Autocomplete component input', () => {
    const onChange = jest.fn()
    const onInput = jest.fn()
    const value = 'test1'
    const state = { value, onChange, onInput }
    const Component = mount<Search>(<Search {...state} />)
    const componentInstance = Component.instance()

    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)

    expect(AutocompleteComponent.prop('onInput')).toEqual(componentInstance._onInput)
    componentInstance._onInput({ target: { value: 'test2' } })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toBe('test2')
    expect(onInput).toHaveBeenCalledTimes(1)
    expect(onInput.mock.calls[0][0]).toEqual({ target: { value: 'test2' } })

    Component.setProps({ onInput: null })

    componentInstance._onInput({ target: { value: 'test2' } })
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange.mock.calls[1][0]).toBe('test2')
    expect(onInput).toHaveBeenCalledTimes(1)

    expect(onChange).toMatchSnapshot()
    expect(onInput).toMatchSnapshot()
  })

  it('should call onChange listener on Autocomplete component select', () => {
    const onChange = jest.fn()
    const onSelect = jest.fn()
    const value = 'test1'
    const state = { value, onChange, onSelect }
    const Component = mount<Search>(<Search {...state} />)
    const componentInstance = Component.instance()

    const AutocompleteComponent = Component.find('.search').find(Autocomplete)
    expect(AutocompleteComponent.length).toBe(1)

    expect(AutocompleteComponent.prop('onSelect')).toEqual(componentInstance._onSelect)
    componentInstance._onSelect({ name: 'test1' })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toBe('test1')
    expect(onSelect).toHaveBeenCalledTimes(1)
    expect(onSelect.mock.calls[0][0]).toEqual({ name: 'test1' })

    Component.setProps({ onSelect: null })

    componentInstance._onSelect({ name: 'test1' })
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange.mock.calls[1][0]).toBe('test1')
    expect(onSelect).toHaveBeenCalledTimes(1)

    expect(onChange).toMatchSnapshot()
    expect(onSelect).toMatchSnapshot()
  })

  it('should call onIconClick listener on icon click', () => {
    const onIconClick = jest.fn()
    const state = { onIconClick }
    const Component = mount<Search>(<Search {...state} />)

    const IconContainerComponent = Component.find('label.iconContainer')
    expect(IconContainerComponent.length).toBe(1)

    IconContainerComponent.simulate('click')
    expect(onIconClick).toHaveBeenCalledTimes(1)
  })
})
