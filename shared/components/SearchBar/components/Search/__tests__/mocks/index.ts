import { IProps } from '../../types'

export const initialState: IProps = {
  value: 'test search string',
  list: [{ id: '0', name: 'test1' }, { id: '1', name: 'test2' }, { id: '2', name: 'test3' }],
  tabIndex: '5',
  name: 'test name',
  autoFocus: true,
  selected: { id: '0', name: 'test1' },
  targets: 'test targets',
  children: 'test children',
  noItemsFoundMessage: 'test no items message',
  onRef: () => {},
  onInit: () => {},
  onChange: () => {},
  onFocus: () => {},
  onBlur: () => {}
}

export const tabletState: IProps = {
  ...initialState,
  mediaType: 'tablet'
}

export const mobileState: IProps = {
  ...initialState,
  mediaType: 'mobile'
}

export const idState: IProps = {
  ...initialState,
  id: 'testId'
}

export const extraClassNamesState: IProps = {
  ...initialState,
  className: 'testClassName',
  inputClassName: 'testInputClassName'
}

export const extraClassNamesMobileState: IProps = {
  ...mobileState,
  ...extraClassNamesState
}

export const placeholderState: IProps = {
  ...initialState,
  placeholder: 'Test placeholder...'
}

export const hiddenIconState: IProps = {
  ...initialState,
  hideIcon: true
}
