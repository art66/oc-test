import { createSelector } from 'reselect'
import cn from 'classnames'
import styles from './index.cssmodule.scss'

export const getMediaTypeClassName = createSelector(
  [mediaType => mediaType],
  (mediaType: 'desktop' | 'tablet' | 'mobile'): string => {
    switch (mediaType) {
      case 'tablet': {
        return styles.tablet
      }
      case 'mobile': {
        return styles.mobile
      }
      default: {
        return null
      }
    }
  }
)

export const getClassName = createSelector(
  [getMediaTypeClassName],
  (mediaTypeClassName: string): string => {
    return cn(styles.search, mediaTypeClassName)
  }
)
