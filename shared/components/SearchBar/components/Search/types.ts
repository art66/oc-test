export interface IAutocompleteItem {
  id: string
  name: string
}

export interface IProps {
  mediaType?: 'desktop' | 'tablet' | 'mobile'
  id?: string
  tabIndex?: string
  list: IAutocompleteItem[]
  name?: string
  inputClassName?: string
  autoFocus?: boolean
  selected?: object
  onChange?: (value?: string) => void
  onInit?: (args?: any) => void
  // TODO: Currently this callback is not running, so args unknown
  onInput?: (event: React.ChangeEvent<HTMLInputElement>) => void
  onSelect?: (item?: IAutocompleteItem) => void
  value?: string
  placeholder?: string
  className?: string
  targets?: string
  children?: any
  noItemsFoundMessage?: string
  hideDropdown?: boolean
  hideIcon?: boolean
  onRef?: (args?: any) => void
  // FIXME: Should be React component type
  onFocus?: (event: React.FocusEvent<any>) => void
  onBlur?: (event: React.FocusEvent<any>) => void
  onIconClick?: (event: React.MouseEvent<HTMLLabelElement>) => void
}
