import React from 'react'
import cn from 'classnames'
import Autocomplete from '@torn/shared/components/Autocomplete'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { searchIconSettings } from '../../constants'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import { getMediaTypeClassName, getClassName } from './selectors'
import { ARIAL_LABEL_TEXT } from './constants'

export class Search extends React.PureComponent<IProps> {
  static defaultProps = {
    id: 'searchBarSearchInput',
    value: '',
    list: [],
    onChange: () => {},
    placeholder: 'search...',
    mediaType: null
  }

  _getMediaTypeClassName() {
    const { mediaType } = this.props

    return getMediaTypeClassName(mediaType)
  }

  _getClassName() {
    const { mediaType } = this.props

    return getClassName(mediaType)
  }

  _getAutocompleteSettings() {
    const {
      id,
      value,
      list,
      placeholder,
      className,
      inputClassName,
      tabIndex,
      name,
      autoFocus,
      selected,
      targets,
      children,
      noItemsFoundMessage,
      hideDropdown,
      hideIcon,
      onRef,
      onInit,
      onFocus,
      onBlur
    } = this.props

    return {
      id,
      className: cn(styles.autocomplete, this._getMediaTypeClassName(), className, {
        [styles.hiddenIcon]: hideIcon
      }),
      inputClassName: cn(styles.input, this._getMediaTypeClassName(), inputClassName, {
        [styles.hiddenIcon]: hideIcon
      }),
      list,
      onInput: this._onInput,
      onSelect: this._onSelect,
      value,
      placeholder,
      tabIndex,
      name,
      autoFocus,
      selected,
      targets,
      children,
      noItemsFoundMessage,
      hideDropdown,
      onRef,
      onInit,
      onFocus,
      onBlur,
      ariaLabel: ARIAL_LABEL_TEXT
    }
  }

  _onInput = event => {
    const {
      target: { value }
    } = event
    const { onChange, onInput } = this.props

    if (onInput) {
      onInput(event)
    }

    onChange(value)
  }

  _onSelect = item => {
    const { name } = item
    const { onChange, onSelect } = this.props

    if (onSelect) {
      onSelect(item)
    }

    onChange(name)
  }

  _renerIcon() {
    const { id, onIconClick, hideIcon } = this.props

    if (hideIcon) {
      return null
    }

    return (
      <label htmlFor={id} className={cn(styles.iconContainer, this._getMediaTypeClassName())} onClick={onIconClick}>
        <SVGIconGenerator {...searchIconSettings} />
      </label>
    )
  }

  render() {
    return (
      <div className={this._getClassName()}>
        <Autocomplete {...this._getAutocompleteSettings()} />
        {this._renerIcon()}
      </div>
    )
  }
}

export default Search
