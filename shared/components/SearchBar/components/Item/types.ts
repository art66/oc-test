type TValue = string

export interface IProps {
  className?: string
  children?: React.ReactChild[] | React.ReactChild | string
  value?: TValue
  onChange?: (value?: TValue) => void
  insensitive?: boolean
  mediaType?: 'desktop' | 'tablet' | 'mobile'
  active?: boolean
  ariaLabel?: string
}
