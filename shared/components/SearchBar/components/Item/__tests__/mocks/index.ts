import { IProps } from '../../types'

export const defaultState: IProps = {
  className: 'testClassName',
  value: 'group1',
  insensitive: true,
  active: true,
  mediaType: null
}

export const tabletState: IProps = {
  ...defaultState,
  mediaType: 'tablet'
}

export const mobileState: IProps = {
  ...defaultState,
  mediaType: 'mobile'
}
