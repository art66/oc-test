import React from 'react'
import { mount } from 'enzyme'

import Item from '../'
import { defaultState, tabletState, mobileState } from './mocks'

describe('<Item />', () => {
  it('should render with default props', () => {
    const Children = () => <div className='unique' />
    const Component = mount<Item>(
      <Item>
        <Children />
      </Item>
    )

    expect(Component.find('.item').length).toBe(1)
    expect(Component.find('.item').find(Children).length).toBe(1)
    expect(Component.find('.item').prop('className')).toBe('item')

    expect(Component).toMatchSnapshot()
  })

  it('should render', () => {
    const Children = () => <div className='unique' />
    const Component = mount<Item>(
      <Item {...defaultState}>
        <Children />
      </Item>
    )

    expect(Component.find('.item').length).toBe(1)
    expect(Component.find('.item').find(Children).length).toBe(1)
    expect(Component.find('.item').prop('className')).toBe('item insensitive active testClassName')

    expect(Component).toMatchSnapshot()
  })

  it('should render tablet media type', () => {
    const Children = () => <div className='unique' />
    const Component = mount<Item>(
      <Item {...tabletState}>
        <Children />
      </Item>
    )

    expect(Component.find('.item').length).toBe(1)
    expect(Component.find('.item').find(Children).length).toBe(1)
    expect(Component.find('.item').prop('className')).toBe('item tablet insensitive active testClassName')

    Component.setProps(defaultState)

    expect(Component.find('.item').prop('className')).toBe('item insensitive active testClassName')

    expect(Component).toMatchSnapshot()
  })

  it('should render mobile media type', () => {
    const Children = () => <div className='unique' />
    const Component = mount<Item>(
      <Item {...mobileState}>
        <Children />
      </Item>
    )

    expect(Component.find('.item').length).toBe(1)
    expect(Component.find('.item').find(Children).length).toBe(1)
    expect(Component.find('.item').prop('className')).toBe('item mobile insensitive active testClassName')

    Component.setProps(defaultState)

    expect(Component.find('.item').prop('className')).toBe('item insensitive active testClassName')

    expect(Component).toMatchSnapshot()
  })

  it('should call onChange listener', () => {
    const onChange = jest.fn()
    const event = {}
    const value = 'test_value'
    const state = { value, onChange }
    const Children = () => <div className='unique' />
    const Component = mount<Item>(
      <Item {...state}>
        <Children />
      </Item>
    )

    expect(Component.find('.item').length).toBe(1)

    Component.find('.item').simulate('click', event, value)

    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toBe('test_value')

    expect(onChange).toMatchSnapshot()
  })
})
