import React from 'react'
import cn from 'classnames'

import styles from './index.cssmodule.scss'
import { IProps } from './types'

export class Item extends React.PureComponent<IProps> {
  static defaultProps = {
    value: '',
    onChange: () => {},
    active: false,
    mediaType: null,
    insensitive: false
  }

  _getMediaTypeClassName() {
    const { mediaType } = this.props

    return cn({
      [styles.tablet]: mediaType === 'tablet',
      [styles.mobile]: mediaType === 'mobile'
    })
  }

  _onClick = () => {
    const { value, onChange, insensitive } = this.props

    if (insensitive) {
      return
    }

    onChange(value)
  }

  render() {
    const { className: classNameExtends, children, insensitive, active, ariaLabel } = this.props

    const className = cn(
      styles.item,
      this._getMediaTypeClassName(),
      {
        [styles.insensitive]: insensitive,
        [styles.active]: active
      },
      classNameExtends
    )

    const ItemTag = !insensitive ? 'button' : 'div'

    return (
      <ItemTag className={className} onClick={this._onClick} aria-label={ariaLabel}>
        {children}
      </ItemTag>
    )
  }
}

export default Item
