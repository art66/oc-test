import InputStringChecker from '..'

describe('InputStringChecker()', () => {
  it('should restrict string from input more than 20 symbols and provide us with corresponding flag', () => {
    const { checkString } = new InputStringChecker({ maxLength: 20 })
    const { isMaxLengthExceeded, value } = checkString({ text: 'hello' })

    expect(value.length).toBe(5)
    expect(isMaxLengthExceeded).toBeFalsy()

    const { isMaxLengthExceeded: almostExceeded, value: valueBeforeMax } = checkString({ text: 'helloWorld' })

    expect(valueBeforeMax.length).toBe(10)
    expect(almostExceeded).toBeFalsy()

    const { isMaxLengthExceeded: exceeded, value: valueMax } = checkString({ text: 'helloWorldLovelyFunny' })

    expect(valueMax.length).toBe(20)
    expect(exceeded).toBeTruthy()
  })
  it('should break typing on space input with its own flag as well', () => {
    const { checkString } = new InputStringChecker({ maxLength: 10, removeSpaces: true })

    const { value, isSpaceInputted } = checkString({ text: 'hello ' })

    expect(value).toBe('hello')
    expect(isSpaceInputted).toBeTruthy()
  })
  it('should return flag "isOnlySpacesInputted: true" if the only spaces inputted', () => {
    const { checkString } = new InputStringChecker({ maxLength: 10 })

    const { value, isOnlySpacesInputted } = checkString({ text: '     ' })

    expect(value).toBe('     ')
    expect(isOnlySpacesInputted).toBeTruthy()
  })
  it('should return flag "isRestrictedSymbolFounded: true" if some of the restricted symbols found', () => {
    const { checkString } = new InputStringChecker({
      maxLength: 10,
      restrictedSymbols: '/',
      restrictedSymbolsToReplace: '/',
      restrictedSymbolsReplaceFlags: 'gi'
    })

    const { value, isRestrictedSymbolFounded } = checkString({ text: 'hell0/' })

    expect(value).toBe('hell0')
    expect(isRestrictedSymbolFounded).toBeTruthy()
  })
  it('should return flag "_isSymbolAllowed: false" if some of the non-allowed symbols found', () => {
    const { checkString } = new InputStringChecker({
      maxLength: 10,
      allowedSymbols: '^[0-9]+?$',
      allowedSymbolsToReplace: '[^0-9]',
      allowedSymbolsReplaceFlags: 'gi'
    })

    const { value, isSymbolAllowed } = checkString({ text: '0/' })

    expect(value).toBe('0')
    expect(isSymbolAllowed).toBeFalsy()
  })
})
