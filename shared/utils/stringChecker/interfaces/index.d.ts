export interface IInputStringChecker {
  text?: string
  maxLength?: number
  restrictedSymbols?: string
  restrictedFlags?: string
  restrictedSymbolsToReplace?: string
  restrictedSymbolsReplaceFlags?: string
  allowedSymbols?: string
  allowedFlags?: string
  allowedSymbolsToReplace?: string
  allowedSymbolsReplaceFlags?: string
  removeSpaces?: boolean
}

export interface IReturn {
  value: string
  isSpaceInputted: boolean
  isMaxLengthExceeded: boolean
  isSymbolAllowed: boolean
  isRestrictedSymbolFounded: boolean
  isOnlySpacesInputted: boolean
}
