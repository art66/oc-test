import { IInputStringChecker, IReturn } from './interfaces'

/**
 *  @name InputStringChecker
 *  @author 3p-sviat
 *  @version 1.1.1
 *  @description Provides strong functionality to check/cut/restrict string on user inputs.
 *
 *  @param {object} config - configuration payload for the string processing
 *  @param {string} config.text - main string to check
 *  @param {number} config.maxLength - string length restriction rule
 *  @param {string} config.restrictedSymbols - some string with enumerated restricted symbols
 *  @param {boolean} config.removeSpaces - a boolean flag for removing spaces around string
 *
 *  @property {_text} string - main string value.
 *  @property {_maxLength} number - string mas length flag if it exceeded.
 *  @property {_restrictedSymbols} string - string with symbols provided.
 *  @property {_isSpaceInputted} boolean - if space were input you will get a flag about that.
 *  @property {_isOnlySpacesInputted} boolean - returns true if all the string consist of spaces only.
 *  @property {_isRemoveEmptySpaces} boolean - recognize if developer need string without spaces in it.
 *  @property {_isRestrictedSymbolFounded} boolean - returns true if some of the restricted symbols were found.
 *  @property {_isMaxLengthExceeded} boolean - returns true if max provided string length were exceeded.
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
class InputStringChecker {
  private _state: {
    text: string
    maxLength: number
    allowedSymbols: string
    allowedFlags: string
    allowedSymbolsToReplace: string
    allowedSymbolsReplaceFlags: string
    restrictedSymbols: string
    restrictedFlags: string
    restrictedSymbolsToReplace: string
    restrictedSymbolsReplaceFlags: string
    isSpaceInputted: boolean
    isOnlySpacesInputted: boolean
    isRemoveEmptySpaces: boolean
    isSymbolAllowed: boolean
    isRestrictedSymbolFounded: boolean
    isMaxLengthExceeded: boolean
  }

  constructor(props: IInputStringChecker) {
    this._state = {
      text: '',
      maxLength: props.maxLength || 100,
      allowedSymbols: props.allowedSymbols || '',
      allowedFlags: props.allowedFlags || 'gi',
      allowedSymbolsToReplace: props.allowedSymbolsToReplace || '',
      allowedSymbolsReplaceFlags: props.allowedSymbolsReplaceFlags || 'i',
      restrictedSymbols: props.restrictedSymbols || '',
      restrictedFlags: props.restrictedFlags || 'gi',
      restrictedSymbolsToReplace: props.restrictedSymbolsToReplace || '',
      restrictedSymbolsReplaceFlags: props.restrictedSymbolsReplaceFlags || 'i',
      isSpaceInputted: false,
      isOnlySpacesInputted: false,
      isRemoveEmptySpaces: props.removeSpaces || false,
      isSymbolAllowed: true,
      isRestrictedSymbolFounded: false,
      isMaxLengthExceeded: false
    }
  }

  _setText = (text: string) => {
    this._state.text = text
  }

  _checkMaxLength = () => {
    const isMaxLengthExceeded = this._state.text && this._state.text.length >= this._state.maxLength

    this._state.isMaxLengthExceeded = !!isMaxLengthExceeded

    if (!isMaxLengthExceeded) {
      return
    }

    this._state.text = this._state.text.substr(0, this._state.maxLength)
  }

  _checkOnEmptySpaces = () => {
    this._state.isOnlySpacesInputted = /^\s+$/.test(this._state.text)

    this._removeEmptySpaces()
  }

  _literalRegExpCreator = ({ symbols, flags }) => new RegExp(symbols, flags)

  _symbolsCheckerRegExp = ({ symbols, flags }) => {
    const matchPattern = this._literalRegExpCreator({ symbols, flags })

    return !this._state.text || this._state.text.match(matchPattern)
  }

  _allowOnlyProvidedSymbols = () => {
    const isNoAllowedSymbolsProvided = !this._state.allowedSymbols || this._state.allowedSymbols.length === 0

    if (isNoAllowedSymbolsProvided) {
      return
    }

    const isSymbolAllowed = !!this._symbolsCheckerRegExp({
      symbols: this._state.allowedSymbols,
      flags: this._state.allowedFlags
    })

    this._state.isSymbolAllowed = isSymbolAllowed

    if (isSymbolAllowed) {
      return
    }

    const replacePattern = this._literalRegExpCreator({
      symbols: this._state.allowedSymbolsToReplace,
      flags: this._state.allowedSymbolsReplaceFlags
    })

    const normalizedStr = this._state.text.replace(replacePattern, '')

    this._setText(normalizedStr)
  }

  // there is two independent method because they can be easily expanded in future!!
  _preventRestrictedSymbols = () => {
    const isNoRestrictSymbolsProvided = !this._state.restrictedSymbols || this._state.restrictedSymbols.length === 0

    if (isNoRestrictSymbolsProvided) {
      return
    }

    const isRestrictedSymbolsFound = !!this._symbolsCheckerRegExp({
      symbols: this._state.restrictedSymbols,
      flags: this._state.restrictedFlags
    })

    this._state.isRestrictedSymbolFounded = isRestrictedSymbolsFound

    if (!isRestrictedSymbolsFound) {
      return
    }

    const replacePattern = this._literalRegExpCreator({
      symbols: this._state.restrictedSymbolsToReplace,
      flags: this._state.restrictedSymbolsReplaceFlags
    })

    const normalizedStr = this._state.text.replace(replacePattern, '')

    this._setText(normalizedStr)
  }

  _removeEmptySpaces = () => {
    if (!this._state.isRemoveEmptySpaces) {
      return
    }

    this._state.isSpaceInputted = !!(this._state.text[this._state.text.length - 1] === ' ')
    this._state.text = this._state.text.replace(/\s+/g, '')
  }

  _getProcessedString = (): IReturn => ({
    value: this._state.text,
    isSpaceInputted: this._state.isSpaceInputted,
    isMaxLengthExceeded: this._state.isMaxLengthExceeded,
    isSymbolAllowed: this._state.isSymbolAllowed,
    isRestrictedSymbolFounded: this._state.isRestrictedSymbolFounded,
    isOnlySpacesInputted: this._state.isOnlySpacesInputted
  })

  /**
   * Returns processed string value along with all its flags: spaces, length, restrict.
   * @name InputStringChecker#checkString
   * @function
   *
   * @readonly
   */
  checkString = ({ text }): IReturn => {
    this._setText(text)

    this._checkMaxLength()
    this._checkOnEmptySpaces()
    this._allowOnlyProvidedSymbols()
    this._preventRestrictedSymbols()

    return this._getProcessedString()
  }
}

export default InputStringChecker
