# Shared/utils/stringChecker - Torn


## 1.1.3
 * Fixed empty string checking regExp.

## 1.1.2
 * Minor state improvements due to the ESLint recommendations.

## 1.1.1
 * Added several additional flag to expand regexp abilities.

## 1.1.0
 * Added tests and interfaces.

## 1.0.0
 * Init commit.
