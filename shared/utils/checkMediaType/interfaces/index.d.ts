export type TMediaType = 'desktop' | 'tablet' | 'mobile'
export interface IReturn {
  isDesktop: boolean
  isTablet: boolean
  isMobile: boolean
}
