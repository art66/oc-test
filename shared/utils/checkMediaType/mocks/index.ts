export const desktop = 'desktop'

export const tablet = 'tablet'

export const mobile = 'mobile'

export const isMobile = {
  isMobile: true,
  isTablet: false,
  isDesktop: false
}

export const isTablet = {
  isMobile: false,
  isTablet: true,
  isDesktop: false
}

export const isDesktop = {
  isMobile: false,
  isTablet: false,
  isDesktop: true
}

export const isManualDesktopMode = true
