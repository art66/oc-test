import checkMediaType from '../'
import { desktop, tablet, mobile, isMobile, isTablet, isDesktop, isManualDesktopMode } from '../mocks'

describe('checkMediaType()', () => {
  it('should return "mobile" flag true', () => {
    expect(checkMediaType(mobile)).toMatchObject(isMobile)
  })
  it('should return "tablet" flag', () => {
    expect(checkMediaType(tablet)).toMatchObject(isTablet)
  })
  it('should return "desktop" flag', () => {
    expect(checkMediaType(desktop)).toMatchObject(isDesktop)
  })
  it('should return "desktop" in case of manualDesktopMode enabled flag', () => {
    expect(checkMediaType(desktop, isManualDesktopMode)).toMatchObject(isDesktop)
  })
})
