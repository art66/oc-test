import { POSSIBLE_LAYOUTS } from './constants'
import { TMediaType, IReturn } from './interfaces'

/**
 *  @name checkMediaType
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description easily get back boolean flags about current responsive state,
 *  @description previously calculated via react-responsive.
 *
 *  @typedef TProps
 *
 *  @params {string} mediaType - current media query calculated by react-responsive.
 *  @params {string} isManualDesktop - an extra flag allows us get current manual mode flag.
 *
 *  @returns {object} currentFlags - an object with three main breakpoints consisted of boolean values.
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

const checkMediaType = (mediaType: TMediaType, isManualDesktop?: boolean): IReturn => {
  return {
    isDesktop: isManualDesktop || mediaType === POSSIBLE_LAYOUTS[0],
    isTablet: mediaType === POSSIBLE_LAYOUTS[1] && !isManualDesktop,
    isMobile: mediaType === POSSIBLE_LAYOUTS[2] && !isManualDesktop
  }
}

export default checkMediaType
