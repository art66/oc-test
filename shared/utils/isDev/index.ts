/**
*  @name isDev
*  @author 3p-sviat
*  @version 1.0.0
*  @description suitable way for checking current project's domain
*
*  @readonly
*/
const isDev = () => {
  const { host } = window.location

  return host.match(/^(dev-www)/i)
}

export default isDev
