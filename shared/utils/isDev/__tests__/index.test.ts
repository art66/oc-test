import isDev from '..'

describe('isDev()', () => {
  it('should return false', () => {
    delete window.location

    window.location = {
      origin: 'http://torn.com',
      host: 'torn.com'
    } as any

    expect(isDev()).toBeFalsy()
  })
  it('should return true', () => {
    delete window.location

    window.location = {
      origin: 'http://dev-www.torn.com',
      host: 'dev-www.torn.com'
    } as any

    expect(isDev()).toBeTruthy()
  })
})
