import Observable from '../observer'

import checkDarkMode from './helpers/checkDarkMode'
import { IMutation, IMutationConfig, IDOMObserverReturn } from './interfaces'

/**
*  @name darkModeChecker
*  @author 3p-sviat
*  @version 1.0.0
*  @description subscribes on DOM Node class updates (with/without .dark-mode).
*
*  @param {function} callback - some function to trigger on updates on demand
*
*  @property {object} targetNode - DOM "body" node that need to be watched on any class changes
*  @property {object} mutationsConfig - configuration object with props for darkModeChecker
*
*  @return {function} callback - with updated value
*
*  @copyright Copyright (c) Torn, LTD.
*/

class DOMObserver extends Observable {
  private _targetNode: HTMLElement
  private _mutationsConfig: IMutationConfig

  constructor() {
    super()

    this._targetNode = document.body

    this._mutationsConfig = {
      attributes: true,
      attributeFilter: ['class'],
      childList: false,
      subtree: false
    }
  }

  _classListMutationCheck = (mutationsList: IMutation[]) => {
    mutationsList.forEach((mutation: IMutation) => {
      if (mutation.type !== 'attributes') return

      this.notify(checkDarkMode(this._targetNode))
    })
  }

  _connectSubscriber = (unit: Function) => {
    if (typeof unit !== 'function') return

    this.subscribe(unit)
  }

  _checkInitRun = () => {
    this.notify(checkDarkMode(this._targetNode))
  }

  _startSubscription = (observer: MutationObserver) => (unit: Function) => {
    this._connectSubscriber(unit)

    this._checkInitRun()

    observer.observe(this._targetNode, this._mutationsConfig)
  }

  _finishSubscription = (observer: MutationObserver) => () => {
    this.unsubscribeAll()

    observer.disconnect()
  }

  run = (): IDOMObserverReturn => {
    const observer = new MutationObserver(this._classListMutationCheck)

    return {
      subscribeOnDarkMode: this._startSubscription(observer),
      unsubscribeFromDarkMode: this._finishSubscription(observer)
    }
  }
}

const { run } = new DOMObserver()
const { subscribeOnDarkMode, unsubscribeFromDarkMode } = run()

export {
  subscribeOnDarkMode,
  unsubscribeFromDarkMode
}
