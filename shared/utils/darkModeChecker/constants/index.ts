export const LIGHT_MODE = false
export const DARK_MODE = true
export const DARK_CLASS = 'dark-mode'
