const notifyConsole = () => {
  console.log(
    '%cDARK MODE ACTIVE',
    'background: #222; color: #fff'
  )
}

export default notifyConsole
