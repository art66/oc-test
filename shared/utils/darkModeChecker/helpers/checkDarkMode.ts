import { DARK_CLASS, DARK_MODE, LIGHT_MODE } from '../constants'
import notifyConsole from './notifyConsole'

const checkDarkMode = (targetNode: HTMLElement): boolean => {
  if (!targetNode.classList.contains(DARK_CLASS)) {
    return LIGHT_MODE
  }

  // @ts-ignore
  if (__DEV__) {
    notifyConsole()
  }

  return DARK_MODE
}

export default checkDarkMode
