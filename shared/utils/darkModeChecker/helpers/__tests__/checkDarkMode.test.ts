import checkDarkMode from '../checkDarkMode'
import { DARK_CLASS } from '../../constants'

describe('checkDarkMode()', () => {
  Object.defineProperty(document.body, 'classList', {
    writable: true
  })

  // @ts-ignore
  document.body.classList = {}

  // mocking the real contains method to be tests here
  document.body.classList.contains = (str: string) => {
    const { className } = document.body
    const currentClasses = className.split(' ')

    return currentClasses.some((strClass: string) => strClass === str)
  }

  it('should return "false" flag in case of .dark-mode class missing inside body classList', () => {
    // @ts-ignore
    document.body.className = ' '

    expect(checkDarkMode(document.body)).toBeFalsy()
  })
  it('should return "true" flag in case .dark-mode class is present inside body classList', () => {
    // @ts-ignore
    document.body.className = DARK_CLASS

    expect(checkDarkMode(document.body)).toBeTruthy()
  })
})
