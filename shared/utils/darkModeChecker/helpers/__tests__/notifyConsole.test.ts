import notifyConsole from '../notifyConsole'

describe('notifyConsole()', () => {
  it('should return "DARK MODE ACTIVE" flag in case the dark mode active', () => {
    const consoleLogSpyOn = jest.spyOn(console, 'log')

    notifyConsole()

    expect(consoleLogSpyOn).toBeCalled()
  })
})
