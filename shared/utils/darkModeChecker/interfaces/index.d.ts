export interface IMutationConfig {
  attributes: boolean,
  attributeFilter: string[],
  childList: boolean,
  subtree: boolean
}

export interface IConfigHolderReturn {
  targetNode: HTMLElement
  mutationsConfig: IMutationConfig
}

export interface IMutation {
  type: string
}

export interface IDOMObserverReturn {
  subscribeOnDarkMode: (callback: Function) => void
  unsubscribeFromDarkMode: () => void
}
