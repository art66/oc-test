import { subscribeOnDarkMode, unsubscribeFromDarkMode } from '..'

describe('darkModeChecker()', () => {
  jest.useFakeTimers()

  it('should subscribe callback and return it with "true/false" flag based on current DOM mode set (5 runs)', () => {
    const statusStore = []

    const callback = jest.fn(status => statusStore.push(status))

    subscribeOnDarkMode(callback)
    jest.runAllTimers() // starting mock timers inside artificial MutationObserver

    expect(callback).toHaveBeenCalledTimes(7)

    expect(statusStore[0]).toBeFalsy()
    expect(statusStore[1]).toBeFalsy()
    expect(statusStore[2]).toBeFalsy()
    expect(statusStore[3]).toBeFalsy()
    expect(statusStore[4]).toBeFalsy()
    expect(statusStore[5]).toBeFalsy()
    expect(statusStore[6]).toBeTruthy()
  })
  it('should unsubscribe from DOM observation', () => {
    // it's a e2e tests, it would not be a real here. Provided just for consistency purposes.
    unsubscribeFromDarkMode()
  })
})
