import { IGetCurrentPageURL, TReturn } from './interfaces'

/**
 *  @name getCurrentPageURL
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description returns current page url string along with its pathname if needed.
 *
 *  @params {configuration} object - an object with options.
 *  @params {isOriginOnly} object.isOriginOnly - if active will return origin along with its pathname.
 *
 *  @return {string} pageURL - a string of current page URL.
 *
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const getCurrentPageURL = (config?: IGetCurrentPageURL): TReturn => {
  const { isOriginOnly = false } = config || {}
  const { origin, pathname } = window.location

  return isOriginOnly ? origin : `${origin}${pathname}`
}

export default getCurrentPageURL
