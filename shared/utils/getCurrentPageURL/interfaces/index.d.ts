export interface IGetCurrentPageURL {
  isOriginOnly?: boolean
}
export type TReturn = string
