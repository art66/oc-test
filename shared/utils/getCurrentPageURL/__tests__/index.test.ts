import getCurrentPageURL from '../'

describe('getCurrentPageURL()', () => {
  delete window.location

  window.location = {
    origin: 'http://localhost',
    pathname: '/testPath.php'
  } as any

  it('should return current url host with path', () => {
    const currentLocation = getCurrentPageURL()

    expect(currentLocation).toBe('http://localhost/testPath.php')
  })
  it('should return current url host with origin only', () => {
    const currentLocation = getCurrentPageURL({ isOriginOnly: true })

    expect(currentLocation).toBe('http://localhost')
  })
})
