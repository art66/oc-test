# Shared/utils/keyDownHandler - Torn


## 1.1.1
 * Fixed JSDoc.

## 1.1.0
 * Added stopPropagation event blocker.
 * Added JSDoc.

## 1.0.0
 * Initial release.
