import { TIsValue } from '../../isValue'

export type TNormalizeParams = TIsValue
export type TReturn = string
