export const oneParam = ['keyOne=someParam']

export const twoParams = ['keyOne=someParam', 'keyTwo=someParam']

export const threeParams = ['keyOne=someParam', 'keyTwo=someParam', 'keyThree=someParam']
