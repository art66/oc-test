import normalizeFetchParams from '../'

import { oneParam, twoParams, threeParams } from '../mocks'

describe('normalizeFetchParams', () => {
  it('normalizeFetchParams should return param string with 1 param and "?" leading sign', () => {
    expect(normalizeFetchParams(oneParam)).toBe('?keyOne=someParam')
  })
  it('normalizeFetchParams should return param string with 1st param and "?" leading sign and 2nd with "&"', () => {
    expect(normalizeFetchParams(twoParams)).toBe('?keyOne=someParam&keyTwo=someParam')
  })
  it('normalizeFetchParams should return param string with 1st param and "?" along "&" for both 2nd and 3thd', () => {
    expect(normalizeFetchParams(threeParams)).toBe('?keyOne=someParam&keyTwo=someParam&keyThree=someParam')
  })
})
