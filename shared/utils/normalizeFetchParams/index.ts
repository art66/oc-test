/**
 *  @name normalizeFetchParams
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description allows make GET queries easily as never before.
 *
 *  @params {string[]} value - an array like strings consisted of key&value pairs.
 *
 *  @return {string} value - concatenated string consisted of correct query string for GET request
 *
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

import isValue from '../isValue'
import { TNormalizeParams, TReturn } from './interfaces'

const normalizeFetchParams = (data: TNormalizeParams[]): TReturn => {
  const foundedParams = []

  data.forEach((param: TNormalizeParams) => {
    const paramSuffix = foundedParams.length === 0 ? '?' : '&'

    isValue(param) && foundedParams.push(`${paramSuffix}${param}`)
  })

  return (foundedParams && foundedParams.join('')) || ''
}

export default normalizeFetchParams
