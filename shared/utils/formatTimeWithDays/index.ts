import { formatTimeNumber } from '@torn/shared/utils'

/**
 *  @name formatTimeWithDays
 *  @author oksanas
 *  @version 1.0.0
 *  @description converts days, hours, minutes, seconds to string format.
 *
 *  @param {number} timestamp - value in seconds.
 *
 *  @return {string} a string with format dd:hh:mm:ss.
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

export const formatTimeWithDays = (timestamp: number): string => {
  let seconds = timestamp

  const days = Math.floor(seconds / (3600 * 24))

  seconds -= days * 3600 * 24
  const hours = Math.floor(seconds / 3600)

  seconds -= hours * 3600
  const minutes = Math.floor(seconds / 60)

  seconds -= minutes * 60

  const text = `
    ${days !== 0 ? `${formatTimeNumber(days)}:` : ''}
    ${hours !== 0 || days !== 0 ? `${formatTimeNumber(hours)}:` : ''}
    ${formatTimeNumber(minutes)}:
    ${formatTimeNumber(seconds)}
  `

  return text.replace(/\s/g, '')
}
