import safariChecker from '../'

describe('safariChecker()', () => {
  it('safariChecker should return "true" in case of detected safary browser', () => {
    // set navigator.userAgent to being writable for fake test of the safary presens
    Object.defineProperty(navigator, 'userAgent', {
      writable: true
    })

    // @ts-ignore just for node tests purposes
    navigator.userAgent = 'safari'

    expect(safariChecker()).toBeTruthy()
  })
  it('safariChecker should "false" in case of some other browser', () => {
    // @ts-ignore just for node tests purposes
    navigator.userAgent = ''

    expect(safariChecker()).toBeFalsy()
  })
})
