/**
*  @name safaryChecker
*  @author 3p-sviat
*  @version 1.1.0
*  @description help to identify is Safari currently in use or something else.
*
*  @return {boolean} flag - true or false based on processed browser data.
*
*  @copyright Copyright (c) Torn, LTD.
*/

import { TReturn } from './interfaces'

const isSafary = (): TReturn => /^((?!chrome|android).)*safari/i.test(navigator.userAgent)

export default isSafary
