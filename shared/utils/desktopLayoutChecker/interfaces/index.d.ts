export interface IMutationConfig {
  attributes: boolean,
  attributeFilter: string[],
  childList: boolean,
  subtree: boolean
}

export interface IConfigHolderReturn {
  targetNode: HTMLElement
  mutationsConfig: IMutationConfig
}

export interface IMutation {
  type: string
}

export interface IDOMObserverReturn {
  subscribeOnDesktopLayout: (callback: Function) => void
  unsubscribeOnDesktopLayout: () => void
}
