import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '../'

describe('desktopLayoutChecker()', () => {
  jest.useFakeTimers()

  it('should subscribe callback and return it with "true/false" flag based on current DOM mode set (5 runs)', () => {
    const statusStore = []

    const callback = jest.fn(status => statusStore.push(status))

    subscribeOnDesktopLayout(callback)
    jest.runAllTimers() // starting mock timers inside artificial MutationObserver

    expect(callback).toHaveBeenCalledTimes(6)

    expect(statusStore[0]).toBeTruthy()
    expect(statusStore[1]).toBeTruthy()
    expect(statusStore[2]).toBeFalsy()
    expect(statusStore[3]).toBeTruthy()
    expect(statusStore[4]).toBeFalsy()
    expect(statusStore[5]).toBeTruthy()
  })
  it('should unsubscribe from DOM observation', () => {
    // it's a e2e tests, it would not be a real here. Provided just for consistency purposes.
    unsubscribeOnDesktopLayout()
  })
})
