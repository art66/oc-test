import { MANUAL_DESKTOP_LAYOUT, REGULAR_LAYOUT } from '../constants'
import { notifyConsole } from '../helpers'

const checkManualDesktopLayout = (targetNode: HTMLElement): boolean => {
  if (!targetNode.classList.contains('r')) {
    notifyConsole()

    return MANUAL_DESKTOP_LAYOUT
  }

  return REGULAR_LAYOUT
}

export default checkManualDesktopLayout
