import checkManualDesktopLayout from '../checkManualDesktopLayout'

describe('desktopLayoutChecker()', () => {
  Object.defineProperty(document.body, 'classList', {
    writable: true
  })

  // @ts-ignore
  document.body.classList = {}

  // mocking the real contains method to be tests here
  document.body.classList.contains = (str: string) => {
    const { className } = document.body
    const currentClasses = className.split(' ')

    return currentClasses.some((strClass: string) => strClass === str)
  }

  it('should return "false" flag in case of .r class missing inside body classList', () => {
    // @ts-ignore
    document.body.className = 'r'

    expect(checkManualDesktopLayout(document.body)).toBeFalsy()
  })
  it('should return "true" flag in case .r class is present inside body classList', () => {
    // @ts-ignore
    document.body.className = 'as'

    expect(checkManualDesktopLayout(document.body)).toBeTruthy()
  })
})
