import notifyConsole from '../notifyConsole'

describe('notifyConsole()', () => {
  it('should return "false" flag in case of .r class missing inside body classList', () => {
    const consoleLogSpyOn = jest.spyOn(console, 'log')

    notifyConsole()

    expect(consoleLogSpyOn).toBeCalled()
  })
})
