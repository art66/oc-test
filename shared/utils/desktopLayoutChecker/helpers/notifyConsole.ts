const notifyConsole = () => {
  console.log(
    '%cThe ' + `%cMANUAL DESKTOP MODE` + '%c is set!!!',
    'background: #fff; color: #111',
    'background: #222; color: #bada55',
    'background: #fff; color: #111'
  )
}

export default notifyConsole
