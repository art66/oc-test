import checkManualDesktopLayout from './checkManualDesktopLayout'
import notifyConsole from './notifyConsole'

export { checkManualDesktopLayout, notifyConsole }
