import Observable from '../observer'

import { checkManualDesktopLayout } from './helpers'
import { IMutation, IMutationConfig, IDOMObserverReturn } from './interfaces'

/**
*  @name desktopLayoutChecker
*  @author 3p-sviat
*  @version 1.0.1
*  @description give us an opportunity to subscribe on legacy layout mode: manual desktop or the regular adaptive one.
*               Notify target app about updates.
*
*  @param {function} callback - some function to trigger on updates on demand
*
*  @property {object} targetNode - DOM "body" node that need to be watched on any class changes
*  @property {object} mutationsConfig - configuration object with props for desktopLayoutChecker
*
*  @return {function} callback - with updated value
*
*  @copyright Copyright (c) Torn, LTD.
*/

class DOMObserver extends Observable {
  private _targetNode: HTMLElement
  private _mutationsConfig: IMutationConfig

  constructor() {
    super()

    this._targetNode = document.body

    this._mutationsConfig = {
      attributes: true,
      attributeFilter: ['class'],
      childList: false,
      subtree: false
    }
  }

  _classListMutationCheck = (mutationsList: IMutation[]) => {
    mutationsList.forEach((mutation: IMutation) => {
      if (mutation.type !== 'attributes') return

      this.notify(checkManualDesktopLayout(this._targetNode))
    })
  }

  _connectSubscriber = (unit: Function) => {
    if (typeof unit !== 'function') return

    this.subscribe(unit)
  }

  _startSubscription = (observer: any) => (unit: Function): IDOMObserverReturn => {
    this._connectSubscriber(unit)

    return observer.observe(this._targetNode, this._mutationsConfig)
  }

  _finishSubscription = (observer: any) => (): Function => {
    this.unsubscribeAll()

    return observer.disconnect()
  }

  run = () => {
    const observer = new MutationObserver(this._classListMutationCheck)

    return {
      subscribeOnDesktopLayout: this._startSubscription(observer),
      unsubscribeOnDesktopLayout: this._finishSubscription(observer)
    }
  }
}

const { run } = new DOMObserver()
const { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } = run()

export {
  subscribeOnDesktopLayout,
  unsubscribeOnDesktopLayout
}

export default subscribeOnDesktopLayout
