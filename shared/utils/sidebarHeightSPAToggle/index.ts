import subscribeOnDOMContentLoaded from '../subscribeOnDOMContentLoaded'

import { IReturnStyles, IReturnNodes } from './interfaces'

import {
  SIDEBAR_NODE,
  SIDEBAR_SHIFT,
  SIDEBAR_SHIFT_REMOVE,
  CONTENT_WRAP_MARGIN_REMOVE,
  CONTENT_WRAP_MARGIN_SET,
  CONTENT_WRAP_NODE,
  MOBILE_WIDTH,
  MAX_APP_WIDTH,
  APP_FLOAT
} from './constants'

/**
*  @name SidebarHeightSPAToggle
*  @author 3p-sviat
*  @version 1.0.0
*  @description SPA-like util allows to set sidebar/content-wrapper correct layout for pages switched by
*  @description client redirect. For Apps, that includes both sidebar/non-sidebar pages inside.
*
*  @property {_contentWrapRef} node - main content wrapper html node reference.
*  @property {_sidebarWrapRef} node - sidebar html node reference.
*
*  @copyright Copyright (c) Torn, LTD.
*/
class SidebarHeightSPAToggle {
  private _appWrapRef: HTMLElement | any
  private _contentWrapRef: HTMLElement | any
  private _sidebarWrapRef: HTMLElement | any

  _validateNode = (refNode: Node, refName: string): boolean => {
    if (!refNode) {
      console.error(`There is no "${refName}" node found! Please, check your html markup.`)

      return false
    }

    return true
  }

  _setRef = (refName: string) => {
    const refNode = document && document.querySelector(refName)
    console.log(refNode, 'NODE')
    if (!this._validateNode(refNode, refName)) {
      throw new Error('No node found.')
    }

    return refNode
  }

  _setContentWrapRef = () => {
    this._contentWrapRef = this._setRef(CONTENT_WRAP_NODE)
  }

  _setSidebarRef = () => {
    this._sidebarWrapRef = this._setRef(SIDEBAR_NODE)
  }

  _setAppRef = (appRef: string) => {
    this._appWrapRef = this._setRef(appRef)
  }

  _setContentWrapTopMargin = (margin: string) => {
    this._contentWrapRef.style.marginTop = margin
  }

  _setSidebarWrapTopShift = (shift: string) => {
    this._sidebarWrapRef.style.top = shift
  }

  _makeAppFitSidebar = () => {
    this._setContentWrapTopMargin(CONTENT_WRAP_MARGIN_SET)
    this._setSidebarWrapTopShift(SIDEBAR_SHIFT)
  }

  _preventAppFitSidebar = () => {
    this._setContentWrapTopMargin(CONTENT_WRAP_MARGIN_REMOVE)
    this._setSidebarWrapTopShift(SIDEBAR_SHIFT_REMOVE)
  }

  _addAppRightFloatLayout = () => {
    this._appWrapRef.style.maxWidth = MAX_APP_WIDTH
    this._appWrapRef.style.float = APP_FLOAT
  }

  _remoteAppRightFloatLayout = () => {
    this._appWrapRef.style.maxWidth = ''
    this._appWrapRef.style.float = ''
  }

  _getNodesStyles = (): IReturnStyles => {
    const { contentWrap, sidebarWrap } = this.getNodes()

    const contentMarginTop = contentWrap.style.marginTop
    const sidebarWrapShiftTop = sidebarWrap.style.top

    return {
      contentMarginTop,
      sidebarWrapShiftTop
    }
  }

  _observeResponsiveChanges = () => {
    const { contentMarginTop, sidebarWrapShiftTop } = this._getNodesStyles()

    const isMobileLayout = window.innerWidth < MOBILE_WIDTH
    const isNotResponsiveSet = contentMarginTop !== CONTENT_WRAP_MARGIN_SET && sidebarWrapShiftTop !== SIDEBAR_SHIFT

    if (isMobileLayout && isNotResponsiveSet) {
      this._makeAppFitSidebar()
    } else if (!isMobileLayout && !isNotResponsiveSet) {
      this._preventAppFitSidebar()
      this._addAppRightFloatLayout()
    }
  }

  _subscribeOnMount = () => {
    subscribeOnDOMContentLoaded([this._addAppRightFloatLayout, this._observeResponsiveChanges])
  }

  _subscribeOnResponsiveChanges = () => {
    window.addEventListener('resize', this._observeResponsiveChanges)
  }

  _unsubscribeSidebarHeightSPAToggle = () => {
    window.removeEventListener('resize', this._observeResponsiveChanges)

    this._preventAppFitSidebar()
    this._remoteAppRightFloatLayout()
  }

  /**
   * Provides access for DOM nodes references.
   * @name SidebarHeightSPAToggle#getNodes
   * @function
   *
   * @readonly
   */
  getNodes = (): IReturnNodes => {
    const contentWrap = this._contentWrapRef
    const sidebarWrap = this._sidebarWrapRef
    const appWrap = this._appWrapRef

    return {
      contentWrap,
      sidebarWrap,
      appWrap
    }
  }

  /**
   * Starting all functionality and subscribes on resize events as well.
   * @name SidebarHeightSPAToggle#runSidebarHeightSPAToggle
   * @function
   *
   */
  runSidebarHeightSPAToggle = (appNodeRef: string) => {
    this._setAppRef(appNodeRef)
    this._setContentWrapRef()
    this._setSidebarRef()
    this._subscribeOnMount()
    this._subscribeOnResponsiveChanges()
  }

  /**
   * Unsubscribes from resize events.
   * @name SidebarHeightSPAToggle#stopSidebarHeightSPAToggle
   * @function
   *
   * @readonly
   */
  stopSidebarHeightSPAToggle = () => {
    this._unsubscribeSidebarHeightSPAToggle()
  }
}

const {
  getNodes,
  runSidebarHeightSPAToggle,
  stopSidebarHeightSPAToggle
} = new SidebarHeightSPAToggle()

export { getNodes, runSidebarHeightSPAToggle, stopSidebarHeightSPAToggle }
