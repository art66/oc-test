export const SIDEBAR_NODE = '#sidebarroot'
export const CONTENT_WRAP_NODE = '.content-wrapper'
export const CONTENT_WRAP_MARGIN_SET = '120px'
export const CONTENT_WRAP_MARGIN_REMOVE = '0px'
export const SIDEBAR_SHIFT = '0px'
export const SIDEBAR_SHIFT_REMOVE = null
export const MOBILE_WIDTH = 600
export const MAX_APP_WIDTH = '784px'
export const APP_FLOAT = 'right'
