import { getNodes, runSidebarHeightSPAToggle, stopSidebarHeightSPAToggle } from '../'

describe('sidebarHeightToggle()', () => {
  document.body.innerHTML =
    '<div>' +
      '<did id="sidebarroot"></div>' +
      '<div class="content-wrapper">' +
        '<div id="app"></div>' +
      '</div>' +
    '</div>'

  it('should put the whole app layout in the right float position with max-width: 784px', () => {
    runSidebarHeightSPAToggle('#app')

    const appNode = document.querySelector('#app')
    const appStyles = appNode.getAttribute('style')

    expect(appStyles).toBe('max-width: 784px; float: right;')
  })
  it('should remove particular sidebar needed styles from app node and make it full screen again', () => {
    stopSidebarHeightSPAToggle()

    const appNode = document.querySelector('#app')
    const appStyles = appNode.getAttribute('style')

    expect(appStyles).toBe('')
  })
  it('should return all the nodes (list)', () => {
    const { contentWrap, sidebarWrap, appWrap } = getNodes()

    expect(contentWrap).toBeTruthy()
    expect(sidebarWrap).toBeTruthy()
    expect(appWrap).toBeTruthy()
  })
})
