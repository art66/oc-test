export interface IReturnStyles {
  contentMarginTop: string
  sidebarWrapShiftTop: string
}

export interface IReturnNodes {
  contentWrap: HTMLElement
  sidebarWrap: HTMLElement
  appWrap: HTMLElement
}
