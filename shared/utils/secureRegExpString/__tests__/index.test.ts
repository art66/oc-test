import securedRegExpString from '../'

describe('securedRegExpString()', () => {
  it('should return processed safe to throw into RegExp string', () => {
    const dangerousString = 'hello[world'

    expect(securedRegExpString(dangerousString)).toBe('hello\\[world')
  })
  it('should return normal string', () => {
    const normalString = 'hello world'

    expect(securedRegExpString(normalString)).toBe(normalString)
  })
})
