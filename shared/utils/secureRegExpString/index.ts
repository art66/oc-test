/**
*  @name securedRegExpString
*  @author 3p-sviat
*  @version 1.0.0
*  @description can process the string with dangerous regExp characters screen with the backslash
*
*  @typedef {string} TArgument
*
*  @params {string} inputValue - basic input string with the perfect layout
*
*  @returns {string} secureRegExpInputValue - normalized string prepared to be used inside regExp
*
*  @copyright Copyright (c) Torn, LTD.
*/

import { REGEXP_RESTRICTED_SYMBOLS } from '../../constants'
import { TArgument } from './interfaces'

const securedRegExpString = (inputValue: TArgument) => {
  const checkOnRestrictedRegExpSymbols = (symbol: TArgument) => REGEXP_RESTRICTED_SYMBOLS.includes(symbol)

  const secureRegExpInputValue = inputValue.split('').map(symbolV => {
    if (checkOnRestrictedRegExpSymbols(symbolV)) {
      return '\\' + symbolV
    }

    return symbolV
  })

  return secureRegExpInputValue.join('')
}

export default securedRegExpString
