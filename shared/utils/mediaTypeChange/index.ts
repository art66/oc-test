/**
*  @name MediaTypeChange
*  @author 3p-sviat
*  @version 1.0.0
*  @description help track which one mediaType is changed or not in the app
*
*  @params {function} callback - some function to trigger on updates on demand
*  @params {string} mediaType - current mediaType
*
*  @return {function} callback - with updated value
*
*  @deprecated Don't use this util for now! It's need to be improved for handling updates when
*              the mediaType is change (for now it send only true value, should be the false included as well!).
*
*  @copyright Copyright (c) Torn, LTD.
*/

class MediaTypeChange {
  media: {
    mediaType: string
    isMediaTypeChanged: boolean
  }
  subscribers: Function[]

  constructor() {
    this.media = {
      mediaType: null,
      isMediaTypeChanged: false
    }
    this.subscribers = []
  }

  setMedia = mediaType => {
    this.media = {
      mediaType,
      isMediaTypeChanged: mediaType !== this.media.mediaType ? true : false
    }
  }

  subscriberAlreadyAdded = unit => this.subscribers.some(func => func.name === unit.name)

  subscribe = unit => {
    if (this.subscriberAlreadyAdded(unit)) return

    this.subscribers.push(unit)
  }

  notify = () => this.subscribers.forEach(unit => unit(this.media.isMediaTypeChanged))

  check = (unit, mediaType) => {
    this.subscribe(unit)
    this.setMedia(mediaType)

    const { isMediaTypeChanged } = this.media

    if (isMediaTypeChanged) {
      this.notify()
    }
  }
}

export default new MediaTypeChange()
