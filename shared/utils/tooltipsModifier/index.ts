import { IProps } from './interfaces'

/**
*  @name tooltipMidifier
*  @author 3p-sviat
*  @version 1.1.0
*  @description can mask original jQuery tooltip layout by providing the custom values
*
*  @params {object} target - main object with the reference on DOM node
*  @params {object} coordFixes - some shift for tooltip appear
*  @params {number} coordFixes.FIX_TOOLTIP_TOP - tooltip top shift
*  @params {number} coordFixes.FIX_TOOLTIP_BOTTOM - tooltip bottom shift
*  @params {number} coordFixes.FIX_TOOLTIP_LEFT - tooltip left shift
*  @params {number} coordFixes.FIX_ARROW_LEFT - arrow left shift
*  @params {object} styles - styling data with clasees inside
*
*  @deprecated it's a legacy util that now doesn't needed since having the react-based tooltips library on Torn
*
*  @copyright Copyright (c) Torn, LTD.
*/

import COORDS_DEFAULT_FIXES from './constants'
import DEFAULT_STYLES from './styles/common.cssmodule.scss'

const getTooltipContainer = ({ target, coordFixes = COORDS_DEFAULT_FIXES, styles = DEFAULT_STYLES }: IProps) => {
  const aim: HTMLElement = target.hasAttribute('title') && target
  const tooltipWrap: HTMLElement = aim && document.querySelector('[role="tooltip"]')
  const { FIX_TOOLTIP_TOP, FIX_TOOLTIP_BOTTOM, FIX_TOOLTIP_LEFT, FIX_ARROW_LEFT } = coordFixes
  const { crimesTooltip, crimesTooltipArrow, arrowTop, arrowBottom } = styles

  if (tooltipWrap) {
    const tooltipArrow = tooltipWrap.firstChild.lastChild

    const getCoords = () => {
      const tooltipCoordsTop = tooltipWrap.getBoundingClientRect().top
      const tooltipCoordsWidth = tooltipWrap.getBoundingClientRect().width
      const aimCoordsTop = aim.getBoundingClientRect().top

      const isTooltipHigher = aimCoordsTop > tooltipCoordsTop
      const fixTooltipHeight = isTooltipHigher ? FIX_TOOLTIP_TOP : FIX_TOOLTIP_BOTTOM

      return {
        tooltipCoordsWidth,
        isTooltipHigher,
        fixTooltipHeight
      }
    }

    const { tooltipCoordsWidth, isTooltipHigher, fixTooltipHeight } = getCoords()

    const tooltipWrapFixer = () => {
      tooltipWrap.classList.add(crimesTooltip)
      tooltipWrap.style.top = parseInt(tooltipWrap.style.top, 10) + fixTooltipHeight + 'px'
      tooltipWrap.style.left = parseInt(tooltipWrap.style.left, 10) - FIX_TOOLTIP_LEFT + 'px'
    }

    const tooltipArrowFixer = () => {
      const extraClassName = isTooltipHigher ? arrowTop : arrowBottom
      const arrowUpOrDown = `${crimesTooltipArrow} ${extraClassName}`

      tooltipArrow.className = arrowUpOrDown
      tooltipArrow.style.left = tooltipCoordsWidth / 2 + FIX_ARROW_LEFT + 'px'
    }

    tooltipWrapFixer()
    tooltipArrowFixer()
  }
}

export default getTooltipContainer
