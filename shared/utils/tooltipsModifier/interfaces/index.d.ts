export interface IProps {
  target: HTMLElement
  coordFixes: {
    FIX_TOOLTIP_TOP: number
    FIX_TOOLTIP_BOTTOM: number
    FIX_TOOLTIP_LEFT: number
    FIX_ARROW_LEFT: number
  }
  styles: {
    crimesTooltip: string
    crimesTooltipArrow: string
    arrowTop: string
    arrowBottom: string
  }
}
