const COORDS_DEFAULT_FIXES = {
  FIX_TOOLTIP_TOP: 10,
  FIX_TOOLTIP_BOTTOM: -10,
  FIX_TOOLTIP_LEFT: 4,
  FIX_ARROW_LEFT: -10
}

export default COORDS_DEFAULT_FIXES
