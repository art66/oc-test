import { debounce, debounceDOM } from '../'

describe('debounce()', () => {
  jest.useFakeTimers()

  it('should fire click event only once per 3 clicks in the row', done => {
    const mockCallback = jest.fn(() => done())

    const simulateClick = () => {
      const eventCreator = document.createEvent('Events')
      eventCreator.initEvent('click', true, true)

      // trying to fire click event up to 3 times in the row and
      // then checking if the callback is trigger only once per each 1000ms delay
      window.dispatchEvent(eventCreator)
      window.dispatchEvent(eventCreator)
      window.dispatchEvent(eventCreator)
    }

    const startDebounce = () => {
      debounceDOM.run({
        event: 'click',
        callback: mockCallback,
        delay: 1000,
        target: window
      })
    }

    startDebounce()
    simulateClick()
    jest.runAllTimers()

    expect(mockCallback).toHaveBeenCalledTimes(1)
  })
  it('should fire debounce inly once on each console.log(3) in a row', done => {
    const mockCallback = jest.fn(() => done())

    const startDebounce = debounce({
      callback: mockCallback,
      delay: 1000
    })

    startDebounce()
    startDebounce()
    startDebounce()

    jest.runAllTimers()

    expect(mockCallback).toHaveBeenCalledTimes(1)
  })
  it('should clear debounce by clearTimer flag provided on the incoming call', () => {
    const mockCallback = jest.fn(() => {})

    const startDebounce = debounce({
      callback: mockCallback,
      delay: 1000
    })

    startDebounce()
    startDebounce()
    startDebounce()
    startDebounce({ clearTimer: true })

    jest.runAllTimers()

    expect(mockCallback).toHaveBeenCalledTimes(0)
  })
})
