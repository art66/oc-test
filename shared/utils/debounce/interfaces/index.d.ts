export interface IDebounce {
  callback: () => void
  delay?: number
}

export interface IDebounceDOM extends IDebounce {
  event?: string
  target?: HTMLInputElement | Window | Node
}
