# Shared/utils/debounce - Torn


## 1.2.2
 * Improved unsubscribing logic.

## 1.2.1
 * Improved Debounce logic.
 * Added pure debounce model as well as its extended version included ability to be subscribed on native DOM events.
 * Updated tests.

## 1.1.0
 * Added tests and interfaces.

## 1.0.0
 * Init commit.
