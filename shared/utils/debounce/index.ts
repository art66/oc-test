import { IDebounce, IDebounceDOM } from './interfaces'
import { INITIAL_STATE, DEFAULT_STATE } from './constants'

/**
 *  @name debounce
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description can give an opportunity to run some callback only once when it's the last one in the thrown sequence.
 *
 *  @typedef {Object} IDebounce
 *
 *  @params {function} callback - some function to trigger on updates on demand
 *  @params {number} delay - time period of triggering
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const debounce = ({ callback = () => {}, delay = 1000 }: IDebounce): Function => {
  let timerID = null

  return ({ clearTimer }: { clearTimer?: boolean } = {}) => {
    if (clearTimer) {
      clearTimeout(timerID)
      timerID = null // to be sure

      return
    }

    if (timerID) {
      clearTimeout(timerID)
    }

    timerID = setTimeout(() => {
      callback()
      timerID = null
    },                   delay)
  }
}

/**
 *  @name debounceDOM
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description extended version of debounce that allow us subscribe on native DOM events by debounce wrap between.
 *
 *  @typedef {Object} IDebounce
 *
 *  @params {object} config - options configuration
 *  @params {function} config.callback - some function to trigger on updates on demand
 *  @params {number} config.delay - time period of triggering
 *  @params {node} config.target - main DOM object to subscribe on
 *  @params {string} config.event - event type of subscription
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
class DebounceDOM {
  _state: any

  private _debounceInit: any

  constructor() {
    this._state = DEFAULT_STATE
  }

  _setDebounceData = ({ event = 'input', callback = () => {}, delay = 1000, target = window }: IDebounceDOM) => {
    this._state = {
      event: event || INITIAL_STATE.event,
      callback: callback || INITIAL_STATE.callback,
      delay: delay || INITIAL_STATE.delay,
      targetNode: target || INITIAL_STATE.targetNode
    }
  }

  _resolveEvent = () => {
    const { callback, delay } = this._state

    return debounce({ callback, delay })
  }

  _clearDebounceState = () => (this._state = DEFAULT_STATE)

  _subscribeDebounce = () => {
    const { targetNode, event } = this._state

    this._debounceInit = this._resolveEvent()

    targetNode.addEventListener(event, this._debounceInit)
  }

  _unsubscribeDebounce = () => {
    const { targetNode, event } = this._state

    targetNode.removeEventListener(event, this._debounceInit)

    // this._clearDebounceState()
  }

  run = (config: IDebounceDOM) => {
    this._setDebounceData(config)
    this._subscribeDebounce()
  }

  stop = () => this._unsubscribeDebounce()
}

/**
 * @deprecated
 */
const debounceDOM = new DebounceDOM()

export { debounce, debounceDOM, DebounceDOM }
