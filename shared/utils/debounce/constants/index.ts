export const DEFAULT_STATE = {
  targetNode: null,
  event: '',
  callback: () => {},
  delay: 0
}

export const INITIAL_STATE = {
  targetNode: window,
  event: 'input',
  callback: () => {},
  delay: 1000
}
