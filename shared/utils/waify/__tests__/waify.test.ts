import waify from '../'

import { secondArg, secondArgSnake, spaceSplit, curringString } from '../mocks'

describe('check waify function', () => {
  it('should append second argument', () => {
    expect(waify('playername', 'hakh')).toEqual(secondArg)
  })
  it('should snake case second second argument', () => {
    expect(waify('playername', 'hakh 123')).toEqual(secondArgSnake)
  })
  it('should split on spaces', () => {
    expect(waify('playername life', 'hakh 123')).toEqual(spaceSplit)
  })
  it('should allow curring', () => {
    const withPlayerName = waify('hakh 123')

    expect(withPlayerName('playername life')).toEqual(curringString)
  })
})
