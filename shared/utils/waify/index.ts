import snakeCase from 'snake-case'
import curryRight from 'lodash/curryRight'
import { TString, TUniq, TReturn } from './interfaces'

/**
*  @name waify
*  @author 3p-harabara, 3p-sviat
*  @version 1.1.0
*  @description legacy one util, we don't know what exactly it's doing.
*
*  @param {string} str  - some string to process
*  @param {string} unique - unique param to set for something?
*
*  @return {string} some string in snake case.
*
*  @deprecated try to avoid with util.
*
*  @copyright Copyright (c) Torn, LTD.
*/
const waify = curryRight((str: TString, unique: TUniq): TReturn => {
  const normalizedUnique = snakeCase(unique)

  return str
    .split(' ')
    .map((strInArr: string) => `${strInArr}_${snakeCase(normalizedUnique)}`)
    .join(' ')
})

export default waify
