import { TFuncList, TReadyStage } from './interfaces'

/**
*  @name subscribeOnDOMContentLoaded
*  @author 3p-sviat
*  @version 1.1.0
*  @description progressive way to handle DOMContentLoaded event. Useful util.
*
*  @params {function} callback - some event handler provided for invoke.
*
*  @copyright Copyright (c) Torn, LTD.
*/
const subscribeOnDOMContentLoaded = (callbackList: TFuncList[]) => {
  const runCallbacks = () => callbackList.forEach((cb: TFuncList) => cb())
  const isDOMContentLoadedPhaseMissed = (): TReadyStage => document.readyState !== 'loading'

  if (isDOMContentLoadedPhaseMissed()) {
    runCallbacks()
  } else {
    document.addEventListener('DOMContentLoaded', runCallbacks)
  }
}

export default subscribeOnDOMContentLoaded
