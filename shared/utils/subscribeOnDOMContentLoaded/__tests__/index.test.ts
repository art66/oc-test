import subscribeOnDOMContentLoaded from '../'

describe('subscribeOnDOMContentLoaded()', () => {
  it('should run mock func after DOMContentLoaded phase', () => {
    Object.defineProperty(window, 'readyState', {
      value: 'ready',
      writable: true
    })

    const funcToBeRuned = jest.fn()

    subscribeOnDOMContentLoaded([funcToBeRuned])
    expect(funcToBeRuned).toHaveBeenCalledTimes(1)
  })
  it('should run mock func inside DOMContentLoaded phase', () => {
    // @ts-ignore
    window.readyState = 'loading'
    const funcToBeRuned = jest.fn()

    subscribeOnDOMContentLoaded([funcToBeRuned])
    expect(funcToBeRuned).toHaveBeenCalledTimes(1)
  })
})
