import manageAppWrapLayout from '../'

const CONTENT_WRAPPER_CLASS = 'content-wrapper'

describe('manageAppWrapLayout()', () => {
  delete window.location

  window.location = {
    origin: 'http://localhost',
    hash: '/'
  } as any

  const contentWrapNode = document.createElement('div')
  contentWrapNode.className = CONTENT_WRAPPER_CLASS

  const domNode = document.body
  domNode.append(contentWrapNode)

  it('should add withSidebar class for .content-wrapper class in case of Sidebar-app mode', () => {
    manageAppWrapLayout({ pagesWithoutSidebar: ['/test', 'test2'] })

    expect(document.querySelector(`.${CONTENT_WRAPPER_CLASS}`).classList.contains('withSidebar')).toBeTruthy()
  })
  it('should add withoutSidebar class for .content-wrapper class in case of Sidebar-app mode', () => {
    window.location.hash = '/test'

    manageAppWrapLayout({ pagesWithoutSidebar: ['/test'] })

    expect(document.querySelector(`.${CONTENT_WRAPPER_CLASS}`).classList.contains('withoutSidebar')).toBeTruthy()
  })
})
