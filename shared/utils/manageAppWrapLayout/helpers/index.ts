export const checkIsPageWithoutSidebar = (currentPaths: string[]) => {
  const currentRoute = window.location.hash

  return currentPaths.includes(currentRoute)
}

export const getWrapNode = () => document.querySelector('.content-wrapper')
