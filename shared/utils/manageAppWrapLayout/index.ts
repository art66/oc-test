import { checkIsPageWithoutSidebar, getWrapNode } from './helpers'
import { IPageWithSidebar } from './interfaces'
import './styles/index.cssmodule.scss'

/**
*  @name manageAppWrapLayout
*  @author 3p-sviat
*  @version 1.0.0
*  @description util for managing react-app layout for both sidebar/non-sidebar apps in one time SPA app.
*
*  @param {string[]} pagesWithoutSidebar  - an array with paths to process.
*
*  @readonly
*
*  @copyright Copyright (c) Torn, LTD.
*/

// it's a hack for managing sidebar/non-sidebar layout for mixed CT&CTE SPA-like app.
// we can't make a better abstraction because of legacy templates.
// where our react app is placed inside the "outer" html markup!
const manageAppWrapLayout = ({ pagesWithoutSidebar }: IPageWithSidebar) => {
  const contentWrapperNode = getWrapNode()
  const isSidebarLayout = !checkIsPageWithoutSidebar(pagesWithoutSidebar)

  if (!isSidebarLayout) {
    contentWrapperNode.classList.add('withoutSidebar')
    contentWrapperNode.classList.remove('withSidebar')
  } else if (isSidebarLayout) {
    contentWrapperNode.classList.remove('withoutSidebar')
    contentWrapperNode.classList.add('withSidebar')
  }
}

export { checkIsPageWithoutSidebar }
export default manageAppWrapLayout
