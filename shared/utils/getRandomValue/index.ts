import { TNumber, TReturn } from './interfaces'

/**
 *  @name getRandomIntInclusive
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description returns a random number in range of min and max, including min and max values as well.
 *
 *  @params {number} min - min border of the random value.
 *  @params {number} max - max border of the random value.
 *
 *  @return {number} value - random value.
 *
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const getRandomIntInclusive = (min: TNumber, max: TNumber): TReturn => {
  const minValue = Math.ceil(min)
  const maxValue = Math.floor(max)

  return Math.floor(Math.random() * (maxValue - minValue + 1)) + minValue
}

export default getRandomIntInclusive
