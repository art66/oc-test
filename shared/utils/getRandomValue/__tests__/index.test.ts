import getRandomIntInclusive from '../'

const ITERATION_COUNT = 90000
const MIN_RANGE = 0
const MAX_RANGE = 5

describe('getRandomIntInclusive', () => {
  const iteratorHelper = (target: number) => {
    let randomValue = null

    Array.from(Array(ITERATION_COUNT).keys()).forEach(() => {
      const randomValueTemp = getRandomIntInclusive(0, 5)

      if (randomValueTemp === target) {
        randomValue = randomValueTemp
      }
    })

    return randomValue
  }

  it('should return a digit in range of 0 - 5', () => {
    const validNumberIncluding = [0, 2, 1, 3, 4, 5]
    const randomValue = getRandomIntInclusive(0, 5)

    expect(validNumberIncluding).toContain(randomValue)
  })
  it('should return a digit 0 in range 0 - 5', () => {
    expect(iteratorHelper(MIN_RANGE)).toBe(MIN_RANGE)
  })
  it('should return a digit 5 in range of 0 - 5', () => {
    expect(iteratorHelper(MAX_RANGE)).toBe(MAX_RANGE)
  })
})
