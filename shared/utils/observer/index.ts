import { TFunction } from './interfaces'

/**
*  @name Observable
*  @author 3p-sviat
*  @version 1.1.0
*  @description it's a classic model of Observer pattern. Can be useful for storage some data that need to be
*               shared between different subscribers (callbacks) once it changed from somewhere else.
*
*  @typedef {TFunction}
*
*  @property {array} _observers[] - it's a storage for subscribers (callbacks).
*
*  @return {function} callback - with updated value
*
*  @copyright Copyright (c) Torn, LTD.
*/
class Observable {
  private _observers: TFunction[]

  constructor() {
    this._observers = []
  }

  /**
   * Subscribes a provided callback to the observation.
   * @name Observable#subscribe
   * @function
   *
   * @params {Function} unit - some callback to subscribe.
  */
  subscribe = (unit: TFunction) => {
    this._observers.push(unit)
  }

  /**
   * Unsubscribes all subscribers from the observation.
   * @name Observable#unsubscribe
   * @function
   *
   * @params {Function} unit - some callback to unsubscribe.
  */
  unsubscribe = (unit: TFunction) => {
    this._observers.filter(observer => observer !== unit)
  }

  /**
   * Unsubscribes all subscribers from the observation.
   * @name Observable#unsubscribeAll
   * @function
   *
   * @readonly
  */
  unsubscribeAll = () => {
    this._observers.splice(0, this._observers.length)
  }

  /**
   * Invokes all callbacks to be notified with some payload provided.
   * @name Observable#notify
   * @function
   *
   * @params {any} payload - some data to notify callbacks with.
   *
  */
  notify = (payload: any) => {
    this._observers.forEach(observer => observer(payload))
  }

  /**
   * Returns an array with subscribers.
   * @name Observable#getUnits
   * @function
   *
   * @readonly
  */
  getUnits = (): TFunction[] => this._observers
}

export default Observable
