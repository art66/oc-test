import Observable from '../'

describe('Observable()', () => {
  it('should subscribe callback and notify it on some changes in store by notify()', () => {
    const callback = jest.fn(status => {
      expect(status).toBeTruthy()
    })

    const { subscribe, notify } = new Observable()

    subscribe(callback)
    notify(true)
  })
})
