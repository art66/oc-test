export interface IPromise {
  blob: () => void
}

export type TImage = string

export type TKey = string | number

export type TError = object | string
