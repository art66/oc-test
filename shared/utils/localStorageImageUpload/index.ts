/**
*  @name localStorageImageUpload
*  @author 3p-sviat
*  @version 1.1.0
*  @description allow ability to store images in base64 format inside browser localStorage.
*               Useful for storing extra large or server-cached images.
*
*  @property {string} cachedImgURL  - images url string to process
*  @property {string|nubmer} key - imagee key for its unique storing inside localStore.
*
*  @copyright Copyright (c) Torn, LTD.
*/

import { fetchImgUrl } from '@torn/shared/utils'
import { IPromise, TImage, TKey, TError } from './interfaces'

const setImgInLocalStorage = (file: Blob) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()

    reader.onload = () => resolve(reader.result)
    reader.onerror = (error: TError) => reject(error)
    reader.readAsDataURL(file)
  })
}

const localStorageImageUpload = (cachedImgURL: TImage, key: TKey) => {
  fetchImgUrl(cachedImgURL)
    .then((response: IPromise) => {
      return response.blob()
    })
    .then((response: Blob) => {
      return setImgInLocalStorage(response)
    })
    .then((image: string) => {
      localStorage[key] = image
    })
    .catch((error: TError) => {
      console.log('Something is going wrong:', error)
    })
}

export default localStorageImageUpload
