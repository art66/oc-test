/**
 *  @name moneyShorter
 *  @author 3p-sviat
 *  @version 1.1.1
 *  @description returns a string pow according to short letter input.
 *
 *  @param {object} config - an object with key value pairs
 *  @param {string} config.value - the main string to process
 *  @param {number} config.userMoney - optional param for calculating real user money
 *
 *  @return {string} value - processed string
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

import { IProps, TReturn } from './interfaces'

const BASIC_LETTER_POW = {
  k: 1000,
  m: 1000000,
  b: 1000000000
}

const ENHANCED_LETTERS_POW = (userMoney: number, value: string): number => {
  if (value.match(/(^\d\d?\d?%)$/i)) {
    const percentValue = value.substr(0, value.length - 1)

    return Number(userMoney * Number(percentValue) / 100)
  }

  const basicValues = {
    half: `${userMoney / 2}`,
    quarter: `${userMoney / 4}`,
    max: userMoney,
    '1/2': `${userMoney / 2}`,
    '1/3': `${userMoney / 3}`,
    '1/4': `${userMoney / 4}`,
    '1/5': `${userMoney / 5}`,
    '1/6': `${userMoney / 6}`,
    '1/7': `${userMoney / 7}`,
    '1/8': `${userMoney / 8}`,
    '1/9': `${userMoney / 9}`
  }

  return Number(basicValues[value])
}

const moneyShorterHook = ({ value, userMoney }: IProps): TReturn => {
  if (!value || typeof value !== 'string' || value.length <= 1) {
    return value
  }

  const powType = value.substr(value.length - 1).toLowerCase()
  const normalizedValue = Number(value.substr(0, value.length - 1))

  if (BASIC_LETTER_POW[powType]) {
    return (Number(normalizedValue * BASIC_LETTER_POW[powType])).toFixed(0)
  }

  if (userMoney && ENHANCED_LETTERS_POW(userMoney, value)) {
    return ENHANCED_LETTERS_POW(userMoney, value).toFixed(0)
  }

  return value
}

export default moneyShorterHook
