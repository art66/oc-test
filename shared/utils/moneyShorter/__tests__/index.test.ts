import moneyShorterHook from '..'

describe('moneyShorterHook()', () => {
  it('should return value in case of incorrect value type', () => {
    expect(moneyShorterHook({ value: undefined })).toBe(undefined)
  })
  it('should return basic value as an inputted one', () => {
    expect(moneyShorterHook({ value: '232' })).toBe('232')
  })
  it('should return value as an input and thrown NaN error because of invalid "number"-like string', () => {
    // const consoleLogSpyOn = jest.spyOn(console, 'error')

    expect(moneyShorterHook({ value: 'hello-world' })).toBe('hello-world')

    // retired
    // expect(consoleLogSpyOn).toBeCalledWith('moneyShorterHook ERROR! Something goes wrong, value: "hello-world" as NaN')
  })
  it('should return value with pow on 1000 (k sign)', () => {
    expect(moneyShorterHook({ value: '313k' })).toBe('313000')
  })
  it('should return value with pow on 1000000 (m sign)', () => {
    expect(moneyShorterHook({ value: '22m' })).toBe('22000000')
  })
  it('should return value with pow on 1000000000 (b sign)', () => {
    expect(moneyShorterHook({ value: '22b' })).toBe('22000000000')
  })
  it('should return value with pow on 1000 in case of capitalized K)', () => {
    expect(moneyShorterHook({ value: '22K' })).toBe('22000')
  })
  it('should return full user value)', () => {
    expect(moneyShorterHook({ value: 'max', userMoney: 10000 })).toBe('10000')
  })
  it('should return max if no user money provided)', () => {
    expect(moneyShorterHook({ value: 'max', userMoney: null })).toBe('max')
  })
})
