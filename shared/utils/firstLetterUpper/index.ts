/**
 *  @name firstLetterUpper
 *  @author 3p-sviat
 *  @version 1.0.1
 *  @description returns the word with the first letter upper in it. Can work with the complicated words
 *
 *  @param {object} config - an object with key value pairs
 *  @param {string} config.value - the main string to process
 *  @param {string} config.delimiter - optional param for multi-words string processing
 *
 *  @return {string} value - processed string
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

import { IProps, TReturn } from './interfaces'

const firstLetterUpper = ({ value, delimiter = '_', isDisabledMultiUpper = false }: IProps): TReturn => {
  if (!value || typeof value !== 'string') {
    return null
  }

  const stringWithFirstLetterUpper = (value.substr(0, 1).toUpperCase() + value.substr(1, value.length)).split('')

  const checkOnMultiUpperLetters = (preNormStrValue: string[]) => {
    if (isDisabledMultiUpper) {
      return
    }

    preNormStrValue.forEach((letter, index) => {
      if (letter !== delimiter) {
        return
      }

      preNormStrValue[index] = ' '
      preNormStrValue[index + 1] = preNormStrValue[index + 1].toUpperCase()
    })
  }

  checkOnMultiUpperLetters(stringWithFirstLetterUpper)

  return stringWithFirstLetterUpper.join('')
}

export default firstLetterUpper
