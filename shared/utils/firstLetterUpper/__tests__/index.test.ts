import firstLetterUpper from '..'

describe('firstLetterUpper()', () => {
  it('should return Hello word with the leading H letter', () => {
    expect(firstLetterUpper({ value: 'hello' })).toBe('Hello')
  })
  it('should return Hello World words with both leading H and W letters by "_" delimiter', () => {
    expect(firstLetterUpper({ value: 'hello-world', delimiter: '-' })).toBe('Hello World')
  })
  it('should return Hello world words with first leading H in case of force disabled multi checking state', () => {
    expect(firstLetterUpper({ value: 'hello world', delimiter: ' ', isDisabledMultiUpper: true })).toBe('Hello world')
  })
})
