export interface IProps {
  value: string
  delimiter?: string
  isDisabledMultiUpper?: boolean
}

export type TReturn = string
