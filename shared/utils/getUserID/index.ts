import { getCookie } from '@torn/shared/utils'
import { TReturn } from './interfaces'

/**
 *  @name getUserID
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description tiny util that returns current session user ID value.
 *
 *  @return {string} ID - founded user ID from cookie.
 *
 *  @readonly
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

const getUserID = (): TReturn => {
  // @ts-ignore
  const getGlobalCookieUtil = Object.prototype.hasOwnProperty.call(window, 'getCookie') && window.getCookie('uid')

  return getCookie('uid') || getGlobalCookieUtil || null
}

export default getUserID
