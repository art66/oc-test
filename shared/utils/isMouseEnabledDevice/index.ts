/**
 *  @name isMouseEnabledDevice
 *  @description check if there is a pointer device
 *
 *  @return {boolean} - true/false
 */
export const isMouseEnabledDevice = () => matchMedia('(pointer:fine)').matches
