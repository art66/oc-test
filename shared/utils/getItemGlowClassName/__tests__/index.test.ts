import getItemGlowClassName from '..'

describe('getItemGlowClassName(\'0\')', () => {
  it('should return the first rarity styles class name', () => {
    expect(getItemGlowClassName('1')).toEqual('rarity1')
  })

  it('should return the second rarity styles class name', () => {
    expect(getItemGlowClassName('2')).toEqual('rarity2')
  })

  it('should return the third rarity styles class name', () => {
    expect(getItemGlowClassName('3')).toEqual('rarity3')
  })

  it('shouldn\'t return a rarity styles class name', () => {
    expect(getItemGlowClassName()).toEqual('')
    expect(getItemGlowClassName(null)).toEqual('')
  })
})
