import cn from 'classnames'
import { FIRST_RARITY, SECOND_RARITY, THIRD_RARITY } from './constants'
import styles from './index.cssmodule.scss'
import { TRarity } from './interfaces'

export default function getItemGlowClassName(rarity?: TRarity) {
  return cn({
    [styles.rarity1]: rarity === FIRST_RARITY,
    [styles.rarity2]: rarity === SECOND_RARITY,
    [styles.rarity3]: rarity === THIRD_RARITY
  })
}
