import { FIRST_RARITY, SECOND_RARITY, THIRD_RARITY } from './constants'

export type TRarity = typeof FIRST_RARITY | typeof SECOND_RARITY | typeof THIRD_RARITY
