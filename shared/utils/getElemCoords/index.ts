import { IData, IReturn } from './interfaces'

/**
 *  @name getElemCoords
 *  @author 3p-sviat
 *  @version 1.0.0
 *  @description returns real element coordinates including particular browsers' scroll bar and offset widths
 *
 *  @param object config - an object with "top" and "left" coords values.
 *  @param object config.topCoords - top coords value.
 *  @param object config.leftCoords - left coords value.
 *
 *  @return {object} - left and top normalized coords.
 *
 *  @example
 *    // basic example
 *    getElemCoords({ topCoords: 30, leftCoords: 30 })
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const getElemCoords = ({ topCoords, leftCoords }: IData): IReturn => {
  const { body, documentElement: docElem } = document || {}

  const scrollTop = window?.pageYOffset || docElem?.scrollTop || body?.scrollTop
  const scrollLeft = window?.pageXOffset || docElem?.scrollLeft || body?.scrollLeft

  const clientTop = docElem?.clientTop || body?.clientTop || 0
  const clientLeft = docElem?.clientLeft || body?.clientLeft || 0

  const top = topCoords + scrollTop - clientTop
  const left = leftCoords + scrollLeft - clientLeft

  return {
    top,
    left
  }
}

export default getElemCoords
