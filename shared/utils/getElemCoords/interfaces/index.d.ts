export interface IData {
  topCoords: number
  leftCoords: number
}

export interface IReturn {
  top: number
  left: number
}
