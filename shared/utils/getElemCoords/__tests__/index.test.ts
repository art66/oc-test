// @ts-nocheck
import getElemCoords from '../index'

describe('getElemCoords()', () => {
  it('should return basic values', () => {
    expect(getElemCoords({ topCoords: 30, leftCoords: 30 })).toEqual({ left: 30, top: 30 })
  })
  it('should return values with scrollBars values', () => {
    document.body.scrollTop = 15
    document.body.scrollLeft = 15

    expect(getElemCoords({ topCoords: 30, leftCoords: 30 })).toEqual({ left: 45, top: 45 })
  })
  it('should return values with window offsets', () => {
    window.pageYOffset = 5
    window.pageXOffset = 5

    expect(getElemCoords({ topCoords: 30, leftCoords: 30 })).toEqual({ left: 35, top: 35 })
  })
  it('should return values with clientTop values', () => {
    Object.defineProperties(
      document.documentElement,
      {
        clientTop: {
          value: 12
        },
        clientLeft: {
          value: 12
        }
      }
    )

    expect(getElemCoords({ topCoords: 30, leftCoords: 30 })).toEqual({ left: 23, top: 23 })
  })
})
