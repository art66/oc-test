import { THex2RGBReturn } from './interfaces'

function hex2rgb(hex: string, opacity: number): THex2RGBReturn {
  let getHex = (hex + '').trim()
  const getOpacity = opacity || 1

  let rgb = null
  const match = getHex.match(/^#?(([0-9a-zA-Z]{3}){1,3})$/)

  if (!match) {
    return null
  }

  rgb = {}
  getHex = match[1]

  // check if 6 letters are provided
  if (getHex.length === 6) {
    rgb.r = parseInt(getHex.substring(0, 2), 16)
    rgb.g = parseInt(getHex.substring(2, 4), 16)
    rgb.b = parseInt(getHex.substring(4, 6), 16)
  } else if (getHex.length === 3) {
    rgb.r = parseInt(getHex.substring(0, 1) + getHex.substring(0, 1), 16)
    rgb.g = parseInt(getHex.substring(1, 2) + getHex.substring(1, 2), 16)
    rgb.b = parseInt(getHex.substring(2, 3) + getHex.substring(2, 3), 16)
  }

  rgb.css = `rgb${getOpacity ? 'a' : ''}(`
  rgb.css += `${rgb.r},${rgb.g},${rgb.b}`
  rgb.css += (getOpacity ? ',' + getOpacity : '') + ')'

  return rgb
}

export default hex2rgb
