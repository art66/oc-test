/**
 *  @name convertImageToCanvas
 *  @author 3p-harabara 3p-sviat
 *  @version 1.1.0
 *  @description creates a canvas-like object based on the image provided.
 *               Basically, it just create blur light shadow around.
 *
 *  @typedef {object} IProps
 *
 *  @param {object} iamgeToCanvasData - image data to process
 *  @param {string} iamgeToCanvasData.imgSrc - image resource
 *  @param {object} iamgeToCanvasData.offsetX - blur shift horizontal
 *  @param {object} iamgeToCanvasData.offsetY - blur shift vertical
 *  @param {object} iamgeToCanvasData.color - blur color
 *  @param {object} iamgeToCanvasData.opacity - blur opacity
 *
 *  @return {object} canvas - processed image that were put inside canvas black-box
 *
 *  @deprecated ported just for some legacy apps in react-apps. Try to avoid them by modern css techniques
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

import isCanvasSupported from './isCanvasSupported'
import hex2rgb from './hex2rgb'
import { ICanvasProps, TCanvasReturn } from './interfaces'

function convertImageToCanvas({
  imgSrc,
  offsetX,
  offsetY,
  color,
  opacity,
  shadowBlur = 10
}: ICanvasProps): TCanvasReturn {
  if (!isCanvasSupported()) return

  const getOffsetX = offsetX !== undefined ? offsetX : 0
  const getOffsetY = offsetY !== undefined ? offsetY : 0

  const imageOffsetWidth = getOffsetX * 2
  const imageOffsetHeight = getOffsetY * 2

  if (!imgSrc || !color) {
    return null
  }

  const canvas = document.createElement('canvas')
  const context = canvas.getContext('2d')
  const img = new Image()

  img.src = imgSrc
  canvas.className = 'item-glow'

  img.addEventListener('load', () => {
    canvas.width = img.width + imageOffsetWidth
    canvas.height = img.height + imageOffsetHeight

    context.shadowBlur = shadowBlur
    context.shadowColor = hex2rgb(color, opacity).css

    context.drawImage(img, getOffsetX, getOffsetY)
  })

  return canvas
}

export default convertImageToCanvas
