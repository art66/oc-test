export type TIsCanvasSupportedReturn = boolean

export interface ICanvasProps {
  imgSrc: string
  offsetX: number
  offsetY: number
  color: string
  opacity: number
  shadowBlur?: number
}

export type TCanvasReturn = HTMLCanvasElement

export type THex2RGBReturn = {
  css: string
  r: string
  g: string
  b: string
}
