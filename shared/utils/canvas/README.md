# Torn's Legacy-Ported Canvas Image Switcher.

### Ported from the original legacy-based one. Can accept the next object for process:

```
convertImageToCanvas({
  imgSrc: string,
  offsetX?: number,
  offsetY?: number,
  color: string,
  opacity: number
})

```

### On the output return generated canvas node with setted image and effects inside.
### For more info check original legacy-based varient of this fucntion!
