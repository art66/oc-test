import { TIsCanvasSupportedReturn } from './interfaces'

function isCanvasSupported(): TIsCanvasSupportedReturn {
  const elem = document.createElement('canvas')

  return !!(elem.getContext && elem.getContext('2d'))
}

export default isCanvasSupported
