/**
 *  @name getTheme
 *  @description check if body has dark-mode class assigned and return theme name, namely dark/light
 *
 *  @return {string} - dark/light
 */
export const getTheme = () => (document.querySelector('body').classList.contains('dark-mode') ? 'dark' : 'light')
