import { IThrottler, IThrottlerDOM } from './interfaces'
import { INITIAL_STATE, DEFAULT_STATE } from './constants'

/**
 *  @name throttler
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description allow us to work in throttler-like way by invoke callback only once per some set period inly
 *
 *  @typedef {Object} IThrottler
 *
 *  @params {function} callback - some function to trigger on updates on demand
 *  @params {number} delay - time period of triggering
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const throttler = ({ callback = () => {}, delay = 50 }: IThrottler): Function => {
  let timerID = null

  return () => {
    if (timerID) return

    timerID = setTimeout(() => {
      callback()
      timerID = null
    },                   delay)
  }
}

/**
 *  @name throttlerDOM
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description allow us to work in throttler-like way by invoke callback only once per some set period inly
 *  @description by the way with ability to subscribe on the native DOM nodes/events though.
 *
 *  @typedef {Object} IThrottlerDOM
 *
 *  @params {object} config - main object with the configuration provided
 *  @params {string} config.event - DOM event to subscribe on
 *  @params {function} config.callback - some function to trigger on updates on demand
 *  @params {number} config.delay - time period of triggering
 *  @params {object} config.target - main window object or any other DOM node
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
class ThrottlerDOM {
  _state: any

  private _throttlerInit: any

  constructor() {
    this._state = DEFAULT_STATE
  }

  _setThrottlerData = ({ event = 'resize', callback = () => {}, delay = 50, target = window }: IThrottlerDOM) => {
    this._state = {
      event: event || INITIAL_STATE.event,
      callback: callback || INITIAL_STATE.callback,
      delay: delay || INITIAL_STATE.delay,
      targetNode: target || INITIAL_STATE.targetNode
    }
  }

  _resolveEvent = () => {
    const { callback, delay } = this._state

    return throttler({ callback, delay })
  }

  _clearThrottlerState = () => {
    this._state = DEFAULT_STATE
  }

  _subscribeThrottler = () => {
    const { targetNode, event } = this._state

    this._throttlerInit = this._resolveEvent()

    targetNode.addEventListener(event, this._throttlerInit)
  }

  _unsubscribeThrottler = () => {
    const { targetNode, event } = this._state

    targetNode.removeEventListener(event, this._throttlerInit)

    // this._clearThrottlerState()
  }

  run = (config: IThrottlerDOM) => {
    this._setThrottlerData(config)
    this._subscribeThrottler()
  }

  stop = () => this._unsubscribeThrottler()
}

/**
 * @deprecated
 */
const throttlerDOM = new ThrottlerDOM()

export { throttler, throttlerDOM, ThrottlerDOM }
