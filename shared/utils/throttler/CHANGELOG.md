# Shared/utils/throttler - Torn


## 1.1.2
 * Improved unsubscribing logic.

## 1.1.1
 * Improved Throttler logic.
 * Added pure debounce model as well as its extended version included ability to be subscribed on native DOM events.
 * Updated tests.

## 1.1.0
 * Added tests and interfaces.

## 1.0.0
 * Init commit.
