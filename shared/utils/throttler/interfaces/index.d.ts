export interface IThrottler {
  callback: Function
  delay?: number
}

export interface IThrottlerDOM {
  event: string
  callback: Function
  delay?: number
  target?: Window | Node
}
