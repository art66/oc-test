import { throttler, throttlerDOM } from '../'

describe('throttler()', () => {
  jest.useFakeTimers()

  it('should fire click event only once per 1000ms by six clicks in the row during 3000ms (3 times total)', done => {
    const mockCallback = jest.fn(() => done())

    const simulateClick = () => {
      const eventCreator = document.createEvent('Events')
      eventCreator.initEvent('click', true, true)

      // trying to fire click event up to 5 times in the row by 500ms delay and
      // then checking if the callback is trigger only once per each 1000ms delay (3 in total)
      Array.from(Array(5).keys()).forEach(key => {
        setTimeout(() => window.dispatchEvent(eventCreator), key * 500)
      })
    }

    const startThrottler = () => {
      throttlerDOM.run({
        event: 'click',
        callback: mockCallback,
        delay: 1000,
        target: window
      })
    }

    startThrottler()
    simulateClick()
    jest.runAllTimers()

    expect(mockCallback).toHaveBeenCalledTimes(3)
  })
  it('should fire throttler only once on each console.log(3) in a row', done => {
    const mockCallback = jest.fn(() => done())

    const startThrottler = throttler({
      callback: mockCallback,
      delay: 1000
    })

    setTimeout(startThrottler, 500)
    setTimeout(startThrottler, 900)
    setTimeout(startThrottler, 1500)
    setTimeout(startThrottler, 1900)
    setTimeout(startThrottler, 2500)
    setTimeout(startThrottler, 2900)

    jest.runAllTimers()

    expect(mockCallback).toHaveBeenCalledTimes(5)
  })
})
