export interface IReturn {
  isBottomHitOnScroll: boolean
  isBottomHitOnView: boolean
}
