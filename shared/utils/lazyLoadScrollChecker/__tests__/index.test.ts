import lazyLoadScrollChecker from '../'

describe('lazyLoadScrollChecker', () => {
  // mocking the real data for testing like in the browser
  Object.defineProperty(window, 'innerHeight', {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 1000
  })
  Object.defineProperty(document.documentElement, 'scrollHeight', {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 1000
  })

  it('should return true in case of hit the bottom of the screen on load phase (while a screen without scroll)', () => {
    const { isBottomHitOnScroll, isBottomHitOnView } = lazyLoadScrollChecker()

    expect(isBottomHitOnView).toBeTruthy()

    // actually it can't be truth so that's why we use an independent "OnView" flag
    expect(isBottomHitOnScroll).toBeTruthy()
  })
  it('should return true in case of hit the bottom while scrolling)', () => {
    // mocking the real data for testing like in the browser
    Object.defineProperties(window, {
      pageYOffset: {
        value: 500,
        writable: true
      },
      innerHeight: {
        value: 500,
        writable: true
      }
    })

    const { isBottomHitOnScroll, isBottomHitOnView } = lazyLoadScrollChecker()

    expect(isBottomHitOnView).toBeFalsy()
    expect(isBottomHitOnScroll).toBeTruthy()
  })
})
