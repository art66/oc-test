/**
 *  @name lazyLoadScrollChecker
 *  @author 3p-sviat
 *  @version 1.0.0
 *  @description checks if the user is currently hit the bottom of the page by scroll
 *
 *  @returns {object} lazyCheck - object with two properties
 *  @returns {boolean} lazyCheck.isBottomHitOnScroll - returns actual scroll position hit by boolean flag.
 *  @returns {boolean} lazyCheck.isBottomHitOnView - returns window hit position by boolean flag.
 *
 *  @readonly
 *
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

import { IReturn } from './interfaces'

const lazyLoadScrollChecker = (): IReturn => {
  const _configuration = {
    windowHeight: window.innerHeight,
    fullPageHeight: document.documentElement.scrollHeight,
    scrollTop: window.pageYOffset
  }

  const _calcCurrentScrollPosition = () => _configuration.fullPageHeight - _configuration.scrollTop

  const _checkScreenCoords = () => {
    const { windowHeight, fullPageHeight, scrollTop } = _configuration

    return {
      isBottomHitOnScroll: _calcCurrentScrollPosition() + scrollTop === fullPageHeight,
      isBottomHitOnView: windowHeight === fullPageHeight
    }
  }

  return _checkScreenCoords()
}

export default lazyLoadScrollChecker
