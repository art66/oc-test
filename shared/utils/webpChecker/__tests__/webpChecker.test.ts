import webpChecker from '../'

describe('webpChecker()', () => {
  it('srcCreator should return fake "false" in case of webp support', done => {
    const callback = jest.fn(status => {
      expect(status).toBe(false)
      done()
    })

    const webpIMG = webpChecker({
      callback
    })

    if (webpIMG.onload) {
      const event = {}

      webpIMG.onload(event)
    }
  })
  it('srcCreator should set global webp support flag "true" inside window object in case of webp support', () => {
    webpChecker()

    expect(window.__WEBPSUPPORT__).toBeTruthy()
  })
  it('srcCreator should return fake "false" and document.body.classList === "webp-support" inside window object in case of webp support', done => {
    const callback = jest.fn(status => {
      expect(status).toBe(false)
      done()
    })

    const webpIMG = webpChecker({
      callback,
      injectBodyClass: true
    })

    if (webpIMG.onload) {
      const event = {}

      webpIMG.onload(event)
      expect(document.body.classList.contains('webp-support')).toBeTruthy()
    }
  })
  it('srcCreator should return false and error log in case of webp support', done => {
    const consoleLogSpyOn = jest.spyOn(console, 'log')

    const callback = jest.fn(status => {
      expect(status).toBe(false)
      done()
    })

    const webpIMG = webpChecker({
      callback
    })

    if (webpIMG.onerror) {
      const event = {
        message: 'some error'
      }

      webpIMG.onerror(event)
      expect(consoleLogSpyOn).toBeCalledWith('Some error is happen during webP support checking: {\"message\":\"some error\"}')
    }

    consoleLogSpyOn.mockRestore()
  })
})
