export interface IWebP {
  callback?: (status: boolean) => void
  injectBodyClass?: boolean
  disableGlobal?: boolean
}

export type TReturn = void
