import { ISW } from './interfaces'

/**
 *  @name swRegisterer
 *  @author 3p-sviat
 *  @version 1.0.0
 *  @description Can handle ServiceWorkers registration on the web page.
 *
 *  @param {object} config - configuration payload for the string processing
 *  @param {string} config.sw - ServiceWorker url
 *  @param {string} config.scope - ServiceWorker scope to install
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const swRegisterer = ({ sw, scope }: ISW): void => {
  if (!navigator || !navigator.serviceWorker) {
    console.error('ServiceWorkers are not supported by this browser')

    return
  }

  try {
    navigator.serviceWorker.register(
      sw,
      __DEV__ ? { scope: 'https://localhost:3000' } : { scope }
    )
    // .then(registration => console.log(' Excellent. SW scope: ', registration.scope))
  } catch (e) {
    console.log('Error in service worker', e)
  }
}

export default swRegisterer
