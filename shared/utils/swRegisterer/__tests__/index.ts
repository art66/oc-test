import swRegisterer from '../'

describe('InputStringChecker()', () => {
  it('should return error if no ServiceWorker api allowed', () => {
    const consoleLogSpyOn = jest.spyOn(console, 'error')

    swRegisterer({ sw: 'js/serviceWorkers/sw.js' })

    expect(consoleLogSpyOn).toBeCalledWith('ServiceWorkers are not supported by this browser')
  })
})
