import numberToArray from '..'

describe('numberToArray()', () => {
  it('should return basic array consisted of 10 elements from 1-10', () => {
    expect(numberToArray(10)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
  })
  it('should return basic value as an inputted one', () => {
    const consoleLogSpyOn = jest.spyOn(console, 'error')

    expect(numberToArray(0)).toEqual([])
    expect(consoleLogSpyOn).toBeCalledWith('Failed to process number value to array: ', 0)
  })
  it('should return an empty array in case of input different from number', () => {
    const consoleLogSpyOn = jest.spyOn(console, 'error')

    // @ts-ignore
    expect(numberToArray('hello')).toEqual([])
    expect(consoleLogSpyOn).toBeCalledWith('Failed to process number value to array: ', 'hello')
  })
  it('should return an empty array in case of negative value', () => {
    const consoleLogSpyOn = jest.spyOn(console, 'error')

    expect(numberToArray(-1)).toEqual([])
    expect(consoleLogSpyOn).toBeCalledWith('Failed to process number value to array: ', -1)
  })
})
