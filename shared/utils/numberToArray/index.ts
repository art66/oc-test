/**
 *  @name numberToArray
 *  @author 3p-sviat
 *  @version 1.0.0
 *  @description convert number to array based on length of the number value.
 *
 *  @param {number} number - number to convert into the array
 *
 *  @return {number[]} array - processed array consisted of numbers
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

import { TProps, TReturn } from './interfaces'

const numberToArray = (count: TProps): TReturn => {
  if (typeof count !== 'number' || count <= 0) {
    console.error('Failed to process number value to array: ', count)

    return []
  }

  const countArray = Array.from(Array(count).keys())

  return countArray.map((key: number) => key + 1)
}

export default numberToArray
