export const initialState = {
  object: {
    first: 'someValue'
  },
  string: 'someValue'
}

export const missedObject = {
  ...initialState,
  object: null
}
export const missedString = {
  ...initialState,
  string: null
}
