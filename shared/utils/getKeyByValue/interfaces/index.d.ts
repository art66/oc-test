export type TTarget = object
export type TValue = string | number | boolean
export type TReturn = string
