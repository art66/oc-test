import isValue from '../isValue'
import { TTarget, TValue, TReturn } from './interfaces'

/**
 *  @name getKeyByValue
 *  @author 3p-sviat
 *  @version 1.1.0
 *  @description allows easily get key from some object by its value provided.
 *
 *  @params {object} object - main target where from you need to get some key.
 *  @params {string | number | boolean} value - some value by which you want to find needed key inside th object.
 *
 *  @return {string} key - founded key or null in case of fail.
 *
 *
 *  @copyright Copyright (c) Torn, LTD.
 */
const getKeyByValue = (object: TTarget, value: TValue): TReturn => {
  if (!object || (object && Object.keys(object).length === 0) || !isValue(value)) {
    console.error('Some params in getKeyByValue function were missed:', object, value)

    return null
  }

  return Object.keys(object).find(key => object[key] === value)
}

export default getKeyByValue
