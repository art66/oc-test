import getKeyByValue from '../'

import { initialState, missedObject, missedString } from '../mocks'

describe('getKeyByValue', () => {
  it('getKeyByValue should return "first" key as value', () => {
    expect(getKeyByValue(initialState.object, initialState.string)).toBe('first')
  })
  it('getKeyByValue should "null" in case of missing object', () => {
    expect(getKeyByValue(missedObject.object, missedObject.string)).toBe(null)
  })
  it('getKeyByValue should "null" in case of missing "key" string', () => {
    expect(getKeyByValue(missedString.object, missedString.string)).toBe(null)
  })
})
