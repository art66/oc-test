import { TArgs, TReturn } from './interfaces'

const arrayFlatter = (array: TArgs): TReturn => {
  const arrSave = [...array]
  const arrTemp = []

  const iterator = (arrayBuff: TArgs): TReturn => {
    arrayBuff.forEach(item => {
      if (item.push) {
        iterator(item)

        return
      }

      arrTemp.push(item)
    })

    return arrTemp
  }

  return iterator(arrSave)
}

export default arrayFlatter
