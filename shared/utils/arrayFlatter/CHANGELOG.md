# Shared/utils/arrayFlatter - Torn


## 1.1.1
 * Added ability to check mediaType with manualDesktopModeFlag.

## 1.1.0
 * Added tests and interfaces.

## 1.0.0
 * Init commit.
