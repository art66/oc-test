import arrayFlatter from '..'
import initialState from '../mocks'

describe('arrayFlatter()', () => {
  it('should return flatten array from [1, 2, 3, 4, 5, 6, [2, 3, 4, 6, [2, 4]], 2, 3, 4, [2, 3]]', () => {
    expect(arrayFlatter(initialState)).toEqual([1, 2, 3, 4, 5, 6, 2, 3, 4, 6, 2, 4, 2, 3, 4, 2, 3])
  })
})
