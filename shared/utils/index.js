/**
*  @name saveLog
*  @author 3p-harabara
*  @version 1.0.0
*  @description legacy one util, send client log to the server.
*
*  @param {object} data - config to process.
*  @param {string} data.desc - current user url location.
*  @param {object} data.error - unexpected error during fetch.
*  @param {object} data.responce - response object with payload.
*
*  @property {object} config - inner config to send on back-end.
*  @property {'omit'|'same-origin'|'include'} config.credentials=same-origin - xss policy flag.
*  @property {object} config.headers - request headers.
*  @property {RequestInit} config['X-Requested-With']=XMLHttpRequest - AJAX or not type request.
*  @property {desc} config.desc=window.location.href - current user location.

*  @copyright Copyright (c) Torn, LTD.
*/

export const saveLog = data => {
  const config = {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
  }

  if (data) {
    config.method = 'post'
    config.body = JSON.stringify({
      desc: window.location.href,
      data
    })
  }

  return fetch('/loader.php?sid=setClientLog', config)
}

/**
*  @name fetchUrl
*  @author 3p-harabara
*  @version 1.0.0
*  @description main fetch util for AJAX requiset sending.
*
*  @param {string} url - image href to fetch.
*  @param {object=} data - some extra payload for POST request sending, instead of GET.
*
*  @property {'omit'|'same-origin'|'include'} credentials='same-origin' - xss request policy.
*  @property {object} headers - headers object.
*  @property {RequestInit} headers['X-Requested-With']='XMLHttpRequest' - ALAX or not way of fetch.
*
*  @return {object} returns payload collected from the server.
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const fetchUrl = (url, data = null, withChecking = false) => {
  const config = {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
  }

  if (data) {
    config.method = 'post'
    config.body = new FormData()

    Object.keys(data).forEach(key => {
      const type = typeof data[key]

      if (['number', 'string'].includes(type)) {
        config.body.append(key, data[key])
      } else {
        config.body.append(key, JSON.stringify(data[key]))
      }
    })
  }

  return fetch(url, config)
    .then(response => response.text()
      .then(text => {
        try {
          const json = text === 'null' ? {} : JSON.parse(text) // preventing fetch crash on null response (that's should not be the reason to fail at all!)

          const checkOnErrors = ({ error, content }) => {
            if (error) {
              throw error
            }

            if (content) {
              throw content
            }
          }

          if (json && withChecking) { // activate error checking once you need this
            checkOnErrors(json)
          }

          return json
        } catch (e) {
          saveLog({ desc: window.location.href, error: e && e.toString(), responce: text })

          throw new Error('Malformed response')
        }
      }))
    .catch(e => {
      throw e
    })
}

/**
*  @name fetchImgUrl
*  @author 3p-sviat
*  @version 1.0.0
*  @description allow ability to fetch some images, useful in cases where we need to
*               load some images async of before initial content load.
*
*  @param {string} url - image href to fetch.
*
*  @property {'omit'|'same-origin'|'include'} credentials='same-origin' - xss request policy.
*  @property {object} headers - headers object.
*  @property {RequestInit} headers['X-Requested-With']='XMLHttpRequest' - AJAX or not way of fetch.
*  @property {RequestInit} headers['Accept]='image/webp,image/png,image/...' - headers object.
*
*  @return {object} fetched image payload.
*
*  @copyright Copyright (c) Torn, LTD.
*/
export const fetchImgUrl = url => {
  const config = {
    credentials: 'same-origin',
    headers: { 'X-Requested-With': 'XMLHttpRequest', Accept: 'image/webp,image/png,image/*,*/*;q=0.8' }
  }

  return fetch(url, config)
}

/**
*  @name toMoney
*  @author 3p-harabara, 3p-sviat
*  @version 1.0.0
*  @description converts regular money value to the more mathemetic-like style (with comma and etc).
*
*  @param {string | number} n='' - value of the money.
*  @param {string} currency='' - type of the game currency.
*  @param {number?} rounder='' - units after comma.
*
*  @return {string} number value with commas.
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const toMoney = (n = '', currency = '', rounder = 0) => {
  const moneyStringify = typeof n === 'number' ? n.toFixed(rounder).toString() : n
  const moneyNormalized = moneyStringify.replace(/,/g, '')
  const money = currency + moneyNormalized.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')

  return money
}

/**
*  @name getURLParameterByName
*  @author 3p-harabara
*  @version 1.0.0
*  @description converts money long format in a short-text variant of it.
*
*  @param {number} n - value of the money.
*  @param {string} currency='' - type of the game currency.
*
*  @return {string} procced value with some short-layout leading symbols.
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const shortMoney = (n, currency = '') => {
  const suffixes = ['', 'k', 'M', 'B', 'T', 'P']

  let suffixIndex = 0

  while (n >= 1000) {
    n /= 1000
    suffixIndex++
  }

  n = Math.round(n * 10) / 10

  return currency + n + suffixes[suffixIndex]
}

/**
*  @name getURLParameterByName
*  @author 3p-sviat
*  @version 1.0.0
*  @description return parsed some string param of the GET url's params.
*
*  @param {string} name - some of the current url's params to get out.
*
*  @return {string|null} procced string peeled of url symbols.
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const getURLParameterByName = name => {
  name = name.replace(/[\[\]]/g, '\\$&')

  const url = window.location.href
  const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`)
  const results = regex.exec(url)

  if (!results) {
    return null
  }

  if (!results[2]) {
    return ''
  }

  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

/**
*  @name getCookie
*  @author 3p-sviat
*  @version 1.0.0
*  @description allow to access cookies of the browser and get out there some key value.
*
*  @param {string} name - cookie to get out.
*
*  @return {string|undefined} cookie value.
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const getCookie = name => {
  const matchPrefix = '(?:^|; )'
  const matchSuffix = '=([^;]*)'
  const replacePattern = name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')

  const matches = document.cookie && document.cookie.match(
    new RegExp(`${matchPrefix}${replacePattern}${matchSuffix}`)
  )

  return matches && matches[1] ? decodeURIComponent(matches[1]) : undefined
}

/**
 *  @name setCookie
 *  @author oksanas
 *  @version 1.0.0
 *  @description allow to set cookies to the browser.
 *
 *  @param {string} name - cookie to set.
 *  @param {string} value - cookie value.
 *  @param {number} days - expiry time of cookie.
 *
 *  @return {string|undefined} cookie value.
 *
 *  @copyright Copyright (c) Torn, LTD.
 */

export const setCookie = (name, value, days) => {
  let expires = ''

  if (days) {
    const date = new Date()

    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
    expires = `; expires=${date.toUTCString()}`
  }

  document.cookie = `${name}=${value || ''}${expires}; SameSite=None; Secure; path=/`
}

/**
*  @name hhmmss
*  @author 3p-harabara
*  @version 1.0.0
*  @description legacy one util, we don't know what its doing.
*
*  @param {number} num - some value.
*
*  @return {string} value - some processed value with leading "0".
*
*  @copyright Copyright (c) Torn, LTD.
*/

function pad(num) {
  return (`0${num}`).slice(-2)
}

/**
*  @name hhmmss
*  @author 3p-harabara
*  @version 1.0.0
*  @description legacy one util, creates right data format layout.
*
*  @param {number} secs  - value in miliseconds.
*
*  @return {string} value=0 - some processed value in data format.
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const hhmmss = secs => {
  if (isNaN(secs)) return '0'

  let minutes = Math.floor(secs / 60)

  secs %= 60

  const hours = Math.floor(minutes / 60)

  minutes %= 60

  return `${(hours ? `${pad(hours)}:` : '') + pad(minutes)}:${pad(secs)}`
}

/**
*  @name withArticle
*  @author 3p-harabara
*  @version 1.0.0
*  @description legacy one util, adds an extra "an" or "a" to the article name.
*
*  @param {string} str  - some string to process
*
*  @return {string} a string with leading "an" or "a".
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const withArticle = str => (['a', 'e', 'i', 'o', 'u'].includes(str.charAt(0).toLowerCase()) ? 'an ' : 'a ') + str

/**
*  @name formatTimeNumber
*  @author 3p-harabara
*  @version 1.0.0
*  @description legacy one util, adds an extra "0" to the return value.
*
*  @param {string | number} val  - some string to process
*
*  @example: 7 -> "07"
*
*  @return {string} a string with leading "0".
*
*  @copyright Copyright (c) Torn, LTD.
*/

export const formatTimeNumber = val => (val.toString().length < 2 ? `0${val}` : val)
