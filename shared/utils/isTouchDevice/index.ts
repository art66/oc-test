/**
 *  @name isTouchDevice
 *  @description check if touch device
 *
 *  @return {boolean} - true/false
 */
export const isTouchDevice = () =>
  ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)
