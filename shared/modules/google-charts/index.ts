const loadScript = Symbol('loadScript');
const GOOGLE_CHARTS_LOADER_URL = 'https://www.gstatic.com/charts/loader.js';

type Options = {
  packages?: string[];
  callback: () => void;
}

class GoogleCharts {
  scriptPromise: Promise<(resolve: Function) => void>;
  api: any;

  [loadScript](version: string, options: Options) {
    if (!this.scriptPromise) {
      this.scriptPromise = new Promise(resolve => {
        const script = document.createElement('script');

        script.type = 'text/javascript';
        script.onload = () => {
          this.api = (window as any).google;
          this.api.charts.load(version, options);
          this.api.charts.setOnLoadCallback(() => {
            resolve()
          })
        };
        script.src = GOOGLE_CHARTS_LOADER_URL;
        document.body.appendChild(script);
      })
    }
    return this.scriptPromise
  }

  load(version: string, options: Options) {
    return this[loadScript](version, options).then(() => {
      this.api.charts.load(version, options);
      this.api.charts.setOnLoadCallback(options.callback);
    })
  }
}

const googleCharts = new GoogleCharts();

export default googleCharts;

// if ((typeof module !== 'undefined') && module.hot) {
//   module.hot.accept()
// }
