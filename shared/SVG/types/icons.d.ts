export interface IShadow {
  active?: boolean
  x?: string | number
  y?: string | number
  blur?: string | number
  color?: string
}

export interface ICustomFilter {
  active: boolean
  filter?: object
}

export interface IFilter {
  ID?: string
  active?: boolean
  shadow?: IShadow
  customFilter?: ICustomFilter
}

export interface IHover {
  event: string
  status: string | number | boolean
}

export interface IGragientItem {
  color?: string
  step?: string
}

export interface IGradient {
  ID?: string
  active: boolean
  coords?: {
    x1?: string | number,
    x2?: string | number,
    y1?: string | number,
    y2?: string | number
  }
  gradientUnits?: string
  scheme?: IGragientItem[]
  transform?: string
  additionalProps?: object
}

export interface IFill {
  name?: string
  color?: string
  stroke?: string
  strokeWidth?: string | number
}

export interface IDimenstions {
  width?: string | number
  height?: string | number
  viewbox?: string
}

export interface IPresets {
  type: string
  subtype: string
}

export interface IDeepSVGProps {
  active: boolean
  parentProps?: object
  childrenProps?: object | object[]
}

export interface IEvents {
  onClick?: object | any
  onHover?: object | any
}

export interface IIConStyleProps {
  customClass?: string
  dimensions?: IDimenstions
  filter?: IFilter
  gradient?: IGradient
  fill?: IFill
  customStyles?: React.CSSProperties
}

export interface IProps extends IIConStyleProps, IEvents {
  iconsHolder?: any
  preset?: IPresets
  isDeepParent?: boolean
  activeDefs?: boolean
  iconName: string
  type?: string | number
  deepProps?: IDeepSVGProps
}

export interface IDefsProps {
  callback: (child: IFilter | IGradient) => JSX.Element
  type: 'filters' | 'gradients'
  def: IFilter | IGradient
  node: Element
  optimizeSVGInjection?: boolean
}
