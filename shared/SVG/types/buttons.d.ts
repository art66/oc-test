export interface IItemsColor {
  color?: string
  step?: string | number
}

export interface ICurrentButton {
  shape: {
    desktop?: string
    tablet?: string
    mobile?: string
  }
}

export interface IProps {
  mediaType?: string
  condition?: string
  buttonName?: string
  buttonType?: string
  customClass?: string
  customColor?: IItemsColor
}
