# SVG - Torn


## 5.2.0
 * Added Demolition icons in Attack sprite.

## 5.1.3
 * Fixed broken icons event listeners after migration to async component load.

## 5.1.2
 * Fixed broken svg handling from particular sources.

## 5.1.1
 * Fixed broken eventHandler subscribing.

## 5.1.0
 * Added ability to choose btn async and sync icons bundle load.

## 5.0.0
 * Added async SVG icons loading, as well as particular apps chunks settings up.

## 4.2.11
 * Minor fixes in SVGIconGenerator.

## 4.2.10
 * Fixed wrong topPageLinks color while non-hovered.

## 4.2.9
 * Added OnlineStatus icons.

## 4.2.8
 * Fixed SVG buttons in tablet mode.

## 4.2.8
 * Fixed Global Defs.

## 4.2.7
 * Added special logic for Safari browser in case of using filters on the figure.

## 4.2.6
 * Fixed broken event props update inside SVGIConGenerator after React bundles update.

## 4.2.5
 * Fixed crimes gold star defs.

## 4.2.4
 * Fixed Defs function fails.

## 4.2.3
 * Fixed incorrect gradientUnits default prop.

## 4.2.2
 * Fixed type error checking.

## 4.2.1
 * Fixed StarContainer and Stars icons defs.

## 4.2.0
 * Added new two attributes for the linearGradient tag: coords and gradientUnits.

## 4.1.2
 * Reverted Defs function fails in case of missing default prop.

## 4.1.1
 * Added 15+ sidebar's icons.
 * Fixed Defs function fails in case of missing default prop.

## 4.0.2
 * Fixed React error about feDropShadow camelCase.
 * Fixed node error while it not found.

## 4.0.1
 * Added missing Parameditor icon

## 4.0.0
 * Major v.4 update!
 * Added the brand new feature pushing all icons defs into single storage place on the client side, instead of duplication the same defs inside each icon!

## 3.12.0
 * Update sidebar presets due to new design rules.

## 3.11.0
 * Added several sidebar icons.

## 3.10.0
 * Update several sidebar icons.

## 3.9.0
 * Fully rewritten sidebar presets.
 * Added ability to set parentNode on one level higher.

## 3.8.0
 * Added five new sidebar icons.

## 3.7.0
 * Added new FollowLinks icon.
 * Updated sidebar Presets.

## 3.6.0
 * Added missing AwardsMedal icon.
 * Added icon in the storybook data.

## 3.5.2
 * Added 36+ sidebar icons.
 * Update Storybook config.
 * Created preset for sidebar icons in reg/jail/hosp modes.
 * Improved firstLetterUpper function for handling all line characters in the string.
## 3.5.1
 Fixed tiny issue in a Star defs.

## 3.5.0
 * Added starContainer icon.
 * Updated storyBook.

## 3.4.3
 * Added new icons to preset config.

## 3.4.2
 * Add preset for TokenShop icon in the presets for top page links.

## 3.4.1
 * Change preset for Calendar page: fix issues with ChristmasTown and Back icons.

## 3.4.0
 * Added 2 new icons.

## 3.3.0
 * Fixed max-height in tablet/mobile modes.

## 3.2.0
 * Added several missed icons.
 * Created topPageLinks preset.

## 3.1.0
 * Added ability to start the event listeners in the future rerenders, once they are set disabled on initial render.

## 3.0.1
 * Add hover and active presets for ICS

## 3.0.0
 * Added NodeJS parser for easy to add svg icons configs.
 * Added more that 90 svg icons for the topPageLinks root.

## 2.6.7
 * Refactored several icon.tsx class methods.
 * Improved stability.

## 2.6.6
 * Fixed icons filter shadow by correct coord X and shadow props.

## 2.6.5
 * Added Cross, Crop, Pointer and Rotate icons.
 * Added presets for Images Crop System.

## 2.6.4
 * Added 6 new icons for top page links.

## 2.6.3
 * Fixed background in Crimes Stars.

## 2.6.2
 * Fixed classes collision in CrimeStars.

## 2.6.1
 * Fixed defs fucntion calculation.

## 2.6.0
 * Added 6 new Crimes Stars with defs and styles.
 * Added ability to set defs explicitly by icon type and also restrict to use them once needed.

## 2.5.2
 * Fixed SevereBurning icon name.

## 2.5.1
 * Icons structure has been separated on independend folders.
 * Minor Icons fixes.

## 2.5.0
 * Added toUpperCase util for better icon name handling.
 * Added Gassed and Loot Attack App icons inside the lib.
 * Improved SVGIconGenerator TS types.

## 2.4.0
 * Events listeners functional has been moved forward from origin svg element to parent node.

## 2.3.0
 * Improved deepProps stability in SVG Icons.

## 2.25.0
 * Added Fountain and Tombstone icons.

## 2.2.0
 * Added custom styles throwing for SVG icons.

## 2.1.0
 * Added feature for handling SVGIconGenereator Events: onHover and onClick.

## 2.0.1
 * Added new feature for manipulating deep SVG icons props.
 * Updated Storybook configs.
 * Minor bug fixes.

## 1.0.0
 * Added Storybook for Icons and Buttons for isolate testing.
 * First stabel version.

## 0.6.1
 * Added 10 new Icons for Crimes.
 * Added new feature - throwing custom props in gradient func attributes.
 * Added new feature - throwing manual filter for icon in filter func.
 * Improved Types Interfaces.
 * Minor fixes.

## 0.5.1
 * Added Presets Configuration for Icons.
 * Improved stability.
 * Minor fixes.

## 0.5.0
 * Added Gradient adding opportunity for Icons.

## 0.4.0
 * Added all ChristmasTown SVG Icons Pack (34 icons).

## 0.3.0
 * Created SVG Icons Color Constants. Partiarly rewritten API of 'fill' prop.

## 0.2.0
 * Created Buttons SVG Generator library.

 ## 0.1.0
 * Created Icons SVG Generator library.

## 0.0.1
 * Initial create.
