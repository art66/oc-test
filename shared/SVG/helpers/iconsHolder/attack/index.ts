import Bleeding from '../../../icons/attack/Bleeding'
import Blinded from '../../../icons/attack/Blinded'
import Blindfire from '../../../icons/attack/Blindfire'
import Burning from '../../../icons/attack/Burning'
import Chilled from '../../../icons/attack/Chilled'
import Crippled from '../../../icons/attack/Crippled'
import Contaminated from '../../../icons/attack/Contaminated'
import Demoralized from '../../../icons/attack/Demoralized'
import Disoriented from '../../../icons/attack/Disoriented'
import Emasculated from '../../../icons/attack/Emasculated'
import Eviscerated from '../../../icons/attack/Eviscerated'
import Frozen from '../../../icons/attack/Frozen'
import Hardened from '../../../icons/attack/Hardened'
import Hastened from '../../../icons/attack/Hastened'
import Hazardous from '../../../icons/attack/Hazardous'
import Maced from '../../../icons/attack/Maced'
import Motivated from '../../../icons/attack/Motivated'
import Paralyzed from '../../../icons/attack/Paralyzed'
import Poisoned from '../../../icons/attack/Poisoned'
import SevereBurning from '../../../icons/attack/SevereBurning'
import Sharpened from '../../../icons/attack/Sharpened'
import Sleeping from '../../../icons/attack/Sleeping'
import Slowed from '../../../icons/attack/Slowed'
import Smoked from '../../../icons/attack/Smoked'
import Spray from '../../../icons/attack/Spray'
import Storage from '../../../icons/attack/Storage'
import Strengthened from '../../../icons/attack/Strengthened'
import Stunned from '../../../icons/attack/Stunned'
import Suppressed from '../../../icons/attack/Suppressed'
import Weakened from '../../../icons/attack/Weakened'
import Withered from '../../../icons/attack/Withered'
import Hits from '../../../icons/attack/Hits'
import Concussed from '../../../icons/attack/Concussed'
import Gassed from '../../../icons/attack/Gassed'
import Loot from '../../../icons/global/Loot'
import Cross from '../../../icons/global/Cross'
import Pointer from '../../../icons/global/Pointer'
import Lacerate from '../../../icons/attack/Lacerate'
import Distraction from '../../../icons/attack/Distraction'
import Blast from '../../../icons/attack/Blast'
import Dimensiokinesis from '../../../icons/attack/Dimensiokinesis'
import Essokinesis from '../../../icons/attack/Essokinesis'
import Insanity from '../../../icons/attack/Insanity'
import Oneirokinesis from '../../../icons/attack/Oneirokinesis'

export default {
  Bleeding,
  Blinded,
  Blindfire,
  Burning,
  Chilled,
  Crippled,
  Contaminated,
  Demoralized,
  Disoriented,
  Emasculated,
  Eviscerated,
  Frozen,
  Hardened,
  Hastened,
  Hazardous,
  Maced,
  Motivated,
  Paralyzed,
  Poisoned,
  SevereBurning,
  Sharpened,
  Sleeping,
  Slowed,
  Smoked,
  Spray,
  Storage,
  Strengthened,
  Stunned,
  Suppressed,
  Weakened,
  Withered,
  Hits,
  Concussed,
  Gassed,
  Loot,
  Cross,
  Pointer,
  Lacerate,
  Distraction,
  Blast,
  Dimensiokinesis,
  Essokinesis,
  Insanity,
  Oneirokinesis
}
