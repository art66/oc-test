import CTFullscreen from '../../../icons/christmastown/СТFullscreen'
import CTHistory from '../../../icons/christmastown/CTHistory'
import CTLayers from '../../../icons/christmastown/CTLayers'
import CTIssue from '../../../icons/christmastown/CTIssue'
import CTObjects from '../../../icons/christmastown/CTObjects'
import CTParameters from '../../../icons/christmastown/CTParameters'
import CTBackups from '../../../icons/christmastown/CTBackups'
import CTAreas from '../../../icons/christmastown/CTAreas'
import CTBlocking from '../../../icons/christmastown/CTBlocking'
import CTPlayer from '../../../icons/christmastown/CTPlayer'
import CTGesture from '../../../icons/christmastown/CTGesture'
import CTCoordinates from '../../../icons/christmastown/CTCoordinates'
import AllMaps from '../../../icons/christmastown/CTAllMaps'
import CTStar from '../../../icons/christmastown/CTStar'
import CTNpc from '../../../icons/christmastown/CTNpc'
import CTNpcSanta from '../../../icons/christmastown/npc/CTNpcSanta'
import CTNpcGrinch from '../../../icons/christmastown/npc/CTNpcGrinch'
import CTNpcBeardedLady from '../../../icons/christmastown/npc/CTNpcBeardedLady'
import CTNpcPenguin from '../../../icons/christmastown/npc/CTNpcPenguin'
import CTNpcShortestMan from '../../../icons/christmastown/npc/CTNpcShortestMan'
import CTNpcSiameseTwins from '../../../icons/christmastown/npc/CTNpcSiameseTwins'
import CTNpcStrongMan from '../../../icons/christmastown/npc/CTNpcStrongMan'
import CTNpcTallestMan from '../../../icons/christmastown/npc/CTNpcTallestMan'
import CTNpcTattooedLady from '../../../icons/christmastown/npc/CTNpcTattooedLady'
import CTNpcTiger from '../../../icons/christmastown/npc/CTNpcTiger'
import CTNpcAstronaut from '../../../icons/christmastown/npc/CTNpcAstronaut'
import CTNpcBartender1 from '../../../icons/christmastown/npc/CTNpcBartender1'
import CTNpcBartender2 from '../../../icons/christmastown/npc/CTNpcBartender2'
import CTNpcBartender3 from '../../../icons/christmastown/npc/CTNpcBartender3'
import CTNpcBartender4 from '../../../icons/christmastown/npc/CTNpcBartender4'
import CTNpcBear from '../../../icons/christmastown/npc/CTNpcBear'
import CTNpcBeaver from '../../../icons/christmastown/npc/CTNpcBeaver'
import CTNpcBison from '../../../icons/christmastown/npc/CTNpcBison'
import CTNpcBoar from '../../../icons/christmastown/npc/CTNpcBoar'
import CTNpcBusinessman1 from '../../../icons/christmastown/npc/CTNpcBusinessman1'
import CTNpcBusinessman2 from '../../../icons/christmastown/npc/CTNpcBusinessman2'
import CTNpcCat from '../../../icons/christmastown/npc/CTNpcCat'
import CTNpcTourist from '../../../icons/christmastown/npc/CTNpcTourist'
import CTNpcChef from '../../../icons/christmastown/npc/CTNpcChef'
import CTNpcCleaner from '../../../icons/christmastown/npc/CTNpcCleaner'
import CTNpcConvict from '../../../icons/christmastown/npc/CTNpcConvict'
import CTNpcCowboy from '../../../icons/christmastown/npc/CTNpcCowboy'
import CTNpcDeer from '../../../icons/christmastown/npc/CTNpcDeer'
import CTNpcDoctor from '../../../icons/christmastown/npc/CTNpcDoctor'
import CTNpcDogWalker from '../../../icons/christmastown/npc/CTNpcDogWalker'
import CTNpcDrunkard1 from '../../../icons/christmastown/npc/CTNpcDrunkard1'
import CTNpcDrunkard2 from '../../../icons/christmastown/npc/CTNpcDrunkard2'
import CTNpcDrunkard3 from '../../../icons/christmastown/npc/CTNpcDrunkard3'
import CTNpcDuck from '../../../icons/christmastown/npc/CTNpcDuck'
import CTNpcElf from '../../../icons/christmastown/npc/CTNpcElf'
import CTNpcFarmer from '../../../icons/christmastown/npc/CTNpcFarmer'
import CTNpcFirefighter1 from '../../../icons/christmastown/npc/CTNpcFirefighter1'
import CTNpcFirefighter2 from '../../../icons/christmastown/npc/CTNpcFirefighter2'
import CTNpcFish from '../../../icons/christmastown/npc/CTNpcFish'
import CTNpcFisherman from '../../../icons/christmastown/npc/CTNpcFisherman'
import CTNpcFox from '../../../icons/christmastown/npc/CTNpcFox'
import CTNpcGardener from '../../../icons/christmastown/npc/CTNpcGardener'
import CTNpcGhost1 from '../../../icons/christmastown/npc/CTNpcGhost1'
import CTNpcGhost2 from '../../../icons/christmastown/npc/CTNpcGhost2'
import CTNpcGoblin1 from '../../../icons/christmastown/npc/CTNpcGoblin1'
import CTNpcGoblin2 from '../../../icons/christmastown/npc/CTNpcGoblin2'
import CTNpcGoblin3 from '../../../icons/christmastown/npc/CTNpcGoblin3'
import CTNpcHandyman1 from '../../../icons/christmastown/npc/CTNpcHandyman1'
import CTNpcHandyman2 from '../../../icons/christmastown/npc/CTNpcHandyman2'
import CTNpcHandyman3 from '../../../icons/christmastown/npc/CTNpcHandyman3'
import CTNpcHandyman4 from '../../../icons/christmastown/npc/CTNpcHandyman4'
import CTNpcHandyman5 from '../../../icons/christmastown/npc/CTNpcHandyman5'
import CTNpcHandyman6 from '../../../icons/christmastown/npc/CTNpcHandyman6'
import CTNpcHare from '../../../icons/christmastown/npc/CTNpcHare'
import CTNpcHedgehog from '../../../icons/christmastown/npc/CTNpcHedgehog'
import CTNpcHunter from '../../../icons/christmastown/npc/CTNpcHunter'
import CTNpcJackal from '../../../icons/christmastown/npc/CTNpcJackal'
import CTNpcLion from '../../../icons/christmastown/npc/CTNpcLion'
import CTNpcLynx from '../../../icons/christmastown/npc/CTNpcLynx'
import CTNpcMechanic1 from '../../../icons/christmastown/npc/CTNpcMechanic1'
import CTNpcMechanic2 from '../../../icons/christmastown/npc/CTNpcMechanic2'
import CTNpcMonster1 from '../../../icons/christmastown/npc/CTNpcMonster1'
import CTNpcMonster2 from '../../../icons/christmastown/npc/CTNpcMonster2'
import CTNpcMoose from '../../../icons/christmastown/npc/CTNpcMoose'
import CTNpcMummy from '../../../icons/christmastown/npc/CTNpcMummy'
import CTNpcMusician1 from '../../../icons/christmastown/npc/CTNpcMusician1'
import CTNpcMusician2 from '../../../icons/christmastown/npc/CTNpcMusician2'
import CTNpcMusician3 from '../../../icons/christmastown/npc/CTNpcMusician3'
import CTNpcMusician4 from '../../../icons/christmastown/npc/CTNpcMusician4'
import CTNpcMusician5 from '../../../icons/christmastown/npc/CTNpcMusician5'
import CTNpcNun from '../../../icons/christmastown/npc/CTNpcNun'
import CTNpcNurse from '../../../icons/christmastown/npc/CTNpcNurse'
import CTNpcOtter from '../../../icons/christmastown/npc/CTNpcOtter'
import CTNpcOwl from '../../../icons/christmastown/npc/CTNpcOwl'
import CTNpcPhotographer from '../../../icons/christmastown/npc/CTNpcPhotographer'
import CTNpcPoliceMan from '../../../icons/christmastown/npc/CTNpcPoliceMan'
import CTNpcPoliceWoman from '../../../icons/christmastown/npc/CTNpcPoliceWoman'
import CTNpcPriest from '../../../icons/christmastown/npc/CTNpcPriest'
import CTNpcPug from '../../../icons/christmastown/npc/CTNpcPug'
import CTNpcRaccoon from '../../../icons/christmastown/npc/CTNpcRaccoon'
import CTNpcSailor from '../../../icons/christmastown/npc/CTNpcSailor'
import CTNpcScarecrow from '../../../icons/christmastown/npc/CTNpcScarecrow'
import CTNpcScientist from '../../../icons/christmastown/npc/CTNpcScientist'
import CTNpcSeal from '../../../icons/christmastown/npc/CTNpcSeal'
import CTNpcSecretary1 from '../../../icons/christmastown/npc/CTNpcSecretary1'
import CTNpcSecretary2 from '../../../icons/christmastown/npc/CTNpcSecretary2'
import CTNpcSkeleton from '../../../icons/christmastown/npc/CTNpcSkeleton'
import CTNpcSnowman from '../../../icons/christmastown/npc/CTNpcSnowman'
import CTNpcSpearman from '../../../icons/christmastown/npc/CTNpcSpearman'
import CTNpcSports1 from '../../../icons/christmastown/npc/CTNpcSports1'
import CTNpcSports2 from '../../../icons/christmastown/npc/CTNpcSports2'
import CTNpcSports3 from '../../../icons/christmastown/npc/CTNpcSports3'
import CTNpcSports4 from '../../../icons/christmastown/npc/CTNpcSports4'
import CTNpcSquirrel from '../../../icons/christmastown/npc/CTNpcSquirrel'
import CTNpcStork from '../../../icons/christmastown/npc/CTNpcStork'
import CTNpcTransport1 from '../../../icons/christmastown/npc/CTNpcTransport1'
import CTNpcTransport2 from '../../../icons/christmastown/npc/CTNpcTransport2'
import CTNpcTransport3 from '../../../icons/christmastown/npc/CTNpcTransport3'
import CTNpcTransport4 from '../../../icons/christmastown/npc/CTNpcTransport4'
import CTNpcTransport5 from '../../../icons/christmastown/npc/CTNpcTransport5'
import CTNpcVampire from '../../../icons/christmastown/npc/CTNpcVampire'
import CTNpcWaitress1 from '../../../icons/christmastown/npc/CTNpcWaitress1'
import CTNpcWaitress2 from '../../../icons/christmastown/npc/CTNpcWaitress2'
import CTNpcWalrus from '../../../icons/christmastown/npc/CTNpcWalrus'
import CTNpcWeasel from '../../../icons/christmastown/npc/CTNpcWeasel'
import CTNpcWolf from '../../../icons/christmastown/npc/CTNpcWolf'
import CTNpcWolverine from '../../../icons/christmastown/npc/CTNpcWolverine'
import CTNpcWomanInRedDress1 from '../../../icons/christmastown/npc/CTNpcWomanInRedDress1'
import CTNpcWomanInRedDress2 from '../../../icons/christmastown/npc/CTNpcWomanInRedDress2'
import CTNpcZombie1 from '../../../icons/christmastown/npc/CTNpcZombie1'
import CTNpcZombie2 from '../../../icons/christmastown/npc/CTNpcZombie2'
import CTNpcZombie3 from '../../../icons/christmastown/npc/CTNpcZombie3'
import CTNpcZombie4 from '../../../icons/christmastown/npc/CTNpcZombie4'
import CTNpcZombie5 from '../../../icons/christmastown/npc/CTNpcZombie5'
import CTNpcKevinTheHoley from '../../../icons/christmastown/npc/CTNpcKevinTheHoley'
import CTNpcBobThePlank from '../../../icons/christmastown/npc/CTNpcBobThePlank'
import CTViewToggle from '../../../icons/christmastown/CTViewToggle'
import CTBrokenOrnament from '../../../icons/christmastown/minigames/CTBrokenOrnament'
import CTAlcohol from '../../../icons/christmastown/minigames/CTAlcohol'
import CTBucks from '../../../icons/christmastown/minigames/CTBucks'
import CTCandy from '../../../icons/christmastown/minigames/CTCandy'
import CTEnergyDrink from '../../../icons/christmastown/minigames/CTEnergyDrink'
import CTMoney from '../../../icons/christmastown/minigames/CTMoney'
import CTTangerine from '../../../icons/christmastown/minigames/CTTangerine'
import CTTightlyWhities from '../../../icons/christmastown/minigames/CTTightlyWhities'
import CTPlayers from '../../../icons/christmastown/CTPlayers'

export default {
  CTFullscreen,
  CTHistory,
  CTLayers,
  CTObjects,
  CTParameters,
  CTBackups,
  CTAreas,
  CTBlocking,
  CTIssue,
  CTPlayer,
  CTGesture,
  CTCoordinates,
  AllMaps,
  CTStar,
  CTNpc,
  CTNpcSanta,
  CTNpcGrinch,
  CTNpcBeardedLady,
  CTNpcPenguin,
  CTNpcShortestMan,
  CTNpcSiameseTwins,
  CTNpcStrongMan,
  CTNpcTallestMan,
  CTNpcTattooedLady,
  CTNpcTiger,
  CTViewToggle,
  CTPlayers,
  CTBrokenOrnament,
  CTAlcohol,
  CTBucks,
  CTCandy,
  CTEnergyDrink,
  CTMoney,
  CTTangerine,
  CTTightlyWhities,
  CTNpcAstronaut,
  CTNpcBartender1,
  CTNpcBartender2,
  CTNpcBartender3,
  CTNpcBartender4,
  CTNpcBear,
  CTNpcBeaver,
  CTNpcBison,
  CTNpcBoar,
  CTNpcBusinessman1,
  CTNpcBusinessman2,
  CTNpcCat,
  CTNpcTourist,
  CTNpcChef,
  CTNpcCleaner,
  CTNpcConvict,
  CTNpcCowboy,
  CTNpcDeer,
  CTNpcDoctor,
  CTNpcDogWalker,
  CTNpcDrunkard1,
  CTNpcDrunkard2,
  CTNpcDrunkard3,
  CTNpcDuck,
  CTNpcElf,
  CTNpcFarmer,
  CTNpcFirefighter1,
  CTNpcFirefighter2,
  CTNpcFish,
  CTNpcFisherman,
  CTNpcFox,
  CTNpcGardener,
  CTNpcGhost1,
  CTNpcGhost2,
  CTNpcGoblin1,
  CTNpcGoblin2,
  CTNpcGoblin3,
  CTNpcHandyman1,
  CTNpcHandyman2,
  CTNpcHandyman3,
  CTNpcHandyman4,
  CTNpcHandyman5,
  CTNpcHandyman6,
  CTNpcHare,
  CTNpcHedgehog,
  CTNpcHunter,
  CTNpcJackal,
  CTNpcLion,
  CTNpcLynx,
  CTNpcMechanic1,
  CTNpcMechanic2,
  CTNpcMonster1,
  CTNpcMonster2,
  CTNpcMoose,
  CTNpcMummy,
  CTNpcMusician1,
  CTNpcMusician2,
  CTNpcMusician3,
  CTNpcMusician4,
  CTNpcMusician5,
  CTNpcNun,
  CTNpcNurse,
  CTNpcOtter,
  CTNpcOwl,
  CTNpcPhotographer,
  CTNpcPoliceMan,
  CTNpcPoliceWoman,
  CTNpcPriest,
  CTNpcPug,
  CTNpcRaccoon,
  CTNpcSailor,
  CTNpcScarecrow,
  CTNpcScientist,
  CTNpcSeal,
  CTNpcSecretary1,
  CTNpcSecretary2,
  CTNpcSkeleton,
  CTNpcSnowman,
  CTNpcSpearman,
  CTNpcSports1,
  CTNpcSports2,
  CTNpcSports3,
  CTNpcSports4,
  CTNpcSquirrel,
  CTNpcStork,
  CTNpcTransport1,
  CTNpcTransport2,
  CTNpcTransport3,
  CTNpcTransport4,
  CTNpcTransport5,
  CTNpcVampire,
  CTNpcWaitress1,
  CTNpcWaitress2,
  CTNpcWalrus,
  CTNpcWeasel,
  CTNpcWolf,
  CTNpcWolverine,
  CTNpcWomanInRedDress1,
  CTNpcWomanInRedDress2,
  CTNpcZombie1,
  CTNpcZombie2,
  CTNpcZombie3,
  CTNpcZombie4,
  CTNpcZombie5,
  CTNpcKevinTheHoley,
  CTNpcBobThePlank
}
