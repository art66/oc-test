import Crop from '../../../icons/ics/Crop'
import Rotate from '../../../icons/ics/Rotate'
import Scale from '../../../icons/ics/Scale'

export default {
  Crop,
  Rotate,
  Scale
}
