
import Hub from '../../../icons/crimes/Hub'
import Fountain from '../../../icons/crimes/Fountain'
import Tombstone from '../../../icons/crimes/Tombstone'
import ArrowCrimes from '../../../icons/crimes/Arrow'
import ScrapYard from '../../../icons/crimes/ScrapYard'
import SecurityCamera from '../../../icons/crimes/SecurityCamera'
import Tide from '../../../icons/crimes/Tide'
import Guard from '../../../icons/crimes/Guard'
import CrimeStars from '../../../icons/crimes/Stars'
import CrimeCity from '../../../icons/crimes/City'
import StarContainer from '../../../icons/crimes/StarContainer'
import TargetCondition from '../../../icons/crimes/TargetCondition'
import Card from '../../../icons/crimes/Card'

export default {
  StarContainer,
  CrimeCity,
  Guard,
  ScrapYard,
  SecurityCamera,
  Tide,
  Fountain,
  Tombstone,
  ArrowCrimes,
  CrimeStars,
  Hub,
  TargetCondition,
  Card
}
