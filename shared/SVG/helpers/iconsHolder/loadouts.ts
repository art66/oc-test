import Equip from '../../icons/loadouts/Equip'
import Reset from '../../icons/loadouts/Reset'
import Edit from '../../icons/loadouts/Edit'
import Save from '../../icons/loadouts/Save'
import DropdownArrowPassive from '../../icons/loadouts/DropdownArrowPassive'
import DropdownArrowActive from '../../icons/loadouts/DropdownArrowActive'

export default {
  Equip,
  Reset,
  Edit,
  Save,
  DropdownArrowPassive,
  DropdownArrowActive
}
