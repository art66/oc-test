import BazaarBuy from '../../../icons/bazaar/Buy'
import BazaarView from '../../../icons/bazaar/View'
import BazaarCross from '../../../icons/bazaar/Cross'
import BazaarOpen from '../../../icons/bazaar/Open'
import BazaarClosed from '../../../icons/bazaar/Closed'
import Lock from '../../../icons/bazaar/Lock'

export default {
  BazaarBuy,
  BazaarView,
  BazaarCross,
  BazaarOpen,
  BazaarClosed,
  Lock
}
