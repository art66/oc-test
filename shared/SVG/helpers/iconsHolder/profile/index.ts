import Attack from '../../../icons/profile/Attack'
import AddToEnemyList from '../../../icons/profile/AddToEnemyList'
import AddToFriendList from '../../../icons/profile/AddToFriendList'
import Bust from '../../../icons/profile/Bust'
import Bail from '../../../icons/profile/Bail'
import InitiateChat from '../../../icons/profile/InitiateChat'
import SendMessage from '../../../icons/profile/SendMessage'
import PlaceBounty from '../../../icons/profile/PlaceBounty'
import Report from '../../../icons/profile/Report'
import Revive from '../../../icons/profile/Revive'
import SendMoney from '../../../icons/profile/SendMoney'
import InitiateTrade from '../../../icons/profile/InitiateTrade'
import ViewBazaar from '../../../icons/profile/ViewBazaar'
import ViewDisplayCabinet from '../../../icons/profile/ViewDisplayCabinet'
import PersonalStats from '../../../icons/profile/PersonalStats'
import CrossDisabled from '../../../icons/profile/CrossDisabled'
import HospitalStatus from '../../../icons/profile/userStatus/HospitalStatus'
import JailStatus from '../../../icons/profile/userStatus/JailStatus'
import LootStatus from '../../../icons/profile/userStatus/LootStatus'
import FederalJailStatus from '../../../icons/profile/userStatus/FederalJailStatus'
import TravelingStatus from '../../../icons/profile/userStatus/TravelingStatus'

export default {
  Attack,
  AddToEnemyList,
  AddToFriendList,
  Bust,
  Bail,
  InitiateChat,
  SendMessage,
  PlaceBounty,
  Report,
  Revive,
  SendMoney,
  InitiateTrade,
  ViewBazaar,
  ViewDisplayCabinet,
  PersonalStats,
  CrossDisabled,
  FederalJailStatus,
  HospitalStatus,
  JailStatus,
  TravelingStatus,
  LootStatus
}
