import DogTags from '../../../icons/calendar/DogTags'
import Easter from '../../../icons/calendar/Easter'
import Elimination from '../../../icons/calendar/Elimination'
import MrMissTorn from '../../../icons/calendar/MrMissTorn'

export default {
  DogTags,
  Easter,
  Elimination,
  MrMissTorn
}
