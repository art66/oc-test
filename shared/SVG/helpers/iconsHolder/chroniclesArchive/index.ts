import ReadStatus1 from '../../../icons/chroniclesArchive/ReadStatus1'
import ReadStatus2 from '../../../icons/chroniclesArchive/ReadStatus2'

export default {
  ReadStatus1,
  ReadStatus2
}
