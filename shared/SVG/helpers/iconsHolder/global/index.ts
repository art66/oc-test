import Gear from '../../../icons/global/Gear'
import Checked from '../../../icons/global/Checked'
import Pen from '../../../icons/global/Pen'
import Star from '../../../icons/global/Star'
import Share from '../../../icons/global/Share'
import Join from '../../../icons/global/Join'
import Trash from '../../../icons/global/Trash'
import Script from '../../../icons/global/Script'
import ArrowLeft from '../../../icons/global/ArrowLeft'
import ArrowRight from '../../../icons/global/ArrowRight'
import Avatar from '../../../icons/global/Avatar'
import Bug from '../../../icons/global/Bug'
import Close from '../../../icons/global/Close'
import Coordinates from '../../../icons/global/Coordinates'
import Copy from '../../../icons/global/Copy'
import CriticalError from '../../../icons/global/CriticalError'
import Cubes from '../../../icons/global/Cubes'
import Cut from '../../../icons/global/Cut'
import FullScreen from '../../../icons/global/FullScreen'
import History from '../../../icons/global/History'
import Layers from '../../../icons/global/Layers'
import Link from '../../../icons/global/Link'
import Loading from '../../../icons/global/Loading'
import Location from '../../../icons/global/Location'
import Map from '../../../icons/global/Map'
import Minus from '../../../icons/global/Minus'
import Move from '../../../icons/global/Move'
import Players from '../../../icons/global/Players'
import Plus from '../../../icons/global/Plus'
import RefreshCircular from '../../../icons/global/Refresh'
import Repeat from '../../../icons/global/Repeat'
import Save from '../../../icons/global/Save'
import Search from '../../../icons/global/Search'
import Settings from '../../../icons/global/Settings'
import Snow from '../../../icons/global/Snow'
import Testing from '../../../icons/global/Testing'
import Undo from '../../../icons/global/Undo'
import Redo from '../../../icons/global/Redo'
import Chat from '../../../icons/global/Chat'
import WalkingMan from '../../../icons/global/WalkingMan'
import Checkpoint from '../../../icons/global/Checkpoint'
import Person from '../../../icons/global/Person'
import Back from '../../../icons/global/Back'
import ArrowLeftThin from '../../../icons/global/ArrowLeftThin'
import ArrowRightThin from '../../../icons/global/ArrowRightThin'
import Loot from '../../../icons/global/Loot'
import Gassed from '../../../icons/global/Gassed'
import Cross from '../../../icons/global/Cross'
import Pointer from '../../../icons/global/Pointer'
import OnlineStatus from '../../../icons/global/OnlineStatus'
import View from '../../../icons/global/View'
import ArrowLeftSuperThin from '../../../icons/global/ArrowLeftSuperThin'
import ArrowRightSuperThin from '../../../icons/global/ArrowRightSuperThin'
import PlusWide from '../../../icons/global/PlusWide'
import Image from '../../../icons/global/Image'
import TableView from '../../../icons/global/TableView'
import ListView from '../../../icons/global/ListView'

export default {
  Gear,
  Checked,
  Pen,
  Star,
  Share,
  Join,
  Trash,
  Script,
  ArrowLeft,
  ArrowRight,
  Avatar,
  Bug,
  Close,
  Coordinates,
  Copy,
  CriticalError,
  Cubes,
  Cut,
  FullScreen,
  History,
  Layers,
  Link,
  Loading,
  Location,
  Map,
  Minus,
  Move,
  Players,
  Plus,
  RefreshCircular,
  Repeat,
  Save,
  Search,
  Settings,
  Snow,
  Testing,
  Undo,
  Redo,
  Chat,
  WalkingMan,
  Checkpoint,
  Person,
  Back,
  ArrowLeftThin,
  ArrowRightThin,
  Loot,
  Gassed,
  Cross,
  Pointer,
  OnlineStatus,
  View,
  ArrowLeftSuperThin,
  ArrowRightSuperThin,
  PlusWide,
  Image,
  TableView,
  ListView
}
