import ActiveBonus from '../../../icons/stockMarket/ActiveBonus'
import PassiveBonus from '../../../icons/stockMarket/PassiveBonus'
import InProgressBonus from '../../../icons/stockMarket/InProgressBonus'

export default {
  ActiveBonus,
  PassiveBonus,
  InProgressBonus
}
