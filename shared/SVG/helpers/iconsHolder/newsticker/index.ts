import NTAnnouncement from '../../../icons/newsticker/Announcement'
import NTFaction from '../../../icons/newsticker/Faction'
import NTNews from '../../../icons/newsticker/News'
import NTStar from '../../../icons/newsticker/Star'
import NTTutorials from '../../../icons/newsticker/Tutorials'
import NTGameClosed from '../../../icons/newsticker/GameClosed'

export default {
  NTAnnouncement,
  NTFaction,
  NTNews,
  NTStar,
  NTTutorials,
  NTGameClosed
}
