import AddEnemy from '../../../icons/topPageLinks/AddEnemy'
import AddListing from '../../../icons/topPageLinks/AddListing'
import AddPoll from '../../../icons/topPageLinks/AddPoll'
import Awards from '../../../icons/topPageLinks/Awards'
import ApplyToBeAHelper from '../../../icons/topPageLinks/ApplyToBeAHelper'
import AllSubmissions from '../../../icons/topPageLinks/AllSubmissions'
import AmmoLocker from '../../../icons/topPageLinks/AmmoLocker'
import Bio from '../../../icons/topPageLinks/Bio'
import Calendar from '../../../icons/topPageLinks/Calendar'
import Bazaar from '../../../icons/topPageLinks/Bazaar'
// import Back2 from '../../../icons/topPageLinks/Back' - deprecated use global "Back" instead!
import CompanyProfile from '../../../icons/topPageLinks/CompanyProfile'
import ChristmasTown from '../../../icons/topPageLinks/ChristmasTown'
import CriminalRecord from '../../../icons/topPageLinks/CriminalRecord'
import City from '../../../icons/topPageLinks/City'
import ExchangesAllow from '../../../icons/topPageLinks/ExchangesAllow'
import DisplayCabinet from '../../../icons/topPageLinks/DisplayCabinet'
import Events from '../../../icons/topPageLinks/Events'
import Delete from '../../../icons/topPageLinks/Delete'
import FortuneTeller from '../../../icons/topPageLinks/FortuneTeller'
import Faction from '../../../icons/topPageLinks/Faction'
import Forum from '../../../icons/topPageLinks/Forum'
import ExchangesDisallow from '../../../icons/topPageLinks/ExchangesDisallow'
import HighLow from '../../../icons/topPageLinks/HighLow'
import Hunting from '../../../icons/topPageLinks/Hunting'
import Home from '../../../icons/topPageLinks/Home'
import GetPromoted from '../../../icons/topPageLinks/GetPromoted'
import ItemsAdd from '../../../icons/topPageLinks/ItemsAdd'
import JobListings from '../../../icons/topPageLinks/JobListings'
import ItemMarket from '../../../icons/topPageLinks/ItemMarket'
import Items from '../../../icons/topPageLinks/Items'
import Legal from '../../../icons/topPageLinks/Legal'
import LastGames from '../../../icons/topPageLinks/LastGames'
import Laptop from '../../../icons/topPageLinks/Laptop'
import LastRolls from '../../../icons/topPageLinks/LastRolls'
import LivePosts from '../../../icons/topPageLinks/LivePosts'
import Mail from '../../../icons/topPageLinks/Mail'
import Log from '../../../icons/topPageLinks/Log'
// import Link from '../../../icons/topPageLinks/Link' - deprecated use global "Link" instead!
import ManageItems from '../../../icons/topPageLinks/ManageItems'
import MapEditor from '../../../icons/topPageLinks/MapEditor'
import Missions from '../../../icons/topPageLinks/Missions'
import ModerateMatches from '../../../icons/topPageLinks/ModerateMatches'
import NewsTicker from '../../../icons/topPageLinks/NewsTicker'
import Mods from '../../../icons/topPageLinks/Mods'
import Newsletter from '../../../icons/topPageLinks/Newsletter'
import MyMaps from '../../../icons/topPageLinks/MyMaps'
import PointsBuilding from '../../../icons/topPageLinks/PointsBuilding'
import PointsMarket from '../../../icons/topPageLinks/PointsMarket'
import Personalize from '../../../icons/topPageLinks/Personalize'
import OverseasBank from '../../../icons/topPageLinks/OverseasBank'
import PostSticky from '../../../icons/topPageLinks/PostSticky'
import PostOffice from '../../../icons/topPageLinks/PostOffice'
import Popout from '../../../icons/topPageLinks/Popout'
import Portfolio from '../../../icons/topPageLinks/Portfolio'
import Quit from '../../../icons/topPageLinks/Quit'
import PreviousWinners from '../../../icons/topPageLinks/PreviousWinners'
import RadioButtonChecked from '../../../icons/topPageLinks/RadioButtonChecked'
import RadioButtonUnchecked from '../../../icons/topPageLinks/RadioButtonUnchecked'
import RecruitCitizens from '../../../icons/topPageLinks/RecruitCitizens'
import Refresh from '../../../icons/topPageLinks/Refresh'
import Rehab from '../../../icons/topPageLinks/Rehab'
import RecentAttacks from '../../../icons/topPageLinks/RecentAttacks'
import Reporters from '../../../icons/topPageLinks/Reporters'
import RentalMarket from '../../../icons/topPageLinks/RentalMarket'
import ReportingPanel from '../../../icons/topPageLinks/ReportingPanel'
import SellingMarket from '../../../icons/topPageLinks/SellingMarket'
import SendReport from '../../../icons/topPageLinks/SendReport'
import StartACompany from '../../../icons/topPageLinks/StartACompany'
import Send from '../../../icons/topPageLinks/Send'
import Statistics from '../../../icons/topPageLinks/Statistics'
import StockExchange from '../../../icons/topPageLinks/StockExchange'
import TicketsBought from '../../../icons/topPageLinks/TicketsBought'
import TellYourStory from '../../../icons/topPageLinks/TellYourStory'
import TrackMatchBpp from '../../../icons/topPageLinks/TrackMatchBpp'
import Team from '../../../icons/topPageLinks/Team'
import Timeout from '../../../icons/topPageLinks/Timeout'
import Trades from '../../../icons/topPageLinks/Trades'
import TokenShop from '../../../icons/topPageLinks/TokenShop'
import Tutorial from '../../../icons/topPageLinks/Tutorial'
import UncheckAll from '../../../icons/topPageLinks/UncheckAll'
import UploadNewImage from '../../../icons/topPageLinks/UploadNewImage'
import ViewProposals from '../../../icons/topPageLinks/ViewProposals'
import ViewListings from '../../../icons/topPageLinks/ViewListings'
import ViewHide from '../../../icons/topPageLinks/ViewHide'
import ViewShow from '../../../icons/topPageLinks/ViewShow'
import ViewToggleList from '../../../icons/topPageLinks/ViewToggleList'
import ViewToggleThumbnails from '../../../icons/topPageLinks/ViewToggleThumbnails'
import ViewYourBugs from '../../../icons/topPageLinks/ViewYourBugs'
import AttackTurn from '../../../icons/topPageLinks/AttackTurn'
import Parameditor from '../../../icons/topPageLinks/Parameditor'
import CrimesHub from '../../../icons/topPageLinks/CrimesHub'

export default {
  AddEnemy,
  AddListing,
  AddPoll,
  Awards,
  ApplyToBeAHelper,
  AllSubmissions,
  AmmoLocker,
  Bio,
  Calendar,
  Bazaar,
  // Back2,
  ChristmasTown,
  CompanyProfile,
  // ChristmasTown2,
  CriminalRecord,
  City,
  ExchangesAllow,
  DisplayCabinet,
  Events,
  Delete,
  FortuneTeller,
  Faction,
  Forum,
  ExchangesDisallow,
  HighLow,
  Hunting,
  Home,
  GetPromoted,
  ItemsAdd,
  JobListings,
  ItemMarket,
  Items,
  Legal,
  LastGames,
  Laptop,
  LastRolls,
  LivePosts,
  Mail,
  Log,
  // Link2,
  ManageItems,
  MapEditor,
  Missions,
  ModerateMatches,
  NewsTicker,
  Mods,
  Newsletter,
  MyMaps,
  PointsBuilding,
  PointsMarket,
  Personalize,
  OverseasBank,
  PostSticky,
  PostOffice,
  Popout,
  Portfolio,
  Quit,
  PreviousWinners,
  RadioButtonChecked,
  RadioButtonUnchecked,
  RecruitCitizens,
  Rehab,
  Refresh,
  RecentAttacks,
  Reporters,
  RentalMarket,
  ReportingPanel,
  SellingMarket,
  SendReport,
  StartACompany,
  Send,
  Statistics,
  StockExchange,
  TicketsBought,
  TellYourStory,
  TrackMatchBpp,
  Team,
  Timeout,
  Trades,
  TokenShop,
  Tutorial,
  UncheckAll,
  UploadNewImage,
  ViewProposals,
  ViewListings,
  ViewHide,
  ViewShow,
  ViewToggleList,
  ViewToggleThumbnails,
  ViewYourBugs,
  AttackTurn,
  Parameditor,
  CrimesHub
}
