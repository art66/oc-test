import RHAccountclosure from '../../../icons/recentHistory/AccountClosure'
import RHAccountcreation from '../../../icons/recentHistory/AccountCreation'
import RHAccountrecovery from '../../../icons/recentHistory/AccountRecovery'
import RHAmmo from '../../../icons/recentHistory/Ammo'
import RHAttacking from '../../../icons/recentHistory/Attacking'
import RHAuctions from '../../../icons/recentHistory/Auctions'
import RHAuthentication from '../../../icons/recentHistory/Authentication'
import RHAwards from '../../../icons/recentHistory/Awards'
import RHBank from '../../../icons/recentHistory/Bank'
import RHBail from '../../../icons/recentHistory/Bail'
import RHBazaars from '../../../icons/recentHistory/Bazaars'
import RHBooks from '../../../icons/recentHistory/Books'
import RHBounties from '../../../icons/recentHistory/Bounties'
import RHCaptcha from '../../../icons/recentHistory/Captcha'
import RHCasino from '../../../icons/recentHistory/Casino'
import RHChat from '../../../icons/recentHistory/Chat'
import RHChristmastown from '../../../icons/recentHistory/ChristmasTown'
import RHChurch from '../../../icons/recentHistory/Church'
import RHCityfinds from '../../../icons/recentHistory/CityFinds'
import RHCompany from '../../../icons/recentHistory/Company'
import RHCompetitions from '../../../icons/recentHistory/Competitions'
import RHCrimes from '../../../icons/recentHistory/Crimes'
import RHDisplaycabinet from '../../../icons/recentHistory/DisplayCabinet'
import RHDonator from '../../../icons/recentHistory/Donator'
import RHDrugs from '../../../icons/recentHistory/Drugs'
import RHDump from '../../../icons/recentHistory/Dump'
import RHEnemieslist from '../../../icons/recentHistory/EnemiesList'
import RHEducation from '../../../icons/recentHistory/Education'
import RHEquipping from '../../../icons/recentHistory/Equipping'
import RHFaction from '../../../icons/recentHistory/Faction'
import RHEvents from '../../../icons/recentHistory/Events'
import RHForums from '../../../icons/recentHistory/Forums'
import RHFriendslist from '../../../icons/recentHistory/FriendsList'
import RHHalloweenbasket from '../../../icons/recentHistory/HalloweenBasket'
import RHGym from '../../../icons/recentHistory/Gym'
import RHHospital from '../../../icons/recentHistory/Hospital'
import RHIgnorelist from '../../../icons/recentHistory/IgnoreList'
import RHImages from '../../../icons/recentHistory/Images'
import RHItemmarket from '../../../icons/recentHistory/ItemMarket'
import RHItemsending from '../../../icons/recentHistory/ItemSending'
import RHItemuse from '../../../icons/recentHistory/ItemUse'
import RHItems from '../../../icons/recentHistory/Items'
import RHAdverts from '../../../icons/recentHistory/Adverts'
import RHJail from '../../../icons/recentHistory/Jail'
import RHMerits from '../../../icons/recentHistory/Merits'
import RHJob from '../../../icons/recentHistory/Job'
import RHLevelup from '../../../icons/recentHistory/LevelUp'
import RHLoan from '../../../icons/recentHistory/Loan'
import RHMessages from '../../../icons/recentHistory/Messages'
import RHMisc from '../../../icons/recentHistory/Misc'
import RHMods from '../../../icons/recentHistory/Mods'
import RHMoney from '../../../icons/recentHistory/Money'
import RHMoneysending from '../../../icons/recentHistory/MoneySending'
import RHMuseum from '../../../icons/recentHistory/Museum'
import RHNewsletters from '../../../icons/recentHistory/Newsletter'
import RHMissions from '../../../icons/recentHistory/Missions'
import RHNotes from '../../../icons/recentHistory/Notes'
import RHParcels from '../../../icons/recentHistory/Parcels'
import RHPointsmarket from '../../../icons/recentHistory/PointsMarket'
import RHPointsbuilding from '../../../icons/recentHistory/PointsBuilding'
import RHPreferences from '../../../icons/recentHistory/Preferences'
import RHProperty from '../../../icons/recentHistory/Property'
import RHRacing from '../../../icons/recentHistory/Racing'
import RHReferrals from '../../../icons/recentHistory/Referrals'
import RHReporting from '../../../icons/recentHistory/Reporting'
import RHSeasonalgift from '../../../icons/recentHistory/SeasonalGift'
import RHShops from '../../../icons/recentHistory/Shops'
import RHStaff from '../../../icons/recentHistory/Staff'
import RHStocks from '../../../icons/recentHistory/Stocks'
import RHTokenshop from '../../../icons/recentHistory/TokenShop'
import RHTrades from '../../../icons/recentHistory/Trades'
import RHTravel from '../../../icons/recentHistory/Travel'
import RHVault from '../../../icons/recentHistory/Vault'
import RHViruses from '../../../icons/recentHistory/Viruses'
import CopyLog from '../../../icons/recentHistory/CopyLog'
import FollowLink from '../../../icons/recentHistory/FollowLink'
import ViewLog from '../../../icons/recentHistory/ViewLog'
import Lock from '../../../icons/bazaar/Lock'

export default {
  RHAccountclosure,
  RHAccountcreation,
  RHAccountrecovery,
  RHAmmo,
  RHAttacking,
  RHAuctions,
  RHAuthentication,
  RHAwards,
  RHBank,
  RHBail,
  RHBazaars,
  RHBooks,
  RHBounties,
  RHCaptcha,
  RHCasino,
  RHChat,
  RHChristmastown,
  RHChurch,
  RHCityfinds,
  RHCompany,
  RHCompetitions,
  RHCrimes,
  RHDisplaycabinet,
  RHDonator,
  RHDrugs,
  RHDump,
  RHEnemieslist,
  RHEducation,
  RHEquipping,
  RHFaction,
  RHEvents,
  RHForums,
  RHFriendslist,
  RHHalloweenbasket,
  RHGym,
  RHHospital,
  RHIgnorelist,
  RHImages,
  RHItemmarket,
  RHItemsending,
  RHItemuse,
  RHItems,
  RHAdverts,
  RHJail,
  RHLevelup,
  RHLoan,
  RHMerits,
  RHJob,
  RHMessages,
  RHMisc,
  RHMods,
  RHMoney,
  RHMoneysending,
  RHMuseum,
  RHNewsletters,
  RHMissions,
  RHNotes,
  RHParcels,
  RHPointsmarket,
  RHPointsbuilding,
  RHPreferences,
  RHProperty,
  RHRacing,
  RHReferrals,
  RHReporting,
  RHSeasonalgift,
  RHShops,
  RHStaff,
  RHStocks,
  RHTokenshop,
  RHTrades,
  RHTravel,
  RHVault,
  RHViruses,
  CopyLog,
  FollowLink,
  ViewLog,
  Lock
}
