// TODO: think about how to load chunk independently.
// One by one correspondent to the app on load.
// const iconsHolder = {
//   attack: import(/* webpackChunkName: "attackSVGIcons" */ './attack'),
//   bazaar: import(/* webpackChunkName: "bazaarSVGIcons" */ './bazaar'),
//   calendar: import(/* webpackChunkName: "calendarSVGIcons" */ './calendar'),
//   christmastown: import(/* webpackChunkName: "christmastownSVGIcons" */ './christmastown'),
//   crimes: import(/* webpackChunkName: "crimesSVGIcons" */ './crimes'),
//   global: import(/* webpackChunkName: "globalSVGIcons" */ './global'),
//   ics: import(/* webpackChunkName: "icsSVGIcons" */ './ics'),
//   profile: import(/* webpackChunkName: "profileSVGIcons" */ './profile'),
//   sidebar: import(/* webpackChunkName: "sidebarSVGIcons" */ './sidebar'),
//   topPageLinks: import(/* webpackChunkName: "topPageLinksSVGIcons" */ './topPageLinks')
// }

// export default iconsHolder

import attack from './attack'
import bazaar from './bazaar'
import calendar from './calendar'
import christmastown from './christmastown'
import crimes from './crimes'
import global from './global'
import ics from './ics'
import profile from './profile'
import sidebar from './sidebar'
import topPageLinks from './topPageLinks'
import chroniclesArchive from './chroniclesArchive'
import factionPotentialList from './faction-potential-list'
import recentHistory from './recentHistory'
import loadouts from './loadouts'
import newsTicker from './newsticker'
import header from './header'
import stockMarket from './stockMarket'

const iconsHolder = {
  ...attack,
  ...bazaar,
  ...calendar,
  ...christmastown,
  ...crimes,
  ...global,
  ...ics,
  ...profile,
  ...sidebar,
  ...topPageLinks,
  ...chroniclesArchive,
  ...factionPotentialList,
  ...recentHistory,
  ...loadouts,
  ...newsTicker,
  ...header,
  ...stockMarket
}

export default iconsHolder
