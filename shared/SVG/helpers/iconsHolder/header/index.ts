import Menu from '../../../icons/header/Menu'
import Contact from '../../../icons/header/Contact'
import Credits from '../../../icons/header/Credits'
import Discord from '../../../icons/header/Discord'
import Register from '../../../icons/header/Register'
import Wiki from '../../../icons/header/Wiki'
import Login from '../../../icons/header/Login'
import AdvancedSearch from '../../../icons/header/AdvancedSearch'
import AvatarCircle from '../../../icons/header/AvatarCircle'
import Profile from '../../../icons/header/Profile'
import DarkMode from '../../../icons/header/DarkMode'
import Headlines from '../../../icons/header/Headlines'
import Logout from '../../../icons/header/Logout'
import ServerInfo from '../../../icons/header/ServerInfo'
import DesktopMode from '../../../icons/header/DesktopMode'
import Clock from '../../../icons/header/Clock'
import ActivityLog from '../../../icons/header/ActivityLog'

export default {
  Menu,
  Contact,
  Credits,
  Discord,
  Register,
  Wiki,
  Login,
  AdvancedSearch,
  AvatarCircle,
  Profile,
  DarkMode,
  Headlines,
  Logout,
  ServerInfo,
  DesktopMode,
  Clock,
  ActivityLog
}
