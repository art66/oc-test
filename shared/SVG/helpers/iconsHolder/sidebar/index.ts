import AwardsMedal from '../../../icons/sidebar/AwardsMedal' // nested from the global one
import Casino from '../../../icons/sidebar/Casino'
// import ChristmasTown from '../../../icons/sidebar/ChristmasTown' // nested from the global one
import ChristmasTown2 from '../../../icons/sidebar/ChristmasTownSantaHat'
import Competitions from '../../../icons/sidebar/Competitions'
// import City from '../../../icons/sidebar/City' // nested from the global one
// import DogTags from '../../../icons/sidebar/DogTags' // nested from the calendar
import Admin from '../../../icons/sidebar/Admin'
import Crimes from '../../../icons/sidebar/Crimes'
import Education from '../../../icons/sidebar/Education'
import EasterEgg from '../../../icons/sidebar/EasterEgg'
import FollowLinks from '../../../icons/sidebar/FollowLinks'
// import Elimination from '../../../icons/sidebar/Elimination' // nested from the top page links
// import Events from '../../../icons/sidebar/Events' // nested from the top page links
// import Faction from '../../../icons/sidebar/Faction' // nested from the top page links
import Enemies from '../../../icons/sidebar/Enemies'
import Forums from '../../../icons/sidebar/Forums'
import Friends from '../../../icons/sidebar/Friends'
import HallOfFame from '../../../icons/sidebar/HallOfFame'
import Gym from '../../../icons/sidebar/Gym'
import Halloween2 from '../../../icons/sidebar/HalloweenGrave'
import Halloween3 from '../../../icons/sidebar/HalloweenPumpkin'
import Halloween from '../../../icons/sidebar/HalloweenWitch'
// import Home from '../../../icons/sidebar/Home' // nested from the global
// import Items from '../../../icons/sidebar/Items' // nested from the global
import Hospital from '../../../icons/sidebar/Hospital'
import Jail from '../../../icons/sidebar/Jail'
import Mailbox from '../../../icons/sidebar/Mailbox'
// import MrMissTorn from '../../../icons/sidebar/MrMissTorn' // nested from the calendar
import Job from '../../../icons/sidebar/Job'
// import Missions from '../../../icons/sidebar/Missions' nested from the top page links
import Newspaper from '../../../icons/sidebar/Newspaper'
import Property from '../../../icons/sidebar/Property'
import Staff from '../../../icons/sidebar/Staff'
import Rules from '../../../icons/sidebar/Rules'
// import RecruitCitizens from '../../../icons/sidebar/RecruitCitizens' // nested from the top page links
import Energy from '../../../icons/sidebar/Energy'
import Life from '../../../icons/sidebar/Life'
import Chain from '../../../icons/sidebar/Chain'
import Happy from '../../../icons/sidebar/Happy'
import Nerve from '../../../icons/sidebar/Nerve'
import Ticket from '../../../icons/sidebar/Ticket'
import AgentStanding from '../../../icons/sidebar/AgentStanding'
import Cash from '../../../icons/sidebar/Cash'
import CasinoTokens from '../../../icons/sidebar/CasinoTokens'
import Difficulty from '../../../icons/sidebar/Difficulty'
import EnergyPoint from '../../../icons/sidebar/EnergyPoint'
import FactionRespect from '../../../icons/sidebar/FactionRespect'
import JobPoints from '../../../icons/sidebar/JobPoints'
import Level from '../../../icons/sidebar/Level'
import Merits from '../../../icons/sidebar/Merits'
import MissionPoints from '../../../icons/sidebar/MissionPoints'
import NervePoint from '../../../icons/sidebar/NervePoint'
import Points from '../../../icons/sidebar/Points'
import Refill from '../../../icons/sidebar/Refill'
import CansEvent from '../../../icons/sidebar/CansEvent'

export default {
  Casino,
  ChristmasTown2,
  Competitions,
  Admin,
  Crimes,
  Education,
  EasterEgg,
  Enemies,
  Forums,
  Friends,
  HallOfFame,
  Gym,
  Halloween2,
  Halloween3,
  Halloween,
  Hospital,
  Jail,
  Mailbox,
  Job,
  Newspaper,
  Property,
  Staff,
  Rules,
  AwardsMedal,
  FollowLinks,
  Energy,
  Life,
  Chain,
  Happy,
  Nerve,
  Ticket,
  AgentStanding,
  Cash,
  CasinoTokens,
  Difficulty,
  EnergyPoint,
  FactionRespect,
  JobPoints,
  Level,
  Merits,
  MissionPoints,
  NervePoint,
  Points,
  Refill,
  CansEvent
}
