import CTESloped from '../buttons/ChristmasTown/Sloped'
import CTERegular from '../buttons/ChristmasTown/Regular'

const buttonsHolder = {
  CTESloped,
  CTERegular
}

export default buttonsHolder
