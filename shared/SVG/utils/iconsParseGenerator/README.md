# Parser for SVG Icons Library

How to use:
  - Just go to the SVG folder (here) and run the CLI command then: `yarn uploadSVG {your_upload_path} {your_target_path} {pattern:some_pattern} {flags:some_flags}`.

  Example:
    `yarn uploadSVG /Users/sviatoslav/Downloads/svg_to_upload /Users/sviatoslav/Desktop/Torn/react-apps/shared/SVG/icons/topPageLinks 'pattern:(<path.*?\/>)|(<polygon.*?\/>)' flags:im`

  FAQ:
   - What flags are reqired here?
   - Just only two: `your_upload_path` and `your_target_path`, the last couple is non-required and can use constants inside.

P.S. Due to changes in the library for SVG you need manual specify the path to iconsHolder for required app. You can do it here: https://i.imgur.com/8mndYRp.png .
It will be fixed later.
