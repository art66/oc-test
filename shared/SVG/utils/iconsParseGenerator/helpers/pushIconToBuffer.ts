const processHandlers = require('../core/processHandlers.ts')

const { ENCODING_TYPE, ICONS_HOLDER_PATHS } = require('../constants/index.ts')

const pushIconToBuffer = (itemsToInsert, element) => {
  const { getName } = processHandlers
  const { iconPath } = ICONS_HOLDER_PATHS
  const { imports = [], iconsNames = [] } = itemsToInsert
  const iconName = getName(element)
  const createIconImportBuffer = Buffer.from(`import ${iconName} from '../../..${iconPath}/${iconName}'`, ENCODING_TYPE)
  const createIconNameBuffer = Buffer.from(`  ${iconName},`, ENCODING_TYPE)

  imports.push(createIconImportBuffer)
  iconsNames.push(createIconNameBuffer)
}

module.exports = pushIconToBuffer
