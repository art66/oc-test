const fileContentMaker = require('../core/fileContentMaker.ts')
const processHandlers = require('../core/processHandlers.ts')

const { ROOT_DIR } = require('../constants/index.ts')

const getIconProps = (element, contents) => {
  const { getName, getContent } = processHandlers

  const iconName = getName(element)
  const iconContent = fileContentMaker(iconName, getContent(contents))
  const iconDir = `${ROOT_DIR}/${iconName}`
  const iconToWrite = `${iconDir}/index.tsx`

  return {
    iconContent,
    iconDir,
    iconToWrite
  }
}

module.exports = getIconProps
