const fs = require('fs')

const writeIconFile = require('./core/writeIconFile.ts')
const addIconsBufferToHolder = require('./core/addIconsBufferToHolder.ts')
const getIconProps = require('./helpers/getIconProps.ts')
const pushIconToBuffer = require('./helpers/pushIconToBuffer.ts')
const { compilationStartAlert, compilationFinishAlert } = require('./helpers/alerts.ts')

const { UPLOAD_DIR, ROOT_DIR, ENCODING_TYPE } = require('./constants/index.ts')

const itemsToInsertBuffer = {
  imports: [],
  iconsNames: []
}

const writeIcons = () => {
  fs.readdirSync(UPLOAD_DIR).forEach(element => {
    fs.readFile(`${UPLOAD_DIR}/${element}`, ENCODING_TYPE, (err, contents) => {
      if (err) {
        console.log('\x1b[31m%s\x1b[0m', `Upload Error! Check paths, please! Error: ${err}`)
        return
      }

      if (element && !element.includes('.svg')) {
        console.log('\x1b[31m%s\x1b[0m', `Non-SVG element found! Revert! File Name: ${element}`)
        return
      }

      const { iconDir } = getIconProps(element, contents)

      if (fs.existsSync(iconDir)) {
        console.log('\x1b[31m%s\x1b[0m', `Icon is already in folder! File writing is failed. ${iconDir}`)
        return
      }

      writeIconFile(element, contents)
      pushIconToBuffer(itemsToInsertBuffer, element)
    })
  })

  addIconsBufferToHolder(itemsToInsertBuffer)
}

compilationStartAlert()
writeIcons()
compilationFinishAlert()
