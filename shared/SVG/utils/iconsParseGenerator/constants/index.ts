const path = require('path')

module.exports = {
  UPLOAD_DIR: process.argv.find(arg => arg.includes('/svg_to_upload')),
  ROOT_DIR: process.argv.find(arg => arg.includes('shared/SVG/icons')),
  MATCH_PATTERN: {
    pattern:
      (process.argv.some(arg => arg.includes('pattern:')) &&
        process.argv.find(arg => arg.includes('pattern:')).substr(8)) ||
      '(<path.*?/>)|(<polygon.*?/>)',
    flags:
      (process.argv.some(arg => arg.includes('flags:')) &&
        process.argv.find(arg => arg.includes('flags:')).substr(6)) ||
      'i'
  },
  ICONS_HOLDER_PATHS: {
    holderPath: path.resolve(__dirname, '../../../helpers/iconsHolder/temporary/index.ts'),
    iconPath: process.argv.find(arg => arg.includes('shared/SVG/icons')).match(/\/icons.*/gi)[0],
    lineFixers: {
      onBeginning: `// --------------------------------------
// !!!FRESH ADDED!!! (Rename me, please!)
// --------------------------------------`,
      onEnd: '\f',
      onNew: '  ,// fresh new icon names below! Check them please asap! Don\'t forget to add in storybook!'
    }
  },
  ENCODING_TYPE: 'utf8'
}
