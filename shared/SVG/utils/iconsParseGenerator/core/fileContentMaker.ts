const fileContentMaker = (name, shape) => `import React from 'react'

const ${name} = {
  shape: (
    ${shape}
  )
}

export default ${name}
`

module.exports = fileContentMaker
