const fs = require('fs')

const {
  ENCODING_TYPE,
  ICONS_HOLDER_PATHS: { holderPath, lineFixers }
} = require('../constants/index.ts')

const addIconsBufferToHolder = (bufferToInsert = {}) => {
  fs.readFile(holderPath, ENCODING_TYPE, (err, contents) => {
    if (err) {
      console.log(err)
      throw err
    }

    const fd = fs.openSync(holderPath, 'w')

    const makeBuffer = (buffer, type) => {
      const { onBeginning, onEnd, onNew } = lineFixers
      const writableBuffer = [...buffer]

      if (type === 'imports') {
        writableBuffer.unshift(onBeginning)
        writableBuffer.push(onEnd)
      } else {
        writableBuffer.unshift(onNew)
      }

      return writableBuffer
    }

    const findlineToInsert = (data, types) => {
      if (types === 'imports') {
        return data.findIndex(line => line === 'const iconsHolder = {')
      }

      return data.findIndex(line => line === '}')
    }

    const filePayload = () => {
      const data = contents.toString().split('\n')

      for (const buffer in bufferToInsert) {
        data.splice(findlineToInsert(data, buffer), 0, ...makeBuffer(bufferToInsert[buffer], buffer))
      }

      return data.join('\n')
    }

    fs.writeSync(fd, filePayload(), err => err && console.log(err))
    fs.close(fd, err => err && console.log(err, 'ERROR!'))
  })
}

module.exports = addIconsBufferToHolder
