const fs = require('fs')

const getIconProps = require('../helpers/getIconProps.ts')
const { ENCODING_TYPE } = require('../constants/index.ts')

const writeIconFile = (element, contents) => {
  const { iconDir, iconToWrite, iconContent } = getIconProps(element, contents)

  if (fs.existsSync(iconDir)) {
    console.log('\x1b[31m%s\x1b[0m', `Icon is already in folder! File writing is failed. ${iconDir}, ${iconToWrite}`)
    return
  }

  const writeConfig = [
    iconToWrite, // file name to create
    iconContent, // generates regular svg icons file markup
    ENCODING_TYPE // encoding type only UTF-8 to use!
  ]

  fs.mkdirSync(iconDir)
  fs.writeFileSync(...writeConfig)

  console.log('\x1b[32m%s\x1b[0m', `Congratulations! Icon File: "${iconToWrite}" has been created successfuly!`)
}

module.exports = writeIconFile
