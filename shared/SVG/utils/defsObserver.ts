class DefsObserver {
  defsID?: {
    filters: string[]
    gradients: string[]
  }

  constructor() {
    this.defsID = {
      filters: [],
      gradients: []
    }
  }

  addDef = ({ type, ID }) => this.defsID[type].push(ID)

  destroyDefs = () => {
    this.defsID = {
      filters: [],
      gradients: []
    }
  }

  getDefs = () => this.defsID
}

export default new DefsObserver()
