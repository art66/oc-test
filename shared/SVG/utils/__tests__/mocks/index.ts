// eslint-disable-next-line @typescript-eslint/quotes
export const defsMock = "<linear-gradient x1='2' x2='3' y1='1' y2='2'></linear-gradient>"

export const svgMockMulti = `
<svg>
  ${defsMock}
</svg>
`

export const svgMockSingle = `<svg>${svgMockMulti}</svg>`
