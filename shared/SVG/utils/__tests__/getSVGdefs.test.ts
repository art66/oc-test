import getSVGdefs from '../getSVGdefs'

import { svgMockMulti, svgMockSingle, defsMock } from './mocks'

describe('SVG -> getSVGdefs', () => {
  it('should return unwrapped SVG defs multi line', () => {
    const matchRegExp = new RegExp(defsMock, 'mi')

    expect(getSVGdefs(svgMockMulti)).toMatch(matchRegExp)
  })
  it('should return unwrapped SVG defs single line', () => {
    const matchRegExp = new RegExp(defsMock, 'mi')

    expect(getSVGdefs(svgMockSingle)).toMatch(matchRegExp)
  })
})
