const firstLetterUpper = (iconName: string) => {
  if (!iconName || typeof iconName !== 'string') {
    return null
  }

  const firstLetterUpper = (iconName.substr(0, 1).toUpperCase() + iconName.substr(1, iconName.length)).split('')

  const allLetersUpper = firstLetterUpper.map((letter, index) => {
    if (letter === '-') {
      firstLetterUpper.splice(index - 1, 0)
      firstLetterUpper[index + 1] = firstLetterUpper[index + 1].toUpperCase()

      return
    }

    return letter
  })

  return allLetersUpper.join('')
}

export default firstLetterUpper
