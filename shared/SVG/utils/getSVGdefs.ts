const getSVGDefs = (svgWithDefs: string): string => {
  const withoutWrapperDefs = (svgWithDefs).replace(/(<svg.*?>)([\s\S]*)(<\/svg>)/im, '$2')

  return withoutWrapperDefs
}

export default getSVGDefs
