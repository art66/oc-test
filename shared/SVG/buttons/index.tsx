import React, { PureComponent } from 'react'
import buttonsHolder from '../utils/buttonsHolder'

import { XMLNS } from '../constants'
import { BUTTON_TYPES } from '../constants/buttons'

import { IProps, IItemsColor, ICurrentButton } from '../types/buttons'
import styles from './index.cssmodule.scss'

class SVGButtonsGenerator extends PureComponent<IProps> {
  _getButtonType = () => {
    const { buttonType = '' } = this.props
    const { FILL, DIMENSIONS, COLOR_SCHEME } = BUTTON_TYPES[buttonType] || BUTTON_TYPES.DEFAULT

    return {
      FILL,
      DIMENSIONS,
      COLOR_SCHEME
    }
  }

  _gradientFiller = (item: IItemsColor) => {
    return <stop key={item.color} offset={item.step} stopColor={item.color} />
  }

  _getFill = (condition: string) => {
    const { FILL } = this._getButtonType()

    const { ID, STROKE, STROKE_WIDTH, GRADIENT_TRANSFORM } = FILL[condition] || FILL

    return {
      ID,
      STROKE,
      STROKE_WIDTH,
      GRADIENT_TRANSFORM
    }
  }

  _getDimenstions = () => {
    const { mediaType = 'desktop' } = this.props
    const { DIMENSIONS } = this._getButtonType()

    const { WIDTH, HEIGHT } = DIMENSIONS[mediaType.toUpperCase()] || DIMENSIONS

    return {
      WIDTH,
      HEIGHT
    }
  }

  _getColorScheme = () => {
    const { condition = '', customColor } = this.props
    const { COLOR_SCHEME } = this._getButtonType()
    const { ID, GRADIENT_TRANSFORM } = this._getFill(condition)

    if (customColor) {
      return (
        <linearGradient id={ID} gradientTransform={GRADIENT_TRANSFORM}>
          {this._gradientFiller(customColor)}
        </linearGradient>
      )
    }

    const schemeCondition = COLOR_SCHEME[condition] || COLOR_SCHEME
    const scheme = schemeCondition.map(item => {
      return this._gradientFiller(item)
    })

    return (
      <linearGradient id={ID} gradientTransform={GRADIENT_TRANSFORM}>
        {scheme}
      </linearGradient>
    )
  }

  _getButtonWithMediaType = (currentButton: ICurrentButton) => {
    const { mediaType = 'desktop' } = this.props
    const { shape } = currentButton

    if (mediaType === 'mobile') {
      return shape.mobile
    }

    if (mediaType === 'tablet') {
      return shape.tablet
    }

    return shape.desktop
  }

  _renderButton = () => {
    const { buttonName } = this.props
    const currentButton = buttonsHolder[buttonName]

    if (!currentButton) {
      return null
    }

    const getButton = this._getButtonWithMediaType(currentButton)
    const { condition = '' } = this.props
    const { ID, STROKE, STROKE_WIDTH } = this._getFill(condition)

    return <path d={getButton} fill={`url(#${ID})`} stroke={STROKE} strokeWidth={STROKE_WIDTH} />
  }

  render() {
    const { customClass = '' } = this.props

    return (
      <svg
        className={`${styles.default} ${customClass}`}
        xmlns={XMLNS}
        width={this._getDimenstions().WIDTH}
        height={this._getDimenstions().HEIGHT}
      >
        {this._getColorScheme()}
        {this._renderButton()}
      </svg>
    )
  }
}

export default SVGButtonsGenerator
