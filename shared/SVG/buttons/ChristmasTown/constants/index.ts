const DIMENSIONS = {
  DESKTOP: {
    WIDTH: '123',
    HEIGHT: '29'
  },
  TABLET: {
    WIDTH: '123',
    HEIGHT: '29'
  },
  MOBILE: {
    WIDTH: '115',
    HEIGHT: '29'
  }
}

const FILL = {
  ACTIVE: {
    ID: 'cte-button-create',
    GRADIENT_TRANSFORM: 'rotate(90)',
    STROKE: '#8dacbb',
    STROKE_WIDTH: '1'
  },
  CLICKED: {
    ID: 'cte-button-create',
    GRADIENT_TRANSFORM: 'rotate(90)',
    STROKE: '#8dacbb',
    STROKE_WIDTH: '1'
  },
  DISABLED: {
    ID: 'cte-button-create',
    GRADIENT_TRANSFORM: 'rotate(90)',
    STROKE: '#c6d5dd',
    STROKE_WIDTH: '1'
  }
}

const COLOR_SCHEME = {
  ACTIVE: [
    {
      step: '0%',
      color: '#DAE3E8'
    },
    {
      step: '25%',
      color: '#EEF2F4'
    },
    {
      step: '60%',
      color: '#BDCED7'
    },
    {
      step: '80%',
      color: '#E8EEF1'
    },
    {
      step: '100%',
      color: '#BACDD6'
    }
  ],
  CLICKED: [
    {
      step: '0%',
      color: '#B9CCD5'
    },
    {
      step: '25%',
      color: '#DCE5EA'
    },
    {
      step: '60%',
      color: '#8AA9B8'
    },
    {
      step: '80%',
      color: '#CEDBE1'
    },
    {
      step: '100%',
      color: '#8DABBA'
    }
  ],
  DISABLED: [
    {
      step: '0%',
      color: '#ECF1F3'
    },
    {
      step: '25%',
      color: '#F5F8F9'
    },
    {
      step: '60%',
      color: '#DEE6EB'
    },
    {
      step: '80%',
      color: '#F2F5F7'
    },
    {
      step: '100%',
      color: '#DFE7EB'
    }
  ]
}

export const CHRISTMAS_TOWN = {
  DIMENSIONS,
  FILL,
  COLOR_SCHEME
}
