import React from 'react'

const Cross = {
  shape: {
    default: (
      <g>
        <polygon points='0 0 5.25 6 0 12 3.5 12 7 8.28 10.5 12 14 12 8.75 6 14 0 10.5 0 7 3.72 3.5 0 0 0' />
      </g>
    )
  }
}

export default Cross
