import React from 'react'

const Lock = {
  shape: {
    default: <path d='M18,10V6A6,6,0,0,0,6,6v4H3V24H21V10ZM8,10V6a4,4,0,0,1,8,0v4Z' />
  }
}

export default Lock
