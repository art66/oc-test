import React from 'react'

const Application = {
  shape: (
    <path d='M8.17.05A20.65,20.65,0,0,1,13,4.64a5.58,5.58,0,0,0-4-.79A5.65,5.65,0,0,0,8.17.05ZM13,7.33V16H0V0H5.47C8.62,0,7.63,5.33,7.63,5.33c2-.49,5.37-.28,5.37,2ZM2.6,10.67H5.85V8H2.6ZM10.4,12H2.6v.67h7.8Zm0-2H7.15v.67H10.4Zm0-2H7.15v.67H10.4Z' />
  )
}

export default Application
