import React from 'react'

const News = {
  shape: (
    <path d='M-1738-124v-9h4l3,3v6Zm1-1.286h5v-4.285l-2.144-2.143H-1737Zm1-.714v-1h3v1Zm0-2v-1h3v1Zm0-2v-1h2v1Z' />
  )
}

export default News
