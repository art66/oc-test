import React from 'react'

const Star = {
  shape: (
    <path d='M337.5,238.188l1.3,4H343l-3.4,2.47,1.3,4-3.4-2.469-3.4,2.469,1.3-4-3.4-2.47h4.2Z' />
  )
}

export default Star
