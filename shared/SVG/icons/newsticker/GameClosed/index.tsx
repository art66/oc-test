import React from 'react'

const GameClosed = {
  shape: (
    <path d='M6,1,0,12H12ZM5.5,5h1V8.5h-1ZM6,10.625A.625.625,0,1,1,6.625,10,.625.625,0,0,1,6,10.625Z' />
  )
}

export default GameClosed
