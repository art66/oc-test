import React from 'react'

const Login = {
  shape: <path d="M6.67,5.83V2.5l6.66,5.83L6.67,14.17V10.83H0v-5ZM8.33,0V1.67h10V15h-10v1.67H20V0Z" />
}

export default Login
