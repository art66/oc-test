import React from 'react'

const Logout = {
  shape: <path d="M10,6.67V4A4,4,0,0,0,2,4V6.67H0V16H12V6.67Zm-6.67,0V4A2.67,2.67,0,0,1,8.67,4V6.67Z" />
}

export default Logout
