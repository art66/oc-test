// @ts-nocheck
import React from 'react'

const defs = {
  default: (
    <defs>
      <filter id="top_header_avatar_filter__regular" x="0" y="0" width="36" height="36" filterUnits="userSpaceOnUse">
        <feOffset dy="1" input="SourceAlpha" />
        <feGaussianBlur stdDeviation="2" result="blur" />
        <feFlood floodOpacity="0.6" />
        <feComposite operator="in" in2="blur" />
        <feComposite in="SourceGraphic" />
      </filter>
      <filter
        id="top_header_avatar_filter__regular___hover"
        x="0"
        y="0"
        width="36"
        height="36"
        filterUnits="userSpaceOnUse"
      >
        <feOffset input="SourceAlpha" />
        <feGaussianBlur stdDeviation="1.5" result="blur" />
        <feFlood floodColor="#fff" floodOpacity="0.251" />
        <feComposite operator="in" in2="blur" />
        <feComposite in="SourceGraphic" />
      </filter>
      <filter id="top_header_avatar_filter__hospital" x="0" y="0" width="36" height="36" filterUnits="userSpaceOnUse">
        <feOffset dy="1" input="SourceAlpha" />
        <feGaussianBlur stdDeviation="2" result="blur" />
        <feFlood floodOpacity="0.6" />
        <feComposite operator="in" in2="blur" />
        <feComposite in="SourceGraphic" />
      </filter>
      <filter
        id="top_header_avatar_filter__hospital___hover"
        x="0"
        y="0"
        width="36"
        height="36"
        filterUnits="userSpaceOnUse"
      >
        <feOffset input="SourceAlpha" />
        <feGaussianBlur stdDeviation="2" result="blur" />
        <feFlood floodColor="#fff" floodOpacity="0.251" />
        <feComposite operator="in" in2="blur" />
        <feComposite in="SourceGraphic" />
      </filter>
      <filter id="top_header_avatar_filter__jail" x="0" y="0" width="36" height="36" filterUnits="userSpaceOnUse">
        <feOffset dy="1" input="SourceAlpha" />
        <feGaussianBlur stdDeviation="2" result="blur" />
        <feFlood floodOpacity="0.4" />
        <feComposite operator="in" in2="blur" />
        <feComposite in="SourceGraphic" />
      </filter>
      <filter
        id="top_header_avatar_filter__jail___hover"
        x="0"
        y="0"
        width="36"
        height="36"
        filterUnits="userSpaceOnUse"
      >
        <feOffset input="SourceAlpha" />
        <feGaussianBlur stdDeviation="1.5" result="blur" />
        <feFlood floodColor="#fff" floodOpacity="0.251" />
        <feComposite operator="in" in2="blur" />
        <feComposite in="SourceGraphic" />
      </filter>
    </defs>
  )
}

export default defs
