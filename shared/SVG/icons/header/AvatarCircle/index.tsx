import React from 'react'
import defs from './defs'

const AvatarCircle = {
  defs,
  shape: (
    <path d="M972.729,532.971a11.848,11.848,0,1,0,3.471,8.379A11.768,11.768,0,0,0,972.729,532.971Zm-8.379,18.254a9.875,9.875,0,1,1,9.875-9.875A9.887,9.887,0,0,1,964.35,551.225Z" />
  )
}

export default AvatarCircle
