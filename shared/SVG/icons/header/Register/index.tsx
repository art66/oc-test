import React from 'react'

const Register = {
  shape: <path d="M8,0a8,8,0,1,0,8,8A8,8,0,0,0,8,0ZM7.17,11.53l-3-2.91L5.4,7.38,7.17,9.05l3.76-3.86,1.24,1.24Z" />
}

export default Register
