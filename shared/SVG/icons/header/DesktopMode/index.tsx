import React from 'react'

const DesktopMode = {
  shape: <path d="M5,12,4,14h6L9,12ZM16,3h1v9H16ZM15,2V14h3V2ZM1,1H13V9H1ZM0,0V11H14V0Z" />
}

export default DesktopMode
