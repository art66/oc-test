import React from 'react'

const Menu = {
  shape: <path d="M12.5,10.5v2h15v-2Zm0,10v2h15v-2Zm0-5v2h15v-2Z" />
}

export default Menu
