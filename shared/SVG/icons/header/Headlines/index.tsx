import React from 'react'

const Headlines = {
  shape: (
    <path d="M5.25,10.5H15v.75H5.25ZM15,8.25H5.25V9H15Zm0-4.5H11.25V4.5H15ZM15,6H11.25v.75H15ZM2.25,0V12.9a.39.39,0,0,1-.31.44A.39.39,0,0,1,1.5,13a.24.24,0,0,1,0-.12V1.5H0v12A1.52,1.52,0,0,0,1.5,15h15A1.52,1.52,0,0,0,18,13.5V0ZM16.5,12.75H3.75V1.5H16.5Zm-6.75-9H5.25v3h4.5Z" />
  )
}

export default Headlines
