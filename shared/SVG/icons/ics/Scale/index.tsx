import React from 'react'

const Scale = {
  shape: (
    // eslint-disable-next-line max-len
    <path d='M19,12V2.83A6,6,0,0,1,16,5V3a6.17,6.17,0,0,0,1.86-1.11A4.17,4.17,0,0,0,19.23,0H21V12ZM9.8,12V10h2v2ZM3,12V2.83A6,6,0,0,1,0,5V3A6.17,6.17,0,0,0,1.86,1.89,4.17,4.17,0,0,0,3.23,0H5V12ZM9.8,5V3h2V5Z' />
  )
}

export default Scale
