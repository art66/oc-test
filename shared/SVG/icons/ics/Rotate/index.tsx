import React, { Fragment } from 'react'

const Rotate = {
  shape: {
    left: (
      <Fragment>
        <path d='M 8 0 L 16 0 L 16 12 L 12 12 L 12 16 L 0 16 L 0 8 L 8 8 L 8 0 ZM 2 10 L 2 14 L 10 14 L 10 10 L 2 10 Z' />
        <path d='M 6 0 L 6 2 C 3.7909 2 2 3.7908 2 6 L 0 6 C 0 2.6862 2.6862 0 6 0 Z' />
      </Fragment>
    ),
    right: (
      <Fragment>
        <path d='M 8 0 L 0 0 L 0 12 L 4 12 L 4 16 L 16 16 L 16 8 L 8 8 L 8 0 ZM 14 10 L 14 14 L 6 14 L 6 10 L 14 10 Z' />
        <path d='M 10 0 L 10 2 C 12.2091 2 14 3.7908 14 6 L 16 6 C 16 2.6862 13.3138 0 10 0 Z' />
      </Fragment>
    )
  }
}

export default Rotate
