import React from 'react'

const Crop = {
  shape: (
    <path d='M 16 1 L 16 0 L 15 0 L 12 3 L 5 3 L 5 0 L 3 0 L 3 3 L 0 3 L 0 5 L 3 5 L 3 13 L 11 13 L 11 16 L 13 16 L 13 13 L 16 13 L 16 11 L 13 11 L 13 4 L 16 1 ZM 5 5 L 10 5 L 5 10 L 5 5 ZM 11 11 L 6 11 L 11 6 L 11 11 ZM 11.0003 11.0003 L 11.0003 11.0003 Z' />
  )
}

export default Crop
