import React from 'react'

const FollowLink = {
  shape: (
    <path d='M3,7.5A7.61,7.61,0,0,1,8.5,1.67V0L12,3.32,8.5,6.67V5A7.89,7.89,0,0,0,3,7.5Zm6,.07V9H1V3H4.3A8.58,8.58,0,0,1,5.47,2H0v8H10V6.62Z' />
  )
}

export default FollowLink
