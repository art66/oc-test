import React from 'react'

const Images = {
  shape: (
    <path d="M3.33,3.67a1,1,0,1,1,1,1,1,1,0,0,1-1-1Zm6,.33L7.65,6.67,6,5.36l-2.67,4h9.34Zm5.34-2.67v9.34H1.33V1.33ZM16,0H0V12H16Z" />
  )
}

export default Images
