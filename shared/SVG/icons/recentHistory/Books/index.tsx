import React from 'react'

const Books = {
  shape: (
    <path data-name="Path 415-2" d="M12.9,2.1V16H15V2.1ZM10.3,16h1.6V0H10.3ZM5.2,16H9.3V3.7H5.2Zm1-11.2H8.3V6.9H6.2ZM0,16H3.6V13.3H0Zm0-3.7H3.6V4.8H0ZM0,3.7H3.6V1.1H0Z" />
  )
}

export default Books
