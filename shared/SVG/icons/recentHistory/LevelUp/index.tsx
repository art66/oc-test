import React from 'react'

const LevelUp = {
  shape: (
    <path d='M10.67,14.67V16H4V14.67Zm0-2.67H4v1.33h6.67ZM4,7.33v3.34h6.67V7.33h4L7.33,0,0,7.33Z' />
  )
}

export default LevelUp
