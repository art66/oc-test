import React from 'react'

const ViewLog = {
  shape: (
    <path d='M8,6V8l4-4L8,0V2S1.36,3.07,0,9C2.9,5.9,8,6,8,6Z' />
  )
}

export default ViewLog
