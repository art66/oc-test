import React from 'react'

const AccountCreation = {
  shape: (
    <path data-name="account creation-2" d="M13.88,12.06c-2.29-.52-4.43-1-3.39-2.94C13.63,3.18,11.32,0,8,0S2.36,3.3,5.51,9.12c1.07,2-1.15,2.43-3.39,2.94C.13,12.52,0,13.49,0,15.17V16H16v-.83C16,13.49,15.87,12.52,13.88,12.06Z" />
  )
}

export default AccountCreation
