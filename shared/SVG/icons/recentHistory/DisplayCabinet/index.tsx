import React from 'react'

const DisplayCabinet = {
  shape: (
    <path data-name="Path 112-2" d="M0,0V16H16V0ZM1.33,4h6V14.67h-6ZM14.67,14.67h-6V4h6Z" />
  )
}

export default DisplayCabinet
