import React from 'react'

const PointsMarket = {
  shape: (
    <path data-name="Path 25-2" d="M9.9,8.1H8.1V6.3H9.9ZM6.3,4.5v7.2H8.1V9.9H9.9a1.8,1.8,0,0,0,1.8-1.8V6.3A1.81,1.81,0,0,0,9.9,4.5Zm8.25,6.3A6.18,6.18,0,0,1,3.09,9.9h-2a8.1,8.1,0,0,0,15.54.9H18L15.75,7.2,13.5,10.8ZM3.45,5.4a6.18,6.18,0,0,1,11.46.9h2A8.1,8.1,0,0,0,1.37,5.4H0L2.25,9,4.5,5.4Z" />
  )
}

export default PointsMarket
