import React from 'react'

const Authentication = {
  shape: (
    <path d="M8.3,11.56,6.67,13.33H5.33v1.34H4V16H0v-.86l4.71-4.71a4.19,4.19,0,0,1-.4-.54L0,14.2V12.67L4.58,8A6.68,6.68,0,0,0,8.3,11.56ZM16,5.33A5.34,5.34,0,1,1,10.67,0h0A5.33,5.33,0,0,1,16,5.33Zm-2-2a1.34,1.34,0,1,0-1.33,1.34A1.34,1.34,0,0,0,14,3.33Z" />
  )
}

export default Authentication
