import React from 'react'

const Captcha = {
  shape: (
    <path data-name="Union 21-2" d="M2.78,12.55h0A7.38,7.38,0,0,1,1.56,11.4L0,13V7.29c0-.1,0-.2,0-.3H6L4.15,8.81a3.39,3.39,0,0,0,2.59,1.55.73.73,0,0,0,.25,0,3.13,3.13,0,0,0,2-.63A3.53,3.53,0,0,0,10,8.6l2.57,2.59a7,7,0,0,1-9.78,1.36ZM14,7H8L9.85,5.15a3.34,3.34,0,0,0-2-1.46c0-1.82,0-3,0-3.64a7,7,0,0,1,4.58,2.52L14,1V6.67c0,.11,0,.2,0,.3ZM.06,6.11A7,7,0,0,1,2.57,1.57L1,0H7C7,1.11,7,3.86,7,6L5.15,4.15a3.39,3.39,0,0,0-1.45,2H.06Z" />
  )
}

export default Captcha
