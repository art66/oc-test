import React from 'react'

const Hospital = {
  shape: (
    <path data-name="Path 1-2" d="M6,0V6H0v4H6v6h4V10h6V6H10V0Z" />
  )
}

export default Hospital
