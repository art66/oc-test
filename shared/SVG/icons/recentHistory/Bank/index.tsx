import React from 'react'

const Bank = {
  shape: (
    <path data-name="Path 15-2" d="M7.89,0,0,4.44H16Zm6.88,4.8H1.21v8.89H.14V16h15.7V13.69H14.77ZM5.32,13.69H3.53V7.29a.9.9,0,0,1,1.79,0Zm3.56,0H7.1V7.29a.89.89,0,0,1,1.78,0Zm3.57,0H10.67V7.29a.89.89,0,1,1,1.78,0Z" />
  )
}

export default Bank
