import React from 'react'

const Donator = {
  shape: (
    <path data-name="Path 38-2" d="M8,0,9.89,5.81H16L11.05,9.4l1.89,5.82L8,11.63,3.05,15.22,4.94,9.4,0,5.81H6.11Z" />
  )
}

export default Donator
