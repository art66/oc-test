import React from 'react'

const Museum = {
  shape: (
    <path data-name="Path 80-2" d="M1,7v8H4V7H6V8H5v7H6V9h4v6h1V8H10V7h2v8h3V7h1V6H0V7ZM0,4V5H16V4ZM2,2V3H14V2ZM4,0V1h8V0Z" />
  )
}

export default Museum
