import React from 'react'

const IgnoreList = {
  shape: (
    <path d="M8,0a8,8,0,1,0,8,8A8,8,0,0,0,8,0Zm4,8.67H4V7.33h8Z" />
  )
}

export default IgnoreList
