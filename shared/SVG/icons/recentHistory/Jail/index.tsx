import React from 'react'

const Jail = {
  shape: (
    <path data-name="Path 3-2" d="M10.88,0V16h1.88V0ZM6.18,11.82H9.94V9.94H6.18ZM0,11.82H2.41V9.94H0Zm13.71,0h2.35V9.94H13.71ZM6.18,6.18H9.94V4.29H6.18ZM0,6.18H2.41V4.29H0Zm13.71,0h2.35V4.29H13.71ZM3.35,0V16H5.24V0Z" />
  )
}

export default Jail
