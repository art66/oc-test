import React from 'react'

const Equipping = {
  shape: (
    <path data-name="Subtraction 8-2" d="M8,16a8,8,0,1,1,8-8A8,8,0,0,1,8,16ZM3,7V9H7v4H9V9h4V7H9V3H7V7Z" />
  )
}

export default Equipping
