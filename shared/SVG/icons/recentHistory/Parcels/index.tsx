import React from 'react'

const Parcels = {
  shape: (
    <path d="M7.67,14.67,2,11.64V9l3.6,2.08L7.67,8.82Zm.66,0V8.8L10.44,11l3.56-2v2.59ZM1.44,7.92,0,7.09l2-2.2L0,3,5.81,0,8,1.59,10.17,0,16,3,14,4.89l2,2.3L14.51,8h0l-3.94,2.18L8.34,7.83l4.94-2.64L8,2.27,2.72,5.19,7.67,7.81v0l-2.2,2.41L2,8.24l-.11-.06-.45-.27h0Z"  fill-rule="evenodd"/>
  )
}

export default Parcels
