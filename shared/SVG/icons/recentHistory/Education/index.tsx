import React from 'react'

const Education = {
  shape: (
    <path data-name="Path 66-2" d="M12.92,6.69V9.81c0,1.7-3.56,2.5-5.54,2.5s-5.53-.86-5.53-2.5V5.94l5.53,3ZM8,0,0,3.54l7.38,4,6.77-2.72V9.23h1.23V4.15ZM16,12.31H13.54a5.15,5.15,0,0,0,.61-2.46h1.23A5.16,5.16,0,0,0,16,12.31Z" />
  )
}

export default Education
