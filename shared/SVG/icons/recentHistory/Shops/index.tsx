import React from 'react'

const Shops = {
  shape: (
    <path d="M6.67,11a1,1,0,1,1-1-1A1,1,0,0,1,6.67,11ZM9,10a1,1,0,1,0,1,1A1,1,0,0,0,9,10Zm.89-3.33L11.21,2H0L2,6.67ZM13.2,0,10.91,8H2.51l.56,1.33H11.9l2.32-8H15.5L16,0Z" />
  )
}

export default Shops
