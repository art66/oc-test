import React from 'react'

const AccountRecovery = {
  shape: (
    <path data-name="Union 34-2" d="M3.85,12.61l.9-1.33a6.39,6.39,0,1,0-.88-4.53H6L3,11.18,0,6.75H2.34a7.87,7.87,0,1,1,1.5,5.86ZM6,11v-.34C6,9.78,6,9.26,7.06,9s2.23-.49,1.7-1.47C7.18,4.65,8.31,3,10,3s2.82,1.59,1.24,4.56c-.51,1,.55,1.21,1.7,1.47S14,9.78,14,10.68V11Z" />
  )
}

export default AccountRecovery
