import React from 'react'

const FriendsList = {
  shape: (
    <path data-name="Path 78-2" d="M11.71,10.43C9.78,10,8,9.58,8.85,7.89,11.51,2.75,9.55,0,6.75,0S2,2.85,4.65,7.89c.9,1.7-1,2.1-2.86,2.54C.11,10.83,0,11.67,0,13.12v.72H13.5v-.72C13.5,11.67,13.39,10.83,11.71,10.43Z" />
  )
}

export default FriendsList
