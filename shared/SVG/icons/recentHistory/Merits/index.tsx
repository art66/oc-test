import React from 'react'

const Merits = {
  shape: (
    <path data-name="Union 27-2" d="M0,9a9,9,0,1,1,9,9A9,9,0,0,1,0,9ZM2.14,9A6.86,6.86,0,1,0,9,2.14H9A6.87,6.87,0,0,0,2.14,9ZM9,11.79,5.91,13.44,6.53,10,4,7.57l3.47-.48L9,3.93l1.53,3.16L14,7.57,11.47,10l.62,3.45Z" />
  )
}

export default Merits
