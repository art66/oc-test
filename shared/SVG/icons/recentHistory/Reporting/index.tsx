import React from 'react'

const Reporting = {
  shape: <path d="M8,0,0,14.67H16ZM7.33,5.33H8.67V10H7.33ZM8,12.87A.87.87,0,1,1,8.87,12a.89.89,0,0,1-.87.87Z" />
}

export default Reporting
