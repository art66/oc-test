import React from 'react'

const ItemSending = {
  shape: (
    <path data-name="Union 41-2" d="M7.58,7.51l5.25-2.82V11L7.58,14ZM0,10.18V3.77L6.42,7.52v6.31ZM14.5,4,18,7.5,14.5,11ZM.7,2.83l2-1.15L9,5.39,7,6.5ZM3.9,1,5.7,0l6.52,3.7-2,1Z" />
  )
}

export default ItemSending
