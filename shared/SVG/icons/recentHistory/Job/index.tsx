import React from 'react'

const Job = {
  shape: (
    <path data-name="Path 51-2" d="M16,13.33H0v-10H16ZM6,0A1.33,1.33,0,0,0,4.67,1.33h0V2.67H6v-1a.34.34,0,0,1,.33-.34H9.67a.34.34,0,0,1,.33.34v1h1.33V1.33A1.33,1.33,0,0,0,10,0Z" />
  )
}

export default Job
