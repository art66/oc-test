import React from 'react'

const Trades = {
  shape: (
    <path data-name="Path 284-2" d="M6.79,6.09V7.75L2.91,3.88,6.79,0V1.66s8,0,8.44,9.28A7.92,7.92,0,0,0,6.79,6.09ZM0,7.06c.55,9.28,8.45,9.28,8.45,9.28V18l3.87-3.88L8.45,10.25v1.66A7.92,7.92,0,0,1,0,7.06Z" />
  )
}

export default Trades
