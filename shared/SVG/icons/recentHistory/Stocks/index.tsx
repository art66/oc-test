import React from 'react'

const Stocks = {
  shape: (
    <path data-name="Path 84-2" d="M6.82,13.33H8.18V16H6.82Zm1.94,0L11.1,16h1.79l-2.33-2.67ZM2.1,16H3.89l2.33-2.67H4.42ZM7.28,6l1,1,2.31-2.25.57.55.41-2L9.54,3.7l.59.57L8.3,6.06l-1-1L4.78,7.47l.48.47Zm4.31,2.68H4.09V3.33H3.41v6h8.18ZM15,2h-.68V12H.68V2H0V0H15ZM13,2H2v8.67H13Z" />
  )
}

export default Stocks
