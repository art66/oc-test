import React from 'react'

const Items = {
  shape: (
    <path data-name="Path 83-2" d="M10.32,6.16,8,7.43.79,3.23,3.11,1.92Zm1.38-.73L14,4.23,6.51,0,4.45,1.16ZM7.33,8.59,0,4.31v7.32l7.33,4.18Zm7.34-3.23v7.22L8.67,16V8.58Z" />
  )
}

export default Items
