import React from 'react'

const Awards = {
  shape: (
    <path data-name="Union 17-2" d="M2,11a5,5,0,1,1,5,5A5,5,0,0,1,2,11Zm5,2,1.85,1L8.5,11.83,10,10.29,7.93,10,7,8l-.93,2L4,10.29l1.5,1.54L5.15,14,7,13ZM8,5l3-5,3,2L11,7ZM0,2,3,0,6,5,3,7ZM6,3,4,0h6L8,3Z" />
  )
}

export default Awards
