import React from 'react'

const CityFinds = {
  shape: (
    <path data-name="Path 294-2" d="M6,0A6,6,0,0,0,0,5.7C0,8.85,2.6,12.61,6,18c3.4-5.39,6-9.15,6-12.3A6,6,0,0,0,6,0ZM6,8.25A2.25,2.25,0,1,1,8.25,6,2.25,2.25,0,0,1,6,8.25Z" />
  )
}

export default CityFinds
