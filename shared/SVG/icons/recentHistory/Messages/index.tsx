import React from 'react'

const Messages = {
  shape: (
    <path data-name="Path 85-2" d="M8,6.24,0,0H16ZM4.38,5.07,0,1.64v8Zm7.24,0L16,9.67v-8Zm-1,.81L8,7.89l-2.58-2L0,11.56H16Z" />
  )
}

export default Messages
