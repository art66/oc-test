import React from 'react'

const DogTags = {
  shape: (
    <path d='M3.36,3.36a1.48,1.48,0,1,1,0,2.1A1.48,1.48,0,0,1,3.36,3.36ZM7.24.88A3,3,0,0,0,3,.88L.88,3a3,3,0,0,0,0,4.24L8,14.31a3,3,0,0,0,4.24,0l2.12-2.12a3,3,0,0,0,0-4.24Z' />
  )
}

export default DogTags
