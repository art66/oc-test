import React from 'react'

const Admin = {
  shape: (
    <path d="M0,1V14.5H18V1ZM9.75,11.5H2.25v-.31c0-.84,0-1.31,1-1.54S5.33,9.2,4.83,8.27C3.35,5.55,4.41,4,6,4S8.64,5.49,7.17,8.27c-.49.92.51,1.14,1.59,1.38s1,.71,1,1.55Zm6,0h-4.5V10h4.5Zm0-3h-4.5V7h4.5Zm0-3h-4.5V4h4.5Z"/>
  )
}

export default Admin
