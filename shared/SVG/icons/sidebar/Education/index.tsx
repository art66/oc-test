import React from 'react'

const Education = {
  shape: (
    <path d="M12.92,7.69v3.12c0,1.7-3.56,2.5-5.54,2.5s-5.53-.86-5.53-2.5V6.94l5.53,3ZM8,1,0,4.54l7.38,4,6.77-2.72v4.41h1.23V5.15ZM16,13.31H13.54a5.26,5.26,0,0,0,.61-2.46h1.23A5.24,5.24,0,0,0,16,13.31Z"/>
  )
}

export default Education
