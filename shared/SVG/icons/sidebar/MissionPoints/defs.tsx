import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='MissionPointsGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#fb7a00' stopOpacity='1' offset='0%'/>
        <stop stopColor='#d90000' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='MissionPointsGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ff8000' stopOpacity='1' offset='0%'/>
        <stop stopColor='#d90000' stopOpacity='1' offset='97.7778%'/>
      </linearGradient>
      <linearGradient id='MissionPointsGradient3' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
