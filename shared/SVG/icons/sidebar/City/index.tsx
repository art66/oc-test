import React from 'react'

const City = {
  shape: (
    <path d="M6,1A6,6,0,0,0,0,6.7C0,9.85,2.6,13.61,6,19c3.4-5.39,6-9.15,6-12.3A6,6,0,0,0,6,1ZM6,9.25A2.25,2.25,0,1,1,8.25,7,2.25,2.25,0,0,1,6,9.25Z"/>
  )
}

export default City
