import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='NervePointGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ca6b32' stopOpacity='1' offset='0%'/>
        <stop stopColor='#b53d2c' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='NervePointGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#b53d2c' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ca6b32' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <filter id='NervePointFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.3765 0  0 0 0 0.1333 0  0 0 0 0.0902 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='NervePointGradient3' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
