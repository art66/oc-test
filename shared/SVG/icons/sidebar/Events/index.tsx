import React from 'react'

const Events = {
  shape: (
    <path d="M9,1a9,9,0,1,0,9,9A9,9,0,0,0,9,1ZM7.27,4.22h3.46l-.88,8.09H8.15ZM9,16.25A1.3,1.3,0,1,1,10.3,15,1.3,1.3,0,0,1,9,16.25Z"/>
  )
}

export default Events
