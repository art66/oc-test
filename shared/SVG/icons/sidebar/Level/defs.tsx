import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='LevelGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#babaab' stopOpacity='1' offset='0%'/>
        <stop stopColor='#686859' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <linearGradient id='LevelGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#686859' stopOpacity='1' offset='0%'/>
        <stop stopColor='#cecebf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='LevelFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.2078 0  0 0 0 0.2078 0  0 0 0 0.149 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='1'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='LevelGradient3' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.298' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
