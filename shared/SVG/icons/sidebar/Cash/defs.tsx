import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='CashGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#99cc00' stopOpacity='1' offset='0%'/>
        <stop stopColor='#336600' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='CashGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#3a7500' stopOpacity='1' offset='0%'/>
        <stop stopColor='#99cc00' stopOpacity='1' offset='98.3333%'/>
      </linearGradient>
      <filter id='CashFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 1 0' />
        <feGaussianBlur result='out' in='out' stdDeviation='1'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='CashGradient3' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
