import React from 'react'

const FollowLinks = {
  shape: (
    <path d='M-1068-159h-15v-14h8v2h-6v10h11v-5h2v7l0,0Zm-8.389-8.233,5.078-5.078L-1074-175h8v8l-3.189-3.189-5.078,5.078Z' transform='translate(1092 184)' />
  )
}

export default FollowLinks
