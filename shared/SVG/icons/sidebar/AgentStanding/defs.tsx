import React from 'react'

const defs = {
  down: (
    <defs>
      <linearGradient id='AgentStandingDownGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#fb7a00' stopOpacity='1' offset='0%'/>
        <stop stopColor='#d90000' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='AgentStandingDownGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#d90000' stopOpacity='1' offset='0%'/>
        <stop stopColor='#fb7a00' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <filter id='AgentStandingDownFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.4 0  0 0 0 0.102 0  0 0 0 0 0  0 0 0 1 0' />
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='AgentStandingDownGradient3' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#fff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#fff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  ),
  up: (
    <defs>
      <linearGradient id='AgentStandingUpGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffbf00' stopOpacity='1' offset='0%'/>
        <stop stopColor='#8c6900' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='AgentStandingUpGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#8c6900' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbf00' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='AgentStandingUpFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0.1882 0  0 0 0 0 0  0 0 0 1 0' />
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='AgentStandingUpGradient3' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#fff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#fff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
