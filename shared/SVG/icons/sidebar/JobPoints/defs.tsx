import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='JobPointsGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#00d2d2' stopOpacity='1' offset='0%'/>
        <stop stopColor='#006666' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='JobPointsGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#006666' stopOpacity='1' offset='0%'/>
        <stop stopColor='#00d9d9' stopOpacity='1' offset='97.7778%'/>
      </linearGradient>
      <filter id='JobPointsFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0 0  0 0 0 0.4 0  0 0 0 0.4 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='1'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <radialGradient id='JobPointsGradient3' cx='55.5556%' cy='59.1054%' r='44.3016%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ceffff' stopOpacity='1' offset='66.6667%'/>
        <stop stopColor='#ceffff' stopOpacity='1' offset='100%'/>
      </radialGradient>
      <linearGradient id='JobPointsGradient4' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
