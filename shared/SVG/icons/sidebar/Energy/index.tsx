import React from 'react'

const Energy = {
  shape: (
    <path d="M123,330l1-5h-4l7-6-1,5h4Z" transform="translate(-115 -314)" />
  )
}

export default Energy
