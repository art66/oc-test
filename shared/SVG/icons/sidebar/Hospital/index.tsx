import React from 'react'

const Hospital = {
  shape: (
    <polygon points="6 1 6 7 0 7 0 11 6 11 6 17 10 17 10 11 16 11 16 7 10 7 10 1 6 1"/>
  )
}

export default Hospital
