import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='CasinoTokensGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#99cc00' stopOpacity='1' offset='0%'/>
        <stop stopColor='#336600' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='CasinoTokensGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#3a7500' stopOpacity='1' offset='0%'/>
        <stop stopColor='#99cc00' stopOpacity='1' offset='98.3333%'/>
      </linearGradient>
      <filter id='CasinoTokensFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <filter id='CasinoTokensFilter2' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow2'/>
      </filter>
      <filter id='CasinoTokensFilter3' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow3'/>
      </filter>
      <filter id='CasinoTokensFilter4' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow4'/>
      </filter>
      <filter id='CasinoTokensFilter5' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow5'/>
      </filter>
      <filter id='CasinoTokensFilter6' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow6'/>
      </filter>
      <filter id='CasinoTokensFilter7' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow7'/>
      </filter>
      <filter id='CasinoTokensFilter8' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow8'/>
      </filter>
      <filter id='CasinoTokensFilter9' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='1'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow9'/>
      </filter>
    </defs>
  )
}

export default defs
