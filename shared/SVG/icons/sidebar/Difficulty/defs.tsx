import React from 'react'

const defs = {
  0: (
    <defs>
      <linearGradient id='DifficultyGradient0_1' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#cecebf' stopOpacity='1' offset='0%'/>
        <stop stopColor='#686859' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient0_2' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#4c6600' stopOpacity='1' offset='0%'/>
        <stop stopColor='#a3d900' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient0_3' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  ),
  1: (
    <defs>
      <linearGradient id='DifficultyGradient1_1' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#cecebf' stopOpacity='1' offset='0%'/>
        <stop stopColor='#686859' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient1_2' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#006633' stopOpacity='1' offset='0%'/>
        <stop stopColor='#00d96d' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <filter id='DifficultyFilter1_1' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='2'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.1882 0  0 0 0 0.251 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='4'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='DifficultyGradient1_3' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#efffbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient1_4' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  ),
  2: (
    <defs>
      <linearGradient id='DifficultyGradient2_1' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#cecebf' stopOpacity='1' offset='0%'/>
        <stop stopColor='#686859' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient2_2' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#664c00' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbf00' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <filter id='DifficultyFilter2_1' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0.1882 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='3'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='DifficultyGradient2_3' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffefbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter2_2' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0.1882 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='3'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow2'/>
      </filter>
      <linearGradient id='DifficultyGradient2_4' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffefbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient2_5' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  ),
  3: (
    <defs>
      <linearGradient id='DifficultyGradient3_1' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#cecebf' stopOpacity='1' offset='0%'/>
        <stop stopColor='#686859' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient3_2' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#8c4600' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ff8000' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <filter id='DifficultyFilter3_1' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0.1255 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='DifficultyGradient3_3' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffdfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter3_2' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0.1255 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow2'/>
      </filter>
      <linearGradient id='DifficultyGradient3_4' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffdfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter3_3' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0.1255 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow3'/>
      </filter>
      <linearGradient id='DifficultyGradient3_5' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffdfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient3_6' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  ),
  4: (
    <defs>
      <linearGradient id='DifficultyGradient4_1' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#cecebf' stopOpacity='1' offset='0%'/>
        <stop stopColor='#686859' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient4_2' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#b22d00' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ff5c26' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <filter id='DifficultyFilter4_1' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='3'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='DifficultyGradient4_3' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter4_2' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='3'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow2'/>
      </filter>
      <linearGradient id='DifficultyGradient4_4' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter4_3' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='3'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow3'/>
      </filter>
      <linearGradient id='DifficultyGradient4_5' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter4_4' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='3'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow4'/>
      </filter>
      <linearGradient id='DifficultyGradient4_6' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient4_7' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  ),
  5: (
    <defs>
      <linearGradient id='DifficultyGradient5_1' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#cecebf' stopOpacity='1' offset='0%'/>
        <stop stopColor='#686859' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient5_2' x1='50%' y1='0%' x2='50%' y2='93.8595%'>
        <stop stopColor='#8c0000' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ff0000' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <filter id='DifficultyFilter5_1' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <linearGradient id='DifficultyGradient5_3' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter5_2' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow2'/>
      </filter>
      <linearGradient id='DifficultyGradient5_4' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter5_3' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow3'/>
      </filter>
      <linearGradient id='DifficultyGradient5_5' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter5_4' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow4'/>
      </filter>
      <linearGradient id='DifficultyGradient5_6' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <filter id='DifficultyFilter5_5' x='-100%' y='-100%' width='300%' height='300%'>

        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.251 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow5'/>
      </filter>
      <linearGradient id='DifficultyGradient5_7' x1='25%' y1='0%' x2='25%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffbfbf' stopOpacity='1' offset='100%'/>
      </linearGradient>
      <linearGradient id='DifficultyGradient5_8' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
