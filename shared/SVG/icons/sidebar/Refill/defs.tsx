import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='RefillGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#00b285' stopOpacity='1' offset='0%'/>
        <stop stopColor='#00664c' stopOpacity='1' offset='97.7778%'/>
      </linearGradient>
      <linearGradient id='RefillGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#00664c' stopOpacity='1' offset='0%'/>
        <stop stopColor='#00b285' stopOpacity='1' offset='97.7778%'/>
      </linearGradient>
      <filter id='RefillFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0.7071' dy='0.7071'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <radialGradient id='RefillGradient3' cx='50%' cy='50%' r='50%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='1' offset='13.8889%'/>
        <stop stopColor='#bfffef' stopOpacity='1' offset='100%'/>
      </radialGradient>
      <linearGradient id='RefillGradient4' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.298' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
