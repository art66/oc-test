import React from 'react'

const Enemies = {
  shape: (
    <g>
      <path d="M13.88,13.06c-2.29-.53-4.43-1-3.39-2.94C13.63,4.18,11.32,1,8,1S2.36,4.3,5.51,10.12c1.07,2-1.15,2.43-3.39,2.94C.13,13.52,0,14.49,0,16.17V17H16v-.83C16,14.49,15.87,13.52,13.88,13.06Z"/>
      <rect x="12.5" y="7.5" width="6" height="2" />
    </g>
  )
}

export default Enemies
