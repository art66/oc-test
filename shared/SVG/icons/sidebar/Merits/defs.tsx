import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='MeritsGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#d862ff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#680bb8' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='MeritsGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#5900b2' stopOpacity='1' offset='0%'/>
        <stop stopColor='#d862ff' stopOpacity='1' offset='97.7778%'/>
      </linearGradient>
      <filter id='MeritsFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0.2 0  0 0 0 0 0  0 0 0 0.4 0  0 0 0 0.651 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='2'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <radialGradient id='MeritsGradient3' cx='50%' cy='50%' r='40.9091%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='1' offset='13.8889%'/>
        <stop stopColor='#efc6fd' stopOpacity='1' offset='100%'/>
      </radialGradient>
      <linearGradient id='MeritsGradient4' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.498' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
