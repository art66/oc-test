import React from 'react'

const Mailbox = {
  shape: (
    <path d="M9,8,0,1H18ZM4.93,6.7,0,2.85v9Zm8.14,0L18,11.88v-9Zm-1.17.91L9,9.87,6.1,7.61,0,14H18Z"/>
  )
}

export default Mailbox
