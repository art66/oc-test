import React from 'react'

const defs = {
  default: (
    <defs>
      <linearGradient id='FactionRespectGradient1' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#999999' stopOpacity='1' offset='0%'/>
        <stop stopColor='#444444' stopOpacity='1' offset='98.8889%'/>
      </linearGradient>
      <linearGradient id='FactionRespectGradient2' x1='50%' y1='0%' x2='50%' y2='100%'>
        <stop stopColor='#222222' stopOpacity='1' offset='0%'/>
        <stop stopColor='#666666' stopOpacity='1' offset='97.7778%'/>
      </linearGradient>
      <filter id='FactionRespectFilter1' x='-100%' y='-100%' width='300%' height='300%'>
        <feOffset result='out' in='SourceGraphic' dx='0' dy='1'/>
        <feColorMatrix result='out' in='out' type='matrix' values='0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 0.4 0'/>
        <feGaussianBlur result='out' in='out' stdDeviation='0'/>
        <feBlend in='SourceGraphic' in2='out' mode='normal' result='Drop_Shadow1'/>
      </filter>
      <radialGradient id='FactionRespectGradient3' cx='50%' cy='50%' r='36.1979%'>
        <stop stopColor='#ffffff' stopOpacity='1' offset='0%'/>
        <stop stopColor='#dddddd' stopOpacity='1' offset='100%'/>
      </radialGradient>
      <linearGradient id='FactionRespectGradient4' x1='50.0004%' y1='0%' x2='50.0004%' y2='100%'>
        <stop stopColor='#ffffff' stopOpacity='0' offset='0%'/>
        <stop stopColor='#ffffff' stopOpacity='0.098' offset='100%'/>
      </linearGradient>
    </defs>
  )
}

export default defs
