
import React from 'react'
import defs from './defs'

const EnergyPoint = {
  defs,
  shape: (
    <g>
      <path d="M 0.095 9.5 C 0.095 4.3057 4.3057 0.095 9.5 0.095 C 14.6943 0.095 18.905 4.3057 18.905 9.5 C 18.905 14.6943 14.6943 18.905 9.5 18.905 C 4.3057 18.905 0.095 14.6943 0.095 9.5 Z" fill="url(#EnergyPointGradient1)"/>
      <path  d="M 2.075 9.5 C 2.075 5.3994 5.3992 2.075 9.5 2.075 C 13.6008 2.075 16.925 5.3994 16.925 9.5 C 16.925 13.6006 13.6008 16.925 9.5 16.925 C 5.3992 16.925 2.075 13.6006 2.075 9.5 Z" fill="url(#EnergyPointGradient2)"/>
      <path filter="url(#EnergyPointFilter1)" d="M 11.75 2 L 5 10.7207 L 9.125 10.7207 L 7.0623 17 L 14 8.2793 L 9.875 8.2793 L 11.75 2 Z" fill="#ffffff"/>
      <path d="M 0.9451 5.5898 C 2.8486 8.2603 5.9707 10 9.5 10 C 13.0293 10 16.1514 8.2603 18.0549 5.5898 C 16.5706 2.3484 13.2986 0.095 9.5 0.095 C 5.7014 0.095 2.4294 2.3484 0.9451 5.5898 Z" fill="url(#EnergyPointGradient3)"/>
    </g>
  )
}

export default EnergyPoint
