import React from 'react'

const Life = {
  shape: (
    <path d="M330.778,320.028,328.5,321.8l-2.25-1.8L324,321.8v3.6l4.5,3.6,4.5-3.6v-3.6Z" transform="translate(-318 -314)"/>
  )
}

export default Life
