import React from 'react'

const Undo = {
  shape: (
    <path d='M17.026 22.957c10.957-11.421-2.326-20.865-10.384-13.309l2.464 2.352h-9.106v-8.947l2.232 2.229c14.794-13.203 31.51 7.051 14.794 17.675z' />
  )
}

export default Undo
