import React from 'react'

const TableView = {
  shape: (
    <path
      d='M6,15H9V12H6ZM6,9H9V6H6ZM6,3H9V0H6Zm6,12h3V12H12Zm0-6h3V6H12Zm0-6h3V0H12ZM0,15H3V12H0ZM0,9H3V6H0ZM0,3H3V0H0Z'
    />
  )
}

export default TableView
