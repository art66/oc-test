import React from 'react'

const Minus = {
  shape: <path d='M0 10h24v4h-24z' />
}

export default Minus
