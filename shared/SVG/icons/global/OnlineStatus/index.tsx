import React from 'react'

const OnlineStatus = {
  shape: {
    default: (
      <path d='M0,6a6,6,0,1,1,6,6A6,6,0,0,1,0,6Z' />
      ),
    online: (
      <path d='M0,6a6,6,0,1,1,6,6A6,6,0,0,1,0,6Z' />
    ),
    idle: (
      <g xmlns='http://www.w3.org/2000/svg'>
        <path d='M0,6a6,6,0,1,1,6,6A6,6,0,0,1,0,6Z' />
        <path d='M5,3V7H9V6H6V3Z' fill='#f2f2f2' />
      </g>
    ),
    offline: (
      <g xmlns='http://www.w3.org/2000/svg'>
        <path d='M0,6a6,6,0,1,1,6,6A6,6,0,0,1,0,6Z' />
        <path d='M3,5H9V7H3Z' fill='#f2f2f2' />
      </g>
    )
  }
}

export default OnlineStatus
