import React from 'react'

const Checkpoint = {
  shape: (
    <path d='M8.8,3.2V.8h2.4V3.2Zm0,3.2V4h2.4V6.4Zm0,3.2V7.2h2.4V9.6Zm0,3.2V10.4h2.4v2.4ZM8,0V16h4V0ZM.8,3.2V.8H3.2V3.2Zm0,3.2V4H3.2V6.4Zm0,3.2V7.2H3.2V9.6Zm0,3.2V10.4H3.2v2.4ZM0,0V16H4V0Z' />
  )
}

export default Checkpoint
