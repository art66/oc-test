import React, { Fragment } from 'react'

const ArrowRightThin = {
  shape: (
    <Fragment>
      <polygon points='0 1 4 1 10 9 4 17 0 17 6 9 0 1' />
      <polygon points='0 1 4 1 10 9 4 17 0 17 6 9 0 1' />
      <polygon points='0 0 4 0 10 8 4 16 0 16 6 8 0 0' />
      <polygon points='0 0 4 0 10 8 4 16 0 16 6 8 0 0' />
    </Fragment>
  )
}

export default ArrowRightThin
