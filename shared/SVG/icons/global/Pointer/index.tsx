import React from 'react'

const Plus = {
  shape: <path d='M 16 7.0651 L 9.3452 9.3452 L 7.0651 16 L 0 0 L 16 7.0651 ZM 16 7.0651 L 16 7.0651 Z' />
}

export default Plus
