import React from 'react'

const Image = {
  shape: {
    default: (
      <path d="M4.38,4.89A1.31,1.31,0,1,1,5.69,6.22,1.31,1.31,0,0,1,4.38,4.89Zm7.87.44-2.2,3.56L7.88,7.15l-3.5,5.3H16.63Zm7-3.55V14.22H1.75V1.78ZM21,0H0V16H21Z" />
    ),
    edit: (
      <path d="M14.7,16.26l-3.36.68L12,13.58Zm-2-3.35,2.69,2.69,7.3-7.3L20,5.61ZM10,13.22l4.26-4.5L12.28,5.66,9.9,9.44,7.56,7.59,3.78,13.22Zm-3.32-8A1.42,1.42,0,1,0,5.18,6.61h0a1.41,1.41,0,0,0,1.42-1.4v0ZM9.8,15.14H1.89V1.89h17V4.07h1.89V0H0V17H9.41Z" />
    ),
    remove: (
      <path d="M18.42,8.5a4.25,4.25,0,1,0,4.25,4.25A4.25,4.25,0,0,0,18.42,8.5Zm2.36,4.72H16.06v-.94h4.72Zm-17,0L7.56,7.59,9.9,9.44l2.38-3.77L14,8.47a6.08,6.08,0,0,0-1.73,4.75ZM14,17H0V0H20.78V7.08a6.14,6.14,0,0,0-1.89-.45V1.89h-17V15.11H12.75A6.28,6.28,0,0,0,14,17ZM5.19,6.61A1.42,1.42,0,1,1,6.61,5.19h0a1.41,1.41,0,0,1-1.4,1.42Z" />
    )
  }
}

export default Image
