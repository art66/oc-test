import React from 'react'

const ListView = {
  shape: (
    <path d='M0,15H15V12H0ZM0,9H15V6H0ZM0,3H15V0H0Z' />
  )
}

export default ListView
