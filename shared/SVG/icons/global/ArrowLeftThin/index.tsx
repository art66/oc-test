import React, { Fragment } from 'react'

const ArrowLeftThin = {
  shape: (
    <Fragment>
      <polygon points='10 1 6 1 0 9 6 17 10 17 4 9 10 1' />
      <polygon points='10 1 6 1 0 9 6 17 10 17 4 9 10 1' />
      <polygon points='10 0 6 0 0 8 6 16 10 16 4 8 10 0' />
      <polygon points='10 0 6 0 0 8 6 16 10 16 4 8 10 0' />
    </Fragment>
  )
}

export default ArrowLeftThin
