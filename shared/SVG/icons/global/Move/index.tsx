import React from 'react'

const Move = {
  shape: (
    <path d='M6.67,3.33h-2L8,0l3.33,3.33h-2v2H6.67V3.33z M9.33,12.67h2L8,16l-3.33-3.33h2v-2h2.67V12.67z M3.33,9.33v2L0,8 l3.33-3.33v2h2v2.67H3.33z M12.67,6.67v-2L16,8l-3.33,3.33v-2h-2V6.67H12.67z' />
  )
}

export default Move
