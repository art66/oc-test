import React from 'react'

const Repeat = {
  shape: (
    <path d='M6.974 22.957c-10.957-11.421 2.326-20.865 10.384-13.309l-2.464 2.352h9.106v-8.947l-2.232 2.229c-14.794-13.203-31.51 7.051-14.794 17.675z' />
  )
}

export default Repeat
