import React from 'react'

const Cross = {
  shape: (
    <path d='M 0 0 L 6.75 8 L 0 16 L 4.5 16 L 9 11.04 L 13.5 16 L 18 16 L 11.25 8 L 18 0 L 13.5 0 L 9 4.96 L 4.5 0 L 0 0 Z' />
  )
}

export default Cross
