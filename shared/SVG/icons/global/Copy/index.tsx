import React from 'react'

const Copy = {
  shape: <path d='M18 6v-6h-18v18h6v6h18v-18h-6zm-12 10h-4v-14h14v4h-10v10zm16 6h-14v-14h14v14z' />
}

export default Copy
