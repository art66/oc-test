import React from 'react'

const PlusWide = {
  shape: <path d='M12,4H8V0H4V4H0V8H4v4H8V8h4Z' />
}

export default PlusWide
