import React from 'react'

const ArrowLeftSuperThin = {
  shape: <path d="M0,0,10,15h1L1,0ZM10,15,0,30H1L11,15Z" transform="translate(11 30) rotate(180)" />
}

export default ArrowLeftSuperThin
