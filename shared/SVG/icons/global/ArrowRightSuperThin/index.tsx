import React from 'react'

const ArrowRightSuperThin = {
  shape: <path d="M-5534-275l10-15h1l-10,15Zm10-15-10-15h1l10,15Z" transform="translate(5534 305)" />
}

export default ArrowRightSuperThin
