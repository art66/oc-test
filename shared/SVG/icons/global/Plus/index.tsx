import React from 'react'

const Plus = {
  shape: <path d='M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z' />
}

export default Plus
