import React from 'react'

const Redo = {
  shape: (
    <path d='M4.65,14.67C-2.65,7.06,6.2.76,11.57,5.8L9.93,7.37H16v-6L14.51,2.89C4.65-5.92-6.5,7.59,4.65,14.67Z' />
  )
}

export default Redo
