import React from 'react'

const FullScreen = {
  shape: <path d='M0 3v18h24v-18h-24zm22 16h-4v-10h-16v-4h20v14z' />
}

export default FullScreen
