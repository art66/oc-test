import React from 'react'

const CTHistory = {
  shape: (
    <path d='M16,8A8,8,0,0,1,0,8,8.28,8.28,0,0,1,.16,6.43H1.54A5.94,5.94,0,0,0,1.33,8,6.71,6.71,0,1,0,4,2.66L5.4,4l-4.67.9L1.63.25,3.09,1.71A7.9,7.9,0,0,1,8,0,8,8,0,0,1,16,8ZM7.33,4V9.33H12V8H8.67V4Z' />
  )
}

export default CTHistory
