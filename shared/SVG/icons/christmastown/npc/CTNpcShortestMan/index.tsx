import React from 'react'

const CTNpcShortestMan = {
  shape: (
    <path d='M5,0S1.39,1.15,1.88,2.67a3.65,3.65,0,0,0,1.25,2v.66H.94a1,1,0,0,0,0,2h.94s.25,1.77.62,2v1.34H1.88V12h2.5V10.67L5,9.33l.63,1.34V12h2.5V10.67H7.5V9.33c.37-.23.63-2,.63-2h.93a1,1,0,0,0,0-2H6.88V4.67a3.72,3.72,0,0,0,1.25-2C8.61,1.15,5,0,5,0Z' />
  )
}

export default CTNpcShortestMan
