import React from 'react'
import * as a from './animation'

const handMotionKeyTimes = '0; 0.06; 0.12; 0.18; 0.24; 0.3; 0.36; 0.42; 0.48; 0.54; 0.6; 0.66; 0.72; 0.78; 0.84; 1'
const handsUpKeyTimes = '0; 0.06; 0.12; 0.18; 0.24; 0.3; 0.7; 0.76; 0.82; 0.88; 0.94; 1'
const pulsingKeyTimes = '0; 0.1; 0.2; 1'

const CTPlayer = {
  shape: {
    default: (
      <g>
        <path className='iconBorderPath' d='M 4.0128 8.1084 C 3.905 8.8446 4.3118 9.8234 5.265 9.98 L 5.2595 9.8994 C 5.2583 9.8993 5.2512 9.9957 5.52 10 C 5.4324 9.9989 5.3448 9.993 5.265 9.98 L 5.5886 14.104 C 5.6744 15.2124 6.5726 16 7.5988 16 L 8.4008 16 C 9.425 16 10.3247 15.2151 10.4109 14.1055 L 10.735 9.985 C 10.6494 9.999 10.5576 10.0076 10.4633 10.0076 C 10.7573 10.0076 10.7399 9.8992 10.7396 9.9025 L 10.735 9.985 C 11.684 9.8292 12.095 8.8582 11.9873 8.1191 L 11.6292 5.6429 C 11.5114 4.8411 11.021 4.2096 10.38 3.915 C 10.5936 3.5391 10.7173 3.1045 10.7173 2.6389 C 10.7173 1.158 9.484 0 8 0 C 6.5154 0 5.2825 1.158 5.2825 2.6389 C 5.2825 3.1044 5.4065 3.5391 5.62 3.915 C 4.9784 4.2092 4.4875 4.8398 4.3706 5.6429 L 4.0128 8.1084 Z' />
        <path className='iconPath' d='M 8 4.2773 C 8.9482 4.2773 9.7173 3.5437 9.7173 2.6389 C 9.7173 1.7336 8.9482 1 8 1 C 7.0515 1 6.2825 1.7336 6.2825 2.6389 C 6.2825 3.5437 7.0515 4.2773 8 4.2773 ZM 10.9873 8.1191 L 10.6292 5.6429 C 10.5574 5.1545 10.0758 4.7334 9.5591 4.7334 L 6.4407 4.7334 C 5.9236 4.7334 5.4418 5.1545 5.3706 5.6429 L 5.0128 8.1084 C 4.9413 8.5972 5.177 9.0024 5.5363 9.0024 C 5.8955 9.0024 6.2214 9.4077 6.2595 9.8997 L 6.5886 14.104 C 6.6267 14.5962 7.0815 15 7.5988 15 L 8.4008 15 C 8.9182 15 9.3727 14.5973 9.4109 14.1055 L 9.7396 9.9025 C 9.7782 9.4103 10.1041 9.0076 10.4633 9.0076 C 10.8225 9.0076 11.0585 8.6079 10.9873 8.1191 Z' />
      </g>
    ),
    handMotion: (
      <g>
        <path className='iconBorderPath'>
          <animate attributeName='d' repeatCount='indefinite' keyTimes={handMotionKeyTimes} calcMode='linear' dur='1s' values={a.handMotionBorder} />
        </path>
        <path className='iconPath'>
          <animate attributeName='d' repeatCount='indefinite' keyTimes={handMotionKeyTimes} calcMode='linear' dur='1s' values={a.handMotionIcon} />
        </path>
      </g>
    ),
    handsUp: (
      <g>
        <path className='iconBorderPath'>
          <animate attributeName='d' repeatCount='indefinite' keyTimes={handsUpKeyTimes} calcMode='linear' dur='1s' values={a.handsUpBorder} />
        </path>
        <path className='iconPath'>
          <animate attributeName='d' repeatCount='indefinite' keyTimes={handsUpKeyTimes} calcMode='linear' dur='1s' values={a.handsUpIcon} />
        </path>
      </g>
    ),
    pulsing: (
      <g>
        <path className='iconBorderPath'>
          <animate attributeName='d' repeatCount='indefinite' keyTimes={pulsingKeyTimes} calcMode='linear' dur='1s' values={a.pulsingBorder} />
        </path>
        <path className='iconPath'>
          <animate attributeName='d' repeatCount='indefinite' keyTimes={pulsingKeyTimes} calcMode='linear' dur='1s' values={a.pulsingIcon} />
        </path>
      </g>
    )
  }
}

export default CTPlayer
