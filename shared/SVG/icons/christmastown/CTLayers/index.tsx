import React from 'react'

const CTLayers = {
  shape: (
    <path d='M14.47,7.11,16,8,8,12.67,0,8l1.53-.89L8,10.88ZM8,14.21,1.54,10.44,0,11.33,8,16l8-4.67-1.53-.89ZM8,1.54l5.35,3.12L8,7.79,2.65,4.67ZM8,0,0,4.67,8,9.34l8-4.67Z' />
  )
}

export default CTLayers
