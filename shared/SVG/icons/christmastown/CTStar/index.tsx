import React from 'react'

const CTStar = {
  shape: (
    <path d='M845,353l5.407,11.192,12.093,1.8-8.75,8.711,2.065,12.3L845,381.192,834.185,387l2.065-12.3-8.75-8.711,12.093-1.8Z' />
  )
}

export default CTStar
