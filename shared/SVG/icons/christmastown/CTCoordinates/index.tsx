import React from 'react'

const CTCoordinates = {
  shape: (
    <path d='M10,6,8,0,6,6,0,8l6,2,2,6,2-6,6-2ZM8,8,9.27,9.27l-.14.47L8,13.07V8L6.73,9.27l-.47-.13L2.93,8H8L6.73,6.74l.13-.47L8,2.94V8L9.26,6.73h0l.47.13L13.06,8H8Zm3.6-1.46L10,6l1.6-1.6-1.8.93L9.46,4.4l4.2-2.07ZM6.53,4.4,6,6,4.4,4.4l.93,1.8-.93.34L2.33,2.34ZM11.6,9.47l2.07,4.2L9.47,11.6,10,10l1.6,1.6-.93-1.8Zm-7.2,0L6,10,4.4,11.6l1.8-.93.33.93-4.2,2.07Z' />
  )
}

export default CTCoordinates
