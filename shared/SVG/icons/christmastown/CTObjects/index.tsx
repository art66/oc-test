import React from 'react'

const CTObjects = {
  shape: (
    <path d='M14,8.18V15H10V10.91H6V15H2V8.18H0L8,0l8,8.18Zm-.67-4V.68h-2V2.11Z' />
  )
}

export default CTObjects
