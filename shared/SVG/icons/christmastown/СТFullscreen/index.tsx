import React from 'react'

const CTFullscreen = {
  shape: (
    <path d='M0,0V12H16V0ZM14.67,10.67H12V4H1.33V1.33H14.66v9.34Z' />
  )
}

export default CTFullscreen
