import React from 'react'

const CTBlocking = {
  shape: (
    <path d='M13.75,13.64H15V15H0V13.64H1.25L6.25,0h2.5ZM10.66,8.86H4.34l-.68,2.05h7.72Zm-2-5.45H6.31L5.62,5.46H9.39l-.72-2Z' />
  )
}

export default CTBlocking
