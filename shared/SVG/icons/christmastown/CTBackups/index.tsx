import React from 'react'

const CTBackups = {
  shape: (
    <path d='M10,2h2V5.33H10Zm6,.67V16H0V0H13.33ZM3.33,6h9.33V1.33H3.33ZM14,8.67H2v6H14ZM12.67,10H3.33v.67h9.33V10Zm0,1.33H3.33V12h9.33v-.67Zm0,1.34H3.33v.67h9.33v-.67Z' />
  )
}

export default CTBackups
