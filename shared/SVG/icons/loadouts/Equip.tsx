import React from 'react'

const Equip = {
  shape: (
    <g transform="translate(-194 -267)">
      <g transform="matrix(1, 0, 0, 1, 194, 267)">
        <circle cx="7" cy="7" r="7" transform="translate(0 1)" />
      </g>
      <path d="M-945-136v-3h-3v-2h3v-3h2v3h3v2h-3v3Z" transform="translate(1145 415)" fill="#fff" />
    </g>
  )
}

export default Equip
