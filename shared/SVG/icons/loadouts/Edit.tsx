import React from 'react'

const Edit = {
  shape: (
    <path
      xmlns="http://www.w3.org/2000/svg"
      d="M4.752,15.043,0,16,.96,11.25ZM1.9,10.307,5.694,14.1,16,3.793,12.208,0,1.9,10.307Z"
      transform="translate(0.64 0.71)"
    />
  )
}

export default Edit
