import React from 'react'

const Reset = {
  shape: (
    <g xmlns="http://www.w3.org/2000/svg" transform="matrix(1, 0, 0, 1, 0, 0)">
      <path
        d="M6.75,9.75,3.4,13.477,0,9.75H2.25a6.743,6.743,0,0,1,11.384-4.9l-1.5,1.673A4.5,4.5,0,0,0,4.5,9.75H6.75ZM14.6,6.023,11.25,9.75H13.5a4.5,4.5,0,0,1-7.631,3.226l-1.5,1.673A6.743,6.743,0,0,0,15.75,9.75H18L14.6,6.023Z"
        transform="translate(1.13 -2.5)"
        strokeMiterlimit="10"
      />
    </g>
  )
}

export default Reset
