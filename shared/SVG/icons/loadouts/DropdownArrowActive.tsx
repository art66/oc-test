import React from 'react'

const DropdownArrowActive = {
  shape: <path d="M1302,21l-5,5V16Z" transform="translate(-1294 -13)" />
}

export default DropdownArrowActive
