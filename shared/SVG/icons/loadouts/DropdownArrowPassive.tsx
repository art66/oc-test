import React from 'react'

const DropdownArrowPassive = {
  shape: <path d="M1302,21l-5,5V16Z" transform="translate(29 -1294) rotate(90)" />
}

export default DropdownArrowPassive
