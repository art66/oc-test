import React from 'react'

const Save = {
  shape: (
    <path
      xmlns="http://www.w3.org/2000/svg"
      d="M13.523,2,6,9.711,2.476,6.371,0,8.848l6,5.819L16,4.477Z"
      transform="translate(0.71 -1.29)"
    />
  )
}

export default Save
