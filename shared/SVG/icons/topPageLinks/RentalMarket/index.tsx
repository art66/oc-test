import React from 'react'

const RentalMarket = {
  shape: (
    <path d="M8,16V1H6V5H2L0,6.5,2,8H6v8ZM14,4H8L9.59,7H14l2-1.47Z"/>
  )
}

export default RentalMarket
