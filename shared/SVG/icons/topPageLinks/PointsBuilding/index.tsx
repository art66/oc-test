import React from 'react'

const PointsBuilding = {
  shape: (
    <path d="M2.14,10A6.86,6.86,0,1,1,9,16.86,6.87,6.87,0,0,1,2.14,10ZM0,10A9,9,0,1,0,9,1,9,9,0,0,0,0,10Zm10,0H8V8h2ZM6,6v8H8V12h2a2,2,0,0,0,2-2V8a2,2,0,0,0-2-2Z"/>
  )
}

export default PointsBuilding
