import React from 'react'

const ModerateMatches = {
  shape: (
    <path d="M12,1H2A2,2,0,0,0,0,3V17a2,2,0,0,0,2,2H12a2,2,0,0,0,2-2V3a2,2,0,0,0-2-2M2,17V3H12V5.4L6.88,10.21,3.66,8.28,3,9l4.17,4.36L12,6.88V17Z"/>
  )
}

export default ModerateMatches
