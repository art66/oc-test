import React from 'react'

const ViewProposals = {
  shape: (
    <path d="M9.35,3.93l-.9,1.25a5.3,5.3,0,1,1-3.32,0L4.22,3.93a6.79,6.79,0,1,0,5.13,0ZM5.64,5l.48.66.67,0,.66,0L7.94,5l.93-1.29L10,2.25,8.94,1H4.63l-1,1.25,1.09,1.5Z"/>
  )
}

export default ViewProposals
