import React from 'react'

const AddEnemy = {
  shape: (
    <path d="M12,11.43c-2-.45-3.83-.85-2.94-2.54C11.79,3.75,9.79,1,6.92,1S2,3.85,4.77,8.89c.92,1.69-1,2.1-2.94,2.54C.11,11.83,0,12.67,0,14.12v.72H13.83v-.72C13.84,12.67,13.72,11.83,12,11.43Zm2.26-3.94V5.76H12.54V7.49H10.81V9.22h1.73V11h1.73V9.22H16V7.49Z"/>
  )
}

export default AddEnemy
