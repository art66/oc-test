import React from 'react'

const StockExchange = {
  shape: (
    <path d="M6.82,14.33H8.18V17H6.82V14.33Zm1.95,0L11.1,17h1.79l-2.33-2.67ZM2.1,17H3.89l2.33-2.67H4.43ZM7.28,7l1,1,2.31-2.25.57.55.41-2-2,.37.58.57L8.31,7.05l-1-1L4.78,8.47l.48.47Zm4.31,2.67H4.09V4.33H3.41v6h8.18ZM15,3h-.68V13H.68V3H0V1H15V3ZM13,3H2.05v8.67H13V3Z"/>
  )
}

export default StockExchange
