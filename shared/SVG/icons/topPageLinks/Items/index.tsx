import React from 'react'

const Items = {
  shape: (
    <path d="M10.32,7.16,8,8.43.79,4.23,3.11,2.92Zm1.38-.73L14,5.23,6.51,1,4.45,2.16ZM7.33,9.59,0,5.31v7.32l7.33,4.18Zm7.34-3.23v7.22L8.67,17V9.58Z"/>
  )
}

export default Items
