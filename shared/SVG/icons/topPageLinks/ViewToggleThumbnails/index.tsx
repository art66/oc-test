import React from 'react'

const ViewToggleThumbnails = {
  shape: (
    <path d="M6,16H9V13H6Zm0-6H9V7H6ZM6,4H9V1H6Zm6,12h3V13H12Zm0-6h3V7H12Zm0-6h3V1H12ZM0,16H3V13H0Zm0-6H3V7H0ZM0,4H3V1H0Z"/>
  )
}

export default ViewToggleThumbnails
