import React from 'react'

const Reporters = {
  shape: (
    <path d="M13.88,13.06c-2.29-.53-4.43-1-3.39-2.94C13.63,4.18,11.32,1,8,1S2.36,4.3,5.51,10.12c1.07,2-1.15,2.42-3.39,2.94C.13,13.52,0,14.49,0,16.17V17H16v-.83c0-1.68-.13-2.65-2.12-3.11"/>
  )
}

export default Reporters
