import React from 'react'

const RadioButtonChecked = {
  shape: (
    <path d="M2,7.5A4.5,4.5,0,1,1,6.5,12,4.5,4.5,0,0,1,2,7.5m-2,0A6.5,6.5,0,1,0,6.5,1,6.5,6.5,0,0,0,0,7.5m4,0A2.48,2.48,0,1,0,6.5,5,2.48,2.48,0,0,0,4,7.5"/>
  )
}

export default RadioButtonChecked
