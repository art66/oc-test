import React from 'react'

const UploadNewImage = {
  shape: (
    <path d="M3.33,7.67H0L6,1l6,6.67H8.67v6.66H3.33Zm7.34,6v2H1.33v-2H0V17H12V13.67Z"/>
  )
}

export default UploadNewImage
