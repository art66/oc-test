import React from 'react'

const Send = {
  shape: (
    <path d="M16,1,12,15.67,6.58,10.84l5.2-5.49-7,4.82L0,9ZM6,12.11V17l2.17-2.95Z"/>
  )
}

export default Send
