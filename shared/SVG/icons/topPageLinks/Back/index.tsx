import React from 'react'

const Back = {
  shape: (
    <path d="M16,13S14.22,4.41,6.42,4.41V1L0,6.7l6.42,5.9V8.75c4.24,0,7.37.38,9.58,4.25"/>
  )
}

export default Back
