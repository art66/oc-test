import React from 'react'

const OverseasBank = {
  shape: (
    <path d="M4.67,15H2V7.67H4.67ZM9.33,7.67H6.67V15H9.33Zm4.67,0H11.33V15H14Zm1.33,8H.67V17H15.33ZM0,7H16L8,1Z"/>
  )
}

export default OverseasBank
