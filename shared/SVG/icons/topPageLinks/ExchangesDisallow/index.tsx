import React from 'react'

const ExchangesDisallow = {
  shape: (
    <path d="M15.56,3.41,14.14,2,11.71,4.43a9.07,9.07,0,0,0-5-1.78V1L2.86,4.88l3.9,3.88V7.1A8.24,8.24,0,0,1,9,7.12l-5.1,5.1A7.76,7.76,0,0,1,0,8,10.58,10.58,0,0,0,2,14.1l-2,2,1.41,1.42,2.05-2a9,9,0,0,0,5,1.84V19l3.9-3.88-3.9-3.87V12.9a8.12,8.12,0,0,1-2.36,0l5.12-5.11a7.69,7.69,0,0,1,4,4.21,10.5,10.5,0,0,0-2.09-6.13Z"/>
  )
}

export default ExchangesDisallow
