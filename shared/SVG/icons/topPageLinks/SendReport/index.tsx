import React from 'react'

const SendReport = {
  shape: (
    <path d="M8,1,0,15.67H16ZM7.33,6.33H8.67V11H7.33ZM8,13.83A.83.83,0,1,1,8.83,13,.83.83,0,0,1,8,13.83Z"/>
  )
}

export default SendReport
