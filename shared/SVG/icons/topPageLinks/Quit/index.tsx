import React from 'react'

const Quit = {
  shape: (
    <polygon points="0 1 5.25 7 0 13 3.5 13 7 9.28 10.5 13 14 13 8.75 7 14 1 10.5 1 7 4.72 3.5 1 0 1"/>
  )
}

export default Quit
