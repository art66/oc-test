import React from 'react'

const AddPoll = {
  shape: (
    <path d="M16,9.65A8,8,0,1,1,7.35,1V9.65ZM8.69,8.31H16A8,8,0,0,0,8.69,1Z"/>
  )
}

export default AddPoll
