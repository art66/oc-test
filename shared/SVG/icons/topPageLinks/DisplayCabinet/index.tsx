import React from 'react'

const DisplayCabinet = {
  shape: (
    <path d="M11,9.4v-2l3-1.8v2Zm0,4v-3l3-1.8v3Zm0,4v-3l3-1.8v3ZM15,16V4L5,1,0,3V15l10,4Zm-5-1.36v3L1,14.37v-3Zm0-4v3L1,10.37v-3Zm0-3v2L1,6.37v-2Zm0-1L1.31,3.48,5,2l8.89,2.67Z"/>
  )
}

export default DisplayCabinet
