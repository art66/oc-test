import React from 'react'

const JobListings = {
  shape: (
    <path d="M12,1H2A2,2,0,0,0,0,3V17a2,2,0,0,0,2,2H12a2,2,0,0,0,2-2V3A2,2,0,0,0,12,1Zm0,16H2V3H12ZM10,5H4V7h6Zm0,4H4v2h6Zm0,4H4v2h6Z"/>
  )
}

export default JobListings
