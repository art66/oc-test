import React from 'react'

const TrackMatchBpp = {
  shape: (
    <path d="M10,1,2,14H4L12,1Zm-4,3A3,3,0,1,0,3,7,3,3,0,0,0,5.94,4ZM1.47,4c0-1.1.68-2,1.5-2s1.5.9,1.5,2S3.8,6,3,6,1.47,5.1,1.47,4ZM11,8a3,3,0,1,0,3,3A3,3,0,0,0,11,8Zm0,5c-.82,0-1.5-.9-1.5-2s.68-2,1.5-2,1.5.9,1.5,2S11.8,13,11,13Z"/>
  )
}

export default TrackMatchBpp
