import React from 'react'

const Parameditor = {
  shape: (
    <path d='M4,12H3v3H1V12H0V10H4ZM3,0H1V8H3Zm7,5H6V7H7v8H9V7h1ZM9,0H7V3H9Zm7,10H12v2h1v3h2V12h1ZM15,0H13V8h2Z' />
  )
}

export default Parameditor
