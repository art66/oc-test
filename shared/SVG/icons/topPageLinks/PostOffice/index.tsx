import React from 'react'

const PostOffice = {
  shape: (
    <path d="M8,2H7A1,1,0,0,0,6,1H5A1,1,0,0,0,4,2H3V3H8Zm2,1L9,4H2L1,3A.93.93,0,0,0,0,4V14a2,2,0,0,0,2,2H9a2,2,0,0,0,2-2V4A.93.93,0,0,0,10,3ZM6,13H2V12H6Zm3-2H2V10H9ZM9,9H2V8H9Z"/>
  )
}

export default PostOffice
