import React from 'react'

const StartACompany = {
  shape: (
    <path d="M1.33,17H0V1H1.33ZM5.44,1.67A4.62,4.62,0,0,0,2.67,2.76v7.57c1.63-2.46,3.52-1.69,5-1.27,3,.83,4.46-1,5.65-3.18C9.05,7.65,9.4,1.67,5.44,1.67"/>
  )
}

export default StartACompany
