import React from 'react'

const AddEnemy = {
  shape: (
    <path  d="M12,1H2A2,2,0,0,0,0,3V17a2,2,0,0,0,2,2H12a2,2,0,0,0,2-2V3A2,2,0,0,0,12,1Zm0,16H2V3H12ZM6,13H8V11h2V9H8V7H6V9H4v2H6Z" />
  )
}

export default AddEnemy
