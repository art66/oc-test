import React from 'react'

const ViewToggleList = {
  shape: (
    <path d="M0,16H15V13H0Zm0-6H15V7H0ZM0,4H15V1H0Z"/>
  )
}

export default ViewToggleList
