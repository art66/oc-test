import React from 'react'

const Newsletter = {
  shape: (
    <path d="M2,10h9v1H2ZM2,7h9V8H2ZM2,4h9V5H2ZM0,1V15H9l4-4V1Z"/>
  )
}

export default Newsletter
