import React from 'react'

const Awards = {
  shape: (
    <path d="M16,2.33a7.16,7.16,0,0,1-4.58,6.3c.2-.37.39-.77.58-1.19a5.92,5.92,0,0,0,3-4.17H13.19c0-.3.07-.62.09-.94Zm-16,0a7.16,7.16,0,0,0,4.58,6.3c-.2-.37-.4-.77-.59-1.19A5.93,5.93,0,0,1,1,3.27H2.81c0-.3-.07-.62-.09-.94ZM12.67,1c0,6.54-3.4,8.7-3.73,11.33H7.07C6.74,9.7,3.33,7.54,3.33,1ZM7.8,9.7A15.27,15.27,0,0,1,6,2.33H4.9A11.89,11.89,0,0,0,7.8,9.7M9,13H7.05c-.14.79-.42,1.75-1.72,1.75v.92h5.34v-.92c-1.34,0-1.58-1-1.71-1.75"/>
  )
}

export default Awards
