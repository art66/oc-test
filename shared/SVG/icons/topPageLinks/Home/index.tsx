import React from 'react'

const Home = {
  shape: (
    <path d="M14,8.86V15.4H9.64V11H6.36V15.4H2V8.86H0L8,1l8,7.86ZM12.91,5.1V2.31H10.73v.88Z"/>
  )
}

export default Home
