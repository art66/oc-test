import React from 'react'

const PostSticky = {
  shape: (
    <path d="M13.33,9.67V1H0V17H5.6c3.24,0,2.23-5.33,2.23-5.33,2,.49,5.5.28,5.5-2M10.67,5h-8V4.33h8Zm0,2h-8V6.33h8Zm0,2h-8V8.33h8Zm-1.4,4.15a5.86,5.86,0,0,0,4.06-.79A21,21,0,0,1,8.38,17a5.57,5.57,0,0,0,.89-3.8"/>
  )
}

export default PostSticky
