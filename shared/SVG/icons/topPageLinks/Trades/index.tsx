import React from 'react'

const Trades = {
  shape: (
    <path d="M6.76,7.1V8.76L2.86,4.88,6.76,1V2.65s7.93,0,8.47,9.31A7.94,7.94,0,0,0,6.76,7.1ZM0,8c.54,9.31,8.47,9.31,8.47,9.31V19l3.9-3.88-3.9-3.87V12.9A7.94,7.94,0,0,1,0,8Z"/>
  )
}

export default Trades
