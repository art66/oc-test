import React from 'react'

const RecruitCitizens = {
  shape: (
    <path xmlns='http://www.w3.org/2000/svg' id='Path_299-2' data-name='Path 299' d='M381,1330a4.662,4.662,0,0,0,3.345-7.914l1.735-1.559a6.993,6.993,0,0,1-5.08,11.806v2.333l-3.865-3.529,3.865-3.471Zm0-14v2.333a6.993,6.993,0,0,0-5.08,11.806l1.735-1.559a4.662,4.662,0,0,1,3.345-7.914V1323l3.865-3.471Z' transform='translate(-374 -1315)' />
  )
}

export default RecruitCitizens
