import React from 'react'

const UncheckAll = {
  shape: (
    <path d="M12,1H2A2,2,0,0,0,0,3V17a2,2,0,0,0,2,2H12a2,2,0,0,0,2-2V3a2,2,0,0,0-2-2m0,16H2V3H12Z"/>
  )
}

export default UncheckAll
