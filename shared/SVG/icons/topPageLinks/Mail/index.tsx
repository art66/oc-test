import React from 'react'

const Mail = {
  shape: (
    <path d="M8,7.24,0,1H16ZM4.38,6.06,0,2.65v8Zm7.24,0L16,10.67v-8Zm-1,.82L8,8.89l-2.58-2L0,12.56H16Z"/>
  )
}

export default Mail
