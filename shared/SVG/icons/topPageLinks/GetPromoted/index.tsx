import React from 'react'

const GetPromoted = {
  shape: (
    <path d="M4,16.38H9v-2H4Zm0-3H9v-3H4ZM6.5,1,0,6.38H4v3H9v-3h4Z"/>
  )
}

export default GetPromoted
