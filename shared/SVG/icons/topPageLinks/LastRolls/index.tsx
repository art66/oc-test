import React from 'react'

const LastRolls = {
  shape: (
    <path d="M9,1A7,7,0,0,0,2.09,7.83H0l3.13,3.5,3.13-3.5H3.83A5.22,5.22,0,1,1,9,13.25a5.15,5.15,0,0,1-3.08-1l-1.2,1.29A6.9,6.9,0,0,0,9,15,7,7,0,0,0,9,1M8.42,4.24v4L11,10.81l.84-.84L9.62,7.75V4.24Z"/>
  )
}

export default LastRolls
