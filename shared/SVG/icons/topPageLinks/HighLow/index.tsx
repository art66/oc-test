import React from 'react'

const HighLow = {
  shape: (
    <path d="M4.5,1,0,6.38H3.28v7H5.72v-7H9Zm8.22,7.38v-7H10.28v7H7L11.5,14,16,8.38Z"/>
  )
}

export default HighLow
