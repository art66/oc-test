import React from 'react'

const Popout = {
  shape: (
    <path d="M0,5V16H13V14H2V5ZM5,5h9v6H5ZM3,13H16V1H3Z"/>
  )
}

export default Popout
