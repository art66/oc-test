import React from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import merge from 'lodash.merge'

// import iconsHolder from '../helpers/iconsHolder'
import { isNAU, defsObserver, getSVGdefs } from '../utils'
import isSafari from '../../utils/safariChecker'
import isValue from '../../utils/isValue'
import PRESETS from '../presets/icons'

import { XMLNS } from '../constants'
import { DEFAULT_FILTER, DEFAULT_DIMENSIONS, DEFAULT_GRADIENT, FILLING } from '../constants/icons'
import { IProps, IFilter, IHover, IShadow, IGragientItem, ICustomFilter, IGradient, IDefsProps } from '../types/icons'

import styles from './index.cssmodule.scss'

class SVGIconGenerator extends React.PureComponent<IProps, any> {
  private _parentNode: any
  private _ref: React.RefObject<SVGSVGElement>
  private _isMounted: boolean

  constructor(props: IProps) {
    super(props)

    this.state = {
      updateType: 'props',
      isIconMounted: false
    }

    this._ref = React.createRef()
  }

  // setting incoming props for internal manipulations via Events
  static getDerivedStateFromProps(nextProps: IProps, prevState: any) {
    if (!prevState.updateType || prevState.updateType === 'props') {
      return {
        ...prevState,
        ...nextProps
      }
    }

    if (prevState.updateType === 'state') {
      return {
        updateType: '' // reset flag to allow update from incoming props
      }
    }

    return null
  }

  // getting parent node and installing event listeners for parent node
  async componentDidMount() {
    this._setAsyncMountFlag(true)

    await this._setIcon()
    this._initRefs()
    this._addHandlerListeners()
  }

  // watching for listeners props update (rare case, only need when listeners doesn't set on initial render)
  componentDidUpdate(prevProps: IProps) {
    const { onClick: prevOnClick = {}, onHover: prevOnHover = {} } = prevProps
    const { onClick = {}, onHover = {} } = this.props

    if (prevOnHover.active !== onHover.active || prevOnClick.active !== onClick.active) {
      this._initRefs()
      this._removeHandlerListeners()
      this._addHandlerListeners()
    }
  }

  // removing event listeners for parent node
  componentWillUnmount() {
    this._removeHandlerListeners()
    this._setAsyncMountFlag(false)
    // defsObserver.destroyDefs()
  }

  _initRefs = () => {
    const { isDeepParent } = this.props

    const refNode = this._ref && this._ref.current
    const refNodeParent = refNode && refNode.parentNode

    this._parentNode = isDeepParent ? refNodeParent && refNodeParent.parentNode : refNodeParent
  }

  _handlerMouseOver = () => {
    this._parentNode.addEventListener('mouseover', this._onEnter)
    this._parentNode.addEventListener('touchstart', this._onEnter)
  }

  _handlerMouseLeave = () => {
    this._parentNode.addEventListener('mouseleave', this._onLeave)
    this._parentNode.addEventListener('touchend', this._onLeave)
  }

  _handlerMouseClick = () => {
    this._parentNode.addEventListener('click', this._onClick)
  }

  _addHandlerListeners = () => {
    const { onClick = {}, onHover = {} } = this.props

    if (!this._parentNode) {
      return
    }

    onHover.active && this._handlerMouseOver()
    onHover.active && this._handlerMouseLeave()
    onClick.active && this._handlerMouseClick()
  }

  _removeHandlerListeners = () => {
    if (!this._parentNode) return

    this._parentNode.removeEventListener('mouseover', this._onEnter)
    this._parentNode.removeEventListener('touchstart', this._onEnter)
    this._parentNode.removeEventListener('mouseleave', this._onLeave)
    this._parentNode.removeEventListener('touchend', this._onLeave)
    this._parentNode.removeEventListener('click', this._onClick)
  }

  _setAsyncMountFlag = (status = false) => {
    this._isMounted = status
  }

  _setIconsState = icons => {
    this.setState({
      iconsToRender: icons,
      updateType: 'state',
      isIconMounted: true
    })
  }

  _setIcon = async () => {
    const { iconsHolder } = this.state

    if (!iconsHolder || iconsHolder instanceof Promise) {
      // eslint-disable-next-line
      const iconsDynamicImport = iconsHolder || await import(/* webpackChunkName: "svgIconsChunk" */ '../helpers/iconsHolder/index')
      const { default: icons } = await iconsDynamicImport

      if (!this._isMounted) {
        return
      }

      this._setIconsState(icons)

      return
    }

    this._setIconsState(iconsHolder)
  }

  _getIcon = () => {
    const { iconsToRender, iconName } = this.state

    if (!iconsToRender || !iconName || !iconsToRender[iconName]) {
      return null
    }

    return iconsToRender[iconName]
  }

  // Tooks already setted props from ./preset/ folder for icon calculation
  _getIconPreset = () => {
    const { preset } = this.state

    if (!preset || !preset.type || !PRESETS[preset.type]) {
      return {}
    }

    const { type, subtype } = preset

    return PRESETS[type][subtype] || PRESETS[type]['DEFAULTS']
  }

  // Getting props for icon building.
  // Can accept preset config or custom one.
  _getIconProps = () => {
    const { filter, gradient, fill, dimensions, deepProps } = this.state
    const iconPreset = this._getIconPreset()

    const iconFill = this._getFill(fill, iconPreset)
    const iconDimensions = this._getDimensions(dimensions, iconPreset)
    const iconFilter = filter || iconPreset.filter || DEFAULT_FILTER
    const iconGradient = gradient || iconPreset.gradient || DEFAULT_GRADIENT
    const iconDeepProps = deepProps || iconPreset.deepProps

    return {
      fill: iconFill,
      dimensions: iconDimensions,
      filter: iconFilter,
      gradient: iconGradient,
      deepProps: iconDeepProps
    }
  }

  _renderGlobalSVGDefs = () => {
    const { addDef, getDefs } = defsObserver
    const { filter, gradient } = this._getIconProps()

    // particular node selectors in the DOM for storing svg's defs
    const mutualSelector = '.svgs_defs_container > svg[class="react_svgs_defs"]'

    if (!document.querySelector('.svgs_defs_container')) {
      // console.error('Error: no any DOM node to svg preset inserting were found!')

      return
    }

    const filtersDefsNode = document.querySelector(`${mutualSelector} > defs[class="react_svgs__filters"]`)
    const gradientsDefsNode = document.querySelector(`${mutualSelector} > defs[class="react_svgs__gradients"]`)

    const defsParamToRender = ({ callback, type, def, node, optimizeSVGInjection }: IDefsProps) => {
      if (getDefs()[type].includes(def.ID) || !node) {
        return null
      }

      addDef({ type, ID: def.ID })

      const svgChildDefs = renderToStaticMarkup(callback(def)) as string
      const defsData = optimizeSVGInjection ? getSVGdefs(svgChildDefs) : svgChildDefs

      node.innerHTML += defsData
    }

    defsParamToRender({
      callback: this._createFilter,
      type: 'filters',
      def: filter,
      node: filtersDefsNode
    })

    defsParamToRender({
      callback: this._getColorGradient,
      type: 'gradients',
      def: gradient,
      node: gradientsDefsNode,
      optimizeSVGInjection: true
    })
  }

  // Can accepts any custom svg styles (gradient, etc.), see MDN for more info.
  _getDefs = () => {
    const { iconName = '', type = '', activeDefs = true } = this.state

    const iconToRender = this._getIcon()

    if (!iconName || !iconToRender.defs || !activeDefs) {
      return null
    }

    if (type) {
      const defsByType = iconToRender.defs[type]

      return (defsByType && defsByType.default) || defsByType || null
    }

    return iconToRender.defs.default || iconToRender.defs
  }

  // Almost all icons on Torns' have own shadows.
  // By default svg/css params is: 1px 1px 1px rgba(255, 255, 255, 0.65).
  // You can customize it on you own.
  _getShadow = (shadow: IShadow = {}) => {
    if (shadow && shadow.active === false) {
      return null
    }

    const config = {
      dx: isValue(shadow.x) ? shadow.x : DEFAULT_FILTER.SHADOW.X,
      dy: isValue(shadow.y) ? shadow.y : DEFAULT_FILTER.SHADOW.Y,
      stdDeviation: isValue(shadow.blur) ? shadow.blur : DEFAULT_FILTER.SHADOW.BLUR,
      floodColor: isValue(shadow.color) ? shadow.color : DEFAULT_FILTER.SHADOW.COLOR
    }

    return (
      // @ts-ignore - fix for TS for React SVG nodes name glitch, "as any" is type fix for react ^16.11.0
      <fedropshadow {...config} />
    )
  }

  // Calculates gradient effect by count of colors provided
  _gradientFiller = (item: IGragientItem) => {
    return <stop key={item.color} offset={item.step} stopColor={item.color} />
  }

  // Respond for gradient color creation of the icon.
  // Link both gradient and icon with created gradient by the same ID setted.
  _getColorGradient = (gradient: IGradient) => {
    const {
      ID = '',
      active = true,
      scheme = [],
      transform = '',
      coords = {},
      gradientUnits = 'objectBoundingBox',
      additionalProps = {}
    } = gradient || {}
    const { x1, x2, y1, y2 } = coords
    const disableGradient = !ID || !active || !gradient || Object.keys(gradient).length === 0

    if (disableGradient) {
      return null
    }

    const gradientScheme = scheme.map(item => {
      return this._gradientFiller(item)
    })

    // use <svg> wrapper just because of issue inside React itself
    // origin: https://github.com/facebook/react/issues/18258
    return (
      <svg>
        <linearGradient
          id={ID}
          x1={x1}
          y1={y1}
          x2={x2}
          y2={y2}
          gradientUnits={gradientUnits}
          gradientTransform={transform}
          {...additionalProps}
        >
          {gradientScheme}
        </linearGradient>
      </svg>
    )
  }

  // Respond for icon colors generation.
  // Can use custom, global-Torn or prepeared preset style
  _getFill = (fill, preset) => {
    const { name = '', color = '', stroke = '', strokeWidth = '' } = fill || {}
    const { fill: presetFill = {} } = preset
    const { color: globalColor = '', stroke: globalStroke = '', strokeWidth: globalStrokeWidth = '' } =
      FILLING[name] || FILLING.DEFAULT || {}

    const colorFill = isNAU(color) ? color : presetFill.color || globalColor
    const colorStroke = isNAU(stroke) ? stroke : presetFill.stroke || globalStroke
    const widthStroke = isNAU(strokeWidth) ? strokeWidth : presetFill.strokeWidth || globalStrokeWidth

    return {
      fill: colorFill,
      stroke: colorStroke,
      strokeWidth: widthStroke
    }
  }

  // Respond for icon size generation.
  _getDimensions = (dimensions, preset) => {
    const { width = '', height = '', viewbox = '' } = dimensions || {}
    const { dimensions: presetDimensions = {} } = preset

    const dimensionWidth = isNAU(width) ? width : presetDimensions.width || DEFAULT_DIMENSIONS.WIDTH
    const dimensionHeight = isNAU(height) ? height : presetDimensions.height || DEFAULT_DIMENSIONS.HEIGHT
    const dimensionViewBox = isNAU(viewbox) ? viewbox : presetDimensions.viewbox || DEFAULT_DIMENSIONS.VIEWBOX

    return {
      width: dimensionWidth,
      height: dimensionHeight,
      viewBox: dimensionViewBox
    }
  }

  // Add manualy any available filter for SVG icon
  _getCustomFilter = (customFilter: ICustomFilter) => {
    if (!customFilter || !customFilter.active) {
      return null
    }

    const iconToRender = this._getIcon()

    return customFilter.filter || iconToRender.filter
  }

  // if filter not setted then should prevent it from render
  _checkDisableFilter = () => {
    const {
      filter: { ID = null, active = true }
    } = this._getIconProps()
    const isFilterDisabled = ID === null || active === false

    return isFilterDisabled
  }

  _getFilter = () => {
    const { filter } = this._getIconProps()
    const isFilterDisabled = this._checkDisableFilter()

    const generatedFilterName = !isFilterDisabled ? `url(#${filter.ID || DEFAULT_FILTER.ID})` : ''

    return generatedFilterName
  }

  // Can be used for injecting any SVG filter avilable.
  // For now it just can deal with shadows.
  _createFilter = (filter: IFilter) => {
    if (this._checkDisableFilter()) {
      return null
    }

    return (
      <filter id={filter.ID || DEFAULT_FILTER.ID}>
        {this._getShadow(filter.shadow)}
        {this._getCustomFilter(filter.customFilter)}
      </filter>
    )
  }

  _renderShapeWithDeepSVGProps = (shapeToRender, deepSVGProps) => {
    const writableIcon = {
      ...shapeToRender,
      props: {
        ...shapeToRender.props,
        ...deepSVGProps.parentProps
      }
    }

    if (writableIcon.props.children && Array.isArray(writableIcon.props.children)) {
      const newShapeWithProps = writableIcon.props.children.map((child, index) => {
        const propsToWrite = deepSVGProps.childrenProps[index] || deepSVGProps.childrenProps[child.props.className]

        const writableIconChild = {
          ...child,
          props: {
            ...child.props,
            ...propsToWrite
          }
        }

        return writableIconChild
      })

      return newShapeWithProps
    }

    return writableIcon
  }

  // Accepts prepared <path /> elements figure shape.
  _renderShape = () => {
    const { type = '' } = this.state
    const { deepProps } = this._getIconProps()
    const filter = this._getFilter()
    const iconToRender = this._getIcon()

    const { shape } = iconToRender
    const shapeToRender = shape[type] || shape.default || shape

    // legacy one approach. You should avoid this for sure!!
    if (deepProps && deepProps.active) {
      return this._renderShapeWithDeepSVGProps(shapeToRender, deepProps)
    }

    // temporary safari fix. Need to be solved by webKit's devs.
    if (isSafari() && filter) {
      const superShapeToRender = {
        ...shapeToRender,
        props: {
          ...shapeToRender.props,
          filter
        }
      }

      const clonedShape = [superShapeToRender, shapeToRender]

      return clonedShape
    }

    return shapeToRender
  }

  // overriding original props in case of onEvent provided
  _propsUpdater = ({ event, status }: IHover) => {
    // eslint-disable-next-line react/destructuring-assignment
    const { active = false, ...eventProps } = this.state[event] || {}

    const isHoverActive = active && status !== false
    const currentParams = isHoverActive ? eventProps : this.props

    this.setState(prevState => ({
      ...merge({}, prevState, currentParams),
      updateType: 'state'
    }))
  }

  _onClick = () => {
    // console.log('CLICK', this.state)
    this._propsUpdater({ event: 'onClick', status: true })
  }

  _onEnter = () => {
    // console.log('HOVER')
    this._propsUpdater({ event: 'onHover', status: true })
  }

  _onLeave = () => {
    // console.log('LEAVE')
    this._propsUpdater({ event: 'onHover', status: false })
  }

  render() {
    const { customClass = '', iconName = '', customStyles, isIconMounted } = this.state
    const { fill, dimensions } = this._getIconProps()
    const iconToRender = this._getIcon()

    // installing global defs configs into a regular DOM markup.
    this._renderGlobalSVGDefs()

    if (!isIconMounted) {
      return null
    }

    if (!iconName || !iconToRender && isIconMounted) {
      console.error(`SVG Icon "${iconName}"`, ' - IS NOT FOUND! PLEASE, CHECK THROWED ICON NAME!')

      return null
    }

    return (
      <svg
        xmlns={XMLNS}
        ref={this._ref}
        className={`${styles.default} ${customClass}`}
        style={customStyles}
        filter={(!isSafari() && this._getFilter()) || ''}
        {...fill}
        {...dimensions}
      >
        {this._getDefs()}
        {this._renderShape()}
      </svg>
    )
  }
}

export default SVGIconGenerator
