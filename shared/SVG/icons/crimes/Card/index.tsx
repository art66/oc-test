import React from 'react'
import defs from './defs'

const Card = {
  defs,
  shape: {
    default: (
      <path
        d='M0,6.333v-2A1.333,1.333,0,0,1,1.333,3H14.667A1.333,1.333,0,0,1,16,4.333v2Zm16,2v5.333A1.334,1.334,0,0,1,14.667,15H1.333A1.333,1.333,0,0,1,0,13.667V8.333Zm-10,4H2V13H6ZM8,11H2v.667H8Zm6,0H12v.667h2Z'
        transform='translate(3)'
        fill='#fff'
      />
    )
  }
}

export default Card
