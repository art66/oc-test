// @ts-nocheck
import React from 'react'

const defs = {
  default: (
    <defs>
      <filter id='a' x='0' y='0' width='22' height='18' filterUnits='userSpaceOnUse'>
        <feOffset input='SourceAlpha' />
        <feGaussianBlur stdDeviation='1' result='b' />
        <feFlood floodOpacity='0.451' />
        <feComposite operator='in' in2='b' />
        <feComposite in='SourceGraphic' />
      </filter>
    </defs>
  )
}

export default defs
