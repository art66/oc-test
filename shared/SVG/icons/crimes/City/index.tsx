import React from 'react'

const CityCenter = {
  shape: {
    default: (
      <g>
        <path className='figure' d='M10.5,33.5V24.29a5.5,5.5,0,1,1,3,0V33.5Z' />
        <path
          className='contour'
          d='M12,14a5,5,0,0,1,1,9.9V33H11V23.9A5,5,0,0,1,12,14m0-1a6,6,0,0,0-2,11.66V34h4V24.66A6,6,0,0,0,12,13Z'
        />
        <path
          className='figure'
          d='M29.5,33.5V7.5h1v-1h1V.5h3v6h1v1h1v26Zm5-6v-1h-3v1Zm0-3v-1h-3v1Zm0-3v-1h-3v1Zm0-3v-1h-3v1Zm0-3v-1h-3v1Zm0-3v-1h-3v1Z'
        />
        <path
          className='contour'
          d='M34,1V7h1V8h1V33H30V8h1V7h1V1h2m-3,9h4V9H31v1m0,3h4V11H31v2m0,3h4V14H31v2m0,3h4V17H31v2m0,3h4V20H31v2m0,3h4V23H31v2m0,3h4V26H31v2M35,0H31V6H30V7H29V34h8V7H36V6H35V0Z'
        />
        <path className='figure' d='M.5,33.5V15.72l6-3.6V33.5Zm4-8v-1h-2v1Zm0-3v-1h-2v1Zm0-3v-2h-2v2Z' />
        <path
          className='contour'
          d='M6,13V33H1V16l5-3M2,20H5V17H2v3m0,3H5V21H2v2m0,3H5V24H2v2m0,2H5V27H2v1M7,11.23l-1.51.91-5,3L0,15.43V34H7V11.23ZM3,18H4v1H3V18Z'
        />
        <polygon
          className='figure'
          points='27.5 33.5 27.5 26.5 26.5 26.5 26.5 33.5 24.5 33.5 24.5 26.5 23.5 26.5 23.5 33.5 21.5 33.5 21.5 2.5 29.5 2.5 29.5 33.5 27.5 33.5'
        />
        <path
          className='contour'
          d='M29,3V33H28V26H26v7H25V26H23v7H22V3h7M26,6h2V5H26V6M23,6h2V5H23V6m3,2h2V7H26V8M23,8h2V7H23V8m3,2h2V9H26v1m-3,0h2V9H23v1m3,2h2V11H26v1m-3,0h2V11H23v1m3,2h2V13H26v1m-3,0h2V13H23v1m3,2h2V15H26v1m-3,0h2V15H23v1m3,2h2V17H26v1m-3,0h2V17H23v1m3,2h2V19H26v1m-3,0h2V19H23v1m3,2h2V21H26v1m-3,0h2V21H23v1M30,2H21V34h3v0h3v0h3V2Z'
        />
        <path className='figure' d='M15.5,33.5V27.25a4.5,4.5,0,1,1,3,0V33.5Z' />
        <path
          className='contour'
          d='M17,19a4,4,0,0,1,1,7.88V33H16V26.88A4,4,0,0,1,17,19m0-1a5,5,0,0,0-2,9.59V34h4V27.59A5,5,0,0,0,17,18Z'
        />
      </g>
    ),
    eastSide: (
      <g>
        <rect className='figure' x='10.5' y='0.5' width='10' height='24' />
        <path
          className='contour'
          d='M20,1V24H11V1h9M12,4h7V3H12V4m0,2h7V5H12V6m0,2h7V7H12V8m0,2h7V9H12v1m0,2h7V11H12v1m0,2h7V13H12v1m0,2h7V15H12v1m0,2h7V17H12v1M21,0H10V25H21V0Z'
        />
        <path
          className='figure'
          d='M32.5,24.5v-3h-3v3h-2v-3h-3v3h-2v0h-2V8.5h16v16Zm0-8v-3h-3v3Zm-5,0v-3h-3v3Zm-5-5v0Z'
        />
        <path
          className='contour'
          d='M36,9v7H35V11H22v5H21V9H36m-2,3v4H33V13H29v3H28V13H24v3H23V12H34m2,5v7H35V19H22v5H21V17H36m-2,3v4H33V21H29v3H28V21H24v3H23V20H34M37,8H20V25h5V22h2v3h3V22h2v3h5V8Zm-7,6h2v2H30V14Zm-5,0h2v2H25V14Z'
        />
        <path className='figure' d='M.5,24.5V6.5h1v-1h8v1h1v18Zm8-6v-2h-6v2Zm0-4v-2h-6v2Zm0-4v-2h-6v2Z' />
        <path
          className='contour'
          d='M9,6V7h1V24H1V7H2V6H9M2,11H9V8H2v3m0,4H9V12H2v3m0,4H9V16H2v3M10,5H1V6H0V25H11V6H10V5ZM3,9H8v1H3V9Zm0,4H8v1H3V13Zm0,4H8v1H3V17Z'
        />
      </g>
    ),
    financial: (
      <g>
        <polygon
          className='figure'
          points='15.5 33.94 15.5 28.94 12.5 28.94 12.5 33.94 9.5 33.94 9.5 0.72 18.5 4.1 18.5 33.94 15.5 33.94'
        />
        <path
          className='contour'
          d='M10,1.44l8,3v29H16v-5H12v5H10v-32m5,7h1v-2H15v2m-3,0h1v-2H12v2m3,3h1v-2H15v2m-3,0h1v-2H12v2m3,3h1v-2H15v2m-3,0h1v-2H12v2m3,3h1v-2H15v2m-3,0h1v-2H12v2m3,3h1v-2H15v2m-3,0h1v-2H12v2m3,3h1v-2H15v2m-3,0h1v-2H12v2m3,3h1v-2H15v2m-3,0h1v-2H12v2M9,0V34.44h4v-5h2v5h4V3.75l-.65-.24-8-3L9,0Z'
        />
        <polygon className='figure' points='0.5 33.94 0.5 7.1 9.5 3.72 9.5 33.94 0.5 33.94' />
        <path
          className='contour'
          d='M9,4.44v29H1v-26l8-3m-3,7H7v-2H6v2m-3,0H4v-2H3v2m3,3H7v-2H6v2m-3,0H4v-2H3v2m3,3H7v-2H6v2m-3,0H4v-2H3v2m3,3H7v-2H6v2m-3,0H4v-2H3v2m3,3H7v-2H6v2m-3,0H4v-2H3v2m3,3H7v-2H6v2m-3,0H4v-2H3v2m3,3H7v-2H6v2m-3,0H4v-2H3v2M10,3l-1.35.51-8,3L0,6.75V34.44H10V3Z'
        />
        <path
          className='figure'
          d='M18.5,33.94v-13h2v-1h1v-6h1v-1h3v-1h1v-1h2v1h1v1h3v1h1v6h1v1h2v13Zm13-14v-1h-3v1Zm-5,0v-1h-3v1Zm5-3v-1h-3v1Zm-5,0v-1h-3v1Z'
        />
        <path
          className='contour'
          d='M28,11.44v1h1v1h3v1h1v6h1v1h2v12H19v-12h2v-1h1v-6h1v-1h3v-1h1v-1h1m0,6h4v-2H28v2m-5,0h4v-2H23v2m5,3h4v-2H28v2m-5,0h4v-2H23v2m7,4h3v-1H30v1m-4,0h3v-1H26v1m-4,0h3v-1H22v1m8,3h3v-1H30v1m-4,0h3v-1H26v1m-4,0h3v-1H22v1m8,3h3v-1H30v1m-4,0h3v-1H26v1m-4,0h3v-1H22v1m7-20H26v1H25v1H22v1H21v6H20v1H18v14H37v-14H35v-1H34v-6H33v-1H30v-1H29v-1Z'
        />
      </g>
    ),
    northSide: (
      <g>
        <path className='figure' d='M3.5,22.5V16.25a4.5,4.5,0,1,1,3,0V22.5Z' />
        <path
          className='contour'
          d='M5,8a4,4,0,0,1,1,7.88V22H4V15.88A4,4,0,0,1,5,8M5,7a5,5,0,0,0-2,9.59V23H7V16.59A5,5,0,0,0,5,7Z'
        />
        <path className='figure' d='M24.5,22.5V13.29a5.5,5.5,0,1,1,3,0V22.5Z' />
        <path
          className='contour'
          d='M26,3a5,5,0,0,1,1,9.9V22H25V12.9A5,5,0,0,1,26,3m0-1a6,6,0,0,0-2,11.66V23h4V13.66A6,6,0,0,0,26,2Z'
        />
        <path className='figure' d='M30.5,22.5V16.25a4.5,4.5,0,1,1,3,0V22.5Z' />
        <path
          className='contour'
          d='M32,8a4,4,0,0,1,1,7.88V22H31V15.88A4,4,0,0,1,32,8m0-1a5,5,0,0,0-2,9.59V23h4V16.59A5,5,0,0,0,32,7Z'
        />
        <path className='figure' d='M15.5,22.5V4.5h8v18Zm6-5v-3h-4v3Z' />
        <path
          className='contour'
          d='M23,5V22H16V5h7m-2,8h1V6H21v7m-2,0h1V6H19v7m-2,0h1V6H17v7m0,5h5V14H17v4M24,4H15V23h9V4ZM18,15h3v2H18V15Z'
        />
        <rect className='figure' x='7.5' y='0.5' width='8' height='22' />
        <path
          className='contour'
          d='M15,1V22H8V1h7M10,4h4V3H10V4m0,2h4V5H10V6m0,2h4V7H10V8m0,2h4V9H10v1m0,2h4V11H10v1m0,2h4V13H10v1m0,2h4V15H10v1M16,0H7V23h9V0Z'
        />
      </g>
    ),
    redLights: (
      <g>
        <rect className='figure' x='0.5' y='5.5' width='8' height='10' />
        <path className='contour' d='M8,6v9H1V6H8M5,9H7V8H5V9M2,9H4V8H2V9m3,3H7V11H5v1M2,12H4V11H2v1M9,5H0V16H9V5Z' />
        <rect className='figure' x='28.5' y='7.5' width='8' height='8' />
        <path className='contour' d='M36,8v7H29V8h7m-3,4h2V11H33v1m-3,0h2V11H30v1m7-5H28v9h9V7Z' />
        <rect className='figure' x='11.5' y='0.5' width='9' height='15' />
        <path className='contour' d='M20,1V15H12V1h8M13,6h6V5H13V6m0,3h6V8H13V9m8-9H11V16H21V0Z' />
        <rect className='figure' x='20.5' y='2.5' width='8' height='13' />
        <path
          className='contour'
          d='M28,3V15H21V3h7M26,6h1V5H26V6M24,6h1V5H24V6M22,6h1V5H22V6m4,2h1V7H26V8M24,8h1V7H24V8M22,8h1V7H22V8m4,2h1V9H26v1m-2,0h1V9H24v1m-2,0h1V9H22v1m7-8H20V16h9V2Z'
        />
        <path className='figure' d='M8.5,15.5V15a4.27,4.27,0,0,1,4-4.5,4.27,4.27,0,0,1,4,4.5v.5Z' />
        <path
          className='contour'
          d='M12.5,11A3.78,3.78,0,0,1,16,15H9a3.78,3.78,0,0,1,3.5-4m0-1A4.77,4.77,0,0,0,8,15v1h9V15a4.77,4.77,0,0,0-4.5-5Z'
        />
      </g>
    ),
    residential: (
      <g>
        <path className='figure' d='M12.5,20.5V11.29a5.5,5.5,0,1,1,3,0V20.5Z' />
        <path
          className='contour'
          d='M14,1a5,5,0,0,1,1,9.9V20H13V10.9A5,5,0,0,1,14,1m0-1a6,6,0,0,0-2,11.66V21h4V11.66A6,6,0,0,0,14,0Z'
        />
        <path className='figure' d='M30.5,22.5V16.25a4.5,4.5,0,1,1,3,0V22.5Z' />
        <path
          className='contour'
          d='M32,8a4,4,0,0,1,1,7.88V22H31V15.88A4,4,0,0,1,32,8m0-1a5,5,0,0,0-2,9.59V23h4V16.59A5,5,0,0,0,32,7Z'
        />
        <polygon
          className='figure'
          points='25.5 24.5 25.5 18.5 24.36 18.5 27 7.94 29.64 18.5 28.5 18.5 28.5 24.5 25.5 24.5'
        />
        <path
          className='contour'
          d='M27,10l2,8H28v6H26V18H25l2-8m0-4.12L26,9.76l-2,8L23.72,19H25v6h4V19h1.28L30,17.76l-2-8L27,5.88Z'
        />
        <path className='figure' d='M27.5,24.5V24a4.27,4.27,0,0,1,4-4.5,4.27,4.27,0,0,1,4,4.5v.5Z' />
        <path
          className='contour'
          d='M31.5,20A3.78,3.78,0,0,1,35,24H28a3.78,3.78,0,0,1,3.5-4m0-1A4.77,4.77,0,0,0,27,24v1h9V24a4.77,4.77,0,0,0-4.5-5Z'
        />
        <path className='figure' d='M4.5,24.5V15.29a5.5,5.5,0,1,1,3,0V24.5Z' />
        <path
          className='contour'
          d='M6,5a5,5,0,0,1,1,9.9V24H5V14.9A5,5,0,0,1,6,5M6,4A6,6,0,0,0,4,15.66V25H8V15.66A6,6,0,0,0,6,4Z'
        />
        <polygon className='figure' points='16.5 24.5 16.5 9.77 20.53 6.34 24.5 9.77 24.5 24.5 16.5 24.5' />
        <path
          className='contour'
          d='M20.53,7,24,10V24H17V10l3.53-3M21,12h2V11H21v1m-3,0h2V11H18v1m3,3h2V14H21v1m-3,0h2V14H18v1m3,3h2V17H21v1m-3,0h2V17H18v1m3,3h2V20H21v1m-3,0h2V20H18v1M20.54,5.68l-.66.56-3.53,3-.35.3V25h9V9.54l-.35-.3-3.46-3-.65-.56Z'
        />
        <polygon className='figure' points='8.5 24.5 8.5 11.77 12.53 8.34 16.5 11.77 16.5 24.5 8.5 24.5' />
        <path
          className='contour'
          d='M12.53,9,16,12V24H9V12l3.53-3M14,14h1V13H14v1m-2,0h1V13H12v1m-2,0h1V13H10v1m4,2h1V15H14v1m-2,0h1V15H12v1m-2,0h1V15H10v1m4,2h1V17H14v1m-2,0h1V17H12v1m-2,0h1V17H10v1m4,2h1V19H14v1m-2,0h1V19H12v1m-2,0h1V19H10v1M12.54,7.68l-.66.56-3.53,3-.35.3V25h9V11.54l-.35-.3-3.46-3-.65-.56Z'
        />
      </g>
    ),
    westSide: (
      <g>
        <path className='figure' d='M20.5,25.5V19.25a4.5,4.5,0,1,1,3,0V25.5Z' />
        <path
          className='contour'
          d='M22,11a4,4,0,0,1,1,7.88V25H21V18.88A4,4,0,0,1,22,11m0-1a5,5,0,0,0-2,9.59V26h4V19.59A5,5,0,0,0,22,10Z'
        />
        <path
          className='figure'
          d='M8.5,25.5v-9a1,1,0,0,0-2,0v9h-2v-9a1,1,0,0,0-2,0v9H.5V9.75L4.83,6.5H6.17L10.5,9.75V25.5Z'
        />
        <path
          className='contour'
          d='M6,7l4,3v1H1V10L5,7H6m4,5V25H9V16.5a1.5,1.5,0,0,0-3,0V25H5V16.5a1.5,1.5,0,0,0-3,0V25H1V12h9M6.33,6H4.67l-.27.2-4,3L0,9.5V26H3V16.5a.5.5,0,0,1,1,0V26H7V16.5a.5.5,0,0,1,1,0V26h3V9.5l-.4-.3-4-3L6.33,6Z'
        />
        <path className='figure' d='M10.5,25.5V.5h8v25Zm6-5v-3h-4v3Z' />
        <path
          className='contour'
          d='M18,1V25H11V1h7M16,16h1V2H16V16m-2,0h1V2H14V16m-2,0h1V2H12V16m0,5h5V17H12v4M19,0H10V26h9V0ZM13,18h3v2H13V18Z'
        />
        <path className='figure' d='M24.5,25.5V16.29a5.5,5.5,0,1,1,3,0V25.5Z' />
        <path
          className='contour'
          d='M26,6a5,5,0,0,1,1,9.9V25H25V15.9A5,5,0,0,1,26,6m0-1a6,6,0,0,0-2,11.66V26h4V16.66A6,6,0,0,0,26,5Z'
        />
        <path className='figure' d='M30.5,25.5V19.25a4.5,4.5,0,1,1,3,0V25.5Z' />
        <path
          className='contour'
          d='M32,11a4,4,0,0,1,1,7.88V25H31V18.88A4,4,0,0,1,32,11m0-1a5,5,0,0,0-2,9.59V26h4V19.59A5,5,0,0,0,32,10Z'
        />
        <path className='figure' d='M18.5,25.5V25a3.5,3.5,0,0,1,7,0v.5Z' />
        <path className='contour' d='M22,22a3,3,0,0,1,3,3H19a3,3,0,0,1,3-3m0-1a4,4,0,0,0-4,4v1h8V25a4,4,0,0,0-4-4Z' />
        <path className='figure' d='M28.5,25.5V25a4.27,4.27,0,0,1,4-4.5,4.27,4.27,0,0,1,4,4.5v.5Z' />
        <path
          className='contour'
          d='M32.5,21A3.78,3.78,0,0,1,36,25H29a3.78,3.78,0,0,1,3.5-4m0-1A4.77,4.77,0,0,0,28,25v1h9V25a4.77,4.77,0,0,0-4.5-5Z'
        />
      </g>
    )
  }
}

export default CityCenter
