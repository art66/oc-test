import React from 'react'
import STYLES from './styles'

const defs = {
  default: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: STYLES['romb'] }} />
    </defs>
  ),
  romb: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: STYLES['romb'] }} />
    </defs>
  ),
  metal: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: STYLES['metal'] }} />
    </defs>
  ),
  bronze: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: STYLES['bronze'] }} />
    </defs>
  ),
  silver: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: STYLES['silver'] }} />
    </defs>
  ),
  gold: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: STYLES['gold'] }} />
    </defs>
  ),
  platinum: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: STYLES['platinum'] }} />
      <linearGradient
        id='platinum_1'
        x1='0.5'
        y1='21.86'
        x2='0.5'
        y2='-4'
        gradientTransform='translate(13 20.86) rotate(180)'
        gradientUnits='userSpaceOnUse'
      >
        <stop offset='0' stopColor='#fff' />
        <stop offset='1' stopColor='#5598a6' />
      </linearGradient>
      <linearGradient id='platinum_2' x1='12.5' y1='24.86' x2='12.5' gradientUnits='userSpaceOnUse'>
        <stop offset='0' stopColor='#fdf' />
        <stop offset='1' stopColor='#fff' />
      </linearGradient>
      <linearGradient
        id='platinum_3'
        x1='0.5'
        y1='21.86'
        x2='0.5'
        y2='-4'
        gradientTransform='translate(13 20.86) rotate(180)'
        gradientUnits='userSpaceOnUse'
      >
        <stop offset='0' stopColor='#ffd24d' stopOpacity='0.75' />
        <stop offset='0.5' stopColor='#acb679' stopOpacity='0.38' />
        <stop offset='1' stopColor='#5598a6' stopOpacity='0' />
      </linearGradient>
    </defs>
  )
}

export default defs
