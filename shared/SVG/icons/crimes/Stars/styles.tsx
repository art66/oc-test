const romb = `.romb_cls-1 {
  fill: url(#romb);
}
.romb_cls-2 {
  fill: #999;
  opacity: 0.35;
  isolation: isolate;
}`

const metal = `.metal_cls-1 {
  fill: #f9f9f2;
}
.metal_cls-2 {
  opacity: 0.7;
  fill: url(#metal);
}
.metal_cls-2,
.metal_cls-3 {
  isolation: isolate;
}
.metal_cls-3 {
  fill: #9b9b8c;
  opacity: 0.4;
}`

const bronze = `.bronze_cls-1 {
  fill: #f9f9f2;
}
.bronze_cls-2 {
  opacity: 0.7;
  fill: url(#bronze);
}
.bronze_cls-2,
.bronze_cls-3 {
  isolation: isolate;
}
.bronze_cls-3 {
  fill: #8c4600;
  opacity: 0.25;
}`

const silver = `.silver_cls-1 {
  fill: url(#silver);
}
.silver_cls-2 {
  fill: #002040;
  opacity: 0.25;
  isolation: isolate;
}`

const gold = `.gold_cls-1 {
  fill: #f9f9f2;
}
.gold_cls-2 {
  opacity: 0.6;
  fill: url(#gold);
}
.gold_cls-2,
.gold_cls-3 {
  isolation: isolate;
}
.gold_cls-3 {
  fill: #b28500;
  opacity: 0.3;
}`

const platinum = `.platinum_cls-1,
.platinum_cls-4,
.platinum_cls-5 {
    isolation: isolate;
}
.platinum_cls-2 {
    fill: #f9f9f2;
}
.platinum_cls-3 {
    fill: url(#platinum_1);
}
.platinum_cls-4 {
    fill: #9797a6;
}
.platinum_cls-4,
.platinum_cls-5 {
    opacity: 0.55;
}
.platinum_cls-5 {
    fill: url(#platinum_2);
}
.platinum_cls-6 {
    mix-blend-mode: color-burn;
}
.platinum_cls-7 {
    mix-blend-mode: hard-light;
    fill: url(#platinum_3);
}`

export default {
  romb,
  metal,
  bronze,
  silver,
  gold,
  platinum
}
