import React from 'react'
import { romb } from './styles'

const defs = {
  romb: (
    <defs>
      <style dangerouslySetInnerHTML={{ __html: romb }} />
      <filter id='romb_container_shadow_bottom' height='150%'>
        <feDropShadow dx='0' dy='1' stdDeviation='.5' floodColor='#7575756e' />
      </filter>
    </defs>
  )
}

export default defs
