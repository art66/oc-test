import React from 'react'
import defs from './defs'

const StarContainer = {
  defs,
  shape: {
    default: (
      <path
        d='
          M 1 1
          L 32 1
          L 42.8 8
          L 42.8 23.8
          L 32 23.8
          L 1 23.8
          Z
        '
      />
    ),
    rotated: (
      <path
        d='
          M 0 9
          L 10.8 1
          L 42.8 1
          L 42.8 23.8
          L 10.8 23.8
          L 9.5 23.8
          L 0 23.8
          Z
        '
      />
    ),
    romb: (
      <g>
        <path
          filter='none'
          d='
            M 1 11
            L 21.4 1
            L 42.8 11
            L 42.8 26
            L 21.4 36
            L 1 26
            L 1 11
            Z
          '
        />
        <path
          className='romb_top_border'
          filter='none'
          d='
            M 0 11
            L 1 11
            L 21.4 1
            L 42.8 11
            L 43.8 11
          '
        />
        <path
          className='romb_bottom_border'
          filter='url(#romb_container_shadow_bottom)'
          d='
            M 43.8 25.5
            L 42.8 25.5
            L 21.4 36
            L 1 25.5
            L 0 25.5
          '
        />
      </g>
    )
  }
}

export default StarContainer
