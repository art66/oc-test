import React from 'react'

const Arrow = {
  shape: {
    default: <polygon points='18 43 17 43 2 22.88 17 3 18 3 3 22.88 18 43' />,
    left: <polygon points='18 43 17 43 2 22.88 17 3 18 3 3 22.88 18 43' />,
    right: <polygon points='2 43 3 43 18 22.88 3 3 2 3 17 22.88 2 43' />
  }
}

export default Arrow
