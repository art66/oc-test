import React from 'react'

const Blinded = {
  shape: (
    <g>
      <path
        className='cls-1'
        d='M22.65,6l-3.29,7,7.64.6-7,3.24,5.47,5.45L18.1,20.26,18.83,28l-4.35-6.42L10.15,28l.73-7.74L3.5,22.34,9,16.89,2,13.65l7.64-.6L6.35,6l6.22,4.5L14.48,2l1.93,8.53Z'
      />
      <path
        className='cls-1'
        d='M15.83,11.05,14.48,5.79l-1.36,5.28L7.76,7.89l2.31,5.93-5.37.42L9.61,16.5,5.77,20.32l5.2-.46.47,5.45,3.06-4.49,3.06,4.49L18,19.86l5.2.46L19.36,16.5l4.92-2.26-5.37-.42,2.31-5.93Z'
      />
      <path
        className='cls-2'
        d='M16.55,9.14,24.33,4.3l-3.54,8.59,8.21.64L21.49,17l5.87,5.84-7.92-1.14L19.15,30l-4.67-6.88L9.83,30l-.29-8.29L1.62,22.87,7.49,17,0,13.56l8.21-.65L4.67,4.32l7.76,4.82L14.48,0Zm-.72,2.91L14.48,6.79l-1.36,5.28L8.76,8.89l2.31,4.93-5.37.42,4.91,2.26L6.77,20.32,12,18.86l-.53,5.45,3.06-4.49,3.06,4.49L17,18.86l5.2,1.46L18.36,16.5l4.92-2.26-5.37-.42,2.31-4.93Z'
      />
    </g>
  )
}

export default Blinded
