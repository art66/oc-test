import React from 'react'

const Distraction = {
  shape: {
    default: (
      <g id='Group_180' data-name='Group 180'>
        <circle
          id='Ellipse_62' data-name='Ellipse 62'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_28' data-name='Ellipse 28'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    1: (
      <g id='Group_180' data-name='Group 180'>
        <circle
          id='Ellipse_62' data-name='Ellipse 62'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_28' data-name='Ellipse 28'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    2: (
      <g id='Group_181' data-name='Group 181'>
        <circle
          id='Ellipse_70' data-name='Ellipse 70'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_71' data-name='Ellipse 71'>
          <circle
            cx='19.42' cy='33.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='33.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_69' data-name='Ellipse 69'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    3: (
      <g id='Group_182' data-name='Group 182'>
        <circle
          id='Ellipse_73' data-name='Ellipse 73'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_72' data-name='Ellipse 72'>
          <circle
            cx='31.22' cy='26.68'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='31.22' cy='26.68'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_72-2' data-name='Ellipse 72-2'>
          <circle
            cx='6.11' cy='25.82'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='6.11' cy='25.82'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_72-3' data-name='Ellipse 72-3'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    4: (
      <g id='Group_183' data-name='Group 183'>
        <circle
          id='Ellipse_75' data-name='Ellipse 75'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_72-4' data-name='Ellipse 72-4'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_77' data-name='Ellipse 77'>
          <circle
            cx='19.42' cy='33.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='33.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_78' data-name='Ellipse 78'>
          <circle
            cx='33.92' cy='19'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='33.92' cy='19'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_79' data-name='Ellipse 79'>
          <circle
            cx='4.92' cy='19'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='4.92' cy='19'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>

    ),
    5: (
      <g id='Group_189' data-name='Group 189'>
        <circle
          id='Ellipse_76' data-name='Ellipse 76'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_72-5' data-name='Ellipse 72-5'>
          <circle
            cx='32.86' cy='15'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='32.86' cy='15'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_72-6' data-name='Ellipse 72-6'>
          <circle
            cx='27.03' cy='31.03'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='27.03' cy='31.03'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_72-7' data-name='Ellipse 72-7'>
          <circle
            cx='9.99' cy='30.44'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='9.99' cy='30.44'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_72-8' data-name='Ellipse 72-8'>
          <circle
            cx='5.28' cy='14.04'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='5.28' cy='14.04'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_72-9' data-name='Ellipse 72-9'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    6: (
      <g id='Group_184' data-name='Group 184'>
        <circle
          id='Ellipse_80' data-name='Ellipse 80'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_81' data-name='Ellipse 81'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-2' data-name='Ellipse 81-2'>
          <circle
            cx='31.72' cy='12.18'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='31.72' cy='12.18'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-3' data-name='Ellipse 81-3'>
          <circle
            cx='31.22' cy='26.68'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='31.22' cy='26.68'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-4' data-name='Ellipse 81-4'>
          <circle
            cx='18.42' cy='33.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='18.42' cy='33.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-5' data-name='Ellipse 81-5'>
          <circle
            cx='6.11' cy='25.82'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='6.11' cy='25.82'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-6' data-name='Ellipse 81-6'>
          <circle
            cx='6.61' cy='11.32'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='6.61' cy='11.32'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    7: (
      <g id='Group_185' data-name='Group 185'>
        <circle
          id='Ellipse_82' data-name='Ellipse 82'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_81-7' data-name='Ellipse 81-7'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-8' data-name='Ellipse 81-8'>
          <circle
            cx='30.5' cy='10.26'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='30.5' cy='10.26'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-9' data-name='Ellipse 81-9'>
          <circle
            cx='33' cy='22.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='33' cy='22.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-10' data-name='Ellipse 81-10'>
          <circle
            cx='25.05' cy='32.15'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='25.05' cy='32.15'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-11' data-name='Ellipse 81-11'>
          <circle
            cx='12.56' cy='32.04'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='12.56' cy='32.04'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-12' data-name='Ellipse 81-12'>
          <circle
            cx='4.78' cy='22.27'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='4.78' cy='22.27'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_81-13' data-name='Ellipse 81-13'>
          <circle
            cx='7.48' cy='10.07'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='7.48' cy='10.07'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    8: (
      <g id='Group_186' data-name='Group 186'>
        <circle
          id='Ellipse_84' data-name='Ellipse 84'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_83' data-name='Ellipse 83'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_83-2' data-name='Ellipse 83-2'>
          <circle
            cx='29.52' cy='9.1'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='29.52' cy='9.1'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_83-3' data-name='Ellipse 83-3'>
          <circle
            cx='33.42' cy='19.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='33.42' cy='19.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_83-4' data-name='Ellipse 83-4'>
          <circle
            cx='28.82' cy='29.61'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='28.82' cy='29.61'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_83-5' data-name='Ellipse 83-5'>
          <circle
            cx='18.42' cy='33.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='18.42' cy='33.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_83-6' data-name='Ellipse 83-6'>
          <circle
            cx='8.31' cy='28.9'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='8.31' cy='28.9'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_83-7' data-name='Ellipse 83-7'>
          <circle
            cx='4.42' cy='18.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='4.42' cy='18.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_83-8' data-name='Ellipse 83-8'>
          <circle
            cx='9.02' cy='8.39'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='9.02' cy='8.39'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    9: (
      <g id='Group_187' data-name='Group 187'>
        <circle
          id='Ellipse_87' data-name='Ellipse 87'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_86' data-name='Ellipse 86'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-2' data-name='Ellipse 86-2'>
          <circle
            cx='28.62' cy='8.21'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='28.62' cy='8.21'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-3' data-name='Ellipse 86-3'>
          <circle
            cx='33.28' cy='16.98'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='33.28' cy='16.98'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-4' data-name='Ellipse 86-4'>
          <circle
            cx='31.22' cy='26.68'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='31.22' cy='26.68'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-5' data-name='Ellipse 86-5'>
          <circle
            cx='23.41' cy='32.8'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='23.41' cy='32.8'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-6' data-name='Ellipse 86-6'>
          <circle
            cx='13.49' cy='32.45'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='13.49' cy='32.45'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-7' data-name='Ellipse 86-7'>
          <circle
            cx='6.11' cy='25.82'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='6.11' cy='25.82'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-8' data-name='Ellipse 86-8'>
          <circle
            cx='4.73' cy='15.99'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='4.73' cy='15.99'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_86-9' data-name='Ellipse 86-9'>
          <circle
            cx='9.98' cy='7.57'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='9.98' cy='7.57'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    ),
    10: (
      <g id='Group_188' data-name='Group 188'>
        <circle
          id='Ellipse_90' data-name='Ellipse 90'
          cx='18.92' cy='19'
          r='15' fill='#f1f1f1'
          stroke='#2b8dad'
          strokeWidth='2'
        />
        <g id='Ellipse_91' data-name='Ellipse 91'>
          <circle
            cx='19.42' cy='4.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='19.42' cy='4.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-2' data-name='Ellipse 91-2'>
          <circle
            cx='27.84' cy='7.56'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='27.84' cy='7.56'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-3' data-name='Ellipse 91-3'>
          <circle
            cx='32.86' cy='15'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='32.86' cy='15'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-4' data-name='Ellipse 91-4'>
          <circle
            cx='32.55' cy='23.96'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='32.55' cy='23.96'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-5' data-name='Ellipse 91-5'>
          <circle
            cx='27.04' cy='31.03'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='27.04' cy='31.03'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-6' data-name='Ellipse 91-6'>
          <circle
            cx='18.42' cy='33.5'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='18.42' cy='33.5'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-7' data-name='Ellipse 91-7'>
          <circle
            cx='9.99' cy='30.44'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='9.99' cy='30.44'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-8' data-name='Ellipse 91-8'>
          <circle
            cx='4.97' cy='23.01'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='4.97' cy='23.01'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-9' data-name='Ellipse 91-9'>
          <circle
            cx='5.28' cy='14.04'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='5.28' cy='14.04'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
        <g id='Ellipse_91-10' data-name='Ellipse 91-10'>
          <circle
            cx='10.8' cy='6.97'
            r='2.5' fill='#2b8dad'
          />
          <circle
            cx='10.8' cy='6.97'
            r='3.25' fill='none'
            stroke='#f1f1f1' strokeWidth='1.5'
          />
        </g>
      </g>
    )
  }
}

export default Distraction
