import React from 'react'

const Smoked = {
  shape: (
    <g>
      <path
        className='cls-1'
        d='M9.67,24A4.87,4.87,0,0,1,4.8,19.39,5.45,5.45,0,0,1,5.66,8.81a6.05,6.05,0,0,1-.19-1.53A6.33,6.33,0,0,1,17.7,5a4.6,4.6,0,0,1,4.35,3.75,5.46,5.46,0,0,1-.55,10.9H17.92a3.7,3.7,0,0,1-3.47,2.45,3.47,3.47,0,0,1-.85-.1A4.9,4.9,0,0,1,9.67,24Z'
      />
      <path
        className='cls-2'
        d='M11.8,2A5.32,5.32,0,0,1,17,6.06a3.31,3.31,0,0,1,.55,0,3.57,3.57,0,0,1,3.59,3.55c0,.06,0,.12,0,.18h.39a4.46,4.46,0,1,1,0,8.92H17.12a2.67,2.67,0,0,1-2.67,2.45,2.75,2.75,0,0,1-1.28-.31A3.88,3.88,0,0,1,5.8,19.16a3.31,3.31,0,0,1,0-.55A4.45,4.45,0,0,1,6.5,9.75h.59a5.17,5.17,0,0,1-.62-2.47A5.3,5.3,0,0,1,11.8,2m0-2A7.32,7.32,0,0,0,4.47,7.28a6.79,6.79,0,0,0,0,.78,6.44,6.44,0,0,0-.64,12A5.88,5.88,0,0,0,14,23.1a3.39,3.39,0,0,0,.45,0,4.71,4.71,0,0,0,4.12-2.45H21.5A6.46,6.46,0,0,0,22.85,7.89a5.59,5.59,0,0,0-4.47-3.81A7.3,7.3,0,0,0,11.8,0Z'
      />
    </g>
  )
}

export default Smoked
