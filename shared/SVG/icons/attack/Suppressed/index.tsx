import React from 'react'

const Suppressed = {
  shape: (
    <g>
      <path className='cls-1' d='M4.5,8a3.5,3.5,0,0,1,0-7H21V8Z' />
      <path className='cls-2' d='M20,2V7H4.5a2.5,2.5,0,0,1,0-5H20m2-2H4.5a4.5,4.5,0,0,0,0,9H22V0Z' />
      <rect className='cls-1' x='20.42' y='1' width='6' height='7' />
      <path className='cls-2' d='M25.42,2V7h-4V2h4m2-2h-8V9h8V0Z' />
      <path className='cls-1' d='M10.5,16a3.5,3.5,0,0,1,0-7H27v7Z' />
      <path className='cls-2' d='M26,10v5H10.5a2.5,2.5,0,0,1,0-5H26m2-2H10.5a4.5,4.5,0,0,0,0,9H28V8Z' />
      <rect className='cls-1' x='26.42' y='9' width='6' height='7' />
      <path className='cls-2' d='M31.42,10v5h-4V10h4m2-2h-8v9h8V8Z' />
      <path className='cls-1' d='M7.5,24a3.5,3.5,0,0,1,0-7H24v7Z' />
      <path className='cls-2' d='M23,18v5H7.5a2.5,2.5,0,0,1,0-5H23m2-2H7.5a4.5,4.5,0,0,0,0,9H25V16Z' />
      <rect className='cls-1' x='23.42' y='17' width='6' height='7' />
      <path className='cls-2' d='M28.42,18v5h-4V18h4m2-2h-8v9h8V16Z' />
    </g>
  )
}

export default Suppressed
