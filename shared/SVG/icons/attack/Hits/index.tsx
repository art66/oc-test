import React from 'react'

import defs from './defs'

const Hits = {
  defs,
  shape: {
    default: (
      <div>h</div>
    ),
    hit: (
      <g>
        <g transform='matrix(1, 0, 0, 1, 0, 0)' filter='url(#atack_glow_hit)'>
          <path d='M1118,318.5a5.5,5.5,0,1,1,5.5,5.5A5.5,5.5,0,0,1,1118,318.5Z' transform='translate(-1106 -301)' fill='#fff' stroke='#c00' strokeWidth='1' />
        </g>
        <path d='M1121,318.5a2.5,2.5,0,1,1,2.5,2.5A2.5,2.5,0,0,1,1121,318.5Z' transform='translate(-1106 -301)' fill='#fff' stroke='#c00' strokeWidth='1' />
        <path d='M1123,318.5a.5.5,0,1,1,.5.5A.5.5,0,0,1,1123,318.5Z' transform='translate(-1106 -301)' fill='#901919' stroke='#c00' strokeWidth='1' />
        <rect width='5' height='1' transform='translate(9 17)' fill='#c00' />
        <rect width='5' height='1' transform='translate(21 17)' fill='#c00' />
        <rect width='1' height='5' transform='translate(17 9)' fill='#c00' />
        <rect width='1' height='5' transform='translate(17 21)' fill='#c00' />
      </g>
    ),
    hitOld: (
      <g>
        <g transform='translate(-35)'>
          <g transform='matrix(1, 0, 0, 1, 35, 0)' filter='url(#atack_glow_hit__old)'>
            <path d='M1176,381.5a2.5,2.5,0,1,1,2.5,2.5A2.5,2.5,0,0,1,1176,381.5Z' transform='translate(-1161 -364)' fill='#fff' stroke='#c00' strokeWidth='1' />
          </g>
          <path d='M1178,381.5a.5.5,0,1,1,.5.5A.5.5,0,0,1,1178,381.5Z' transform='translate(-1126 -364)' fill='#a00' stroke='#c00' strokeWidth='1' />
        </g>
      </g>
    ),
    miss: (
      <g transform='translate(0 -35)'>
        <g transform='matrix(1, 0, 0, 1, 0, 35)' filter='url(#atack_glow_miss)'>
          <path d='M566,291.5a5.5,5.5,0,1,1,5.5,5.5A5.5,5.5,0,0,1,566,291.5Z' transform='translate(-554 -274)' fill='#fff' stroke='#999' strokeWidth='1' />
        </g>
        <path d='M569,291.5a2.5,2.5,0,1,1,2.5,2.5A2.5,2.5,0,0,1,569,291.5Z' transform='translate(-554 -239)' fill='#fff' stroke='#999' strokeWidth='1' />
        <path d='M571,291.5a.5.5,0,1,1,.5.5A.5.5,0,0,1,571,291.5Z' transform='translate(-554 -239)' fill='#333' stroke='#999' strokeWidth='1' />
        <rect width='5' height='1' transform='translate(9 52)' fill='#999' />
        <rect width='1' height='5' transform='translate(17 56)' fill='#999' />
        <rect width='5' height='1' transform='translate(21 52)' fill='#999' />
        <rect width='1' height='5' transform='translate(17 44)' fill='#999' />
      </g>
    ),
    missOld: (
      <g transform='translate(-35 -35)'>
        <g transform='matrix(1, 0, 0, 1, 35, 35)' filter='url(#atack_glow_miss__old)'>
          <path d='M597,263.5a2.5,2.5,0,1,1,2.5,2.5A2.5,2.5,0,0,1,597,263.5Z' transform='translate(-582 -246)' fill='#fff' stroke='#999' strokeWidth='1' />
        </g>
        <path d='M599,263.5a.5.5,0,1,1,.5.5A.5.5,0,0,1,599,263.5Z' transform='translate(-547 -211)' fill='#333' stroke='#999' strokeWidth='1' />
      </g>
    )
  }
}

export default Hits
