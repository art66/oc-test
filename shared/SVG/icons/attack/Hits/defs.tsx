// @ts-nocheck
import React from 'react'

const defs = {
  hit: (
    <filter id='atack_glow_hit' x='-0.5' y='-0.5' width='36' height='36' filterUnits='userSpaceOnUse'>
      <feOffset input='SourceAlpha' />
      <feGaussianBlur stdDeviation='4' result='b' />
      <feFlood floodColor='red' floodOpacity='0.651' />
      <feComposite operator='in' in2='b' />
      <feComposite in='SourceGraphic' />
    </filter>
  ),
  hitOld: (
    <filter id='atack_glow_hit__old' x='2.5' y='2.5' width='30' height='30' filterUnits='userSpaceOnUse'>
      <feOffset input='SourceAlpha' />
      <feGaussianBlur stdDeviation='4' result='b' />
      <feFlood floodColor='red' floodOpacity='0.651' />
      <feComposite operator='in' in2='b' />
      <feComposite in='SourceGraphic' />
    </filter>
  ),
  miss: (
    <filter id='atack_glow_miss' x='2.5' y='2.5' width='30' height='30' filterUnits='userSpaceOnUse'>
      <feOffset input='SourceAlpha' />
      <feGaussianBlur stdDeviation='3' result='b' />
      <feFlood floodOpacity='0.451' />
      <feComposite operator='in' in2='b' />
      <feComposite in='SourceGraphic' />
    </filter>
  ),
  missOld: (
    <filter id='atack_glow_miss__old' x='5.5' y='5.5' width='24' height='24' filterUnits='userSpaceOnUse'>
      <feOffset input='SourceAlpha' />
      <feGaussianBlur stdDeviation='3' result='b' />
      <feFlood floodOpacity='0.451' />
      <feComposite operator='in' in2='b' />
      <feComposite in='SourceGraphic' />
    </filter>
  )
}

export default defs
