import React from 'react'

const Paralyzed = {
  shape: (
    <g>
      <rect className='cls-2' y='16.5' width='4' height='2' />
      <rect className='cls-2' x='31' y='16.5' width='4' height='2' />
      <rect className='cls-2' x='15.5' y='32' width='4' height='2' transform='translate(-15.5 50.5) rotate(-90)' />
      <circle className='cls-1' cx='17.67' cy='17.5' r='13' />
      <path
        className='cls-2'
        d='M17.67,5.5a12,12,0,1,1-12,12,12,12,0,0,1,12-12m0-2a14,14,0,1,0,14,14,14,14,0,0,0-14-14Z'
      />
      <path className='cls-2' d='M9.13,7.71A11.9,11.9,0,0,0,7.72,9.14L26,27.45A14.54,14.54,0,0,0,27.45,26Z' />
      <rect className='cls-2' x='15.5' y='1' width='4' height='2' transform='translate(15.5 19.5) rotate(-90)' />
    </g>
  )
}

export default Paralyzed
