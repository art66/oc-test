import React from 'react'

const Dimensiokinesis = {
  shape: (
    <g id='Layer_2' data-name='Layer 2'>
      <g id='icons'>
        <path d='M31,25H26.54A17.54,17.54,0,0,0,23,16.49l5-7.91a1,1,0,1,0-1.69-1.07l-4.76,7.55A8.12,8.12,0,0,0,17,13.33V1a1,1,0,0,0-2,0V13.42a8.46,8.46,0,0,0-4.27,2L6.27,7.55a1,1,0,0,0-1.74,1l4.76,8.37A17.28,17.28,0,0,0,6.1,25H1a1,1,0,0,0,0,2H31a1,1,0,0,0,0-2ZM8.12,25a16.57,16.57,0,0,1,2.24-6.21L13.89,25ZM15,22.91l-3.27-5.76A6.65,6.65,0,0,1,15,15.44Zm2-7.59a6.11,6.11,0,0,1,3.5,1.43L17,22.3ZM17.66,25l4.25-6.73A17.14,17.14,0,0,1,24.5,25Z' fill='#b22d00'/>
      </g>
    </g>
  )
}

export default Dimensiokinesis
