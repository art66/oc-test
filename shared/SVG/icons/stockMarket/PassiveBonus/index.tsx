import React from 'react'

const PassiveBonus = {
  shape: (
    <path d='M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0ZM10.75,17.292l-4.5-4.364L8.107,11.07l2.643,2.506,5.643-5.784L18.25,9.649Z' />
  )
}

export default PassiveBonus
