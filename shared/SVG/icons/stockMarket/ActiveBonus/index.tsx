import React from 'react'

const ActiveBonus = {
  shape: <path d='M0,12A12,12,0,1,0,12,0,12,12,0,0,0,0,12Zm18-1H14v7H10V11H6l6-6Z' />
}

export default ActiveBonus
