import React from 'react'

const PersonalStats = {
  shape: <path d='M600,208h-4v-4h4Zm6,0h-4v-8h4Zm6,0h-4V195h4Zm6,0h-4V189h4Zm1,2H595v2h24Z' />
}

export default PersonalStats
