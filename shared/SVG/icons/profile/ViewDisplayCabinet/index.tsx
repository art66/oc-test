import React from 'react'

const ViewDisplayCabinet = {
  shape: <path d='M704,188v24h24V188Zm2,6h9v16h-9Zm20,16h-9V194h9Z' />
}

export default ViewDisplayCabinet
