import React from 'react'

const CrossDisabled = {
  shape: (
    <path d='M556.393,363l12.061,14-12.061,14,1,1,14-11.94,14,11.94,1-1-12.06-14,12.06-14-1-1-14,11.94-14-11.94Z' />
  )
}

export default CrossDisabled
