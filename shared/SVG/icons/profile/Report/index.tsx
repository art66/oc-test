import React from 'react'

const Report = {
  shape: <path d='M446.1,188l-12,22h24Zm-1,8h2v7h-2Zm1,11.3a1.3,1.3,0,1,1,1.3-1.3A1.324,1.324,0,0,1,446.1,207.3Z' />
}

export default Report
