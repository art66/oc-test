import React from 'react'

const SendMessage = {
  shape: (
    <path d='M176.5,200.2,165,191h23Zm-5.2-1.7-6.3-5v11.8Zm10.4,0,6.3,6.8V193.5Zm-1.5,1.1-3.7,3-3.7-3L165,208h23Z' />
  )
}

export default SendMessage
