import React from 'react'

const JailStatus = {
  shape: (
    <path d='M329,1295v5H299v-5Zm-21.667-25a6.667,6.667,0,1,1,13.334,0v5H324v-5a10,10,0,0,0-20,0v5h3.333ZM329,1283.33v-5H299v5Zm0,8.34v-5H299v5Z' />
  )
}

export default JailStatus
