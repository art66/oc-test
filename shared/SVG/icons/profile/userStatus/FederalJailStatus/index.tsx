import React from 'react'

const FederalJailStatus = {
  shape: (
    <path d='M366,1262l-20,37h40Zm-1.667,13.45h3.334v11.78h-3.334ZM366,1294.38a2.105,2.105,0,1,1,2.083-2.11A2.1,2.1,0,0,1,366,1294.38Z' />
  )
}

export default FederalJailStatus
