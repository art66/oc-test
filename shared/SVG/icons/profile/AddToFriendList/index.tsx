import React from 'react'

const AddToFriendList = {
  shape: <path d='M494,198h4v-4h4v4h4v4h-4v4h-4v-4h-4Zm-5,2a11,11,0,1,0,11-11A10.968,10.968,0,0,0,489,200Z' />
}

export default AddToFriendList
