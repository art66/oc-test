import React from 'react'

const Bust = {
  shape: (
    <path d='M782,191.3V189H758v2.3h1.5v16.3H758v2.3h24v-2.3h-1.5V191.3Zm-3.1,16.4h-2.3v-6.3a6.685,6.685,0,0,1-1.6,2.5v3.8h-2.3V206h-1.5v1.7h-2.3V206h-1.5v1.7H765v-3.8a7.16,7.16,0,0,1-1.6-2.5v6.3h-2.3V191.4h2.3v5.3a6.686,6.686,0,0,1,1.6-2.5v-2.8h2.3v1.2a4.19,4.19,0,0,1,1.5-.4v-.8h2.3v.8a6.578,6.578,0,0,1,1.5.4v-1.2H775v2.8a6.437,6.437,0,0,1,1.5,2.5v-5.3h2.3v16.3Zm-4.6-13.1-3.5,1.5L769,192l-.8,4.6-3.3-.6,1.8,2.4L764,200l3.6.7-.4,3,2.9-2.3,4.7,2.6-2.1-4,3.4-1.3-3.3-1Zm-1.5,4.2-1.7.7,1.1,2.1-2.3-1.3-1.4,1.1.2-1.5-1.9-.4,1.4-.8-.9-1.2,1.7.3.4-2.3.8,1.9,1.8-.8-.9,1.7Z' />
  )
}

export default Bust
