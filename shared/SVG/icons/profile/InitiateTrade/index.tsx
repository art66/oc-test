import React from 'react'

const InitiateTrade = {
  shape: (
    <path d='M336.8,195.8v2.4l-5.6-5.6,5.6-5.6v2.4s11.5,0,12.2,13.4A11.429,11.429,0,0,0,336.8,195.8Zm-9.8,1.4c.8,13.4,12.2,13.4,12.2,13.4V213l5.6-5.6-5.6-5.6v2.4A11.429,11.429,0,0,1,327,197.2Z' />
  )
}

export default InitiateTrade
