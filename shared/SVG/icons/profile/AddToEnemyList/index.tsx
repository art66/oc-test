import React from 'react'

const AddToEnemyList = {
  shape: <path d='M548,198h12v4H548Zm-5,2a11,11,0,1,0,11-11A10.968,10.968,0,0,0,543,200Z' />
}

export default AddToEnemyList
