import React from 'react'

const SendMoney = {
  shape: (
    <path d='M292,204.8c0-3.7-3.2-5.2-6.4-6.5v-4.8a17.263,17.263,0,0,1,4.5,1l.8-3.4a21.85,21.85,0,0,0-5.2-.9V188h-2.1v2.2c-4.2.6-6.4,3.1-6.4,5.9,0,4,3.8,5.3,6.4,6.2v4.9a15.249,15.249,0,0,1-5.5-1.3l-1,3.4a14.016,14.016,0,0,0,6.4,1.5v2.1h2.1v-2.2C289.1,210.3,292,208.4,292,204.8Zm-8.6-11v3.6C281.7,196.4,281.6,194.5,283.4,193.8Zm2.2,13.1v-3.7C287.5,204.2,287.3,206.1,285.6,206.9Z' />
  )
}

export default SendMoney
