
# Global SVG Buttons Library

How to use:
  - Quick Start:

  for SVGButtonGenerator usage you just need to set explicitly name of icon which you want to get on the ouptut. Let's imagine you need to render `ChristmasTown` buttonName. So, throw the `iconName='ChristmasTown'` string param to it and you'll see how magic happen:

    ```<SVGButtonsGenerator iconName='ChristmasTown' />```

  ...and by this simple way you'll get the icon with default pre-configurated props getted from constant parameters: `dimensions`, `filling`, `filter` and etc.
