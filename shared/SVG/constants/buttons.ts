import { CHRISTMAS_TOWN } from '../buttons/ChristmasTown/constants'

// ---------------------------------
// DEFAULT FALLBACK BUTTONS TYPE SCHEME
// ---------------------------------
const DEFAULT = {
  // -------------------------------
  // SVG BUTTONS DEFAULT DIMENSIONS
  // -------------------------------
  DIMENSIONS: {
    WIDTH: '123',
    HEIGHT: '29'
  },
  // -------------------------------
  // SVG BUTTONS DEFAULT FILL
  // -------------------------------
  FILL: {
    ID: 'default-svg-button',
    GRADIENT_TRANSFORM: 'rotate(0)',
    STROKE: '#fff',
    STROKE_WIDTH: '1'
  },
  // ---------------------------------
  // AVAILABLE SVG DEFAULT COLOR SCHEMES
  // ---------------------------------
  COLOR_SCHEME: [
    {
      step: '0%',
      color: '#DAE3E8'
    },
    {
      step: '25%',
      color: '#EEF2F4'
    },
    {
      step: '60%',
      color: '#BDCED7'
    },
    {
      step: '80%',
      color: '#E8EEF1'
    },
    {
      step: '100%',
      color: '#BACDD6'
    }
  ]
}

// ---------------------------------
// AVAILABLE BUTTONS TYPES SCHEMES
// ---------------------------------
// TODO: needs to check and set default Torns' buttons style here.
export const BUTTON_TYPES = {
  DEFAULT,
  CHRISTMAS_TOWN
}

export default BUTTON_TYPES
