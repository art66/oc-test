// ---------------------------
// SVG ICONS BASICS DIMENSIONS
// ---------------------------
export const DEFAULT_DIMENSIONS = {
  WIDTH: '15',
  HEIGHT: '15',
  VIEWBOX: '0 0 24 24'
}

// ---------------------------
// SVG ICONS BASICS COLORS
// ---------------------------
export const FILLING = {
  DEFAULT: {
    color: '#fff',
    stroke: '#fff',
    strokeWidth: '1'
  },
  BLUE_DIMMED: {
    color: '#d4dfe5',
    stroke: 'transparent',
    strokeWidth: 0
  },
  BLUE_DIMMED_DARK: {
    color: '#74c0fc',
    stroke: 'transparent',
    strokeWidth: 0
  },
  BLUE_HOVERED: {
    color: '#7fa1b2',
    stroke: 'transparent',
    strokeWidth: 0
  },
  BLUE_HOVERED_DARK: {
    color: '#a5d8ff',
    stroke: 'transparent',
    strokeWidth: 0
  },
  ORANGE_DELETED: {
    color: '#e98867',
    stroke: 'transparent',
    strokeWidth: 0
  },
  ORANGE_DELETED_DARK: {
    color: '#ff4343',
    stroke: 'transparent',
    strokeWidth: 0
  },
  ORANGE_DIMMED: {
    color: '#e98867',
    stroke: 'transparent',
    strokeWidth: 0
  },
  ORANGE_HOVERED: {
    color: '#f77245',
    stroke: 'transparent',
    strokeWidth: 0
  },
  GREY_DIMMED: {
    color: '#ddd',
    stroke: 'transparent',
    strokeWidth: 0
  },
  GREY_COMMON: {
    color: '#a7a7a7',
    stroke: 'transparent',
    strokeWidth: 0
  },
  GREY_HOVERED: {
    color: '#888',
    stroke: 'transparent',
    strokeWidth: 0
  },
  PARTIAL_ERROR: {
    color: '#A58C6A',
    stroke: 'transparent',
    strokeWidth: 0
  },
  CRITICAL_ERROR: {
    color: '#A56F6A',
    stroke: 'transparent',
    strokeWidth: 0
  },
  CURRENT_ACTION: {
    color: '#668FA3',
    stroke: 'transparent',
    strokeWidth: 0
  },
  CHECK_DEFAULT: {
    color: '#A6C170',
    stroke: 'transparent',
    strokeWidth: 0
  },
  CHECK_UNDONE: {
    color: '#E1EACF',
    stroke: 'transparent',
    strokeWidth: 0
  },
  TOP_PAGE_LINK_DIMMED: {
    color: '#777',
    stroke: 'transparent',
    strokeWidth: 0
  },
  TOP_PAGE_LINK_HOVERED: {
    color: '#666',
    stroke: 'transparent',
    strokeWidth: 0
  }
}

// ---------------------------
// SVG ICONS BASICS SHADOW FILTER
// ---------------------------
export const DEFAULT_FILTER = {
  ID: 'defaultFilter',
  SHADOW: {
    X: 0,
    Y: 0.7,
    BLUR: 0.1,
    COLOR: '#fff'
  }
}

// ---------------------------
// SVG ICONS BASICS GRADIENT
// ---------------------------
export const DEFAULT_GRADIENT = {
  ID: 'defaultIconsGradient',
  scheme: [
    { step: '0', color: '#999' },
    { step: '1', color: '#ccc' }
  ],
  transform: 'rotate(90)',
  active: true
}
