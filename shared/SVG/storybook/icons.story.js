import React from 'react'
import { select, color, text, boolean } from '@storybook/addon-knobs'
import { createSVGGeneratorContainerNode } from '../../../.storybook/utils'

import SVGIconsGenerator from '../icons'

import { SVG_ICON_NAMES, SVG_TYPES, CONFIGURATION_GROUPS, DEEP_SVG_PROPS_OBJECT } from './data/icons'

const { commonGroup, fillGroup, dimensionsGroup, filterGroup, presetGroup, gradientGroup } = CONFIGURATION_GROUPS

const initSVGDefsContainer = () => {
  if (!document.querySelector('.svgs_defs_container')) {
    // This is a fix in case we miss a container node to inject filters/gradients units
    createSVGGeneratorContainerNode()
  }
}

export const Default = () => {
  initSVGDefsContainer()

  const config = {
    iconName: select('Icon Name:', SVG_ICON_NAMES, SVG_ICON_NAMES[0], commonGroup),
    type: text('Icon Type:', '', commonGroup),
    onHover: {
      active: boolean('Active Hover:', true, commonGroup),
      fill: { color: color('Color Hover:', 'red', commonGroup) }
    },
    preset: {
      type: select('Type:', SVG_TYPES, SVG_TYPES[0], presetGroup),
      subtype: text('Subtype:', '', presetGroup)
    },
    fill: {
      color: color('Color:', 'green', fillGroup),
      stroke: color('Stroke:', '', fillGroup),
      strokeWidth: text('Stroke width:', 0, fillGroup)
    },
    dimensions: {
      width: text('Width:', '24px', dimensionsGroup),
      height: text('Height:', '24px', dimensionsGroup),
      viewbox: text('ViewBox:', '0 0 24 24', dimensionsGroup)
    },
    filter: {
      active: boolean('Filter:', true, filterGroup),
      ID: text('Filter ID:', '', filterGroup),
      shadow: {
        active: boolean('Shadow:', true, filterGroup),
        x: text('X Shadow Coord:', undefined, filterGroup),
        y: text('Y Shadow Coord:', undefined, filterGroup),
        blur: text('Shadow Blur:', undefined, filterGroup),
        color: color('Shadow Color:', '', filterGroup)
      },
      customFilter: {
        active: boolean('Custom Filter flag:', false, filterGroup),
        filter: text('Custom Filter:', '<feBlur>1</feBlur>', filterGroup)
      }
    },
    gradient: {
      active: boolean('Gradient:', true, gradientGroup),
      ID: text('Gradient ID', '', gradientGroup),
      scheme: [
        {
          step: text('Step 1:', 0, gradientGroup),
          color: color('Gradient Color 1:', 'black', gradientGroup)
        },
        {
          step: text('Step 2:', 1, gradientGroup),
          color: color('Gradient Color 2:', 'white', gradientGroup)
        }
      ],
      transform: text('Transform', 'rotate(90)', gradientGroup),
      additionalProps: text('Additional Gradient Props:', '<feDropShadow>3<feDropShadow>', gradientGroup)
    }
  }

  return <SVGIconsGenerator {...config} />
}

Default.story = {
  name: 'default'
}

export const DeepPropsAsObject = () => {
  initSVGDefsContainer()

  const config = {
    iconName: select('Icon Name:', SVG_ICON_NAMES, SVG_ICON_NAMES[0], commonGroup),
    type: text('Icon Type:', '', commonGroup),
    preset: {
      type: select('Type:', SVG_TYPES, SVG_TYPES[0], presetGroup),
      subtype: text('Subtype:', '', presetGroup)
    }
  }

  return <SVGIconsGenerator {...config} />
}

DeepPropsAsObject.story = {
  name: 'deep props as object'
}

export const DeepPropsAsArray = () => {
  initSVGDefsContainer()

  const config = {
    iconName: select('Icon Name:', SVG_ICON_NAMES, SVG_ICON_NAMES[0], commonGroup),
    type: text('Icon Type:', '', commonGroup),
    deepProps: DEEP_SVG_PROPS_OBJECT,
    preset: {
      type: select('Type:', SVG_TYPES, SVG_TYPES[0], presetGroup),
      subtype: text('Subtype:', '', presetGroup)
    }
  }

  return <SVGIconsGenerator {...config} />
}

DeepPropsAsArray.story = {
  name: 'deep props as array'
}

export const PresetConfiguration = () => {
  initSVGDefsContainer()

  const config = {
    iconName: select('Name:', SVG_ICON_NAMES, SVG_ICON_NAMES[0], commonGroup),
    preset: {
      type: select('Type:', SVG_TYPES, SVG_TYPES[0], presetGroup),
      subtype: text('Subtype:', '', presetGroup)
    }
  }

  return <SVGIconsGenerator {...config} />
}

PresetConfiguration.story = {
  name: 'preset configuration'
}

export const WithoutShadow = () => {
  initSVGDefsContainer()

  const config = {
    iconName: select('Name:', SVG_ICON_NAMES, SVG_ICON_NAMES[0], commonGroup),
    dimensions: {
      width: text('Width:', '20px', dimensionsGroup),
      height: text('Height:', '30px', dimensionsGroup),
      viewbox: text('ViewBox:', '0 0 20 30', dimensionsGroup)
    },
    filter: {
      active: boolean('Filter:', true, filterGroup),
      ID: text('Filter ID:', '', filterGroup),
      shadow: {
        active: boolean('Shadow:', false, filterGroup),
        x: text('X Shadow Coord:', 0, filterGroup),
        y: text('Y Shadow Coord:', 0, filterGroup),
        blur: text('Shadow Blur:', 0, filterGroup),
        color: color('Shadow Color:', '', filterGroup)
      },
      customFilter: {
        active: boolean('Custom Filter flag:', false, filterGroup),
        filter: text('Custom Filter:', '<feBlur>1</feBlur>', filterGroup)
      }
    }
  }

  return <SVGIconsGenerator {...config} />
}

WithoutShadow.story = {
  name: 'without shadow'
}

export const FillByGradient = () => {
  initSVGDefsContainer()

  const config = {
    iconName: select('Name:', SVG_ICON_NAMES, SVG_ICON_NAMES[0], commonGroup),
    fill: {
      color: color('Color:', '', fillGroup),
      stroke: color('Stroke:', '', fillGroup),
      strokeWidth: text('Stroke width:', 0, fillGroup)
    },
    dimensions: {
      width: text('Width:', '20px', dimensionsGroup),
      height: text('Height:', '30px', dimensionsGroup),
      viewbox: text('ViewBox:', '0 0 20 30', dimensionsGroup)
    },
    filter: {
      active: boolean('Filter:', true, filterGroup),
      ID: text('Filter ID:', '', filterGroup),
      shadow: {
        active: boolean('Shadow:', false, filterGroup),
        x: text('X Shadow Coord:', 0, filterGroup),
        y: text('Y Shadow Coord:', 0, filterGroup),
        blur: text('Shadow Blur:', 0, filterGroup),
        color: color('Shadow Color:', '', filterGroup)
      },
      customFilter: {
        active: boolean('Custom Filter flag:', false, filterGroup),
        filter: text('Custom Filter:', '<feBlur>1</feBlur>', filterGroup)
      }
    },
    gradient: {
      active: boolean('Gradient:', true, gradientGroup),
      ID: text('Gradient ID', '', gradientGroup),
      scheme: [
        {
          step: text('Step 1:', 0, gradientGroup),
          color: color('Gradient Color 1:', 'black', gradientGroup)
        },
        {
          step: text('Step 2:', 1, gradientGroup),
          color: color('Gradient Color 2:', 'white', gradientGroup)
        }
      ],
      transform: text('Transform', 'rotate(90)', gradientGroup),
      additionalProps: text('Additional Gradient Props:', '<feDropShadow>3<feDropShadow>', gradientGroup)
    }
  }

  return <SVGIconsGenerator {...config} />
}

FillByGradient.story = {
  name: 'fill by gradient'
}

export default {
  title: 'Shared/SVGIconsGenerator'
}
