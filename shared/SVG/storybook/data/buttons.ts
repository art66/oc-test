export const SVG_BUTTON_NAME = ['CTESloped', 'CTERegular']
export const SVG_BUTTON_TYPE = ['CHRISTMAS_TOWN']
export const BUTTON_STATE = {
  ACTIVE: 'ACTIVE',
  HOVERED: 'CLICKED',
  CLICKED: 'DISABLED'
}
export const LAYOUTS = ['desktop', 'tablet', 'mobile']
export const BUTTONS_STATES_ARRAY = Object.values(BUTTON_STATE)
