import React from 'react'
import { select } from '@storybook/addon-knobs'

import SVGButtonsGenerator from '../buttons'
import { SVG_BUTTON_NAME, SVG_BUTTON_TYPE, BUTTONS_STATES_ARRAY, LAYOUTS } from './data/buttons'

export const Default = () => {
  const config = {
    buttonName: select('Name:', SVG_BUTTON_NAME, SVG_BUTTON_NAME[0]),
    buttonType: select('Type:', SVG_BUTTON_TYPE, SVG_BUTTON_TYPE[0]),
    condition: select('Condition:', BUTTONS_STATES_ARRAY, BUTTONS_STATES_ARRAY[0]),
    mediaType: select('MediaType', LAYOUTS, LAYOUTS[0])
  }

  return <SVGButtonsGenerator {...config} />
}

Default.story = {
  name: 'default'
}

export const TabletMode = () => {
  const config = {
    buttonName: select('Name:', SVG_BUTTON_NAME, SVG_BUTTON_NAME[0]),
    buttonType: select('Type:', SVG_BUTTON_TYPE, SVG_BUTTON_TYPE[0]),
    condition: select('Condition:', BUTTONS_STATES_ARRAY, BUTTONS_STATES_ARRAY[0]),
    mediaType: select('MediaType', LAYOUTS, LAYOUTS[1])
  }

  return <SVGButtonsGenerator {...config} />
}

TabletMode.story = {
  name: 'tablet mode'
}

export const MobileMode = () => {
  const config = {
    buttonName: select('Name:', SVG_BUTTON_NAME, SVG_BUTTON_NAME[0]),
    buttonType: select('Type:', SVG_BUTTON_TYPE, SVG_BUTTON_TYPE[0]),
    condition: select('Condition:', BUTTONS_STATES_ARRAY, BUTTONS_STATES_ARRAY[0]),
    mediaType: select('MediaType', LAYOUTS, LAYOUTS[2])
  }

  return <SVGButtonsGenerator {...config} />
}

MobileMode.story = {
  name: 'mobile mode'
}

export default {
  title: 'Shared/SVGButtonsGenerator'
}
