const DEFAULTS = {
  fill: {
    color: '#777',
    strokeWidth: 0,
    stroke: 'transparent'
  },
  filter: {
    active: true,
    ID: 'top_svg_icon',
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 0.1,
      color: 'rgba(255, 255, 255, 0.45)'
    }
  },
  dimensions: {
    width: 16,
    height: 15,
    viewbox: '0 1 16 13'
  }
}

export const topPageLinks = {
  DEFAULTS,
  LastRolls: {
    ...DEFAULTS,
    dimensions: {
      width: 16,
      height: 16,
      viewbox: '0 0 16 16'
    }
  },
  ViewToggleList: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '-1 1 16 15'
    }
  },
  ViewToggleThumbnails: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '-1 1 16 15'
    }
  },
  Back: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '0 0 16 13'
    }
  },
  AttackTurn: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '0 0 16.9 19'
    }
  },
  Timeout: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '0 0 10.67 17'
    }
  },
  TokenShop: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '0 0 17 17'
    }
  },
  ManageItems: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      width: 18,
      height: 16,
      viewbox: '0 0 16 17'
    }
  },
  ItemsAdd: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      width: 17,
      height: 16,
      viewbox: '0 0 16.67 17'
    }
  },
  BazaarOpen: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      width: 15,
      height: 15,
      viewbox: '0 0 18 19'
    }
  },
  BazaarClosed: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      width: 15,
      height: 15,
      viewbox: '0 0 18 19'
    }
  },
  Trades: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      width: 16,
      height: 15,
      viewbox: '0 1 15.23 19'
    }
  },
  AllMaps: {
    ...DEFAULTS,
    dimensions: {
      width: 17,
      height: 17,
      viewbox: '0 0 16 16'
    }
  },
  MapEditor: {
    ...DEFAULTS,
    dimensions: {
      width: 19,
      height: 19,
      viewbox: '0 0 19 19'
    }
  },
  MyMaps: {
    ...DEFAULTS,
    dimensions: {
      width: 17,
      height: 17,
      viewbox: '0 0 17 17'
    }
  },
  ChristmasTown: {
    ...DEFAULTS,
    dimensions: {
      width: 17,
      height: 17,
      viewbox: '0 0 17 17'
    }
  },
  Parameditor: {
    ...DEFAULTS,
    dimensions: {
      width: 17,
      height: 17,
      viewbox: '0 0 17 17'
    }
  },
  Items: {
    ...DEFAULTS,
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '-1.5 1 17 16'
    }
  },
  CTNpc: {
    ...DEFAULTS,
    dimensions: {
      width: 17,
      height: 19,
      viewbox: '0 0 17 19'
    }
  },
  Calendar: {
    ...DEFAULTS,
    dimensions: {
      width: 17,
      height: 17,
      viewbox: '0 0 17 17'
    }
  },
  CrimesHub: {
    ...DEFAULTS,
    dimensions: {
      width: 16,
      height: 17,
      viewbox: '0 -1 16 17'
    }
  },
  ItemMarket: {
    ...DEFAULTS,
    dimensions: {
      width: 15,
      height: 16,
      viewbox: '0 0 18 16'
    }
  },
  NewsTicker: {
    ...DEFAULTS,
    dimensions: {
      width: 18,
      height: 16,
      viewbox: '2 0 18 16'
    }
  },
  City: {
    ...DEFAULTS,
    dimensions: {
      width: 18,
      height: 16,
      viewbox: '-4 1 18 16'
    }
  },
  Log: {
    ...DEFAULTS,
    dimensions: {
      width: 16,
      height: 17,
      viewbox: '-2 1 17 19'
    }
  },
  ViewShow: {
    ...DEFAULTS,
    dimensions: {
      width: 16,
      height: 15,
      viewbox: '0 -1 16 14'
    }
  },
  Events: {
    ...DEFAULTS,
    dimensions: {
      width: 16,
      height: 15,
      viewbox: '0 1 16 16'
    }
  },
  Home: {
    ...DEFAULTS,
    dimensions: {
      width: 16,
      height: 15,
      viewbox: '0 2 16 14'
    }
  },
  Elimination: {
    ...DEFAULTS,
    dimensions: {
      width: 16,
      height: 16,
      viewbox: '0 0 18 18'
    }
  },
  Easter: {
    ...DEFAULTS,
    dimensions: {
      width: 13,
      height: 17,
      viewbox: '0 0 13 17'
    }
  }
}

export default topPageLinks
