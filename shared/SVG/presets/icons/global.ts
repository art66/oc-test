import { DEFAULT_FILTER, DEFAULT_GRADIENT } from '../../constants/icons'

export const COPY_ICON = {
  iconName: 'Copy',
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '0 0 24 24'
  },
  gradient: DEFAULT_GRADIENT,
  filter: DEFAULT_FILTER,
  fill: {
    color: 'url(#defaultIconsGradient)',
    strokeWidth: '0'
  },
  onHover: {
    active: true,
    gradient: {
      transform: 'rotate(90)',
      scheme: [
        { step: '0', color: '#666' },
        { step: '1', color: '#999' }
      ],
      ID: 'hoverDefaultIconsGradient'
    },
    fill: {
      color: 'url(#hoverDefaultIconsGradient)',
      strokeWidth: '0'
    }
  }
}

export const LINK_ICON = {
  iconName: 'Link',
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '0 0 24 24'
  },
  gradient: DEFAULT_GRADIENT,
  filter: DEFAULT_FILTER,
  fill: {
    color: 'url(#defaultIconsGradient)',
    strokeWidth: '0'
  },
  onHover: {
    active: true,
    gradient: {
      transform: 'rotate(90)',
      scheme: [
        { step: '0', color: '#666' },
        { step: '1', color: '#999' }
      ],
      ID: 'hoverDefaultIconsGradient'
    },
    fill: {
      color: 'url(#hoverDefaultIconsGradient)',
      strokeWidth: '0'
    }
  }
}

export default {
  COPY_ICON,
  LINK_ICON
}
