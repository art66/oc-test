export const DEFAULT_FILL = {
  color: '#3e3e3e',
  stroke: 'transparent',
  strokeWidth: '0'
}

export const HOVER_FILL = {
  ...DEFAULT_FILL,
  color: '#161616'
}

export const ACTIVE_FILL = {
  ...DEFAULT_FILL,
  color: '#00a3d8'
}

const DISABLE_FILL = {
  ...DEFAULT_FILL,
  color: '#bbb'
}

export const DEFAULT_GRADIENT = {
  ID: 'tool',
  active: true,
  transform: 'rotate(90)',
  scheme: [
    { step: '0', color: 'var(--ics-tool-btn-gradient-stop-1)' },
    { step: '1', color: 'var(--ics-tool-btn-gradient-stop-2)' }
  ]
}

export const HOVER_GRADIENT = {
  ID: 'toolHover',
  active: true,
  transform: 'rotate(90)',
  scheme: [
    { step: '0', color: 'var(--ics-tool-btn-hover-gradient-stop-1)' },
    { step: '1', color: 'var(--ics-tool-btn-hover-gradient-stop-2)' }
  ]
}

export const DEFAULT_GRADIENT_FILL = {
  ...DEFAULT_FILL,
  color: 'url(#tool)'
}

export const HOVER_GRADIENT_FILL = {
  ...DEFAULT_FILL,
  color: 'url(#toolHover)'
}

const DISABLE_FILTER = {
  ID: null,
  shadow: {
    active: false
  }
}

const DEFAULT_DIMENSIONS = {
  viewbox: '0 0 16 16',
  width: 16,
  height: 16
}

const DEFAULT_PROPERTIES = {
  fill: DEFAULT_GRADIENT_FILL,
  dimensions: DEFAULT_DIMENSIONS,
  filter: DISABLE_FILTER,
  gradient: DEFAULT_GRADIENT
}

const CROSS = {
  fill: DEFAULT_FILL,
  dimensions: {
    viewbox: '0 0 18 16',
    width: 18,
    height: 16
  },
  filter: DISABLE_FILTER,
  gradient: {
    ID: 'crossGradient',
    active: true,
    transform: 'rotate(90)',
    scheme: [
      { step: '0', color: '#ccc' },
      { step: '1', color: '#888' }
    ]
  }
}

const SCALE = {
  ...DEFAULT_PROPERTIES,
  dimensions: {
    viewbox: '0 0 21 12',
    width: 21,
    height: 12
  }
}

const CROP = {
  ...DEFAULT_PROPERTIES
}

const POINTER = {
  ...DEFAULT_PROPERTIES
}

const ROTATE = {
  ...DEFAULT_PROPERTIES
}

const PLUS = {
  ...DEFAULT_PROPERTIES,
  dimensions: {
    ...DEFAULT_PROPERTIES.dimensions,
    viewbox: '2 2 20 20'
  }
}

const MINUS = {
  ...DEFAULT_PROPERTIES,
  dimensions: {
    ...DEFAULT_PROPERTIES.dimensions,
    viewbox: '0 4 18 18'
  }
}

const DEFAULT_HOVER = {
  ...DEFAULT_PROPERTIES,
  fill: HOVER_GRADIENT_FILL,
  gradient: HOVER_GRADIENT
}

const SCALE_HOVER = {
  ...DEFAULT_HOVER,
  dimensions: {
    viewbox: '0 0 21 12',
    width: 21,
    height: 12
  }
}

const PLUS_HOVER = {
  ...PLUS,
  fill: HOVER_GRADIENT_FILL,
  gradient: HOVER_GRADIENT
}

const MINUS_HOVER = {
  ...MINUS,
  fill: HOVER_GRADIENT_FILL,
  gradient: HOVER_GRADIENT
}

const ACTIVE = {
  ...DEFAULT_PROPERTIES,
  fill: ACTIVE_FILL
}

const DISABLE = {
  ...DEFAULT_PROPERTIES,
  fill: DISABLE_FILL
}

export const ics = {
  CROSS,
  POINTER,
  ROTATE,
  PLUS,
  MINUS,
  CROP,
  SCALE,
  DEFAULT_HOVER,
  PLUS_HOVER,
  MINUS_HOVER,
  ACTIVE,
  DISABLE,
  SCALE_HOVER
}

export default ics
