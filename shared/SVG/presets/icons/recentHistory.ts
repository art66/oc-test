const DEFAULT_FILL = {
  color: '#fff',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DEFAULT_FILL_LIGHT = {
  color: '#999',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DISABLE_FILTER = {
  ID: null,
  shadow: {
    active: false
  }
}

const DEFAULT = {
  fill: DEFAULT_FILL,
  filter: DISABLE_FILTER
}

const POSITIVE_LOG = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#d8f5a2'
  }
}

const GREY = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#888'
  }
}

const GREY_LIGHT = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#bbb'
  }
}

const BLUE = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#74c0fc'
  }
}

const BLUE_LIGHT = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#069'
  }
}

const NEGATIVE_LOG = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#ffa8a8'
  }
}

const DEFAULT_LIGHT = {
  fill: DEFAULT_FILL_LIGHT,
  filter: DISABLE_FILTER
}

const POSITIVE_LOG_LIGHT = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#8eab38'
  }
}

const NEGATIVE_LOG_LIGHT = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#d55858'
  }
}

export const recentHistory = {
  DEFAULT,
  POSITIVE_LOG,
  NEGATIVE_LOG,
  GREY,
  GREY_LIGHT,
  BLUE,
  BLUE_LIGHT,
  DEFAULT_LIGHT,
  POSITIVE_LOG_LIGHT,
  NEGATIVE_LOG_LIGHT
}

export default recentHistory
