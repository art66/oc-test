const DEFAULT = {
  fill: {
    color: '#888888',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 9,
    height: 11,
    viewbox: '0 0 9 11'
  },
  filter: {
    active: true,
    ID: 'default_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 0.5,
      color: '#00000073'
    }
  }
}

const DEFAULT_FILL_JAIL = {
  fill: {
    color: '#BDBDAE',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const DEFAULT_FILL_HOSTIPAL = {
  fill: {
    color: '#DDDDDD',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const DEFAULT_FILL_TRAVELING = {
  fill: {
    color: '#8FA6BC',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const FACTION = {
  ...DEFAULT,
  dimensions: {
    width: 9,
    height: 11,
    viewbox: '976 334 9 11'
  }
}

const STAR = {
  ...DEFAULT,
  dimensions: {
    width: 12,
    height: 13,
    viewbox: '332 238 10 11'
  }
}

const NEWS = {
  ...DEFAULT,
  dimensions: {
    width: 10,
    height: 12,
    viewbox: '-1740.5 -134.5 9 11'
  }
}

const FACTION_HOSPITAL = {
  ...FACTION,
  ...DEFAULT_FILL_HOSTIPAL
}

const STAR_HOSPITAL = {
  ...STAR,
  ...DEFAULT_FILL_HOSTIPAL
}

const NEWS_HOSPITAL = {
  ...NEWS,
  ...DEFAULT_FILL_HOSTIPAL
}

const FACTION_JAIL = {
  ...FACTION,
  ...DEFAULT_FILL_JAIL
}

const STAR_JAIL = {
  ...STAR,
  ...DEFAULT_FILL_JAIL
}

const NEWS_JAIL = {
  ...NEWS,
  ...DEFAULT_FILL_JAIL
}

const FACTION_TRAVELING = {
  ...FACTION,
  ...DEFAULT_FILL_TRAVELING
}

const STAR_TRAVELING = {
  ...STAR,
  ...DEFAULT_FILL_TRAVELING
}

const NEWS_TRAVELING = {
  ...NEWS,
  ...DEFAULT_FILL_TRAVELING
}

const URGENT_FACTION = {
  ...DEFAULT,
  dimensions: {
    width: 9,
    height: 11,
    viewbox: '976 334 9 11'
  },
  fill: {
    color: '#FF9673',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const URGENT_ANNOUNCEMENT = {
  ...DEFAULT,
  dimensions: {
    width: 9,
    height: 11,
    viewbox: '976 430 9 11'
  },
  fill: {
    color: '#D3B351',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const URGENT_FACTION_HOSPITAL_JAIL = {
  ...URGENT_FACTION,
  fill: {
    color: '#FFE6DD',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  filter: {
    active: true,
    ID: 'urgent_faction_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 3,
      color: '#FF4000'
    }
  }
}

const URGENT_ANNOUNCEMENT_HOSPITAL_JAIL = {
  ...URGENT_ANNOUNCEMENT,
  fill: {
    color: '#FFF4D2',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  filter: {
    active: true,
    ID: 'urgent_announcement_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 3,
      color: '#FFBF00'
    }
  }
}

const TUTORIALS = {
  ...DEFAULT,
  fill: {
    color: '#FFFFFF',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 9,
    height: 11,
    viewbox: '173 62 9 11'
  },
  filter: {
    active: false,
    ID: null,
    shadow: {
      active: false
    }
  }
}

const GAME_CLOSED = {
  ...DEFAULT,
  fill: {
    color: '#FFFFFF',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 12,
    height: 11,
    viewbox: '0 1 12 11'
  },
  filter: {
    active: true,
    ID: 'game_closed_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 3,
      color: '#FFFFFF80'
    }
  }
}

export const newsTicker = {
  FACTION,
  FACTION_HOSPITAL,
  FACTION_JAIL,
  STAR,
  STAR_HOSPITAL,
  STAR_JAIL,
  NEWS,
  NEWS_HOSPITAL,
  NEWS_JAIL,
  TUTORIALS,
  URGENT_ANNOUNCEMENT,
  URGENT_ANNOUNCEMENT_HOSPITAL_JAIL,
  URGENT_FACTION,
  URGENT_FACTION_HOSPITAL_JAIL,
  FACTION_TRAVELING,
  NEWS_TRAVELING,
  STAR_TRAVELING,
  GAME_CLOSED
}

export default newsTicker
