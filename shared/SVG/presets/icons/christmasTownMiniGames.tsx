import React from 'react'

const DEFAULT = {
  gradient: {
    active: true,
    transform: 'rotate(90)',
    scheme: [
      { step: '0', color: 'rgba(255, 255, 255, 0.75)' },
      { step: '1', color: '#fff' }
    ],
    ID: 'christmasTownMiniGamesGradient'
  },
  fill: {
    color: 'url(#christmasTownMiniGamesGradient)',
    strokeWidth: '0'
  },
  filter: {
    active: true,
    ID: 'christmasTownMiniGamesShadow',
    // shadow: {
    //   active: true,
    //   x: 0,
    //   y: 0,
    //   blur: 0.45,
    //   color: '#fff'
    // }
    customFilter: {
      active: true,
      filter: (
        // <filter
        //   xmlns="http://www.w3.org/2000/svg"
        //   id="b"
        //   x="0"
        //   y="0"
        //   width="63"
        //   height="70"
        //   filterUnits="userSpaceOnUse"
        // >
        <>
          {/* @ts-ignore */}
          <feoffset input="SourceAlpha" />
          {/* @ts-ignore */}
          <fegaussianblur stdDeviation="5" result="c" />
          {/* @ts-ignore */}
          <feflood floodColor="#fff" floodOpacity="0.6" />
          {/* @ts-ignore */}
          <fecomposite operator="in" in2="c" />
          {/* @ts-ignore */}
          <fecomposite in="SourceGraphic" />
        </>
        // </filter>
      )
    }
  }
}

const CTBrokenOrnament = {
  ...DEFAULT,
  dimensions: {
    width: 33,
    height: 40,
    viewbox: '0 0 33 40'
  }
}

const CTBrokenOrnamentTablet = {
  ...CTBrokenOrnament,
  dimensions: {
    ...CTBrokenOrnament.dimensions,
    width: 29,
    height: 36
  }
}

const CTBrokenOrnamentMobile = {
  ...CTBrokenOrnament,
  dimensions: {
    ...CTBrokenOrnament.dimensions,
    width: 24,
    height: 29
  }
}

const CTEnergyDrink = {
  ...DEFAULT,
  dimensions: {
    width: 23,
    height: 36,
    viewbox: '0 0 23 36'
  }
}

const CTEnergyDrinkTablet = {
  ...CTEnergyDrink,
  dimensions: {
    ...CTEnergyDrink.dimensions,
    width: 21,
    height: 32
  }
}

const CTEnergyDrinkMobile = {
  ...CTEnergyDrink,
  dimensions: {
    ...CTEnergyDrink.dimensions,
    width: 15,
    height: 24
  }
}

const CTMoney = {
  ...DEFAULT,
  dimensions: {
    width: 44,
    height: 29,
    viewbox: '0 0 50 29'
  }
}

const CTMoneyTablet = {
  ...CTMoney,
  dimensions: {
    ...CTMoney.dimensions,
    width: 50,
    height: 26
  }
}

const CTMoneyMobile = {
  ...CTMoney,
  dimensions: {
    ...CTMoney.dimensions,
    width: 34,
    height: 20
  }
}

const CTAlcohol = {
  ...DEFAULT,
  dimensions: {
    width: 21,
    height: 40,
    viewbox: '0 0 21 40'
  }
}

const CTAlcoholTablet = {
  ...CTAlcohol,
  dimensions: {
    ...CTAlcohol.dimensions,
    width: 19,
    height: 36
  }
}

const CTAlcoholMobile = {
  ...CTAlcohol,
  dimensions: {
    ...CTAlcohol.dimensions,
    width: 16,
    height: 30
  }
}

const CTTangerine = {
  ...DEFAULT,
  dimensions: {
    width: 31,
    height: 41,
    viewbox: '0 0 31 41'
  }
}

const CTTangerineTablet = {
  ...CTTangerine,
  dimensions: {
    ...CTTangerine.dimensions,
    width: 28,
    height: 37
  }
}

const CTTangerineMobile = {
  ...CTTangerine,
  dimensions: {
    ...CTTangerine.dimensions,
    width: 22,
    height: 28
  }
}

const CTBucks = {
  ...DEFAULT,
  dimensions: {
    width: 27,
    height: 40,
    viewbox: '0 0 27 40'
  }
}

const CTBucksTablet = {
  ...CTBucks,
  dimensions: {
    ...CTBucks.dimensions,
    width: 24,
    height: 36
  }
}

const CTBucksMobile = {
  ...CTBucks,
  dimensions: {
    ...CTBucks.dimensions,
    width: 18,
    height: 27
  }
}

const CTTightlyWhities = {
  ...DEFAULT,
  dimensions: {
    width: 40,
    height: 28,
    viewbox: '0 0 40 28'
  }
}

const CTTightlyWhitiesTablet = {
  ...CTTightlyWhities,
  dimensions: {
    ...CTTightlyWhities.dimensions,
    width: 34,
    height: 23
  }
}

const CTTightlyWhitiesMobile = {
  ...CTTightlyWhities,
  dimensions: {
    ...CTTightlyWhities.dimensions,
    width: 29,
    height: 20
  }
}

const CTCandy = {
  ...DEFAULT,
  dimensions: {
    width: 40,
    height: 40,
    viewbox: '0 0 40 40'
  }
}

const CTCandyTablet = {
  ...CTCandy,
  dimensions: {
    ...CTCandy.dimensions,
    width: 36,
    height: 36
  }
}

const CTCandyMobile = {
  ...CTCandy,
  dimensions: {
    ...CTCandy.dimensions,
    width: 28,
    height: 28
  }
}

export default {
  CTBrokenOrnament,
  CTEnergyDrink,
  CTMoney,
  CTAlcohol,
  CTTangerine,
  CTBucks,
  CTTightlyWhities,
  CTCandy,
  CTBrokenOrnamentTablet,
  CTEnergyDrinkTablet,
  CTMoneyTablet,
  CTAlcoholTablet,
  CTTangerineTablet,
  CTBucksTablet,
  CTTightlyWhitiesTablet,
  CTCandyTablet,
  CTBrokenOrnamentMobile,
  CTEnergyDrinkMobile,
  CTMoneyMobile,
  CTAlcoholMobile,
  CTTangerineMobile,
  CTBucksMobile,
  CTTightlyWhitiesMobile,
  CTCandyMobile
}
