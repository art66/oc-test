const Search = {
  fill: {
    color: 'url(#search_bar_search_gradient)',
    strokeWidth: '0'
  },
  gradient: {
    ID: 'search_bar_search_gradient',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#999' }, { step: '1', color: '#ccc' }]
  },
  dimensions: {
    width: 17,
    height: 17,
    viewbox: '0 0 14 14'
  },
  filter: {
    active: true,
    ID: 'search_bar_search_filter',
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 0.1,
      color: 'rgba(255, 255, 255, 0.25)'
    }
  }
}

export default {
  Search
}
