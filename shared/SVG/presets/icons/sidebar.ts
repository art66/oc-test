export const DEFAULT_FILL = {
  color: '#3e3e3e',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DEFAULT_DIMENSIONS = {
  viewbox: '0 0 18 18',
  width: 18,
  height: 17
}

const DEFAULT_FILTER = {
  ID: 'svg_sidebar_mobile',
  active: true,
  shadow: {
    active: true,
    x: 0,
    y: 1,
    blur: 1,
    color: '#000000'
  }
}

// ============================
// REGULAR  PRESET
// ============================
const REGULAR_DESKTOP_GOLD = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop_gold)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_regular_desktop_gold_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1.2,
      color: '#f4b701'
    }
  },
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop_gold',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#f4b701' }, { step: '1', color: '#b08505' }]
  }
}

const REGULAR_DESKTOP = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#999999' }, { step: '1', color: '#CCCCCC' }]
  }
}

const REGULAR_DESKTOP_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop_hover)'
  },
  filter: {
    ID: 'svg_sidebar_desktop_regular_hover_filter',
    active: true,
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 0.1,
      color: '#ffffff'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#AAAAAA' }, { step: '1', color: '#555555' }]
  }
}

const REGULAR_DESKTOP_ACTIVE = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop_active)'
  },
  filter: {
    ...REGULAR_DESKTOP_HOVER.filter,
    ID: 'svg_sidebar_desktop_regular_active_filter'
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop_active',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#555555' }, { step: '1', color: '#AAAAAA' }]
  }
}

const REGULAR_DESKTOP_GREEN = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop_green)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop_green',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#799427' }, { step: '1', color: '#a3c248' }]
  }
}

const REGULAR_DESKTOP_GREEN_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop_green_hover)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop_green_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#799427' }, { step: '1', color: '#a3c248' }]
  },
  filter: {
    ID: 'svg_sidebar_desktop_regular_green_hover_filter',
    active: true,
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 1.2,
      color: '#a3d90091'
    }
  }
}

const REGULAR_DESKTOP_GREEN_ACTIVE = {
  ...REGULAR_DESKTOP_GREEN_HOVER,
  fill: {
    ...REGULAR_DESKTOP_GREEN_HOVER.fill,
    color: 'url(#sidebar_svg_gradient_regular_green_desktop_active)'
  },
  filter: {
    ...REGULAR_DESKTOP_GREEN_HOVER.filter,
    ID: 'svg_sidebar_green_regular_desktop_green_active_filter'
  },
  gradient: {
    ...REGULAR_DESKTOP_GREEN_HOVER.gradient,
    ID: 'sidebar_svg_gradient_regular_green_desktop_active'
  }
}

const REGULAR_DESKTOP_RED = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop_red)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop_red',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#EF6B3E' }, { step: '1', color: '#CD491D' }]
  }
}

const REGULAR_DESKTOP_RED_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_desktop_green_hover)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_desktop_green_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#EF6B3E' }, { step: '1', color: '#CD491D' }]
  },
  filter: {
    ID: 'svg_sidebar_desktop_regular_green_hover_filter',
    active: true,
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 1.2,
      color: '#ef6b3e70'
    }
  }
}

const REGULAR_DESKTOP_RED_ACTIVE = {
  ...REGULAR_DESKTOP_RED_HOVER,
  fill: {
    ...REGULAR_DESKTOP_RED_HOVER.fill,
    color: 'url(#sidebar_svg_gradient_regular_red_desktop_active)'
  },
  filter: {
    ...REGULAR_DESKTOP_RED_HOVER.filter,
    ID: 'svg_sidebar_green_regular_desktop_red_active_filter'
  },
  gradient: {
    ...REGULAR_DESKTOP_RED_HOVER.gradient,
    ID: 'sidebar_svg_gradient_regular_red_desktop_active'
  }
}

// ============================
// REGULAR MOBILE PRESET
// ============================
const REGULAR_MOBILE_GOLD = {
  ...REGULAR_DESKTOP_GOLD
}

const REGULAR_MOBILE_GOLD_HOVER = {
  ...REGULAR_DESKTOP_GOLD
}

const REGULAR_MOBILE_GOLD_ACTIVE = {
  ...REGULAR_DESKTOP_GOLD
}

const REGULAR_MOBILE = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_mobile)'
  },
  filter: {
    ...DEFAULT_FILTER,
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1.3,
      color: '#000000'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_mobile',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#888888' }, { step: '1', color: '#666666' }]
  }
}

const REGULAR_MOBILE_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_mobile_hover)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  filter: {
    ID: 'svg_sidebar_mobile_regular_hover_filter',
    active: true,
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1,
      color: '#FFFFFF'
    }
  },
  gradient: {
    ID: 'sidebar_svg_gradient_regular_mobile_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#DDDDDD' }, { step: '1', color: '#AAAAAA' }]
  }
}

const REGULAR_MOBILE_ACTIVE = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_mobile_active)'
  },
  dimensions: DEFAULT_DIMENSIONS,
  filter: {
    ID: 'svg_sidebar_mobile_regular_active_filter',
    active: true,
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1,
      color: '#FFFFFF80'
    }
  },
  gradient: {
    ID: 'sidebar_svg_gradient_regular_mobile_active',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#DDDDDD' }, { step: '1', color: '#AAAAAA' }]
  }
}

const REGULAR_MOBILE_RED = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_red_mobile)'
  },
  filter: {
    active: false,
    ID: null
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_red_mobile',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#EF6B3E' }, { step: '1', color: '#CD491D' }]
  }
}

const REGULAR_MOBILE_RED_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_red_mobile_hover)'
  },
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_regular_mobile_red_hover_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1.2,
      color: '#EF6B3E'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_red_mobile_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#EF6B3E' }, { step: '1', color: '#CD491D' }]
  }
}

const REGULAR_MOBILE_RED_ACTIVE = {
  ...REGULAR_MOBILE_RED_HOVER,
  fill: {
    ...REGULAR_MOBILE_RED_HOVER.fill,
    color: 'url(#sidebar_svg_gradient_regular_red_mobile_active)'
  },
  filter: {
    ...REGULAR_MOBILE_RED_HOVER.filter,
    ID: 'svg_sidebar_green_regular_mobile_red_active_filter'
  },
  gradient: {
    ...REGULAR_MOBILE_RED_HOVER.gradient,
    ID: 'sidebar_svg_gradient_regular_red_mobile_active'
  }
}

const REGULAR_MOBILE_GREEN = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_green_mobile)'
  },
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_regular_mobile_green_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1.2,
      color: '#85b20070'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_green_mobile',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#AFC372' }, { step: '1', color: '#798D3C' }]
  }
}

const REGULAR_MOBILE_GREEN_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_regular_green_mobile_hover)'
  },
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_regular_mobile_green_hover_filter',
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1.2,
      color: '#85B200'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_regular_green_mobile_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#BBDC59' }, { step: '1', color: '#85A623' }]
  }
}

const REGULAR_MOBILE_GREEN_ACTIVE = {
  ...REGULAR_MOBILE_GREEN_HOVER,
  fill: {
    ...REGULAR_MOBILE_GREEN_HOVER.fill,
    color: 'url(#sidebar_svg_gradient_regular_green_mobile_active)'
  },
  filter: {
    ...REGULAR_MOBILE_GREEN_HOVER.filter,
    ID: 'svg_sidebar_green_regular_mobile_green_active_filter'
  },
  gradient: {
    ...REGULAR_MOBILE_GREEN_HOVER.gradient,
    ID: 'sidebar_svg_gradient_regular_green_mobile_active'
  }
}

// ============================
// JAIL PRESET
// ============================
const JAIL_DESKTOP = {
  ...REGULAR_DESKTOP
}

const JAIL_DESKTOP_HOVER = {
  ...REGULAR_DESKTOP_HOVER
}

const JAIL_DESKTOP_ACTIVE = {
  ...REGULAR_DESKTOP_ACTIVE
}

const JAIL_DESKTOP_GREEN = {
  ...REGULAR_DESKTOP_GREEN
}

const JAIL_DESKTOP_GREEN_HOVER = {
  ...REGULAR_DESKTOP_GREEN_HOVER
}

const JAIL_DESKTOP_GREEN_ACTIVE = {
  ...REGULAR_DESKTOP_GREEN_ACTIVE
}

const JAIL_DESKTOP_RED = {
  ...REGULAR_DESKTOP_RED
}

const JAIL_DESKTOP_RED_HOVER = {
  ...REGULAR_DESKTOP_RED_HOVER
}

const JAIL_DESKTOP_RED_ACTIVE = {
  ...REGULAR_DESKTOP_RED_ACTIVE
}
// ============================
// JAIL MOBILE PRESET
// ============================
const JAIL_MOBILE = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_jail_mobile)'
  },
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_jail_mobile_filter',
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 1.3,
      color: '#000000'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_jail_mobile',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#BDBCAF' }, { step: '1', color: '#8A8973' }]
  }
}

const JAIL_MOBILE_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_jail_mobile_hover)'
  },
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_jail_mobile_filter_hover',
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 1.3,
      color: '#FFFFFF'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_jail_mobile_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#FFFFFF' }, { step: '1', color: '#FFFFFF' }]
  }
}

const JAIL_MOBILE_ACTIVE = {
  ...JAIL_MOBILE_HOVER,
  fill: {
    ...JAIL_MOBILE_HOVER.fill,
    color: 'url(#sidebar_svg_gradient_jail_green_mobile_active)'
  },
  filter: {
    ...JAIL_MOBILE_HOVER.filter,
    ID: 'svg_sidebar_green_jail_green_active_filter'
  },
  gradient: {
    ...JAIL_MOBILE_HOVER.gradient,
    ID: 'sidebar_svg_gradient_jail_green_mobile_active'
  }
}

const JAIL_MOBILE_GREEN = {
  ...REGULAR_MOBILE_GREEN
}

const JAIL_MOBILE_GREEN_HOVER = {
  ...REGULAR_MOBILE_GREEN_HOVER
}

const JAIL_MOBILE_GREEN_ACTIVE = {
  ...REGULAR_MOBILE_GREEN_ACTIVE
}

const JAIL_MOBILE_RED = {
  ...REGULAR_MOBILE_RED
}

const JAIL_MOBILE_RED_HOVER = {
  ...REGULAR_MOBILE_RED_HOVER
}

const JAIL_MOBILE_RED_ACTIVE = {
  ...REGULAR_MOBILE_RED_ACTIVE
}
// ============================
// HOSPITAL PRESET
// ============================
const HOSPITAL_DESKTOP = {
  ...REGULAR_DESKTOP
}

const HOSPITAL_DESKTOP_HOVER = {
  ...REGULAR_DESKTOP_HOVER
}

const HOSPITAL_DESKTOP_ACTIVE = {
  ...REGULAR_DESKTOP_ACTIVE
}

const HOSPITAL_DESKTOP_GREEN = {
  ...REGULAR_DESKTOP_GREEN
}

const HOSPITAL_DESKTOP_GREEN_HOVER = {
  ...REGULAR_DESKTOP_GREEN_HOVER
}

const HOSPITAL_DESKTOP_GREEN_ACTIVE = {
  ...REGULAR_DESKTOP_GREEN_ACTIVE
}

const HOSPITAL_DESKTOP_RED = {
  ...REGULAR_DESKTOP_RED
}

const HOSPITAL_DESKTOP_RED_HOVER = {
  ...REGULAR_DESKTOP_RED_HOVER
}

const HOSPITAL_DESKTOP_RED_ACTIVE = {
  ...REGULAR_DESKTOP_RED_ACTIVE
}
// ============================
// HOSPITAL MOBILE PRESET
// ============================
const HOSPITAL_MOBILE = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_hospital_mobile)'
  },
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_hospital_mobile_filter',
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 1.3,
      color: '#000000'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_hospital_mobile',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#DDDDDD' }, { step: '1', color: '#AAAAAA' }]
  }
}

const HOSPITAL_MOBILE_HOVER = {
  fill: {
    ...DEFAULT_FILL,
    color: 'url(#sidebar_svg_gradient_hospital_mobile_hover)'
  },
  filter: {
    ...DEFAULT_FILTER,
    ID: 'svg_sidebar_green_hospital_mobile_filter_hover',
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 1.3,
      color: '#FFFFFF'
    }
  },
  dimensions: DEFAULT_DIMENSIONS,
  gradient: {
    ID: 'sidebar_svg_gradient_hospital_mobile_hover',
    active: true,
    transform: 'rotate(90)',
    scheme: [{ step: '0', color: '#FFFFFF' }, { step: '1', color: '#FFFFFF' }]
  }
}

const HOSPITAL_MOBILE_ACTIVE = {
  ...HOSPITAL_MOBILE_HOVER,
  fill: {
    ...HOSPITAL_MOBILE_HOVER.fill,
    color: 'url(#sidebar_svg_gradient_hospital_green_mobile_active)'
  },
  filter: {
    ...HOSPITAL_MOBILE_HOVER.filter,
    ID: 'svg_sidebar_green_hospital_green_active_filter'
  },
  gradient: {
    ...HOSPITAL_MOBILE_HOVER.gradient,
    ID: 'sidebar_svg_gradient_hospital_green_mobile_active'
  }
}

const HOSPITAL_MOBILE_GREEN = {
  ...REGULAR_MOBILE_GREEN
}

const HOSPITAL_MOBILE_GREEN_HOVER = {
  ...REGULAR_MOBILE_GREEN_HOVER
}

const HOSPITAL_MOBILE_GREEN_ACTIVE = {
  ...REGULAR_MOBILE_GREEN_ACTIVE
}

const HOSPITAL_MOBILE_GOLD = {
  ...REGULAR_MOBILE_GOLD
}

const HOSPITAL_MOBILE_GOLD_HOVER = {
  ...REGULAR_MOBILE_GOLD_HOVER
}

const HOSPITAL_MOBILE_GOLD_ACTIVE = {
  ...REGULAR_MOBILE_GOLD_ACTIVE
}

const HOSPITAL_MOBILE_RED = {
  ...REGULAR_MOBILE_RED
}

const HOSPITAL_MOBILE_RED_HOVER = {
  ...REGULAR_MOBILE_RED_HOVER
}

const HOSPITAL_MOBILE_RED_ACTIVE = {
  ...REGULAR_MOBILE_RED_ACTIVE
}

// ============================

const REGULARS = {
  REGULAR_DESKTOP_GOLD,
  REGULAR_DESKTOP,
  REGULAR_DESKTOP_HOVER,
  REGULAR_DESKTOP_ACTIVE,
  REGULAR_DESKTOP_GREEN,
  REGULAR_DESKTOP_GREEN_HOVER,
  REGULAR_DESKTOP_GREEN_ACTIVE,
  REGULAR_DESKTOP_RED,
  REGULAR_DESKTOP_RED_HOVER,
  REGULAR_DESKTOP_RED_ACTIVE,
  REGULAR_MOBILE_GOLD,
  REGULAR_MOBILE_GOLD_HOVER,
  REGULAR_MOBILE_GOLD_ACTIVE,
  REGULAR_MOBILE,
  REGULAR_MOBILE_HOVER,
  REGULAR_MOBILE_ACTIVE,
  REGULAR_MOBILE_GREEN,
  REGULAR_MOBILE_GREEN_HOVER,
  REGULAR_MOBILE_GREEN_ACTIVE,
  REGULAR_MOBILE_RED,
  REGULAR_MOBILE_RED_HOVER,
  REGULAR_MOBILE_RED_ACTIVE
}

const JAILS = {
  JAIL_DESKTOP,
  JAIL_DESKTOP_HOVER,
  JAIL_DESKTOP_ACTIVE,
  JAIL_DESKTOP_GREEN,
  JAIL_DESKTOP_GREEN_HOVER,
  JAIL_DESKTOP_GREEN_ACTIVE,
  JAIL_DESKTOP_RED,
  JAIL_DESKTOP_RED_HOVER,
  JAIL_DESKTOP_RED_ACTIVE,
  JAIL_MOBILE,
  JAIL_MOBILE_HOVER,
  JAIL_MOBILE_ACTIVE,
  JAIL_MOBILE_GREEN,
  JAIL_MOBILE_GREEN_HOVER,
  JAIL_MOBILE_GREEN_ACTIVE,
  JAIL_MOBILE_RED,
  JAIL_MOBILE_RED_HOVER,
  JAIL_MOBILE_RED_ACTIVE
}

const HOSPITALS = {
  HOSPITAL_DESKTOP,
  HOSPITAL_DESKTOP_HOVER,
  HOSPITAL_DESKTOP_ACTIVE,
  HOSPITAL_DESKTOP_GREEN,
  HOSPITAL_DESKTOP_GREEN_HOVER,
  HOSPITAL_DESKTOP_GREEN_ACTIVE,
  HOSPITAL_DESKTOP_RED,
  HOSPITAL_DESKTOP_RED_HOVER,
  HOSPITAL_DESKTOP_RED_ACTIVE,
  HOSPITAL_MOBILE,
  HOSPITAL_MOBILE_HOVER,
  HOSPITAL_MOBILE_ACTIVE,
  HOSPITAL_MOBILE_GREEN,
  HOSPITAL_MOBILE_GREEN_HOVER,
  HOSPITAL_MOBILE_GREEN_ACTIVE,
  HOSPITAL_MOBILE_RED,
  HOSPITAL_MOBILE_RED_HOVER,
  HOSPITAL_MOBILE_RED_ACTIVE,
  HOSPITAL_MOBILE_GOLD,
  HOSPITAL_MOBILE_GOLD_HOVER,
  HOSPITAL_MOBILE_GOLD_ACTIVE
}

export const sidebar = {
  ...REGULARS,
  ...JAILS,
  ...HOSPITALS
}

export default sidebar
