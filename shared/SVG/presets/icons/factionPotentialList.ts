const DEFAULTS = {
  fill: {
    color: '#ccc',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 13,
    height: 16,
    viewbox: '0 0 13 16'
  },
  filter: {
    ID: null,
    active: false,
    shadow: {
      active: false
    }
  }
}

export const factionPotentialList = {
  APPLICATION_STATE_NONE: DEFAULTS,
  APPLICATION_STATE_PENDING: {
    ...DEFAULTS,
    fill: {
      ...DEFAULTS.fill,
      color: '#a1b664'
    }
  },
  APPLICATION_STATE_DECLINED: {
    ...DEFAULTS,
    fill: {
      ...DEFAULTS.fill,
      color: '#ee8966'
    }
  }
}

export default factionPotentialList
