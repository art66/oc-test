const RESET_FILL = {
  color: '#fff',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DISABLE_FILTER = {
  ID: null,
  shadow: {
    active: false
  }
}

export const RESET_DEFAULTS = {
  fill: RESET_FILL,
  filter: DISABLE_FILTER
}

const DEFAULTS_FOR_STATUS_ICONS = {
  filter: DISABLE_FILTER,
  dimensions: {
    width: 7,
    height: 7,
    viewbox: '0 0 12 12'
  }
}

export const COMPANY_STAR = {
  filter: {
    ID: 'companyStarFilter',
    active: true,
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 2,
      color: '#a3d900'
    }
  },
  fill: {
    color: '#a3d900',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 17,
    height: 17,
    viewbox: '-2 -2 9 9'
  }
}

export const COMPANY_STAR_EMPTY = {
  filter: DISABLE_FILTER,
  fill: {
    color: '#666',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 17,
    height: 17,
    viewbox: '-2 -2 9 9'
  }
}

export const ONLINE_STATUS = {
  ...DEFAULTS_FOR_STATUS_ICONS,
  fill: {
    color: 'url("#statusOnline")',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  gradient: {
    active: true,
    ID: 'statusOnline',
    transform: 'translate(4003.99 5792.17) scale(11.88)',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: '-243.94',
      y1: '-487.37',
      x2: '-243.94',
      y2: '-486.36'
    },
    scheme: [
      {
        step: '0',
        color: '#a3d900'
      },
      {
        step: '1',
        color: '#4c6600'
      }
    ]
  }
}

export const AWAY_STATUS = {
  ...DEFAULTS_FOR_STATUS_ICONS,
  fill: {
    color: 'url("#statusIdle")',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  gradient: {
    active: true,
    ID: 'statusIdle',
    transform: 'matrix(11.88, 0, 0, -11.88, 11469.59, 5991.42)',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: '-964.96',
      y1: '504.33',
      x2: '-964.96',
      y2: '503.31'
    },
    scheme: [
      {
        step: '0',
        color: '#ffbf00'
      },
      {
        step: '1',
        color: '#b25900'
      }
    ]
  }
}

export const OFFLINE_STATUS = {
  ...DEFAULTS_FOR_STATUS_ICONS,
  fill: {
    color: 'url("#statusOffline")',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  gradient: {
    active: true,
    ID: 'statusOffline',
    transform: 'matrix(11.88, 0, 0, -11.88, 10388.51, 5992.42)',
    gradientUnits: 'userSpaceOnUse',
    coords: {
      x1: '-873.96',
      y1: '504.42',
      x2: '-873.96',
      y2: '503.31'
    },
    scheme: [
      {
        step: '0',
        color: '#ccc'
      },
      {
        step: '1',
        color: '#666'
      }
    ]
  }
}

export const header = {
  RESET_DEFAULTS,
  ONLINE_STATUS,
  AWAY_STATUS,
  OFFLINE_STATUS,
  COMPANY_STAR,
  COMPANY_STAR_EMPTY
}

export default header
