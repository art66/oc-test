export const DEFAULT_FILL = {
  color: 'rgba(63, 49, 36, 0.6)',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DEFAULT_DIMENSIONS = {
  viewbox: '0 0 57.47 25.45',
  width: 57.47,
  height: 25.45
}

const DEFAULT_PROPERTIES = {
  fill: DEFAULT_FILL,
  dimensions: DEFAULT_DIMENSIONS,
  filter: {
    ID: null,
    active: false,
    shadow: {
      active: false
    }
  }
}

export const chroniclesArchive = {
  DEFAULT_PROPERTIES
}

export default chroniclesArchive
