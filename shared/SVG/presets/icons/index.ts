import { crimes } from './crimes'
import { calendar } from './calendar'
import { topPageLinks } from './topPageLinks'
import { ics } from './ics'
import { sidebar } from './sidebar'
import { profile } from './profile'
import searchBar from './searchBar'
import bazaar from './bazaar'
import christmasTown from './christmastown'
import christmasTownMiniGames from './christmasTownMiniGames'
import { chroniclesArchive } from './chroniclesArchive'
import Loadouts from './loadouts'
import factionPotentialList from './factionPotentialList'
import faction from './faction'
import recentHistory from './recentHistory'
import newsTicker from './newsticker'
import header from './header'
import activityLog from './activityLog'
import global from './global'
import stockMarket from './stockMarket'
import personalStats from './personalStats'

const PRESETS = {
  crimes,
  calendar,
  topPageLinks,
  ics,
  sidebar,
  searchBar,
  profile,
  bazaar,
  christmasTown,
  christmasTownMiniGames,
  chroniclesArchive,
  factionPotentialList,
  faction,
  recentHistory,
  Loadouts,
  newsTicker,
  header,
  activityLog,
  global,
  stockMarket,
  personalStats
}

export default PRESETS
