export const DEFAULT_FILL = {
  color: '#666659',
  stroke: 'transparent',
  strokeWidth: '0'
}

export const DISABLE_FILL = {
  color: 'url(#calendarDM)',
  stroke: 'transparent',
  strokeWidth: '0'
}

export const DEFAULT_GRADIENT_FILL = {
  color: 'url(#calendar)',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DEFAULT_GRADIENT = {
  ID: 'calendar',
  active: true,
  transform: 'rotate(90)',
  scheme: [
    { step: '0', color: 'var(--calendar-default-arrow-gradient-stop-1)' },
    { step: '1', color: 'var(--calendar-default-arrow-gradient-stop-2)' }
  ]
}

export const DM_DISABLED_GRADIENT = {
  ID: 'calendarDM',
  active: true,
  transform: 'rotate(90)',
  scheme: [
    { step: '0', color: 'var(--calendar-disabled-arrow-gradient-stop-1)' },
    { step: '1', color: 'var(--calendar-disabled-arrow-gradient-stop-2)' }
  ]
}

const DISABLE_FILTER = {
  ID: null,
  shadow: {
    active: false
  }
}

const CHRISTMAS_TOWN = {
  fill: DEFAULT_FILL,
  dimensions: {
    width: 20,
    height: 20,
    viewbox: '-1 1 18 18'
  },
  filter: DISABLE_FILTER
}

const DOG_TAGS = {
  fill: DEFAULT_FILL,
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '0 0 15.19 15.19'
  },
  filter: DISABLE_FILTER
}

const ELIMINATION = {
  fill: DEFAULT_FILL,
  dimensions: {
    width: 18,
    height: 18,
    viewbox: '0 0 18 18'
  },
  filter: DISABLE_FILTER
}

const EASTER = {
  fill: DEFAULT_FILL,
  dimensions: {
    width: 13,
    height: 17,
    viewbox: '0 0 13 17'
  },
  filter: DISABLE_FILTER
}

const MR_MISS_TORN = {
  fill: DEFAULT_FILL,
  dimensions: {
    width: 17,
    height: 17,
    viewbox: '0 0 17 17'
  },
  filter: DISABLE_FILTER
}

const BACK = {
  fill: DEFAULT_GRADIENT_FILL,
  dimensions: {
    width: 22,
    height: 19,
    viewbox: '0 0 18 15'
  },
  filter: DISABLE_FILTER,
  gradient: DEFAULT_GRADIENT
}

const RIGHT_ARROW_THIN = {
  fill: DEFAULT_GRADIENT_FILL,
  dimensions: {
    width: 10,
    height: 17,
    viewbox: '0 0 10 17'
  },
  filter: DISABLE_FILTER,
  gradient: DEFAULT_GRADIENT
}

const LEFT_ARROW_THIN = {
  fill: DEFAULT_GRADIENT_FILL,
  dimensions: {
    width: 10,
    height: 17,
    viewbox: '0 0 10 17'
  },
  filter: DISABLE_FILTER,
  gradient: DEFAULT_GRADIENT
}

export const calendar = {
  CHRISTMAS_TOWN,
  DOG_TAGS,
  ELIMINATION,
  EASTER,
  MR_MISS_TORN,
  BACK,
  RIGHT_ARROW_THIN,
  LEFT_ARROW_THIN
}

export default calendar
