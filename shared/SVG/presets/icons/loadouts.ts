const getDefaultFilter = iconId => ({
  active: true,
  ID: `loadouts_${iconId}_shadow`,
  shadow: {
    active: true,
    x: 0,
    y: 1,
    blur: 0.1,
    color: '#fff'
  }
})

const getHeaderFilter = iconId => ({
  active: true,
  ID: `loadouts_${iconId}_shadow`,
  shadow: {
    active: true,
    x: 0,
    y: 0,
    blur: 0.5,
    color: '#999'
  }
})

const getDefaults = iconName => {
  const iconId = iconName.toLowerCase()

  return {
    iconName,
    gradient: {
      active: true,
      transform: 'rotate(90)',
      scheme: [
        { step: '0', color: '#888' },
        { step: '1', color: '#ccc' }
      ],
      ID: `loadouts_${iconId}_gradient`
    },
    fill: {
      color: `url(#loadouts_${iconId}_gradient)`,
      strokeWidth: '0'
    },
    filter: getDefaultFilter('iconId'),
    onHover: {
      active: true,
      gradient: {
        transform: 'rotate(90)',
        scheme: [
          { step: '0', color: '#444' },
          { step: '1', color: '#888' }
        ],
        ID: `loadouts_${iconId}_hover_gradient`
      },
      fill: {
        color: `url(#loadouts_${iconId}_hover_gradient)`
      }
    }
  }
}

const getDefaultsSimple = iconName => {
  return {
    iconName,
    fill: {
      color: '#888',
      strokeWidth: '0'
    },
    onHover: {
      active: true,
      fill: {
        color: '#333'
      }
    }
  }
}

const getLocked = iconName => {
  const iconId = iconName.toLowerCase()

  return {
    iconName,
    fill: {
      color: '#ccc',
      strokeWidth: '0'
    },
    filter: getDefaultFilter(iconId)
  }
}

const getHeader = iconName => {
  const iconId = iconName.toLowerCase()

  return {
    iconName,
    fill: {
      color: '#999',
      strokeWidth: '0'
    },
    filter: getHeaderFilter(iconId),
    onHover: {
      active: true,
      fill: {
        color: '#ddd'
      }
    }
  }
}

export const BACK = {
  ...getHeader('Back'),
  dimensions: {
    width: 15,
    height: 13,
    viewbox: '0 0 15 13'
  }
}

export const DROPDOWN_ARROW_PASSIVE = {
  ...getHeader('DropdownArrowPassive'),
  dimensions: {
    width: 16,
    height: 11,
    viewbox: '0 0 16 11'
  }
}

export const DROPDOWN_ARROW_ACTIVE = {
  ...getHeader('DropdownArrowActive'),
  dimensions: {
    width: 11,
    height: 16,
    viewbox: '0 0 11 16'
  }
}

export const EQUIP = {
  ...getDefaults('Equip'),
  dimensions: {
    width: 15,
    height: 16,
    viewbox: '0 0 15 16'
  }
}

export const EQUIP_LOCKED = {
  ...getLocked('Equip'),
  dimensions: EQUIP.dimensions
}

export const RESET = {
  ...getDefaults('Reset'),
  dimensions: {
    width: 20.267,
    height: 15.5,
    viewbox: '0 0 20.267 15.5'
  }
}

export const RESET_LOCKED = {
  ...getLocked('Reset'),
  dimensions: RESET.dimensions
}

export const EDIT = {
  ...getDefaults('Edit'),
  dimensions: {
    width: 17.346,
    height: 18.346,
    viewbox: '0 0 17.346 18.346'
  }
}

export const EDIT_LOCKED = {
  ...getLocked('Edit'),
  dimensions: EDIT.dimensions
}

export const SAVE = {
  ...getDefaults('Save'),
  dimensions: {
    width: 17.416,
    height: 15.083,
    viewbox: '0 0 17.416 15.083'
  }
}

export const SAVE_LOCKED = {
  ...getLocked('Save'),
  dimensions: SAVE.dimensions
}

export const CLOSE = {
  ...getDefaultsSimple('Close'),
  dimensions: {
    width: 12,
    height: 12,
    viewbox: '0 0 25 25'
  }
}

export default {
  ArrowSuperThin: {
    fill: {
      color: '#ccc',
      strokeWidth: 0,
      stroke: 'transparent'
    },
    dimensions: {
      width: 11,
      height: 30,
      viewbox: '0 0 11 30'
    }
  },
  EQUIP,
  RESET
}
