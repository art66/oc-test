const DEFAULT_FILL = {
  color: '#999',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DEFAULT_DIMENSIONS = {
  viewbox: '599.36 433.421 14 12',
  width: 14,
  height: 12
}

const DEFAULT_GRADIENT_DARK_MODE = {
  ID: 'linear-gradient-dark-mode',
  active: true,
  transform: 'rotate(0)',
  scheme: [{ step: '0', color: '#666' }, { step: '1', color: '#888' }]
}

const HOVER_GRADIENT_DARK_MODE = {
  ID: 'linear-gradient-hover-dark-mode',
  active: true,
  transform: 'rotate(0)',
  scheme: [{ step: '0', color: '#999' }, { step: '1', color: '#ddd' }]
}

const DEFAULT_GRADIENT_FILL_DARK_MODE = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient-dark-mode)'
}

const DEFAULT_FILTER = {
  active: true,
  ID: 'default_filter',
  shadow: {
    active: true,
    x: 0,
    y: 1,
    blur: 0,
    color: '#FFFFFFA6'
  }
}

const DEFAULT_FILTER_DARK_MODE = {
  active: true,
  ID: 'default_filter_dark_mode',
  shadow: {
    active: true,
    x: 0,
    y: 1,
    blur: 2,
    color: '#00000073'
  }
}

const DEFAULT = {
  fill: DEFAULT_FILL,
  dimensions: DEFAULT_DIMENSIONS,
  filter: DEFAULT_FILTER
}

const DEFAULT_DARK_MODE = {
  fill: DEFAULT_GRADIENT_FILL_DARK_MODE,
  dimensions: DEFAULT_DIMENSIONS,
  gradient: DEFAULT_GRADIENT_DARK_MODE,
  filter: DEFAULT_FILTER_DARK_MODE
}

export const HOVER = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: '#555'
  }
}

export const HOVER_DARK_MODE = {
  ...DEFAULT_DARK_MODE,
  fill: {
    ...DEFAULT_DARK_MODE.fill,
    color: 'url(#linear-gradient-hover-dark-mode)'
  },
  gradient: HOVER_GRADIENT_DARK_MODE,
  filter: {
    ...DEFAULT_DARK_MODE.filter,
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 2,
      color: '#000000'
    }
  }
}

export const personalStats = {
  DEFAULT,
  DEFAULT_DARK_MODE,
  HOVER,
  HOVER_DARK_MODE
}

export default personalStats
