const DEFAULTS = {
  fill: {
    color: '#888888',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  filter: {
    ID: null,
    active: false,
    shadow: {
      active: false
    }
  }
}

export const BLUE_FILL = {
  ...DEFAULTS.fill,
  color: '#7fa1b2'
}

const DEFAULTS_BLUE = {
  ...DEFAULTS,
  fill: BLUE_FILL
}

export const DISABLE_FILL = {
  ...DEFAULTS.fill,
  color: '#d4dfe5'
}

const DEFAULTS_FULLSCREEN = {
  fill: BLUE_FILL,
  dimensions: {
    width: 30,
    height: 30,
    viewbox: '-6 -6 30 30'
  }
}

const FULLSCREEN_MODE = {
  fill: BLUE_FILL,
  dimensions: {
    width: 16,
    height: 12,
    viewbox: '0 0 16 12'
  }
}

const SEARCH = {
  fill: BLUE_FILL,
  dimensions: {
    width: 14,
    height: 14,
    viewbox: '0 0 14 14'
  }
}

const GESTURE = {
  ...DEFAULTS,
  fill: {
    ...DEFAULTS.fill,
    color: 'var(--ct-hud-secondary-button-color)'
  }
}

const PLAYER = {
  ...DEFAULTS,
  fill: {
    ...DEFAULTS.fill,
    color: '#00bfff'
  }
}

const DANGER = {
  ...DEFAULTS,
  fill: {
    ...DEFAULTS.fill,
    color: '#eb8766'
  }
}

const OBJECTS = {
  ...DEFAULTS_BLUE,
  dimensions: {
    width: 16,
    height: 15,
    viewbox: '0 0 16 15'
  }
}

const PARAMETERS = {
  ...OBJECTS
}

const AREAS = {
  ...OBJECTS
}

const NPC = {
  filter: DEFAULTS.filter,
  fill: BLUE_FILL,
  dimensions: {
    width: 17,
    height: 20,
    viewbox: '0 0 17 20'
  }
}

const BLOCKS = {
  ...DEFAULTS_BLUE,
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '0 0 15 15'
  }
}

const REDO = {
  ...DEFAULTS_BLUE,
  dimensions: {
    width: 16,
    height: 14.67,
    viewbox: '0 0 16 14.67'
  }
}

const UNDO = {
  ...DEFAULTS_BLUE,
  dimensions: {
    width: 16,
    height: 16,
    viewbox: '0 0 24 22'
  }
}

const ACTIVE_GRADIENT = {
  ID: 'active_star',
  active: true,
  transform: 'rotate(90)',
  scheme: [{ step: '0', color: '#35a5d5' }, { step: '1', color: '#97d1e9' }]
}

const INACTIVE_GRADIENT = {
  ID: 'inactive_star',
  active: true,
  transform: 'rotate(90)',
  scheme: [{ step: '0', color: 'rgba(53, 165, 213, 0.2)' }, { step: '1', color: 'rgba(151, 209, 233, 0.2)' }]
}

const ACTIVE_STAR = {
  fill: {
    color: 'url(#active_star)',
    stroke: 'transparent',
    strokeWidth: 0
  },
  dimensions: {
    width: 35,
    height: 34,
    viewbox: '827.5 353 35 34'
  },
  gradient: ACTIVE_GRADIENT
}

const INACTIVE_STAR = {
  fill: {
    color: 'url(#inactive_star)',
    stroke: '#97d1e9',
    strokeWidth: 1
  },
  dimensions: {
    width: 35,
    height: 34,
    viewbox: '827.5 353 35 34'
  },
  gradient: INACTIVE_GRADIENT
}

const ADD_REMOVE_TRIGGER = {
  dimensions: {
    width: 12,
    height: 12,
    viewbox: '0 0 24 24'
  },
  fill: {
    color: '#18A2CF',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const ADD_REMOVE_TRIGGER_HOVERED = {
  ...ADD_REMOVE_TRIGGER,
  fill: {
    ...DEFAULTS.fill,
    color: '#107fcf'
  }
}

const ADD_REMOVE_TRIGGER_DISABLED = {
  ...ADD_REMOVE_TRIGGER,
  fill: {
    ...DEFAULTS.fill,
    color: '#ccc'
  }
}

const UPLOAD_IMAGE_DEFAULT = {
  dimensions: {
    width: 21,
    height: 16,
    viewbox: '0 0 21 16'
  },
  fill: {
    color: '#18A2CF',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const UPLOAD_IMAGE_HOVERED = {
  ...UPLOAD_IMAGE_DEFAULT,
  fill: {
    ...UPLOAD_IMAGE_DEFAULT.fill,
    color: '#107fcf'
  }
}

const UPLOAD_IMAGE_DISABLED = {
  ...UPLOAD_IMAGE_DEFAULT,
  fill: {
    ...UPLOAD_IMAGE_DEFAULT.fill,
    color: '#ccc'
  }
}

const REMOVE_EDIT_IMAGE_DEFAULT = {
  ...UPLOAD_IMAGE_DEFAULT,
  dimensions: {
    width: 22.69,
    height: 17,
    viewbox: '0 0 22.69 17'
  }
}

const REMOVE_EDIT_IMAGE_HOVERED = {
  ...REMOVE_EDIT_IMAGE_DEFAULT,
  fill: {
    ...REMOVE_EDIT_IMAGE_DEFAULT.fill,
    color: '#107fcf'
  }
}

const REMOVE_EDIT_IMAGE_DISABLED = {
  ...REMOVE_EDIT_IMAGE_DEFAULT,
  fill: {
    ...REMOVE_EDIT_IMAGE_DEFAULT.fill,
    color: '#ccc'
  }
}

const LIBRARY_ICONS = {
  dimensions: {
    width: 14,
    height: 14,
    viewbox: '0 0 24 24'
  },
  fill: {
    color: '#bbb',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const NO_EDIT = {
  dimensions: {
    width: 14,
    height: 14,
    viewbox: '0 0 16 16'
  },
  fill: {
    color: '#bbb',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const DELETE_COORD = {
  dimensions: {
    width: 12,
    height: 12,
    viewbox: '-3 -5 24 24'
  },
  fill: {
    color: '#aaa',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const DELETE_COORD_HOVER = {
  ...DELETE_COORD,
  fill: {
    ...DELETE_COORD.fill,
    color: '#eb8766'
  }
}

const VIEW_TOGGLE = {
  dimensions: {
    width: 16,
    height: 17,
    viewbox: '0 0 16 17'
  },
  fill: {
    ...DEFAULTS.fill,
    color: '#7fa1b2'
  }
}

const ACTIVE_VIEW_TOGGLE = {
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '0 0 20 19'
  },
  fill: {
    color: '#7fa1b2',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const INACTIVE_VIEW_TOGGLE = {
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '0 0 20 19'
  },
  fill: {
    color: 'rgba(127, 161, 178, 0.4)',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const ACTIVE_VIEW_TOGGLE_SHOW = {
  dimensions: {
    width: 16,
    height: 9,
    viewbox: '0 0 16 16'
  },
  fill: {
    color: '#82C91E',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const INACTIVE_VIEW_TOGGLE_SHOW = {
  dimensions: {
    width: 16,
    height: 9,
    viewbox: '0 0 16 16'
  },
  fill: {
    color: 'rgba(127, 161, 178, 0.4)',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

const CT_PLAYERS = {
  dimensions: {
    width: 14,
    height: 14,
    viewbox: '0 0 14 14'
  },
  fill: {
    color: '#668fa3',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

export const christmasTown = {
  DEFAULTS,
  DEFAULTS_BLUE,
  DEFAULTS_FULLSCREEN,
  SEARCH,
  DANGER,
  GESTURE,
  PLAYER,
  OBJECTS,
  PARAMETERS,
  AREAS,
  BLOCKS,
  REDO,
  UNDO,
  FULLSCREEN_MODE,
  ACTIVE_STAR,
  INACTIVE_STAR,
  ADD_REMOVE_TRIGGER,
  ADD_REMOVE_TRIGGER_HOVERED,
  ADD_REMOVE_TRIGGER_DISABLED,
  UPLOAD_IMAGE_DEFAULT,
  UPLOAD_IMAGE_DISABLED,
  UPLOAD_IMAGE_HOVERED,
  REMOVE_EDIT_IMAGE_DEFAULT,
  REMOVE_EDIT_IMAGE_HOVERED,
  REMOVE_EDIT_IMAGE_DISABLED,
  LIBRARY_ICONS,
  NPC,
  DELETE_COORD,
  DELETE_COORD_HOVER,
  ACTIVE_VIEW_TOGGLE,
  INACTIVE_VIEW_TOGGLE,
  VIEW_TOGGLE,
  ACTIVE_VIEW_TOGGLE_SHOW,
  INACTIVE_VIEW_TOGGLE_SHOW,
  NO_EDIT,
  CT_PLAYERS
}

export default christmasTown
