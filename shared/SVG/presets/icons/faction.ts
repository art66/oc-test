const DEFAULT = {
  fill: {
    color: 'url(#faction)',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  dimensions: {
    width: 15,
    height: 15,
    viewbox: '0 0 24 24'
  },
  gradient: {
    ID: 'faction',
    active: true,
    transform: 'rotate(90)',
    scheme: [
      { step: '0', color: 'var(--faction-default-icon-bg-stop-1)' },
      { step: '1', color: 'var(--faction-default-icon-bg-stop-2)' }
    ]
  }
}

const DEFAULT_HOVER = {
  ...DEFAULT,
  fill: {
    ...DEFAULT.fill,
    color: 'url(#factionHover)'
  },
  gradient: {
    ...DEFAULT.gradient,
    ID: 'factionHover',
    scheme: [
      { step: '0', color: 'var(--faction-hover-icon-bg-stop-1)' },
      { step: '1', color: 'var(--faction-hover-icon-bg-stop-2)' }
    ]
  }
}

const CHECK_DIMENSIONS = {
  width: 16,
  height: 16,
  viewbox: '0 0 24 24'
}

const SEARCH = {
  ...DEFAULT,
  dimensions: {
    width: 14,
    height: 14,
    viewbox: '0 0 14 14'
  }
}

const SEARCH_HOVER = {
  ...DEFAULT_HOVER,
  dimensions: {
    width: 14,
    height: 14,
    viewbox: '0 0 14 14'
  }
}

const VIEW_APPLICATION = {
  ...DEFAULT,
  dimensions: {
    width: 22,
    height: 14,
    viewbox: '0 0 20 12'
  }
}

const VIEW_APPLICATION_HOVER = {
  ...VIEW_APPLICATION,
  fill: {
    ...VIEW_APPLICATION.fill,
    color: 'url(#factionHover)'
  },
  gradient: {
    ...VIEW_APPLICATION.gradient,
    ID: 'factionHover',
    scheme: [
      { step: '0', color: 'var(--faction-hover-icon-bg-stop-1)' },
      { step: '1', color: 'var(--faction-hover-icon-bg-stop-2)' }
    ]
  }
}

const PERMISSION_CHECKBOX_LEVEL_LOW = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-low-icon-color)'
  }
}

const PERMISSION_CHECKBOX_LEVEL_MID = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-mid-icon-color)'
  }
}

const PERMISSION_CHECKBOX_LEVEL_HIGH = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-high-icon-color)'
  }
}

const PERMISSION_CHECKBOX_LEVEL_DEFAULT = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-default-icon-color)'
  }
}

const PERMISSION_CHECKBOX_INACTIVE = {
  ...DEFAULT,
  dimensions: {
    width: 12,
    height: 12,
    viewbox: '0 0 24 24'
  },
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-default-icon-color)'
  }
}

const PERMISSION_CHECKBOX_LEVEL_LOW_HOVER = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-low-icon-color-hover)'
  }
}

const PERMISSION_CHECKBOX_LEVEL_MID_HOVER = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-mid-icon-color-hover)'
  }
}

const PERMISSION_CHECKBOX_LEVEL_HIGH_HOVER = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-high-icon-color-hover)'
  }
}

const PERMISSION_CHECKBOX_LEVEL_DEFAULT_HOVER = {
  ...DEFAULT,
  dimensions: CHECK_DIMENSIONS,
  fill: {
    ...DEFAULT.fill,
    color: 'var(--faction-position-permission-default-icon-color-hover)'
  }
}

const PERMISSION_CHECKBOX_INACTIVE_HOVER = {
  ...PERMISSION_CHECKBOX_INACTIVE,
  fill: {
    ...PERMISSION_CHECKBOX_INACTIVE.fill,
    color: 'var(--faction-position-default-icon-color-hover)'
  }
}

const REMOVE = {
  ...DEFAULT,
  dimensions: {
    ...DEFAULT.dimensions,
    width: 12,
    height: 12
  }
}

const FACTION_POSITIONS_LIST_ADD = {
  ...DEFAULT,
  dimensions: {
    viewbox: '-6 -6 24 24',
    width: 24,
    height: 24
  }
}

const REMOVE_ACTIVE = {
  ...REMOVE,
  fill: {
    ...REMOVE.fill,
    color: '#ff5c26'
  }
}

const REMOVE_DISABLED = {
  ...REMOVE,
  fill: {
    ...REMOVE.fill,
    color: 'var(--faction-position-permissions-icon-fill-disabled)'
  }
}

const REMOVE_BOLD = {
  ...REMOVE,
  fill: {
    ...REMOVE.fill,
    color: '#444'
  }
}

export const faction = {
  DEFAULT,
  SEARCH,
  SEARCH_HOVER,
  VIEW_APPLICATION,
  VIEW_APPLICATION_HOVER,
  FACTION_POSITIONS_LIST_ADD,
  REMOVE,
  REMOVE_ACTIVE,
  REMOVE_DISABLED,
  REMOVE_BOLD,
  PERMISSION_CHECKBOX_LEVEL_LOW,
  PERMISSION_CHECKBOX_LEVEL_MID,
  PERMISSION_CHECKBOX_LEVEL_HIGH,
  PERMISSION_CHECKBOX_INACTIVE,
  PERMISSION_CHECKBOX_LEVEL_DEFAULT,
  PERMISSION_CHECKBOX_LEVEL_LOW_HOVER,
  PERMISSION_CHECKBOX_LEVEL_MID_HOVER,
  PERMISSION_CHECKBOX_LEVEL_HIGH_HOVER,
  PERMISSION_CHECKBOX_LEVEL_DEFAULT_HOVER,
  PERMISSION_CHECKBOX_INACTIVE_HOVER
}

export default faction
