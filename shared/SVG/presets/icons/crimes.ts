const DEFAULTS = {
  fill: {
    color: 'url(#crimes)',
    stroke: 'black',
    strokeWidth: '0'
  },
  dimensions: {
    width: '17px',
    height: '17px',
    viewbox: '0 0 17 17'
  },
  filter: {
    ID: null,
    active: false,
    shadow: {
      active: false
    }
  },
  gradient: {
    ID: 'crimes',
    active: true,
    scheme: [{ step: '0', color: '#cecebf' }, { step: '1', color: '#9b9b8c' }]
  }
}

export const crimes = {
  DEFAULTS,
  SEARCHFORCASH: {
    ...DEFAULTS,
    gradient: {
      ...DEFAULTS.gradient,
      transform: 'rotate(90)'
    },
    dimensions: {
      ...DEFAULTS.dimensions,
      viewbox: '0 0 27 27'
    }
  },
  BOOTLEGGING: {
    ...DEFAULTS
  },
  GRAFFITI: {
    ...DEFAULTS,
    fill: {
      ...DEFAULTS.fill,
      color: 'url(#crimes)'
    },
    gradient: {
      ...DEFAULTS.gradient,
      scheme: [{ step: '0', color: '#ffdc73' }, { step: '1', color: '#dda80b' }],
      transform: 'rotate(90)'
    },
    deepProps: {
      active: true,
      childrenProps: {
        contour: {
          style: {
            fill: '#F9F9F2'
          }
        }
      }
    }
  },
  SHOPLIFTING: {
    ...DEFAULTS,
    gradient: {
      ...DEFAULTS.gradient,
      scheme: [{ step: '0', color: '#9B9B8C' }, { step: '1', color: '#CECEBF' }],
      transform: 'rotate(90)'
    }
  },
  PICKPOCKETING: {
    ...DEFAULTS,
    gradient: {
      ...DEFAULTS.gradient,
      scheme: [{ step: '0', color: '#9B9B8C' }, { step: '1', color: '#CECEBF' }],
      transform: 'rotate(90)'
    }
  }
}

export default crimes
