const DEFAULT_FILL = {
  color: '#aaa',
  stroke: 'transparent',
  strokeWidth: '0'
}

const DISABLE_FILTER = {
  ID: null,
  shadow: {
    active: false
  }
}

const DEFAULT_GRADIENT = {
  scheme: [
    {
      step: 0,
      color: '#888'
    },
    {
      step: 1,
      color: '#666'
    }
  ],
  ID: 'date-picker-default-arrow-gradient',
  transform: 'rotate(90)'
}

const HOVERED_GRADIENT = {
  scheme: [
    {
      step: 0,
      color: '#ddd'
    },
    {
      step: 1,
      color: '#999'
    }
  ],
  ID: 'date-picker-hover-arrow-gradient',
  transform: 'rotate(90)'
}

const DISABLED_GRADIENT = {
  scheme: [
    {
      step: 0,
      color: '#222'
    },
    {
      step: 1,
      color: '#333'
    }
  ],
  ID: 'date-picker-disabled-arrow-gradient',
  transform: 'rotate(90)'
}

const DEFAULT = {
  fill: DEFAULT_FILL,
  filter: DISABLE_FILTER,
  gradient: DEFAULT_GRADIENT
}

const DISABLED = {
  ...DEFAULT,
  fill: {
    ...DEFAULT_FILL,
    color: '#cecece'
  },
  gradient: DISABLED_GRADIENT
}

const HOVERED = {
  ...DEFAULT,
  fill: {
    ...DEFAULT_FILL,
    color: '#777'
  },
  gradient: HOVERED_GRADIENT
}

const DEFAULT_ARROW = {
  fill: {
    color: '#ccc',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  filter: DISABLE_FILTER,
  gradient: DEFAULT_GRADIENT
}

const HOVERED_ARROW = {
  ...DEFAULT_ARROW,
  fill: {
    ...DEFAULT_ARROW.fill,
    color: '#999'
  },
  gradient: HOVERED_GRADIENT
}

const DISABLED_ARROW = {
  ...DEFAULT_ARROW,
  fill: {
    ...DEFAULT_ARROW.fill,
    color: '#ececec'
  },
  gradient: DISABLED_GRADIENT
}

const RESET_ICON = {
  fill: {
    color: '#888',
    stroke: 'transparent',
    strokeWidth: '0'
  },
  filter: DISABLE_FILTER
}

const CALENDAR_ICON = {
  ...DEFAULT,
  fill: {
    color: '#999',
    stroke: 'transparent',
    strokeWidth: '0'
  }
}

export const activityLog = {
  DEFAULT,
  DISABLED,
  HOVERED,
  DEFAULT_ARROW,
  HOVERED_ARROW,
  DISABLED_ARROW,
  RESET_ICON,
  CALENDAR_ICON
}

export default activityLog
