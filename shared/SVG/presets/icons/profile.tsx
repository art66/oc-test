const DEFAULT_FILL = {
  color: '#3e3e3e',
  stroke: '#d4d4d4',
  strokeWidth: '0'
}

const DEFAULT_DIMENSIONS = {
  viewbox: '0 0 44 44',
  width: 44,
  height: 44
}

const DEFAULT_GRADIENT = {
  ID: 'linear-gradient',
  active: true,
  transform: 'rotate(90)',
  scheme: [{ step: '0', color: '#999' }, { step: '1', color: '#ccc' }]
}

const DEFAULT_GRADIENT_DARK_MODE = {
  ID: 'linear-gradient-dark-mode',
  active: true,
  transform: 'rotate(90)',
  scheme: [{ step: '0', color: '#666' }, { step: '1', color: '#888' }]
}

const DEFAULT_GREEN_GRADIENT = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-green',
  scheme: [{ step: '0', color: '#7f9e5f' }, { step: '1', color: '#c3d2b5' }]
}

const DEFAULT_GREEN_GRADIENT_DARK_MODE = {
  ...DEFAULT_GRADIENT_DARK_MODE,
  ID: 'linear-gradient-green-dark-mode',
  scheme: [{ step: '0', color: '#C3D2B5' }, { step: '1', color: '#7F9E5F' }]
}

const DEFAULT_RED_GRADIENT = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-red',
  scheme: [{ step: '0', color: '#9e745f' }, { step: '1', color: '#d2bfb5' }]
}

const DEFAULT_RED_GRADIENT_DARK_MODE = {
  ...DEFAULT_GRADIENT_DARK_MODE,
  ID: 'linear-gradient-red-dark-mode',
  scheme: [{ step: '0', color: '#E1B9A4' }, { step: '1', color: '#945231' }]
}

const DEFAULT_DISABLE_GRADIENT_DARK_MODE = {
  ...DEFAULT_GRADIENT_DARK_MODE,
  ID: 'linear-gradient-disable-dark-mode',
  scheme: [{ step: '0', color: '#000000' }, { step: '1', color: '#111111' }]
}

const DEFAULT_GRADIENT_FILL = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient)'
}

const DEFAULT_GRADIENT_FILL_DARK_MODE = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient-dark-mode)'
}

const DEFAULT_GRADIENT_GREEN_FILL = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient-green)'
}

const DEFAULT_GRADIENT_GREEN_FILL_DARK_MODE = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient-green-dark-mode)'
}

const DEFAULT_GRADIENT_RED_FILL = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient-red)'
}

const DEFAULT_GRADIENT_RED_FILL_DARK_MODE = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient-red-dark-mode)'
}

const DEFAULT_GRADIENT_DISABLED_FILL_DARK_MODE = {
  ...DEFAULT_FILL,
  color: 'url(#linear-gradient-disable-dark-mode)',
  opacity: 0.5
}

const DISABLE_FILTER = {
  ID: null,
  shadow: {
    active: false
  }
}

const DEFAULT_PROPERTIES = {
  fill: DEFAULT_GRADIENT_FILL,
  dimensions: DEFAULT_DIMENSIONS,
  filter: DISABLE_FILTER,
  gradient: DEFAULT_GRADIENT
}

const DEFAULT_PROPERTIES_DARK_MODE = {
  fill: DEFAULT_GRADIENT_FILL_DARK_MODE,
  dimensions: DEFAULT_DIMENSIONS,
  gradient: DEFAULT_GRADIENT_DARK_MODE
}

const HOVER_GRADIENT_BLUE = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-2',
  scheme: [{ step: '0', color: '#65aacc' }, { step: '1', color: '#afd2e4' }]
}

const HOVER_GRADIENT_BLUE_DARK_MODE = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-2-dark-mode',
  scheme: [{ step: '0', color: '#AFD2E4' }, { step: '1', color: '#65AACC' }]
}

const HOVER_GRADIENT_RED = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-red-hover',
  scheme: [{ step: '0', color: '#945231' }, { step: '1', color: '#e1b9a4' }]
}

const HOVER_GRADIENT_RED_DARK_MODE = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-red-hover-dark-mode',
  scheme: [{ step: '0', color: '#E1B9A4' }, { step: '1', color: '#945231' }]
}

const HOVER_GRADIENT_GREEN = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-green-hover',
  scheme: [{ step: '0', color: '#638a3b' }, { step: '1', color: '#c3daab' }]
}

const HOVER_GRADIENT_GREEN_DARK_MODE = {
  ...DEFAULT_GRADIENT,
  ID: 'linear-gradient-green-hover-dark-mode',
  scheme: [{ step: '0', color: '#C3D2B5' }, { step: '1', color: '#7F9E5F' }]
}

// CROSS-DISABLED
export const CROSS_DISABLED = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'rgba(217, 54, 0, 0.5)'
  }
}

export const CROSS_DISABLED_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: {
    ...DEFAULT_PROPERTIES_DARK_MODE.fill,
    color: 'rgba(217, 54, 0, 0.5)'
  }
}

// CLICKABLE DISABLED
export const CLICKABLE_DISABLED = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'rgba(153, 153, 153, 0.4)'
  }
}

export const CLICKABLE_DISABLED_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: DEFAULT_GRADIENT_DISABLED_FILL_DARK_MODE,
  gradient: DEFAULT_DISABLE_GRADIENT_DARK_MODE
}

// ACTIVE - DEFAULT
export const ACTIVE = {
  ...DEFAULT_PROPERTIES
}

export const ACTIVE_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE
}

export const HIDDEN = {
  ...DEFAULT_PROPERTIES
}

export const HIDDEN_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE
}

// ACTIVE - GREEN
export const GREEN_ACTIVE = {
  ...DEFAULT_PROPERTIES,
  fill: DEFAULT_GRADIENT_GREEN_FILL,
  gradient: DEFAULT_GREEN_GRADIENT
}

export const GREEN_ACTIVE_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: DEFAULT_GRADIENT_GREEN_FILL_DARK_MODE,
  gradient: DEFAULT_GREEN_GRADIENT_DARK_MODE
}

// ACTIVE - RED
export const RED_ACTIVE = {
  ...DEFAULT_PROPERTIES,
  fill: DEFAULT_GRADIENT_RED_FILL,
  gradient: DEFAULT_RED_GRADIENT
}

export const RED_ACTIVE_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: DEFAULT_GRADIENT_RED_FILL_DARK_MODE,
  gradient: DEFAULT_RED_GRADIENT_DARK_MODE
}

// DISABALED - DEFAULT
export const DISABLED = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'rgba(153, 153, 153, 0.4)'
  }
}

export const DISABLED_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: DEFAULT_GRADIENT_DISABLED_FILL_DARK_MODE,
  gradient: DEFAULT_DISABLE_GRADIENT_DARK_MODE
}

// DISABLED - GREEN
export const GREEN_DISABLED = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'rgb(175,190,159)'
  }
}

export const GREEN_DISABLED_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: {
    ...DEFAULT_PROPERTIES_DARK_MODE.fill,
    color: 'rgb(175,190,159)'
  }
}

// DISABLED - RED
export const RED_DISABLED = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'rgb(190,169,159)'
  }
}

export const RED_DISABLED_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: {
    ...DEFAULT_PROPERTIES_DARK_MODE.fill,
    color: 'rgb(190,169,159)'
  }
}

// HOVER BLUE
export const HOVER = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'url(#linear-gradient-2)'
  },
  gradient: HOVER_GRADIENT_BLUE
}

export const HOVER_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: {
    ...DEFAULT_PROPERTIES_DARK_MODE.fill,
    color: 'url(#linear-gradient-2-dark-mode)'
  },
  gradient: HOVER_GRADIENT_BLUE_DARK_MODE
}

// HOVER RED
export const RED_HOVER = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'url(#linear-gradient-red-hover)'
  },
  gradient: HOVER_GRADIENT_RED
}

export const RED_HOVER_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: {
    ...DEFAULT_PROPERTIES_DARK_MODE.fill,
    color: 'url(#linear-gradient-red-hover-dark-mode)'
  },
  gradient: HOVER_GRADIENT_RED_DARK_MODE
}

// HOVER GREEN
export const GREEN_HOVER = {
  ...DEFAULT_PROPERTIES,
  fill: {
    ...DEFAULT_PROPERTIES.fill,
    color: 'url(#linear-gradient-green-hover)'
  },
  gradient: HOVER_GRADIENT_GREEN
}

export const GREEN_HOVER_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: {
    ...DEFAULT_PROPERTIES_DARK_MODE.fill,
    color: 'url(#linear-gradient-green-hover-dark-mode)'
  },
  gradient: HOVER_GRADIENT_GREEN_DARK_MODE
}

const DEFAULT_PROPERTIES_USER_STATUS = {
  fill: {
    color: 'rgba(195,64,21,0.75)',
    stroke: 'transparent',
    strokeWidth: '0',
    opacity: '0'
  },
  dimensions: {
    viewbox: '0 0 40 40',
    width: 40,
    height: 40
  }
}

const JAIL_STATUS = {
  ...DEFAULT_PROPERTIES_USER_STATUS,
  dimensions: {
    ...DEFAULT_PROPERTIES_USER_STATUS.dimensions,
    viewbox: '294 1260 40 40'
  }
}

const HOSPITAL_STATUS = {
  ...DEFAULT_PROPERTIES_USER_STATUS,
  dimensions: {
    ...DEFAULT_PROPERTIES_USER_STATUS.dimensions,
    viewbox: '229 1260 40 40'
  }
}

const FEDERAL_STATUS = {
  ...DEFAULT_PROPERTIES_USER_STATUS,
  fill: {
    ...DEFAULT_PROPERTIES_USER_STATUS.fill,
    color: 'rgba(195,64,21,0.85)'
  },
  dimensions: {
    ...DEFAULT_PROPERTIES_USER_STATUS.dimensions,
    viewbox: '346 1260 40 40'
  }
}

const TRAVELLING_STATUS = {
  ...DEFAULT_PROPERTIES_USER_STATUS,
  fill: {
    ...DEFAULT_PROPERTIES_USER_STATUS.fill,
    color: 'rgba(43,141,173,0.7)'
  },
  dimensions: {
    ...DEFAULT_PROPERTIES_USER_STATUS.dimensions,
    viewbox: '408 1259 40 40'
  }
}

const LOOT_STATUS = {
  ...DEFAULT_PROPERTIES_USER_STATUS,
  fill: {
    ...DEFAULT_PROPERTIES_USER_STATUS.fill,
    color: 'rgba(217,163,0,0.8)'
  },
  dimensions: {
    ...DEFAULT_PROPERTIES_USER_STATUS.dimensions,
    viewbox: '0 1 40 40'
  }
}

export const profile = {
  CLICKABLE_DISABLED,
  CLICKABLE_DISABLED_DARK_MODE,
  CROSS_DISABLED,
  CROSS_DISABLED_DARK_MODE,
  RED_DISABLED,
  RED_DISABLED_DARK_MODE,
  GREEN_DISABLED,
  GREEN_DISABLED_DARK_MODE,
  RED_ACTIVE,
  RED_ACTIVE_DARK_MODE,
  GREEN_ACTIVE,
  GREEN_ACTIVE_DARK_MODE,
  DISABLED,
  DISABLED_DARK_MODE,
  GREEN_HOVER,
  GREEN_HOVER_DARK_MODE,
  RED_HOVER,
  RED_HOVER_DARK_MODE,
  HOVER,
  HOVER_DARK_MODE,
  ACTIVE,
  ACTIVE_DARK_MODE,
  HIDDEN,
  HIDDEN_DARK_MODE,
  JAIL_STATUS,
  HOSPITAL_STATUS,
  FEDERAL_STATUS,
  TRAVELLING_STATUS,
  LOOT_STATUS
}

export default profile
