export const DEFAULT_FILL = {
  color: '#74b816',
  stroke: 'transparent',
  strokeWidth: '0'
}

export const DEFAULT_FILL_DARK_MODE = {
  color: '#94D82D',
  stroke: 'transparent',
  strokeWidth: '0'
}

export const INACTIVE_FILL = {
  ...DEFAULT_FILL,
  color: '#d4d4d4'
}

export const INACTIVE_FILL_DARK_MODE = {
  ...DEFAULT_FILL,
  color: '#666'
}

export const READY_FILL = {
  ...DEFAULT_FILL,
  color: '#5c7cfa'
}

export const READY_FILL_DARK_MODE = {
  ...DEFAULT_FILL,
  color: '#5C7CFA'
}

const DISABLE_FILTER = {
  ID: null,
  shadow: {
    active: false
  }
}

const DEFAULT_DIMENSIONS = {
  viewbox: '0 0 24 24',
  width: 22,
  height: 22
}

const DEFAULT_PROPERTIES = {
  fill: DEFAULT_FILL,
  dimensions: DEFAULT_DIMENSIONS,
  filter: DISABLE_FILTER
}

const INACTIVE_PROPERTIES = {
  ...DEFAULT_PROPERTIES,
  fill: INACTIVE_FILL
}

const READY_PROPERTIES = {
  ...DEFAULT_PROPERTIES,
  fill: READY_FILL
}

const DEFAULT_PROPERTIES_DARK_MODE = {
  fill: DEFAULT_FILL_DARK_MODE,
  dimensions: DEFAULT_DIMENSIONS,
  filter: DISABLE_FILTER
}

const INACTIVE_PROPERTIES_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: INACTIVE_FILL_DARK_MODE
}

const READY_PROPERTIES_DARK_MODE = {
  ...DEFAULT_PROPERTIES_DARK_MODE,
  fill: READY_FILL_DARK_MODE
}

export const stockMarket = {
  DEFAULT_PROPERTIES,
  INACTIVE_PROPERTIES,
  READY_PROPERTIES,
  DEFAULT_PROPERTIES_DARK_MODE,
  INACTIVE_PROPERTIES_DARK_MODE,
  READY_PROPERTIES_DARK_MODE
}

export default stockMarket
