export const GRADIENT = [
  { step: '0', color: '#999' },
  { step: '1', color: '#ccc' }
]
export const ACTIVE_GRADIENT = [
  { step: '0', color: '#666' },
  { step: '1', color: '#999' }
]

export const DEFAULT = {
  gradient: {
    active: true,
    transform: 'rotate(90)',
    scheme: GRADIENT,
    ID: 'bazaar_view_gradient'
  },
  fill: {
    color: 'url(#bazaar_view_gradient)',
    strokeWidth: '0'
  },
  filter: {
    active: true,
    ID: 'bazaar_view_shadow',
    shadow: {
      active: true,
      x: 0,
      y: 1,
      blur: 0.1,
      color: '#fff'
    }
  },
  onHover: {
    active: true,
    gradient: {
      transform: 'rotate(90)',
      scheme: ACTIVE_GRADIENT,
      ID: 'bazaar_view_hover_gradient'
    },
    fill: {
      color: 'url(#bazaar_view_hover_gradient)',
      strokeWidth: '0'
    }
  }
}

export const BAZAAR_VIEW_ICON_PRESET = {
  ...DEFAULT,
  iconName: 'BazaarView',
  dimensions: {
    width: 16,
    height: 10,
    viewbox: '0 0 18 11'
  }
}

export const BAZAAR_BUY_ICON_PRESET = {
  ...DEFAULT,
  iconName: 'BazaarBuy',
  dimensions: {
    width: 16,
    height: 15,
    viewbox: '0 0 16 15.97'
  }
}

export const BAZAAR_CROSS_ICON_PRESET = {
  ...DEFAULT,
  iconName: 'BazaarCross',
  dimensions: {
    width: 12,
    height: 11,
    viewbox: '0 0 14 13'
  }
}

export const LOCK_ICON_PRESET = {
  iconName: 'Lock',
  dimensions: {
    width: 24,
    height: 30,
    viewbox: '0 0 24 30'
  },
  gradient: {
    active: true,
    transform: 'rotate(90)',
    scheme: [
      { step: '0', color: '#fff' },
      { step: '1', color: '#eee' }
    ],
    ID: `lock_gradient`
  },
  fill: {
    color: `url(#lock_gradient)`,
    strokeWidth: '0'
  },
  filter: {
    active: true,
    ID: `lock_shadow`,
    shadow: {
      active: true,
      x: 0,
      y: 0,
      blur: 1,
      color: '#c92a2a'
    }
  }
}

export default {
  BAZAAR_VIEW_ICON_PRESET,
  BAZAAR_BUY_ICON_PRESET,
  BAZAAR_CROSS_ICON_PRESET,
  LOCK_ICON_PRESET
}

// FIXME: The colors are wrong.
