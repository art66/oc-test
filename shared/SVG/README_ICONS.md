
# Global SVG Icons Library

How to use:
  - Quick Start:

  for SVGIconGenerator usage you just need to set explicitly name of icon which you want to get on the ouptut. Let's imagine you need to render `Chat` icon. So, throw the `iconName='Chat'` string param to it and you'll see how magic happen:

    ```<SVGIconGenerator iconName='Chat' />```

  ...and by this simple way you'll get the icon with default pre-configurated props getted from constant parameters: `dimensions`, `filling`, `filter` and etc.

  - Advanced SVG Configuration:

  if we talk about real-world App, just settings up ison name probably woln't not enought. To cut off borring icon settings process you can once create the preset inside `./presets` folder for some cogort of the icons(check alredy created crimes preset for example) and then use them everywere:

    ```<SVGIconGenerator
        iconsHolder={import(/* webpackChunkName: "globalSVGIcons" */ '@torn/shared/SVG/helpers/iconsHolder/global')}
        iconName='Chat'
        preset: {{
          type: 'crimes',
          subtype: 'SEARCHFORCASH'
        }}
      />```

  ...this is the most balanced way under the straingforward and hardwest of settings up the SVG icon.

  - Custom SVG configuration:

  in case, when you need to make some customizations of your icon (probably you'll always need this) or change some props of presets configuration - you need to set icon configuration manually by proding is via props:

    ```<SVGIconGenerator
          iconName='Chat'
          dimensions={{
            width: '20px',
            height: '30px',
            virwbox: '0 0 20 30'
          }}
          fill={{
            color: '#111',
            stroke: '#fff',
            strokeWidth: '1'
          }}
          filter={{
            ID: 'awesome_SVG_ID',
            active: true,
            shadow: {
              active: true,
              x: 0,
              y: 1,
              blur: 1,
              color: 'black'
            }
            customFilter: {
              active: true,
              filter: (
                <feBlur>1<feBlur>
              )
            }
          }}
          gradient: {
            scheme: [
              {
                step: 0,
                color: 'black'
              },
              {
                step: 1,
                color: 'white'
              }
            ],
            ID: 'some_ID',
            transform: 'rotate(90)',
            additionalProps: (
              <feDropShadow>3<feDropShadow>
            )
          }
        />```

  ...please, note, if you are using both `preset` and `custom` configurations for one icon, the last one will override each equvivalent prop filed setted in `preset` configuration.

  - !!!PRO!!! Level in SVG configuration:

  in case, when you need to make some manual customizing by handling some Events, you can try to set props that you need to change via `onClick` or `onHover` handlers dynamically:

    ```<SVGIconGenerator
          iconName='Chat'
          fill={{
            color: '#111',
            stroke: '#fff',
            strokeWidth: '1'
          }}
          onHover={{
            active: true,
            fill: {
              color: 'red'
            }
          }}
          onClick={{
            active: true,
            fill: {
              color: 'yellow'
            }
          }}
        />```

  so, here we see that in Events configs we have put only `color` of the whole `fill` config that we also have separatly in the original SVGIcongenerator props. And that's the main point here. SVGIcongenerator logic will merge original and partial-Events props provided in one common object via *deep merge* to use them all together.

FAQ:
 - Can I make icon config cusumization variosly, for example just set `width` in `dimensions` or `color` in `fill` props?
 - Shure, SVGIconGenerator will only grab these props for overring the default ones. All non-setted props will be getted from the default constants configuration.

 - Can I use both `preset` and `custom` icon configurations in the same time?
 - Shure, SVGIconGenerator will set up icon configuration from the `preset` configuration and for props that you'll provide exclictly it will override the `presets` ones. If somde props will be unsetted, the SVGIconGenerator will grab them from default constants config.

 - What props should be throwed in SVGIconGenerator for its normal work?
 - There is only one reqired prop `iconName` that must be setted each time obviously. All other props are non-reqired.

 - What types SVGIconGenerator props can assepts (string, object, number)?
 - This info you can check out in the `./types/icon.ts` file.

  - I want to take SVG Children an over control, how can I can?
  - This is not a typical situation for SVG Icon usage, most probably you would don't need this. Anyway, we have an API to deal with such task. So, you need to set up the two extra props for SVGIconGenerator: `setDeepSVGProps` boolean flag, need to activate this feature and `deepSVGProps` - data object with props for SVG childrens. Example is below:

  ```
    const deepSVGProps = {
      parentProps: {
        fill: 'red',
        style: {
          color: 'black',
          width: '30px',
          height: '30px'
        }
      },
      childrenProps: { // can also assept an array of children props with ID propvided for each. See `childrenPropsAsArray` config below.
        contour: {
          style: {
            fill: 'green'
          }
        },
        figure: {
          style: {
            fill: 'red'
          }
        }
      }
    }

    const childrenPropsAsArray = [
      {
        ID: 0,
        style: {
          fill: 'yellow'
        }
      },
      {
        ID: 1,
        style: {
          fill: 'green'
        }
      },
      {
        ID: 2,
        style: {
          fill: 'red'
        }
      }
    ]
  ```

Other info:
  - Extra bonus, regExp pattern for wrapping all icons in the quoutes after parsing from iconHolder exports list! Try: `\'Icon`, Icon2, Icon3...\'.replace(/(\w*[^,|\n|\f|\r])/gi, '"$1"')`. Works with multilines also!
