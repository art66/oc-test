import LoadingIndicator from '@torn/shared/components/LoadingIndicator'
import PropTypes from 'prop-types'
import React from 'react'

const boolToStr = val => val || ''

class LoadingIndicatorHOC extends React.PureComponent {
  render() {
    const { loading, options = {}, WrappedComponent, ...rest } = this.props

    if (loading) {
      const className = boolToStr(options.className) + boolToStr(options.inheritClass && ` ${rest.className}`)

      return <LoadingIndicator className={className} />
    }

    return <WrappedComponent {...rest} />
  }
}

export default LoadingIndicatorHOC

LoadingIndicatorHOC.propTypes = {
  loading: PropTypes.bool,
  options: PropTypes.object,
  WrappedComponent: PropTypes.elementType.isRequired
}
