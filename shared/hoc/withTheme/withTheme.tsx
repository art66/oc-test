import { TOGGLE_DARK_MODE_EVENT } from '@torn/header/src/constants'
import { TToggleDarkModeEventPayload } from '@torn/header/src/interfaces/TToggleDarkModeEventPayload'
import { getCookie } from '@torn/shared/utils'
import React, { useEffect, useState } from 'react'

type TTheme = 'dark' | 'light'
export type TWithThemeInjectedProps = { theme: TTheme }

const boolToValue = (isDarkMode: boolean) => (isDarkMode ? 'dark' : 'light')

export const withTheme = <T extends TWithThemeInjectedProps>(WrappedComponent: React.ComponentType<T>) => (
  props: Omit<T, keyof TWithThemeInjectedProps>
) => {
  const [theme, setTheme] = useState<TTheme>(boolToValue(getCookie('darkModeEnabled') === 'true'))
  const setEventValue = (event: CustomEventInit<TToggleDarkModeEventPayload>) =>
    setTheme(boolToValue(event.detail.checked))

  useEffect(() => {
    window.addEventListener(TOGGLE_DARK_MODE_EVENT, setEventValue)
    return () => window.removeEventListener(TOGGLE_DARK_MODE_EVENT, setEventValue)
  }, [])
  return <WrappedComponent {...({ ...props, theme } as T)} />
}
