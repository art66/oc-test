import { connect } from 'react-redux'
import InfoBox from '@torn/shared/components/InfoBox'

const mapStateToProps = state => ({
  ...state.infoBox
})

export default connect(mapStateToProps)(InfoBox)
