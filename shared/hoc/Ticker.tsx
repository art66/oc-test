import React, { Component } from 'react'

interface IProps {
  options: {
    operation: (number) => number
    iteratedProperty: string
    delay: number
    start: number
    end: number
    callbackName: string
  }
  wrapProps: {
    extraTickCallback: (number) => void
    callback: () => void
  }
  WrappedComponent: Component
}

interface IState {
  tick: number
}

const minus = x => x - 1

export class Ticker extends Component<IProps, IState> {
  private interval: any

  constructor(props) {
    super(props)

    this.state = { tick: 0 }
  }

  componentDidMount() {
    this.onUpdate(this.props)
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props) !== JSON.stringify(prevProps)) {
      this.onUpdate(this.props)
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  onUpdate(props) {
    const { options } = this.props
    const { operation = minus, iteratedProperty = 'timeLeft', delay = 1000, start, end, callbackName } = options
    const initialTick = props.wrapProps[iteratedProperty] || start

    clearInterval(this.interval)
    this.setState({ tick: initialTick })

    this.interval = setInterval(() => {
      const { tick } = this.state
      const callback = this.props.wrapProps[callbackName]
      const { extraTickCallback } = this.props.wrapProps

      this.setState({ tick: operation(tick) }, () => {
        extraTickCallback && extraTickCallback(tick)

        if (end !== undefined && tick === end) {
          callback && callback()
          clearInterval(this.interval)
        }
      })
    }, delay)
  }

  render() {
    const { tick } = this.state
    const { WrappedComponent, wrapProps, options } = this.props
    const { iteratedProperty = 'timeLeft' } = options

    // @ts-ignore
    return <WrappedComponent {...wrapProps} {...{ [iteratedProperty]: tick }} />
  }
}

export default Ticker
