/* eslint-disable global-require */
// for some reasons postccs config doesn't work with dynamic imports
const cssConfig = require('./config/postCSS')

// postcss-preset-env use autoprefixer under the hood,
// so we don't need to import it additionally
module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-preset-env')(cssConfig['postcss-preset-env']),
    require('cssnano')(cssConfig['cssnano'])
  ]
}
