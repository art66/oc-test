import * as actionTypes from '../actions/recentHistory/actionTypes'
import LogHelper from '../utils/logHelper'

const initialState = {}

const ACTION_HANDLERS = {
  [actionTypes.SET_INIT_RECENT_HISTORY_DATA]: (state, action) => {
    return {
      ...state,
      history: action.payload.log,
      startFrom: action.payload.startFrom,
      isFetching: false
    }
  },
  [actionTypes.CHANGE_ACTION_GROUP]: (state, action) => {
    const logHelper = new LogHelper(state.history, action.payload)

    return {
      ...state,
      history: logHelper.recalculateHistory()
    }
  },
  [actionTypes.TOGGLE_SIMILAR_ACTIONS]: (state, action) => {
    return {
      ...state,
      history: state.history.map((group) => {
        return {
          ...group,
          log: group.log.map((record) => {
            if (action.payload.actionId === record.ID) {
              return {
                ...record,
                similar: {
                  ...record.similar,
                  open: action.payload.open
                }
              }
            }
            return record
          })
        }
      })
    }
  },
  [actionTypes.TOGGLE_ACTION_EXPAND_STATE]: (state, action) => {
    const { actionId, parentId } = action.payload

    return {
      ...state,
      history: state.history.map((group) => {
        return {
          ...group,
          log: group.log.map((record) => {
            if (parentId && record.similar) {
              return {
                ...record,
                similar: {
                  ...record.similar,
                  log: record.similar.log.map((similarRecord) => {
                    return actionId === similarRecord.ID ?
                      {
                        ...similarRecord,
                        expanded: !similarRecord.expanded
                      } :
                      similarRecord
                  })
                }
              }
            }
            if (actionId === record.ID) {
              return {
                ...record,
                expanded: !record.expanded
              }
            }
            return record
          })
        }
      })
    }
  },
  [actionTypes.SET_FETCH_STATE]: (state, action) => {
    return {
      ...state,
      isFetching: action.payload.isFetching
    }
  },
  [actionTypes.SET_NEXT_CHUNK]: (state, action) => {
    return {
      ...state,
      history: [...state.history.slice(0, state.history.length - 1), ...action.payload.log],
      startFrom: action.payload.startFrom
    }
  },
  [actionTypes.SET_START_FROM]: (state, action) => {
    return {
      ...state,
      startFrom: action.payload.startFrom
    }
  },
  [actionTypes.RESET_RESENT_HISTORY_DATA]: () => initialState
}

export { ACTION_HANDLERS as RECENT_HISTORY_ACTION_HANDLERS }

export default function recentHistoryReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
