import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import headerReducer from './header'
import newsTickerReducer from './newsTicker'
import recentHistoryReducer from './recentHistory'

export const makeRootReducer = (asyncReducers?: object): any => {
  return combineReducers({
    ...asyncReducers,
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    }),
    header: headerReducer,
    newsTicker: newsTickerReducer,
    recentHistory: recentHistoryReducer
  })
}

export const injectReducer = (store: any, { key, reducer }: any) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return
  store.asyncReducers[key] = reducer

  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
