import uniqBy from 'lodash/uniqBy'
import * as actionTypes from '../actions/actionTypes'
import initialState from '../constants/initialState'
import { FACTIONS_FIELD_NAME } from '../constants/advancedSearch'

const ACTION_HANDLERS = {
  [actionTypes.SET_INITIAL_HEADER_DATA]: (state, action) => {
    const {
      data: { user, settings, logo, links, globalSearch, advancedSearch, serverState, headlines }
    } = action.payload

    return {
      ...state,
      user,
      settings,
      logo,
      headlines,
      globalSearch: {
        ...state.globalSearch,
        ...globalSearch
      },
      advancedSearch: {
        ...state.advancedSearch,
        ...advancedSearch
      },
      serverState,
      leftMenu: {
        ...state.leftMenu,
        links
      }
    }
  },
  [actionTypes.HEADER_DATA_IS_FETCHED]: (state, action) => {
    return {
      ...state,
      headerDataIsFetched: action.payload.dataIsFetched
    }
  },
  [actionTypes.TOGGLE_TOP_LEFT_MENU]: (state, action) => {
    return {
      ...state,
      leftMenu: {
        ...state.leftMenu,
        open: action.payload.open
      }
    }
  },
  [actionTypes.SET_AUTOCOMPLETE_RESULT_LIST]: (state, action) => {
    const { resultList } = action.payload

    return {
      ...state,
      globalSearch: {
        ...state.globalSearch,
        resultList
      }
    }
  },
  [actionTypes.UPDATE_GLOBAL_SEARCH_INPUT_VALUE]: (state, action) => {
    const { inputValue } = action.payload

    return {
      ...state,
      globalSearch: {
        ...state.globalSearch,
        inputValue
      }
    }
  },
  [actionTypes.TOGGLE_GLOBAL_SEARCH]: (state, action) => {
    return {
      ...state,
      globalSearch: {
        ...state.globalSearch,
        open: action.payload.open
      }
    }
  },
  [actionTypes.SET_ADVANCED_SEARCH_AUTOCOMPLETE_RESULT_LIST]: (state, action) => {
    return {
      ...state,
      advancedSearch: {
        ...state.advancedSearch,
        factionsResultList: action.payload.resultList
      }
    }
  },
  [actionTypes.TOGGLE_TOP_RIGHT_MENU]: (state, action) => {
    return {
      ...state,
      rightMenu: {
        ...state.rightMenu,
        open: action.payload.open
      }
    }
  },
  [actionTypes.SWITCH_SETTINGS]: (state, action) => {
    return {
      ...state,
      rightMenu: {
        ...state.rightMenu,
        [action.payload.settingName]: action.payload.value
      }
    }
  },
  [actionTypes.TOGGLE_TC_CLOCK]: (state, action) => {
    return {
      ...state,
      TCClock: {
        ...state.TCClock,
        open: action.payload.open
      }
    }
  },
  [actionTypes.TOGGLE_RECENT_HISTORY]: (state, action) => {
    return {
      ...state,
      recentHistory: {
        ...state.recentHistory,
        open: action.payload.open
      }
    }
  },
  [actionTypes.TOGGLE_ADVANCED_SEARCH]: (state, action) => {
    return {
      ...state,
      advancedSearch: {
        ...state.advancedSearch,
        open: action.payload.open
      }
    }
  },
  [actionTypes.UPDATE_ADVANCED_SEARCH_FORM_FIELD]: (state, action) => {
    return {
      ...state,
      advancedSearch: {
        ...state.advancedSearch,
        formData: {
          ...state.advancedSearch.formData,
          [action.payload.fieldName]: action.payload.value
        }
      }
    }
  },
  [actionTypes.RESET_ADVANCED_SEARCH_FORM]: state => {
    return {
      ...state,
      advancedSearch: {
        ...state.advancedSearch,
        formData: initialState.advancedSearch.formData
      }
    }
  },
  [actionTypes.UPDATE_AVATAR]: (state, action) => {
    const { avatar } = action.payload

    return {
      ...state,
      user: {
        ...state.user,
        data: {
          ...state.user.data,
          avatar: {
            ...state.user.data.avatar,
            ...avatar
          }
        }
      }
    }
  },
  [actionTypes.SET_ADVANCED_SEARCH_FORM_DATA]: (state, action) => {
    return {
      ...state,
      advancedSearch: {
        ...state.advancedSearch,
        formData: {
          ...state.advancedSearch.formData,
          ...action.payload.formData
        }
      }
    }
  },
  [actionTypes.SET_GLOBAL_SEARCH_DATA]: (state, action) => {
    return {
      ...state,
      globalSearch: {
        ...state.globalSearch,
        optionsList: action.payload.optionsList
      }
    }
  },
  [actionTypes.UPDATE_USER_DATA]: (state, action) => {
    return {
      ...state,
      user: {
        ...state.user,
        data: {
          ...state.user.data,
          ...action.payload.data
        },
        state: {
          ...state.user.state,
          ...action.payload.state
        }
      }
    }
  },
  [actionTypes.SET_EAT_LETTER]: (state, action) => {
    return {
      ...state,
      logo: {
        ...state.logo,
        additionalData: {
          ...state.logo.additionalData,
          eatLetter: action.payload.letter
        }
      }
    }
  },
  [actionTypes.SET_IS_EATING_LETTER]: (state, action) => {
    return {
      ...state,
      logo: {
        ...state.logo,
        additionalData: {
          ...state.logo.additionalData,
          isEating: action.payload.isEatingLetter
        }
      }
    }
  },
  [actionTypes.SET_BIRTHDAY_BONUSES]: (state, action) => {
    const {
      data: { timeout, used }
    } = action.payload
    const eatLetterState = state.logo.additionalData.eatLetter
    const lettersList = state.logo.additionalData.letters.map(item => {
      if (item.name.toLowerCase() === eatLetterState && used) {
        return {
          ...item,
          used,
          timeout
        }
      }

      return item
    })

    return {
      ...state,
      logo: {
        ...state.logo,
        additionalData: {
          ...state.logo.additionalData,
          letters: lettersList
        }
      }
    }
  },
  [actionTypes.ADD_ADVANCED_SEARCH_FACTION_FIELD]: (state, action) => {
    const factions = state.advancedSearch.formData[FACTIONS_FIELD_NAME] || []

    return {
      ...state,
      advancedSearch: {
        ...state.advancedSearch,
        formData: {
          ...state.advancedSearch.formData,
          factions: uniqBy(
            [...factions, { id: action.payload.id, name: action.payload.name, respect: action.payload.respect }],
            'id'
          )
        }
      }
    }
  },
  [actionTypes.REMOVE_ADVANCED_SEARCH_FACTION_FIELD]: (state, action) => {
    return {
      ...state,
      advancedSearch: {
        ...state.advancedSearch,
        formData: {
          ...state.advancedSearch.formData,
          factions: state.advancedSearch.formData[FACTIONS_FIELD_NAME].filter(item => item.id !== action.payload.id)
        }
      }
    }
  }
}

export default function headerReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
