/* eslint-disable @typescript-eslint/naming-convention */
import { createReducer, ActionType } from 'typesafe-actions'
import * as actions from '../actions/newsTicker'
import { getNewsTickerState } from '../utils'

type RootAction = ActionType<typeof actions>
type NesTickerStore = typeof initialState

const initialState = {
  headlines: [],
  activeHeadlineIndex: 0,
  isEnabled: getNewsTickerState()
}

const newsTickerReducer = createReducer<NesTickerStore, RootAction>(initialState)
  .handleAction(actions.setHeadlinesData, (state, action) => {
    const { headlines } = action.payload

    return {
      ...state,
      headlines: headlines ?? [],
      activeHeadlineIndex: 0
    }
  })
  .handleAction(actions.setActiveHeadlineIndex, (state, action) => {
    return {
      ...state,
      activeHeadlineIndex: action.payload.index
    }
  })

export default newsTickerReducer
