import { formatString } from './index'
import { checkRecordsForSimilarity } from './actionHelpers'
import { getCurrentTimeInSeconds } from './timeHelpers'
import * as c from '../constants/recentHistory'
import IAction from '../interfaces/recentHistory/IAction'
import IGroup from '../interfaces/recentHistory/IGroup'

export default class LogGroupsGenerator {
  log: IAction[]
  groupedLogs: IGroup[]
  currIndex: number
  amountOfSimilarActions: number
  logsGroup: IGroup
  constructor(log: IAction[], currIndex?: number) {
    this.log = log
    this.groupedLogs = []
    this.currIndex = currIndex || 0
    this.amountOfSimilarActions = 1
    this.logsGroup = {
      timeSeparator: null,
      log: []
    }
  }

  resetAmountOfSimilarActions = () => {
    this.amountOfSimilarActions = 1
  }

  increaseAmountOfSimilarActions = () => {
    this.amountOfSimilarActions += 1
  }

  addActionToGroup = (timeLessThanLimit, currAction) => {
    if (timeLessThanLimit) {
      this.logsGroup.log.push(currAction)
    }
  }

  unitSimilarActions = ({ index, amountHiddenSimilarActions, isLastRecordInLog, isSimilarActions }) => {
    const startIndexSimilarActions = index - amountHiddenSimilarActions
    const isLastSimilarRecordInGroup = isLastRecordInLog && isSimilarActions
    const amount = isLastSimilarRecordInGroup ? amountHiddenSimilarActions + 1 : amountHiddenSimilarActions
    const start = isLastSimilarRecordInGroup ? startIndexSimilarActions + 1 : startIndexSimilarActions
    const similarActions = this.logsGroup.log.splice(start, amount)
    const indexStorageOfSimilarActions = isLastSimilarRecordInGroup ?
      this.logsGroup.log.length - 1 :
      startIndexSimilarActions - 1

    this.logsGroup.log[indexStorageOfSimilarActions] = {
      ...this.logsGroup.log[indexStorageOfSimilarActions],
      similar: {
        open: false,
        log: similarActions
      }
    }
  }

  unitSimilarActionsInGroup = () => {
    for (let index = 0; index < this.logsGroup.log.length; index += 1) {
      const record = this.logsGroup.log[index]
      const prevRecord = index ? this.logsGroup.log[index - 1] : null
      const isSimilarActions = checkRecordsForSimilarity(record, prevRecord)
      const isLastRecordInLog = index === this.logsGroup.log.length - 1

      if (isSimilarActions) {
        this.increaseAmountOfSimilarActions()
      }

      if (!isSimilarActions || isLastRecordInLog) {
        if (this.amountOfSimilarActions >= c.MAX_OF_SIMILAR_ACTIONS) {
          const amountHiddenSimilarActions = this.amountOfSimilarActions - c.AMOUNT_OF_SIMILAR_DISPLAYED_ACTIONS

          this.unitSimilarActions({ index, amountHiddenSimilarActions, isLastRecordInLog, isSimilarActions })
          index -= amountHiddenSimilarActions
        }
        this.resetAmountOfSimilarActions()
      }
    }
  }

  createLogGroup = (timeSeparator, boundaryTime): any => {
    const currTime = getCurrentTimeInSeconds()

    this.logsGroup = {
      timeSeparator: timeSeparator,
      log: []
    }

    for (let logIndex = this.currIndex; logIndex < this.log.length; logIndex += 1) {
      const isLastLog = logIndex === this.log.length - 1
      const currAction = this.log[logIndex]
      const amountOfTimeAgo = currTime - currAction.time
      const timeLessThanLimit = amountOfTimeAgo < boundaryTime

      this.addActionToGroup(timeLessThanLimit, currAction)

      const needPushLogGroup = this.logsGroup.log.length && (!timeLessThanLimit || isLastLog)

      if (needPushLogGroup) {
        this.currIndex = !timeLessThanLimit ? logIndex : logIndex + 1
        this.unitSimilarActionsInGroup()
        this.groupedLogs.push(this.logsGroup)
        break
      }
    }
  }

  generateTimeSeparator = (number, label) => {
    return {
      number,
      label: formatString(number, label)
    }
  }

  groupLogsByTimeUnit = (boundaryUnitsAmount, secondsInOneUnit, label) => {
    for (let index = 2; index <= boundaryUnitsAmount; index += 1) {
      const unitsAmount = index - 1
      const secondsInUnit = index * secondsInOneUnit
      const timeSeparator = this.generateTimeSeparator(unitsAmount, label)

      this.createLogGroup(timeSeparator, secondsInUnit)
    }
  }

  groupLogByYears = (secondsInOneUnit, label) => {
    let index = 2

    while (this.currIndex <= this.log.length - 1) {
      const unitsAmount = index - 1
      const secondsInUnit = index * secondsInOneUnit
      const timeSeparator = this.generateTimeSeparator(unitsAmount, label)

      this.createLogGroup(timeSeparator, secondsInUnit)
      index += 1
    }
  }

  getGroupedLogs = () => {
    this.createLogGroup(null, c.SECONDS_IN_ONE_HOUR)
    this.groupLogsByTimeUnit(c.HOURS_IN_ONE_DAY, c.SECONDS_IN_ONE_HOUR, c.HOUR_LABEL)
    this.groupLogsByTimeUnit(c.DAYS_IN_ONE_MONTH, c.SECONDS_IN_ONE_DAY, c.DAY_LABEL)
    this.groupLogsByTimeUnit(c.MONTHS_IN_ONE_YEAR, c.SECONDS_IN_ONE_MONTH, c.MONTH_LABEL)
    this.groupLogByYears(c.SECONDS_IN_ONE_YEAR, c.YEAR_LABEL)
    return this.groupedLogs
  }
}
