export const sortLogByTime = log => {
  return log.sort((firstLog, secondLog) => secondLog.time - firstLog.time)
}

export const getLogsFromLastGroup = history => {
  const actionsFromLastGroup = []
  const lastGroup = history[history.length - 1]

  lastGroup.log.forEach(record => {
    if (record.similar) {
      const similarActions = record.similar.log

      actionsFromLastGroup.push({ ...record, similar: null })
      similarActions.forEach(similarRecord => {
        actionsFromLastGroup.push(similarRecord)
      })
    } else {
      actionsFromLastGroup.push(record)
    }
  })

  return actionsFromLastGroup
}
