import { getCookie } from '@torn/shared/utils'
import IWindow from '../interfaces/IWindow'
import {
  CONDITION_NOT_CHECKBOX,
  CONDITION_SELECTOR,
  EMPTY_DROPDOWN_VALUE,
  FACTIONS_FIELD_NAME
} from '../constants/advancedSearch'
import { MULTIJAIL_USER_STATUS } from '../constants'
import { NEWS_TICKER_IS_ENABLED } from '../constants/newsTicker'
import { setNewsTickerCookie } from './newsTickerHelper'
import IAdvancedSearchFormData, { IFactionAutocompleteValue, ISelectValue } from '../interfaces/IAdvancedSearchFormData'

export const isFederalJailed = userStatus => {
  return userStatus === MULTIJAIL_USER_STATUS
}

export const isDesktopView = mediaType => {
  return mediaType === 'desktop' || !document.body.classList.contains('r')
}

export const addEmptyItemToOptionList = list => {
  return [EMPTY_DROPDOWN_VALUE, ...list]
}

export const getDesktopModeState = () => {
  const sidebarStatesCookie = getCookie('sidebarStates')
  let desktopModeEnabled = false

  try {
    const sidebarStates = sidebarStatesCookie && JSON.parse(sidebarStatesCookie)

    desktopModeEnabled = sidebarStates && sidebarStates.isDesktopVersion
  } catch (e) {
    console.error(e)
  }

  return desktopModeEnabled
}

export const getDarkModeState = () => {
  const darkModeCookie = getCookie('darkModeEnabled')

  return darkModeCookie && darkModeCookie === 'true'
}

export const getNewsTickerState = () => {
  const newsTickerState = getCookie(NEWS_TICKER_IS_ENABLED) !== 'false'

  setNewsTickerCookie(newsTickerState)

  return newsTickerState
}

export const getTCClockState = () => {
  const TCClockCookie = getCookie('tcClockEnabled')

  return TCClockCookie && TCClockCookie === 'true'
}

export const formatNumber = num => {
  return num < 10 ? `0${num}` : num
}

export const getFormattedDateAndTime = () => {
  const win = window as IWindow
  const week = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  const time = win.getCurrentTimestamp()
  const date = new Date(time)
  const h = formatNumber(date.getUTCHours())
  const m = formatNumber(date.getUTCMinutes())
  const s = formatNumber(date.getUTCSeconds())
  const d = formatNumber(date.getUTCDate())
  const mth = formatNumber(date.getUTCMonth() + 1)
  const y = date.getUTCFullYear().toString().substr(2)

  return `${week[date.getUTCDay()]} ${h}:${m}:${s} - ${d}/${mth}/${y}`
}

export const getAdvancedSearchResult = (formData: IAdvancedSearchFormData) => {
  const str = Object.entries(formData)
    .map(([key, val]) => {
      let value = val

      if ((val as ISelectValue).id || (val as ISelectValue).id === '') {
        value = (val as ISelectValue).id
      }

      if (key === FACTIONS_FIELD_NAME) {
        if (
          (formData[CONDITION_NOT_CHECKBOX] && (formData[CONDITION_SELECTOR] as ISelectValue).id === 'inFaction')
          || !(val as IFactionAutocompleteValue[]).length
        ) {
          value = null
        } else {
          value = (val as IFactionAutocompleteValue[]).map(item => item.id)
        }
      }

      return value ? `${key}=${value}` : null
    })
    .filter(item => item)
    .join('&')

  window.location.href = `page.php?${str}`
}

export const isOutsideElement = (ref, target) => {
  if (!document.body.contains(target)) return false

  return !ref?.current?.contains(target)
}

export const formatString = (number, str) => {
  return number === 1 ? str : `${str}s`
}

export const getCursorCoords = e => {
  if (e.type === 'touchstart' || e.type === 'touchend' || e.type === 'touchmove') {
    const evt = e.originalEvent || e
    const touch = evt.touches[0] || evt.changedTouches[0]

    return {
      x: touch.pageX,
      y: touch.pageY
    }
  } else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove') {
    return {
      x: e.clientX,
      y: e.clientY
    }
  }
}
