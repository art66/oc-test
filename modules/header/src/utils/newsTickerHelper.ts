import { setCookie } from '@torn/shared/utils'
import {
  ACTIVE_HEADLINE,
  NEWS_TICKER_COOKIE_NAME,
  NEWS_TICKER_IS_ENABLED,
  COOKIE_EXPIRES_IN_DAYS
} from '../constants/newsTicker'

export const removeCookie = name => {
  setCookie(name, null, -1)
}

export const removeNewsTickerDataFromStorage = () => {
  try {
    localStorage.removeItem(ACTIVE_HEADLINE)
    removeCookie(NEWS_TICKER_COOKIE_NAME)
  } catch (e) {
    console.log(e)
  }
}

export const setNewsTickerCookie = newsTickerState => {
  setCookie(NEWS_TICKER_IS_ENABLED, `${newsTickerState}`, COOKIE_EXPIRES_IN_DAYS)
}

export const getCurrentTimestamp = () => Math.round(new Date().getTime() / 1000)
