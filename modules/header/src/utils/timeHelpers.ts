import IWindow from '../interfaces/IWindow'
import * as c from '../constants/recentHistory'
import { formatNumber, formatString } from './index'

export const getCurrentTimeInSeconds = () => {
  return (window as IWindow).getCurrentTimestamp() / 1000
}

const getPassedTime = (time, divider, label) => {
  return {
    timeValue: Math.floor(time / divider),
    label: label
  }
}

export const getFormattedPassedTime = time => {
  if (time < c.SECONDS_IN_ONE_MINUTE) {
    return getPassedTime(time, 1, c.SECONDS_TIMER_LABEL)
  }
  if (time < c.SECONDS_IN_ONE_HOUR) {
    return getPassedTime(time, c.SECONDS_IN_ONE_MINUTE, c.MINUTES_TIMER_LABEL)
  }
  if (time < c.SECONDS_IN_ONE_DAY) {
    return getPassedTime(time, c.SECONDS_IN_ONE_HOUR, c.HOURS_TIMER_LABEL)
  }
  if (time < c.SECONDS_IN_ONE_MONTH) {
    return getPassedTime(time, c.SECONDS_IN_ONE_DAY, c.DAYS_TIMER_LABEL)
  }
  if (time < c.SECONDS_IN_ONE_YEAR) {
    return getPassedTime(time, c.SECONDS_IN_ONE_MONTH, c.MONTHS_TIMER_LABEL)
  }

  return getPassedTime(time, c.SECONDS_IN_ONE_YEAR, c.YEARS_TIMER_LABEL)
}

const getActionTimeInObject = time => {
  const date = new Date(time * 1000)
  const day = formatNumber(date.getUTCDate())
  const month = formatNumber(date.getUTCMonth() + 1)
  const year = formatNumber(date.getUTCFullYear())
  const hours = formatNumber(date.getUTCHours())
  const minutes = formatNumber(date.getUTCMinutes())
  const seconds = formatNumber(date.getUTCSeconds())

  return {
    seconds,
    minutes,
    hours,
    year,
    month,
    day
  }
}

export const getActionTime = time => {
  const { seconds, minutes, hours, year, month, day } = getActionTimeInObject(time)

  return `${hours}:${minutes}:${seconds} - ${day}/${month}/${`${year}`.slice(-2)}`
}

export const getTimeSeparator = (passedTime, label, denominator) => {
  const number = Math.floor(passedTime / denominator)

  return {
    number,
    label: formatString(number, label)
  }
}
