import { getTimeSeparator } from './timeHelpers'
import * as c from '../constants/recentHistory'
import * as styleConst from '../constants/recentHistory/styles'

export const determineActionTimeSeparator = (passedTime) => {
  if (passedTime < c.SECONDS_IN_ONE_HOUR) {
    return null
  }
  if (passedTime < c.SECONDS_IN_ONE_DAY) {
    return getTimeSeparator(passedTime, c.HOUR_LABEL, c.SECONDS_IN_ONE_HOUR)
  }
  if (passedTime < c.SECONDS_IN_ONE_MONTH) {
    return getTimeSeparator(passedTime, c.DAY_LABEL, c.SECONDS_IN_ONE_DAY)
  }
  if (passedTime < c.SECONDS_IN_ONE_YEAR) {
    return getTimeSeparator(passedTime, c.MONTH_LABEL, c.SECONDS_IN_ONE_MONTH)
  }
  return getTimeSeparator(passedTime, c.YEAR_LABEL, c.SECONDS_IN_ONE_YEAR)
}

export const isEqualTimeSeparators = (firstTimeSeparator, secondTimeSeparator) => {
  return (
    firstTimeSeparator?.number === secondTimeSeparator?.number
    && firstTimeSeparator?.label === secondTimeSeparator?.label
  )
}

export const isExistingTimeSeparator = (history, timeSeparator) => {
  return history.some((group) => {
    return isEqualTimeSeparators(group.timeSeparator, timeSeparator)
  })
}

export const checkRecordsForSimilarity = (currAction, prevAction) => {
  return prevAction && currAction.category === prevAction.category
}

export const getActionOpacity = (timeSeparator) => {
  if (timeSeparator === null) {
    return styleConst.WITHIN_HOUR_OPACITY
  }
  if (timeSeparator.label.includes(c.HOUR_LABEL)) {
    return styleConst.WITHIN_DAY_OPACITY
  }
  if (timeSeparator.label.includes(c.DAY_LABEL)) {
    return styleConst.WITHIN_MONTH_OPACITY
  }
  if (timeSeparator.label.includes(c.MONTH_LABEL)) {
    return styleConst.WITHIN_YEAR_OPACITY
  }
  return styleConst.AFTER_YEAR_OPACITY
}
