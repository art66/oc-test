export const saveHeaderDataToStorage = (data) => {
  try {
    const strData = JSON.stringify({
      ...data,
      headlines: null
    })

    sessionStorage.setItem('headerData', strData)
  } catch (e) {
    console.error(e)
  }
}

export const getHeaderDataFromStorage = () => {
  try {
    const storageData = sessionStorage.getItem('headerData')

    return JSON.parse(storageData)
  } catch (e) {
    console.error(e)
  }

  return null
}

export const removeHeaderDataFromStorage = () => {
  try {
    sessionStorage.removeItem('headerData')
  } catch (e) {
    console.error(e)
  }
}
