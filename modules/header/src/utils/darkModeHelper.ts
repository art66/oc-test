import { setCookie } from '@torn/shared/utils'
import { DARK_MODE_CLASS_NAME } from '../constants'

export const handleToggleDarkMode = darkModeEnabled => {
  const expiresInDays = 3650
  const celebration = document.body.getAttribute('data-celebration')
  const country = document.body.getAttribute('data-country')

  setCookie('darkModeEnabled', `${!darkModeEnabled}`, expiresInDays) // will store for 10 years

  if (darkModeEnabled) {
    document.body.classList.remove(DARK_MODE_CLASS_NAME)
    document.body.removeAttribute('data-dark-mode-logo')
  } else {
    document.body.classList.add(DARK_MODE_CLASS_NAME)

    if (celebration === 'none' && country === 'torn') {
      document.body.setAttribute('data-dark-mode-logo', 'regular')
    }
  }
}
