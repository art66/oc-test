import { sortLogByTime } from './historyHelpers'
import { isEqualTimeSeparators, isExistingTimeSeparator, checkRecordsForSimilarity } from './actionHelpers'
import IAction from '../interfaces/recentHistory/IAction'
import ITimeSeparator from '../interfaces/recentHistory/ITimeSeparator'
import IGroup from '../interfaces/recentHistory/IGroup'
import {
  AMOUNT_OF_SIMILAR_DISPLAYED_ACTIONS,
  MAX_OF_SIMILAR_ACTIONS,
  MIN_OF_HIDDEN_SIMILAR_ACTIONS
} from '../constants/recentHistory'

export default class LogHelper {
  replacedAction: IAction
  newHistory: IGroup[]
  timeSeparatorExisted: boolean
  oldTimeSeparator: ITimeSeparator
  newTimeSeparator: ITimeSeparator
  actionId: string
  parentId: string | undefined
  history: IGroup[]
  constructor(history, { oldTimeSeparator, newTimeSeparator, actionId, parentId }) {
    this.oldTimeSeparator = oldTimeSeparator
    this.newTimeSeparator = newTimeSeparator
    this.actionId = actionId
    this.parentId = parentId
    this.history = history
    this.timeSeparatorExisted = isExistingTimeSeparator(history, newTimeSeparator)
    this.newHistory = []
    this.replacedAction = null
  }

  filterListOfActions = (log) => {
    return log.filter((record) => {
      const isNotEqualIds = record.ID !== this.actionId

      if (!isNotEqualIds) {
        this.replacedAction = record
      }

      return isNotEqualIds
    })
  }

  filterLog = (item) => {
    const filteredLog = this.filterListOfActions(item.log)

    if (filteredLog.length) {
      this.newHistory.push({ ...item, log: filteredLog })
    }
  }

  filterSimilarActions = (item) => {
    let hiddenActions = null
    let indexForAddingEls = null
    const changedLog = item.log.map((record, index) => {
      if (record.ID === this.parentId) {
        if (record.similar?.log?.length > MIN_OF_HIDDEN_SIMILAR_ACTIONS) {
          return {
            ...record,
            similar: {
              ...record.similar,
              log: this.filterListOfActions(record.similar.log)
            }
          }
        }
        indexForAddingEls = index
        hiddenActions = this.filterListOfActions(record.similar.log)

        return { ...record, similar: null }
      }
      return record
    })

    if (hiddenActions && indexForAddingEls) {
      changedLog.splice(indexForAddingEls + 1, 0, ...hiddenActions)
    }

    if (changedLog.length) {
      this.newHistory.push({ ...item, log: changedLog })
    }
  }

  modifyLogGroup = (item) => {
    const group = {
      ...item,
      log: this.replacedAction ? sortLogByTime([this.replacedAction, ...item.log]) : item.log
    }

    this.newHistory.push(this.recalculateSimilarActionsInGroup(group))
  }

  getChangedStorageOfSimilarActions = (action, similar, log) => {
    return {
      ...action,
      similar: {
        ...similar,
        log
      }
    }
  }

  getOptionsForGettingHiddenActions = (isLastActionInGroup, index, amountSimilarActions) => {
    const lastSimilarActionIndex = isLastActionInGroup ? index : index - 1
    const amountHiddenActions = amountSimilarActions - AMOUNT_OF_SIMILAR_DISPLAYED_ACTIONS
    const indexOfNewStorage = lastSimilarActionIndex - amountHiddenActions

    return { lastSimilarActionIndex, amountHiddenActions, indexOfNewStorage }
  }

  updateSimilarGroup = (log, options) => {
    const changedLog = log
    const { prevAction, amountSimilarActions, indexOfNewStorage, lastSimilarActionIndex, amountHiddenActions } = options
    const needUpdateSimilarActions = prevAction?.similar && amountSimilarActions > AMOUNT_OF_SIMILAR_DISPLAYED_ACTIONS
    const needUnitSimilarActions = !prevAction?.similar && amountSimilarActions >= MAX_OF_SIMILAR_ACTIONS

    if (needUpdateSimilarActions) {
      changedLog[lastSimilarActionIndex] = { ...prevAction, similar: null }

      const hiddenActions = changedLog.splice(indexOfNewStorage + 1, amountHiddenActions)
      const updatedSimilarActions = sortLogByTime([...hiddenActions, ...prevAction.similar.log])

      changedLog[indexOfNewStorage] = this.getChangedStorageOfSimilarActions(
        changedLog[indexOfNewStorage],
        prevAction.similar,
        updatedSimilarActions
      )

      return { changedLog, amountHiddenActions: hiddenActions.length }
    } else if (needUnitSimilarActions) {
      const hiddenActions = changedLog.splice(indexOfNewStorage + 1, amountHiddenActions)

      changedLog[indexOfNewStorage] = this.getChangedStorageOfSimilarActions(
        changedLog[indexOfNewStorage],
        { open: false },
        hiddenActions
      )

      return { changedLog, amountHiddenActions: hiddenActions.length }
    }

    return { changedLog, amountHiddenActions: 0 }
  }

  getUpdatedAmountOfSimilarActions = (isSimilarActions, amountSimilarActions) => {
    return isSimilarActions ? amountSimilarActions + 1 : amountSimilarActions
  }

  recalculateSimilarActionsInGroup = (group) => {
    let log = [...group.log]
    let amountSimilarActions = 1

    for (let index = 0; index < log.length; index++) {
      const prevAction = index ? log[index - 1] : null
      const currAction = log[index]
      const isSimilarActions = checkRecordsForSimilarity(currAction, prevAction)
      const isLastActionInGroup = index === log.length - 1

      amountSimilarActions = this.getUpdatedAmountOfSimilarActions(isSimilarActions, amountSimilarActions)

      if (!isSimilarActions || isLastActionInGroup) {
        const options = {
          prevAction,
          amountSimilarActions,
          ...this.getOptionsForGettingHiddenActions(isLastActionInGroup, index, amountSimilarActions)
        }
        const { changedLog, amountHiddenActions: amount } = this.updateSimilarGroup(log, options)

        log = changedLog
        index -= amount
        amountSimilarActions = 1
      }
    }

    return { ...group, log }
  }

  addActionToNewGroup = () => {
    if (!this.timeSeparatorExisted && this.replacedAction) {
      this.newHistory.push({ timeSeparator: this.newTimeSeparator, log: [this.replacedAction] })
    }
  }

  recalculateHistory = () => {
    for (let groupIndex = 0; groupIndex < this.history.length; groupIndex += 1) {
      const item = this.history[groupIndex]
      const { timeSeparator } = item
      const moveActionToExistedGroup =
        this.timeSeparatorExisted && isEqualTimeSeparators(this.newTimeSeparator, timeSeparator)
      const removeActionFromGroup = isEqualTimeSeparators(this.oldTimeSeparator, timeSeparator)

      if (removeActionFromGroup) {
        this.parentId ? this.filterSimilarActions(item) : this.filterLog(item)
        this.addActionToNewGroup()
      } else if (moveActionToExistedGroup) {
        this.modifyLogGroup(item)
      } else {
        this.newHistory.push(item)
      }
    }

    return this.newHistory
  }
}
