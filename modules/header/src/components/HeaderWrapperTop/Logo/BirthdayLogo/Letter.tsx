import React, { Component } from 'react'
import ILetter from '../../../../interfaces/ILetter'
import IWindow from '../../../../interfaces/IWindow'

interface IProps {
  letterData: ILetter
  getBirthdayBonuses: (letter: string) => void
  setEatLetter: (letter: string) => void
  setIsEatingLetter: (isEatingLetter: boolean) => void
  isEating: boolean
  eatLetter: string
}

interface IState {
  isLetterClicked: boolean
}

class Letter extends Component<IProps, IState> {
  timerOut: any

  constructor(props: IProps) {
    super(props)

    this.state = {
      isLetterClicked: false
    }
  }

  componentDidUpdate(prevProps: Readonly<IProps>): void {
    const { isEating } = this.props

    if (prevProps.isEating !== isEating && isEating) {
      this.startAnimation()
    }
  }

  componentWillUnmount(): void {
    clearTimeout(this.timerOut)
  }

  startAnimation = () => {
    const { letterData, eatLetter, setEatLetter, setIsEatingLetter } = this.props

    if (letterData.name.toLowerCase() === eatLetter) {
      this.setState({
        isLetterClicked: true
      })

      this.timerOut = setTimeout(() => {
        this.setState({
          isLetterClicked: false
        })
      }, 1000)

      setEatLetter(null)
      setIsEatingLetter(false)
    }
  }

  _handleGetBonuses = e => {
    e.preventDefault()
    const { getBirthdayBonuses, letterData } = this.props
    const win = window as IWindow
    const currentTime = win.getCurrentTimestamp() / 1000 - 1
    const letterName = letterData.name.toLowerCase()

    if (letterData.used >= 10 || letterData.used < 0) {
      return
    }

    if (currentTime >= letterData.timeout) {
      getBirthdayBonuses(letterName)
    }
  }

  render() {
    const {
      letterData: { name, message, used, timeout }
    } = this.props
    const { isLetterClicked } = this.state
    const letterName = name.toLowerCase()
    const bonusStyle = isLetterClicked ? { display: 'block' } : {}

    return (
      // eslint-disable-next-line jsx-a11y/click-events-have-key-events
      <div
        className={`${letterName}-letter ${isLetterClicked ? 'scaled' : ''}`}
        data-letter={letterName}
        onClick={this._handleGetBonuses}
        onTouchEnd={this._handleGetBonuses}
      >
        <span className={`letter ${letterName}-used-${used}`} letter-timeout={timeout} letter-used={used} />
        <span className={`bonus ${isLetterClicked ? 'appeared' : ''}`} style={bonusStyle}>
          {message}
        </span>
      </div>
    )
  }
}

export default Letter
