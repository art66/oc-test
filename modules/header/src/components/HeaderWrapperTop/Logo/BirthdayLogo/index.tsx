import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { getBirthdayBonuses, setEatLetter, setIsEatingLetter } from '../../../../actions'
import IBirthdayLogo from '../../../../interfaces/IBirthdayLogo'
import Letter from './Letter'

interface IProps {
  additionalData: IBirthdayLogo
  getBirthdayBonuses: (letter: string) => void
  setEatLetter: (letter: string) => void
  setIsEatingLetter: (isEatingLetter: boolean) => void
  isUnscaled: boolean
}

class BirthdayLogo extends Component<IProps> {
  renderLetters = () => {
    const {
      additionalData: { letters, isEating, eatLetter },
      getBirthdayBonuses: getBonuses,
      setEatLetter: setLetter,
      setIsEatingLetter: setIsEating
    } = this.props

    return letters.map(item => {
      return (
        <Letter
          key={item.name}
          letterData={item}
          eatLetter={eatLetter}
          isEating={isEating}
          getBirthdayBonuses={getBonuses}
          setEatLetter={setLetter}
          setIsEatingLetter={setIsEating}
        />
      )
    })
  }

  render() {
    const {
      additionalData: { anniversary },
      isUnscaled
    } = this.props
    const letterStyles = cn({
      letters: true,
      unscaled: isUnscaled
    })

    return (
      <a aria-label='Index page' className={letterStyles} href='/'>
        {this.renderLetters()}
        <span className='ribbon'>{anniversary}th YEAR ANNIVERSARY</span>
        <span className='bonus wait' />
      </a>
    )
  }
}

const mapDispatchToProps = {
  getBirthdayBonuses,
  setEatLetter,
  setIsEatingLetter
}

export default connect(null, mapDispatchToProps)(BirthdayLogo)
