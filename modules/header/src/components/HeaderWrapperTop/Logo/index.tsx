import React, { PureComponent } from 'react'
import { LOGGED_OUT_USER_STATUS } from '../../../constants'
import IUser from '../../../interfaces/IUser'
import ILogo from '../../../interfaces/ILogo'
import ISettings from '../../../interfaces/ISettings'
import BirthdayLogo from './BirthdayLogo'

interface IProps {
  user: IUser
  logo: ILogo
  settings: ISettings
}

interface IState {
  isUnscaled: boolean
}

class Logo extends PureComponent<IProps, IState> {
  logoRef: HTMLDivElement

  constructor(props: IProps) {
    super(props)

    this.state = {
      isUnscaled: false
    }
  }

  getLogoTitle = () => {
    const {
      user: {
        state: { status }
      },
      logo: { title, name },
      settings: { emptyLinks }
    } = this.props
    const isLoggedOut = status === LOGGED_OUT_USER_STATUS
    const titleExisted = !isLoggedOut && title
    const isNotBirthdayLogo = name !== 'torn-birthday'
    const isTitleEnabled = titleExisted && (emptyLinks || (!emptyLinks && isNotBirthdayLogo))

    return isTitleEnabled ? { title } : null
  }

  _handleClick = () => {
    const {
      logo: { name }
    } = this.props
    const { isUnscaled } = this.state

    if (name === 'torn-birthday') {
      if (!isUnscaled) {
        document.addEventListener('click', this._handleOutsideClick, false)
        this.setState(() => ({
          isUnscaled: !isUnscaled
        }))
      }
    }
  }

  _handleOutsideClick = (e) => {
    const { isUnscaled } = this.state

    if (!this.logoRef.contains(e.target)) {
      this.setState(() => ({
        isUnscaled: !isUnscaled
      }))
      document.removeEventListener('click', this._handleOutsideClick, false)
    }
  }

  renderBirthdayLogo = () => {
    const { isUnscaled } = this.state
    const {
      logo: { additionalData }
    } = this.props

    return <BirthdayLogo additionalData={additionalData} isUnscaled={isUnscaled} />
  }

  renderLogo = () => {
    const {
      user: {
        state: { isLoggedIn }
      },
      logo: { name }
    } = this.props

    const isBirthdayLogo = name === 'torn-birthday'

    return isLoggedIn && isBirthdayLogo ? this.renderBirthdayLogo() : <a aria-label='Index page' href='/' />
  }

  render() {
    return (
      // eslint-disable-next-line jsx-a11y/click-events-have-key-events
      <div
        id='tcLogo'
        itemProp='image'
        className='logo'
        role='banner'
        ref={(el) => (this.logoRef = el)}
        onFocus={this._handleClick}
        onClick={this._handleClick}
        {...this.getLogoTitle()}
      >
        {this.renderLogo()}
        <h1>Text based RPG - TORN</h1>
      </div>
    )
  }
}

export default Logo
