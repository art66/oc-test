import React, { PureComponent } from 'react'
import GlobalSearch from './GlobalSearch'
import AdvancedSearch from './AdvancedSearch'
import TCCLock from './TCClock'
import RightMenu from './RightMenu'
import RecentHistory from './RecentHistory'
import LogoutButton from './LogoutButton'
import { isFederalJailed } from '../../../utils'
import IGlobalSearch from '../../../interfaces/IGlobalSearch'
import IUser from '../../../interfaces/IUser'

interface IProps {
  user: IUser
  globalSearch: IGlobalSearch
}

class NavigationToolbar extends PureComponent<IProps> {
  renderToolbarContent = () => {
    const {
      user: {
        data,
        state: { status, isDonator }
      },
      globalSearch
    } = this.props
    const isFedJailed = isFederalJailed(status)

    return !isFedJailed ? (
      <>
        <GlobalSearch globalSearch={globalSearch} />
        {isDonator ? <AdvancedSearch /> : null}
        <TCCLock />
        <RecentHistory />
        <RightMenu userData={data} />
      </>
    ) : (
      <LogoutButton logoutHash={data.logoutHash} />
    )
  }

  render() {
    return (
      <div className='header-buttons-wrapper'>
        <ul className='toolbar clearfix'>{this.renderToolbarContent()}</ul>
      </div>
    )
  }
}

export default NavigationToolbar
