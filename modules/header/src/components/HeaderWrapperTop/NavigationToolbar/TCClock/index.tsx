import React, { PureComponent, RefObject } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import headerIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/header'
import { isDesktopView } from '../../../../utils'
import { DEFAULT_PRESET, TC_CLOCK_ICON_DIMENSIONS } from '../../../../constants/icons'
import { toggleTCClock } from '../../../../actions'
import IWindow from '../../../../interfaces/IWindow'
import ClockTooltip from './ClockTooltip'

const INITIAL_TOP_POSITION_DESKTOP = 39
const INITIAL_TOP_POSITION_TABLET_MOBILE = 34

interface IProps {
  TCClock: {
    open: boolean
  }
  browser: {
    mediaType: string
  }
  toggleTCClock: (open: boolean) => void
}

interface IState {
  isSticky: boolean
  right: null | number
}

class TCCLock extends PureComponent<IProps, IState> {
  clockTooltip: RefObject<HTMLDivElement>
  clockWrapper: RefObject<HTMLLIElement>
  rightOffsetDiff: number
  constructor(props: IProps) {
    super(props)
    this.state = {
      isSticky: false,
      right: null
    }
    this.clockTooltip = React.createRef()
    this.clockWrapper = React.createRef()
  }

  componentDidMount() {
    this.calculateRightOffsetDiff()
    this.setTooltipPosition()
    window.addEventListener('scroll', this.setTooltipPosition)
    window.addEventListener('resize', this.setTooltipPosition)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.setTooltipPosition)
    window.removeEventListener('resize', this.setTooltipPosition)
  }

  getRightOffset = element => {
    let left = 0
    let width = 0

    if (element) {
      const rect = element.getBoundingClientRect()

      left = rect.left
      width = rect.width
    }

    return document.documentElement.clientWidth - (left + width)
  }

  calculateRightOffsetDiff = () => {
    const tcClockTooltip = this.clockTooltip.current
    const tcClockWrapper = this.clockWrapper.current
    const clockWrapperRightOffset = this.getRightOffset(tcClockWrapper)
    const tcClockTooltipRightOffset = this.getRightOffset(tcClockTooltip)

    this.rightOffsetDiff = clockWrapperRightOffset - tcClockTooltipRightOffset
  }

  setTooltipPosition = () => {
    const {
      browser: { mediaType }
    } = this.props
    const tcClockWrapper = this.clockWrapper.current
    const clockWrapperRightOffset = this.getRightOffset(tcClockWrapper)
    const scrollTop = window.pageYOffset
    const isDesktop = isDesktopView(mediaType)
    const isMobileView = mediaType === 'mobile' && !isDesktop
    const initialTopPosition = isDesktop ? INITIAL_TOP_POSITION_DESKTOP : INITIAL_TOP_POSITION_TABLET_MOBILE

    if (scrollTop > initialTopPosition) {
      if (isMobileView) {
        this.updatePosition(true, null)
      } else {
        const right = clockWrapperRightOffset - this.rightOffsetDiff

        this.updatePosition(true, right)
      }
    } else {
      this.updatePosition(false, null)
    }
  }

  updatePosition = (isSticky, right) => {
    this.setState({
      isSticky,
      right
    })
  }

  _handleToggleTCClock = () => {
    const {
      TCClock: { open },
      toggleTCClock: toggleClock
    } = this.props
    const win = window as IWindow
    const week = 7

    win.setCookie('tcClockEnabled', !open, week)
    toggleClock(!open)
  }

  render() {
    const {
      TCClock: { open }
    } = this.props
    const { right, isSticky } = this.state

    return (
      <li ref={this.clockWrapper} className={cn('tc-clock', { active: open, sticky: isSticky })}>
        <button
          type="button"
          className="top_header_button button tc_clock"
          onClick={this._handleToggleTCClock}
          aria-label={`${open ? 'Close' : 'Open'} TC clock`}
        >
          <SVGIconGenerator
            iconsHolder={headerIconsHolder}
            iconName="Clock"
            preset={DEFAULT_PRESET}
            dimensions={TC_CLOCK_ICON_DIMENSIONS}
          />
        </button>
        <ClockTooltip open={open} ref={this.clockTooltip} right={right} isSticky={isSticky} />
      </li>
    )
  }
}

export default connect(
  (state: any) => ({
    TCClock: state.header.TCClock,
    browser: state.browser
  }),
  {
    toggleTCClock
  }
)(TCCLock)
