import React, { Component } from 'react'
import cn from 'classnames'
import s from './index.cssmodule.scss'
import { getFormattedDateAndTime } from '../../../../utils'

interface IProps {
  right: null | number
  isSticky: boolean
  open: boolean
  tipRef: React.Ref<HTMLDivElement>
}

interface IPrivateProps {
  right: null | number
  isSticky: boolean
  open: boolean
}

interface IState {
  formattedDateTime: string
}

class ClockTooltip extends Component<IProps, IState> {
  timer: any
  constructor(props: IProps) {
    super(props)
    this.state = {
      formattedDateTime: getFormattedDateAndTime()
    }
  }

  componentDidMount() {
    this.timer = setInterval(this.updateDateTime, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  updateDateTime = () => {
    this.setState({
      formattedDateTime: getFormattedDateAndTime()
    })
  }

  render() {
    const { formattedDateTime } = this.state
    const { right, tipRef, open } = this.props
    const tipStyles = right ? { right: `${right}px` } : {}

    return (
      <div ref={tipRef} className={cn('tc-clock-tooltip', { [s.hide]: !open })} style={tipStyles}>
        <p>
          <span className="server-date-time">{formattedDateTime}</span>
        </p>
      </div>
    )
  }
}

export default React.forwardRef<HTMLDivElement, IPrivateProps>((props, ref) => <ClockTooltip tipRef={ref} {...props} />)
