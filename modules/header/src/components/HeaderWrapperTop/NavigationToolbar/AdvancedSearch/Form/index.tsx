import React, { Component } from 'react'
import { getCookie } from '@torn/shared/utils'
import { addEmptyItemToOptionList, getAdvancedSearchResult } from '../../../../../utils'
import * as c from '../../../../../constants/advancedSearch'
import { SEARCH_FACTION_OPTION } from '../../../../../constants'
import CommonInputField from '../CommonInputField'
import OptionSelector from '../OptionSelector'
import Range from '../Range'
import AutocompleteInputField from '../AutocompleteInputField'
import IAdvancedSearch from '../../../../../interfaces/IAdvancedSearch'
import IFaction from '../../../../../interfaces/globalSearch/IFaction'
import s from '../index.cssmodule.scss'

interface IProps {
  advancedSearch: IAdvancedSearch
  updateAdvancedSearchFormField: (fieldName: string, value: any) => void
  autocompleteSearchFaction: (query: string, option: string) => void
  setAdvancedSearchAutocompleteResultList: (resultList: IFaction[]) => void
  resetAdvancedSearchForm: () => void
  addAdvancedSearchFactionField: (id: number, name: string, respect: string) => void
  removeAdvancedSearchFactionField: (id: number) => void
}

class Form extends Component<IProps> {
  factionTimeout: any
  getPropsForOptionSelector = key => {
    const {
      advancedSearch: {
        optionsList: { gender, properties, searchConditions, lastAction },
        formData
      }
    } = this.props
    const searchConditionNot = formData && formData[c.CONDITION_NOT_CHECKBOX]

    switch (key) {
      case c.GENDER_SELECTOR:
        return {
          label: 'Gender',
          dropdown: { list: addEmptyItemToOptionList(gender), name: c.GENDER_SELECTOR }
        }
      case c.PROPERTY_SELECTOR:
        return {
          label: 'Property',
          dropdown: { list: addEmptyItemToOptionList(properties), name: c.PROPERTY_SELECTOR }
        }
      case c.CONDITION_SELECTOR:
        return {
          label: 'Condition',
          dropdown: {
            list: addEmptyItemToOptionList(searchConditions),
            name: c.CONDITION_SELECTOR,
            className: s.searchConditionsDropdown
          },
          checkbox: {
            id: 'search-condition-not',
            name: c.CONDITION_NOT_CHECKBOX,
            label: 'Not',
            value: searchConditionNot
          }
        }
      case c.LAST_ACTION_SELECTOR:
        return {
          label: 'Last action',
          dropdown: { list: addEmptyItemToOptionList(lastAction), name: c.LAST_ACTION_SELECTOR }
        }
      default:
        return null
    }
  }

  handleSelectFaction = selected => {
    const { updateAdvancedSearchFormField } = this.props

    updateAdvancedSearchFormField(c.FACTION_AUTOCOMPLETE, '')
    this.props.addAdvancedSearchFactionField(selected.id, selected.name, selected.respect)
  }

  handleFocusInFactionAutocomplete = () => {
    const { setAdvancedSearchAutocompleteResultList } = this.props

    setAdvancedSearchAutocompleteResultList([])
  }

  _handleFormSubmit = e => {
    e.preventDefault()
    const {
      advancedSearch: { formData }
    } = this.props
    const userID = getCookie('uid')

    if (formData[c.FACTION_AUTOCOMPLETE] && !formData[c.FACTION_ID_AUTOCOMPLETE]) {
      return
    }

    try {
      const formDataToSave = JSON.stringify(formData)

      localStorage.setItem(`advancedSearchFormData_${userID}`, formDataToSave)
    } catch (err) {
      console.error(err)
    }

    getAdvancedSearchResult(formData)
  }

  _handleFormReset = () => {
    const { resetAdvancedSearchForm } = this.props
    const userID = getCookie('uid')

    try {
      localStorage.removeItem(`advancedSearchFormData_${userID}`)
    } catch (err) {
      console.error(err)
    }
    resetAdvancedSearchForm()
  }

  validateNumberInput = e => {
    const regex = /[^0-9]/g

    return regex.test(e.target.value)
  }

  validateTextInput = e => {
    const regex = /[\u0400-\u04FF]/g

    return regex.test(e.target.value)
  }

  updatePlayerNameValue = e => {
    const { updateAdvancedSearchFormField } = this.props
    const disableUpdateField = this.validateTextInput(e)

    if (!disableUpdateField) {
      updateAdvancedSearchFormField(c.PLAYER_NAME_INPUT, e.target.value)
    }
  }

  updateFactionNameValue = e => {
    const { autocompleteSearchFaction, updateAdvancedSearchFormField } = this.props
    const inputValue = e.target.value

    updateAdvancedSearchFormField(c.FACTION_AUTOCOMPLETE, inputValue)

    if (!inputValue) {
      updateAdvancedSearchFormField(c.FACTION_ID_AUTOCOMPLETE, '')
    }
    clearTimeout(this.factionTimeout)
    this.factionTimeout = setTimeout(() => {
      autocompleteSearchFaction(inputValue, SEARCH_FACTION_OPTION)
    }, 100)
  }

  updateGenderValue = nextValue => {
    const { updateAdvancedSearchFormField } = this.props

    updateAdvancedSearchFormField(c.GENDER_SELECTOR, nextValue)
  }

  updatePropertyValue = nextValue => {
    const { updateAdvancedSearchFormField } = this.props

    updateAdvancedSearchFormField(c.PROPERTY_SELECTOR, nextValue)
  }

  updateConditionValue = nextValue => {
    const { updateAdvancedSearchFormField } = this.props

    updateAdvancedSearchFormField(c.CONDITION_SELECTOR, nextValue)
  }

  updateSearchConditionCheckboxValue = () => {
    const {
      advancedSearch: { formData },
      updateAdvancedSearchFormField
    } = this.props

    updateAdvancedSearchFormField(c.CONDITION_NOT_CHECKBOX, !formData[c.CONDITION_NOT_CHECKBOX])
  }

  updateLevelFromValue = e => {
    const { updateAdvancedSearchFormField } = this.props
    const disableUpdateField = this.validateNumberInput(e)

    if (!disableUpdateField) {
      updateAdvancedSearchFormField(c.LEVEL_FROM_INPUT, e.target.value)
    }
  }

  updateLevelToValue = e => {
    const { updateAdvancedSearchFormField } = this.props
    const disableUpdateField = this.validateNumberInput(e)

    if (!disableUpdateField) {
      updateAdvancedSearchFormField(c.LEVEL_TO_INPUT, e.target.value)
    }
  }

  updateDaysOldFromValue = e => {
    const { updateAdvancedSearchFormField: updateFormField } = this.props
    const disableUpdateField = this.validateNumberInput(e)

    if (!disableUpdateField) {
      updateFormField(c.DAYS_OLD_FROM_INPUT, e.target.value)
    }
  }

  updateDaysOldToValue = e => {
    const { updateAdvancedSearchFormField } = this.props
    const disableUpdateField = this.validateNumberInput(e)

    if (!disableUpdateField) {
      updateAdvancedSearchFormField(c.DAYS_OLD_TO_INPUT, e.target.value)
    }
  }

  updateOffencesFromValue = e => {
    const { updateAdvancedSearchFormField } = this.props
    const disableUpdateField = this.validateNumberInput(e)

    if (!disableUpdateField) {
      updateAdvancedSearchFormField(c.OFFENCES_FROM_INPUT, e.target.value)
    }
  }

  updateOffencesToValue = e => {
    const { updateAdvancedSearchFormField } = this.props
    const disableUpdateField = this.validateNumberInput(e)

    if (!disableUpdateField) {
      updateAdvancedSearchFormField(c.OFFENCES_TO_INPUT, e.target.value)
    }
  }

  updateLastActionValue = nextValue => {
    const { updateAdvancedSearchFormField } = this.props

    updateAdvancedSearchFormField(c.LAST_ACTION_SELECTOR, nextValue)
  }

  renderFactionAutocomplete = () => {
    const {
      advancedSearch: { formData, factionsResultList }
    } = this.props
    const { searchConditionNot, searchConditions } = formData

    return !searchConditionNot || searchConditions?.id !== 'inFaction' ? (
      <AutocompleteInputField
        label='Faction'
        name={c.FACTION_AUTOCOMPLETE}
        idFieldName={c.FACTION_ID_AUTOCOMPLETE}
        idFieldValue={formData[c.FACTION_ID_AUTOCOMPLETE] || ''}
        value={formData[c.FACTION_AUTOCOMPLETE] || ''}
        list={factionsResultList || []}
        searchType={SEARCH_FACTION_OPTION}
        wrapperClassName='factionField'
        handleInput={this.updateFactionNameValue}
        handleSelectItem={this.handleSelectFaction}
        handleFocus={this.handleFocusInFactionAutocomplete}
        factions={formData[c.FACTIONS_FIELD_NAME]}
        removeAdvancedSearchFactionField={this.props.removeAdvancedSearchFactionField}
      />
    ) : null
  }

  render() {
    const {
      advancedSearch: { formData }
    } = this.props

    return (
      <form action='page.php' method='GET' className='form-search-extend' onSubmit={this._handleFormSubmit}>
        <fieldset>
          <legend className='title'>Search users by...</legend>
          <input type='hidden' name={c.SID_INPUT} value={formData[c.SID_INPUT]} />
          <div className='line' />
          <ul className='advancedSearchFormBody clearfix'>
            <CommonInputField
              id='search-playername'
              label='Name'
              name={c.PLAYER_NAME_INPUT}
              handleInput={this.updatePlayerNameValue}
              value={formData[c.PLAYER_NAME_INPUT] || ''}
            />
            {this.renderFactionAutocomplete()}
            <OptionSelector
              {...this.getPropsForOptionSelector(c.GENDER_SELECTOR)}
              handleChangeOption={this.updateGenderValue}
              selected={formData[c.GENDER_SELECTOR] || c.EMPTY_DROPDOWN_VALUE}
            />
            <OptionSelector
              {...this.getPropsForOptionSelector(c.PROPERTY_SELECTOR)}
              handleChangeOption={this.updatePropertyValue}
              selected={formData[c.PROPERTY_SELECTOR] || c.EMPTY_DROPDOWN_VALUE}
            />
            <OptionSelector
              {...this.getPropsForOptionSelector(c.CONDITION_SELECTOR)}
              handleChangeOption={this.updateConditionValue}
              handleChangeCheckbox={this.updateSearchConditionCheckboxValue}
              selected={formData[c.CONDITION_SELECTOR] || c.EMPTY_DROPDOWN_VALUE}
            />
            <Range
              {...c.PROPS_FOR_RANGES.level}
              handleChangeFromValue={this.updateLevelFromValue}
              handleChangeToVale={this.updateLevelToValue}
            />
            <Range
              {...c.PROPS_FOR_RANGES.daysOld}
              handleChangeFromValue={this.updateDaysOldFromValue}
              handleChangeToVale={this.updateDaysOldToValue}
            />
            <Range
              {...c.PROPS_FOR_RANGES.offences}
              handleChangeFromValue={this.updateOffencesFromValue}
              handleChangeToVale={this.updateOffencesToValue}
            />
            <OptionSelector
              {...this.getPropsForOptionSelector(c.LAST_ACTION_SELECTOR)}
              handleChangeOption={this.updateLastActionValue}
              selected={formData[c.LAST_ACTION_SELECTOR] || c.EMPTY_DROPDOWN_VALUE}
            />
          </ul>
          <div className='line' />
          <div className='bottom'>
            <button type='button' className={`btn reset ${s.resetBtn}`} onClick={this._handleFormReset}>
              Reset
            </button>
            <button className='torn-btn' type='submit'>
              Search
            </button>
          </div>
        </fieldset>
      </form>
    )
  }
}

export default Form
