import React, { ChangeEvent, PureComponent } from 'react'

interface IProps {
  id: string
  name: string
  label: string
  handleInput: (e: ChangeEvent) => void
  value: string
}

class CommonInputField extends PureComponent<IProps> {
  render() {
    const { id, name, label, value, handleInput } = this.props

    return (
      <li>
        <label htmlFor={id} className="label">
          {label}
        </label>
        <div className="input-wrapper right">
          <input id={id} name={name} type="text" value={value} autoComplete="off" onChange={handleInput} />
        </div>
      </li>
    )
  }
}

export default CommonInputField
