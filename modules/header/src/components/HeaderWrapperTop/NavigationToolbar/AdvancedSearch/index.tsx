import React, { Component, RefObject } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { getCookie } from '@torn/shared/utils'
import { SVGIconGenerator } from '@torn/shared/SVG'
import headerIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/header'
import {
  autocompleteSearchFaction,
  toggleAdvancedSearch,
  updateAdvancedSearchFormField,
  resetAdvancedSearchForm,
  setAdvancedSearchFormData,
  setAdvancedSearchAutocompleteResultList,
  addAdvancedSearchFactionField,
  removeAdvancedSearchFactionField
} from '../../../../actions'
import { ADVANCED_SEARCH_ICON_DIMENSIONS, DEFAULT_PRESET } from '../../../../constants/icons'
import IAdvancedSearch from '../../../../interfaces/IAdvancedSearch'
import Form from './Form'
import { isOutsideElement } from '../../../../utils'
import IStore from '../../../../interfaces/IStore'
import IFaction from '../../../../interfaces/globalSearch/IFaction'

interface IProps {
  headerDataIsFetched: boolean
  advancedSearch: IAdvancedSearch
  disableAdvancedSearch: boolean
  autocompleteSearchFaction: (query: string, option: string) => void
  toggleAdvancedSearch: (open: boolean) => void
  updateAdvancedSearchFormField: (fieldName: string, value: any) => void
  resetAdvancedSearchForm: () => void
  setAdvancedSearchFormData: (formData: { [key: string]: string }) => void
  setAdvancedSearchAutocompleteResultList: (resultList: IFaction[]) => void
  addAdvancedSearchFactionField: (id: number, name: string, respect: string) => void
  removeAdvancedSearchFactionField: (id: number) => void
}

class AdvancedSearch extends Component<IProps> {
  factionTimeout: any
  advancedSearchRef: RefObject<HTMLLIElement>

  constructor(props) {
    super(props)
    this.advancedSearchRef = React.createRef()
  }

  componentDidMount() {
    const { setAdvancedSearchFormData: setFormData } = this.props
    const userID = getCookie('uid')

    document.addEventListener('click', this.handleClickOutside)

    try {
      const savedData = localStorage.getItem(`advancedSearchFormData_${userID}`)

      if (savedData) {
        const savedDataObj = JSON.parse(savedData)

        setFormData(savedDataObj)
      }
    } catch (err) {
      console.error(err)
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
  }

  handleClickOutside = event => {
    const {
      advancedSearch: { open },
      toggleAdvancedSearch: togglePanel
    } = this.props
    const isOutside = isOutsideElement(this.advancedSearchRef, event.target)

    if (open && isOutside) {
      togglePanel(false)
    }
  }

  _handleToggleAdvancedSearch = () => {
    const {
      advancedSearch: { open },
      toggleAdvancedSearch: togglePanel,
      headerDataIsFetched
    } = this.props

    headerDataIsFetched && togglePanel(!open)
  }

  renderForm = () => {
    const {
      advancedSearch,
      updateAdvancedSearchFormField: updateFormField,
      autocompleteSearchFaction: searchFaction,
      resetAdvancedSearchForm: resetForm,
      setAdvancedSearchAutocompleteResultList: setResultList
    } = this.props

    return advancedSearch.optionsList ? (
      <div className='form-wrap advanced-search'>
        <Form
          advancedSearch={advancedSearch}
          updateAdvancedSearchFormField={updateFormField}
          autocompleteSearchFaction={searchFaction}
          resetAdvancedSearchForm={resetForm}
          setAdvancedSearchAutocompleteResultList={setResultList}
          addAdvancedSearchFactionField={this.props.addAdvancedSearchFactionField}
          removeAdvancedSearchFactionField={this.props.removeAdvancedSearchFactionField}
        />
      </div>
    ) : null
  }

  render() {
    const {
      advancedSearch: { open },
      disableAdvancedSearch
    } = this.props

    return (
      <li ref={this.advancedSearchRef} className={cn('find-extend', { open })}>
        <button
          type='button'
          className='top_header_button button'
          disabled={disableAdvancedSearch}
          aria-label={`${open ? 'Close' : 'Open'} advanced search`}
          onClick={this._handleToggleAdvancedSearch}
        >
          <SVGIconGenerator
            iconsHolder={headerIconsHolder}
            iconName='AdvancedSearch'
            preset={DEFAULT_PRESET}
            dimensions={ADVANCED_SEARCH_ICON_DIMENSIONS}
          />
        </button>
        {this.renderForm()}
      </li>
    )
  }
}

export default connect(
  (state: IStore) => ({
    advancedSearch: state.header.advancedSearch,
    disableAdvancedSearch: state.header.settings?.hideAdvancedSearch,
    headerDataIsFetched: state.header.headerDataIsFetched
  }),
  {
    autocompleteSearchFaction,
    toggleAdvancedSearch,
    updateAdvancedSearchFormField,
    resetAdvancedSearchForm,
    setAdvancedSearchFormData,
    setAdvancedSearchAutocompleteResultList,
    addAdvancedSearchFactionField,
    removeAdvancedSearchFactionField
  }
)(AdvancedSearch)
