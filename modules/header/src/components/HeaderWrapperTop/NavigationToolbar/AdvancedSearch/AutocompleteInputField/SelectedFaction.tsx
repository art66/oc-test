import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import IAdvancedSearchFaction from '../../../../../interfaces/IAdvabcedSearchFaction'
import s from '../index.cssmodule.scss'

interface IProps {
  faction: IAdvancedSearchFaction
  removeFaction: (id: number) => void
}

class SelectedFaction extends Component<IProps> {
  handleRemoveFaction = () => this.props.removeFaction(this.props.faction.id)

  render() {
    const { faction } = this.props

    return (
      <div className={s.selectedFactionWrapper}>
        <button type='button' className={s.closeBtn} onClick={this.handleRemoveFaction}>
          <SVGIconGenerator iconName='Close' customClass={s.closeIcon} />
        </button>
        <div className={s.selectedFaction}>
          <div className={s.name}>{faction.name}</div>
          <div className={s.respect}>respect: {faction.respect}</div>
        </div>
      </div>
    )
  }
}

export default SelectedFaction
