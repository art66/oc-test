import React, { ChangeEvent, Component } from 'react'
import Autocomplete from '../../GlobalSearch/AutocompleteSearch/Autocomplete'
import IFaction from '../../../../../interfaces/globalSearch/IFaction'
import IAdvancedSearchFaction from '../../../../../interfaces/IAdvabcedSearchFaction'
import SelectedFaction from './SelectedFaction'
import s from '../index.cssmodule.scss'

interface IProps {
  label: string
  list: IFaction[]
  name: string
  value: string
  searchType: string
  wrapperClassName: string
  idFieldName: string
  idFieldValue: string
  handleInput: (e: ChangeEvent) => void
  handleSelectItem: (selected: IFaction) => void
  handleFocus: () => void
  factions: IAdvancedSearchFaction[]
  removeAdvancedSearchFactionField: (id: number) => void
}

class AutocompleteInputField extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps): boolean {
    const { idFieldValue, value, list, factions } = this.props
    const { idFieldValue: nextIdFieldValue, value: nextValue, list: nextList, factions: nextFactions } = nextProps
    const strProps = JSON.stringify({ idFieldValue, value, list, factions })
    const strNextProps = JSON.stringify({
      idFieldValue: nextIdFieldValue,
      value: nextValue,
      list: nextList,
      factions: nextFactions
    })

    return strProps !== strNextProps
  }

  renderFactions = () => {
    const { factions } = this.props

    return factions?.map(faction => (
      <SelectedFaction key={faction.id} faction={faction} removeFaction={this.props.removeAdvancedSearchFactionField} />
    ))
  }

  render() {
    const {
      label,
      list,
      name,
      searchType,
      wrapperClassName,
      handleInput,
      value,
      handleSelectItem,
      idFieldName,
      idFieldValue,
      handleFocus
    } = this.props

    return (
      <li className={wrapperClassName}>
        <label htmlFor='search-faction' className='label'>
          {label}
        </label>
        <div className={`right ${s.outerInputWrapper}`}>
          {this.renderFactions()}
          <div className={`input-wrapper right ${s.inputWrapper}`}>
            <input type='hidden' name={idFieldName} value={idFieldValue} />
            <Autocomplete
              name={name}
              value={value}
              list={list}
              searchType={searchType}
              noItemsFoundMessage='No results'
              inputClassName={s.autocompleteInput}
              activeInputWrapperClassName={s.open}
              onInput={handleInput}
              preventRedirect={true}
              onSelect={handleSelectItem}
              hideDropdown={!(list && list.length)}
              onFocus={handleFocus}
            />
          </div>
        </div>
      </li>
    )
  }
}

export default AutocompleteInputField
