import React, { Component } from 'react'
import cn from 'classnames'
import Dropdown from '../../../../../../../../shared/components/Dropdown'
import s from '../index.cssmodule.scss'

interface IProps {
  label: string
  checkbox?: {
    id: string
    name: string
    label: string
    value: boolean
  }
  dropdown: {
    list: { id: string; name: string }[]
    name: string
    className?: string
  }
  selected: {
    id: string
    name: string
  }
  handleChangeOption: (nextValue: any) => void
  handleChangeCheckbox?: () => void
}

class OptionSelector extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps): boolean {
    const { checkbox, selected } = this.props
    const { checkbox: nextCheckbox, selected: nextSelected } = nextProps
    const checkboxValChanged = checkbox && nextCheckbox && checkbox.value !== nextCheckbox.value
    const selectedChanged = selected.id !== nextSelected.id

    return checkboxValChanged || selectedChanged
  }

  renderCheckbox = () => {
    const { checkbox, handleChangeCheckbox: updateCheckboxValue } = this.props

    return checkbox ? (
      <>
        <input
          className='checkbox-css'
          type='checkbox'
          name={checkbox.name}
          id={checkbox.id}
          checked={checkbox.value || false}
          onChange={updateCheckboxValue}
        />
        <label htmlFor={checkbox.id} className='marker-css search-condition-not'>
          {checkbox.label}
        </label>
      </>
    ) : null
  }

  render() {
    const { label, dropdown, handleChangeOption: changeOption, selected } = this.props
    const dropdownCn = cn(s.advancedSearchDropdown, [dropdown.className])

    return (
      <li>
        <label className='label'>{label}</label>
        {this.renderCheckbox()}
        <div className='select-wrapper right'>
          <Dropdown
            name={dropdown.name}
            className={dropdownCn}
            list={dropdown.list}
            onChange={changeOption}
            selected={selected}
          />
        </div>
      </li>
    )
  }
}

export default OptionSelector
