import React, { ChangeEvent, Component } from 'react'
import { connect } from 'react-redux'
import IStore from '../../../../../interfaces/IStore'

interface IProps {
  label: string
  fromValue?: string
  toValue?: string
  inputId: { from: string; to: string }
  inputName: { from: string; to: string }
  handleChangeFromValue: (e: ChangeEvent) => void
  handleChangeToVale: (e: ChangeEvent) => void
}

class Range extends Component<IProps> {
  render() {
    const {
      label,
      inputId,
      inputName,
      fromValue,
      toValue,
      handleChangeFromValue: changeFromValue,
      handleChangeToVale: changeToVale
    } = this.props

    return (
      <li>
        <label htmlFor={inputId.from} className="label">
          {label}
        </label>
        <div className="right level-wrap">
          <div className="input-wrapper from left">
            <input
              id={inputId.from}
              name={inputName.from}
              type="number"
              min="0"
              value={fromValue || ''}
              autoComplete="off"
              onChange={changeFromValue}
            />
          </div>
          <label htmlFor={inputId.to}>to</label>
          <div className="input-wrapper to right">
            <input
              id={inputId.to}
              name={inputName.to}
              type="number"
              min="0"
              value={toValue || ''}
              autoComplete="off"
              onChange={changeToVale}
            />
          </div>
        </div>
      </li>
    )
  }
}

export default connect((state: IStore, ownProps: IProps) => {
  const { inputName } = ownProps

  return {
    fromValue: state.header.advancedSearch.formData[inputName.from],
    toValue: state.header.advancedSearch.formData[inputName.to]
  }
}, {})(Range)
