import React, { Component, RefObject } from 'react'
import cn from 'classnames'
import { connect } from 'react-redux'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import headerIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/header'
import { DEFAULT_PRESET, AVATAR_CIRCLE_DIMENSIONS } from '../../../../constants/icons'
import { toggleTopRightMenu, switchSettings, updateAvatar } from '../../../../actions'
import IServerState from '../../../../interfaces/IServerState'
import IRightMenu from '../../../../interfaces/IRighMenu'
import IWindow from '../../../../interfaces/IWindow'
import MenuContent from './MenuContent'
import { isOutsideElement } from '../../../../utils'
import IStore from '../../../../interfaces/IStore'

interface IProps {
  rightMenu: IRightMenu
  serverState: IServerState
  disableSettingsDropdown: boolean
  headerDataIsFetched: boolean
  userData: {
    hospitalStamp?: number
    jailStamp?: number
    logoutHash: string
    userID: number
    avatar: {
      isDefault: boolean
      link: string
    }
  }
  mediaType: string
  toggleTopRightMenu: (open: boolean) => void
  switchSettings: (settingName: string, value: boolean) => void
  updateAvatar: (avatar: { link: string; isDefault: boolean }) => void
  hasAccessToDarkMode: boolean
}

class RightMenu extends Component<IProps> {
  rightMenuRef: RefObject<HTMLLIElement>
  constructor(props) {
    super(props)
    this.rightMenuRef = React.createRef()
  }

  componentDidMount() {
    const win = window as IWindow

    win.WebsocketHandler.addEventListener('header', 'updateAvatar', this.updateAvatar)
    document.addEventListener('click', this.handleClickOutside)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    const {
      rightMenu: { open },
      toggleTopRightMenu: toggleRightMenu
    } = this.props
    const isOutside = isOutsideElement(this.rightMenuRef, event.target)

    if (open && isOutside) {
      toggleRightMenu(false)
    }
  }

  updateAvatar = (data) => {
    const { updateAvatar: update } = this.props

    update(data.image)
  }

  _handleToggleRightMenu = () => {
    const {
      rightMenu: { open },
      toggleTopRightMenu: toggleRightMenu,
      headerDataIsFetched
    } = this.props

    headerDataIsFetched && toggleRightMenu(!open)
  }

  renderMenuContent = () => {
    const {
      rightMenu,
      serverState,
      mediaType,
      userData: { logoutHash, userID },
      switchSettings: toggle,
      hasAccessToDarkMode
    } = this.props

    return serverState ? (
      <MenuContent
        userID={userID}
        logoutHash={logoutHash}
        serverName={serverState.serverName}
        rightMenu={rightMenu}
        mediaType={mediaType}
        switchSettings={toggle}
        hasAccessToDarkMode={hasAccessToDarkMode}
      />
    ) : null
  }

  render() {
    const {
      rightMenu,
      userData: { avatar },
      disableSettingsDropdown
    } = this.props
    const { open } = rightMenu

    return (
      <li ref={this.rightMenuRef} className={cn('avatar', { active: open, default: avatar.isDefault })}>
        <button
          type='button'
          className='top_header_button button'
          disabled={disableSettingsDropdown}
          aria-label={`${open ? 'Close' : 'Open'} menu`}
          onClick={this._handleToggleRightMenu}
        >
          <div className='icon-wrapper'>
            <div className='profile-image-wrapper'>
              <img className='mini-avatar-image' src={avatar.link} alt='' />
            </div>
            <div className='circle-wrapper'>
              <SVGIconGenerator
                iconsHolder={headerIconsHolder}
                iconName='AvatarCircle'
                preset={DEFAULT_PRESET}
                dimensions={AVATAR_CIRCLE_DIMENSIONS}
                customClass='avatar_svg'
              />
            </div>
          </div>
        </button>
        {this.renderMenuContent()}
      </li>
    )
  }
}

export default connect(
  (state: IStore) => ({
    rightMenu: state.header.rightMenu,
    serverState: state.header.serverState,
    mediaType: state.browser.mediaType,
    hasAccessToDarkMode: state.header.settings?.hasAccessToDarkMode,
    disableSettingsDropdown: state.header.settings?.hideSettingsDropdown,
    headerDataIsFetched: state.header.headerDataIsFetched
  }),
  {
    toggleTopRightMenu,
    switchSettings,
    updateAvatar
  }
)(RightMenu)
