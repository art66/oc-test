import React, { Component } from 'react'
import headerIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/header'
import recentHistoryHolder from '@torn/shared/SVG/helpers/iconsHolder/recentHistory'
import SVGIconGenerator from '../../../../../../../../shared/SVG/icons'
import { DEFAULT_PRESET, RIGHT_MENU_ICONS_DIMENSIONS } from '../../../../../constants/icons'
import IWindow from '../../../../../interfaces/IWindow'
import { DESKTOP_MODE_SETTING, NEWS_TICKER_SETTING, TOGGLE_DARK_MODE_EVENT } from '../../../../../constants'
import IRightMenu from '../../../../../interfaces/IRighMenu'
import { removeHeaderDataFromStorage } from '../../../../../utils/storageHelper'
import { removeNewsTickerDataFromStorage } from '../../../../../utils/newsTickerHelper'
import { TToggleDarkModeEventPayload } from '../../../../../interfaces/TToggleDarkModeEventPayload'

interface IProps {
  userID: string | number
  logoutHash: string
  serverName: string
  mediaType: string
  rightMenu: IRightMenu
  switchSettings: (settingName: string, value: boolean) => void
  hasAccessToDarkMode: boolean
}

declare let window: IWindow

class MenuContent extends Component<IProps> {
  _handleToggleDarkMode = (event: React.ChangeEvent<HTMLInputElement>) => {
    const onChangeTornModeEvent = new CustomEvent<TToggleDarkModeEventPayload>(TOGGLE_DARK_MODE_EVENT, {
      detail: { checked: event.target.checked }
    })

    window.dispatchEvent(onChangeTornModeEvent)
  }

  _handleToggleDesktopMode = () => {
    const {
      switchSettings,
      rightMenu: { desktopModeEnabled }
    } = this.props

    window.toggleDesktopMode()
    switchSettings(DESKTOP_MODE_SETTING, !desktopModeEnabled)
  }

  _handleToggleNewsTicker = () => {
    const {
      switchSettings,
      rightMenu: { newsTickerEnabled }
    } = this.props

    switchSettings(NEWS_TICKER_SETTING, !newsTickerEnabled)
  }

  _handleClickLogoutBtn = () => {
    removeHeaderDataFromStorage()
    removeNewsTickerDataFromStorage()
  }

  renderDarkModeToggle = () => {
    const {
      rightMenu: { darkModeEnabled },
      hasAccessToDarkMode
    } = this.props

    return hasAccessToDarkMode ? (
      <li className='setting dark-mode'>
        <label htmlFor='dark-mode-state' className='setting-container'>
          <div className='icon-wrapper'>
            <SVGIconGenerator
              iconsHolder={headerIconsHolder}
              iconName='DarkMode'
              preset={DEFAULT_PRESET}
              dimensions={RIGHT_MENU_ICONS_DIMENSIONS.darkMode}
            />
          </div>
          <span className='setting-name'>Dark Mode</span>
          <div className='choice-container'>
            <input
              className='checkbox-css'
              type='checkbox'
              id='dark-mode-state'
              checked={darkModeEnabled}
              onChange={this._handleToggleDarkMode}
            />
            <span className='marker-css' />
          </div>
        </label>
      </li>
    ) : null
  }

  render() {
    const {
      rightMenu: { desktopModeEnabled, newsTickerEnabled },
      userID,
      logoutHash,
      serverName
    } = this.props

    return (
      <ul className='settings-menu'>
        <li className='link'>
          <a href={`/profiles.php?XID=${userID}`}>
            <div className='icon-wrapper'>
              <SVGIconGenerator
                iconsHolder={headerIconsHolder}
                iconName='Profile'
                preset={DEFAULT_PRESET}
                dimensions={RIGHT_MENU_ICONS_DIMENSIONS.profile}
              />
            </div>
            <span className='link-text'>View Profile</span>
          </a>
        </li>
        {this.renderDarkModeToggle()}
        <li className='setting desktop-view'>
          <label htmlFor='desktop-view-state' className='setting-container'>
            <div className='icon-wrapper'>
              <SVGIconGenerator
                iconsHolder={headerIconsHolder}
                iconName='DesktopMode'
                preset={DEFAULT_PRESET}
                dimensions={RIGHT_MENU_ICONS_DIMENSIONS.desktopMode}
              />
            </div>
            <span className='setting-name'>Desktop View</span>
            <div className='choice-container'>
              <input
                className='checkbox-css'
                type='checkbox'
                id='desktop-view-state'
                checked={desktopModeEnabled}
                onChange={this._handleToggleDesktopMode}
              />
              <span className='marker-css' />
            </div>
          </label>
        </li>
        <li className='setting newsticker'>
          <label htmlFor='newsticker-state' className='setting-container'>
            <div className='icon-wrapper'>
              <SVGIconGenerator
                iconsHolder={headerIconsHolder}
                iconName='Headlines'
                preset={DEFAULT_PRESET}
                dimensions={RIGHT_MENU_ICONS_DIMENSIONS.headlines}
              />
            </div>
            <span className='setting-name'>News Ticker</span>
            <div className='choice-container'>
              <input
                className='checkbox-css'
                type='checkbox'
                id='newsticker-state'
                checked={newsTickerEnabled}
                onChange={this._handleToggleNewsTicker}
              />
              <span className='marker-css' />
            </div>
          </label>
        </li>
        <li className='link'>
          <a href='/preferences.php'>
            <div className='icon-wrapper'>
              <SVGIconGenerator
                iconsHolder={recentHistoryHolder}
                iconName='RHPreferences'
                preset={DEFAULT_PRESET}
                dimensions={RIGHT_MENU_ICONS_DIMENSIONS.settings}
              />
            </div>
            <span className='link-text'>Settings</span>
          </a>
        </li>
        {!(window.isTornMobileApp && window.isTornMobileApp()) ? (
          <li className='link'>
            <a onClick={this._handleClickLogoutBtn} href={`/logout.php?hash=${logoutHash}`}>
              <div className='icon-wrapper'>
                <SVGIconGenerator
                  iconsHolder={headerIconsHolder}
                  iconName='Logout'
                  preset={DEFAULT_PRESET}
                  dimensions={RIGHT_MENU_ICONS_DIMENSIONS.logout}
                />
              </div>
              <span className='link-text'>Logout</span>
            </a>
          </li>
        ) : null}
        <li className='server-info'>
          <div className='info-container'>
            <div className='icon-wrapper'>
              <SVGIconGenerator
                iconsHolder={headerIconsHolder}
                iconName='ServerInfo'
                preset={DEFAULT_PRESET}
                dimensions={RIGHT_MENU_ICONS_DIMENSIONS.serverInfo}
              />
            </div>
            <span className='info-text'>
              Server:{' '}
              <a className='server-link' href='/usersonline.php'>
                {serverName}
              </a>
            </span>
          </div>
        </li>
      </ul>
    )
  }
}

export default MenuContent
