import React, { PureComponent } from 'react'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import { DEFAULT_PRESET, RIGHT_MENU_ICONS_DIMENSIONS } from '../../../../constants/icons'
import { removeHeaderDataFromStorage } from '../../../../utils/storageHelper'

interface IProps {
  logoutHash: string
}

class LogoutButton extends PureComponent<IProps> {
  _handleClickLogoutBtn = () => {
    removeHeaderDataFromStorage()
  }

  render() {
    const { logoutHash } = this.props

    return (
      <li className="logout">
        <a
          href={`/logout.php?hash=${logoutHash}`}
          className="top_header_link"
          aria-label="Logout"
          onClick={this._handleClickLogoutBtn}
        >
          <SVGIconGenerator iconName="Logout" preset={DEFAULT_PRESET} dimensions={RIGHT_MENU_ICONS_DIMENSIONS.logout} />
        </a>
      </li>
    )
  }
}

export default LogoutButton
