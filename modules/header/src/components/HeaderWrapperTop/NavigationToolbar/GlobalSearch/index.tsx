import React, { Component, RefObject } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { IBrowser } from 'redux-responsive'
import Dropdown from '@torn/shared/components/Dropdown'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import globalIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import { getCookie } from '@torn/shared/utils'
import { setAutocompleteResultList, toggleGlobalSearch } from '../../../../actions'
import { DEFAULT_PRESET, SEARCH_ICON_DIMENSIONS_TOUCH } from '../../../../constants/icons'
import IGlobalSearch from '../../../../interfaces/IGlobalSearch'
import IWindow from '../../../../interfaces/IWindow'
import AutocompleteSearch from './AutocompleteSearch'
import SearchTypeOptions from './SearchTypeOptions'
import { isDesktopView, isOutsideElement } from '../../../../utils'
import {
  SEARCH_COMPANY_OPTION,
  SEARCH_FACTION_OPTION,
  SEARCH_FORUM_POSTS_OPTION,
  SEARCH_HELP_OPTION,
  SEARCH_PLAYER_OPTION
} from '../../../../constants'
import IStore from '../../../../interfaces/IStore'
import s from './index.cssmodule.scss'

interface IProps {
  globalSearch: IGlobalSearch
  browser: IBrowser
  setAutocompleteResultList: (resultList: any[]) => void
  toggleGlobalSearch: (open: boolean) => void
}

class GlobalSearch extends Component<IProps> {
  searchPanelRef: RefObject<HTMLLIElement>
  constructor(props: IProps) {
    super(props)
    this.searchPanelRef = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
  }

  getSearchType = () => {
    const {
      globalSearch: { optionsList }
    } = this.props
    const savedSearchType = getCookie('userSearchType')
    const searchTypeIndex = savedSearchType ? parseInt(savedSearchType, 10) : 0

    return optionsList[searchTypeIndex] || optionsList[0]
  }

  handleClickOutside = event => {
    const {
      globalSearch: { open },
      toggleGlobalSearch: toggleGlobalSearchVisibility,
      browser: { mediaType }
    } = this.props
    const isDesktop = isDesktopView(mediaType)
    const isOutside = isOutsideElement(this.searchPanelRef, event.target)

    if (open && !isDesktop && isOutside) {
      toggleGlobalSearchVisibility(false)
    }
  }

  _handleSubmitSearchForm = e => {
    const {
      globalSearch: { inputValue }
    } = this.props
    const searchType = this.getSearchType()

    if (searchType.id === SEARCH_FORUM_POSTS_OPTION) {
      window.location.href = `/forums.php#!p=search&f=0&y=0&q=${inputValue}`
    } else if (searchType.id === SEARCH_PLAYER_OPTION) {
      const regex = /([^(\[\])]+)(\[(\d+)\])*/i
      const match = regex.exec(inputValue)

      if (match) {
        window.location.href = match[3] ?
          `/profiles.php?XID=${match[3]}` :
          `/page.php?sid=UserList&playername=${match[1]}`
      } else {
        window.location.href = `/page.php?sid=UserList&playername=${inputValue}`
      }
    } else if (searchType.id === SEARCH_FACTION_OPTION) {
      window.location.href = `/factions.php?step=listing&q=${inputValue}`
    } else if (searchType.id === SEARCH_COMPANY_OPTION) {
      window.location.href = `/joblist.php?step=search#/p=search&q=${inputValue}`
    } else if (searchType.id === SEARCH_HELP_OPTION) {
      return true
    }

    e.preventDefault()
    return false
  }

  _handleSearchTypeChange = nextSearchType => {
    const {
      globalSearch: { optionsList },
      setAutocompleteResultList: setResultList
    } = this.props
    const searchType = this.getSearchType()
    const win = window as IWindow
    const searchTypeIndex = optionsList.findIndex(item => {
      return item.id === nextSearchType.id
    })

    win.setCookie('userSearchType', searchTypeIndex)

    if (searchType.id !== nextSearchType.id) {
      setResultList([])
    }
  }

  _handleToggleGlobalSearch = () => {
    const {
      globalSearch: { open },
      toggleGlobalSearch: toggleGlobalSearchVisibility
    } = this.props

    toggleGlobalSearchVisibility(!open)
  }

  renderSearchTypeDropdown = () => {
    const {
      browser: { mediaType },
      globalSearch: { optionsList }
    } = this.props
    const isDesktop = isDesktopView(mediaType)
    const searchType = this.getSearchType()

    return isDesktop ? (
      <Dropdown
        name='mode'
        tabIndex={0}
        list={optionsList}
        onChange={this._handleSearchTypeChange}
        selected={searchType}
        className={`${s.userSearchType} react-dropdown-default`}
      />
    ) : (
      <SearchTypeOptions searchType={searchType} handleSearchTypeChange={this._handleSearchTypeChange} />
    )
  }

  renderSearchForm = () => {
    const {
      browser: { mediaType },
      globalSearch
    } = this.props
    const searchType = this.getSearchType()
    const isDesktop = isDesktopView(mediaType)
    const showSearchForm = isDesktop || (!isDesktop && globalSearch.open)

    return showSearchForm ? (
      <div className={cn('find', s.searchFormWrapper)}>
        <form
          className='clearfix'
          action={searchType.id === SEARCH_HELP_OPTION ? '/wiki/index.php' : '/search.php'}
          method=''
          id='searchForm'
          name='searchForm'
          onSubmit={this._handleSubmitSearchForm}
        >
          <AutocompleteSearch searchType={searchType} />
          <div className={s.searchTypeWrapper} id='search-type-wrapper'>
            {this.renderSearchTypeDropdown()}
          </div>
        </form>
      </div>
    ) : null
  }

  render() {
    const { globalSearch } = this.props

    return (
      <li ref={this.searchPanelRef} className='find-wrapper' role='search'>
        <button
          type='button'
          className={cn('search-mobile-button', s.searchBtn, { [s.open]: globalSearch?.open })}
          aria-label={`${globalSearch?.open ? 'Close' : 'Open'} global search`}
          onClick={this._handleToggleGlobalSearch}
        >
          <i className='top_header_button search-mobile-button-icon'>
            <SVGIconGenerator
              iconsHolder={globalIconsHolder}
              iconName='Search'
              preset={DEFAULT_PRESET}
              dimensions={SEARCH_ICON_DIMENSIONS_TOUCH}
            />
          </i>
        </button>
        {this.renderSearchForm()}
      </li>
    )
  }
}

export default connect(
  (state: IStore) => ({
    browser: state.browser
  }),
  {
    setAutocompleteResultList,
    toggleGlobalSearch
  }
)(GlobalSearch)
