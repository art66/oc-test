// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'autocomplete-no-items': string;
  'autocomplete-wrapper': string;
  'company': string;
  'companyStarSVG': string;
  'dropdown-caret': string;
  'dropdown-content': string;
  'dropdown-opened': string;
  'faction': string;
  'globalSvgShadow': string;
  'hide': string;
  'input-text': string;
  'item': string;
  'name': string;
  'player': string;
  'respect': string;
  'scroll-area': string;
  'scroll-area-content': string;
  'scrollarea': string;
  'scrollbar': string;
  'scrollbar-container': string;
  'searchInput': string;
  'selected': string;
  'stars': string;
  'statusIconWrapper': string;
  'toggler': string;
  'torn-react-autocomplete': string;
  'vertical': string;
}
export const cssExports: CssExports;
export default cssExports;
