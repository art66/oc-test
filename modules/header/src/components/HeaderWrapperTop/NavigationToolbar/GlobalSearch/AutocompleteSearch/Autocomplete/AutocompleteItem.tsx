import React, { PureComponent } from 'react'
import cn from 'classnames'
import ICompany from '../../../../../../interfaces/globalSearch/ICompany'
import IFaction from '../../../../../../interfaces/globalSearch/IFaction'
import IItem from '../../../../../../interfaces/globalSearch/IItem'
import IPlace from '../../../../../../interfaces/globalSearch/IPlace'
import IPlayer from '../../../../../../interfaces/globalSearch/IPlayer'
import s from './index.cssmodule.scss'

type TResultItem = ICompany | IFaction | IItem | IPlace | IPlayer

interface IProps {
  item: TResultItem
  selected?: TResultItem
  targets?: string
  onClick: (item: TResultItem) => void
  onMouseEnter?: (item: TResultItem) => void
  customClass?: string
  preventRedirect?: boolean
}

class AutocompleteItem extends PureComponent<IProps> {
  _handleItemClick = (e) => {
    const { item, onClick, preventRedirect } = this.props

    if (preventRedirect) {
      e.preventDefault()
    }

    onClick(item)
  }

  _handleItemMouseEnter = () => {
    const { item, onMouseEnter } = this.props

    onMouseEnter(item)
  }

  render() {
    const { item, selected, children, customClass } = this.props

    return (
      <a
        className={cn(s['item'], customClass, { [s.selected]: item.id === selected?.id })}
        onClick={this._handleItemClick}
        onMouseEnter={this._handleItemMouseEnter}
        href={item.url}
      >
        {children}
      </a>
    )
  }
}

export default AutocompleteItem
