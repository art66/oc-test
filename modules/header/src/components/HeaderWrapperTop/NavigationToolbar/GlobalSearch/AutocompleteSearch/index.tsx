import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import globalIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import Autocomplete from './Autocomplete'
import { DEFAULT_PRESET, SEARCH_ICON_DIMENSIONS } from '../../../../../constants/icons'
import IGlobalSearch from '../../../../../interfaces/IGlobalSearch'
import {
  autocompleteSearchByOption,
  updateGlobalSearchInputValue,
  setAutocompleteResultList
} from '../../../../../actions'
import {
  SEARCH_FACTION_OPTION,
  SEARCH_PLAYER_OPTION,
  SEARCH_COMPANY_OPTION,
  SEARCH_PLACE_OPTION,
  SEARCH_ITEM_MARKET_OPTION,
  SEARCH_FORUM_POSTS_OPTION,
  SEARCH_HELP_OPTION
} from '../../../../../constants'
import IStore from '../../../../../interfaces/IStore'
import IPlayer from '../../../../../interfaces/globalSearch/IPlayer'
import IFaction from '../../../../../interfaces/globalSearch/IFaction'
import ICompany from '../../../../../interfaces/globalSearch/ICompany'
import IItem from '../../../../../interfaces/globalSearch/IItem'
import IPlace from '../../../../../interfaces/globalSearch/IPlace'
import sInput from './Autocomplete/index.cssmodule.scss'
import s from './index.cssmodule.scss'

type TResultItem = ICompany | IFaction | IItem | IPlace | IPlayer

interface IProps {
  globalSearch: IGlobalSearch
  autocompleteSearchByOption: (query: string, option: string) => void
  updateGlobalSearchInputValue: (value: string) => void
  searchType: { id: string; name: string }
  setAutocompleteResultList: (resultList: TResultItem[]) => void
}

class AutocompleteSearch extends PureComponent<IProps> {
  timeout: any
  componentWillUnmount() {
    clearTimeout(this.timeout)
  }

  _handleAutocompleteFocus = () => {
    const { setAutocompleteResultList: setResultList } = this.props

    setResultList([])
  }

  _handleAutocompleteChange = (e) => {
    clearTimeout(this.timeout)
    const {
      searchType,
      autocompleteSearchByOption: searchByOption,
      updateGlobalSearchInputValue: updateInputValue
    } = this.props
    const options = [
      SEARCH_PLAYER_OPTION,
      SEARCH_FACTION_OPTION,
      SEARCH_COMPANY_OPTION,
      SEARCH_PLACE_OPTION,
      SEARCH_ITEM_MARKET_OPTION
    ]
    const isExistedSearchType = options.find((item) => item === searchType.id)
    const inputValue = e.target.value

    updateInputValue(inputValue)

    if (isExistedSearchType) {
      this.timeout = setTimeout(() => {
        searchByOption(inputValue, searchType.id)
      }, 250)
    }
  }

  _handleAutocompleteSelect = () => {}

  _handleInputChange = (e) => {
    const { updateGlobalSearchInputValue: updateInputValue } = this.props

    updateInputValue(e.target.value)
  }

  _renderSearchInputField = () => {
    const {
      searchType,
      globalSearch: { resultList, inputValue }
    } = this.props
    const isDisabledAutocomplete = searchType.id === SEARCH_FORUM_POSTS_OPTION || searchType.id === SEARCH_HELP_OPTION

    return isDisabledAutocomplete ? (
      <input
        className={sInput.searchInput}
        placeholder='search...'
        name={searchType.id === SEARCH_HELP_OPTION ? 'search' : 'userword'}
        value={inputValue || ''}
        onChange={this._handleInputChange}
        autoComplete='off'
      />
    ) : (
      <Autocomplete
        name='userword'
        value={inputValue || ''}
        placeholder='search...'
        list={resultList || []}
        searchType={searchType.id}
        noItemsFoundMessage=''
        hideDropdown={!(resultList && resultList.length)}
        onFocus={this._handleAutocompleteFocus}
        onInput={this._handleAutocompleteChange}
        onSelect={this._handleAutocompleteSelect}
      />
    )
  }

  render() {
    return (
      <div className={cn('input-wrapper', 'ac-main', 'search', 'left', s.searchWrapper)}>
        {this._renderSearchInputField()}
        <button
          id='header-search'
          aria-label='Search'
          type='submit'
          className={cn('top_header_button', 'search', s.searchBtn)}
        >
          <SVGIconGenerator
            iconsHolder={globalIconsHolder}
            iconName='Search'
            preset={DEFAULT_PRESET}
            dimensions={SEARCH_ICON_DIMENSIONS}
          />
        </button>
      </div>
    )
  }
}

export default connect(
  (state: IStore) => ({
    globalSearch: state.header.globalSearch
  }),
  {
    autocompleteSearchByOption,
    updateGlobalSearchInputValue,
    setAutocompleteResultList
  }
)(AutocompleteSearch)
