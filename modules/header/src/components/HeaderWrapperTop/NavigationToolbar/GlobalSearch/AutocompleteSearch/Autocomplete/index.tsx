import React, { PureComponent, RefObject } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import globalIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/global'
import AutocompleteItem from './AutocompleteItem'
import { EMPTY_STAR_ICON_PRESET, ONLINE_STATUS_ICONS, STAR_ICON_PRESET } from '../../../../../../constants/icons'
import {
  SEARCH_COMPANY_OPTION,
  SEARCH_FACTION_OPTION,
  SEARCH_ITEM_MARKET_OPTION,
  SEARCH_PLACE_OPTION,
  SEARCH_PLAYER_OPTION,
  KEY_CODE_TAB,
  KEY_CODE_ENTER,
  KEY_CODE_ESCAPE,
  KEY_CODE_ARROW_UP,
  KEY_CODE_ARROW_DOWN,
  SEARCH_COMPANY_MAX_STARS_AMOUNT
} from '../../../../../../constants'
import ICompany from '../../../../../../interfaces/globalSearch/ICompany'
import IFaction from '../../../../../../interfaces/globalSearch/IFaction'
import IItem from '../../../../../../interfaces/globalSearch/IItem'
import IPlace from '../../../../../../interfaces/globalSearch/IPlace'
import IPlayer from '../../../../../../interfaces/globalSearch/IPlayer'
import s from './index.cssmodule.scss'

type TResultList = ICompany[] | IFaction[] | IItem[] | IPlace[] | IPlayer[]
type TResultItem = ICompany | IFaction | IItem | IPlace | IPlayer

interface IProps {
  hideDropdown: boolean
  noItemsFoundMessage: string
  onInputClick: () => void
  list: TResultList
  value: string
  onRef: (item?: any) => void
  onInput: (item?: any) => void
  name: string
  searchType: string
  selected?: TResultItem
  onFocus?: () => void
  onBlur?: () => void
  onSelect?: (item?: TResultItem) => void
  targets?: string
  className?: string
  inputClassName?: string
  activeInputWrapperClassName?: string
  placeholder?: string
  autoFocus?: boolean
  ariaLabel?: string
  preventRedirect?: boolean
}

interface IState {
  dropdownIsActive: boolean
  dropdownIsVisible: boolean
  value: string
  selected: TResultItem
}

class Autocomplete extends PureComponent<IProps, IState> {
  inputWrapper: RefObject<HTMLDivElement>
  input: RefObject<HTMLInputElement>

  static defaultProps = {
    hideDropdown: false,
    noItemsFoundMessage: 'No person could be found',
    onInputClick: () => {},
    onRef: () => {}
  }

  constructor(props, context) {
    super(props, context)
    const { selected, value } = this.props

    this.inputWrapper = React.createRef()
    this.input = React.createRef()
    this.state = {
      dropdownIsActive: false,
      dropdownIsVisible: false,
      selected,
      value
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { value } = prevState

    if (nextProps.value !== value) {
      return {
        value: nextProps.value
      }
    }

    return null
  }

  componentDidMount() {
    const { onRef } = this.props

    window.addEventListener('click', this.hideDropdownIfInactive, false)
    window.addEventListener('keydown', this._handleKeyPress, false)
    onRef(this)
  }

  componentWillUnmount() {
    const { onRef } = this.props

    window.removeEventListener('click', this.hideDropdownIfInactive, false)
    window.removeEventListener('keydown', this._handleKeyPress, false)
    onRef(null)
  }

  _handlePressEnter = (e, selected) => {
    e.preventDefault()
    const { preventRedirect } = this.props

    if (preventRedirect) {
      this._handleSelect(selected)
      this.input.current.blur()
    } else {
      window.location.href = selected.url
    }
  }

  _handleKeyPress = (e) => {
    const { selected } = this.state

    if (!this.inputWrapper.current.contains(document.activeElement)) {
      return
    }

    if (e.keyCode === KEY_CODE_ARROW_UP) {
      e.preventDefault()
      this._selectPrev()
    } else if (e.keyCode === KEY_CODE_ARROW_DOWN) {
      e.preventDefault()
      this._selectNext()
    } else if (e.keyCode === KEY_CODE_ENTER && selected) {
      this._handlePressEnter(e, selected)
    } else if (e.keyCode === KEY_CODE_ESCAPE) {
      this.hideDropdown()
    } else if (e.keyCode === KEY_CODE_TAB) {
      this.closeDropdown()
    }
  }

  hideDropdown = () => {
    this.setState({ dropdownIsVisible: false })
  }

  hideDropdownIfInactive = () => {
    const { dropdownIsActive } = this.state

    if (!dropdownIsActive) {
      this.setState({ dropdownIsVisible: false })
    }
  }

  closeDropdown = () => {
    this.setState({ dropdownIsVisible: false, dropdownIsActive: false })
  }

  openDropdown = () => {
    this.setState({ dropdownIsActive: true, dropdownIsVisible: true })
  }

  _showDropdown = () => {
    const { dropdownIsActive } = this.state

    if (dropdownIsActive) {
      this.setState({ dropdownIsVisible: true })
    }
  }

  _handleInputClick = () => {
    const { dropdownIsActive } = this.state
    const { onInputClick } = this.props

    onInputClick()

    if (dropdownIsActive) {
      this.setState({ dropdownIsVisible: true })
    }
  }

  _handleInputFocus = () => {
    const { onFocus } = this.props

    this.openDropdown()
    onFocus && onFocus()
  }

  _handleFocus = () => {
    this.openDropdown()
  }

  _handleBlur = () => {
    const { onBlur } = this.props

    if (onBlur) {
      onBlur()
    }

    this.setState({ dropdownIsActive: false })
  }

  _handleInputChange = (e) => {
    const { onInput } = this.props

    this.setState({ value: e.target.value })
    this._showDropdown()
    onInput && onInput(e)
  }

  _handleSelect = (selected) => {
    const { onSelect } = this.props

    this.setState({
      value: selected.name,
      dropdownIsActive: false,
      selected
    })
    this.hideDropdownIfInactive()
    onSelect && onSelect(selected)
    this.closeDropdown()
  }

  _selectNext = () => {
    const { list } = this.props
    const { selected } = this.state

    const selectedIndex = this._getSelectedIndex()
    const selectedItem = list[selectedIndex + 1] || selected

    this.setState({
      selected: selectedItem
    })
  }

  _selectPrev = () => {
    const { list } = this.props
    const { selected } = this.state

    const selectedIndex = this._getSelectedIndex()
    const selectedItem = list[selectedIndex - 1] || selected

    this.setState({
      selected: selectedItem
    })
  }

  _setSelected = (item) => {
    this.setState({
      selected: item
    })
  }

  _getSelectedIndex = () => {
    const { list } = this.props
    const { selected } = this.state

    return list.findIndex((item) => item === selected) || 0
  }

  _renderPlayers = (list) => {
    const { selected } = this.state
    const { preventRedirect } = this.props

    return list.map((item) => {
      return (
        <AutocompleteItem
          key={item.id}
          item={item}
          selected={selected}
          onClick={this._handleSelect}
          onMouseEnter={this._setSelected}
          customClass={s.player}
          preventRedirect={preventRedirect}
        >
          <span className={s.statusIconWrapper}>
            <SVGIconGenerator
              iconsHolder={globalIconsHolder}
              iconName={ONLINE_STATUS_ICONS.name}
              type={ONLINE_STATUS_ICONS.type[item.online]}
              preset={ONLINE_STATUS_ICONS.preset[item.online]}
            />
          </span>
          <span className={s['name']}>
            {item.name} [{item.id}]
          </span>
        </AutocompleteItem>
      )
    })
  }

  _renderFactions = (list) => {
    const { selected } = this.state
    const { preventRedirect } = this.props

    return list.map((item) => {
      return (
        <AutocompleteItem
          key={item.id}
          item={item}
          selected={selected}
          onClick={this._handleSelect}
          onMouseEnter={this._setSelected}
          customClass={s.faction}
          preventRedirect={preventRedirect}
        >
          <span className={s['name']}>{item.name}</span>
          <span className={s.respect}>respect: {item.respect}</span>
        </AutocompleteItem>
      )
    })
  }

  _renderCompaniesStars = (starsAmount) => {
    const stars = Array.from(Array(SEARCH_COMPANY_MAX_STARS_AMOUNT), (_, index) => index)

    return stars.map((item) => {
      return (
        <SVGIconGenerator
          iconsHolder={globalIconsHolder}
          key={item}
          iconName='Star'
          preset={item < starsAmount ? STAR_ICON_PRESET : EMPTY_STAR_ICON_PRESET}
          customClass={s.companyStarSVG}
        />
      )
    })
  }

  _renderCompanies = (list) => {
    const { selected } = this.state
    const { preventRedirect } = this.props

    return list.map((item) => {
      return (
        <AutocompleteItem
          key={item.id}
          item={item}
          selected={selected}
          onClick={this._handleSelect}
          onMouseEnter={this._setSelected}
          customClass={s.company}
          preventRedirect={preventRedirect}
        >
          <span className={s['name']}>{item.name}</span>
          <span className={s.stars}>{this._renderCompaniesStars(item.stars)}</span>
        </AutocompleteItem>
      )
    })
  }

  _renderPlaces = (list) => {
    const { selected } = this.state
    const { preventRedirect } = this.props

    return list.map((item) => {
      return (
        <AutocompleteItem
          key={item.id}
          item={item}
          selected={selected}
          onClick={this._handleSelect}
          onMouseEnter={this._setSelected}
          customClass={s.company}
          preventRedirect={preventRedirect}
        >
          <span className={s['name']}>{item.name}</span>
        </AutocompleteItem>
      )
    })
  }

  _renderItemsInMarket = (list) => {
    const { selected } = this.state
    const { preventRedirect } = this.props

    return list.map((item) => {
      return (
        <AutocompleteItem
          key={item.id}
          item={item}
          selected={selected}
          onClick={this._handleSelect}
          onMouseEnter={this._setSelected}
          customClass={s.company}
          preventRedirect={preventRedirect}
        >
          <span className={s['name']}>{item.name}</span>
        </AutocompleteItem>
      )
    })
  }

  _renderListItems = (list) => {
    const { noItemsFoundMessage, searchType } = this.props

    if (!list.length) {
      return <div className={s['autocomplete-no-items']}>{noItemsFoundMessage}</div>
    }

    if (searchType === SEARCH_PLAYER_OPTION) {
      return this._renderPlayers(list)
    } else if (searchType === SEARCH_FACTION_OPTION) {
      return this._renderFactions(list)
    } else if (searchType === SEARCH_COMPANY_OPTION) {
      return this._renderCompanies(list)
    } else if (searchType === SEARCH_PLACE_OPTION) {
      return this._renderPlaces(list)
    } else if (searchType === SEARCH_ITEM_MARKET_OPTION) {
      return this._renderItemsInMarket(list)
    }
  }

  _renderResultDropdown = () => {
    const { dropdownIsVisible } = this.state
    const { list, hideDropdown } = this.props

    const show = !hideDropdown && dropdownIsVisible && list

    return (
      <div
        className={cn({ [s.hide]: !show })}
        tabIndex={0}
        onFocus={this._handleFocus}
        onBlur={this._handleBlur}
      >
        <div className={s['scroll-area']}>
          <div className={s['dropdown-content']}>{this._renderListItems(list)}</div>
        </div>
      </div>
    )
  }

  _renderAutocomplete = () => {
    const { dropdownIsVisible, value } = this.state
    const {
      list,
      name,
      inputClassName,
      placeholder,
      autoFocus,
      hideDropdown,
      ariaLabel,
      activeInputWrapperClassName
    } = this.props
    const show = !hideDropdown && dropdownIsVisible && list
    const customActiveClass = show && activeInputWrapperClassName ? activeInputWrapperClassName : ''

    return (
      <div ref={this.inputWrapper} className={cn(s['autocomplete-wrapper'], customActiveClass, { [s.active]: show })}>
        <input
          type='text'
          name={name}
          value={value || ''}
          className={inputClassName ? `${inputClassName} ${s.searchInput}` : s.searchInput}
          placeholder={placeholder || ''}
          autoFocus={autoFocus || false}
          onChange={this._handleInputChange}
          onClick={this._handleInputClick}
          onFocus={this._handleInputFocus}
          onBlur={this._handleBlur}
          autoComplete='off'
          aria-label={ariaLabel}
          ref={this.input}
        />
        {this._renderResultDropdown()}
      </div>
    )
  }

  render() {
    const { className } = this.props

    return (
      <div className={cn(s['torn-react-autocomplete'], { [s[className]]: className })}>
        {this._renderAutocomplete()}
      </div>
    )
  }
}

export default Autocomplete
