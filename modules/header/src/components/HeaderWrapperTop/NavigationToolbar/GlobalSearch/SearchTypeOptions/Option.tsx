import React, { PureComponent } from 'react'

interface IProps {
  item: {
    id: string
    name: string
  }
  index: number
  checked: boolean
  handleSearchTypeChange: (nextSearchType: { id: string; name: string }) => void
}

class Option extends PureComponent<IProps> {
  _handleChangeTypeSearch = () => {
    const { item, handleSearchTypeChange } = this.props

    handleSearchTypeChange(item)
  }

  render() {
    const { item, index, checked } = this.props

    return (
      <li className={index % 2 ? 'search-type-r' : 'search-type-l'}>
        <div className="choice-container">
          <input
            id={item.id}
            className="radio-css light"
            type="radio"
            name="search-type"
            value={item.id}
            checked={checked}
            onChange={this._handleChangeTypeSearch}
          />
          <label htmlFor={item.id} className="marker-css">
            {item.name}
          </label>
        </div>
      </li>
    )
  }
}

export default Option
