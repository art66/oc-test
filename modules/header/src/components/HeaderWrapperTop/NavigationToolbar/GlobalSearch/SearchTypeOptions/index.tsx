import React, { Component } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Option from './Option'
import IGlobalSearch from '../../../../../interfaces/IGlobalSearch'
import IStore from '../../../../../interfaces/IStore'
import s from './index.cssmodule.scss'

interface IProps {
  globalSearch: IGlobalSearch
  searchType: {
    id: string
    name: string
  }
  handleSearchTypeChange: (nextSearchType: { id: string; name: string }) => void
}

class SearchTypeOptions extends Component<IProps> {
  renderOptionsList = () => {
    const {
      globalSearch: { optionsList },
      searchType,
      handleSearchTypeChange
    } = this.props

    return optionsList.map((item, index) => {
      return (
        <Option
          key={item.id}
          item={item}
          index={index}
          checked={searchType.id === item.id}
          handleSearchTypeChange={handleSearchTypeChange}
        />
      )
    })
  }

  render() {
    const {
      globalSearch: { optionsList }
    } = this.props

    return (
      <div className={cn('search-type-radio', 'visible', s.searchTypeOptions)}>
        <ul className={cn('radio-items', 'clearfix', s.optionsList)}>
          {this.renderOptionsList()}
          {optionsList.length % 2 ? <li className="search-type-r" /> : null}
        </ul>
      </div>
    )
  }
}

export default connect((state: IStore) => ({
  globalSearch: state.header.globalSearch
}))(SearchTypeOptions)
