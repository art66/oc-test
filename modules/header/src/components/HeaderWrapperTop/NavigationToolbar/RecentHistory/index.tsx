import React, { Component, RefObject } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import headerIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/header'
import { ACTIVITY_LOG_ICON_DIMENSIONS, DEFAULT_PRESET } from '../../../../constants/icons'
import { toggleRecentHistory } from '../../../../actions'
import { isOutsideElement } from '../../../../utils'
import RecentHistoryApp from './RecentHistoryApp'
import IStore from '../../../../interfaces/IStore'

interface IProps {
  recentHistory: {
    open: boolean
  }
  disableActivityLog: boolean
  headerDataIsFetched: boolean
  toggleRecentHistory: (open: boolean) => void
}

class RecentHistory extends Component<IProps> {
  recentHistoryRef: RefObject<HTMLLIElement>
  constructor(props: IProps) {
    super(props)
    this.recentHistoryRef = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    const {
      recentHistory: { open },
      toggleRecentHistory: toggle
    } = this.props
    const isOutside = isOutsideElement(this.recentHistoryRef, event.target)

    if (open && isOutside) {
      toggle(false)
    }
  }

  _handleToggleRecentHistory = () => {
    const {
      recentHistory: { open },
      toggleRecentHistory: toggle,
      headerDataIsFetched
    } = this.props

    headerDataIsFetched && toggle(!open)
  }

  render() {
    const {
      recentHistory: { open },
      disableActivityLog
    } = this.props

    return (
      <li ref={this.recentHistoryRef} id='recent-history-wrapper' className={cn('recently-history', { open })}>
        <button
          type='button'
          className='top_header_button button recently-history'
          onClick={this._handleToggleRecentHistory}
          disabled={disableActivityLog}
          aria-label={`${open ? 'Close' : 'Open'} activity log`}
        >
          <SVGIconGenerator
            iconsHolder={headerIconsHolder}
            iconName='ActivityLog'
            preset={DEFAULT_PRESET}
            dimensions={ACTIVITY_LOG_ICON_DIMENSIONS}
          />
        </button>
        <div className='recent-history-content'>{open ? <RecentHistoryApp /> : null}</div>
      </li>
    )
  }
}

export default connect(
  (state: IStore) => ({
    recentHistory: state.header.recentHistory,
    disableActivityLog: state.header.settings?.hideActivityLog,
    headerDataIsFetched: state.header.headerDataIsFetched
  }),
  {
    toggleRecentHistory
  }
)(RecentHistory)
