import React, { Component } from 'react'
import { isEqualTimeSeparators } from '../../../../../../../utils/actionHelpers'
import TimeSeparator from './TimeSeparator'
import Action from './Action'
import ITimeSeparator from '../../../../../../../interfaces/recentHistory/ITimeSeparator'
import IAction from '../../../../../../../interfaces/recentHistory/IAction'

interface IProps {
  group: {
    timeSeparator: ITimeSeparator | null
    log: IAction[]
  }
  mode: string
  isStaffVersion: boolean
  toggleSimilarActions: (actionId: string, open: boolean) => void
  toggleActionExpandState: (actionId: string, parentId: string | undefined) => void
  changeActionGroup: (
    oldTimeSeparator: ITimeSeparator,
    newTimeSeparator: ITimeSeparator,
    actionId: string,
    parentId: string | undefined
  ) => void
  darkModeEnabled: boolean
}

class LogGroup extends Component<IProps> {
  shouldComponentUpdate(nextProps: IProps) {
    const {
      group: { timeSeparator, log },
      darkModeEnabled
    } = this.props
    const {
      group: { timeSeparator: nextTimeSeparator, log: nextLog },
      darkModeEnabled: nextDarkModeEnabled
    } = nextProps
    const timeSeparatorChanged = !isEqualTimeSeparators(timeSeparator, nextTimeSeparator)
    const setOfRecordsChanged = JSON.stringify(log) !== JSON.stringify(nextLog)
    const DMStateChanged = darkModeEnabled !== nextDarkModeEnabled

    return timeSeparatorChanged || setOfRecordsChanged || DMStateChanged
  }

  renderLog = () => {
    const {
      group: { log, timeSeparator },
      mode,
      darkModeEnabled,
      isStaffVersion
    } = this.props

    return log.map(action => {
      return (
        <Action
          key={action.ID}
          action={action}
          mode={mode}
          isStaffVersion={isStaffVersion}
          timeSeparator={timeSeparator}
          toggleSimilarActions={this.props.toggleSimilarActions}
          toggleActionExpandState={this.props.toggleActionExpandState}
          changeActionGroup={this.props.changeActionGroup}
          darkModeEnabled={darkModeEnabled}
        />
      )
    })
  }

  render() {
    const {
      group: { timeSeparator, log },
      mode
    } = this.props

    return (
      <>
        {timeSeparator && log.length ? <TimeSeparator timeSeparator={timeSeparator} mode={mode} /> : null}
        {this.renderLog()}
      </>
    )
  }
}

export default LogGroup
