import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchRecentHistoryData, resetRecentHistoryData } from '../../../../../actions/recentHistory'
import Preloader from './Preloader'
import LogListWrapper from './LogList'
import BottomLink from './BottomLink'
import IGroup from '../../../../../interfaces/recentHistory/IGroup'
import IStore from '../../../../../interfaces/IStore'
import '../../../../../styles/global.scss'

interface IProps {
  fetchRecentHistoryData: () => void
  resetRecentHistoryData: () => void
  recentHistory: {
    history: IGroup[]
  }
}

class RecentHistoryApp extends Component<IProps> {
  shouldComponentUpdate(nextProps): boolean {
    const {
      recentHistory: { history }
    } = this.props

    return !history !== !nextProps.recentHistory.history
  }

  componentDidMount() {
    const { fetchRecentHistoryData: fetchData } = this.props

    fetchData()
  }

  componentWillUnmount() {
    const { resetRecentHistoryData: resetData } = this.props

    resetData()
  }

  renderContent = () => {
    const {
      recentHistory: { history }
    } = this.props

    return history ? (
      <>
        <LogListWrapper />
        <BottomLink />
      </>
    ) : (
      <Preloader />
    )
  }

  render() {
    return <div className='recentHistory'>{this.renderContent()}</div>
  }
}

const mapStateToProps = (state: IStore) => ({
  recentHistory: state.recentHistory
})

const mapDispatchToProps = {
  fetchRecentHistoryData,
  resetRecentHistoryData
}

export default connect(mapStateToProps, mapDispatchToProps)(RecentHistoryApp)
