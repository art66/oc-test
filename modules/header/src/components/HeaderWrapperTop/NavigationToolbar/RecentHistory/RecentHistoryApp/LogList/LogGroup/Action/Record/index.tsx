import React, { Component } from 'react'
import IAction from '../../../../../../../../../interfaces/recentHistory/IAction'
import ITimeSeparator from '../../../../../../../../../interfaces/recentHistory/ITimeSeparator'
import ActionTime from './ActionTime'
import ActionIcon from './ActionIcon'
import ActionText from './ActionText'
import CopySection from './CopySection'
import Warning from './Warning'

interface IProps {
  action: IAction
  timeSeparator: ITimeSeparator
  parentId?: string
  mode: string
  isStaffVersion: boolean
  toggleActionExpandState: (actionId: string, parentId: string | undefined) => void
  changeActionGroup: (
    oldTimeSeparator: ITimeSeparator,
    newTimeSeparator: ITimeSeparator,
    actionId: string,
    parentId: string | undefined
  ) => void
  darkModeEnabled: boolean
}

class Record extends Component<IProps> {
  _handleExpandingRecord = () => {
    const {
      action: { ID },
      parentId,
      toggleActionExpandState: toggleAction
    } = this.props

    toggleAction(ID, parentId)
  }

  render() {
    const { action, timeSeparator, parentId, mode, darkModeEnabled, isStaffVersion } = this.props
    const { time, text, category, params, ID, expanded } = action
    const { inaccurate } = params

    return (
      <>
        <td>
          <ActionTime
            time={time}
            actionId={ID}
            timeSeparator={timeSeparator}
            parentId={parentId}
            isStaffVersion={isStaffVersion}
            changeActionGroup={this.props.changeActionGroup}
            darkModeEnabled={darkModeEnabled}
          />
        </td>
        <td>
          <ActionIcon category={category} params={params} mode={mode} />
          <ActionText
            text={text}
            params={params}
            parentId={parentId}
            actionId={ID}
            expanded={expanded}
            isStaffVersion={isStaffVersion}
            toggleActionExpandState={this.props.toggleActionExpandState}
          />
          {inaccurate ? <Warning mode={mode} /> : null}
          {expanded ? <CopySection action={action} mode={mode} isStaffVersion={isStaffVersion} /> : null}
        </td>
      </>
    )
  }
}

export default Record
