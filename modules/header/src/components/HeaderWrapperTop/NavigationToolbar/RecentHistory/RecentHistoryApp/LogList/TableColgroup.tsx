import React from 'react'
import cn from 'classnames'
import s from './index.cssmodule.scss'

interface IProps {
  isStaffVersion: boolean
}

const TableColgroup = (props: IProps) => {
  return (
    <colgroup className={cn({ [s.staff]: props.isStaffVersion })}>
      <col className={s.timeCol} />
      <col />
    </colgroup>
  )
}

export default TableColgroup
