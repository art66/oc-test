import React from 'react'
import { connect } from 'react-redux'
import Preloader from '@torn/shared/components/Preloader'
import LogGroup from './LogGroup'
import IGroup from '../../../../../../interfaces/recentHistory/IGroup'
import ITimeSeparator from '../../../../../../interfaces/recentHistory/ITimeSeparator'
import {
  toggleActionExpandState,
  toggleSimilarActions,
  changeActionGroup
} from '../../../../../../actions/recentHistory'
import TableColgroup from './TableColgroup'
import s from './index.cssmodule.scss'

interface IProps {
  isFetching: boolean
  history: IGroup[]
  mode: string
  isStaffVersion?: boolean
  toggleSimilarActions: (actionId: string, open: boolean) => void
  toggleActionExpandState: (actionId: string, parentId: string | undefined) => void
  changeActionGroup: (
    oldTimeSeparator: ITimeSeparator,
    newTimeSeparator: ITimeSeparator,
    actionId: string,
    parentId: string | undefined
  ) => void
  darkModeEnabled: boolean
}

const LogList = (props: IProps) => {
  const getMode = () => (props.darkModeEnabled ? 'dark' : props.mode)

  const renderGroups = () => {
    return props.history.map(group => {
      const key = group.timeSeparator ? group.timeSeparator.number + group.timeSeparator.label : 'recent'

      return (
        <LogGroup
          key={key}
          group={group}
          mode={getMode()}
          isStaffVersion={props.isStaffVersion}
          darkModeEnabled={props.darkModeEnabled}
          toggleSimilarActions={props.toggleSimilarActions}
          toggleActionExpandState={props.toggleActionExpandState}
          changeActionGroup={props.changeActionGroup}
        />
      )
    })
  }

  const renderPreloader = () => {
    const { isFetching } = props

    return isFetching ? (
      <div className={s.preloaderWrapper}>
        <Preloader />
      </div>
    ) : null
  }

  return (
    <>
      <table className={s.logTable}>
        <TableColgroup isStaffVersion={props.isStaffVersion} />
        <tbody>{renderGroups()}</tbody>
      </table>
      {renderPreloader()}
    </>
  )
}

export default connect(() => ({}), {
  toggleActionExpandState,
  toggleSimilarActions,
  changeActionGroup
})(LogList)
