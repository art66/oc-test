import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { PRESET_TYPE, DEFAULT_PRESET } from '../../../../../../constants/recentHistory'
import { FOLLOW_LINK_DIMENSIONS } from '../../../../../../constants/recentHistory/icons'
import s from './index.cssmodule.scss'

class BottomLink extends Component {
  render() {
    return (
      <div className={`${s.historyFooter} recent-history-footer`}>
        <a className={s.link} href='/page.php?sid=log'>
          <SVGIconGenerator
            iconName='ViewLog'
            preset={{ type: PRESET_TYPE, subtype: DEFAULT_PRESET }}
            dimensions={FOLLOW_LINK_DIMENSIONS}
          />
          <span className={s.text}>View Log</span>
        </a>
      </div>
    )
  }
}

export default BottomLink
