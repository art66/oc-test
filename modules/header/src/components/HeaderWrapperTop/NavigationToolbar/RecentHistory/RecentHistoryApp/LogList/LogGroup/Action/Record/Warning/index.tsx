import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { GREY_PRESET, PRESET_TYPE } from '../../../../../../../../../../constants/recentHistory'
import { BUG_DIMENSIONS } from '../../../../../../../../../../constants/recentHistory/icons'
import s from './index.cssmodule.scss'

interface IProps {
  mode: string
}

class Warning extends Component<IProps> {
  getPreset = () => {
    const { mode } = this.props

    return mode === 'light' ? `${GREY_PRESET}_LIGHT` : GREY_PRESET
  }

  render() {
    return (
      <div className={s.warning}>
        <span className={s.iconWrapper}>
          <SVGIconGenerator
            iconName='Bug'
            preset={{ type: PRESET_TYPE, subtype: this.getPreset() }}
            dimensions={BUG_DIMENSIONS}
          />
        </span>
        <span className={s.text}>The log above may appear incorrectly</span>
      </div>
    )
  }
}

export default Warning
