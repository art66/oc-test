import React, { Component } from 'react'
import Preloader from '@torn/shared/components/Preloader'
import s from './index.cssmodule.scss'

class PreloaderWrapper extends Component<{}> {
  render() {
    return (
      <div className={s.preloaderWrapper}>
        <Preloader />
      </div>
    )
  }
}

export default PreloaderWrapper
