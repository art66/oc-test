import React, { Component } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import {
  BLUE_PRESET,
  PRESET_TYPE,
  SHOWING_COPY_MESSAGE_DURATION
} from '../../../../../../../../../../constants/recentHistory'
import { COPY_LOG_DIMENSIONS } from '../../../../../../../../../../constants/recentHistory/icons'
import IAction from '../../../../../../../../../../interfaces/recentHistory/IAction'
import IWindow from '../../../../../../../../../../interfaces/IWindow'
import { getActionTime } from '../../../../../../../../../../utils/timeHelpers'
import s from './index.cssmodule.scss'

interface IProps {
  action: IAction
  mode: string
  isStaffVersion: boolean
}

interface IState {
  isCopied: boolean
}

class CopySection extends Component<IProps, IState> {
  timeout: any
  constructor(props: IProps) {
    super(props)
    this.state = {
      isCopied: false
    }
  }

  shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    const { isCopied } = this.state
    const { mode } = this.props

    return isCopied !== nextState.isCopied || mode !== nextProps.mode
  }

  componentWillUnmount() {
    clearTimeout(this.timeout)
  }

  getPresetType = () => {
    const { mode } = this.props

    return mode === 'light' ? `${BLUE_PRESET}_LIGHT` : BLUE_PRESET
  }

  _handleCopyLog = e => {
    e.stopPropagation()
    const win = window as IWindow
    const {
      action: { time, ID }
    } = this.props
    const formattedTime = getActionTime(time)
    const textEl = document.getElementById(`text-${ID}`)
    const pureText = textEl.innerText || textEl.textContent
    const copiedText = `${formattedTime} ${pureText}`

    win.copyTextToClipboard(copiedText)
    this.changeCopyState(true)
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this.changeCopyState(false), SHOWING_COPY_MESSAGE_DURATION)
  }

  changeCopyState = isCopied => {
    this.setState({
      isCopied
    })
  }

  render() {
    const { isCopied } = this.state

    return (
      <div className={cn(s.copySection)}>
        <button type='button' className={s.copyBtn} onClick={this._handleCopyLog}>
          <span className={s.iconWrapper}>
            <SVGIconGenerator
              iconName='CopyLog'
              customClass={s.copyLogIcon}
              preset={{ type: PRESET_TYPE, subtype: this.getPresetType() }}
              dimensions={COPY_LOG_DIMENSIONS}
            />
          </span>
          <span className={s.text}>Copy Log</span>
        </button>
        {isCopied ? <span className={s.copyMsg}>Log copied!</span> : null}
      </div>
    )
  }
}

export default CopySection
