import React, { PureComponent } from 'react'
import cn from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/recentHistory'
import { ICONS, DEFAULT_CATEGORY } from '../../../../../../../../../../constants/recentHistory/icons'
import {
  POSITIVE_COLOR,
  NEGATIVE_COLOR,
  POSITIVE_LOG_PRESET,
  NEGATIVE_LOG_PRESET,
  DEFAULT_PRESET,
  PRESET_TYPE
} from '../../../../../../../../../../constants/recentHistory'
import IActionParams from '../../../../../../../../../../interfaces/recentHistory/IActionParams'
import s from './index.cssmodule.scss'

interface IProps {
  category: string
  params: IActionParams
  mode: string
}

class ActionIcon extends PureComponent<IProps> {
  getIconSubtype = () => {
    const { params } = this.props

    switch (params.color) {
      case POSITIVE_COLOR:
        return this.addPresetPrefix(POSITIVE_LOG_PRESET)
      case NEGATIVE_COLOR:
        return this.addPresetPrefix(NEGATIVE_LOG_PRESET)
      default:
        return this.addPresetPrefix(DEFAULT_PRESET)
    }
  }

  getIconParams = () => {
    const { category } = this.props

    return ICONS[category] || ICONS[DEFAULT_CATEGORY]
  }

  addPresetPrefix = (preset) => {
    const { mode } = this.props

    return `${preset}${mode === 'light' ? '_LIGHT' : ''}`
  }

  render() {
    const { name, dimensions } = this.getIconParams()

    return (
      <span className={cn(s.iconWrapper, 'icon-wrapper')}>
        <SVGIconGenerator
          iconsHolder={iconsHolder}
          iconName={name}
          preset={{ type: PRESET_TYPE, subtype: this.getIconSubtype() }}
          dimensions={dimensions}
        />
      </span>
    )
  }
}

export default ActionIcon
