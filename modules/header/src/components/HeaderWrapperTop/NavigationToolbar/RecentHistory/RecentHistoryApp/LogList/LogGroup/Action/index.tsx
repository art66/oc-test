import React, { PureComponent } from 'react'
import cn from 'classnames'
import IAction from '../../../../../../../../interfaces/recentHistory/IAction'
import ITimeSeparator from '../../../../../../../../interfaces/recentHistory/ITimeSeparator'
import SimilarActions from './SimilarActions'
import Record from './Record'
import { getActionOpacity } from '../../../../../../../../utils/actionHelpers'
import s from './index.cssmodule.scss'

interface IProps {
  action: IAction
  mode: string
  isStaffVersion: boolean
  timeSeparator: ITimeSeparator
  toggleSimilarActions: (actionId: string, open: boolean) => void
  toggleActionExpandState: (actionId: string, parentId: string | undefined) => void
  changeActionGroup: (
    oldTimeSeparator: ITimeSeparator,
    newTimeSeparator: ITimeSeparator,
    actionId: string,
    parentId: string | undefined
  ) => void
  darkModeEnabled: boolean
}

class Action extends PureComponent<IProps> {
  _handleExpandingRecord = e => {
    const {
      action: { ID },
      toggleActionExpandState: toggleAction
    } = this.props

    if (!e.ctrlKey && !e.metaKey) {
      toggleAction(ID, null)
    }
  }

  renderSimilarActions = () => {
    const { action, timeSeparator, mode, darkModeEnabled, isStaffVersion } = this.props

    return action.similar ? (
      <SimilarActions
        action={action}
        timeSeparator={timeSeparator}
        mode={mode}
        isStaffVersion={isStaffVersion}
        toggleSimilarActions={this.props.toggleSimilarActions}
        toggleActionExpandState={this.props.toggleActionExpandState}
        changeActionGroup={this.props.changeActionGroup}
        darkModeEnabled={darkModeEnabled}
      />
    ) : null
  }

  render() {
    const { action, timeSeparator, mode, darkModeEnabled, isStaffVersion } = this.props
    const opacity = getActionOpacity(timeSeparator)

    return (
      <>
        <tr
          className={cn(mode, { [s.active]: action.expanded })}
          style={{ opacity }}
          onClick={this._handleExpandingRecord}
        >
          <Record
            action={action}
            timeSeparator={timeSeparator}
            mode={mode}
            isStaffVersion={isStaffVersion}
            toggleActionExpandState={this.props.toggleActionExpandState}
            changeActionGroup={this.props.changeActionGroup}
            darkModeEnabled={darkModeEnabled}
          />
        </tr>
        {this.renderSimilarActions()}
      </>
    )
  }
}

export default Action
