import React, { PureComponent } from 'react'
import cn from 'classnames'
import ITimeSeparator from '../../../../../../../../interfaces/recentHistory/ITimeSeparator'
import { getActionOpacity } from '../../../../../../../../utils/actionHelpers'
import s from './index.cssmodule.scss'

interface IProps {
  timeSeparator: ITimeSeparator
  mode: string
}

class TimeSeparator extends PureComponent<IProps> {
  render() {
    const { timeSeparator, mode } = this.props
    const opacity = getActionOpacity(timeSeparator)

    return (
      <tr className={cn(s.timeSeparator, 'time-separator', mode)}>
        <td colSpan={2} style={{ opacity }}>
          {timeSeparator.number} {timeSeparator.label.toUpperCase()} AGO
        </td>
      </tr>
    )
  }
}

export default TimeSeparator
