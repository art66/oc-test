import React, { Component } from 'react'
import cn from 'classnames'
import Record from '../Record'
import IAction from '../../../../../../../../../interfaces/recentHistory/IAction'
import ITimeSeparator from '../../../../../../../../../interfaces/recentHistory/ITimeSeparator'
import { getActionOpacity } from '../../../../../../../../../utils/actionHelpers'
import s from './index.cssmodule.scss'
import TableColgroup from '../../../TableColgroup'

interface IProps {
  action: IAction
  timeSeparator: ITimeSeparator
  mode: string
  isStaffVersion: boolean
  toggleSimilarActions: (actionId: string, open: boolean) => void
  toggleActionExpandState: (actionId: string, parentId: string | undefined) => void
  changeActionGroup: (
    oldTimeSeparator: ITimeSeparator,
    newTimeSeparator: ITimeSeparator,
    actionId: string,
    parentId: string | undefined
  ) => void
  darkModeEnabled: boolean
}

class SimilarActions extends Component<IProps> {
  _handleTogglingSimilarActions = () => {
    const {
      action: {
        ID,
        similar: { open }
      },
      toggleSimilarActions: toggle
    } = this.props

    toggle(ID, !open)
  }

  _handleExpandingRecord = () => {
    const {
      action: { ID },
      toggleActionExpandState: toggleAction
    } = this.props

    toggleAction(ID, ID)
  }

  renderRecords = () => {
    const {
      action: {
        ID,
        similar: { log }
      },
      timeSeparator,
      mode,
      darkModeEnabled,
      isStaffVersion
    } = this.props

    return log.map(record => (
      <tr
        key={record.ID}
        className={cn({ [s.active]: record.expanded })}
        onClick={() => this.props.toggleActionExpandState(record.ID, ID)}
      >
        <Record
          action={record}
          mode={mode}
          isStaffVersion={isStaffVersion}
          timeSeparator={timeSeparator}
          parentId={ID}
          toggleActionExpandState={this.props.toggleActionExpandState}
          changeActionGroup={this.props.changeActionGroup}
          darkModeEnabled={darkModeEnabled}
        />
      </tr>
    ))
  }

  render() {
    const { action, mode, timeSeparator, isStaffVersion } = this.props
    const {
      similar: { log, open }
    } = action
    const opacity = getActionOpacity(timeSeparator)

    return (
      <>
        <tr className={cn(s.xSimilar, mode, { [s.staff]: isStaffVersion })} style={{ opacity }}>
          <td colSpan={2}>
            <button
              type='button'
              className={cn(s.xSimilarBtn, { [s.hide]: open })}
              onClick={this._handleTogglingSimilarActions}
            >
              <span className={cn(s.arrowWrapper, { [s.open]: open })} />
              <span className={s.label}>{log.length} similar</span>
            </button>
          </td>
        </tr>
        <tr className={cn(s.similarWrapper, mode, { [s.show]: open })} style={{ opacity }}>
          <td colSpan={2}>
            <table className={s.similarTable}>
              <TableColgroup isStaffVersion={isStaffVersion} />
              <tbody>{this.renderRecords()}</tbody>
            </table>
          </td>
        </tr>
      </>
    )
  }
}

export default SimilarActions
