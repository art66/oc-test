import React, { Component } from 'react'
import Tooltip from '@torn/shared/components/Tooltip'
import cn from 'classnames'
import ITimeSeparator from '../../../../../../../../../../interfaces/recentHistory/ITimeSeparator'
import { determineActionTimeSeparator, isEqualTimeSeparators } from '../../../../../../../../../../utils/actionHelpers'
import {
  getCurrentTimeInSeconds,
  getFormattedPassedTime,
  getActionTime
} from '../../../../../../../../../../utils/timeHelpers'
import s from './index.cssmodule.scss'

interface IProps {
  time: number
  actionId: string
  isStaffVersion: boolean
  timeSeparator: ITimeSeparator
  changeActionGroup: (
    oldTimeSeparator: ITimeSeparator,
    newTimeSeparator: ITimeSeparator,
    actionId: string,
    parentId: string | undefined
  ) => void
  parentId?: string
  darkModeEnabled: boolean
}

interface IState {
  timeValue: number
  label: string
  passedTime: number
}

class ActionTime extends Component<IProps, IState> {
  timer: any

  constructor(props: IProps) {
    super(props)
    const passedTime = this.getPassedTime()
    const { timeValue, label } = getFormattedPassedTime(passedTime)

    this.state = {
      timeValue,
      label,
      passedTime
    }
  }

  componentDidMount() {
    this.startTimer()
  }

  shouldComponentUpdate(nextProps, nextState: IState) {
    const { timeValue, label } = this.state
    const { darkModeEnabled } = this.props
    const { timeValue: nextTimeValue, label: nextLabel } = nextState
    const { darkModeEnabled: nextDarkModeEnabled } = nextProps

    return timeValue !== nextTimeValue || label !== nextLabel || darkModeEnabled !== nextDarkModeEnabled
  }

  componentWillUnmount() {
    this.stopTimer()
  }

  getPassedTime = () => {
    const { time } = this.props
    const currTime = getCurrentTimeInSeconds()

    return currTime - time < 0 ? 0 : currTime - time
  }

  getTipStyles = () => {
    const { darkModeEnabled } = this.props

    return darkModeEnabled ?
      {
        style: {
          background: '#444',
          zIndex: 999999
        },
        arrowStyle: {
          color: '#444',
          borderColor: 'rgba(0, 0, 0, 0.2)'
        }
      } :
      {
        style: {
          zIndex: 999999
        }
      }
  }

  updateActionGroup = passedTime => {
    const { timeSeparator: oldTimeSeparator, changeActionGroup: changeGroup, actionId, parentId } = this.props
    const newTimeSeparator = determineActionTimeSeparator(passedTime)

    if (!isEqualTimeSeparators(oldTimeSeparator, newTimeSeparator)) {
      changeGroup(oldTimeSeparator, newTimeSeparator, actionId, parentId)
    }
  }

  updateTime = () => {
    const { passedTime } = this.state
    const updatedPassedTime = passedTime + 1
    const { timeValue, label } = getFormattedPassedTime(updatedPassedTime)

    this.updateActionGroup(passedTime)

    this.setState({
      timeValue,
      label,
      passedTime: updatedPassedTime
    })
  }

  startTimer = () => {
    this.timer = setInterval(this.updateTime, 1000)
  }

  stopTimer = () => {
    clearInterval(this.timer)
  }

  renderTime = actionTime => {
    const { timeValue, label } = this.state
    const { isStaffVersion, actionId } = this.props

    return isStaffVersion ? (
      <>
        <span className={s.timeValue} id={`action-${actionId}`}>
          {actionTime}
        </span>
        <span style={{ width: 0, display: 'inline-block' }}>&nbsp;</span>
      </>
    ) : (
      <span className={cn(s.timeValue, s.shortened)} id={`action-${actionId}`} data-time-mark={`${timeValue}${label}`}>
        <span style={{ overflow: 'hidden', width: '1px', height: '1px', display: 'block' }}>{actionTime}</span>
      </span>
    )
  }

  render() {
    const { actionId, time, isStaffVersion } = this.props
    const tipStyle = this.getTipStyles()
    const actionTime = getActionTime(time)

    return (
      <span className={s.time}>
        {!isStaffVersion ? (
          <Tooltip parent={`action-${actionId}`} position='top' arrow='center' style={tipStyle}>
            {actionTime}
          </Tooltip>
        ) : null}
        {this.renderTime(actionTime)}
      </span>
    )
  }
}

export default ActionTime
