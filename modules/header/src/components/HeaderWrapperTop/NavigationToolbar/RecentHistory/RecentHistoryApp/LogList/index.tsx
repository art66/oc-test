import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import LogList from './LogList'
import { DISTANCE_BEFORE_LOAD_NEXT_CHUNK, DEFAULT_HEIGHT } from '../../../../../../constants/recentHistory'
import { loadNextChunk } from '../../../../../../actions/recentHistory'
import IGroup from '../../../../../../interfaces/recentHistory/IGroup'
import IStore from '../../../../../../interfaces/IStore'
import s from './index.cssmodule.scss'

interface IProps {
  history: IGroup[]
  isFetching: boolean
  logsEndReached: boolean
  loadNextChunk: () => void
  darkModeEnabled: boolean
}

class LogListWrapper extends PureComponent<IProps> {
  scrollArea: React.RefObject<HTMLDivElement>
  timer: any
  height: number
  constructor(props: IProps) {
    super(props)
    this.scrollArea = React.createRef()
  }

  componentDidMount() {
    this.height = Math.ceil(this.scrollArea.current.getBoundingClientRect().height)
    this.timer = setInterval(this.loadChunks, 200)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  loadChunks = () => {
    const { loadNextChunk: loadActions, isFetching, logsEndReached } = this.props

    if (!isFetching) {
      this.height = Math.ceil(this.scrollArea.current.getBoundingClientRect().height)

      if (this.height < DEFAULT_HEIGHT && !logsEndReached) {
        loadActions()
      }
    }

    if (logsEndReached || this.height >= DEFAULT_HEIGHT) {
      clearInterval(this.timer)
    }
  }

  _handleScroll = e => {
    e.stopPropagation()
    const { loadNextChunk: loadActions, isFetching, logsEndReached } = this.props
    const { height: containerHeight } = this.scrollArea.current.getBoundingClientRect()
    const { scrollTop, scrollHeight } = this.scrollArea.current
    const maxScrollTop = scrollHeight - containerHeight
    const diff = maxScrollTop - scrollTop

    if (diff <= DISTANCE_BEFORE_LOAD_NEXT_CHUNK && !isFetching && !logsEndReached) {
      loadActions()
    }
  }

  render() {
    const { isFetching, history, darkModeEnabled } = this.props

    return (
      <div className={`${s.logScrollArea} scrollarea`} onScroll={this._handleScroll} ref={this.scrollArea}>
        <LogList isFetching={isFetching} history={history} mode='dark' darkModeEnabled={darkModeEnabled} />
      </div>
    )
  }
}

const mapStateToProps = (state: IStore) => ({
  history: state.recentHistory.history,
  isFetching: state.recentHistory.isFetching,
  logsEndReached: !+state.recentHistory.startFrom,
  darkModeEnabled: state.header.rightMenu.darkModeEnabled
})

const mapDispatchToProps = {
  loadNextChunk
}

export default connect(mapStateToProps, mapDispatchToProps)(LogListWrapper)
