import React, { PureComponent } from 'react'
import cn from 'classnames'
import IActionParams from '../../../../../../../../../../interfaces/recentHistory/IActionParams'
import { POSITIVE_COLOR, NEGATIVE_COLOR } from '../../../../../../../../../../constants/recentHistory'
import s from './index.cssmodule.scss'

interface IProps {
  text: string
  isStaffVersion: boolean
  params: IActionParams
  parentId: string | undefined
  actionId: string
  toggleActionExpandState: (actionId: string, parentId: string | undefined) => void
  expanded: boolean
}

class ActionText extends PureComponent<IProps> {
  getTextClassNames = () => {
    const { params, expanded } = this.props

    return cn({
      'log-text': true,
      [s.text]: true,
      [s.collapsed]: !expanded,
      [s.positive]: params.color === POSITIVE_COLOR,
      [s.negative]: params.color === NEGATIVE_COLOR,
      [s.italic]: params.italic,
      [s.important]: params.bold,
      [s.corrupted]: params.inaccurate
    })
  }

  render() {
    const { text, actionId } = this.props
    const classNames = this.getTextClassNames()

    return <span id={`text-${actionId}`} className={classNames} dangerouslySetInnerHTML={{ __html: text }} />
  }
}

export default ActionText
