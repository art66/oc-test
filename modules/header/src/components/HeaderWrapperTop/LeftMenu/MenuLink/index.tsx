import React, { Component } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder'
import ILink from '../../../../interfaces/ILink'
import { LEFT_MENU_ICONS_DIMENSIONS, DEFAULT_PRESET } from '../../../../constants/icons'

interface IProps {
  link: ILink
  isDesktop: boolean
}

class MenuLink extends Component<IProps> {
  renderIcon = () => {
    const {
      isDesktop,
      link: { icon }
    } = this.props

    return !isDesktop ? (
      <div className='icon-wrapper'>
        <SVGIconGenerator
          iconsHolder={iconsHolder}
          iconName={icon}
          preset={DEFAULT_PRESET}
          dimensions={LEFT_MENU_ICONS_DIMENSIONS[icon]}
        />
      </div>
    ) : null
  }

  render() {
    const {
      link: { name, link, inNewTab }
    } = this.props
    const target = inNewTab ? { target: '_blank' } : null

    return (
      <li className='menu-item-link'>
        <a href={link} {...target}>
          {this.renderIcon()}
          <span className='link-text'>{name}</span>
        </a>
      </li>
    )
  }
}

export default MenuLink
