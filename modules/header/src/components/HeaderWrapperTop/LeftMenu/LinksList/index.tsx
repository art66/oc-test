import React, { Component } from 'react'
import MenuLink from '../MenuLink'
import ILink from '../../../../interfaces/ILink'

interface IProps {
  links: ILink[]
  isDesktop: boolean
}

class LinksList extends Component<IProps> {
  renderLinks = () => {
    const { links, isDesktop } = this.props

    return links.map(link => <MenuLink key={link.name} isDesktop={isDesktop} link={link} />)
  }

  render() {
    return <ul className="menu-items clearfix">{this.renderLinks()}</ul>
  }
}

export default LinksList
