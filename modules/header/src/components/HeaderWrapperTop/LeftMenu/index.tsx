import React, { Component, RefObject } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import { IBrowser } from 'redux-responsive'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import headerIconsHolder from '@torn/shared/SVG/helpers/iconsHolder/header'
import { DEFAULT_PRESET, MENU_ICON_DIMENSIONS } from '../../../constants/icons'
import LinksList from './LinksList'
import { isDesktopView, isOutsideElement } from '../../../utils'
import { toggleTopLeftMenu } from '../../../actions'
import ILeftMenu from '../../../interfaces/ILeftMenu'
import s from './index.cssmodule.scss'

interface IProps {
  browser: IBrowser
  leftMenu: ILeftMenu
  toggleTopLeftMenu: (open: boolean) => void
}

class LeftMenu extends Component<IProps> {
  leftMenuRef: RefObject<HTMLDivElement>
  constructor(props) {
    super(props)
    this.leftMenuRef = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
  }

  handleClickOutside = event => {
    const {
      leftMenu: { open },
      toggleTopLeftMenu: toggleMenu,
      browser: { mediaType }
    } = this.props
    const isDesktop = isDesktopView(mediaType)
    const isOutside = isOutsideElement(this.leftMenuRef, event.target)

    if (open && !isDesktop && isOutside) {
      toggleMenu(false)
    }
  }

  _handleToggleMenu = () => {
    const {
      leftMenu: { open },
      toggleTopLeftMenu: toggleMenu
    } = this.props

    toggleMenu(!open)
  }

  renderMenuButton = isDesktop => {
    const {
      leftMenu: { open }
    } = this.props

    return !isDesktop ? (
      <button
        type="button"
        className="top_header_button header-menu-icon"
        aria-label={`${open ? 'Close' : 'Open'} menu`}
        onClick={this._handleToggleMenu}
      >
        <SVGIconGenerator
          iconsHolder={headerIconsHolder}
          iconName="Menu"
          preset={DEFAULT_PRESET}
          dimensions={MENU_ICON_DIMENSIONS}
        />
      </button>
    ) : null
  }

  render() {
    const {
      browser: { mediaType },
      leftMenu: { open, links }
    } = this.props
    const isDesktop = isDesktopView(mediaType)
    const c = cn('header-menu', 'left', s.leftMenu, {
      active: open
    })

    return (
      <div ref={this.leftMenuRef} className={c}>
        {this.renderMenuButton(isDesktop)}
        {open || isDesktop ? <LinksList isDesktop={isDesktop} links={links} /> : null}
      </div>
    )
  }
}

export default connect(
  (state: any) => ({
    browser: state.browser
  }),
  {
    toggleTopLeftMenu
  }
)(LeftMenu)
