import React, { Component } from 'react'
import SVGIconGenerator from '@torn/shared/SVG/icons'
import { DEFAULT_PRESET, LOGIN_ICON_DIMENSIONS } from '../../../constants/icons'
import { removeHeaderDataFromStorage } from '../../../utils/storageHelper'

interface IProps {}

class LoginForm extends Component<IProps> {
  formSubmitted: boolean
  constructor(props: IProps) {
    super(props)
    this.formSubmitted = false
  }

  handleSubmit = e => {
    if (this.formSubmitted) {
      e.preventDefault()
    }

    this.formSubmitted = true
    removeHeaderDataFromStorage()
  }

  render() {
    return (
      <div className='log-in-wrap'>
        <div className='log-in hide-checkbox'>
          <form id='frmLogin' name='login' method='post' action='/authenticate.php' onSubmit={this.handleSubmit}>
            <div style={{ marginBottom: '-3px' }}>
              <a
                id='recover-account-link'
                className='recover-account-link headerLink'
                title='Recover account'
                href='/account_recovery.php'
              >
                Recover account
              </a>
            </div>
            <div className='input-wrapper first-input input-wrapper--container left'>
              <input id='player' className='txtbx' type='email' placeholder='email address' name='player' />
            </div>
            <div className='input-wrapper input-wrapper--container left'>
              <input id='password' className='txtbx' type='password' placeholder='password' name='password' />
            </div>
            <span className='login-wrap clear'>
              <span className='svg-wrap'>
                <SVGIconGenerator iconName='Login' preset={DEFAULT_PRESET} dimensions={LOGIN_ICON_DIMENSIONS} />
              </span>
              <input type='hidden' name='redirectUrl' value={window.location.href} />
              <input className='login' type='submit' value='Login' name='btnLogin' />
            </span>
          </form>
        </div>
      </div>
    )
  }
}

export default LoginForm
