import React, { PureComponent } from 'react'
import Logo from './Logo'
import LeftMenu from './LeftMenu'
import ISettings from '../../interfaces/ISettings'
import IUser from '../../interfaces/IUser'
import ILogo from '../../interfaces/ILogo'
import ILeftMenu from '../../interfaces/ILeftMenu'
import NavigationToolbar from './NavigationToolbar'
import LoginForm from './LoginForm'
import { isFederalJailed } from '../../utils'
import IGlobalSearch from '../../interfaces/IGlobalSearch'

interface IProps {
  settings: ISettings
  user: IUser
  logo: ILogo
  leftMenu: ILeftMenu
  globalSearch: IGlobalSearch
}

class HeaderWrapperTop extends PureComponent<IProps> {
  renderHeaderNavigation = () => {
    const {
      settings: { emptyHeader },
      user,
      globalSearch
    } = this.props
    const {
      state: { isLoggedIn }
    } = user

    return !emptyHeader ? (
      <div className="header-navigation right">
        {isLoggedIn ? <NavigationToolbar user={user} globalSearch={globalSearch} /> : <LoginForm />}
      </div>
    ) : null
  }

  render() {
    const { settings, user, logo, leftMenu } = this.props
    const { emptyLinks } = settings
    const {
      state: { status }
    } = user
    const isFedJailed = isFederalJailed(status)

    return (
      <div className="header-wrapper-top">
        <div className="container clearfix">
          <Logo user={user} settings={settings} logo={logo} />
          {!emptyLinks && !isFedJailed ? <LeftMenu leftMenu={leftMenu} /> : null}
          {this.renderHeaderNavigation()}
        </div>
      </div>
    )
  }
}

export default HeaderWrapperTop
