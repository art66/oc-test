import React, { Component } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { initNewsTicker, tabHidden, tabVisible } from '../../actions/newsTicker'
import HeadlinesSlider from './HeadlinesSlider'
import { shouldShowHeadlines } from '../../selectors/newsTicker'

type TProps = TReduxProps

class HeaderWrapperBottom extends Component<TProps> {
  componentDidMount() {
    window.addEventListener('visibilitychange', this.visibilityChangeHandler)
    this.props.initNewsTicker()
  }

  componentWillUnmount() {
    window.removeEventListener('visibilitychange', this.visibilityChangeHandler)
  }

  visibilityChangeHandler = () => {
    document.visibilityState === 'hidden' ? this.props.tabHidden() : this.props.tabVisible()
  }

  renderContent = () => {
    return this.props.shouldShowHeadlines && <HeadlinesSlider />
  }

  render() {
    return (
      <div className='header-wrapper-bottom'>
        <div className='container'>
          <div className='header-bottom-text news-ticker-new'>{this.renderContent()}</div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  shouldShowHeadlines: shouldShowHeadlines(state)
})

const mapDispatchToProps = {
  initNewsTicker,
  tabHidden,
  tabVisible
}

const connector = connect(mapStateToProps, mapDispatchToProps)

export type TReduxProps = ConnectedProps<typeof connector>

export default connector(HeaderWrapperBottom)
