/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { getActiveHeadline } from '../../../selectors/newsTicker'
import Headline from '../Headline'
import './index.cssmodule.scss'

type TProps = TReduxProps

class HeadlinesSlider extends Component<TProps> {
  render() {
    const { activeHeadline } = this.props

    if (!activeHeadline) {
      return null
    }

    return (
      <div id='header-swiper-container' className='header-swiper-container swiper-no-swiping'>
        <TransitionGroup id='news-ticker-slider-wrapper' className='news-ticker-slider-wrapper'>
          <CSSTransition key={activeHeadline.headline} classNames='news-ticker' timeout={900}>
            <Headline headline={activeHeadline} />
          </CSSTransition>
        </TransitionGroup>
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  activeHeadline: getActiveHeadline(state)
})

const connector = connect(mapStateToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(HeadlinesSlider)
