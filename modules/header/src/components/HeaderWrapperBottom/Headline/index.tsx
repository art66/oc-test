import cn from 'classnames'
import React, { Component, RefObject } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { scrollEnded, scrollStarted, headlineClick } from '../../../actions/newsTicker'
import IHeadline from '../../../interfaces/IHeadline'
import { getIsTravelling, getMediaType, getUserStatus } from '../../../selectors'
import CountDown from '../CowntDown/CountDown'
import HeadlineIcon from '../HeadlineIcon'

type TProps = TReduxProps & {
  headline: IHeadline
}

/* tslint:disable:jsx-no-lambda */
class Headline extends Component<TProps> {
  countdownInterval: number
  scrollWrapRef: RefObject<HTMLDivElement>

  constructor(props: TProps) {
    super(props)

    this.scrollWrapRef = React.createRef()
  }

  getAriaLabelMessage = () => {
    const {
      headline: { type, isGlobal }
    } = this.props

    return `${isGlobal ? 'Urgent ' : ''}${type} headline`
  }

  handleTouchStar = () => {
    this.props.scrollStarted()
  }

  handleTouchEnd = () => {
    this.props.scrollEnded()

    this.scrollWrapRef.current.scroll({
      top: 0,
      left: -this.scrollWrapRef.current.scrollLeft,
      behavior: 'smooth'
    })
  }

  renderShadow = side => {
    const { mediaType } = this.props

    return mediaType !== 'desktop' && <div className={`${side}-shadow-box active`} />
  }

  renderHeadlineContent = () => {
    const { headline } = this.props

    return (
      <span className='headline-content'>
        <span className='' dangerouslySetInnerHTML={{ __html: headline.headline }} />
        {headline.endTime ? <CountDown endTime={headline.endTime} diffClientServerTime={0} /> : null}
      </span>
    )
  }

  render() {
    const { headline, status, isTravelling } = this.props
    const areaLabel = this.getAriaLabelMessage()
    const slideStyles = cn({
      'news-ticker-slide': true,
      announcement: headline.isGlobal,
      [headline.type]: true
    })

    return (
      // eslint-disable-next-line jsx-a11y/click-events-have-key-events
      <div className={slideStyles} data-type={headline.type} onClick={this.props.headlineClick}>
        {this.renderShadow('left')}
        <div
          className='scroll-wrap'
          ref={this.scrollWrapRef}
          onTouchEnd={this.handleTouchEnd}
          onTouchStart={this.handleTouchStar}
        >
          <HeadlineIcon type={headline.type} isGlobal={headline.isGlobal} status={status} isTravelling={isTravelling} />
          {headline.link ? (
            <a
              href={headline.link}
              className={cn('headline', status)}
              aria-label={areaLabel}
              onClick={e => e.stopPropagation()}
            >
              {this.renderHeadlineContent()}
            </a>
          ) : (
            <span className={cn('headline', status)} aria-label={areaLabel}>
              {this.renderHeadlineContent()}
            </span>
          )}
        </div>
        {this.renderShadow('right')}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  status: getUserStatus(state),
  isTravelling: getIsTravelling(state),
  mediaType: getMediaType(state)
})

const mapDispatchToProps = {
  scrollEnded,
  scrollStarted,
  headlineClick
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type TReduxProps = ConnectedProps<typeof connector>

export default connector(Headline)
