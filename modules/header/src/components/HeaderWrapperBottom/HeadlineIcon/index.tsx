import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsHolder from '@torn/shared/SVG/helpers/iconsHolder/newsticker'
import cn from 'classnames'
import React, { Component } from 'react'
import { NEWS_TICKER_ICONS } from '../../../constants/icons'
import { PRESET } from '../../../constants/newsTicker'
import { HeadlinesType } from '../../../interfaces/IHeadline'
import s from './index.cssmodule.scss'

interface IProps {
  type: HeadlinesType
  isGlobal: boolean
  status: string
  isTravelling: boolean
}

class HeadlineIcon extends Component<IProps> {
  getIconData = () => {
    const { type, isGlobal, status, isTravelling } = this.props
    const userStatus = status === 'ok' && isTravelling ? 'traveling' : status

    let icon

    if ([HeadlinesType.Donation, HeadlinesType.Tutorial, HeadlinesType.GameClosed].includes(type)) {
      icon = type
    } else if (type === HeadlinesType.Faction) {
      icon = isGlobal ? 'urgentFaction' : HeadlinesType.Faction
    } else {
      icon = isGlobal ? 'announcement' : 'news'
    }

    const { name } = NEWS_TICKER_ICONS[icon]
    const preset = NEWS_TICKER_ICONS[icon].subtype[userStatus]

    return {
      name,
      preset
    }
  }

  render() {
    const icon = this.getIconData()

    return (
      <SVGIconGenerator
        customClass={cn(s.iconSVG, s[icon.name])}
        iconsHolder={iconsHolder}
        iconName={icon.name}
        preset={{ type: PRESET, subtype: icon.preset }}
      />
    )
  }
}

export default HeadlineIcon
