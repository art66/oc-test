import React from 'react'
import { formatTimeWithDays } from '@torn/shared/utils/formatTimeWithDays'
import { getCurrentTimestamp } from '../../../utils/newsTickerHelper'

interface IProps {
  endTime: number
  diffClientServerTime: number
}

interface IState {
  currentServerTime: number
}

class CountDown extends React.Component<IProps, IState> {
  interval: any

  constructor(props: IProps) {
    super(props)

    const { diffClientServerTime } = this.props

    this.state = {
      currentServerTime: getCurrentTimestamp() + diffClientServerTime
    }
  }

  componentDidMount() {
    const { diffClientServerTime } = this.props

    this.interval = setInterval(
      () =>
        this.setState({
          currentServerTime: getCurrentTimestamp() + diffClientServerTime
        }),
      1000
    )
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  render() {
    const { currentServerTime } = this.state
    const { endTime } = this.props
    const headlineTime = endTime <= currentServerTime ? 0 : endTime - currentServerTime

    return <span className='news-ticker-countdown'>{`[${formatTimeWithDays(headlineTime)}]`}</span>
  }
}

export default CountDown
