import React from 'react'
import CountDown from './CountDown'

export const countDown = () => <CountDown endTime={1605174601} diffClientServerTime={1} />

countDown.story = {
  name: 'CountDown'
}

export default {
  title: 'NewsTicker/CountDown'
}
