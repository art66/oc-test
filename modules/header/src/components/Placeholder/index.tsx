import React, { Component } from 'react'
import cn from 'classnames'
import { getCookie } from '@torn/shared/utils'

interface IProps {}

class Placeholder extends Component<IProps> {
  getPlaceholderClassNames = () => {
    return cn('header', 'msg', 'responsive-sidebar-header', {
      'logged-out': !getCookie('PHPSESSID')
    })
  }

  render() {
    return (
      <div id="topHeader" className={this.getPlaceholderClassNames()}>
        <div className="header-wrapper-top" />
        <div className="header-wrapper-bottom" />
      </div>
    )
  }
}

export default Placeholder
