import cn from 'classnames'
import React, { Component } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { fetchInitialHeaderData, fetchUpdatedHeaderData, setInitialHeaderData, toggleDarkMode } from '../actions'
import { HOSPITAL_USER_STATUS, JAIL_USER_STATUS, LOGGED_OUT_USER_STATUS, TOGGLE_DARK_MODE_EVENT } from '../constants'
import HeaderWrapperBottom from './HeaderWrapperBottom'
import IWindow from '../interfaces/IWindow'
import { getHeaderDataFromStorage } from '../utils/storageHelper'
import HeaderWrapperTop from './HeaderWrapperTop'
import Placeholder from './Placeholder'
declare const window: IWindow

type TProps = TConnectedProps

class App extends Component<TProps> {
  componentDidMount() {
    const savedData = getHeaderDataFromStorage()

    savedData && this.props.setInitialHeaderData(savedData)
    this.props.fetchInitialHeaderData()

    window.WebsocketHandler.addEventListener('sidebar', 'onLeaveFromJail', this.handleChangeHospitalJailState)
    window.WebsocketHandler.addEventListener('sidebar', 'onRevive', this.handleChangeHospitalJailState)
    window.WebsocketHandler.addEventListener('sidebar', 'onHospital', this.handleChangeHospitalJailState)
    window.WebsocketHandler.addEventListener('sidebar', 'onJail', this.handleChangeHospitalJailState)
    window.addEventListener(TOGGLE_DARK_MODE_EVENT, this.props.toggleDarkMode)
  }

  componentWillUnmount() {
    window.removeEventListener(TOGGLE_DARK_MODE_EVENT, this.props.toggleDarkMode)
  }

  getHeaderClassNames = (user, logo) => {
    const {
      state: { status }
    } = user
    const { name } = logo
    const isLoggedOut = status === LOGGED_OUT_USER_STATUS

    return cn('header', 'msg', 'responsive-sidebar-header', {
      'logged-out': isLoggedOut,
      [name]: !isLoggedOut && name
    })
  }

  getJailHospitalStamp = user => {
    const {
      state: { status },
      data: { jailStamp, hospitalStamp }
    } = user
    const isInJail = status === JAIL_USER_STATUS
    const isInHospital = status === HOSPITAL_USER_STATUS

    if (isInJail && jailStamp) {
      return {
        'data-jail': jailStamp
      }
    } else if (isInHospital && hospitalStamp) {
      return {
        'data-hospital': hospitalStamp
      }
    }

    return null
  }

  handleChangeHospitalJailState = () => {
    const { fetchUpdatedHeaderData: getUpdateHeaderData } = this.props

    getUpdateHeaderData()
  }

  render() {
    const {
      header: { settings, user, logo, leftMenu, globalSearch }
    } = this.props
    const savedData = getHeaderDataFromStorage()
    const userData = user || savedData?.user
    const settingsData = settings || savedData?.settings
    const logoData = logo || savedData?.logo
    const leftMenuData = leftMenu.links ? leftMenu : { open: false, links: savedData?.links }
    const globalSearchData = globalSearch.optionsList ? globalSearch : savedData?.globalSearch

    if (!userData) {
      return <Placeholder />
    }

    const headerClassNames = this.getHeaderClassNames(userData, logoData)
    const hospitalJailStamp = this.getJailHospitalStamp(userData)

    return (
      <div id='topHeaderBanner' className={headerClassNames} {...hospitalJailStamp}>
        <HeaderWrapperTop
          settings={settingsData}
          user={userData}
          logo={logoData}
          leftMenu={leftMenuData}
          globalSearch={globalSearchData}
        />
        <HeaderWrapperBottom />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    header: state.header,
    recentHistory: state.recentHistory,
    newsTicker: state.newsTicker
  }
}

const mapDispatchToProps = {
  fetchInitialHeaderData,
  setInitialHeaderData,
  fetchUpdatedHeaderData,
  toggleDarkMode
}

const connector = connect(mapStateToProps, mapDispatchToProps)

export type TConnectedProps = ConnectedProps<typeof connector>

export default connector(App)
