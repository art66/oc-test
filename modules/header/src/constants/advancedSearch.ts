export const GENDER_SELECTOR = 'gender'
export const PROPERTY_SELECTOR = 'properties'
export const CONDITION_SELECTOR = 'searchConditions'
export const LAST_ACTION_SELECTOR = 'lastAction'
export const CONDITION_NOT_CHECKBOX = 'searchConditionNot'
export const LEVEL_FROM_INPUT = 'levelFrom'
export const LEVEL_TO_INPUT = 'levelTo'
export const DAYS_OLD_FROM_INPUT = 'daysOldFrom'
export const DAYS_OLD_TO_INPUT = 'daysOldTo'
export const OFFENCES_FROM_INPUT = 'offencesFrom'
export const OFFENCES_TO_INPUT = 'offencesTo'
export const FACTION_AUTOCOMPLETE = 'faction'
export const FACTION_ID_AUTOCOMPLETE = 'factionID'
export const FACTIONS_FIELD_NAME = 'factions'
export const PLAYER_NAME_INPUT = 'playername'
export const SID_INPUT = 'sid'

export const EMPTY_DROPDOWN_VALUE = { id: '', name: '' }

export const PROPS_FOR_RANGES = {
  level: {
    label: 'Level',
    inputId: { from: 'search-levelFrom', to: 'search-levelTo' },
    inputName: { from: LEVEL_FROM_INPUT, to: LEVEL_TO_INPUT }
  },
  daysOld: {
    label: 'Days old',
    inputId: { from: 'search-daysOldFrom', to: 'search-daysOldTo' },
    inputName: { from: DAYS_OLD_FROM_INPUT, to: DAYS_OLD_TO_INPUT }
  },
  offences: {
    label: 'Offences',
    inputId: { from: 'search-offencesFrom', to: 'search-offencesTo' },
    inputName: { from: OFFENCES_FROM_INPUT, to: OFFENCES_TO_INPUT }
  }
}
