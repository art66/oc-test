export const DEFAULT_USER_STATUS = 'ok'
export const JAIL_USER_STATUS = 'jail'
export const HOSPITAL_USER_STATUS = 'hospital'
export const MULTIJAIL_USER_STATUS = 'multijail'
export const LOGGED_OUT_USER_STATUS = 'logged-out'
export const BIRTHDAY_LOGO_NAME = 'torn-birthday'

export const SEARCH_PLAYER_OPTION = 'player'
export const SEARCH_FACTION_OPTION = 'faction'
export const SEARCH_COMPANY_OPTION = 'company'
export const SEARCH_PLACE_OPTION = 'places'
export const SEARCH_ITEM_MARKET_OPTION = 'imarket'
export const SEARCH_FORUM_POSTS_OPTION = 'forum'
export const SEARCH_HELP_OPTION = 'wiki'

export const SEARCH_COMPANY_MAX_STARS_AMOUNT = 10

export const KEY_CODE_TAB = 9
export const KEY_CODE_ENTER = 13
export const KEY_CODE_ESCAPE = 27
export const KEY_CODE_ARROW_UP = 38
export const KEY_CODE_ARROW_DOWN = 40

export const DARK_MODE_SETTING = 'darkModeEnabled'
export const DESKTOP_MODE_SETTING = 'desktopModeEnabled'
export const NEWS_TICKER_SETTING = 'newsTickerEnabled'

export const DARK_MODE_CLASS_NAME = 'dark-mode'

export const TOGGLE_DARK_MODE_EVENT = 'onChangeTornMode'
export const SETTINGS_CHANNEL_NAME = 'SettingsBroadcastChannel'
