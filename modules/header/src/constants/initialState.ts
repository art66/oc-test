import { getDarkModeState, getDesktopModeState, getNewsTickerState, getTCClockState } from '../utils'

export const initialState = {
  headerDataIsFetched: false,
  advancedSearch: {
    open: false,
    formData: {
      sid: 'UserList'
    }
  },
  globalSearch: {
    inputValue: ''
  },
  leftMenu: {
    open: false
  },
  rightMenu: {
    open: false,
    desktopModeEnabled: getDesktopModeState(),
    darkModeEnabled: getDarkModeState(),
    newsTickerEnabled: getNewsTickerState()
  },
  TCClock: {
    open: getTCClockState()
  },
  recentHistory: {
    open: false
  }
}

export default initialState
