import { HeadlinesType } from '../interfaces/IHeadline'

export const SLIDER_INTERVAL = 15000
export const SLIDE_HEIGHT_DESKTOP = 30
export const SLIDE_HEIGHT_MOBILE = 25
export const COOKIE_EXPIRES_IN_DAYS = 3650 // 10 years
export const SECONDS_IN_HOUR = 3600
export const SECONDS_IN_MINUTE = 60
export const HOURS_IN_DAY = 24
export const SLIDE_ANIMATION_DURATION = 500
export const SLIDES_LIMIT = 10
export const NEWS_TICKER_COOKIE_NAME = 'newsTicker'
export const NEWS_TICKER_IS_ENABLED = 'newsTickerIsEnabled'
export const ACTIVE_HEADLINE = 'activeHeadlineReact'

export const PRESET = 'newsTicker'
export const STATUS_FOR_VIEW_HEADLINES = ['ok', 'jail', 'hospital']

export const MOBILE_MEDIA_TYPE = 'mobile'

export const URGENT_HEADLINES_TYPES_OFF = [
  HeadlinesType.General,
  HeadlinesType.Tutorial,
  HeadlinesType.Maintenance,
  HeadlinesType.GameClosed
]
