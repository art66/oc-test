const getDimensions = (width, height) => ({
  width,
  height,
  viewbox: `0 0 ${width} ${height}`
})

const DEFAULT_DIMENSIONS = getDimensions(16, 16)

export const BUG_DIMENSIONS = {
  ...getDimensions(11, 11),
  viewbox: '0 0 20 24'
}

export const FOLLOW_LINK_DIMENSIONS = getDimensions(12, 9)

export const COPY_LOG_DIMENSIONS = getDimensions(14, 14)

export const ICONS = {
  Accountclosure: {
    name: 'RHAccountclosure',
    dimensions: getDimensions(15.95, 15.95)
  },
  Accountcreation: {
    name: 'RHAccountcreation',
    dimensions: DEFAULT_DIMENSIONS
  },
  Accountrecovery: {
    name: 'RHAccountrecovery',
    dimensions: getDimensions(18.01, 15.75)
  },
  Adverts: {
    name: 'RHAdverts',
    dimensions: getDimensions(14.4, 16)
  },
  Ammo: {
    name: 'RHAmmo',
    dimensions: getDimensions(15.41, 16.05)
  },
  Attacking: {
    name: 'RHAttacking',
    dimensions: getDimensions(18.01, 12.7)
  },
  Auctions: {
    name: 'RHAuctions',
    dimensions: DEFAULT_DIMENSIONS
  },
  Authentication: {
    name: 'RHAuthentication',
    dimensions: DEFAULT_DIMENSIONS
  },
  Awards: {
    name: 'RHAwards',
    dimensions: getDimensions(14, 16)
  },
  Bail: {
    name: 'RHBail',
    dimensions: getDimensions(14.67, 16)
  },
  Bank: {
    name: 'RHBank',
    dimensions: DEFAULT_DIMENSIONS
  },
  Bazaars: {
    name: 'RHBazaars',
    dimensions: getDimensions(16, 16.2)
  },
  Books: {
    name: 'RHBooks',
    dimensions: getDimensions(15, 16)
  },
  Bounties: {
    name: 'RHBounties',
    dimensions: getDimensions(18, 18)
  },
  Captcha: {
    name: 'RHCaptcha',
    dimensions: getDimensions(14, 13.96)
  },
  Casino: {
    name: 'RHCasino',
    dimensions: DEFAULT_DIMENSIONS
  },
  Chat: {
    name: 'RHChat',
    dimensions: getDimensions(16, 14.67)
  },
  Christmastown: {
    name: 'RHChristmastown',
    dimensions: getDimensions(18, 18)
  },
  Church: {
    name: 'RHChurch',
    dimensions: DEFAULT_DIMENSIONS
  },
  Cityfinds: {
    name: 'RHCityfinds',
    dimensions: getDimensions(12, 18)
  },
  Company: {
    name: 'RHCompany',
    dimensions: getDimensions(15, 16)
  },
  Competitions: {
    name: 'RHCompetitions',
    dimensions: getDimensions(20, 14.28)
  },
  Crimes: {
    name: 'RHCrimes',
    dimensions: getDimensions(18, 18)
  },
  Displaycabinet: {
    name: 'RHDisplaycabinet',
    dimensions: DEFAULT_DIMENSIONS
  },
  Donator: {
    name: 'RHDonator',
    dimensions: getDimensions(16, 15.22)
  },
  Drugs: {
    name: 'RHDrugs',
    dimensions: getDimensions(16, 17)
  },
  Dump: {
    name: 'RHDump',
    dimensions: getDimensions(18, 10.01)
  },
  Education: {
    name: 'RHEducation',
    dimensions: getDimensions(16, 12.31)
  },
  Enemieslist: {
    name: 'RHEnemieslist',
    dimensions: getDimensions(16, 13.84)
  },
  Equipping: {
    name: 'RHEquipping',
    dimensions: DEFAULT_DIMENSIONS
  },
  Events: {
    name: 'RHEvents',
    dimensions: DEFAULT_DIMENSIONS
  },
  Faction: {
    name: 'RHFaction',
    dimensions: getDimensions(12.47, 17)
  },
  Forums: {
    name: 'RHForums',
    dimensions: getDimensions(15.99, 14.67)
  },
  Friendslist: {
    name: 'RHFriendslist',
    dimensions: getDimensions(16, 13.84)
  },
  Gym: {
    name: 'RHGym',
    dimensions: getDimensions(20, 9.9)
  },
  Halloweenbasket: {
    name: 'RHHalloweenbasket',
    dimensions: getDimensions(16, 15)
  },
  Hospital: {
    name: 'RHHospital',
    dimensions: DEFAULT_DIMENSIONS
  },
  Ignorelist: {
    name: 'RHIgnorelist',
    dimensions: DEFAULT_DIMENSIONS
  },
  Images: {
    name: 'RHImages',
    dimensions: getDimensions(16, 12)
  },
  Itemmarket: {
    name: 'RHItemmarket',
    dimensions: getDimensions(18, 15)
  },
  Items: {
    name: 'RHItems',
    dimensions: getDimensions(14.67, 16)
  },
  Itemsending: {
    name: 'RHItemsending',
    dimensions: getDimensions(18, 14)
  },
  Itemuse: {
    name: 'RHItemuse',
    dimensions: DEFAULT_DIMENSIONS
  },
  Jail: {
    name: 'RHJail',
    dimensions: getDimensions(16.06, 16)
  },
  Job: {
    name: 'RHJob',
    dimensions: getDimensions(16, 13.33)
  },
  Levelup: {
    name: 'RHLevelup',
    dimensions: getDimensions(14.67, 16)
  },
  Loan: {
    name: 'RHLoan',
    dimensions: getDimensions(15, 18)
  },
  Merits: {
    name: 'RHMerits',
    dimensions: getDimensions(18, 18)
  },
  Messages: {
    name: 'RHMessages',
    dimensions: getDimensions(16, 11.56)
  },
  Misc: {
    name: 'RHMisc',
    dimensions: {
      ...getDimensions(15, 19)
    }
  },
  Missions: {
    name: 'RHMissions',
    dimensions: getDimensions(16, 14.66)
  },
  Mods: {
    name: 'RHMods',
    dimensions: getDimensions(16, 14)
  },
  Money: {
    name: 'RHMoney',
    dimensions: getDimensions(9.33, 16)
  },
  Moneysending: {
    name: 'RHMoneysending',
    dimensions: DEFAULT_DIMENSIONS
  },
  Museum: {
    name: 'RHMuseum',
    dimensions: getDimensions(16, 15)
  },
  Newsletters: {
    name: 'RHNewsletters',
    dimensions: getDimensions(16, 13.33)
  },
  Notes: {
    name: 'RHNotes',
    dimensions: getDimensions(14, 14)
  },
  Parcels: {
    name: 'RHParcels',
    dimensions: getDimensions(16, 14.67)
  },
  Pointsbuilding: {
    name: 'RHPointsbuilding',
    dimensions: getDimensions(18, 18)
  },
  Pointsmarket: {
    name: 'RHPointsmarket',
    dimensions: getDimensions(18, 16.2)
  },
  Preferences: {
    name: 'RHPreferences',
    dimensions: DEFAULT_DIMENSIONS
  },
  Property: {
    name: 'RHProperty',
    dimensions: DEFAULT_DIMENSIONS
  },
  Racing: {
    name: 'RHRacing',
    dimensions: getDimensions(15.01, 14)
  },
  Referrals: {
    name: 'RHReferrals',
    dimensions: getDimensions(16.04, 16.09)
  },
  Reporting: {
    name: 'RHReporting',
    dimensions: getDimensions(16, 14.67)
  },
  Seasonalgift: {
    name: 'RHSeasonalgift',
    dimensions: DEFAULT_DIMENSIONS
  },
  Shops: {
    name: 'RHShops',
    dimensions: getDimensions(16, 12)
  },
  Staff: {
    name: 'RHStaff',
    dimensions: DEFAULT_DIMENSIONS
  },
  Stocks: {
    name: 'RHStocks',
    dimensions: getDimensions(15, 16)
  },
  Tokenshop: {
    name: 'RHTokenshop',
    dimensions: getDimensions(18, 18)
  },
  Trades: {
    name: 'RHTrades',
    dimensions: getDimensions(15.23, 18)
  },
  Travel: {
    name: 'RHTravel',
    dimensions: getDimensions(15.99, 14.93)
  },
  Vault: {
    name: 'RHVault',
    dimensions: getDimensions(16.01, 14)
  },
  Viruses: {
    name: 'RHViruses',
    dimensions: getDimensions(13.33, 16)
  }
}

export const DEFAULT_CATEGORY = 'Misc'
