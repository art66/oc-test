export const scrollbarStyle = {
  borderRadius: '4px',
  background: '#666'
}

export const scrollbarContainerStyle = {
  background: 'none',
  opacity: 1
}

export const WITHIN_HOUR_OPACITY = 1
export const WITHIN_DAY_OPACITY = 0.95
export const WITHIN_MONTH_OPACITY = 0.9
export const WITHIN_YEAR_OPACITY = 0.85
export const AFTER_YEAR_OPACITY = 0.8
