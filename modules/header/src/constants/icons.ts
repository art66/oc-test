export const MENU_ICON_DIMENSIONS = {
  width: 27,
  height: 24,
  viewbox: '6.5 4.5 27 24'
}

export const LOGIN_ICON_DIMENSIONS = {
  width: 17,
  height: 15.67,
  viewbox: '0 0 20 17.67'
}

export const SEARCH_ICON_DIMENSIONS = {
  width: 21,
  height: 21,
  viewbox: '-6 -5 24 24'
}

export const SEARCH_ICON_DIMENSIONS_TOUCH = {
  width: 25,
  height: 25,
  viewbox: '-6 -5 24 24'
}

export const ADVANCED_SEARCH_ICON_DIMENSIONS = {
  width: 28,
  height: 28,
  viewbox: '6 5.5 28 28'
}

export const STAR_ICON_PRESET = {
  type: 'header',
  subtype: 'COMPANY_STAR'
}

export const EMPTY_STAR_ICON_PRESET = {
  type: 'header',
  subtype: 'COMPANY_STAR_EMPTY'
}

export const DEFAULT_PRESET = {
  type: 'header',
  subtype: 'RESET_DEFAULTS'
}

export const ONLINE_STATUS_ICONS = {
  name: 'OnlineStatus',
  type: {
    online: 'online',
    away: 'idle',
    offline: 'oflline'
  },
  preset: {
    online: {
      type: 'header',
      subtype: 'ONLINE_STATUS'
    },
    away: {
      type: 'header',
      subtype: 'AWAY_STATUS'
    },
    offline: {
      type: 'header',
      subtype: 'OFFLINE_STATUS'
    }
  }
}

export const LEFT_MENU_ICONS_DIMENSIONS = {
  Register: {
    width: 28,
    height: 28,
    viewbox: '-6 -5 28 28'
  },
  Wiki: {
    width: 30.016,
    height: 23.462,
    viewbox: '-6.01 -5.06 30.016 23.462'
  },
  Contact: {
    width: 16,
    height: 16,
    viewbox: '0 0 16 16'
  },
  Credits: {
    width: 30.05,
    height: 24.884,
    viewbox: '-6 -5.01 30.05 24.884'
  },
  Discord: {
    width: 27.75,
    height: 30,
    viewbox: '-6 -6 27.75 30'
  },
  Rules: {
    width: 25.77,
    height: 29,
    viewbox: '-6 -4 25.77 29'
  },
  Forums: {
    width: 28,
    height: 26.67,
    viewbox: '-6 -4 28 26.67'
  },
  Staff: {
    width: 28,
    height: 28,
    viewbox: '-6 -4 28 28'
  },
  Newspaper: {
    width: 18,
    height: 15,
    viewbox: '0 0 18 15'
  }
}

export const AVATAR_CIRCLE_DIMENSIONS = {
  width: 36,
  height: 36,
  viewbox: '946.5 523.5 36 36'
}

export const RIGHT_MENU_ICONS_DIMENSIONS = {
  profile: {
    width: 28,
    height: 28,
    viewbox: '-6 -4 28 28'
  },
  darkMode: {
    width: 28.005,
    height: 28.001,
    viewbox: '-6.01 -4 28.005 28.001'
  },
  desktopMode: {
    width: 30,
    height: 26,
    viewbox: '-6 -4 30 26'
  },
  headlines: {
    width: 30,
    height: 27,
    viewbox: '-6 -4 30 27'
  },
  settings: {
    width: 28,
    height: 28,
    viewbox: '-6 -4 28 28'
  },
  logout: {
    width: 24,
    height: 28,
    viewbox: '-6 -4 24 28'
  },
  serverInfo: {
    width: 28.411,
    height: 23.881,
    viewbox: '-6.41 -3.88 28.411 23.881'
  }
}

export const TC_CLOCK_ICON_DIMENSIONS = {
  width: 28,
  height: 28,
  viewbox: '6 5 30 30'
}

export const ACTIVITY_LOG_ICON_DIMENSIONS = {
  width: 25,
  height: 28,
  viewbox: '-3 -3.5 25 28'
}

export const NEWS_TICKER_ICONS = {
  donation: {
    name: 'NTStar',
    subtype: {
      ok: 'STAR',
      hospital: 'STAR_HOSPITAL',
      jail: 'STAR_JAIL',
      traveling: 'STAR_TRAVELING'
    }
  },
  tutorial: {
    name: 'NTTutorials',
    subtype: {
      ok: 'TUTORIALS',
      hospital: 'TUTORIALS',
      jail: 'TUTORIALS',
      traveling: 'TUTORIALS'
    }
  },
  faction: {
    name: 'NTFaction',
    subtype: {
      ok: 'FACTION',
      hospital: 'FACTION_HOSPITAL',
      jail: 'FACTION_JAIL',
      traveling: 'FACTION_TRAVELING'
    }
  },
  urgentFaction: {
    name: 'NTFaction',
    subtype: {
      ok: 'URGENT_FACTION',
      hospital: 'URGENT_FACTION_HOSPITAL_JAIL',
      jail: 'URGENT_FACTION_HOSPITAL_JAIL',
      traveling: 'URGENT_FACTION_HOSPITAL_JAIL'
    }
  },
  news: {
    name: 'NTNews',
    subtype: {
      ok: 'NEWS',
      hospital: 'NEWS_HOSPITAL',
      jail: 'NEWS_JAIL',
      traveling: 'NEWS_TRAVELING'
    }
  },
  announcement: {
    name: 'NTAnnouncement',
    subtype: {
      ok: 'URGENT_ANNOUNCEMENT',
      hospital: 'URGENT_ANNOUNCEMENT_HOSPITAL_JAIL',
      jail: 'URGENT_ANNOUNCEMENT_HOSPITAL_JAIL',
      traveling: 'URGENT_ANNOUNCEMENT_HOSPITAL_JAIL'
    }
  },
  gameClosed: {
    name: 'NTGameClosed',
    subtype: {
      ok: 'GAME_CLOSED',
      hospital: 'GAME_CLOSED',
      jail: 'GAME_CLOSED',
      traveling: 'GAME_CLOSED'
    }
  }
}
