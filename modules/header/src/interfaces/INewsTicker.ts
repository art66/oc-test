import IHeadline from './IHeadline'

export default interface INewsTicker {
  headlines: IHeadline[]
  activeHeadlineIndex: number
  isEnabled: boolean
}
