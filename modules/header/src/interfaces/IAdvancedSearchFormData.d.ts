import IAdvancedSearchFaction from './IAdvabcedSearchFaction'

export interface ISelectValue {
  id: string
  name: string
}

export interface IFactionAutocompleteValue extends IAdvancedSearchFaction {}

export default interface IAdvancedSearchFormData {
  [key: string]: IFactionAutocompleteValue[] | string | ISelectValue | number[]
}
