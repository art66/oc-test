export default interface IAdvancedSearchFaction {
  id: number
  name: string
  respect: number
}
