import IPlayer from './globalSearch/IPlayer'
import IFaction from './globalSearch/IFaction'
import IItem from './globalSearch/IItem'
import IPlace from './globalSearch/IPlace'
import ICompany from './globalSearch/ICompany'

export default interface IGlobalSearch {
  optionsList: {
    id: string
    name: string
  }[]
  resultList?: IPlayer[] | IFaction[] | IItem[] | IPlace[] | ICompany[]
  inputValue?: string
  open?: boolean
}
