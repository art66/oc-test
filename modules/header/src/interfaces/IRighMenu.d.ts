export default interface IRightMenu {
  open: boolean
  desktopModeEnabled: boolean
  darkModeEnabled: boolean
  newsTickerEnabled: boolean
}
