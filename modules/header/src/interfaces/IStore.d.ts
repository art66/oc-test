import { IBrowser } from 'redux-responsive'
import IAdvancedSearch from './IAdvancedSearch'
import IGlobalSearch from './IGlobalSearch'
import ILeftMenu from './ILeftMenu'
import IRighMenu from './IRighMenu'
import IUser from './IUser'
import ISettings from './ISettings'
import ILogo from './ILogo'
import IServerState from './IServerState'
import IHeadline from '../interfaces/IHeadline'
import IGroup from './recentHistory/IGroup'
import INewsTicker from './INewsTicker'

export default interface IStore {
  browser: IBrowser
  header: {
    headerDataIsFetched: boolean
    advancedSearch: IAdvancedSearch
    globalSearch: IGlobalSearch
    leftMenu: ILeftMenu
    rightMenu: IRighMenu
    TCClock: { open: boolean }
    recentHistory: { open: boolean }
    user: IUser
    settings: ISettings
    logo: ILogo
    headlines: {
      headlines: IHeadline[]
    }
    serverState: IServerState
  },
  recentHistory: {
    startFrom: string
    isFetching: boolean
    history: IGroup[]
  },
  newsTicker: INewsTicker
}
