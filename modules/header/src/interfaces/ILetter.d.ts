export default interface ILetter {
  name: string,
  message: string,
  used: number,
  timeout: number
}
