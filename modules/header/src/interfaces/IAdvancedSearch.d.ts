import IFaction from './globalSearch/IFaction'

export default interface IAdvancedSearch {
  optionsList: {
    gender: any
    properties: any
    searchConditions: any
    lastAction: any
  }
  factionsResultList?: IFaction[]
  open: boolean
  formData: any
}
