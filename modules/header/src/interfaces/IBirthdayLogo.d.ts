import ILetter from './ILetter'

export default interface IBirthdayLogo {
  anniversary: number,
  letters: ILetter[]
  isEating: boolean
  eatLetter: string
}
