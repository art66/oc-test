type TStatus = 'ok' | 'jail' | 'hospital' | 'multijail' | 'logged-out'

export default interface IUser {
  state: {
    status: TStatus
    isLoggedIn: boolean
    isDonator: boolean
    isTravelling: boolean
  }
  data: {
    hospitalStamp?: number
    jailStamp?: number
    logoutHash: string
    userID: number
    avatar: {
      isDefault: boolean
      link: string
    }
  }
}
