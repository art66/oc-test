export enum HeadlinesType {
  General = 'general',
  Tutorial = 'tutorial',
  Donation = 'donation',
  Faction = 'faction',
  Maintenance = 'maintenance',
  GameClosed = 'gameClosed'
}

export default interface IHeadline {
  ID: number
  headline: string
  type: HeadlinesType
  link: string
  isGlobal: boolean
  countdown: number
  endTime: number
}
