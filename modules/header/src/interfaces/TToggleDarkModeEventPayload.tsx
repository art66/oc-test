export type TToggleDarkModeEventPayload = {
  checked: boolean
}
