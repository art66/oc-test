export default interface IServerState {
  currentTime: number
  serverName: string
}
