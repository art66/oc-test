export default interface ILink {
  name: string
  link: string
  icon: string
  inNewTab: boolean
}
