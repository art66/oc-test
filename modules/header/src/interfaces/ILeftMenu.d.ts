import ILink from './ILink'

export default interface ILeftMenu {
  open: boolean
  links: ILink[]
}
