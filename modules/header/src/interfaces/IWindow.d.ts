export default interface IWindow extends Window {
  deleteChatsCookie?: () => void
  setCookie?: (name: string, value: any, expires?: any) => void
  toggleDesktopMode?: () => void
  getCurrentTimestamp?: () => number
  WebsocketHandler?: any
  changeBackdropTopOffset?: () => void
  copyTextToClipboard?: (text: string) => void
  isMobileMedia?: () => boolean
  isTornMobileApp?: () => boolean
}
