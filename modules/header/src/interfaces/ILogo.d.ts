import IBirthdayLogo from './IBirthdayLogo'

export default interface ILogo {
  name: string
  title: string
  additionalData?: IBirthdayLogo
}
