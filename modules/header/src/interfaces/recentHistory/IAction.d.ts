import IActionParams from './IActionParams'

export default interface IAction {
  ID: string
  time: number
  text: string
  category: string
  params: IActionParams
  copyBlockActivated?: boolean
  expanded?: boolean
  similar?: {
    open: boolean
    log: IAction[]
  }
  table?: {
    name?: string
    value?: string
  }[]
}
