export default interface ITimeSeparator{
  number: number
  label: string
}
