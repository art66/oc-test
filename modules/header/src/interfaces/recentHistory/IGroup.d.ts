import ITimeSeparator from './ITimeSeparator'
import IAction from './IAction'

export default interface IGroup {
  timeSeparator: ITimeSeparator
  log: IAction[]
}
