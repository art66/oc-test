export default interface IActionParams {
  italic?: 1 | 0
  color?: string
  bold?: 1 | 0
  inaccurate?: 1 | 0
}
