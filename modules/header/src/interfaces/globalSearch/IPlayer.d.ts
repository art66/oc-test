type TOnlineStatus = 'online' | 'away' | 'offline'

export default interface IPlayer {
  id: number
  name: string
  online: TOnlineStatus
  onlineIndex: number
  url: string
}
