export default interface IFaction {
  name: string
  respect: string
  id: string
  url: string
}
