export default interface IPlace {
  id: string
  name: string
  url: string
}
