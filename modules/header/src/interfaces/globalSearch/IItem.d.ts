export default interface IItem {
  id: string
  name: string
  total: string
  url: string
}
