export default interface ICompany {
  id: number
  name: string
  stars: number
  url: string
}
