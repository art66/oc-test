export default interface ISettings {
  emptyHeader: boolean
  emptyLinks: boolean
  hideActivityLog: boolean
  hideAdvancedSearch: boolean
  hideSettingsDropdown: boolean
  hasAccessToDarkMode?: boolean
}
