import { takeEvery, takeLatest, throttle } from 'redux-saga/effects'
import { SWITCH_SETTINGS } from '../../actions/actionTypes'
import {
  initNewsTicker,
  fetchHeadlines,
  setNextHeadline,
  setActiveHeadlineIndex,
  tabVisible
} from '../../actions/newsTicker'
import { autoscrollHeadlineSaga } from './autoscrollHeadlineSaga'
import { fetchHeadlinesSaga } from './fetchHeadlinesSaga'
import { saveActiveHeadlineDataSaga } from './saveActiveHeadlineDataSaga'
import { setActiveHeadlineSaga } from './setActiveHeadlineSaga'
import { synchronizeNewsTickerSaga } from './synchronizeNewsTickerSaga'
import { toggleNewsTickerStateSaga } from './toggleNewsTickerStateSaga'

const filterNewsTickerEnabledActions = (action) =>
  action.type === SWITCH_SETTINGS && action.payload.settingName === 'newsTickerEnabled'

export default function* index() {
  yield takeEvery(initNewsTicker, autoscrollHeadlineSaga)
  yield takeLatest(fetchHeadlines, fetchHeadlinesSaga)
  yield throttle(1000, setNextHeadline, setActiveHeadlineSaga)
  yield takeEvery(setActiveHeadlineIndex, saveActiveHeadlineDataSaga)
  yield takeLatest(filterNewsTickerEnabledActions, toggleNewsTickerStateSaga)
  yield takeEvery(tabVisible, synchronizeNewsTickerSaga)
}
