import { put } from 'redux-saga/effects'
import { getCookie } from '@torn/shared/utils'
import { setHeadlinesData } from '../../actions/newsTicker'
import { ACTIVE_HEADLINE, NEWS_TICKER_IS_ENABLED } from '../../constants/newsTicker'
import IHeadline from '../../interfaces/IHeadline'
import { NEWS_TICKER_SETTING } from '../../constants'
import { switchSettings } from '../../actions'
import { getCurrentTimestamp } from '../../utils/newsTickerHelper'

const getActiveHeadlineFromStorage = (): IHeadline | null => {
  try {
    return JSON.parse(localStorage.getItem(ACTIVE_HEADLINE))
  } catch (e) {
    console.log(e)
  }

  return null
}

function* syncNewsTickerEnabledState() {
  const newsTickerCookie = getCookie(NEWS_TICKER_IS_ENABLED)

  yield put(switchSettings(NEWS_TICKER_SETTING, newsTickerCookie && newsTickerCookie === 'true'))
}

function* syncActiveHeadline() {
  const activeHeadline = getActiveHeadlineFromStorage()
  const isExpiredTimeHeadline = activeHeadline?.endTime && activeHeadline.endTime < getCurrentTimestamp()

  yield activeHeadline && !isExpiredTimeHeadline && put(setHeadlinesData([activeHeadline]))
}

export function* synchronizeNewsTickerSaga() {
  yield syncActiveHeadline()
  yield syncNewsTickerEnabledState()
}
