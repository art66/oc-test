import { select } from 'redux-saga/effects'
import { setCookie } from '@torn/shared/utils'
import { NEWS_TICKER_COOKIE_NAME, COOKIE_EXPIRES_IN_DAYS, ACTIVE_HEADLINE } from '../../constants/newsTicker'
import { getHeadlines } from '../../selectors/newsTicker'
import { HeadlinesType } from '../../interfaces/IHeadline'

const convertToCookieFormat = (activeHeadline, headlinesLeft) => {
  return {
    activeDonationID: activeHeadline.type === HeadlinesType.Donation ? activeHeadline.ID : null,
    activeHeadlineID: activeHeadline.ID,
    activeHeadlineType: activeHeadline.type,
    headlinesLeft
  }
}

const convertToStorageFormat = activeHeadline => {
  return {
    ...activeHeadline,
    timeSaved: new Date().getTime()
  }
}

const saveData = (activeHeadline, headlinesLeft) => {
  setCookie(
    NEWS_TICKER_COOKIE_NAME,
    JSON.stringify(convertToCookieFormat(activeHeadline, headlinesLeft)),
    COOKIE_EXPIRES_IN_DAYS
  )
  try {
    localStorage.setItem(ACTIVE_HEADLINE, JSON.stringify(convertToStorageFormat(activeHeadline)))
  } catch (e) {
    console.log(e)
  }
}

export function* saveActiveHeadlineDataSaga(action) {
  const activeHeadlineIndex = action.payload.index
  const headlines = yield select(getHeadlines)
  const activeHeadline = headlines?.[activeHeadlineIndex]
  const headlinesLeft = headlines.length - activeHeadlineIndex - 1

  activeHeadline && saveData(activeHeadline, headlinesLeft)
}
