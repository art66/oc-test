import { all, put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { setHeadlinesData } from '../../actions/newsTicker'
import { getActiveHeadlineId, getActiveHeadlineType } from '../../selectors/newsTicker'

export function* fetchHeadlinesSaga() {
  try {
    const [activeId, activeType] = yield all([select(getActiveHeadlineId), select(getActiveHeadlineType)])
    const { headlines } = yield fetchUrl('page.php?sid=newsTickers', {
      limit: 10,
      newsTickerActiveHeadline: activeId,
      newsTickerActiveHeadlineType: activeType
    })

    yield put(setHeadlinesData(headlines))
  } catch (e) {
    console.error(e)
  }
}
