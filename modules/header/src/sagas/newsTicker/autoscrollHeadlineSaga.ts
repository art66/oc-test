import { cancel, delay, fork, put, take, select } from 'redux-saga/effects'
import { isActionOf } from 'typesafe-actions'
import {
  setNextHeadline,
  scrollStarted,
  headlineClick,
  tabHidden,
  scrollEnded,
  tabVisible
} from '../../actions/newsTicker'
import { getNewsTickerEnabled } from '../../selectors'

function* starAutoscrollSaga() {
  while (true) {
    yield delay(15000)
    yield put(setNextHeadline())
  }
}

export function* autoscrollHeadlineSaga() {
  while (true) {
    const scrollTask = yield fork(starAutoscrollSaga)

    const action = yield take([scrollStarted, headlineClick, tabHidden])

    yield cancel(scrollTask)

    if (isActionOf(scrollStarted, action)) {
      yield take(scrollEnded)
    }

    if (isActionOf(tabHidden, action)) {
      yield take(tabVisible)
    }

    if (isActionOf(headlineClick, action)) {
      const isEnabled = yield select(getNewsTickerEnabled)

      yield isEnabled && put(setNextHeadline())
    }
  }
}
