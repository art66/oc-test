import { all, put, select } from 'redux-saga/effects'
import { getActiveHeadlineIndex, getHeadlines } from '../../selectors/newsTicker'
import { fetchHeadlines, setActiveHeadlineIndex } from '../../actions/newsTicker'

export function* setActiveHeadlineSaga() {
  const [activeIndex, headlines] = yield all([select(getActiveHeadlineIndex), select(getHeadlines)])

  if (activeIndex >= headlines.length - 1) {
    yield put(fetchHeadlines())
  } else {
    yield put(setActiveHeadlineIndex(activeIndex + 1))
  }
}
