import { call, put, select } from 'redux-saga/effects'
import { setNewsTickerCookie } from '../../utils/newsTickerHelper'
import { getActiveHeadline } from '../../selectors/newsTicker'
import { fetchHeadlines, setActiveHeadlineIndex } from '../../actions/newsTicker'

export function* toggleNewsTickerStateSaga(action) {
  yield call(setNewsTickerCookie, action.payload.value)
  yield action.payload.value === false && put(setActiveHeadlineIndex(0))

  const activeHeadline = yield select(getActiveHeadline)

  yield !activeHeadline && put(fetchHeadlines())
}
