import { takeEvery, put, select, call } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import * as actionTypes from '../actions/recentHistory/actionTypes'
import { setFetchState, setInitRecentHistoryData, setNextChunk, setStartFrom } from '../actions/recentHistory'
import LogGroupsGenerator from '../utils/logGroupsGenerator'
import { getHistory, getNextPeriod } from '../selectors'
import { getLogsFromLastGroup } from '../utils/historyHelpers'

export function* fetchRecentHistoryData() {
  try {
    const data = yield fetchUrl('page.php?sid=activityLogData')

    if (data.log) {
      const logHelper = new LogGroupsGenerator(data.log)
      const log = logHelper.getGroupedLogs()

      yield put(setInitRecentHistoryData(log, data.startFrom))
    }
  } catch (e) {
    console.error(e)
  }
}

export function* loadNextChunk() {
  try {
    const startFrom = yield select(getNextPeriod)

    yield put(setFetchState(true))
    const data = yield fetchUrl(`page.php?sid=activityLogData&startFrom=${startFrom}`)
    const history = yield select(getHistory)

    if (+data.startFrom && data.log && history) {
      const recordsFromLastGroup = yield call(getLogsFromLastGroup, history)
      const logHelper = new LogGroupsGenerator([...recordsFromLastGroup, ...data.log])
      const log = logHelper.getGroupedLogs()

      yield put(setNextChunk(log, data.startFrom))
    }

    if (!+data.startFrom) {
      yield put(setStartFrom(data.startFrom))
    }

    yield put(setFetchState(false))
  } catch (e) {
    console.error(e)
  }
}

export default function* recentHistory() {
  yield takeEvery(actionTypes.FETCH_RECENT_HISTORY_DATA, fetchRecentHistoryData)
  yield takeEvery(actionTypes.LOAD_NEXT_CHUNK, loadNextChunk)
}
