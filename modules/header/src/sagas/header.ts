import { takeEvery, put, call, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import * as actionTypes from '../actions/actionTypes'
import {
  setInitialHeaderData,
  setAutocompleteResultList,
  setAdvancedSearchAutocompleteResultList,
  setGlobalSearchData,
  setBirthdayBonuses,
  setEatLetter,
  setIsEatingLetter,
  updateUserData,
  headerDataIsFetched,
  switchSettings
} from '../actions'
import { saveHeaderDataToStorage } from '../utils/storageHelper'
import { getDarkModeEnabled } from '../selectors'
import { DARK_MODE_SETTING } from '../constants'
import { handleToggleDarkMode } from '../utils/darkModeHelper'

export function* fetchInitialHeaderData() {
  try {
    const data = yield fetchUrl('/page.php?sid=TopBanner')

    yield put(setInitialHeaderData(data))
    yield put(headerDataIsFetched(true))
    yield call(saveHeaderDataToStorage, data)
  } catch (e) {
    console.error(e)
  }
}

export function* autocompleteSearchByOption(action) {
  const { query, option } = action.payload
  const data = yield fetchUrl('autocompleteHeaderAjaxAction.php', { q: query, option })

  yield put(setAutocompleteResultList(data.list))
}

export function* autocompleteSearchFaction(action) {
  const { query, option } = action.payload
  const data = yield fetchUrl('autocompleteHeaderAjaxAction.php', { q: query, option })

  yield put(setAdvancedSearchAutocompleteResultList(data.list))
}

export function* fetchUpdatedHeaderData() {
  const data = yield fetchUrl('page.php?sid=TopBanner', { step: 'updateHeaderData' })

  yield put(setGlobalSearchData(data.globalSearch))
  yield put(updateUserData(data.user))
}

export function* getBirthdayBonuses(action) {
  const { letter } = action.payload
  const data = yield fetchUrl('/birthday.php', { step: 'eatLetter', eat: letter })

  yield put(setEatLetter(letter))
  yield put(setBirthdayBonuses(data))
  yield put(setIsEatingLetter(data.success))
}

export function* toggleDarkMode() {
  const darkModeEnabled = yield select(getDarkModeEnabled)

  yield put(switchSettings(DARK_MODE_SETTING, !darkModeEnabled))
  yield call(handleToggleDarkMode, darkModeEnabled)
}

export default function* header() {
  yield takeEvery(actionTypes.FETCH_INITIAL_HEADER_DATA, fetchInitialHeaderData)
  yield takeEvery(actionTypes.AUTOCOMPLETE_SEARCH_BY_OPTION, autocompleteSearchByOption)
  yield takeEvery(actionTypes.AUTOCOMPLETE_SEARCH_FACTION, autocompleteSearchFaction)
  yield takeEvery(actionTypes.FETCH_UPDATED_HEADER_DATA, fetchUpdatedHeaderData)
  yield takeEvery(actionTypes.GET_BIRTHDAY_BONUSES, getBirthdayBonuses)
  yield takeEvery(actionTypes.TOGGLE_DARK_MODE, toggleDarkMode)
}
