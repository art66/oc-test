import { all } from 'redux-saga/effects'
import header from './header'
import newsTicker from './newsTicker'
import recentHistory from './recentHistory'

export default function* rootSaga() {
  yield all([header(), recentHistory(), newsTicker()])
}
