import IStore from '../interfaces/IStore'

export const getNextPeriod = (state: IStore) => state.recentHistory.startFrom
export const getHistory = (state: IStore) => state.recentHistory.history
export const getUserStatus = (state: IStore) => state.header.user?.state.status
export const getIsTravelling = (state: IStore) => state.header.user?.state.isTravelling
export const getIsLoggedIn = (state: IStore) => state.header.user?.state.isLoggedIn
export const getInitialHeadlines = (state: IStore) => state.header?.headlines?.headlines || []
export const getNewsTickerEnabled = (state: IStore) => state.header.rightMenu.newsTickerEnabled
export const getDarkModeEnabled = state => state.header.rightMenu.darkModeEnabled
export const getMediaType = (state: IStore) => state.browser.mediaType
