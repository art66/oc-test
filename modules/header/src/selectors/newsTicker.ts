import { createSelector } from 'reselect'
import { STATUS_FOR_VIEW_HEADLINES, URGENT_HEADLINES_TYPES_OFF } from '../constants/newsTicker'
import IStore from '../interfaces/IStore'
import { getInitialHeadlines, getNewsTickerEnabled, getUserStatus } from './index'
import IHeadline from '../interfaces/IHeadline'

const filterUrgentHeadlines = (headlines: IHeadline[]) =>
  headlines.filter((headline) => headline && URGENT_HEADLINES_TYPES_OFF.includes(headline.type) && headline.isGlobal)

const getFetchedHeadlines = (state: IStore) => state.newsTicker.headlines
const getAllHeadlines = createSelector(
  [getFetchedHeadlines, getInitialHeadlines],
  (fetchedHeadlines, initialHeadlines) => (fetchedHeadlines.length ? fetchedHeadlines : initialHeadlines)
)

export const getActiveHeadlineIndex = (state: IStore) => state.newsTicker.activeHeadlineIndex
export const getHeadlines = createSelector([getAllHeadlines, getNewsTickerEnabled], (allHeadlines, isEnabled) =>
  isEnabled ? allHeadlines : filterUrgentHeadlines(allHeadlines))
export const getActiveHeadline = createSelector(
  [getHeadlines, getActiveHeadlineIndex],
  (headlines, activeHeadlineIndex) => headlines[activeHeadlineIndex]
)
export const getActiveHeadlineId = createSelector([getActiveHeadline], (activeHeadline) => activeHeadline?.ID)
export const getActiveHeadlineType = createSelector([getActiveHeadline], (activeHeadline) => activeHeadline?.type)
export const shouldShowHeadlines = (state: IStore) => STATUS_FOR_VIEW_HEADLINES.includes(getUserStatus(state))
