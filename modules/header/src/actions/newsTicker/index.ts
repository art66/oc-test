import { createAction } from 'typesafe-actions'
import IHeadline from '../../interfaces/IHeadline'

export const fetchHeadlines = createAction('newsticker/FETCH_HEADLINES')()
export const setHeadlinesData = createAction('newsticker/SET_HEADLINES_DATA', (headlines: IHeadline[]) => ({
  headlines
}))()
export const setActiveHeadlineIndex = createAction('newsticker/SET_ACTIVE_HEADLINE_INDEX', (index: number) => ({
  index
}))()
export const setNextHeadline = createAction('newsticker/SET_NEXT_HEADLINE')()
export const headlineClick = createAction('newsticker/HEADLINE_CLICK')()
export const initNewsTicker = createAction('newsticker/INIT_NEWS_TICKER')()
export const scrollStarted = createAction('newsticker/SCROLL_STARTED')()
export const scrollEnded = createAction('newsticker/SCROLL_ENDED')()
export const tabHidden = createAction('newsticker/TAB_HIDDEN')()
export const tabVisible = createAction('newsticker/TAB_VISIBLE')()
