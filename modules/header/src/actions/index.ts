import { createAction } from 'typesafe-actions'
import * as actionTypes from './actionTypes'

export const fetchInitialHeaderData = createAction(actionTypes.FETCH_INITIAL_HEADER_DATA)()
export const setInitialHeaderData = createAction(actionTypes.SET_INITIAL_HEADER_DATA, data => ({ data }))()
export const headerDataIsFetched = createAction(actionTypes.HEADER_DATA_IS_FETCHED, dataIsFetched => ({
  dataIsFetched
}))()
export const toggleTopLeftMenu = createAction(actionTypes.TOGGLE_TOP_LEFT_MENU, open => ({ open }))()
export const autocompleteSearchByOption = createAction(actionTypes.AUTOCOMPLETE_SEARCH_BY_OPTION, (query, option) => ({
  query,
  option
}))()
export const setAutocompleteResultList = createAction(actionTypes.SET_AUTOCOMPLETE_RESULT_LIST, resultList => ({
  resultList
}))()
export const updateGlobalSearchInputValue = createAction(actionTypes.UPDATE_GLOBAL_SEARCH_INPUT_VALUE, inputValue => ({
  inputValue
}))()
export const toggleGlobalSearch = createAction(actionTypes.TOGGLE_GLOBAL_SEARCH, open => ({ open }))()
export const autocompleteSearchFaction = createAction(actionTypes.AUTOCOMPLETE_SEARCH_FACTION, (query, option) => ({
  query,
  option
}))()
export const setAdvancedSearchAutocompleteResultList = createAction(
  actionTypes.SET_ADVANCED_SEARCH_AUTOCOMPLETE_RESULT_LIST,
  resultList => ({ resultList })
)()
export const toggleTopRightMenu = createAction(actionTypes.TOGGLE_TOP_RIGHT_MENU, open => ({ open }))()
export const switchSettings = createAction(actionTypes.SWITCH_SETTINGS, (settingName, value) => ({
  settingName,
  value
}))()
export const toggleTCClock = createAction(actionTypes.TOGGLE_TC_CLOCK, open => ({ open }))()
export const toggleRecentHistory = createAction(actionTypes.TOGGLE_RECENT_HISTORY, open => ({ open }))()
export const toggleAdvancedSearch = createAction(actionTypes.TOGGLE_ADVANCED_SEARCH, open => ({ open }))()
export const updateAdvancedSearchFormField = createAction(
  actionTypes.UPDATE_ADVANCED_SEARCH_FORM_FIELD,
  (fieldName, value) => ({ fieldName, value })
)()
export const updateUserData = createAction(actionTypes.UPDATE_USER_DATA, ({ data, state }) => ({ data, state }))()
export const resetAdvancedSearchForm = createAction(actionTypes.RESET_ADVANCED_SEARCH_FORM)()
export const updateAvatar = createAction(actionTypes.UPDATE_AVATAR, avatar => ({ avatar }))()
export const setAdvancedSearchFormData = createAction(actionTypes.SET_ADVANCED_SEARCH_FORM_DATA, formData => ({
  formData
}))()
export const fetchUpdatedHeaderData = createAction(actionTypes.FETCH_UPDATED_HEADER_DATA)()
export const setGlobalSearchData = createAction(actionTypes.SET_GLOBAL_SEARCH_DATA, ({ optionsList }) => ({
  optionsList
}))()
export const setEatLetter = createAction(actionTypes.SET_EAT_LETTER, letter => ({ letter }))()
export const getBirthdayBonuses = createAction(actionTypes.GET_BIRTHDAY_BONUSES, letter => ({ letter }))()
export const setBirthdayBonuses = createAction(actionTypes.SET_BIRTHDAY_BONUSES, data => ({ data }))()
export const setIsEatingLetter = createAction(actionTypes.SET_IS_EATING_LETTER, isEatingLetter => ({
  isEatingLetter
}))()
export const toggleDarkMode = createAction(actionTypes.TOGGLE_DARK_MODE)()
export const addAdvancedSearchFactionField = createAction(
  actionTypes.ADD_ADVANCED_SEARCH_FACTION_FIELD,
  (id, name, respect) => ({ id, name, respect })
)()
export const removeAdvancedSearchFactionField = createAction(actionTypes.REMOVE_ADVANCED_SEARCH_FACTION_FIELD, id => ({
  id
}))()
