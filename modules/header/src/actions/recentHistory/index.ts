import { createAction } from 'typesafe-actions'
import * as actionTypes from './actionTypes'

export const fetchRecentHistoryData = createAction(actionTypes.FETCH_RECENT_HISTORY_DATA)()
export const setInitRecentHistoryData = createAction(actionTypes.SET_INIT_RECENT_HISTORY_DATA, (log, startFrom) => ({
  log,
  startFrom
}))()
export const changeActionGroup = createAction(
  actionTypes.CHANGE_ACTION_GROUP,
  (oldTimeSeparator, newTimeSeparator, actionId, parentId) => ({
    oldTimeSeparator,
    newTimeSeparator,
    actionId,
    parentId
  })
)()
export const toggleSimilarActions = createAction(actionTypes.TOGGLE_SIMILAR_ACTIONS, (actionId, open) => ({
  actionId,
  open
}))()
export const toggleActionExpandState = createAction(actionTypes.TOGGLE_ACTION_EXPAND_STATE, (actionId, parentId) => ({
  actionId,
  parentId
}))()
export const loadNextChunk = createAction(actionTypes.LOAD_NEXT_CHUNK)()
export const setFetchState = createAction(actionTypes.SET_FETCH_STATE, (isFetching) => ({ isFetching }))()
export const setNextChunk = createAction(actionTypes.SET_NEXT_CHUNK, (log, startFrom) => ({ log, startFrom }))()
export const setStartFrom = createAction(actionTypes.SET_START_FROM, (startFrom) => ({ startFrom }))()
export const resetRecentHistoryData = createAction(actionTypes.RESET_RESENT_HISTORY_DATA)()
