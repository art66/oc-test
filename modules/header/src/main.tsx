import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import { Provider } from 'react-redux'
import rootStore from './store/createStore'
import App from './components/App'

declare global {
  // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    renderRecentHistory: () => void
    unmountRecentHistory: () => void
  }
}

const MOUNT_NODE = document.getElementById('header-root')

let render = () => {
  ReactDOM.render(
    <Provider store={rootStore}>
      <App />
    </Provider>,
    MOUNT_NODE
  )
}

// This code is excluded from production bundle
// @ts-ignore
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderApp = render
    const renderError = (error) => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept()
  }
}

render()
