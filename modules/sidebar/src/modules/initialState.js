export default {
  sidebarFetched: false,
  linkWithContextMenu: 0,
  statusIcons: {
    visible: true,
    onTop: false,
    size: 'big',
    icons: {
      gender: {
        iconID: 55,
        title: '<b>Male</b>',
        link: null,
        timestamp: 4444,
        timeToUpdate: 44557
      },
      status: {
        iconID: 56,
        title: '<b>Job</b><br>Bagboy in a Grocery Shop',
        link: 'home.php',
        timestamp: null,
        timeToUpdate: null
      },
      jail: {
        iconID: 57,
        title: '<b>Online</b>',
        link: 'home.php',
        timestamp: null,
        timeToUpdate: null
      },
      home: {
        iconID: 59,
        title: '<b>Online</b>',
        link: 'home.php',
        timestamp: null,
        timeToUpdate: null
      }
    }
  },
  user: {
    name: 'sergeyd',
    money: {
      value: 25000
    },
    level: {
      value: 11,
      upgradePossibility: true,
      link: '/level2.php?vkey=52614858653398'
    },
    points: {
      value: 17,
      link: '/points.php'
    },
    merits: {
      value: 10,
      link: '/awards.php#/merits'
    }
  },
  bars: {
    energy: {
      name: 'Energy',
      timeToUpdate: 12400,
      amount: 100,
      max: 150
    },
    nerve: {
      name: 'Nerve',
      timeToUpdate: 3600,
      amount: 1,
      max: 17
    },
    happy: {
      name: 'Happy',
      timeToUpdate: 3600,
      amount: 1825,
      max: 1825
    },
    life: {
      name: 'Life',
      timeToUpdate: 3600,
      amount: 990,
      max: 990
    },
    chain: {
      name: 'Chain',
      timeToUpdate: 3600,
      amount: 63,
      max: 250,
      countDown: 1825
    }
  },
  areas: {
    home: {
      linkOrder: 0,
      name: 'Home',
      icon: 'home',
      link: '/'
    },
    items: {
      linkOrder: 1,
      name: 'Items',
      icon: 'items',
      link: '/item.php'
    },
    city: {
      linkOrder: 2,
      name: 'City',
      icon: 'city',
      link: '/city.php'
    },
    job: {
      linkOrder: 3,
      name: 'Job',
      icon: 'job',
      link: '/jobs.php'
    },
    gym: {
      linkOrder: 4,
      name: 'Gym',
      icon: 'gym',
      link: '/gym.php'
    },
    properties: {
      linkOrder: 5,
      name: 'Properties',
      icon: 'property',
      link: '/properties.php'
    },
    education: {
      linkOrder: 6,
      name: 'Education',
      icon: 'education',
      link: '/education.php'
    },
    crimes: {
      linkOrder: 7,
      name: 'Crimes',
      icon: 'crimes',
      link: '/crimes.php'
    },
    missions: {
      linkOrder: 8,
      name: 'Missions',
      icon: 'missions',
      link: '/loader.php?sid=missions'
    },
    newspaper: {
      linkOrder: 9,
      name: 'Newspapers',
      icon: 'newspaper',
      link: '/newspaper.php'
    },
    jail: {
      linkOrder: 10,
      name: 'Jail',
      icon: 'jail',
      link: '/jailview.php'
    },
    hospital: {
      linkOrder: 11,
      name: 'Hospital',
      icon: 'hospital',
      link: '/hospitalview.php'
    },
    casino: {
      linkOrder: 12,
      name: 'Casino',
      icon: 'casino',
      link: '/casino.php'
    },
    forums: {
      linkOrder: 13,
      name: 'Forums',
      icon: 'forums',
      link: '/forums.php'
    },
    hall_of_fame: {
      linkOrder: 14,
      name: 'Hall of Fame',
      icon: 'hall-of-fame',
      link: '/halloffame.php'
    },
    factions: {
      linkOrder: 15,
      name: 'Factions',
      icon: 'factions',
      link: '/factions.php?step=your'
    },
    my_faction: {
      linkOrder: 16,
      name: 'My Faction',
      icon: 'factions',
      link: '/factions.php'
    }
  },
  lists: {
    staff: {
      linkOrder: 17,
      name: 'Staff',
      icon: 'staff',
      link: '/staff.php',
      elements: null
    },
    friends: {
      linkOrder: 18,
      name: 'Friends',
      icon: 'friends',
      link: '/friendlist.php',
      elements: null
    },
    enemies: {
      linkOrder: 19,
      name: 'Enemies',
      icon: 'enemies',
      link: '/blacklist.php',
      elements: null
    }
  },
  account: {
    messages: {
      linkOrder: 20,
      name: 'Messages',
      icon: 'messages',
      amount: 35,
      link: '/messages.php'
    },
    events: {
      linkOrder: 21,
      name: 'Events',
      icon: 'events',
      amount: 10,
      link: '/events.php'
    },
    awards: {
      linkOrder: 21,
      name: 'Awards',
      icon: 'awards',
      amount: null,
      link: '/awards.php'
    }
  },
  favorites: [],
  footer: {
    serverName: 'App7'
  }
}
