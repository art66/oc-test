import { createLogger } from 'redux-logger'

const reduxLogger = createLogger({
  collapsed: true,
  timestamp: false,
  diff: true
})

export default reduxLogger
