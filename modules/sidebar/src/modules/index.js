import { calculateResponsiveState } from 'redux-responsive'
import subscribeOnDOMContentLoaded from '@torn/shared/utils/subscribeOnDOMContentLoaded'

import { fetchUrl, getCookie, changeJailHospRegularStyles } from '../utils/index'
// eslint-disable-next-line no-redeclare
/* global WebsocketHandler */

// ------------------------------------
// Constants
// ------------------------------------
export const GET_AJAX_ERROR = 'sidebar/GET_AJAX_ERROR'
export const SET_SIDEBAR_DATA = 'sidebar/SET_SIDEBAR_DATA'
export const UPDATE_MONEY = 'sidebar/UPDATE_MONEY'
export const UPDATE_LEVEL = 'sidebar/UPDATE_LEVEL'
export const UPDATE_BAR = 'sidebar/UPDATE_BAR'
export const UPDATE_BARS = 'sidebar/UPDATE_BARS'
export const UPDATE_USER_FIELD = 'sidebar/UPDATE_USER_FIELD'
export const UPDATE_ACCOUNT_BUTTON_COUNT = 'sidebar/UPDATE_ACCOUNT_BUTTON_COUNT'
export const UPDATE_STATUS_ICONS = 'sidebar/UPDATE_STATUS_ICONS'
export const UPDATE_BAR_TIME = 'sidebar/UPDATE_BAR_TIME'
export const SET_SIDEBAR_MODE = 'sidebar/SET_SIDEBAR_MODE'
export const UPDATE_CHAIN_AMOUNT = 'sidebar/UPDATE_CHAIN_AMOUNT'
export const UPDATE_CHAIN_COOLDOWN = 'sidebar/UPDATE_CHAIN_COOLDOWN'
export const SET_SELECTED_STATUS_ICON = 'sidebar/SET_SELECTED_STATUS_ICON'
export const SET_AREAS_SLIDER_STATUS = 'sidebar/SET_AREAS_SLIDER_STATUS'
export const SET_AREA_FAVORITE = 'sidebar/SET_AREA_FAVORITE'
export const SET_USER_STATUS = 'sidebar/SET_USER_STATUS'
export const UPDATE_AREAS = 'sidebar/UPDATE_AREAS'
export const UPDATE_LISTS = 'sidebar/UPDATE_LISTS'
export const RESET_SIDEBAR = 'sidebar/RESET_SIDEBAR'
export const SET_SLIDER_HINT_DISABLE = 'sidebar/SET_SLIDER_HINT_DISABLE'
export const SET_PROGRESS_BAR_FETCH_ACTIVE = 'sidebar/SET_PROGRESS_BAR_FETCH_ACTIVE'
export const SET_LIST_STATUS = 'sidebar/SET_LIST_STATUS'
export const SET_LIST_LINK_STATUS = 'sidebar/SET_LIST_LINK_STATUS'
export const SET_INFORMATION = 'sidebar/SET_INFORMATION'
export const LINK_CONTEXT_MENU = 'sidebar/LINK_CONTEXT_MENU'
export const SET_ACCOUNT_BUTTONS = 'sidebar/SET_ACCOUNT_BUTTONS'
export const SET_AREA_AMOUNT = 'sidebar/SET_AREA_AMOUNT'
export const SET_DARK_MODE_STATE = 'sidebar/SET_DARK_MODE_STATE'
export const SET_AREA_STATUS = 'sidebar/SET_AREA_STATUS'

// ------------------------------------
// InitialState
// ------------------------------------
const initialState = {
  sliderHintDisabled: false,
  selectedStatusIcon: null,
  progressBarFetchActive: false,
  linkWithContextMenu: ''
}

// ------------------------------------
// Actions
// ------------------------------------

export const getAjaxError = error => {
  console.error(error)
  return {
    type: GET_AJAX_ERROR,
    error
  }
}

const setSidebarData = (data, sync) => {
  return {
    type: SET_SIDEBAR_DATA,
    data,
    sync
  }
}

const updateBar = (name, bar) => {
  return {
    type: UPDATE_BAR,
    name: name.toLowerCase(),
    bar
  }
}

const updateBars = bars => {
  return {
    type: UPDATE_BARS,
    bars
  }
}

const updateUserField = (field, data) => ({
  type: UPDATE_USER_FIELD,
  field,
  data
})

const updateAccountButtonCount = (buttonName, count) => ({
  type: UPDATE_ACCOUNT_BUTTON_COUNT,
  buttonName,
  count
})

const updateStatusIcons = statusIcons => ({
  type: UPDATE_STATUS_ICONS,
  statusIcons
})

export const setSelectedStatusIcon = iconKey => ({
  type: SET_SELECTED_STATUS_ICON,
  iconKey
})

export const setAreasSliderStatus = status => ({
  type: SET_AREAS_SLIDER_STATUS,
  status
})

const setAreaFavorite = (area, favorite) => ({
  type: SET_AREA_FAVORITE,
  area,
  favorite
})

const setUserStatus = status => ({
  type: SET_USER_STATUS,
  status
})

const setAreaAmount = (areaName, amount) => ({
  type: SET_AREA_AMOUNT,
  areaName,
  amount
})

const updateAreas = (areas, sync = false) => ({
  type: UPDATE_AREAS,
  areas,
  sync
})

const updateLists = lists => ({
  type: UPDATE_LISTS,
  lists
})

export const resetSidebar = () => ({
  type: RESET_SIDEBAR
})

// eslint-disable-next-line @typescript-eslint/naming-convention
const _setSliderHintDisable = () => ({
  type: SET_SLIDER_HINT_DISABLE
})

const setProgressBarFetchActive = activeStatus => {
  return {
    type: SET_PROGRESS_BAR_FETCH_ACTIVE,
    activeStatus
  }
}

export const updateChainAmount = setZero => {
  return {
    type: UPDATE_CHAIN_AMOUNT,
    setZero
  }
}

// eslint-disable-next-line @typescript-eslint/naming-convention
const _setListStatus = listName => {
  return {
    type: SET_LIST_STATUS,
    listName
  }
}

// eslint-disable-next-line @typescript-eslint/naming-convention
const _setListLinkStatus = listLinkName => {
  return {
    type: SET_LIST_LINK_STATUS,
    listLinkName
  }
}

export const setInformation = data => {
  return {
    type: SET_INFORMATION,
    data
  }
}

export const linkContextMenu = (linkKey = '') => ({
  type: LINK_CONTEXT_MENU,
  linkKey
})

export const setAccountButtons = account => ({
  type: SET_ACCOUNT_BUTTONS,
  account
})

export const setDarkModeState = darkModeEnabled => ({
  type: SET_DARK_MODE_STATE,
  darkModeEnabled
})

export const setAreaStatus = (areaName, status) => ({
  type: SET_AREA_STATUS,
  areaName,
  status
})

// ------------------------------------
// Middleware
// ------------------------------------
// some legacy action, moved from createStore scope here by invoking inside Sidebar Container Component
export const calcResponsive = () => dispatch => dispatch(calculateResponsiveState(window))

export const getBars = () => (dispatch, getState) => {
  const {
    sidebar: { progressBarFetchActive }
  } = getState()

  if (!progressBarFetchActive) {
    dispatch(setProgressBarFetchActive(true))
    fetchUrl('getBars').then(data => {
      dispatch(setProgressBarFetchActive(false))

      return dispatch(updateBars(data))
    })
  }
}

export const updateBarTime = (bar, updateBarData, fetchData, updateBarValue) => (dispatch, getState) => {
  const { progressBarFetchActive } = getState().sidebar

  if (updateBarData && fetchData) {
    dispatch(getBars())
  } else if (updateBarData && !fetchData && updateBarValue && !progressBarFetchActive) {
    dispatch(updateBar(bar.name, { amount: bar.amount + bar.step }))
  }
}

export const setSliderHintDisable = () => dispatch => {
  dispatch(_setSliderHintDisable())
  const userID = getCookie('uid')

  document.cookie = `disableHintTooltip_${userID}=true; expires=Thu, 01 Jan 2050 00:00:00 UTC;`
}

export const fetchSidebarData = () => dispatch => {
  return fetchUrl('getSidebarData', { fullUrl: window.location.href }).then(
    data => {
      dispatch(setSidebarData(data, true))

      return data
    },
    error => dispatch(getAjaxError(error))
  )
}

export const loadSidebarData = () => dispatch => {
  const userID = getCookie('uid') || null

  const loadSidebarDataFromLocalStorage = () => {
    const sidebarLocalStorageData = (sessionStorage && sessionStorage.getItem(`sidebarData${userID}`)) || null

    if (!sidebarLocalStorageData || sidebarLocalStorageData.length === 0) {
      return
    }

    try {
      dispatch(setSidebarData(JSON.parse(sidebarLocalStorageData), false))

      dispatch(setSelectedStatusIcon(null))
    } catch (error) {
      console && console.error && console.error(error)
    }
  }

  const setHintTooltipStatus = () => {
    const hideHintTooltip = getCookie(`disableHintTooltip_${userID}`)

    if (hideHintTooltip) {
      dispatch(setSliderHintDisable())
    }
  }

  const loadActualSidebarData = () => {
    try {
      const sidebarDataElement = document.getElementById('sidebar_data')
      const sidebarDataAreas = sidebarDataElement ? sidebarDataElement.innerText : ''

      if (sidebarDataAreas) {
        dispatch(updateAreas(JSON.parse(sidebarDataAreas), true))
        dispatch(setSelectedStatusIcon(null))
      }
    } catch (error) {
      console && console.error && console.error(error)
    }

    dispatch(fetchSidebarData())
  }

  loadSidebarDataFromLocalStorage()
  setHintTooltipStatus()
  subscribeOnDOMContentLoaded([loadActualSidebarData])
}

export const fetchStatusIcons = () => dispatch => {
  return fetchUrl('getStatusIcons').then(data => {
    dispatch(updateStatusIcons(data))
  })
}

export const fetchInformation = () => dispatch => {
  return fetchUrl('getInformation').then(
    data => {
      dispatch(setInformation(data))
    },
    error => dispatch(getAjaxError(error))
  )
}

export const setListStatus = listName => (dispatch, getState) => {
  dispatch(_setListStatus(listName))
  const currentListsStatuses = getState().sidebar.listsStatuses

  return fetchUrl('setListsStatuses', currentListsStatuses).then(
    () => {},
    error => dispatch(getAjaxError(error))
  )
}

export const setListLinkStatus = listLinkName => (dispatch, getState) => {
  dispatch(_setListLinkStatus(listLinkName.toLowerCase()))
  const currentListsLinksStatuses = getState().sidebar.listsLinksStatuses

  return fetchUrl('setListsLinksStatuses', currentListsLinksStatuses).then(
    () => {},
    error => dispatch(getAjaxError(error))
  )
}

export const fetchAreasLinksData = () => dispatch => {
  return fetchUrl('getAreasLinks').then(
    data => {
      dispatch(updateAreas(data))
    },
    error => dispatch(getAjaxError(error))
  )
}

export const syncSidebarDataOnTabFocus = () => dispatch => {
  return fetchUrl('sync', { fullUrl: window.location.href }).then(
    data => {
      dispatch(updateAreas(data.areas))
      dispatch(setAccountButtons(data.account))
      dispatch(updateBars(data.bars))
    },
    error => dispatch(getAjaxError(error))
  )
}

const updateBarByWS = (barName, data) => dispatch => {
  dispatch(updateBar(barName, data))
}

export const subscribeForSidebarChanges = () => dispatch => {
  const handler = new WebsocketHandler('sidebar')
  const onMoneyChangeEvent = document.createEvent('CustomEvent')

  onMoneyChangeEvent.initCustomEvent('onMoneyChange', false, false, {})
  handler.setActions({
    updateSidebar: () => {
      dispatch(fetchSidebarData())
    },
    updateMoney: data => {
      dispatch(updateUserField('money', { value: data.money }))
      document.dispatchEvent(onMoneyChangeEvent)
    },
    updateLevel: data => {
      dispatch(updateUserField('level', { value: data.level }))
    },
    updatePointsCount: data => {
      const onPointsChangeEvent = new CustomEvent('onPointsChange', { detail: data })

      document.dispatchEvent(onPointsChangeEvent)
      dispatch(updateUserField('points', { value: data.count }))
    },
    updateRefillsCount: data => {
      dispatch(updateUserField('refills', { value: data.count }))
    },
    updateMeritsCount: data => {
      dispatch(updateUserField('merits', { value: data.count }))
    },
    updateFactionApplicationsCounter: data => {
      dispatch(setAreaAmount(data.linkSlug, data.amount))
    },
    updateCompanyApplicationsCounter: data => {
      dispatch(setAreaAmount(data.linkSlug, data.amount))
    },
    updateEnergy: data => {
      dispatch(updateBarByWS('energy', data))
    },
    updateNerve: data => {
      dispatch(updateBarByWS('nerve', data))
      dispatch(setAreaStatus('crimes', data.amount >= data.max ? 'available' : null))
    },
    updateHappy: data => {
      dispatch(updateBarByWS('happy', data))
    },
    updateLife: data => {
      dispatch(updateBarByWS('life', data))
    },
    updateChain: data => {
      dispatch(updateBar('chain', data.chain))
    },
    updateMsgCount: data => {
      dispatch(updateAccountButtonCount('messages', data.count))
    },
    updateEventsCount: data => {
      dispatch(updateAccountButtonCount('events', data.count))
    },
    updateAwardsCount: data => {
      dispatch(updateAccountButtonCount('awards', data.count))
    },
    updateIcons: data => {
      dispatch(updateStatusIcons(data.statusIcons))
    },
    onHospital: () => {
      changeJailHospRegularStyles('hospital')
      fetchUrl('getAreasLinks').then(
        data => {
          dispatch(setUserStatus('inHospital'))
          dispatch(updateAreas(data))
        },
        error => dispatch(getAjaxError(error))
      )
    },
    onJail: () => {
      changeJailHospRegularStyles('jail')
      fetchUrl('getAreasLinks').then(
        data => {
          dispatch(setUserStatus('inJail'))
          dispatch(updateAreas(data))
        },
        error => dispatch(getAjaxError(error))
      )
    },
    onRevive: () => {
      changeJailHospRegularStyles('regular')
      fetchUrl('getAreasLinks').then(
        data => {
          dispatch(setUserStatus(false))
          dispatch(updateAreas(data))
        },
        error => dispatch(getAjaxError(error))
      )
    },
    onLeaveFromJail: () => {
      changeJailHospRegularStyles('regular')
      fetchUrl('getAreasLinks').then(
        data => {
          dispatch(setUserStatus(false))
          dispatch(updateAreas(data))
        },
        error => dispatch(getAjaxError(error))
      )
    },

    onFriendListChange: () => {
      fetchUrl('getListLinks').then(
        data => dispatch(updateLists(data)),
        error => dispatch(getAjaxError(error))
      )
    },
    onBlackListChange: () => {
      fetchUrl('getListLinks').then(
        data => dispatch(updateLists(data)),
        error => dispatch(getAjaxError(error))
      )
    }
  })
}

const saveSidebarMode = (isMobileMode, windowSize) => {
  return {
    type: SET_SIDEBAR_MODE,
    isMobileMode,
    windowSize
  }
}

export const setSidebarMode = () => (dispatch, getState) => {
  const isMobileMode = document.body.classList.contains('d') && document.body.classList.contains('r')
  const mediaType = getState().browser.mediaType === 'infinity' ? 'desktop' : getState().browser.mediaType
  const browserSize = isMobileMode ? mediaType : 'desktop'

  dispatch(saveSidebarMode(isMobileMode, browserSize))
}

export const addFavoriteArea = area => dispatch => {
  dispatch(setAreaFavorite(area, true))
  const { linkOrder } = area

  return fetchUrl('addLinkToFavorites', {
    linkOrder
  }).then(
    () => {},
    error => dispatch(getAjaxError(error))
  )
}

export const removeFavoriteArea = area => dispatch => {
  dispatch(setAreaFavorite(area, false))
  const { linkOrder } = area

  return fetchUrl('removeLinkFromFavorites', {
    linkOrder
  }).then(
    () => {},
    error => dispatch(getAjaxError(error))
  )
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_SIDEBAR_MODE]: (state, action) => {
    return {
      ...state,
      isMobileMode: action.isMobileMode,
      windowSize: action.windowSize
    }
  },
  [SET_SIDEBAR_DATA]: (state, action) => {
    const status = action.data.user && action.data.user.status

    if (action.sync) {
      if (status && status === 'inJail') {
        changeJailHospRegularStyles('jail')
      } else if (status && status === 'inHospital') {
        changeJailHospRegularStyles('hospital')
      } else if (!status) {
        changeJailHospRegularStyles('regular')
      }
    }

    return {
      ...state,
      ...action.data,
      dataSync: action.sync,
      sidebarFetched: true,
      progressBarFetchActive: false
    }
  },
  [UPDATE_MONEY]: (state, action) => ({
    ...state,
    user: {
      ...state.user,
      money: {
        ...state.user.money,
        value: action.payload
      }
    }
  }),
  [UPDATE_LEVEL]: (state, action) => ({
    ...state,
    user: {
      ...state.user,
      level: {
        ...state.user.level,
        value: action.payload
      }
    }
  }),
  [UPDATE_BAR]: (state, action) => {
    return {
      ...state,
      bars: {
        ...state.bars,
        [action.name]: {
          ...state.bars[action.name],
          ...action.bar
        }
      }
    }
  },
  [UPDATE_BARS]: (state, action) => {
    return {
      ...state,
      bars: {
        ...state.bars,
        ...action.bars
      }
    }
  },
  [UPDATE_CHAIN_AMOUNT]: (state, action) => {
    return {
      ...state,
      bars: {
        ...state.bars,
        chain: {
          ...state.bars.chain,
          amount: action.setZero ? 0 : state.bars.chain.amount - 1
        }
      }
    }
  },
  [UPDATE_USER_FIELD]: (state, action) => ({
    ...state,
    user: {
      ...state.user,
      [action.field]: {
        ...state.user[action.field],
        ...action.data
      }
    }
  }),
  [UPDATE_ACCOUNT_BUTTON_COUNT]: (state, action) => ({
    ...state,
    account: {
      ...state.account,
      [action.buttonName]: {
        ...state.account[action.buttonName],
        amount: action.count,
        status: action.count > 0 ? 'available' : null
      }
    }
  }),
  [UPDATE_STATUS_ICONS]: (state, action) => ({
    ...state,
    statusIcons: action.statusIcons
  }),
  [UPDATE_BAR_TIME]: (state, action) => {
    return {
      ...state,
      bars: {
        ...state.bars,
        [action.barName]: {
          ...state.bars[action.barName],
          timeToUpdate: action.secondsToEnd
        }
      }
    }
  },
  [SET_SELECTED_STATUS_ICON]: (state, action) => ({
    ...state,
    selectedStatusIcon: action.iconKey
  }),
  [SET_AREAS_SLIDER_STATUS]: (state, action) => ({
    ...state,
    areasSliderActive: action.status
  }),
  [SET_AREA_AMOUNT]: (state, action) => {
    return {
      ...state,
      areas: {
        ...state.areas,
        [action.areaName]: {
          ...state.areas[action.areaName],
          amount: action.amount
        }
      }
    }
  },
  [SET_AREA_STATUS]: (state, action) => {
    if (state.areas[action.areaName]) {
      return {
        ...state,
        areas: {
          ...state.areas,
          [action.areaName]: {
            ...state.areas[action.areaName],
            status: action.status
          }
        }
      }
    }
    return state
  },
  [SET_AREA_FAVORITE]: (state, action) => {
    const { area, favorite } = action

    return {
      ...state,
      [area.parentName]: {
        ...state[area.parentName],
        [area.areaName]: {
          ...state[area.parentName][area.areaName],
          favorite
        }
      }
    }
  },
  [SET_USER_STATUS]: (state, action) => {
    return {
      ...state,
      user: {
        ...state.user,
        status: action.status
      }
    }
  },
  [UPDATE_AREAS]: (state, action) => {
    return {
      ...state,
      areas: action.areas,
      dataSync: action.sync
    }
  },
  [UPDATE_LISTS]: (state, action) => {
    return {
      ...state,
      lists: action.lists
    }
  },
  [RESET_SIDEBAR]: () => {
    return {
      ...initialState
    }
  },
  [SET_SLIDER_HINT_DISABLE]: state => {
    return {
      ...state,
      sliderHintDisabled: true
    }
  },
  [SET_PROGRESS_BAR_FETCH_ACTIVE]: (state, action) => {
    return {
      ...state,
      progressBarFetchActive: action.activeStatus
    }
  },
  [SET_LIST_STATUS]: (state, action) => {
    return {
      ...state,
      listsStatuses: {
        ...state.listsStatuses,
        [action.listName]: !state.listsStatuses[action.listName]
      }
    }
  },
  [SET_LIST_LINK_STATUS]: (state, action) => {
    return {
      ...state,
      listsLinksStatuses: {
        ...state.listsLinksStatuses,
        [action.listLinkName]: !state.listsLinksStatuses[action.listLinkName]
      }
    }
  },
  [SET_INFORMATION]: (state, action) => {
    return {
      ...state,
      ...action.data
    }
  },
  [LINK_CONTEXT_MENU]: (state, action) => {
    return {
      ...state,
      linkWithContextMenu: action.linkKey
    }
  },
  [SET_ACCOUNT_BUTTONS]: (state, action) => {
    return {
      ...state,
      account: action.account
    }
  },
  [SET_DARK_MODE_STATE]: (state, action) => {
    return {
      ...state,
      darkModeEnabled: action.darkModeEnabled
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
export default function sidebarReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
