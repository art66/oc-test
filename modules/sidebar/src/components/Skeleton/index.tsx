import React from 'react'

import styles from './index.cssmodule.scss'

/* eslint-disable max-len */
class Skeleton extends React.Component {
  _renderInfo = () => {
    return (
      <div className={`${styles.infoSectionWrapPlaceholder}`}>
        <span className={styles.infoIcon}>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='default___3oPC0 '
            filter='url(#sidebar__accountLinks_filter)'
            fill='url(#sidebar__accountLinks_svg_gradient)'
            stroke='#fff'
            strokeWidth='0'
            width='34'
            height='34'
            viewBox='-7 -9 34 34'
          >
            <linearGradient id='sidebar__accountLinks_svg_gradient' gradientTransform='rotate(90)'>
              <stop offset='0' stopColor='#999999' />
              <stop offset='1' stopColor='#CCCCCC' />
            </linearGradient>
            <filter id='sidebar__accountLinks_filter'>
              <feDropShadow dx='0' dy='1' stdDeviation='0.1' floodColor='#fff' />
            </filter>
            <filter id='svg_sidebar_filter_dark_mode'>
              <feDropShadow dx='0' dy='1' stdDeviation='2' floodColor='#00000073' />
            </filter>
            <path d='M9,8,0,1H18ZM4.93,6.7,0,2.85v9Zm8.14,0L18,11.88v-9Zm-1.17.91L9,9.87,6.1,7.61,0,14H18Z' />
          </svg>
        </span>
        <span className={styles.infoIcon}>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='default___3oPC0 '
            filter='url(#sidebar__accountLinks_filter)'
            fill='url(#sidebar__accountLinks_svg_gradient)'
            stroke='#fff'
            strokeWidth='0'
            width='34'
            height='34'
            viewBox='-6 -6 30 30'
          >
            <path d='M8,1a8,8,0,1,0,8,8A8,8,0,0,0,8,1ZM6.47,3.87H9.53l-.77,7.18H7.24ZM8,14.55A1.15,1.15,0,1,1,9.15,13.4,1.14,1.14,0,0,1,8,14.55Z' />
          </svg>
        </span>
        <span className={styles.infoIcon}>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='default___3oPC0 '
            filter='url(#sidebar__accountLinks_filter)'
            fill='url(#sidebar__accountLinks_svg_gradient)'
            stroke='#fff'
            strokeWidth='0'
            width='34'
            height='34'
            viewBox='2 0 34 34'
          >
            <path
              d='M-696-155a5.006,5.006,0,0,1,5-5,5.005,5.005,0,0,1,5,5,5.006,5.006,0,0,1-5,5A5.007,5.007,0,0,1-696-155Zm5,1.975,1.853,1.024-.354-2.171,1.5-1.537-2.074-.317L-691-158l-.928,1.975-2.072.317,1.5,1.537-.354,2.171,1.854-1.024Zm1-7.976,3-5,3,2-3,5Zm-8-3,3-2,3,5-3,2Zm6,1-2-3h6l-2,3Z'
              transform='translate(709 175)'
            />
          </svg>
        </span>
      </div>
    )
  }

  _renderPreloader = () => {
    return (
      <div className={styles.preloader}>
        <div className={styles.dots}>
          <span className={styles.dot} />
          <span className={`${styles.dot} ${styles['dot-1']}`} />
          <span className={`${styles.dot} ${styles['dot-2']}`} />
          <span className={`${styles.dot} ${styles['dot-3']}`} />
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className={styles.skeletonWrap}>
        <div className={styles.sidebarWrapPlaceholder}>
          <div>
            <div className={styles.sidebarTitlePlaceholder}>Information</div>
            <div className={`${styles.contentPlaceholder} ${styles.contentPlaceholderStatuses}`}>
              {this._renderPreloader()}
            </div>
          </div>
          <div className={styles.sectionInfo}>{this._renderInfo()}</div>
          <div>
            <div className={styles.sidebarTitlePlaceholder}>Areas</div>
            <div className={`${styles.contentPlaceholder} ${styles.contentPlaceholderAreas}`}>
              {this._renderPreloader()}
            </div>
          </div>
          <div className={styles.sectionLists}>
            <div className={styles.sidebarTitlePlaceholder}>Lists</div>
            <div className={`${styles.contentPlaceholder} ${styles.contentPlaceholderLists}`}>
              {this._renderPreloader()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Skeleton
