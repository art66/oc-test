import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Swiper, Slide } from 'react-dynamic-swiper'

import ProgressBars from './ProgressBars'
import StatusIcons from './StatusIcons'
import Points from './Points'
import ToggleBlock from './ToggleBlock'

import s from '../styles/Information.cssmodule.scss'
import mainStyles from '../styles/Sidebar.cssmodule.scss'

class Information extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pointsFullValue: true
    }

    this._informationBlock = React.createRef()
    this._pointsBlock = React.createRef()
    this._iconsBlock = React.createRef()
  }

  componentDidMount() {
    this.swiper = null

    const {
      sidebar: { windowSize }
    } = this.props

    if (windowSize === 'mobile') {
      this.informationBlock = this._informationBlock && this._informationBlock.current
      this.pointsBlock = this._pointsBlock && this._pointsBlock.current
      this.iconsBlock = this._iconsBlock && this._iconsBlock.current

      this.resizePoints(0)

      window.addEventListener('resize', this.resizePoints)
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { pointsFullValue } = this.state
    const {
      sidebar: { windowSize, statusIcons }
    } = this.props
    const {
      sidebar: { statusIcons: nextStatusIcons }
    } = nextProps

    if (windowSize === 'mobile') {
      return (
        pointsFullValue !== nextState.pointsFullValue
        || (statusIcons?.icons && Object.keys(statusIcons.icons).length) !==
          (nextStatusIcons?.icons && Object.keys(nextStatusIcons?.icons).length)
      )
    }
    return true
  }

  componentDidUpdate(prevProps) {
    const { sidebar: { windowSize, statusIcons } = {} } = prevProps

    if (windowSize !== 'mobile') {
      return
    }

    const {
      sidebar: { statusIcons: { icons: nextIcons } = {} }
    } = this.props || {}

    if ((nextIcons && Object.keys(nextIcons).length) !== (statusIcons.icons && Object.keys(statusIcons.icons).length)) {
      if (this.swiper) {
        this.updateSlides(this.swiper)
        this.forceUpdate()
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizePoints)
  }

  resizePoints = offset => {
    const infoWidth = this.pointsBlock ? this.informationBlock.offsetWidth : 0
    const pointsWidth = this.pointsBlock ? this.pointsBlock.offsetWidth : 0
    const iconsWidth = this.iconsBlock ? this.iconsBlock.offsetWidth : 0

    if (offset !== 0) {
      offset = 32

      if (Math.abs(infoWidth - (pointsWidth + iconsWidth)) < offset) {
        return
      }
    }

    this.setState({
      pointsFullValue: infoWidth >= pointsWidth + iconsWidth
    })
  }

  // swiper must be update/resize view in init
  updateSlides = swiper => {
    clearTimeout(this.updateSliderTimeout)

    this.updateSliderTimeout = setTimeout(() => {
      swiper.update(true)

      this.swiper = swiper
    }, 50)
  }

  renderDesktopLayout = () => {
    const { sidebar: { user, windowSize, statusIcons } = {} } = this.props
    const { visible, onTop } = statusIcons

    const infoClass = windowSize === 'mobile' ? s['user-information-mobile'] : s['user-information']

    return (
      <div className={infoClass}>
        <ToggleBlock title='Information' toggle={false}>
          <div className={s['content']}>
            {statusIcons && visible && onTop ? (
              <div>
                <StatusIcons />
                <hr className={mainStyles['delimiter']} />
              </div>
            ) : (
              ''
            )}
            <p className={s['menu-info-row']}>
              <span className={s['menu-name']}>Name:</span>
              <a href={user.link} className={s['menu-value']}>
                {user.name}
              </a>
            </p>
            <Points />
            <hr className={mainStyles['delimiter']} />
            <ProgressBars />
            {statusIcons && visible && onTop === false ? (
              <div>
                <hr className={mainStyles['delimiter']} />
                <StatusIcons />
              </div>
            ) : (
              ''
            )}
          </div>
        </ToggleBlock>
      </div>
    )
  }

  renderMobileLayout = () => {
    const { pointsFullValue } = this.state
    const { sidebar: { windowSize, statusIcons } = {} } = this.props
    const { visible } = statusIcons

    const infoClass = windowSize === 'mobile' ? s['user-information-mobile'] : s['user-information']

    const options = {
      freeMode: true,
      slidesPerView: 'auto'
    }

    return (
      <div className={infoClass} ref={this._informationBlock}>
        <ProgressBars />
        <Swiper
          onInitSwiper={swiper => {
            this.updateSlides(swiper)
          }}
          swiperOptions={options}
          pagination={false}
          prevButton={<div className={`${s['swiper-button']} ${s['button-prev']}`} />}
          nextButton={<div className={`${s['swiper-button']} ${s['button-next']}`} />}
          wrapperClassName={s['swiper']}
        >
          <Slide className={s['slide']}>
            <Points ref={this._pointsBlock} fullValue={pointsFullValue} />
          </Slide>
          <Slide className={s['slide']}>{statusIcons && visible ? <StatusIcons ref={this._iconsBlock} /> : ''}</Slide>
        </Swiper>
      </div>
    )
  }

  render() {
    const { sidebar: { windowSize } = {} } = this.props

    if (windowSize === 'desktop' || windowSize === 'tablet') {
      return this.renderDesktopLayout()
    }

    return this.renderMobileLayout()
  }
}

Information.propTypes = {
  sidebar: PropTypes.object
}

Information.defaultProps = {
  sidebar: {}
}

const mapStateToProps = state => ({
  sidebar: state.sidebar
})

export default connect(mapStateToProps)(Information)
