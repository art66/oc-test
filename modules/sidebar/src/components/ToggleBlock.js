import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import s from '../styles/ToggleBlock.cssmodule.scss'
import classnames from 'classnames'
import { setListStatus } from '../modules'

class ToggleBlock extends Component {
  constructor(props) {
    super(props)
    this.toggleContent = this.toggleContent.bind(this)
  }

  toggleContent() {
    const { title } = this.props
    this.props.setListStatus(title.toLowerCase())
  }

  getHeader() {
    const { title, toggle, user, windowSize, listsStatuses } = this.props
    const headerClass = classnames({
      [s['header']]: true,
      [s['toggled']]: toggle && windowSize === 'tablet',
      [s[windowSize]]: true,
      [s['in-jail']]: user.status && user.status === 'inJail',
      [s['in-hospital']]: user.status && user.status === 'inHospital'
    })

    if (windowSize === 'desktop') {
      return <h2 className={headerClass}>{title}</h2>
    } else if (windowSize === 'tablet') {
      if (toggle) {
        let arrowClass = listsStatuses[title.toLowerCase()]
          ? `${s['header-arrow']} ${s['activated']}`
          : s['header-arrow']
        return (
          <h2 className={headerClass} onClick={this.toggleContent}>
            {title}
            <i className={arrowClass} />
          </h2>
        )
      } else {
        return <h2 className={headerClass}>{title}</h2>
      }
    }
  }

  render() {
    const { children, title, windowSize, listsStatuses } = this.props
    let openedIf
    if (title === 'Information') {
      openedIf = true
    } else {
      openedIf = listsStatuses[title.toLowerCase()] || windowSize === 'desktop'
    }

    const contentClass = openedIf ? s['toggle-content'] : `${s['toggle-content']} ${s['hidden']}`

    return (
      <div className={s['toggle-block']}>
        {this.getHeader()}

        <div className={contentClass}>{children}</div>
      </div>
    )
  }
}

ToggleBlock.propTypes = {
  title: PropTypes.string,
  windowSize: PropTypes.string,
  user: PropTypes.object,
  toggle: PropTypes.bool,
  children: PropTypes.node,
  setListStatus: PropTypes.func,
  listsStatuses: PropTypes.object
}

const mapStateToProps = (state, props) => ({
  windowSize: state.sidebar.windowSize,
  user: state.sidebar.user,
  listsStatuses: state.sidebar.listsStatuses
})

const mapDispatchToProps = {
  setListStatus
}

export default connect(mapStateToProps, mapDispatchToProps)(ToggleBlock)
