import { isTouchDevice } from '@torn/shared/utils/isTouchDevice'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchSidebarData, setSelectedStatusIcon } from '../modules'
import s from '../styles/StatusIcons.cssmodule.scss'
import tooltipStyles from '../styles/Tooltip.cssmodule.scss'
import { formatTime, formatTimeLong } from '../utils'
import Tooltip from './Tooltip'
/* global getCurrentTimestamp */

class StatusIcon extends Component {
  constructor(props) {
    super(props)
    this.createTooltip = this.createTooltip.bind(this)
    this.state = {
      timer: 0
    }
  }

  componentDidMount() {
    const { icon } = this.props

    if (icon.timerExpiresAt && this.getTimeInSeconds() > 0) {
      this.updateTime()
    }
  }

  componentWillUnmount() {
    clearInterval(this.timeUpdateInterval)
  }

  componentDidUpdate(prevProps) {
    const { icon: { timerExpiresAt: nextTime } = {} } = this.props
    const { icon: { timerExpiresAt: currTime } = {} } = prevProps

    if (!currTime && nextTime) {
      this.updateTime()
    }
  }

  getTimeInSeconds = () => {
    const {
      icon: { timerExpiresAt }
    } = this.props

    return timerExpiresAt - getCurrentTimestamp() / 1000
  }

  getIconCooldown() {
    const { icon } = this.props

    if (icon.timerExpiresAt && this.getTimeInSeconds() > 0) {
      if (icon.factionUpgrade) {
        return (
          <p className={tooltipStyles['static-width']}>
            {formatTime(this.state.timer * 1000, true, true)} / {icon.factionUpgrade}
          </p>
        )
      }

      if (icon.isShortFormatTimer) {
        return <p>{formatTime(this.state.timer * 1000, true, true)}</p>
      }

      return <p>{formatTimeLong(this.state.timer)}</p>
    }
  }

  updateTime() {
    clearInterval(this.timeUpdateInterval)
    this.updateIcon()
    this.timeUpdateInterval = setInterval(() => {
      this.updateIcon()
    }, 1000)
  }

  updateIcon() {
    const iconTime = this.getTimeInSeconds()

    if (iconTime > 0) {
      this.setState({
        timer: iconTime
      })
    } else if (iconTime <= 0) {
      this.props.fetchSidebarData()
      clearInterval(this.timeUpdateInterval)
    }
  }

  createTooltip(blockId) {
    const { windowSize, icon } = this.props
    const tooltipPosition = windowSize === 'mobile' ? 'bottom' : 'top'
    const longStringClass = icon.subtitle && icon.subtitle.length > 50 ? tooltipStyles['long-string'] : ''

    return (
      <Tooltip position={tooltipPosition} arrow='center' parent={`${blockId}`}>
        <b dangerouslySetInnerHTML={{ __html: icon.title }} />
        {icon.subtitle ? <p className={longStringClass} dangerouslySetInnerHTML={{ __html: icon.subtitle }} /> : ''}
        {this.getIconCooldown()}
      </Tooltip>
    )
  }

  clickOnIcon = e => {
    const { iconKey, selectedStatusIcon } = this.props

    if (isTouchDevice() && selectedStatusIcon !== iconKey) {
      e.preventDefault()
      this.props.setSelectedStatusIcon(iconKey)
    }
  }

  render() {
    const { icon } = this.props
    const blockId = `${icon.iconID}-sidebar`

    return (
      <li className={s[icon.iconID]}>
        {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
        <a id={blockId} href={icon.link || '#'} onClick={e => this.clickOnIcon(e, blockId)} tabIndex={0} />
        {this.createTooltip(blockId)}
      </li>
    )
  }
}

StatusIcon.propTypes = {
  icon: PropTypes.object,
  iconKey: PropTypes.string,
  windowSize: PropTypes.string,
  setSelectedStatusIcon: PropTypes.func,
  selectedStatusIcon: PropTypes.string,
  fetchSidebarData: PropTypes.func
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  selectedStatusIcon: state.sidebar.selectedStatusIcon
})

const mapDispatchToProps = {
  setSelectedStatusIcon,
  fetchSidebarData
}

export default connect(mapStateToProps, mapDispatchToProps)(StatusIcon)
