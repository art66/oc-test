import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { TOGGLE_DARK_MODE_EVENT } from '@torn/header/src/constants'
import Information from './Information'
import Areas from './Areas'
import AccountLinks from './AccountLinks'
import Lists from './Lists'
import Footer from './Footer'
import SidebarBlock from './SidebarBlock'
import Skeleton from './Skeleton'
import { checkDarkMode } from '../utils'
import 'react-dynamic-swiper/lib/styles.css'
import GoToTopBtn from './GoToTopBtn'
import s from '../styles/Sidebar.cssmodule.scss'
import '../styles/vars.scss'
import '../styles/DM_vars.scss'

class Sidebar extends Component {
  static propTypes = {
    sidebar: PropTypes.object,
    subscribeForSidebarChanges: PropTypes.func,
    loadSidebarData: PropTypes.func,
    fetchSidebarData: PropTypes.func,
    fetchInformation: PropTypes.func,
    setSidebarMode: PropTypes.func,
    calcResponsive: PropTypes.func,
    syncSidebarDataOnTabFocus: PropTypes.func,
    setDarkModeState: PropTypes.func
  }

  static defaultProps = {
    sidebar: {},
    subscribeForSidebarChanges: () => {},
    loadSidebarData: () => {},
    fetchSidebarData: () => {},
    fetchInformation: () => {},
    setSidebarMode: () => {},
    calcResponsive: () => {},
    syncSidebarDataOnTabFocus: () => {},
    setDarkModeState: () => {}
  }

  constructor(props) {
    super(props)

    this.state = {
      windowSize: null
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { sidebar: { windowSize: nextWindowSize } = {} } = nextProps
    const { windowSize: currWindowSize } = prevState

    if (nextWindowSize !== currWindowSize) {
      window.highlightElement()
    }

    return {
      windowSize: nextWindowSize
    }
  }

  shouldComponentUpdate(prevProps, prevState) {
    const {
      sidebar: { windowSize, user, sidebarFetched }
    } = this.props
    const { windowSize: windowSizeInState } = this.state
    const windowSizeChanged = prevProps.sidebar.windowSize !== windowSize || windowSizeInState !== prevState.windowSize
    const userChanged = JSON.stringify(user) !== JSON.stringify(prevProps.sidebar.user)
    const isSidebarFetched = sidebarFetched !== prevProps.sidebar.sidebarFetched

    return windowSizeChanged || userChanged || isSidebarFetched
  }

  componentDidMount() {
    const {
      loadSidebarData,
      subscribeForSidebarChanges,
      fetchInformation,
      sidebar: { user }
    } = this.props

    loadSidebarData()
    subscribeForSidebarChanges()
    this.updateDarkModeState()
    this.removeSidebarSkeleton()

    window.refreshTopOfSidebar = () => fetchInformation()
    window.getUserStatus = () => (user && user.status) || 'ok'
    window.addEventListener(TOGGLE_DARK_MODE_EVENT, this.updateDarkModeState)

    this.addGlobalListeners()
    this.setSidebarMode()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calculateResponsiveState)
    window.removeEventListener('resize', this.setSidebarMode)
    window.removeEventListener('focus', this.onFocusFetch)
    window.removeEventListener('load', this.setSidebarMode)
    window.removeEventListener('hashchange', this.onHashChange)
    window.removeEventListener(TOGGLE_DARK_MODE_EVENT, this.updateDarkModeState)
  }

  onFocusFetch = () => {
    const { syncSidebarDataOnTabFocus } = this.props

    this.forceUpdate()
    syncSidebarDataOnTabFocus()
  }

  onHashChange = () => {
    const { fetchSidebarData } = this.props

    const isForumPage = location.href.indexOf('forums.php') !== -1

    if (isForumPage) {
      fetchSidebarData()
    }
  }

  updateDarkModeState = () => {
    const { setDarkModeState } = this.props
    const darkModeEnabled = checkDarkMode()

    setDarkModeState(darkModeEnabled)
  }

  setSidebarMode = () => {
    const { setSidebarMode } = this.props

    setSidebarMode()
  }

  calculateResponsiveState = () => {
    const { calcResponsive } = this.props

    calcResponsive()
  }

  addGlobalListeners() {
    window.addEventListener('resize', this.calculateResponsiveState)
    window.addEventListener('resize', this.setSidebarMode)
    window.addEventListener('focus', this.onFocusFetch)
    window.addEventListener('load', this.setSidebarMode)
    window.addEventListener('hashchange', this.onHashChange)
  }

  removeSidebarSkeleton = () => {
    const skeletonNode = document && document.querySelector('.sidebarContainerPlaceholder')

    if (skeletonNode) {
      skeletonNode.remove()
    }
  }

  sidebarClasses = () => {
    const { sidebar: { windowSize } = {} } = this.props

    return classnames({
      [s['sidebar']]: true,
      [s[windowSize]]: true
    })
  }

  renderDesktopSidebar = () => {
    return (
      <React.Fragment>
        <SidebarBlock>
          <Information />
          <AccountLinks />
        </SidebarBlock>
        <SidebarBlock>
          <Areas />
        </SidebarBlock>
        <SidebarBlock>
          <Lists />
          <Footer />
        </SidebarBlock>
      </React.Fragment>
    )
  }

  renderMobileSidebar = () => {
    return (
      <React.Fragment>
        <SidebarBlock name='mobile'>
          <Areas />
          <Information />
        </SidebarBlock>
      </React.Fragment>
    )
  }

  renderComponents() {
    const { sidebar: { windowSize } = {} } = this.props

    if (windowSize === 'desktop' || windowSize === 'tablet') {
      return this.renderDesktopSidebar()
    }

    return this.renderMobileSidebar()
  }

  render() {
    const {
      sidebar: { windowSize, sidebarFetched = false }
    } = this.props

    if (!sidebarFetched) {
      return <Skeleton />
    }

    return (
      <div className={`${s['sidebar']} ${windowSize}`}>
        <div className={this.sidebarClasses()} id='sidebar'>
          {this.renderComponents()}
          <GoToTopBtn windowSize={windowSize} />
        </div>
      </div>
    )
  }
}

export default Sidebar
