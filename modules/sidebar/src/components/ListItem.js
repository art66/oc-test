import PropTypes from 'prop-types'
import React, { Component } from 'react'
import classnames from 'classnames'
import s from '../styles/List.cssmodule.scss'
import Tooltip from './Tooltip'

class ListItem extends Component {
  createTooltip(blockId) {
    const { item } = this.props

    return (
      <Tooltip position='top' arrow='center' parent={`${blockId}`}>
        <p>{item.status}</p>
      </Tooltip>
    )
  }

  render() {
    const { item, listName } = this.props
    const blockId = `${listName}_user_${item.name}`
    const statusClass = classnames({
      [s['online']]: item.status === 'Online',
      [s['idle']]: item.status === 'Idle'
    })

    return (
      <li className={statusClass}>
        <span className={s['user-status']} />
        <a href={item.link} id={blockId}>
          {item.name}
          {this.createTooltip(blockId)}
        </a>
        <span className={s['last-active']}>{Math.floor(item.lastAction / 60)}m</span>
      </li>
    )
  }
}

ListItem.propTypes = {
  item: PropTypes.object,
  listName: PropTypes.string
}

export default ListItem
