import React from 'react'
import { SVGIconGenerator, firstLetterUpper } from '@torn/shared/SVG/'
import { svgIconsDimensions } from '../constants/svgIconsDimensions'
import { DEFAULT_DARK_MODE_FILTER } from '../constants/svgIconsDarkModeProps'
import s from '../styles/Area.cssmodule.scss'

const UNSUPPORTED_ICONS_FIXER = {
  factions: 'Faction',
  messages: 'Mailbox',
  awards: 'AwardsMedal',
  'halloween-pumpkin': 'HalloweenPumpkin',
  'competition-mr-ms-torn': 'MrMissTorn'
}
const PRESETS_ICONS_HOLDER = {
  JAIL: {
    type: 'sidebar',
    subtype: 'JAIL'
  },
  HOSPITAL: {
    type: 'sidebar',
    subtype: 'HOSPITAL'
  },
  REGULAR: {
    type: 'sidebar',
    subtype: 'REGULAR'
  }
}

interface IProps {
  sidebarIcons: any
  windowSize: string
  field: string
  userStatus: string
  isFullHeight: boolean
  isHover: boolean
  iconName: string
  status: string
  styleStatus: string
  darkModeEnabled?: boolean
  dimensions?: {
    width: number
    height: number
  }
}

class SVGIcon extends React.PureComponent<IProps> {
  static defaultProps: IProps = {
    sidebarIcons: null,
    windowSize: 'desktop',
    field: '',
    userStatus: '',
    isFullHeight: false,
    isHover: false,
    iconName: '',
    status: '',
    styleStatus: ''
  }

  getDarkModeFilter = () => {
    const { darkModeEnabled } = this.props

    return darkModeEnabled ? { filter: DEFAULT_DARK_MODE_FILTER } : {}
  }

  _calcLayoutVariants = () => {
    const { windowSize, field, userStatus, status, styleStatus } = this.props

    const isGoldLighting = styleStatus === 'gold'
    const isGreenLighting = (status && status === 'available') || styleStatus === 'blue'
    const isRedLighting = styleStatus === 'red' || field === 'rules'
    const isMobileLayout = windowSize === 'mobile'
    const inHospital = (userStatus && userStatus) === 'inHospital'
    const inJail = (userStatus && userStatus) === 'inJail'

    return {
      lighting: {
        isGoldLighting,
        isGreenLighting,
        isRedLighting
      },
      viewport: {
        isMobileLayout
      },
      status: {
        inHospital,
        inJail
      }
    }
  }

  _getActualTemplate = () => {
    const {
      status: { inJail, inHospital }
    } = this._calcLayoutVariants()
    const { JAIL, HOSPITAL, REGULAR } = PRESETS_ICONS_HOLDER
    const svgPresetName = (inJail && JAIL) || (inHospital && HOSPITAL) || REGULAR

    return svgPresetName
  }

  _getMediaResolution = () => {
    const {
      viewport: { isMobileLayout }
    } = this._calcLayoutVariants()
    const { darkModeEnabled } = this.props
    const layoutTypeForPrest = isMobileLayout || darkModeEnabled ? 'MOBILE' : 'DESKTOP'

    return layoutTypeForPrest
  }

  _getPresetNamespace = () => {
    const svgPresetName = this._getActualTemplate()
    const nomalizedPresetNameWithPrefixes = this._addPresetPrefixes(svgPresetName)

    return nomalizedPresetNameWithPrefixes
  }

  _getColorPresetType = () => {
    const {
      lighting: { isGoldLighting, isGreenLighting, isRedLighting }
    } = this._calcLayoutVariants()

    const colorTypeForPreset =
      (isGreenLighting && '_GREEN') || (isRedLighting && '_RED') || (isGoldLighting && '_GOLD') || ''

    return colorTypeForPreset
  }

  _getActiveStatusForPreset = () => {
    const { status } = this.props

    const activeStatusForPreset = status && status === 'active' ? '_ACTIVE' : ''

    return activeStatusForPreset
  }

  _addPresetPrefixes = (presetName: { type: string; subtype: string }) => {
    const layoutTypeForPrest = this._getMediaResolution()
    const colorTypeForPreset = this._getColorPresetType()
    const activeStatusForPreset = this._getActiveStatusForPreset()
    const currentSVGPresetLayout = `${layoutTypeForPrest}${colorTypeForPreset}${activeStatusForPreset}`

    const nomalizedPresetName = {
      ...presetName,
      subtype: `${presetName.subtype}_${currentSVGPresetLayout}`
    }

    return nomalizedPresetName
  }

  _getSVGIconName = () => {
    const { iconName } = this.props

    const SVGIconName = UNSUPPORTED_ICONS_FIXER[iconName] || iconName
    const normalizedSVGIconName = firstLetterUpper(SVGIconName)

    return normalizedSVGIconName
  }

  _getSVGIconDimensions = () => {
    const { dimensions: customDimensions } = this.props

    if (customDimensions) {
      return customDimensions
    }

    const iconName = this._getSVGIconName()
    const { dimensions } = svgIconsDimensions(iconName)

    return dimensions
  }

  _getSVGIconHover = () => {
    const { isHover = false } = this.props

    const { type = '', subtype = '' } = this._getActualTemplate() || {}
    const layoutTypeForPrest = this._getMediaResolution()
    const {
      lighting: { isGoldLighting = false }
    } = this._calcLayoutVariants()
    const colorTypeForPreset = this._getColorPresetType()

    const isActive = isHover && !isGoldLighting

    const hoverConfig = {
      active: isActive,
      preset: {
        type,
        subtype: `${subtype}_${layoutTypeForPrest}${colorTypeForPreset}_HOVER`
      }
    }

    return hoverConfig
  }

  render() {
    const { sidebarIcons, isFullHeight } = this.props
    const SVGIconName = this._getSVGIconName()
    const SVGPreset = this._getPresetNamespace()
    const SVGDimensions = this._getSVGIconDimensions()
    const SVGHover = this._getSVGIconHover()

    return (
      <span className={`${s.svgIconWrap} ${isFullHeight ? s.fullHeightSVG : ''}`}>
        <SVGIconGenerator
          iconsHolder={sidebarIcons}
          iconName={SVGIconName}
          preset={SVGPreset}
          dimensions={SVGDimensions}
          onHover={SVGHover}
        />
      </span>
    )
  }
}

export default SVGIcon
