import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Preloader.cssmodule.scss'
import { connect } from 'react-redux'
import classnames from 'classnames'

class Preloader extends Component {
  render() {
    const dots = [1, 2, 3, 4]
    const { windowSize } = this.props

    const preloaderClass = classnames({
      [s['preloader']]: true,
      [s[windowSize]]: true
    })

    return (
      <div className={preloaderClass}>
        <div className={s['dots']}>
          {dots.map((dot, i) => {
            return <span key={i} className={`${s.dot} ${s[`dot-${i}`]}`} />
          })}
        </div>
      </div>
    )
  }
}

Preloader.propTypes = {
  windowSize: PropTypes.string
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize
})

export default connect(mapStateToProps)(Preloader)
