import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { SVGIconGenerator } from '@torn/shared/SVG'
import Tooltip from './Tooltip'
import { getShortNumber, numberFormat } from '../utils/index'
import { DISABLE_FILTER } from '../constants/svgIconsDarkModeProps'
import s from '../styles/Point.cssmodule.scss'

const SVG_DEFAULT_PROPS = {
  dimensions: {
    width: 19,
    height: 19,
    viewbox: '0 0 19 19'
  },
  fill: {
    strokeWidth: 0
  }
}

const SVG_NAMES_HOLDER = {
  money: {
    iconName: 'Cash'
  },
  level: {
    iconName: 'Level'
  },
  energy: {
    iconName: 'EnergyPoint'
  },
  points: {
    iconName: 'Points'
  },
  merits: {
    iconName: 'Merits'
  },
  refills: {
    iconName: 'Refill'
  }
}

class Point extends Component {
  constructor(props) {
    super(props)
    this.touchOnPoints = this.touchOnPoints.bind(this)
    this.clickOnPoints = this.clickOnPoints.bind(this)
    this.touchIcon = false
  }

  getPointBlock() {
    const { name } = this.props
    const { value } = this.props.data

    if (name === 'Merits') {
      return this.createPointBlock(true)
    } else if (name === 'Points') {
      return this.createPointBlock(value > 0)
    } else if (name === 'Refills') {
      return this.createPointBlock(value > 0)
    } else if (name === 'Level') {
      return this.createPointBlock(this.props.data.upgradePossibility)
    } else if (name === 'Money') {
      return this.createPointBlock()
    }
  }

  getPointsValue() {
    const { windowSize, fullValue } = this.props
    const { name } = this.props
    let { value } = this.props.data

    if (windowSize === 'mobile') {
      value = fullValue ? numberFormat(value) : getShortNumber(value)
      return name === 'Money' ? `$${value}` : value
    }
    value = numberFormat(value)
    return name === 'Money' ? `$${value}` : value
  }

  getDarkModeFilter = () => {
    const { darkModeEnabled } = this.props

    return darkModeEnabled ? { filter: DISABLE_FILTER } : {}
  }

  createTooltip(blockId) {
    const { name, windowSize } = this.props
    const { value } = this.props.data
    const { link, upgradePossibility } = this.props.data
    let position, text, linkText

    if (windowSize === 'mobile') {
      position = 'bottom'
      if (name === 'Level') {
        linkText = upgradePossibility ? `<a href=${link}>Upgrade your Level<a>` : ''
        text = `You have ${this.getPointsValue()} ${name}. ${linkText}`
      } else if (name !== 'Money') {
        linkText = value ? `<a href=${link}>Use ${name}<a>` : ''
        text = `You have ${numberFormat(value)} ${name.toLowerCase()}. ${linkText}`
      } else {
        text = `You have $${numberFormat(value)}`
      }
    } else {
      position = 'top'
      if (name === 'Level') {
        text = 'Upgrade your level'
      } else {
        text = `Use your ${name}`
      }
    }
    return (
      <Tooltip position={position} arrow='center' parent={`${blockId}`}>
        <p dangerouslySetInnerHTML={{ __html: text }} />
      </Tooltip>
    )
  }

  _generateSVGIcon = name => {
    return (
      <SVGIconGenerator
        customClass={s['point-icon']}
        iconName={SVG_NAMES_HOLDER[name].iconName}
        fill={SVG_DEFAULT_PROPS.fill}
        dimensions={SVG_DEFAULT_PROPS.dimensions}
        {...this.getDarkModeFilter()}
      />
    )
  }

  touchOnPoints(e) {
    e.preventDefault()
    const { name } = this.props

    if (name === 'Points') {
      this.touchIcon = !this.touchIcon
    }
  }

  clickOnPoints(e) {
    e.preventDefault()
    const { name } = this.props

    if (name === 'Points' && !this.touchIcon) {
      const link = `${window.location.origin}/points.php`

      window.open(link, '_self')
    }
  }

  createPointBlock(addUseLink) {
    const { name } = this.props
    const { value } = this.props.data
    const { windowSize } = this.props
    let valueClass, moneyClass

    if (name === 'Money') {
      if (value > 0) {
        moneyClass = s['money-positive']
      } else if (value < 0) {
        moneyClass = s['money-negative']
      }
      valueClass = `${s['value']} ${moneyClass}`
    } else {
      const pointsClass = name === 'Points' && windowSize === 'mobile' ? s['points'] : ''

      valueClass = `${s['value']} ${pointsClass}`
    }

    if (windowSize === 'desktop' || windowSize === 'tablet') {
      return (
        <p className={s['point-block']} tabIndex={1}>
          <span className={s['name']}>{name}:</span>
          {name === 'Money' ? (
            <span className={valueClass} id='user-money' data-money={value}>
              {this.getPointsValue()}
            </span>
          ) : (
            <span className={valueClass}>{this.getPointsValue()}</span>
          )}
          {addUseLink ? this.createUseLink() : ''}
        </p>
      )
    }
    const blockId = `points${name}`

    return (
        <a
          href='#'
          className={`${s['point-block']} ${s[windowSize]}`}
          tabIndex={1}
          id={blockId}
          onClick={this.clickOnPoints}
          onTouchStart={this.touchOnPoints}
          onContextMenu={e => e.preventDefault()}
        >
          {this._generateSVGIcon(name.toLowerCase())}
          {name === 'Money' ? (
            <span className={valueClass} id='user-money' data-money={value}>
              {this.getPointsValue()}
            </span>
          ) : (
            <span className={valueClass}>{this.getPointsValue()}</span>
          )}
          {this.createTooltip(blockId)}
        </a>
    )
  }

  createUseLink() {
    const { name } = this.props
    const { link } = this.props.data
    const text = name === 'Level' ? '[upgrade]' : '[use]'
    const blockId = `points${name}`

    return (
      <a href={link} className={s['use']} id={blockId}>
        {text}
        {this.createTooltip(blockId)}
      </a>
    )
  }

  render() {
    return this.getPointBlock()
  }
}

Point.propTypes = {
  fullValue: PropTypes.bool,
  windowSize: PropTypes.string,
  data: PropTypes.object,
  name: PropTypes.string,
  darkModeEnabled: PropTypes.bool
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  darkModeEnabled: state.sidebar.darkModeEnabled
})

export default connect(mapStateToProps)(Point)
