import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { connect } from 'react-redux'

import StatusIcon from './StatusIcon'

import s from '../styles/StatusIcons.cssmodule.scss'

class StatusIcons extends Component {
  render() {
    const { myForwardedRef, sidebar: { statusIcons, windowSize } } = this.props
    // const size = windowSize === 'mobile' ? 'big' : statusIcons.size

    const c = classnames({
      [s['status-icons']]: true,
      [s['mobile']]: windowSize === 'mobile',
      [s['big']]: true
    })

    return (
      <ul ref={myForwardedRef} className={c}>
        {statusIcons.icons && Object.keys(statusIcons.icons).map((icon, i) => {
          const currIcon = statusIcons.icons[icon]

          return <StatusIcon icon={currIcon} iconKey={icon} key={i} />
        })}
      </ul>
    )
  }
}

StatusIcons.propTypes = {
  myForwardedRef: PropTypes.object,
  sidebar: PropTypes.object
}

StatusIcons.defaultProps = {
  myForwardedRef: {},
  sidebar: {}
}

const mapStateToProps = state => ({
  sidebar: state.sidebar
})

const ConnectedStatusIcons = connect(mapStateToProps)(StatusIcons)

export default React.forwardRef((props, ref) => <ConnectedStatusIcons {...props} myForwardedRef={ref} />)
