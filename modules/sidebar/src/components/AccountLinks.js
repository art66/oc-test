import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { ACCOUNT_SVG_ICONS } from '../constants/svgIconsDimensions'
import { getAccountSVGIconsColors } from '../constants/svgIconsProps'
import s from '../styles/AccountLinks.cssmodule.scss'

class AccountLinks extends Component {
  _getWrapClass = elem => {
    if (elem.status === 'active') {
      return `${s['wrap']} ${s['active']}`
    }

    return `${s['wrap']}`
  }

  _renderSVGIcon = (icon, status) => {
    const { active, regular } = getAccountSVGIconsColors()
    const { fill, filter, gradient } = status ? active : regular
    const { name, dimensions } = icon

    return <SVGIconGenerator iconName={name} fill={fill} dimensions={dimensions} filter={filter} gradient={gradient} />
  }

  _classNameHolder = () => {
    const {
      account: { messages = {}, events = {}, awards = {} }
    } = this.props

    const classesMessages = classnames({
      [s['linkWrap']]: true,
      [s['messages']]: true,
      [s['positive']]: messages.amount
    })

    const classesEvents = classnames({
      [s['linkWrap']]: true,
      [s['events']]: true,
      [s['positive']]: events.amount
    })

    const classesAwards = classnames({
      [s['linkWrap']]: true,
      [s['awards']]: true,
      [s['positive']]: awards.amount
    })

    return {
      classesMessages,
      classesEvents,
      classesAwards
    }
  }

  render() {
    const {
      account: { messages = {}, events = {}, awards = {} }
    } = this.props

    const { classesMessages, classesEvents, classesAwards } = this._classNameHolder()
    const { awardsCon, eventsCon, messagesCon } = ACCOUNT_SVG_ICONS

    return (
      <div className={s['account-links-wrap']}>
        <div className={s['account-links']}>
          <div className={this._getWrapClass(messages)}>
            <a href={messages.link} className={classesMessages} id='nav-messages'>
              {this._renderSVGIcon(messagesCon, messages.amount)}
              {messages.amount ? <span className={s['amount']}>{messages.amount}</span> : ''}
            </a>
          </div>
          <div className={this._getWrapClass(events)}>
            <a href={events.link} className={classesEvents} id='nav-events'>
              {this._renderSVGIcon(eventsCon, events.amount)}
              {events.amount ? <span className={s['amount']}>{events.amount}</span> : ''}
            </a>
          </div>
          <div className={this._getWrapClass(awards)}>
            <a href={awards.link} className={classesAwards} id='nav-awards'>
              {this._renderSVGIcon(awardsCon, awards.amount)}
              {awards.amount ? <span className={s['amount']}>{awards.amount}</span> : ''}
            </a>
          </div>
        </div>
      </div>
    )
  }
}

AccountLinks.propTypes = {
  account: PropTypes.object,
  darkModeEnabled: PropTypes.bool
}

const mapStateToProps = state => ({
  account: state.sidebar.account,
  darkModeEnabled: state.sidebar.darkModeEnabled
})

export default connect(mapStateToProps)(AccountLinks)
