import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import iconsSet from '@torn/shared/SVG/helpers/iconsHolder'
import List from './List'
import SVGIcon from './AreaSVGIcon'
import { addFavoriteArea, removeFavoriteArea, setListLinkStatus, linkContextMenu } from '../modules'
import { formatAreasCounter } from '../utils'
import s from '../styles/Area.cssmodule.scss'
import mainStyles from '../styles/Sidebar.cssmodule.scss'

class Area extends Component {
  constructor(props) {
    super(props)

    this.state = {
      favorite: false,
      contextMenuFading: false,
      pressed: false
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { linkWithContextMenu } = prevState

    if (nextProps.linkWithContextMenu && nextProps.linkWithContextMenu !== linkWithContextMenu) {
      return {
        contextMenuFading: false,
        linkWithContextMenu
      }
    }

    return {
      linkWithContextMenu
    }
  }

  componentDidMount() {
    const {
      area: { favorite }
    } = this.props

    this.setState({
      favorite
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { windowSize } = this.props

    if (windowSize === 'mobile') {
      const contextMenuStateChanged = this.contextMenuStateChanged(nextProps.linkWithContextMenu)
      const nextAreasPropsInSlider = {
        qty: nextProps.areasQty,
        index: nextProps.index,
        areasPerSlide: nextProps.areasPerSlide
      }
      const areaStateChangedInSlider = this.areaStateChangedInSlider(nextState, nextAreasPropsInSlider)

      if (contextMenuStateChanged) {
        return true
      }

      if (areaStateChangedInSlider) {
        return true
      }
    }

    const newData = { ...nextProps.area, ...nextProps.user, ...nextProps.listsLinksStatuses }
    const commonNextProps = {
      windowSize: nextProps.windowSize,
      field: nextProps.field,
      dataSync: nextProps.dataSync,
      darkModeEnabled: nextProps.darkModeEnabled
    }

    return this.commonDataChanged(newData, commonNextProps) || this.state.pressed !== nextState.pressed
  }

  handlePreventContextMenu = e => {
    const { windowSize } = this.props

    if (windowSize !== 'mobile') {
      return
    }

    e.preventDefault()
    e.stopPropagation()

    return false
  }

  areaStateChangedInSlider = (nextState, nextProps) => {
    const { favorite, contextMenuFading } = this.state
    const { areasQty, areasPerSlide, index } = this.props
    const stateChanged = nextState.favorite !== favorite || nextState.contextMenuFading !== contextMenuFading
    const propsChanged =
      nextProps.qty !== areasQty || nextProps.index !== index || nextProps.areasPerSlide !== areasPerSlide

    return stateChanged || propsChanged
  }

  contextMenuStateChanged = nextLinkWithContextMenu => {
    const { linkWithContextMenu, area } = this.props
    const linkWithContextMenuChanged = nextLinkWithContextMenu !== linkWithContextMenu
    const contextMenuClosing = linkWithContextMenu === area.linkOrder
    const contextMenuOpening = nextLinkWithContextMenu === area.linkOrder

    return linkWithContextMenuChanged && (contextMenuClosing || contextMenuOpening)
  }

  commonDataChanged = (newData, nextProps) => {
    const { area, user, listsLinksStatuses, windowSize, field, dataSync, darkModeEnabled } = this.props
    const oldData = { ...area, ...user, ...listsLinksStatuses }
    const dataUpdated = JSON.stringify(newData) !== JSON.stringify(oldData)

    return (
      nextProps.windowSize !== windowSize
      || nextProps.darkModeEnabled !== darkModeEnabled
      || nextProps.field !== field
      || nextProps.dataSync !== dataSync
      || dataUpdated
    )
  }

  pressLink = () => {
    const { linkContextMenuRun } = this.props

    this.disableClickOnLink = false

    this.toggleFvoriteTimeout = setTimeout(() => {
      const { area, areasSliderActive } = this.props

      this.disableClickOnLink = true

      if (!areasSliderActive) {
        linkContextMenuRun(area.linkOrder)
      }
    }, 500)
  }

  unPressLink = e => {
    const { areasSliderActive, area, linkContextMenuRun } = this.props

    clearTimeout(this.toggleFvoriteTimeout)

    if (!areasSliderActive && !this.disableClickOnLink) {
      this.pressArealLink()
    }

    if (e.ctrlKey) {
      return
    }

    if (this.disableClickOnLink && !areasSliderActive) {
      e.preventDefault()

      linkContextMenuRun(area.linkOrder)
    }
  }

  toggleContent = e => {
    const {
      setListLinkStatusRun,
      area: { elements, name }
    } = this.props

    if (elements === null) {
      return
    }

    e.preventDefault()

    setListLinkStatusRun(name)
  }

  toggleLinkFavoriteStatus = () => {
    const { favorite } = this.state
    const { setFavoriteNew, addFavoriteAreaRun, removeFavoriteAreaRun, area } = this.props

    setFavoriteNew()

    if (favorite) {
      removeFavoriteAreaRun(area)

      this.setState({ favorite: false })
    } else {
      addFavoriteAreaRun(area)

      this.setState({ favorite: true })
    }
  }

  isContextMenuShiftedRight = () => {
    const { areasPerSlide, index, areasQty } = this.props

    const number = index + 1

    if (number === 1 || number === 2) {
      return true
    } else if (number - 1 < areasPerSlide) {
      return false
    } else if (number < areasPerSlide) {
      return false
    } else if (number === areasQty || number + 1 === areasQty) {
      return false
    } else if (number - (1 % areasPerSlide) === 0) {
      return true
    } else if ((number - 1 - areasPerSlide) % areasPerSlide === 0) {
      return true
    } else if ((number - 2 - areasPerSlide) % areasPerSlide === 0) {
      return true
    }
  }

  isContextMenuShiftedLeft = () => {
    const { areasPerSlide, index } = this.props
    const number = index + 1

    if (number < areasPerSlide) {
      return false
    }
    if (number % areasPerSlide === 0) {
      return true
    }
    if ((number - areasPerSlide) % areasPerSlide === 0) {
      return true
    }
  }

  _renderAreaSVGIcon = () => {
    const {
      field,
      windowSize,
      user: { status: userStatus } = {},
      area: { icon, status, styleStatus } = {},
      darkModeEnabled
    } = this.props

    const SVGConfig = {
      windowSize,
      field,
      userStatus,
      iconName: icon,
      status: this.state.pressed ? 'active' : status,
      styleStatus,
      isHover: false
    }

    return <SVGIcon sidebarIcons={iconsSet} darkModeEnabled={darkModeEnabled} {...SVGConfig} />
  }

  _renderPopUpSVGIcon = icon => {
    const { favorite } = this.state
    const { user: { status: userStatus } = {}, darkModeEnabled } = this.props

    const isStarredArea = icon === 'Star' && favorite ? 'gold' : ''

    const SVGconfig = {
      windowSize: 'mobile',
      iconName: icon,
      userStatus,
      isFullHeight: true,
      styleStatus: isStarredArea,
      isHover: true
    }

    return <SVGIcon darkModeEnabled={darkModeEnabled} {...SVGconfig} />
  }

  renderFavoriteStar = () => {
    const { favorite } = this.state
    const { windowSize } = this.props

    if (!favorite || windowSize !== 'mobile') {
      return null
    }

    return (
      <span className={s.mobileSVGFavoriteStar}>
        <SVGIconGenerator
          iconsHolder={iconsSet}
          iconName='Star'
          dimensions={{ width: 7, height: 7, viewbox: '0 0 5 5' }}
          filter={{ active: false, ID: null }}
          fill={{ color: '#9f7c1a', strokeWidth: 0 }}
        />
      </span>
    )
  }

  renderCounter = () => {
    const {
      windowSize,
      area: { amount }
    } = this.props

    return windowSize === 'mobile' && amount ? <div className={s.mobileAmount}>{formatAreasCounter(amount)}</div> : null
  }

  pressArealLink = () => {
    if (this.props.windowSize === 'mobile') {
      this.setState({
        pressed: true
      })
    }
  }

  renderAreaIcon = () => {
    const {
      windowSize,
      area: { link, name, shortName, amount }
    } = this.props

    if (windowSize === 'mobile') {
      return (
        <a
          href={link}
          tabIndex='0'
          className={`${s.mobileLink} sidebarMobileLink`}
          onMouseDown={this.pressLink}
          onClick={this.unPressLink}
          onTouchStart={this.pressLink}
          onTouchEnd={this.unPressLink}
          onContextMenu={this.handlePreventContextMenu}
        >
          {this._renderAreaSVGIcon()}
          <span>{shortName || name}</span>
        </a>
      )
    }

    return (
      <a href={link} className={s.desktopLink}>
        {this._renderAreaSVGIcon()}
        <span className={s.linkName}>{name}</span>
        {amount ? <span className={s.amount}>{amount}</span> : null}
      </a>
    )
  }

  renderToggle = () => {
    const {
      windowSize,
      listsLinksStatuses,
      area: { name, elements }
    } = this.props

    const arrowClass = classnames({
      [s['arrow']]: true,
      [s['activated']]: listsLinksStatuses[name.toLowerCase()],
      [s['disabled']]: !elements || !elements.length
    })

    if ((elements || elements === null) && windowSize !== 'mobile') {
      return (
        <div role='button' tabIndex={0} onKeyPress={undefined} className={s['info']} onClick={this.toggleContent}>
          <span className={s['amount']}>{elements ? elements.length : 0}</span>
          <span className={arrowClass} />
        </div>
      )
    }

    return null
  }

  renderLinkList = () => {
    const {
      listName,
      windowSize,
      listsLinksStatuses,
      area: { name, elements }
    } = this.props

    if (windowSize !== 'mobile' && elements && listsLinksStatuses[name.toLowerCase()]) {
      return <List lists={elements} listName={listName} />
    }

    return null
  }

  renderContextMenu = () => {
    const { contextMenuFading } = this.state
    const {
      linkWithContextMenu,
      windowSize,
      area: { link, linkOrder }
    } = this.props

    const contextClass = classnames(s.contextMenu, {
      [s.fading]: contextMenuFading,
      [s.shiftRight]: this.isContextMenuShiftedRight()
    })

    if (linkOrder && linkWithContextMenu === linkOrder && windowSize === 'mobile') {
      return (
        <div className={contextClass} onMouseDown={this.preventClick}>
          <button
            type='button'
            style={{ border: 'none', padding: 0, margin: 0, outline: 'none', background: 'transparent' }}
            className={s.contexMenuOption}
            onContextMenu={this.handlePreventContextMenu}
            onClick={this.toggleLinkFavoriteStatus}
          >
            {this._renderPopUpSVGIcon('Star')}
          </button>
          <a
            href={link}
            rel='noopener noreferrer'
            target='_blank'
            className={s.contexMenuOption}
            onContextMenu={this.handlePreventContextMenu}
          >
            {this._renderPopUpSVGIcon('FollowLinks')}
          </a>
          <span className={s.arrow} />
        </div>
      )
    }

    return null
  }

  render() {
    const { favorite, pressed } = this.state
    const {
      user,
      field,
      windowSize,
      dataSync,
      area: { link, status, styleStatus }
    } = this.props

    let selfStatus = status

    if (!dataSync && (!status || status === 'active')) {
      selfStatus = window.location.href.indexOf(link) !== -1 ? 'active' : null
    }

    const areaClass = classnames({
      [s[`area-${windowSize}`]]: true,
      [s['favorite']]: favorite,
      [s['available']]: selfStatus && selfStatus === 'available',
      [s['active']]: (selfStatus && selfStatus === 'active') || pressed,
      [mainStyles['highlight-active']]: field === 'rules',
      [s['warning']]: styleStatus === 'red' || field === 'rules',
      [s['attention']]: styleStatus === 'blue',
      [s['in-jail']]: user.status && user.status === 'inJail',
      [s['in-hospital']]: user.status && user.status === 'inHospital'
    })

    return (
      <div className={areaClass} id={`nav-${field}`}>
        <div className={s['area-row']}>
          {this.renderFavoriteStar()}
          {this.renderCounter()}
          {this.renderAreaIcon()}
          {this.renderToggle()}
        </div>
        {this.renderLinkList()}
        {this.renderContextMenu()}
      </div>
    )
  }
}

Area.propTypes = {
  area: PropTypes.object,
  user: PropTypes.object,
  field: PropTypes.string,
  windowSize: PropTypes.string,
  areasSliderActive: PropTypes.bool,
  dataSync: PropTypes.bool,
  favorite: PropTypes.bool,
  addFavoriteAreaRun: PropTypes.func,
  removeFavoriteAreaRun: PropTypes.func,
  setFavoriteNew: PropTypes.func,
  setListLinkStatusRun: PropTypes.func,
  listsLinksStatuses: PropTypes.object,
  listName: PropTypes.string,
  linkWithContextMenu: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  linkContextMenuRun: PropTypes.func,
  areasPerSlide: PropTypes.number,
  index: PropTypes.number,
  areasQty: PropTypes.number,
  darkModeEnabled: PropTypes.bool
}

Area.defaultProps = {
  area: {},
  user: {},
  field: '',
  windowSize: '',
  areasSliderActive: false,
  dataSync: false,
  favorite: false,
  addFavoriteAreaRun: () => {},
  removeFavoriteAreaRun: () => {},
  setFavoriteNew: () => {},
  setListLinkStatusRun: () => {},
  listsLinksStatuses: {},
  listName: '',
  linkWithContextMenu: '',
  linkContextMenuRun: () => {},
  areasPerSlide: null,
  index: null,
  areasQty: null
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  areasSliderActive: state.sidebar.areasSliderActive,
  dataSync: state.sidebar.dataSync,
  user: state.sidebar.user,
  listsLinksStatuses: state.sidebar.listsLinksStatuses,
  linkWithContextMenu: state.sidebar.linkWithContextMenu,
  darkModeEnabled: state.sidebar.darkModeEnabled
})

const mapDispatchToProps = {
  addFavoriteAreaRun: addFavoriteArea,
  removeFavoriteAreaRun: removeFavoriteArea,
  setListLinkStatusRun: setListLinkStatus,
  linkContextMenuRun: linkContextMenu
}

export default connect(mapStateToProps, mapDispatchToProps)(Area)
