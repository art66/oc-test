import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Swiper, Slide } from 'react-dynamic-swiper'
import classnames from 'classnames'
import { DebounceDOM } from '@torn/shared/utils/debounce'

import Area from './Area'
import ToggleBlock from './ToggleBlock'

import { setAreasSliderStatus, setSliderHintDisable, linkContextMenu } from '../modules'

import s from '../styles/Areas.cssmodule.scss'

const SWIPER_UPDATE_TIME = 100

class Areas extends Component {
  constructor(props) {
    super(props)

    this.state = {
      windowWidth: window.innerWidth || document.documentElement.clientWidth,
      favoriteNew: false
    }
  }

  componentDidMount() {
    this.sortedAreas = this.sortAreas(this.areasToArray())

    if (this.isMobile()) {
      this.handleSliderHintAppear()

      // it should be used through onBlur event on the Area button itself.
      // Besides onBlue event doesn't fire on mobile Chrome & Safari.
      // So it should be handle by document event globally
      window.addEventListener('click', this.handleContextMenuHide)
      window.addEventListener('focus', this.handleWindowFocus)
    }

    this.trackCurrentWidth()
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { windowWidth } = this.state
    const {
      sidebar: { areas, lists, account, linkWithContextMenu }
    } = this.props

    const nextData = { ...nextProps.sidebar.account, ...nextProps.sidebar.areas, ...nextProps.sidebar.lists }
    const oldData = { ...account, ...areas, ...lists }
    const newPropsNotAvailable = JSON.stringify(nextData) === JSON.stringify(oldData)

    if (this.isMobile()) {
      if (nextState.windowWidth !== windowWidth) {
        return true
      }
      if (linkWithContextMenu !== nextProps.sidebar.linkWithContextMenu) {
        return true
      }
    }

    return !newPropsNotAvailable
  }

  componentDidUpdate(prevProps) {
    const {
      sidebar: { windowSize: prevWindowsSize }
    } = prevProps
    const {
      sidebar: { windowSize }
    } = this.props

    if (!this.swiper) {
      return
    }

    this.runSwiperTimer()

    if (prevWindowsSize !== windowSize && this.isMobile()) {
      window.removeEventListener('click', this.handleContextMenuHide)
      window.removeEventListener('focus', this.handleWindowFocus)
      window.addEventListener('click', this.handleContextMenuHide)
      window.addEventListener('focus', this.handleWindowFocus)
    }
  }

  componentWillUnmount() {
    this.untrackedCurrentWidth()
    this.removeSliderHintTimer()
    this.removeSwiperTimer()

    window.removeEventListener('click', this.hideHintTooltip)
    window.removeEventListener('resize', this.handleChangeWidth)
    window.removeEventListener('click', this.handleContextMenuHide)
    window.removeEventListener('focus', this.handleWindowFocus)
  }

  getAreasAmount = () => {
    const { windowWidth } = this.state

    if (windowWidth <= 150) {
      return 3
    } else if (windowWidth <= 200) {
      return 4
    } else if (windowWidth <= 250) {
      return 6
    } else if (windowWidth <= 280) {
      return 7
    } else if (windowWidth <= 358) {
      return 8
    } else if (windowWidth <= 400) {
      return 9
    } else if (windowWidth <= 450) {
      return 10
    } else if (windowWidth <= 500) {
      return 11
    }

    return 12
  }

  handleWindowFocus = () => {
    this.saveSwiperTransform()
  }

  handleSliderHintAppear = () => {
    const {
      sidebar: { sliderHintDisabled }
    } = this.props

    if (!sliderHintDisabled) {
      this.runSliderHintTimer()

      window.addEventListener('click', this.hideHintTooltip)
    }
  }

  handleInitSwiper = swiper => {
    this.swiper = swiper
    this.updateSlides(swiper)
  }

  handleContextMenuHide = e => {
    const currentNode = e.target
    const { linkContextMenuRun } = this.props

    this.saveSwiperTransform()
    !currentNode.closest('.sidebarMobileLink') && linkContextMenuRun(0)
  }

  // some legacy "swiper" arg here, omit it for now
  // eslint-disable-next-line no-unused-vars
  handleSlide = (swiper, e) => {
    e.stopPropagation()

    const { setAreasSliderStatusRun } = this.props

    setAreasSliderStatusRun(true)
    this.saveSwiperTransform()
  }

  handleTouchEnd = () => {
    this.saveSwiperTransform()

    setTimeout(() => {
      const { setAreasSliderStatusRun } = this.props

      setAreasSliderStatusRun(false)
    }, 200)
  }

  handleChangeWidth = () => {
    this.setCurrentWidth()
    if (this.swiper) {
      this.resetSwiperTransform()
    }
  }

  resetSwiperTransform = () => {
    this.swiperTransform = 'translate3d(0px, 0px, 0px)'
  }

  setCurrentWidth = () => {
    this.setState({ windowWidth: window.innerWidth || document.documentElement.clientWidth })
  }

  removeSwiperTimer = () => clearInterval(this._swiperTimerID)

  runSwiperTimer = () => {
    const translate = this.swiper.getTranslate(this.swiper.wrapper[0])

    if (translate > 0) {
      this.resetSwiperTransform()
    }

    this._swiperTimerID = setTimeout(() => {
      this.updateSwiperWrapperTransform()
    }, SWIPER_UPDATE_TIME)
  }

  updateSwiperWrapperTransform = () => {
    this.swiper.wrapper[0].style.transform = this.swiperTransform
  }

  removeSliderHintTimer = () => clearInterval(this._timerSlideID)

  runSliderHintTimer = () => {
    this._timerSlideID = setTimeout(() => {
      this.hideHintTooltip()
    }, 15000)
  }

  isMobile = () => {
    const {
      sidebar: { windowSize }
    } = this.props

    return windowSize === 'mobile'
  }

  saveSwiperTransform = () => {
    if (this.swiper) {
      this.swiperTransform = this.swiper.wrapper[0].style.transform
    }
  }

  trackCurrentWidth = () => {
    this._debounce = new DebounceDOM()
    this._debounce.run({ event: 'resize', callback: this.handleChangeWidth, delay: 100, target: window })
  }

  untrackedCurrentWidth = () => this._debounce.stop()

  areasClassHolder = () => {
    const {
      sidebar: { user, windowSize }
    } = this.props

    const areasClass = classnames({
      [s['areas']]: true,
      areasWrapper: true,
      [s[`areas-${windowSize}`]]: true,
      [s['in-jail']]: this.isMobile() && user.status && user.status === 'inJail',
      [s['in-hospital']]: this.isMobile() && user.status && user.status === 'inHospital'
    })

    return areasClass
  }

  setFavoriteNew = () => {
    const { favoriteNew } = this.state

    if (!favoriteNew) {
      this.setState({
        favoriteNew: true
      })
    }
  }

  showStartHint() {
    const { sidebar } = this.props

    if (!sidebar.sliderHintDisabled) {
      return (
        <div className={s['start-hint']}>
          <p>Swipe to browse areas</p>
          <p>Tap and hold to save favorites</p>
        </div>
      )
    }

    return null
  }

  // swiper must be update/resize view in init
  updateSlides = swiper => {
    swiper.update(true)
    clearTimeout(this.updateSliderTimeout)
    // transform should be manually updated after call swiper.update
    // for preventing glitches when calling context menu on the areas
    this.updateSwiperWrapperTransform()

    this.updateSliderTimeout = setTimeout(() => {
      swiper.update(true)
      this.updateSwiperWrapperTransform()
    }, 100)
  }

  hideHintTooltip = () => {
    const { setSliderHintDisableRun } = this.props

    setSliderHintDisableRun()
    this.forceUpdate()
  }

  sortAreas = allAreas => {
    const favorites = []
    const notFavorites = []

    allAreas.forEach(area => {
      if (area.favorite) {
        favorites.push(area)
      } else {
        notFavorites.push(area)
      }
    })

    const sortedFavorites = favorites.sort((a, b) => {
      return a.added - b.added
    })

    return [...sortedFavorites, ...notFavorites]
  }

  areasToArray() {
    const {
      sidebar: { account, areas, lists }
    } = this.props

    const getListElements = (list, listName) => {
      return Object.keys(list).map(elem => {
        return {
          ...list[elem],
          areaName: elem,
          parentName: listName
        }
      })
    }

    const areasArray = getListElements(areas, 'areas')
    const listsArray = getListElements(lists, 'lists')
    const accountArray = getListElements(account, 'account')

    return [...accountArray, ...areasArray, ...listsArray]
  }

  renderDesktopAreas = () => {
    const {
      sidebar: { areas }
    } = this.props

    const areasClass = this.areasClassHolder()
    const areasMapKeys = Object.keys(areas)

    if (!areas || areasMapKeys.length === 0) {
      return null
    }

    const areasToRender = areasMapKeys.map((area, i) => (
      <Area area={areas[area]} field={area} key={i} />
    ))

    return (
      <div className={areasClass}>
        <ToggleBlock title='Areas' toggle>
          {areasToRender}
        </ToggleBlock>
      </div>
    )
  }

  renderMobileAreas = () => {
    const { favoriteNew } = this.state
    const {
      sidebar: { linkWithContextMenu }
    } = this.props

    const areasAmount = this.getAreasAmount()
    const areasClass = this.areasClassHolder()

    const options = {
      slidesPerView: areasAmount,
      slidesPerGroup: areasAmount,
      longSwipesRatio: 0.1
    }

    let selfSortedAreas = this.sortedAreas

    if (!favoriteNew) {
      selfSortedAreas = this.sortAreas(this.areasToArray())
      this.sortedAreas = selfSortedAreas
    }

    return (
      <div className={areasClass}>
        {this.showStartHint()}
        <Swiper
          onInitSwiper={this.handleInitSwiper}
          swiperOptions={options}
          pagination={false}
          prevButton={<div className={`${s['swiper-button']} ${s['button-prev']}`} />}
          nextButton={<div className={`${s['swiper-button']} ${s['button-next']}`} />}
          onSliderMove={this.handleSlide}
          onTouchEnd={this.handleTouchEnd}
          onTouchStart={this.saveSwiperTransform}
          wrapperClassName={s['swiper']}
        >
          {selfSortedAreas.map((area, i) => {
            return (
              <Slide
                key={i}
                className={classnames(s['slide'], { [s.contextMenuActive]: linkWithContextMenu === area.linkOrder })}
              >
                <Area
                  area={area}
                  field={area.areaName}
                  key={i}
                  index={i}
                  areasQty={selfSortedAreas.length}
                  setFavoriteNew={this.setFavoriteNew}
                  areasPerSlide={areasAmount}
                />
              </Slide>
            )
          })}
        </Swiper>
      </div>
    )
  }

  render() {
    const {
      sidebar: { windowSize }
    } = this.props

    if (windowSize === 'desktop' || windowSize === 'tablet') {
      return this.renderDesktopAreas()
    }

    return this.renderMobileAreas()
  }
}

Areas.propTypes = {
  sidebar: PropTypes.object,
  setAreasSliderStatusRun: PropTypes.func,
  setSliderHintDisableRun: PropTypes.func,
  windowWidth: PropTypes.number,
  linkContextMenuRun: PropTypes.func,
  linkWithContextMenu: PropTypes.number
}

Areas.defaultProps = {
  sidebar: {},
  setAreasSliderStatusRun: () => {},
  setSliderHintDisableRun: () => {},
  windowWidth: null,
  linkContextMenuRun: () => {},
  linkWithContextMenu: null
}

const mapStateToProps = state => ({
  sidebar: state.sidebar,
  windowWidth: state.browser.width
})

const mapDispatchToProps = {
  setAreasSliderStatusRun: setAreasSliderStatus,
  setSliderHintDisableRun: setSliderHintDisable,
  linkContextMenuRun: linkContextMenu
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Areas)
