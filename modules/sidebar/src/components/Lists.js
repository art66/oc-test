import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Area from './Area'
import ToggleBlock from './ToggleBlock'

class Lists extends Component {
  getAreas() {
    const { lists } = this.props

    return (
      <ToggleBlock title="Lists" toggle>
        {Object.keys(lists).map((list, i) => {
          return <Area area={lists[list]} listName={list} key={i} field={list} />
        })}
      </ToggleBlock>
    )
  }
  render() {
    return this.getAreas()
  }
}

Lists.propTypes = {
  lists: PropTypes.object
}

const mapStateToProps = state => ({
  lists: state.sidebar.lists
})

export default connect(mapStateToProps)(Lists)
