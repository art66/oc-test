import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { shortMoney } from '@torn/shared/utils'
import { formatTime, getOrdinalSuffix, getCurrentTimeInSeconds } from '../utils/index'
import Tooltip from './Tooltip'
import { updateBarTime, updateChainAmount, getBars } from '../modules'
import s from '../styles/ChainBar.cssmodule.scss'

const FIXED_COEFFICIENT_BONUSES = [0, 10, 25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000]
const COOLDOWN_SECONDS_PER_CHAIN = 6

class ChainBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      canFollowTheLink: false
    }
  }

  componentDidMount() {
    const {
      bar: { timestampToUpdate, endCoolDownTimestamp }
    } = this.props

    if (timestampToUpdate) {
      this.updateTime()
    }
    if (endCoolDownTimestamp) {
      this.updateCoolDown()
    }
  }

  componentDidUpdate(prevProps) {
    const { bar: { timestampToUpdate, endCoolDownTimestamp } = {} } = prevProps
    const { time, cooldown } = this.getTimerData()

    // update timer if attack start after component loaded
    if (time > 0 && timestampToUpdate === 0) {
      this.updateTime()
    }

    // update cooldown if attack start after component loaded
    if (!endCoolDownTimestamp && cooldown > 0) {
      this.updateCoolDown()
    }
  }

  componentWillUnmount() {
    clearInterval(this.timeUpdateInterval)
    clearInterval(this.cooldownUpdateInterval)
  }

  getOneBarTimeInTooltip() {
    const { time, cooldown } = this.getTimerData()

    if (time > 0) {
      return <span>({formatTime(time * 1000)})</span>
    } else if (cooldown > 0) {
      return <span>({formatTime(cooldown * 1000, true, true)})</span>
    }

    return null
  }

  getTimeValue = (timestamp, diff) => {
    return timestamp && diff > 0 ? diff : 0
  }

  getTimerData = () => {
    const {
      bar: { timestampToUpdate, endCoolDownTimestamp }
    } = this.props
    const currTimestamp = getCurrentTimeInSeconds()
    const timeDiff = timestampToUpdate - currTimestamp
    const cooldownDiff = endCoolDownTimestamp - currTimestamp
    const time = this.getTimeValue(timestampToUpdate, timeDiff)
    const cooldown = this.getTimeValue(endCoolDownTimestamp, cooldownDiff)

    return { time, cooldown }
  }

  getBarDescription() {
    const {
      bar: { endCoolDownTimestamp }
    } = this.props
    const { time, cooldown } = this.getTimerData()
    const timeLeftClass = classnames({
      [s['bar-timeleft']]: true,
      [s['bar-time-low']]: !endCoolDownTimestamp && time < 60 && time > 0
    })
    const formattedTime = cooldown > 0 ? formatTime(cooldown * 1000, true, true) : formatTime(time * 1000)

    return <p className={timeLeftClass}>{formattedTime}</p>
  }

  getValuesOnMobile() {
    const {
      bar: { amount, max, endCoolDownTimestamp },
      windowWidth
    } = this.props

    if (
      ((max >= 10000 && windowWidth < 510) || (max >= 1000 && windowWidth < 450) || windowWidth < 360)
      && !endCoolDownTimestamp
    ) {
      return <p className={s['bar-value']}>{amount}</p>
    } else if (endCoolDownTimestamp) {
      return <p className={s['bar-value']}>{max}</p>
    }

    return (
      <p className={s['bar-value']}>
        {amount} / {shortMoney(max)}
      </p>
    )
  }

  updateTime() {
    clearInterval(this.timeUpdateInterval)
    this.updateBar()
    this.timeUpdateInterval = setInterval(() => {
      this.updateBar()
    }, 1000)
  }

  updateBar() {
    const { getBars: updateBars } = this.props
    const { time } = this.getTimerData()

    if (time > 0) {
      this.forceUpdate()
    } else {
      updateBars()
      clearInterval(this.timeUpdateInterval)
    }
  }

  updateCoolDown() {
    clearInterval(this.cooldownUpdateInterval)
    this.updateBarCoolDown()
    this.cooldownUpdateInterval = setInterval(() => {
      this.updateBarCoolDown()
    }, 1000)
  }

  updateBarCoolDown() {
    const { updateChainAmount: updateAmount, getBars: updateBars } = this.props
    const { cooldown } = this.getTimerData()

    if (cooldown > 1) {
      this.forceUpdate()
    } else {
      clearInterval(this.cooldownUpdateInterval)
      updateAmount(true)
      updateBars()
    }
  }

  createTooltip(blockId, position) {
    const {
      bar: { amount, max, bonuses }
    } = this.props
    const { time, cooldown } = this.getTimerData()
    let title
    let message

    if (cooldown > 0) {
      title = `Chain ${max} achieved`
      message = `This chain will complete its cooldown in ${formatTime(cooldown * 1000, true, true)}`
    } else if (amount === 0) {
      title = 'Chain inactive'
      message = 'Make 10 hits within 5 minutes to start a chain'
    } else if (amount >= 1 && amount <= 9) {
      title = 'Chain warm-up'
      message = `Make ${10 - amount} more hits within ${formatTime(time * 1000)} to start a chain`
    } else if (amount >= 10) {
      title = 'Chain active'
      const nextAmount = amount + 1

      message = `The ${nextAmount}${getOrdinalSuffix(nextAmount)} hit in this chain will
         provide ${this.isFixedBonus(max, amount) ? '' : 'x'}${bonuses} bonus respect`
    }

    return (
      <Tooltip position={position} arrow='center' parent={blockId}>
        {this.renderMobileTitleForTooltip()}
        <b>{title}</b>
        <p>{message}</p>
      </Tooltip>
    )
  }

  isFixedBonus = (max, amount) => FIXED_COEFFICIENT_BONUSES.includes(max) && max - amount === 1

  _handleClickOnChainBar = e => {
    if (this.props.bar?.link === '#' || !this.state.canFollowTheLink) {
      e.preventDefault()
    }
    if (!this.state.canFollowTheLink) {
      this.setState({ canFollowTheLink: true })
    }
  }

  renderTick = (item, tickWidth) => {
    return <li key={item} style={{ width: `${tickWidth}%` }} />
  }

  renderTicks = () => {
    const tickWidth = 10
    const ticks = [...new Array(tickWidth - 1)].map((_, i) => {
      return i + 1
    })

    return <ul className={s['tick-list']}>{ticks.map(item => this.renderTick(item, tickWidth))}</ul>
  }

  renderMobileTitleForTooltip = () => {
    const {
      windowSize,
      bar: { amount, max }
    } = this.props
    const { cooldown } = this.getTimerData()

    return windowSize === 'mobile' && !cooldown ? (
      <p>
        Chain: {amount} / {max}&nbsp;{this.getOneBarTimeInTooltip()}
      </p>
    ) : null
  }

  renderBonusesLabel = () => {
    const {
      bar: { amount, max, bonuses }
    } = this.props

    return bonuses && bonuses > 1 ? (
      <div className={s['speed']}>
        <span className={s['value']}>
          {this.isFixedBonus(max, amount) ? '' : 'x'}
          {bonuses}
        </span>
      </div>
    ) : null
  }

  render() {
    const {
      bar: { name, amount, max, link },
      windowSize
    } = this.props
    const { cooldown } = this.getTimerData()
    const barClass = classnames({
      [s['chain-bar']]: true,
      [s['bar-desktop']]: windowSize === 'desktop' || windowSize === 'tablet',
      [s['bar-mobile']]: windowSize === 'mobile'
    })
    const progressClass = classnames({
      [s['progress']]: true,
      [s['progress-40']]: amount >= 10 && amount < 25,
      [s['progress-50']]: amount >= 25,
      [s['cooldown']]: cooldown > 0
    })
    let progressWidth

    if (cooldown > 0) {
      progressWidth = `${Math.round((cooldown / COOLDOWN_SECONDS_PER_CHAIN / max) * 100)}%`
    } else {
      progressWidth = `${Math.round((amount / max) * 100)}%`
    }

    const blockId = `bar${name}`

    if (windowSize === 'desktop' || windowSize === 'tablet') {
      return (
        <a
          href={link}
          className={barClass}
          id={blockId}
          tabIndex={0}
          onClick={this._handleClickOnChainBar}
          onContextMenu={this._handleClickOnChainBar}
        >
          <div className={s['bar-stats']}>
            <p className={s['bar-name']}>{name}:</p>
            <p className={s['bar-value']}>{cooldown > 0 ? `${max}` : `${amount}/${shortMoney(max)}`}</p>
            {this.getBarDescription()}
          </div>
          <div className={progressClass}>
            <div className={s['progress-line-wrap']}>
              <div className={s['progress-line']} style={{ width: progressWidth }} />
            </div>
          </div>
          {this.renderBonusesLabel()}
          <div>{this.renderTicks()}</div>
          {this.createTooltip(blockId, 'right')}
        </a>
      )
    }
    return (
      <a
        href={link}
        className={barClass}
        id={blockId}
        tabIndex={0}
        onClick={this._handleClickOnChainBar}
        onContextMenu={this._handleClickOnChainBar}
      >
        <div className={s['bar-stats']}>
          <i className={`${s['bar-icon']} ${s[name.toLowerCase()]}`} />
          {this.getValuesOnMobile()}
        </div>

        <div className={progressClass}>
          <div className={s['progress-line']} style={{ width: progressWidth }} />
        </div>
        {this.createTooltip(blockId, 'bottom')}
      </a>
    )
  }
}

ChainBar.propTypes = {
  windowSize: PropTypes.string,
  windowWidth: PropTypes.number,
  bar: PropTypes.object,
  user: PropTypes.object,
  updateBarTime: PropTypes.func,
  updateChainAmount: PropTypes.func,
  getBars: PropTypes.func
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  windowWidth: state.browser.width,
  user: state.sidebar.user
})

const mapDispatchToProps = {
  updateBarTime,
  updateChainAmount,
  getBars
}

export default connect(mapStateToProps, mapDispatchToProps)(ChainBar)
