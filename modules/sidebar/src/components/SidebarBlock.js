import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import classnames from 'classnames'
import s from '../styles/SidebarBlock.cssmodule.scss'

class SidebarBlock extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    windowSize: PropTypes.string
  }

  static defaultProps = {
    children: null,
    windowSize: ''
  }

  render() {
    const { children, windowSize } = this.props

    const blockClass = classnames({
      [s['sidebar-block']]: true,
      [s[windowSize]]: true
    })

    return (
      <div className={`${blockClass}`}>
        <div className={s['content']}>{children}</div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize
})

export default connect(mapStateToProps)(SidebarBlock)
