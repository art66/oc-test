import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import s from '../styles/ProgressBars.cssmodule.scss'
import ProgressBar from './ProgressBar'
import ChainBar from './ChainBar'
import { createEventForEnergyRecovering, getCurrentTimeInSeconds } from '../utils'
import { getMillisecondsToBarUpdate, getBarTime } from '../utils/barsHelper'
import * as c from '../constants'
import { SVG_ICONS_PROPS_HOLDER } from '../constants/svgIconsProps'
import { updateBarTime } from '../modules'

class ProgressBars extends Component {
  componentDidMount() {
    const { bars } = this.props
    const needUpdateTime = Object.keys(bars).some(key => key !== c.CHAIN.toLowerCase() && bars[key].timestampToUpdate)

    this.syncEnergyEvent = createEventForEnergyRecovering()

    if (needUpdateTime) {
      this.updateTime()
    }
  }

  componentDidUpdate(prevProps) {
    const { bars } = this.props
    const { bars: prevBars } = prevProps
    const needUpdateTime = Object.keys(bars).some(
      key => key !== c.CHAIN.toLowerCase() && !prevBars[key].timestampToUpdate && bars[key].timestampToUpdate
    )

    if (needUpdateTime) {
      this.updateTime()
    }
  }

  componentWillUnmount() {
    clearInterval(this.timeUpdateInterval)
  }

  updateTime() {
    clearInterval(this.timeUpdateInterval)

    this.updateBars()
    this.timeUpdateInterval = setInterval(() => {
      this.updateBars()
    }, 1000)
  }

  updateBars = () => {
    const { bars } = this.props

    Object.keys(bars).forEach(key => {
      if (key !== c.CHAIN.toLowerCase()) {
        this.updateBar(bars[key])
      }
    })
  }

  updateBar(bar) {
    const { updateBarTime: updateTime, user } = this.props
    const oneBarTime = getMillisecondsToBarUpdate(bar, user) / 1000
    const defaultBarTime = getBarTime(bar, user).oneBarTime * 60
    const currTime = getCurrentTimeInSeconds()
    const timeDiff = bar.timestampToUpdate - currTime

    if (oneBarTime >= defaultBarTime - 1 && bar.timestampToUpdate !== 0) {
      updateTime(bar, true, false, true)
      if (bar.name === c.ENERGY) {
        window.dispatchEvent(this.syncEnergyEvent)
      }
    } else if (timeDiff + 1 > 0 && bar.timestampToUpdate) {
      this.forceUpdate()
    } else if (timeDiff + 1 === 0 && bar.timestampToUpdate) {
      updateTime(bar, true, true)

      if (this.checkIfNeedsClearInterval(currTime)) {
        clearInterval(this.timeUpdateInterval)
      }
    }
  }

  checkIfNeedsClearInterval = currTime => {
    const { bars } = this.props

    Object.keys(bars).every(key => {
      const timeDiff = bars[key].timestampToUpdate - currTime

      return timeDiff + 1 < 1 || !bars[key].timestampToUpdate
    })
  }

  render() {
    const { windowSize, bars } = this.props
    const barsClass = windowSize === 'mobile' ? s['bars-mobile'] : ''

    return (
      <div className={barsClass}>
        <ProgressBar bar={bars.energy} svgIcon={{ ...SVG_ICONS_PROPS_HOLDER.Energy }} />
        <ProgressBar bar={bars.nerve} svgIcon={{ ...SVG_ICONS_PROPS_HOLDER.Nerve }} />
        <ProgressBar bar={bars.happy} svgIcon={{ ...SVG_ICONS_PROPS_HOLDER.Happy }} />
        <ProgressBar bar={bars.life} svgIcon={{ ...SVG_ICONS_PROPS_HOLDER.Life }} />
        {bars.chain ? <ChainBar bar={bars.chain} svgIcon={{ ...SVG_ICONS_PROPS_HOLDER.Chain }} /> : ''}
      </div>
    )
  }
}

ProgressBars.propTypes = {
  windowSize: PropTypes.string,
  bars: PropTypes.object,
  updateBarTime: PropTypes.func,
  user: PropTypes.object
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  user: state.sidebar.user,
  bars: state.sidebar.bars
})

const mapDispatchToProps = {
  updateBarTime
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgressBars)
