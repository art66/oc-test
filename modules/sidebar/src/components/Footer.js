import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from '../styles/Footer.cssmodule.scss'
import classnames from 'classnames'
import { connect } from 'react-redux'

const TICK_INTERVAL = 1000

/* global getCurrentTimestamp */
class Footer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      serverTime: getCurrentTimestamp(),
      serverTimeString: '',
      serverDateString: ''
    }
    this.tick = this.tick.bind(this)
    this.updateState = this.updateState.bind(this)
  }

  // deprecated due to react v.16.8 migration
  // UNSAFE_componentWillMount() {
  //   this.tick()
  //   this.timer = setInterval(this.tick, TICK_INTERVAL)
  //   window.addEventListener('focus', this.updateState)
  // }

  componentDidMount() {
    this.tick()
    this.timer = setInterval(this.tick, TICK_INTERVAL)
    window.addEventListener('focus', this.updateState)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
    window.removeEventListener('focus', this.updateState)
  }

  updateState() {
    this.setState({
      serverTime: getCurrentTimestamp()
    })
  }

  tick() {
    let week = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

    function parseNumber(n) {
      return n < 10 ? '0' + n : n
    }

    let date = new Date(this.state.serverTime)
    let h = parseNumber(date.getUTCHours())
    let m = parseNumber(date.getUTCMinutes())
    let s = parseNumber(date.getUTCSeconds())
    let d = parseNumber(date.getUTCDate())
    let mth = parseNumber(date.getUTCMonth() + 1)
    let y = date
      .getUTCFullYear()
      .toString()
      .substr(2)

    let strTime = week[date.getUTCDay()] + ' ' + h + ':' + m + ':' + s
    let strDate = '- ' + d + '/' + mth + '/' + y

    this.setState({
      serverTime: this.state.serverTime + 1000,
      serverTimeString: strTime,
      serverDateString: strDate
    })
  }

  render() {
    const { footer } = this.props
    const { serverTimeString, serverDateString } = this.state
    const c = classnames({
      [s['footer-menu']]: true,
      [s['left']]: true
    })

    return (
      <div className={c}>
        <div className={s['date']}>
          <span className={s['server-time']}> {serverTimeString} </span>
          <span className={s['server-date']}> {serverDateString} </span>
        </div>
        <div className={s['server']}>
          <span className={s['server-title']}> Server: </span>
          <a className={s['server-name']} href="/usersonline.php">
            {' '}
            {footer.serverName}
          </a>
        </div>
      </div>
    )
  }
}

Footer.propTypes = {
  footer: PropTypes.object
}

const mapStateToProps = state => ({
  footer: state.sidebar.footer
})

export default connect(mapStateToProps)(Footer)
