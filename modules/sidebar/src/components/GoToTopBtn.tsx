import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import cn from 'classnames'
import s from '../styles/GoToTop.cssmodule.scss'
const MOBILE_LEFT_OFFSET = 20
const SIDEBAR_OFFSET = 40
const ITERATION_DURATION = 5
const ANIMATION_DURATION = 250
const AMOUNT_ITERATIONS = ANIMATION_DURATION / ITERATION_DURATION

interface IProps {
  browser: {
    mediaType: string
  }
  windowSize: string
}

interface IState {
  left: number
  show: boolean
}

class GoToTopBtn extends Component<IProps, IState> {
  el: HTMLElement
  constructor(props: IProps) {
    super(props)
    this.el = document.createElement('div')
    this.el.id = 'go-to-top-btn'
    this.state = {
      left: this.getLeftOffset(),
      show: false
    }
  }

  componentDidMount() {
    if (!document.getElementById('go-to-top-btn')) {
      document.body.appendChild(this.el)
    }
    this.showGoToTopBtn()
    window.addEventListener('resize', this.updateLeftOffset)
    window.addEventListener('resize', this.showGoToTopBtn)
    window.addEventListener('scroll', this.showGoToTopBtn)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateLeftOffset)
    window.removeEventListener('resize', this.showGoToTopBtn)
    window.removeEventListener('scroll', this.showGoToTopBtn)
  }

  shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    const { left, show } = this.state
    const { windowSize } = this.props

    return nextState.show !== show || nextState.left !== left || windowSize !== nextProps.windowSize
  }

  getLeftOffset = () => {
    const contentContainerEl = document.getElementById('mainContainer')
    const leftOffset = contentContainerEl && contentContainerEl.getBoundingClientRect().left || 0

    return leftOffset
  }

  updateLeftOffset = () => {
    this.setState({
      left: this.getLeftOffset()
    })
  }

  showGoToTopBtn = () => {
    const { browser, windowSize } = this.props
    const logoEl = document.getElementById('tcLogo')
    const sidebarRootEl = document.getElementById('sidebarroot')
    const sidebarHeight = sidebarRootEl.getBoundingClientRect().height
    const logoHeight = logoEl && logoEl.getBoundingClientRect().height
    let show = (sidebarHeight + SIDEBAR_OFFSET + logoHeight) <= window.pageYOffset

    if (browser.mediaType === 'mobile') {
      show = (window.innerHeight * 2) <= window.pageYOffset
    }

    if (windowSize !== browser.mediaType) {
      show = false
    }

    this.setState({
      show: show
    })
  }

  scrollToTop = () => {
    window.scrollTo(0, 0)
  }

  smoothScrollToTop = () => {
    const step = window.pageYOffset / AMOUNT_ITERATIONS
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset

      if (pos > 0) {
        window.scrollTo(0, pos - step)
      } else {
        window.clearInterval(scrollToTop)
      }
    }, ITERATION_DURATION)
  }

  render() {
    const { browser } = this.props
    const { left, show } = this.state
    const style = browser.mediaType === 'mobile' ? { left: `${MOBILE_LEFT_OFFSET}px` } : { left: `${left}px` }
    const goToTop = (
      <button style={style} className={cn(s.goToTopBtn, { [s.show]: show })} onClick={this.smoothScrollToTop} />
    )

    return ReactDOM.createPortal(goToTop, this.el)
  }
}

const mapStateToProps = state => ({
  browser: state.browser
})
const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(GoToTopBtn)
