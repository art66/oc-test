import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { formatTime, getCurrentTimeInSeconds } from '../utils'
import { getMillisecondsToBarUpdate, getBarTime, getTickParams } from '../utils/barsHelper'
import Tooltip from './Tooltip'
import * as c from '../constants'
import s from '../styles/ProgressBar.cssmodule.scss'
import tooltipStyles from '../styles/Tooltip.cssmodule.scss'

class ProgressBar extends Component {
  getBarDescription() {
    const { bar, user } = this.props
    const { amount, max, timestampToUpdate, name } = bar

    if (amount > max) {
      if (timestampToUpdate && name !== c.ENERGY) {
        return (
          <p className={`${s['bar-descr']} ${s['bar-time-low']}`}>
            {formatTime(getMillisecondsToBarUpdate(bar, user))}
          </p>
        )
      }
      return <p className={s['bar-descr']}>OVER</p>
    } else if (amount < max) {
      return <p className={s['bar-descr']}>{formatTime(getMillisecondsToBarUpdate(bar, user))}</p>
    } else if (amount === max) {
      return <p className={s['bar-descr']}>FULL</p>
    }

    return null
  }

  getOneBarTimeInTooltip() {
    const { bar, user } = this.props
    const { amount, max } = bar

    if (amount < max) {
      return <span>({formatTime(getMillisecondsToBarUpdate(bar, user))})</span>
    } else if (this.isHappinessOver()) {
      return (
        <span>
          (<span className={tooltipStyles['bar-time-low']}>{formatTime(getMillisecondsToBarUpdate(bar, user))}</span>)
        </span>
      )
    }

    return null
  }

  getTooltipSubtitle() {
    const {
      bar: { name, amount, max, timestampToUpdate }
    } = this.props
    const currTime = getCurrentTimeInSeconds()
    const timeDiff = timestampToUpdate - currTime
    const time = timeDiff > 0 ? timeDiff : 0

    if (amount === max) {
      return <p className={s['bar-descr']}>You have full {name.toLowerCase()}</p>
    } else if (amount > max) {
      const overfullHappinessNotExpired = this.checkHappinessExpiration()

      if (name === c.HAPPY && overfullHappinessNotExpired) {
        return <p className={s['bar-descr']}>You have over full happiness</p>
      }
      if (timestampToUpdate && name === c.HAPPY) {
        return <p className={s['bar-descr']}>You have over full happiness which will expire soon</p>
      }
      return <p className={s['bar-descr']}>You have over full {name.toLowerCase()}</p>
    } else if (amount < max) {
      return (
        <p className={s['bar-descr']}>
          Full {name.toLowerCase()} in {formatTime(time * 1000, true, true)}
        </p>
      )
    }

    return null
  }

  getValuesOnMobile() {
    const {
      bar: { amount, max },
      windowWidth
    } = this.props

    if (
      ((max >= 10000 || amount >= 10000) && windowWidth < 460)
      || ((max >= 1000 || amount >= 1000) && windowWidth < 408)
      || ((max >= 100 || amount >= 100) && windowWidth < 354)
    ) {
      return <p className={s['bar-value']}>{amount}</p>
    }
    return (
      <p className={s['bar-value']}>
        {amount} / {max}
      </p>
    )
  }

  createTicksList() {
    return <ul className={s['tick-list']}>{this.renderTicks()}</ul>
  }

  createTooltip(blockId, position) {
    const { bar, user } = this.props
    const time = getBarTime(bar, user)

    return (
      <Tooltip position={position} arrow='center' parent={blockId}>
        {this.renderBarStatsDescription()}
        <b>
          {bar.name} increased by {time.waitTime} every {time.oneBarTime} minutes
        </b>
        {this.getTooltipSubtitle()}
      </Tooltip>
    )
  }

  checkHappinessExpiration = () => {
    const { bookIcon } = this.props
    const bookWithEffectUnexpiredOverfullHappiness = {
      title: 'Ignorance is Bliss',
      iconID: 'icon68'
    }

    return (
      bookIcon
      && bookIcon.iconID === bookWithEffectUnexpiredOverfullHappiness.iconID
      && bookIcon.bookTitle === bookWithEffectUnexpiredOverfullHappiness.title
    )
  }

  isHappinessOver() {
    const {
      windowSize,
      bar: { amount, max, name, timestampToUpdate }
    } = this.props
    const currTime = getCurrentTimeInSeconds()
    const timeDiff = timestampToUpdate - currTime
    const time = timeDiff > 0 ? timeDiff : 0

    return windowSize === 'mobile' && name === c.HAPPY && amount > max && time
  }

  renderTicks = () => {
    const { bar } = this.props
    const { tickCount, tickWidth } = getTickParams(bar)
    const ticks = [...new Array(tickCount - 1)].map((_, i) => {
      return i + 1
    })

    return ticks.map(tick => {
      return <li key={tick} style={{ width: `${tickWidth}%` }} />
    })
  }

  renderBarStatsDescription = () => {
    const {
      bar: { name, amount, max },
      windowSize
    } = this.props

    return windowSize === 'mobile' ? (
      <p>
        {name}: {amount} / {max} {this.getOneBarTimeInTooltip()}
      </p>
    ) : null
  }

  renderBar() {
    const { windowSize, svgIcon, bar, user } = this.props
    const { name, amount, max } = bar
    const oneBarTime = getBarTime(bar, user).oneBarTime * 60
    const timeLine = oneBarTime * 1000 - getMillisecondsToBarUpdate(bar, user)
    const timePercents = (timeLine / (oneBarTime * 1000)) * 100

    const barClass = classnames({
      [s['bar']]: true,
      [s[name.toLowerCase()]]: true,
      [s['bar-desktop']]: windowSize === 'desktop' || windowSize === 'tablet',
      [s['bar-mobile']]: windowSize === 'mobile',
      [s['bar-time-low']]: this.isHappinessOver()
    })

    const progressClass = classnames({
      [s['progress']]: true,
      [s['full']]: amount >= max || timePercents >= 100
    })

    const blockId = `bar${name}`

    if (windowSize === 'desktop' || windowSize === 'tablet') {
      return (
        // eslint-disable-next-line jsx-a11y/anchor-is-valid
        <a
          href='#'
          className={barClass}
          id={blockId}
          tabIndex={0}
          onClick={e => e.preventDefault()}
          onContextMenu={e => e.preventDefault()}
        >
          <div className={s['bar-stats']}>
            <p className={s['bar-name']}>{name}:</p>
            <p className={s['bar-value']}>
              {amount}/{max}
            </p>
            {this.getBarDescription()}
          </div>
          <div className={progressClass}>
            <div className={s['progress-line-timer']} style={{ width: `${timePercents}%` }} />
            <div className={s['progress-line']} style={{ width: `${Math.round((amount / max) * 100)}%` }} />
          </div>
          <div>{this.createTicksList()}</div>
          {this.createTooltip(blockId, 'right')}
        </a>
      )
    }
    return (
      // eslint-disable-next-line jsx-a11y/anchor-is-valid
      <a
        href='#'
        className={barClass}
        id={blockId}
        tabIndex={0}
        onClick={e => e.preventDefault()}
        onContextMenu={e => e.preventDefault()}
      >
        <div className={`${s['bar-stats']} ${s['bar-stats-flex']}`}>
          <i className={s['svg-icon-wrap']}>
            <SVGIconGenerator
              iconName={svgIcon.name}
              fill={svgIcon.fill}
              dimensions={svgIcon.dimensions}
              filter={svgIcon.filter}
            />
          </i>
          {this.getValuesOnMobile()}
        </div>

        <div className={progressClass}>
          <div className={s['progress-line']} style={{ width: `${Math.round((amount / max) * 100)}%` }} />
        </div>
        {this.createTooltip(blockId, 'bottom')}
      </a>
    )
  }

  render() {
    return this.renderBar()
  }
}

ProgressBar.propTypes = {
  svgIcon: PropTypes.object,
  windowSize: PropTypes.string,
  windowWidth: PropTypes.number,
  bar: PropTypes.object,
  user: PropTypes.object,
  bookIcon: PropTypes.object,
  getBarTime: PropTypes.func
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  windowWidth: state.browser.width,
  user: state.sidebar.user,
  bookIcon: state.sidebar.statusIcons && state.sidebar.statusIcons.icons && state.sidebar.statusIcons.icons.book
})

export default connect(mapStateToProps, {})(ProgressBar)
