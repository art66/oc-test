import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Point from './Point'

import s from '../styles/Points.cssmodule.scss'

class Points extends Component {
  render() {
    const {
      myForwardedRef,
      windowSize,
      fullValue,
      user: { money, level, points, refills, merits }
    } = this.props

    const pointsClass = windowSize === 'mobile' ? s['points-mobile'] : s['points']

    if (windowSize === 'desktop' || windowSize === 'tablet') {
      return (
        <div ref={myForwardedRef} className={pointsClass}>
          <Point data={money} name="Money" />
          <Point data={level} name="Level" />
          <Point data={points} name="Points" />
          {refills && refills.value > 0 && <Point data={refills} name="Refills" />}
          {merits.value ? <Point data={merits} name="Merits" /> : ''}
        </div>
      )
    } else {
      return (
        <div ref={myForwardedRef} className={pointsClass}>
          <Point data={money} name="Money" fullValue={fullValue} />
          <Point data={points} name="Points" fullValue={fullValue} />
        </div>
      )
    }
  }
}

Points.propTypes = {
  myForwardedRef: PropTypes.object,
  fullValue: PropTypes.bool,
  windowSize: PropTypes.string,
  user: PropTypes.object
}

Points.defaultProps = {
  myForwardedRef: {},
  fullValue: false,
  windowSize: undefined,
  user: {}
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  user: state.sidebar.user
})

const ConnectedPoints = connect(mapStateToProps)(Points)

export default React.forwardRef((props, ref) => <ConnectedPoints {...props} myForwardedRef={ref} />)
