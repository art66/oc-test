import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ScrollArea from 'react-scrollbar'
import s from '../styles/List.cssmodule.scss'
import ListItem from './ListItem'
const SCROLL_ITEMS_QUANTITY = 9

class List extends Component {
  render() {
    const { lists, listName } = this.props

    return (
      <ScrollArea
        className={s['scroll-area']}
        horizontal={false}
        stopScrollPropagation={lists.length > SCROLL_ITEMS_QUANTITY}
        smoothScrolling={false}
      >
        <ul className={s['list']}>
          {lists.map((item, index) => {
            return <ListItem key={index} item={item} listName={listName} />
          })}
        </ul>
      </ScrollArea>
    )
  }
}

List.propTypes = {
  lists: PropTypes.array,
  listName: PropTypes.string
}

export default List
