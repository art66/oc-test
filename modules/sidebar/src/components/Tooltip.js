import { isMouseEnabledDevice } from '@torn/shared/utils/isMouseEnabledDevice'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ToolTip from 'react-portal-tooltip'
import { connect } from 'react-redux'
import style from '../styles/Tooltip.cssmodule.scss'

class Tooltip extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isTooltipActive: false
    }
    this.tooltipTimeout = props.windowSize === 'mobile' ? 500 : 0
    if (isMouseEnabledDevice()) {
      this.showEvent = 'mouseenter'
      this.hideEvent = 'mouseleave'
    } else {
      this.showEvent = 'focus'
      this.hideEvent = 'blur'
    }
  }

  componentDidMount() {
    this.parent = document.getElementById(this.props.parent)

    this.parent?.addEventListener(this.showEvent, this.showTooltip)
    this.parent?.addEventListener(this.hideEvent, this.hideTooltip)
  }

  componentWillUnmount() {
    this.parent?.removeEventListener(this.showEvent, this.showTooltip)
    this.parent?.removeEventListener(this.hideEvent, this.hideTooltip)
  }

  showTooltip = () => {
    this.setState({ isTooltipActive: true })
  }

  hideTooltip = () => {
    this.setState({ isTooltipActive: false })
  }

  getStyles = () => {
    const tooltipShadow = this.props.darkModeEnabled ? '0px 0px 2px #000000a6' : '0px 0px 8px #0000004d'
    const tooltipBg = this.props.darkModeEnabled ? '#444' : '#f2f2f2'
    const arrowBorder = this.props.darkModeEnabled ? '#00000033' : '#dadada'

    return {
      style: {
        background: tooltipBg,
        padding: 8,
        boxShadow: tooltipShadow
      },
      arrowStyle: {
        color: tooltipBg,
        borderColor: arrowBorder,
        transition: 'none'
      }
    }
  }

  render() {
    return (
      <ToolTip
        active={this.state.isTooltipActive}
        position={this.props.position}
        arrow={this.props.arrow}
        parent={`#${this.props.parent}`}
        style={this.getStyles()}
        tooltipTimeout={this.tooltipTimeout}
      >
        <div className={style.tooltip}>{this.props.children}</div>
      </ToolTip>
    )
  }
}

Tooltip.propTypes = {
  children: PropTypes.node,
  position: PropTypes.string,
  arrow: PropTypes.string,
  parent: PropTypes.string,
  windowSize: PropTypes.string,
  mediaType: PropTypes.string,
  darkModeEnabled: PropTypes.bool
}

const mapStateToProps = state => ({
  windowSize: state.sidebar.windowSize,
  mediaType: state.browser.mediaType,
  darkModeEnabled: state.sidebar.darkModeEnabled
})

export default connect(mapStateToProps)(Tooltip)
