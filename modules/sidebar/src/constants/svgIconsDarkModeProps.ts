export const DISABLE_FILTER = {
  active: false
}

export const DEFAULT_DARK_MODE_FILTER = {
  ID: 'svg_sidebar_dark_mode',
  active: true,
  shadow: {
    active: true,
    x: 0,
    y: 1,
    blur: 2,
    color: '#00000073'
  }
}
