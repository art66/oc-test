import { checkDarkMode } from '../utils'
import { DEFAULT_DARK_MODE_FILTER } from './svgIconsDarkModeProps'

const DEFAULT_ACCOUNT_ICONS_FILTER = {
  ID: 'sidebar__accountLinks_filter',
  active: true,
  shadow: {
    active: true,
    x: 0,
    y: 1,
    blur: 0.1,
    color: '#fff'
  }
}

export const getAccountSVGIconsColors = () => {
  const isDarkModeEnabled = checkDarkMode()
  const DARK_MODE_GRADIENT_ID = 'sidebar__accountLinks_svg_gradient_regular_dark_mode'
  const LIGHT_MODE_GRADIENT_ID = 'sidebar__accountLinks_svg_gradient_regular'
  const DARK_MODE_GRADIENT = [
    { step: '0', color: '#666' },
    { step: '1', color: '#888' }
  ]
  const LIGHT_MODE_GRADIENT = [
    { step: '0', color: '#999' },
    { step: '1', color: '#ccc' }
  ]
  const GRADIENT_ID = isDarkModeEnabled ? DARK_MODE_GRADIENT_ID : LIGHT_MODE_GRADIENT_ID
  const GRADIENT_SCHEME = isDarkModeEnabled ? DARK_MODE_GRADIENT : LIGHT_MODE_GRADIENT
  const FILTER = isDarkModeEnabled ? DEFAULT_DARK_MODE_FILTER : DEFAULT_ACCOUNT_ICONS_FILTER

  const ACCOUNT_SVG_ICONS_COLORS = {
    regular: {
      fill: {
        color: `url(#${GRADIENT_ID})`,
        strokeWidth: 0
      },
      filter: FILTER,
      gradient: {
        ID: GRADIENT_ID,
        active: true,
        transform: 'rotate(90)',
        scheme: GRADIENT_SCHEME
      }
    },
    active: {
      fill: {
        color: 'url(#sidebar__accountLinks_svg_gradient_active)',
        strokeWidth: 0
      },
      filter: {
        ID: 'sidebar__accountLinks_filter_active',
        active: true,
        shadow: {
          active: true,
          x: 0,
          y: 1,
          blur: 3,
          color: '#a3d900de'
        }
      },
      gradient: {
        ID: 'sidebar__accountLinks_svg_gradient_active',
        active: true,
        transform: 'rotate(90)',
        scheme: [
          { step: '0', color: '#799427' },
          { step: '1', color: '#a3c248' }
        ]
      }
    }
  }

  return ACCOUNT_SVG_ICONS_COLORS
}

export const SVG_ICONS_PROPS_DEFAULTS = {
  filter: {
    active: false,
    ID: null
  },
  dimensions: {
    width: 12,
    height: 12,
    viewbox: '4.5 5.5 11 11'
  }
}

export const SVG_ICONS_PROPS_HOLDER = {
  Energy: {
    name: 'Energy',
    fill: { color: '#588e22', strokeWidth: 0 },
    ...SVG_ICONS_PROPS_DEFAULTS,
    dimensions: {
      width: 11,
      height: 11,
      viewbox: '4.5 5 11 11'
    }
  },
  Nerve: {
    name: 'Nerve',
    fill: { color: '#bc4d2e', strokeWidth: 0 },
    ...SVG_ICONS_PROPS_DEFAULTS,
    dimensions: {
      ...SVG_ICONS_PROPS_DEFAULTS.dimensions,
      viewbox: '4.5 5.5 11 11'
    }
  },
  Happy: {
    name: 'Happy',
    fill: { color: '#9f9126', strokeWidth: 0 },
    ...SVG_ICONS_PROPS_DEFAULTS,
    dimensions: {
      ...SVG_ICONS_PROPS_DEFAULTS.dimensions,
      viewbox: '4 4.7 11 11'
    }
  },
  Life: {
    name: 'Life',
    fill: { color: '#515dd3', strokeWidth: 0 },
    ...SVG_ICONS_PROPS_DEFAULTS,
    dimensions: {
      ...SVG_ICONS_PROPS_DEFAULTS.dimensions,
      viewbox: '5.5 5 11 11'
    }
  },
  Chain: {
    name: 'Chain',
    fill: { color: '#747474', strokeWidth: 0 },
    ...SVG_ICONS_PROPS_DEFAULTS,
    dimensions: {
      ...SVG_ICONS_PROPS_DEFAULTS.dimensions,
      viewbox: '5 4.5 11 11'
    }
  }
}
