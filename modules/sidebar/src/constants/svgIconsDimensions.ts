export const ACCOUNT_SVG_ICONS = {
  messagesCon: {
    name: 'Mailbox',
    dimensions: {
      width: 34,
      height: 34,
      viewbox: '-7 -9 34 34'
    }
  },
  eventsCon: {
    name: 'Events',
    dimensions: {
      width: 34,
      height: 34,
      viewbox: '-6 -6 30 30'
    }
  },
  awardsCon: {
    name: 'AwardsMedal',
    dimensions: {
      width: 34,
      height: 34,
      viewbox: '2 0 34 34'
    }
  }
}

export const svgIconsDimensions = (name: string) => {
  const dimensionsByName = {
    default: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '0 0 18 18'
      }
    },
    Admin: {
      dimensions: {
        width: 18,
        height: 13.5,
        viewbox: '0 1 18 13.5'
      }
    },
    Awards: {
      dimensions: {
        width: 16,
        height: 15.67,
        viewbox: '0 0 16 15.67'
      }
    },
    AwardsMedal: {
      dimensions: {
        width: 14,
        height: 16,
        viewbox: '11 9 14 16'
      }
    },
    Casino: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '0 1 16 16'
      }
    },
    ChristmasTown: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '0 1 16 16'
      }
    },
    ChristmasTownSantaHat: {
      dimensions: {
        width: 20,
        height: 15,
        viewbox: '2 1 16 15'
      }
    },
    City: {
      dimensions: {
        width: 12,
        height: 18,
        viewbox: '.3 1 10 16'
      }
    },
    Competitions: {
      dimensions: {
        width: 20,
        height: 14.27,
        viewbox: '-.5 1 21 14'
      }
    },
    Crimes: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '-.5 0 19 19'
      }
    },
    DogTags: {
      dimensions: {
        width: 15.19,
        height: 15.19,
        viewbox: '0 .5 16 16'
      }
    },
    EwasterEgg: {
      dimensions: {
        width: 13,
        height: 18,
        viewbox: '0 0 13 18'
      }
    },
    Education: {
      dimensions: {
        width: 18,
        height: 14.31,
        viewbox: '0 1 16 12.31'
      }
    },
    Elimination: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '0 1 18 18'
      }
    },
    Enemies: {
      dimensions: {
        width: 20,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    Events: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '-1 0 18 18'
      }
    },
    Faction: {
      dimensions: {
        width: 12.47,
        height: 17,
        viewbox: '0 1 11.47 16'
      }
    },
    Forums: {
      dimensions: {
        width: 16,
        height: 14.67,
        viewbox: '0 1 16 14.67'
      }
    },
    Friends: {
      dimensions: {
        width: 21,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    Gym: {
      dimensions: {
        width: 20,
        height: 10,
        viewbox: '7 13 20 10'
      }
    },
    HallOfFame: {
      dimensions: {
        width: 16,
        height: 14.67,
        viewbox: '0 1 16 14.67'
      }
    },
    HalloweenWitch: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '0 1 18 18'
      }
    },
    HalloweenGrave: {
      dimensions: {
        width: 20,
        height: 18,
        viewbox: '3 1 16 15'
      }
    },
    HalloweenPumpkin: {
      dimensions: {
        width: 16,
        height: 15,
        viewbox: '0 1 16 15'
      }
    },
    Home: {
      dimensions: {
        width: 17,
        height: 14,
        viewbox: '1 1 15 14'
      }
    },
    Hospital: {
      dimensions: {
        width: 16,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    Items: {
      dimensions: {
        width: 14.67,
        height: 16,
        viewbox: '0 1 14.67 16'
      }
    },
    Jail: {
      dimensions: {
        width: 17,
        height: 17,
        viewbox: '0 1 17 17'
      }
    },
    Job: {
      dimensions: {
        width: 16,
        height: 13.33,
        viewbox: '0 1 16 13.33'
      }
    },
    Mailbox: {
      dimensions: {
        width: 18,
        height: 13,
        viewbox: '0 1 18 13'
      }
    },
    Missions: {
      dimensions: {
        width: 18,
        height: 16.5,
        viewbox: '0 .5 16 14.5'
      }
    },
    MrMissTorn: {
      dimensions: {
        width: 17,
        height: 14,
        viewbox: '1.5 0 15 15'
      }
    },
    Newspaper: {
      dimensions: {
        width: 18,
        height: 15,
        viewbox: '0 1 18 15'
      }
    },
    Property: {
      dimensions: {
        width: 16,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    RecruitCitizens: {
      dimensions: {
        width: 15,
        height: 20,
        viewbox: '0 1 14 19'
      }
    },
    Rules: {
      dimensions: {
        width: 15,
        height: 18,
        viewbox: '0 1 15 18'
      }
    },
    Staff: {
      dimensions: {
        width: 16,
        height: 16,
        viewbox: '0 1 16 16'
      }
    },
    FollowLinks: {
      dimensions: {
        width: 34,
        height: 34,
        viewbox: '0 0 34 34'
      }
    },
    Star: {
      dimensions: {
        width: 34,
        height: 34,
        viewbox: '-2.3 -2.1 9 9'
      }
    },
    Ticket: {
      dimensions: {
        width: 18,
        height: 18,
        viewbox: '8 8 18 18'
      }
    },
    MrMsTorn: {
      dimensions: {
        width: 17,
        height: 14,
        viewbox: '0 0 17 14'
      }
    },
    EasterEgg: {
      dimensions: {
        width: 13,
        height: 17,
        viewbox: '0 2 13 13'
      }
    },
    CansEvent: {
      dimensions: {
        width: 34,
        height: 34,
        viewbox: '-9 -9 34 34'
      }
    }
  }

  return dimensionsByName[name] ? dimensionsByName[name] : dimensionsByName.default
}
