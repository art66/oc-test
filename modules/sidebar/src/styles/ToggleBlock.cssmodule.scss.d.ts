// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'activated': string;
  'desktop': string;
  'globalSvgShadow': string;
  'header': string;
  'header-arrow': string;
  'hidden': string;
  'in-hospital': string;
  'in-jail': string;
  'tablet': string;
  'toggle-block': string;
  'toggle-content': string;
  'toggled': string;
}
export const cssExports: CssExports;
export default cssExports;
