interface ISidebar {
  user: {
    userID: number
  }
}

interface ISetSessionUserData {
  sidebar: ISidebar
}

const SIDEBAR_SESSION_KEY = 'sidebarData'

class SessionDataHandler {
  _resetSessionUserData = () => {
    const isAuth = location.href.indexOf('authenticate.php') !== -1

    if (!isAuth || !sessionStorage) {
      return
    }

    try {
      Object.keys(sessionStorage).forEach(data => {
        if (data.startsWith && data.startsWith(SIDEBAR_SESSION_KEY)) {
          sessionStorage.removeItem(data)
        }
      })

    } catch (error) {
      console.error(error)
    }
  }

  _setSessionUserData = (sidebarData: ISidebar) => {
    if (!sessionStorage || !sidebarData || !sidebarData.user) {
      return
    }

    try {
      const { user } = sidebarData || {}
      const { userID } = user || {}

      sessionStorage.setItem(`${SIDEBAR_SESSION_KEY}${userID}`, JSON.stringify(sidebarData))
    } catch (error) {
      console && console.error && console.error(error)
    }
  }

  run = (newStoreData: ISetSessionUserData) => {
    const { sidebar } = newStoreData

    this._resetSessionUserData()
    this._setSessionUserData(sidebar)
  }
}

const { run: sessionDataHandler } = new SessionDataHandler()

export default sessionDataHandler
