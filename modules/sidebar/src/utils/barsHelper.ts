import { getCurrentTimeInSeconds } from './index'
import * as c from '../constants'

export const getBarTime = (bar, user) => {
  const time: { waitTime?: number; oneBarTime?: number } = {}
  const { name, step } = bar

  if (name === c.ENERGY) {
    time.waitTime = step
    time.oneBarTime = user.donator ? 10 : 15
  } else if (name === c.NERVE) {
    time.waitTime = step
    time.oneBarTime = 5
  } else if (name === c.HAPPY) {
    time.waitTime = step
    time.oneBarTime = 15
  } else if (name === c.LIFE) {
    time.waitTime = step
    time.oneBarTime = 5
  }

  return time
}

export const getMillisecondsToBarUpdate = (bar, user) => {
  const { timestampToUpdate } = bar
  const oneBarTime = getBarTime(bar, user).oneBarTime * 60
  const currTime = getCurrentTimeInSeconds()
  const timeDiff = timestampToUpdate - currTime
  const time = timeDiff > 0 ? timeDiff : 0

  return (time % oneBarTime) * 1000
}

export const getTickParams = (bar) => {
  const { name, max } = bar

  if (name === c.ENERGY) {
    const oneTick = 25
    const tickCount = Math.floor(max / oneTick)

    return {
      tickCount,
      tickWidth: 100 / tickCount
    }
  } else if (name === c.NERVE) {
    const oneTick = 5

    return {
      tickCount: Math.ceil(max / oneTick),
      tickWidth: (oneTick / max) * 100
    }
  } else if (name === c.HAPPY) {
    const tickCount = 10

    return {
      tickCount,
      tickWidth: 100 / tickCount
    }
  } else if (name === c.LIFE) {
    const oneTick = 5
    const tickCount = Math.ceil(100 / oneTick)

    return {
      tickCount: Math.ceil(100 / oneTick),
      tickWidth: 100 / tickCount
    }
  }
}
