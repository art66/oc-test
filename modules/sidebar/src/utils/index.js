import { MAIN_URL } from '../constants'
import getEndTime from './getEndTime'

// eslint-disable-next-line no-redeclare
/* global fetch, addRFC, getCurrentTimestamp */

export const plural = val => {
  return val === 1 ? '' : 's'
}

// eslint-disable-next-line max-statements
export function formatTime(time, withHours, hoursMore24) {
  let days = Math.floor(time / (1000 * 60 * 60 * 24))

  days = addLeadZero(days)

  let hours

  if (hoursMore24) {
    hours = Math.floor(time / (1000 * 60 * 60))
  } else {
    hours = Math.floor((time / (1000 * 60 * 60)) % 24)
  }

  hours = addLeadZero(hours)
  let minutes = Math.floor((time / (1000 * 60)) % 60)

  minutes = addLeadZero(minutes)
  let seconds = Math.floor((time / 1000) % 60)

  seconds = addLeadZero(seconds)

  if (withHours) {
    let result = ''

    result += days > 0 && !hoursMore24 ? `${days}:` : ''
    result += `${hours}:`
    result += `${minutes}:${seconds}`
    return result
  }
  return `${minutes}:${seconds}`
}
// FIXME: Should enable eslint and refactor

export function formatTimeLong(time) {
  const days = Math.floor(time / (3600 * 24))
  const hours = Math.floor((time / 3600) % 24)
  const minutes = Math.floor((time / 60) % 60)
  const seconds = Math.floor(time % 60)
  let result = ''

  result += days > 0 ? `${days} day${plural(+days)}, ` : ''
  result += hours > 0 ? `${hours} hour${plural(+hours)}, ` : ''
  result += minutes > 0 ? `${minutes} minute${plural(+minutes)} and ` : ''
  result += `${seconds} second${plural(+seconds)}`

  return result
}

function addLeadZero(time) {
  return time < 10 ? `0${time}` : time
}

export function fetchUrl(url, data) {
  return fetch(addRFC(MAIN_URL + url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      let error
      let json

      try {
        json = JSON.parse(text)
        if (json && json.error) {
          // eslint-disable-next-line prefer-destructuring
          error = json.error
        }
      } catch (e) {
        error = `Server responded with: ${text}`
      }

      if (error) {
        throw new Error(error)
      } else {
        return json
      }
    })
  })
}

export function numberFormat(number, decimals, decPoint, separator) {
  number = (`${number}`).replace(/[^0-9+\-Ee.]/g, '')
  const n = !isFinite(+number) ? 0 : +number
  const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  const sep = typeof separator === 'undefined' ? ',' : separator
  const dec = typeof decPoint === 'undefined' ? '.' : decPoint
  let s = ''
  const toFixedFix = (n, prec) => {
    const k = Math.pow(10, prec)

    return `${(Math.round(n * k) / k).toFixed(prec)}`
  }

  s = (prec ? toFixedFix(n, prec) : `${Math.round(n)}`).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}

export function getShortNumber(num) {
  if (Math.abs(num) < 1000) {
    return num
  }

  const units = ['k', 'M', 'B', 'T', 'P', 'E', 'Z', 'Y']

  const getCurrentDecimal = () => {
    let decimal

    for (let i = units.length - 1; i >= 0; i--) {
      decimal = Math.pow(1000, i + 1)
      if (num <= -decimal || num >= decimal) {
        return {
          decimal,
          prefix: units[i]
        }
      }
    }
  }
  const currentDecimal = getCurrentDecimal().decimal
  const currentPrefix = getCurrentDecimal().prefix
  let value = Math.round((num / currentDecimal) * 10) / 10

  value = value === 1000 ? 999.9 : value

  return value + currentPrefix
}

export function getCookie(name) {
  const value = `; ${document.cookie}`
  const parts = value.split(`; ${name}=`)

  if (parts.length === 2) {
    return parts
      .pop()
      .split(';')
      .shift()
  }
}

export function getOrdinalSuffix(i) {
  const j = i % 10
  const k = i % 100

  if (j === 1 && k !== 11) {
    return 'st'
  }
  if (j === 2 && k !== 12) {
    return 'nd'
  }
  if (j === 3 && k !== 13) {
    return 'rd'
  }
  return 'th'
}

export function changeJailHospRegularStyles(nextState) {
  const regular = 'colors/regular'
  const hospital = 'colors/hospital'
  const jail = 'colors/jail'

  const links = document.getElementsByTagName('link')

  Array.prototype.forEach.call(links, link => {
    const currentHref = link.getAttribute('href')
    const isRegular = currentHref && currentHref.indexOf(regular) !== -1
    const isHospital = currentHref && currentHref.indexOf(hospital) !== -1
    const isJail = currentHref && currentHref.indexOf(jail) !== -1

    let newHref

    if (isRegular && nextState !== 'regular') {
      newHref = currentHref.replace(regular, `colors/${nextState}`)
      document.body.setAttribute('data-layout', nextState)
      link.setAttribute('href', newHref)
    } else if (isHospital && nextState !== 'hospital') {
      newHref = currentHref.replace(hospital, `colors/${nextState}`)
      document.body.setAttribute('data-layout', nextState)
      link.setAttribute('href', newHref)
    } else if (isJail && nextState !== 'jail') {
      newHref = currentHref.replace(jail, `colors/${nextState}`)
      document.body.setAttribute('data-layout', nextState)
      link.setAttribute('href', newHref)
    }
  })
}

export const createEventForEnergyRecovering = () => {
  const syncRecoveringEnergyWithSidebarEvent = document.createEvent('CustomEvent')

  syncRecoveringEnergyWithSidebarEvent.initCustomEvent('syncRecoveringEnergyWithSidebar', false, false, {})
  return syncRecoveringEnergyWithSidebarEvent
}

export const formatAreasCounter = (number) => {
  const min = 1000
  const max = 9000
  const maxLabel = '9k+'
  const strNumber = `${number}`
  const regexp = /^[1-9](0){3}$/
  const isEqual = strNumber.match(regexp)

  if (number < min) {
    return number
  }
  if (number > max) {
    return maxLabel
  }

  const formattedThousands = `${strNumber.charAt(0)}k`

  return isEqual ? formattedThousands : `${formattedThousands}+`
}

export const getCurrentTimeInSeconds = () => {
  return Math.round(getCurrentTimestamp() / 1000)
}

export const checkDarkMode = () => {
  const DMCookieValue = getCookie('darkModeEnabled')

  return DMCookieValue === 'true'
}

export { default as getEndTime } from './getEndTime'

export default {
  getEndTime
}
