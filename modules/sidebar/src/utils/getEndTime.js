export default (secondsToEnd = 0, fromTime = Date.now()) => fromTime + secondsToEnd * 1000
