import { connect } from 'react-redux'
import {
  subscribeForSidebarChanges,
  loadSidebarData,
  fetchSidebarData,
  setSidebarMode,
  resetSidebar,
  fetchInformation,
  calcResponsive,
  syncSidebarDataOnTabFocus,
  setDarkModeState
} from '../modules'

import Sidebar from '../components/Sidebar'

const mapStateToProps = state => ({
  sidebar: state.sidebar
})

const mapDispatchToProps = {
  subscribeForSidebarChanges,
  loadSidebarData,
  fetchSidebarData,
  setSidebarMode,
  resetSidebar,
  fetchInformation,
  calcResponsive,
  syncSidebarDataOnTabFocus,
  setDarkModeState
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
