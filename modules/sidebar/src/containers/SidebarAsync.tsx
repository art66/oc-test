import React from 'react'
import Loadable from 'react-loadable'

import reducer from '../modules/index'

import Skeleton from '../components/Skeleton'

type TAsyncComponent = any
type TConfig = {
  rootStore: object
  injector: (store: object, options: object) => void
}

// this is async version of common Sidebar App. You can use this entry-point for asynchronous
// Sidebar injection inside some particular react-based app with easy.
const SidebarBundle = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const Sidebar = await import(/* webpackChunkName: "sidebar" */ './Sidebar')

    return Sidebar
  },
  render(asyncComponent: TAsyncComponent, { rootStore, injector }: TConfig) {
    const { default: Sidebar } = asyncComponent

    injector(rootStore, { reducer, key: 'sidebar' })

    return <Sidebar />
  },
  loading: () => <Skeleton />
})

export default SidebarBundle
