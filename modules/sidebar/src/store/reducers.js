import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import sidebar from '../modules'

export const makeRootReducer = asyncReducers => {
  return combineReducers({
    ...asyncReducers,
    sidebar,
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    })
  })
}

export default makeRootReducer
