import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'

import reduxLogger from '../modules/middleware'
import makeRootReducer from './reducers'
import sessionDataHandler from '../utils/sessionDataHandler'
import { getCookie } from '../utils'

export default (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================

  // const updateSidebarMiddleWare = store => next => action => {
  //   const sidebarLS = localStorage.getItem('sidebarData')
  //   if (sidebarLS && sidebarLS.length) {
  //     console.log('midl ', store.getState().sidebar);
  //     // localStorage.setItem('sidebarData', JSON.stringify(store.getState().sidebar))
  //   }
  //   next(action)
  // }

  const middleware = [thunk]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  if (__DEV__) {
    if (getCookie('uid') !== '2072301') {
      middleware.push(reduxLogger)
    }

    const { devToolsExtension } = window

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(
      responsiveStoreEnhancer,
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }

  // We need to make Sidebar SPA-like for sure, it mean prevent any eye-visible refreshes on the page's reload phase.
  // Basically, the browser's SessionStorage help us deal with it. So Sidebar view model load significantly fast without
  // initial fetch response awaiting. That's why here we shall subscribe on every store update to be ready for load
  // the last actual sidebar data once page will be refreshed.
  store.subscribe(() => sessionDataHandler(store.getState()))

  return store
}
