import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'

import createStore from './store/createStore'
import AppContainer from './layout/index'

const MOUNT_NODE = document.getElementById('sidebarroot')

// ========================================================
// Store Instantiation
// ========================================================
// @ts-ignore
const initialState = window.___INITIAL_STATE__
const store = createStore(initialState)

// ========================================================
// Render Setup
// ========================================================
const render = () => {
  ReactDOM.render(<AppContainer store={store} />, MOUNT_NODE)
}

const renderError = error => {
  ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
}

// This code is excluded from production bundle
if (__DEV__) {
  // ========================================================
  // DEVELOPMENT STAGE! HOT MODULE REPLACE ACTIVATION!
  // ========================================================
  const devRender = () => {
    if (module.hot) {
      module.hot.accept('./layout/index', render)
    }

    render()
  }

  // Wrap render in try/catch
  try {
    import('@torn/header')
    devRender()
  } catch (error) {
    console.error(error)
    renderError(error)
  }
} else {
  // ========================================================
  // PRODUCTION GO!
  // ========================================================
  render()
}
