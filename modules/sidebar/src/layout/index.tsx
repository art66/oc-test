import React from 'react'
import { Provider } from 'react-redux'
import Sidebar from '../containers/Sidebar'
import isValue from '@torn/shared/utils/isValue'

import createStore from '../store/createStore'
import { IProps } from './types'

export class AppContainer extends React.Component<IProps> {
  render() {
    const { store, isSPA = false } = this.props

    return (
      <Provider store={!isValue(isSPA) ? store : createStore()}>
        <Sidebar />
      </Provider>
    )
  }
}

export default AppContainer
