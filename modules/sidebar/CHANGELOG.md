# Sidebar - Torn


## 2.8.3
 * Prevented SVG flashing by synchronously imported icons.

## 2.8.3
 * Fixed broken Context Menu hiding functionality on mobile and its buttons click.

## 2.8.2
 * Fixed broken SessionStore update on some particular WS responses.

## 2.8.1
 * Improved debounce unsubscribing logic to prevent memory leak while running.

## 2.8.0
 * Significantly refactored Sidebar App.

## 2.7.0
 * Improved Skeletons layout.
 * Improved SidebarBlock Component in favor of Pure.

## 2.6.2
 * Fixed broken react refs in Points ans Status Components.

## 2.6.1
 * Improved core controller current Sidebar configuration in favor of easy SPA-like include in other apps.
 * Fixed unfired middleware actions on DOMContentLoaded phase.

## 2.5.0
 * Added Skeleton for Async load.

## 2.4.0
 * Cut unused packages from package.json.
 * Create right Async abstraction model of Sidebar injection inside other react-based apps.
 * Refactoring of several components has been made.

## 2.3.1
 * Added ability to inject sidebar "on fly" inside any of the SPA apps.

## 2.3.0
 * Added pure SVG icons into StatusIcons section.
 * Fixed HMR.

## 2.2.0
 * Added skeleton remove logic.

## 2.1.0
 * Replaced legacy png icons stats by svg.

## 2.0.0
 * Cut and replaced deprecated methods: componentWillMount, componentWillReceiveProps.

## 1.11.0
 * Update sidebar SVG icons configs!
 * Minor layout improvements.

# 1.10.0
 * Improved SVG AccountLinks icons filters/gradients.

# 1.9.0
 * Updated SVG AccountLinks icons.

# 1.8.0
 * Created favorite SVG stars in mobile layout.

# 1.7.0
 * 'Fixed svgIconsDimensions constants.

# 1.6.0
 * Fixed Jobs icon dimensions.
 * Fixed rows height in tablet layout.

# 1.5.0
 * Fully added SVGIcons styles layout in mobile/desktop: (reg/jail/hosp) modes.
 * Fixed contextMenu layout in jailed/hosp modes.

# 1.4.0
 * Add new SVGIcons to stats wrap.

# 1.3.2
 * Rename AreaSVGIcon Component.

# 1.3.1
 * Added Arrow Icons to lists toggles.
 * Fixed viewBox for AccountLinks icons.

# 1.2.1
 * Added PopUp icons.
 * Minor code refactoring.

# 1.1.1
 * Added SVG icons for AccountLinks Component.
 * Minor refactor & fixes.

# 1.0.0
 * Refactored SVGIcon implementation by moving its configuration inside separate Class.

# 0.0.1
 * Replaced PNG icons by pure SVG-ones.
 * Cutted the all legacy eslint packages from package.json.
