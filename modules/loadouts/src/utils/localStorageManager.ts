import LocalStorageManager from '@torn/shared/utils/localStorageManager'

const localStorageManager = new LocalStorageManager()

export default localStorageManager
