export type TSetFirstRef = (ref: Element) => any
export type TsetDeactivatorRef = (ref: Element) => any

export type TSetRef = (ref: Element) => any

export type TOnArrowNavigationItemFocus = (event: React.FocusEvent<HTMLElement>) => any
