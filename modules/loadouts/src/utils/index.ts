import fromPairs from 'lodash.frompairs'

export const objectMap = (obj, fn) =>
  fromPairs(
    Object.entries(obj).map(([k, v], i) => {
      const itemSettings = fn(v, k, i)

      return itemSettings
    })
  )

export const objectAutoMap = (obj, fn) => fromPairs(Object.entries(obj).map(([k, v], i) => [k, fn(v, k, i)]))

export const objectForEach = (obj, fn) => Object.entries(obj).forEach(([k, v], i) => fn(v, k, i))

export const objectFilter = (obj, predicate) => fromPairs(Object.entries(obj).filter(predicate))

export const objectFind = (obj, fn) => {
  const result = Object.entries(obj).find(([k, v], i) => fn(v, k, i))

  return result ? result[1] : null
}

export const resolvePriority = (firstValue, secondValue) => {
  if (firstValue < secondValue) {
    return 1
  } else if (firstValue > secondValue) {
    return -1
  }

  return 0
}
