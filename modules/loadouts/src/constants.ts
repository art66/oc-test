import tooltipStyles from './styles/tooltip.cssmodule.scss'
import tooltipTopStyles from './styles/tooltipTop.cssmodule.scss'
import tooltipBottomStyles from './styles/tooltipBottom.cssmodule.scss'
import tooltipCurrentSlotItem from './styles/tooltipItem.cssmodule.scss'

export const MOBILE_TYPE = 'mobile'
export const TABLET_TYPE = 'tablet'
export const DESKTOP_TYPE = 'desktop'

export const PRIMARY_ITEM_TYPE = 'Primary'
export const SECONDARY_ITEM_TYPE = 'Secondary'
export const MELEE_ITEM_TYPE = 'Melee'
export const TEMPORARY_ITEM_TYPE = 'Temporary'
export const DEFENSIVE_ITEM_TYPE = 'Defensive'
export const CLOTHING_ITEM_TYPE = 'Clothing'

export const AMMO_TYPE_STANDARD = 1
export const AMMO_TYPE_HOLLOW_POINT = 2
export const AMMO_TYPE_TRACER = 4
export const AMMO_TYPE_PIERCING = 3
export const AMMO_TYPE_INCENDIARY = 5

export const AMMO_TYPE_TITLES = {
  [AMMO_TYPE_STANDARD]: 'Standard',
  [AMMO_TYPE_HOLLOW_POINT]: 'Hollow point',
  [AMMO_TYPE_TRACER]: 'Tracer',
  [AMMO_TYPE_PIERCING]: 'Piercing',
  [AMMO_TYPE_INCENDIARY]: 'Incendiary'
}

export const TOOLTIP_CONFIG = {
  showTooltip: true,
  addArrow: true,
  position: { x: 'center', y: 'bottom' },
  manualCoodsFix: { fixX: 8, fixY: 0 },
  overrideStyles: { ...tooltipStyles }
}

export const TOP_TOOLTIP_CONFIG = {
  ...TOOLTIP_CONFIG,
  position: { x: 'center', y: 'top' },
  manualCoodsFix: { ...TOOLTIP_CONFIG.manualCoodsFix, fixX: -10, fixY: 0 },
  overrideStyles: { ...tooltipBottomStyles }
}

export const ITEM_CONFIG = {
  ...TOP_TOOLTIP_CONFIG,
  manualCoodsFix: { ...TOP_TOOLTIP_CONFIG.manualCoodsFix, fixX: -4, fixY: 0 },
  overrideStyles: { ...tooltipCurrentSlotItem }
}

export const EXP_BAR_TOOLTIP_CONFIG = {
  ...TOP_TOOLTIP_CONFIG,
  manualCoodsFix: { ...TOP_TOOLTIP_CONFIG.manualCoodsFix, fixX: -6, fixY: 0 },
  overrideStyles: { ...tooltipTopStyles }
}

export const UPGRADE_STATE_EQUIPED = 'UPGRADE_STATE_EQUIPED'
export const UPGRADE_STATE_FREE = 'UPGRADE_STATE_FREE'
export const UPGRADE_STATE_EQUIPED_ON_OTHER = 'UPGRADE_STATE_EQUIPED_ON_OTHER'
export const UPGRADE_STATE_DISABLED = 'UPGRADE_STATE_DISABLED'
