import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'
import { Provider } from 'react-redux'
import { bindActionCreators } from 'redux'
import AppContainer from './containers/AppContainer'
import store from './store/createStore'
import { getEquippedItemsRequestAction } from './modules/actions'
import { getEquipedItemsList, getItemInReview } from './modules/selectors'
import './styles/global.scss'
import './styles/lightMode.cssmodule.scss'
import './styles/darkMode.cssmodule.scss'

const MOUNT_NODE = document.getElementById('loadoutsRoot')

// ========================================================
// Render Setup
// ========================================================

const Root = props => (
  <Provider store={store}>
    <AppContainer {...props} />
  </Provider>
)

const render = () => {
  ReactDOM.render(<Root store={store} history={history} />, MOUNT_NODE)
}

const renderError = error => {
  ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
}

// This code is excluded from production bundle
if (__DEV__) {
  // importing real sidebar app bundle only for developing stage
  // import('../../sidebar/src/main')
  // adding Redux store for live accessing in the browser console
  window.store = store
  // ========================================================
  // DEVELOPMENT STAGE! HOT MODULE REPLACE ACTIVATION!
  // ========================================================
  const devRender = () => {
    if (module.hot) {
      module.hot.accept('./containers/AppContainer', () => render())
    }

    render()
  }

  // Wrap render in try/catch
  try {
    devRender()
  } catch (error) {
    console.error(error)
    renderError(error)
  }
} else {
  // ========================================================
  // PRODUCTION GO!
  // ========================================================
  render()
}

/// ///////////////////////////////
/// //// Interface for legacy code
/// ////

/* ACTIONS */
const actions = bindActionCreators(
  {
    getEquippedItemsRequestAction
  },
  store.dispatch
)

const subscribe = {
  onEquippedItemsChange: handler => {
    let previousEquippedItems = getEquipedItemsList(store.getState())

    store.subscribe(() => {
      const currentlyEquippedItems = getEquipedItemsList(store.getState())
      const { lastAction } = store.getState()

      if (lastAction?.type === 'EQUIP_SLOT_SUCCESS') {
        handler(previousEquippedItems, currentlyEquippedItems)
        previousEquippedItems = currentlyEquippedItems
      }
    })
  },
  onModsChanged: handler => {
    store.subscribe(() => {
      const state = store.getState()
      const { lastAction } = state

      if (lastAction?.type === 'ATTACH_UPGRADE_SUCCESS' || lastAction?.type === 'DETACH_UPGRADE_SUCCESS') {
        const itemInReview = getItemInReview(state)
        const { upgrades } = itemInReview

        handler(itemInReview, upgrades)
      }
    })
  }
}

window.loadouts = {
  render,
  actions,
  subscribe
}
