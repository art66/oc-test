import currentLoadouts from './slots'
import currentItems from './equippedItems'

export default {
  currentHairs: [],
  currentItems,
  currentLoadouts,
  currentSettings: { currentSetID: 1, slotsMax: 3 },
  gender: 'male',
  userID: 2265266
}
