import { useCallback } from 'react'
import useOutsideAlerter from './useOutsideAlerter'
import useTooltipWAI from './useTooltipWAI'

export default function useTooltipAlertWAI() {
  const tooltipWAI = useTooltipWAI()

  useOutsideAlerter(
    useCallback(() => {
      tooltipWAI.setActive(false)
    }, [tooltipWAI.setActive])
  )

  return tooltipWAI
}
