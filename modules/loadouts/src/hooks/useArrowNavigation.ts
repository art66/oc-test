import { useCallback, useState } from 'react'

export default function useArrowNavigation(settings: { lenght: number }) {
  const { lenght } = settings

  const [itemsRefs, setItemsRefs] = useState([])
  const [focusedItemIndex, setFocusedItemIndex] = useState(null)
  const [lastLength, setLastLength] = useState(lenght)

  const changeIndex = useCallback(
    (direction: boolean) => {
      let nextFocusedItemIndex

      if (direction) {
        nextFocusedItemIndex = focusedItemIndex < lenght - 1 ? focusedItemIndex + 1 : 0
      } else {
        nextFocusedItemIndex = focusedItemIndex > 0 ? focusedItemIndex - 1 : lenght - 1
      }

      itemsRefs[nextFocusedItemIndex]?.focus()

      setFocusedItemIndex(nextFocusedItemIndex)
    },
    [focusedItemIndex, lenght, itemsRefs]
  )

  const onContainerKeyDown = useCallback(
    (event) => {
      if (event.key === 'ArrowLeft') {
        changeIndex(false)

        event.stopPropagation()
      } else if (event.key === 'ArrowRight') {
        changeIndex(true)

        event.stopPropagation()
      }
    },
    [focusedItemIndex, lenght, itemsRefs]
  )

  const setItemRef = useCallback(
    (index: number, ref: Element) => {
      if (lenght !== lastLength) {
        setItemsRefs([])

        setLastLength(lenght)
      }

      itemsRefs[index] = ref

      setItemsRefs(itemsRefs)
    },
    [lastLength, lenght]
  )

  const onItemFocus = useCallback(
    (index: number) => {
      if (index !== focusedItemIndex) {
        setFocusedItemIndex(index)
      }
    },
    [focusedItemIndex, setFocusedItemIndex]
  )

  return { onContainerKeyDown, setItemRef, onItemFocus }
}
