export type TKeyHandler = (event: React.KeyboardEvent<HTMLButtonElement>) => void

export type TKeysHandlers = { [key: string]: TKeyHandler }
