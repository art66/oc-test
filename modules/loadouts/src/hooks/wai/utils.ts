import { useCallback, useEffect, useState } from 'react'
import { objectFilter, objectForEach } from '../../utils'
import { ARROW_LEFT_KEY, ARROW_RIGHT_KEY, ENTER_KEY, ESCAPE_KEY, TAB_KEY } from './constants'
import { TKeyHandler, TKeysHandlers } from './interfaces'

export function useKey(handler: TKeyHandler, key?: string) {
  if (!key) {
    return useCallback(
      (event) => {
        handler(event)
      },
      [handler]
    )
  }

  return useCallback(
    (event) => {
      if (event.key === key) {
        handler(event)
      }
    },
    [handler]
  )
}

export function useKeys(keysHandlers?: TKeysHandlers, excludeKeysHandlers?: TKeysHandlers) {
  return useCallback(
    (event) => {
      const keyHandler = keysHandlers[event.key]
      const filteredExcludeKeysHandlers = objectFilter(excludeKeysHandlers, ([notKey]) => notKey !== event.key)

      if (keyHandler) {
        keyHandler(event)
      } else if (filteredExcludeKeysHandlers) {
        objectForEach(filteredExcludeKeysHandlers, (handler) => handler(event))
      }
    },
    [keysHandlers, excludeKeysHandlers]
  )
}

export function useNotKey(handler: TKeyHandler, key: string) {
  return useCallback(
    (event) => {
      if (event.key !== key) {
        handler(event)
      }
    },
    [handler]
  )
}

export function useArrowLeftKey(handler) {
  return useKey(handler, ARROW_LEFT_KEY)
}

export function useArrowRightKey(handler) {
  return useKey(handler, ARROW_RIGHT_KEY)
}

export function useTabKey(handler) {
  return useKey(handler, TAB_KEY)
}

export function useEnterKey(handler) {
  return useKey(handler, ENTER_KEY)
}

export function useEscKey(handler) {
  return useKey(handler, ESCAPE_KEY)
}

export function useNotEnterKey(handler) {
  return useNotKey(handler, ENTER_KEY)
}

export function useTabBlocker(disable?: boolean) {
  return useTabKey((event) => {
    if (!disable) {
      event.preventDefault()
    }
  })
}

export function noPropagate(event) {
  event.stopPropagation()
}

export function useCombineListeners(...listeners: TKeyHandler[]) {
  return useCallback(
    (event) => {
      listeners.forEach((listener) => listener(event))
    },
    [listeners]
  )
}

export function useFocuser(ref) {
  const [focused, setFocused] = useState(false)

  const focus = useCallback(() => {
    setFocused(true)
  }, [setFocused])
  const onFocus = useCallback(() => {
    setFocused(false)

    ref.removeEventListener('focus', onFocus)
  }, [ref, setFocused])

  useEffect(() => {
    if (ref && focused) {
      ref.removeEventListener('focus', onFocus)
      ref.addEventListener('focus', onFocus)
    }
  }, [ref, focused, onFocus])

  if (ref && focused) {
    setTimeout(() => {
      ref.focus()
    })
  }
  // FIXME: Should be more stable

  return {
    focused,
    setFocused,
    focus
  }
}

// export function useFocuser(ref) {
//   const [focused, setFocused] = useState(false)

//   const focus = useCallback(() => setFocused(true), [])
//   const onFocus = useCallback(() => setFocused(false), [])

//   if (ref && focused) {
//     setTimeout(() => {
//       ref.focus()
//     })
//   }

//   return {
//     focus,
//     onFocus,
//     focused,
//     setFocused
//   }
// }

export function useReFocuser(ref) {
  const [focused, setFocused] = useState(false)

  const focus = useCallback(() => setFocused(true), [setFocused])
  const onFocus = useCallback(() => {
    setFocused(false)

    ref.removeEventListener('focus', onFocus)
  }, [ref, setFocused])
  const onBlur = useCallback(() => {
    ref.addEventListener('focus', onFocus)

    ref.focus()

    ref.removeEventListener('blur', onBlur)
  }, [ref, onFocus])

  useEffect(() => {
    if (ref && focused) {
      ref.addEventListener('blur', onBlur)

      ref.blur()
    }
  }, [ref, focused, onBlur])

  return {
    focused,
    setFocused,
    focus
  }
}
