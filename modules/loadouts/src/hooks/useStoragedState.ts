import { useCallback, useMemo, useState } from 'react'
import localStorageManager from '../utils/localStorageManager'

export default function useStoragedState(key, initialState) {
  const defaultState = useMemo(() => {
    return localStorageManager.hasKey({ storeKey: key }) ?
      localStorageManager.getStore({ storeKey: key }) :
      localStorageManager.addStore({ storeKey: key, storeData: initialState })
  }, [key, initialState])

  const [state, setStatePure] = useState(defaultState)

  const setState = useCallback(
    (nextState) => {
      setStatePure(nextState)

      localStorageManager.updateStore({ storeKey: key, storeData: nextState })
    },
    [setStatePure, key]
  )

  return [state, setState]
}
