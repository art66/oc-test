import { useCallback, useEffect, useState } from 'react'

export default function useOutsideAlerter(handler) {
  const [containerRef, setContainerRef] = useState(null)

  const handleClickOutside = useCallback(
    (event) => {
      if (containerRef && !containerRef.contains(event.target)) {
        handler()
      }
    },
    [containerRef, handler]
  )

  useEffect(() => {
    if (containerRef) {
      document.addEventListener('click', handleClickOutside)
    }

    return () => {
      document.removeEventListener('click', handleClickOutside)
    }
  }, [containerRef, handleClickOutside])

  return {
    containerRef,
    setContainerRef
  }
}
