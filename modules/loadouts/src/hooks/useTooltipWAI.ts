import { useCallback, useEffect, useState } from 'react'

export default function useTooltipWAI() {
  const [isActive, setActive] = useState(false)
  const [containerRef, setContainerRef] = useState(null)
  const [activatorRef, setActivatorRef] = useState(null)
  const [firstRef, setFirstRef] = useState(null)
  const [lastRef, setLastRef] = useState(null)
  const [deactivatorRef, setDeactivatorRef] = useState(null)
  const [deactivatorAdditionalRef, setDeactivatorAdditionalRef] = useState(null)

  const onActivatorClick = useCallback(() => {
    setActive(true)
  }, [setActive])

  const onContainerEscapeKey = useCallback(
    (event) => {
      if (event.key === 'Escape') {
        if (activatorRef) {
          activatorRef.focus()
        }

        setActive(false)
      }
    },
    [activatorRef, setActive]
  )

  const onContainerEnterKey = useCallback(
    (event) => {
      if (event.key === 'Enter') {
        setActive(true)
      }
    },
    [activatorRef, setActive]
  )

  const onDeactivatorClick = useCallback(
    (event) => {
      if (activatorRef) {
        activatorRef.focus()
      }

      setActive(false)

      event.stopPropagation()
      event.preventDefault()
    },
    [activatorRef, setActive]
  )

  const onDeactivatorKey = useCallback(
    (event) => {
      if (event.key === 'Enter') {
        if (activatorRef) {
          activatorRef.focus()
        }

        setActive(false)

        event.stopPropagation()
        event.preventDefault()
      }
    },
    [activatorRef, setActive]
  )

  const onLastKey = useCallback(
    (event) => {
      if (event.key === 'Tab' && containerRef !== lastRef) {
        if (firstRef) {
          firstRef.focus()
        }

        event.preventDefault()
      }
    },
    [firstRef, containerRef, lastRef]
  )

  useEffect(() => {
    if (activatorRef) {
      activatorRef.addEventListener('click', onActivatorClick)
    }

    return () => {
      if (activatorRef) {
        activatorRef.removeEventListener('click', onActivatorClick)
      }
    }
  }, [activatorRef, onActivatorClick])

  useEffect(() => {
    if (containerRef) {
      containerRef.addEventListener('keydown', onContainerEscapeKey)
      containerRef.addEventListener('keypress', onContainerEnterKey)
    }

    return () => {
      if (containerRef) {
        containerRef.removeEventListener('keydown', onContainerEscapeKey)
        containerRef.removeEventListener('keypress', onContainerEnterKey)
      }
    }
  }, [containerRef, onContainerEscapeKey, onContainerEnterKey])

  useEffect(() => {
    if (deactivatorRef) {
      deactivatorRef.focus()
      deactivatorRef.addEventListener('keydown', onDeactivatorKey)
      deactivatorRef.addEventListener('click', onDeactivatorClick)
    }

    return () => {
      if (deactivatorRef) {
        deactivatorRef.removeEventListener('keydown', onDeactivatorKey)
        deactivatorRef.removeEventListener('click', onDeactivatorClick)
      }
    }
  }, [deactivatorRef, onDeactivatorClick])

  useEffect(() => {
    if (deactivatorAdditionalRef) {
      if (!deactivatorRef) {
        deactivatorAdditionalRef.focus()
      }

      deactivatorAdditionalRef.addEventListener('keydown', onDeactivatorKey)
      deactivatorAdditionalRef.addEventListener('click', onDeactivatorClick)
    }

    return () => {
      if (deactivatorAdditionalRef) {
        deactivatorAdditionalRef.removeEventListener('keydown', onDeactivatorKey)
        deactivatorAdditionalRef.removeEventListener('click', onDeactivatorClick)
      }
    }
  }, [deactivatorAdditionalRef, onDeactivatorClick])

  useEffect(() => {
    if (lastRef) {
      lastRef.addEventListener('keydown', onLastKey)
    }

    return () => {
      if (lastRef) {
        lastRef.removeEventListener('keydown', onLastKey)
      }
    }
  }, [lastRef, onLastKey])

  useEffect(() => {
    if (firstRef) {
      firstRef.focus()
    }
  }, [firstRef])

  return {
    isActive,
    setContainerRef,
    setActivatorRef,
    setFirstRef,
    setLastRef,
    setDeactivatorRef,
    setDeactivatorAdditionalRef,
    setActive,
    containerRef,
    activatorRef,
    deactivatorRef,
    deactivatorAdditionalRef,
    firstRef,
    lastRef
  }
}
