import { useCallback } from 'react'
import useSwipeLeft from './useSwipeLeft'
import useSwipeRight from './useSwipeRight'

export default function useHorisontalSwipe(handler, distance = 75) {
  const onSwipeLeftHandler = useCallback(() => {
    handler(true)
  }, [handler])
  const onSwipeRightHandler = useCallback(() => {
    handler(false)
  }, [handler])

  const swipeLeft = useSwipeLeft(onSwipeLeftHandler, distance)
  const swipeRight = useSwipeRight(onSwipeRightHandler, distance)

  const onStart = useCallback(
    (event) => {
      swipeLeft.onTouchStart(event)
      swipeLeft.onMouseDown(event)
      swipeRight.onTouchStart(event)
      swipeRight.onMouseDown(event)
    },
    [swipeLeft.onTouchStart, swipeLeft.onMouseDown, swipeRight.onTouchStart, swipeRight.onMouseDown]
  )

  const onMove = useCallback(
    (event) => {
      swipeLeft.onTouchMove(event)
      swipeLeft.onMouseMove(event)
      swipeRight.onTouchMove(event)
      swipeRight.onMouseMove(event)
    },
    [swipeLeft.onTouchMove, swipeLeft.onMouseMove, swipeRight.onTouchMove, swipeRight.onMouseMove]
  )

  const onEnd = useCallback(
    (event) => {
      swipeLeft.onTouchEnd(event)
      swipeLeft.onMouseUp(event)
      swipeRight.onTouchEnd(event)
      swipeRight.onMouseUp(event)
    },
    [swipeLeft.onTouchEnd, swipeLeft.onMouseUp, swipeRight.onTouchEnd, swipeRight.onMouseUp]
  )

  return {
    onTouchStart: onStart,
    onMouseDown: onStart,
    onTouchMove: onMove,
    onMouseMove: onMove,
    onTouchEnd: onEnd,
    onMouseUp: onEnd
  }
}
