import { useCallback, useState } from 'react'

export default function useSwipeRight(handler, distance = 75) {
  const [startX, setStartX] = useState(null)

  const onMove = useCallback(
    (event) => {
      if (startX === null) return

      const x = event.touches ? event.touches[0].clientX : event.clientX

      const xDiff = startX - x

      if (xDiff < -distance) {
        handler()

        setStartX(x)
      } else if (xDiff > distance) {
        setStartX(x)
      }
    },
    [startX, handler, setStartX]
  )

  const onEnd = useCallback(
    (_event) => {
      setStartX(null)
    },
    [setStartX]
  )

  const onStart = useCallback(
    (event) => {
      const x = event.touches ? event.touches[0].clientX : event.clientX

      setStartX(x)
    },
    [setStartX]
  )

  return {
    onTouchStart: onStart,
    onMouseDown: onStart,
    onTouchMove: onMove,
    onMouseMove: onMove,
    onTouchEnd: onEnd,
    onMouseUp: onEnd
  }
}
