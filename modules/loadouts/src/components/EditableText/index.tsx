import React, { memo, useCallback, /* useEffect, */ useMemo, useState } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { EDIT, SAVE } from '@torn/shared/SVG/presets/icons/loadouts'
import styles from './index.cssmodule.scss'
import { TProps } from './types'
import { useWAI } from './hooks'

export default memo(function EditableText(props: TProps) {
  const { defaultTitle, save } = props

  const [title, setTitle] = useState(defaultTitle)
  const [nextTitle, setNextTitle] = useState(title)
  const [isTitleEditorIconShowing, showTitleEditorIcon] = useState(false)

  const onSave = useCallback(() => {
    if (title !== nextTitle) {
      setTitle(nextTitle)

      save(nextTitle)
    }
  }, [nextTitle, title, save])

  const onTitleMouseEnter = useCallback(() => showTitleEditorIcon(true), [])
  const onTitleMouseLeave = useCallback(() => showTitleEditorIcon(false), [])
  const onTitleInput = useCallback((event) => {
    setNextTitle(event.target.value)

    event.preventDefault()
  }, [])

  const wai = useWAI({ onSave })

  const titleView = useMemo(() => !wai.isActive && <p className={styles.title}>{title}</p>, [wai.isActive, title])
  const titleEditorIconView = useMemo(
    () =>
      isTitleEditorIconShowing &&
      !wai.isActive && (
        <button type="button" className={styles.editButton} aria-label="Edit title">
          <SVGIconGenerator {...EDIT} />
        </button>
      ),
    [isTitleEditorIconShowing, wai.isActive]
  )
  const titleEditorInputView = useMemo(
    () =>
      wai.isActive && (
        <div className={styles.titleEditor}>
          <input
            ref={wai.setInputRef}
            className={styles.titleEditorInput}
            type="text"
            value={nextTitle}
            onChange={onTitleInput}
            onKeyDown={wai.onInputKeyDown}
            aria-label="Slot title"
          />
          <button
            type="button"
            className={styles.saveButton}
            onKeyDown={wai.onSaveButtonKeyDown}
            onClick={onSave}
            aria-label="Save"
          >
            <SVGIconGenerator {...SAVE} />
          </button>
        </div>
      ),
    [wai.isActive, nextTitle, onTitleInput, wai.setInputRef, wai.onSaveButtonKeyDown, wai.onInputKeyDown, onSave]
  )

  return (
    <div
      ref={wai.setContainerRef}
      className={styles.titleContainer}
      onClick={wai.onContainerClick}
      onKeyDown={wai.onContainerKeyDown}
      onMouseEnter={onTitleMouseEnter}
      onMouseLeave={onTitleMouseLeave}
      tabIndex={0}
      aria-label={title}
    >
      {titleView}
      {titleEditorInputView}
      {titleEditorIconView}
    </div>
  )
})
