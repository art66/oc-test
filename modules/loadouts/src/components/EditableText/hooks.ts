import { useCallback, useState } from 'react'
import { useFocuser, useTabKey } from '../../hooks'
import { useCombineListeners, useEnterKey, useEscKey } from '../../hooks/wai/utils'

export function useWAI(props) {
  const { onSave: onSaveProp } = props

  const [isActive, setActive] = useState(false)
  const [containerRef, setContainerRef] = useState(null)
  const [inputRef, setInputRef] = useState(null)

  const containerRefFocuser = useFocuser(containerRef)
  const inputRefFocuser = useFocuser(inputRef)

  const onActivate = useCallback(() => {
    setActive(true)

    setTimeout(() => {
      inputRefFocuser.focus()
    })
  }, [inputRefFocuser.focus])
  const onDeactivate = useCallback(() => {
    setActive(false)

    setTimeout(() => {
      containerRefFocuser.focus()
    })
  }, [setActive, containerRefFocuser.focus])
  const onSave = useCallback(
    (event) => {
      onSaveProp()

      onDeactivate()

      event.stopPropagation()
    },
    [onSaveProp, onDeactivate]
  )

  const onContainerClick = onActivate

  const onContainerKeyDown = useEnterKey(onActivate)
  // FIXME: It avoids reactions on the slots arrow navigation. It should be realized in a more correct way

  const onInputKeyDown = useCombineListeners(useEscKey(onDeactivate), useEnterKey(onSave))

  const onSaveButtonKeyDown = useCombineListeners(useTabKey(inputRefFocuser.focus), useEnterKey(onSave))

  return {
    isActive,
    setActive,
    containerRefFocuser,
    containerRef,
    setContainerRef,
    setInputRef,
    inputRef,
    onDeactivate,
    onContainerClick,
    onContainerKeyDown,
    onSaveButtonKeyDown,
    onInputKeyDown
  }
}
