import { IItem } from '../../modules/interfaces'

export interface IProps {
  id: string
  name: IItem['name']
  type: IItem['type']
}
