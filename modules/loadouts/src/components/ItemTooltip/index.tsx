import React, { memo, useEffect } from 'react'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { IProps } from './types'
import styles from './index.cssmodule.scss'
import { ITEM_CONFIG } from '../../constants'

export default memo(function ItemTooltip(props: IProps) {
  const { id, name, type } = props

  useEffect(() => {
    tooltipsSubscriber.subscribe({
      ID: id,
      child: (
        <>
          <p className={styles.tooltipName}>{name}</p>
          <p className={styles.tooltipType}>({type})</p>
        </>
      ),
      customConfig: ITEM_CONFIG
    })

    return () => {
      tooltipsSubscriber.unsubscribe({
        ID: id,
        child: null
      })
    }
  }, [id, name, type])

  return null
})
