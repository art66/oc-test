import { TSetRef } from '../../utils/types'

export interface IProps {
  message: string | string[]
  onOkay?: () => any
  onYes?: () => any
  onNo?: () => any
  wai: {
    setContainerRef: TSetRef
    setFirstRef: TSetRef
    setLastRef: TSetRef
    setDeactivatorRef: TSetRef
    setDeactivatorAdditionalRef: TSetRef
    activatorRef: HTMLButtonElement
    setActive: (isActive: boolean) => any
  }
}
