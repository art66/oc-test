// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'globalSvgShadow': string;
  'important': string;
  'message': string;
  'messageButton': string;
  'messageButtons': string;
  'messageDescription': string;
  'textRow': string;
}
export const cssExports: CssExports;
export default cssExports;
