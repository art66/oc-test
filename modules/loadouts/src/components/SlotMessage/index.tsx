import React, { useCallback, useMemo } from 'react'
import { IProps } from './types'
import cn from 'classnames'
import styles from './index.cssmodule.scss'

export default (function SlotMessage(props: IProps) {
  const { message, onOkay, onYes, onNo, wai } = props

  const getLastButtonRefSetter = useCallback(
    (ref) => {
      wai.setLastRef(ref)
      wai.setDeactivatorRef(ref)
    },
    [wai.setLastRef, wai.setDeactivatorRef]
  )

  const onYesExtended = useCallback(() => {
    if (wai.activatorRef) {
      wai.activatorRef.focus()
    }

    wai.setActive(false)

    onYes()
  }, [onYes, wai.activatorRef])

  const messageView = useMemo(() => {
    if (Array.isArray(message)) {
      return message.map((row) => (
        <span key={row} className={styles.textRow}>
          {row}
        </span>
      ))
    }

    return message
  }, [onOkay])
  const buttonsContainerCns = useMemo(() => cn(styles.messageButtons, onYes && styles.important), [onYes])
  const buttonsView = useMemo(() => {
    if (onYes) {
      return (
        <>
          <button onClick={onYesExtended} type="button" className={styles.messageButton}>
            Yes
          </button>
          <button ref={getLastButtonRefSetter} type="button" className={styles.messageButton} onClick={onNo}>
            No
          </button>
        </>
      )
    }

    return (
      <button ref={getLastButtonRefSetter} type="button" className={styles.messageButton} onClick={onOkay}>
        Okay
      </button>
    )
  }, [onOkay, onNo, onYes, wai.setLastRef])

  return (
    <div ref={wai.setContainerRef} className={styles.message}>
      <p ref={wai.setFirstRef} className={styles.messageDescription} tabIndex={0} role="alert" aria-live="assertive">
        {messageView}
      </p>
      <div className={buttonsContainerCns}>{buttonsView}</div>
    </div>
  )
})
