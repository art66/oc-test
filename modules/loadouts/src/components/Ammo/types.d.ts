import { IItem } from '../../modules/interfaces'

export type TItemProps = Pick<IItem, 'currentAmmoType' | 'type' | 'clips' | 'isBlank'>

export interface IProps extends TItemProps {
  itemId: IItem['uniqueId']
}
