import { createSelector } from '@reduxjs/toolkit'
import { MELEE_ITEM_TYPE, PRIMARY_ITEM_TYPE, SECONDARY_ITEM_TYPE, TEMPORARY_ITEM_TYPE } from '../../constants'
import { TNormalizedAmmo } from '../../containers/ItemReview/interfaces'

export const getNormalizedAmmo = createSelector(
  [data => data],
  (ammoData: any): TNormalizedAmmo => {
    const { type, clips } = ammoData

    let ammo
    let ammoDescription
    let ammoAmount
    if (type === PRIMARY_ITEM_TYPE || type === SECONDARY_ITEM_TYPE) {
      if (clips) {
        const { currentAmount, maxAmmo, maxClips, amount } = clips

        if (currentAmount < 1000 && maxAmmo < 1000) {
          ammo = `${currentAmount}/${maxAmmo} (${maxClips})`
        } else {
          ammo = `${currentAmount} (${maxClips})`
        }

        ammoDescription =
          `Ammo amount is ${currentAmount}, maximum number of ammo is` +
          ` ${maxAmmo}, maximum number of clips is ${maxClips}. Tatal amount is ${amount}.`
        ammoAmount = amount
      } else {
        ammo = 'N/A'
        ammoDescription = `The ammo amount is not available`
        ammoAmount = null
      }
    } else if (type === MELEE_ITEM_TYPE) {
      ammo = 'eternity'
      ammoDescription = `Ammo amount is eternity`
      ammoAmount = null
    } else if (type === TEMPORARY_ITEM_TYPE) {
      ammo = '1'
      ammoDescription = `Ammo amount is 1`
      ammoAmount = null
    } else {
      ammo = null
      ammoDescription = ''
      ammoAmount = null
    }

    return {
      ammo,
      ammoDescription,
      ammoAmount
    }
  }
)
