import React, { memo, useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { getNormalizedAmmo } from './selectors'
import { switchAmmoRequestAction } from '../../modules/actions'
import {
  AMMO_TYPE_HOLLOW_POINT,
  AMMO_TYPE_TRACER,
  AMMO_TYPE_PIERCING,
  AMMO_TYPE_INCENDIARY,
  AMMO_TYPE_TITLES,
  AMMO_TYPE_STANDARD
} from '../../constants'
import { getIsSwitchAmmoLoading } from '../../modules/selectors'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import { useEnterKey } from '../../hooks'

export default memo((props: IProps) => {
  const { itemId, currentAmmoType, type, clips, isBlank } = props

  const ammoColor = cn({
    [styles.standard]: currentAmmoType === AMMO_TYPE_STANDARD,
    [styles.hollowPoint]: currentAmmoType === AMMO_TYPE_HOLLOW_POINT,
    [styles.tracer]: currentAmmoType === AMMO_TYPE_TRACER,
    [styles.piercing]: currentAmmoType === AMMO_TYPE_PIERCING,
    [styles.incendiary]: currentAmmoType === AMMO_TYPE_INCENDIARY
  })

  const dispatch = useDispatch()

  const { ammo, ammoDescription, ammoAmount } = getNormalizedAmmo({ type, clips })

  const isSwitchAmmoLoading = useSelector(getIsSwitchAmmoLoading)

  const tooltipId = useMemo(() => `${itemId}-ammo-tooltip`, [itemId])

  const currentAmmoTypeTitle = useMemo(() => (currentAmmoType ? AMMO_TYPE_TITLES[currentAmmoType] : ''), [
    currentAmmoType
  ])

  const onSwitchAmmo = useCallback(() => !isBlank && currentAmmoType && dispatch(switchAmmoRequestAction(itemId)), [
    currentAmmoType,
    dispatch,
    switchAmmoRequestAction,
    itemId,
    isBlank
  ])

  const onAmmoEnterKeyPress = useEnterKey(onSwitchAmmo)

  const ammoTitleView = useMemo(() => {
    if (ammo === 'eternity') {
      return <i className={styles.eternity} />
    } else if (!isBlank) {
      return ammo
    }

    return ''
  }, [ammo, isBlank])

  useEffect(() => {
    if (ammo && currentAmmoTypeTitle) {
      tooltipsSubscriber.subscribe({
        ID: tooltipId,
        child: (
          <div className={cn(styles.ammoTooltip, ammoColor)}>
            <p className={styles.currentAmmoTypeTitle}>{currentAmmoTypeTitle}</p>
            <p className={styles.ammoAvailable}>{ammoAmount} available</p>
          </div>
        )
      })

      return () => {
        tooltipsSubscriber.unsubscribe({
          ID: tooltipId,
          child: null
        })
      }
    }
  }, [tooltipId, ammoColor, currentAmmoTypeTitle, ammo, ammoAmount])

  return (
    <div
      className={styles.ammo}
      tabIndex={0}
      aria-label={`${currentAmmoTypeTitle ? `Ammo type is "${currentAmmoTypeTitle}". ` : ''}${ammoDescription}`}
      onClick={onSwitchAmmo}
      onKeyPress={onAmmoEnterKeyPress}
    >
      <span id={tooltipId} className={cn(styles.ammoDescription, ammoColor, isSwitchAmmoLoading && styles.loading)}>
        {ammoTitleView}
      </span>
    </div>
  )
})
