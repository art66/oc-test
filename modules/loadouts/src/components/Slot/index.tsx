import React, { useCallback, useMemo, memo } from 'react'
import cn from 'classnames'
import { EQUIP, RESET, EQUIP_LOCKED, RESET_LOCKED } from '@torn/shared/SVG/presets/icons/loadouts'
import { useDispatch, useSelector } from 'react-redux'
import styles from './index.cssmodule.scss'
import { IProps } from './types'
import { equipSlotRequestAction, resetSlotRequestAction, setLoadoutTitleRequestAction } from '../../modules/actions'
import { getCurrentSlotId } from '../../modules/selectors'
import SlotItems from '../SlotItems'
import EditableText from '../EditableText'
import { useBindArrowNavigation, useIcon, useMessage } from './hooks'

export default memo(function Slot(props: IProps) {
  const { className, index, slotId, title, items, blank, setRef, onFocus } = props

  const dispatch = useDispatch()

  const currentSlotId = useSelector(getCurrentSlotId)

  const isCurrent = useMemo(() => slotId === currentSlotId, [slotId, currentSlotId])

  const { setIndexedRef, onFocusHandler } = useBindArrowNavigation({ index, setRef, onFocus })

  const onResetMessageYesClick = useCallback(() => {
    dispatch(resetSlotRequestAction(slotId))
  }, [slotId, dispatch, resetSlotRequestAction])

  const { view: resetMessageView, wai: resetMessageWAI } = useMessage({
    message: ['Are you sure you want', 'to reset this loadout?'],
    onYes: onResetMessageYesClick
  })
  const { view: resetCurrentMessageView, wai: resetCurrentMessageWAI } = useMessage({
    message: ['This is your current Loadout.', 'Please switch to a different one', 'to reset this slot.']
  })
  const { view: resetBlankMessageView, wai: resetBlankMessageWAI } = useMessage({
    message: ['This is a blank Loadout.', 'You can\'t reset it.']
  })

  const setActiatorRef = useCallback(
    (ref) => {
      if (isCurrent) {
        resetCurrentMessageWAI.setActivatorRef(ref)
      } else if (blank) {
        resetBlankMessageWAI.setActivatorRef(ref)
      } else {
        resetMessageWAI.setActivatorRef(ref)
      }
    },
    [
      isCurrent,
      blank,
      resetBlankMessageWAI.setActivatorRef,
      resetMessageWAI.setActivatorRef,
      resetCurrentMessageWAI.setActivatorRef
    ]
  )

  const onEquipClick = useCallback(() => dispatch(equipSlotRequestAction(slotId)), [dispatch, slotId])
  const saveTitle = useCallback((nextTitle) => dispatch(setLoadoutTitleRequestAction({ slotId, nextTitle })), [slotId])

  const classNames = useMemo(() => cn(styles.slot, { [styles.current]: isCurrent }, className), [isCurrent, className])

  const equipIconView = useIcon({ isActive: isCurrent, iconConfig: EQUIP, lockedIconConfig: EQUIP_LOCKED })
  const resetIconView = useIcon({ isActive: isCurrent, iconConfig: RESET, lockedIconConfig: RESET_LOCKED })

  const bodyContentView = useMemo(() => {
    if (resetMessageWAI.isActive) {
      return resetMessageView
    } else if (resetCurrentMessageWAI.isActive) {
      return resetCurrentMessageView
    } else if (resetBlankMessageWAI.isActive) {
      return resetBlankMessageView
    }

    return <SlotItems items={items} />
  }, [items, resetMessageWAI.isActive, resetCurrentMessageWAI.isActive, resetBlankMessageWAI.isActive])
  const ariaLabel = useMemo(() => {
    return isCurrent ? `"${title}" is the current set` : `"${title}" set`
  }, [title, isCurrent])

  return (
    <li ref={setIndexedRef} className={classNames} onFocus={onFocusHandler} tabIndex={0} aria-label={ariaLabel}>
      <header className={styles.header}>
        <EditableText defaultTitle={title} save={saveTitle} />
        <div className={styles.buttons}>
          <button type='button' ref={setActiatorRef} className={styles.button} aria-label='Reset loadout'>
            {resetIconView}
          </button>
          <button
            type='button'
            className={styles.button}
            onClick={onEquipClick}
            aria-label='Equip loadout'
            disabled={isCurrent}
          >
            {equipIconView}
          </button>
        </div>
      </header>
      <section className={styles.body}>{bodyContentView}</section>
    </li>
  )
})
