import { TSlot } from '../../modules/interfaces'

export interface IProps extends TSlot {
  className?: string
  index: number
  setRef?: (index: number, ref: Element) => void
  onFocus?: (index: number) => void
}
