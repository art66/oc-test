import React, { useCallback, useMemo } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import SlotMessage from '../SlotMessage'
import { useTooltipWAI } from '../../hooks'
import styles from './index.cssmodule.scss'

export const useMessage = (props) => {
  const { message, onOkay, onYes, onNo } = props

  const wai = useTooltipWAI()

  const view = useMemo(() => <SlotMessage message={message} onOkay={onOkay} onYes={onYes} onNo={onNo} wai={wai} />, [
    message,
    onOkay,
    onYes,
    onNo,
    wai
  ])

  return { view, wai }
}

export const useIcon = (props) => {
  const { isActive, iconConfig, lockedIconConfig } = props

  return useMemo(
    () =>
      !isActive ? (
        <SVGIconGenerator customClass={styles.icon} {...iconConfig} />
      ) : (
        <SVGIconGenerator customClass={styles.icon} {...lockedIconConfig} />
      ),
    [isActive, iconConfig, lockedIconConfig]
  )
}

export function useBindArrowNavigation(props) {
  const { index, setRef, onFocus } = props

  const setIndexedRefCallback = useCallback((ref) => setRef(index, ref), [index, setRef])
  const onFocusHandlerCallback = useCallback(() => onFocus(index), [index, onFocus])

  const setIndexedRef = useMemo(() => {
    if (setRef) {
      return setIndexedRefCallback
    }
  }, [setIndexedRefCallback, setRef])
  const onFocusHandler = useMemo(() => {
    if (setRef) {
      return onFocusHandlerCallback
    }
  }, [onFocusHandlerCallback, onFocus])

  return {
    setIndexedRef,
    onFocusHandler
  }
}
