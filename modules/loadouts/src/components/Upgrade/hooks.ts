import { useCallback, useState } from 'react'
import { useFocuser, useEnterKey, useEscKey, useTabKey } from '../../hooks'

export function useWAI(disabled?: boolean) {
  const [isActive, setActive] = useState(false)
  const [activatorRef, setActivatorRefPure] = useState(null)
  const [firstRef, setFirstRef] = useState(null)
  const [lastRef, setLastRef] = useState(null)

  const setActivatorRef = useCallback((ref) => !disabled && setActivatorRefPure(ref), [disabled, setActivatorRefPure])

  const activatorRefFocuser = useFocuser(activatorRef)
  const firstRefFocuser = useFocuser(firstRef)

  const onActivate = useCallback(() => {
    setActive(true)

    firstRefFocuser.focus()
  }, [setActive, firstRefFocuser.focus])
  const onDeactivate = useCallback(() => {
    setActive(false)

    activatorRefFocuser.focus()
  }, [setActive])

  const onActivatorClick = useCallback(
    (event) => {
      if (!isActive) {
        onActivate()
      } else {
        onDeactivate()
      }

      event.preventDefault()
    },
    [isActive, onActivate, onDeactivate]
  )
  const onActivatorKeyUp = useEnterKey((event) => {
    onActivate()

    event.preventDefault()
  })

  const onContainerEscKeyDown = useEscKey(onDeactivate)

  const onLastItemTabKeyDown = useTabKey(firstRefFocuser.focus)

  return {
    isActive,
    setActive,
    activatorRef,
    setActivatorRef,
    setFirstRef,
    setLastRef,
    lastRef,
    firstRef,
    onActivatorClick,
    onActivatorKeyUp,
    onContainerEscKeyDown,
    onLastItemTabKeyDown
  }
}
