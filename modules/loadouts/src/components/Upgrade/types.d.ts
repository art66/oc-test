import { TItemUpgrade } from '../../modules/interfaces'
import { IProps as ITooltipConfig } from '@torn/shared/components/TooltipNew/types'

export interface IProps extends TItemUpgrade {
  itemId: string
  viewId: string
  className?: string
  id: string
  index: number
  tooltipConfig?: ITooltipConfig
  disabled?: boolean
}
