/* eslint-disable react/jsx-handler-names */
import React from 'react'
import cn from 'classnames'
import { IProps } from './types'
import styles from './index.cssmodule.scss'
import Bonus from '../Bonus'
import UpgradePanel from '../UpgradePanel'
import { useWAI } from './hooks'
import { useOutsideAlerter } from '../../hooks'

const Upgrade = (props: IProps) => {
  const { itemId, viewId, className, title, desc, icon, hoverover, tooltipConfig, index, isBlank, disabled } = props
  const wai = useWAI(isBlank && disabled)
  const outsideAlerter = useOutsideAlerter(() => wai.setActive(false))
  const upgradesPanelView = () =>
    !isBlank
    && !disabled
    && wai.isActive && (
      <UpgradePanel
        itemId={itemId}
        onKeyDown={wai.onContainerEscKeyDown}
        onLastItemTabKeyDown={wai.onLastItemTabKeyDown}
        setFirstRef={wai.setFirstRef}
        setLastRef={wai.setLastRef}
        itemUpgradeIndex={index}
      />
    )
  const ariaLabel = () => {
    if (title && desc) {
      return `"${title}" modification: ${desc}. Attach additional modification`
    } else if (title) {
      return `"${title}" modification. Attach additional modification`
    } else if (!isBlank) {
      return 'Attach modification'
    }

    return 'Blank modification'
  }

  return (
    <div ref={outsideAlerter.setContainerRef} className={cn(className, styles.upgrade)}>
      <Bonus
        setRef={wai.setActivatorRef}
        onClick={wai.onActivatorClick}
        onKeyUp={wai.onActivatorKeyUp}
        viewId={viewId}
        id={viewId}
        icon={icon}
        hoverover={hoverover}
        tooltipConfig={tooltipConfig}
        ariaLabel={ariaLabel()}
      />
      {upgradesPanelView()}
    </div>
  )
}

export default Upgrade
