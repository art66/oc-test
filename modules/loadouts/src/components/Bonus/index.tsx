import React, { useEffect, useMemo } from 'react'
import cn from 'classnames'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import { IProps } from './types'
import styles from './index.cssmodule.scss'

const Bonus = (props: IProps) => {
  const {
    viewId,
    className,
    title,
    desc,
    icon,
    hoverover,
    tooltipConfig,
    ariaLabel: defaultAriaLabel,
    setRef,
    onClick,
    onKeyUp
  } = props

  const classNames = cn(styles.bonus, className)
  const iconClassNames = `bonus-attachment-${icon}`
  const tooltipId = `tooltip-id-${viewId}`

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    if (hoverover) {
      tooltipsSubscriber.subscribe({
        ID: tooltipId,
        customConfig: tooltipConfig,
        // eslint-disable-next-line react/no-danger
        child: <span dangerouslySetInnerHTML={{ __html: hoverover }} />
      })

      return () => {
        tooltipsSubscriber.unsubscribe({
          ID: tooltipId,
          child: null
        })
      }
    }
  }, [tooltipId, hoverover, tooltipConfig])
  const ariaLabel = useMemo(() => {
    if (defaultAriaLabel) {
      return defaultAriaLabel
    } else if (title) {
      return `"${title}" bonus: ${desc}`
    }

    return 'No bonus attached'
  }, [defaultAriaLabel, desc, title])

  return (
    <button
      ref={setRef}
      type='button'
      className={classNames}
      aria-label={ariaLabel}
      onClick={onClick}
      onKeyUp={onKeyUp}
    >
      <i id={tooltipId} className={iconClassNames} />
    </button>
  )
}

export default Bonus
