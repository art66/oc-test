import { TBonus } from '../../modules/interfaces'
import { IProps as ITooltipConfig } from '@torn/shared/components/TooltipNew/types'

export interface IProps extends TBonus {
  viewId: string
  className?: string
  id: string
  tooltipConfig?: ITooltipConfig
  ariaLabel?: string
  setRef?: (ref: HTMLElement) => any
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => any
  onKeyUp?: (event: React.KeyboardEvent<HTMLButtonElement>) => any
}
