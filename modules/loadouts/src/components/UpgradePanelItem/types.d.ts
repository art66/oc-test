import { TItemUpgrade } from '../../modules/interfaces'
import { IDetailsUpgrade } from '../UpgradePanel/types'

export interface IProps extends IDetailsUpgrade {
  itemUpgradeIndex: number
  index: number
  id: TItemUpgrade['id']
  onHover: (index: number) => void
  setRef?: (ref: Element) => void
  onFocus?: (index: number) => void
  onActivate: (upgradeId: string) => void
  onDeactivate: (upgradeId: string) => void
  onKeyDown?: (event: React.KeyboardEvent<HTMLButtonElement>) => any
}
