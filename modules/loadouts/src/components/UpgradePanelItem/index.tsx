import React, { memo, useCallback, useMemo } from 'react'
import cn from 'classnames'
import { IProps } from './types'
import styles from './index.cssmodule.scss'
import {
  UPGRADE_STATE_EQUIPED,
  UPGRADE_STATE_EQUIPED_ON_OTHER,
  UPGRADE_STATE_DISABLED,
  UPGRADE_STATE_FREE
} from '../../constants'

export default memo(function UpgradePanelItem(props: IProps) {
  const {
    id,
    index,
    abbilityTitle,
    imgClassName,
    state,
    onKeyDown,
    onHover,
    onFocus,
    setRef,
    onActivate,
    onDeactivate
  } = props

  const onMouseEnter = useCallback(() => onHover(index), [onHover, index])
  const onMouseLeave = useCallback(() => onHover(null), [onHover, index])
  const onClick = useCallback(
    (event) => {
      if (state === UPGRADE_STATE_FREE || state === UPGRADE_STATE_EQUIPED_ON_OTHER) {
        onActivate(id)
      } else if (state === UPGRADE_STATE_EQUIPED) {
        onDeactivate(id)
      }

      event.stopPropagation()
    },
    [id, onActivate]
  )
  const onFocusHandler = useCallback(() => onFocus(index), [index, onFocus])

  const classNames = useMemo(
    () =>
      cn(styles.item, {
        [styles.equiped]: state === UPGRADE_STATE_EQUIPED,
        [styles.equipedOnOther]: state === UPGRADE_STATE_EQUIPED_ON_OTHER,
        [styles.disabled]: state === UPGRADE_STATE_DISABLED
      }),
    [state]
  )
  const imgClassNames = useMemo(() => cn(styles.image, imgClassName), [imgClassName])
  const ariaLabel = useMemo(() => {
    if (state === UPGRADE_STATE_FREE) {
      return `Attach "${abbilityTitle}" modification`
    } else if (state === UPGRADE_STATE_EQUIPED_ON_OTHER) {
      return `Reattach "${abbilityTitle}" modification`
    } else if (state === UPGRADE_STATE_EQUIPED) {
      return `Detach "${abbilityTitle}" modification`
    }

    return `Disabled "${abbilityTitle}" modification`
  }, [state, abbilityTitle])

  return (
    <button
      type="button"
      ref={setRef}
      className={classNames}
      aria-label={ariaLabel}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onClick={onClick}
      onFocus={onFocusHandler}
      onKeyDown={onKeyDown}
    >
      <i className={imgClassNames} />
    </button>
  )
})
