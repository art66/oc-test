import React, { memo, useEffect, useMemo } from 'react'
import { tooltipsSubscriber } from '@torn/shared/components/TooltipNew'
import styles from './index.cssmodule.scss'
import { TProps } from './types'
import { EXP_BAR_TOOLTIP_CONFIG } from '../../constants'

export default memo(function ExpBar(props: TProps) {
  const { id, expPercent } = props

  const tooltipId = useMemo(() => `exp-bar-tooltip-${id}`, [id])

  useEffect(() => {
    tooltipsSubscriber.subscribe({
      ID: tooltipId,
      child: <>{expPercent}%</>,
      customConfig: EXP_BAR_TOOLTIP_CONFIG
    })

    return () => {
      tooltipsSubscriber.unsubscribe({
        ID: tooltipId,
        child: null
      })
    }
  }, [tooltipId, expPercent])

  return (
    <div className={styles.exp} tabIndex={0} aria-label={`Exp is ${expPercent}%`}>
      <div className={styles.perc} style={{ width: `${expPercent}%` }} />
      <div id={tooltipId} className={styles.tooltipActivator} />
    </div>
  )
})
