import React, { memo, useCallback, useMemo } from 'react'
import cn from 'classnames'
import { TProps } from './types'
import styles from './index.cssmodule.scss'
import SlotItem from '../SlotItem'
import { MAX_SLOT_ITEMS_COUNT } from './constants'
import { useArrowNavigation } from '../../hooks'
import slotItemStyles from '../SlotItem/index.cssmodule.scss'

export default memo(function SlotItems(props: TProps) {
  const { items } = props
  const listLength = items.length

  const isHasExtraItems = useMemo(() => listLength > MAX_SLOT_ITEMS_COUNT, [listLength])
  const maxItemIndex = useMemo(() => (isHasExtraItems ? MAX_SLOT_ITEMS_COUNT - 1 : listLength), [listLength])

  const arrowNavigation = useArrowNavigation({ lenght: isHasExtraItems ? maxItemIndex + 1 : maxItemIndex })

  const setIndexedRef = useCallback(
    (ref) => {
      arrowNavigation.setItemRef(maxItemIndex, ref)
    },
    [maxItemIndex, arrowNavigation.setItemRef]
  )
  const onFocusHandler = useCallback(() => {
    arrowNavigation.onItemFocus(maxItemIndex)
  }, [maxItemIndex, arrowNavigation.onItemFocus])

  const itemsView = useMemo(() => {
    return items
      .slice(0, maxItemIndex)
      .map((item, index) => (
        <SlotItem
          {...item}
          key={item.uniqueId}
          index={index}
          className={styles.slotItem}
          setRef={arrowNavigation.setItemRef}
          onFocus={arrowNavigation.onItemFocus}
        />
      ))
  }, [items, arrowNavigation.setItemRef, arrowNavigation.onItemFocus, maxItemIndex])
  const extraItemsCountView = useMemo(() => {
    if (listLength > maxItemIndex) {
      const extraItemsCount = listLength - maxItemIndex

      return (
        <li
          ref={setIndexedRef}
          className={cn(styles.extraCount, slotItemStyles.slotItem, styles.slotItem)}
          onFocus={onFocusHandler}
          aria-label={`Extra ${extraItemsCount} items not shown in this list.`}
          tabIndex={0}
        >
          +{extraItemsCount}
        </li>
      )
    }
  }, [listLength, maxItemIndex, setIndexedRef, onFocusHandler])

  return (
    <ul className={styles.slotItems} onKeyDown={arrowNavigation.onContainerKeyDown}>
      {itemsView}
      {extraItemsCountView}
    </ul>
  )
})
