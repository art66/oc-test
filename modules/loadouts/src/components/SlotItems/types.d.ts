import { TItems } from '../../modules/interfaces'

export type TProps = {
  items: TItems
}
