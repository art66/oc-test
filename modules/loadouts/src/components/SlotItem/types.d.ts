import { IItem } from '../../modules/interfaces'

export interface IProps extends IItem {
  index: number
  className?: string
  setRef?: (index: number, ref: Element) => void
  onFocus?: (index: number) => void
}
