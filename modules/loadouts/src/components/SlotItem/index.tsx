import React, { memo, useCallback } from 'react'
import cn from 'classnames'
import getItemGlowClassName from '@torn/shared/utils/getItemGlowClassName'
import { IProps } from './types'
import styles from './index.cssmodule.scss'
import ItemTooltip from '../ItemTooltip'

export default memo((props: IProps) => {
  const { className, index, uniqueId, name, imgSmallSrc, type, exists, rarity, setRef, onFocus } = props

  const classNames = cn(styles.slotItem, !exists && styles.notExists, className)
  const tooltipId = `${uniqueId}-slot-item_tooltip`
  const ariaLabel = `"${name}" item`

  const setIndexedRef = useCallback(
    ref => {
      setRef(index, ref)
    },
    [index]
  )
  const onFocusHandler = useCallback(() => {
    onFocus(index)
  }, [index])

  return (
    <li
      ref={setIndexedRef}
      id={tooltipId}
      className={classNames}
      aria-label={ariaLabel}
      tabIndex={0}
      onFocus={onFocusHandler}
    >
      <img className={cn(styles.img, getItemGlowClassName(rarity))} src={imgSmallSrc} />
      <ItemTooltip id={tooltipId} name={name} type={type} />
    </li>
  )
})
