export type TProps = {
  name: string
  className?: string
  iconClassName?: string
  value?: string | number
}
