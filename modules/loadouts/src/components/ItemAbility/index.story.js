import React from 'react'
import { text, number } from '@storybook/addon-knobs'

import ItemAbility from './index'
import { DEFAULT_STATE } from './mocks'

export default {
  title: 'Modules.Item.ItemAbility'
}

export const Defaults = () => {
  const config = {
    ...DEFAULT_STATE,
    value: text('Value', '41.90')
  }

  return <ItemAbility {...config} />
}

Defaults.story = {
  name: 'defaults'
}

export const ValueAsNumber = () => {
  const config = {
    ...DEFAULT_STATE,
    value: number('Value', 41.9)
  }

  return <ItemAbility {...config} />
}

ValueAsNumber.story = {
  name: 'value as number'
}

export const WithoutValue = () => {
  const config = {
    ...DEFAULT_STATE
  }

  return <ItemAbility {...config} />
}

WithoutValue.story = {
  name: 'without value'
}
