import React, { memo, useMemo } from 'react'
import cn from 'classnames'
import { TProps } from './types'
import styles from './index.cssmodule.scss'

export default memo(function ItemAbility(props: TProps) {
  const { className: extraClassName, iconClassName: extraIconClassName, name, value } = props

  const className = useMemo(() => cn(styles.itemAbility, extraClassName), [extraClassName])
  const iconClassName = useMemo(() => cn(styles.accuracyBonusIcon, extraIconClassName), [extraIconClassName])
  const normalizedValue = useMemo(() => value || 'N/A', [value])

  return (
    <span className={className} tabIndex={0} aria-label={`${name} bonus is ${value}`}>
      <i className={iconClassName} />
      {normalizedValue}
    </span>
  )
})
