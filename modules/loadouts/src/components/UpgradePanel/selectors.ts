import { createSelector } from 'reselect'
import { TItemId, TUpgrades } from '../../modules/interfaces'
import {
  UPGRADE_STATE_DISABLED,
  UPGRADE_STATE_EQUIPED_ON_OTHER,
  UPGRADE_STATE_EQUIPED,
  UPGRADE_STATE_FREE
} from '../../constants'
import { resolvePriority } from '../../utils'
import { TUpgradeState } from '../../types'
import { TUpgradePriority, TDetailsUpgrades, TUpgradeHintTitle } from './types'

export const isArmoryAllowUpgrade = createSelector([data => data], ({ itemId, armoryList }) =>
  armoryList.some(armoryItem => armoryItem.armouryID === itemId))

export const getUpgradeState = createSelector(
  [data => data],
  ({ attached, attachedToItemId, itemUniqueId, armoryList }): TUpgradeState => {
    if (!isArmoryAllowUpgrade({ itemId: itemUniqueId, armoryList })) {
      return UPGRADE_STATE_DISABLED
    } else if (attached && typeof attachedToItemId === 'string' && attachedToItemId !== itemUniqueId) {
      return UPGRADE_STATE_EQUIPED_ON_OTHER
    } else if (attached && typeof attachedToItemId === 'string' && attachedToItemId === itemUniqueId) {
      return UPGRADE_STATE_EQUIPED
    }

    return UPGRADE_STATE_FREE
  }
)

export const getHintTitle = createSelector<
{ state: TUpgradeState; attachedToArmouryName: string },
{ state: TUpgradeState; attachedToArmouryName: string },
string
>(
  [data => data],
  ({ state, attachedToArmouryName }): TUpgradeHintTitle => {
    if (state === UPGRADE_STATE_EQUIPED) {
      return '(click mod to detach)'
    } else if (state === UPGRADE_STATE_FREE) {
      return '(click mod to attach)'
    } else if (state === UPGRADE_STATE_EQUIPED_ON_OTHER) {
      return `(attached to ${attachedToArmouryName})`
    } else if (state === UPGRADE_STATE_DISABLED) {
      return '(not compatible)'
    }

    return ''
  }
)

export const getAttachedToArmouryName = createSelector([(data: any) => data], ({ armoryId, armouryList }): string => {
  const armory = armouryList.find(listArmory => listArmory.armouryID === armoryId)

  return armory ? armory.type : ''
})

export const getUpgradeStatePriority = createSelector(
  [data => data],
  (state: TUpgradeState): TUpgradePriority => {
    if (state === UPGRADE_STATE_EQUIPED) {
      return 1
    } else if (state === UPGRADE_STATE_FREE) {
      return 3
    } else if (state === UPGRADE_STATE_EQUIPED_ON_OTHER) {
      return 2
    } else if (state === UPGRADE_STATE_DISABLED) {
      return 4
    }

    return 5
  }
)

export const getComponentUpgrades = createSelector(
  [data => data],
  ({
    itemUniqueId,
    itemWeptype,
    upgrades
  }: {
    itemUniqueId: TItemId
    itemWeptype: string
    upgrades: TUpgrades
  }): TDetailsUpgrades => {
    const componentUpgrades = upgrades.map(upgrade => {
      const state = getUpgradeState({
        compatibility: upgrade.compatibility,
        attachedToItemId: upgrade.attachedToItemId,
        itemWeptype,
        itemUniqueId,
        attached: upgrade.attached,
        armoryList: upgrade.armouryList
      })

      const attachedToArmouryName = getAttachedToArmouryName({
        armoryId: upgrade.attachedToItemId,
        armouryList: upgrade.armouryList
      })
      const hintTitle = getHintTitle({
        state,
        attachedToArmouryName
      })

      return {
        ...upgrade,
        hintTitle,
        state
      }
    })

    return componentUpgrades.sort((first, second) => {
      const firstStatePriority = getUpgradeStatePriority(first.state)
      const secondStatePriority = getUpgradeStatePriority(second.state)

      const statePrioritiesDifference = firstStatePriority - secondStatePriority

      if (statePrioritiesDifference !== 0) {
        return statePrioritiesDifference
      }

      return resolvePriority(first.cost, second.cost) + resolvePriority(first.value, second.value)
    })
  }
)
