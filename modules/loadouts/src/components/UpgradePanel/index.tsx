/* eslint-disable react/jsx-handler-names,consistent-return */
import React, { useEffect, useState, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { IProps } from './types'
import styles from './index.cssmodule.scss'
import { getUpgradesRequestAction, detachUpgradeRequestAction } from '../../modules/actions'
import UpgradePanelItem from '../UpgradePanelItem'
import { getComponentUpgrades } from './selectors'
import { UPGRADE_STATE_DISABLED } from '../../constants'
import { UPGRADES_LIST_MAX } from './constants'
import { useArrowNavigation } from '../../hooks'
import { getIsDescsLoaded, getIsUpgradesLoaded, getItemInReview, getUpgrades } from '../../modules/selectors'
import { useOnAttach, useWai } from './hooks'

const UpgradePanel = (props: IProps) => {
  const { itemId, itemUpgradeIndex, setFirstRef, setLastRef, onKeyDown: onKeyDownProp, onLastItemTabKeyDown } = props
  const dispatch = useDispatch()

  const [activeIndex, setActiveIndex] = useState(null)

  const itemInReview = useSelector(getItemInReview)
  const upgrades = useSelector(getUpgrades)
  const detailedUpgrades = getComponentUpgrades({
    itemUniqueId: itemInReview.uniqueId,
    itemWeptype: itemInReview.weptype,
    upgrades
  })
  const isDescsLoaded = useSelector(getIsDescsLoaded)
  const isUpgradesLoaded = useSelector(getIsUpgradesLoaded)

  const activeUpgrade = detailedUpgrades[activeIndex]

  useEffect(() => {
    dispatch(getUpgradesRequestAction())
  }, [dispatch])

  const onItemHover = useCallback(index => setActiveIndex(index), [])
  const onAttach = useOnAttach(itemId, detailedUpgrades)
  const onDetach = useCallback(upgradeId => dispatch(detachUpgradeRequestAction({ itemId, upgradeId })), [
    dispatch,
    itemId
  ])

  const upgradesLength = detailedUpgrades.length
  const maxItemIndex = upgradesLength < UPGRADES_LIST_MAX ? upgradesLength - 1 : UPGRADES_LIST_MAX - 1
  const upgradeIsActive = typeof activeIndex === 'number'

  const wai = useWai({
    setFirstRef,
    setLastRef,
    maxItemIndex,
    upgradeIsActive,
    onLastItemTabKeyDown
  })
  const arrowNavigation = useArrowNavigation({ lenght: UPGRADES_LIST_MAX })
  const setItemRefByIndex = useCallback(
    (index: number) => ref => {
      wai.getItemRefSetterByIndex(index)(ref)
      arrowNavigation.setItemRef(index, ref)
    },
    [wai, arrowNavigation]
  )

  const detailedUpgradesListView = detailedUpgrades.slice(0, UPGRADES_LIST_MAX).map((upgrade, index) => {
    const onKeyDown = index === maxItemIndex && upgradeIsActive ? onLastItemTabKeyDown : undefined

    return (
      <UpgradePanelItem
        {...upgrade}
        setRef={setItemRefByIndex(index)}
        onFocus={arrowNavigation.onItemFocus}
        onKeyDown={onKeyDown}
        itemUpgradeIndex={itemUpgradeIndex}
        key={upgrade.id}
        onHover={onItemHover}
        index={index}
        onActivate={onAttach}
        onDeactivate={onDetach}
      />
    )
  })

  const abbilityTitle = () => {
    if (!isDescsLoaded || !isUpgradesLoaded) return

    if (activeUpgrade) {
      return activeUpgrade.abbilityTitle
    } else if (detailedUpgrades.length > UPGRADES_LIST_MAX) {
      return (
        <>
          Select Weapon Attachment{' '}
          <a ref={setLastRef} className={styles.more} href='/loader.php?sid=itemsMods'>
            ({detailedUpgrades.length} more)
          </a>
        </>
      )
    }

    if (detailedUpgrades.length === 0) {
      return 'No Weapon Attachment'
    }

    return 'Select Weapon Attachment'
  }

  return (
    <div role='none' className={styles.upgradePanel} onKeyDown={onKeyDownProp}>
      <div role='none' className={styles.list} onKeyDown={arrowNavigation.onContainerKeyDown}>
        {detailedUpgradesListView}
      </div>
      <div className={styles.info}>
        <p>{abbilityTitle()}</p>
        <p className={cn(activeUpgrade && activeUpgrade.state === UPGRADE_STATE_DISABLED ? styles.redDesc : '')}>
          {activeUpgrade ? activeUpgrade.hintTitle : ''}
        </p>
      </div>
    </div>
  )
}

export default UpgradePanel
