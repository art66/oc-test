import { TUpgrade } from '../../modules/interfaces'
import { TUpgradeState } from '../../types'
import { TSetFirstRef, TsetDeactivatorRef } from '../../utils/types'

export interface IProps {
  itemId: string
  itemUpgradeIndex: number
  setFirstRef: TSetFirstRef
  setLastRef: TsetDeactivatorRef
  onKeyDown: (event: React.KeyboardEvent<HTMLDivElement>) => any
  onLastItemTabKeyDown: (event: React.KeyboardEvent<HTMLButtonElement>) => any
}

export type TUpgradePriority = number

export type TUpgradeHintTitle = string

export interface IDetailsUpgrade extends TUpgrade {
  hintTitle: string
  state: TUpgradeState
  // attachedToArmouryName: string
  // replaceId: string
}

export type TDetailsUpgrades = IDetailsUpgrade[]
