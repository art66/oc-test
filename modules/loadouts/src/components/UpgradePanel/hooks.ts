import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { attachUpgradeRequestAction } from '../../modules/actions'
import { getEquipedItemById } from '../../modules/selectors'
import { TDetailsUpgrades } from './types'

export const useWai = (props) => {
  const { setFirstRef, setLastRef, maxItemIndex, upgradeIsActive } = props

  const getItemRefSetterByIndex = useCallback(
    (index) => {
      const refSetters = []

      if (index === 0) {
        refSetters.push(setFirstRef)
      }

      if (index === maxItemIndex && upgradeIsActive) {
        refSetters.push(setLastRef)
      }

      return (ref) => refSetters.forEach((refSetter) => refSetter(ref))
    },
    [setFirstRef, setLastRef, maxItemIndex, upgradeIsActive]
  )

  return {
    getItemRefSetterByIndex
  }
}

export function useOnAttach(itemId: string, upgrades: TDetailsUpgrades) {
  const dispatch = useDispatch()

  const targetItem = useSelector(getEquipedItemById)(itemId)

  const onAttach = useCallback(
    (upgradeId) => {
      const attachItemId = itemId

      const destinyItemUpgrade = upgrades.find(
        (upgrade) => upgradeId === upgrade.id && upgrade.attached && upgrade.attachedToItemId !== itemId
      )
      const destinyAttachedToItemId = destinyItemUpgrade ? destinyItemUpgrade.attachedToItemId : null

      let detachItemId

      const attachUpgradeId = upgradeId

      let detachUpgradeId
      const targetUpgrade = upgrades.find((upgrade) => upgrade.id === upgradeId)
      const targetUpgradeIsDual = targetUpgrade ? targetUpgrade.isDual : null
      const attachedTargetItemUpgrades = upgrades.filter(
        (upgrade) => upgrade.attached && upgrade.attachedToItemId === attachItemId
      )
      const attachedTargetItemUpgradesLength = attachedTargetItemUpgrades.length
      const lastAttachedTargetItemUpgrade = attachedTargetItemUpgrades[attachedTargetItemUpgradesLength - 1]
      const similarTypeAttachedUpgrades = attachedTargetItemUpgrades.filter(
        (upgrade) => upgrade.type === targetUpgrade.type
      )
      const similarTypeAttachedUpgradesLength = similarTypeAttachedUpgrades.length
      const targetItemIsDual = targetItem.isDual

      let extraDetachItemId
      let extraDetachUpgradeId

      if (destinyAttachedToItemId) {
        detachItemId = destinyAttachedToItemId
        detachUpgradeId = destinyItemUpgrade.id

        if (attachedTargetItemUpgradesLength === 2) {
          extraDetachItemId = itemId
          extraDetachUpgradeId = lastAttachedTargetItemUpgrade.id
        }
      } else if (attachedTargetItemUpgradesLength < 2) {
        if (similarTypeAttachedUpgradesLength === 1) {
          if (targetItemIsDual && similarTypeAttachedUpgrades[0].isDual && targetUpgradeIsDual) {
          } else {
            detachItemId = similarTypeAttachedUpgrades[0].attachedToItemId
            detachUpgradeId = similarTypeAttachedUpgrades[0].id
          }
        }
      } else if (attachedTargetItemUpgradesLength === 2) {
        if (similarTypeAttachedUpgradesLength === 1) {
          if (targetItemIsDual && similarTypeAttachedUpgrades[0].isDual && targetUpgradeIsDual) {
            detachItemId = lastAttachedTargetItemUpgrade.attachedToItemId
            detachUpgradeId = lastAttachedTargetItemUpgrade.id
          } else {
            detachItemId = similarTypeAttachedUpgrades[0].attachedToItemId
            detachUpgradeId = similarTypeAttachedUpgrades[0].id
          }
        } else if (similarTypeAttachedUpgradesLength === 2) {
          detachItemId = similarTypeAttachedUpgrades[1].attachedToItemId
          detachUpgradeId = similarTypeAttachedUpgrades[1].id
        } else {
          detachItemId = lastAttachedTargetItemUpgrade.attachedToItemId
          detachUpgradeId = lastAttachedTargetItemUpgrade.id
        }
      }

      dispatch(
        attachUpgradeRequestAction({
          attachItemId,
          attachUpgradeId,
          detachItemId,
          detachUpgradeId,
          extraDetachItemId,
          extraDetachUpgradeId
        })
      )
    },
    [itemId, upgrades]
  )

  return onAttach
}
