import React, { memo, useCallback, useMemo } from 'react'
import cn from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import getItemGlowClassName from '@torn/shared/utils/getItemGlowClassName'
import styles from './index.cssmodule.scss'
import { setItemInReviewAction } from '../../modules/actions'
import { getItemInReviewIndex } from '../../modules/selectors'
import ItemTooltip from '../ItemTooltip'
import { IProps } from './types'

export default memo((props: IProps) => {
  const { className, uniqueId, index, type, imgMediumSrc, name, rarity, setRef, onFocus } = props

  const dispatch = useDispatch()

  const activate = useCallback(() => dispatch(setItemInReviewAction(index)), [dispatch, index])
  const setIndexedRef = useCallback(ref => setRef(index, ref), [index, setRef])
  const onFocusHandler = useCallback(() => onFocus(index), [index, onFocus])

  const activeIndex = useSelector(getItemInReviewIndex)

  const imgId = `currentSlotItemId_${uniqueId}`
  const active = activeIndex === index
  const classNames = cn(styles.item, { [styles.active]: active }, className)
  const ariaLabel = useMemo(() => {
    if (!active) {
      return `Select "${name}" item`
    }

    return `Item "${name}" selected`
  }, [active, name])

  return (
    <button
      ref={setIndexedRef}
      type='button'
      className={classNames}
      onClick={activate}
      aria-label={ariaLabel}
      onFocus={onFocusHandler}
    >
      <div className={styles.arrow} />
      <img id={imgId} className={cn(styles.img, getItemGlowClassName(rarity))} src={imgMediumSrc} alt={name} />
      <ItemTooltip id={imgId} name={name} type={type} />
    </button>
  )
})
