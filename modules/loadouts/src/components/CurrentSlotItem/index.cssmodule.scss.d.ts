// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'arrow': string;
  'globalSvgShadow': string;
  'img': string;
  'item': string;
  'tooltipName': string;
  'tooltipType': string;
}
export const cssExports: CssExports;
export default cssExports;
