import { IItem } from '../../modules/interfaces'

export interface IProps extends IItem {
  className?: string
  index: number
  setRef?: (index: number, ref: Element) => void
  onFocus?: (index: number) => void
}
