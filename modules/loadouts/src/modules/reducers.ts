import { createReducer, PayloadAction } from '@reduxjs/toolkit'
import {
  TCommonState,
  TItemUniqueIndex,
  TSlotId,
  TUpgrades,
  TLoadouts,
  TUpgradesDescs,
  TItemUpgrades,
  IItem
} from './interfaces'
import {
  checkDarkModeAction,
  setItemInReviewAction,
  getEquippedItemsRequestAction,
  getEquippedItemsSuccessAction,
  getEquippedItemsFailureAction,
  equipSlotRequestAction,
  equipSlotSuccessAction,
  equipSlotFailureAction,
  resetSlotRequestAction,
  resetSlotSuccessAction,
  resetSlotFailureAction,
  getUpgradesRequestAction,
  getUpgradesSuccessAction,
  getUpgradesFailureAction,
  setSlotNameAction,
  setItemUpgradesAction,
  switchAmmoRequestAction,
  switchAmmoSuccessAction,
  switchAmmoFailureAction,
  setLoadoutsAction,
  getUpgradesDescsSuccessAction,
  equipSlotAction,
  setUpgradesAction,
  setInfoBoxErrorAction,
  clearInfoBoxAction
} from './actions'
import { BLANK_ITEMS, INFO_BOX_ERROR_COLOR } from './constants'
import localStorageManager from '../utils/localStorageManager'

export const commonInitialState: TCommonState = {
  isDesktopLayoutSetted: null,
  isDarkMode: null,
  infoBox: {
    loading: false,
    msg: null,
    color: null,
    log: null
  },
  loadouts: {
    equippedItems: [...BLANK_ITEMS],
    equipedModelItems: [],
    slots: [],
    settings: { currentSlotId: null, slotsMax: null, gender: '' },
    isLoading: true,
    error: false,
    isEquipedItemsLoading: false,
    equipedItemsError: false,
    isResetSlotLoading: true,
    resetSlotError: false,
    isSwitchAmmoLoading: false,
    switchAmmoError: false
  },
  upgrades: {
    list: [],
    descs: {},
    isLoaded: false,
    isDescsLoaded: false,
    isLoading: false,
    error: false
  },
  itemInReviewIndex: localStorageManager.hasKey({ storeKey: 'itemInReviewIndex' }) ?
    localStorageManager.getStore({ storeKey: 'itemInReviewIndex' }) :
    localStorageManager.addStore({ storeKey: 'itemInReviewIndex', storeData: 0 })
}

const ACTION_HANDLERS = {
  [setInfoBoxErrorAction.type]: (state: TCommonState, action: PayloadAction<string>) => {
    state.infoBox.msg = action.payload
    state.infoBox.color = INFO_BOX_ERROR_COLOR
  },
  [clearInfoBoxAction.type]: (state: TCommonState) => {
    state.infoBox = {
      loading: false,
      msg: null,
      color: null,
      log: null
    }
  },
  [setLoadoutsAction.type]: (state: TCommonState, action: PayloadAction<TLoadouts>) => {
    const { equippedItems, equipedModelItems, slots, settings } = action.payload

    state.loadouts.equippedItems = equippedItems
    state.loadouts.equipedModelItems = equipedModelItems
    state.loadouts.slots = slots
    state.loadouts.settings = settings
  },
  [checkDarkModeAction.type]: (state: TCommonState, action: PayloadAction<boolean>) => {
    const isDarkMode = action.payload

    state.isDarkMode = isDarkMode
  },
  [getEquippedItemsRequestAction.type]: (state: TCommonState) => {
    state.loadouts.isLoading = true
    state.loadouts.error = false
  },
  [getEquippedItemsSuccessAction.type]: (state: TCommonState) => {
    state.loadouts.isLoading = false
  },
  [getEquippedItemsFailureAction.type]: (state: TCommonState) => {
    state.loadouts.isLoading = false
    state.loadouts.error = true
  },
  [setItemInReviewAction.type]: (state: TCommonState, action: PayloadAction<TItemUniqueIndex>) => {
    const index = action.payload

    state.itemInReviewIndex = index

    localStorageManager.addStore({ storeKey: 'itemInReviewIndex', storeData: state.itemInReviewIndex })
  },
  [equipSlotAction.type]: (state: TCommonState, action: PayloadAction<TSlotId>) => {
    const slotId = action.payload

    state.loadouts.settings.currentSlotId = slotId
  },
  [equipSlotRequestAction.type]: (state: TCommonState) => {
    state.loadouts.isEquipedItemsLoading = true
    state.loadouts.equipedItemsError = false
  },
  [equipSlotSuccessAction.type]: (state: TCommonState) => {
    state.loadouts.isEquipedItemsLoading = false
  },
  [equipSlotFailureAction.type]: (state: TCommonState) => {
    state.loadouts.isEquipedItemsLoading = false
    state.loadouts.equipedItemsError = false
  },
  [resetSlotRequestAction.type]: (state: TCommonState) => {
    state.loadouts.isResetSlotLoading = true
    state.loadouts.resetSlotError = false
  },
  [resetSlotSuccessAction.type]: (state: TCommonState) => {
    state.loadouts.isResetSlotLoading = false
  },
  [resetSlotFailureAction.type]: (state: TCommonState) => {
    state.loadouts.isResetSlotLoading = false
    state.loadouts.resetSlotError = true
  },
  [getUpgradesRequestAction.type]: (state: TCommonState) => {
    state.upgrades.isLoading = true
    state.upgrades.error = false
  },
  [getUpgradesSuccessAction.type]: (state: TCommonState) => {
    state.upgrades.isLoaded = true
    state.upgrades.isLoading = false
  },
  [setUpgradesAction.type]: (state: TCommonState, action: PayloadAction<TUpgrades>) => {
    const upgrades = action.payload

    state.upgrades.list = upgrades
  },
  [getUpgradesFailureAction.type]: (state: TCommonState) => {
    state.upgrades.isLoading = false
    state.upgrades.error = true
  },
  // [getUpgradesDescsRequestAction.type]: (state: TCommonState) => {},
  [getUpgradesDescsSuccessAction.type]: (state: TCommonState, action: PayloadAction<TUpgradesDescs>) => {
    const descs = action.payload

    state.upgrades.descs = descs
    state.upgrades.isDescsLoaded = true
  },
  // [getUpgradesDescsFailureAction.type]: (state: TCommonState) => {},
  [setSlotNameAction.type]: (state: TCommonState, action: PayloadAction<{ slotId: string; nextTitle: string }>) => {
    const { slotId, nextTitle } = action.payload

    state.loadouts.slots.forEach(slot => {
      if (slot.slotId === slotId) {
        slot.title = nextTitle
      }
    })
  },
  [setItemUpgradesAction.type]: (
    state: TCommonState,
    action: PayloadAction<{ itemId: string; itemUpgrades: TItemUpgrades }>
  ) => {
    const { itemId, itemUpgrades } = action.payload

    const itemIndex = state.loadouts.equippedItems.findIndex(equipedItem => equipedItem.uniqueId === itemId)

    state.loadouts.equippedItems[itemIndex].upgrades = itemUpgrades
  },
  [switchAmmoRequestAction.type]: (state: TCommonState) => {
    state.loadouts.isSwitchAmmoLoading = true
    state.loadouts.switchAmmoError = false
  },
  [switchAmmoSuccessAction.type]: (
    state: TCommonState,
    action: PayloadAction<{
      armoryId: string
      currentAmmoType?: IItem['currentAmmoType']
      clips?: IItem['clips']
    }>
  ) => {
    const { armoryId, currentAmmoType, clips } = action.payload

    let ammoType

    state.loadouts.equippedItems = state.loadouts.equippedItems.map(item => {
      if (item.armoryId === armoryId) {
        ammoType = item.ammoType

        return Object.assign(item, { currentAmmoType, clips })
      }

      return item
    })
    state.loadouts.equippedItems = state.loadouts.equippedItems.map(item => {
      if (item.ammoType === ammoType) {
        return Object.assign(item, { currentAmmoType })
      }

      return item
    })

    state.loadouts.isSwitchAmmoLoading = false
  },
  [switchAmmoFailureAction.type]: (state: TCommonState) => {
    state.loadouts.isSwitchAmmoLoading = false
    state.loadouts.switchAmmoError = true
  }
}

export default createReducer(commonInitialState, ACTION_HANDLERS)
