import BLANK_ITEMS from './blankItems'

export const PRIMARY_SLOT = '1'
export const SECONDARY_SLOT = '2'
export const MELEE_SLOT = '3'
export const TEMPORARY_SLOT = '5'

export const ITEMS_ORDER = [PRIMARY_SLOT, SECONDARY_SLOT, MELEE_SLOT, TEMPORARY_SLOT, '6', '4', '7', '8', '9']

export { BLANK_ITEMS }

export const INFO_BOX_ERROR_COLOR = 'red'
