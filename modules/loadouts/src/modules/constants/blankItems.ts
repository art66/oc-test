import { TItems } from '../interfaces'
import {
  PRIMARY_ITEM_TYPE,
  SECONDARY_ITEM_TYPE,
  MELEE_ITEM_TYPE,
  TEMPORARY_ITEM_TYPE,
  DEFENSIVE_ITEM_TYPE
} from '../../constants'

export default [
  {
    slotId: '1',
    uniqueId: 'blank-1',
    type: PRIMARY_ITEM_TYPE,
    imgSmallSrc: '/images/items/primarys.png',
    imgMediumSrc: '/images/items/primarym.png',
    imgLargeSrc: '/images/items/primary.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '2',
    uniqueId: 'blank-2',
    type: SECONDARY_ITEM_TYPE,
    imgSmallSrc: '/images/items/secondarys.png',
    imgMediumSrc: '/images/items/secondarym.png',
    imgLargeSrc: '/images/items/secondary.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '3',
    uniqueId: 'blank-3',
    type: MELEE_ITEM_TYPE,
    imgSmallSrc: '/images/items/melees.png',
    imgMediumSrc: '/images/items/meleem.png',
    imgLargeSrc: '/images/items/melee.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '5',
    uniqueId: 'blank-4',
    type: TEMPORARY_ITEM_TYPE,
    imgSmallSrc: '/images/items/temporarys.png',
    imgMediumSrc: '/images/items/temporarym.png',
    imgLargeSrc: '/images/items/temporary.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '6',
    uniqueId: 'blank-6',
    type: DEFENSIVE_ITEM_TYPE,
    imgSmallSrc: '/images/items/heads.png',
    imgMediumSrc: '/images/items/headm.png',
    imgLargeSrc: '/images/items/head.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '4',
    uniqueId: 'blank-5',
    type: DEFENSIVE_ITEM_TYPE,
    imgSmallSrc: '/images/items/bodys.png',
    imgMediumSrc: '/images/items/bodym.png',
    imgLargeSrc: '/images/items/body.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '7',
    uniqueId: 'blank-7',
    type: DEFENSIVE_ITEM_TYPE,
    imgSmallSrc: '/images/items/legss.png',
    imgMediumSrc: '/images/items/legsm.png',
    imgLargeSrc: '/images/items/legs.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '8',
    uniqueId: 'blank-8',
    type: DEFENSIVE_ITEM_TYPE,
    imgSmallSrc: '/images/items/feets.png',
    imgMediumSrc: '/images/items/feetm.png',
    imgLargeSrc: '/images/items/feet.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  },
  {
    slotId: '9',
    uniqueId: 'blank-9',
    type: DEFENSIVE_ITEM_TYPE,
    imgSmallSrc: '/images/items/handss.png',
    imgMediumSrc: '/images/items/handsm.png',
    imgLargeSrc: '/images/items/hands.png',
    name: 'Empty',
    ammoType: null,
    bonuses: [
      {
        icon: 'bonus-attachment-emasculated',
        title: 'Emasculate',
        hoverover: '<b>Emasculate</b><br/>Happiness increased by 15% of maximum upon final hit'
      },
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      }
    ],
    upgrades: [
      {
        id: 'blank-bonus-1',
        icon: 'blank-bonus-25'
      },
      {
        id: 'blank-bonus-2',
        icon: 'blank-bonus-25'
      }
    ],
    damage: null,
    accuracy: null,
    weptype: '4',
    armoryId: '8',
    exists: true,
    isBlank: true
  }
] as TItems
