import { put, select, take } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { USER_UPGRADES_URL, ATTACH_UPGRADE_URL, DETACH_UPGRADE_URL } from './constants'
import { getNormalizedUpgrades } from './selectors'
import {
  attachUpgradeFailureAction,
  attachUpgradeSuccessAction,
  getUpgradesFailureAction,
  getUpgradesSuccessAction,
  getUpgradesDescsSuccessAction,
  setUpgradesAction,
  detachUpgradeSuccessAction,
  detachUpgradeFailureAction,
  setInfoBoxErrorAction,
  clearInfoBoxAction
} from '../actions'
import { getIsDescsLoaded } from '../selectors'
import { upgradesMutation } from './mutations'

export default function* getUpgradesSaga() {
  const isDescsLoaded = yield select(getIsDescsLoaded)

  if (!isDescsLoaded) {
    yield take(getUpgradesDescsSuccessAction.type)
  }

  try {
    const response = yield fetchUrl(USER_UPGRADES_URL, null, null)

    if (response.error) {
      throw new Error(response.error)
    }

    const upgrades = getNormalizedUpgrades(response)

    yield put(setUpgradesAction(upgrades))

    yield put(getUpgradesSuccessAction())
    yield put(clearInfoBoxAction())
  } catch (error) {
    console.error(error)

    yield put(getUpgradesFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}

export function* attachUpgradeSaga(action) {
  const {
    attachItemId,
    detachItemId,
    attachUpgradeId,
    detachUpgradeId,
    extraDetachItemId,
    extraDetachUpgradeId
  } = action.payload

  const undoResolveAttachUpgradeSaga = yield upgradesMutation({
    attachItemId,
    attachUpgradeId,
    detachItemId,
    detachUpgradeId,
    extraDetachItemId,
    extraDetachUpgradeId
  })

  try {
    const response = yield fetchUrl(
      ATTACH_UPGRADE_URL({
        itemId: attachItemId,
        attachUpgradeId,
        detachUpgradeId: attachUpgradeId !== detachUpgradeId ? detachUpgradeId : ''
      }),
      null,
      null
    )

    if (response.error) {
      throw new Error(response.error)
    }

    const upgrades = getNormalizedUpgrades(response.result.userUpgrades)

    yield put(setUpgradesAction(upgrades))

    yield put(attachUpgradeSuccessAction())
    yield put(clearInfoBoxAction())
  } catch (error) {
    yield undoResolveAttachUpgradeSaga()

    console.error(error)

    yield put(attachUpgradeFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}

export function* detachUpgradeSaga(action) {
  const { itemId, upgradeId } = action.payload

  const undoUpgradeMutationSaga = yield upgradesMutation({
    detachItemId: itemId,
    detachUpgradeId: upgradeId
  })

  try {
    const response = yield fetchUrl(DETACH_UPGRADE_URL({ itemId, upgradeId }), null, null)

    if (response.error) {
      throw new Error(response.error)
    }

    const upgrades = getNormalizedUpgrades(response.result.userUpgrades)

    yield put(setUpgradesAction(upgrades))

    yield put(detachUpgradeSuccessAction())
    yield put(clearInfoBoxAction())
  } catch (error) {
    yield undoUpgradeMutationSaga()

    console.error(error)

    yield put(detachUpgradeFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}
