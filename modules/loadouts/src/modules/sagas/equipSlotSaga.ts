import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  equipSlotAction,
  equipSlotFailureAction,
  equipSlotSuccessAction,
  setInfoBoxErrorAction,
  clearInfoBoxAction
} from '../actions'
import { EQUIP_SLOT_URL } from './constants'
import { getNormalizedLoadouts } from './selectors'
import setLoadoutsSaga from './setLoadoutsSaga'
import { getCurrentSlotId } from '../selectors'

export default function* equipSlotSaga(action) {
  const slotId = action.payload

  const prevSlotId = yield select(getCurrentSlotId)

  yield put(equipSlotAction(slotId))

  try {
    const response = yield fetchUrl(EQUIP_SLOT_URL(slotId), null, null)

    if (response.error) {
      throw new Error(response.error)
    }

    const loadoutsData = getNormalizedLoadouts(response)

    yield setLoadoutsSaga(loadoutsData)

    yield put(equipSlotSuccessAction())
    yield put(clearInfoBoxAction())
  } catch (error) {
    console.error(error)

    yield put(equipSlotAction(prevSlotId))
    yield put(equipSlotFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}
