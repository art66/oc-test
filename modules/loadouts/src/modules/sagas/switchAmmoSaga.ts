import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { switchAmmoFailureAction, switchAmmoSuccessAction, setInfoBoxErrorAction, clearInfoBoxAction } from '../actions'
import { SWITCH_AMMO_URL } from './constants'

export default function* switchAmmoSaga(action) {
  const armoryId = action.payload

  try {
    const response = yield fetchUrl(SWITCH_AMMO_URL(armoryId), null, null)

    if (response.error) {
      throw response.error
    }

    const { clips, currentAmmoType } = response

    yield put(switchAmmoSuccessAction({ armoryId, clips, currentAmmoType }))
    yield put(clearInfoBoxAction())
  } catch (error) {
    console.error(error)

    yield put(switchAmmoFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}
