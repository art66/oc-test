import { put, select } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import {
  setLoadoutTitleSuccessAction,
  setLoadoutTitleFailureAction,
  setSlotNameAction,
  setInfoBoxErrorAction,
  clearInfoBoxAction
} from '../actions'
import { SET_LOADOUT_TITLE_URL } from './constants'
import { getCurrentSlotId, getSlotName } from '../selectors'

export default function* setLoadoutTitleSaga(action) {
  const { slotId, nextTitle } = action.payload

  const currentSlotId = yield select(getCurrentSlotId)
  const slotName = yield select(getSlotName)

  yield put(setSlotNameAction({ slotId, nextTitle }))

  try {
    const response = yield fetchUrl(SET_LOADOUT_TITLE_URL(slotId, encodeURIComponent(nextTitle)), null, null)

    if (response.error) {
      throw response.error
    }

    if (response.title !== nextTitle) {
      yield put(setSlotNameAction({ slotId, nextTitle: response.title }))
    }

    yield put(setLoadoutTitleSuccessAction())
    yield put(clearInfoBoxAction())
  } catch (error) {
    console.error(error)

    yield put(setLoadoutTitleFailureAction())

    if (currentSlotId === slotId) {
      yield put(setSlotNameAction(slotName))
    }
    yield put(setInfoBoxErrorAction(error.message))
  }
}
