import { createSelector } from 'reselect'
import fromPairs from 'lodash.frompairs'
import { IArmourRaw, TArmourRawItem } from '@torn/shared/components/UserModel/interfaces'
import { objectForEach, objectAutoMap, objectFind } from '../../utils'
import {
  TItemUniqueId,
  TItems,
  TLoadouts,
  TSlots,
  TBonus,
  IItem,
  TBonuses,
  TUpgrades,
  TItemUpgrades,
  TSlotsMax,
  TSlot,
  TUpgradesDescs,
  TItemUpgrade,
  TUpgrade,
  TUpgradeDesc
} from '../interfaces'
import { ITEMS_ORDER } from '../constants'
import BLANK_ITEMS from '../constants/blankItems'
import {
  IPureItems,
  IPureItem,
  TPureLoadouts,
  TPureSlots,
  TPureUpgradeDescs,
  TPureUpgrades,
  TPureUpgradeDesc,
  TPureUpgrade
} from './types'
import store from '../../store/createStore'
import { getDescs } from '../selectors'
import { TState } from '../../store/types'

const BONUSES_BLANK_LIST = [0, 1]

export const getNormalizedItems = createSelector(
  [items => items],
  (pureItems: IPureItems): TItems => {
    const items = objectAutoMap(
      pureItems,
      (item: IPureItem, slotId: TItemUniqueId): IItem => {
        const bonuses = BONUSES_BLANK_LIST.map(
          (_, index): TBonus => {
            if (item.currentBonuses && item.currentBonuses[index]) {
              const currentBonus = item.currentBonuses[index]

              return {
                id: currentBonus.bonusID,
                icon: currentBonus.icon,
                title: currentBonus.title,
                desc: currentBonus.desc,
                hoverover: currentBonus.hoverover
              }
            }

            return {
              id: `blank-bonus-${index}`,
              icon: 'blank-bonus-25'
            }
          }
        ) as TBonuses

        const upgrades = BONUSES_BLANK_LIST.map(
          (_, index): TItemUpgrade => {
            if (item.currentUpgrades && item.currentUpgrades[index]) {
              const currentUpgrade = item.currentUpgrades[index]

              return {
                id: currentUpgrade.upgradeID,
                icon: currentUpgrade.icon,
                title: currentUpgrade.title,
                desc: currentUpgrade.desc,
                hoverover: currentUpgrade.hoverover
              }
            }

            return null
          }
        ) as TItemUpgrades

        const type = item.type2

        return {
          uniqueId: item.uniqueId,
          name: item.name,
          type,
          imgMediumSrc: `/images/items/${item.itemID}/medium.png`,
          imgSmallSrc: `/images/items/${item.itemID}/small.png`,
          imgLargeSrc: `/images/items/${item.itemID}/large.png`,
          slotId,
          damage: item.dmg,
          bonuses,
          upgrades,
          armor: item.arm,
          accuracy: item.accuracy,
          weptype: item.weptype,
          armoryId: item.armoryID,
          exists: item.exists,
          expPercent: item.expPercent,
          ammoType: item.ammotype,
          currentAmmoType: item.currentAmmoType,
          clips: item.clips,
          isDual: item.isDual === '1',
          rarity: item.rarity
        }
      }
    ) as TItems

    const equippedItems = []
    const itemsCopy = { ...items }

    ITEMS_ORDER.forEach(slotId => {
      const item = items[slotId]

      if (item) {
        delete itemsCopy[slotId]

        return equippedItems.push(item)
      }

      const blankListItemIndex = BLANK_ITEMS.findIndex((blankItem: IItem) => blankItem.slotId === slotId)
      const BLANK_ITEM = BLANK_ITEMS[blankListItemIndex]

      return equippedItems.push({ ...BLANK_ITEM })
    })

    objectForEach(itemsCopy, item => {
      equippedItems.push(item)
    })

    return equippedItems
  }
)

export const getNormalizedSlotItems = createSelector(
  [(data: any) => data],
  (data): TItems => {
    const { pureItems, isCurrentSlot, currentItems } = data

    if (isCurrentSlot) {
      return currentItems.map(currentItem => ({
        ...currentItem,
        exists: true
      }))
    }

    if (!pureItems) {
      return [...BLANK_ITEMS]
    }

    return getNormalizedItems(pureItems)
  }
)

export const getNormalizedSlots = createSelector(
  [(data: any) => data],
  (data: { pureSlots: TPureSlots; slotsMax: TSlotsMax; currentSlotId: string; currentItems: TItems }): TSlots => {
    const { pureSlots, slotsMax = 3, currentSlotId, currentItems } = data

    const slots = Array.from({ length: slotsMax }).map(
      (_, index): TSlot => {
        const blankSlotId = (index + 1).toString()
        const pureSlot = pureSlots[blankSlotId]
        const isBlank = !pureSlot

        if (!pureSlot) {
          const pureSlotItems = getNormalizedSlotItems({
            isCurrentSlot: blankSlotId === currentSlotId,
            isBlank,
            currentItems
          })

          return {
            slotId: blankSlotId,
            title: `Loadout #${blankSlotId}`,
            items: pureSlotItems,
            blank: true
          }
        }

        const slotId = pureSlot.setID.toString()
        const title = pureSlot.title || `Loadout #${slotId}`
        const blank = false

        const items = getNormalizedSlotItems({
          pureItems: pureSlot.items,
          isCurrentSlot: slotId === currentSlotId,
          isBlank,
          currentItems
        })

        return {
          slotId,
          title,
          items,
          blank
        }
      }
    )

    return slots
  }
)

export const normalizeModelItems = createSelector(
  [data => data],
  (data): IArmourRaw => {
    const { pureEquippedItems, pureHairs } = data as {
      pureEquippedItems: TPureLoadouts['currentItems']
      pureHairs: TPureLoadouts['currentHairs']
    }

    const normalizedItems = fromPairs(
      Object.entries(pureEquippedItems).map(([key, pureEquippedItem]): [string, TArmourRawItem] => {
        return [
          key,
          {
            item: [pureEquippedItem]
          }
        ]
      })
    )

    const normalizedHeircuts = fromPairs(
      Object.entries(pureHairs).map(([key, pureHair]): [string, TArmourRawItem] => {
        return [
          key,
          {
            item: [pureHair]
          }
        ]
      })
    )

    return {
      ...normalizedItems,
      ...normalizedHeircuts
    }
  }
)

export const getNormalizedLoadouts = createSelector(
  [data => data],
  (loadouts: TPureLoadouts): TLoadouts => {
    const {
      currentItems: pureEquippedItems,
      currentHairs: pureHairs,
      currentLoadouts: pureSlots,
      currentSettings: pureSettings,
      gender
    } = loadouts

    const equippedItems = getNormalizedItems(pureEquippedItems)
    const currentSlotId = pureSettings.currentSetID.toString()
    const slotsMax = pureSettings.setsMax
    const slots = getNormalizedSlots({ pureSlots, slotsMax, currentItems: equippedItems, currentSlotId })
    const currentSlot = slots.find(slot => currentSlotId === slot.slotId)
    const slotName = currentSlot ? currentSlot.title : ''
    const settings = {
      currentSlotId,
      slotsMax: pureSettings.setsMax,
      gender,
      slotName
    }
    const equipedModelItems = normalizeModelItems({ pureEquippedItems, pureHairs })

    return {
      equippedItems,
      equipedModelItems,
      slots,
      settings
    }
  }
)

export const findUpgradesDesc = createSelector(
  [(data: any) => data.id, (data: any) => data.descs],
  (id: TPureUpgradeDesc['ID'], descs: TPureUpgradeDescs): TPureUpgradeDesc => {
    return objectFind(descs, desc => id === desc.ID) as TPureUpgradeDesc
  }
)

export const getNormalizedUpgradesDescs = createSelector(
  [(data: any) => data],
  (pureDescs: TPureUpgradeDescs): TUpgradesDescs => {
    const descs = fromPairs(
      pureDescs.map(pureDesc => {
        const {
          ID: id,
          title,
          cssClass,
          description,
          effectDesc,
          compatibility,
          icon,
          type,
          category,
          dualFit
        } = pureDesc

        return [
          id,
          { id, title, cssClass, description, effectDesc, compatibility, icon, type, category, isDual: dualFit === '1' }
        ]
      })
    )

    return descs
  }
)

export const getNormalizedUpgrade = createSelector(
  [(data: any) => data],
  ({ pureUpgrade, desc }: { pureUpgrade: TPureUpgrade; desc: TUpgradeDesc }): TUpgrade => {
    const { armouryID, armoury: armouryList, attached, cost, value } = pureUpgrade
    const { id, cssClass, description, effectDesc, compatibility, type, isDual } = desc

    return {
      id,
      imgClassName: `mod-icon-small-${cssClass}`,
      abbilityTitle: `${description} ${effectDesc}`,
      compatibility,
      type,
      attachedToItemId: armouryID,
      attached: attached === 'true',
      armouryList,
      isDual,
      cost,
      value
    }
  }
)

export const getNormalizedUpgrades = createSelector(
  [(data: any) => data],
  (pureUpgrades: TPureUpgrades): TUpgrades => {
    const state = store.getState() as TState
    const descs = getDescs(state)

    const upgrades: TUpgrades = pureUpgrades.map(pureUpgrade => {
      return getNormalizedUpgrade({ pureUpgrade, desc: descs[pureUpgrade.upgradeID] })
    })

    return upgrades
  }
)
