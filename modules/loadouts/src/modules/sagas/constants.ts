export const LOADOUTS_URL = '/page.php?sid=itemsLoadouts&step=getEquippedItems'

export const SET_LOADOUT_TITLE_URL = (slotId, title) =>
  `/page.php?sid=itemsLoadouts&step=changeLoadoutTitle&setID=${slotId}&title=${title}`

export const EQUIP_SLOT_URL = (slotId) => `/page.php?sid=itemsLoadouts&step=changeLoadout&setID=${slotId}`

export const USER_UPGRADES_URL = 'loader.php?sid=itemsMods&mode=json&step=getUserUpgrades'
export const ALL_UPGRADES_URL = 'loader.php?sid=itemsMods&mode=json&step=getAllUpgrades'

export const ATTACH_UPGRADE_URL = ({
  itemId,
  detachUpgradeId = '',
  attachUpgradeId
}: {
  itemId: string
  attachUpgradeId: string
  detachUpgradeId?: string
}) =>
  'loader.php?sid=itemsMods&mode=json&step=reattach' +
  `&armouryID=${itemId}&upgradeID=${attachUpgradeId}&replaceUpgradeID=${detachUpgradeId}`

export const DETACH_UPGRADE_URL = ({ itemId, upgradeId }: { itemId: string; upgradeId: string }) =>
  `loader.php?sid=itemsMods&mode=json&step=detach&armouryID=${itemId}&upgradeID=${upgradeId}`

export const RESET_SLOT_URL = (id) => `page.php?sid=itemsLoadouts&step=clearLoadout&setID=${id}`
export const SWITCH_AMMO_URL = (id) => `page.php?sid=itemsLoadouts&step=changeAmmoType&armouryID=${id}`
