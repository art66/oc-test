import { TGender } from '@torn/shared/components/UserModel/interfaces'
import { TItemUniqueId, TItemType, TAmmoType, TExp, TSlotId, TSlotsMax } from '../interfaces'
import { TRarity } from '@torn/shared/utils/getItemGlowClassName/interfaces'

export type TPureBonus = {
  bonusID: string
  icon: string
  title: string
  desc: string
  hoverover: string
}

export type IPureItemUpgrade = {
  upgradeID: string
  icon: string
  title: string
  desc: string
  hoverover: string
}

export type TClips = {
  currentAmount: number
  maxAmmo: number
  maxClips: number
  amount: number
}

export type TPureBonuses = [TPureBonus?, TPureBonus?]
export type IPureItemUpgrades = [IPureItemUpgrade?, IPureItemUpgrade?]

export type TPureAmmo = { ammotype?: string; currentAmmoType?: TAmmoType; clips?: TClips }

export type TNormalizedAmmo = {
  ammoType?: string
  currentAmmoType?: TAmmoType
  clips?: TClips
}

export interface IPureItem extends TPureAmmo {
  uniqueId: TItemUniqueId
  type2: TItemType
  armoryID: string
  itemID: string
  accuracy: string
  dmg: string
  ammo: string
  currentBonuses: TPureBonuses
  currentUpgrades: IPureItemUpgrades
  expPercent?: number
  name: string
  weptype: string
  exists: boolean
  rarity?: TRarity
  exp?: TExp

  armouryID: string
  userID: string
  type: string
  equiped: string
  ID: string
  acc: number
  equipSlot: string
  maxammo: string
  ammotype: string
  rofmin: string
  rofmax: string
  scary: string
  isDual: string
  isMask: string
  stealthLevel: number
  slotsMap: string
  changedSlotsMap: string
  slotsMapMask: string
  slotsMapMaskHair: string
  upgrades: string
  armourNotCover: string
  slotsMapBackground: string
  roffixed: number
  quality: number
  arm: string
}

export type IPureItems = {
  [id: string]: IPureItem
}

export type TPureSlot = {
  setID: number
  title: string
  items: IPureItems
}

export type TPureSlots = {
  [id: string]: TPureSlot
}

export type TPureSettings = { currentSetID: TSlotId; setsMax: TSlotsMax }

export type TPureHaircut = {
  itemID: string
  ID: string
  equiped: number
  type: string
  type2: string
  slotsMap: string
}

export type TPureHaircuts = {
  [id: string]: TPureHaircut
}

export type TPureLoadouts = {
  currentItems: IPureItems
  currentLoadouts: TPureSlots
  currentSettings: TPureSettings
  gender: TGender
  currentHairs: TPureHaircuts
}

export type TPureUpgradeDesc = {
  ID: string
  cssClass: string
  description: string
  effectDesc: string
  compatibility: string
  icon: string
  title: string
  type: string
  category: string
  dualFit: string
}

export type TPureUpgradeDescs = TPureUpgradeDesc[]

export type TPureUpgradeArmory = {
  armouryID: string
  type: string
}

export type TPureUpgradeArmoryList = TPureUpgradeArmory[]

export type TPureUpgrade = {
  upgradeID: string
  armouryID: string
  attached: string
  armoury: TPureUpgradeArmoryList
  cost: number
  value: number
}

export type TPureUpgrades = TPureUpgrade[]
