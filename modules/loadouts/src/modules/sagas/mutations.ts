import { put, select } from 'redux-saga/effects'
import { setUpgradesAction, setItemUpgradesAction } from '../actions'
import { createItemUpgrade, getItemUpgrades, getUpgrades } from '../selectors'
import { TItemUpgrades, TUpgrades } from '../interfaces'

function sortItemUpgrades(itemUpgrades) {
  return itemUpgrades.sort((prevItemUpgrade, nextItemUpgrade) => {
    if (!prevItemUpgrade && nextItemUpgrade) {
      return 1
    } else if (prevItemUpgrade && !nextItemUpgrade) {
      return -1
    }

    return 0
  })
}

export function* attachUpgradeMutationSaga({ itemId, upgradeId }) {
  const upgrades: TUpgrades = yield select(getUpgrades)

  const nextUpgrades = upgrades.map(upgrade => {
    if (upgrade.id === upgradeId) {
      return {
        ...upgrade,
        attached: true,
        attachedToItemId: itemId
      }
    }

    return upgrade
  })

  yield put(setUpgradesAction(nextUpgrades))

  return function* undo() {
    yield put(setUpgradesAction(upgrades))
  }
}

export function* attachItemUpgradesMutationSaga({ itemId, upgradeId, replaceUpgradeId }) {
  const targetItemUpgrades: TItemUpgrades = (yield select(getItemUpgrades))(itemId)

  let nextTargetItemUpgrades: TItemUpgrades = [...targetItemUpgrades]
  const nextTargetItemUpgrade = (yield select(createItemUpgrade))(upgradeId)

  const nextTargetItemUpgradesBlankItemIndex = nextTargetItemUpgrades.findIndex(itemUpgrade => !itemUpgrade)

  if (replaceUpgradeId) {
    const nextTargetItemUpgradesDestinyItemIndex = nextTargetItemUpgrades.findIndex(
      itemUpgrade => itemUpgrade.id === replaceUpgradeId
    )

    if (nextTargetItemUpgradesDestinyItemIndex > 0) {
      nextTargetItemUpgrades.splice(nextTargetItemUpgradesDestinyItemIndex, 1, nextTargetItemUpgrade)
    }
  } else if (nextTargetItemUpgradesBlankItemIndex >= 0) {
    nextTargetItemUpgrades.splice(nextTargetItemUpgradesBlankItemIndex, 1, nextTargetItemUpgrade)
  }

  nextTargetItemUpgrades = sortItemUpgrades(nextTargetItemUpgrades)

  yield put(setItemUpgradesAction({ itemId, itemUpgrades: nextTargetItemUpgrades }))

  return function* undo() {
    yield put(setItemUpgradesAction({ itemId, itemUpgrades: targetItemUpgrades }))
  }
}

export function* detachUpgradeMutationSaga(upgradeId) {
  const upgrades: TUpgrades = yield select(getUpgrades)

  const nextUpgrades = upgrades.map(upgrade => {
    if (upgrade.id === upgradeId) {
      return {
        ...upgrade,
        attached: false,
        attachedToItemId: null
      }
    }

    return upgrade
  })

  yield put(setUpgradesAction(nextUpgrades))

  return function* undo() {
    yield put(setUpgradesAction(upgrades))
  }
}

export function* detachItemUpgradesMutationSaga({ itemId, upgradeId }) {
  const itemUpgrades: TItemUpgrades = (yield select(getItemUpgrades))(itemId)

  let nextItemUpgrades: TItemUpgrades = [...itemUpgrades]
  const nextItemUpgradesItemIndex = nextItemUpgrades.findIndex(nextItemUpgrade => nextItemUpgrade.id === upgradeId)
  const nextDestinyBlankItemUpgrade = null

  nextItemUpgrades.splice(nextItemUpgradesItemIndex, 1, nextDestinyBlankItemUpgrade)

  nextItemUpgrades = sortItemUpgrades(nextItemUpgrades)

  yield put(setItemUpgradesAction({ itemId, itemUpgrades: nextItemUpgrades }))

  return function* undo() {
    yield put(setItemUpgradesAction({ itemId, itemUpgrades: itemUpgrades }))
  }
}

export function* upgradesMutation({
  attachItemId,
  attachUpgradeId,
  detachItemId,
  detachUpgradeId,
  extraDetachItemId,
  extraDetachUpgradeId
}: {
  attachItemId?: string
  attachUpgradeId?: string
  detachItemId?: string
  detachUpgradeId?: string
  extraDetachItemId?: string
  extraDetachUpgradeId?: string
}) {
  let undoDetachUpgradeMutationSaga
  let undoDetachItemUpgradesMutationSaga

  if (detachUpgradeId) {
    undoDetachUpgradeMutationSaga = yield detachUpgradeMutationSaga(detachUpgradeId)

    undoDetachItemUpgradesMutationSaga = yield detachItemUpgradesMutationSaga({
      itemId: detachItemId,
      upgradeId: detachUpgradeId
    })
  }

  let undoExtraDetachUpgradeMutationSaga

  if (extraDetachItemId) {
    undoExtraDetachUpgradeMutationSaga = yield detachUpgradeMutationSaga(extraDetachUpgradeId)
  }

  let undoAttachUpgradeMutationSaga
  let undoAttachItemUpgradesMutationSaga

  if (attachItemId && attachUpgradeId) {
    undoAttachUpgradeMutationSaga = yield attachUpgradeMutationSaga({
      itemId: attachItemId,
      upgradeId: attachUpgradeId
    })

    undoAttachItemUpgradesMutationSaga = yield attachItemUpgradesMutationSaga({
      itemId: attachItemId,
      upgradeId: attachUpgradeId,
      replaceUpgradeId: extraDetachUpgradeId
    })
  }

  return function* undo() {
    undoAttachUpgradeMutationSaga && (yield undoAttachUpgradeMutationSaga())
    undoExtraDetachUpgradeMutationSaga && (yield undoExtraDetachUpgradeMutationSaga())
    undoDetachUpgradeMutationSaga && (yield undoDetachUpgradeMutationSaga())

    undoAttachItemUpgradesMutationSaga && (yield undoAttachItemUpgradesMutationSaga())
    // undoExtraItemUpgradesMutationSaga && (yield undoExtraItemUpgradesMutationSaga())
    undoDetachItemUpgradesMutationSaga && (yield undoDetachItemUpgradesMutationSaga())
  }
}
