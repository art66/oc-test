import { select, put } from 'redux-saga/effects'
import { setItemInReviewAction, setLoadoutsAction } from '../actions'
import { getItemInReviewIndex } from '../selectors'

export default function* setLoadoutsSaga(loadoutsData) {
  const itemInReviewIndex = yield select(getItemInReviewIndex)

  if (loadoutsData.equippedItems.length - 1 < itemInReviewIndex) {
    yield put(setItemInReviewAction(loadoutsData.equippedItems.length - 1))
  }

  yield put(setLoadoutsAction(loadoutsData))
}
