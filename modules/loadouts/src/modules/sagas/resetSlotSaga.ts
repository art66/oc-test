import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { resetSlotFailureAction, resetSlotSuccessAction, setInfoBoxErrorAction, clearInfoBoxAction } from '../actions'
import { RESET_SLOT_URL } from './constants'
import { getNormalizedLoadouts } from './selectors'
import setLoadoutsSaga from './setLoadoutsSaga'

export default function* resetSlotSaga(action) {
  try {
    const slotId = action.payload

    const response = yield fetchUrl(RESET_SLOT_URL(slotId), null, null)

    if (response.error) {
      throw new Error(response.error)
    }

    const loadoutsData = getNormalizedLoadouts(response)

    yield setLoadoutsSaga(loadoutsData)

    yield put(resetSlotSuccessAction())
    yield put(clearInfoBoxAction())
  } catch (error) {
    console.error(error)

    yield put(resetSlotFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}
