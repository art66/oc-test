import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { getEquippedItemsSuccessAction, getEquippedItemsFailureAction, setInfoBoxErrorAction } from '../actions'
import { LOADOUTS_URL } from './constants'
import { getNormalizedLoadouts } from './selectors'
import setLoadoutsSaga from './setLoadoutsSaga'

export default function* getLoadoutsRequestSaga() {
  try {
    const response = yield fetchUrl(LOADOUTS_URL, null, null)

    if (response.error) {
      throw new Error(response.error)
    }

    const loadoutsData = getNormalizedLoadouts(response)

    yield setLoadoutsSaga(loadoutsData)

    yield put(getEquippedItemsSuccessAction())
  } catch (error) {
    console.error(error)

    yield put(getEquippedItemsFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}
