import { put } from 'redux-saga/effects'
import { fetchUrl } from '@torn/shared/utils'
import { ALL_UPGRADES_URL } from './constants'
import {
  getUpgradesDescsFailureAction,
  getUpgradesDescsSuccessAction,
  setInfoBoxErrorAction,
  clearInfoBoxAction
} from '../actions'
import { getNormalizedUpgradesDescs } from './selectors'

export default function* getUpgradesDescsSaga() {
  try {
    const response = yield fetchUrl(ALL_UPGRADES_URL, null, null)

    if (response.error) {
      throw new Error(response.error)
    }

    const descs = getNormalizedUpgradesDescs(response)

    yield put(getUpgradesDescsSuccessAction(descs))
    yield put(clearInfoBoxAction())
  } catch (error) {
    console.error(error)

    yield put(getUpgradesDescsFailureAction())
    yield put(setInfoBoxErrorAction(error.message))
  }
}
