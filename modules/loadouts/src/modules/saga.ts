import { takeLatest } from 'redux-saga/effects'
import getLoadoutsRequestSaga from './sagas/getLoadoutsRequestSaga'
import setLoadoutTitleSaga from './sagas/setLoadoutTitleSaga'
import equipSlotSaga from './sagas/equipSlotSaga'
import getUpgradesSaga, { attachUpgradeSaga, detachUpgradeSaga } from './sagas/getUpgradesSaga'
import resetSlotSaga from './sagas/resetSlotSaga'
import {
  attachUpgradeRequestAction,
  detachUpgradeRequestAction,
  equipSlotRequestAction,
  getEquippedItemsSuccessAction,
  getEquippedItemsRequestAction,
  getUpgradesDescsRequestAction,
  getUpgradesRequestAction,
  resetSlotRequestAction,
  setLoadoutTitleRequestAction,
  switchAmmoRequestAction
} from './actions'
import switchAmmoSaga from './sagas/switchAmmoSaga'
import getUpgradesDescsSaga from './sagas/getUpgradesDescsSaga'

export default function* rootSaga() {
  yield takeLatest(getEquippedItemsRequestAction, getLoadoutsRequestSaga)
  yield takeLatest(setLoadoutTitleRequestAction, setLoadoutTitleSaga)
  yield takeLatest(equipSlotRequestAction, equipSlotSaga)
  yield takeLatest(getUpgradesDescsRequestAction, getUpgradesDescsSaga)
  yield takeLatest([getUpgradesRequestAction, getEquippedItemsSuccessAction], getUpgradesSaga)
  yield takeLatest(attachUpgradeRequestAction, attachUpgradeSaga)
  yield takeLatest(detachUpgradeRequestAction, detachUpgradeSaga)
  yield takeLatest(resetSlotRequestAction, resetSlotSaga)
  yield takeLatest(switchAmmoRequestAction, switchAmmoSaga)
}
