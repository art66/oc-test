import { createAction } from '@reduxjs/toolkit'
import { TNormalizedAmmo } from './sagas/types'
import { getItemInReviewIndex, getMaxItemIndex } from './selectors'
import { TItemUpgrades, TLoadouts, TUpgrades, TUpgradesDescs } from './interfaces'

export const checkDarkModeAction = createAction<boolean>('CHECK_DARK_MODE')

export const setInfoBoxErrorAction = createAction<string>('SET_INFO_BOX_ERROR')
export const clearInfoBoxAction = createAction<string>('CLEAR_INFO_BOX')

export const setLoadoutsAction = createAction<TLoadouts>('SET_CURRENT_ITEMS')

export const getEquippedItemsRequestAction = createAction('GET_EQUIPPED_ITEMS_REQUEST')
export const getEquippedItemsSuccessAction = createAction<TLoadouts>('GET_EQUIPPED_ITEMS_SUCCESS')
export const getEquippedItemsFailureAction = createAction('GET_EQUIPPED_ITEMS_FAILURE')

export const setEquippedItemsAction = createAction<TLoadouts>('SET_EQUIPPED_ITEMS')

export const setItemInReviewAction = createAction<number>('SET_ITEM_IN_REVIEW')

export const slideItemInReviewAction = side => (dispatch, getState) => {
  const state = getState()

  const maxItemIndex = getMaxItemIndex(state)
  const currentItemInReviewIndex = getItemInReviewIndex(state)

  let nextItemInReviewIndex

  if (!side) {
    if (currentItemInReviewIndex !== 0) {
      nextItemInReviewIndex = currentItemInReviewIndex - 1
    } else {
      nextItemInReviewIndex = maxItemIndex - 1
    }
  } else if (currentItemInReviewIndex !== maxItemIndex - 1) {
    nextItemInReviewIndex = currentItemInReviewIndex + 1
  } else {
    nextItemInReviewIndex = 0
  }

  dispatch(setItemInReviewAction(nextItemInReviewIndex))
}

export const setLoadoutTitleRequestAction = createAction<{ slotId: string; nextTitle: string }>(
  'SET_LOADOUT_TITLE_REQUEST'
)
export const setLoadoutTitleSuccessAction = createAction('SET_LOADOUT_TITLE_SUCCESS')
export const setLoadoutTitleFailureAction = createAction('SET_LOADOUT_TITLE_FAILURE')

export const equipSlotAction = createAction<string>('EQUIP_SLOT')

export const equipSlotRequestAction = createAction<string>('EQUIP_SLOT_REQUEST')
export const equipSlotSuccessAction = createAction<TLoadouts>('EQUIP_SLOT_SUCCESS')
export const equipSlotFailureAction = createAction('EQUIP_SLOT_FAILURE')

export const getUpgradesDescsRequestAction = createAction('GET_UPGRADES_DESCS_REQUEST')
export const getUpgradesDescsSuccessAction = createAction<TUpgradesDescs>('GET_UPGRADES_DESCS_SUCCESS')
export const getUpgradesDescsFailureAction = createAction('GET_UPGRADES_DESCS_FAILURE')

export const setUpgradesAction = createAction<TUpgrades>('SET_UPGRADES')

export const getUpgradesRequestAction = createAction('GET_UPGRADES_REQUEST')
export const getUpgradesSuccessAction = createAction('GET_UPGRADES_SUCCESS')
export const getUpgradesFailureAction = createAction('GET_UPGRADES_FAILURE')

export const activateUpgradeAction = createAction<{ index: number; id: string }>('ATTACH_UPGRADE_REQUEST')

export const attachUpgradeRequestAction = createAction<{
  attachItemId: string
  attachUpgradeId: string
  detachItemId?: string
  detachUpgradeId?: string
  extraDetachItemId?: string
  extraDetachUpgradeId?: string
}>('ATTACH_UPGRADE_REQUEST')
export const attachUpgradeSuccessAction = createAction('ATTACH_UPGRADE_SUCCESS')
export const attachUpgradeFailureAction = createAction('ATTACH_UPGRADE_FAILURE')

export const detachUpgradeRequestAction = createAction<{ itemId: string; upgradeId: string }>('DETACH_UPGRADE_REQUEST')
export const detachUpgradeSuccessAction = createAction('DETACH_UPGRADE_SUCCESS')
export const detachUpgradeFailureAction = createAction('DETACH_UPGRADE_FAILURE')

// export const reattachUpgradeRequestAction = createAction<{ index: number; upgradeId: string }>(
//   'REATTACH_UPGRADE_REQUEST'
// )
// export const reattachUpgradeSuccessAction = createAction('REATTACH_UPGRADE_SUCCESS')
// export const reattachUpgradeFailureAction = createAction('REATTACH_UPGRADE_FAILURE')

// export const replaceUpgradeRequestAction = createAction<{ index: number; upgradeId: string; replaceId: string }>(
//   'REPLACE_UPGRADE_REQUEST'
// )
// export const replaceUpgradeSuccessAction = createAction('REPLACE_UPGRADE_SUCCESS')
// export const replaceUpgradeFailureAction = createAction('REPLACE_UPGRADE_FAILURE')

export const setItemUpgradesAction = createAction<{ itemId: string; itemUpgrades: TItemUpgrades }>('SET_ITEM_UPGRADES')

export const resetSlotRequestAction = createAction<string>('RESET_SLOT_REQUEST')
export const resetSlotSuccessAction = createAction<TLoadouts>('RESET_SLOT_SUCCESS')
export const resetSlotFailureAction = createAction('RESET_SLOT_FAILURE')

export const setSlotNameAction = createAction<{ slotId: string; nextTitle: string }>('SET_SLOT_NAME')

export const switchAmmoRequestAction = createAction<string>('SWITCH_AMMO_REQUEST')
export const switchAmmoSuccessAction = createAction<TNormalizedAmmo>('SWITCH_AMMO_SUCCESS')
export const switchAmmoFailureAction = createAction('SWITCH_AMMO_FAILURE')
