export const createBlankUpgrade = index => {
  return {
    id: `blank-upgrade-${index}`,
    icon: 'blank-bonus-25',
    isBlank: true,
    disabled: true
  }
}

export const createAddUpgrade = index => {
  return {
    id: `blank-upgrade-${index}`,
    icon: 'add-mod',
    isBlank: true
  }
}
