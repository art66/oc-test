import { IArmourRaw } from '@torn/shared/components/UserModel/interfaces'
import {
  PRIMARY_ITEM_TYPE,
  SECONDARY_ITEM_TYPE,
  MELEE_ITEM_TYPE,
  TEMPORARY_ITEM_TYPE,
  DEFENSIVE_ITEM_TYPE,
  AMMO_TYPE_HOLLOW_POINT,
  AMMO_TYPE_STANDARD,
  AMMO_TYPE_TRACER,
  AMMO_TYPE_PIERCING,
  AMMO_TYPE_INCENDIARY
} from '../constants'
import {
  TPureBonus,
  IPureItem,
  TPureSlot,
  TPureUpgradeDesc,
  TPureLoadouts,
  TPureAmmo,
  // IPureItemUpgrade,
  TPureUpgrade
} from './sagas/types'

export type TIsLoading = boolean
export type TLoaded = boolean
export type TError = boolean
export type TSlotId = string
export type TSlotsMax = number

export type TItemType =
  | typeof PRIMARY_ITEM_TYPE
  | typeof SECONDARY_ITEM_TYPE
  | typeof MELEE_ITEM_TYPE
  | typeof TEMPORARY_ITEM_TYPE
  | typeof DEFENSIVE_ITEM_TYPE

export type TAmmoType =
  | typeof AMMO_TYPE_HOLLOW_POINT
  | typeof AMMO_TYPE_STANDARD
  | typeof AMMO_TYPE_TRACER
  | typeof AMMO_TYPE_PIERCING
  | typeof AMMO_TYPE_INCENDIARY

export type TExp = {
  value: number
  max: number
}

export interface IInfoBox {
  loading?: boolean
  msg?: string
  color?: 'red' | 'green'
  log?: string[]
}

export type TBonus = {
  id: string
  icon: TPureBonus['icon']
  title?: TPureBonus['title']
  desc?: TPureBonus['desc']
  hoverover?: TPureBonus['hoverover']
}

export type TItemUpgrade = {
  id: string
  icon: TPureBonus['icon']
  title?: TPureBonus['title']
  desc?: TPureBonus['desc']
  hoverover?: TPureBonus['hoverover']
  isBlank?: boolean
}

export type TBonuses = [TBonus?, TBonus?]
export type TItemUpgrades = [TItemUpgrade?, TItemUpgrade?]

export type TItemUniqueId = string
export type TItemId = string

export interface IItem {
  uniqueId: IPureItem['uniqueId']
  type: TItemType
  imgSmallSrc: string
  imgMediumSrc: string
  imgLargeSrc: string
  slotId: string
  damage?: IPureItem['dmg']
  accuracy?: IPureItem['accuracy']
  armor?: string
  bonuses: TBonuses
  upgrades: TItemUpgrades
  expPercent?: IPureItem['expPercent']
  name: IPureItem['name']
  weptype: IPureItem['weptype']
  armoryId: TItemId
  exists: IPureItem['exists']
  isDual: boolean
  rarity?: IPureItem['rarity']
  isBlank?: boolean
  ammoType?: string
  currentAmmoType?: TPureAmmo['currentAmmoType']
  clips?: TPureAmmo['clips']
}

export type TItems = IItem[]

export type TItemUniqueIndex = number

export type TSlot = {
  slotId: TSlotId
  title: TPureSlot['title']
  items: TItems
  blank: boolean
}

export type TSlots = TSlot[]

export type TSettings = {
  currentSlotId?: TSlotId
  slotsMax?: TSlotsMax
  gender: TPureLoadouts['gender']
}

export type TLoadouts = {
  equippedItems: TItems
  equipedModelItems: IArmourRaw
  slots: TSlots
  settings: TSettings
}

export type TUpgrade = {
  id: TPureUpgradeDesc['ID']
  imgClassName: string
  abbilityTitle: string
  compatibility: TPureUpgradeDesc['compatibility']
  type: TPureUpgradeDesc['type']
  attachedToItemId: TPureUpgrade['armouryID']
  attached: boolean
  armouryList: TPureUpgrade['armoury']
  isDual: boolean
  cost: TPureUpgrade['cost']
  value: TPureUpgrade['value']
}

export type TUpgrades = TUpgrade[]

export type TUpgradeDesc = {
  id: TPureUpgradeDesc['ID']
  title: TPureUpgradeDesc['title']
  cssClass: TPureUpgradeDesc['cssClass']
  description: TPureUpgradeDesc['description']
  effectDesc: TPureUpgradeDesc['effectDesc']
  compatibility: TPureUpgradeDesc['compatibility']
  icon: TPureUpgradeDesc['icon']
  type: TPureUpgradeDesc['type']
  isDual: boolean
  // category: TPureUpgradeDesc['category']
}

export type TUpgradesDescs = {
  [id: string]: TUpgradeDesc
}

export type TCommonState = {
  isDesktopLayoutSetted?: boolean
  isDarkMode?: boolean
  infoBox?: IInfoBox
  loadouts: {
    equippedItems: TLoadouts['equippedItems']
    equipedModelItems: TLoadouts['equipedModelItems']
    slots: TLoadouts['slots']
    settings: TLoadouts['settings']
    isLoading: TIsLoading
    error: TError
    isEquipedItemsLoading: TIsLoading
    equipedItemsError: TError
    isResetSlotLoading: TIsLoading
    resetSlotError: TError
    // isSavingTitle: TIsLoading
    // savingTitleError: TError
    isSwitchAmmoLoading: TIsLoading
    switchAmmoError: TError
  }
  itemInReviewIndex: TItemUniqueIndex
  upgrades: {
    list: TUpgrades
    descs: TUpgradesDescs
    isLoaded: TLoaded
    isDescsLoaded: TLoaded
    isLoading: TIsLoading
    error: TError
  }
}
