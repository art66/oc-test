import { createSelector } from 'reselect'
import { TGender } from '@torn/shared/components/UserModel/interfaces'
import { TMediaType, TState } from '../store/types'
import {
  IItem,
  TItemUniqueIndex,
  TItems,
  TSlots,
  TSlotId,
  TUpgrades,
  TItemId,
  TLoadouts,
  TIsLoading,
  TUpgradesDescs,
  TItemUpgrade,
  TItemUpgrades,
  TSettings,
  TItemType,
  TItemUniqueId
} from './interfaces'
import {
  PRIMARY_ITEM_TYPE,
  SECONDARY_ITEM_TYPE,
  TEMPORARY_ITEM_TYPE,
  MELEE_ITEM_TYPE,
  DEFENSIVE_ITEM_TYPE,
  CLOTHING_ITEM_TYPE,
  DESKTOP_TYPE,
  TABLET_TYPE,
  MOBILE_TYPE
} from '../constants'

export const getMediaType = (state: TState): TMediaType => state.browser.mediaType
export const isDarkModeSelector = (state: TState) => state.common.isDarkMode
export const infoBoxSelector = (state: TState) => state.common.infoBox
export const getIsDesktop = (state: TState): boolean => state.browser.mediaType === DESKTOP_TYPE
export const getIsTablet = (state: TState): boolean => state.browser.mediaType === TABLET_TYPE
export const getIsMobile = (state: TState): boolean => state.browser.mediaType === MOBILE_TYPE

export const getMainContainerWidth = createSelector([getMediaType], (mediaType: string) => {
  if (mediaType === 'tablet') {
    return 386
  }

  if (mediaType === 'mobile') {
    return 320
  }

  return 784
})

export const getEquipedItemsList = (state: TState) => state.common.loadouts.equippedItems
export const getEquipedItemById = createSelector([getEquipedItemsList], items => id =>
  items.find(item => item.uniqueId === id))
export const getEquipedModelItems = (state: TState): TLoadouts['equipedModelItems'] => {
  return state.common.loadouts.equipedModelItems
}
export const getSlots = (state: TState): TSlots => state.common.loadouts.slots
export const getItemInReviewIndex = (state: TState): TItemUniqueIndex => state.common.itemInReviewIndex
export const getItemInReview = createSelector(
  [getEquipedItemsList, getItemInReviewIndex],
  (equippedItems: TItems, itemInReviewIndex: TItemUniqueIndex): IItem => {
    return equippedItems[itemInReviewIndex]
  }
)
export const getItemInReviewMods = createSelector<TState, IItem, TItemUpgrades>([getItemInReview], item => {
  return item.upgrades
})
export const getItemInReviewUniqueId = createSelector(
  [getItemInReview],
  (itemInReview: IItem): TItemId => itemInReview.uniqueId
)
export const getItemInReviewUpgrades = createSelector([getItemInReview], (item: IItem): TItemUpgrades => item.upgrades)
export const getCurrentSlotId = (state: TState): TSlotId => state.common.loadouts.settings.currentSlotId
export const getUpgrades = (state: TState): TUpgrades => state.common.upgrades.list
export const upgradesLoadedSelector = (state: TState) => state.common.upgrades.isLoaded
export const upgradesIsLoadingSelector = (state: TState) => state.common.upgrades.isLoading
export const getDescs = (state: TState): TUpgradesDescs => state.common.upgrades.descs
export const getGender = (state: TState): TGender => state.common.loadouts.settings.gender
export const getIsLoading = (state: TState): TIsLoading => state.common.loadouts.isLoading
export const getMaxSlots = (state: TState): TSettings['slotsMax'] => state.common.loadouts.settings.slotsMax
export const getIsEquipedItemsLoading = (state: TState): TIsLoading => state.common.loadouts.isEquipedItemsLoading
export const getIsResetSlotLoading = (state: TState): TIsLoading => state.common.loadouts.isResetSlotLoading
export const getIsSwitchAmmoLoading = (state: TState): TIsLoading => state.common.loadouts.isSwitchAmmoLoading

export const getItemUpgrades = createSelector(
  [getEquipedItemsList],
  itemsList => (itemId: TItemUniqueId): TItemUpgrades => {
    const item = itemsList.find(seekingItem => seekingItem.uniqueId === itemId)

    return item?.upgrades
  }
)

export const getIsDescsLoaded = (state: TState): TIsLoading => state.common.upgrades.isDescsLoaded
export const getIsUpgradesLoaded = (state: TState): TIsLoading => state.common.upgrades.isLoaded

export const getItemInReviewType = createSelector([getItemInReview], (item): TItemType => item.type)

export const getSlotName = createSelector([getSlots, getCurrentSlotId], (slots, currentSlotId): string => {
  const currentSlot = slots.find(slot => currentSlotId === slot.slotId)

  return currentSlot ? currentSlot.title : ''
})

export const getEquipedItemsListLength = createSelector(
  [getEquipedItemsList],
  (equippedItems: TItems): number => equippedItems.length
)

export const getMaxItemIndex = createSelector(
  [getEquipedItemsListLength, getIsDesktop],
  (length: number, isDesktop: boolean): number => {
    if (isDesktop) {
      return length > 18 ? 17 : length
    }

    return length > 20 ? 19 : length
  }
)

export const getAllItemsLength = createSelector(
  [getMaxItemIndex, getEquipedItemsListLength],
  (maxItemIndex: number, itemsLength: number): number => {
    return itemsLength <= maxItemIndex ? maxItemIndex : maxItemIndex + 1
  }
)

export const getIsPrimary = createSelector([type => type], (type): boolean => type === PRIMARY_ITEM_TYPE)
export const getIsSeconary = createSelector([type => type], (type): boolean => type === SECONDARY_ITEM_TYPE)
export const getIsMelee = createSelector([type => type], (type): boolean => type === MELEE_ITEM_TYPE)
export const getIsTemporary = createSelector([type => type], (type): boolean => type === TEMPORARY_ITEM_TYPE)
export const getIsDefencive = createSelector([type => type], (type): boolean => type === DEFENSIVE_ITEM_TYPE)
export const getIsClothing = createSelector([type => type], (type): boolean => type === CLOTHING_ITEM_TYPE)

export const getIsFirearms = createSelector(
  [getIsPrimary, getIsSeconary],
  (isPrimary, isSeconary): boolean => isPrimary || isSeconary
)

export const getIsPermanentWeapon = createSelector(
  [getIsFirearms, getIsMelee],
  (isFirearms, isMelee): boolean => isFirearms || isMelee
)

export const getIsWeapon = createSelector(
  [getIsPermanentWeapon, getIsTemporary],
  (isPermanentWeapon, isTemporary): boolean => isPermanentWeapon || isTemporary
)

export const getPrimary = createSelector(
  [items => items],
  (items: TItems): IItem => items.find(item => item.type === PRIMARY_ITEM_TYPE)
)
export const getSeconary = createSelector(
  [items => items],
  (items: TItems): IItem => items.find(item => item.type === SECONDARY_ITEM_TYPE)
)

export const getFirearms = createSelector(
  [getEquipedItemsList],
  (items: TItems): TItems => {
    const primaryItem = getPrimary(items)
    const seconaryItem = getSeconary(items)

    const firearms = []

    if (primaryItem) {
      firearms.push(primaryItem)
    }

    if (seconaryItem) {
      firearms.push(seconaryItem)
    }

    return firearms
  }
)

export const getFirearmsExcludeGetter = createSelector([getFirearms], (items: TItems) => (uniqueId: string): TItems => {
  const result = items.filter(item => item.uniqueId !== uniqueId)

  return result
})

export const getFirearmsExcludeItemInReview = createSelector(
  [getFirearms, getItemInReviewUniqueId],
  (items: TItems, itemInReviewUniqueId: string): TItems => {
    const result = items.filter(item => item.uniqueId !== itemInReviewUniqueId)

    return result
  }
)

export const createItemUpgrade = createSelector([getUpgrades, getDescs], (_upgrades, descs) => (id): TItemUpgrade => {
  const { icon, title, effectDesc } = descs[id]

  return {
    id,
    icon,
    title,
    hoverover: `<b>${title}</b><br />${effectDesc}`
  }
})

// export const createUpgrade = createSelector([getUpgrades, getDescs], (upgrades, descs) => (id): TItemUpgrade => {
//   const upgrade: TItemUpgrade = upgrades.find((upgrade) => id === upgrade.id)
//   const { icon, title, effectDesc } = descs[id]

//   return {
//     id,
//     imgClassName: `mod-icon-small-${cssClass}`,
//     abbilityTitle: `${description} ${effectDesc}`,
//     compatibility,
//     type,
//     attachedToItemId: armouryID,
//     attachedToArmouryName: getAttachedToArmouryName({ armoryId: armouryID, armouryList }),
//     attached: attached === 'true'
//   }

//   return {
//     id,
//     icon,
//     title,
//     hoverover: `<b>${title}</b><br />${effectDesc}`
//   }
// })
