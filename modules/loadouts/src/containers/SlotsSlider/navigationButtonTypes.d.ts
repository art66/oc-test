export type TProps = {
  index: number
  active: boolean
  setRef?: (index: number, ref: Element) => void
  onFocus?: (index: number) => void
  onActivate: (index) => any
}
