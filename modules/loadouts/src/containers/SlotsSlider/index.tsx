import React, { memo, useCallback, useMemo, useState } from 'react'
import cn from 'classnames'
import { useSelector } from 'react-redux'
import styles from './index.cssmodule.scss'
import { getMainContainerWidth, getMaxSlots, getSlots } from '../../modules/selectors'
import Slot from '../../components/Slot'
import NavigationButton from './NavigationButton'
import { useArrowNavigation } from '../../hooks'
import { useHorisontalSwipe } from '../../hooks/events'

export default memo(function SlotsSlider() {
  const [activeItemIndex, setActiveItemIndex] = useState(0)
  const slots = useSelector(getSlots)
  const maxSlots = useSelector(getMaxSlots)

  const arrowNavigation = useArrowNavigation({ lenght: maxSlots })

  const slotsView = useMemo(() => {
    return slots.map((slot, index) => <Slot key={slot.slotId} index={index} className={styles.slot} {...slot} />)
  }, [slots])
  // const slotsView = useMemo(() => {
  //   const slot = slots.find((_slot, index) => activeItemIndex === index)

  //   if (!slot) return null

  //   return <Slot key={slot.slotId} className={styles.slot} {...slot} />
  // }, [slots, activeItemIndex, arrowNavigation.getOnItemFocusByIndex])
  const navigationButtonsView = useMemo(() => {
    return slots.map((_slot, index) => {
      return (
        <NavigationButton
          key={index}
          index={index}
          active={activeItemIndex === index}
          setRef={arrowNavigation.setItemRef}
          onFocus={arrowNavigation.onItemFocus}
          onActivate={setActiveItemIndex}
        />
      )
    })
  }, [slots, activeItemIndex, arrowNavigation.setItemRef, arrowNavigation.onItemFocus, setActiveItemIndex])
  const classNames = useMemo(
    () =>
      cn(
        styles.slots,
        activeItemIndex > 0 && styles.leftHighlight,
        activeItemIndex < slots.length - 1 && styles.rightHighlight
      ),
    [activeItemIndex, slots.length]
  )

  const horisontalSwipeHandler = useCallback(
    (increase) => {
      let nextItemInReviewIndex

      if (increase) {
        if (activeItemIndex < maxSlots - 1) {
          nextItemInReviewIndex = activeItemIndex + 1
        } else {
          nextItemInReviewIndex = 0
        }
      } else if (activeItemIndex > 0) {
        nextItemInReviewIndex = activeItemIndex - 1
      } else {
        nextItemInReviewIndex = maxSlots - 1
      }

      setActiveItemIndex(nextItemInReviewIndex)
    },
    [activeItemIndex, maxSlots, setActiveItemIndex]
  )

  const horisontalSwipe = useHorisontalSwipe(horisontalSwipeHandler)

  const mainContainerWidth = useSelector(getMainContainerWidth)
  const sliderContainerPositionStyles = useMemo(() => {
    return {
      transform: `translateX(-${activeItemIndex * mainContainerWidth}px)`
    }
  }, [activeItemIndex, mainContainerWidth])

  return (
    <div className={classNames} {...horisontalSwipe}>
      <ul className={styles.sliderContainer} style={sliderContainerPositionStyles}>
        {slotsView}
      </ul>
      <aside className={styles.navigation} onKeyDown={arrowNavigation.onContainerKeyDown}>
        {navigationButtonsView}
      </aside>
    </div>
  )
})
