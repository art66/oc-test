import React, { memo, useCallback, useMemo } from 'react'
import cn from 'classnames'
import styles from './navigationButton.cssmodule.scss'
import { TProps } from './navigationButtonTypes'

export default memo((props: TProps) => {
  const { index, active, setRef, onFocus, onActivate } = props

  const setIndexedRef = useCallback((ref) => setRef(index, ref), [index, setRef])
  const onClick = useCallback(() => onActivate(index), [onActivate, index])
  const onFocusHandler = useCallback(() => onFocus(index), [index, onFocus])
  const ariaLabel = useMemo(() => `Select ${index} slot`, [index])

  return (
    <button
      type='button'
      ref={setIndexedRef}
      onFocus={onFocusHandler}
      className={cn(styles.button, active && styles.active)}
      onClick={onClick}
      aria-label={ariaLabel}
    />
  )
})
