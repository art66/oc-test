import React, { memo, useMemo } from 'react'
import { useSelector } from 'react-redux'
import styles from './index.cssmodule.scss'
import { getMaxSlots, getSlots } from '../../modules/selectors'
import Slot from '../../components/Slot'
import { useArrowNavigation } from '../../hooks'

export default memo(function Slots() {
  const slots = useSelector(getSlots)
  const maxSlots = useSelector(getMaxSlots)

  const arrowNavigation = useArrowNavigation({ lenght: maxSlots })

  const setsView = useMemo(
    () =>
      slots.map((slot, index) => (
        <Slot
          key={slot.slotId}
          index={index}
          className={styles.slot}
          setRef={arrowNavigation.setItemRef}
          onFocus={arrowNavigation.onItemFocus}
          {...slot}
        />
      )),
    [slots, arrowNavigation.setItemRef]
  )

  return (
    <ul className={styles.slots} onKeyDown={arrowNavigation.onContainerKeyDown}>
      {setsView}
    </ul>
  )
})
