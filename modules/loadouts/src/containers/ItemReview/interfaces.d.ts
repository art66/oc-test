import { TItemUpgrades, TUpgrades } from '../../modules/interfaces'

export interface IProps {}

export type TNormalizedAmmo = {
  ammo?: string
  ammoDescription: string
  ammoAmount: number
}

export interface IItemUpgradesSelector {
  itemId: string
  itemUpgrades: TItemUpgrades
  upgrades: TUpgrades
  upgradesLoaded: boolean
}
