import { createSelector } from 'reselect'
import identity from 'lodash/identity'
import { IItemUpgradesSelector } from './interfaces'
import { isArmoryAllowUpgrade } from '../../components/UpgradePanel/selectors'
import { createAddUpgrade, createBlankUpgrade } from '../../modules/utils'

export const itemUpgradesSelector = createSelector<IItemUpgradesSelector, IItemUpgradesSelector, any>(
  identity,
  ({ itemId, itemUpgrades, upgrades, upgradesLoaded }) => {
    const allowedUpgradesCount = upgrades.filter(({ armouryList }) =>
      isArmoryAllowUpgrade({ itemId, armoryList: armouryList })).length

    return [0, 1].reduce((normalizedUpgrades, i) => {
      if (itemUpgrades[i]) {
        normalizedUpgrades.push(itemUpgrades[i])
      } else if (upgradesLoaded) {
        if (i < allowedUpgradesCount) {
          normalizedUpgrades.push(createAddUpgrade(i))
        } else {
          normalizedUpgrades.push(createBlankUpgrade(i))
        }
      }

      return normalizedUpgrades
    }, [])
  }
)
