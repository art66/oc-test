/* eslint-disable max-len */
import React, { useCallback } from 'react'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { useSelector, useDispatch } from 'react-redux'
import cn from 'classnames'
import getItemGlowClassName from '@torn/shared/utils/getItemGlowClassName'
import styles from './index.cssmodule.scss'
import {
  getIsFirearms,
  getIsPermanentWeapon,
  getIsWeapon,
  getIsDefencive,
  getItemInReview,
  getUpgrades,
  upgradesLoadedSelector
} from '../../modules/selectors'
import Bonus from '../../components/Bonus'
import { ICONS_SETTINGS_SLIDER_ARROW } from './constants'
import { TOOLTIP_CONFIG, TOP_TOOLTIP_CONFIG } from '../../constants'
import ItemAbility from '../../components/ItemAbility'
import { slideItemInReviewAction } from '../../modules/actions'
import Upgrade from '../../components/Upgrade'
import ExpBar from '../../components/ExpBar'
import Ammo from '../../components/Ammo'
import { itemUpgradesSelector } from './selectors'

const leftArrowIcon = <SVGIconGenerator iconName='ArrowLeftSuperThin' preset={ICONS_SETTINGS_SLIDER_ARROW} />
const rightArrowIcon = <SVGIconGenerator iconName='ArrowRightSuperThin' preset={ICONS_SETTINGS_SLIDER_ARROW} />

const ItemReview = () => {
  const dispatch = useDispatch()

  const {
    uniqueId,
    type,
    imgLargeSrc,
    bonuses,
    upgrades,
    expPercent,
    damage,
    armor,
    currentAmmoType,
    clips,
    accuracy,
    name,
    isBlank,
    rarity
  } = useSelector(getItemInReview)
  const allUpgrades = useSelector(getUpgrades)
  const upgradesLoaded = useSelector(upgradesLoadedSelector)
  const itemUpgrades = itemUpgradesSelector({
    itemId: uniqueId,
    itemUpgrades: upgrades,
    upgrades: allUpgrades,
    upgradesLoaded
  })

  const upgradesView = () => {
    if (!isBlank && getIsFirearms(type)) {
      return itemUpgrades.map((upgrade, index) => {
        const id = `upgrade-${uniqueId}-${index}`

        return (
          <Upgrade
            key={id}
            itemId={uniqueId}
            index={index}
            className={styles[`upgrade${index}`]}
            {...upgrade}
            viewId={id}
            tooltipConfig={TOP_TOOLTIP_CONFIG}
            isBlank={isBlank}
          />
        )
      })
    }

    return null
  }
  const bonusesWeaponView = () =>
    !isBlank
    && getIsPermanentWeapon(type)
    && bonuses.map((bonus, index) => {
      const id = `bonus-${uniqueId}-${index}`

      return (
        <Bonus
          key={id}
          className={styles[`bonusWeapon${index}`]}
          {...bonus}
          viewId={id}
          tooltipConfig={TOP_TOOLTIP_CONFIG}
        />
      )
    })
  const bonusesDefenciveView = () =>
    !isBlank
    && getIsDefencive(type)
    && bonuses.map((bonus, index) => {
      const id = `bonus-${uniqueId}-${index}`

      return (
        <Bonus
          key={id}
          id={id}
          className={styles[`bonusDefence${index}`]}
          viewId={id}
          {...bonus}
          tooltipConfig={TOOLTIP_CONFIG}
        />
      )
    })

  const expBarView = () =>
    getIsWeapon(type) && typeof expPercent === 'number' && <ExpBar id={uniqueId} expPercent={expPercent} />
  const damageView = () =>
    getIsPermanentWeapon(type)
    && damage && <ItemAbility className={styles.damage} name='Damage' iconClassName={styles.demageIcon} value={damage} />
  const accuracyView = () =>
    getIsPermanentWeapon(type)
    && accuracy && (
      <ItemAbility className={styles.accuracy} name='Accuracy' iconClassName={styles.accuracyIcon} value={accuracy} />
    )
  const ammoView = () =>
    getIsWeapon(type) && (
      <Ammo itemId={uniqueId} currentAmmoType={currentAmmoType} type={type} clips={clips} isBlank={isBlank} />
    )
  const armorView = () =>
    getIsDefencive(type)
    && armor && <ItemAbility className={styles.armor} name='Armor' iconClassName={styles.armorIcon} value={armor} />
  const defencePanelView = () =>
    getIsDefencive(type) && (
      <div className={styles.defencePanel}>
        {armorView()}
        {bonusesDefenciveView()}
      </div>
    )

  const onArrowLeftHandler = useCallback(() => dispatch(slideItemInReviewAction(false)), [dispatch])
  const onArrowRightClick = useCallback(() => dispatch(slideItemInReviewAction(true)), [dispatch])

  return (
    <div className={styles.itemReview}>
      {upgradesView()}
      <div className={styles.type} aria-label={`Item type is "${type}"`}>
        {type}
      </div>
      {bonusesWeaponView()}
      <button type='button' className={styles.arrowLeft} onClick={onArrowLeftHandler} aria-label='Select previous item'>
        {leftArrowIcon}
      </button>
      <img className={cn(styles.img, getItemGlowClassName(rarity))} src={imgLargeSrc} alt={name} />
      <button type='button' className={styles.arrowRight} onClick={onArrowRightClick} aria-label='Select next item'>
        {rightArrowIcon}
      </button>
      {expBarView()}
      {damageView()}
      {ammoView()}
      {accuracyView()}
      {defencePanelView()}
    </div>
  )
}

export default ItemReview
