import { useCallback, useState } from 'react'
import { useReFocuser } from '../../hooks'
import { useEnterKey, useTabBlocker, useEscKey } from '../../hooks/wai'
import { useCombineListeners } from '../../hooks/wai/utils'

export function useWAI() {
  const [isActive, setActive] = useState(false)
  const [ref, setRef] = useState(null)

  const refReFocuser = useReFocuser(ref)

  const onToggle = useCallback(() => {
    setActive(!isActive)

    refReFocuser.focus()
  }, [setActive, refReFocuser.focus, isActive])
  const onDeactivate = useCallback(() => {
    setActive(false)

    refReFocuser.focus()
  }, [setActive, refReFocuser.focus])

  const onContainerClick = onToggle

  const onContainerKeyDown = useCombineListeners(useEnterKey(onToggle), useEscKey(onDeactivate), useTabBlocker(!isActive))

  return {
    isActive,
    setActive,
    ref,
    setRef,
    refReFocuser,
    onContainerClick,
    onContainerKeyDown
  }
}
