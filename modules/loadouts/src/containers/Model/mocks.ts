import { IArmourRaw } from '@torn/shared/components/UserModel/interfaces'

export const ARMOR: IArmourRaw = {
  1: {
    item: [
      {
        armouryID: '4712576496',
        armoryID: '4712576496',
        userID: '1',
        itemID: '484',
        type: 'Item',
        equiped: '1',
        ammo: '8',
        type2: 'Primary',
        dmg: 60.0372,
        ID: '484',
        arm: '0',
        accuracy: '74',
        acc: 41.515,
        equipSlot: '1',
        name: 'AK74u',
        maxammo: '30',
        ammotype: '9',
        rofmin: '4',
        rofmax: '6',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '3.5',
        slotsMap: '',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '6',
        upgrades: '',
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0,
        quality: 10.92
      }
    ]
  },
  2: {
    item: [
      {
        armouryID: '5227923198',
        armoryID: '5227923198',
        userID: '1',
        itemID: '255',
        type: 'Weapon',
        equiped: '2',
        ammo: '6',
        type2: 'Secondary',
        dmg: 265.3525,
        ID: '255',
        arm: '0',
        accuracy: '68',
        acc: 42.614,
        equipSlot: '2',
        name: 'Flamethrower',
        maxammo: '1',
        ammotype: '18',
        rofmin: '1',
        rofmax: '1',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '1.1',
        slotsMap: '',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '1',
        upgrades: '',
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 1,
        quality: 66.85,
        currentBonuses: {
          31: {
            title: 'Burn',
            hoverover: '<b>Burn</b><br/>39% chance to apply Burning',
            icon: 'burning'
          }
        },
        maxmugs: '4',
        currentUpgrades: [
          {
            title: 'Extra Clips x2',
            hoverover: '<b>Extra Clips x2</b><br/>+2 extra clips',
            upgradeID: '15',
            icon: 'extra-clip-x2'
          },
          {
            title: 'Tripod',
            hoverover: '<b>Tripod</b><br/>+2.00 Accuracy -30% Dexterity',
            upgradeID: '22',
            icon: 'tripod'
          }
        ],
        finalAccuracy: 2
      }
    ]
  },
  3: {
    item: [
      {
        armouryID: '5715552315',
        armoryID: '5715552315',
        userID: '1',
        itemID: '245',
        type: 'Item',
        equiped: '3',
        ammo: '0',
        type2: 'Melee',
        dmg: 3.00216,
        ID: '245',
        arm: '0',
        accuracy: '127',
        acc: 55.407,
        equipSlot: '3',
        name: 'Bo Staff',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '5.6',
        slotsMap: '',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '8',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0,
        quality: 8.86
      }
    ]
  },
  4: {
    item: [
      {
        armouryID: '3209651856',
        armoryID: '3209651856',
        userID: '1',
        itemID: '743',
        type: 'Item',
        equiped: '4',
        ammo: '0',
        type2: 'Clothing',
        dmg: '0',
        ID: '743',
        arm: '0',
        accuracy: '0',
        acc: '0',
        equipSlot: '4',
        name: "Christmas Sweater '15",
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["B2"]',
        changedSlotsMap: '["L3"]',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0
      }
    ]
  },
  5: {
    item: [
      {
        armouryID: '3390664753',
        armoryID: '3390664753',
        userID: '1',
        itemID: '616',
        type: 'Weapon',
        equiped: '5',
        ammo: '0',
        type2: 'Temporary',
        dmg: 70.5264,
        ID: '616',
        arm: '0',
        accuracy: '100',
        acc: 91.1495,
        equipSlot: '5',
        name: 'Trout',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '7.3',
        slotsMap: '',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '7',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0,
        quality: 15.26
      }
    ]
  },
  6: {
    item: [
      {
        armouryID: '5384169638',
        armoryID: '5384169638',
        userID: '1',
        itemID: '1027',
        type: 'Item',
        equiped: '6',
        ammo: '0',
        type2: 'Clothing',
        dmg: '0',
        ID: '1027',
        arm: '0',
        accuracy: '0',
        acc: '0',
        equipSlot: '6',
        name: 'Badge : 15th Anniversary',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["B6"]',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0
      }
    ]
  },
  7: {
    item: [
      {
        armouryID: '3000161316',
        armoryID: '3000161316',
        userID: '1',
        itemID: '648',
        type: 'Armour',
        equiped: '7',
        ammo: '0',
        type2: 'Defensive',
        dmg: '0',
        ID: '648',
        arm: 21.5595,
        accuracy: '0',
        acc: '0',
        equipSlot: '7',
        name: 'Leather Pants',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["L3"]',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0,
        quality: 31.19
      }
    ]
  },
  8: {
    item: [
      {
        armouryID: '4813856880',
        armoryID: '4813856880',
        userID: '1',
        itemID: '653',
        type: 'Item',
        equiped: '8',
        ammo: '0',
        type2: 'Defensive',
        dmg: '0',
        ID: '653',
        arm: 40.8115,
        accuracy: '0',
        acc: '0',
        equipSlot: '8',
        name: 'Combat Boots',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["F3"]',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0,
        quality: 56.23
      }
    ]
  },
  9: {
    item: [
      {
        armouryID: '3207417877',
        armoryID: '3207417877',
        userID: '1',
        itemID: '650',
        type: 'Armour',
        equiped: '9',
        ammo: '0',
        type2: 'Defensive',
        dmg: '0',
        ID: '650',
        arm: 22.863500000000002,
        accuracy: '0',
        acc: '0',
        equipSlot: '9',
        name: 'Leather Gloves',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["D3"]',
        changedSlotsMap: '["D2"]',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0,
        quality: 57.27
      }
    ]
  },
  10: {
    item: [
      {
        armouryID: '5190376054',
        armoryID: '5190376054',
        userID: '1',
        itemID: '53',
        type: 'Item',
        equiped: 10,
        ammo: '0',
        type2: 'Jewelry',
        dmg: '0',
        ID: '53',
        arm: '0',
        accuracy: '0',
        acc: '0',
        equipSlot: '6',
        name: 'Gold Ring',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["R3"]',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0
      }
    ]
  },
  11: {
    item: [
      {
        armouryID: '5726394059',
        armoryID: '5726394059',
        userID: '1',
        itemID: '55',
        type: 'Item',
        equiped: 11,
        ammo: '0',
        type2: 'Jewelry',
        dmg: '0',
        ID: '55',
        arm: '0',
        accuracy: '0',
        acc: '0',
        equipSlot: '6',
        name: 'Pearl Necklace',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["P3"]',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0
      }
    ]
  },
  12: {
    item: [
      {
        armouryID: '5763848588',
        armoryID: '5763848588',
        userID: '1',
        itemID: '60',
        type: 'Item',
        equiped: 12,
        ammo: '0',
        type2: 'Jewelry',
        dmg: '0',
        ID: '60',
        arm: '0',
        accuracy: '0',
        acc: '0',
        equipSlot: '6',
        name: 'Gold Watch',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["W3"]',
        changedSlotsMap: '',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0
      }
    ]
  },
  13: {
    item: [
      {
        armouryID: '5487471942',
        armoryID: '5487471942',
        userID: '1',
        itemID: '404',
        type: 'Item',
        equiped: 13,
        ammo: '0',
        type2: 'Clothing',
        dmg: '0',
        ID: '404',
        arm: '0',
        accuracy: '0',
        acc: '0',
        equipSlot: '6',
        name: 'Bandana',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '1',
        stealthLevel: '0',
        slotsMap: '["N3","M3"]',
        changedSlotsMap: '["B5"]',
        slotsMapMask: '',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0
      }
    ]
  },
  14: {
    item: [
      {
        armouryID: '5386949877',
        armoryID: '5386949877',
        userID: '1',
        itemID: '1025',
        type: 'Item',
        equiped: 14,
        ammo: '0',
        type2: 'Clothing',
        dmg: '0',
        ID: '1025',
        arm: '0',
        accuracy: '0',
        acc: '0',
        equipSlot: '6',
        name: 'Bathrobe',
        maxammo: '0',
        ammotype: '0',
        rofmin: '0',
        rofmax: '0',
        scary: '0',
        isDual: '0',
        isMask: '0',
        stealthLevel: '0',
        slotsMap: '["B5","L5"]',
        changedSlotsMap: '',
        slotsMapMask: 'bathrobe-mask',
        slotsMapMaskHair: '',
        weptype: '0',
        upgrades: null,
        armourNotCover: '',
        slotsMapBackground: '',
        roffixed: 0
      }
    ]
  },
  15: {
    item: [
      {
        itemID: 'moustache_5',
        ID: 'moustache_5',
        equiped: 15,
        type: 'Item',
        type2: 'Clothing',
        slotsMap: '["H-1"]'
      }
    ]
  },
  16: {
    item: [
      {
        itemID: 'hair_16',
        ID: 'hair_16',
        equiped: 16,
        type: 'Item',
        type2: 'Clothing',
        slotsMap: '["H-2"]'
      }
    ]
  },
  999: {
    item: [
      {
        type: 'Weapon',
        name: 'Fists',
        dmg: 1,
        accuracy: 100,
        acc: 50,
        ID: 999,
        stealthLevel: 3
      }
    ]
  },
  1000: {
    item: [
      {
        type: 'Weapon',
        name: 'Kick',
        dmg: 40,
        accuracy: 70,
        acc: 40,
        ID: 1000,
        stealthLevel: 3
      }
    ]
  }
}
