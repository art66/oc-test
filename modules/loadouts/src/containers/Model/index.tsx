import React, { memo, useMemo } from 'react'
import cn from 'classnames'
import UserModel from '@torn/shared/components/UserModel'
import { useSelector } from 'react-redux'
import styles from './index.cssmodule.scss'
import { getEquipedModelItems, getGender, getIsLoading, isDarkModeSelector } from '../../modules/selectors'
import { useOutsideAlerter } from '../../hooks'
import { useWAI } from './hooks'

export default memo(function Model() {
  const isDarkMode = useSelector(isDarkModeSelector)
  const equipedModelItems = useSelector(getEquipedModelItems)
  const gender = useSelector(getGender)
  const isLoading = useSelector(getIsLoading)

  const wai = useWAI()
  const outsideAlerter = useOutsideAlerter(() => wai.setActive(false))

  const className = useMemo(() => cn(styles.model, wai.isActive && styles.isFullSize), [wai.isActive])
  const modelView = useMemo(
    () =>
      !isLoading && (
        <div ref={wai.isActive ? outsideAlerter.setContainerRef : () => {}}>
          <UserModel
            classNameWrap={styles.userModel}
            gender={gender}
            equippedArmour={equipedModelItems}
            withContainer={true}
            isDarkMode={isDarkMode}
            isCentered={true}
          />
        </div>
      ),
    [isDarkMode, isLoading, gender, equipedModelItems, wai.isActive, outsideAlerter.setContainerRef]
  )
  const ariaLabel = useMemo(() => (!wai.isActive ? 'Model: Open' : 'Model: Close'), [wai.isActive])

  return (
    <div
      ref={wai.setRef}
      className={className}
      aria-label={ariaLabel}
      tabIndex={0}
      onKeyDown={wai.onContainerKeyDown}
      onClick={wai.onContainerClick}
    >
      {modelView}
    </div>
  )
})
