import { TProps } from '../../types'

export const initialState: TProps = {
  mediaType: 'desktop'
}

export const tabletState: TProps = {
  ...initialState,
  mediaType: 'tablet'
}

export const mobileState: TProps = {
  ...initialState,
  mediaType: 'mobile'
}
