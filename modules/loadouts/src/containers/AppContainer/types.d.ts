export type TProps = {
  mediaType: 'desktop' | 'tablet' | 'mobile'
}
