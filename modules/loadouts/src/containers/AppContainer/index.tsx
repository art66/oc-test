import React, { memo, useState, useCallback, useMemo, useEffect } from 'react'
import cn from 'classnames'
import ToolTip from '@torn/shared/components/TooltipNew'
import { useDispatch, useSelector } from 'react-redux'
import { SVGIconGenerator } from '@torn/shared/SVG'
import { BACK, DROPDOWN_ARROW_PASSIVE, DROPDOWN_ARROW_ACTIVE } from '@torn/shared/SVG/presets/icons/loadouts'
import { subscribeOnDarkMode, unsubscribeFromDarkMode } from '@torn/shared/utils/darkModeChecker'
import InfoBox from '@torn/shared/components/InfoBox'
import styles from './index.cssmodule.scss'
import CurrentSlot from '../CurrentSlot'
import { TOOLTIP_CONFIG } from '../../constants'
import Slots from '../Slots'
import { checkDarkModeAction, getEquippedItemsRequestAction } from '../../modules/actions'
import { getIsDesktop, getSlotName, infoBoxSelector } from '../../modules/selectors'
import SlotsSlider from '../SlotsSlider'
import { useStoragedState } from '../../hooks'

const backIconView = <SVGIconGenerator {...BACK} />
const dropdownArrowPassiveIconView = <SVGIconGenerator {...DROPDOWN_ARROW_PASSIVE} />
const dropdownArrowActiveIconView = <SVGIconGenerator {...DROPDOWN_ARROW_ACTIVE} />

export default memo(() => {
  const dispatch = useDispatch()

  useEffect(() => {
    subscribeOnDarkMode(payload => dispatch(checkDarkModeAction(payload)))

    dispatch(getEquippedItemsRequestAction())

    return () => {
      unsubscribeFromDarkMode()
    }
  }, [])

  const [contentIsShowing, slotShowContent] = useStoragedState('contentIsShowing', true)
  const [slotsIsShowing, slotShowSets] = useState(false)

  const slotName = useSelector(getSlotName)
  const isDesktop = useSelector(getIsDesktop)
  const infoBox = useSelector(infoBoxSelector)

  const onContentToggle = useCallback(() => slotShowContent(!contentIsShowing), [contentIsShowing])
  const onSetsToggle = useCallback(() => {
    slotShowSets(!slotsIsShowing)

    slotShowContent(true)
  }, [slotShowSets, slotsIsShowing, slotShowContent])

  const content = useMemo(() => {
    if (!slotsIsShowing) {
      return <CurrentSlot />
    }

    if (!isDesktop) {
      return <SlotsSlider />
    }

    return <Slots />
  }, [slotsIsShowing, isDesktop])
  const contentContainer = useMemo(() => contentIsShowing && <div className={styles.content}>{content}</div>, [
    contentIsShowing,
    content
  ])
  const setsButtonView = useMemo(
    () =>
      !slotsIsShowing ? (
        <button
          type='button'
          className={cn(styles.button, styles.setsIcon)}
          onClick={onSetsToggle}
          aria-label='Loadouts'
        />
      ) : (
        <button type='button' className={cn(styles.button, styles.backIcon)} onClick={onSetsToggle} aria-label='Back'>
          {backIconView}
        </button>
      ),
    [slotsIsShowing]
  )
  const collapseButtonView = useMemo(() => {
    if (!contentIsShowing) {
      return (
        <button type='button' className={styles.button} onClick={onContentToggle} aria-label='Open'>
          {dropdownArrowActiveIconView}
        </button>
      )
    }
    return (
      <button type='button' className={styles.button} onClick={onContentToggle} aria-label='Collapse'>
        {dropdownArrowPassiveIconView}
      </button>
    )
  }, [contentIsShowing])

  return (
    <>
      <InfoBox {...infoBox} showDangerously />
      <div className={styles.main}>
        <header className={cn(styles.header, { [styles.minimize]: !contentIsShowing })}>
          <p className={styles.title} role='heading' aria-level={2} tabIndex={0}>
            {slotName}
          </p>
          <nav className={styles.icons}>
            {setsButtonView}
            {collapseButtonView}
          </nav>
        </header>
        {contentContainer}
        <ToolTip {...TOOLTIP_CONFIG} />
      </div>
    </>
  )
})
