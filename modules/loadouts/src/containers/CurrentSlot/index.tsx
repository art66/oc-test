import React, { memo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import ItemReview from '../ItemReview'
import CurrentSlotItems from '../CurrentSlotItems'
import Model from '../Model'
import { getItemInReview, getIsDescsLoaded } from '../../modules/selectors'
import { getUpgradesDescsRequestAction, getUpgradesRequestAction } from '../../modules/actions'

export default memo(function AppContainer() {
  const dispatch = useDispatch()

  const itemInReview = useSelector(getItemInReview)
  const isDescsLoaded = useSelector(getIsDescsLoaded)

  useEffect(() => {
    if (!isDescsLoaded) {
      dispatch(getUpgradesDescsRequestAction())
    }

    dispatch(getUpgradesRequestAction())
  }, [])

  return (
    <section className={styles.currentSlot}>
      <div className={cn(styles.cell, styles.modelContainer)}>
        <div className={styles.model}>
          <Model />
        </div>
      </div>
      <div className={styles.rightPanel}>
        <div className={cn(styles.cell, styles.itemReviewContainer)}>{itemInReview && <ItemReview />}</div>
        <div className={cn(styles.cell, styles.itemsContainer)}>
          <div className={styles.items}>
            <CurrentSlotItems />
          </div>
        </div>
      </div>
    </section>
  )
})
