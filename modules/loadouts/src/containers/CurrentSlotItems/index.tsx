import React, { memo, useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import styles from './index.cssmodule.scss'
import itemStyles from '../../components/CurrentSlotItem/index.cssmodule.scss'
import {
  getEquipedItemsList,
  getMaxItemIndex,
  getEquipedItemsListLength,
  getAllItemsLength
} from '../../modules/selectors'
import CurrentSlotItem from '../../components/CurrentSlotItem'
import { useArrowNavigation } from '../../hooks'

export default memo(function CurrentSlotItems() {
  const maxItemIndex = useSelector(getMaxItemIndex)
  const equippedItems = useSelector(getEquipedItemsList)
  const listLength = useSelector(getEquipedItemsListLength)
  const allItemsLength = useSelector(getAllItemsLength)

  const arrowNavigation = useArrowNavigation({ lenght: allItemsLength })

  const setIndexedRef = useCallback(
    ref => {
      arrowNavigation.setItemRef(maxItemIndex, ref)
    },
    [maxItemIndex]
  )
  const onFocusHandler = useCallback(() => {
    arrowNavigation.onItemFocus(maxItemIndex)
  }, [maxItemIndex])

  const listItemsView = useMemo(
    () =>
      equippedItems
        .slice(0, maxItemIndex)
        .map((item, index) => (
          <CurrentSlotItem
            key={item.uniqueId}
            index={index}
            className={styles.item}
            setRef={arrowNavigation.setItemRef}
            onFocus={arrowNavigation.onItemFocus}
            {...item}
          />
        )),
    [equippedItems, maxItemIndex, arrowNavigation.setItemRef, arrowNavigation.onItemFocus]
  )
  const extraItemsCountView = useMemo(() => {
    if (equippedItems.length > maxItemIndex) {
      const extraItemsCount = listLength - maxItemIndex

      return (
        <div
          className={cn(itemStyles.item, styles.item, styles.extraCount)}
          ref={setIndexedRef}
          onFocus={onFocusHandler}
          tabIndex={0}
          aria-label={`Extra ${extraItemsCount} items not shown in this list.`}
        >
          +{extraItemsCount}
        </div>
      )
    }
  }, [equippedItems.length, maxItemIndex, listLength, setIndexedRef, onFocusHandler])

  return (
    <div className={styles.equippedItems} onKeyDown={arrowNavigation.onContainerKeyDown}>
      {listItemsView}
      {extraItemsCountView}
    </div>
  )
})
