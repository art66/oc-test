const subscribersList = {}

export const subscribe = (type, handler) => {
  if (!subscribersList[type]) {
    subscribersList[type] = []
  }

  subscribersList[type].push(handler)
}

const emit = (type, payload) => {
  if (subscribersList[type]) {
    subscribersList[type].forEach((subscriber) => {
      subscriber(payload)
    })
  }
}

export default () => (next) => (action) => {
  next(action)

  if (typeof action === 'object' && typeof action.type === 'string') {
    const { type, ...payload } = action

    emit(type, payload)
  }
}
