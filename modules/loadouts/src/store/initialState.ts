import subscribeOnDesktopLayout from '@torn/shared/utils/desktopLayoutChecker'
import { commonInitialState } from '../modules/reducers'
import { TState } from './types'

const initialState: TState = {
  common: commonInitialState
}

subscribeOnDesktopLayout(payload => (initialState.common.isDesktopLayoutSetted = payload))

export default initialState
