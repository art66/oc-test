import { applyMiddleware, compose, createStore } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'
import initialState from './initialState'
import saga, { rootSaga, runSaga } from './middleware/saga'
import logger from './middleware/reduxLogger'
import subscribeOnAction from './middleware/subscribeOnAction'
import makeRootReducer from './rootReducer'

const rootStore = () => {
  // ========================================================
  // Store and History Instantiation
  // ========================================================

  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [saga, thunk, subscribeOnAction]

  if (__DEV__) {
    middleware.push(logger)
  }

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  if (__DEV__ && window.__REDUX_DEVTOOLS_EXTENSION__) {
    enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(responsiveStoreEnhancer, applyMiddleware(...middleware), ...enhancers)
  )

  store.runSaga = runSaga
  runSaga(rootSaga)
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }

  return store
}

export default rootStore()
