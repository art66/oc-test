import { TCommonState } from '../modules/interfaces'
import { MOBILE_TYPE, TABLET_TYPE, DESKTOP_TYPE } from '../constants'

export type TMediaType = typeof MOBILE_TYPE | typeof TABLET_TYPE | typeof DESKTOP_TYPE

export interface TState {
  common: TCommonState
  browser?: {
    mediaType: TMediaType
  }
}
