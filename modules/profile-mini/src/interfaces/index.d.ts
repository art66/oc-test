import { TFactionRank } from '@torn/shared/components/UserInfo/interfaces'

interface IXAxis {
  offset: number
  coordinate: number
}

interface IYAxis {
  side: string
  coordinate: number
}

interface ILastAction {
  seconds: number | 'Unknown'
}

interface IUserFaction {
  id: number
  tag?: string
  tagImage?: string
  rank?: TFactionRank
}

export interface IPosition {
  x: IXAxis
  y: IYAxis
}

export interface ICoordinate {
  x: number
  y: number
}

export interface IUser {
  userID: string | number
  playerName: string
  profileImage: string
  awardImage: string
  faction?: IUserFaction
  level: number
  rank: {
    userRank: string
    userTitle: string
  }
  age: string
  role: string
  isFriend: boolean
  isEnemy: boolean
  signUp: number
  lastAction?: ILastAction
}

export interface ICurrentUser {
  playerName: string
  userID: string | number
  money: number
  canSendAnonymously: boolean
  loggedIn: boolean
}

export interface IIcon {
  id: number
  type: string
  title: string
  description: string
}

export interface IUserSettings {
  showImages: boolean
}
