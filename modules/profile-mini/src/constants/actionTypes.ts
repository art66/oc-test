export const FETCH_PROFILE_DATA = 'FETCH_PROFILE_DATA'
export const UPDATE_PROFILE_DATA = 'UPDATE_PROFILE_DATA'
export const SET_PROFILE_DATA = 'SET_PROFILE_DATA'
export const FETCH_PROFILE_DATA_FAILURE = 'FETCH_PROFILE_DATA_FAILURE'
export const RESET_STATE = 'RESET_STATE'
