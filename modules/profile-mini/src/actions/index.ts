import * as actionTypes from '../constants/actionTypes'

export const fetchMiniProfileRequest = (userID: number) => ({
  type: actionTypes.FETCH_PROFILE_DATA,
  userID
})

export const updateProfileData = () => ({
  type: actionTypes.UPDATE_PROFILE_DATA
})

export const fetchMiniProfileSuccess = (data: any) => ({
  type: actionTypes.SET_PROFILE_DATA,
  data
})

export const fetchMiniProfileFailure = () => ({
  type: actionTypes.FETCH_PROFILE_DATA_FAILURE
})

export const resetState = () => ({
  type: actionTypes.RESET_STATE
})
