import { updateProfileData } from './index'

declare const WebsocketHandler: any

const initWS = (dispatch: any): void => {
  WebsocketHandler.addEventListener('sidebar', 'onHospital', () => dispatch(updateProfileData()))
  WebsocketHandler.addEventListener('sidebar', 'onRevive', () => dispatch(updateProfileData()))
  WebsocketHandler.addEventListener('sidebar', 'onJail', () => dispatch(updateProfileData()))
  WebsocketHandler.addEventListener('sidebar', 'onLeaveFromJail', () => dispatch(updateProfileData()))
  WebsocketHandler.addEventListener('sidebar', 'updateMoney', () => dispatch(updateProfileData()))
}

export default initWS
