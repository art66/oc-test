import React from 'react'
import ReactDOM from 'react-dom'
import MiniProfile from './containers/AppContainer'

declare global { // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    renderMiniProfile: any
  }
}

// ========================================================
// Declare global function for rendering app
// ========================================================

window.renderMiniProfile = (element, props) => {
  ReactDOM.render(<MiniProfile eventProps={props} />, element)
}
