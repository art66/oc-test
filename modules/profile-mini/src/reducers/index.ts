import profileButtons, { ACTION_HANDLERS as buttonsActionHandlers } from '@torn/profile/src/modules/profileButtons'
import initialState from './intialState'
import {
  FETCH_PROFILE_DATA,
  UPDATE_PROFILE_DATA,
  SET_PROFILE_DATA,
  FETCH_PROFILE_DATA_FAILURE,
  RESET_STATE
} from '../constants/actionTypes'

const ACTION_HANDLERS = {
  [FETCH_PROFILE_DATA]: state => {
    return {
      ...state,
      loading: true
    }
  },
  [UPDATE_PROFILE_DATA]: state => {
    return {
      ...state,
      loading: true
    }
  },
  [SET_PROFILE_DATA]: (state, action) => {
    return {
      ...state,
      ...action.data,
      loading: false,
      profileButtons: {
        ...action.data.profileButtons,
        addUserDialog: state.profileButtons.addUserDialog,
        isVisibleCashDialog: state.profileButtons.isVisibleCashDialog,
        dialog: state.profileButtons.dialog,
        moneyAmount: state.profileButtons.moneyAmount,
        moneyDesc: state.profileButtons.moneyDesc,
        anonymous: state.profileButtons.anonymous
      }
    }
  },
  [FETCH_PROFILE_DATA_FAILURE]: state => {
    return {
      ...state,
      loading: false
    }
  },
  [RESET_STATE]: () => {
    return initialState
  }
}

export default function profileReducer(state: any = initialState, action: any) {
  const handler = ACTION_HANDLERS[action.type]

  if (!handler && buttonsActionHandlers[action.type]) {
    return {
      ...state,
      profileButtons: profileButtons(state.profileButtons, action)
    }
  }

  return handler ? handler(state, action) : state
}
