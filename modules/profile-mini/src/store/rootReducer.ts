import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'
import profileReducer from '../reducers'

export const makeRootReducer = (): any => {
  return combineReducers({
    browser: createResponsiveStateReducer({
      mobile: 600,
      tablet: 1000,
      desktop: 5000
    }),
    profile: profileReducer
  })
}


export default makeRootReducer
