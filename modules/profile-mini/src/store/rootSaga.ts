import { all } from 'redux-saga/effects'
import miniProfile from '../sagas'

export default function* rootSaga() {
  yield all([miniProfile()])
}
