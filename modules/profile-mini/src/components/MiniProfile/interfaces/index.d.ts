import IUserStatus from '@torn/profile/src/interfaces/IUserStatus'
import IProfileButtons from '@torn/profile/src/interfaces/IProfileButtons'
import { IPosition, ICurrentUser, IUser, IIcon, IUserSettings } from '../../../interfaces'

export interface IState {
  position: IPosition
  increaseZIndex: boolean
}

export interface IProps {
  profile: {
    loading: boolean
    profileButtons: IProfileButtons
    user: IUser
    currentUser: ICurrentUser
    icons: IIcon[]
    userStatus: IUserStatus
    settings: IUserSettings
  }
  eventProps: {
    userID: number | string
    event: any
  }
  showDialog: (text: string, confirmAction: string, color: string) => void
  hideDialog: () => void
  confirmDialog: () => void
  showSendCashDialog: () => void
  showButtonMsg: (msg: string) => void
  hideButtonMsg: () => void
  addToFriendList: (reason: string) => void
  addToEnemyList: (reason: string) => void
  setMoneyData: (moneyAmount: string, moneyDesc: string, anonymous: boolean) => void
  toggleAddUserDialog: (show: boolean, actionType: string) => void
  fetchMiniProfileRequest: (userID: number | string) => void
  resetState: () => void
}
