import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import cn from 'classnames'
import Preloader from '@torn/shared/components/Preloader'
import UserInfo from '@torn/shared/components/UserInfo'
import UserInfoBlock from '@torn/profile/src/components/UserInformation/UserInfoBlock'
import Level from '@torn/profile/src/components/UserInformation/Level'
import Rank from '@torn/profile/src/components/UserInformation/Rank'
import Age from '@torn/profile/src/components/UserInformation/Age'
import Icons from '@torn/profile/src/components/BasicInfoIconsList'
import ProfileButtons from '@torn/profile/src/components/ProfileButtons'
import UserStatus from '@torn/profile/src/components/UserStatus'
import DialoguesWrapper from '@torn/profile/src/components/DialoguesWrapper'
import * as profileButtonsActions from '@torn/profile/src/modules/profileButtons'

import { fetchMiniProfileRequest as fetchMiniProfileRequestAction, resetState as resetStateAction } from '../../actions'
import * as c from '../../constants'
import { IState, IProps } from './interfaces'
import { ICoordinate } from '../../interfaces'
import s from './index.cssmodule.scss'
import './index.scss'
import WithTooltip from '../WithTooltip'

class MiniProfile extends PureComponent<IProps, IState> {
  state = {
    position: {
      x: {
        offset: 0,
        coordinate: 0
      },
      y: {
        side: c.Y_TOP,
        coordinate: 0
      }
    },
    increaseZIndex: false
  }

  componentDidMount() {
    const { eventProps, fetchMiniProfileRequest } = this.props
    const increaseZIndex = !!eventProps.event.target.closest('#chatRoot, .recentHistory')

    fetchMiniProfileRequest(eventProps.userID)
    this.setContainerPosition()
    this.setState({ increaseZIndex })
  }

  componentWillUnmount() {
    const { resetState } = this.props

    resetState()
  }

  getUserInfoConfigObject = (ID, name, imageUrl) => ({ ID, name, imageUrl })

  getUserInfoConfig = () => {
    const {
      profile: { user }
    } = this.props
    const { faction, userID, playerName, awardImage } = user
    const tag = faction && !faction?.tagImage ? null : faction?.tag

    return {
      userConfig: this.getUserInfoConfigObject(userID, playerName, awardImage),
      factionConfig: faction ? { ID: faction.id, name: tag, imageUrl: faction.tagImage, rank: faction.rank } : null
    }
  }

  getOffsetX = (edgeRect: DOMRect, miniProfile: HTMLElement, clickPosition: ICoordinate): number => {
    const miniProfileHalfWidth = miniProfile.offsetWidth / 2

    if (clickPosition.x - miniProfileHalfWidth < edgeRect.left + c.MIN_OFFSET_FROM_PAGE_EDGE) {
      return clickPosition.x - miniProfileHalfWidth - edgeRect.left - c.MIN_OFFSET_FROM_PAGE_EDGE
    }

    if (clickPosition.x + miniProfileHalfWidth > edgeRect.left + edgeRect.width - c.MIN_OFFSET_FROM_PAGE_EDGE) {
      return clickPosition.x + miniProfileHalfWidth - (edgeRect.left + edgeRect.width) + c.MIN_OFFSET_FROM_PAGE_EDGE
    }
  }

  getPositionSideY = (edgeRect: DOMRect, miniProfile: HTMLElement, clickPosition: ICoordinate): string => {
    const topEdge = edgeRect.top < 0 ? 0 : edgeRect.top

    return topEdge > clickPosition.y - miniProfile.offsetHeight - c.DOP_OFFSET_Y ? c.Y_BOTTOM : c.Y_TOP
  }

  getCoordinateX = (miniProfile: HTMLElement, clickPosition: ICoordinate): number => {
    return clickPosition.x - miniProfile.offsetWidth / 2 + window.scrollX
  }

  getCoordinateY = (sideY: string, miniProfile: HTMLElement, clickPosition: ICoordinate): number => {
    switch (sideY) {
      case c.Y_BOTTOM:
        return clickPosition.y + c.DOP_OFFSET_Y + window.scrollY
      case c.Y_TOP:
        return clickPosition.y - (miniProfile.offsetHeight + c.DOP_OFFSET_Y) + window.scrollY
    }
  }

  getPositionX = (edgeRect: DOMRect, miniProfile: HTMLElement, clickPosition: ICoordinate) => {
    const offsetX = this.getOffsetX(edgeRect, miniProfile, clickPosition) || 0
    const coordinateX = this.getCoordinateX(miniProfile, clickPosition)

    return {
      offset: offsetX,
      coordinate: coordinateX
    }
  }

  getPositionY = (edgeRect: DOMRect, miniProfile: HTMLElement, clickPosition: ICoordinate) => {
    const positionSideY = this.getPositionSideY(edgeRect, miniProfile, clickPosition)
    const coordinateY = this.getCoordinateY(positionSideY, miniProfile, clickPosition)

    return {
      side: positionSideY,
      coordinate: coordinateY
    }
  }

  getClickPosition = () => {
    const {
      eventProps: { event }
    } = this.props

    if (event.type.includes(c.TOUCH_EVENT_PREFIX)) {
      const touch = event.touches[0] || event.changedTouches[0]

      return {
        x: touch.clientX,
        y: touch.clientY
      }
    }

    return {
      x: event.clientX,
      y: event.clientY
    }
  }

  setContainerPosition = (): void => {
    const clickPosition = this.getClickPosition()
    const miniProfile = document.querySelector('.mini-profile-wrapper') as HTMLElement
    const edgeX = document.querySelector('body') as HTMLElement
    const edgeY = document.querySelector('#mainContainer') as HTMLElement

    this.setState({
      position: {
        x: this.getPositionX(edgeX.getBoundingClientRect(), miniProfile, clickPosition),
        y: this.getPositionY(edgeY.getBoundingClientRect(), miniProfile, clickPosition)
      }
    })
  }

  renderUserInformation = () => {
    const {
      profile,
      showDialog,
      hideDialog,
      showButtonMsg,
      hideButtonMsg,
      toggleAddUserDialog,
      showSendCashDialog,
      addToFriendList,
      addToEnemyList,
      fetchMiniProfileRequest
    } = this.props
    const { user, icons, userStatus, profileButtons } = profile
    const { userConfig, factionConfig } = this.getUserInfoConfig()

    return (
      <>
        <div className={s.userImageWrapper}>
          <a href={`/userimages.php?XID=${user.userID}`} className={s.userImageLink}>
            {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
            <img src={user.profileImage} className={s.userImage} alt='Profile image' />
          </a>
        </div>
        <div className={s.userProfileWrapper}>
          <UserInfo
            user={userConfig}
            faction={factionConfig}
            status={{ mode: null }}
            customStyles={{ container: s.userNameWrapper }}
          />
          <div className={s.userInfoWrapper}>
            <WithTooltip id={`level-${user.userID}`} tooltipContent='Level'>
              <UserInfoBlock customClass='level'>
                <Level level={user.level} />
              </UserInfoBlock>
            </WithTooltip>
            <WithTooltip id={`rank-${user.userID}`} tooltipContent='Rank'>
              <UserInfoBlock customClass='rank'>
                <Rank rank={user.rank} />
              </UserInfoBlock>
            </WithTooltip>
            <WithTooltip id={`age-${user.userID}`} tooltipContent='Age'>
              <UserInfoBlock customClass='age'>
                <Age age={user.age} />
              </UserInfoBlock>
            </WithTooltip>
          </div>
          <Icons isMiniProfile={true} icons={icons} userID={user.userID} />
          <UserStatus isMiniProfile={true} user={user} userStatus={userStatus} />
          <ProfileButtons
            isMiniProfile={true}
            profileButtons={profileButtons}
            showButtonMsg={showButtonMsg}
            hideButtonMsg={hideButtonMsg}
            showDialog={showDialog}
            hideDialog={hideDialog}
            user={user}
            toggleAddUserDialog={toggleAddUserDialog}
            showSendCashDialog={showSendCashDialog}
            addToFriendList={addToFriendList}
            addToEnemyList={addToEnemyList}
            fetchProfileData={() => fetchMiniProfileRequest(user.userID)}
          />
        </div>
      </>
    )
  }

  renderMainContent = () => {
    const { profile, fetchMiniProfileRequest, ...restProps } = this.props

    return profile.loading ? (
      <Preloader />
    ) : (
      <DialoguesWrapper
        isMiniProfile={true}
        user={profile.user}
        currentUser={profile.currentUser}
        profileButtons={profile.profileButtons}
        loading={profile.loading}
        fetchProfileData={() => fetchMiniProfileRequest(profile.user.userID)}
        content={this.renderUserInformation()}
        {...restProps}
      />
    )
  }

  render() {
    const {
      position: { x, y },
      increaseZIndex
    } = this.state
    const xPosition = x.coordinate - x.offset

    return (
      <div
        style={{ left: xPosition, top: y.coordinate }}
        className={cn(s.wrapper, s[y.side], increaseZIndex && s.increaseZIndex, 'mini-profile-wrapper')}
      >
        <div className={s.arrow} style={{ left: `calc(50% - 16px + ${x.offset}px)` }} />
        {this.renderMainContent()}
      </div>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    profile: state.profile
  }
}

const mapDispatchToProps = {
  showDialog: profileButtonsActions.showDialog,
  hideDialog: profileButtonsActions.hideDialog,
  confirmDialog: profileButtonsActions.confirmDialog,
  showSendCashDialog: profileButtonsActions.showSendCashDialog,
  showButtonMsg: profileButtonsActions.showButtonMsg,
  hideButtonMsg: profileButtonsActions.hideButtonMsg,
  addToFriendList: profileButtonsActions.addToFriendList,
  addToEnemyList: profileButtonsActions.addToEnemyList,
  setMoneyData: profileButtonsActions.setMoneyData,
  toggleAddUserDialog: profileButtonsActions.toggleAddUserDialog,
  fetchMiniProfileRequest: fetchMiniProfileRequestAction,
  resetState: resetStateAction
}

export default connect(mapStateToProps, mapDispatchToProps)(MiniProfile)
