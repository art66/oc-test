import React, { Component, ReactNode } from 'react'
import Tooltip from '@torn/shared/components/Tooltip'

interface IProps {
  id: string
  tooltipContent: string | ReactNode
  children: ReactNode
}

class WithTooltip extends Component<IProps> {
  render() {
    const { id, tooltipContent, children } = this.props

    return (
      <>
        <Tooltip
          position='top'
          arrow='center'
          parent={id}
        >
          {tooltipContent}
        </Tooltip>
        <div id={id}>
          {children}
        </div>
      </>
    )
  }
}

export default WithTooltip
