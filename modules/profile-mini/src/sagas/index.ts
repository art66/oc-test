import { put, takeEvery, select } from 'redux-saga/effects'
import * as actionTypes from '../constants/actionTypes'
import { fetchMiniProfileSuccess } from '../actions'
import fetchUrl from '../utils/fetchURL'

function* fetchMiniProfile(action: any) {
  try {
    const { userID } = action
    const data = yield fetchUrl(`step=getUserNameContextMenu&XID=${userID}`)

    yield put(fetchMiniProfileSuccess(data))
  } catch (error) {
    console.error(error)
  }
}

function* updateMiniProfile() {
  try {
    const userID = yield select(state => state.profile?.user?.userID)

    if (!userID) return

    const data = yield fetchUrl(`step=getUserNameContextMenu&XID=${userID}`)

    yield put(fetchMiniProfileSuccess(data))
  } catch (error) {
    console.error(error)
  }
}

export default function* miniProfile() {
  yield takeEvery(actionTypes.FETCH_PROFILE_DATA, fetchMiniProfile)
  yield takeEvery(actionTypes.UPDATE_PROFILE_DATA, updateMiniProfile)
}
