/* global addRFC */
const MAIN_URL = '/profiles.php?'

export default function fetchUrl(url: string, data: object = {}) {
  // @ts-ignore
  return fetch(addRFC(MAIN_URL + url), {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.text().then(text => {
      try {
        const json = JSON.parse(text)

        return json
      } catch (e) {
        throw new Error(`Server responded with: ${text}`)
      }
    })
  })
}
