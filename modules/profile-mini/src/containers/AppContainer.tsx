import React, { PureComponent } from 'react'
import { Provider } from 'react-redux'
import rootStore from '../store/createStore'
import MiniProfile from '../components/MiniProfile'

interface IProps {
  eventProps: {
    userID: number | string,
    event: any
  }
}

class AppContainer extends PureComponent<IProps> {
  render() {
    const { eventProps } = this.props

    return (
      <Provider store={rootStore}>
        <MiniProfile eventProps={eventProps} />
      </Provider>
    )
  }
}

export default AppContainer
