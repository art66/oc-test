const path = require('path')
const { PWD, remoteStaticPath } = process.env
const APP_ROOT = `${path.resolve(PWD)}/src/`

module.exports = {
  app_index: `${APP_ROOT}index.html`,
  app_entry: `${APP_ROOT}main`,
  app_client: APP_ROOT,
  app_public: path.resolve(__dirname, '../', remoteStaticPath),
  app_local: '/',
  app_remote: remoteStaticPath.replace('static/', '/'),
  template_src_path: `${APP_ROOT}template.ejs`
}
