const { merge } = require('webpack-merge')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const localPaths = require('./localPaths')
const common = require('./webpack.common.js')

const REMOTE_TEMPLATE_NAME = 'app-template.php'
// ------------------------------------
// Optimization
// ------------------------------------
const compOptimization = {
  minimize: __PROD__, // webpack grab it only for production!
  splitChunks: {
    chunks: 'all',
    minChunks: 2
  },
  minimizer: [
    // webpack grab it only for production!
    new TerserPlugin({
      test: /\.(js|jsx|ts|tsx)?$/i,
      parallel: true,
      extractComments: false,
      terserOptions: {
        module: true,
        output: {
          ecma: '5',
          comments: false,
          beautify: false
        },
        compress: {
          ecma: '5',
          toplevel: true,
          dead_code: true
        }
      }
    }),
    new OptimizeCSSAssetsPlugin({})
  ]
}

// ------------------------------------
// Plugins
// ------------------------------------
const plugins = [
  new HtmlWebpackPlugin({
    template: localPaths.template_src_path,
    filename: REMOTE_TEMPLATE_NAME,
    publicPath: localPaths.app_remote,
    minify: false,
    inject: false
  })
]

const createConfig = () => ({
  mode: 'production',
  name: 'production configuration',
  optimization: compOptimization,
  plugins: plugins,
  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    publicPath: 'auto',
    path: localPaths.app_public
  },
  entry: {
    app: ['whatwg-fetch', localPaths.app_entry]
  }
})

module.exports = merge(common, createConfig())
