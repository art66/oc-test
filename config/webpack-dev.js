const webpack = require('webpack')
const { merge } = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const localPaths = require('./localPaths')
const commonConfig = require('./webpack.common.js')

// ------------------------------------
// Plugins
// ------------------------------------
const plugins = [
  new HtmlWebpackPlugin({
    template: localPaths.app_index,
    minify: false,
    inject: 'body'
  }),
  new webpack.HotModuleReplacementPlugin()
]

const devConfig = {
  mode: 'development',
  name: 'development configuration',
  plugins: plugins,
  output: {},
  entry: {
    app: [
      './src/main',
      'webpack-hot-middleware/client?reload=true&overlay=true&dynamicPublicPath=true&path=__webpack_hmr'
    ]
  }
}

module.exports = merge(commonConfig, devConfig)
