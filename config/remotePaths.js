const boxConfig = require('./boxConfig')
const { remoteStaticPath } = process.env

module.exports = {
  remote_static_base: `/home/${boxConfig.userFolderName}/torn/${remoteStaticPath}`
}
