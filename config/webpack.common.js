const webpack = require('webpack')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const globals = require('../globals/js/environments')
const localPaths = require('./localPaths')

const webpackConfig = {
  target: ['web', 'es5'],
  stats: {}, // stats is overridden by webpack-dev-middleware! No need to duplicate them here.
  devtool: false,
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    'react-dom/server': 'ReactDOMServer'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)?$/,
        loader: 'ts-loader',
        options: {
          compilerOptions: {
            target: 'ES5'
          }
        },
        exclude: [/node_modules/]
      },
      {
        type: 'javascript/auto',
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false
            }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.scss$/,
        exclude: /\.cssmodule\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
              modules: {
                mode: 'global'
              }
            }
          },
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              additionalData: '@import "styles/index.scss";',
              sassOptions: {
                includePaths: [path.resolve(__dirname, '../globals')]
              }
            }
          }
        ]
      },
      {
        test: /(\.cssmodule\.scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-modules-typescript-loader',
          {
            loader: 'css-loader',
            options: {
              url: false,
              modules: {
                localIdentName: process.env.isGlobal ?
                  `${process.env.localPath}_[local]___[hash:base64:5]` :
                  '[local]___[hash:base64:5]'
              }
            }
          },
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              additionalData: '@import "styles/index.scss";',
              sassOptions: {
                includePaths: [path.resolve(__dirname, '../globals')]
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[name].css'
    }),
    new webpack.DefinePlugin(globals),
    new webpack.SourceMapDevToolPlugin({
      test: /\.(js|jsx|ts|tsx|json|css)$/,
      filename: '[file].map[query]',
      exclude: ['vendor.js']
    })
  ],
  performance: {
    hints: false
  },
  resolve: {
    modules: [localPaths.app_client, 'node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
  }
}

module.exports = webpackConfig
