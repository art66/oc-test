module.exports = {
  useTabs: false,
  printWidth: 120,
  tabWidth: 2,
  singleQuote: true,
  jsxSingleQuote: true,
  arrowParens: 'avoid',
  trailingComma: 'none',
  jsxBracketSameLine: false,
  semi: false
}
