import React from 'react'
import Loadable from 'react-loadable'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/services/rootReducer'
import reducer from './modules/reducers'
import initWS from './modules/websockets'

// Your preloader should be here
const Preloader = () => <div />

const MainRoute = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const MainRouteComponent = await import(/* webpackChunkName: "mainRoute" */ './containers/layout')

    console.log('sdsd')

    // sync reducers injection
    injectReducer(rootStore, { reducer, key: 'main-route' })
    initWS(rootStore)

    return MainRouteComponent
  },
  loading: Preloader
})

export default MainRoute
