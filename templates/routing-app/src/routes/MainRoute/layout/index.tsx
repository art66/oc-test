import React from 'react'
import { Link } from 'react-router-dom'

// import styles from './index.cssmodule.scss'

import { IProps } from './interfaces'
import { IFetchAttempt } from '../modules/interfaces'

// the main endpoint of route injection
class AppLayout extends React.Component<IProps> {
  componentDidMount() {
    console.log('sdsd')
    const { fetchAttempt } = this.props

    fetchAttempt({} as IFetchAttempt)
  }

  render() {
    return (
      <div>
        Hello! Im a main route Path!!!
        <Link to='/exampleSubRoute'>
          SubRoute Link
        </Link>
      </div>
    )
  }
}

export default AppLayout
