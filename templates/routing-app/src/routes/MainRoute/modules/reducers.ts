// some reducers here. The same link in on top level
import initialState from './initialState'

import { FETCH_ATTEMPT, FETCH_SAVED } from '../constants'
import { IState, IFetchAttempt, IFetchSaved } from './interfaces'

// ------------------------------------
// Action Handlers
// ------------------------------------
// you need to fill all of your reducers cases inside this object like an arrow functions
const ACTION_HANDLERS = {
  [FETCH_ATTEMPT]: (state: IState, action: IFetchAttempt) => ({
    ...state,
    ...action
  }),
  [FETCH_SAVED]: (state: IState, action: IFetchSaved) => ({
    ...state,
    ...action
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
// you need to place here right typization and initialState bind
const reducer = (state: any = initialState, action: any) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
