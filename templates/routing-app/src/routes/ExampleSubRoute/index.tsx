import React from 'react'
import Loadable from 'react-loadable'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/services/rootReducer'
import reducer from './modules/reducers'
import initWS from './modules/websockets'

const Preloader = () => <div />

const ExampleSubRoute = Loadable({
  loader: async () => {
    // tslint:disable-next-line:space-in-parens
    const ExampleSubRouteComponent = await import(/* webpackChunkName: "exampleSubRoute" */ './containers/layout')

    // sync reducers injection
    injectReducer(rootStore, { reducer, key: 'exampleSubRoute' })
    initWS(rootStore)

    return ExampleSubRouteComponent
  },
  loading: Preloader
})

export default ExampleSubRoute
