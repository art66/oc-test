import React from 'react'
import { Link } from 'react-router-dom'
import { IFetchAttempt } from '../../MainRoute/modules/interfaces'

// import styles from './index.cssmodule.scss'

import { IProps } from './interfaces'

// the main endpoint of route injection
class AppLayout extends React.Component<IProps> {
  componentDidMount() {
    const { fetchAttempt } = this.props

    fetchAttempt({} as IFetchAttempt)
  }

  render() {
    return (
      <div>
        Hello! Im a sub route Path!!!
        <Link to='/'>
          MainRoute Link
        </Link>
      </div>
    )
  }
}

export default AppLayout
