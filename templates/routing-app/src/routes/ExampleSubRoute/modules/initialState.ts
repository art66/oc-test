import { IState } from './interfaces'

const initialState: IState = {
  payload: null
}

export default initialState
