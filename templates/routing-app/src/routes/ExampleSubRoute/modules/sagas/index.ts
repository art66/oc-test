import { takeLatest } from 'redux-saga/effects'

import loadMainData from './loadMainData'

import { FETCH_ATTEMPT } from '../../constants'

export default function* rootSaga() {
  yield takeLatest(FETCH_ATTEMPT, loadMainData)
}
