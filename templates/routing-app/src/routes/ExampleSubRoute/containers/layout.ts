import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IFetchAttempt } from '../modules/interfaces'

import AppLayout from '../layout'

import { loadDataAttempt } from '../modules/actions'

const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchAttempt: (payload: IFetchAttempt) => dispatch(loadDataAttempt(payload))
})

export default connect(null, mapDispatchToProps)(AppLayout)
