// Here you can place any global constants.
// Please, note that for constant used in a separate folders only
// you probably would like to create its own constants folder either.

// --------------------------
// REDUX NAMESPACES
// --------------------------
export const SHOW_DEBUG_BOX = 'SHOW_DEBUG_BOX'
export const HIDE_DEBUG_BOX = 'HIDE_DEBUG_BOX'
export const SHOW_INFO_BOX = 'SHOW_INFO_BOX'
export const HIDE_INFO_BOX = 'HIDE_INFO_BOX'
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE'
export const MEDIA_SCREEN_CHANGED = 'redux-responsive/CALCULATE_RESPONSIVE_STATE'
export const ROUTE_HEADER_SET = 'ROUTE_HEADER_SET'
export const MANUAL_DESKTOP_MODE = 'MANUAL_DESKTOP_MODE'

// --------------------------
// APP HEADER PATHS IDs
// --------------------------
export const PATHS_ID = {
  '': 1,
  exampleSubRoute: 2
}

// --------------------------
// LIST WITHOUT SIDEBAR
// --------------------------
export const PATHS_WITHOUT_SIDEBAR = []
