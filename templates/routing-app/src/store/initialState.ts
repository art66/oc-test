import currentLocation from '../utils/getWindowLocation'
import { IInitialState } from '../interfaces/IStore'
import { PATHS_ID } from '../constants'

const initialState: IInitialState = {
  locationCurrent: currentLocation,
  isDesktopManualLayout: false,
  appID: 'router-app',
  pageID: PATHS_ID[currentLocation],
  debug: null,
  info: null
}

export default initialState
