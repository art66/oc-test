export interface IInitialState {
  isDesktopManualLayout?: any
  locationCurrent?: string
  appID?: string
  pageID?: number
  info?: string
  debug?: {
    msg: string | object
  }
  debugCloseAction?: () => void
}
