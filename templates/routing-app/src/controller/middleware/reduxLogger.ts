import { createLogger } from 'redux-logger'

// Can log all the redux actions dispatches by displaying them inside browser's console panel
const logger = createLogger({
  collapsed: true,
  timestamp: false,
  diff: true
})

export default logger
