import React from 'react'
import { Dispatch, Store } from 'redux'
import { connect } from 'react-redux'

import AppHeader from '@torn/shared/components/AppHeader'
import DebugBox from '@torn/shared/components/DebugBox'
import InfoBox from '@torn/shared/components/InfoBox'
import { subscribeOnDesktopLayout, unsubscribeOnDesktopLayout } from '@torn/shared/utils/desktopLayoutChecker'

import Sidebar from '../components/Sidebar'
import Welcome from '../components/Welcome'

import { infoShow, debugHide, checkManualDesktopMode } from '../controller/actions/common'
import { IProps, IContainerStore } from './interfaces'

import styles from './index.cssmodule.scss'
import '../styles/global.cssmodule.scss'

class AppLayout extends React.PureComponent<IProps> {
  componentDidMount() {
    this._checkManualDesktopMode()
  }

  componentWillUnmount() {
    unsubscribeOnDesktopLayout()
  }

  _checkManualDesktopMode = () => {
    const { checkManualMode } = this.props

    subscribeOnDesktopLayout((payload: boolean) => checkManualMode(payload))
  }

  // TODO: you can cut it if your app doesn't need this to include!
  _renderSidebar = () => {
    return <Sidebar />
  }

  _renderHeader = () => {
    const { appID, pageID } = this.props

    const APP_HEADER = {
      active: true,
      titles: {
        default: {
          ID: 0,
          title: '<YOUR_APP_TITLE>'
        }
      }
    }

    return <AppHeader appID={appID} pageID={pageID} clientProps={APP_HEADER} />
  }

  _renderInfoDebugAreas = () => {
    const { debug, debugCloseAction, info } = this.props

    return (
      <React.Fragment>
        {debug && <DebugBox debugMessage={debug} close={debugCloseAction} isBeatifyError={true} />}
        {info && <InfoBox msg={info} />}
      </React.Fragment>
    )
  }

  _renderBody = () => {
    return (
      <div className={styles.bodyWrap}>
        <Welcome />
      </div>
    )
  }

  render() {
    return (
      <div className={styles.appWrap}>
        {this._renderSidebar()}
        {this._renderHeader()}
        {this._renderInfoDebugAreas()}
        {this._renderBody()}
      </div>
    )
  }
}

const mapStateToProps = (state: IContainerStore & Store<any>) => ({
  appID: state.common.appID,
  pageID: state.common.pageID,
  debug: state.common.debug,
  info: state.common.info,
  mediaType: state.browser.mediaType
})

const mapDispatchToState = (dispatch: Dispatch) => ({
  checkManualMode: (status: boolean) => dispatch(checkManualDesktopMode({ status })),
  debugCloseAction: () => dispatch(debugHide()),
  infoBoxShow: (msg: string) => dispatch(infoShow(msg))
})

export default connect(
  mapStateToProps,
  mapDispatchToState
)(AppLayout)
