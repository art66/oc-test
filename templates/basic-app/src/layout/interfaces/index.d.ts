import { TMediaType } from '@torn/shared/utils/checkMediaType/interfaces'
import { IInitialState } from '../../interfaces/IStore'

export interface IProps extends IInitialState {
  checkManualMode: (status: boolean) => void
  infoBoxShow: (msg: string) => void
  mediaType: TMediaType
}

export interface IContainerStore {
  common: IInitialState
  browser: any
}
