import React from 'react'
import ReactDOM from 'react-dom'
import RedBox from 'redbox-react'

import rootStore from './store/createStore'
import AppContainer from './core'

declare global { // fix for missing windows methods in TypeScript for some reasons
  // tslint:disable-next-line:naming-convention
  interface Window {
    state: any
  }
}

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('react-root')

console.log(rootStore, 'store')

let render = () => {
  ReactDOM.render(
    <AppContainer store={rootStore} />,
    MOUNT_NODE
  )
}

// ========================================================
// Developer Tools Setup
// ========================================================
if (__DEV__) {
  // importing real sidebar app bundle only for developing stage
  // import('../../sidebar/src/main')
  window.state = rootStore.getState()
}

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderApp = render
    const renderError = (error: any) => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./core', render)
  }
}

// ========================================================
// Go!
// ========================================================
render()
