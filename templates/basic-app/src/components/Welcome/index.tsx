// TODO: this is just an example of ho to structure your components. Delete me once the app will be ready :)
import React from 'react'

import { IOwnProps, IProps } from './interfaces'
import styles from './index.cssmodule.scss'

const WELCOME_MESSAGE = `It's your time to code here anything. Have Fun! :)`

class ExampleComponent extends React.Component<IOwnProps & IProps> {
  _renderTornLogo = () => {
    return (
      <div className={styles.logoWrap}>
        <img
          className={styles.logo}
          src='https://dev-www.torn.com/images/v2/header/logo/976_default.png'
          alt='torn_logo'
        />
      </div>
    )
  }

  _renderWelcome = () => {
    return <div className={styles.text}>{WELCOME_MESSAGE}</div>
  }

  render() {
    return (
      <div className={styles.welcomeWrap}>
        {this._renderTornLogo()}
        {this._renderWelcome()}
      </div>
    )
  }
}

export default ExampleComponent
