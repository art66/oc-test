import React from 'react'
import ReactDOM from 'react-dom'
import SidebarGlobal from '@torn/sidebar/src/containers/SidebarAsync'

import rootStore from '../../store/createStore'
import { injectReducer } from '../../store/services/rootReducer'

class Sidebar extends React.Component {
  render() {
    return ReactDOM.createPortal(
      <SidebarGlobal rootStore={rootStore} injector={injectReducer} />,
      document.querySelector('#sidebarroot')
    )
  }
}

export default Sidebar
