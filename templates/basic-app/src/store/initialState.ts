import currentLocation from '../utils/getWindowLocation'
import { IInitialState } from '../interfaces/IStore'

const initialState: IInitialState = {
  isDesktopManualLayout: false,
  locationCurrent: currentLocation,
  appID: 'basic-app',
  pageID: null,
  debug: null,
  info: null
}

export default initialState
