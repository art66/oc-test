import { all } from 'redux-saga/effects'

import exampleSaga from '../../controller/sagas'

export default function* rootSaga() {
  yield all([exampleSaga()])
}
