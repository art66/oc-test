import initialState from '../../store/initialState'
import locationPath from '../helpers/locationPath'

import { IDebugShow, IInfoShow, IType, ILocationChange } from '../../interfaces/IController'
import { IInitialState } from '../../interfaces/IStore'

import {
  PATHS_ID,
  SHOW_DEBUG_BOX,
  HIDE_DEBUG_BOX,
  SHOW_INFO_BOX,
  HIDE_INFO_BOX,
  LOCATION_CHANGE
} from '../../constants'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SHOW_DEBUG_BOX]: (state: IInitialState, action: IDebugShow) => ({
    ...state,
    debug: action.msg
  }),
  [HIDE_DEBUG_BOX]: (state: IInitialState) => ({
    ...state,
    debug: null
  }),
  [SHOW_INFO_BOX]: (state: IInitialState, action: IInfoShow) => ({
    ...state,
    info: action.msg
  }),
  [HIDE_INFO_BOX]: (state: IInitialState) => ({
    ...state,
    info: null
  }),
  [LOCATION_CHANGE]: (state: IInitialState, action: ILocationChange) => {
    const currentPath = locationPath(action.payload.location.hash)

    return {
      ...state,
      debug: null,
      info: null,
      locationCurrent: currentPath,
      pageID: PATHS_ID[currentPath]
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const reducer = (state: IInitialState = initialState, action: IType) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
