import { render } from '@testing-library/react'
import React from 'react'
import { createMemoryHistory } from 'history'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { Router } from 'react-router'

const renderWithWrappers = (
  ui,
  { route = '/', history = createMemoryHistory({ initialEntries: [route] }) } = {},
  { initialState, store = createStore(() => {}, initialState) } = {}
) => {
  return {
    ...render(
      <Provider store={store}>
        <Router history={history}>{ui}</Router>
      </Provider>
    ),
    history,
    store
  }
}

const mockWindowProperty = (property, value) => {
  const { [property]: originalProperty } = window

  delete window[property]
  beforeAll(() => {
    Object.defineProperty(window, property, {
      configurable: true,
      writable: true,
      value
    })
  })
  afterAll(() => {
    window[property] = originalProperty
  })
}

export { renderWithWrappers, mockWindowProperty }
