const { REACT_TEMPLATES_PATH, REACT_MODULES_PATH, REACT_APPS_PATH } = require('../globals/constants')

const getFolderPath = () => {
  const { ENTITY_TYPE, APP_TYPE } = process.env

  const type = ENTITY_TYPE || APP_TYPE

  if (type === 'template' || type === 'templates') {
    return REACT_TEMPLATES_PATH
  } else if (type === 'module' || type === 'modules') {
    return REACT_MODULES_PATH
  }

  return REACT_APPS_PATH
}

module.exports = getFolderPath
